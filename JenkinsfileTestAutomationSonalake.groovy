String slackChannel = "#qa-fineos"

pipeline {
  agent {
    label 'linux && node'
  }

  options {
    timestamps()
    buildDiscarder(logRotator(numToKeepStr: '10'))
    timeout(time: 7, unit: 'HOURS')
  }

  parameters {
    string(
      name: 'BRANCH',
      defaultValue: 'origin/develop',
      description: 'Branch name used for tests.'
    )
    string(
      name: 'TEST_DATA_REPO_BRANCH',
      defaultValue: 'origin/develop',
      description: 'Branch name used to fetch test data from fineos-portals-test-data repository.'
    )
    string(
      name: 'CUCUMBER_TAGS',
      defaultValue: params.CUCUMBER_TAGS,
      description: 'Cucumber tests tags to be executed.'
    )
    string(
      name: 'TESTS_CONFIG',
      defaultValue: 'jenkinsConfigHeadless.js',
      description: 'Protractor configuration used for tests.'
    )
    string(
      name: 'TEST_ENV',
      defaultValue: params.TEST_ENV,
      description: 'Name of Employer Portal environment used for tests.'
    )
    choice(
      name: 'BROWSER_RESOLUTION',
      choices: ['1366,768', '1024,768', '1920,1080'],
      description: 'browser resolution'
    )
    string(
      name: 'EMAIL_RECIPIENTS',
      defaultValue: params.EMAIL_RECIPIENTS,
      description: 'List of email recipients.'
    )
  }

  stages {
    stage('Checkout Project') {
      steps {
        checkout([
            $class: 'GitSCM', branches: [[name: BRANCH]],
            userRemoteConfigs: [
                [url: 'git@bitbucket.org:fineoshackers/fineos-portals.git', credentialsId:'066dc64d-086d-4ced-a921-c03a7bffb995']
            ]
        ])
      }
    }
    stage('Checkout Test Data') {
      steps {
        dir('packages/fineos-common/e2e/employer-portal/testData/generatedTestData') {
          checkout([
              $class: 'GitSCM', branches: [[name: TEST_DATA_REPO_BRANCH]],
              userRemoteConfigs: [
                  [url: 'git@bitbucket.org:fineoshackers/fineos-portals-test-data.git', credentialsId:'066dc64d-086d-4ced-a921-c03a7bffb995']
              ]
          ])
        }
      }
    }
    stage('Run Employer Portal e2e tests') {
      steps {
        script {
            sh "./gradlew run_e2e_tests -PtestsConfig=${TESTS_CONFIG} -PtestEnv=${TEST_ENV} -PcucumberTags=${CUCUMBER_TAGS} -Presolution=${BROWSER_RESOLUTION}"
        }
      }
    }
  }

  post {
    always {
        cucumber buildStatus: 'UNSTABLE', failedScenariosNumber: 1,
            classifications: [
                [$class: 'Classification', key: 'Environment:', value: TEST_ENV],
                [$class: 'Classification', key: 'Cucumber tags:', value: CUCUMBER_TAGS]
            ],
            fileIncludePattern: '**/dailyTestReport.*.json',
            jsonReportDirectory: 'packages/fineos-common/e2e/reports',
            sortingMethod: 'ALPHABETICAL'
    }

    success {
        slackSend channel: slackChannel,
            color: 'good',
            message: "Job: ${env.JOB_NAME} with build number ${env.BUILD_NUMBER} has succeeded. Check cucumber report at ${env.BUILD_URL}"
    }

    unstable {
        slackSend channel: slackChannel,
            color: '#f04c3f',
            message: "Job: ${env.JOB_NAME} with build number ${env.BUILD_NUMBER} is unstable. Check cucumber report at ${env.BUILD_URL}"

        notifyEmailRecipients ("UNSTABLE", [EMAIL_RECIPIENTS])
    }

    failure {
        slackSend channel: slackChannel,
            color: '#f04c3f',
            message: "Job: ${env.JOB_NAME} with build number ${env.BUILD_NUMBER} has failed. Check cucumber report at ${env.BUILD_URL}"

        notifyEmailRecipients ("FAILED", [EMAIL_RECIPIENTS])
    }

    cleanup {
        cleanWs()
    }
  }
}

def notifyEmailRecipients(msg, recipientsList) {
    mail (
        subject: "${msg}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: """<p>${msg}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
        <p>Check cucumber report at "<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p>""",
        to: recipientsList.join(', '),
        cc: '',
        bcc: '',
        charset: 'UTF-8',
        from: '',
        mimeType: 'text/html',
        replyTo: ''
    )
}
