pipeline {
  agent {
    label 'linux && node'
  }

  options {
    timestamps()
    buildDiscarder(logRotator(numToKeepStr: '20'))
    timeout(time: 4, unit: 'HOURS')
  }

  parameters {
    string(
      name: 'CUCUMBER_TAGS',
      defaultValue: '@regression',
      description: 'Cucumber tests tags to be executed.'
    )
    string(
      name: 'TEST_ENV',
      defaultValue: params.TEST_ENV,
      description: 'Name of Employer Portal environment used for tests.'
    )
    string(
      name: 'TEST_DATA_REPO_BRANCH',
      defaultValue: 'origin/develop',
      description: 'Branch name used to fetch test data from fineos-portals-test-data repository.'
    )
    choice(
      name: 'BROWSER_RESOLUTION',
      choices: ['1366,768', '1024,768', '1920,1080'],
      description: 'browser resolution'
    )
    string(
      name: 'EMAIL_RECIPIENTS',
      defaultValue: params.EMAIL_RECIPIENTS,
      description: 'List of email recipients. Define email addresses in EMAIL_RECIPIENTS parameter (jenkins UI)'
    )
  }

  stages {
    stage('Checkout Test Data') {
      steps {
        dir('packages/fineos-common/e2e/employer-portal/testData/generatedTestData') {
          checkout([
              $class: 'GitSCM', branches: [[name: TEST_DATA_REPO_BRANCH]],
              userRemoteConfigs: [
                  [url: 'git@bitbucket.org:fineoshackers/fineos-portals-test-data.git', credentialsId:'066dc64d-086d-4ced-a921-c03a7bffb995']
              ]
          ])
        }
      }
    }
    stage('Run Employer Portal e2e tests') {
      steps {
        script {
            sh "./gradlew run_e2e_tests -PtestsConfig=jenkinsConfigHeadless.js -PtestEnv=${TEST_ENV} -PcucumberTags=${CUCUMBER_TAGS} -Presolution=${BROWSER_RESOLUTION}"
        }
      }
    }
  }

  post {
    always {
        cucumber buildStatus: 'UNSTABLE', failedScenariosNumber: 1,
            classifications: [
                [$class: 'Classification', key: 'Environment:', value: TEST_ENV],
                [$class: 'Classification', key: 'Cucumber tags:', value: CUCUMBER_TAGS]
            ],
            fileIncludePattern: '**/dailyTestReport.*.json',
            jsonReportDirectory: 'packages/fineos-common/e2e/reports',
            sortingMethod: 'NATURAL'
    }

    unstable {
         notifyEmailRecipients ("UNSTABLE", [EMAIL_RECIPIENTS])
    }

    failure {
         notifyEmailRecipients ("FAILED", [EMAIL_RECIPIENTS])
    }

    cleanup {
        cleanWs()
    }
  }
}

def notifyEmailRecipients(msg, recipientsList) {
    mail (
        subject: "${msg}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: """<p>${msg}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
        <p>Check cucumber report at "<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p>""",
        to: recipientsList.join(', '),
        cc: '',
        bcc: '',
        charset: 'UTF-8',
        from: '',
        mimeType: 'text/html',
        replyTo: ''
    )
}
