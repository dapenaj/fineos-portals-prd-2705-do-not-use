pipeline {
  agent {
    label 'linux && !fast-aws-deploy && !build6'
  }

  triggers {
		pollSCM 'H/5 * * * *'
  }

  parameters {
    string(
      name: 'buildBranch',
      defaultValue: 'develop',
      description: 'What branch to build'
    )
	  booleanParam(
      name: 'incrementFineosCommonDevVersion',
      defaultValue: false,
      description: 'Should the fineos-common dev version be incremented?'
    )
    booleanParam(
      name: 'incrementPortalDevVersion',
      defaultValue: true,
      description: 'Should the portals dev version be incremented in a smart way?'
    )
//     booleanParam(
//       name: 'incrementCustomerPortalDevVersion',
//       defaultValue: false,
//       description: 'Should the customer-portal dev version be incremented?'
//     )
    booleanParam(
      name: 'deployFineosCommonToNexus',
      defaultValue: true,
      description: 'Should the fineos-common be deployed to nexus?'
    )
    string(
      name: 'internalPublishingRepo',
      defaultValue: 'http://atlanta.sonalake.corp:8081/nexus/content/repositories/npm-internal/',
      description: 'The target NPM repo for publication'
    )
    string(
      name: 'credentialsId',
      defaultValue: '066dc64d-086d-4ced-a921-c03a7bffb995',
      description: 'The ID of the required credentials in the jenkins credential store'
    )
	}

  options {
    disableConcurrentBuilds()
	  buildDiscarder(logRotator( daysToKeepStr: '5', numToKeepStr: '5'))
  }

  stages {
    stage('Checkout') {
      steps {
        checkout scm
      }
    }

    stage ('Clean') {
      steps {
        sh './gradlew clean'
      }
    }

    stage('Publish fineos-common') {
			when {
        equals expected: "true",
               actual: deployFineosCommonToNexus
      }
			steps {
				sh "./gradlew publish_fineos_common -Dpublishing.nexus.url=$internalPublishingRepo"
			}
		}

    stage('Increment fineos-common') {
			when {
        equals expected: "true",
               actual: incrementFineosCommonDevVersion
      }
			steps {
				sshagent(["$credentialsId"]) {
					sh "./gradlew version_patch_fineos_common"
				}
			}
		}

    stage('Publish employer-portal') {
			when {
        equals expected: "true",
               actual: deployFineosCommonToNexus
      }
			steps {
				sh "./gradlew publish_employer_portal -Dpublishing.nexus.url=$internalPublishingRepo"
			}
		}

    stage('Increment portal') {
			when {
        equals expected: "true",
               actual: incrementPortalDevVersion
      }
			steps {
				sshagent(["$credentialsId"]) {
					sh "./gradlew auto_version_update -Pbranch=$buildBranch"
				}
			}
		}

//     stage('Publish customer-portal') {
// 			when {
//         equals expected: "true",
//                actual: deployFineosCommonToNexus
//       }
// 			steps {
// 				sh "./gradlew publish_customer_portal -Dpublishing.nexus.url=$internalPublishingRepo"
// 			}
// 		}

//     stage('Increment customer-portal') {
// 			when {
//         equals expected: "true",
//                actual: incrementCustomerPortalDevVersion
//       }
// 			steps {
// 				sshagent(["$credentialsId"]) {
// 					sh "./gradlew version_patch_customer_portal"
// 				}
// 			}
// 		}

    stage('Archive Results') {
      steps {
        junit allowEmptyResults: false,
              testResults: 'reports/**/junit.xml'
      }
    }
  }

  post {
    always {
      junit allowEmptyResults: false,
            testResults: 'reports/**/junit.xml'
    }

    unsuccessful {
      slackSend channel: '#client-fineos',
                color: '#f04c3f',
                message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} has failed"
    }

    fixed {
      slackSend channel: '#client-fineos',
                color: '#35b558',
                message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} has been fixed"
    }
  }
}
