#!/bin/bash

LICENSE_FILE=./LICENSE

COPYRIGHT=$(sed -e 's/^/  /' $LICENSE_FILE)
LONGEST_LINE=$(cat ./LICENSE | awk '{ print length }' | sort -n | tail -1)
COMMENT_SEPARATOR=$(head -c $(expr $LONGEST_LINE + 2) < /dev/zero | tr '\0' '-')
NEW_LINE=$'\n'
COMMENT_START="/*$COMMENT_SEPARATOR$NEW_LINE"
COMMENT_END="$NEW_LINE$COMMENT_SEPARATOR*/"

COMMENT="$COMMENT_START$COPYRIGHT$COMMENT_END$NEW_LINE$NEW_LINE"

function patch_package_by_pattern() {
    for FILE_NAME in $(find "./packages/$1/src" -iname $2); do
    if [[ $(<$FILE_NAME) != *"$COPYRIGHT"* ]]; then
        echo "Adding comment to $FILE_NAME"
        echo "$COMMENT$(cat $FILE_NAME)" > $FILE_NAME
    fi
  done
}

function patch_package {
  patch_package_by_pattern $1 '*.ts'
  patch_package_by_pattern $1 '*.tsx'
  patch_package_by_pattern $1 '*.js'
  patch_package_by_pattern $1 '*.scss'
}

patch_package "fineos-common"
patch_package "employer-portal"
