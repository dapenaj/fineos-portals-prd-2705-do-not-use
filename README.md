# fineos-portals

This repository contains several packages/portals in a monorepo format.

For development purpose [yarn](https://classic.yarnpkg.com/en/) should be used. Installing dependencies with yarn:

```
yarn install
```

In order to run environment please use:

```
cd ./packages/customer-portal && yarn run start
cd ./packages/employer-portal && yarn run start
```

For testing:

```
cd ./packages/customer-portal && yarn run test
cd ./packages/employer-portal && yarn run test
cd ./packages/fineos-common && yarn run test
```

For building libraries:

```
cd ./packages/fineos-common && yarn run build:lib
```

There is also a possibility to run local build connected to real AdminSuits API:

```
yarn run start:connected -- --portal=erp_8
```

Portal is based on test data which is located in `fineos-common/e2e/employer-portal/testData/generatedTestData` directory.
There is also possibility to change the user, by providing user name pattern:

```
yarn run start:connected -- --portal=erp_8 --user=user1
```

*NOTE:* when running local build connected to real API, CORS policy need to be disabled, this can be 
achieved by using [browser plugin](https://mybrowseraddon.com/access-control-allow-origin.html) or 
browser can be [run with disabled CORS](https://stackoverflow.com/a/58658101/3927185).

## fineos-common

Common components that used for building multiple FINEOS portals. These components are build using [React|https://reactjs.org/].

Also fineos-common contain error page which is deployed to emplyer-portal. 

## employer-portal

FINEOS Employer Portal for HR delivers an integrated view of accommodations, claims and absence cases, and streamlined interaction of employers with carriers through messaging

### e2e on local employer-portal

In order to run e2e tests on local build, we need to run first local build connected to real AdminSuits API with `no signature` mode:

```
yarn run start:connected -- --portal=erp_8 --no-signature
```

After, tests can be executed from `fineos-common/e2e`:

```
TAGS="@smoke" ENV=erp_8 npm run local
```

Please take in account that, `TAGS` variable can contain any tests tags, and `ENV` should contain 
proper portal environment name.

For headless mode, please use `local:headless` task:

```
TAGS="@smoke" ENV=erp_8 npm run local:headless
```

If you plan to run tests belonging to few feature files it is not recommended running them in parallel:

```
TAGS="@smoke" ENV=erp_8 npm run local -- --threads=1
```
