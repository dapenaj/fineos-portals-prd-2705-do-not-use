buildscript {
    if (!project.hasProperty('nexusUser')) throw new Exception("The nexusUser parameter cannot be null, e.g. -PnexusUser=ahappyuser")
    if (!project.hasProperty('nexusPassword')) throw new Exception("The nexusPassword parameter cannot be null, e.g. -PnexusPassword=pass12345")
    repositories {
        maven {
            url = 'http://nexus/content/repositories/node-js/'
        }
        maven {
            url = 'http://nexus/content/repositories/GradlePlugins/'
        }
        maven {
            url = 'http://nexus/content/groups/public/'
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }
    dependencies {
        classpath 'com.moowork.gradle:gradle-node-plugin:1.3.1'
        classpath 'com.fineos.gradle.plugins:fineos-aws-plugin:1.0.0.4'
    }
}

def s3Bucket = project.hasProperty('destinationBucket') ? project['destinationBucket'] : 'fineos-release-binaries-fhs'
def s3Folder = project.hasProperty('destinationFolder') ? project['destinationFolder'] : ''
def destFileName = project.hasProperty('destinationFileName') ? project['destinationFileName'] : 'employer-portal.zip'

apply plugin: 'base'
apply plugin: 'maven-publish'
apply plugin: 'com.moowork.node'
apply plugin: 'com.fineos.gradle.plugin.aws.s3'

group 'com.fineos.portals'
version '21.6.0-SNAPSHOT'

node {
    version = '12.13.1'
    download = true
    workDir = file("${projectDir}/build-node")
    nodeModulesDir = file("${projectDir}/node_modules")
    // instructs the plugin to use our repo to download npm
    distBaseUrl = 'http://nexus/content/repositories/node-js/'
}

clean {
	delete "/node_modules"
	delete "/build-node"
	delete "/build"
	delete "/coverage"
	delete "/reports"
	delete "/packages/**/node_modules"
	delete "/packages/**/build-node"
	delete "/packages/**/build"
	delete "/packages/**/coverage"
	delete "/packages/**/reports"
}


/**
 * root project
 */
task install(type: YarnTask) {
  args = ['install', '--no-lockfile']
}

/**
 * fineos-common
 */
//task commonInstall(type: YarnTask, dependsOn: install) {
//  workingDir = file("${projectDir}/packages/fineos-common")
//  args = ['install', '--no-lockfile']
//}

//task commonInstallPeers(type: YarnTask, dependsOn: commonInstall) {
//  workingDir = file("${projectDir}/packages/fineos-common")
//  args = ["run", "install:peers"]
//}

task commonBuild(type: YarnTask, dependsOn: [ install ]) {
  workingDir = file("${projectDir}/packages/fineos-common")
  args = ["run", "build:lib"]
}

task commonVersionPrerelease(type: NpmTask) {
  workingDir = file("${projectDir}/packages/fineos-common")
  args = ['version', 'prerelease', '--no-git-tag-version']
}

task commonCommitVersionChange(type: NpmTask) {
  workingDir = file("${projectDir}/packages/fineos-common")
  args = ['run', 'commit-version-change']
}

task commonPublishPrerelease(type: NpmTask) {
  workingDir = file("${projectDir}/packages/fineos-common")
  args = ['publish', '--tag', 'next', '-ddd']
}

/**
 * employer-portal
 */
//task employerPortalInstall(type: YarnTask, dependsOn: install) {
//  workingDir = file("${projectDir}/packages/employer-portal")
//  args = ['install', '--no-lockfile']
//}

task employerPortalBuild(type: YarnTask, dependsOn: [ install ]) {
  workingDir = file("${projectDir}/packages/employer-portal")
  args = ['run', 'build']
}

task employerPortalPublishPrerelease(type: NpmTask) {
  workingDir = file("${projectDir}/packages/employer-portal")
  args = ['publish', '--tag', 'next', '-ddd']
}


/*
// This task is hooked into build task so that the ng build task is run at the end
// this will be dumped into the 'dist' directory
task npm_build(type: NpmTask, dependsOn: npm_install) {
    args = ['run', 'build:lib:dev']
}
//build.dependsOn(npm_build)

// publishes the repo - you are required to configure the auth and repository yourself
task publish(type: NpmTask) {
    args = ['publish', '-d']
}

// publishes the repo - you are required to configure the auth and repository yourself
task publishPrerelease(type: NpmTask) {
    args = ['publish', '--tag', 'next', '-ddd']
}

// increments the prerelease version for automated builds
task versionPrerelease(type: NpmTask) {
    args = ['version', 'prerelease', '--no-git-tag-version']
}

task commitVersionChange(type: NpmTask) {
    args = ['run', 'commit-version-change']
}
*/


// Maven publication

publishing {
    publications {
        maven(MavenPublication) {
            artifactId = 'employer-portal'
            artifact("${projectDir}/packages/employer-portal/output/portal.zip") {
                extension 'zip'
            }
            artifact("${projectDir}/packages/employer-portal/output/maintenance.zip") {
                extension 'zip'
                classifier 'maintenance'
            }
        }
    }
    repositories {
        maven {
            url = isSnapshot() ? 'http://nexus/content/repositories/portals-maven-dev' : 'http://nexus/content/repositories/portals-maven-prod'
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }
}


// AWS deployment

publishS3Artifact {
    s3Destination = new URI("s3://${s3Bucket}/${s3Folder}/${destFileName}")
    sourceFile = file("${projectDir}/packages/employer-portal/output/portal.zip")
    doFirst {
    	if (!s3Bucket) throw new Exception("The destinationBucket parameter cannot be null, e.g. -PdestinationBucket=fineos-portals-fhs-eu-west-1")
    	if (!s3Folder) throw new Exception("The destinationFolder parameter cannot be null, e.g. -PdestinationFolder=erportal")
    	if (!destFileName) throw new Exception("The destinationFileName parameter cannot be null, e.g. -PdestinationFileName=employer-portal.zip")
    }
}


task install_npm(type: NpmTask) {
	workingDir = file("${project.projectDir}/packages/fineos-common/e2e")
	args = ["install"]
  }

 task run_protractor(type: NpmTask, dependsOn: "install_npm") {
	workingDir = file("${project.projectDir}/packages/fineos-common/e2e")
	args = ["run", "webdriver-update"]
 }

 task run_e2e_tests(type: NodeTask, dependsOn: "run_protractor") {
   def TEST_CONFIG = project.getProperties().getOrDefault("testsConfig", 'jenkinsConfigHeadless.js')
   def TEST_ENV = project.getProperties().getOrDefault("testEnv", 'erp_4')
   def CUCUMBER_TAGS = project.getProperties().getOrDefault("cucumberTags", '@smoke')

   workingDir = file("${project.projectDir}/packages/fineos-common/e2e")
   script = file("${project.projectDir}/packages/fineos-common/e2e/node_modules/protractor/bin/protractor")
   args = ["config/$TEST_CONFIG","--env=$TEST_ENV", "--tags=\"$CUCUMBER_TAGS\""]
 }


// utilities

def isSnapshot() {
    return project.version.endsWith('-SNAPSHOT');
}
