# Customer Portal - Decision trees and forms

The mechanism for definition the `event details` decision tree, and `request details` forms are defined as follows.

## Event Details decision trees

The model is defined in `EventDetailOptionService`

The decision tree is defined as an directed acyclic graph:

```
    const EVENT_OPTION_MODEL: EventOptionEdge[] = [
          { from: EventOptionId.SICKNESS,
            to: EventOptionId.SICKNESS_TIMEOFF,
            requiredRoles : [Role.ABSENCE_USER]
          },

        { from: EventOptionId.ORIGIN, to: EventOptionId.ACCIDENT },
        ...
    ];
```

Where:

- Every question in the tree is a simple single selection from a set of options.
- If the `requiredRoles` is not set then every user can see it
- If the `requiredRoles` has _any_ roles, then the user must have _at least one_ of these
- The required roles are applied along the entire path - so if a user can't see a parent,
  they can't see a child, even if that child has no limitations on its own.
  This allows for a node to be possible on one path, but not another.
- The first set of options are those elements defined below `EventOptionId.ORIGIN`
- If the user gets to an option with no children they have reached the end
  of the decision tree, and they can be presented with the forms

## Request Details forms

The model is defined in `RequestDetailFormService`

### What forms exist?

This is defined as a simple map of form id to form details

```
const FORM_MODEL: Map<RequestDetailFormId, RequestDetailForm> = new Map<
  RequestDetailFormId,
  RequestDetailForm>([
  [
    // the key
    RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
    {
      id: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
      // the fields
      layout: [
        // required field that is a long text field
        { id: RequestDetailFieldId.PROCEDURE_NAME, answerType: RequestDetailAnswerType.LONG_TEXT},
        // an optional date field
        { id: RequestDetailFieldId.DONATION_DATE, answerType: RequestDetailAnswerType.DATE, optional: true},
        // a list of options that may or may not result in a sub form
        { id: RequestDetailFieldId.DONATION_OVERNIGHT_REQUIRED, answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            // if selected then this will link on to a subform
            {
              id: RequestDetailFieldId.YES,
              targetFormId:RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY
            },
            // if selected then this will not link on to a subfirm
            { id: RequestDetailFieldId.NO }
          ]
        }
      ]
    }
  ]
])
```

## What forms to start with?

The second part of the model defines what forms to start with, given a path of `EventOptionId`s

```
const FORM_USAGE: RequestDetailFormUsage[] = [
  // if the path contains any of "defaultFor" then show the user TIME_OFF_FOR_SICKNESS
  {  formId: RequestDetailFormId.TIME_OFF_FOR_SICKNESS, defaultFor: [EventOptionId.SICKNESS_TIMEOFF]},

  // this is a more complex case
  {
    // show this form
    formId: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
    // if the path to this form contains any of these
    defaultFor: [
      EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL,
      EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
    ],
    // but if the path had selected this other form then remove TIME_OFF_FOR_SICKNESS
    // it has been replaced by this one
    negates: [RequestDetailFormId.TIME_OFF_FOR_SICKNESS]
  }
];

```
