# Demo builder

# What does this do?

It provides a `Jenkinsfile` that

- Builds a docker image that is prepared with tomcat and JDK 11
- Clones the mock server project, installs it, and then copies it into a docker image
- Clones this react project, and then downloads its built version from the nexus repo, and copies it into the docker image
- Is prepared to start the mock server and tomcat

# Where are things installed?

- The react application is deployed as the root tomcat app - `/usr/local/tomcat/webapps/ROOT`
  - This will be available on `localhost:8080`
- The mock server is deployed to - `/demo/mockServer`
- This config is used [env.json](env.json)
  - note that for a demo, the `noAuthentication` strategy is used

# How to import and run this image

To import the image this builds, download it from jenkins and load it.

    docker load -i customer_portal_demo.tgz

You will see that it creates an image named `customer_portal_react_demo:latest`

    REPOSITORY                   TAG                 IMAGE ID            CREATED             SIZE
    customer_portal_react_demo   latest              abf6f44a0d61        6 minutes ago       622MB

There is a [docker-compose.yml](docker-compose.yml) file provided with all the required parameters, but at the moment the only things you need to do is open some ports.
This is a simple demo, and so has no volume, and so will lose any data after a restart.

    docker run -d  \
        -p 3000:3000 -p 8080:8080 \
        --name CustomerPortalDemo \
        fineos/customer_portal_react_demo

There will now be a container running named `CustomerPortalDemo` you can access the demo on http://localhost:8080

## What if I want to override the default demo config?

You can copy your own [env.json](env.json) to a directory you own (e.g. `~/wherever`) , and then configure the container to mount `/demo-config`to it.

    docker run -d  \
        -p 3000:3000 -p 8080:8080 \
        --name CustomerPortalDemo \
        -v ~/wherever:/demo-config \
        fineos/customer_portal_react_demo

Normally, for a demo, the only reason you'd likely want to change the [env.json](env.json) would be to change this setting to say where to find the APIs to use some public hostname.

    "apiUrls": {
        "root": "http://localhost:3000/api/v1"
    },

# What if I want to do this locally, without using the full CI process?

If you just want to try this locally, without doing a full jenkins build, you can do so as follows::

- In the directory _above_ the `customer-portal-react` project, create two symlinks
  - `mockServer`-> `mock-customer-portal-api`
  - `testServer`-> `customer-portal-react` (i.e. this is in effect a duplicate )
- In `mockServer` run `npm install`
- In `testServer` run `./gradlew build` (i.e. it's the release build we want)
- Finally in `customer-portal-react/demo` run
  - `docker-compose build`
  - `docker-compose up`
- Now, you can access the demo on http://localhost:8080
