# Customer Portal - React

- [Customer Portal - React](#customer-portal---react)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Development](#development)
  - [Production](#production)
  - [Testing](#testing)
  - [Linting and Formatting](#linting-and-formatting)
  - [CI Building](#ci-building)
  - [Styling](#styling)

## Prerequisites

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Install [Node.js® and npm](https://nodejs.org/en/download/) if they are not already on your machine. Node version 12.2.0 was used for this application. see `.nvmrc` file.

## Installation

**NOTE:** this depends upon elements that are not published publicly - you will need to set up your `.npmrc` appropriately to use a local npm repo that has these components installed

e.g. for a local nexus this could be

```
registry=http://atlanta.sonalake.corp:8081/nexus/content/groups/npm-all/
```

Stored either in `.npmrc` or `~/.npmrc`

```
npm install
```

## Development

```
npm run start
```

This will open a development server on http://localhost:3000/

## Production

```
npm run build
```

This will generate a `./build` folder in the root directory.

```
/build
  /static
    /css
    /js
    /media
  asset-manifest.json
  favicon.ico
  index.html
  manifest.json
  service-worker.js
```

For real production build, loader should be built and injected into HTML page, using following command
```
npm run postbuild
```

Loader will force loading from secured AWS S3. Due to this requirement code splitting cannot be used, as we can't hijack into webpack loading.

If you want to just build loader, you can use:
```
npm run build:loader
```

Actual injecting can be found in node script here:

```
 ./scripts/inject-loader.js
```

## Testing

[Jest](https://jestjs.io/) is used for Unit testing and [Cypress](https://www.cypress.io/) is used for e2e testing.

```
npm run test
```

Run unit tests during development with a live reload.

```
npm run test:coverage
```

Generate a unit test coverage report located in the `./covergae` folder.

```
npm run test:ci
```

Run unit tests for continuous integration. Generates JUnit and sonar reports.

```
npm run e2e
```

Run headless e2e tests.

```
npm run e2e:cypress
```

Run e2e tests in a browser using the cypress interface.

## Linting and Formatting

Husky and lint-staged are used to hook into `pre-commit` to run linting [(tslint)](https://palantir.github.io/tslint/) and formatting [(prettier)](https://prettier.io/) to ensure a uniform codebase.

## CI Building

### Build tasks
The following `gradle` targets and files exist for CI work.

- `clean build` - wraps a call to the following scripts `build lint test`
- `sonar` - executes a sonar quality check, you need to tell it the sonar host to which the check should be loaded, e.g. `./gradlew sonar -Dsonar.host.url=http://quality.sonalake.corp:9000`
- `test_e2e` - runs the full end-to-end tests, requires a target server to be available for requests


### Pipeline definitions

Two pipelines are provided, and need to be defined in jenkins inthis order

 - `ci-e2e/Jenkinsfile` - this creates a docker image with both the mock server and this test application, and runs the `test_e2e` target. 
  - This needs to be named `fineos-customer-portal-react-e2e` because the main pipeline calls out to it
 - `Jenkinsfile` - this runs a full build, and if a `deployToNexus` parameter is set to `true` it will call out the previous e2e job and then push the built version to nexus


The following parameters are provided:

- `buildBranch` - what branch on the repo to check out
- `sonarHost` - to what host should the sonar quality check results be sent
- `credentialsId` - id of the secure credentials in jenkins that can be used to checkout the project
- `internalPublishingRepo` - if `deployToNexus` parameter is set to `true` then the built version is pushed to nexus


## Styling

For this application we use SCSS as our styling language. In the `src/styles` folder there are a number of SCSS files that are used throughout the application. These are broken down into the following folders.

- `/mixins` - elevations, fonts etc
- `/overrides` - ant design and/or external library style overrides
- `/variables` - colors, elevation, spacing, typography etc


# Configuring the decision trees and forms for intake

The process for how to define these trees and forms is defined [in this document](DECISION_TREE.md)


# Building a demo docker image

The process for how to build a demo docker image is defined [in this document](demo/README.md)
