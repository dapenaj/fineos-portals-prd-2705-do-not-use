import S3 from 'aws-sdk/clients/s3';

const parseUrlFragments = (url: string) =>
  url
    .substr(1)
    .split('#')
    .filter(Boolean)
    .reduce(
      (params, pair) => {
        const [key, value] = pair.split('=');

        params[key] = decodeURIComponent(value);

        return params;
      },
      {} as { [key: string]: string | string[] }
    );

const error = document.createElement('P');
const errorMessage = document.createTextNode(
  'Unfortunately, the application cannot be loaded right now. Please try to refresh the page, if the problem persists contact your system administrator.'
);
error.appendChild(errorMessage);

const request = new XMLHttpRequest();
request.open('GET', 'config/env.json');

request.onload = () => {
  if (request.status === 200) {
    const {
      authenticationStrategy,
      login: { url },
      aws: { region, secureResourceEnvPrefix, secureBucketName }
    } = JSON.parse(request.responseText);
    const fragments = parseUrlFragments(document.location.hash);
    // tslint:disable: no-string-literal
    const accessKeyId = fragments['AccessKeyId'];
    const secretAccessKey = fragments['SecretKey'];
    const sessionToken = fragments['SessionToken'];

    const AUTHENTICATION_IS_REQUIRED: boolean =
      authenticationStrategy !== 'noAuthentication';

    if (
      !(accessKeyId && secretAccessKey && sessionToken) &&
      AUTHENTICATION_IS_REQUIRED
    ) {
      window.location = url;
      return;
    }

    const s3 = new S3({
      accessKeyId,
      secretAccessKey,
      sessionToken,
      region
    });

    const appendScript = (src: string) => {
      const scriptTag = document.createElement('script');
      scriptTag.async = false;

      // REMOVE UNTIL FINEOS IS READY
      // const key = secureResourceEnvPrefix + src;
      // scriptTag.src = AUTHENTICATION_IS_REQUIRED
      //   ? s3.getSignedUrl('getObject', {
      //       Bucket: secureBucketName,
      //       Key: key
      //     })
      //   : src;

      scriptTag.src = src;

      document.body.appendChild(scriptTag);
    };

    for (let bundleUrl of (window as any).___FINEOS_BUNDLES) {
      appendScript(bundleUrl);
    }
  } else {
    document.body.appendChild(error);
    return;
  }
};

request.onerror = () => {
  document.body.appendChild(error);
  return;
};

request.send();
