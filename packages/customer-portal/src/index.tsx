import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/*
 * IE Polyfill imports
 */
// tslint:disable: no-import-side-effect
import 'react-app-polyfill/ie9';
import 'react-app-polyfill/stable';

// due to disabled lazy loading, this styles should be included before any other styles for view components
// so component's styles would normally have higher priority than default one
import './styles/index.scss';
import { buildStore } from './app/store';
import App from './app';

// configure the store
const store = buildStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
