import React from 'react';
import { MemoryRouter } from 'react-router';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';

import { buildStore } from '../../app/store/store';
import { getMessagesForLocale } from '../../app/i18n/utils';
import { saveCredentials } from '../common/utils';
import { AppConfigProvider } from '../../app/shared/contexts';
import { appConfigFixture } from '../common/fixtures';

export const DefaultContext = ({
  children,
  customStore,
  ...routerProps
}: any) => {
  saveCredentials();
  return (
    <Provider store={customStore || buildStore()}>
      <AppConfigProvider config={appConfigFixture}>
        <IntlProvider locale="en" messages={getMessagesForLocale('en')}>
          <MemoryRouter {...routerProps}>{children}</MemoryRouter>
        </IntlProvider>
      </AppConfigProvider>
    </Provider>
  );
};
