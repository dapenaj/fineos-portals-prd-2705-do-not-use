import * as Enzyme from 'enzyme';
import 'jest-enzyme';
import {
  configure,
  shallow,
  mount,
  render,
  ShallowWrapper,
  ReactWrapper
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as renderer from 'react-test-renderer';

configure({ adapter: new Adapter() });

export { shallow, mount, render, ShallowWrapper, ReactWrapper, renderer };
export default Enzyme;
