import {
  EventOptionId,
  RequestDetailFormId,
  RequestDetailField,
  RequestDetailFieldId
} from '../../../app/shared/types';
import {
  EventDetailOptionService,
  RequestDetailFormService
} from '../../../app/shared/services';

/**
 * Assert that a given path in the intake options has the expected set of children
 * @param path option path
 * @param expected expected children (can be empty)
 */
export const thenPathHasChildren = (
  path: EventOptionId[],
  expected: EventOptionId[]
) => {
  expect(expected).toStrictEqual(
    EventDetailOptionService.getInstance().next(path)
  );
};

/**
 * Assert that the given path in the intake options has no children
 * @param path option path
 */
export const thenPathHasNoChildren = (path: EventOptionId[]) => {
  expect(EventDetailOptionService.getInstance().next(path)).toHaveLength(0);
};

/**
 * Assert that the given path in the intake option has the following set of forms configured
 * @param path the option path
 * @param expectedFormIds the forms we expect in for this path, in this order
 */
export const thenPathHasDefaultForms = (
  path: EventOptionId[],
  expectedFormIds: RequestDetailFormId[]
) => {
  const formIds = RequestDetailFormService.getInstance()
    .getDefaultForms(path)
    .map(r => r.id);
  expect(expectedFormIds).toEqual(formIds);
};

/**
 * Assert that the given form has the following fields
 * @param expectedFormId the form
 * @param expectedFields  the expected field - only the id and answerType are considered, use thenFormHasSubForm to figure out the options
 */
export const thenFormHasFields = (
  expectedFormId: RequestDetailFormId,
  expectedFields: RequestDetailField[]
) => {
  const form = RequestDetailFormService.getInstance().getForm(expectedFormId);
  expect(form).toBeDefined();
  const out =
    form &&
    form.layout.map(r => {
      return {
        id: r.id,
        answerType: r.answerType
      };
    });
  expect(expectedFields).toEqual(out);
};

/**
 * Assert that the given field in the given form has an option with the given subform
 * @param formId  the form being tested
 * @param fieldId  The field in the form being tested
 * @param optionFieldId  The option being tested
 * @param expectedTargetFormId  The expected form id
 */
export const thenFormHasSubForm = (
  formId: RequestDetailFormId,
  fieldId: RequestDetailFieldId,
  optionFieldId: RequestDetailFieldId,
  expectedTargetFormId?: RequestDetailFormId
) => {
  const form = RequestDetailFormService.getInstance().getForm(formId);

  const observedField = form && form.layout.find(r => r.id === fieldId);
  const observedOption =
    observedField &&
    observedField.options &&
    observedField.options.find(r => r.id === optionFieldId);

  expect(observedOption).toBeDefined();
  const observedTargetFormId = observedOption
    ? observedOption.targetFormId
    : undefined;

  if (expectedTargetFormId) {
    expect(observedTargetFormId).toBeDefined();
    if (observedTargetFormId) {
      expect(observedTargetFormId).toEqual(observedTargetFormId);
    }
  } else {
    expect(observedTargetFormId).toBeUndefined();
  }
};

/**
 * Assert that the given field in the given form has an option with NO subform
 * @param formId  the form being tested
 * @param fieldId  The field in the form being tested
 * @param optionFieldId  The option being tested
 */
export const thenFormHasNoSubForm = (
  formId: RequestDetailFormId,
  fieldId: RequestDetailFieldId,
  option: RequestDetailFieldId
) => {
  thenFormHasSubForm(formId, fieldId, option, undefined);
};
