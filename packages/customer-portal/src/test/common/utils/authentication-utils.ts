import { overrideSdkAndApiConfigs } from '../../../app/config';
import { AuthenticationService } from '../../../app/shared/services';

const service = AuthenticationService.getInstance();

export function saveCredentials() {
  overrideSdkAndApiConfigs();
  service.saveCredentials('');
}
