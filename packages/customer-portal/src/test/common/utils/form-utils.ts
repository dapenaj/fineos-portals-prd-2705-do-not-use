import { ReactWrapper } from 'enzyme';
import { DatePicker, Select } from 'antd';
import moment from 'moment';

import Enzyme from '../../config/enzyme';

import { releaseEventLoop } from './common-utils';

export type InputChangeProps = {
  wrapper: Enzyme.ReactWrapper;
  name?: string;
  type?: string;
  value: Date | number | string | null;
};

export const changeInputValue = ({
  wrapper,
  name,
  value,
  type
}: InputChangeProps): Enzyme.ReactWrapper => {
  const typeSearchString = type ? `[type='${type}']` : '';
  const inputsByType = wrapper.find(`input${typeSearchString}`);
  const inputByName = inputsByType.find(`input[name='${name}']`);

  inputByName.simulate('change', { target: { value, name } });

  return inputByName;
};

export function changeAnyInputValue(
  wrapper: ReactWrapper,
  name: string,
  value: string
) {
  return changeInputValue({
    wrapper,
    name,
    value
  });
}

export function changeRadioButtonInputValue(
  wrapper: ReactWrapper,
  name: string,
  value: any
) {
  const inputByName = wrapper
    .find(`input[type="radio"][name="${name}"]`)
    .filter({
      value
    });
  inputByName.simulate('change', { target: { value, name } });
}

export function changeSelectValue(
  wrapper: ReactWrapper,
  selectName: string,
  value: string | number
) {
  const select = wrapper.find(`[name="${selectName}"]`).find(Select);
  const option = (select.prop('children') as any[]).find(
    child => child.value === value
  );

  (select!.prop('onChange') as any)(value, option);
}

export function selectAnySelectOption(
  wrapper: ReactWrapper,
  selectName: string,
  index?: number
) {
  const select = wrapper.find(`[name="${selectName}"]`).find(Select);
  const option = (select!.prop('children') as any[])[index ? index : 0];
  const value = option.props.value;

  (select!.prop('onChange') as any)(value, option);
}

export function changeDatePicker(
  wrapper: ReactWrapper,
  name: string,
  value: string
) {
  const datePicker = wrapper.findWhere(
    node => node.is(DatePicker) && node.prop('name') === name
  );
  datePicker.prop('onChange')(moment(value), value);
}

export function findSelectedRadioOption(wrapper: ReactWrapper, name: string) {
  return wrapper
    .find(name)
    .find('input[type="radio"]')
    .findWhere(el => el.props().checked);
}

export async function submitForm(wrapper: ReactWrapper) {
  wrapper
    .find('form')
    .hostNodes()
    .simulate('submit');

  await releaseEventLoop();
  wrapper.update();
}
