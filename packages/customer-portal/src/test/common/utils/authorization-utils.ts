import { SdkConfigProps } from 'fineos-js-api-client';

import { Role } from '../../../app/shared/types';
import { AuthenticationService } from '../../../app/shared/services';

/**
 * Set things up so then the authorization role will return this role
 * as the authorized role
 */
const givenRole = (role?: Role) => {
  // this will just translate our role into the "real" one
  jest
    .spyOn(SdkConfigProps, 'authorizationRole')
    .mockImplementation((roleName: string) => {
      switch (roleName) {
        case Role.ABSENCE_SUPERVISOR:
          return Role.ABSENCE_SUPERVISOR;
        case Role.CLAIMS_USER:
          return Role.CLAIMS_USER;
        case Role.ABSENCE_USER:
          return Role.ABSENCE_USER;
        default:
          return `Don't know about role: ` + roleName;
      }
    });

  AuthenticationService.getInstance().displayRoles = role ? [role] : [];
};
export const givenAbsenceUser = () => {
  givenRole(Role.ABSENCE_USER);
};

export const givenCustomerUser = () => {
  givenRole(Role.CLAIMS_USER);
};

export const givenUserWithNoRole = () => {
  givenRole(undefined);
};

export function promiseTest(done: any, promise: any): void {
  return promise
    .then(() => {
      done();
    })
    .catch((err: any) => {
      console.log('Test failed: ' + JSON.stringify(err));
      done(err);
    });
}
