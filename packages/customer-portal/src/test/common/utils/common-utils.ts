export const releaseEventLoop = (): Promise<void> =>
  new Promise<void>(f => setTimeout(f));

export const givenPromise = <T>(type: T) => {
  // mock something to return thes;
  const fn = jest.fn();
  fn.mockReturnValue(Promise.resolve(type));

  return fn;
};

export const givenRejectedPromise = <T>(type: T) => {
  // mock something to return thes;
  const fn = jest.fn();
  fn.mockReturnValue(Promise.reject(type));

  return fn;
};

export const mockField = <T>(
  entity: any,
  mockedFieldName: string,
  mockedField: T
) => {
  Object.defineProperty(entity, mockedFieldName, {
    value: mockedField
  });
  return mockedField;
};
