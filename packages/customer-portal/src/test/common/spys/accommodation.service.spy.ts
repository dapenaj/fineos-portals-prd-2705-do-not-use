import { AccommodationService } from 'fineos-js-api-client';

import { accommodationFixture } from '../fixtures';

const service = AccommodationService.getInstance();

export const startAccommodationSpy = jest
  .spyOn(service, 'startAccommodation')
  .mockReturnValue(Promise.resolve(accommodationFixture));
