import { MessageService } from 'fineos-js-api-client';

import { messagesFixture } from '../fixtures';

import { RequestType } from './spys.type';

const service: MessageService = MessageService.getInstance();

export const fetchMessagesSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getMessages')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(messagesFixture)
        : Promise.reject('messagesFixture failure')
    );

export const addMessageSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'addMessage')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(messagesFixture[0])
        : Promise.reject('addMessage failure')
    );
