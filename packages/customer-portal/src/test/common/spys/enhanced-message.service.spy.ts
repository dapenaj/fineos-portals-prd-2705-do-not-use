import { EnhancedMessageService } from '../../../app/shared/services';
import { EnhancedMessage } from '../../../app/shared/types';
import { messagesFixture } from '../fixtures';

import { RequestType } from './spys.type';

const service: EnhancedMessageService = EnhancedMessageService.getInstance();

const enhancedMessages: EnhancedMessage[] = messagesFixture.map(message => ({
  notificationCaseId: 'NTN-21',
  ...message
}));

export const fetchEnhancedMessagesSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'fetchMessages')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(enhancedMessages)
        : Promise.reject('messagesFixture failure')
    );

export const fetchEnhancedMessagesByCaseIdSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'fetchMessagesByCaseId')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(enhancedMessages)
        : Promise.reject('addMessage failure')
    );
