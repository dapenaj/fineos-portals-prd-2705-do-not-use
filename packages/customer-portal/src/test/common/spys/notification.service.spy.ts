import { NotificationService } from '../../../app/shared/services';
import {
  notificationsFixture,
  myTeamLeaveFixture,
  entitlementOptionsfixture
} from '../fixtures';

import { RequestType } from './spys.type';

const service = NotificationService.getInstance();

export const fetchNotificationsSpy = () =>
  jest
    .spyOn(service, 'fetchNotifications')
    .mockReturnValue(Promise.resolve(notificationsFixture));

export const completeNotificationSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'completeNotification')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(notificationsFixture[0])
        : Promise.reject('could not complete notification')
    );

export const fetchSupervisedNotificationsSpy = (type: RequestType) =>
  jest.spyOn(service, 'fetchSupervisedNotifications').mockReturnValue(
    type === 'success'
      ? Promise.resolve({
          total: myTeamLeaveFixture.length,
          data: myTeamLeaveFixture
        })
      : Promise.reject('fetchSupervisedNotifications failure')
  );

export const fetchNotificationEntitlementsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'fetchNotificationEntitlements')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(entitlementOptionsfixture)
        : Promise.reject('fetchNotificationEntitlements failure')
    );

export const getCaseIdsFromNotificationSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getCaseIdsFromNotification')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(['NTN-21', 'ABS-1', 'CLM-1', 'BEN-1'])
        : Promise.reject(['NTN-21', 'ABS-1', 'CLM-1'])
    );
