import { OccupationService } from 'fineos-js-api-client';

import { occupationsFixture } from '../fixtures';

import { RequestType } from './spys.type';

const service = OccupationService.getInstance();

export const fetchOccupationSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getOccupationDetails')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(occupationsFixture[0])
        : Promise.reject('getOccupationDetails failed')
    );
