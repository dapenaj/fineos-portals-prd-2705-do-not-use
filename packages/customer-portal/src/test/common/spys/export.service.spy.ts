import { ExportService } from '../../../app/shared/services/export/export.service';

const service = ExportService.getInstance();

export const exportToCsvSpy = () =>
  jest.spyOn(service, 'exportToCsv').mockReturnValue();
