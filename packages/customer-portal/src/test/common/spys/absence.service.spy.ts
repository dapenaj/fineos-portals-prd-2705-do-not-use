import { AbsenceService } from 'fineos-js-api-client';

import {
  absencesFixture,
  absencePeriodDecisionsFixture,
  supervisedAbsencesFixture,
  absenceReasons
} from '../fixtures';

import { RequestType } from './spys.type';

const service = AbsenceService.getInstance();

export const getAbsenceDetailsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getAbsenceDetails')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(absencesFixture[0])
        : Promise.reject('getAbsenceDetails failed')
    );

export const getAbsenceReasonsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getAbsenceReasons')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(absenceReasons)
        : Promise.reject('getAbsenceReasons failed')
    );

export const getSupervisorAbsenceDetailsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getSupervisedAbsenceDetails')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(supervisedAbsencesFixture[0])
        : Promise.reject('getSupervisedAbsenceDetails failed')
    );

export const getAbsencePeriodDecisionsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getAbsencePeriodDecisions')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(absencePeriodDecisionsFixture[0])
        : Promise.reject('getAbsencePeriodDecisions failed')
    );

export const additionalTimeOffForContinuousPeriodSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'additionalTimeOffForReducedPeriod')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(1)
        : Promise.reject('additionalTimeOffForContinuousPeriod failed')
    );

export const reduceTimeOffForContinuousPeriodSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'reduceTimeOffForContinuousPeriod')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(2)
        : Promise.reject('reduceTimeOffForContinuousPeriod failed')
    );

export const movedTimeOffForContinuousPeriodSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'movedTimeOffForContinuousPeriod')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(3)
        : Promise.reject('movedTimeOffForContinuousPeriod failed')
    );

export const additionalTimeOffForReducedPeriodSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'additionalTimeOffForReducedPeriod')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(4)
        : Promise.reject('additionalTimeOffForReducedPeriod failed')
    );

export const reduceTimeOffForReducedPeriodSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'reduceTimeOffForReducedPeriod')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(5)
        : Promise.reject('reduceTimeOffForReducedPeriod failed')
    );

export const movedTimeOffForReducedPeriodSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'movedTimeOffForReducedPeriod')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(6)
        : Promise.reject('movedTimeOffForReducedPeriod failed')
    );

export const deleteTimeOffRequestSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'deleteTimeOffRequest')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(7)
        : Promise.reject('deleteTimeOffRequest failed')
    );
