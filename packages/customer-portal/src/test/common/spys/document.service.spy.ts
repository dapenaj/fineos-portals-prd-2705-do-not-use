import { ClaimService, DocumentService } from 'fineos-js-api-client';

import {
  documentsFixture,
  supportingEvidenceFixture,
  documentResponseFixture
} from '../fixtures';

import { RequestType } from './spys.type';

const claimService = ClaimService.getInstance();

const documentService = DocumentService.getInstance();

export const fetchDocumentSpy = (type: RequestType) =>
  jest
    .spyOn(claimService, 'findDocuments')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(documentsFixture)
        : Promise.reject('findDocuments failure')
    );

export const markDocumentAsReadSpy = (type: RequestType) =>
  jest
    .spyOn(documentService, 'markDocumentAsRead')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(documentsFixture[0])
        : Promise.reject('markDocumentAsRead failure')
    );

export const getSupportingEvidenceSpy = (type: RequestType) =>
  jest
    .spyOn(documentService, 'getSupportingEvidence')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(supportingEvidenceFixture)
        : Promise.reject('getSupportingEvidence failure')
    );

export const uploadCaseDocumentSpy = (type: RequestType) =>
  jest
    .spyOn(documentService, 'uploadCaseDocument')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(documentResponseFixture[0])
        : Promise.reject('uploadCaseDocument failure')
    );

export const uploadSupportingEvidenceSpy = (type: RequestType) =>
  jest
    .spyOn(documentService, 'uploadSupportingEvidence')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve()
        : Promise.reject('uploadSupportingEvidence failure')
    );
