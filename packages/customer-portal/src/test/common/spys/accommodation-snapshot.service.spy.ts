import { AccommodationSnapshotService } from '../../../app/shared/services';

import { RequestType } from './spys.type';

const service = AccommodationSnapshotService.getInstance();

export const submitSnapshotSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'submitSnapshot')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve({})
        : Promise.reject('cannot create snapshot!')
    );
