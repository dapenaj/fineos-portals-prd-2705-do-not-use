import { StrictEFormService } from '../../../app/shared/services/eform';
import { confirmReturnForm } from '../fixtures';

import { RequestType } from './spys.type';

const strictEFormService = StrictEFormService.getInstance();

export const getConfirmReturnSpy = (type: RequestType) =>
  jest
    .spyOn(strictEFormService, 'getConfirmReturnEForm')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(confirmReturnForm)
        : Promise.reject('getConfirmReturnEForm failure')
    );

export const updateConfirmReturnEFormSpy = (type: RequestType) =>
  jest
    .spyOn(strictEFormService, 'updateConfirmReturnEForm')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve()
        : Promise.reject('updateConfirmReturnEFormSpy failure')
    );
