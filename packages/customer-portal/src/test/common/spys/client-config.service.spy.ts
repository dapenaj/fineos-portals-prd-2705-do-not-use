import { clientConfigFixture } from '../fixtures';
import { ClientConfigService } from '../../../app/shared/services';

import { RequestType } from './spys.type';

const service = ClientConfigService.getInstance();

export const fetchClientConfigSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'fetchClientConfig')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(clientConfigFixture)
        : Promise.reject('fetchClientConfig failure')
    );
