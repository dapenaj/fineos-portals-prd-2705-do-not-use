import { ClaimService } from 'fineos-js-api-client';

import {
  disabilityBenefitSummariesFixture,
  benefitsFixture,
  claimSummariesFixture,
  paymentFixture,
  paymentLinesFixture
} from '../fixtures';

import { RequestType } from './spys.type';

const service = ClaimService.getInstance();

export const readDisabilityBenefitSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'readDisabilityBenefit')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(disabilityBenefitSummariesFixture[0])
        : Promise.reject('readDisabilityBenefit failure')
    );

export const findBenefitsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'findBenefits')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(benefitsFixture)
        : Promise.reject('findBenefits failure')
    );

export const findClaimSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'findClaims')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(claimSummariesFixture)
        : Promise.reject('findClaims failure')
    );

export const findPaymentsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'findPayments')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(paymentFixture)
        : Promise.reject('findPayments failure')
    );

export const findPaymentLinesSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getPaymentLines')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(paymentLinesFixture)
        : Promise.reject('getPaymentLines failure')
    );
