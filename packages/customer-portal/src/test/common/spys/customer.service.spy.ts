import { CustomerService } from 'fineos-js-api-client';

import {
  customerFixture,
  contactFixture,
  paymentPreferenceFixture,
  employeeFixture
} from '../fixtures';

import { RequestType } from './spys.type';

const service = CustomerService.getInstance();

export const fetchCustomerDetailsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getCustomerDetails')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(customerFixture)
        : Promise.reject('getCustomerDetails failed')
    );

export const fetchContactDetailsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getCustomerContactDetails')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(contactFixture)
        : Promise.reject('getCustomerContactDetails failed')
    );

export const updateCustomerDetailsSpy = jest
  .spyOn(service, 'updateCustomerDetails')
  .mockReturnValue(Promise.resolve(customerFixture));

export const updateContactDetailsSpy = jest
  .spyOn(service, 'updateCustomerContactDetails')
  .mockReturnValue(Promise.resolve(contactFixture));

export const fetchPaymentPreferencesSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'findPaymentPreferences')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(paymentPreferenceFixture)
        : Promise.reject('findPaymentPreferences Failure')
    );

export const addPaymentPreferenceSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'addPaymentPreference')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(paymentPreferenceFixture[0])
        : Promise.reject('findPaymentPreferences Failure')
    );

export const updatePaymentPreferenceSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'updatePaymentPreference')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(paymentPreferenceFixture[0])
        : Promise.reject('findPaymentPreferences Failure')
    );

export const getSupervisedEmployeesSpy = (type: RequestType) =>
  jest.spyOn(service, 'getSupervisedEmployees').mockReturnValue(
    type === 'success'
      ? Promise.resolve({
          total: employeeFixture.length,
          data: employeeFixture
        })
      : Promise.reject('getSupervisedEmployees Failure')
  );
