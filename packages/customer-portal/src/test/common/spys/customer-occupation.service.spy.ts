import {
  CustomerOccupationService,
  OccupationService
} from 'fineos-js-api-client';

import { customerOccupationsFixture } from '../fixtures';

import { RequestType } from './spys.type';

const service = CustomerOccupationService.getInstance();
const occupationService = OccupationService.getInstance();

export const fetchCustomerOccupationsSpy = (type: RequestType) =>
  jest
    .spyOn(service, 'getCustomerOccupations')
    .mockReturnValue(
      type === 'success'
        ? Promise.resolve(customerOccupationsFixture)
        : Promise.reject('getCustomerOccupations failed')
    );

export const updateWorkDetailsSpy = jest
  .spyOn(service, 'updateWorkDetails')
  .mockReturnValue(Promise.resolve(1));
