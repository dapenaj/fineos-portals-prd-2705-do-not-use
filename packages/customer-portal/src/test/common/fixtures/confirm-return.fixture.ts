import moment from 'moment';

import { ConfirmCaseReturn } from '../../../app/modules/notifications/notification-detail/required-from-me';

export const confirmReturnForm: ConfirmCaseReturn = {
  employeeConfirmed: false,
  expectedRTWDate: moment('2019-07-08'),
  id: 12,
  returningAsExpected: null,
  actualRTWDate: null,
  employerNotified: undefined,
  rtwConfirmationNotes: undefined,
  caseId: 'NTN-873'
};

export const confirmReturnFormWithoutExpectedDate: ConfirmCaseReturn = {
  employeeConfirmed: false,
  id: 13,
  actualRTWDate: null,
  rtwConfirmationNotes: undefined,
  expectedRTWDate: null,
  returningAsExpected: false,
  employerNotified: false,
  caseId: 'NTN-873'
};
