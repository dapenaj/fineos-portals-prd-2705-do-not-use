import {
  PaymentLine,
  ChequePaymentInfo,
  AccountTransferInfo
} from 'fineos-js-api-client';

import { CasePayment } from '../../../app/shared/types';

const chequePaymentFixture = {
  chequeNumber: '213123'
} as ChequePaymentInfo;

const accountTransferInfoFixture = {
  bankAccountNumber: '',
  bankInstituteName: 'the iron bank'
} as AccountTransferInfo;

export const eftPaymentFixture = {
  claimId: 'CLM-1',
  rootCaseNumber: 'NTN-21',
  effectiveDate: '2017-08-18',
  paymentAddress: 'Castle Black, The Wall',
  paymentAmount: '200',
  paymentDate: '2017-08-13',
  paymentId: 'PAY-3',
  paymentMethod: 'Elec Funds Transfer',
  paymentType: 'Recurring',
  periodEndDate: '2017-08-28',
  periodStartDate: '2017-08-06',
  benefitCaseNumber: 'BEN-1',
  nominatedPayeeName: 'Rickard Stark',
  accountTransferInfo: accountTransferInfoFixture
} as CasePayment;

export const paymentFixture = [
  {
    claimId: 'CLM-1',
    rootCaseNumber: 'NTN-21',
    effectiveDate: '2017-08-18',
    paymentAddress: 'Castle Black, The Wall',
    paymentAmount: '200',
    paymentDate: '2017-08-13',
    paymentId: 'PAY-3',
    paymentMethod: 'Check',
    paymentType: 'Recurring',
    periodEndDate: '2017-08-28',
    periodStartDate: '2017-08-06',
    benefitCaseNumber: 'BEN-1',
    chequePaymentInfo: chequePaymentFixture
  }
] as CasePayment[];

export const paymentLinesFixture = [
  {
    lineType: 'pension',
    amount: '500',
    startDate: '2017-08-28',
    endDate: '2017-09-28'
  }
] as PaymentLine[];
