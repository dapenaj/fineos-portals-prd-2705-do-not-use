import { AbsenceStatus } from 'fineos-js-api-client';

import { TimeOffPeriodType } from '../../../app/shared/types';

export const leavePeriodFixture = [
  {
    type: TimeOffPeriodType.TIME_OFF,
    leavePeriod: {
      startDate: '2019-01-01',
      endDate: '2019-02-01',
      lastDayWorked: '2018-12-01',
      expectedReturnToWorkDate: '2019-03-01',
      status: AbsenceStatus.Known
    }
  },
  {
    type: TimeOffPeriodType.TIME_OFF,
    leavePeriod: {
      startDate: '2019-01-02',
      endDate: '2019-02-02',
      lastDayWorked: '2018-12-02',
      expectedReturnToWorkDate: '2019-03-02',
      status: AbsenceStatus.Known
    }
  },
  {
    type: TimeOffPeriodType.REDUCED_SCHEDULE,
    leavePeriod: {
      startDate: '2019-01-03',
      endDate: '2019-02-03',
      status: AbsenceStatus.Known
    }
  },
  {
    type: TimeOffPeriodType.REDUCED_SCHEDULE,
    leavePeriod: {
      startDate: '2019-01-04',
      endDate: '2019-02-04',
      status: AbsenceStatus.Known
    }
  },
  {
    type: TimeOffPeriodType.EPISODIC,
    leavePeriod: {
      startDate: '2019-01-05',
      endDate: '2019-02-05'
    }
  },
  {
    type: TimeOffPeriodType.EPISODIC,
    leavePeriod: {
      startDate: '2019-01-06',
      endDate: '2019-02-06'
    }
  }
];
