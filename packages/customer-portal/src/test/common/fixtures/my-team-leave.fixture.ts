import { MyTeamLeave } from '../../../app/shared/types';

import { notificationsFixture, absencePeriods } from './notification.fixture';

export const myTeamLeaveFixture = [
  {
    ...notificationsFixture[1],
    member: 'John D.',
    absencePeriods
  },
  {
    ...notificationsFixture[2],
    member: 'John D.',
    absencePeriods
  },
  {
    ...notificationsFixture[4],
    member: 'Gregor S.',
    absencePeriods
  }
] as MyTeamLeave[];
