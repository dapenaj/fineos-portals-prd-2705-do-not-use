import { TeamMember } from 'fineos-js-api-client';

export const employeeFixture: TeamMember[] = [
  {
    firstName: 'Arch',
    lastName: 'Stanton',
    secondName: '',
    initials: 'AS',
    employeeId: '1'
  },
  {
    firstName: 'Bob',
    lastName: 'Dobalina',
    secondName: '',
    initials: 'BD',
    employeeId: '2'
  },
  {
    firstName: 'Charles',
    lastName: 'Aznavoour',
    secondName: '',
    initials: 'CA',
    employeeId: '3'
  },
  {
    firstName: 'Frank',
    lastName: 'Capra',

    secondName: '',
    initials: 'FC',
    employeeId: '4'
  }
];
