import { AbsencePeriodDecision } from 'fineos-js-api-client';

export const decisionFixture = {
  absenceCaseId: 'ABS-1186',
  absencePeriodStatus: 'Time off period',
  absencePeriodType: 'Time off period',
  actualForRequestedEpisodic: false,
  adjudicationStatus: 'Accepted',
  applicabilityStatus: 'Applicable',
  approvalReason: 'Please Select',
  availabilityStatus: 'As Certified',
  balanceDeduction: 0,
  decisionStatus: 'Approved',
  denialReason: 'Please Select',
  eligibilityStatus: 'Met',
  endDate: '2019-12-31',
  evidenceStatus: 'N/A',
  fixedYearStartDay: 0,
  fixedYearStartMonth: 'Please Select',
  leavePlanCategory: 'Non-Paid',
  leavePlanId: 'd6f58386-2dd0-481e-95e2-40da2455cf9f',
  leavePlanName: 'My Child Bonding Plan',
  leavePlanShortName: 'My Child Bonding Plan',
  leaveRequestId: 'PL:14432:0000001555',
  parentPeriodId: '',
  periodId: 'PL:14474:0000000822',
  qualifier1: 'Postnatal Disability',
  qualifier2: '',
  reasonName: 'Pregnancy/Maternity',
  selectedLeavePlanId: 'PL:14437:0000000408',
  startDate: '2019-01-01',
  timeBankMethod: 'Please Select',
  timeDecisionReason: '',
  timeDecisionStatus: 'Approved',
  timeEntitlement: 8,
  timeEntitlementBasis: 'Days',
  timeWithinPeriod: 0,
  timeWithinPeriodBasis: 'Please Select',
  timeRequested: 1
} as AbsencePeriodDecision;

export const reducedDecisionFixture = {
  absenceCaseId: 'ABS-1186',
  absencePeriodStatus: 'Reduced time off period',
  absencePeriodType: 'Reduced time off period',
  actualForRequestedEpisodic: false,
  adjudicationStatus: 'Accepted',
  applicabilityStatus: 'Applicable',
  approvalReason: 'Please Select',
  availabilityStatus: 'As Certified',
  balanceDeduction: 0,
  decisionStatus: 'Approved',
  denialReason: 'Please Select',
  eligibilityStatus: 'Met',
  endDate: '2029-12-31',
  evidenceStatus: 'N/A',
  fixedYearStartDay: 0,
  fixedYearStartMonth: 'Please Select',
  leavePlanCategory: 'Non-Paid',
  leavePlanId: 'd6f58386-2dd0-481e-95e2-40da2455cf9f',
  leavePlanName: 'My Child Bonding Plan',
  leavePlanShortName: 'My Child Bonding Plan',
  leaveRequestId: 'PL:14432:0000001556',
  parentPeriodId: '',
  periodId: 'PL:14474:0000000822',
  qualifier1: 'Other',
  qualifier2: '',
  reasonName: 'Pregnancy/Maternity',
  selectedLeavePlanId: 'PL:14437:0000000408',
  startDate: '2029-01-01',
  timeBankMethod: 'Please Select',
  timeDecisionReason: '',
  timeDecisionStatus: 'Approved',
  timeEntitlement: 8,
  timeEntitlementBasis: 'Days',
  timeWithinPeriod: 0,
  timeWithinPeriodBasis: 'Please Select',
  timeRequested: 1
} as AbsencePeriodDecision;
