import { CustomerOccupation, Occupation } from 'fineos-js-api-client';

export const customerOccupationsFixture = [
  {
    dateJobBegan: '2014-02-02',
    dateJobEnded: '',
    daysWorkedPerWeek: 5,
    employer: 'FinCorp Ltd',
    endPosReason: '',
    hoursWorkedPerWeek: 40,
    jobDesc: '',
    jobTitle: 'Line Supervisor',
    remarks: '',
    selfEmployed: false,
    workScheduleDescription: '',
    employmentCategory: '',
    jobStrenuous: '',
    endEmploymentReason: '',
    employmentLocation: '',
    additionalEmploymentCategory: '',
    employmentStatus: '',
    employmentTitle: '',
    employeeId: '',
    primary: true,
    occupationId: 1,
    codeId: '',
    codeName: '',
    overrideDaysWorkedPerWeek: false
  },
  {
    dateJobBegan: '2014-02-06',
    dateJobEnded: '',
    daysWorkedPerWeek: 5,
    employer: 'FinCorp Ltd',
    endPosReason: '',
    hoursWorkedPerWeek: 40,
    jobDesc: '',
    jobTitle: 'Line Supervisor',
    remarks: '',
    selfEmployed: false,
    workScheduleDescription: '',
    employmentCategory: '',
    jobStrenuous: '',
    endEmploymentReason: '',
    employmentLocation: '',
    additionalEmploymentCategory: '',
    employmentStatus: '',
    employmentTitle: '',
    employeeId: '',
    primary: true,
    occupationId: 1,
    codeId: '',
    codeName: '',
    overrideDaysWorkedPerWeek: false
  }
] as CustomerOccupation[];

export const occupationsFixture = [
  {
    employeeId: '123',
    dateOfHire: '2011-06-22',
    department: 'Engineering',
    manager: 'Mark Hernandez',
    jobTitle: 'Logistics Manager',
    workSite: 'City Centre',
    workCity: 'Cleveland',
    workState: 'OH',
    workPattern: 'Every second week',
    hoursWorkedPerWeek: 20,
    hoursWorkedPerYear: 40
  }
] as Occupation[];
