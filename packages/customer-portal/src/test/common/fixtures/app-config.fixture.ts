import { AppConfig } from '../../../app/shared/types';

import { clientConfigFixture } from './client-config.fixture';
import { customerDetailFixture } from './customer-detail.fixture';

export const appConfigFixture: AppConfig = {
  clientConfig: clientConfigFixture,
  customerDetail: customerDetailFixture
};
