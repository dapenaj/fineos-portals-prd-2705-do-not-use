import { ClientConfig } from '../../../app/shared/types';
import * as clientConfig from '../../../../public/config/client-config.json';

export const clientConfigFixture: ClientConfig = clientConfig as ClientConfig;
