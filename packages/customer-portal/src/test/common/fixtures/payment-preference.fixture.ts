import { PaymentPreference } from 'fineos-js-api-client';

export const paymentPreferenceFixture = [
  {
    paymentMethod: 'Hand Typed Check',
    description: 'Check from Jon',
    effectiveFrom: '',
    effectiveTo: '',
    isDefault: true,
    paymentPreferenceId: '201',
    chequeDetails: {
      nameToPrintOnCheck: ''
    },
    nominatedPayee: ''
  },
  {
    paymentMethod: 'Standing Order',
    isDefault: false,
    paymentPreferenceId: '202',
    accountDetails: {
      routingNumber: '4321',
      accountNo: '1234567',
      accountType: 'Checking',
      accountName: 'Tony Montana'
    }
  },
  {
    paymentMethod: 'Wire Transfer',
    isDefault: false,
    paymentPreferenceId: '204',
    accountDetails: {
      routingNumber: '9876',
      accountNo: '4566463',
      accountType: 'savings',
      accountName: 'Joey Soprano'
    }
  },
  {
    paymentMethod: 'Accounting',
    isDefault: false,
    paymentPreferenceId: '205',
    accountDetails: {
      routingNumber: '9999',
      accountNo: '9999333',
      accountType: 'savings',
      accountName: 'Tommy Vercetti'
    }
  }
] as PaymentPreference[];

export const paymentPreferenceEftDefaultFixture = [
  {
    paymentMethod: 'Hand Typed Check',
    description: 'Check from Jon',
    effectiveFrom: '',
    effectiveTo: '',
    isDefault: false,
    paymentPreferenceId: '201',
    chequeDetails: {
      nameToPrintOnCheck: ''
    },
    nominatedPayee: ''
  },
  {
    paymentMethod: 'Standing Order',
    isDefault: false,
    paymentPreferenceId: '202',
    accountDetails: {
      routingNumber: '4321',
      accountNo: '1234567',
      accountType: 'Checking',
      accountName: 'Tony Montana'
    }
  },
  {
    paymentMethod: 'Wire Transfer',
    isDefault: true,
    paymentPreferenceId: '204',
    accountDetails: {
      routingNumber: '9876',
      accountNo: '4566463',
      accountType: 'savings',
      accountName: 'Joey Soprano'
    }
  },
  {
    paymentMethod: 'Accounting',
    isDefault: false,
    paymentPreferenceId: '205',
    accountDetails: {
      routingNumber: '9999',
      accountNo: '9999333',
      accountType: 'savings',
      accountName: 'Tommy Vercetti'
    }
  }
] as PaymentPreference[];
