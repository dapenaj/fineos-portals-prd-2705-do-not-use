import {
  CaseDocument,
  Base64DocumentUploadResponse
} from 'fineos-js-api-client';

export const documentsFixture: CaseDocument[] = [
  {
    caseId: 'NTN-21',
    rootCaseId: 'NTN-21',
    documentId: 1,
    name: 'Employee Statement',
    type: 'Employee Statement 1',
    fileExtension: 'xml',
    fileName: 'employee_statement_01',
    originalFilename: 'employee_statement_01',
    receivedDate: '2017-06-06',
    effectiveFrom: '2017-08-14',
    effectiveTo: '2017-08-18',
    description: 'This is the file description.',
    title: 'Employee Statement',
    status: 'pending',
    privacyTag: 'private',
    createdBy: 'employee',
    dateCreated: '2019-07-19',
    isRead: false
  },
  {
    caseId: 'NTN-22',
    rootCaseId: 'NTN-22',
    documentId: 2,
    name: 'Employee Statement',
    type: 'Employee Statement 2',
    fileExtension: 'xml',
    fileName: 'employee_statement_02',
    originalFilename: 'employee_statement_02',
    receivedDate: '2017-12-06',
    effectiveFrom: '2017-08-14',
    effectiveTo: '2017-08-18',
    description: 'This is the file description.',
    title: 'Employee Statement',
    status: 'pending',
    privacyTag: 'private',
    createdBy: 'employee',
    dateCreated: '2019-07-19',
    isRead: true
  }
] as CaseDocument[];

export const documentResponseFixture = [
  { documentId: 1 }
] as Base64DocumentUploadResponse[];
