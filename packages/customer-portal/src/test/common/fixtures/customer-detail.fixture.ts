import {
  occupationsFixture,
  customerOccupationsFixture
} from './occupations.fixture';
import { customerFixture } from './customer.fixture';
import { CustomerDetail } from './../../../app/shared/types/customer.type';
import { contactFixture } from './contact.fixture';

export const customerDetailFixture: CustomerDetail = {
  customer: customerFixture,
  occupation: occupationsFixture[0],
  customerOccupations: customerOccupationsFixture,
  contact: contactFixture
};
