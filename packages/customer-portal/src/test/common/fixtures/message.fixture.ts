import { Message } from 'fineos-js-api-client';

export const messagesFixture = [
  {
    messageId: 1,
    caseId: 'CLM-1',
    subject: 'Phasellus aliquet tristique maximus testowyTekst',
    narrative: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    contactTime: '2017-09-25T14:16:23.279',
    msgOriginatesFromPortal: true,
    read: false
  },
  {
    messageId: 2,
    caseId: 'ABS-7',
    subject: 'Etiam vitae est at ex feugiat condimentum',
    narrative: 'testowyTekst Aliquam auctor a ligula non imperdiet.',
    contactTime: '2017-10-01T09:45:23.279',
    msgOriginatesFromPortal: false,
    read: true
  },
  {
    messageId: 3,
    caseId: 'CLM-2',
    subject: 'Maecenas vitae magna neque',
    narrative:
      'Cras vulputate varius odio, eget aliquet tellus ultricies sit amet.',
    contactTime: '2017-10-03T10:13:23.279',
    msgOriginatesFromPortal: true,
    read: true
  },
  {
    messageId: 4,
    caseId: 'CLM-3',
    subject: 'Sed lacinia mi nec felis elementum vehicula',
    narrative: 'Fusce a orci aliquam, vulputate erat id, gravida eros.',
    contactTime: '2017-10-04T18:47:23.279',
    msgOriginatesFromPortal: true,
    read: false
  },
  {
    messageId: 5,
    caseId: '0',
    subject: 'Etiam vitae est at ex feugiat condimentum',
    narrative: 'testowyTekst Aliquam auctor a ligula non imperdiet.',
    contactTime: '2017-10-01T09:45:23.279',
    msgOriginatesFromPortal: false,
    read: false
  }
] as Message[];
