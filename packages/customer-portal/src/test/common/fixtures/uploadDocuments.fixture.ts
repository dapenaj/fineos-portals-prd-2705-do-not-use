import { SupportingEvidence } from 'fineos-js-api-client';

import { UploadDocument } from '../../../app/shared/types';

export const supportingEvidenceFixture: SupportingEvidence[] = [
  {
    rootCaseId: 'NTN-21',
    uploadCaseNumber: 'NTN-21-ABS-1',
    docReceived: false,
    name: 'Document 1'
  }
];

export const uploadDocumentsFixture: UploadDocument[] = [
  {
    id: 0,
    caseId: 'NTN-21',
    evidence: supportingEvidenceFixture[0]
  }
];
