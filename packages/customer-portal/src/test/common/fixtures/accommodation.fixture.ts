import { AccommodationCaseSummary } from 'fineos-js-api-client';

export const accommodationFixture = {
  accommodationCaseId: 'ACC-324',
  status: 'Pending',
  notifiedBy: '',
  notificationDate: '',
  accommodationDecision: '',
  decisionDate: '2018-08-28',
  pregnancyRelated: 'Yes',
  caseHandler: 'Benny Smith',
  caseHandlerPhoneNumber: '01-234-56789',
  caseHandlerEmailAddress: '',
  workplaceAccommodationDetails: [
    {
      accommodationCategory:
        'Physical workplace modifications or workstation relocation',
      accommodationType: 'Make materials and equipment within reach',
      accommodationDescription: 'New desk required',
      id: 'WAC-56',
      creationDate: '2018-08-29',
      implementDate: '2018-09-12',
      implementedDate: '2018-09-14',
      accommodationEndDate: '2019-12-30'
    },
    {
      accommodationCategory: 'Reassignment',
      accommodationType: 'Different Workstation',
      accommodationDescription: '',
      id: 'WAC-12',
      creationDate: '2018-08-29',
      implementDate: '2018-09-12',
      implementedDate: '2018-09-14',
      accommodationEndDate: '2019-12-30'
    }
  ],
  limitations: [
    {
      limitationType: 'Blind',
      id: 'LIM-45'
    },
    {
      limitationType: 'Writing/Spelling',
      id: 'LIM-23'
    }
  ],
  additionalNotes: '',
  notificationCaseId: 'NTN-368',
  notificationReason: 'Accommodation required to remain at work'
} as AccommodationCaseSummary;
