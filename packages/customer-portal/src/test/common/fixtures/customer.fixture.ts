import { Customer } from 'fineos-js-api-client';

export const customerFixture = {
  firstName: 'Jane',
  lastName: 'Doe',
  secondName: '',
  initials: 'JD',
  needsInterpretor: false,
  placeOfBirth: '',
  gender: 'Female',
  dateOfBirth: '1988-01-01',
  maritalStatus: 'Single',
  nationality: 'Unknown',
  title: 'Ms',
  preferredContactMethod: 'Unknown',
  idNumber: '741852963',
  identificationNumberType: 'ID',
  securedClient: true,
  staff: true,
  partyType: 'party',
  customerAddress: {
    address: {
      premiseNo: '123',
      addressLine1: 'Main street',
      addressLine2: `Where it's at`,
      addressLine3: 'Two turn tables',
      addressLine4: 'And a microphone',
      addressLine5: 'Somewhere',
      addressLine6: 'CA',
      addressLine7: 'Way up high',
      postCode: 'AB123456',
      country: 'USA'
    }
  }
} as Customer;
