import { Contact } from 'fineos-js-api-client';

export const contactFixture: Contact = {
  phoneNumbers: [
    {
      phoneNumberType: 'Home',
      intCode: '353',
      areaCode: '1',
      telephoneNo: '234567',
      preferred: true
    }
  ],
  emailAddresses: [
    {
      preferred: false,
      emailAddress: 'bob@dobalina.com'
    }
  ]
};
