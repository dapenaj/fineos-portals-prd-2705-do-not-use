import { EFormSummary, EForm, EFormAttribute } from 'fineos-js-api-client';

import { ConfirmReturnEForm } from '../../../app/shared/services';

export const confirmReturnEFormFixture = {
  id: 1,
  expectedRTWDate: null,
  employeeConfirmed: true,
  returningAsExpected: null,
  actualRTWDate: null,
  employerNotified: true,
  rtwConfirmationNotes: 'value'
} as ConfirmReturnEForm;

export const eFormSummaryFixture = {
  eFormId: 1,
  eFormTypeId: 'string',
  effectiveDateFrom: '2019-10-09',
  effectiveDateTo: '2019-10-09',
  eFormType: 'Confirm Return to Work Details'
} as EFormSummary;

export const eFormFixture = {
  eFormId: 1,
  eFormType: 'Confirm Return to Work Details',
  eFormAttributes: [
    {
      name: 'expectedReturnDate',
      dateValue: '2020-01-01'
    } as EFormAttribute,
    {
      name: 'employeeConfirmed',
      booleanValue: false
    } as EFormAttribute
  ]
} as EForm;
