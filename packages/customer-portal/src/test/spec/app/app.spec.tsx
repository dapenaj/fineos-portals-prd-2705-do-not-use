import React from 'react';

import { mount, DefaultContext } from '../../config';
import {
  fetchClientConfigSpy,
  fetchContactDetailsSpy,
  fetchCustomerDetailsSpy,
  fetchCustomerOccupationsSpy,
  fetchOccupationSpy
} from '../../common/spys';
import { releaseEventLoop } from '../../common/utils';
import { Routes } from '../../../app/modules/routes';
import App from '../../../app';

describe('App', () => {
  const mountApp = () =>
    mount(
      <DefaultContext>
        <App />
      </DefaultContext>
    );

  test('render', async () => {
    fetchClientConfigSpy('success');
    fetchContactDetailsSpy('success');
    fetchCustomerDetailsSpy('success');
    fetchCustomerOccupationsSpy('success');
    fetchOccupationSpy('success');

    const wrapper = mountApp();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Routes)).toExist();
  });
});
