import { SdkConfigProps } from 'fineos-js-api-client';

import { AuthorizationService } from '../../../../../app/shared/services';
import { Role } from '../../../../../app/shared/types';

describe('Authorization Service', () => {
  const service = AuthorizationService.getInstance();

  describe('isAbsenceUser', () => {
    it('should be true if user has absenceUser role', () => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.ABSENCE_USER);

      service.authenticationService.displayRoles = [Role.ABSENCE_USER];

      expect(service.isAbsenceUser).toBe(true);
    });

    it('should be false if user does not have absenceUser role', () => {
      service.authenticationService.displayRoles = ['test-role'];
      expect(service.isAbsenceUser).toBe(false);
    });

    it('should be true if user is absence supervisor', () => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.ABSENCE_SUPERVISOR);

      service.authenticationService.displayRoles = [Role.ABSENCE_SUPERVISOR];

      expect(service.isAbsenceUser).toBe(true);
    });
  });

  describe('isAbsenceSupervisor', () => {
    it('should be true if user has absenceSupervisor role', () => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.ABSENCE_SUPERVISOR);

      service.authenticationService.displayRoles = [Role.ABSENCE_SUPERVISOR];

      expect(service.isAbsenceSupervisor).toBe(true);
    });

    it('should be false if user does not have absenceSupervisor role', () => {
      service.authenticationService.displayRoles = ['test-role'];

      expect(service.isAbsenceSupervisor).toBe(false);
    });
  });

  describe('isClaimsUser', () => {
    it('should be true if user has claimsUser role', () => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.CLAIMS_USER);

      service.authenticationService.displayRoles = [Role.CLAIMS_USER];

      expect(service.isClaimsUser).toBe(true);
    });

    it('should be false if user does not have claimsUser role', () => {
      service.authenticationService.displayRoles = ['test-role'];

      expect(service.isClaimsUser).toBe(false);
    });

    it('should be true if user is absence user', () => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.ABSENCE_USER);

      service.authenticationService.displayRoles = [Role.ABSENCE_USER];

      expect(service.isClaimsUser).toBe(true);
    });

    it('should be true if user is absence supervisor', () => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.ABSENCE_SUPERVISOR);

      service.authenticationService.displayRoles = [Role.ABSENCE_SUPERVISOR];

      expect(service.isClaimsUser).toBe(true);
    });
  });
});
