import { RequestDetailFormService } from '../../../../../app/shared/services';
import {
  EventOptionId,
  RequestDetailFormUsage
} from '../../../../../app/shared/types';
import {
  RequestDetailFormId,
  RequestDetailForm,
  RequestDetailFieldId,
  RequestDetailField,
  RequestDetailAnswerType
} from '../../../../../app/shared/types';

describe('Request Detail Form Service', () => {
  // A sample model so changes to the one in the singleton won't affect this test of the basic functionality
  const TEST_FORM_USAGE: RequestDetailFormUsage[] = [
    {
      formId: RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
      defaultFor: [EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS]
    },
    {
      formId: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
      defaultFor: [
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL,
        EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
      ],
      negates: [RequestDetailFormId.TIME_OFF_FOR_SICKNESS]
    }
  ];

  // A sample model so changes to the one in the singleton won't affect this test of the basic functionality
  const TEST_FORM_MODEL: Map<RequestDetailFormId, RequestDetailForm> = new Map<
    RequestDetailFormId,
    RequestDetailForm
  >([
    [
      RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
      {
        id: RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
        layout: [
          {
            id: RequestDetailFieldId.DESCRIBE_SICKNESS,
            answerType: RequestDetailAnswerType.LONG_TEXT
          }
        ]
      }
    ],
    [
      RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
      {
        id: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
        layout: [
          {
            id: RequestDetailFieldId.DONATION_OVERNIGHT_REQUIRED,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.YES,
                targetFormId:
                  RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY
              },
              { id: RequestDetailFieldId.NO }
            ]
          }
        ]
      }
    ],
    [
      RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY,
      {
        id: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY,
        layout: [
          {
            id: RequestDetailFieldId.DONATION_OVERNIGHT_DURATION,
            answerType: RequestDetailAnswerType.DATE_RANGE
          }
        ]
      }
    ]
  ]);

  const service = new RequestDetailFormService(
    TEST_FORM_USAGE,
    TEST_FORM_MODEL
  );

  describe('Check Forms for valid path', () => {
    it('default forms should be as expected', () => {
      const path: EventOptionId[] = [
        EventOptionId.SICKNESS,
        EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS,
        EventOptionId.SICKNESS_TIMEOFF_DONATION,
        EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
      ];

      const expected: RequestDetailFormId[] = [
        RequestDetailFormId.MEDICAL_DONATION_PROCEDURE
      ];
      const observed: RequestDetailFormId[] = service
        .getDefaultForms(path)
        .map(r => r.id);

      expect(expected).toStrictEqual(observed);
    });

    it('A path with no forms should work ok', () => {
      const path: EventOptionId[] = [EventOptionId.SICKNESS];

      const expected: RequestDetailFormId[] = [];
      const observed: RequestDetailFormId[] = service
        .getDefaultForms(path)
        .map(r => r.id);

      expect(expected).toStrictEqual(observed);
    });

    it('A form sub selection should work', () => {
      // get the form - it should exist
      const testForm: RequestDetailForm | undefined = service.getForm(
        RequestDetailFormId.MEDICAL_DONATION_PROCEDURE
      );
      expect(testForm).toBeDefined();
      if (testForm) {
        // this will scan the form to find a field with a given id
        const findField = (
          form: RequestDetailForm,
          fieldId: RequestDetailFieldId
        ) => form.layout.find(r => r.id === fieldId);

        // get this field - it should exist
        const field: RequestDetailField | undefined = findField(
          testForm,
          RequestDetailFieldId.DONATION_OVERNIGHT_REQUIRED
        );
        expect(field).toBeDefined();
        if (field) {
          // confirm it's of type options
          expect(field.answerType).toStrictEqual(
            RequestDetailAnswerType.OPTIONS
          );
          expect(field.options).toBeDefined();

          // check the two options
          if (field.options) {
            // the first option should be yes, and have a form, and the form should be findable
            expect(field.options[0].id).toStrictEqual(RequestDetailFieldId.YES);
            expect(field.options[0].targetFormId).toStrictEqual(
              RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY
            );
            // find the form
            if (field.options[0].targetFormId) {
              const testSubForm:
                | RequestDetailForm
                | undefined = service.getForm(field.options[0].targetFormId);
              expect(testSubForm).toBeDefined();
            }

            // the second option should be no, and not ahve any form
            expect(field.options[1].id).toStrictEqual(RequestDetailFieldId.NO);
            expect(field.options[1].targetFormId).toBeUndefined();
          }
        }
      }
    });
  });
});
