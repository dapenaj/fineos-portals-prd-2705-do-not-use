import { EventDetailOptionService } from '../../../../../../app/shared/services';
import {
  EventOptionId,
  RequestDetailFieldId,
  RequestDetailFormId,
  RequestDetailAnswerType
} from '../../../../../../app/shared/types';
import {
  givenAbsenceUser,
  givenCustomerUser
} from '../../../../../common/utils';
import {
  thenPathHasNoChildren,
  thenPathHasChildren,
  thenPathHasDefaultForms,
  thenFormHasFields,
  thenFormHasSubForm,
  thenFormHasNoSubForm
} from '../../../../../common/utils';

describe('Intake: Sickness', () => {
  describe('Intake: Sickness: EventDetails', () => {
    describe('Check option tree - claims users', () => {
      beforeEach(() => {
        givenCustomerUser();
      });

      it('Sickness should be available', () => {
        expect(EventDetailOptionService.getInstance().start()).toContain(
          EventOptionId.SICKNESS
        );
      });

      it('Sickness should have no sub options', () => {
        thenPathHasNoChildren([EventOptionId.SICKNESS]);
      });
    });

    describe('Check option tree', () => {
      describe('Check option tree - absence users', () => {
        beforeEach(() => {
          givenAbsenceUser();
        });

        it('Sickness should be available', () => {
          expect(EventDetailOptionService.getInstance().start()).toContain(
            EventOptionId.SICKNESS
          );
        });

        it('Sickness should have have sub options checking why is the absence', () => {
          thenPathHasChildren(
            [EventOptionId.SICKNESS],
            [
              EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS,
              EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE,
              EventOptionId.SICKNESS_TIMEOFF_DONATION
            ]
          );
        });

        it('If the reason is to donate, the user should be asked what is being donated', () => {
          thenPathHasChildren(
            [EventOptionId.SICKNESS, EventOptionId.SICKNESS_TIMEOFF_DONATION],
            [
              EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
            ]
          );
        });
      });
    });
  });

  describe('Intake: Sickness: RequestDetails', () => {
    describe('Derive forms - absence users', () => {
      beforeEach(() => {
        givenAbsenceUser();
      });

      describe('Derive forms - time off for preventive care ', () => {
        it('Derive forms - time off for preventive - has no default forms', () => {
          thenPathHasDefaultForms(
            [EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE],
            []
          );
        });
      });

      describe('Derive forms - time off for sickness ', () => {
        it('Derive forms - time off for sickness - default forms', () => {
          thenPathHasDefaultForms(
            [EventOptionId.SICKNESS],
            [RequestDetailFormId.TIME_OFF_FOR_SICKNESS]
          );

          thenPathHasDefaultForms(
            [
              EventOptionId.SICKNESS,
              EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
            ],
            [RequestDetailFormId.TIME_OFF_FOR_SICKNESS]
          );

          thenFormHasFields(RequestDetailFormId.TIME_OFF_FOR_SICKNESS, [
            {
              id: RequestDetailFieldId.DESCRIBE_SICKNESS,
              answerType: RequestDetailAnswerType.LONG_TEXT
            },
            {
              id: RequestDetailFieldId.IS_SICKNESS_WORK_RELATED,
              answerType: RequestDetailAnswerType.BOOLEAN
            },
            {
              id: RequestDetailFieldId.FIRST_SYMPTOM_DATE,
              answerType: RequestDetailAnswerType.DATE
            },
            {
              id: RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
              answerType: RequestDetailAnswerType.OPTIONS
            },
            {
              id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
              answerType: RequestDetailAnswerType.LONG_TEXT
            }
          ]);

          thenFormHasSubForm(
            RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
            RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
            RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
            RequestDetailFieldId.YES,
            RequestDetailFormId.MEDICAL_TREATMENT_FORM
          );
        });

        it('Derive forms - time off for sickness - medical treatment form', () => {
          thenFormHasFields(RequestDetailFormId.MEDICAL_TREATMENT_FORM, [
            {
              id: RequestDetailFieldId.DATE_FIRST_TREATED,
              answerType: RequestDetailAnswerType.DATE
            },
            {
              id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
              answerType: RequestDetailAnswerType.OPTIONS
            }
          ]);

          thenFormHasSubForm(
            RequestDetailFormId.MEDICAL_TREATMENT_FORM,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.NO,
            RequestDetailFormId.NO_OVERNIGHT_STAY_MEDICAL_TREATMENT
          );

          thenFormHasSubForm(
            RequestDetailFormId.MEDICAL_TREATMENT_FORM,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.YES,
            RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
          );
        });

        it('Derive forms - time off for sickness - yes overnight stay', () => {
          thenFormHasFields(
            RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT,
            [
              {
                id: RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION,
                answerType: RequestDetailAnswerType.DATE_RANGE
              }
            ]
          );
        });

        it('Derive forms - time off for sickness - no overnight stay', () => {
          thenFormHasFields(
            RequestDetailFormId.NO_OVERNIGHT_STAY_MEDICAL_TREATMENT,
            [
              {
                id: RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT,
                answerType: RequestDetailAnswerType.BOOLEAN
              },
              {
                id: RequestDetailFieldId.IS_SICKNESS_CHRONIC,
                answerType: RequestDetailAnswerType.BOOLEAN
              },
              {
                id: RequestDetailFieldId.IS_MULTIPLE_TREATMENT,
                answerType: RequestDetailAnswerType.BOOLEAN
              }
            ]
          );
        });
      });

      describe('Derive forms - time off for donation', () => {
        it('Derive forms - no forms for blood donation', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.SICKNESS,
              EventOptionId.SICKNESS_TIMEOFF_DONATION,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD
            ],
            []
          );
        });
      });

      describe('Derive forms - non-blood donation', () => {
        it('Derive forms - stemcell donation-  has default forms', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.SICKNESS,
              EventOptionId.SICKNESS_TIMEOFF_DONATION,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL
            ],
            [RequestDetailFormId.MEDICAL_DONATION_PROCEDURE]
          );

          thenFormHasFields(RequestDetailFormId.MEDICAL_DONATION_PROCEDURE, [
            {
              id: RequestDetailFieldId.DESCRIBE_PROCEDURE,
              answerType: RequestDetailAnswerType.LONG_TEXT
            },
            {
              id: RequestDetailFieldId.DONATION_DATE,
              answerType: RequestDetailAnswerType.DATE
            },
            {
              id: RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
              answerType: RequestDetailAnswerType.OPTIONS
            }
          ]);

          thenFormHasNoSubForm(
            RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
            RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
            RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.YES,
            RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY
          );

          thenFormHasFields(
            RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY,
            [
              {
                id: RequestDetailFieldId.DONATION_OVERNIGHT_DURATION,
                answerType: RequestDetailAnswerType.DATE_RANGE
              }
            ]
          );
        });

        it('Derive forms - bone marrow donation-  has default forms', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.SICKNESS,
              EventOptionId.SICKNESS_TIMEOFF_DONATION,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW
            ],
            [RequestDetailFormId.MEDICAL_DONATION_PROCEDURE]
          );

          // this points to the same form as above - not testing it again
        });

        it('Derive forms - medical donation procedure - has exepected fields', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.SICKNESS,
              EventOptionId.SICKNESS_TIMEOFF_DONATION,
              EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
            ],
            [RequestDetailFormId.MEDICAL_DONATION_PROCEDURE]
          );
        });
        // this points to the same form as above - not testing it again
      });
    });
  });
});
