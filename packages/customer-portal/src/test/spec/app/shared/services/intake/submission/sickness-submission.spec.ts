import {
  AbsenceCaseNotifiedBy,
  AbsenceService,
  AdditionalConditionsService,
  ClaimService,
  IntakeSource
} from 'fineos-js-api-client';
import moment from 'moment';
import each from 'jest-each';

import {
  CLAIM_CASE_TYPE,
  DISABILITY_REQUEST_SOURCE
} from '../../../../../../../app/shared/constants';
import { SicknessIntake } from '../../../../../../../app/shared/intakes';
import {
  givenAbsenceUser,
  givenPromise,
  mockField
} from '../../../../../../common/utils';
import {
  EventOptionId,
  RequestDetailFieldId,
  TimeOff
} from '../../../../../../../app/shared/types';
import { IntakeSubmission } from '../../../../../../../app/shared/services';
import { API_DATE_FORMAT } from '../../../../../../../app/shared/utils';

import {
  episodicLeavePeriods,
  episodicRequests,
  reducedScheduleLeavePeriods,
  reducedScheduleRequests,
  testProcessShouldWork,
  timeOffLeavePeriods,
  timeOffRequests
} from './submission-test-utils';

describe('Sickness Intake Submission', () => {
  const NOTIFICATION_ID = 'CASE123';
  const ABSENCE_ID = 'ABSENCE_ID';
  const CLAIM_ID = 'CLAIM_ID';

  let intake: SicknessIntake;
  let mockAbsenceService: AbsenceService;
  let mockClaimService: ClaimService;
  let mockAdditionalConditionsService: AdditionalConditionsService;

  beforeEach(() => {
    intake = new SicknessIntake();

    mockAbsenceService = mockField(
      intake,
      'absenceService',
      new AbsenceService()
    );

    mockClaimService = mockField(intake, 'claimService', new ClaimService());

    mockAdditionalConditionsService = mockField(
      intake,
      'additionalConditionsService',
      new AdditionalConditionsService()
    );
  });

  const givenAllPostCaseRequestsWork = () => {
    // assume all underlying service requests work
    mockAbsenceService.addHospitalisationDetails = givenPromise({});
    mockClaimService.updateMedicalDetails = givenPromise({});
    mockClaimService.updateDiabilityDetails = givenPromise({});
    mockClaimService.addHospitalisationDetails = givenPromise({});
    mockClaimService.indicateMultiplePeriodsInAbsence = givenPromise({});
    mockAdditionalConditionsService.addAdditionalConditions = givenPromise({});
  };

  describe('Absence User', () => {
    beforeEach(() => {
      givenAbsenceUser();
    });

    test('Preventive Care - Time off', done => {
      const selectedOptions = [
        EventOptionId.SICKNESS,
        EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE
      ];

      // I am taking time off
      const timeOff: TimeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      };

      // given a simple birth disability model
      const model = new IntakeSubmission(selectedOptions, new Map(), timeOff);

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        absenceId: ABSENCE_ID
      });

      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
          result.createdAbsence
        );
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2018-08-25',
          notificationCaseId: NOTIFICATION_ID,
          notificationReason:
            'Sickness, treatment required for a medical condition or any other medical procedure'
        });

        expect(mockAbsenceService.registerAbsence).toBeCalledWith({
          notificationReason:
            'Sickness, treatment required for a medical condition or any other medical procedure',
          reason: 'Preventative Care - Employee',
          notifiedBy: AbsenceCaseNotifiedBy.Employee,
          intakeSource: IntakeSource.SelfService,
          episodicRequests,
          reducedScheduleRequests,
          timeOffRequests
        });

        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            claimIncurredDate: '2018-08-25',
            disabilityDateFromCustomer: '2018-08-25',
            eventType: 'Sickness',
            claimType: 'STD',
            expectedReturnToWorkDate: '2019-02-28',
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            notificationDate: moment().format(API_DATE_FORMAT),
            workRelated: false,
            source: DISABILITY_REQUEST_SOURCE
          }
        );

        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
      });
    });

    describe('Donation - Time Off', () => {
      test('Blood Donating', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_DONATION,
          EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD
        ];

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(selectedOptions, new Map(), timeOff);

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Medical Donation - Employee',
            reasonQualifier1: 'Blood',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      test('Blood Stem Cell Donating', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_DONATION,
          EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(RequestDetailFieldId.DESCRIBE_PROCEDURE, 'donating');
        fieldValues.set(RequestDetailFieldId.DONATION_DATE, '2019-07-04');
        fieldValues.set(
          RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
          false
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Medical Donation - Employee',
            reasonQualifier1: 'Blood Stem Cell',
            additionalComments: 'donating',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'donating',
              symptomsFirstAppeared: '2019-07-04'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              dateSymptomsFirstAppeared: '2019-07-04',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      test('Bone Marrow Donating', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_DONATION,
          EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_PROCEDURE,
          'donating bone'
        );
        fieldValues.set(RequestDetailFieldId.DONATION_DATE, '2019-07-05');
        fieldValues.set(
          RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
          true
        );
        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2019-08-05',
          '2019-08-14'
        ]);

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Medical Donation - Employee',
            reasonQualifier1: 'Bone Marrow',
            additionalComments: 'donating bone',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'donating bone',
              symptomsFirstAppeared: '2019-07-05'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              dateSymptomsFirstAppeared: '2019-07-05',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2019-08-05',
            endDate: '2019-08-14'
          });

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      test('Organ Donating', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_DONATION,
          EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_PROCEDURE,
          'donating organ'
        );
        fieldValues.set(RequestDetailFieldId.DONATION_DATE, '2019-07-05');
        fieldValues.set(
          RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
          true
        );
        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2019-08-05',
          '2019-08-14'
        ]);

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Medical Donation - Employee',
            reasonQualifier1: 'Organ',
            additionalComments: 'donating organ',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'donating organ',
              symptomsFirstAppeared: '2019-07-05'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              dateSymptomsFirstAppeared: '2019-07-05',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2019-08-05',
            endDate: '2019-08-14'
          });

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });
    });

    describe('Sickness - Time off', () => {
      test('Serious Health Condition - Work Related', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'I fell down the stairs!'
        );

        // sickness is work related
        fieldValues.set(RequestDetailFieldId.IS_SICKNESS_WORK_RELATED, true);

        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-25');

        // I required medical treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          true
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-08-25');

        // sickness requires overnight hospital stay
        fieldValues.set(RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY, true);

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'It hurt'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'It hurt'
          });

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Serious Health Condition - Employee',
            reasonQualifier1: 'Work Related',
            reasonQualifier2: 'Sickness',
            additionalComments: 'It hurt',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'I fell down the stairs!',
              firstDoctorVisitDate: '2018-08-25',
              symptomsFirstAppeared: '2018-08-25'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'It hurt',
              dateSymptomsFirstAppeared: '2018-08-25',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDateConfirmed: true,
              startDate: '2018-08-25',
              endDate: '2018-08-30'
            }
          );

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      test('Serious Health Condition - Not Work Related', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'I fell down the stairs!'
        );

        // sickness is not work related
        fieldValues.set(RequestDetailFieldId.IS_SICKNESS_WORK_RELATED, false);

        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-25');

        // I required medical treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          true
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-08-25');

        // sickness requires overnight hospital stay
        fieldValues.set(RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY, true);

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'It hurt'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'It hurt'
          });

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Serious Health Condition - Employee',
            reasonQualifier1: 'Not Work Related',
            reasonQualifier2: 'Sickness',
            additionalComments: 'It hurt',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'I fell down the stairs!',
              firstDoctorVisitDate: '2018-08-25',
              symptomsFirstAppeared: '2018-08-25'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'It hurt',
              dateSymptomsFirstAppeared: '2018-08-25',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDateConfirmed: true,
              startDate: '2018-08-25',
              endDate: '2018-08-30'
            }
          );

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      each([
        [
          [
            [RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT, false],
            [RequestDetailFieldId.IS_SICKNESS_CHRONIC, true],
            [RequestDetailFieldId.IS_MULTIPLE_TREATMENT, true]
          ]
        ],
        [
          [
            [RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT, true],
            [RequestDetailFieldId.IS_SICKNESS_CHRONIC, false],
            [RequestDetailFieldId.IS_MULTIPLE_TREATMENT, true]
          ]
        ],
        [
          [
            [RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT, true],
            [RequestDetailFieldId.IS_SICKNESS_CHRONIC, true],
            [RequestDetailFieldId.IS_MULTIPLE_TREATMENT, false]
          ]
        ],
        [
          [
            [RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT, true],
            [RequestDetailFieldId.IS_SICKNESS_CHRONIC, false],
            [RequestDetailFieldId.IS_MULTIPLE_TREATMENT, false]
          ]
        ],
        [
          [
            [RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT, false],
            [RequestDetailFieldId.IS_SICKNESS_CHRONIC, true],
            [RequestDetailFieldId.IS_MULTIPLE_TREATMENT, false]
          ]
        ],
        [
          [
            [RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT, false],
            [RequestDetailFieldId.IS_SICKNESS_CHRONIC, false],
            [RequestDetailFieldId.IS_MULTIPLE_TREATMENT, true]
          ]
        ]
      ]).test(
        'Serious when medical treatment received, test case %#',
        (options, done) => {
          const selectedOptions = [
            EventOptionId.SICKNESS,
            EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
          ];

          const fieldValues = new Map<RequestDetailFieldId, any>();

          fieldValues.set(
            RequestDetailFieldId.DESCRIBE_SICKNESS,
            'I fell down the stairs!'
          );

          // sickness is not work related
          fieldValues.set(RequestDetailFieldId.IS_SICKNESS_WORK_RELATED, false);

          fieldValues.set(
            RequestDetailFieldId.FIRST_SYMPTOM_DATE,
            '2018-08-25'
          );

          // I required medical treatment
          fieldValues.set(
            RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
            true
          );

          fieldValues.set(
            RequestDetailFieldId.DATE_FIRST_TREATED,
            '2018-08-26'
          );

          // sickness not requires overnight hospital stay
          fieldValues.set(
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            false
          );

          for (const [field, value] of options) {
            fieldValues.set(field, value);
          }

          fieldValues.set(
            RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
            'It hurt'
          );

          const timeOff: TimeOff = {
            outOfWork: true,

            // these dates are backwards, but we should pick the second one as it's earliest
            timeOffLeavePeriods,
            reducedScheduleLeavePeriods,
            episodicLeavePeriods
          };

          const model = new IntakeSubmission(
            selectedOptions,
            fieldValues,
            timeOff
          );

          // given the absence and claims creation are ok
          mockAbsenceService.registerAbsence = givenPromise({
            notificationCaseId: NOTIFICATION_ID,
            absenceId: ABSENCE_ID
          });
          mockClaimService.startClaim = givenPromise({
            notificationCaseId: NOTIFICATION_ID,
            claimId: CLAIM_ID
          });

          givenAllPostCaseRequestsWork();

          // when we process it, the notification id should come back
          testProcessShouldWork(intake, done, model, result => {
            expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
            expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
              result.createdAbsence
            );
            expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

            expect(
              mockAdditionalConditionsService.addAdditionalConditions
            ).toBeCalledWith({
              caseId: ABSENCE_ID,
              additionalDescription: 'It hurt'
            });

            expect(mockClaimService.startClaim).toBeCalledWith(
              CLAIM_CASE_TYPE,
              {
                claimIncurredDate: '2018-08-25',
                notificationCaseId: NOTIFICATION_ID,
                notificationReason:
                  'Sickness, treatment required for a medical condition or any other medical procedure'
              }
            );

            expect(mockAbsenceService.registerAbsence).toBeCalledWith({
              notificationReason:
                'Sickness, treatment required for a medical condition or any other medical procedure',
              reason: 'Serious Health Condition - Employee',
              reasonQualifier1: 'Not Work Related',
              reasonQualifier2: 'Sickness',
              additionalComments: 'It hurt',
              notifiedBy: AbsenceCaseNotifiedBy.Employee,
              intakeSource: IntakeSource.SelfService,
              episodicRequests,
              reducedScheduleRequests,
              timeOffRequests
            });

            expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
              CLAIM_ID,
              {
                condition: 'I fell down the stairs!',
                symptomsFirstAppeared: '2018-08-25',
                firstDoctorVisitDate: '2018-08-26'
              }
            );

            expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
              CLAIM_ID,
              {
                // date of first absence
                claimIncurredDate: '2018-08-25',
                disabilityDateFromCustomer: '2018-08-25',
                eventType: 'Sickness',
                claimType: 'STD',
                claimAdditionalInfo: 'It hurt',
                dateSymptomsFirstAppeared: '2018-08-25',
                expectedReturnToWorkDate: '2019-02-28',
                employeeDateLastWorked: '2018-08-24',
                firstDayMissedWork: '2018-08-25',
                notificationDate: moment().format(API_DATE_FORMAT),
                workRelated: false,
                source: DISABILITY_REQUEST_SOURCE
              }
            );

            expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith(
              {
                caseId: ABSENCE_ID,
                ovenightAtHospital: false
              }
            );

            expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

            expect(
              mockClaimService.indicateMultiplePeriodsInAbsence
            ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
          });
        }
      );

      test('Non Serious - Not Overnight Hospital', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'I fell down the stairs!'
        );

        // sickness is not work related
        fieldValues.set(RequestDetailFieldId.IS_SICKNESS_WORK_RELATED, false);

        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-25');

        // I required medical treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          true
        );

        fieldValues.set(RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY, false);

        fieldValues.set(
          RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT,
          false
        );
        fieldValues.set(RequestDetailFieldId.IS_SICKNESS_CHRONIC, false);
        fieldValues.set(RequestDetailFieldId.IS_MULTIPLE_TREATMENT, false);

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'It hurt'
        );

        const timeOff: TimeOff = {
          outOfWork: true,

          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'It hurt'
          });

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Sickness - Non-Serious Health Condition - Employee',
            additionalComments: 'It hurt',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'I fell down the stairs!',
              symptomsFirstAppeared: '2018-08-25'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'It hurt',
              dateSymptomsFirstAppeared: '2018-08-25',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      test('Serious - Not Overnight Hospital', done => {
        const selectedOptions = [
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'I fell down the stairs!'
        );

        // sickness is work related
        fieldValues.set(RequestDetailFieldId.IS_SICKNESS_WORK_RELATED, true);

        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-25');

        // I required medical treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          true
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-08-25');

        fieldValues.set(RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY, false);

        fieldValues.set(
          RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT,
          true
        );
        fieldValues.set(RequestDetailFieldId.IS_SICKNESS_CHRONIC, true);
        fieldValues.set(RequestDetailFieldId.IS_MULTIPLE_TREATMENT, true);

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'It hurt'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'It hurt'
          });

          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure'
          });

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason:
              'Sickness, treatment required for a medical condition or any other medical procedure',
            reason: 'Serious Health Condition - Employee',
            reasonQualifier1: 'Work Related',
            reasonQualifier2: 'Sickness',
            additionalComments: 'It hurt',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'I fell down the stairs!',
              firstDoctorVisitDate: '2018-08-25',
              symptomsFirstAppeared: '2018-08-25'
            }
          );

          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'It hurt',
              dateSymptomsFirstAppeared: '2018-08-25',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });
    });
  });
});
