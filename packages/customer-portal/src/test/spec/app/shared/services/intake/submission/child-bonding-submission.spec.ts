import {
  AbsenceCaseNotifiedBy,
  AbsenceService,
  IntakeSource,
  NewAbsenceRequest,
  SdkConfigProps
} from 'fineos-js-api-client';

import { ChildBondingIntake } from '../../../../../../../app/shared/intakes';
import { IntakeSubmission } from '../../../../../../../app/shared/services/intake/submission';
import {
  EventOptionId,
  Role,
  TimeOff
} from '../../../../../../../app/shared/types';
import { AuthenticationService } from '../../../../../../../app/shared/services/authentication';

import {
  timeOffLeavePeriods,
  reducedScheduleLeavePeriods,
  episodicLeavePeriods,
  timeOffRequests,
  reducedScheduleRequests,
  episodicRequests
} from './submission-test-utils';

describe('Intake Submission: Child Bonding', () => {
  let action: ChildBondingIntake;

  beforeEach(() => {
    action = new ChildBondingIntake();
  });

  test('should support EventOptionId.CHILD_BONDING', () => {
    const childBondingSubmission = new IntakeSubmission(
      [EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_NEWBORN],
      new Map(),
      {
        outOfWork: true
      }
    );
    expect(action.supports(childBondingSubmission)).toBe(true);

    const pregnancySubmission = new IntakeSubmission(
      [EventOptionId.PREGNANCY],
      new Map(),
      { outOfWork: true }
    );
    expect(action.supports(pregnancySubmission)).toBe(false);
  });

  describe('process', () => {
    let registerAbsenceMock: jest.Mock;
    let originalRegisterAbsence: any;
    let originalAuthorizationRole: any;

    beforeEach(() => {
      const absenceService = AbsenceService.getInstance();

      registerAbsenceMock = jest.fn();
      originalRegisterAbsence = absenceService.registerAbsence;
      absenceService.registerAbsence = registerAbsenceMock;
      AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];
      originalAuthorizationRole = SdkConfigProps.authorizationRole;

      SdkConfigProps.authorizationRole = jest
        .fn()
        .mockImplementation(val => val);
    });

    afterEach(() => {
      const absenceService = AbsenceService.getInstance();
      absenceService.registerAbsence = originalRegisterAbsence;
      AuthenticationService.getInstance().displayRoles = [];
      SdkConfigProps.authorizationRole = originalAuthorizationRole;
    });

    test('should create proper absence for newborn flow', async () => {
      await action.process(
        new IntakeSubmission(
          [EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_NEWBORN],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Child Bonding',
            notificationCaseId: '',
            notificationReason:
              'Bonding with a new child (adoption/ foster care/ newborn)',
            reasonQualifier1: 'Newborn'
          })
        )
      );
    });

    test('should create proper absence for foster flow', async () => {
      await action.process(
        new IntakeSubmission(
          [EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_FOSTER],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Child Bonding',
            notificationCaseId: '',
            notificationReason:
              'Bonding with a new child (adoption/ foster care/ newborn)',
            reasonQualifier1: 'Foster Care'
          })
        )
      );
    });

    test('should create proper absence for adopted flow', async () => {
      await action.process(
        new IntakeSubmission(
          [EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_ADOPTED],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Child Bonding',
            notificationCaseId: '',
            notificationReason:
              'Bonding with a new child (adoption/ foster care/ newborn)',
            reasonQualifier1: 'Adoption'
          })
        )
      );
    });

    test('should create proper absence with time-off details', async () => {
      const timeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      } as TimeOff;

      await action.process(
        new IntakeSubmission(
          [EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_ADOPTED],
          new Map(),
          timeOff
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            timeOffRequests,
            episodicRequests,
            reducedScheduleRequests
          })
        )
      );
    });
  });
});
