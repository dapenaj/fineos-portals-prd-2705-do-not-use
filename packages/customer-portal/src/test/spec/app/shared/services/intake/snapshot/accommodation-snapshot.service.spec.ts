import { DocumentService } from 'fineos-js-api-client';
import { stripIndents } from 'common-tags';

import {
  AccommodationSnapshotService,
  IntakeAccommodationSubmission,
  IntakeSubmissionResult
} from '../../../../../../../app/shared/services';
import {
  givenAbsenceUser,
  mockField,
  givenPromise,
  promiseTest
} from '../../../../../../common/utils';
import { EventOptionId } from '../../../../../../../app/shared/types';
import {
  PersonalDetailsFormValue,
  WorkDetailsFormValue,
  AccommodationValue
} from '../../../../../../../app/modules/intake';
import { customerFixture } from '../../../../../../common/fixtures';
import { b64EncodeUnicode } from '../../../../../../../app/shared/utils';

describe('AccommodationSnapshotService', () => {
  let snapshotService: AccommodationSnapshotService;
  let documentService: DocumentService;

  beforeEach(() => {
    givenAbsenceUser();

    snapshotService = AccommodationSnapshotService.getInstance();
    documentService = mockField(
      snapshotService,
      'documentService',
      new DocumentService()
    );
    documentService.uploadCaseDocument = givenPromise({});
  });

  test('Submission for Accommodation', done => {
    const model = givenSuccessfulSubmission();

    const result: IntakeSubmissionResult = {
      createdNotificationId: 'NOT-123',
      createdAccommodation: { id: 'ACC-123' },
      createdAbsence: {},
      createdClaim: {},
      postCaseErrors: [],
      isClaimRequired: false,
      isAbsenceRequired: false
    };

    promiseTest(
      done,
      snapshotService.submitSnapshot(model, result).then(() => {
        const expected = {
          documentType: 'Customer Portal Snapshot Document',
          fileName: 'Portal Intake Snapshot NOT-123',
          description: 'Portal Intake Snapshot document',
          fileContentsText: b64EncodeUnicode(getExpectedText()),
          fileExtension: 'txt'
        };

        expect(documentService.uploadCaseDocument).toBeCalledWith(
          'NOT-123',
          expected
        );
      })
    );
  });

  const givenSuccessfulSubmission = (): IntakeAccommodationSubmission => {
    const personalDetails: PersonalDetailsFormValue = {
      firstName: customerFixture.firstName,
      lastName: customerFixture.lastName,
      dateOfBirth: customerFixture.dateOfBirth,
      phoneNumber: '1-353-234567',
      email: 'bob@dobalina.com',
      premiseNo: customerFixture.customerAddress.address.premiseNo,
      addressLine1: customerFixture.customerAddress.address.addressLine1,
      addressLine2: customerFixture.customerAddress.address.addressLine2,
      addressLine3: customerFixture.customerAddress.address.addressLine3,
      city: customerFixture.customerAddress.address.addressLine4,
      state: customerFixture.customerAddress.address.addressLine6,
      postCode: customerFixture.customerAddress.address.postCode,
      country: 'USA'
    };

    const workDetails: WorkDetailsFormValue = {
      employer: 'FinCorp Ltd',
      jobTitle: 'Line Supervisor',
      dateJobBegan: '2014-02-02'
    };

    const selectedOptions = [EventOptionId.ACCOMMODATION];

    const accommodation: AccommodationValue = {
      pregnancyRelated: 'Yes',
      limitations: [
        { limitationType: 'Dietary Needs' },
        { limitationType: 'Headache' }
      ],
      workPlaceAccommodations: [
        {
          accommodationCategory:
            'Physical workplace modifications or workstation relocation',
          accommodationType: 'Widen doorway',
          accommodationDescription: ''
        },
        {
          accommodationCategory: 'Other Accommodation',
          accommodationType: 'Other',
          accommodationDescription: 'Broken Leg'
        }
      ],
      additionalNotes: 'These are some additional notes'
    };

    return new IntakeAccommodationSubmission(
      selectedOptions,
      accommodation,
      workDetails,
      personalDetails
    );
  };

  // tslint:disable:max-line-length
  // only disabling tslint rule for getExpectedText()
  const getExpectedText = (errorText = '') => {
    const appliedErrorText = errorText
      ? `\n- Error submitting post case request: ${errorText}`
      : '';
    return stripIndents`-------------------------------------
    General : Main
    -------------------------------------
    - Notification Case Id: NOT-123
    - No request to create Absence
    - No request to create Claim
    - Created Accommodation : ACC-123${appliedErrorText}
    -------------------------------------
    Your Request : Personal Details
    -------------------------------------
    - First name: Jane
    - Last name: Doe
    - Date of birth: 1988-01-01
    - Phone number: 1-353-234567
    - Email: bob@dobalina.com
    - Number: 123
    - Address Line 1: Main street
    - Address Line 2: Where it's at
    - Address Line 3: Two turn tables
    - City: And a microphone
    - State: CA
    - Zip Code: AB123456
    -------------------------------------
    Your Request : Work Details
    -------------------------------------
    - Employer: FinCorp Ltd
    - Job title: Line Supervisor
    - Date of hire: 2014-02-02
    -------------------------------------
    Your Request : Initial Request
    -------------------------------------
    - How can we help you?: I require an accommodation to remain in work
    -------------------------------------
    Details : Request Details
    -------------------------------------
    - Is your request pregnancy related?: Yes
    - Additional notes: These are some additional notes
    -------------------------------------
    Details : Limitation: 1
    -------------------------------------
    - Dietary Needs
    -------------------------------------
    Details : Limitation: 2
    -------------------------------------
    - Headache
    -------------------------------------
    Details : Work place accommodations: 1
    -------------------------------------
    - Category of accommodation: Physical workplace modifications or workstation relocation
    - What type of accommodation are you looking for?: Widen doorway
    - Add a description for this accommodation:
    -------------------------------------
    Details : Work place accommodations: 2
    -------------------------------------
    - Category of accommodation: Other Accommodation
    - What type of accommodation are you looking for?: Other
    - Add a description for this accommodation: Broken Leg`;
  };
});
