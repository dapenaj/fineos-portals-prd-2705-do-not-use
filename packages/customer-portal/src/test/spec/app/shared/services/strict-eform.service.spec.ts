import { CaseService } from 'fineos-js-api-client';

import { StrictEFormService } from '../../../../../app/shared/services';
import { mockField, givenPromise, promiseTest } from '../../../../common/utils';

import {
  confirmReturnEFormFixture,
  eFormSummaryFixture,
  eFormFixture
} from './../../../../common/fixtures/eform.fixture';

describe('Strict EForm Service', () => {
  const service = StrictEFormService.getInstance();
  let mockCaseService: CaseService;
  mockCaseService = mockField(service, 'caseService', new CaseService());

  it('getConfirmReturnEForm', done => {
    mockCaseService.getEForms = givenPromise({}).mockReturnValue([
      eFormSummaryFixture
    ]);
    mockCaseService.readEForm = givenPromise({}).mockReturnValue(eFormFixture);

    promiseTest(
      done,
      service.getConfirmReturnEForm('NTN-12').then(eForm => {
        if (eForm) {
          expect(eForm.id).toBe(1);
          expect(mockCaseService.getEForms).toBeCalled();
          expect(mockCaseService.readEForm).toBeCalled();
        }
      })
    );
  });

  it('updateConfirmReturnEForm', done => {
    mockCaseService.updateEForm = givenPromise({}).mockReturnValue([
      eFormSummaryFixture
    ]);

    promiseTest(
      done,
      service
        .updateConfirmReturnEForm('NTN-12', confirmReturnEFormFixture)
        .then(eForm => {
          expect(mockCaseService.updateEForm).toBeCalled();
        })
    );
  });
});
