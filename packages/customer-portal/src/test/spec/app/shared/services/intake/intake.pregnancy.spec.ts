import { EventDetailOptionService } from '../../../../../../app/shared/services';
import {
  EventOptionId,
  RequestDetailFieldId,
  RequestDetailFormId,
  RequestDetailAnswerType
} from '../../../../../../app/shared/types';
import {
  givenAbsenceUser,
  givenCustomerUser
} from '../../../../../common/utils';
import {
  thenPathHasNoChildren,
  thenPathHasChildren,
  thenPathHasDefaultForms,
  thenFormHasFields,
  thenFormHasSubForm,
  thenFormHasNoSubForm
} from '../../../../../common/utils';

describe('Intake: Pregnancy', () => {
  describe('Intake: Pregnancy: EventDetails', () => {
    describe('Check option tree', () => {
      describe('Check option tree - absence users', () => {
        beforeEach(() => {
          givenAbsenceUser();
        });

        it('Pregnancy should be available', () => {
          expect(EventDetailOptionService.getInstance().start()).toContain(
            EventOptionId.PREGNANCY
          );
        });

        it('Pregnancy should pre/post natal options and termination', () => {
          thenPathHasChildren(
            [EventOptionId.PREGNANCY],
            [
              EventOptionId.PREGNANCY_POSTNATAL,
              EventOptionId.PREGNANCY_PRENATAL,
              EventOptionId.PREGNANCY_TERMINATION
            ]
          );
        });

        it('If the baby has been born then ask if it is a birth or port partum disability', () => {
          thenPathHasChildren(
            [EventOptionId.PREGNANCY, EventOptionId.PREGNANCY_POSTNATAL],
            [
              EventOptionId.PREGNANCY_BIRTH_DISABILITY,
              EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
            ]
          );
        });

        it('If the baby has not been born then ask if the request is for prenatal care, disability or a birth disability prefile', () => {
          thenPathHasChildren(
            [EventOptionId.PREGNANCY, EventOptionId.PREGNANCY_PRENATAL],
            [
              EventOptionId.PREGNANCY_PRENATAL_CARE,
              EventOptionId.PREGNANCY_PRENATAL_DISABILITY,
              EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY
            ]
          );
        });

        it('If the pregnancy has terminated there should be no leaves below this', () => {
          thenPathHasNoChildren([
            EventOptionId.PREGNANCY,
            EventOptionId.PREGNANCY_TERMINATION
          ]);
        });
      });
    });

    describe('Check option tree - claims users', () => {
      beforeEach(() => {
        givenCustomerUser();
      });

      it('Pregnancy should be available', () => {
        expect(EventDetailOptionService.getInstance().start()).toContain(
          EventOptionId.PREGNANCY
        );
      });

      it('Pregnancy should post natal options and termination', () => {
        thenPathHasChildren(
          [EventOptionId.PREGNANCY],
          [
            EventOptionId.PREGNANCY_POSTNATAL,
            EventOptionId.PREGNANCY_PRENATAL,
            EventOptionId.PREGNANCY_TERMINATION
          ]
        );
      });

      it('If the baby has been born then ask if it is a birth or port partum disability', () => {
        thenPathHasChildren(
          [EventOptionId.PREGNANCY, EventOptionId.PREGNANCY_POSTNATAL],
          [
            EventOptionId.PREGNANCY_BIRTH_DISABILITY,
            EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
          ]
        );
      });

      it('If the pregnancy has terminated there should be no leaves below this', () => {
        thenPathHasNoChildren([
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_TERMINATION
        ]);
      });
    });
  });

  describe('Intake: Pregnancy: RequestDetails', () => {
    describe('Derive forms - absence users', () => {
      beforeEach(() => {
        givenAbsenceUser();
      });

      describe('Derive forms - time off for prenatal care ', () => {
        it('Derive forms - time off for prenatal care for disability', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.PREGNANCY,
              EventOptionId.PREGNANCY_PRENATAL,
              EventOptionId.PREGNANCY_PRENATAL_DISABILITY
            ],
            [RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY]
          );

          thenFormHasFields(RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY, [
            {
              id: RequestDetailFieldId.DESCRIBE_SICKNESS,
              answerType: RequestDetailAnswerType.LONG_TEXT
            },
            {
              id: RequestDetailFieldId.FIRST_SYMPTOM_DATE,
              answerType: RequestDetailAnswerType.DATE
            },
            {
              id: RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
              answerType: RequestDetailAnswerType.OPTIONS
            },
            {
              id: RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
              answerType: RequestDetailAnswerType.DATE
            },
            {
              id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
              answerType: RequestDetailAnswerType.LONG_TEXT
            }
          ]);

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY,
            RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY,
            RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
            RequestDetailFieldId.YES,
            RequestDetailFormId.PREGNANCY_MEDICAL_TREATMENT_FORM
          );

          thenFormHasFields(
            RequestDetailFormId.PREGNANCY_MEDICAL_TREATMENT_FORM,
            [
              {
                id: RequestDetailFieldId.DATE_FIRST_TREATED,
                answerType: RequestDetailAnswerType.DATE
              },
              {
                id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
                answerType: RequestDetailAnswerType.OPTIONS
              }
            ]
          );
          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_MEDICAL_TREATMENT_FORM,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.YES,
            RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY,
            RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
            RequestDetailFieldId.NO
          );
        });

        it('Derive forms - time off for prenatal care ', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.PREGNANCY,
              EventOptionId.PREGNANCY_PRENATAL,
              EventOptionId.PREGNANCY_PRENATAL_CARE
            ],
            [RequestDetailFormId.PREGNANCY_PRENATAL_CARE]
          );

          thenFormHasFields(RequestDetailFormId.PREGNANCY_PRENATAL_CARE, [
            {
              id: RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
              answerType: RequestDetailAnswerType.DATE
            }
          ]);
        });
        it('Derive forms - time off for prenatal care - birth disability ', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.PREGNANCY,
              EventOptionId.PREGNANCY_PRENATAL,
              EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY
            ],
            [RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY]
          );

          thenFormHasFields(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            [
              {
                id: RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
                answerType: RequestDetailAnswerType.OPTIONS
              },
              {
                id: RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
                answerType: RequestDetailAnswerType.DATE
              },
              {
                id: RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
                answerType: RequestDetailAnswerType.OPTIONS
              },
              {
                id: RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
                answerType: RequestDetailAnswerType.OPTIONS
              }
            ]
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.YES,
            RequestDetailFormId.EXPECTED_OVERNIGHT_STAY_MEDICAL_TREATMENT
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN
          );
          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
          );
          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
            RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
            RequestDetailFieldId.YES,
            RequestDetailFormId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS
          );

          thenFormHasFields(
            RequestDetailFormId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS,
            [
              {
                id:
                  RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS,
                answerType: RequestDetailAnswerType.LONG_TEXT
              }
            ]
          );
        });
      });

      describe('Derive forms - time off for postnatal care ', () => {
        it('Derive forms - time off for postnatal care - birth disability ', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.PREGNANCY,
              EventOptionId.PREGNANCY_POSTNATAL,
              EventOptionId.PREGNANCY_BIRTH_DISABILITY
            ],
            [RequestDetailFormId.PREGNANCY_POSTPARTUM]
          );

          thenFormHasFields(RequestDetailFormId.PREGNANCY_POSTPARTUM, [
            {
              id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
              answerType: RequestDetailAnswerType.OPTIONS
            },
            {
              id: RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
              answerType: RequestDetailAnswerType.DATE
            },
            {
              id: RequestDetailFieldId.NEWBORN_COUNT,
              answerType: RequestDetailAnswerType.NUMBER_SELECT
            },
            {
              id: RequestDetailFieldId.PREGNANCY_DELIVERY,
              answerType: RequestDetailAnswerType.OPTIONS
            },
            {
              id: RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
              answerType: RequestDetailAnswerType.OPTIONS
            },
            {
              id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
              answerType: RequestDetailAnswerType.LONG_TEXT
            }
          ]);

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.YES,
            RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.PREGNANCY_DELIVERY,
            RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_POSTPARTUM,
            RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
            RequestDetailFieldId.YES,
            RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS
          );

          thenFormHasFields(
            RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS,
            [
              {
                id:
                  RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
                answerType: RequestDetailAnswerType.LONG_TEXT
              }
            ]
          );
        });
        it('Derive forms - time off for postnatal care - postpartum disability ', () => {
          thenPathHasDefaultForms(
            [
              EventOptionId.PREGNANCY,
              EventOptionId.PREGNANCY_POSTNATAL,
              EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
            ],
            [RequestDetailFormId.PREGNANCY_POSTPARTUM]
          );

          // Same fields as above form
        });
      });
      describe('Derive forms - time off for termination ', () => {
        it('Derive forms - time off for termination ', () => {
          thenPathHasDefaultForms(
            [EventOptionId.PREGNANCY, EventOptionId.PREGNANCY_TERMINATION],
            [RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION]
          );

          thenFormHasFields(
            RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION,
            [
              {
                id: RequestDetailFieldId.FIRST_SYMPTOM_DATE,
                answerType: RequestDetailAnswerType.DATE
              },
              {
                id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
                answerType: RequestDetailAnswerType.OPTIONS
              },
              {
                id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
                answerType: RequestDetailAnswerType.LONG_TEXT
              }
            ]
          );

          thenFormHasNoSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.NO
          );

          thenFormHasSubForm(
            RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION,
            RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            RequestDetailFieldId.YES,
            RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
          );
        });
      });
    });
  });
});
