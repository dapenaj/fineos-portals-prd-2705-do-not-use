import { AccommodationIntake } from '../../../../../../../app/shared/intakes';
import {
  IntakeAccommodationSubmission,
  IntakeAccommodationSubmissionService
} from '../../../../../../../app/shared/services';
import { EventOptionId } from '../../../../../../../app/shared/types';
import { givenAbsenceUser } from '../../../../../../common/utils';
import { AccommodationValue } from '../../../../../../../app/modules/intake';
import {
  startAccommodationSpy,
  completeNotificationSpy,
  updateWorkDetailsSpy,
  submitSnapshotSpy
} from '../../../../../../common/spys';

const accommodation: AccommodationValue = {
  pregnancyRelated: 'Yes',
  limitations: [
    { limitationType: 'Dietary Needs' },
    { limitationType: 'Headache' }
  ],
  workPlaceAccommodations: [
    {
      accommodationCategory:
        'Physical workplace modifications or workstation relocation',
      accommodationType: 'Widen doorway',
      accommodationDescription: ''
    },
    {
      accommodationCategory: 'Other Accommodation',
      accommodationType: 'Other',
      accommodationDescription: 'Broken Leg'
    }
  ],
  additionalNotes: 'These are some additional notes'
};

describe('Intake Accommodation Submission', () => {
  let intake: AccommodationIntake;
  let service: IntakeAccommodationSubmissionService;
  let accommodationSpy: jest.SpyInstance;
  let customerOccupationSpy: jest.SpyInstance;
  let notificationSpy: jest.SpyInstance;
  let snapshotSpy: jest.SpyInstance;

  beforeEach(() => {
    jest.clearAllMocks();

    intake = new AccommodationIntake();
    service = new IntakeAccommodationSubmissionService();

    accommodationSpy = startAccommodationSpy;
    notificationSpy = completeNotificationSpy('success');
    customerOccupationSpy = updateWorkDetailsSpy;
    snapshotSpy = submitSnapshotSpy('success');
  });

  describe('Initial Request Selection', () => {
    beforeEach(() => {
      givenAbsenceUser();
    });

    test('should work for ACCOMMODATION', async () => {
      const submission = new IntakeAccommodationSubmission(
        [EventOptionId.ACCOMMODATION],
        accommodation
      );

      try {
        const result = await service.process(submission);

        expect(result).toEqual(
          jasmine.objectContaining({
            createdAbsence: {},
            createdAccommodation: { id: 'ACC-324' },
            createdClaim: {},
            createdNotificationId: 'NTN-368',
            isAbsenceRequired: false,
            isClaimRequired: false,
            postCaseErrors: []
          })
        );
      } catch (error) {
        console.log('Process Error: ', JSON.stringify(error));
      }
    });

    test('should not work for others', async () => {
      const submission = new IntakeAccommodationSubmission(
        [EventOptionId.ACCIDENT],
        accommodation
      );

      try {
        await service.process(submission);
      } catch (error) {
        expect(error).toEqual(
          jasmine.objectContaining({
            createdAbsence: {},
            createdClaim: {},
            generalError: 'No method is implemented to handle this request',
            isAbsenceRequired: false,
            isClaimRequired: false,
            postCaseErrors: []
          })
        );
      }
    });
  });

  describe('Complete Intake', () => {
    const submission = new IntakeAccommodationSubmission(
      [EventOptionId.ACCOMMODATION],
      accommodation
    );

    beforeEach(() => {
      givenAbsenceUser();
    });

    test('complete successful intake', async () => {
      const result = {
        createdAbsence: {},
        createdAccommodation: { id: 'ACC-324' },
        createdClaim: {},
        createdNotificationId: 'NTN-368',
        isAbsenceRequired: false,
        isClaimRequired: false,
        postCaseErrors: []
      };

      const complete = await service.completeIntake(submission, result);

      expect(complete.generalError).toBeUndefined();
      expect(complete.postCaseErrors).toStrictEqual([]);

      expect(snapshotSpy).toHaveBeenCalled();
      expect(notificationSpy).toHaveBeenCalled();
    });

    test('complete unsuccessful intake', async () => {
      const result = {
        createdAbsence: {},
        createdAccommodation: { id: 'ACC-324' },
        createdClaim: {},
        createdNotificationId: 'NTN-368',
        isAbsenceRequired: false,
        isClaimRequired: false,
        postCaseErrors: ['oh no!']
      };

      const complete = await service.completeIntake(submission, result);
      expect(complete.generalError).toBeUndefined();
      expect(complete.postCaseErrors).toStrictEqual(['oh no!']);

      expect(snapshotSpy).toHaveBeenCalled();
      expect(notificationSpy).not.toHaveBeenCalled();
    });

    test('complete unsuccessful intake - no case id', async () => {
      const result = {
        createdAbsence: {},
        createdAccommodation: {},
        createdClaim: {},
        isAbsenceRequired: false,
        isClaimRequired: false,
        postCaseErrors: [],
        generalError: 'no case at all'
      };

      const complete = await service.completeIntake(submission, result);
      expect(complete.generalError).toBe('no case at all');
      expect(complete.postCaseErrors).toStrictEqual([]);

      expect(notificationSpy).not.toHaveBeenCalled();
    });

    test('complete successful intake - snapshot fail', async () => {
      snapshotSpy = submitSnapshotSpy('failure');

      const result = {
        createdAbsence: {},
        createdAccommodation: { id: 'ACC-324' },
        createdClaim: {},
        createdNotificationId: 'NTN-368',
        isAbsenceRequired: false,
        isClaimRequired: false,
        postCaseErrors: ['cannot create snapshot!']
      };

      const complete = await service.completeIntake(submission, result);
      expect(complete.generalError).toBeUndefined();
      expect(complete.postCaseErrors[0]).toEqual('cannot create snapshot!');

      expect(snapshotSpy).toHaveBeenCalled();
    });

    test('complete successful intake - complete fail', async () => {
      notificationSpy = completeNotificationSpy('success');

      const result = {
        createdAbsence: {},
        createdAccommodation: { id: 'ACC-324' },
        createdClaim: {},
        createdNotificationId: 'NTN-368',
        isAbsenceRequired: false,
        isClaimRequired: false,
        postCaseErrors: ['could not complete notification']
      };

      const complete = await service.completeIntake(submission, result);
      expect(complete.generalError).toBeUndefined();
      expect(complete.postCaseErrors).toStrictEqual([
        'could not complete notification'
      ]);

      expect(snapshotSpy).toHaveBeenCalled();
      expect(notificationSpy).not.toHaveBeenCalled();
    });
  });
});
