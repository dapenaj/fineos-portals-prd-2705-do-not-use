import {
  AbsenceService,
  SdkConfigProps,
  NewAbsenceRequest,
  IntakeSource,
  AbsenceCaseNotifiedBy
} from 'fineos-js-api-client';

import { CareForAFamilyMemberIntake } from '../../../../../../../app/shared/intakes';
import {
  IntakeSubmission,
  AuthenticationService
} from '../../../../../../../app/shared/services';
import {
  EventOptionId,
  Role,
  TimeOff
} from '../../../../../../../app/shared/types';

import {
  timeOffLeavePeriods,
  reducedScheduleLeavePeriods,
  episodicLeavePeriods,
  timeOffRequests,
  episodicRequests,
  reducedScheduleRequests
} from './submission-test-utils';

describe('Intake Submission: Care for a family Member', () => {
  let action: CareForAFamilyMemberIntake;

  beforeEach(() => {
    action = new CareForAFamilyMemberIntake();
  });

  test('should support EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS and EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER', () => {
    const sicknessSubmission = new IntakeSubmission(
      [EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS],
      new Map(),
      { outOfWork: true }
    );

    expect(action.supports(sicknessSubmission)).toBe(true);

    const otherSubmission = new IntakeSubmission(
      [
        EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
        EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREVENTIVE_CARE
      ],
      new Map(),
      { outOfWork: true }
    );

    expect(action.supports(otherSubmission)).toBe(true);
  });

  describe('process', () => {
    let registerAbsenceMock: jest.Mock;
    let originalRegisterAbsence: any;
    let originalAuthorizationRole: any;

    beforeEach(() => {
      const absenceService = AbsenceService.getInstance();

      registerAbsenceMock = jest.fn();
      originalRegisterAbsence = absenceService.registerAbsence;
      absenceService.registerAbsence = registerAbsenceMock;
      AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];
      originalAuthorizationRole = SdkConfigProps.authorizationRole;

      SdkConfigProps.authorizationRole = jest
        .fn()
        .mockImplementation(val => val);
    });

    afterEach(() => {
      const absenceService = AbsenceService.getInstance();
      absenceService.registerAbsence = originalRegisterAbsence;
      AuthenticationService.getInstance().displayRoles = [];
      SdkConfigProps.authorizationRole = originalAuthorizationRole;
    });

    test('should create absence serios sickness flow', async () => {
      await action.process(
        new IntakeSubmission(
          [EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Care for a Family Member',
            notificationCaseId: '',
            notificationReason: 'Caring for a family member',
            reasonQualifier1: 'Serious Health Condition'
          })
        )
      );
    });

    test('should create absence preventive care flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREVENTIVE_CARE
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Care for a Family Member',
            notificationCaseId: '',
            notificationReason: 'Caring for a family member',
            reasonQualifier1: 'Sickness - Non-Serious Health Condition'
          })
        )
      );
    });

    test('should create absence pregnancy flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREGNANCY
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Care for a Family Member',
            notificationCaseId: '',
            notificationReason: 'Caring for a family member',
            reasonQualifier1: 'Pregnancy Related'
          })
        )
      );
    });

    test('should create absence domestic violence - medical flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_VIOLENCE,
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_YES
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Care for a Family Member',
            notificationCaseId: '',
            notificationReason: 'Caring for a family member',
            reasonQualifier1: 'Right to Leave',
            reasonQualifier2: 'Medical Related'
          })
        )
      );
    });

    test('should create absence domestic violence - non medical flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_VIOLENCE,
            EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_NO
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            intakeSource: IntakeSource.SelfService,
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            reason: 'Care for a Family Member',
            notificationCaseId: '',
            notificationReason: 'Caring for a family member',
            reasonQualifier1: 'Right to Leave',
            reasonQualifier2: 'Non Medical'
          })
        )
      );
    });

    test('should create proper absence with time-off details', async () => {
      const timeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      } as TimeOff;

      await action.process(
        new IntakeSubmission(
          [EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_ADOPTED],
          new Map(),
          timeOff
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            timeOffRequests,
            episodicRequests,
            reducedScheduleRequests
          })
        )
      );
    });
  });
});
