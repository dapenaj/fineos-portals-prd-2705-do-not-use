import { EventDetailOptionService } from '../../../../../../app/shared/services';
import {
  EventOptionId,
  RequestDetailFieldId,
  RequestDetailFormId,
  RequestDetailAnswerType
} from '../../../../../../app/shared/types';
import { givenAbsenceUser } from '../../../../../common/utils';
import {
  thenPathHasChildren,
  thenPathHasDefaultForms,
  thenFormHasFields,
  thenFormHasSubForm
} from '../../../../../common/utils';

describe('Intake: Other', () => {
  beforeEach(() => {
    givenAbsenceUser();
  });

  describe('Intake: Other: EventDetails', () => {
    describe('Check option tree', () => {
      it('Other should be available', () => {
        expect(EventDetailOptionService.getInstance().start()).toContain(
          EventOptionId.OTHER
        );
      });

      it('Other should have sub options', () => {
        thenPathHasChildren(
          [EventOptionId.OTHER],
          [EventOptionId.OTHER_EMPLOYEE, EventOptionId.OTHER_FAMILY_MEMBER]
        );
      });

      it('Other - Employee should have sub options', () => {
        thenPathHasChildren(
          [EventOptionId.OTHER, EventOptionId.OTHER_EMPLOYEE],
          [
            EventOptionId.OTHER_EMPLOYEE_JURY_DUTY,
            EventOptionId.OTHER_EMPLOYEE_VOTING,
            EventOptionId.OTHER_EMPLOYEE_WITNESS,
            EventOptionId.OTHER_EMPLOYEE_EDUCATION,
            EventOptionId.OTHER_EMPLOYEE_MILITARY,
            EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
            EventOptionId.OTHER_EMPLOYEE_UNION,
            EventOptionId.OTHER_EMPLOYEE_EMERGENCY
          ]
        );
        thenPathHasChildren(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_VIOLENCE
          ],
          [
            EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES,
            EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO
          ]
        );
      });

      it('Other - Family Member should have sub options', () => {
        thenPathHasChildren(
          [EventOptionId.OTHER, EventOptionId.OTHER_FAMILY_MEMBER],
          [
            EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_DONATION,
            EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE,
            EventOptionId.OTHER_FAMILY_MEMBER_PASSED_AWAY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_EDUCATION,
            EventOptionId.OTHER_FAMILY_MEMBER_EMERGENCY
          ]
        );
        thenPathHasChildren(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY
          ],
          [
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_COUNSELING,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_LEGAL,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_EVENT,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL
          ]
        );
      });
    });
  });

  describe('Intake: Other: RequestDetails', () => {
    describe('Other - Employee forms', () => {
      it('Majority of Other - Employee options have no default forms', () => {
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_EDUCATION
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_MILITARY
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
            EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_UNION
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_EMERGENCY
          ],
          []
        );
      });

      it('If Employee needs medical treatment, then there is a default form', () => {
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
            EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES
          ],
          [RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM]
        );

        thenFormHasFields(RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM, [
          {
            id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            answerType: RequestDetailAnswerType.OPTIONS
          }
        ]);

        thenFormHasSubForm(
          RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM,
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.NO
        );

        thenFormHasSubForm(
          RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM,
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES,
          RequestDetailFormId.OTHER_OVERNIGHT_STAY_MEDICAL_TREATMENT
        );

        thenFormHasFields(
          RequestDetailFormId.OTHER_OVERNIGHT_STAY_MEDICAL_TREATMENT,
          [
            {
              id: RequestDetailFieldId.OTHER_OVERNIGHT_HOSPITAL_STAY_DURATION,
              answerType: RequestDetailAnswerType.DATE_RANGE
            }
          ]
        );
      });
    });

    describe('Other - Family Member forms', () => {
      it('Majority of Other - Family Member options have no default forms', () => {
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_PASSED_AWAY
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_EDUCATION
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_EMERGENCY
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_COUNSELING
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_LEGAL
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_EVENT
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE
          ],
          []
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL
          ],
          []
        );
      });

      it('If Family Member needs medical treatment, then there is a default form', () => {
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_DONATION
          ],
          [RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM]
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE
          ],
          [RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM]
        );
        thenPathHasDefaultForms(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER
          ],
          [RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM]
        );

        thenFormHasFields(
          RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM,
          [
            {
              id: RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITAL_STAY,
              answerType: RequestDetailAnswerType.OPTIONS
            }
          ]
        );

        thenFormHasSubForm(
          RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM,
          RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.NO
        );

        thenFormHasSubForm(
          RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM,
          RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES,
          RequestDetailFormId.OTHER_FAMILY_MEMBER_HOSPITALIZATION_FORM
        );

        thenFormHasFields(
          RequestDetailFormId.OTHER_FAMILY_MEMBER_HOSPITALIZATION_FORM,
          [
            {
              id:
                RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITALIZATION_DATE,
              answerType: RequestDetailAnswerType.DATE_RANGE
            }
          ]
        );
      });
    });
  });
});
