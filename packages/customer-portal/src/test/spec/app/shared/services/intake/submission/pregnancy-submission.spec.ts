import {
  AbsenceService,
  ClaimService,
  PregnancyService,
  AbsenceCaseNotifiedBy,
  IntakeSource,
  AdditionalConditionsService
} from 'fineos-js-api-client';
import moment from 'moment';

import {
  givenAbsenceUser,
  givenPromise,
  mockField,
  givenCustomerUser
} from '../../../../../../common/utils';
import {
  CLAIM_CASE_TYPE,
  DISABILITY_REQUEST_SOURCE
} from '../../../../../../../app/shared/constants';
import { IntakeSubmission } from '../../../../../../../app/shared/services';
import { PregnancyIntake } from '../../../../../../../app/shared/intakes';
import {
  EventOptionId,
  RequestDetailFieldId,
  TimeOff
} from '../../../../../../../app/shared/types';
import { API_DATE_FORMAT } from '../../../../../../../app/shared/utils';

import {
  testProcessShouldWork,
  timeOffLeavePeriods,
  reducedScheduleLeavePeriods,
  episodicLeavePeriods,
  timeOffRequests,
  reducedScheduleRequests,
  episodicRequests,
  episodicUndefinedTimeRequests,
  timeOffUndefinedTimeRequests
} from './submission-test-utils';

describe('Pregnancy Intake Submission', () => {
  const NOTIFICATION_ID = 'CASE123';
  const ABSENCE_ID = 'ABSENCE_ID';
  const CLAIM_ID = 'CLAIM_ID';

  let intake: PregnancyIntake;
  let mockAbsenceService: AbsenceService;
  let mockClaimService: ClaimService;
  let mockPregnancyService: PregnancyService;
  let mockAdditionalConditionsService: AdditionalConditionsService;

  beforeEach(() => {
    intake = new PregnancyIntake();

    mockAbsenceService = mockField(
      intake,
      'absenceService',
      new AbsenceService()
    );

    mockClaimService = mockField(intake, 'claimService', new ClaimService());

    mockPregnancyService = mockField(
      intake,
      'pregnancyService',
      new PregnancyService()
    );

    mockAdditionalConditionsService = mockField(
      intake,
      'additionalConditionsService',
      new AdditionalConditionsService()
    );
  });

  const givenAllPostCaseRequestsWork = () => {
    // assume all underlying service requests work
    mockAbsenceService.addHospitalisationDetails = givenPromise({});
    mockClaimService.updateMedicalDetails = givenPromise({});
    mockClaimService.updateDiabilityDetails = givenPromise({});
    mockClaimService.addHospitalisationDetails = givenPromise({});
    mockClaimService.updatePregnancyDetails = givenPromise({});
    mockClaimService.indicateMultiplePeriodsInAbsence = givenPromise({});
    mockPregnancyService.addPostnatalDetails = givenPromise({});
    mockPregnancyService.addPrenatalDetails = givenPromise({});
    mockAdditionalConditionsService.addAdditionalConditions = givenPromise({});
  };

  describe('Absence User', () => {
    beforeEach(() => {
      givenAbsenceUser();
    });

    describe('Birth Disability', () => {
      it('Birth Disability with claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_BIRTH_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        // my babies' birthday was
        fieldValues.set(
          RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
          '2018-08-26'
        );

        // I had three babies
        fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_DELIVERY,
          RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
        );

        // there were complications
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          RequestDetailFieldId.YES
        );
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
          'Breach'
        );

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'So many babies'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            complicationDetails: 'Breach',
            deliveryDate: '2018-08-26',
            deliveryType: 'Multiple Births',
            numberOfNewborns: 3,
            pregnancyComplications: true
          });

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'So many babies'
          });

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Birth Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests,
            timeOffRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Breach'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'So many babies',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDateConfirmed: true,
              startDate: '2018-08-25',
              endDate: '2018-08-30'
            }
          );

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
            CLAIM_ID,
            {
              deliveryType: 'Multiple Births',
              pregnancyComplications: true,
              actualDeliveryDate: '2018-08-26'
            }
          );

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      it('Birth Disability with claim but no hospitalisation', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_BIRTH_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.NO
        );

        // my babies' birthday was
        fieldValues.set(
          RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
          '2018-08-26'
        );

        // I had three babies
        fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_DELIVERY,
          RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
        );

        // there were complications
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          RequestDetailFieldId.YES
        );
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
          'Breach'
        );

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'So many babies'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,

          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            complicationDetails: 'Breach',
            deliveryDate: '2018-08-26',
            deliveryType: 'Multiple Births',
            numberOfNewborns: 3,
            pregnancyComplications: true
          });

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'So many babies'
          });

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11

          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Birth Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: episodicUndefinedTimeRequests,
            reducedScheduleRequests: reducedScheduleRequests,
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Breach'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'So many babies',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
            CLAIM_ID,
            {
              deliveryType: 'Multiple Births',
              pregnancyComplications: true,
              actualDeliveryDate: '2018-08-26'
            }
          );

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      it('Birth Disability without claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_BIRTH_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        // my babies' birthday was
        fieldValues.set(
          RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
          '2018-08-26'
        );

        // I had three babies
        fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_DELIVERY,
          RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
        );

        // there were complications
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          RequestDetailFieldId.YES
        );
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
          'Breach'
        );

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'So many babies'
        );

        // I am taking a reduced time off only - so no claim
        const timeOff: TimeOff = {
          outOfWork: true,

          // no contimuous timouts, so no claim
          timeOffLeavePeriods: [],
          reducedScheduleLeavePeriods,
          episodicLeavePeriods: []
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({});

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            complicationDetails: 'Breach',
            deliveryDate: '2018-08-26',
            deliveryType: 'Multiple Births',
            numberOfNewborns: 3,
            pregnancyComplications: true
          });

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'So many babies'
          });

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Birth Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: [],
            reducedScheduleRequests: reducedScheduleRequests,
            timeOffRequests: []
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });
    });

    describe('Birth Prenatal care', () => {
      it('Prenatal care with time-off', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_CARE
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,

          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,

          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: []
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).not.toBeCalled();

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Prenatal Care',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: [],
            reducedScheduleRequests: [],
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });

      it('Prenatal care without time-off', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_CARE
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods: [],
          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).not.toBeCalled();

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Prenatal Care',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests: [],
            timeOffRequests: []
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();
          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });
    });

    describe('Birth Prenatal birth disability', () => {
      it('Birth Prenatal birth disability with hospitalisation and claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I expect to stay in hospital
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-02-03',
          '2018-02-04'
        ]);

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        // and this kind of birth
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
          RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL
        );

        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
          RequestDetailFieldId.YES
        );

        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'Pre-eclampsia'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,

          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,

          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: []
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).not.toBeCalled();

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Birth Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: [],
            reducedScheduleRequests: [],
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Pre-eclampsia'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-02-03',
            endDate: '2018-02-04'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDateConfirmed: true,
              startDate: '2018-02-03',
              endDate: '2018-02-04'
            }
          );

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
            CLAIM_ID,
            {
              expectedDeliveryDate: '2018-08-26',
              deliveryType: 'Vaginal',
              pregnancyComplications: true
            }
          );

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      it('Birth Prenatal birth disability with no claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I have this condition
        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'Pre-eclampsia'
        );

        // I have this condition
        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-02-03');

        // I have received treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-02-06');

        // I was hospitalised
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.NO
        );

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'My doctor said I need bed rest'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods: [],
          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'My doctor said I need bed rest'
          });

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();
          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Birth Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests: [],
            timeOffRequests: []
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });
    });

    describe('Prenatal disability', () => {
      it('Prenatal disability with hospitalisation and claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I have this condition
        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'Pre-eclampsia'
        );

        // I have this condition
        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-02-03');

        // I have received treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-02-06');

        // I was hospitalised
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-02-06',
          '2018-02-10'
        ]);

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'My doctor said I need bed rest'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: []
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'My doctor said I need bed rest'
          });

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Prenatal Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: [],
            reducedScheduleRequests: [],
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Pre-eclampsia',
              symptomsFirstAppeared: '2018-02-03',
              firstDoctorVisitDate: '2018-02-06'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimAdditionalInfo: 'My doctor said I need bed rest',
              claimIncurredDate: '2018-08-25',
              dateSymptomsFirstAppeared: '2018-02-03',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-02-06',
            endDate: '2018-02-10'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDate: '2018-02-10',
              endDateConfirmed: true,
              startDate: '2018-02-06'
            }
          );

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
            CLAIM_ID,
            {
              expectedDeliveryDate: '2018-08-26'
            }
          );

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      it('Prenatal disability with no claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I have this condition
        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'Pre-eclampsia'
        );

        // I have this condition
        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-02-03');

        // I have received treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-02-06');

        // I was hospitalised
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.NO
        );

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'My doctor said I need bed rest'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods: [],
          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'My doctor said I need bed rest'
          });

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();
          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Prenatal Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests,
            reducedScheduleRequests: [],
            timeOffRequests: []
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });

      it('Prenatal disability with claim but no hospitalisation', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_PRENATAL_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I have this condition
        fieldValues.set(
          RequestDetailFieldId.DESCRIBE_SICKNESS,
          'Pre-eclampsia'
        );

        // I have this condition
        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-02-03');

        // I have received treatment
        fieldValues.set(
          RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-02-06');

        // I was hospitalised
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.NO
        );

        // I expect to have this birth date
        fieldValues.set(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          '2018-08-26'
        );

        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'My doctor said I need bed rest'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods: [],
          episodicLeavePeriods: []
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            deliveryDate: '2018-08-26'
          });

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'My doctor said I need bed rest'
          });

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Prenatal Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: [],
            reducedScheduleRequests: [],
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Pre-eclampsia',
              symptomsFirstAppeared: '2018-02-03',
              firstDoctorVisitDate: '2018-02-06'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimAdditionalInfo: 'My doctor said I need bed rest',
              claimIncurredDate: '2018-08-25',
              dateSymptomsFirstAppeared: '2018-02-03',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: false
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
            CLAIM_ID,
            {
              expectedDeliveryDate: '2018-08-26'
            }
          );

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });
    });

    describe('Pregnancy Terminated', () => {
      it('Pregnancy Terminated with claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_TERMINATION
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // when did I first notice symptoms
        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-26');

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'Cancer treatment'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'Cancer treatment'
          });

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Other',
            reasonQualifier2: 'Miscarriage',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: episodicUndefinedTimeRequests,
            reducedScheduleRequests: reducedScheduleRequests,
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Miscarriage',
              symptomsFirstAppeared: '2018-08-26'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              dateSymptomsFirstAppeared: '2018-08-26',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'Cancer treatment',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDateConfirmed: true,
              startDate: '2018-08-25',
              endDate: '2018-08-30'
            }
          );

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      it('Pregnancy Terminated without claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_TERMINATION
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // when did I first notice symptoms
        fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-26');

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'Cancer treatment'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods: [],
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({});

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'Cancer treatment'
          });

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Other',
            reasonQualifier2: 'Miscarriage',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: episodicUndefinedTimeRequests,
            reducedScheduleRequests: reducedScheduleRequests,
            timeOffRequests: []
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();
          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });
    });

    describe('Postpartum Disability', () => {
      it('Postpartum Disability with claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        // my babies' birthday was
        fieldValues.set(
          RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
          '2018-08-26'
        );

        // I had three babies
        fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_DELIVERY,
          RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
        );

        // there were complications
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          RequestDetailFieldId.YES
        );
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
          'Breach'
        );

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'So many babies'
        );

        // I am taking time off
        const timeOff: TimeOff = {
          outOfWork: true,
          // these dates are backwards, but we should pick the second one as it's earliest
          timeOffLeavePeriods,
          reducedScheduleLeavePeriods,
          episodicLeavePeriods
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });

        mockClaimService.startClaim = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          claimId: CLAIM_ID
        });

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            complicationDetails: 'Breach',
            deliveryDate: '2018-08-26',
            deliveryType: 'Multiple Births',
            numberOfNewborns: 3,
            pregnancyComplications: true
          });

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'So many babies'
          });

          // row 7
          expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
            claimIncurredDate: '2018-08-25',
            notificationCaseId: NOTIFICATION_ID,
            notificationReason: 'Pregnancy, birth or related medical treatment'
          });

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Postnatal Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: episodicUndefinedTimeRequests,
            reducedScheduleRequests: reducedScheduleRequests,
            timeOffRequests: timeOffUndefinedTimeRequests
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).toBeCalledWith(
            CLAIM_ID,
            {
              condition: 'Breach'
            }
          );

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
            CLAIM_ID,
            {
              // date of first absence
              claimIncurredDate: '2018-08-25',
              disabilityDateFromCustomer: '2018-08-25',
              eventType: 'Sickness',
              claimType: 'STD',
              claimAdditionalInfo: 'So many babies',
              expectedReturnToWorkDate: '2019-02-28',
              employeeDateLastWorked: '2018-08-24',
              firstDayMissedWork: '2018-08-25',
              notificationDate: moment().format(API_DATE_FORMAT),
              workRelated: false,
              source: DISABILITY_REQUEST_SOURCE
            }
          );

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
            CLAIM_ID,
            {
              endDateConfirmed: true,
              startDate: '2018-08-25',
              endDate: '2018-08-30'
            }
          );

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
            CLAIM_ID,
            {
              deliveryType: 'Multiple Births',
              pregnancyComplications: true,
              actualDeliveryDate: '2018-08-26'
            }
          );

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
        });
      });

      it('Postpartum Disability without claim', done => {
        const selectedOptions = [
          EventOptionId.PREGNANCY,
          EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
        ];

        const fieldValues = new Map<RequestDetailFieldId, any>();

        // I stayed overnight in hospital
        fieldValues.set(
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.YES
        );

        fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
          '2018-08-25',
          '2018-08-30'
        ]);

        // my babies' birthday was
        fieldValues.set(
          RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
          '2018-08-26'
        );

        // I had three babies
        fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_DELIVERY,
          RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
        );

        // there were complications
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          RequestDetailFieldId.YES
        );
        fieldValues.set(
          RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
          'Breach'
        );

        // I have something else to say
        fieldValues.set(
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          'So many babies'
        );

        // I am taking a reduced time off only - so no claim
        const timeOff: TimeOff = {
          outOfWork: true,
          // no contimuous timouts, so no claim
          timeOffLeavePeriods: [],
          reducedScheduleLeavePeriods,
          episodicLeavePeriods: []
        };

        // given a simple birth disability model
        const model = new IntakeSubmission(
          selectedOptions,
          fieldValues,
          timeOff
        );

        // given the absence and claims creation are ok
        mockAbsenceService.registerAbsence = givenPromise({
          notificationCaseId: NOTIFICATION_ID,
          absenceId: ABSENCE_ID
        });
        mockClaimService.startClaim = givenPromise({});

        givenAllPostCaseRequestsWork();

        // when we process it, the notification id should come back
        testProcessShouldWork(intake, done, model, result => {
          expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
          expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
            result.createdAbsence
          );
          expect({}).toEqual(result.createdClaim);

          // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

          // row 3 - work details - covered in submission service

          // row 4
          expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

          // row 5
          expect(mockPregnancyService.addPostnatalDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            complicationDetails: 'Breach',
            deliveryDate: '2018-08-26',
            deliveryType: 'Multiple Births',
            numberOfNewborns: 3,
            pregnancyComplications: true
          });

          // row 6
          expect(
            mockAdditionalConditionsService.addAdditionalConditions
          ).toBeCalledWith({
            caseId: ABSENCE_ID,
            additionalDescription: 'So many babies'
          });

          // row 7
          expect(mockClaimService.startClaim).not.toBeCalled();

          // row 8, 9, 10, 11
          expect(mockAbsenceService.registerAbsence).toBeCalledWith({
            notificationReason: 'Pregnancy, birth or related medical treatment',
            reason: 'Pregnancy/Maternity',
            reasonQualifier1: 'Postnatal Disability',
            notifiedBy: AbsenceCaseNotifiedBy.Employee,
            intakeSource: IntakeSource.SelfService,
            episodicRequests: [],
            reducedScheduleRequests: reducedScheduleRequests,
            timeOffRequests: []
          });

          // row 12, 13, 14
          expect(mockClaimService.updateMedicalDetails).not.toBeCalled();

          // row 15 => 22 - disability claim - missing
          expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();

          // 23 row -  absence hospitalisation
          expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
            caseId: ABSENCE_ID,
            ovenightAtHospital: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          });

          // 24 row -  claim hospitalisation
          expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

          // row 25 - pregnancy details
          expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

          // row 26 - multiple timeoffs
          expect(
            mockClaimService.indicateMultiplePeriodsInAbsence
          ).not.toBeCalled();
        });
      });
    });
  });

  describe('Claims User', () => {
    beforeEach(() => {
      givenCustomerUser();
    });

    it('Birth Disability with claim', done => {
      const selectedOptions = [
        EventOptionId.PREGNANCY,
        EventOptionId.PREGNANCY_BIRTH_DISABILITY
      ];

      const fieldValues = new Map<RequestDetailFieldId, any>();

      // I stayed overnight in hospital
      fieldValues.set(
        RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        '2018-08-25',
        '2018-08-30'
      ]);

      // my babies' birthday was
      fieldValues.set(RequestDetailFieldId.ACTUAL_DELIVERY_DATE, '2018-08-26');

      // I had three babies
      fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_DELIVERY,
        RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
      );

      // there were complications
      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
        'Breach'
      );

      // I have something else to say
      fieldValues.set(
        RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
        'So many babies'
      );

      // I am taking time off
      const timeOff: TimeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      };

      // given a simple birth disability model
      const model = new IntakeSubmission(selectedOptions, fieldValues, timeOff);

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({});
      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      // when we process it, the notification id should come back
      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({}).toEqual(result.createdAbsence);
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

        // row 3 - work details - covered in submission service

        // row 4
        expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

        // row 5
        expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

        // row 6
        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).not.toBeCalled();

        // row 7
        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2018-08-25',
          notificationReason: 'Pregnancy, birth or related medical treatment'
        });

        // row 8, 9, 10, 11
        expect(mockAbsenceService.registerAbsence).not.toBeCalled();

        // row 12, 13, 14
        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'Breach'
        });

        // row 15 => 22 - disability claim - missing
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            // date of first absence
            claimIncurredDate: '2018-08-25',
            disabilityDateFromCustomer: '2018-08-25',
            eventType: 'Sickness',
            claimType: 'STD',
            claimAdditionalInfo: 'So many babies',
            expectedReturnToWorkDate: '2019-02-28',
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            notificationDate: moment().format(API_DATE_FORMAT),
            workRelated: false,
            source: DISABILITY_REQUEST_SOURCE
          }
        );

        // 23 row -  absence hospitalisation
        expect(mockAbsenceService.addHospitalisationDetails).not.toBeCalled();

        // 24 row -  claim hospitalisation
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDateConfirmed: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          }
        );

        // row 25 - pregnancy details
        expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
          CLAIM_ID,
          {
            deliveryType: 'Multiple Births',
            pregnancyComplications: true,
            actualDeliveryDate: '2018-08-26'
          }
        );

        // row 26 - multiple timeoffs
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });

    it('Birth Prenatal birth disability with hospitalisation and claim', done => {
      const selectedOptions = [
        EventOptionId.PREGNANCY,
        EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY
      ];

      const fieldValues = new Map<RequestDetailFieldId, any>();

      // I expect to stay in hospital
      fieldValues.set(
        RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        '2018-02-03',
        '2018-02-04'
      ]);

      // I expect to have this birth date
      fieldValues.set(
        RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
        '2018-08-26'
      );

      // and this kind of birth
      fieldValues.set(
        RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
        RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL
      );

      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.DESCRIBE_SICKNESS, 'Pre-eclampsia');

      // I am taking time off
      const timeOff: TimeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods: [timeOffLeavePeriods[1]],
        reducedScheduleLeavePeriods: [],
        episodicLeavePeriods: []
      };

      // given a simple birth disability model
      const model = new IntakeSubmission(selectedOptions, fieldValues, timeOff);

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({});
      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      // when we process it, the notification id should come back
      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({}).toEqual(result.createdAbsence);
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

        // row 3 - work details - covered in submission service

        // row 4
        expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

        // row 5
        expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

        // row 6
        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).not.toBeCalled();

        // row 7
        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2018-08-25',
          notificationReason: 'Pregnancy, birth or related medical treatment'
        });

        // row 8, 9, 10, 11
        expect(mockAbsenceService.registerAbsence).not.toBeCalled();

        // row 12, 13, 14
        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'Pre-eclampsia'
        });

        // row 15 => 22 - disability claim - missing
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            // date of first absence
            claimIncurredDate: '2018-08-25',
            disabilityDateFromCustomer: '2018-08-25',
            eventType: 'Sickness',
            claimType: 'STD',
            expectedReturnToWorkDate: '2019-02-28',
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            notificationDate: moment().format(API_DATE_FORMAT),
            workRelated: false,
            source: DISABILITY_REQUEST_SOURCE
          }
        );

        // 23 row -  absence hospitalisation
        expect(mockAbsenceService.addHospitalisationDetails).not.toBeCalled();

        // 24 row -  claim hospitalisation
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDateConfirmed: true,
            startDate: '2018-02-03',
            endDate: '2018-02-04'
          }
        );

        // row 25 - pregnancy details
        expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
          CLAIM_ID,
          {
            expectedDeliveryDate: '2018-08-26',
            deliveryType: 'Vaginal',
            pregnancyComplications: true
          }
        );

        // row 26 - multiple timeoffs
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });

    it('Postpartum Disability with claim', done => {
      const selectedOptions = [
        EventOptionId.PREGNANCY,
        EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
      ];

      const fieldValues = new Map<RequestDetailFieldId, any>();

      // I stayed overnight in hospital
      fieldValues.set(
        RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        '2018-08-25',
        '2018-08-30'
      ]);

      // my babies' birthday was
      fieldValues.set(RequestDetailFieldId.ACTUAL_DELIVERY_DATE, '2018-08-26');

      // I had three babies
      fieldValues.set(RequestDetailFieldId.NEWBORN_COUNT, 3);

      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_DELIVERY,
        RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
      );

      // there were complications
      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
        'Breach'
      );

      // I have something else to say
      fieldValues.set(
        RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
        'So many babies'
      );

      // I am taking time off
      const timeOff: TimeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods: [timeOffLeavePeriods[1]],
        reducedScheduleLeavePeriods: [],
        episodicLeavePeriods: []
      };

      // given a simple birth disability model
      const model = new IntakeSubmission(selectedOptions, fieldValues, timeOff);

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({});
      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      // when we process it, the notification id should come back
      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({}).toEqual(result.createdAbsence);
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

        // row 3 - work details - covered in submission service

        // row 4
        expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

        // row 5
        expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

        // row 6
        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).not.toBeCalled();

        // row 7
        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2018-08-25',
          notificationReason: 'Pregnancy, birth or related medical treatment'
        });

        // row 8, 9, 10, 11
        expect(mockAbsenceService.registerAbsence).not.toBeCalled();
        // row 12, 13, 14
        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'Breach'
        });

        // row 15 => 22 - disability claim - missing
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            // date of first absence
            claimIncurredDate: '2018-08-25',
            disabilityDateFromCustomer: '2018-08-25',
            eventType: 'Sickness',
            claimType: 'STD',
            claimAdditionalInfo: 'So many babies',
            expectedReturnToWorkDate: '2019-02-28',
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            notificationDate: moment().format(API_DATE_FORMAT),
            workRelated: false,
            source: DISABILITY_REQUEST_SOURCE
          }
        );

        // 23 row -  absence hospitalisation
        expect(mockAbsenceService.addHospitalisationDetails).not.toBeCalled();

        // 24 row -  claim hospitalisation
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDateConfirmed: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          }
        );

        // row 25 - pregnancy details
        expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
          CLAIM_ID,
          {
            deliveryType: 'Multiple Births',
            pregnancyComplications: true,
            actualDeliveryDate: '2018-08-26'
          }
        );

        // row 26 - multiple timeoffs
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });

    it('Pregnancy Terminated with claim', done => {
      const selectedOptions = [
        EventOptionId.PREGNANCY,
        EventOptionId.PREGNANCY_TERMINATION
      ];

      const fieldValues = new Map<RequestDetailFieldId, any>();

      // when did I first notice symptoms
      fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-08-26');

      // I stayed overnight in hospital
      fieldValues.set(
        RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        '2018-08-25',
        '2018-08-30'
      ]);

      // I have something else to say
      fieldValues.set(
        RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
        'Cancer treatment'
      );

      // I am taking time off
      const timeOff: TimeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      };

      // given a simple birth disability model
      const model = new IntakeSubmission(selectedOptions, fieldValues, timeOff);

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({});
      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      // when we process it, the notification id should come back
      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({}).toEqual(result.createdAbsence);
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

        // row 3 - work details - covered in submission service

        // row 4
        expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

        // row 5
        expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

        // row 6
        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).not.toBeCalled();

        // row 7
        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2018-08-25',
          notificationReason: 'Pregnancy, birth or related medical treatment'
        });

        // row 8, 9, 10, 11
        expect(mockAbsenceService.registerAbsence).not.toBeCalled();

        // row 12, 13, 14
        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'Miscarriage',
          symptomsFirstAppeared: '2018-08-26'
        });

        // row 15 => 22 - disability claim - missing
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            // date of first absence
            claimIncurredDate: '2018-08-25',
            dateSymptomsFirstAppeared: '2018-08-26',
            disabilityDateFromCustomer: '2018-08-25',
            eventType: 'Sickness',
            claimType: 'STD',
            claimAdditionalInfo: 'Cancer treatment',
            expectedReturnToWorkDate: '2019-02-28',
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            notificationDate: moment().format(API_DATE_FORMAT),
            workRelated: false,
            source: DISABILITY_REQUEST_SOURCE
          }
        );

        // 23 row -  absence hospitalisation
        expect(mockAbsenceService.addHospitalisationDetails).not.toBeCalled();

        // 24 row -  claim hospitalisation
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDateConfirmed: true,
            startDate: '2018-08-25',
            endDate: '2018-08-30'
          }
        );

        // row 25 - pregnancy details
        expect(mockClaimService.updatePregnancyDetails).not.toBeCalled();

        // row 26 - multiple timeoffs
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });

    it(' Prenatal disability with hospitalisation and claim', done => {
      const selectedOptions = [
        EventOptionId.PREGNANCY,
        EventOptionId.PREGNANCY_PRENATAL_DISABILITY
      ];

      const fieldValues = new Map<RequestDetailFieldId, any>();

      // I have this condition
      fieldValues.set(RequestDetailFieldId.DESCRIBE_SICKNESS, 'Pre-eclampsia');

      // I have this condition
      fieldValues.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2018-02-03');

      // I have received treatment
      fieldValues.set(
        RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.DATE_FIRST_TREATED, '2018-02-06');

      // I was hospitalised
      fieldValues.set(
        RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );

      fieldValues.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        '2018-02-06',
        '2018-02-10'
      ]);

      // I expect to have this birth date
      fieldValues.set(
        RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
        '2018-08-26'
      );

      fieldValues.set(
        RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
        'My doctor said I need bed rest'
      );

      // I am taking time off
      const timeOff: TimeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods: [],
        episodicLeavePeriods: []
      };

      // given a simple birth disability model
      const model = new IntakeSubmission(selectedOptions, fieldValues, timeOff);

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({});
      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      // when we process it, the notification id should come back
      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({}).toEqual(result.createdAbsence);
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        // this order looks odd, but it matches the data mapping so it's easier to validatate the requirements

        // row 3 - work details - covered in submission service

        // row 4
        expect(mockPregnancyService.addPrenatalDetails).not.toBeCalled();

        // row 5
        expect(mockPregnancyService.addPostnatalDetails).not.toBeCalled();

        // row 6
        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).not.toBeCalled();

        // row 7
        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2018-08-25',
          notificationReason: 'Pregnancy, birth or related medical treatment'
        });

        // row 8, 9, 10, 11
        expect(mockAbsenceService.registerAbsence).not.toBeCalled();

        // row 12, 13, 14
        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'Pre-eclampsia',
          symptomsFirstAppeared: '2018-02-03',
          firstDoctorVisitDate: '2018-02-06'
        });

        // row 15 => 22 - disability claim - missing
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            // date of first absence
            claimAdditionalInfo: 'My doctor said I need bed rest',
            claimIncurredDate: '2018-08-25',
            dateSymptomsFirstAppeared: '2018-02-03',
            disabilityDateFromCustomer: '2018-08-25',
            eventType: 'Sickness',
            claimType: 'STD',
            expectedReturnToWorkDate: '2019-02-28',
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            notificationDate: moment().format(API_DATE_FORMAT),
            workRelated: false,
            source: DISABILITY_REQUEST_SOURCE
          }
        );

        // 23 row -  absence hospitalisation
        expect(mockAbsenceService.addHospitalisationDetails).not.toBeCalled();

        // 24 row -  claim hospitalisation
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDate: '2018-02-10',
            endDateConfirmed: true,
            startDate: '2018-02-06'
          }
        );

        // row 25 - pregnancy details
        expect(mockClaimService.updatePregnancyDetails).toBeCalledWith(
          CLAIM_ID,
          {
            expectedDeliveryDate: '2018-08-26'
          }
        );

        // row 26 - multiple timeoffs
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });
  });
});
