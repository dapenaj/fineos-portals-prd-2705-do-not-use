import { DocumentService, AbsenceStatus } from 'fineos-js-api-client';
import { stripIndents } from 'common-tags';

import {
  givenAbsenceUser,
  givenPromise,
  promiseTest,
  mockField
} from '../../../../../../common/utils';
import {
  IntakeSubmission,
  IntakeSubmissionResult,
  SnapshotService
} from '../../../../../../../app/shared/services';
import {
  EventOptionId,
  RequestDetailFieldId,
  TimeOff
} from '../../../../../../../app/shared/types';
import {
  PersonalDetailsFormValue,
  WrapUpSubmission
} from '../../../../../../../app/modules/intake';
import { customerFixture } from '../../../../../../common/fixtures';
import { WorkDetailsFormValue } from '../../../../../../../app/modules/intake/your-request/work-details/form';
import { b64EncodeUnicode } from '../../../../../../../app/shared/utils';

describe('SnapshotService ', () => {
  let snapshotService: SnapshotService;
  let documentService: DocumentService;

  beforeEach(() => {
    givenAbsenceUser();
    snapshotService = SnapshotService.getInstance();
    documentService = mockField(
      snapshotService,
      'documentService',
      new DocumentService()
    );
    documentService.uploadCaseDocument = givenPromise({});
  });

  it('Submission for pregnancy', done => {
    // given we have a successfule submission
    const model = givenSuccessfulSubmission();

    // and everything worked
    const result: IntakeSubmissionResult = {
      createdNotificationId: 'NOT-123',
      createdAbsence: { id: 'ABS-123' },
      createdClaim: { id: 'CLM-123' },
      postCaseErrors: [],
      isClaimRequired: true,
      isAbsenceRequired: true
    };

    const wrapUpSubmission: WrapUpSubmission = {
      additionalDetailsValue: [
        {
          incomeType: 'Sick Pay',
          frequency: 'Weekly',
          amount: 1000,
          startDate: '2019-02-18',
          endDate: '2019-08-18'
        },
        {
          incomeType: 'Sick Pay',
          frequency: 'Monthly',
          amount: 10000,
          startDate: '2019-02-28',
          endDate: '2019-08-28'
        }
      ]
    };

    promiseTest(
      done,
      snapshotService
        .submitSnapshot(model, result, wrapUpSubmission)
        .then(() => {
          // given the above model, this is what we expect to be created

          const expected = {
            documentType: 'Customer Portal Snapshot Document',
            fileName: 'Portal Intake Snapshot NOT-123',
            description: 'Portal Intake Snapshot document',
            fileContentsText: b64EncodeUnicode(getExpectedText()),
            fileExtension: 'txt'
          };

          expect(documentService.uploadCaseDocument).toBeCalledWith(
            'NOT-123',
            expected
          );

          expect(documentService.uploadCaseDocument).toBeCalledWith(
            'ABS-123',
            expected
          );
          expect(documentService.uploadCaseDocument).toBeCalledWith(
            'CLM-123',
            expected
          );
        })
    );
  });

  it('Submission for pregnancy when error happen', done => {
    // given we have a successfule submission
    const model = givenSuccessfulSubmission();

    // and one error occurred
    const result: IntakeSubmissionResult = {
      createdNotificationId: 'NOT-123',
      createdAbsence: { id: 'ABS-123' },
      createdClaim: { id: 'CLM-123' },
      postCaseErrors: ['some error'],
      isClaimRequired: true,
      isAbsenceRequired: true
    };

    const wrapUpSubmission: WrapUpSubmission = {
      additionalDetailsValue: [
        {
          incomeType: 'Sick Pay',
          frequency: 'Weekly',
          amount: 1000,
          startDate: '2019-02-18',
          endDate: '2019-08-18'
        },
        {
          incomeType: 'Sick Pay',
          frequency: 'Monthly',
          amount: 10000,
          startDate: '2019-02-28',
          endDate: '2019-08-28'
        }
      ]
    };

    promiseTest(
      done,
      snapshotService
        .submitSnapshot(model, result, wrapUpSubmission)
        .then(() => {
          // given the above model, this is what we expect to be created

          const expected = {
            documentType: 'Customer Portal Snapshot Document',
            fileName: 'Portal Intake Snapshot NOT-123 (Incomplete)',
            description: 'Portal Intake Snapshot document',
            fileContentsText: b64EncodeUnicode(getExpectedText('some error')),
            fileExtension: 'txt'
          };

          expect(documentService.uploadCaseDocument).toBeCalledWith(
            'NOT-123',
            expected
          );

          expect(documentService.uploadCaseDocument).toBeCalledWith(
            'ABS-123',
            expected
          );
          expect(documentService.uploadCaseDocument).toBeCalledWith(
            'CLM-123',
            expected
          );
        })
    );
  });

  const givenSuccessfulSubmission = () => {
    // given we have this kind of pregnancy
    const selectedOptions = [
      EventOptionId.PREGNANCY,
      EventOptionId.PREGNANCY_PRENATAL,
      EventOptionId.PREGNANCY_PRENATAL_DISABILITY
    ];

    // and we've set these fields
    const selectedFields = new Map<RequestDetailFieldId, any>();
    selectedFields.set(
      RequestDetailFieldId.DESCRIBE_SICKNESS,
      'I put the lime in the coconut'
    );
    selectedFields.set(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '1976-08-24');
    selectedFields.set(
      RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
      RequestDetailFieldId.YES
    );
    selectedFields.set(RequestDetailFieldId.DATE_FIRST_TREATED, '1976-08-25');

    selectedFields.set(
      RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
      RequestDetailFieldId.YES
    );
    selectedFields.set(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
      '1976-08-26',
      '1976-08-30'
    ]);

    selectedFields.set(
      RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
      '1976-08-26'
    );
    selectedFields.set(
      RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
      'And drank them both up'
    );

    // I am taking time off
    const timeOff: TimeOff = {
      outOfWork: true,

      // these dates are backwards, but we should pick the second one as it's earliest
      timeOffLeavePeriods: [
        {
          startDate: '2019-03-01',
          endDate: '2019-04-01',
          lastDayWorked: '2019-03-01',
          expectedReturnToWorkDate: '2019-04-01',
          startDateFullDay: true,
          endDateFullDay: true,
          status: AbsenceStatus.Estimated
        },
        {
          startDate: '2018-08-25',
          endDate: '2019-02-25',
          lastDayWorked: '2018-08-24',
          expectedReturnToWorkDate: '2019-02-28',
          startDateFullDay: true,
          endDateFullDay: false,
          endDateOffHours: 3,
          endDateOffMinutes: 30,
          returnToWorkFullDate: '2019-02-29',
          status: AbsenceStatus.Known
        },
        {
          startDate: '2019-08-25',
          endDate: '2020-02-25',
          lastDayWorked: '2019-08-24',
          expectedReturnToWorkDate: '2020-02-28',
          startDateFullDay: false,
          startDateOffHours: 4,
          endDateFullDay: false,
          endDateOffMinutes: 30,
          returnToWorkFullDate: '2020-02-29',
          status: AbsenceStatus.Estimated
        }
      ],

      reducedScheduleLeavePeriods: [
        {
          startDate: '2019-04-02',
          endDate: '2019-05-02',
          status: AbsenceStatus.Known
        },
        {
          startDate: '2019-05-03',
          endDate: '2019-06-03',
          status: AbsenceStatus.Estimated
        }
      ],
      episodicLeavePeriods: [
        {
          startDate: '2019-06-04',
          endDate: '2019-07-04'
        },
        {
          startDate: '2019-08-05'
        }
      ]
    };

    const personalDetails: PersonalDetailsFormValue = {
      firstName: customerFixture.firstName,
      lastName: customerFixture.lastName,
      dateOfBirth: customerFixture.dateOfBirth,
      phoneNumber: '1-353-234567',
      email: 'bob@dobalina.com',
      premiseNo: customerFixture.customerAddress.address.premiseNo,
      addressLine1: customerFixture.customerAddress.address.addressLine1,
      addressLine2: customerFixture.customerAddress.address.addressLine2,
      addressLine3: customerFixture.customerAddress.address.addressLine3,
      city: customerFixture.customerAddress.address.addressLine4,
      state: customerFixture.customerAddress.address.addressLine6,
      postCode: customerFixture.customerAddress.address.postCode,
      country: 'USA'
    };

    const workDetails: WorkDetailsFormValue = {
      employer: 'FinCorp Ltd',
      jobTitle: 'Line Supervisor',
      dateJobBegan: '2014-02-02'
    };

    // this is the submission model
    return new IntakeSubmission(
      selectedOptions,
      selectedFields,
      timeOff,
      workDetails,
      personalDetails
    );
  };

  // tslint:disable:max-line-length
  // only disabling tslint rule for getExpectedText()
  const getExpectedText = (errorText = '') => {
    const appliedErrorText = errorText
      ? `\n- Error submitting post case request: ${errorText}`
      : '';
    return stripIndents`-------------------------------------
    General : Main
    -------------------------------------
    - Notification Case Id: NOT-123
    - Created Absence : ABS-123
    - Created Claim : CLM-123${appliedErrorText}
    -------------------------------------
    Your Request : Personal Details
    -------------------------------------
    - First name: Jane
    - Last name: Doe
    - Date of birth: 1988-01-01
    - Phone number: 1-353-234567
    - Email: bob@dobalina.com
    - Number: 123
    - Address Line 1: Main street
    - Address Line 2: Where it's at
    - Address Line 3: Two turn tables
    - City: And a microphone
    - State: CA
    - Zip Code: AB123456
    -------------------------------------
    Your Request : Work Details
    -------------------------------------
    - Employer: FinCorp Ltd
    - Job title: Line Supervisor
    - Date of hire: 2014-02-02
    -------------------------------------
    Your Request : Initial Request
    -------------------------------------
    - How can we help you?: A pregnancy, birth or related medical treatment
    - Has your baby been born yet?: No
    - Specify the option which most closely represents why you are taking time off:: Prenatal disability
    -------------------------------------
    Details : Request Details - Pregnancy: Prenatal Disability
    -------------------------------------
    - Briefly describe your condition: I put the lime in the coconut
    - When did you first notice the symptoms? An approximate date will suffice.: 1976-08-24
    - Are you receiving or did you receive any medical treatment for this condition?: YES
    - When is your expected delivery date?: 1976-08-26
    - Anything else you wish to tell us which will help us understand what happened to you?: And drank them both up
    -------------------------------------
    Details : Request Details - Medical Treatment (Sickness)
    -------------------------------------
    - When were you first treated for this condition? An approximate date will suffice.: 1976-08-25
    - Did you stay overnight in hospital?: YES
    -------------------------------------
    Details : Request Details - Hospital Stay (Medical Treatment)
    -------------------------------------
    - What dates did you stay in hospital?: 1976-08-26,1976-08-30
    -------------------------------------
    Details : Time Off - Continuous: 1
    -------------------------------------
    - When was the last day you worked?: 2019-03-01
    - I worked a partial day: No
    - When was the first day of your absence?: 2019-03-01
    - When is the last day of your absence?: 2019-04-01
    - When do you expect to return to work?: 2019-04-01
    - Will you be working normal hours the day you return?: Yes
    - Have these dates been confirmed?: Estimated
    -------------------------------------
    Details : Time Off - Continuous: 2
    -------------------------------------
    - When was the last day you worked?: 2018-08-24
    - I worked a partial day: No
    - When was the first day of your absence?: 2018-08-25
    - When is the last day of your absence?: 2019-02-25
    - When do you expect to return to work?: 2019-02-28
    - Will you be working normal hours the day you return?: No
    - How long were you absent that day?: 03:30
    - When do you expect to return to work full-time? An estimate will suffice.: 2019-02-29
    - Have these dates been confirmed?: Known
    -------------------------------------
    Details : Time Off - Continuous: 3
    -------------------------------------
    - When was the last day you worked?: 2019-08-24
    - I worked a partial day: Yes
    - How long were you absent that day?: 04:00
    - When was the first day of your absence?: 2019-08-25
    - When is the last day of your absence?: 2020-02-25
    - When do you expect to return to work?: 2020-02-28
    - Will you be working normal hours the day you return?: No
    - How long were you absent that day?: 00:30
    - When do you expect to return to work full-time? An estimate will suffice.: 2020-02-29
    - Have these dates been confirmed?: Estimated
    -------------------------------------
    Details : Time Off - Reduced Schedule: 1
    -------------------------------------
    - When is/was your first day of work under a reduced schedule?: 2019-04-02
    - When is your last day of work under a reduced schedule?: 2019-05-02
    - Have these dates been confirmed?: Known
    -------------------------------------
    Details : Time Off - Reduced Schedule: 2
    -------------------------------------
    - When is/was your first day of work under a reduced schedule?: 2019-05-03
    - When is your last day of work under a reduced schedule?: 2019-06-03
    - Have these dates been confirmed?: Estimated
    -------------------------------------
    Details : Time Off - Intermittent: 1
    -------------------------------------
    - When does your intermittent leave period start?: 2019-06-04
    - If you are requesting intermittent leave for a limited period of time, you can enter here the last day of this period:: 2019-07-04
    -------------------------------------
    Details : Time Off - Intermittent: 2
    -------------------------------------
    - When does your intermittent leave period start?: 2019-08-05
    - If you are requesting intermittent leave for a limited period of time, you can enter here the last day of this period:: Unknown
    -------------------------------------
    Wrap Up : Additional Income Source : 1
    -------------------------------------
    - Income type: Sick Pay
    - How often are you paid?: Weekly
    - How much are you paid?: 1000
    - When did you first receive payment for this?: 2019-02-18
    - When will you receive your last payment for this?: 2019-08-18
    -------------------------------------
    Wrap Up : Additional Income Source : 2
    -------------------------------------
    - Income type: Sick Pay
    - How often are you paid?: Monthly
    - How much are you paid?: 10000
    - When did you first receive payment for this?: 2019-02-28
    - When will you receive your last payment for this?: 2019-08-28`;
  };
});
