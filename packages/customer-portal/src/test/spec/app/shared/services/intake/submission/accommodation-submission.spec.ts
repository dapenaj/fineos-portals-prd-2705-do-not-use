import {
  AccommodationService,
  SdkConfigProps,
  AccommodationRequest
} from 'fineos-js-api-client';

import { IntakeAccommodationSubmission } from '../../../../../../../app/shared/services/intake/submission/intake-accommodation-submission/intake-accommodation-submission';
import { AccommodationIntake } from '../../../../../../../app/shared/intakes';
import { AccommodationValue } from '../../../../../../../app/modules/intake';
import { EventOptionId, Role } from '../../../../../../../app/shared/types';
import { AuthenticationService } from '../../../../../../../app/shared/services';

describe('Intake Submission: Accommodation', () => {
  let action: AccommodationIntake;

  const accommodation: AccommodationValue = {
    pregnancyRelated: 'Yes',
    limitations: [
      { limitationType: 'Dietary Needs' },
      { limitationType: 'Headache' }
    ],
    workPlaceAccommodations: [
      {
        accommodationCategory:
          'Physical workplace modifications or workstation relocation',
        accommodationType: 'Widen doorway',
        accommodationDescription: ''
      },
      {
        accommodationCategory: 'Other Accommodation',
        accommodationType: 'Other',
        accommodationDescription: 'Broken Leg'
      }
    ],
    additionalNotes: 'These are some additional notes'
  };

  beforeEach(() => {
    action = new AccommodationIntake();
  });

  test('should support EventOptionId.ACCOMMODATION', () => {
    const accommodationSubmission = new IntakeAccommodationSubmission(
      [EventOptionId.ACCOMMODATION],
      accommodation
    );

    expect(action.supports(accommodationSubmission)).toBe(true);
  });

  describe('process', () => {
    let startAccommodationMock: jest.Mock;
    let originalStartAccommodation: any;
    let originalAuthorizationRole: any;

    beforeEach(() => {
      const accommodationService = AccommodationService.getInstance();

      startAccommodationMock = jest.fn();
      originalStartAccommodation = accommodationService.startAccommodation;
      accommodationService.startAccommodation = startAccommodationMock;
      AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];
      originalAuthorizationRole = SdkConfigProps.authorizationRole;

      SdkConfigProps.authorizationRole = jest
        .fn()
        .mockImplementation(val => val);
    });

    afterEach(() => {
      const accommodationService = AccommodationService.getInstance();
      accommodationService.startAccommodation = originalStartAccommodation;
      AuthenticationService.getInstance().displayRoles = [];
      SdkConfigProps.authorizationRole = originalAuthorizationRole;
    });

    test('should create accommodation - accommodation flow', async () => {
      await action.process(
        new IntakeAccommodationSubmission(
          [EventOptionId.ACCOMMODATION],
          accommodation
        )
      );

      expect(startAccommodationMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new AccommodationRequest(), {
            pregnancyRelated: 'Yes',
            limitations: accommodation.limitations,
            workPlaceAccommodations: accommodation.workPlaceAccommodations,
            additionalNotes: 'These are some additional notes',
            notificationReason: 'Accommodation required to remain at work'
          })
        )
      );
    });
  });
});
