import {
  AbsenceService,
  ClaimService,
  NotificationCaseSummary
} from 'fineos-js-api-client';

import { NotificationService } from '../../../../../app/shared/services';
import {
  givenAbsenceUser,
  givenCustomerUser,
  givenUserWithNoRole,
  givenPromise,
  promiseTest
} from '../../../../common/utils';

import {
  benefitsFixture,
  absencesFixture,
  notificationsFixture
} from './../../../../common/fixtures/notification.fixture';

describe('Notification Service', () => {
  const service = NotificationService.getInstance();

  // we will override these service's findNotifications with new mocks each time
  const mockAbsenceService = new AbsenceService();
  Object.defineProperty(service, 'absenceService', {
    value: mockAbsenceService
  });

  const mockClaimService = new ClaimService();
  Object.defineProperty(service, 'claimService', { value: mockClaimService });

  beforeEach(() => {
    mockAbsenceService.findNotifications = givenNotifications([
      'CASE1',
      'CASE2'
    ]);
    mockAbsenceService.getAbsenceDetails = givenAbsenceDetails();

    mockClaimService.findNotifications = givenNotifications(['CASE2', 'CASE3']);
    mockClaimService.findBenefits = givenBenefits();
  });

  describe('#getInstance', () => {
    it('Validate singleton', () => {
      // confirm we get the right type back
      const firstCall = NotificationService.getInstance();
      expect(firstCall).toBeInstanceOf(NotificationService);
      // now confirm we always get back the same instance
      expect(NotificationService.getInstance()).toBe(firstCall);
    });
  });

  describe('#fetchNotifications', () => {
    it('Absence user should use absence service', done => {
      givenAbsenceUser();

      promiseTest(
        done,
        service.fetchNotifications().then(r => {
          expect(mockAbsenceService.findNotifications).toBeCalled();
          expect(mockClaimService.findNotifications).not.toBeCalled();

          expect(['CASE1', 'CASE2']).toEqual(r.map(f => f.notificationCaseId));
        })
      );
    });

    it('Claims user should use claims service', done => {
      givenCustomerUser();
      promiseTest(
        done,
        service.fetchNotifications().then(r => {
          expect(mockAbsenceService.findNotifications).not.toBeCalled();
          expect(mockClaimService.findNotifications).toBeCalled();
          expect(['CASE2', 'CASE3']).toEqual(r.map(f => f.notificationCaseId));
        })
      );
    });

    it('Users with no roles should see nothing', done => {
      givenUserWithNoRole();
      promiseTest(
        done,
        service.fetchNotifications().then(r => {
          expect(mockAbsenceService.findNotifications).not.toBeCalled();
          expect(mockClaimService.findNotifications).not.toBeCalled();
          expect([]).toEqual(r.map(f => f.notificationCaseId));
        })
      );
    });
  });

  describe('#completeNotification', () => {
    beforeEach(() => {
      mockAbsenceService.completeIntake = givenNotification('NTN-1');
      mockClaimService.completeIntake = givenNotification('NTN-2');
    });

    it('Absence user should use absence service', done => {
      givenAbsenceUser();
      promiseTest(
        done,
        service.completeNotification('CASE1').then(r => {
          expect(mockAbsenceService.completeIntake).toBeCalledWith('CASE1');
          expect(mockClaimService.completeIntake).not.toBeCalled();

          expect('NTN-1').toEqual(r && r.notificationCaseId);
        })
      );
    });

    it('Claims user should use claims service', done => {
      givenCustomerUser();

      promiseTest(
        done,
        service.completeNotification('CASE2').then(r => {
          expect(mockAbsenceService.completeIntake).not.toBeCalled();
          expect(mockClaimService.completeIntake).toBeCalledWith('CASE2');

          expect('NTN-2').toEqual(r && r.notificationCaseId);
        })
      );
    });

    it('Users with no roles should see nothing', done => {
      givenUserWithNoRole();
      promiseTest(
        done,
        service.completeNotification('CASE1').then(r => {
          expect(mockAbsenceService.completeIntake).not.toBeCalled();
          expect(mockClaimService.completeIntake).not.toBeCalled();

          expect(r).toBeUndefined();
        })
      );
    });
  });

  describe('fetchNotificationEntitlements', () => {
    it('should return entitlements for an absence user', done => {
      givenAbsenceUser();

      promiseTest(
        done,
        service
          .fetchNotificationEntitlements(notificationsFixture[0])
          .then(result => {
            expect(result).toEqual([
              {
                text: 'Job-protected Leave',
                value: 'ABS-1,0absence'
              },
              {
                text: 'Wage Replacement (Short Term Disability)',
                value: '0,0benefit'
              },
              {
                text: 'Wage Replacement (Paid Leaves)',
                value: 'ABS-1,0benefit'
              },
              {
                text:
                  'Physical workplace modifications or workstation relocation | Make materials and equipment within reach',
                value: 'ACC-35,0accommodation'
              }
            ]);
          })
      );
    });

    it('should return entitlements for a claims user', done => {
      givenCustomerUser();

      promiseTest(
        done,
        service
          .fetchNotificationEntitlements(notificationsFixture[0])
          .then(result => {
            expect(result).toEqual([
              {
                value: '0,0benefit',
                text: 'Wage Replacement (Short Term Disability)'
              }
            ]);
          })
      );
    });
  });
});

const givenNotifications = (caseIds: string[]) => {
  // build cases for these ids
  const cases = caseIds.map(id => {
    const notification = new NotificationCaseSummary();
    notification.notificationCaseId = id;
    return notification;
  });

  // mock something to return thes;
  const fn = jest.fn();
  fn.mockReturnValue(Promise.resolve(cases));

  return fn;
};

const givenNotification = (caseId: string) => {
  const notification = new NotificationCaseSummary();
  notification.notificationCaseId = caseId;

  // mock something to return thes;
  const fn = jest.fn();
  return fn.mockReturnValue(Promise.resolve(notification));
};

const givenBenefits = () => {
  const fn = jest.fn();
  return fn.mockReturnValue(Promise.resolve(benefitsFixture));
};

const givenAbsenceDetails = () => {
  const fn = jest.fn();
  return fn.mockReturnValue(Promise.resolve(absencesFixture[0]));
};
