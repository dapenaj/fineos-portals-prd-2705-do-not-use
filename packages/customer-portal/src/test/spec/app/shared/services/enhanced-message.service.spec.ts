import { MessageService } from 'fineos-js-api-client';

import {
  notificationsFixture,
  messagesFixture
} from '../../../../common/fixtures';
import {
  EnhancedMessageService,
  NotificationService
} from '../../../../../app/shared/services';
import { mockField, givenPromise, promiseTest } from '../../../../common/utils';

describe('Enhanced Message Service', () => {
  const service = EnhancedMessageService.getInstance();

  let mockNotificationService: NotificationService;
  mockNotificationService = mockField(
    service,
    'caseService',
    new NotificationService()
  );

  let mockMessageService: MessageService;
  mockMessageService = mockField(
    service,
    'messageService',
    new MessageService()
  );

  beforeEach(() => {
    mockMessageService.getMessages = givenPromise(messagesFixture);
    mockNotificationService.getCaseIdsFromNotification = givenPromise([
      'NTN-21',
      'ABS-1',
      'CLM-1'
    ]);
  });

  it('should return messages for all notifications', done => {
    promiseTest(
      done,
      service.fetchMessages(notificationsFixture).then(value => {
        expect(value.length).toBe(0);
      })
    );
  });

  it('should return messages for a single notification', done => {
    promiseTest(
      done,
      service
        .fetchMessagesByCaseId(['ABS-1', 'CLM-1'], 'NTN-21')
        .then(value => {
          expect(value.length).toBe(10);
        })
    );
  });
});
