import {
  AbsenceStatus,
  RequestDate,
  ReducedSchedulePeriodRequest,
  NewAbsenceRequest,
  TimeOffPeriodRequest
} from 'fineos-js-api-client';

import {
  IntakeSubmission,
  IntakeSubmissionResult
} from '../../../../../../../app/shared/services';
import { promiseTest } from '../../../../../../common/utils';
import {
  TimeOffLeavePeriod,
  ReducedScheduleLeavePeriod,
  EpisodicLeavePeriod
} from '../../../../../../../app/shared/types';

export const testProcessShouldWork = (
  intake: any,
  done: jest.DoneCallback,
  model: IntakeSubmission,
  testCallback: (result: IntakeSubmissionResult) => any
) => {
  promiseTest(
    done,
    intake
      .process(model)
      .then(
        (onfulfilled: IntakeSubmissionResult) => {
          testCallback(onfulfilled);
        },
        (onrejected: any) => {
          done.fail('Rejected request: ' + onrejected);
          return Promise.reject('Rejected request: ' + onrejected);
        }
      )
      .catch((e: any) => {
        done.fail('Bad request: ' + e);
        return Promise.reject('Bad request: ' + e);
      })
  );
};

export const timeOffLeavePeriods: TimeOffLeavePeriod[] = [
  {
    startDate: '2019-03-01',
    endDate: '2019-04-01',
    lastDayWorked: '2018-08-24',
    expectedReturnToWorkDate: '2019-04-01',
    endDateFullDay: true,
    status: AbsenceStatus.Estimated
  },
  {
    startDate: '2018-08-25',
    endDate: '2019-02-25',
    lastDayWorked: '2018-08-24',
    expectedReturnToWorkDate: '2019-02-28',
    endDateFullDay: false,
    endDateOffHours: 3,
    endDateOffMinutes: 30,
    status: AbsenceStatus.Known
  }
];

export const reducedScheduleLeavePeriods: ReducedScheduleLeavePeriod[] = [
  {
    startDate: '2019-04-02',
    endDate: '2019-05-02',
    status: AbsenceStatus.Known
  },
  {
    startDate: '2019-05-03',
    endDate: '2019-06-03',
    status: AbsenceStatus.Estimated
  }
];

export const episodicLeavePeriods: EpisodicLeavePeriod[] = [
  {
    startDate: '2019-06-04',
    endDate: '2019-07-04'
  },
  {
    startDate: '2019-08-05',
    endDate: '2019-09-05'
  }
];

export const reducedScheduleRequests = [
  {
    endDate: '2019-05-02',
    startDate: '2019-04-02',
    status: 'Known'
  },
  {
    endDate: '2019-06-03',
    startDate: '2019-05-03',
    status: 'Estimated'
  }
];

export const episodicRequests = [
  {
    endDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-07-04'
    },
    startDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-06-04'
    }
  },
  {
    endDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-09-05'
    },
    startDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-08-05'
    }
  }
];

export const timeOffRequests = [
  {
    startDate: {
      date: '2019-03-01'
    } as RequestDate,
    endDate: {
      date: '2019-04-01'
    } as RequestDate,
    endDateFullDay: true,
    lastDayWorked: '2018-08-24',
    expectedReturnToWorkDate: '2019-04-01',
    status: AbsenceStatus.Estimated
  },
  {
    startDate: {
      date: '2018-08-25'
    } as RequestDate,
    endDate: {
      _offHours: 3,
      _offMinutes: 30,
      date: '2019-02-25'
    },
    endDateFullDay: false,
    endDateOffHours: 3,
    endDateOffMinutes: 30,
    lastDayWorked: '2018-08-24',
    expectedReturnToWorkDate: '2019-02-28',
    status: AbsenceStatus.Known
  }
] as TimeOffPeriodRequest[];

export const episodicUndefinedTimeRequests = [
  {
    endDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-07-04'
    },
    startDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-06-04'
    }
  },
  {
    endDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-09-05'
    },
    startDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-08-05'
    }
  }
];

export const timeOffUndefinedTimeRequests = [
  {
    endDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-04-01'
    },
    endDateFullDay: true,
    endDateOffHours: undefined,
    endDateOffMinutes: undefined,
    lastDayWorked: '2018-08-24',
    expectedReturnToWorkDate: '2019-04-01',
    startDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2019-03-01'
    },
    startDateFullDay: undefined,
    startDateOffHours: undefined,
    startDateOffMinutes: undefined,
    status: 'Estimated'
  },
  {
    endDate: { _offHours: 3, _offMinutes: 30, date: '2019-02-25' },
    endDateFullDay: false,
    endDateOffHours: 3,
    endDateOffMinutes: 30,
    lastDayWorked: '2018-08-24',
    expectedReturnToWorkDate: '2019-02-28',
    startDate: {
      _offHours: undefined,
      _offMinutes: undefined,
      date: '2018-08-25'
    },
    startDateFullDay: undefined,
    startDateOffHours: undefined,
    startDateOffMinutes: undefined,
    status: 'Known'
  }
];
