import { EventDetailOptionService } from '../../../../../app/shared/services';
import {
  EventOptionEdge,
  EventOptionId,
  IntakeSectionStepId,
  Role
} from '../../../../../app/shared/types';
import { givenAbsenceUser, givenCustomerUser } from '../../../../common/utils';

describe('Event Detail Option Service', () => {
  // Specifically creating our own test model rather than use the singleton version.
  // We will test the _real_ model in other tests - this is just to test the base config
  const TEST_EVENT_DETAIL_OPTIONS_TREE: EventOptionEdge[] = [
    { from: EventOptionId.ORIGIN, to: EventOptionId.SICKNESS },
    { from: EventOptionId.ORIGIN, to: EventOptionId.ACCIDENT },
    {
      from: EventOptionId.SICKNESS,
      to: EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS,
      requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR]
    },

    {
      from: EventOptionId.SICKNESS,
      to: EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE,
      requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR]
    },
    {
      from: EventOptionId.SICKNESS,
      to: EventOptionId.SICKNESS_TIMEOFF_DONATION,
      requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR],
      skipSteps: [IntakeSectionStepId.TIME_OFF]
    },

    {
      from: EventOptionId.SICKNESS_TIMEOFF_DONATION,
      to: EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD,
      skipSteps: [IntakeSectionStepId.SUPPORTING_EVIDENCE]
    },
    {
      from: EventOptionId.SICKNESS_TIMEOFF_DONATION,
      to: EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL,
      skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
    }
  ];
  const service = new EventDetailOptionService(TEST_EVENT_DETAIL_OPTIONS_TREE);

  describe('Check Option children', () => {
    it('default options should be as expected', () => {
      givenAbsenceUser();
      const observed: EventOptionId[] = service.start();
      const expected: EventOptionId[] = [
        EventOptionId.SICKNESS,
        EventOptionId.ACCIDENT
      ];

      expect(expected).toStrictEqual(observed);
    });

    it('path to valid element should find children', () => {
      givenAbsenceUser();

      const path: EventOptionId[] = [
        EventOptionId.SICKNESS,
        EventOptionId.SICKNESS_TIMEOFF_DONATION
      ];
      const observed: EventOptionId[] = service.next(path);
      const expected: EventOptionId[] = [
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD,
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL
      ];

      expect(expected).toStrictEqual(observed);
    });

    it('path to valid element should find nothing if user does not have a role that can see it', () => {
      // given we only have a claims role
      givenCustomerUser();

      // when I try to see what children sickness has?
      const path: EventOptionId[] = [
        EventOptionId.SICKNESS,
        // this requires absense user
        EventOptionId.SICKNESS_TIMEOFF_DONATION,
        // this requires nothing on its own,
        // but since the parent requires it we should
        // see nothing
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD
      ];
      const observed: EventOptionId[] = service.next(path);

      // then I should see nothing
      const expected: EventOptionId[] = [];
      expect(expected).toStrictEqual(observed);
    });

    it('path to tail element should find nothing', () => {
      givenAbsenceUser();

      const path: EventOptionId[] = [
        EventOptionId.SICKNESS,
        EventOptionId.SICKNESS_TIMEOFF_DONATION,
        EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
      ];
      const observed: EventOptionId[] = service.next(path);
      const expected: EventOptionId[] = [];

      expect(expected).toStrictEqual(observed);
    });

    it('path to invalid element should find nothing without breaking', () => {
      givenAbsenceUser();

      const path: EventOptionId[] = [
        EventOptionId.SICKNESS,
        EventOptionId.ACCIDENT
      ];
      const observed: EventOptionId[] = service.next(path);
      const expected: EventOptionId[] = [];

      expect(expected).toStrictEqual(observed);
    });
  });

  describe('getSkipSteps', () => {
    it('should return all skipTests ancestors', () => {
      expect(
        service.getSkipSteps([
          EventOptionId.SICKNESS,
          EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
        ])
      ).toEqual([]);

      expect(
        service.getSkipSteps([EventOptionId.SICKNESS_TIMEOFF_DONATION])
      ).toEqual([IntakeSectionStepId.TIME_OFF]);

      expect(
        service.getSkipSteps([
          EventOptionId.SICKNESS_TIMEOFF_DONATION,
          EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD
        ])
      ).toEqual([
        IntakeSectionStepId.TIME_OFF,
        IntakeSectionStepId.SUPPORTING_EVIDENCE
      ]);

      expect(
        service.getSkipSteps([
          EventOptionId.SICKNESS_TIMEOFF_DONATION,
          EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL
        ])
      ).toEqual([
        IntakeSectionStepId.TIME_OFF,
        IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES
      ]);
    });
  });
});
