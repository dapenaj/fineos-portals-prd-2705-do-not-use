import { AuthenticationStrategy, SdkConfigProps } from 'fineos-js-api-client';
import { advanceTo } from 'jest-date-mock';
import moment from 'moment';

import { AuthenticationService } from '../../../../../app/shared/services';

describe('Authentication Service', () => {
  const service = AuthenticationService.getInstance();

  it('should redirect to logout url on logout', () => {
    service.logout();

    expect(document.location.href).toContain('localhost/#');
  });

  describe('for AuthenticationStrategy.NONE', () => {
    beforeEach(() => {
      jest
        .spyOn(service, 'authStrategy', 'get')
        .mockReturnValue(AuthenticationStrategy.NONE);
    });

    it('should reutn isAuthenticated true', () => {
      expect(service.isAuthenticated).toBe(true);
    });

    it('should set insecure strategy on the serviceFactory', () => {
      jest
        .spyOn(SdkConfigProps, 'debugUserId', 'get')
        .mockReturnValue('test-user-id');

      service.saveCredentials('');

      expect(service.serviceFactory.strategy.getUserId()).toEqual(
        'test-user-id'
      );
    });
  });

  describe('for AuthenticationStrategy.COGNITO', () => {
    const setParams = () => {
      service.accessKeyId = 'test-access-key-id';
      service.secretKey = 'test-secret';
      service.sessionToken = 'test-token';
      service.userId = 'test-id+';
      service.role = 'test-role';
    };

    beforeEach(() => {
      advanceTo(new Date(2017, 6, 15, 13, 45));

      jest
        .spyOn(service, 'authStrategy', 'get')
        .mockReturnValue(AuthenticationStrategy.COGNITO);
    });

    it('should return isAuthenticated true if all params have been provided and token is valid', () => {
      setParams();
      service.expiration = moment([2017, 6, 15, 13, 46]).toISOString();
      expect(service.isAuthenticated).toBe(true);
    });

    it('should set cognito strategy on the serviceFactory using the params from the url', () => {
      service.saveCredentials(
        `#AccessKeyId=test-access-key-id#Expiration=${moment([
          2017,
          6,
          15
        ]).toISOString()}#SecretKey=test-secret#SessionToken=test-token#UserID=test-id+#Role=test-role#DisplayRole=test-role`
      );

      expect(service.serviceFactory.strategy.getUserId()).toEqual('test-id+');
      expect(service.displayRoles[0]).toEqual('test-role');
    });
  });
});
