import {
  AbsenceService,
  SdkConfigProps,
  NewAbsenceRequest,
  IntakeSource,
  AbsenceCaseNotifiedBy
} from 'fineos-js-api-client';

import { OtherIntake } from '../../../../../../../app/shared/intakes';
import {
  IntakeSubmission,
  AuthenticationService
} from '../../../../../../../app/shared/services';
import {
  EventOptionId,
  Role,
  TimeOff
} from '../../../../../../../app/shared/types';

import {
  timeOffLeavePeriods,
  reducedScheduleLeavePeriods,
  episodicLeavePeriods,
  timeOffRequests,
  episodicRequests,
  reducedScheduleRequests
} from './submission-test-utils';

describe('Intake Submission: Other', () => {
  let action: OtherIntake;

  beforeEach(() => {
    action = new OtherIntake();
  });

  test('should support EventOptionId.OTHER', () => {
    const intakeSubmission = new IntakeSubmission(
      [EventOptionId.OTHER],
      new Map(),
      { outOfWork: true }
    );

    expect(action.supports(intakeSubmission)).toBe(true);
  });

  describe('process', () => {
    let registerAbsenceMock: jest.Mock;
    let originalRegisterAbsence: any;
    let originalAuthorizationRole: any;

    const commonProps = {
      intakeSource: IntakeSource.SelfService,
      notifiedBy: AbsenceCaseNotifiedBy.Employee,
      notificationReason: 'Out of work for another reason',
      notificationCaseId: ''
    };

    beforeAll(() => {
      AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];
      originalAuthorizationRole = SdkConfigProps.authorizationRole;
      SdkConfigProps.authorizationRole = jest
        .fn()
        .mockImplementation(val => val);
    });

    beforeEach(() => {
      const absenceService = AbsenceService.getInstance();
      originalRegisterAbsence = absenceService.registerAbsence;
      registerAbsenceMock = jest.fn();
      absenceService.registerAbsence = registerAbsenceMock;
    });

    afterEach(() => {
      AbsenceService.getInstance().registerAbsence = originalRegisterAbsence;
    });

    afterAll(() => {
      AuthenticationService.getInstance().displayRoles = [];
      SdkConfigProps.authorizationRole = originalAuthorizationRole;
    });

    test('should create absence for OTHER_EMPLOYEE_JURY_DUTY flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_JURY_DUTY
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Civic Duty',
            reasonQualifier1: 'Jury'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_VOTING flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_VOTING
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Civic Duty',
            reasonQualifier1: 'Voting'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_WITNESS flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_WITNESS
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Civic Duty',
            reasonQualifier1: 'Witness'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_EDUCATION flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_EDUCATION
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Educational Activity - Employee'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_MILITARY flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_MILITARY
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military - Employee'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
            EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Personal - Employee',
            reasonQualifier1: 'Right to Leave',
            reasonQualifier2: 'Medical Related'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
            EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Personal - Employee',
            reasonQualifier1: 'Right to Leave',
            reasonQualifier2: 'Non Medical'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_UNION flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_UNION
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Union Business'
          })
        )
      );
    });

    test('should create absence for OTHER_EMPLOYEE_EMERGENCY flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_EMERGENCY
          ],
          new Map(),
          {
            outOfWork: true
          }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Public Health Emergency - Employee'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MEDICAL_DONATION flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_DONATION
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Medical Donation - Family'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Preventative Care - Family Member'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_PASSED_AWAY flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_PASSED_AWAY
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Bereavement'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Caregiver'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Childcare and School Activities'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_COUNSELING flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_COUNSELING
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Counseling'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_LEGAL flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_LEGAL
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Financial & Legal Arrangements'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_EVENT flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_EVENT
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Military Events & Related Activities'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Parental Care'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1:
              'Post Deployment Activities - Including Bereavement'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Rest & Recuperation'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Short Notice Deployment'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
            EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Military Exigency Family',
            reasonQualifier1: 'Other Additional Activities'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_EDUCATION flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_EDUCATION
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Educational Activity - Family'
          })
        )
      );
    });

    test('should create absence for OTHER_FAMILY_MEMBER_EMERGENCY flow', async () => {
      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_FAMILY_MEMBER,
            EventOptionId.OTHER_FAMILY_MEMBER_EMERGENCY
          ],
          new Map(),
          { outOfWork: true }
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), commonProps, {
            reason: 'Public Health Emergency - Family'
          })
        )
      );
    });

    test('should create absence with time-off details', async () => {
      const timeOff = {
        outOfWork: true,
        // these dates are backwards, but we should pick the second one as it's earliest
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      } as TimeOff;

      await action.process(
        new IntakeSubmission(
          [
            EventOptionId.OTHER,
            EventOptionId.OTHER_EMPLOYEE,
            EventOptionId.OTHER_EMPLOYEE_JURY_DUTY
          ],
          new Map(),
          timeOff
        )
      );

      expect(registerAbsenceMock).toBeCalledWith(
        jasmine.objectContaining(
          Object.assign(new NewAbsenceRequest(), {
            timeOffRequests,
            episodicRequests,
            reducedScheduleRequests
          })
        )
      );
    });
  });
});
