import {
  AbsenceCaseNotifiedBy,
  AbsenceService,
  IntakeSource,
  ClaimService,
  AdditionalConditionsService
} from 'fineos-js-api-client';
import moment from 'moment';

import {
  CLAIM_CASE_TYPE,
  DISABILITY_REQUEST_SOURCE
} from '../../../../../../../app/shared/constants';
import { AccidentIntake } from '../../../../../../../app/shared/intakes';
import { IntakeSubmission } from '../../../../../../../app/shared/services';
import {
  EventOptionId,
  TimeOff,
  RequestDetailFieldId
} from '../../../../../../../app/shared/types';
import { API_DATE_FORMAT } from '../../../../../../../app/shared/utils';
import {
  givenAbsenceUser,
  givenCustomerUser,
  givenPromise,
  mockField
} from '../../../../../../common/utils';

import {
  episodicLeavePeriods,
  episodicRequests,
  reducedScheduleLeavePeriods,
  reducedScheduleRequests,
  testProcessShouldWork,
  timeOffLeavePeriods,
  timeOffRequests
} from './submission-test-utils';

describe('Accident Intake Submission', () => {
  const NOTIFICATION_ID = 'CASE123';
  const ABSENCE_ID = 'ABSENCE_ID';
  const CLAIM_ID = 'CLAIM_ID';

  let intake: AccidentIntake;
  let mockAbsenceService: AbsenceService;
  let mockClaimService: ClaimService;
  let mockAdditionalConditionsService: AdditionalConditionsService;

  beforeEach(() => {
    intake = new AccidentIntake();

    mockAbsenceService = mockField(
      intake,
      'absenceService',
      new AbsenceService()
    );

    mockClaimService = mockField(intake, 'claimService', new ClaimService());

    mockAdditionalConditionsService = mockField(
      intake,
      'additionalConditionsService',
      new AdditionalConditionsService()
    );
  });

  const givenAllPostCaseRequestsWork = () => {
    // assume all underlying service requests work
    mockClaimService.updateMedicalDetails = givenPromise({});
    mockClaimService.updateDiabilityDetails = givenPromise({});
    mockClaimService.addHospitalisationDetails = givenPromise({});

    mockAdditionalConditionsService.addAdditionalConditions = givenPromise({});
    mockAbsenceService.addHospitalisationDetails = givenPromise({});
    mockClaimService.indicateMultiplePeriodsInAbsence = givenPromise({});
  };

  test('should support EventOptionId.ACCIDENT', () => {
    const intakeSubmission = new IntakeSubmission(
      [EventOptionId.ACCIDENT],
      new Map(),
      { outOfWork: true }
    );
    expect(intake.supports(intakeSubmission)).toBe(true);
  });

  describe('Absence User', () => {
    beforeEach(() => {
      givenAbsenceUser();
    });

    it('should create an absence and claim for accident flow', done => {
      const fieldValues = new Map<RequestDetailFieldId, any>();
      fieldValues.set(
        RequestDetailFieldId.DESCRIBE_ACCIDENT,
        'broke finger on keyboard'
      );
      fieldValues.set(RequestDetailFieldId.ACCIDENT_DATE, '2019-02-12');
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_WORK_RELATED,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED,
        '2019-02-13'
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ['2019-02-13', '2019-02-23']
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
        'it was really gross'
      );

      const timeOff: TimeOff = {
        outOfWork: true,
        timeOffLeavePeriods,
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      };

      const model = new IntakeSubmission(
        [EventOptionId.ACCIDENT],
        fieldValues,
        timeOff
      );

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        absenceId: ABSENCE_ID
      });

      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
          result.createdAbsence
        );
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        expect(mockAbsenceService.registerAbsence).toBeCalledWith({
          notifiedBy: AbsenceCaseNotifiedBy.Employee,
          notificationReason: 'Accident or treatment required for an injury',
          intakeSource: IntakeSource.SelfService,
          reason: 'Serious Health Condition - Employee',
          additionalComments: '',
          reasonQualifier1: 'Work Related',
          reasonQualifier2: 'Accident / Injury',
          timeOffRequests,
          episodicRequests,
          reducedScheduleRequests
        });

        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          notificationCaseId: NOTIFICATION_ID,
          notificationReason: 'Accident or treatment required for an injury',
          claimIncurredDate: '2019-02-12'
        });

        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'broke finger on keyboard'
        });
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            eventType: 'Accident',
            claimType: 'STD',
            source: DISABILITY_REQUEST_SOURCE,
            notificationDate: moment().format(API_DATE_FORMAT),
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2018-08-25',
            expectedReturnToWorkDate: '2019-02-28',
            workRelated: true,
            claimIncurredDate: '2019-02-12',
            disabilityDateFromCustomer: '2019-02-12',
            accidentDate: '2019-02-12',
            claimAdditionalInfo: 'it was really gross'
          }
        );
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDateConfirmed: true,
            startDate: '2019-02-13',
            endDate: '2019-02-23'
          }
        );

        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).toBeCalledWith({
          caseId: ABSENCE_ID,
          additionalDescription: 'it was really gross'
        });
        expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
          caseId: ABSENCE_ID,
          ovenightAtHospital: true,
          startDate: '2019-02-13',
          endDate: '2019-02-23'
        });
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).toBeCalledWith(CLAIM_ID, ABSENCE_ID);
      });
    });

    it('should only create absence when no continuous timeoff', done => {
      const fieldValues = new Map<RequestDetailFieldId, any>();
      fieldValues.set(
        RequestDetailFieldId.DESCRIBE_ACCIDENT,
        'broke finger on keyboard'
      );
      fieldValues.set(RequestDetailFieldId.ACCIDENT_DATE, '2019-02-12');
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_WORK_RELATED,
        RequestDetailFieldId.NO
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED,
        '2019-02-13'
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ['2019-02-13', '2019-02-23']
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
        'it was really gross'
      );

      const timeOff: TimeOff = {
        outOfWork: true,
        timeOffLeavePeriods: [],
        reducedScheduleLeavePeriods,
        episodicLeavePeriods
      };

      const model = new IntakeSubmission(
        [EventOptionId.ACCIDENT],
        fieldValues,
        timeOff
      );

      // given the absence and claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        absenceId: ABSENCE_ID
      });
      mockClaimService.startClaim = givenPromise({});

      givenAllPostCaseRequestsWork();

      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({ id: ABSENCE_ID, isMedicalProviderRelated: false }).toEqual(
          result.createdAbsence
        );
        expect({}).toEqual(result.createdClaim);

        expect(mockAbsenceService.registerAbsence).toBeCalledWith({
          notifiedBy: AbsenceCaseNotifiedBy.Employee,
          notificationReason: 'Accident or treatment required for an injury',
          intakeSource: IntakeSource.SelfService,
          reason: 'Serious Health Condition - Employee',
          additionalComments: '',
          reasonQualifier1: 'Not Work Related',
          reasonQualifier2: 'Accident / Injury',
          timeOffRequests: [],
          episodicRequests,
          reducedScheduleRequests
        });

        expect(mockClaimService.startClaim).not.toBeCalled();

        expect(mockClaimService.updateMedicalDetails).not.toBeCalled();
        expect(mockClaimService.updateDiabilityDetails).not.toBeCalled();
        expect(mockClaimService.addHospitalisationDetails).not.toBeCalled();

        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).toBeCalledWith({
          caseId: ABSENCE_ID,
          additionalDescription: 'it was really gross'
        });
        expect(mockAbsenceService.addHospitalisationDetails).toBeCalledWith({
          caseId: ABSENCE_ID,
          ovenightAtHospital: true,
          startDate: '2019-02-13',
          endDate: '2019-02-23'
        });
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });
  });

  describe('Claims User', () => {
    beforeEach(() => {
      givenCustomerUser();
    });

    it('should create a claim for accident flow', done => {
      const fieldValues = new Map<RequestDetailFieldId, any>();
      fieldValues.set(
        RequestDetailFieldId.DESCRIBE_ACCIDENT,
        'broke finger on keyboard'
      );
      fieldValues.set(RequestDetailFieldId.ACCIDENT_DATE, '2019-02-12');
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_WORK_RELATED,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED,
        '2019-02-13'
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_IS_OVERNIGHT_HOSPITAL_STAY,
        RequestDetailFieldId.YES
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ['2019-02-13', '2019-02-23']
      );
      fieldValues.set(
        RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
        'it was really gross'
      );

      const timeOff: TimeOff = {
        outOfWork: true,
        timeOffLeavePeriods: [timeOffLeavePeriods[0]]
      };

      const model = new IntakeSubmission(
        [EventOptionId.ACCIDENT],
        fieldValues,
        timeOff
      );

      // given the claims creation are ok
      mockAbsenceService.registerAbsence = givenPromise({});
      mockClaimService.startClaim = givenPromise({
        notificationCaseId: NOTIFICATION_ID,
        claimId: CLAIM_ID
      });

      givenAllPostCaseRequestsWork();

      testProcessShouldWork(intake, done, model, result => {
        expect(NOTIFICATION_ID).toEqual(result.createdNotificationId);
        expect({}).toEqual(result.createdAbsence);
        expect({ id: CLAIM_ID }).toEqual(result.createdClaim);

        expect(mockAbsenceService.registerAbsence).not.toBeCalled();
        expect(mockClaimService.startClaim).toBeCalledWith(CLAIM_CASE_TYPE, {
          claimIncurredDate: '2019-02-12',
          notificationReason: 'Accident or treatment required for an injury'
        });

        expect(mockClaimService.updateMedicalDetails).toBeCalledWith(CLAIM_ID, {
          condition: 'broke finger on keyboard'
        });
        expect(mockClaimService.updateDiabilityDetails).toBeCalledWith(
          CLAIM_ID,
          {
            eventType: 'Accident',
            claimType: 'STD',
            source: DISABILITY_REQUEST_SOURCE,
            notificationDate: moment().format(API_DATE_FORMAT),
            employeeDateLastWorked: '2018-08-24',
            firstDayMissedWork: '2019-03-01',
            expectedReturnToWorkDate: '2019-04-01',
            workRelated: true,
            claimIncurredDate: '2019-02-12',
            disabilityDateFromCustomer: '2019-02-12',
            accidentDate: '2019-02-12',
            claimAdditionalInfo: 'it was really gross'
          }
        );
        expect(mockClaimService.addHospitalisationDetails).toBeCalledWith(
          CLAIM_ID,
          {
            endDateConfirmed: true,
            startDate: '2019-02-13',
            endDate: '2019-02-23'
          }
        );

        expect(
          mockAdditionalConditionsService.addAdditionalConditions
        ).not.toBeCalled();
        expect(mockAbsenceService.addHospitalisationDetails).not.toBeCalled();
        expect(
          mockClaimService.indicateMultiplePeriodsInAbsence
        ).not.toBeCalled();
      });
    });
  });
});
