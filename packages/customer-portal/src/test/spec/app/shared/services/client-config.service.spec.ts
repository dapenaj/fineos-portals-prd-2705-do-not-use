import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import { ClientConfigService } from '../../../../../app/shared/services';
import { clientConfigFixture } from '../../../../common/fixtures';

describe('Client Config Service', () => {
  let mockAxios: MockAdapter;

  const service = ClientConfigService.getInstance();

  beforeEach(() => {
    mockAxios = new MockAdapter(axios);
  });

  afterEach(() => {
    mockAxios.restore();
  });

  test('should fetch the client configuration', async () => {
    mockAxios
      .onGet('/config/client-config.json')
      .replyOnce(200, clientConfigFixture);

    const result = await service.fetchClientConfig();

    expect(result).toStrictEqual(clientConfigFixture);
  });

  test('handle an error', async () => {
    mockAxios.onGet('/config/client-config.json').replyOnce(404, 'error');

    expect.assertions(1);

    try {
      await service.fetchClientConfig();
    } catch (error) {
      expect(error).toBe('error');
    }
  });
});
