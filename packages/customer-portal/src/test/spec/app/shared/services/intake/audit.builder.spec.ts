import { AssertionError } from 'assert';
import { stripIndents } from 'common-tags';

import { AuditService } from '../../../../../../app/shared/services';
import { Audit, AuditDetails } from '../../../../../../app/shared/types';

describe('AuditService', () => {
  const service = AuditService.getInstance();

  describe('AuditBuilder', () => {
    const thenSectionHasMessages = (
      audit: Audit,
      stepName: string,
      sectionName: string,
      messages: AuditDetails<object>[]
    ) => {
      const step = audit.steps.find(s => s.label === stepName);
      expect(step).toBeDefined();

      if (step) {
        const section = step.values.find(s => s.label === sectionName);
        expect(section).toBeDefined();
        if (section) {
          expect(section.values).toEqual(messages);
        }
      }
    };

    it('Validate can create a full model', () => {
      const builder = service.startAudit();

      // given I have added messages from different sections in
      // different orders
      builder.addMessage('General', 'Header', 'creator', 'Arch Stanton');
      builder.addMessage(
        'Personal details',
        'Event Options',
        'choice',
        'I am sick'
      );
      builder.addMessage(
        'Personal details',
        'Event Options',
        'choice',
        'I need time off'
      );
      builder.addMessage(
        'Personal details',
        'Event Options',
        'choice',
        'I am donating an organ'
      );
      builder.addMessage('General', 'Header', 'Was successful', 'true');
      builder.addMessage(
        'Personal details',
        'Forms',
        'Time Off In Hospital: Date Range',
        ['18/11/1975', '26/08/1976']
      );

      // now, validate the output
      const audit = builder.getAudit();

      thenSectionHasMessages(audit, 'General', 'Header', [
        { label: 'creator', values: 'Arch Stanton' as any },
        { label: 'Was successful', values: 'true' as any }
      ]);

      thenSectionHasMessages(audit, 'Personal details', 'Event Options', [
        { label: 'choice', values: 'I am sick' as any },
        { label: 'choice', values: 'I need time off' as any },
        { label: 'choice', values: 'I am donating an organ' as any }
      ]);

      thenSectionHasMessages(audit, 'Personal details', 'Forms', [
        {
          label: 'Time Off In Hospital: Date Range',
          values: ['18/11/1975', '26/08/1976'] as any
        }
      ]);

      const expected = stripIndents`-------------------------------------
      General : Header
      -------------------------------------
      - creator: Arch Stanton
      - Was successful: true
      -------------------------------------
      Personal details : Event Options
      -------------------------------------
      - choice: I am sick
      - choice: I need time off
      - choice: I am donating an organ
      -------------------------------------
      Personal details : Forms
      -------------------------------------
      - Time Off In Hospital: Date Range: 18/11/1975,26/08/1976`;

      expect(service.buildAuditLog(audit)).toStrictEqual(expected);
    });
  });
});
