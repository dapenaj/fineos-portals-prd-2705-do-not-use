import { saveAs } from 'file-saver';

import { releaseEventLoop } from '../../../../common/utils';
import { ExportService } from '../../../../../app/shared/services';
import { CsvDetails } from '../../../../../app/shared/types';

jest.mock('file-saver', () => ({ saveAs: jest.fn() }));

describe('Export Service', () => {
  const service = ExportService.getInstance();

  window.URL.createObjectURL = jest.fn();

  const rows = [
    [
      'Arch Stanton',
      '2017-08-01',
      'NTN-42',
      'Pregnancy, birth or related medical treatment',
      '2017-08-06',
      '2017-08-18',
      'Bob Dobalina',
      '01 23 45678'
    ],
    [
      'Arch Stanton',
      '2017-08-01',
      'NTN-65',
      'Accident or treatment for injury',
      '2017-08-06',
      '2017-08-18',
      'Beth Sanchez',
      '1-234-765000'
    ]
  ];

  const columns = [
    'Employee',
    'Notified On',
    'Case Id',
    'Reason',
    'First day missing work',
    'Expecetd return',
    'Absence case Handler',
    `Case manager's contact`
  ];

  it('should export a csv file', async () => {
    const spy = jest.spyOn(service, 'exportToCsv');
    const csv: CsvDetails = { columns, rows, filename: 'Test' };

    service.exportToCsv(csv);

    await releaseEventLoop();

    expect(spy).toHaveBeenCalledWith(csv);
  });

  it('should export a binary file', () => {
    const document = {
      fileName: 'test-file',
      fileExtension: 'txt',
      contentType: 'text/plain',
      base64EncodedFileContents: 'VGVzdCBzdHJpbmc=', // 'Test string' in base64
      fileSizeInBytes: 11,
      description: '',
      managedReqId: 1
    };

    service.exportBinary(document, 'test');

    expect(saveAs).toHaveBeenCalledWith(
      new Blob(
        new Uint8Array(
          Array.from('Test string').map(char => char.charCodeAt(0))
        ) as any
      ),
      'test-file.txt'
    );
  });
});
