import {
  NewAbsenceRequest,
  StartClaimRequest,
  CustomerOccupationService,
  AbsenceService,
  ClaimService,
  AbsenceStatus
} from 'fineos-js-api-client';

import {
  absencesFixture,
  absenceReasons
} from '../../../../../../common/fixtures';
import {
  givenAbsenceUser,
  givenCustomerUser,
  promiseTest,
  mockField,
  givenPromise,
  givenRejectedPromise
} from '../../../../../../common/utils';
import {
  IntakeSubmission,
  IntakeSubmissionResult,
  IntakeSubmissionService,
  NotificationService,
  SnapshotService
} from '../../../../../../../app/shared/services';
import {
  AbstractIntake,
  StartClaimRequestDetails
} from '../../../../../../../app/shared/intakes';
import { CLAIM_CASE_TYPE } from '../../../../../../../app/shared/constants';
import { EventOptionId } from '../../../../../../../app/shared/types';
import { WrapUpSubmission } from '../../../../../../../app/modules/intake/wrap-up';

class TestIntake extends AbstractIntake {
  caseRequests: ((
    result: IntakeSubmissionResult,
    submission: IntakeSubmission
  ) => Promise<any>)[] = [];

  private type: EventOptionId;

  constructor(type: EventOptionId) {
    super();
    this.type = type;
  }

  supports(submission: IntakeSubmission): boolean {
    return submission.isEventOptionSelected(this.type);
  }

  protected buildStartAbsenceRequest(
    submission: IntakeSubmission
  ): NewAbsenceRequest {
    return new NewAbsenceRequest();
  }
  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    submission: IntakeSubmission
  ): StartClaimRequestDetails {
    return {
      claimCaseType: CLAIM_CASE_TYPE,
      request: new StartClaimRequest()
    };
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    submission: IntakeSubmission
  ) => Promise<any>)[] {
    return this.caseRequests;
  }
}

const NOTIFICATION_ID = 'CASE123';
const ABSENCE_ID = 'ABSENCE_ID';
const CLAIM_ID = 'CLAIM_ID';

describe('Intake Submission Service', () => {
  // we rebuild these mocks on each test
  let sicknessIntake: TestIntake;
  let pregnancyIntake: TestIntake;
  let service: IntakeSubmissionService;
  let mockAbsenceService: AbsenceService;
  let mockClaimService: ClaimService;
  let mockCustomerOccupationService: CustomerOccupationService;
  let mockNotificationService: NotificationService;
  let mockSnapshotService: SnapshotService;

  const mockCaseRequests = (target: TestIntake, requests: Promise<any>[]) => {
    requests.forEach(a => {
      target.caseRequests.push(() => a);
    });
  };

  const wrapUpSubmission: WrapUpSubmission = {
    additionalDetailsValue: [
      {
        incomeType: 'Sick Pay',
        frequency: 'Weekly',
        amount: 1000,
        startDate: '2019-02-18',
        endDate: '2019-08-18'
      },
      {
        incomeType: 'Sick Pay',
        frequency: 'Monthly',
        amount: 10000,
        startDate: '2019-02-28',
        endDate: '2019-08-28'
      }
    ]
  };

  beforeEach(() => {
    jest.clearAllMocks();

    sicknessIntake = new TestIntake(EventOptionId.SICKNESS);
    pregnancyIntake = new TestIntake(EventOptionId.PREGNANCY);

    service = new IntakeSubmissionService();

    jest.spyOn(sicknessIntake, 'process');
    jest.spyOn(pregnancyIntake, 'process');

    mockAbsenceService = mockField(
      sicknessIntake,
      'absenceService',
      new AbsenceService()
    );
    mockClaimService = mockField(
      sicknessIntake,
      'claimService',
      new ClaimService()
    );

    mockCustomerOccupationService = mockField(
      sicknessIntake,
      'customerOccupationService',
      new CustomerOccupationService()
    );

    // WorkDetailsFormValue
    mockField(pregnancyIntake, 'absenceService', mockAbsenceService);
    mockField(pregnancyIntake, 'claimService', mockClaimService);

    mockSnapshotService = mockField(
      service,
      'snapshotService',
      new SnapshotService()
    );

    mockField(
      service,
      'customerOccupationService',
      mockCustomerOccupationService
    );
    mockField(service, 'availableIntakes', [sicknessIntake, pregnancyIntake]);
    mockNotificationService = mockField(
      service,
      'notificationService',
      new NotificationService()
    );

    // we will assume for all tests that the requests work, we can
    // override this for tests that fail
    // given the absence and claims creation are ok
    mockAbsenceService.registerAbsence = givenPromise({
      notificationCaseId: NOTIFICATION_ID,
      absenceId: ABSENCE_ID
    });
    mockClaimService.startClaim = givenPromise({
      notificationCaseId: NOTIFICATION_ID,
      claimId: CLAIM_ID
    });

    mockCustomerOccupationService.updateWorkDetails = givenPromise({});
    mockNotificationService.completeNotification = givenPromise({});
  });

  describe('Request selection', () => {
    beforeEach(() => {
      givenAbsenceUser();
    });

    it('Check selection should not pick the wrong request', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false
        }
      );

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(pregnancyIntake.process).toBeCalled();
          expect(sicknessIntake.process).not.toBeCalled();
        })
      );
    });

    it('Check selection should not pick the wrong request (different request)', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.SICKNESS],
        new Map(),
        {
          outOfWork: false
        }
      );

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(pregnancyIntake.process).not.toBeCalled();
          expect(sicknessIntake.process).toBeCalled();
        })
      );
    });
  });

  describe('Absence User', () => {
    beforeEach(() => {
      givenAbsenceUser();
    });

    it('Check absence user with no continuous timeoff but absence is medical related should set isMedicalProviderRelated true', done => {
      // mock the absence reasons and absence details API calls
      mockAbsenceService.getAbsenceReasons = givenPromise(absenceReasons);
      mockAbsenceService.getAbsenceDetails = givenPromise(absencesFixture[0]);

      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false
        }
      );

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBeUndefined();
          expect(result.postCaseErrors).toStrictEqual([]);

          expect(result.createdNotificationId).toEqual(NOTIFICATION_ID);
          expect(result.createdAbsence).toEqual({
            id: ABSENCE_ID,
            isMedicalProviderRelated: true
          });
          // i.e. no errors or id - this is ok, we have no continuous times
          expect(result.createdClaim).toEqual({});
        })
      );
    });

    it('Check absence user with no continuous timeoff should not create a claim', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false
        }
      );

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBeUndefined();
          expect(result.postCaseErrors).toStrictEqual([]);

          expect(result.createdNotificationId).toEqual(NOTIFICATION_ID);
          expect(result.createdAbsence).toEqual({
            id: ABSENCE_ID,
            isMedicalProviderRelated: false
          });
          // i.e. no errors or id - this is ok, we have no continuous times
          expect(result.createdClaim).toEqual({});
        })
      );
    });

    it('Check absence user with continuous timeoff should create a claim', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false,
          timeOffLeavePeriods: [
            {
              startDate: '2018-08-25',
              endDate: '2019-02-25',
              lastDayWorked: '2019-02-25',
              expectedReturnToWorkDate: '2019-02-28',
              endDateFullDay: false,
              endDateOffHours: 3,
              endDateOffMinutes: 30,
              status: AbsenceStatus.Known
            }
          ]
        }
      );

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBeUndefined();
          expect(result.postCaseErrors).toStrictEqual([]);

          expect(result.createdNotificationId).toEqual(NOTIFICATION_ID);
          expect(result.createdAbsence).toEqual({
            id: ABSENCE_ID,
            isMedicalProviderRelated: false
          });
          expect(result.createdClaim).toEqual({ id: CLAIM_ID });
        })
      );
    });

    it('Check we attempt to create a claim even if the absence fails', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false,
          timeOffLeavePeriods: [
            {
              startDate: '2018-08-25',
              endDate: '2019-02-25',
              lastDayWorked: '2019-02-25',
              expectedReturnToWorkDate: '2019-02-28',
              endDateFullDay: false,
              endDateOffHours: 3,
              endDateOffMinutes: 30,
              status: AbsenceStatus.Known
            }
          ]
        }
      );
      // given the absence creation failed

      mockAbsenceService.registerAbsence = givenRejectedPromise(
        'we are all doomed'
      );

      // given we have some case requests that worked and some that didn't
      const caseRequest1 = Promise.resolve('I worked');
      const caseRequest2 = Promise.reject('I did not work');

      mockCaseRequests(pregnancyIntake, [caseRequest1, caseRequest2]);

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBe('Absence failed');
          expect(result.postCaseErrors).toStrictEqual(['I did not work']);

          // we should have still gotten the notification id from the claim
          expect(result.createdNotificationId).toEqual(NOTIFICATION_ID);
          expect(result.createdAbsence).toEqual({ error: 'we are all doomed' });
          // i.e. no errors or id - this is ok, we have no continuous times
          expect(result.createdClaim).toEqual({ id: CLAIM_ID });

          // something broke, so we can't close things
          expect(
            mockNotificationService.completeNotification
          ).not.toHaveBeenCalled();
        })
      );
    });

    it('Check post case are called without incident', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false,
          timeOffLeavePeriods: [
            {
              startDate: '2018-08-25',
              endDate: '2019-02-25',
              lastDayWorked: '2019-02-25',
              expectedReturnToWorkDate: '2019-02-28',
              endDateFullDay: false,
              endDateOffHours: 3,
              endDateOffMinutes: 30,
              status: AbsenceStatus.Known
            }
          ]
        },
        {
          employer: 'Arch Stanton',
          jobTitle: 'Moldering',
          dateJobBegan: '1976-08-26'
        }
      );
      // given we have some case requests that worked
      const caseRequest1 = Promise.resolve('I worked');
      const caseRequest2 = Promise.resolve('I also worked');

      mockCaseRequests(pregnancyIntake, [caseRequest1, caseRequest2]);

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBeUndefined();
          expect(result.postCaseErrors).toStrictEqual([]);

          // if there are work details to update, then we submit them to both
          //  claims and cases if there are both
          expect(
            mockCustomerOccupationService.updateWorkDetails
          ).toHaveBeenCalledWith({
            caseId: ABSENCE_ID,
            dateJobBegan: '1976-08-26',
            employer: 'Arch Stanton',
            jobTitle: 'Moldering'
          });

          expect(
            mockCustomerOccupationService.updateWorkDetails
          ).toHaveBeenCalledWith({
            caseId: CLAIM_ID,
            dateJobBegan: '1976-08-26',
            employer: 'Arch Stanton',
            jobTitle: 'Moldering'
          });
        })
      );
    });

    it('Check post case are called with incident', done => {
      // given we have some case requests that worked
      const caseRequest1 = Promise.reject('I broke');
      const caseRequest2 = Promise.reject('I also broke');

      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false
        },
        {
          employer: 'Arch Stanton',
          jobTitle: 'Moldering',
          dateJobBegan: '1976/08/26'
        }
      );

      mockCaseRequests(pregnancyIntake, [caseRequest1, caseRequest2]);

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBeUndefined();
          expect(result.postCaseErrors).toStrictEqual([
            'I broke',
            'I also broke'
          ]);

          expect(
            mockCustomerOccupationService.updateWorkDetails
          ).toHaveBeenCalledWith({
            caseId: 'ABSENCE_ID',
            dateJobBegan: '1976-08-26',
            employer: 'Arch Stanton',
            jobTitle: 'Moldering'
          });
        })
      );
    });
  });

  describe('Customer User', () => {
    beforeEach(() => {
      givenCustomerUser();
    });

    it('Check claims user with continuous timeoff should create a claim, but no absence', done => {
      const submission = new IntakeSubmission(
        [EventOptionId.PREGNANCY],
        new Map(),
        {
          outOfWork: false,
          timeOffLeavePeriods: [
            {
              startDate: '2018-08-25',
              endDate: '2019-02-25',
              lastDayWorked: '2019-02-25',
              expectedReturnToWorkDate: '2019-02-28',
              endDateFullDay: false,
              endDateOffHours: 3,
              endDateOffMinutes: 30,
              status: AbsenceStatus.Known
            }
          ]
        },
        {
          employer: 'Arch Stanton',
          jobTitle: 'Moldering',
          dateJobBegan: '1976-08-26'
        }
      );

      promiseTest(
        done,
        service.process(submission).then(result => {
          expect(result.generalError).toBeUndefined();
          expect(result.postCaseErrors).toStrictEqual([]);
          expect(mockClaimService.startClaim).toHaveBeenCalled();

          expect(result.createdNotificationId).toEqual(NOTIFICATION_ID);
          expect(result.createdAbsence).toEqual({});
          expect(result.createdClaim).toEqual({ id: CLAIM_ID });
        })
      );
    });
  });

  describe('Completing an intake', () => {
    const submission = new IntakeSubmission([], new Map(), {
      outOfWork: false
    });

    it('Complete an intake that was successful', done => {
      mockSnapshotService.submitSnapshot = givenPromise({});
      mockNotificationService.completeNotification = givenPromise({});

      const result = {
        createdNotificationId: 'NOT-123',
        postCaseErrors: [],
        createdAbsence: { id: 'ABS-1' },
        createdClaim: { id: 'CLM-2' },
        isClaimRequired: true,
        isAbsenceRequired: true
      };

      promiseTest(
        done,
        service
          .completeIntake(submission, result, wrapUpSubmission)
          .then(isComplete => {
            expect(isComplete.generalError).toBeUndefined();
            expect(isComplete.postCaseErrors).toStrictEqual([]);

            expect(mockSnapshotService.submitSnapshot).toBeCalled();
            expect(mockNotificationService.completeNotification).toBeCalled();
          })
      );
    });

    it('Complete an intake that was unsuccessful', done => {
      mockSnapshotService.submitSnapshot = givenPromise({});
      mockNotificationService.completeNotification = givenPromise({});

      const result = {
        createdNotificationId: 'NOT-123',
        postCaseErrors: ['oh no!'],
        createdAbsence: { id: 'ABS-1' },
        createdClaim: { id: 'CLM-2' },
        isClaimRequired: true,
        isAbsenceRequired: true
      };

      promiseTest(
        done,
        service
          .completeIntake(submission, result, wrapUpSubmission)
          .then(isComplete => {
            // we should submit a snapshot
            // there will be something here of interest to someone for debugging
            expect(mockSnapshotService.submitSnapshot).toHaveBeenCalled();

            // we should not try to complete it
            expect(
              mockNotificationService.completeNotification
            ).not.toHaveBeenCalled();
          })
      );
    });

    it('Complete an intake that was unsuccessful and has no case id', done => {
      mockSnapshotService.submitSnapshot = givenPromise({});
      mockNotificationService.completeNotification = givenPromise({});

      const result = {
        generalError: 'no case at all',
        postCaseErrors: [],
        createdAbsence: {},
        createdClaim: {},
        isClaimRequired: false,
        isAbsenceRequired: false
      };

      promiseTest(
        done,
        service
          .completeIntake(submission, result, wrapUpSubmission)
          .then(isComplete => {
            // we should not submit a snapshot
            expect(mockSnapshotService.submitSnapshot).not.toHaveBeenCalled();

            // we should not try to complete it
            expect(
              mockNotificationService.completeNotification
            ).not.toHaveBeenCalled();
          })
      );
    });

    it('Complete an intake that was successful, but the snapshot failed', done => {
      givenAbsenceUser();

      mockSnapshotService.submitSnapshot = givenRejectedPromise(
        'cannot create snapshot!'
      );
      mockNotificationService.completeNotification = givenPromise({});

      const result = {
        createdNotificationId: 'NOT-123',
        postCaseErrors: [],
        createdAbsence: { id: 'ABS-1' },
        createdClaim: { id: 'CLM-2' },
        isClaimRequired: false,
        isAbsenceRequired: true
      };

      promiseTest(
        done,
        service
          .completeIntake(submission, result, wrapUpSubmission)
          .then(isComplete => {
            expect(isComplete.generalError).toBeUndefined();
            expect(isComplete.postCaseErrors).toStrictEqual([
              'cannot create snapshot!'
            ]);

            expect(mockSnapshotService.submitSnapshot).toHaveBeenCalled();
            expect(
              mockNotificationService.completeNotification
            ).not.toHaveBeenCalled();
          })
      );
    });

    it('Complete an intake that was successful, but the completion failed', done => {
      mockSnapshotService.submitSnapshot = givenPromise({});
      mockNotificationService.completeNotification = givenRejectedPromise(
        'cannot complete!'
      );

      const result = {
        createdNotificationId: 'NOT-123',
        postCaseErrors: [],
        createdAbsence: { id: 'ABS-1' },
        createdClaim: { id: 'CLM-2' },
        isClaimRequired: true,
        isAbsenceRequired: true
      };

      promiseTest(
        done,
        service
          .completeIntake(submission, result, wrapUpSubmission)
          .then(isComplete => {
            expect(isComplete.generalError).toBeUndefined();
            expect(isComplete.postCaseErrors).toStrictEqual([
              'cannot complete!'
            ]);

            expect(mockSnapshotService.submitSnapshot).toHaveBeenCalled();
            expect(
              mockNotificationService.completeNotification
            ).toHaveBeenCalled();
          })
      );
    });
  });
});
