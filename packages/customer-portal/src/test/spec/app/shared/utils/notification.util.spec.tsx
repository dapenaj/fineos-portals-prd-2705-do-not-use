import { AbsenceSummary } from 'fineos-js-api-client';
import { ClaimSummary } from 'fineos-js-api-client';

import {
  getNotificationStatus,
  getCaseProgress,
  getReasonQualifier
} from '../../../../../app/shared/utils';
import { NotificationStatusResult } from '../../../../../app/shared/types';

const pendingClaims = [{ status: 'Pending' }] as ClaimSummary[];
const pendingAbsences = [{ status: 'Pending' }] as AbsenceSummary[];

const someDecidedClaims = [{ status: 'Decided' }] as ClaimSummary[];

const decidedClaims = [{ status: 'Completion' }] as ClaimSummary[];
const decidedAbsences = [{ status: 'Closed' }] as AbsenceSummary[];

describe('Get Notification Status', () => {
  test('correct status is returned', () => {
    const pendingStatus = getNotificationStatus(pendingClaims, pendingAbsences);
    const someDecidedStatus = getNotificationStatus(
      someDecidedClaims,
      pendingAbsences
    );
    const decidedStatus = getNotificationStatus(decidedClaims, decidedAbsences);

    expect(pendingStatus).toBe('Pending');
    expect(someDecidedStatus).toBe('Some Decided');
    expect(decidedStatus).toBe('Decided');
  });

  test('an empty status is returned', () => {
    expect(getNotificationStatus([{ status: 'test' } as ClaimSummary])).toBe(
      'Undetermined'
    );
  });
});

describe('Get Case Progress', () => {
  test('correct case progress is returned', () => {
    const pendingCaseProgress = getCaseProgress(
      NotificationStatusResult.PENDING
    );
    const someDecidedCaseProgress = getCaseProgress(
      NotificationStatusResult.SOME_DECIDED
    );
    const decidedCaseProgress = getCaseProgress(
      NotificationStatusResult.DECIDED
    );

    expect(pendingCaseProgress).toBe(
      'NOTIFICATIONS.NOTIFICATION_STATUS.PENDING'
    );
    expect(someDecidedCaseProgress).toBe(
      'NOTIFICATIONS.NOTIFICATION_STATUS.SOME_DECIDED'
    );
    expect(decidedCaseProgress).toBe(
      'NOTIFICATIONS.NOTIFICATION_STATUS.DECIDED'
    );
  });

  test('undertermined status is returned', () => {
    expect(getCaseProgress(NotificationStatusResult.UNDETERMINED)).toBe(
      'NOTIFICATIONS.NOTIFICATION_STATUS.UNDETERMINED'
    );
  });

  describe('PeriodDecisionDefaultProperties', () => {
    test('get postpartum reason qualifier', async () => {
      expect(getReasonQualifier('Postnatal Disability')).toEqual(
        ' - Postpartum disability'
      );
    });
    test('get termination reason qualifier', async () => {
      expect(getReasonQualifier('Other')).toEqual(' - Termination');
    });
    test('get default reason qualifier', async () => {
      expect(getReasonQualifier('Birth Disability')).toEqual(
        ' - Birth Disability'
      );
    });
  });
});
