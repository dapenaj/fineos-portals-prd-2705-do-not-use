import { ApiError } from 'fineos-js-api-client';

import {
  getErrorMessageKeyValue,
  retrieveErrorMessages,
  retrieveNotificationType
} from '../../../../../app/shared/utils';

describe('getErrorMessageKeyValue', () => {
  test('should format validation values', () => {
    const result = getErrorMessageKeyValue(
      '',
      { validationMessage: 'test' },
      ''
    );
    expect(result).toEqual({ key: '', value: 'test' });
  });

  test('should format json values', () => {
    const result = getErrorMessageKeyValue('correlationId', '', {
      correlationId: 'this is that'
    });
    expect(result).toEqual({ key: 'correlationId', value: 'this is that' });
  });
});

describe('retrieveErrorMessages', () => {
  test('should return error messages', () => {
    const result = retrieveErrorMessages({
      correlationId: 'this is that'
    });

    expect(result).toEqual([{ key: 'correlationId', value: 'this is that' }]);
  });
});

describe('retrieveNotificationType', () => {
  test('should return ApiError notification types', () => {
    expect(retrieveNotificationType(new ApiError('error', 500), 'info')).toBe(
      'error'
    );
    expect(retrieveNotificationType(new ApiError('error', 400), 'info')).toBe(
      'warning'
    );
    expect(retrieveNotificationType(new ApiError('error', 200), 'info')).toBe(
      'success'
    );
    expect(retrieveNotificationType(new ApiError('error', 300), 'info')).toBe(
      'info'
    );
  });

  test('should return type if content is not ApiError', () => {
    expect(retrieveNotificationType('test', 'warning')).toBe('warning');
  });
});
