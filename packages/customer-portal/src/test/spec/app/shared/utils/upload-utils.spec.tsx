import {
  getFileExtension,
  isValidFileExtension
} from '../../../../../app/shared/utils';

describe('upload-utils', () => {
  test('valid extensions', () => {
    const extension = getFileExtension('test.txt');

    expect(isValidFileExtension(extension)).toBe(true);
  });

  test('invalid extensions', () => {
    const extension = getFileExtension('test.html');

    expect(isValidFileExtension(extension)).toBe(false);
  });
});
