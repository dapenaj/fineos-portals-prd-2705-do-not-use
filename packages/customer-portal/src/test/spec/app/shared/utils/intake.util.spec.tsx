import { Phone, EmailAddress } from 'fineos-js-api-client';

import {
  getCustomerEmail,
  getCustomerPhoneNumber
} from '../../../../../app/shared/utils';

describe('getCustomerEmail', () => {
  test('should return an email or empty string', () => {
    expect(
      getCustomerEmail([
        { emailAddress: 'test', preferred: true }
      ] as EmailAddress[])
    ).toBe('test');

    expect(
      getCustomerEmail([
        { emailAddress: 'test', preferred: false }
      ] as EmailAddress[])
    ).toBe('test');

    expect(getCustomerEmail([] as EmailAddress[])).toBe('');
  });
});

describe('getCustomerPhoneNumber', () => {
  test('should return a phone or empty string', () => {
    expect(
      getCustomerPhoneNumber([
        { areaCode: '0', intCode: '0', telephoneNo: '0', preferred: true }
      ] as Phone[])
    ).toStrictEqual({
      areaCode: '0',
      intCode: '0',
      telephoneNo: '0',
      preferred: true
    });

    expect(
      getCustomerPhoneNumber([
        { areaCode: '0', intCode: '0', telephoneNo: '0', preferred: false }
      ] as Phone[])
    ).toStrictEqual({
      areaCode: '0',
      intCode: '0',
      telephoneNo: '0',
      preferred: false
    });

    expect(getCustomerPhoneNumber([] as Phone[])).toBe(undefined);
  });
});
