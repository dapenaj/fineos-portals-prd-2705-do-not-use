import React from 'react';
import { Phone } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';

import {
  formatPhoneToString,
  formatStringToPhoneNumber,
  formatValidationMessage,
  formatDateForDisplay,
  formatDateForApi,
  formatInitial
} from '../../../../../app/shared/utils';

describe('formatPhoneToString', () => {
  test('should format', () => {
    expect(
      formatPhoneToString({
        intCode: '0',
        areaCode: '1',
        telephoneNo: '2'
      } as Phone)
    ).toBe('0-1-2');
  });
});

describe('formatStringToPhoneNumber', () => {
  test('should format', () => {
    expect(formatStringToPhoneNumber('0-1-2')).toStrictEqual({
      intCode: '0',
      areaCode: '1',
      telephoneNo: '2'
    });
  });
});

describe('formatValidationMessage', () => {
  test('should format', () => {
    expect(formatValidationMessage('NAVBAR_LINKS.HOME')).toStrictEqual(
      <FormattedMessage id="NAVBAR_LINKS.HOME" values={{}} />
    );
  });
});

describe('formatDateForDisplay', () => {
  test('should format', () => {
    expect(formatDateForDisplay('2017-06-06')).toBe('06-06-2017');
  });

  test('should not format', () => {
    expect(formatDateForDisplay('')).toBe('');
  });
});

describe('formatDateForApi', () => {
  test('should format', () => {
    expect(formatDateForApi('2017-06-06')).toBe('2017-06-06');
  });

  describe('formatInitials', () => {
    test('should format', () => {
      expect(formatInitial('!Johnson')).toBe('J');
    });
  });
});
