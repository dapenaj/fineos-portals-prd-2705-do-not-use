import React from 'react';

import { shallow } from '../../../../config';
import { ContentLayout } from '../../../../../app/shared/layouts';

describe('ContentLayout', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<ContentLayout />)).toMatchSnapshot();
  });
});
