import React from 'react';

import { shallow } from '../../../../config';
import { Initials } from '../../../../../app/shared/ui/initials';
import { customerFixture } from '../../../../common/fixtures';

describe('Initials', () => {
  const props = { customer: customerFixture };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<Initials {...props} />)).toMatchSnapshot();
  });
});
