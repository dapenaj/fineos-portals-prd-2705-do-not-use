import React from 'react';

import { shallow } from '../../../../config';
import { Dropdown } from '../../../../../app/shared/ui/header/dropdown';
import { customerFixture } from '../../../../common/fixtures';

describe('Dropdown', () => {
  const props = {
    links: [
      { path: '/', name: 'HEADER.MY_PROFILE' },
      { path: '/', name: 'HEADER.HEADER.LOGOUT' }
    ],
    customer: customerFixture
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<Dropdown {...props} />)).toMatchSnapshot();
  });
});
