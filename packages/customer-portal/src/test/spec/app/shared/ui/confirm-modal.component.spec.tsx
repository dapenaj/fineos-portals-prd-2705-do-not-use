import React from 'react';

import { shallow } from '../../../../config';
import { ConfirmModal } from '../../../../../app/shared/ui';

describe('ConfirmModal', () => {
  const props = {
    visible: true,
    handleConfirm: jest.fn(),
    handleCancel: jest.fn(),
    message: 'The body text'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<ConfirmModal {...props} />)).toMatchSnapshot();
  });
});
