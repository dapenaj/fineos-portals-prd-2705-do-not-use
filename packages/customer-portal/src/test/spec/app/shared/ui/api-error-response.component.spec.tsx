import React from 'react';
import { ApiError } from 'fineos-js-api-client';

import { shallow } from '../../../../config';
import { ApiErrorResponse } from '../../../../../app/shared/ui';

describe('ApiErrorResponse', () => {
  const error = new ApiError('error', 500);
  error.message = 'test';

  test('render -- SNAPSHOT', () => {
    expect(shallow(<ApiErrorResponse {...{ error }} />)).toMatchSnapshot();
  });

  const json = new ApiError('error', 500);
  json.json = {
    error: 'oh noes!',
    correlationId: 'this is that',
    errorDetail: 'it broke',
    stacktrace: 'a series of unfortunate events'
  };
  json.error = 'error';
  json.correlationId = 'error';
  json.errorDetail = 'error';

  test('render - json -- SNAPSHOT', () => {
    expect(
      shallow(<ApiErrorResponse {...{ error: json }} />)
    ).toMatchSnapshot();
  });

  const validation = new ApiError('error', 422);
  validation.json = [
    { validationMessage: 'Absence reason is invalid.' },
    { validationMessage: 'incorrect text message.' }
  ];
  validation.error = 'error';
  validation.correlationId = 'error';
  validation.errorDetail = 'error';

  test('render - validation -- SNAPSHOT', () => {
    expect(
      shallow(<ApiErrorResponse {...{ error: validation }} />)
    ).toMatchSnapshot();
  });
});
