import React from 'react';

import { shallow } from '../../../../config';
import { Spinner } from '../../../../../app/shared/ui';

describe('Spinner', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<Spinner />)).toMatchSnapshot();
  });
});
