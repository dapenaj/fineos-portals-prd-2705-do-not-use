import React from 'react';

import { shallow } from '../../../../config';
import { Alert } from '../../../../../app/shared/ui';
import { AntType } from '../../../../../app/shared/types';

describe('Alert', () => {
  const props = {
    message: 'ERROR.ERROR_TEXT',
    type: 'info' as AntType,
    description: 'ERROR.ERROR_EXAMPLE'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<Alert {...props} />)).toMatchSnapshot();
  });
});
