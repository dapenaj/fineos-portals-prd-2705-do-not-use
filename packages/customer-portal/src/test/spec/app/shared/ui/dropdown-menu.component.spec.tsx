import React from 'react';
import { Button } from 'antd';

import Enzyme from '../../../../config/enzyme';
import { shallow, DefaultContext, mount } from '../../../../config';
import { DropdownMenu } from '../../../../../app/shared/ui/header/dropdown/dropdown-menu';
import { AuthenticationService } from '../../../../../app/shared/services';

describe('DropdownMenu', () => {
  const props = {
    links: [
      { path: '/', name: 'HEADER.MY_PROFILE' },
      { path: '/', name: 'HEADER.LOGOUT' }
    ]
  };

  const mountDropdownMenu = () => {
    return mount(
      <DefaultContext>
        <DropdownMenu />
      </DefaultContext>
    );
  };

  const service: AuthenticationService = AuthenticationService.getInstance();

  // describe('logout', () => {
  //   let wrapper: Enzyme.ReactWrapper;

  //   beforeEach(() => {
  //     wrapper = mountDropdownMenu();
  //   });

  //   test('should log the user out', async () => {
  //     const spy = jest.spyOn(service, 'logout');

  //     wrapper
  //       .find(Button)
  //       .at(1)
  //       .simulate('click');

  //     expect(spy).toBeCalled();
  //   });
  // });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<DropdownMenu />)).toMatchSnapshot();
  });
});
