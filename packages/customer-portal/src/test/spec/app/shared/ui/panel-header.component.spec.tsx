import React from 'react';

import { shallow } from '../../../../config';
import { PanelHeader } from '../../../../../app/shared/ui/panel/panel-header/panel-header.component';

describe('PanelHeader', () => {
  const props = {
    badgeDot: true,
    iconType: 'unordered-list',
    title: 'the title',
    notification: 'Unread Text'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PanelHeader {...props} />)).toMatchSnapshot();
  });
});
