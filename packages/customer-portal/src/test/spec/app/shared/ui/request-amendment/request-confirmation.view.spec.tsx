import React from 'react';

import { shallow } from '../../../../../config';
import { RequestConfirmationView } from '../../../../../../app/shared/ui';

describe('RequestConfirmationView', () => {
  const props = {
    isEdit: 'REQUEST_AMENDMENT',
    onClose: () => jest.fn(),
    messageText: 'delete request'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<RequestConfirmationView {...props} />)).toMatchSnapshot();
  });
});
