import React from 'react';

import { shallow } from '../../../../../config';
import { RequiredFromMeHeader } from '../../../../../../app/shared/ui/required-from-me-panel';
import { supportingEvidenceFixture } from '../../../../../common/fixtures';

describe('RequiredFromMeHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RequiredFromMeHeader
          requiredActionsCount={supportingEvidenceFixture.length}
        />
      )
    ).toMatchSnapshot();
  });
});
