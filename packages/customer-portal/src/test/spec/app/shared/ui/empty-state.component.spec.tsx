import React from 'react';

import { shallow } from '../../../../config';
import { EmptyState } from '../../../../../app/shared/ui';

describe('EmptyState', () => {
  const props = {
    children: 'I am a child'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<EmptyState {...props} />)).toMatchSnapshot();
  });
});
