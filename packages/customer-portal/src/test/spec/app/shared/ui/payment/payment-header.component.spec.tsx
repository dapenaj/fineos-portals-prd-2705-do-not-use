import React from 'react';

import { shallow } from '../../../../../config';
import { paymentFixture } from '../../../../../common/fixtures';
import { PaymentHeader } from '../../../../../../app/shared/ui';

describe('PaymentHeader ', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<PaymentHeader payment={paymentFixture[0]} />)
    ).toMatchSnapshot();
  });
});
