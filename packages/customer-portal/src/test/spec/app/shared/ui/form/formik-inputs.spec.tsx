import React from 'react';
import { ReactWrapper, mount } from 'enzyme';
import { Formik, Form } from 'formik';

import { DefaultContext } from '../../../../../config';
import { releaseEventLoop } from '../../../../../common/utils';
import {
  RequestDetailFieldId,
  SelectOption
} from '../../../../../../app/shared/types';
import {
  FormikTextInput,
  FormLabel,
  FormikNumberInput,
  FormikTextAreaInput,
  FormikDatePickerInput,
  FormikDateRangeInput,
  FormikRadioGroupInput,
  FormikHoursInput,
  FormikMinutesInput,
  FormikSelectInput,
  FormikMoneyInput
} from '../../../../../../app/shared/ui';

describe('formik-fields', () => {
  const defaultFieldId = RequestDetailFieldId.PREGNANCY_DELIVERY;
  const defaultProps = {
    name: defaultFieldId,
    isDisabled: true,
    label: (
      <FormLabel label={`INTAKE.DETAILS.REQUEST_DETAILS.${defaultFieldId}`} />
    ),
    validate: (value: any) => {
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
      return undefined;
    }
  };

  describe('FormikTextInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <Form>
                <FormikTextInput {...defaultProps} />
              </Form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-text-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-text-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikNumberInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikNumberInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-number-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-number-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikTextAreaInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikTextAreaInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-textarea-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-textarea-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikDatePickerInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikDatePickerInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-date-picker-input"]')
      ).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-date-picker-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikDateRangeInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikDateRangeInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-date-range-input"]')
      ).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-date-range-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikRadioGroupInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikRadioGroupInput
                  {...defaultProps}
                  optionPrefix="INTAKE.DETAILS.REQUEST_DETAILS."
                  radioOptions={[
                    RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL,
                    RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION,
                    RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN,
                    RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
                  ]}
                  // onChange={e => this.handleRadioGroupInputChange(e.target.value)}
                />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikHoursInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikHoursInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-hours-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-hours-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikMinutesInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikMinutesInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-minutes-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-minutes-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikSelectInput', () => {
    let wrapper: ReactWrapper;
    const selectedLeavePlanNames: SelectOption[] = [
      {
        value: '1',
        text: '1'
      },
      {
        value: '2',
        text: '2'
      },
      {
        value: '3',
        text: '3'
      }
    ];

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            <form>
              <FormikSelectInput
                {...defaultProps}
                options={selectedLeavePlanNames}
              />
            </form>
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-select-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-select-input"] .has-error')
      ).toExist();
    });
  });

  describe('FormikMoneyInput', () => {
    let wrapper: ReactWrapper;

    const mountField = (props?: any) => {
      return mount(
        <DefaultContext>
          <Formik {...props} initialValues={{}} onSubmit={jest.fn()}>
            {() => (
              <form>
                <FormikMoneyInput {...defaultProps} />
              </form>
            )}
          </Formik>
        </DefaultContext>
      );
    };

    test('should render', async () => {
      wrapper = mountField();
      await releaseEventLoop();

      expect(wrapper.find('[data-test-el="formik-number-input"]')).toExist();
    });

    test('should render an error', async () => {
      wrapper = mountField({
        initialTouched: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' },
        initialErrors: { PREGNANCY_DELIVERY: 'COMMON.VALIDATION.REQUIRED' }
      });
      await releaseEventLoop();

      expect(
        wrapper.find('[data-test-el="formik-number-input"] .has-error')
      ).toExist();
    });
  });
});
