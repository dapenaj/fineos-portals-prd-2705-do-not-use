import React from 'react';
import { mount, ReactWrapper } from 'enzyme';

import { DefaultContext } from '../../../../../config';
import {
  NewMessageModalForm,
  ConfirmModal,
  NewMessageModalFormViewComponent
} from '../../../../../../app/shared/ui';
import {
  notificationsFixture,
  entitlementOptionsfixture
} from '../../../../../common/fixtures';
import { releaseEventLoop } from '../../../../../common/utils';
import { fetchNotificationEntitlementsSpy } from '../../../../../common/spys';

describe('New Message Modal Form', () => {
  let wrapper: ReactWrapper;

  const mountNewMessageModalForm = (props: any) => {
    return mount(
      <DefaultContext>
        <NewMessageModalForm {...props} />
      </DefaultContext>
    );
  };

  describe('New Message Form - all', () => {
    beforeEach(() => {
      wrapper = mountNewMessageModalForm({
        type: 'all',
        visible: true,
        notifications: notificationsFixture,
        onCancel: jest.fn()
      });
    });

    const showConfirmation = () => {
      wrapper
        .find('[data-test-el="modal-footer-cancel-btn"]')
        .hostNodes()
        .simulate('click');
    };

    test('should render', () => {
      expect(wrapper.find(NewMessageModalForm)).toExist();
    });

    test('handle relates to change', async () => {
      fetchNotificationEntitlementsSpy('success');

      const instance = wrapper
        .find(NewMessageModalFormViewComponent)
        .instance() as NewMessageModalFormViewComponent;
      const spy = jest.spyOn(
        instance.notificationService,
        'fetchNotificationEntitlements'
      );

      instance.handleRelatesToChange('NTN-21');

      await releaseEventLoop();

      expect(spy).toHaveBeenCalledWith(notificationsFixture[0]);
    });

    test('confirmation - submit', () => {
      showConfirmation();

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(true);

      wrapper
        .find('[data-test-el="confirmation-submit-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(false);
    });

    test('confirmation - cancel', () => {
      showConfirmation();

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(true);

      wrapper
        .find('[data-test-el="confirmation-cancel-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(false);
    });
  });

  describe('New Message Form - recent', () => {
    beforeEach(() => {
      wrapper = mountNewMessageModalForm({
        type: 'recent',
        visible: true,
        relatesToOptions: [{ text: 'NTN-21', value: 'NTN-21' }],
        entitlements: { 'NTN-21': entitlementOptionsfixture },
        notification: notificationsFixture[0]
      });
    });

    test('should render', async () => {
      fetchNotificationEntitlementsSpy('success');

      await releaseEventLoop();

      expect(wrapper.find(NewMessageModalForm)).toExist();
    });
  });
});
