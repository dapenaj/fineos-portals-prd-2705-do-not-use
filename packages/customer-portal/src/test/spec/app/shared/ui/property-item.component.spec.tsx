import React from 'react';

import { shallow } from '../../../../config';
import { PropertyItem } from '../../../../../app/shared/ui';

describe('PropertyItem', () => {
  const props = {
    label: 'description',
    value: <p>value</p>
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PropertyItem {...props} />)).toMatchSnapshot();
  });
});
