import React from 'react';

import { shallow } from '../../../../../config';
import { paymentFixture } from '../../../../../common/fixtures';
import { Payment } from '../../../../../../app/shared/ui';

describe('Payment ', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<Payment payment={paymentFixture[0]} />)).toMatchSnapshot();
  });
});
