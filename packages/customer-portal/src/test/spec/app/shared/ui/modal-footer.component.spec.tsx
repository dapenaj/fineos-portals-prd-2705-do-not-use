import React from 'react';

import { shallow } from '../../../../config';
import { ModalFooter } from '../../../../../app/shared/ui';

describe('ModalFooter', () => {
  const props = {
    onActionClick: jest.fn(),
    isDisabled: true,
    actionText: 'ok',
    onCancelClick: jest.fn(),
    cancelText: 'cancel'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<ModalFooter {...props} />)).toMatchSnapshot();
  });
});
