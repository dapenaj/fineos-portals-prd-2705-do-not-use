import React from 'react';

import { shallow } from '../../../../../config';
import { paymentFixture } from '../../../../../common/fixtures';
import { PaymentDetails } from '../../../../../../app/shared/ui';

describe('PaymentDetails ', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<PaymentDetails payment={paymentFixture[0]} />)
    ).toMatchSnapshot();
  });
});
