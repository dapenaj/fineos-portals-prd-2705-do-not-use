import React from 'react';

import { shallow } from '../../../../../config';
import { RequiredFromMeDocumentsList } from '../../../../../../app/shared/ui/required-from-me-panel';
import { uploadDocumentsFixture } from '../../../../../common/fixtures';

describe('RequiredFromMeDocumentsList', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RequiredFromMeDocumentsList
          documents={uploadDocumentsFixture}
          additionalCaseText={true}
          onRefreshRequiredFromMe={jest.fn()}
        />
      )
    ).toMatchSnapshot();
  });
});
