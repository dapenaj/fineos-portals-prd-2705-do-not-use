import React from 'react';

import { shallow } from '../../../../../config';
import { paymentLinesFixture } from '../../../../../common/fixtures';
import { PaymentBreakdownDetails } from '../../../../../../app/shared/ui';

describe('MyPaymentsView', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <PaymentBreakdownDetails
          payeeName="sean"
          paymentLine={paymentLinesFixture[0]}
        />
      )
    ).toMatchSnapshot();
  });

  test('render - expanded -- SNAPSHOT', () => {
    const wrapper = shallow(
      <PaymentBreakdownDetails
        payeeName="sean"
        paymentLine={paymentLinesFixture[0]}
      />
    );

    wrapper
      .find('[data-test-el="payment-line-btn"]')
      .first()
      .simulate('click');
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
  });
});
