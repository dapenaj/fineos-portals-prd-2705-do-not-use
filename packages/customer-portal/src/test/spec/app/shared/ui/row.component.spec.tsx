import React from 'react';

import { shallow } from '../../../../config';
import { Row } from '../../../../../app/shared/ui';

describe('Row', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<Row />)).toMatchSnapshot();
  });
});
