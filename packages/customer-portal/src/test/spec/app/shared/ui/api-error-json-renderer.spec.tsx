import React from 'react';

import { shallow } from '../../../../config';
import { ApiErrorJsonRenderer } from '../../../../../app/shared/ui';
import { KeyValue } from '../../../../../app/shared/types';

const errors: KeyValue[] = [
  { key: '', value: 'test' },
  { key: 'test', value: 'test' }
];

describe('ApiErrorJsonRenderer', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<ApiErrorJsonRenderer {...{ errors }} />)).toMatchSnapshot();
  });
});
