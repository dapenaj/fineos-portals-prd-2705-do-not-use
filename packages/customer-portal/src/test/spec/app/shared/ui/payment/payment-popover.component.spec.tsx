import React from 'react';

import { shallow } from '../../../../../config';
import { PaymentPopover } from '../../../../../../app/shared/ui';

describe('PaymentPopover ', () => {
  test('render - check -- SNAPSHOT', () => {
    const props = {
      nominatedPayeeName: 'John Snow',
      chequePaymentInfo: {
        chequeNumber: '213212'
      }
    };
    expect(shallow(<PaymentPopover {...props} />)).toMatchSnapshot();
  });
  test('render - eft -- SNAPSHOT', () => {
    const props = {
      nominatedPayeeName: 'John Snow',
      accountTransferInfo: {
        bankInstituteName: 'Iron Bank',
        bankAccountNumber: '099878787'
      }
    };
    expect(shallow(<PaymentPopover {...props} />)).toMatchSnapshot();
  });
  test('render - no payee -- SNAPSHOT', () => {
    const props = {
      chequePaymentInfo: {
        chequeNumber: '213212'
      }
    };
    expect(shallow(<PaymentPopover {...props} />)).toMatchSnapshot();
  });
});
