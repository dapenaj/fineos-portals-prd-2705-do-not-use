import React from 'react';

import { shallow } from '../../../../config';
import { InlinePropertyItem } from '../../../../../app/shared/ui';

describe('InlinePropertyItem', () => {
  const props = {
    label: 'description',
    value: <p>value</p>
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<InlinePropertyItem {...props} />)).toMatchSnapshot();
  });
});
