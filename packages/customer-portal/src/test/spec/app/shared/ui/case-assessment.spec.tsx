import React from 'react';

import { shallow } from '../../../../config';
import { CaseAssessment } from '../../../../../app/shared/ui';

describe('PanelHeader', () => {
  const props = {
    claims: [],
    absences: []
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<CaseAssessment {...props} />)).toMatchSnapshot();
  });
});
