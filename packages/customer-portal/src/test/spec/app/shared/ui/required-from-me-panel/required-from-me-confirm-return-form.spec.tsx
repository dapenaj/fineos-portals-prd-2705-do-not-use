import React from 'react';
import moment from 'moment';
import { Modal } from 'antd';

import { DefaultContext, mount } from '../../../../../config';
import { RequiredFromMeConfirmReturnForm } from '../../../../../../app/shared/ui/required-from-me-panel/required-from-me-confirm-return-form';
import { RequiredFromMeConfirmReturnFormOwnProps } from '../../../../../../app/shared/ui/required-from-me-panel/required-from-me-confirm-return-form/required-from-me-confirm-return-form.container';
import {
  confirmReturnForm,
  confirmReturnFormWithoutExpectedDate
} from '../../../../../common/fixtures';
import {
  changeDatePicker,
  changeRadioButtonInputValue,
  releaseEventLoop
} from '../../../../../common/utils';
import { FinishScreen } from '../../../../../../app/shared/ui/required-from-me-panel/required-from-me-confirm-return-form/finish-screen';
import { ModalFooter } from '../../../../../../app/shared/ui';

describe('RequiredFromMeConfirmReturnForm', () => {
  let onSaveMock: jest.Mock;
  let onCloseMock: jest.Mock;
  let afterCloseMock: jest.Mock;

  const clickEvent = () =>
    new MouseEvent('click') as React.MouseEvent<HTMLElement>;

  const mountRequiredFromMeConfirmReturnForm = (
    props?: Partial<RequiredFromMeConfirmReturnFormOwnProps>
  ) => {
    onSaveMock = jest.fn().mockReturnValue(Promise.resolve());
    onCloseMock = jest.fn();
    afterCloseMock = jest.fn();

    return mount(
      <DefaultContext>
        <RequiredFromMeConfirmReturnForm
          showModal={true}
          onClose={onCloseMock}
          afterClose={afterCloseMock}
          confirmReturn={confirmReturnForm}
          onSave={onSaveMock}
          {...props}
        />
      </DefaultContext>
    );
  };

  test('trigger modal closing', () => {
    const wrapper = mountRequiredFromMeConfirmReturnForm();
    wrapper.find(Modal).prop('onCancel')!(clickEvent());
    expect(onCloseMock).toHaveBeenCalled();

    wrapper.find(ModalFooter).prop('onCancelClick')();

    expect(onCloseMock).toHaveBeenCalled();
  });

  describe('confirm expected return date', () => {
    test('ok button is disabled till confirm expected return date', async () => {
      const wrapper = mountRequiredFromMeConfirmReturnForm();
      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(true);

      changeRadioButtonInputValue(wrapper, 'returningAsExpected', true);

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(false);
    });

    test('show finish screen if saved', async () => {
      const wrapper = mountRequiredFromMeConfirmReturnForm();

      changeRadioButtonInputValue(wrapper, 'returningAsExpected', true);

      await releaseEventLoop();
      wrapper.update();

      expect(onSaveMock).not.toHaveBeenCalled();
      expect(wrapper.find(FinishScreen)).not.toExist();

      wrapper.find(ModalFooter).prop('onSubmitClick')();

      await releaseEventLoop();

      wrapper.setProps({
        children: (
          <RequiredFromMeConfirmReturnForm
            {...wrapper.find(RequiredFromMeConfirmReturnForm).props()}
            confirmReturn={{
              ...wrapper
                .find(RequiredFromMeConfirmReturnForm)
                .prop('confirmReturn'),
              returningAsExpected: true,
              employeeConfirmed: true
            }}
          />
        )
      });

      wrapper.update();

      expect(onSaveMock).toHaveBeenCalled();
      expect(wrapper.find(FinishScreen)).toExist();
    });
  });

  describe('confirm another return date', () => {
    test('ok button is disabled till all required data provided', async () => {
      const wrapper = mountRequiredFromMeConfirmReturnForm();

      changeRadioButtonInputValue(wrapper, 'returningAsExpected', false);

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(true);

      changeDatePicker(wrapper, 'actualRTWDate', '2019-08-01');

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(true);

      changeRadioButtonInputValue(wrapper, 'employerNotified', true);

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(false);
    });

    test('show finish screen if saved', async () => {
      const wrapper = mountRequiredFromMeConfirmReturnForm();

      changeRadioButtonInputValue(wrapper, 'returningAsExpected', false);
      changeDatePicker(wrapper, 'actualRTWDate', '2019-08-01');
      changeRadioButtonInputValue(wrapper, 'employerNotified', true);

      wrapper.find(ModalFooter).prop('onSubmitClick')();

      await releaseEventLoop();

      wrapper.setProps({
        children: (
          <RequiredFromMeConfirmReturnForm
            {...wrapper.find(RequiredFromMeConfirmReturnForm).props()}
            confirmReturn={{
              ...wrapper
                .find(RequiredFromMeConfirmReturnForm)
                .prop('confirmReturn'),
              returningAsExpected: false,
              actualRTWDate: moment('2019-08-01'),
              employeeConfirmed: true
            }}
          />
        )
      });

      wrapper.update();

      expect(onSaveMock).toHaveBeenCalled();
      expect(wrapper.find(FinishScreen)).toExist();
    });

    test('ok button is disabled till actualRTWDate provided', async () => {
      const wrapper = mountRequiredFromMeConfirmReturnForm({
        confirmReturn: confirmReturnFormWithoutExpectedDate
      });

      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(true);
      changeDatePicker(wrapper, 'actualRTWDate', '2019-08-01');

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(ModalFooter).prop('isDisabled')).toEqual(false);
    });
  });
});
