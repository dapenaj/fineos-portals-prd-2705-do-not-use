import React from 'react';
import { FormattedMessage } from 'react-intl';

import { shallow } from '../../../../config';
import { PageTitle } from '../../../../../app/shared/ui';

describe('PageTitle', () => {
  const props = { children: <FormattedMessage id="HOME.HERO_ACTION" /> };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PageTitle {...props} />)).toMatchSnapshot();
  });
});
