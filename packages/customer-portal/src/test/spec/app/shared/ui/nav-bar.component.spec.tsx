import React from 'react';

import { DefaultContext, mount } from '../../../../config';
import { NavBar } from '../../../../../app/shared/ui';
import { releaseEventLoop } from '../../../../common/utils';

describe('NavBar', () => {
  const props = {
    loading: false,
    fetchMessages: jest.fn(),
    messages: null
  };

  const mountNavBar = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <NavBar {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render', async () => {
    const wrapper = mountNavBar();
    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(NavBar)).toExist();
  });
});
