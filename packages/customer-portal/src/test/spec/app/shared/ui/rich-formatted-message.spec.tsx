import React from 'react';

import { shallow } from '../../../../config';
import { RichFormattedMessage } from '../../../../../app/shared/ui/intl';

describe('RichFormattedMessage', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RichFormattedMessage
          id={'INTAKE.DETAILS.REQUEST_DETAILS_TOOLTIP.IS_SICKNESS_CHRONIC'}
          values={{ p: () => 'test' }}
        />
      )
    ).toMatchSnapshot();
  });
});
