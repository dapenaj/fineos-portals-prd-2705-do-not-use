import React from 'react';

import { DefaultContext, mount } from '../../../../config';
import { Header } from '../../../../../app/shared/ui';
import { appConfigFixture } from '../../../../common/fixtures';

describe('Header', () => {
  const props = {
    config: appConfigFixture
  };

  const mountHeader = () =>
    mount(
      <DefaultContext>
        <Header {...props} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountHeader();

    wrapper.update();

    expect(wrapper.find(Header)).toExist();
  });
});
