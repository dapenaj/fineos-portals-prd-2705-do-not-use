import React from 'react';

import { DefaultContext, mount } from '../../../../../config';
import { paymentFixture } from '../../../../../common/fixtures';
import { findPaymentLinesSpy } from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';
import { Spinner, PaymentBreakdown } from '../../../../../../app/shared/ui';

describe('PaymentBreakDown', () => {
  const props = {
    payment: paymentFixture[0]
  };

  const mountPaymentBreakdown = () =>
    mount(
      <DefaultContext>
        <PaymentBreakdown {...props} />
      </DefaultContext>
    );

  test('render loading', async () => {
    findPaymentLinesSpy('success');

    const wrapper = mountPaymentBreakdown();

    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
  });

  test('render success', async () => {
    findPaymentLinesSpy('success');

    const wrapper = mountPaymentBreakdown();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Spinner)).not.toExist();
    expect(wrapper.find('[data-test-el="show-payment-breakdown"]')).toExist();
  });

  test('should expand to reveal details', async () => {
    findPaymentLinesSpy('success');

    const wrapper = mountPaymentBreakdown();

    await releaseEventLoop();
    wrapper.update();

    expect(
      wrapper.find('[data-test-el="payment-breakdown-details"]')
    ).not.toExist();

    wrapper
      .find('[data-test-el="show-payment-breakdown"]')
      .first()
      .simulate('click');

    await releaseEventLoop();
    wrapper.update();

    expect(
      wrapper.find('[data-test-el="payment-breakdown-details"]')
    ).toExist();
  });

  test('render failure', async () => {
    findPaymentLinesSpy('failure');

    const wrapper = mountPaymentBreakdown();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Spinner)).not.toExist();
    expect(
      wrapper.find('[data-test-el="show-payment-breakdown"]')
    ).not.toExist();
  });
});
