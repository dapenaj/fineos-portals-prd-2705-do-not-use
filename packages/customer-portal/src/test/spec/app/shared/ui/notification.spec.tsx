import React from 'react';
import { notification } from 'antd';
import { ApiError } from 'fineos-js-api-client';

import {
  showNotification,
  ApiErrorResponse
} from '../../../../../app/shared/ui';
import { NotificationAlert } from '../../../../../app/shared/types';

describe('showNotification', () => {
  test('render', () => {
    const spy = jest.spyOn(notification, 'success');

    const alert: NotificationAlert = {
      type: 'success',
      title: 'test',
      content: 'test'
    };

    showNotification(alert);

    expect(spy).toHaveBeenCalledWith({
      message: alert.title,
      description: alert.content,
      duration: 5,
      placement: 'topRight'
    });
  });

  test('render - ApiError', () => {
    const apiSpy = jest.spyOn(notification, 'error');

    const apiAlert: NotificationAlert = {
      type: 'error',
      title: 'test',
      content: new ApiError('test', 500),
      duration: 3
    };

    showNotification(apiAlert);

    expect(apiSpy).toHaveBeenCalledWith({
      message: apiAlert.title,
      description: (
        <ApiErrorResponse
          error={
            {
              message: 'test',
              status: 500
            } as ApiError
          }
        />
      ),
      placement: 'topRight',
      duration: 3
    });
  });
});
