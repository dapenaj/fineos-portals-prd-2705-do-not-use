import React from 'react';

import { shallow } from '../../../../config';
import { ViewLoading } from '../../../../../app/shared/ui';

describe('ViewLoading', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<ViewLoading />)).toMatchSnapshot();
  });
});
