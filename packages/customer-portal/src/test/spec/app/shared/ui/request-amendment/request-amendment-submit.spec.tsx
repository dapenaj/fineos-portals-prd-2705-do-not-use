import { AbsenceService } from 'fineos-js-api-client';

import { getAmendmentRequest } from '../../../../../../app/shared/ui';
import {
  TimeOffAmendmentType,
  AdditionalTimeOffAmendment,
  ReducedTimeOffAmendment,
  MovedTimeOffAmendment,
  AbsenceAmendment
} from '../../../../../../app/shared/types';

describe('Request Amendment Submit', () => {
  const service = AbsenceService.getInstance();

  const additionalTimeOffForContinuousPeriodSpy = jest
    .spyOn(service, 'additionalTimeOffForContinuousPeriod')
    .mockReturnValue(Promise.resolve(1));

  const additionalTimeOffForReducedPeriodSpy = jest
    .spyOn(service, 'additionalTimeOffForReducedPeriod')
    .mockReturnValue(Promise.resolve(2));

  const reduceTimeOffForContinuousPeriodSpy = jest
    .spyOn(service, 'reduceTimeOffForContinuousPeriod')
    .mockReturnValue(Promise.resolve(3));

  const reduceTimeOffForReducedPeriodSpy = jest
    .spyOn(service, 'reduceTimeOffForReducedPeriod')
    .mockReturnValue(Promise.resolve(4));

  const movedTimeOffForContinuousPeriodSpy = jest
    .spyOn(service, 'movedTimeOffForContinuousPeriod')
    .mockReturnValue(Promise.resolve(5));

  const movedTimeOffForReducedPeriodSpy = jest
    .spyOn(service, 'movedTimeOffForReducedPeriod')
    .mockReturnValue(Promise.resolve(6));

  const reduced: ReducedTimeOffAmendment = {
    leavePeriodId: 'P0001234',
    leavePeriodStatus: 'approved',
    requestReason: 'reason',
    originalLastDayAbsent: '1988-01-01',
    newLastDayAbsent: '1988-01-01',
    newExpectedReturnDay: '1988-01-01',
    returnDay: 'Partial Day',
    returnHours: '1',
    returnMinutes: '5'
  };

  const additional: AdditionalTimeOffAmendment = {
    ...reduced,
    sameReason: 'NO',
    newReason: 'new reason'
  };

  const moved: MovedTimeOffAmendment = {
    ...reduced,
    originalFirstDayAbsent: '1988-01-01',
    newFirstWorkDay: '1988-01-01',
    newLastWorkDay: '1988-01-01',
    newLastWorkDayPartial: 'YES',
    newLastWorkDayHours: '3',
    newLastWorkDayMinutes: '5',
    newFirstDayAbsent: '1988-01-01'
  };

  const movedPartial: MovedTimeOffAmendment = {
    ...reduced,
    originalFirstDayAbsent: '1988-01-01',
    newFirstWorkDay: '1988-01-01',
    newLastWorkDay: '1988-01-01',
    newLastWorkDayPartial: 'NO',
    newLastWorkDayHours: '',
    newLastWorkDayMinutes: '',
    newFirstDayAbsent: '1988-01-01'
  };

  test('returns the correct request', () => {
    getAmendmentRequest(
      TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
      {
        periodId: '1',
        startDate: '2019-10-10',
        absencePeriodType: 'Continuous'
      } as AbsenceAmendment,
      additional
    );

    expect(additionalTimeOffForContinuousPeriodSpy).toHaveBeenCalled();

    getAmendmentRequest(
      TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
      {
        absenceCaseId: 'ABS-1',
        periodId: '1',
        startDate: '2019-10-10',
        absencePeriodType: 'Reduced'
      } as AbsenceAmendment,
      additional
    );

    expect(additionalTimeOffForReducedPeriodSpy).toHaveBeenCalled();

    getAmendmentRequest(
      TimeOffAmendmentType.REDUCED_TIME_OFF,
      {
        absenceCaseId: 'ABS-1',
        periodId: '1',
        startDate: '2019-10-10',
        absencePeriodType: 'Continuous'
      } as AbsenceAmendment,
      reduced
    );

    expect(reduceTimeOffForContinuousPeriodSpy).toHaveBeenCalled();

    getAmendmentRequest(
      TimeOffAmendmentType.REDUCED_TIME_OFF,
      {
        absenceCaseId: 'ABS-1',
        periodId: '1',
        startDate: '2019-10-10',
        absencePeriodType: 'Reduced'
      } as AbsenceAmendment,
      reduced
    );

    expect(reduceTimeOffForReducedPeriodSpy).toHaveBeenCalled();

    getAmendmentRequest(
      TimeOffAmendmentType.MOVED_TIME_OFF,
      {
        absenceCaseId: 'ABS-1',
        periodId: '1',
        startDate: '2019-10-10',
        absencePeriodType: 'Continuous'
      } as AbsenceAmendment,
      moved
    );

    expect(movedTimeOffForContinuousPeriodSpy).toHaveBeenCalled();

    getAmendmentRequest(
      TimeOffAmendmentType.MOVED_TIME_OFF,
      {
        absenceCaseId: 'ABS-1',
        periodId: '1',
        startDate: '2019-10-10',
        absencePeriodType: 'Reduced'
      } as AbsenceAmendment,
      movedPartial
    );

    expect(movedTimeOffForReducedPeriodSpy).toHaveBeenCalled();
  });
});
