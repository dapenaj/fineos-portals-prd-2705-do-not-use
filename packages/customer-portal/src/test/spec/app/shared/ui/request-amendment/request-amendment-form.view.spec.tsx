import React from 'react';

import { shallow } from '../../../../../config';
import { decisionFixture } from '../../../../../common/fixtures';
import { RequestAmendmentForm } from '../../../../../../app/shared/ui';

describe('RequestAmendmentFormView', () => {
  const props = {
    amendment: decisionFixture,
    closeForm: () => {
      jest.fn();
    },
    isSupervisor: false
  } as any;

  test('render -- SNAPSHOT', () => {
    expect(shallow(<RequestAmendmentForm {...props} />)).toMatchSnapshot();
  });
});
