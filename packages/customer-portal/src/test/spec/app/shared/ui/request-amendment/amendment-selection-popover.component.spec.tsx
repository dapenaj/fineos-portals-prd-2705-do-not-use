import React from 'react';

import { shallow } from '../../../../../config';
import {
  TimeOffAmendmentPopoverProps,
  AmendmentSelectionPopover
} from '../../../../../../app/shared/ui';

describe('RequestAmendmentFormView', () => {
  const props = {
    content: 'ADDITIONAL_TIME_OFF'
  } as TimeOffAmendmentPopoverProps;

  test('render -- SNAPSHOT', () => {
    expect(shallow(<AmendmentSelectionPopover {...props} />)).toMatchSnapshot();
  });
});
