import React from 'react';

import { shallow } from '../../../../../config';
import { RequiredFromMePanel } from '../../../../../../app/shared/ui/required-from-me-panel';
import { uploadDocumentsFixture } from '../../../../../common/fixtures';

describe('RequiredFromMePanel', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RequiredFromMePanel
          documents={uploadDocumentsFixture}
          loading={false}
          confirmReturn={null}
          onRefreshRequiredFromMe={jest.fn()}
        />
      )
    ).toMatchSnapshot();
  });
});
