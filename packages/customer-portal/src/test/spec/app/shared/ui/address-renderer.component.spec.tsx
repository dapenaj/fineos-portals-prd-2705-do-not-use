import React from 'react';

import { shallow } from '../../../../config';
import { AddressRenderer } from '../../../../../app/shared/ui';
import { customerFixture } from '../../../../common/fixtures';

describe('AddressRenderer', () => {
  const props = {
    address: customerFixture.customerAddress.address
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<AddressRenderer {...props} />)).toMatchSnapshot();
  });
});
