import React from 'react';

import { shallow } from '../../../../config';
import { PanelFooter } from '../../../../../app/shared/ui/panel/panel-footer/panel-footer.component';

describe('PanelFooter', () => {
  const props = {
    leftAction: 'left Text',
    rightAction: 'right text'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PanelFooter {...props} />)).toMatchSnapshot();
  });
});
