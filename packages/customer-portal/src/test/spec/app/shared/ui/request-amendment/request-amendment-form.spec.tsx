import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { RadioChangeEvent } from 'antd/lib/radio';

import { DefaultContext } from '../../../../../config';
import { decisionFixture } from '../../../../../common/fixtures';
import {
  releaseEventLoop,
  findSelectedRadioOption
} from '../../../../../common/utils';
import {
  TimeOffAmendmentType,
  MovedTimeOffAmendment
} from '../../../../../../app/shared/types';
import {
  RequestAmendment,
  RequestAmendmentForm
} from '../../../../../../app/shared/ui';

describe('RequestAmendmentForm', () => {
  const mock = jest.fn();
  const closeFormMock = mock;
  const renderConfirmationMock = mock;

  const props = {
    amendment: decisionFixture,
    closeForm: closeFormMock,
    isSupervisor: false,
    renderConfirmation: renderConfirmationMock
  };

  const mountRequestAmendment = (amendmentProps: any) => {
    return mount(
      <DefaultContext>
        <RequestAmendment {...amendmentProps} />
      </DefaultContext>
    );
  };

  const wrapper = mountRequestAmendment(props);

  const form = wrapper
    .find(RequestAmendmentForm)
    .instance() as RequestAmendmentForm;

  async function changeLeavePeriodType(
    requestAmendmentWrapper: ReactWrapper,
    type: TimeOffAmendmentType
  ) {
    (requestAmendmentWrapper
      .find(RequestAmendmentForm)
      .instance() as RequestAmendmentForm).handleTypeChange({
      target: { value: type }
    } as RadioChangeEvent);

    await releaseEventLoop();
    requestAmendmentWrapper.update();
  }

  test('renders request amendment form', async () => {
    expect(wrapper.find(RequestAmendmentForm)).toExist();

    expect(
      wrapper.find('[data-test-el="amended-time-off-type-input"]').length
    ).toBe(2);
  });

  test('displays correct form based on time off amendment type selected', async () => {
    await releaseEventLoop();

    await changeLeavePeriodType(wrapper, TimeOffAmendmentType.REDUCED_TIME_OFF);
    wrapper.update();

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="amended-time-off-type-input"]'
      )
    ).toHaveValue(TimeOffAmendmentType.REDUCED_TIME_OFF);
    expect(wrapper.find('.formGroup').length).toBe(3);

    await changeLeavePeriodType(wrapper, TimeOffAmendmentType.MOVED_TIME_OFF);
    wrapper.update();

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="amended-time-off-type-input"]'
      )
    ).toHaveValue(TimeOffAmendmentType.MOVED_TIME_OFF);
    expect(wrapper.find('.formGroup').length).toBe(5);

    await changeLeavePeriodType(
      wrapper,
      TimeOffAmendmentType.ADDITIONAL_TIME_OFF
    );
    wrapper.update();

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="amended-time-off-type-input"]'
      )
    ).toHaveValue(TimeOffAmendmentType.ADDITIONAL_TIME_OFF);
    expect(wrapper.find('.formGroup').length).toBe(4);
  });

  test('when partial last day is selected', async () => {
    await releaseEventLoop();

    await changeLeavePeriodType(wrapper, TimeOffAmendmentType.MOVED_TIME_OFF);

    wrapper.update();

    form.handleNewLastWorkDayPartialChange(true);

    await releaseEventLoop();

    expect(
      (form.props.values.amendedPeriod as MovedTimeOffAmendment)
        .newLastWorkDayHours
    ).toBe(0);

    form.handleNewLastWorkDayPartialChange(false);

    expect(
      (form.props.values.amendedPeriod as MovedTimeOffAmendment)
        .newLastWorkDayHours
    ).toBe(undefined);
  });

  test('when partial return day is selected', async () => {
    form.handleEndDateFullDayChange(false);

    expect(
      (form.props.values.amendedPeriod as MovedTimeOffAmendment).returnHours
    ).toBe(0);

    form.handleEndDateFullDayChange(true);

    expect(
      (form.props.values.amendedPeriod as MovedTimeOffAmendment).returnHours
    ).toBe(undefined);
  });

  test('submit form', async () => {
    await releaseEventLoop();

    await changeLeavePeriodType(wrapper, TimeOffAmendmentType.REDUCED_TIME_OFF);

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="amended-time-off-type-input"]'
      )
    ).toHaveValue(TimeOffAmendmentType.REDUCED_TIME_OFF);

    wrapper.update();

    form.props.setFieldValue('amendedPeriod.newLastDayAbsent', '2019-01-01');
    form.props.setFieldValue(
      'amendedPeriod.newExpectedReturnDay',
      '2019-01-01'
    );
    form.props.setFieldValue('amendedPeriod.returnDay', true);

    wrapper
      .find('[data-test-el="amended-time-off-form"]')
      .at(0)
      .simulate('submit');

    await releaseEventLoop();

    wrapper.update();
  });
});
