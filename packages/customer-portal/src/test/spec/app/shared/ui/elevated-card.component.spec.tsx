import React from 'react';

import { shallow } from '../../../../config';
import { ElevatedCard } from '../../../../../app/shared/ui';

describe('ElevatedCard', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<ElevatedCard />)).toMatchSnapshot();
  });
});
