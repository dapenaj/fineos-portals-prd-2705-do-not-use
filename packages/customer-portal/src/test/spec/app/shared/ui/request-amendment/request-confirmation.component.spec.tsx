import React from 'react';

import { mount, DefaultContext } from '../../../../../config';
import { RequestConfirmationModal } from '../../../../../../app/shared/ui';
import { clientConfigFixture } from '../../../../../common/fixtures';

describe('RequestConfirmation', () => {
  const props = {
    isEdit: 'REQUEST_AMENDMENT',
    config: clientConfigFixture,
    isSupervisor: false,
    renderConfirmation: () => jest.fn()
  };

  const mountPeriodDecisionProperties = () => {
    return mount(
      <DefaultContext>
        <RequestConfirmationModal {...props} />
      </DefaultContext>
    );
  };

  test('render confirmation', () => {
    const wrapper = mountPeriodDecisionProperties();
    expect(wrapper.find(RequestConfirmationModal)).toExist();
  });
});
