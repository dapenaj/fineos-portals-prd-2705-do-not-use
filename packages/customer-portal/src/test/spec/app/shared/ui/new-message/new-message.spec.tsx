import React from 'react';
import { mount, ReactWrapper } from 'enzyme';

import { DefaultContext } from '../../../../../config';
import {
  NewMessage,
  NewMessageComponent,
  NewMessageModalForm
} from '../../../../../../app/shared/ui';
import {
  notificationsFixture,
  messagesFixture
} from '../../../../../common/fixtures';
import {
  fetchNotificationEntitlementsSpy,
  addMessageSpy
} from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';
import { NewMessageFormValue } from '../../../../../../app/shared/ui';

describe('New Message', () => {
  let wrapper: ReactWrapper;

  const mountNewMessage = (props: any) => {
    return mount(
      <DefaultContext>
        <NewMessage {...props} />
      </DefaultContext>
    );
  };

  const newMessage: NewMessageFormValue = {
    subject: 'New Subject',
    narrative: 'New Narrative',
    caseId: 'ABS-1',
    relatesTo: 'NTN-21'
  };

  describe('New Message - all', () => {
    let instance: NewMessageComponent;

    beforeEach(() => {
      wrapper = mountNewMessage({
        type: 'all',
        notifications: [notificationsFixture[0]],
        onMessageSent: () => jest.fn()
      });

      instance = wrapper
        .find(NewMessageComponent)
        .instance() as NewMessageComponent;
    });

    test('should render', async () => {
      fetchNotificationEntitlementsSpy('success');

      await releaseEventLoop();

      expect(wrapper.find(NewMessageModalForm)).toExist();
    });

    test('should open the new message modal', async () => {
      fetchNotificationEntitlementsSpy('success');

      await releaseEventLoop();

      wrapper.update();

      wrapper
        .find('[data-test-el="new-message-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(NewMessageModalForm).prop('visible')).toBe(true);
      expect(
        wrapper.find(NewMessageModalForm).prop('relatesToOptions')
      ).toEqual(undefined);
      expect(wrapper.find(NewMessageModalForm).prop('entitlements')).toEqual(
        undefined
      );
    });

    test('submit new message', async () => {
      fetchNotificationEntitlementsSpy('success');
      addMessageSpy('success');

      await releaseEventLoop();

      const addSpy = jest.spyOn(instance, 'sendMessage');

      const expected = {
        caseId: 'ABS-1',
        narrative: 'New Narrative',
        subject: 'New Subject'
      };

      instance.handleSubmit(newMessage);

      await releaseEventLoop();

      expect(addSpy).toHaveBeenCalledWith(expected);
    });
  });

  describe('New Message - recent', () => {
    let instance: NewMessageComponent;

    beforeEach(() => {
      wrapper = mountNewMessage({
        type: 'recent',
        notification: notificationsFixture[0]
      });
      instance = wrapper
        .find(NewMessageComponent)
        .instance() as NewMessageComponent;
    });

    test('should render', async () => {
      fetchNotificationEntitlementsSpy('success');

      await releaseEventLoop();

      expect(wrapper.find(NewMessageModalForm)).toExist();
    });

    test('should open the new message modal', async () => {
      fetchNotificationEntitlementsSpy('success');

      await releaseEventLoop();

      wrapper.update();

      wrapper
        .find('[data-test-el="new-message-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(NewMessageModalForm).prop('visible')).toBe(true);
    });

    test('submit new message', async () => {
      fetchNotificationEntitlementsSpy('success');
      addMessageSpy('success');

      await releaseEventLoop();

      const addSpy = jest.spyOn(instance, 'sendMessage');

      const expected = {
        caseId: 'ABS-1',
        narrative: 'New Narrative',
        subject: 'New Subject'
      };

      instance.handleSubmit(newMessage);

      await releaseEventLoop();

      expect(addSpy).toHaveBeenCalledWith(expected);
    });
  });
});
