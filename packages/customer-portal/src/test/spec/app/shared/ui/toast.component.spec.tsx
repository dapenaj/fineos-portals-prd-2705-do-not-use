import { message } from 'antd';

import { showToast } from '../../../../../app/shared/ui';
import { AntType } from '../../../../../app/shared/types';

describe('showToast', () => {
  test('render', () => {
    const spy = jest.spyOn(message, 'info');

    const props = {
      type: 'info' as AntType,
      title: 'title',
      duration: 5
    };

    showToast(props);

    expect(spy).toHaveBeenCalledWith('title', 5);
  });
});
