import React from 'react';
import moment from 'moment';

import { DefaultContext, mount } from '../../../../../config';
import { FinishScreen } from '../../../../../../app/shared/ui';
import { ConfirmReturnEForm } from '../../../../../../app/shared/services/eform/types';

describe('FinishScreen', () => {
  const mountFinishScreen = (confirmReturn: Partial<ConfirmReturnEForm>) => {
    const props = {
      confirmReturn: {
        id: 1,
        expectedRTWDate: moment.utc('2019-01-01'),
        employeeConfirmed: true,
        returningAsExpected: null,
        actualRTWDate: moment.utc('2019-01-01'),
        employerNotified: undefined,
        rtwConfirmationNotes: '',
        ...confirmReturn
      }
    };

    return mount(
      <DefaultContext>
        <FinishScreen {...props} />
      </DefaultContext>
    );
  };

  test('when confirmed expected date -- render', () => {
    expect(
      mountFinishScreen({ returningAsExpected: true }).find(FinishScreen)
    ).toMatchSnapshot();
  });

  test('when confirmed another date -- render', () => {
    expect(
      mountFinishScreen({ returningAsExpected: false }).find(FinishScreen)
    ).toMatchSnapshot();
  });
});
