import React from 'react';

import { shallow, DefaultContext, mount } from '../../../../../config';
import {
  RequiredFromMeDocumentModal,
  DocumentUpload
} from '../../../../../../app/shared/ui';
import { uploadDocumentsFixture } from '../../../../../common/fixtures';
import { releaseEventLoop } from '../../../../../common/utils';

describe('RequiredFromMeDocumentModal', () => {
  const mountRequiredFromMeModal = () => {
    return mount(
      <DefaultContext>
        <RequiredFromMeDocumentModal
          document={uploadDocumentsFixture[0]}
          onRefreshRequiredFromMe={jest.fn()}
        />
      </DefaultContext>
    );
  };

  test('showModal', async () => {
    const wrapper = mountRequiredFromMeModal();

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(DocumentUpload)).not.toExist();

    wrapper
      .find('[data-test-el="required-document-upload-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(DocumentUpload)).toExist();
  });

  test('closeModal', async () => {
    const wrapper = mountRequiredFromMeModal();

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(DocumentUpload)).not.toExist();

    wrapper
      .find('[data-test-el="required-document-upload-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(DocumentUpload)).toExist();

    wrapper
      .find('[data-test-el="cancel-document-upload-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();
  });

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RequiredFromMeDocumentModal
          document={uploadDocumentsFixture[0]}
          onRefreshRequiredFromMe={jest.fn()}
        />
      )
    ).toMatchSnapshot();
  });
});
