import React from 'react';
import { DocumentUploadRequest } from 'fineos-js-api-client';

import { mount, DefaultContext, ReactWrapper } from '../../../../../config';
import {
  RequiredFromMeUpload,
  RequiredFromMeUploadComponent
} from '../../../../../../app/shared/ui';
import { uploadDocumentsFixture } from '../../../../../common/fixtures';
import {
  uploadCaseDocumentSpy,
  uploadSupportingEvidenceSpy,
  markDocumentAsReadSpy
} from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';

describe('RequiredFromMeUpload', () => {
  let wrapper: ReactWrapper;
  let instance: RequiredFromMeUploadComponent;

  const mock = jest.fn();

  const props = {
    document: uploadDocumentsFixture[0],
    onCloseModal: mock,
    onCloseModalAndRefresh: mock
  };

  const request: DocumentUploadRequest = {
    documentType: 'string',
    fileName: 'string',
    fileExtension: 'string',
    description: 'string',
    fileContentsText: 'string'
  };

  const mountRequiredFromMeUpload = () =>
    mount(
      <DefaultContext>
        <RequiredFromMeUpload {...props} />
      </DefaultContext>
    );

  beforeEach(() => {
    wrapper = mountRequiredFromMeUpload();

    instance = wrapper
      .find('RequiredFromMeUploadComponent')
      .instance() as RequiredFromMeUploadComponent;
  });

  test('render', async () => {
    wrapper.update();

    expect(wrapper.find(RequiredFromMeUploadComponent)).toExist();
  });

  test('upload a file', async () => {
    uploadCaseDocumentSpy('success');
    uploadSupportingEvidenceSpy('success');
    markDocumentAsReadSpy('success');

    instance.handleOnSubmit({
      ...uploadDocumentsFixture[0],
      request
    });

    await releaseEventLoop();

    wrapper.update();

    expect(instance.state.loading).toBe(false);
    expect(instance.state.showErrorModal).toBe(false);
  });

  test('upload a file - failure', async () => {
    uploadCaseDocumentSpy('failure');

    instance.handleOnSubmit({
      ...uploadDocumentsFixture[0],
      request
    });

    await releaseEventLoop();
    wrapper.update();
    expect(instance.state.error).toBe('upload');
  });

  test('receive a file - failure', async () => {
    uploadCaseDocumentSpy('success');
    uploadSupportingEvidenceSpy('failure');

    instance.handleOnSubmit({
      ...uploadDocumentsFixture[0],
      request
    });

    await releaseEventLoop();
    wrapper.update();
    expect(instance.state.error).toBe('received');
  });

  test('handleCancelModal', () => {
    instance.handleCancelModal();
    expect(instance.state.showErrorModal).toBe(false);
  });
});
