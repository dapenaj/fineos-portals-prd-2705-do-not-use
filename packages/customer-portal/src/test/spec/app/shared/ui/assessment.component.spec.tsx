import React from 'react';
import { FormattedMessage } from 'react-intl';

import { shallow } from '../../../../config';
import { Assessment } from '../../../../../app/shared/ui';

describe('PanelHeader', () => {
  const props = {
    title: <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT" />
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<Assessment {...props} />)).toMatchSnapshot();
  });
});
