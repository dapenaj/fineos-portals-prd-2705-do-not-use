import React from 'react';
import { mount } from 'enzyme';
import { notification } from 'antd';

import { DefaultContext } from '../../../../../config';
import {
  decisionFixture,
  reducedDecisionFixture
} from '../../../../../common/fixtures';
import { releaseEventLoop } from '../../../../../common/utils';
import { additionalTimeOffForContinuousPeriodSpy } from '../../../../../common/spys';
import {
  RequestAmendmentFormValue,
  TimeOffAmendmentType
} from '../../../../../../app/shared/types';
import {
  RequestAmendment,
  RequestAmendmentView,
  handleSubmit
} from '../../../../../../app/shared/ui';

describe('RequestAmendmentForm', () => {
  const mock = jest.fn();
  const closeFormMock = mock;
  const renderConfirmationMock = mock;

  const continuousAmendmentProps = {
    amendment: decisionFixture,
    closeForm: closeFormMock,
    isSupervisor: false,
    renderConfirmation: renderConfirmationMock
  };

  const reducedAmendmentProps = {
    amendment: reducedDecisionFixture,
    closeForm: closeFormMock,
    isSupervisor: false,
    renderConfirmation: renderConfirmationMock
  };

  const continuousAmendmentSubmissionProps = {
    props: {
      amendment: continuousAmendmentProps.amendment,
      closeForm: continuousAmendmentProps.closeForm,
      isSupervisor: false,
      renderConfirmation: continuousAmendmentProps.renderConfirmation,
      intl: { formatMessage: jest.fn() }
    }
  } as any;

  const reducedAmendmentSubmissionProps = {
    props: {
      amendment: reducedAmendmentProps.amendment,
      closeForm: reducedAmendmentProps.closeForm,
      renderConfirmation: reducedAmendmentProps.renderConfirmation,
      intl: { formatMessage: jest.fn() }
    }
  } as any;

  const additionalTimeOffValues = {
    type: TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
    amendedPeriod: {
      leavePeriodId: 'P0001234',
      requestReason: 'reason',
      leavePeriodStatus: 'approved',
      sameReason: 'NO',
      newReason: 'new reason',
      originalLastDayAbsent: '1988-01-01',
      newLastDayAbsent: '1988-01-01',
      newExpectedReturnDay: '1988-01-01',
      returnDay: 'Partial Day',
      returnHours: '0',
      returnMinutes: '0'
    }
  } as RequestAmendmentFormValue;

  const spy = jest.spyOn(notification, 'error');

  describe('Submit Time Off for amendment', () => {
    const mountRequestContinuousPeriodAmendment = () =>
      mount(
        <DefaultContext>
          <RequestAmendment {...continuousAmendmentProps} />
        </DefaultContext>
      );

    const wrapper = mountRequestContinuousPeriodAmendment();

    test('additional time off for a continuous period request', async () => {
      additionalTimeOffForContinuousPeriodSpy('success');

      expect(wrapper.find(RequestAmendmentView)).toExist();

      handleSubmit(additionalTimeOffValues, continuousAmendmentSubmissionProps);

      await releaseEventLoop();

      expect(spy).not.toHaveBeenCalledWith(notification, 'error');
    });

    test('additional time off for a continuous period error', async () => {
      additionalTimeOffForContinuousPeriodSpy('failure');

      handleSubmit(additionalTimeOffValues, reducedAmendmentSubmissionProps);

      await releaseEventLoop();

      expect(spy).toHaveBeenCalled();
    });
  });
});
