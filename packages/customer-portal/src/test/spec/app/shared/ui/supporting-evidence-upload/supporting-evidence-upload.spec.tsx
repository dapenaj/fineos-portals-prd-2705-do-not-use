import React from 'react';
import { UploadChangeParam, RcFile } from 'antd/lib/upload';
import { UploadFile } from 'antd/lib/upload/interface';
import { Upload as AntUpload } from 'antd';

import { mount, DefaultContext } from '../../../../../config';
import { DocumentUpload } from '../../../../../../app/shared/ui';
import { uploadDocumentsFixture } from '../../../../../common/fixtures';
import {
  uploadCaseDocumentSpy,
  uploadSupportingEvidenceSpy,
  markDocumentAsReadSpy
} from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';

describe('DocumentUpload', () => {
  const mock = jest.fn();

  const props = {
    loading: false,
    document: uploadDocumentsFixture[0],
    onCancel: mock,
    onSubmit: mock
  };

  const mountDocumentUpload = () =>
    mount(
      <DefaultContext>
        <DocumentUpload {...props} />
      </DefaultContext>
    );

  test('render', async () => {
    const wrapper = mountDocumentUpload();

    wrapper.update();

    expect(wrapper.find(AntUpload)).toExist();
  });

  test('upload a file', async () => {
    const file = new File([], 'test');
    uploadCaseDocumentSpy('success');
    uploadSupportingEvidenceSpy('success');
    markDocumentAsReadSpy('success');

    const wrapper = mountDocumentUpload();
    const instance = wrapper
      .find('DocumentUpload')
      .instance() as DocumentUpload;

    instance.handleOnUploadChange({
      file: {
        status: 'done',
        name: 'test',
        type: 'txt',
        originFileObj: file
      }
    } as UploadChangeParam<UploadFile>);

    instance.handleUpload();

    await releaseEventLoop();

    wrapper.update();

    expect(instance.state.status).toBe(undefined);
  });

  test('beforeUpload', async () => {
    const wrapper = mountDocumentUpload();
    const instance = wrapper
      .find('DocumentUpload')
      .instance() as DocumentUpload;

    const file = {} as RcFile;

    instance.overrideAction('test');

    instance.handleBeforeUpload(file, [file]);

    expect(instance.state.status).toBe('done');
  });
});
