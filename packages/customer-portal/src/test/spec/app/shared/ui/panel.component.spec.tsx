import React from 'react';

import { shallow } from '../../../../config';
import { Panel } from '../../../../../app/shared/ui/panel/panel.component';

describe('Panel', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<Panel />)).toMatchSnapshot();
  });
});
