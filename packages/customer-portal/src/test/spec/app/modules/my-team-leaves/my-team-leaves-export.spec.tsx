import React from 'react';
import { Button } from 'antd';
import { advanceTo } from 'jest-date-mock';
import moment from 'moment';

import { MyTeamLeavesExport } from '../../../../../app/modules/my-team-leaves';
import { DefaultContext, mount } from '../../../../config';
import { myTeamLeaveFixture } from '../../../../common/fixtures';
import { ExportService } from '../../../../../app/shared/services';
import { CsvDetails } from '../../../../../app/shared/types';
import { EXPORT_DATE_FORMAT } from '../../../../../app/shared/utils';

describe('MyTeamLeavesExport', () => {
  const props = {
    loading: false,
    myTeamLeaves: myTeamLeaveFixture
  };

  const service = ExportService.getInstance();

  const mountMyTeamLeavesExport = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyTeamLeavesExport {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountMyTeamLeavesExport();

    wrapper.update();

    expect(wrapper.find(Button)).toExist();
  });

  test('export my team leaves', () => {
    advanceTo(new Date(2017, 6, 15, 13, 45));

    const spy = jest.spyOn(service, 'exportToCsv').mockReturnValue();

    const wrapper = mountMyTeamLeavesExport();

    wrapper.update();

    wrapper.find(Button).simulate('click');

    const expected: CsvDetails = {
      columns: [
        'Team Member',
        'Notified on',
        'Case Id',
        'Reason',
        'First day missing work',
        'Expected return',
        'Absence case manager',
        `Case manager's contact`
      ],
      rows: [
        [
          'John D.',
          '08-06-2017',
          'NTN-22',
          'Accident or treatment for injury',
          '08-06-2017',
          '08-06-2017',
          'James Peach',
          '1-1234567809'
        ],
        [
          'John D.',
          '08-06-2017',
          'NTN-23',
          'Pregnancy, birth or related medical treatment',
          '08-06-2017',
          '08-06-2017',
          'James Peach',
          '1-1234567809'
        ],
        [
          'Gregor S.',
          '08-06-2017',
          'NTN-25',
          'Out of work for another reason',
          '08-01-2019',
          '08-01-2019',
          'James Peach',
          '1-1234567809'
        ]
      ],
      filename: `MY_TEAM_LEAVES_${moment().format(EXPORT_DATE_FORMAT)}`
    };

    expect(spy).toHaveBeenCalledWith(jasmine.objectContaining(expected));
  });
});
