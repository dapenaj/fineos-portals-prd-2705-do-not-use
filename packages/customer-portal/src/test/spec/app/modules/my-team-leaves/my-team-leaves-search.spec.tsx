import React from 'react';
import { Select } from 'antd';

import { MyTeamLeavesSearch } from '../../../../../app/modules/my-team-leaves';
import { DefaultContext, mount } from '../../../../config';
import { employeeFixture } from '../../../../common/fixtures';

describe('MyTeamLeavesSearch', () => {
  const props = {
    loading: false,
    employees: employeeFixture,
    onEmployeeIdChange: jest.fn()
  };

  const mountMyTeamLeavesSearch = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyTeamLeavesSearch {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountMyTeamLeavesSearch();

    wrapper.update();

    expect(wrapper.find(Select)).toExist();
  });
});
