import React from 'react';

import { shallow } from '../../../../config';
import {
  myTeamLeaveFixture,
  employeeFixture
} from '../../../../common/fixtures';
import { MyTeamLeavesView } from '../../../../../app/modules/my-team-leaves';

describe('MyTeamLeavesView', () => {
  const mock = jest.fn();
  const props = {
    employees: employeeFixture,
    employeesLoading: false,
    onEmployeeIdChange: mock,
    onFilterChange: mock,
    myTeamLeaves: myTeamLeaveFixture,
    myTeamLeavesLoading: false
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyTeamLeavesView {...props} />)).toMatchSnapshot();
  });
});
