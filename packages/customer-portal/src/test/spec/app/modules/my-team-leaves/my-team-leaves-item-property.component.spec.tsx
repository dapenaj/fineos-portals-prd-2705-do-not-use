import React from 'react';

import { shallow } from '../../../../config';
import { myTeamLeaveFixture } from '../../../../common/fixtures';
import { MyTeamLeavesItemProperty } from '../../../../../app/modules/my-team-leaves';

describe('MyTeamLeavesItemProperty', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyTeamLeavesItemProperty />)).toMatchSnapshot();
  });
});
