import React from 'react';

import { shallow } from '../../../../config';
import { myTeamLeaveFixture } from '../../../../common/fixtures';
import { MyTeamLeavesItemHeader } from '../../../../../app/modules/my-team-leaves';

describe('MyTeamLeavesItemHeader', () => {
  const props = {
    myTeamLeave: myTeamLeaveFixture[1]
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyTeamLeavesItemHeader {...props} />)).toMatchSnapshot();
  });
});
