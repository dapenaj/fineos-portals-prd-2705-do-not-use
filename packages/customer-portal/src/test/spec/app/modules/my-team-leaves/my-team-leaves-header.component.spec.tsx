import React from 'react';

import { shallow } from '../../../../config';
import { employeeFixture } from '../../../../common/fixtures';
import { MyTeamLeavesHeader } from '../../../../../app/modules/my-team-leaves';

describe('MyTeamLeavesHeader', () => {
  const props = {
    loading: false,
    employees: employeeFixture,
    onEmployeeIdChange: jest.fn()
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyTeamLeavesHeader {...props} />)).toMatchSnapshot();
  });
});
