import React from 'react';
import { Button } from 'antd';

import { MyTeamLeavesFilters } from '../../../../../app/modules/my-team-leaves';
import { DefaultContext, mount } from '../../../../config';

describe('MyTeamLeavesFilters', () => {
  const filterChangeSpy = jest.fn();
  const props = {
    onFilterChange: filterChangeSpy,
    loading: false
  };

  const mountMyTeamLeavesFilters = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyTeamLeavesFilters {...props} {...extraProps} />
      </DefaultContext>
    );

  test('show filters', () => {
    const wrapper = mountMyTeamLeavesFilters();

    expect(wrapper.find(Button)).toHaveText('All team members');

    wrapper.find(Button).simulate('click');

    wrapper.update();

    expect(wrapper.find('ul[role="menu"]')).toExist();
  });

  test('select filter', () => {
    const wrapper = mountMyTeamLeavesFilters();

    expect(wrapper.find(Button)).toHaveText('All team members');

    wrapper.find(Button).simulate('click');

    wrapper.update();

    wrapper
      .find('li[role="menuitem"]')
      .at(2)
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(Button)).toHaveText('Coming back In the next 7 days');

    wrapper.find(Button).simulate('click');

    wrapper.update();

    wrapper
      .find('li[role="menuitem"]')
      .at(5)
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(Button)).toHaveText(
      'Going on leave In the next 2 days'
    );

    wrapper.find(Button).simulate('click');

    wrapper.update();

    wrapper
      .find('li[role="menuitem"]')
      .at(0)
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(Button)).toHaveText('All team members');
  });
});
