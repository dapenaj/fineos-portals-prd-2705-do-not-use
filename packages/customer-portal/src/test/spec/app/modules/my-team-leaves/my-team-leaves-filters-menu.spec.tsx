import React from 'react';
import { Menu } from 'antd';

import {
  MyTeamLeavesFiltersMenu,
  filters
} from '../../../../../app/modules/my-team-leaves';
import { DefaultContext, mount } from '../../../../config';

const { Item } = Menu;

describe('MyTeamLeavesFiltersMenu', () => {
  const filterClickSpy = jest.fn();
  const outsideClickSpy = jest.fn();

  const props = {
    onFilterClick: filterClickSpy,
    onOutsideClick: outsideClickSpy
  };

  const mountMyTeamLeavesFiltersMenu = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyTeamLeavesFiltersMenu {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountMyTeamLeavesFiltersMenu();

    expect(wrapper.find(Item).length).toEqual(9);
  });

  test('filter select', () => {
    const wrapper = mountMyTeamLeavesFiltersMenu();

    const spy = jest.spyOn(props, 'onFilterClick');

    wrapper
      .find(Item)
      .at(2)
      .simulate('click');

    wrapper.update();

    expect(spy).toHaveBeenCalledWith(filters[2]);
  });
});
