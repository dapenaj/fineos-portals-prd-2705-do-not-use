import React from 'react';

import { shallow } from '../../../../config';
import { myTeamLeaveFixture } from '../../../../common/fixtures';
import { MyTeamLeavesList } from '../../../../../app/modules/my-team-leaves';

describe('MyTeamLeavesList', () => {
  const props = {
    loading: false,
    myTeamLeaves: myTeamLeaveFixture
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyTeamLeavesList {...props} />)).toMatchSnapshot();
  });
});
