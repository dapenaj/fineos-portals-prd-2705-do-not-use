import React from 'react';
import { IntlProvider } from 'react-intl';
import { mount, shallow } from 'enzyme';

import { absencePeriods } from '../../../../common/fixtures';
import { DefaultContext } from '../../../../config';
import {
  RequestConfirmationModal,
  ConfirmModal
} from '../../../../../app/shared/ui';
import { deleteTimeOffRequestSpy } from '../../../../common/spys';
import { releaseEventLoop } from '../../../../common/utils';
import {
  LeavePeriodActions,
  LeavePeriodActionsComponent
} from '../../../../../app/modules/my-team-leaves';

describe('LeavePeriodActions', () => {
  const props = {
    member: 'Joe',
    absenceCaseId: 'ABS-1',
    absencePeriod: absencePeriods[3]
  };

  const mountLeavePeriodActions = () => {
    return mount(
      <DefaultContext>
        <LeavePeriodActions {...props} />
      </DefaultContext>
    );
  };

  test('render component', () => {
    const wrapper = mountLeavePeriodActions();

    expect(wrapper.find(LeavePeriodActions)).toExist();

    expect(wrapper.find('[data-test-el="request-change"]')).toExist();

    expect(wrapper.find('[data-test-el="request-deletion"]')).toExist();
  });

  test('should toggle the edit form', async () => {
    const wrapper = mountLeavePeriodActions();

    const periodActions = wrapper
      .find(LeavePeriodActionsComponent)
      .instance() as LeavePeriodActionsComponent;

    periodActions.showSupervisorEditForm();

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find('[data-test-el="amended-time-off-form"]')).toExist();

    periodActions.closeForm();

    await releaseEventLoop();
    wrapper.update();

    expect(
      wrapper.find('[data-test-el="amended-time-off-form"]')
    ).not.toExist();
  });

  test('should toggle the confirmation modal', async () => {
    const wrapper = mountLeavePeriodActions();

    const periodActions = wrapper
      .find(LeavePeriodActionsComponent)
      .instance() as LeavePeriodActionsComponent;

    periodActions.toggleSupervisorConfirmation();

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(RequestConfirmationModal)).toExist();
  });

  test('test confirm modal', async () => {
    const wrapper = mountLeavePeriodActions();

    const periodActions = wrapper
      .find(LeavePeriodActionsComponent)
      .instance() as LeavePeriodActionsComponent;

    periodActions.showConfirmModal();

    wrapper.update();

    expect(periodActions.state.showModal).toBe(true);
    expect(wrapper.find(ConfirmModal)).toExist();

    wrapper.find('button.ant-btn-danger').simulate('click');

    expect(periodActions.state.showModal).toBe(false);

    wrapper
      .find('[data-test-el="confirmation-cancel-button"]')
      .at(0)
      .simulate('click');

    expect(periodActions.state.showModal).toBe(false);
  });

  test('should try to delete a period line', async () => {
    deleteTimeOffRequestSpy('success');

    const wrapper = mountLeavePeriodActions();
    const periodActions = wrapper
      .find(LeavePeriodActionsComponent)
      .instance() as LeavePeriodActionsComponent;

    periodActions.deleteLeavePeriod();

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(RequestConfirmationModal)).toExist();
  });

  test('render - pending -- SNAPSHOT', () => {
    expect(
      shallow(
        <IntlProvider locale="en">
          <LeavePeriodActions {...props} />
        </IntlProvider>
      )
    ).toMatchSnapshot();
  });

  test('render with payment history -- SNAPSHOT', () => {
    expect(
      shallow(
        <IntlProvider locale="en">
          <LeavePeriodActions {...props} />
        </IntlProvider>
      )
    ).toMatchSnapshot();
  });
});
