import React from 'react';
import { RouteComponentProps } from 'react-router';

import MyTeamLeaves, {
  MyTeamLeavesView,
  MyTeamLeavesComponent,
  TeamLeaveFilter
} from '../../../../../app/modules/my-team-leaves';
import { DefaultContext, mount } from '../../../../config';
import {
  getSupervisedEmployeesSpy,
  fetchSupervisedNotificationsSpy,
  getSupervisorAbsenceDetailsSpy
} from '../../../../common/spys';
import { clientConfigFixture } from '../../../../common/fixtures';
import { history } from '../../../../../app/config';
import { AuthenticationService } from '../../../../../app/shared/services';
import { Role } from '../../../../../app/shared/types';
import { releaseEventLoop } from '../../../../common/utils';
import { Spinner, EmptyList } from '../../../../../app/shared/ui';

describe('MyTeamLeaves', () => {
  const routerProps = { history: history } as RouteComponentProps;

  const mountMyTeamLeaves = () =>
    mount(
      <DefaultContext>
        <MyTeamLeaves {...routerProps} />
      </DefaultContext>
    );

  beforeEach(() => {
    AuthenticationService.getInstance().displayRoles = [
      Role.ABSENCE_SUPERVISOR
    ];
  });

  test('render - loading', async () => {
    getSupervisedEmployeesSpy('success');
    fetchSupervisedNotificationsSpy('success');
    getSupervisorAbsenceDetailsSpy('success');

    const wrapper = mountMyTeamLeaves();

    await releaseEventLoop();
    await releaseEventLoop();

    expect(wrapper.find(MyTeamLeavesView)).toExist();
    expect(wrapper.find(Spinner)).toExist();
  });

  test('render - success', async () => {
    getSupervisedEmployeesSpy('success');
    fetchSupervisedNotificationsSpy('success');
    getSupervisorAbsenceDetailsSpy('success');

    const wrapper = mountMyTeamLeaves();

    await releaseEventLoop();
    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyTeamLeavesView)).toExist();
  });

  test('render - failure', async () => {
    getSupervisedEmployeesSpy('success');
    fetchSupervisedNotificationsSpy('failure');
    getSupervisorAbsenceDetailsSpy('success');

    const wrapper = mountMyTeamLeaves();

    await releaseEventLoop();
    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyTeamLeavesView)).toExist();
    expect(wrapper.find(EmptyList)).toExist();
  });

  test('handleFilterChange', () => {
    const wrapper = mountMyTeamLeaves();
    const instance = wrapper
      .find(MyTeamLeavesComponent)
      .instance() as MyTeamLeavesComponent;
    const spy = jest.spyOn(instance, 'fetchMyTeamLeaves');

    instance.handleFilterChange({
      startDate: '2019-03-01',
      endDate: '2019-03-02',
      sort: 'expectedRTWDate'
    } as TeamLeaveFilter);

    expect(spy).toHaveBeenCalled();
  });

  test('handleEmployeeIdChange', () => {
    const wrapper = mountMyTeamLeaves();
    const instance = wrapper
      .find(MyTeamLeavesComponent)
      .instance() as MyTeamLeavesComponent;
    const spy = jest.spyOn(instance, 'fetchMyTeamLeaves');

    instance.handleEmployeeIdChange('1');

    expect(spy).toHaveBeenCalled();
  });
});
