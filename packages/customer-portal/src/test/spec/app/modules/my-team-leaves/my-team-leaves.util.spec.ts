import {
  absenceSummariesFixture,
  myTeamLeaveFixture
} from '../../../../common/fixtures';
import {
  getAbsenceCaseHandler,
  getCaseManagersContact,
  getMyTeamLeavesRows,
  getMyTeamLeaveRowValue,
  MY_TEAM_LEAVES_EXPORT_HEADERS
} from '../../../../../app/modules/my-team-leaves';
import { MyTeamLeavesExportColumn } from '../../../../../app/shared/types';

describe('My Team Leaves Utils', () => {
  test('getAbsenceCaseHandler', () => {
    expect(getAbsenceCaseHandler(absenceSummariesFixture)).toEqual(
      'James Peach'
    );
    expect(getAbsenceCaseHandler([])).toEqual('--');
  });

  test('getCaseManagersContact', () => {
    expect(getCaseManagersContact(absenceSummariesFixture)).toEqual(
      '1-1234567809'
    );
    expect(getCaseManagersContact([])).toEqual('--');
  });

  test('getMyTeamLeavesRows', () => {
    const columns: MyTeamLeavesExportColumn[] = MY_TEAM_LEAVES_EXPORT_HEADERS.map(
      ({ header, field }) => ({
        header,
        field
      })
    );

    expect(getMyTeamLeavesRows(columns, myTeamLeaveFixture)).toStrictEqual([
      [
        'John D.',
        '08-06-2017',
        'NTN-22',
        'Accident or treatment for injury',
        '08-06-2017',
        '08-06-2017',
        'James Peach',
        '1-1234567809'
      ],
      [
        'John D.',
        '08-06-2017',
        'NTN-23',
        'Pregnancy, birth or related medical treatment',
        '08-06-2017',
        '08-06-2017',
        'James Peach',
        '1-1234567809'
      ],
      [
        'Gregor S.',
        '08-06-2017',
        'NTN-25',
        'Out of work for another reason',
        '08-01-2019',
        '08-01-2019',
        'James Peach',
        '1-1234567809'
      ]
    ]);
  });

  test('getMyTeamLeaveRowValue', () => {
    expect(
      getMyTeamLeaveRowValue(
        'absenceCaseManager',
        undefined,
        absenceSummariesFixture
      )
    ).toBe('James Peach');

    expect(
      getMyTeamLeaveRowValue(
        'caseManagersContact',
        undefined,
        absenceSummariesFixture
      )
    ).toBe('1-1234567809');

    expect(
      getMyTeamLeaveRowValue('member', 'John D.', absenceSummariesFixture)
    ).toBe('John D.');

    expect(
      getMyTeamLeaveRowValue(
        'dateFirstMissingWork',
        '10-10-2019',
        absenceSummariesFixture
      )
    ).toBe('10-10-2019');

    expect(
      getMyTeamLeaveRowValue(
        'notificationReason',
        'Accident or treatment for injury',
        absenceSummariesFixture
      )
    ).toBe('Accident or treatment for injury');
  });
});
