import React from 'react';

import { shallow } from '../../../../config';
import { absencePeriods } from '../../../../common/fixtures';
import { LeavePeriodList } from '../../../../../app/modules/my-team-leaves';

describe('LeavePeriodList', () => {
  const props = {
    loading: false,
    absencePeriods
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<LeavePeriodList {...props} />)).toMatchSnapshot();
  });
});
