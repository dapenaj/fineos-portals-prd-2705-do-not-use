import {
  buildTimeOffFromFormValue,
  TimeOffFormValue,
  buildWorkDetailsForm,
  buildPersonalDetailsForm
} from '../../../../../app/modules/intake';
import {
  leavePeriodFixture,
  customerOccupationsFixture,
  customerFixture,
  contactFixture
} from '../../../../common/fixtures';

describe('Intake Utils', () => {
  test('buildTimeOffFromFormValue', async () => {
    const originalValues: TimeOffFormValue = {
      outOfWork: true,
      leavePeriods: leavePeriodFixture
    };

    const results = buildTimeOffFromFormValue(originalValues);

    expect(results.outOfWork).toBe(originalValues.outOfWork);
    expect(results.timeOffLeavePeriods!.length).toBe(2);
    expect(results.reducedScheduleLeavePeriods!.length).toBe(2);
    expect(results.episodicLeavePeriods!.length).toBe(2);
  });

  test('buildWorkDetailsForm', async () => {
    const originalValues = customerOccupationsFixture[0];

    const results = buildWorkDetailsForm(originalValues);

    expect(results!.employer).toBe(originalValues.employer);
    expect(results!.jobTitle).toBe(originalValues.jobTitle);
    expect(results!.dateJobBegan).toBe(originalValues.dateJobBegan);

    // also the function can take no params and will return no result
    expect(buildWorkDetailsForm()).toBeUndefined();
  });

  test('buildPersonalDetailsForm', async () => {
    const originalValues = {
      detail: customerFixture,
      contact: contactFixture
    };

    const results = buildPersonalDetailsForm(originalValues);

    expect(results!.firstName).toBe(customerFixture.firstName);
    expect(results!.lastName).toBe(customerFixture.lastName);
    expect(results!.dateOfBirth).toBe(customerFixture.dateOfBirth);
    expect(results!.email).toBe(contactFixture.emailAddresses[0].emailAddress);
    expect(results!.phoneNumber).toBe(contactFixture.phoneNumbers[0]);
    expect(results!.premiseNo).toBe(
      customerFixture.customerAddress.address.premiseNo
    );
    expect(results!.addressLine1).toBe(
      customerFixture.customerAddress.address.addressLine1
    );
    expect(results!.addressLine2).toBe(
      customerFixture.customerAddress.address.addressLine2
    );
    expect(results!.addressLine3).toBe(
      customerFixture.customerAddress.address.addressLine3
    );
    expect(results!.city).toBe(
      customerFixture.customerAddress.address.addressLine4
    );
    expect(results!.state).toBe(
      customerFixture.customerAddress.address.addressLine6
    );
    expect(results!.postCode).toBe(
      customerFixture.customerAddress.address.postCode
    );
    expect(results!.country).toBe(
      customerFixture.customerAddress.address.country
    );

    // also the function can take no params and will return no result
    expect(buildPersonalDetailsForm()).toBeUndefined();
  });
});
