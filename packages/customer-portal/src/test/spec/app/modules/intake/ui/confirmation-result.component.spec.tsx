import React from 'react';

import { shallow } from '../../../../../config';
import { ConfirmationResult } from '../../../../../../app/modules/intake';
import {
  ConfirmationCase,
  ConfirmationType
} from '../../../../../../app/shared/types';

describe('ConfirmationResult', () => {
  test('render - Failure -- SNAPSHOT', () => {
    const props = {
      notificationId: null,
      confirmation: { type: ConfirmationType.Failure } as ConfirmationCase
    };

    expect(shallow(<ConfirmationResult {...props} />)).toMatchSnapshot();
  });

  test('render - Successful -- SNAPSHOT', () => {
    const props = {
      notificationId: 'NTN-1',
      confirmation: { type: ConfirmationType.Successful } as ConfirmationCase
    };

    expect(shallow(<ConfirmationResult {...props} />)).toMatchSnapshot();
  });

  test('render - PartialSuccess -- SNAPSHOT', () => {
    const props = {
      notificationId: 'NTN-1',
      confirmation: {
        type: ConfirmationType.PartialSuccess
      } as ConfirmationCase
    };

    expect(shallow(<ConfirmationResult {...props} />)).toMatchSnapshot();
  });
});
