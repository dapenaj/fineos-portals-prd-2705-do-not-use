import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow } from '../../../../../../config';
import { MedicalProviderItem } from '../../../../../../../app/modules/intake/wrap-up/supporting-evidence/medical-provider-item';

describe('MedicalProviderItem', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <MedicalProviderItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          medicalProvider={{
            name: 'John',
            phone: '1223344'
          }}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <MedicalProviderItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          medicalProvider={{
            name: 'John',
            phone: '1223344',
            addressLine: 'Address Line'
          }}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <MedicalProviderItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          medicalProvider={{
            name: 'John',
            phone: '1223344',
            addressLine: 'Address Line',
            city: 'City'
          }}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <MedicalProviderItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          medicalProvider={{
            name: 'John',
            phone: '1223344',
            addressLine: 'Address Line',
            city: 'City',
            state: 'TX',
            country: 'USA'
          }}
        />
      )
    ).toMatchSnapshot();
  });
});
