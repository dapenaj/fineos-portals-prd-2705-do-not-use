import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  PersonalDetailsForm,
  PersonalDetailsFormView
} from '../../../../../../../app/modules/intake/your-request';
import {
  customerFixture,
  contactFixture
} from '../../../../../../common/fixtures';
import { releaseEventLoop } from '../../../../../../common/utils';

describe('PersonalDetailsForm', () => {
  const mock = jest.fn();
  const submitStepSpy = mock;

  const props = {
    detail: customerFixture,
    contact: contactFixture,
    loading: false,
    outsideUSA: false,
    submitStep: submitStepSpy,
    isActive: true
  };

  const mountPersonalDetailsForm = () => {
    return mount(
      <DefaultContext>
        <PersonalDetailsForm {...props} />
      </DefaultContext>
    );
  };

  test('render', () => {
    const wrapper = mountPersonalDetailsForm();
    expect(wrapper.find(PersonalDetailsFormView)).toExist();
  });

  test('submit', async () => {
    const wrapper = mountPersonalDetailsForm();

    await releaseEventLoop();

    wrapper.find('form').simulate('submit');

    await releaseEventLoop();
  });
});
