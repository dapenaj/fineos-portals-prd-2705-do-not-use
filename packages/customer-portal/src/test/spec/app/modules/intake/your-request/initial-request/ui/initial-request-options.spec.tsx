import React from 'react';

import { shallow, DefaultContext, mount } from '../../../../../../../config';
import { InitialRequestOptions } from '../../../../../../../../app/modules/intake';
import { EventOptionId } from '../../../../../../../../app/shared/types';

describe('InitialRequestOptions', () => {
  const optionSelectSpy = jest.fn();
  const props = {
    index: 1,
    selections: [EventOptionId.SICKNESS],
    options: [EventOptionId.SICKNESS, EventOptionId.ACCIDENT],
    isActive: true,
    optionSelect: optionSelectSpy
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<InitialRequestOptions {...props} />)).toMatchSnapshot();
  });

  describe('option select', () => {
    const mountInitialRequestOptions = () => {
      return mount(
        <DefaultContext>
          <InitialRequestOptions {...props} />
        </DefaultContext>
      );
    };

    test('first option selected', () => {
      const wrapper = mountInitialRequestOptions();

      wrapper
        .find('input[type="radio"]')
        .at(0)
        .simulate('change', { target: { checked: true } });

      expect(optionSelectSpy).toHaveBeenCalled();
    });
  });
});
