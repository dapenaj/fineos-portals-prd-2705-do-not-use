import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  IntakeConfirmation,
  IntakeConfirmationView
} from '../../../../../../../app/modules/intake/ui';

describe('IntakeConfirmation', () => {
  test('should render', () => {
    const wrapper = mount(
      <DefaultContext>
        <IntakeConfirmation />
      </DefaultContext>
    );

    expect(wrapper.find(IntakeConfirmationView)).toExist();
  });
});
