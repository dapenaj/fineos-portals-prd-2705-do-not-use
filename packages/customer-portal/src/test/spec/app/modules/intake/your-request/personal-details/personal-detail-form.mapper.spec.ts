import { Phone, Customer } from 'fineos-js-api-client';
import moment from 'moment';

import {
  mapToContact,
  mapToCustomer,
  PersonalDetailsFormValue
} from '../../../../../../../app/modules/intake/your-request';
import {
  customerFixture,
  contactFixture
} from '../../../../../../common/fixtures';

describe('mapToCustomer', () => {
  test('it should map the form value to the api', () => {
    const formvalues = {
      firstName: 'test first name',
      lastName: 'test last name',
      dateOfBirth: 'test dob',
      premiseNo: 'test premise no',
      addressLine1: 'test address line 1',
      addressLine2: 'test address line 2',
      addressLine3: 'test address line 3',
      city: 'test city',
      state: 'test state',
      postCode: 'test post code',
      country: 'AK'
    } as PersonalDetailsFormValue;

    const expected = {
      customerAddress: {
        address: {
          addressLine1: 'test address line 1',
          addressLine2: 'test address line 2',
          addressLine3: 'test address line 3',
          addressLine4: 'test city',
          addressLine5: '',
          addressLine6: 'test state',
          addressLine7: '',
          country: 'AK',
          postCode: 'test post code',
          premiseNo: 'test premise no'
        }
      },
      securedClient: true,
      staff: true,
      partyType: 'party',
      dateOfBirth: 'test dob',
      dateOfBirthMoment: moment('1988-01-01T00:00:00.000Z'),
      firstName: 'test first name',
      gender: 'Female',
      idNumber: '741852963',
      identificationNumberType: 'ID',
      initials: 'RS',
      lastName: 'test last name',
      maritalStatus: 'Single',
      nationality: 'Unknown',
      needsInterpretor: false,
      placeOfBirth: '',
      preferredContactMethod: 'Unknown',
      secondName: '',
      title: 'Ms'
    } as Customer;

    expect(mapToCustomer(customerFixture, formvalues).firstName).toBe(
      expected.firstName
    );
  });
});

describe('mapToContact', () => {
  test('it should map the form value to the api', () => {
    const testEmail = 'test-email';
    const testPhone = { intCode: 'a', areaCode: 'test', telephoneNo: 'phone' };

    expect(
      mapToContact(contactFixture, {
        email: testEmail,
        phoneNumber: testPhone
      } as PersonalDetailsFormValue)
    ).toStrictEqual({
      emailAddresses: [
        { emailAddress: 'bob@dobalina.com', preferred: false },
        { emailAddress: testEmail, preferred: false }
      ],
      phoneNumbers: [
        {
          phoneNumberType: 'Home',
          intCode: '353',
          areaCode: '1',
          telephoneNo: '234567',
          preferred: true
        },
        {
          phoneNumberType: 'Phone',
          intCode: 'a',
          areaCode: 'test',
          telephoneNo: 'phone',
          preferred: false
        }
      ]
    });

    expect(
      mapToContact(
        {
          ...contactFixture,
          emailAddresses: [
            ...contactFixture.emailAddresses,
            { emailAddress: testEmail, preferred: false }
          ],
          phoneNumbers: [
            ...contactFixture.phoneNumbers,
            {
              telephoneNo: 'phone',
              preferred: false
            } as Phone
          ]
        },
        {
          email: testEmail,
          phoneNumber: testPhone
        } as PersonalDetailsFormValue
      )
    ).toStrictEqual({
      emailAddresses: [
        ...contactFixture.emailAddresses,
        { emailAddress: testEmail, preferred: false }
      ],
      phoneNumbers: [
        ...contactFixture.phoneNumbers,
        {
          preferred: false,
          telephoneNo: 'phone'
        }
      ]
    });
  });
});
