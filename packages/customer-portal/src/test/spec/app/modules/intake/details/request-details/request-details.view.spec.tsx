import React from 'react';

import { DefaultContext, mount, shallow } from '../../../../../../config';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  RequestDetailsView,
  RequestDetailsForm,
  RequestDetailsValue,
  AccommodationValue,
  Accommodation
} from '../../../../../../../app/modules/intake';
import {
  RequestDetailFormId,
  RequestDetailAnswerType,
  RequestDetailFieldId,
  IntakeSectionStep
} from '../../../../../../../app/shared/types';

describe('RequestDetailsView', () => {
  const mock = jest.fn();
  const submitStepMock = mock;

  const props = {
    forms: [
      {
        id: RequestDetailFormId.PREGNANCY_POSTPARTUM,
        layout: [
          {
            id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.NO
              },
              {
                id: RequestDetailFieldId.YES,
                targetFormId:
                  RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
              }
            ]
          },
          {
            id: RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
            answerType: RequestDetailAnswerType.DATE
          },
          {
            id: RequestDetailFieldId.NEWBORN_COUNT,
            answerType: RequestDetailAnswerType.COUNT
          },
          {
            id: RequestDetailFieldId.PREGNANCY_DELIVERY,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN
              },
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL
              },
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION
              },
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
              }
            ]
          },
          {
            id: RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.NO
              },
              {
                id: RequestDetailFieldId.YES,
                targetFormId:
                  RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS
              }
            ]
          },
          {
            id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
            answerType: RequestDetailAnswerType.LONG_TEXT
          }
        ]
      }
    ],
    loading: false,
    step: { isActive: true } as IntakeSectionStep<
      RequestDetailsValue | AccommodationValue
    >,
    accommodationSelected: false,
    stepSubmit: submitStepMock
  };

  const mountRequestDetailsView = (extraProps?: object) => {
    return mount(
      <DefaultContext>
        <RequestDetailsView {...props} {...extraProps} />
      </DefaultContext>
    );
  };

  test('render', async () => {
    const wrapper = mountRequestDetailsView();
    await releaseEventLoop();

    expect(wrapper.find(RequestDetailsForm)).toExist();
  });

  test('render - Accommodation', async () => {
    const wrapper = mountRequestDetailsView({ accommodationSelected: true });
    await releaseEventLoop();

    expect(wrapper.find(Accommodation)).toExist();
  });

  test('submit', async () => {
    const wrapper = mountRequestDetailsView();

    await releaseEventLoop();

    wrapper.find('form').simulate('submit');
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<RequestDetailsView {...props} />)).toMatchSnapshot();
  });
});
