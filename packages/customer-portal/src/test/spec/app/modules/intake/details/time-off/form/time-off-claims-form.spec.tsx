import React from 'react';

import { DefaultContext, mount, shallow } from '../../../../../../../config';
import { releaseEventLoop } from '../../../../../../../common/utils';
import { TimeOffClaimsForm } from '../../../../../../../../app/modules/intake/details/time-off/claims-form';

describe('RequestDetailsFormView', () => {
  const mock = jest.fn();
  const submitStepMock = mock;

  const props = {
    isActive: true,
    submitStep: submitStepMock,
    submitting: false
  };

  const mountTimeOffClaimsForm = () => {
    return mount(
      <DefaultContext>
        <TimeOffClaimsForm {...props} />
      </DefaultContext>
    );
  };

  test('render', async () => {
    const wrapper = mountTimeOffClaimsForm();
    await releaseEventLoop();

    expect(wrapper.find(TimeOffClaimsForm)).toExist();
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<TimeOffClaimsForm {...props} />)).toMatchSnapshot();
  });
});
