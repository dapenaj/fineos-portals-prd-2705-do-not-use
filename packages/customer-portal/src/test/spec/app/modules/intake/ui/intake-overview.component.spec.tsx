import React from 'react';

import { shallow } from '../../../../../config';
import { IntakeOverview } from '../../../../../../app/modules/intake';
import { IntakeSection } from '../../../../../../app/shared/types';

describe('IntakeOverview', () => {
  const mock = jest.fn();

  const props = {
    sections: [{ isActive: true, isComplete: true } as IntakeSection<any>],
    onSectionClick: mock
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<IntakeOverview {...props} />)).toMatchSnapshot();
  });
});
