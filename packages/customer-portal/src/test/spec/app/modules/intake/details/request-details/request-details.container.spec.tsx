import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  RequestDetailsValue,
  RequestDetailsContainer,
  RequestDetailsView,
  YourRequestValue
} from '../../../../../../../app/modules/intake';
import {
  IntakeSectionStep,
  EventOptionId,
  IntakeSectionId
} from '../../../../../../../app/shared/types';
import { RequestDetailFormService } from '../../../../../../../app/shared/services';

describe('RequestDetails', () => {
  const requestDetailsFormService = RequestDetailFormService.getInstance();
  const getDefaultFormsSpy = jest.spyOn(
    requestDetailsFormService,
    'getDefaultForms'
  );

  const submitIntakeWorkflowStepMock = jest.fn();

  const props = {
    step: { isActive: true, isComplete: false } as IntakeSectionStep<
      RequestDetailsValue
    >,
    sections: [
      {
        id: IntakeSectionId.YOUR_REQUEST,
        isActive: false,
        isComplete: true,
        steps: [],
        title: {},
        value: {
          initialRequest: [
            EventOptionId.PREGNANCY,
            EventOptionId.PREGNANCY_PRENATAL,
            EventOptionId.PREGNANCY_PRENATAL_CARE
          ],
          workDetails: {},
          personalDetails: {}
        } as YourRequestValue
      }
    ],
    submitIntakeWorkflowStep: submitIntakeWorkflowStepMock
  };

  const mountRequestDetails = () => {
    return mount(
      <DefaultContext>
        <RequestDetailsContainer {...props} />
      </DefaultContext>
    );
  };

  test('render', async () => {
    const wrapper = mountRequestDetails();
    await releaseEventLoop();

    expect(getDefaultFormsSpy).toHaveBeenCalledWith([
      EventOptionId.PREGNANCY,
      EventOptionId.PREGNANCY_PRENATAL,
      EventOptionId.PREGNANCY_PRENATAL_CARE
    ]);

    expect(wrapper.find(RequestDetailsView)).toExist();
  });

  test('step submit', async () => {
    const wrapper = mountRequestDetails();

    await releaseEventLoop();
    wrapper.update();

    (wrapper
      .find('RequestDetailsContainer')
      .instance() as RequestDetailsContainer).handleSubmit({
      EXPECTED_DELIVERY_DATE: 'string'
    });

    expect(submitIntakeWorkflowStepMock).toHaveBeenCalledWith(
      expect.objectContaining({
        value: { EXPECTED_DELIVERY_DATE: 'string' }
      })
    );
  });
});
