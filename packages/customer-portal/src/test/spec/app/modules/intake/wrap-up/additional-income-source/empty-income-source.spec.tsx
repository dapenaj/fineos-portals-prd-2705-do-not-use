import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow } from '../../../../../../config';
import { EmptyIncomeSource } from '../../../../../../../app/modules/intake/wrap-up/additional-income-source/empty-income-source';

describe('EmptyIncomeSource', () => {
  test('render when enabled -- SNAPSHOT', () => {
    expect(
      shallow(<EmptyIncomeSource onAdd={_noop} isDisabled={false} />)
    ).toMatchSnapshot();
  });

  test('render when disabled -- SNAPSHOT', () => {
    expect(
      shallow(<EmptyIncomeSource onAdd={_noop} isDisabled={true} />)
    ).toMatchSnapshot();
  });
});
