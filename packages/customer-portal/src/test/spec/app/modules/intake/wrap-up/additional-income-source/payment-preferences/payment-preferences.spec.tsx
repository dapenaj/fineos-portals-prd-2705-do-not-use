import React from 'react';
import moment from 'moment';
import { PaymentPreference } from 'fineos-js-api-client';

import {
  DefaultContext,
  mount,
  ReactWrapper
} from '../../../../../../../config';
import {
  paymentPreferenceFixture,
  paymentPreferenceEftDefaultFixture
} from '../../../../../../../common/fixtures';
import { fetchPaymentPreferencesSpy } from '../../../../../../../common/spys';
import { releaseEventLoop } from '../../../../../../../common/utils';
import { PaymentType } from '../../../../../../../../app/shared/types';
import { API_DATE_FORMAT } from '../../../../../../../../app/shared/utils';
import {
  PaymentPreferences,
  PaymentPreferencesComponent
} from '../../../../../../../../app/modules/intake/wrap-up';

describe('PaymentPreferences', () => {
  const props = {
    onPaymentPreferenceChange: jest.fn(),
    onAddPaymentPreference: jest.fn(),
    onProceedClick: jest.fn(),
    isDisabled: true,
    isSubmitting: false
  };

  const mountPaymentPreferences = () => {
    return mount(
      <DefaultContext>
        <PaymentPreferences {...props} />
      </DefaultContext>
    );
  };

  test('render PaymentPreferences', () => {
    fetchPaymentPreferencesSpy('success');
    const wrapper = mountPaymentPreferences();
    expect(wrapper.find(PaymentPreferences)).toExist();
  });

  describe('getDefaultEftAccountDetails', () => {
    let wrapper: ReactWrapper;
    let instance: PaymentPreferencesComponent;

    beforeEach(async () => {
      fetchPaymentPreferencesSpy('success');

      wrapper = mountPaymentPreferences();
      await releaseEventLoop();
      wrapper.update();

      instance = wrapper
        .find(PaymentPreferencesComponent)
        .instance() as PaymentPreferencesComponent;
    });

    test('should return an appropriate value if unable to find a preference', async () => {
      // getDefaultEftAccountDetails passed nothing
      expect(instance.getDefaultEftAccountDetails([])).toStrictEqual({});

      // getDefaultEftAccountDetails passed an array without any eft
      const fixtureCheckOnly = [paymentPreferenceEftDefaultFixture[0]];
      expect(
        instance.getDefaultEftAccountDetails(fixtureCheckOnly)
      ).toStrictEqual({});
    });

    test('should return an appropriate value when there is a default', async () => {
      // getDefaultEftAccountDetails passed an array with a default
      expect(
        instance.getDefaultEftAccountDetails(paymentPreferenceEftDefaultFixture)
      ).toStrictEqual({
        routingNumber: '9876',
        accountNo: '4566463',
        accountType: 'savings',
        accountName: 'Joey Soprano',
        paymentPreferenceId: '204'
      });
    });

    test('should return an appropriate value when there is no default', async () => {
      // getDefaultEftAccountDetails passed an array without a default
      // - where today is not within the effective date range
      const fixture = [...paymentPreferenceEftDefaultFixture];
      fixture[2].isDefault = false;
      expect(instance.getDefaultEftAccountDetails(fixture)).toStrictEqual({
        routingNumber: '4321',
        accountNo: '1234567',
        accountType: 'Checking',
        accountName: 'Tony Montana',
        paymentPreferenceId: '202'
      });
    });

    test('should return an appropriate value when there is no default but a preference is within effective date range', async () => {
      // getDefaultEftAccountDetails passed an array without a default
      // - where at least one effectiveFrom and effectiveTo encases today
      const fixture = [
        ...paymentPreferenceEftDefaultFixture,
        {
          paymentMethod: 'Stock Account',
          isDefault: false,
          paymentPreferenceId: '206',
          effectiveFrom: moment()
            .subtract(7, 'd')
            .format(API_DATE_FORMAT),
          effectiveTo: moment()
            .add(7, 'd')
            .format(API_DATE_FORMAT),
          accountDetails: {
            routingNumber: '6767',
            accountNo: '9999555',
            accountType: 'savings',
            accountName: 'Stephen Fry'
          }
        } as PaymentPreference
      ];
      fixture[2].isDefault = false;

      expect(instance.getDefaultEftAccountDetails(fixture)).toStrictEqual({
        routingNumber: '6767',
        accountNo: '9999555',
        accountType: 'savings',
        accountName: 'Stephen Fry',
        paymentPreferenceId: '206'
      });
    });
  });

  describe('mapSelectedToCheckPaymentPreference', () => {
    let wrapper: ReactWrapper;
    let instance: PaymentPreferencesComponent;

    beforeEach(async () => {
      fetchPaymentPreferencesSpy('success');

      wrapper = mountPaymentPreferences();
      await releaseEventLoop();
      wrapper.update();

      instance = wrapper
        .find(PaymentPreferencesComponent)
        .instance() as PaymentPreferencesComponent;
    });

    test('should map a check payment preference', async () => {
      expect(instance.mapSelectedToCheckPaymentPreference()).toStrictEqual(
        paymentPreferenceFixture[0]
      );
    });

    test('should map a check payment preference if there is no preferences of type Check', async () => {
      instance.setState({ paymentPreferences: [] });
      await releaseEventLoop();
      wrapper.update();

      expect(instance.mapSelectedToCheckPaymentPreference()).toStrictEqual({
        paymentMethod: PaymentType.CHECK,
        isDefault: true,
        chequeDetails: {
          nameToPrintOnCheck: 'Jane Doe'
        }
      });
    });
  });

  describe('mapSelectedToEftPaymentPreference', () => {
    let wrapper: ReactWrapper;
    let instance: PaymentPreferencesComponent;

    beforeEach(async () => {
      fetchPaymentPreferencesSpy('success');

      wrapper = mountPaymentPreferences();
      await releaseEventLoop();
      wrapper.update();

      instance = wrapper
        .find(PaymentPreferencesComponent)
        .instance() as PaymentPreferencesComponent;
    });

    test('should return the default value if passed in', async () => {
      const defaultObj = {} as PaymentPreference;
      expect(
        instance.mapSelectedToEftPaymentPreference(defaultObj)
      ).toStrictEqual({ ...defaultObj, isDefault: true });
    });

    test('should find the payment preference for the currently selected account', async () => {
      // with a paymentPreferenceId
      instance.setState({
        selectedEftAccount: {
          ...paymentPreferenceFixture[2].accountDetails!,
          paymentPreferenceId: paymentPreferenceFixture[2].paymentPreferenceId
        }
      });
      await releaseEventLoop();
      wrapper.update();

      expect(instance.mapSelectedToEftPaymentPreference()).toStrictEqual({
        ...paymentPreferenceFixture[2],
        isDefault: true
      });

      // without a paymentPreferenceId
      instance.setState({
        selectedEftAccount: {
          ...paymentPreferenceFixture[3].accountDetails!
        }
      });
      await releaseEventLoop();
      wrapper.update();

      expect(instance.mapSelectedToEftPaymentPreference()).toStrictEqual({
        paymentMethod: PaymentType.ELECTRONIC_FUNDS_TRANSFER,
        isDefault: true,
        accountDetails: paymentPreferenceFixture[3].accountDetails!
      });
    });
  });
});
