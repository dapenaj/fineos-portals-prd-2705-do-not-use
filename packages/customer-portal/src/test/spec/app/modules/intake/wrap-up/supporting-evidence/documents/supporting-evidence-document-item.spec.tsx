import React from 'react';
import { Modal } from 'antd';

import {
  mount,
  DefaultContext,
  ReactWrapper
} from '../../../../../../../config';
import { uploadDocumentsFixture } from '../../../../../../../common/fixtures';
import { SupportingEvidenceDocumentItem } from '../../../../../../../../app/modules/intake';
import { releaseEventLoop } from '../../../../../../../common/utils';

describe('SupportingEvidenceDocumentItem', () => {
  let wrapper: ReactWrapper;
  let instance: SupportingEvidenceDocumentItem;

  const mock = jest.fn();

  const props = {
    loading: false,
    isActive: true,
    document: uploadDocumentsFixture[0],
    onDeleteDocumentation: mock,
    onAddDocumentation: mock
  };

  const mountSupportingEvidenceDocumentItem = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <SupportingEvidenceDocumentItem {...props} {...extraProps} />
      </DefaultContext>
    );

  beforeEach(() => {
    wrapper = mountSupportingEvidenceDocumentItem();
    instance = wrapper
      .find('SupportingEvidenceDocumentItem')
      .instance() as SupportingEvidenceDocumentItem;
  });

  test('showModal', async () => {
    wrapper.update();

    wrapper
      .find('[data-test-el="supporting-evidence-upload-btn"]')
      .hostNodes()
      .simulate('click');

    await releaseEventLoop();

    expect(wrapper.find(Modal)).toExist();
  });

  test('handleOnCancel', () => {
    wrapper.update();

    instance.handleOnCancel();

    expect(instance.state.showModal).toBe(false);
  });

  test('handleSubmit', () => {
    const spy = jest.spyOn(instance.props, 'onAddDocumentation');

    wrapper.update();

    instance.handleSubmit(uploadDocumentsFixture[0]);

    expect(spy).toHaveBeenCalled();
    expect(instance.state.showModal).toBe(false);
  });

  test('handleDelete', async () => {
    wrapper = mountSupportingEvidenceDocumentItem({
      document: {
        ...uploadDocumentsFixture[0],
        request: {}
      }
    });

    const spy = jest.spyOn(instance.props, 'onDeleteDocumentation');

    wrapper.update();

    expect(instance.state.showModal).toBe(false);

    wrapper.update();

    wrapper
      .find('[data-test-el="supporting-evidence-delete-btn"]')
      .hostNodes()
      .simulate('click');

    expect(spy).toHaveBeenCalled();
  });
});
