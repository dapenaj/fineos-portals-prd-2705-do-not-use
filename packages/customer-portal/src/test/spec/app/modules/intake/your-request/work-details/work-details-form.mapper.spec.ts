import { CustomerOccupation } from 'fineos-js-api-client';

import {
  mapToCustomerOccupation,
  WorkDetailsFormValue
} from '../../../../../../../app/modules/intake/your-request';
import { customerOccupationsFixture } from '../../../../../../common/fixtures';

describe('mapToCustomerOccupation', () => {
  test('it should map the form value to the api', () => {
    const formvalues = {
      jobTitle: 'Test',
      employer: customerOccupationsFixture[0].employer,
      dateJobBegan: customerOccupationsFixture[0].dateJobBegan
    } as WorkDetailsFormValue;

    const expected = {
      ...customerOccupationsFixture[0],
      jobTitle: 'Test'
    } as CustomerOccupation;

    expect(
      mapToCustomerOccupation(customerOccupationsFixture[0], formvalues)
    ).toStrictEqual(expected);
  });
});
