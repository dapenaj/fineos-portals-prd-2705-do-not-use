import React from 'react';

import { shallow } from '../../../../../../config';
import { IntakeConfirmationTypeComponent } from '../../../../../../../app/modules/intake/ui/intake-confirmation/intake-confirmation-type';
import { ConfirmationType } from '../../../../../../../app/shared/types';
import { appConfigFixture } from '../../../../../../common/fixtures';

describe('IntakeConfirmationTypeComponent', () => {
  const defaultProps = {
    prevPath: '',
    config: appConfigFixture
  };

  test('render - Failure -- SNAPSHOT', () => {
    const props = {
      ...defaultProps,
      createdNotificationId: null,
      type: ConfirmationType.Failure
    };

    expect(
      shallow(<IntakeConfirmationTypeComponent {...props} />)
    ).toMatchSnapshot();
  });

  test('render - Successful -- SNAPSHOT', () => {
    const props = {
      ...defaultProps,
      createdNotificationId: 'NTN-1',
      type: ConfirmationType.Successful
    };

    expect(
      shallow(<IntakeConfirmationTypeComponent {...props} />)
    ).toMatchSnapshot();
  });

  test('render - PartialSuccess -- SNAPSHOT', () => {
    const props = {
      ...defaultProps,
      createdNotificationId: 'NTN-1',
      type: ConfirmationType.PartialSuccess
    };

    expect(
      shallow(<IntakeConfirmationTypeComponent {...props} />)
    ).toMatchSnapshot();
  });
});
