import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  WorkDetailsForm,
  WorkDetailsFormComponent
} from '../../../../../../../app/modules/intake/your-request/work-details/form';
import { customerOccupationsFixture } from '../../../../../../common/fixtures';
import { releaseEventLoop } from '../../../../../../common/utils';

describe('WorkDetailsForm', () => {
  const mock = jest.fn();
  const submitStepSpy = mock;
  const cancelSpy = mock;

  const props = {
    occupation: customerOccupationsFixture[0],
    loading: false,
    submitStep: submitStepSpy,
    isActive: true,
    showEmployer: true,
    showCancel: true,
    submitLabel: 'INTAKE.CONTROLS.OK',
    cancel: cancelSpy
  };

  const mountWorkDetailsForm = () => {
    return mount(
      <DefaultContext>
        <WorkDetailsForm {...props} />
      </DefaultContext>
    );
  };

  test('render', () => {
    const wrapper = mountWorkDetailsForm();
    expect(wrapper.find(WorkDetailsFormComponent)).toExist();
  });

  test('submit', async () => {
    const wrapper = mountWorkDetailsForm();

    await releaseEventLoop();

    wrapper.find('form').simulate('submit');

    await releaseEventLoop();
  });
});
