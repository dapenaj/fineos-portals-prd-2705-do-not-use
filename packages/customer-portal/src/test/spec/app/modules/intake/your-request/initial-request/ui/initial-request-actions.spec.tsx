import React from 'react';

import { shallow } from '../../../../../../../config';
import { InitialRequestControls } from '../../../../../../../../app/modules/intake';

describe('InitialRequestControls', () => {
  const submitFormSpy = jest.fn();
  const props = {
    disabled: false,
    submitForm: submitFormSpy
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<InitialRequestControls {...props} />)).toMatchSnapshot();
  });
});
