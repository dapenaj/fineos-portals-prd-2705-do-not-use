import { ApiConfigProps } from 'fineos-js-api-client';
import moment from 'moment';

import { validateField } from '../../../../../../../../app/modules/intake/details/request-details/form/request-details-validation';
import { RequestDetailFieldId } from '../../../../../../../../app/shared/types';

const required = 'COMMON.VALIDATION.REQUIRED';

describe('Request Details Validation', () => {
  test('Accident Description', () => {
    expect(validateField(RequestDetailFieldId.DESCRIBE_ACCIDENT, '')).toBe(
      required
    );

    expect(
      validateField(RequestDetailFieldId.DESCRIBE_ACCIDENT, undefined)
    ).toBe(required);

    expect(validateField(RequestDetailFieldId.DESCRIBE_ACCIDENT, '1234')).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.MINIMUM_CHARS'
    );

    expect(
      validateField(
        RequestDetailFieldId.DESCRIBE_ACCIDENT,
        '12345678901234567890123456789012345678901234567890' +
          '12345678901234567890123456789012345678901234567890' +
          '12345678901234567890123456789012345678901234567890' +
          '12345678901234567890123456789012345678901234567890'
      )
    ).toBe('INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.MAXIMUM_CHARS');

    expect(
      validateField(RequestDetailFieldId.DESCRIBE_ACCIDENT, 'a normal string')
    ).toBeUndefined();
  });

  test('First Symptom Date', () => {
    expect(validateField(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '')).toBe(
      required
    );

    expect(
      validateField(RequestDetailFieldId.FIRST_SYMPTOM_DATE, undefined)
    ).toBe(required);

    expect(
      validateField(
        RequestDetailFieldId.FIRST_SYMPTOM_DATE,
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.FIRST_SYMPTOM_DATE_FUTURE_DATE'
    );

    expect(
      validateField(RequestDetailFieldId.FIRST_SYMPTOM_DATE, '2019-01-01')
    ).toBeUndefined();
  });

  test('First Treated', () => {
    expect(validateField(RequestDetailFieldId.DATE_FIRST_TREATED, '')).toBe(
      required
    );

    expect(
      validateField(RequestDetailFieldId.DATE_FIRST_TREATED, undefined)
    ).toBe(required);

    expect(
      validateField(
        RequestDetailFieldId.DATE_FIRST_TREATED,
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.DATE_FIRST_TREATED_FUTURE_DATE'
    );

    expect(
      validateField(RequestDetailFieldId.DATE_FIRST_TREATED, '2019-01-01')
    ).toBeUndefined();
  });

  test('Accident Date', () => {
    expect(validateField(RequestDetailFieldId.ACCIDENT_DATE, '')).toBe(
      required
    );

    expect(validateField(RequestDetailFieldId.ACCIDENT_DATE, undefined)).toBe(
      required
    );

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_DATE,
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACCIDENT_DATE_FUTURE_DATE'
    );

    expect(
      validateField(RequestDetailFieldId.ACCIDENT_DATE, '2019-01-01')
    ).toBeUndefined();
  });

  test('Accident First Treatment Date', () => {
    expect(
      validateField(RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED, '')
    ).toBe(required);

    expect(
      validateField(RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED, undefined)
    ).toBe(required);

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED,
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACCIDENT_DATE_FIRST_TREATED_FUTURE_DATE'
    );

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED,
        '2019-01-01'
      )
    ).toBeUndefined();
  });

  test('Accident Overnight Hospital Dates', () => {
    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ''
      )
    ).toBe(required);

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        undefined
      )
    ).toBe(required);

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        [
          moment()
            .add(1, 'y')
            .format(ApiConfigProps.dateOnlyFormat)
        ]
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION_FUTURE_DATE'
    );

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ['2019-01-01']
      )
    ).toBeUndefined();
  });

  test('Accident Additional Comments', () => {
    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
        '12345678901234567890123456789012345678901234567890' +
          '12345678901234567890123456789012345678901234567890' +
          '12345678901234567890123456789012345678901234567890' +
          '12345678901234567890123456789012345678901234567890'
      )
    ).toBe('INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.MAXIMUM_CHARS');

    expect(
      validateField(RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS, '')
    ).toBeUndefined();

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
        undefined
      )
    ).toBeUndefined();

    expect(
      validateField(
        RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
        'a normal string'
      )
    ).toBeUndefined();
  });

  test('Actual Delivery Date', () => {
    expect(validateField(RequestDetailFieldId.ACTUAL_DELIVERY_DATE, '')).toBe(
      required
    );
    expect(
      validateField(RequestDetailFieldId.ACTUAL_DELIVERY_DATE, undefined)
    ).toBe(required);
    expect(
      validateField(
        RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACTUAL_DELIVERY_FUTURE_DATE'
    );
    expect(
      validateField(RequestDetailFieldId.ACTUAL_DELIVERY_DATE, '2019-01-01')
    ).toBeUndefined();
  });

  test('Expected Delivery Date', () => {
    expect(validateField(RequestDetailFieldId.EXPECTED_DELIVERY_DATE, '')).toBe(
      required
    );
    expect(
      validateField(RequestDetailFieldId.EXPECTED_DELIVERY_DATE, undefined)
    ).toBe(required);
    expect(
      validateField(RequestDetailFieldId.EXPECTED_DELIVERY_DATE, '2019-01-01')
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.EXPECTED_DELIVERY_PAST_DATE'
    );
    expect(
      validateField(
        RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      )
    ).toBeUndefined();
  });

  test('Expected Overnight Hospital Dates', () => {
    expect(
      validateField(
        RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ''
      )
    ).toBe(undefined);
    expect(
      validateField(
        RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
        undefined
      )
    ).toBe(undefined);
    expect(
      validateField(
        RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
        '2019-01-01'
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.EXPECTED_HOSPITAL_PAST_DATE'
    );
    expect(
      validateField(
        RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
        ['2019-01-01']
      )
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.EXPECTED_HOSPITAL_PAST_DATE'
    );
    expect(
      validateField(
        RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
        [
          moment()
            .add(1, 'y')
            .format(ApiConfigProps.dateOnlyFormat)
        ]
      )
    ).toBeUndefined();
  });

  test('overnight hospital stay duration', () => {
    expect(
      validateField(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, '')
    ).toBe(required);

    expect(
      validateField(
        RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION,
        undefined
      )
    ).toBe(required);

    expect(
      validateField(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        moment()
          .add(1, 'y')
          .format(ApiConfigProps.dateOnlyFormat)
      ])
    ).toBe(
      'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.OVERNIGHT_HOSPITAL_STAY_DURATION_FUTURE_DATE'
    );

    expect(
      validateField(RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION, [
        '2019-01-01'
      ])
    ).toBeUndefined();
  });

  test('unrequired fields', () => {
    expect(
      validateField(RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS, '')
    ).toBe(undefined);
  });
});
