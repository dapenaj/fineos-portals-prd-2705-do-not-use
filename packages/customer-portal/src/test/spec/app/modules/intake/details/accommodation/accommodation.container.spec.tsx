import React from 'react';
import { noop as _noop } from 'lodash';
import { AccommodationService } from 'fineos-js-api-client';

import { mount, DefaultContext, ReactWrapper } from '../../../../../../config';
import {
  AccommodationValue,
  AccommodationView,
  WorkplaceAccommodationPlaceholder,
  WorkplaceAccommodationPanel,
  WorkplaceAccommodationFormView,
  AccommodationContainer,
  intakeSections
} from '../../../../../../../app/modules/intake';
import {
  IntakeSectionStep,
  IntakeSection,
  IntakeSectionId,
  EventOptionId
} from '../../../../../../../app/shared/types';
import {
  selectAnySelectOption,
  releaseEventLoop
} from '../../../../../../common/utils';
import { IntakeAccommodationSubmissionService } from '../../../../../../../app/shared/services';
import { startAccommodationSpy } from '../../../../../../common/spys';
import { SectionValue } from '../../../../../../../app/store/intake/workflow';

describe('AccommodationContainer', () => {
  let submitStepMock: jest.Mock;

  const sections = [
    {
      id: IntakeSectionId.YOUR_REQUEST,
      title: {},
      isActive: false,
      isComplete: true,
      steps: [
        {
          id: 'PERSONAL_DETAILS',
          title: {},
          isComplete: true,
          isActive: false,
          value: {
            detail: {
              firstName: 'Jane',
              lastName: 'Doe',
              secondName: '',
              initials: 'JD',
              needsInterpretor: false,
              placeOfBirth: '',
              gender: 'Female',
              dateOfBirth: '1988-01-01',
              maritalStatus: 'Single',
              nationality: 'Unknown',
              title: 'Ms',
              preferredContactMethod: 'Unknown',
              idNumber: '741852963',
              identificationNumberType: 'ID',
              securedClient: true,
              staff: true,
              partyType: 'party',
              customerAddress: {
                address: {
                  premiseNo: '123',
                  addressLine1: 'Main street',
                  addressLine2: `Where it's at`,
                  addressLine3: 'Two turn tables',
                  addressLine4: 'And a microphone',
                  addressLine5: '',
                  addressLine6: 'CA',
                  addressLine7: '',
                  postCode: 'AB123456',
                  country: 'USA'
                }
              }
            },
            contact: {
              emailAddresses: [
                {
                  preferred: true,
                  emailAddress: 'bob@dobalina.com'
                }
              ],
              phoneNumbers: [
                {
                  phoneNumberType: 'Home',
                  intCode: '353',
                  areaCode: '1',
                  telephoneNo: '234567',
                  preferred: true
                }
              ]
            }
          }
        },
        {
          id: 'WORK_DETAILS',
          title: {},
          isComplete: true,
          isActive: false,
          value: {
            occupation: {
              dateJobBegan: '2014-02-02',
              dateJobEnded: '',
              daysWorkedPerWeek: 5,
              employer: 'FinCorp Ltd',
              endPosReason: '',
              hoursWorkedPerWeek: 40,
              jobDesc: '',
              jobTitle: 'Line Supervisor',
              remarks: '',
              selfEmployed: false,
              workScheduleDescription: '',
              employmentCategory: '',
              jobStrenuous: '',
              endEmploymentReason: '',
              employmentLocation: '',
              additionalEmploymentCategory: '',
              employmentStatus: '',
              employmentTitle: '',
              employeeId: '',
              primary: true,
              occupationId: 1,
              codeId: '',
              codeName: '',
              overrideDaysWorkedPerWeek: false,
              extensionAttributes: []
            }
          }
        },
        {
          id: 'INITIAL_REQUEST',
          title: {},
          isComplete: true,
          isActive: false,
          value: ['PREGNANCY', 'PREGNANCY_PRENATAL', 'PREGNANCY_PRENATAL_CARE']
        }
      ],
      value: {
        personalDetails: {
          detail: {
            firstName: 'Jane',
            lastName: 'Doe',
            secondName: '',
            initials: 'JD',
            needsInterpretor: false,
            placeOfBirth: '',
            gender: 'Female',
            dateOfBirth: '1988-01-01',
            maritalStatus: 'Single',
            nationality: 'Unknown',
            title: 'Ms',
            preferredContactMethod: 'Unknown',
            idNumber: '741852963',
            identificationNumberType: 'ID',
            securedClient: true,
            staff: true,
            partyType: 'party',
            customerAddress: {
              address: {
                premiseNo: '123',
                addressLine1: 'Main street',
                addressLine2: `Where it's at`,
                addressLine3: 'Two turn tables',
                addressLine4: 'And a microphone',
                addressLine5: '',
                addressLine6: 'CA',
                addressLine7: '',
                postCode: 'AB123456',
                country: 'USA'
              }
            }
          },
          contact: {
            emailAddresses: [
              {
                preferred: true,
                emailAddress: 'bob@dobalina.com'
              }
            ],
            phoneNumbers: [
              {
                phoneNumberType: 'Home',
                intCode: '353',
                areaCode: '1',
                telephoneNo: '234567',
                preferred: true
              }
            ]
          }
        },
        workDetails: {
          occupation: {
            dateJobBegan: '2014-02-02',
            dateJobEnded: '',
            daysWorkedPerWeek: 5,
            employer: 'FinCorp Ltd',
            endPosReason: '',
            hoursWorkedPerWeek: 40,
            jobDesc: '',
            jobTitle: 'Line Supervisor',
            remarks: '',
            selfEmployed: false,
            workScheduleDescription: '',
            employmentCategory: '',
            jobStrenuous: '',
            endEmploymentReason: '',
            employmentLocation: '',
            additionalEmploymentCategory: '',
            employmentStatus: '',
            employmentTitle: '',
            employeeId: '',
            primary: true,
            occupationId: 1,
            codeId: '',
            codeName: '',
            overrideDaysWorkedPerWeek: false,
            extensionAttributes: []
          }
        },
        initialRequest: [EventOptionId.ACCOMMODATION]
      }
    },
    {
      id: IntakeSectionId.DETAILS,
      title: {},
      isActive: true,
      isComplete: false,
      skip: false,
      steps: [
        {
          id: 'REQUEST_DETAILS',
          title: {},
          isComplete: false,
          isActive: true
        }
      ]
    }
  ] as IntakeSection<SectionValue>[];

  const mountAccommodationContainer = () => {
    submitStepMock = jest.fn();

    const props = {
      step: { isActive: true, isComplete: false } as IntakeSectionStep<
        AccommodationValue
      >,
      submitStep: submitStepMock,
      sections: sections
    };

    return mount(
      <DefaultContext>
        <AccommodationContainer {...props} />
      </DefaultContext>
    );
  };

  async function addAccommodation(wrapper: ReactWrapper) {
    wrapper
      .find('[data-test-el="add-accommodation-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).not.toExist();
    expect(wrapper.find(WorkplaceAccommodationFormView)).toExist();

    selectAnySelectOption(wrapper, 'accommodationCategory');
    selectAnySelectOption(wrapper, 'accommodationType');

    wrapper
      .find('form[name="workplace-accommodation-form"]')
      .hostNodes()
      .simulate('submit');

    return await releaseEventLoop();
  }

  test('should render', async () => {
    const wrapper = mountAccommodationContainer();
    expect(wrapper.find(AccommodationView)).toExist();
    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).toExist();
  });

  test('should add a new work place accommodation', async () => {
    const wrapper = mountAccommodationContainer();

    await addAccommodation(wrapper);

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).toExist();

    expect(wrapper.find(WorkplaceAccommodationPanel).length).toBe(1);
    expect(
      wrapper
        .find(WorkplaceAccommodationPanel)
        .first()
        .prop('accommodation')
    ).toEqual(
      jasmine.objectContaining({
        accommodationCategory:
          'Physical workplace modifications or workstation relocation',
        accommodationType: 'Widen doorway',
        accommodationDescription: ''
      })
    );
  });

  test('should add multiple work place accommodations', async () => {
    const wrapper = mountAccommodationContainer();

    await addAccommodation(wrapper);

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).toExist();

    wrapper
      .find('[data-test-el="add-another-accommodation-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).not.toExist();
    expect(wrapper.find(WorkplaceAccommodationFormView)).toExist();

    selectAnySelectOption(wrapper, 'accommodationCategory', 1);

    await releaseEventLoop();
    wrapper.update();

    selectAnySelectOption(wrapper, 'accommodationType');

    wrapper
      .find('form[name="workplace-accommodation-form"]')
      .hostNodes()
      .simulate('submit');

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPanel).length).toBe(2);
    expect(
      wrapper
        .find(WorkplaceAccommodationPanel)
        .first()
        .prop('accommodation')
    ).toEqual(
      jasmine.objectContaining({
        accommodationCategory:
          'Physical workplace modifications or workstation relocation',
        accommodationType: 'Widen doorway',
        accommodationDescription: ''
      })
    );
    expect(
      wrapper
        .find(WorkplaceAccommodationPanel)
        .at(1)
        .prop('accommodation')
    ).toEqual(
      jasmine.objectContaining({
        accommodationCategory: 'Policy modification',
        accommodationType: 'Policy modification',
        accommodationDescription: ''
      })
    );
  });

  test('should allow to edit a work place accommodation', async () => {
    const wrapper = mountAccommodationContainer();

    await addAccommodation(wrapper);

    wrapper.update();

    wrapper
      .find('[data-test-el="intake-panel-edit-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).not.toExist();
    expect(wrapper.find(WorkplaceAccommodationFormView)).toExist();

    selectAnySelectOption(wrapper, 'accommodationCategory', 1);

    await releaseEventLoop();
    wrapper.update();

    selectAnySelectOption(wrapper, 'accommodationType');

    wrapper
      .find('form[name="workplace-accommodation-form"]')
      .hostNodes()
      .simulate('submit');

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationPanel).length).toBe(1);
    expect(
      wrapper
        .find(WorkplaceAccommodationPanel)
        .first()
        .prop('accommodation')
    ).toEqual(
      jasmine.objectContaining({
        accommodationCategory: 'Policy modification',
        accommodationType: 'Policy modification',
        accommodationDescription: ''
      })
    );
  });

  test('should allow to remove a work place accommodation', async () => {
    const wrapper = mountAccommodationContainer();

    await addAccommodation(wrapper);

    wrapper.update();

    wrapper
      .find('[data-test-el="intake-panel-delete-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();
    expect(wrapper.find(WorkplaceAccommodationPanel).length).toBe(0);
    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).toExist();
  });

  test('should allow to cancel from the work place accommodation form', async () => {
    const wrapper = mountAccommodationContainer();

    wrapper
      .find('[data-test-el="add-accommodation-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationFormView)).toExist();

    wrapper
      .find('[data-test-el="cancel-workplace-accommodation"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkplaceAccommodationFormView)).not.toExist();
    expect(wrapper.find(WorkplaceAccommodationPlaceholder)).toExist();
  });

  describe('submit', () => {
    const startAccommodation = startAccommodationSpy;
    let completeIntake: typeof IntakeAccommodationSubmissionService.prototype.completeIntake;

    beforeEach(() => {
      const submissionService = IntakeAccommodationSubmissionService.getInstance();
      completeIntake = submissionService.completeIntake;
      submissionService.completeIntake = jest
        .fn()
        .mockImplementation((model, result) => Promise.resolve(result));
    });

    afterEach(() => {
      IntakeAccommodationSubmissionService.getInstance().completeIntake = completeIntake;
    });

    test('step submit', async () => {
      const wrapper = mountAccommodationContainer();

      await addAccommodation(wrapper);
      wrapper.update();

      expect(
        wrapper.find('[data-test-el="submit-accommodation"]').hostNodes()
      ).not.toBeDisabled();

      (wrapper
        .find('AccommodationContainer')
        .instance() as AccommodationContainer).handleStepSubmit({
        pregnancyRelated: 'YES',
        limitations: ['test-limitation'],
        workPlaceAccommodations: [
          {
            accommodationCategory:
              'Physical workplace modifications or workstation relocation',
            accommodationType: 'Widen doorway',
            accommodationDescription: ''
          }
        ],
        additionalNotes: ''
      });

      await releaseEventLoop();

      expect(submitStepMock).toHaveBeenCalled();
    });
  });
});
