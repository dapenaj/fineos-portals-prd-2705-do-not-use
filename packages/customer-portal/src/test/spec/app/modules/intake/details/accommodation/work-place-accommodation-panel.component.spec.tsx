import React from 'react';
import { noop as _noop } from 'lodash';
import { WorkPlaceAccommodation } from 'fineos-js-api-client';

import { shallow } from '../../../../../../config';
import { WorkplaceAccommodationPanel } from '../../../../../../../app/modules/intake';

describe('WorkplaceAccommodationPanel', () => {
  test('render when enabled -- SNAPSHOT', () => {
    const accommodation: WorkPlaceAccommodation = {
      accommodationCategory:
        'Physical workplace modifications or workstation relocation',
      accommodationType: 'Widen doorway',
      accommodationDescription: ''
    };

    expect(
      shallow(
        <WorkplaceAccommodationPanel
          accommodation={accommodation}
          onDelete={_noop}
          onEdit={_noop}
          disabled={false}
        />
      )
    ).toMatchSnapshot();
  });

  test('render when other -- SNAPSHOT', () => {
    const accommodation: WorkPlaceAccommodation = {
      accommodationCategory: 'Other accommodation',
      accommodationType: 'Other',
      accommodationDescription: 'Broken Leg'
    };

    expect(
      shallow(
        <WorkplaceAccommodationPanel
          accommodation={accommodation}
          onDelete={_noop}
          onEdit={_noop}
          disabled={true}
        />
      )
    ).toMatchSnapshot();
  });
});
