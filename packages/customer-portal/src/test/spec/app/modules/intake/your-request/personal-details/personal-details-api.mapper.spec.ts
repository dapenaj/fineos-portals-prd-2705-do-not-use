import { Customer, Contact } from 'fineos-js-api-client';

import {
  mapCustomerDetails,
  PersonalDetailsFormValue,
  mapCustomerContactDetails
} from '../../../../../../../app/modules/intake/your-request';
import {
  customerFixture,
  contactFixture
} from '../../../../../../common/fixtures';

describe('mapCustomerDetails', () => {
  test('it should map a Customer to the form', () => {
    const empty: Partial<PersonalDetailsFormValue> = {
      firstName: '',
      lastName: '',
      dateOfBirth: '',
      premiseNo: '',
      addressLine1: '',
      addressLine2: ``,
      addressLine3: '',
      city: '',
      state: '',
      postCode: '',
      country: 'USA'
    };

    expect(mapCustomerDetails(null)).toStrictEqual(empty);

    expect(mapCustomerDetails({} as Customer)).toStrictEqual(empty);

    expect(mapCustomerDetails(customerFixture)).toStrictEqual({
      addressLine1: 'Main street',
      addressLine2: `Where it's at`,
      addressLine3: 'Two turn tables',
      city: 'And a microphone',
      country: 'USA',
      dateOfBirth: '1988-01-01',
      firstName: 'Jane',
      lastName: 'Doe',
      postCode: 'AB123456',
      premiseNo: '123',
      state: 'CA'
    });
  });
});

describe('mapCustomerContactDetails', () => {
  test('it should map a Contact to the form', () => {
    const empty: Partial<PersonalDetailsFormValue> = {
      email: '',
      phoneNumber: null
    };

    expect(mapCustomerContactDetails(null)).toStrictEqual(empty);

    expect(mapCustomerContactDetails({} as Contact)).toStrictEqual(empty);

    expect(mapCustomerContactDetails(contactFixture)).toStrictEqual({
      email: 'bob@dobalina.com',
      phoneNumber: {
        phoneNumberType: 'Home',
        intCode: '353',
        areaCode: '1',
        telephoneNo: '234567',
        preferred: true
      }
    });
  });
});
