import React from 'react';

import { DefaultContext, mount, shallow } from '../../../../../../../config';
import { releaseEventLoop } from '../../../../../../../common/utils';
import { RequestDetailsForm } from '../../../../../../../../app/modules/intake';
import {
  RequestDetailFormId,
  RequestDetailAnswerType,
  RequestDetailFieldId
} from '../../../../../../../../app/shared/types';

describe('RequestDetailsFormView', () => {
  const mock = jest.fn();
  const submitStepMock = mock;

  const props = {
    forms: [
      {
        id: RequestDetailFormId.PREGNANCY_POSTPARTUM,
        layout: [
          {
            id: RequestDetailFieldId.NEWBORN_COUNT,
            answerType: RequestDetailAnswerType.COUNT
          }
        ]
      }
    ],
    loading: false,
    isActive: true,
    submitStep: submitStepMock
  };

  const mountRequestDetailsView = () => {
    return mount(
      <DefaultContext>
        <RequestDetailsForm {...props} />
      </DefaultContext>
    );
  };

  test('render', async () => {
    const wrapper = mountRequestDetailsView();
    await releaseEventLoop();

    expect(wrapper.find(RequestDetailsForm)).toExist();
  });

  test('submit', async () => {
    const wrapper = mountRequestDetailsView();
    await releaseEventLoop();

    expect(wrapper.find(RequestDetailsForm)).toExist();

    const input = wrapper
      .find('[data-test-el="formik-number-input"] input')
      .first();
    input.simulate('change', {
      persist: () => null,
      target: {
        value: '23'
      }
    });

    wrapper.find('form').simulate('submit');
    await releaseEventLoop();

    expect(props.submitStep).toHaveBeenCalled();
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<RequestDetailsForm {...props} />)).toMatchSnapshot();
  });
});
