import { getTitle } from '../../../../../../../../app/modules/intake/your-request/initial-request/ui';
import { EventOptionId } from '../../../../../../../../app/shared/types';

describe('getTitle', () => {
  it('should return the correct title', () => {
    expect(getTitle(EventOptionId.SICKNESS, 1)).toBe('SICKNESS_ONE');
    expect(getTitle(EventOptionId.SICKNESS, 2)).toBe('SICKNESS_TWO');
  });
});
