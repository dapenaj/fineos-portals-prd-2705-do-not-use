import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow } from '../../../../../../../config';
import { AnotherMedicalProviderPlaceholder } from '../../../../../../../../app/modules/intake/wrap-up';

describe('AnotherMedicalProviderPlaceholder', () => {
  test('render when enabled -- SNAPSHOT', () => {
    expect(
      shallow(
        <AnotherMedicalProviderPlaceholder onAdd={_noop} isDisabled={false} />
      )
    ).toMatchSnapshot();
  });

  test('render when disabled -- SNAPSHOT', () => {
    expect(
      shallow(
        <AnotherMedicalProviderPlaceholder onAdd={_noop} isDisabled={true} />
      )
    ).toMatchSnapshot();
  });
});
