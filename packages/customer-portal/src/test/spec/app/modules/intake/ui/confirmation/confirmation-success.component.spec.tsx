import React from 'react';

import { shallow } from '../../../../../../config';
import { ConfirmationSuccess } from '../../../../../../../app/modules/intake/ui/intake-confirmation/results';
import { ConfirmationType } from '../../../../../../../app/shared/types/confirmation.type';

describe('ConfirmationSuccess', () => {
  const props = {
    content: {
      type: ConfirmationType.Successful,
      header: '',
      phoneNumber: ''
    },
    notificationId: '1234'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<ConfirmationSuccess {...props} />)).toMatchSnapshot();
  });
});
