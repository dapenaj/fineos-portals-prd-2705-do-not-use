import React from 'react';

import { shallow } from '../../../../../config';
import { IntakeHelp } from '../../../../../../app/modules/intake';

describe('IntakeHelp', () => {
  const props = {
    title: 'test',
    message: 'test',
    phoneNumber: 'test'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<IntakeHelp {...props} />)).toMatchSnapshot();
  });
});
