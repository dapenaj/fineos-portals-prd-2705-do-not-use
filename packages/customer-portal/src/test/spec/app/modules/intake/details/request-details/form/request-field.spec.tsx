import React from 'react';
import { ReactWrapper } from 'enzyme';
import { Formik } from 'formik';

import { DefaultContext, mount } from '../../../../../../../config';
import { releaseEventLoop } from '../../../../../../../common/utils';
import { RequestField } from '../../../../../../../../app/modules/intake';
import {
  RequestDetailAnswerType,
  RequestDetailFieldId,
  RequestDetailFieldOption,
  RequestDetailFormId
} from '../../../../../../../../app/shared/types';

describe('RequestField', () => {
  const defaultFieldId = RequestDetailFieldId.PREGNANCY_DELIVERY;
  const defaultProps = {
    isActive: true
  };

  const mountRequestField = (
    answerType: RequestDetailAnswerType,
    options?: RequestDetailFieldOption[]
  ) => {
    return mount(
      <DefaultContext>
        <Formik initialValues={{}} onSubmit={jest.fn()}>
          {props => (
            <form>
              <RequestField
                {...defaultProps}
                {...props}
                field={{
                  id: defaultFieldId,
                  answerType,
                  options
                }}
              />
            </form>
          )}
        </Formik>
      </DefaultContext>
    );
  };

  describe('render', () => {
    let wrapper: ReactWrapper;

    test('should render a text input', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.STRING);
      await releaseEventLoop();
      expect(wrapper.find('[data-test-el="formik-text-input"]')).toExist();
    });

    test('should render a number input', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.COUNT);
      await releaseEventLoop();
      expect(wrapper.find('[data-test-el="formik-number-input"]')).toExist();

      wrapper = mountRequestField(RequestDetailAnswerType.CURRENCY);
      await releaseEventLoop();
      expect(wrapper.find('[data-test-el="formik-number-input"]')).toExist();

      wrapper = mountRequestField(RequestDetailAnswerType.DECIMAL_VALUE);
      await releaseEventLoop();
      expect(wrapper.find('[data-test-el="formik-number-input"]')).toExist();
    });

    test('should render a textarea input', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.LONG_TEXT);
      await releaseEventLoop();
      expect(wrapper.find('[data-test-el="formik-textarea-input"]')).toExist();
    });

    test('should render a date picker', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.DATE);
      await releaseEventLoop();
      expect(
        wrapper.find('[data-test-el="formik-date-picker-input"]')
      ).toExist();
    });

    test('should render a date range picker', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.DATE_RANGE);
      await releaseEventLoop();
      expect(
        wrapper.find('[data-test-el="formik-date-range-input"]')
      ).toExist();
    });

    test('should render a radio group', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.BOOLEAN);
      await releaseEventLoop();
      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();

      wrapper = mountRequestField(RequestDetailAnswerType.OPTIONS, [
        { id: RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL },
        { id: RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION },
        { id: RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN },
        { id: RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE }
      ]);
      await releaseEventLoop();
      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
    });

    test('should render a default component', async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.NONE);
      await releaseEventLoop();
      expect(wrapper.find('[data-test-el="request-field-default"]')).toExist();
    });
  });

  describe('additional fields', () => {
    let wrapper: ReactWrapper;

    const selectRadioGroupOption = async (index: number) => {
      wrapper
        .find('[data-test-el="formik-radio-group-input"] input[type="radio"]')
        .at(index)
        .simulate('change', {
          persist: () => null
        });

      await releaseEventLoop();
      wrapper.update();
    };

    beforeEach(async () => {
      wrapper = mountRequestField(RequestDetailAnswerType.OPTIONS, [
        {
          id: RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          targetFormId:
            RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS
        },
        {
          id: RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
          targetFormId: 'bad-fake' as RequestDetailFormId
        },
        { id: RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE }
      ]);
      await releaseEventLoop();
    });

    test('should render additional fields depending on radio group value', async () => {
      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(
        wrapper.find('[data-test-el="formik-textarea-input"]')
      ).not.toExist();

      selectRadioGroupOption(0);

      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(wrapper.find('[data-test-el="formik-textarea-input"]')).toExist();
    });

    test('should remove additional fields when needed', async () => {
      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(
        wrapper.find('[data-test-el="formik-textarea-input"]')
      ).not.toExist();

      selectRadioGroupOption(0);

      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(wrapper.find('[data-test-el="formik-textarea-input"]')).toExist();

      selectRadioGroupOption(2);

      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(
        wrapper.find('[data-test-el="formik-textarea-input"]')
      ).not.toExist();
    });

    test(`should not render additional fields if they can't be found`, async () => {
      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(
        wrapper.find('[data-test-el="formik-textarea-input"]')
      ).not.toExist();

      selectRadioGroupOption(1);

      expect(
        wrapper.find('[data-test-el="formik-radio-group-input"]')
      ).toExist();
      expect(
        wrapper.find('[data-test-el="formik-textarea-input"]')
      ).not.toExist();
    });
  });
});
