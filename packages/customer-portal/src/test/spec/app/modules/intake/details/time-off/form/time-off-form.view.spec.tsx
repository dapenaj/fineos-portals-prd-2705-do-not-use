import React from 'react';

import { shallow } from '../../../../../../../config';
import {
  TimeOffFormView,
  TimeOffFormViewProps
} from '../../../../../../../../app/modules/intake';

describe('TimeOffFormView', () => {
  const props = {
    isActive: true
  } as TimeOffFormViewProps;

  test('render -- SNAPSHOT', () => {
    expect(shallow(<TimeOffFormView {...props} />)).toMatchSnapshot();
  });
});
