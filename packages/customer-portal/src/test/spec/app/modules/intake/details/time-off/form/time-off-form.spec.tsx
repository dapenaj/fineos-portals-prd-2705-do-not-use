import React from 'react';
import { ReactWrapper } from 'enzyme';
import { AbsenceStatus } from 'fineos-js-api-client';
import { RadioChangeEvent } from 'antd/lib/radio';
import moment from 'moment';

import { DefaultContext, mount, shallow } from '../../../../../../../config';
import {
  releaseEventLoop,
  changeDatePicker,
  submitForm,
  findSelectedRadioOption
} from '../../../../../../../common/utils';
import {
  TimeOffForm,
  TimeOffFormView,
  AddLeavePeriodPlaceholder
} from '../../../../../../../../app/modules/intake';
import {
  LeavePeriodPanel,
  LeavePeriod,
  TimeOffSelectionPopover,
  TimeOffSelectionPopoverProps
} from '../../../../../../../../app/modules/intake/details/time-off';
import { LeavePeriodFormView } from '../../../../../../../../app/modules/intake/details/time-off/form/leave-period-form';
import { validateAdditionalFields } from '../../../../../../../../app/modules/intake/details/time-off/form/time-off-validation';
import {
  TimeOffPeriodType,
  TimeOffLeavePeriod
} from '../../../../../../../../app/shared/types';

describe('RequestDetailsFormView', () => {
  const mock = jest.fn();
  const submitStepMock = mock;

  const props = {
    isActive: true,
    submitStep: submitStepMock,
    submitting: false
  };

  const mountTimeOffForm = () => {
    return mount(
      <DefaultContext>
        <TimeOffForm {...props} />
      </DefaultContext>
    );
  };

  async function openForm(wrapper: ReactWrapper, useAddAnother: boolean) {
    const buttonSelector = useAddAnother
      ? '[data-test-el="add-another-time-off-btn"]'
      : '[data-test-el="add-time-off-btn"]';
    wrapper
      .find(buttonSelector)
      .hostNodes()
      .simulate('click');

    await releaseEventLoop();
    wrapper.update();
  }

  async function addTimeOffPeriod(
    wrapper: ReactWrapper,
    isAnother: boolean = false
  ) {
    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    openForm(wrapper, isAnother);

    expect(wrapper.find(LeavePeriodFormView)).toExist();

    (wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView).props.onSubmit({
      type: TimeOffPeriodType.TIME_OFF,
      leavePeriod: {
        lastDayWorked: '2018-01-01',
        startDateFullDay: true,
        startDate: '2018-01-02',
        endDate: '2018-01-03',
        expectedReturnToWorkDate: '2018-01-04',
        endDateFullDay: true,
        status: AbsenceStatus.Known
      }
    });
  }

  async function addReducedSchedulePeriod(
    wrapper: ReactWrapper,
    isAnother: boolean = false,
    startDate: string = '2018-02-01',
    endDate: string = '2018-02-02'
  ) {
    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    openForm(wrapper, isAnother);

    expect(wrapper.find(LeavePeriodFormView)).toExist();

    (wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView).props.onSubmit({
      type: TimeOffPeriodType.REDUCED_SCHEDULE,
      leavePeriod: {
        startDate,
        endDate,
        status: AbsenceStatus.Known
      }
    });
  }

  async function addEpisodicPeriod(
    wrapper: ReactWrapper,
    isAnother: boolean = false
  ) {
    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    openForm(wrapper, isAnother);

    expect(wrapper.find(LeavePeriodFormView)).toExist();

    (wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView).props.onSubmit({
      type: TimeOffPeriodType.EPISODIC,
      leavePeriod: {
        startDate: '2018-03-01',
        endDate: '2018-03-02'
      }
    });
  }

  async function changeLeavePeriodType(
    wrapper: ReactWrapper,
    type: TimeOffPeriodType
  ) {
    (wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView).handleTypeChange({
      target: { value: type }
    } as RadioChangeEvent);

    await releaseEventLoop();
    wrapper.update();
  }

  test('render', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(TimeOffForm)).toExist();
    expect(wrapper.find(TimeOffFormView)).toExist();
    expect(wrapper.find(AddLeavePeriodPlaceholder)).toExist();
  });

  test('should add a new leave period', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(AddLeavePeriodPlaceholder)).toExist();
    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);

    addTimeOffPeriod(wrapper);
    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(1);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .first()
        .prop('leavePeriod')
    ).toEqual(
      jasmine.objectContaining({
        type: TimeOffPeriodType.TIME_OFF,
        leavePeriod: {
          lastDayWorked: '2018-01-01',
          startDate: '2018-01-02',
          startDateFullDay: true,
          endDate: '2018-01-03',
          endDateFullDay: true,
          expectedReturnToWorkDate: '2018-01-04',
          status: AbsenceStatus.Known
        }
      })
    );
  });

  test('should add multiple types of leave periods', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(AddLeavePeriodPlaceholder)).toExist();
    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);

    addTimeOffPeriod(wrapper);
    await releaseEventLoop();
    wrapper.update();

    addReducedSchedulePeriod(wrapper, true);
    await releaseEventLoop();
    wrapper.update();

    addEpisodicPeriod(wrapper, true);
    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(3);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .at(0)
        .prop('leavePeriod')
    ).toEqual(
      jasmine.objectContaining({
        type: TimeOffPeriodType.TIME_OFF,
        leavePeriod: {
          lastDayWorked: '2018-01-01',
          startDate: '2018-01-02',
          startDateFullDay: true,
          endDate: '2018-01-03',
          endDateFullDay: true,
          expectedReturnToWorkDate: '2018-01-04',
          status: AbsenceStatus.Known
        }
      })
    );
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .at(1)
        .prop('leavePeriod')
    ).toEqual(
      jasmine.objectContaining({
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2018-02-01',
          endDate: '2018-02-02',
          status: AbsenceStatus.Known
        }
      })
    );
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .at(2)
        .prop('leavePeriod')
    ).toEqual(
      jasmine.objectContaining({
        type: TimeOffPeriodType.EPISODIC,
        leavePeriod: {
          startDate: '2018-03-01',
          endDate: '2018-03-02'
        }
      })
    );
  });

  test('should allow editing a leave period', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(AddLeavePeriodPlaceholder)).toExist();
    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);

    addReducedSchedulePeriod(wrapper);

    wrapper.update();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(1);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .first()
        .prop('leavePeriod')
    ).toEqual(
      jasmine.objectContaining({
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2018-02-01',
          endDate: '2018-02-02',
          status: AbsenceStatus.Known
        }
      })
    );

    wrapper
      .find('[data-test-el="intake-panel-edit-btn"]')
      .hostNodes()
      .simulate('click');
    wrapper.update();

    changeDatePicker(wrapper, 'leavePeriod.endDate', '2018-02-11');
    await submitForm(wrapper);

    expect(wrapper.find(LeavePeriodPanel).length).toBe(1);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .first()
        .prop('leavePeriod')
    ).toEqual(
      jasmine.objectContaining({
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2018-02-01',
          endDate: '2018-02-11',
          status: AbsenceStatus.Known
        }
      })
    );
  });

  test('should allow deleting a leave period', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(AddLeavePeriodPlaceholder)).toExist();
    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);

    addReducedSchedulePeriod(wrapper);

    wrapper.update();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(1);

    wrapper
      .find('[data-test-el="intake-panel-delete-btn"]')
      .hostNodes()
      .simulate('click');

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);
  });

  test('should cancel the leave period form', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);

    openForm(wrapper, false);

    expect(wrapper.find(LeavePeriodFormView)).toExist();

    // close
    wrapper
      .find(LeavePeriodFormView)
      .find('[data-test-el="step-cancel"]')
      .hostNodes()
      .simulate('click');
    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);
  });

  test('should display a different set of inputs based on the selected type', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    openForm(wrapper, false);
    expect(wrapper.find(LeavePeriodFormView)).toExist();

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="leave-period-type-input"]'
      )
    ).toHaveValue(TimeOffPeriodType.TIME_OFF);
    expect(wrapper.find('.formGroup').length).toBe(7);

    await changeLeavePeriodType(wrapper, TimeOffPeriodType.REDUCED_SCHEDULE);

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="leave-period-type-input"]'
      )
    ).toHaveValue(TimeOffPeriodType.REDUCED_SCHEDULE);
    expect(wrapper.find('.formGroup').length).toBe(4);

    await changeLeavePeriodType(wrapper, TimeOffPeriodType.EPISODIC);

    expect(
      findSelectedRadioOption(
        wrapper,
        '[data-test-el="leave-period-type-input"]'
      )
    ).toHaveValue(TimeOffPeriodType.EPISODIC);
    expect(wrapper.find('.formGroup').length).toBe(3);
  });

  test('should set/unset values based when startDateFull changes', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    openForm(wrapper, false);

    const form = wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView;

    let values = form.props.values.leavePeriod as TimeOffLeavePeriod;
    expect(values.startDateOffHours).toBeUndefined();
    expect(values.startDateOffMinutes).toBeUndefined();

    form.handleStartDateFullDayChange(true);

    values = form.props.values.leavePeriod as TimeOffLeavePeriod;
    expect(values.startDateOffHours).toBe(0);
    expect(values.startDateOffMinutes).toBe(0);

    form.handleStartDateFullDayChange(false);

    values = form.props.values.leavePeriod as TimeOffLeavePeriod;
    expect(values.startDateOffHours).toBeUndefined();
    expect(values.startDateOffMinutes).toBeUndefined();

    values.lastDayWorked = '2019-01-01';

    form.handleStartDateFullDayChange(true);

    values = form.props.values.leavePeriod as TimeOffLeavePeriod;
    expect(values.startDateOffHours).toBe(0);
    expect(values.startDateOffMinutes).toBe(0);
    expect(values.startDate).toBe('2019-01-01');

    form.handleStartDateFullDayChange(false);

    values = form.props.values.leavePeriod as TimeOffLeavePeriod;
    expect(values.startDateOffHours).toBeUndefined();
    expect(values.startDateOffMinutes).toBeUndefined();
    expect(values.startDate).toBe('2019-01-02');
  });

  test('should set/unset values based when endDateFull changes', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    openForm(wrapper, false);

    const form = wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView;

    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).endDateOffHours
    ).toBeUndefined();
    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).endDateOffMinutes
    ).toBeUndefined();

    form.handleEndDateFullDayChange(false);

    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).endDateOffHours
    ).toBe(0);
    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).endDateOffMinutes
    ).toBe(0);

    form.props.setFieldValue('leavePeriod.returnToWorkFullDate', '2019-01-01');

    form.handleEndDateFullDayChange(true);

    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).endDateOffHours
    ).toBeUndefined();
    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).endDateOffMinutes
    ).toBeUndefined();
    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod).returnToWorkFullDate
    ).toBeUndefined();
  });

  test('should set/unset values based when returnToWorkFullDate changes', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    openForm(wrapper, false);

    const form = wrapper
      .find(LeavePeriodFormView)
      .instance() as LeavePeriodFormView;

    const newDateString = '2019-02-02';

    form.props.setFieldValue('leavePeriod.endDate', '2019-01-01');
    form.props.setFieldValue(
      'leavePeriod.expectedReturnToWorkDate',
      newDateString
    );

    form.handleReturnToWorkFullDateChange(moment(newDateString), newDateString);

    expect(
      (form.props.values.leavePeriod as TimeOffLeavePeriod)
        .expectedReturnToWorkDate
    ).toBe(newDateString);
    expect(form.props.values.leavePeriod.endDate).toBe(newDateString);
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<TimeOffForm {...props} />)).toMatchSnapshot();
  });

  test('should display a warning if leave periods overlap', async () => {
    const wrapper = mountTimeOffForm();
    await releaseEventLoop();

    expect(wrapper.find(AddLeavePeriodPlaceholder)).toExist();
    expect(wrapper.find(LeavePeriodPanel).length).toBe(0);

    addReducedSchedulePeriod(wrapper, false, '2019-02-01', '2019-02-10');
    await releaseEventLoop();
    wrapper.update();

    addReducedSchedulePeriod(wrapper, true, '2019-02-05', '2019-02-13');
    await releaseEventLoop();
    wrapper.update();

    addReducedSchedulePeriod(wrapper, true, '2019-02-21', '2019-02-26');
    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(LeavePeriodFormView)).not.toExist();

    expect(wrapper.find(LeavePeriodPanel).length).toBe(3);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .at(0)
        .prop('isOverlapping')
    ).toEqual(true);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .at(1)
        .prop('isOverlapping')
    ).toEqual(true);
    expect(
      wrapper
        .find(LeavePeriodPanel)
        .at(2)
        .prop('isOverlapping')
    ).toEqual(false);
  });

  test('should show disabled intermittent leave popover message', async () => {
    const wrapper = mountTimeOffForm();

    const popoverProps = {
      content: 'EPISODIC'
    } as TimeOffSelectionPopoverProps;

    addEpisodicPeriod(wrapper, false);
    await releaseEventLoop();
    wrapper.update();

    openForm(wrapper, true);

    expect(shallow(<TimeOffSelectionPopover {...popoverProps} />)).toExist();
  });
});

describe('RequestDetailsForm - validateAdditionalFields', () => {
  const leavePeriod = {
    type: TimeOffPeriodType.TIME_OFF,
    leavePeriod: {}
  } as LeavePeriod;

  const required = 'COMMON.VALIDATION.REQUIRED';

  test('should require at least hours or minutes for startDate', () => {
    expect(
      validateAdditionalFields('startDateOffHours', undefined, leavePeriod)
    ).toBe(required);
    expect(
      validateAdditionalFields('startDateOffMinutes', undefined, leavePeriod)
    ).toBe(required);

    expect(
      validateAdditionalFields('startDateOffHours', undefined, {
        type: leavePeriod.type,
        leavePeriod: {
          ...leavePeriod.leavePeriod,
          startDateOffHours: 12,
          startDateOffMinutes: undefined
        }
      })
    ).toBeUndefined();

    expect(
      validateAdditionalFields('startDateOffHours', undefined, {
        type: leavePeriod.type,
        leavePeriod: {
          ...leavePeriod.leavePeriod,
          startDateOffHours: undefined,
          startDateOffMinutes: 25
        }
      })
    ).toBeUndefined();
  });

  test('should require at least hours or minutes for endDate', () => {
    expect(
      validateAdditionalFields('endDateOffHours', undefined, leavePeriod)
    ).toBe(required);
    expect(
      validateAdditionalFields('endDateOffMinutes', undefined, leavePeriod)
    ).toBe(required);

    expect(
      validateAdditionalFields('endDateOffHours', undefined, {
        type: leavePeriod.type,
        leavePeriod: {
          ...leavePeriod.leavePeriod,
          endDateOffHours: 12,
          endDateOffMinutes: undefined
        }
      })
    ).toBeUndefined();

    expect(
      validateAdditionalFields('endDateOffHours', undefined, {
        type: leavePeriod.type,
        leavePeriod: {
          ...leavePeriod.leavePeriod,
          endDateOffHours: undefined,
          endDateOffMinutes: 25
        }
      })
    ).toBeUndefined();
  });

  test('should check returnToWorkFullDate has a value', () => {
    expect(
      validateAdditionalFields('returnToWorkFullDate', undefined, leavePeriod)
    ).toBe(required);
    expect(
      validateAdditionalFields('returnToWorkFullDate', null, leavePeriod)
    ).toBe(required);
    expect(
      validateAdditionalFields('returnToWorkFullDate', 'value', leavePeriod)
    ).toBeUndefined();
  });

  test('should return undefined for any other fields', () => {
    expect(
      validateAdditionalFields('anything', 'value', leavePeriod)
    ).toBeUndefined();
  });
});
