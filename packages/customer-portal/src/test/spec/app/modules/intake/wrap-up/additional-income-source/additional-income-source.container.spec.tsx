import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { ClaimService, CustomerService } from 'fineos-js-api-client';
import { noop as _noop } from 'lodash';
import { Radio } from 'antd';

import { DefaultContext } from '../../../../../../config';
import {
  changeAnyInputValue,
  changeDatePicker,
  releaseEventLoop,
  selectAnySelectOption,
  submitForm,
  changeRadioButtonInputValue
} from '../../../../../../common/utils';
import {
  fetchPaymentPreferencesSpy,
  addPaymentPreferenceSpy,
  updatePaymentPreferenceSpy
} from '../../../../../../common/spys';
import {
  IntakeSubmissionService,
  IntakeSubmission
} from '../../../../../../../app/shared/services';
import {
  IntakeSectionStep,
  PaymentTypeLabel,
  PaymentPreferenceAccountType
} from '../../../../../../../app/shared/types';
import { showNotification } from '../../../../../../../app/shared/ui';
import { parseDate } from '../../../../../../../app/shared/utils';
import {
  AdditionalIncomeValue,
  AdditionalIncomeSourceContainer,
  PaymentPreferences,
  PaymentPreferencesOptions,
  AccountDetailsForm,
  AccountDetails,
  AccountDetailsListItem
} from '../../../../../../../app/modules/intake/wrap-up';
import { IncomeSourceForm } from '../../../../../../../app/modules/intake/wrap-up/additional-income-source/income-source-form';
import { EmptyIncomeSource } from '../../../../../../../app/modules/intake/wrap-up/additional-income-source/empty-income-source';
import { IncomeSourceItem } from '../../../../../../../app/modules/intake/wrap-up/additional-income-source/income-source-item';

describe('AdditionalIncomeSourceContainer', () => {
  let submitIntakeWorkflowStepMock: jest.Mock;

  const mountAdditionalIncomeSourceContainer = () => {
    submitIntakeWorkflowStepMock = jest.fn();

    const props = {
      step: { isActive: true, isComplete: false } as IntakeSectionStep<
        AdditionalIncomeValue
      >,
      submitStep: submitIntakeWorkflowStepMock,
      previousSubmissionState: {
        model: {} as IntakeSubmission,
        result: {
          postCaseErrors: [],
          createdAbsence: {},
          createdClaim: { id: 'CLM-43' },
          isClaimRequired: true,
          isAbsenceRequired: true
        }
      }
    };

    return mount(
      <DefaultContext>
        <AdditionalIncomeSourceContainer {...props} />
      </DefaultContext>
    );
  };

  async function addOneDefaultIncomeSource(wrapper: ReactWrapper) {
    wrapper
      .find('[data-test-el="add-new-income"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    selectAnySelectOption(wrapper, 'incomeType');
    selectAnySelectOption(wrapper, 'frequency');

    changeAnyInputValue(wrapper, 'amount', '10');
    changeDatePicker(wrapper, 'startDate', '2018-01-02');
    changeDatePicker(wrapper, 'endDate', '2018-01-03');

    await submitForm(wrapper);
  }

  async function fillAccountDetailsForm(wrapper: ReactWrapper) {
    changeAnyInputValue(wrapper, 'accountName', 'acc-test');
    changeAnyInputValue(wrapper, 'routingNumber', 'routing-test');
    changeAnyInputValue(wrapper, 'accountNo', '4815162342');

    changeRadioButtonInputValue(
      wrapper,
      'accountType',
      PaymentPreferenceAccountType.CHECKING
    );

    await submitForm(wrapper);
  }

  test('should render', async () => {
    fetchPaymentPreferencesSpy('success');
    const wrapper = mountAdditionalIncomeSourceContainer();

    expect(wrapper.find(EmptyIncomeSource)).toExist();
    expect(wrapper.find(PaymentPreferences)).toExist();
  });

  describe('income source', () => {
    test('should add new income source', async () => {
      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      expect(wrapper.find(IncomeSourceItem).length).toBe(1);
      expect(
        wrapper
          .find(IncomeSourceItem)
          .first()
          .prop('incomeSource')
      ).toEqual(
        jasmine.objectContaining({
          frequency: 'Monthly',
          incomeType: 'No Fault Auto Insurance',
          amount: 10,
          startDate: '2018-01-02',
          endDate: '2018-01-03'
        })
      );
    });

    test('should allow to add more then one new income source', async () => {
      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      wrapper.update();
      wrapper
        .find('[data-test-el="add-another-income-source"]')
        .hostNodes()
        .simulate('click');

      selectAnySelectOption(wrapper, 'incomeType');
      selectAnySelectOption(wrapper, 'frequency');

      changeAnyInputValue(wrapper, 'amount', '30.21');
      changeDatePicker(wrapper, 'startDate', '2017-01-02');
      changeDatePicker(wrapper, 'endDate', '2017-01-03');

      await submitForm(wrapper);

      expect(wrapper.find(IncomeSourceItem).length).toBe(2);
      expect(
        wrapper
          .find(IncomeSourceItem)
          .first()
          .prop('incomeSource')
      ).toEqual(
        jasmine.objectContaining({
          frequency: 'Monthly',
          incomeType: 'No Fault Auto Insurance',
          amount: 10,
          startDate: '2018-01-02',
          endDate: '2018-01-03'
        })
      );
      expect(
        wrapper
          .find(IncomeSourceItem)
          .at(1)
          .prop('incomeSource')
      ).toEqual(
        jasmine.objectContaining({
          frequency: 'Monthly',
          incomeType: 'No Fault Auto Insurance',
          amount: 30.21,
          startDate: '2017-01-02',
          endDate: '2017-01-03'
        })
      );
    });

    test('should allow to edit income source', async () => {
      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      wrapper
        .find('[data-test-el="edit-intake-list-item"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      changeAnyInputValue(wrapper, 'amount', '0.12');
      await submitForm(wrapper);

      expect(wrapper.find(IncomeSourceItem).length).toBe(1);
      expect(
        wrapper
          .find(IncomeSourceItem)
          .first()
          .prop('incomeSource')
      ).toEqual(
        jasmine.objectContaining({
          frequency: 'Monthly',
          incomeType: 'No Fault Auto Insurance',
          amount: 0.12,
          startDate: '2018-01-02',
          endDate: '2018-01-03'
        })
      );
    });

    test('should allow to remove income source', async () => {
      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      wrapper
        .find('[data-test-el="delete-intake-list-item"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      expect(wrapper.find(IncomeSourceItem).length).toBe(0);
      expect(wrapper.find(EmptyIncomeSource)).toExist();
    });

    test('should cancel not income source for add', () => {
      const wrapper = mountAdditionalIncomeSourceContainer();

      wrapper
        .find('[data-test-el="add-new-income"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      selectAnySelectOption(wrapper, 'incomeType');
      selectAnySelectOption(wrapper, 'frequency');

      wrapper
        .find('[data-test-el="income-source-form-cancel-control"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(IncomeSourceForm)).not.toExist();
      expect(wrapper.find(EmptyIncomeSource)).toExist();
    });

    test('should cancel not income source for edit', async () => {
      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      wrapper
        .find('[data-test-el="edit-intake-list-item"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      changeAnyInputValue(wrapper, 'amount', '0.12');
      wrapper
        .find('[data-test-el="income-source-form-cancel-control"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(IncomeSourceItem).length).toBe(1);
      expect(
        wrapper
          .find(IncomeSourceItem)
          .first()
          .prop('incomeSource')
      ).toEqual(
        jasmine.objectContaining({
          frequency: 'Monthly',
          incomeType: 'No Fault Auto Insurance',
          amount: 10,
          startDate: '2018-01-02',
          endDate: '2018-01-03'
        })
      );
    });
  });

  describe('Payment Preference', () => {
    test('should render if paymentPreferences API call fails', async () => {
      fetchPaymentPreferencesSpy('failure');

      const wrapper = mountAdditionalIncomeSourceContainer();
      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(PaymentPreferences)).toExist();
      expect(wrapper.find(PaymentPreferencesOptions)).toExist();
      expect(wrapper.find(Radio).length).toBe(2);
    });

    test('should show a list of EFT options if there are EFT Accounts', async () => {
      fetchPaymentPreferencesSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();
      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(PaymentPreferences)).toExist();

      expect(wrapper.find(AccountDetails)).not.toExist();

      changeRadioButtonInputValue(
        wrapper,
        'paymentType',
        PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
      );
      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetails)).toExist();

      expect(wrapper.find(AccountDetailsListItem).length).toBe(3);
    });

    test('should display a modal to add a new Account to the list', async () => {
      fetchPaymentPreferencesSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();
      await releaseEventLoop();
      wrapper.update();

      changeRadioButtonInputValue(
        wrapper,
        'paymentType',
        PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
      );
      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetails)).toExist();
      expect(wrapper.find(AccountDetailsListItem).length).toBe(3);

      wrapper
        .find('[data-test-el="account-details-modal-btn"]')
        .hostNodes()
        .simulate('click');

      await releaseEventLoop();
      wrapper.update();

      changeAnyInputValue(wrapper, 'routingNumber', 'routing-test');
      changeAnyInputValue(wrapper, 'accountNo', '4815162342');

      changeRadioButtonInputValue(
        wrapper,
        'accountType',
        PaymentPreferenceAccountType.CHECKING
      );

      wrapper
        .find('[data-test-el="modal-footer-submit-btn"]')
        .hostNodes()
        .simulate('click');

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetailsListItem).length).toBe(4);
    });

    test('should show the Account Details form if no EFT Accounts are available', async () => {
      fetchPaymentPreferencesSpy('failure');

      const wrapper = mountAdditionalIncomeSourceContainer();
      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetailsForm)).not.toExist();

      changeRadioButtonInputValue(
        wrapper,
        'paymentType',
        PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
      );

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetailsForm)).toExist();
    });

    test('should only enable the submit button when the Account Details form is filled', async () => {
      fetchPaymentPreferencesSpy('failure');

      const wrapper = mountAdditionalIncomeSourceContainer();
      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetailsForm)).not.toExist();

      changeRadioButtonInputValue(
        wrapper,
        'paymentType',
        PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
      );

      await releaseEventLoop();
      wrapper.update();

      expect(wrapper.find(AccountDetailsForm)).toExist();

      expect(
        wrapper
          .find('[data-test-el="account-details-form-view-submit-btn"]')
          .hostNodes()
          .prop('disabled')
      ).toBe(false);

      changeAnyInputValue(wrapper, 'accountName', 'acc-test');
      changeAnyInputValue(wrapper, 'routingNumber', 'routing-test');
      changeAnyInputValue(wrapper, 'accountNo', '4815162342');
      changeRadioButtonInputValue(
        wrapper,
        'accountType',
        PaymentPreferenceAccountType.CHECKING
      );

      expect(
        wrapper
          .find('[data-test-el="account-details-form-view-submit-btn"]')
          .hostNodes()
          .prop('disabled')
      ).toBe(false);
    });
  });

  describe('submit', () => {
    const addOtherIncomeSourceMock = jest.fn();
    const addPaymentPreferenceMock = jest.fn();
    const updatePaymentPreferenceMock = jest.fn();
    let originalAddOtherIncomeSource: typeof ClaimService.prototype.addOtherIncomeSource;
    let originalIntakeSubmissionServiceCompleteIntake: typeof IntakeSubmissionService.prototype.completeIntake;
    let originalAddPaymentPreference: typeof CustomerService.prototype.addPaymentPreference;
    let originalUpdatePaymentPreference: typeof CustomerService.prototype.updatePaymentPreference;

    beforeEach(() => {
      // mock completeIntake
      const intakeSubmissionService = IntakeSubmissionService.getInstance();
      originalIntakeSubmissionServiceCompleteIntake =
        intakeSubmissionService.completeIntake;
      intakeSubmissionService.completeIntake = jest
        .fn()
        .mockImplementation((model, result) => Promise.resolve(result));

      // mock addOtherIncomeSource
      originalAddOtherIncomeSource = ClaimService.getInstance()
        .addOtherIncomeSource;
      ClaimService.getInstance().addOtherIncomeSource = addOtherIncomeSourceMock;

      // mock addPaymentPreference
      originalAddPaymentPreference = CustomerService.getInstance()
        .addPaymentPreference;
      CustomerService.getInstance().addPaymentPreference = addPaymentPreferenceMock;

      // mock updatePaymentPreference
      originalUpdatePaymentPreference = CustomerService.getInstance()
        .updatePaymentPreference;
      CustomerService.getInstance().updatePaymentPreference = updatePaymentPreferenceMock;
    });

    afterEach(() => {
      IntakeSubmissionService.getInstance().completeIntake = originalIntakeSubmissionServiceCompleteIntake;
      ClaimService.getInstance().addOtherIncomeSource = originalAddOtherIncomeSource;
      CustomerService.getInstance().addPaymentPreference = originalAddPaymentPreference;
      CustomerService.getInstance().updatePaymentPreference = originalUpdatePaymentPreference;
    });

    test('should send data to ClaimService', async () => {
      fetchPaymentPreferencesSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();
      await releaseEventLoop();
      wrapper.update();

      await addOneDefaultIncomeSource(wrapper);
      addOtherIncomeSourceMock.mockReturnValueOnce(new Promise(_noop));

      wrapper
        .find('[data-test-el="payment-preferences-save-btn"]')
        .hostNodes()
        .simulate('click');

      expect(addOtherIncomeSourceMock).toBeCalledWith(
        'CLM-43',
        jasmine.objectContaining({
          frequency: 'Monthly',
          incomeType: 'No Fault Auto Insurance',
          amount: 10,
          startDate: parseDate('2018-01-02'),
          endDate: parseDate('2018-01-03')
        })
      );
    });

    test('should block UI on submit', async () => {
      fetchPaymentPreferencesSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);
      addOtherIncomeSourceMock.mockReturnValueOnce(new Promise(_noop));

      expect(
        wrapper
          .find('[data-test-el="add-another-income-source"]')
          .hostNodes()
          .prop('disabled')
      ).toBe(false);

      expect(
        wrapper
          .find('[data-test-el="payment-preferences-save-btn"]')
          .hostNodes()
          .prop('disabled')
      ).toBe(false);

      expect(wrapper.find(IncomeSourceItem).prop('isDisabled')).toBe(false);

      wrapper
        .find('[data-test-el="payment-preferences-save-btn"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      expect(
        wrapper
          .find('[data-test-el="payment-preferences-save-btn"]')
          .hostNodes()
          .prop('disabled')
      ).toBe(true);

      expect(
        wrapper
          .find('[data-test-el="add-another-income-source"]')
          .hostNodes()
          .prop('disabled')
      ).toBe(true);

      expect(wrapper.find(IncomeSourceItem).prop('isDisabled')).toBe(true);
    });

    test('should invoke submitIntakeWorkflowStep after success submitting', async () => {
      fetchPaymentPreferencesSpy('success');
      updatePaymentPreferenceSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      addOtherIncomeSourceMock.mockReturnValueOnce(Promise.resolve(true));

      wrapper
        .find('[data-test-el="payment-preferences-save-btn"]')
        .hostNodes()
        .simulate('click');

      await releaseEventLoop();

      expect(submitIntakeWorkflowStepMock).toBeCalled();
    });

    test('should update payment preferences during submitting', async () => {
      fetchPaymentPreferencesSpy('success');
      updatePaymentPreferenceSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);
      addOtherIncomeSourceMock.mockReturnValueOnce(Promise.resolve(true));

      wrapper
        .find('[data-test-el="payment-preferences-save-btn"]')
        .hostNodes()
        .simulate('click');

      await releaseEventLoop();

      expect(
        CustomerService.getInstance().updatePaymentPreference
      ).toBeCalledWith(
        {
          paymentMethod: 'Hand Typed Check',
          description: 'Check from Jon',
          effectiveFrom: '',
          effectiveTo: '',
          isDefault: true,
          chequeDetails: {
            nameToPrintOnCheck: ''
          },
          nominatedPayee: ''
        },
        '201'
      );
    });

    test('should add a new payment preference during submitting', async () => {
      fetchPaymentPreferencesSpy('failure');
      addPaymentPreferenceSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);
      addOtherIncomeSourceMock.mockReturnValueOnce(Promise.resolve(true));

      // switch to EFT and fill out the form
      changeRadioButtonInputValue(
        wrapper,
        'paymentType',
        PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
      );

      await releaseEventLoop();
      wrapper.update();

      // filling out the form also submits the step
      await fillAccountDetailsForm(wrapper);

      await releaseEventLoop();

      expect(CustomerService.getInstance().addPaymentPreference).toBeCalledWith(
        {
          paymentMethod: 'Elec Funds Transfer',
          isDefault: true,
          accountDetails: {
            accountName: 'acc-test',
            routingNumber: 'routing-test',
            accountNo: '4815162342',
            accountType: 'CHECKING'
          }
        }
      );
    });

    test('should complete intake after success submitting', async () => {
      fetchPaymentPreferencesSpy('success');
      updatePaymentPreferenceSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);

      addOtherIncomeSourceMock.mockReturnValueOnce(Promise.resolve(true));

      wrapper
        .find('[data-test-el="payment-preferences-save-btn"]')
        .hostNodes()
        .simulate('click');

      await releaseEventLoop();

      expect(
        IntakeSubmissionService.getInstance().completeIntake
      ).toBeCalledWith(
        {},
        {
          postCaseErrors: [],
          createdAbsence: {},
          createdClaim: { id: 'CLM-43' },
          isClaimRequired: true,
          isAbsenceRequired: true
        },
        {
          additionalDetailsValue: [
            {
              amount: 10,
              endDate: '2018-01-03',
              frequency: 'Monthly',
              incomeType: 'No Fault Auto Insurance',
              startDate: '2018-01-02'
            }
          ]
        }
      );
    });

    test('should go next after failed submit with updated postCaseErrors', async () => {
      fetchPaymentPreferencesSpy('success');
      updatePaymentPreferenceSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();

      await addOneDefaultIncomeSource(wrapper);
      addOtherIncomeSourceMock.mockReturnValueOnce(Promise.reject('error'));

      wrapper
        .find('[data-test-el="payment-preferences-save-btn"]')
        .hostNodes()
        .simulate('click');

      await releaseEventLoop();

      expect(submitIntakeWorkflowStepMock).toBeCalledWith({
        isActive: true,
        isComplete: false,
        value: {
          incomeSources: [
            {
              frequency: 'Monthly',
              incomeType: 'No Fault Auto Insurance',
              amount: 10,
              startDate: '2018-01-02',
              endDate: '2018-01-03'
            }
          ],
          submissionState: {
            model: {},
            result: {
              postCaseErrors: ['error'],
              createdAbsence: {},
              createdClaim: { id: 'CLM-43' },
              isClaimRequired: true,
              isAbsenceRequired: true
            }
          }
        }
      });
    });

    test('should render showNotification', async () => {
      fetchPaymentPreferencesSpy('success');

      const wrapper = mountAdditionalIncomeSourceContainer();
      expect(wrapper.find(showNotification));
    });
  });
});
