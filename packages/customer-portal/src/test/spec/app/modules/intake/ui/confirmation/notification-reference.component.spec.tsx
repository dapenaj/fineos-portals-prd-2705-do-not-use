import React from 'react';

import { shallow } from '../../../../../../config';
import { NotificationReference } from '../../../../../../../app/modules/intake/ui/intake-confirmation/results';

describe('ConfirmationSuccess', () => {
  const props = {
    notificationId: '1',
    headerText: 'test',
    footerText: 'test'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<NotificationReference {...props} />)).toMatchSnapshot();
  });
});
