import React from 'react';
import { SdkConfigProps } from 'fineos-js-api-client';

import { DefaultContext, mount } from '../../../../../../config';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  Role,
  IntakeSectionStep,
  IntakeSectionStepId,
  IntakeSection
} from '../../../../../../../app/shared/types';
import {
  AuthorizationService,
  IntakeSubmissionService,
  IntakeSubmissionResult
} from '../../../../../../../app/shared/services';
import { SectionValue } from '../../../../../../../app/store/intake/workflow';
import {
  TimeOffContainer,
  TimeOffView,
  TimeOffValue,
  TimeOffForm,
  TimeOffClaimsForm
} from '../../../../../../../app/modules/intake';

describe('TimeOff', () => {
  const authorizationService: AuthorizationService = AuthorizationService.getInstance();
  const intakeService: IntakeSubmissionService = IntakeSubmissionService.getInstance();

  const processMock = jest.spyOn(intakeService, 'process');
  processMock.mockReturnValue(
    Promise.resolve({
      createdClaim: {
        id: 'CLM123'
      }
    } as IntakeSubmissionResult)
  );

  const submitStepMock = jest.fn();

  const sections = [
    {
      id: 0,
      title: {},
      isActive: false,
      isComplete: true,
      steps: [
        {
          id: 'PERSONAL_DETAILS',
          title: {},
          isComplete: true,
          isActive: false,
          value: {
            detail: {
              firstName: 'Jane',
              lastName: 'Doe',
              secondName: '',
              initials: 'JD',
              needsInterpretor: false,
              placeOfBirth: '',
              gender: 'Female',
              dateOfBirth: '1988-01-01',
              maritalStatus: 'Single',
              nationality: 'Unknown',
              title: 'Ms',
              preferredContactMethod: 'Unknown',
              idNumber: '741852963',
              identificationNumberType: 'ID',
              securedClient: true,
              staff: true,
              partyType: 'party',
              customerAddress: {
                address: {
                  premiseNo: '123',
                  addressLine1: 'Main street',
                  addressLine2: `Where it's at`,
                  addressLine3: 'Two turn tables',
                  addressLine4: 'And a microphone',
                  addressLine5: '',
                  addressLine6: 'CA',
                  addressLine7: '',
                  postCode: 'AB123456',
                  country: 'USA'
                }
              }
            },
            contact: {
              emailAddresses: [
                {
                  preferred: true,
                  emailAddress: 'bob@dobalina.com'
                }
              ],
              phoneNumbers: [
                {
                  phoneNumberType: 'Home',
                  intCode: '353',
                  areaCode: '1',
                  telephoneNo: '234567',
                  preferred: true
                }
              ]
            }
          }
        },
        {
          id: 'WORK_DETAILS',
          title: {},
          isComplete: true,
          isActive: false,
          value: {
            occupation: {
              dateJobBegan: '2014-02-02',
              dateJobEnded: '',
              daysWorkedPerWeek: 5,
              employer: 'FinCorp Ltd',
              endPosReason: '',
              hoursWorkedPerWeek: 40,
              jobDesc: '',
              jobTitle: 'Line Supervisor',
              remarks: '',
              selfEmployed: false,
              workScheduleDescription: '',
              employmentCategory: '',
              jobStrenuous: '',
              endEmploymentReason: '',
              employmentLocation: '',
              additionalEmploymentCategory: '',
              employmentStatus: '',
              employmentTitle: '',
              employeeId: '',
              primary: true,
              occupationId: 1,
              codeId: '',
              codeName: '',
              overrideDaysWorkedPerWeek: false,
              extensionAttributes: []
            }
          }
        },
        {
          id: 'INITIAL_REQUEST',
          title: {},
          isComplete: true,
          isActive: false,
          value: ['PREGNANCY', 'PREGNANCY_PRENATAL', 'PREGNANCY_PRENATAL_CARE']
        }
      ],
      value: {
        personalDetails: {
          detail: {
            firstName: 'Jane',
            lastName: 'Doe',
            secondName: '',
            initials: 'JD',
            needsInterpretor: false,
            placeOfBirth: '',
            gender: 'Female',
            dateOfBirth: '1988-01-01',
            maritalStatus: 'Single',
            nationality: 'Unknown',
            title: 'Ms',
            preferredContactMethod: 'Unknown',
            idNumber: '741852963',
            identificationNumberType: 'ID',
            securedClient: true,
            staff: true,
            partyType: 'party',
            customerAddress: {
              address: {
                premiseNo: '123',
                addressLine1: 'Main street',
                addressLine2: `Where it's at`,
                addressLine3: 'Two turn tables',
                addressLine4: 'And a microphone',
                addressLine5: '',
                addressLine6: 'CA',
                addressLine7: '',
                postCode: 'AB123456',
                country: 'USA'
              }
            }
          },
          contact: {
            emailAddresses: [
              {
                preferred: true,
                emailAddress: 'bob@dobalina.com'
              }
            ],
            phoneNumbers: [
              {
                phoneNumberType: 'Home',
                intCode: '353',
                areaCode: '1',
                telephoneNo: '234567',
                preferred: true
              }
            ]
          }
        },
        workDetails: {
          occupation: {
            dateJobBegan: '2014-02-02',
            dateJobEnded: '',
            daysWorkedPerWeek: 5,
            employer: 'FinCorp Ltd',
            endPosReason: '',
            hoursWorkedPerWeek: 40,
            jobDesc: '',
            jobTitle: 'Line Supervisor',
            remarks: '',
            selfEmployed: false,
            workScheduleDescription: '',
            employmentCategory: '',
            jobStrenuous: '',
            endEmploymentReason: '',
            employmentLocation: '',
            additionalEmploymentCategory: '',
            employmentStatus: '',
            employmentTitle: '',
            employeeId: '',
            primary: true,
            occupationId: 1,
            codeId: '',
            codeName: '',
            overrideDaysWorkedPerWeek: false,
            extensionAttributes: []
          }
        },
        initialRequest: [
          'PREGNANCY',
          'PREGNANCY_PRENATAL',
          'PREGNANCY_PRENATAL_CARE'
        ]
      }
    },
    {
      id: 1,
      title: {},
      isActive: true,
      isComplete: false,
      skip: false,
      steps: [
        {
          id: 'REQUEST_DETAILS',
          title: {},
          isComplete: false,
          isActive: true
        },
        {
          id: 'TIME_OFF',
          title: {},
          isComplete: false,
          isActive: false
        }
      ]
    }
  ] as IntakeSection<SectionValue>[];

  const currentSection = {
    id: 1,
    title: {},
    isActive: true,
    isComplete: false,
    skip: false,
    steps: [
      {
        id: 'REQUEST_DETAILS',
        title: {},
        isComplete: true,
        isActive: false,
        value: {
          EXPECTED_DELIVERY_DATE: '2019-08-05'
        }
      },
      {
        id: 'TIME_OFF',
        title: {},
        isComplete: false,
        isActive: true
      }
    ]
  } as IntakeSection<SectionValue>;

  const props = {
    step: {
      id: IntakeSectionStepId.TIME_OFF,
      isComplete: false,
      isActive: false
    } as IntakeSectionStep<TimeOffValue>,
    sections,
    currentSection,
    submitStep: submitStepMock
  };

  const mountTimeOff = () => {
    return mount(
      <DefaultContext>
        <TimeOffContainer {...props} />
      </DefaultContext>
    );
  };

  let originalIntakeSubmissionServiceCompleteIntake: typeof IntakeSubmissionService.prototype.completeIntake;

  beforeEach(() => {
    const intakeSubmissionService = IntakeSubmissionService.getInstance();
    originalIntakeSubmissionServiceCompleteIntake =
      intakeSubmissionService.completeIntake;
    intakeSubmissionService.completeIntake = jest.fn();
  });

  afterEach(() => {
    IntakeSubmissionService.getInstance().completeIntake = originalIntakeSubmissionServiceCompleteIntake;
  });

  describe('- Absence Users', () => {
    beforeEach(() => {
      authorizationService.authenticationService.displayRoles = [
        Role.ABSENCE_USER
      ];
    });

    test('render', async () => {
      const wrapper = mountTimeOff();
      await releaseEventLoop();

      expect(wrapper.find(TimeOffView)).toExist();
      expect(wrapper.find(TimeOffForm)).toExist();
    });

    test('step submit', async () => {
      const wrapper = mountTimeOff();

      await releaseEventLoop();
      wrapper.update();

      (wrapper
        .find('TimeOffContainer')
        .instance() as TimeOffContainer).handleStepSubmit({
        outOfWork: true,
        leavePeriods: []
      });

      await releaseEventLoop();
      wrapper.update();

      expect(submitStepMock).toHaveBeenCalled();
    });
  });

  describe('- Claims Users', () => {
    beforeEach(() => {
      jest
        .spyOn(SdkConfigProps, 'authorizationRole')
        .mockReturnValue(Role.CLAIMS_USER);

      authorizationService.authenticationService.displayRoles = [
        Role.CLAIMS_USER
      ];
    });

    test('render', async () => {
      const wrapper = mountTimeOff();
      await releaseEventLoop();

      expect(wrapper.find(TimeOffView)).toExist();
      expect(wrapper.find(TimeOffClaimsForm)).toExist();
    });

    test('step submit', async () => {
      const wrapper = mountTimeOff();

      await releaseEventLoop();
      wrapper.update();

      (wrapper
        .find('TimeOffContainer')
        .instance() as TimeOffContainer).handleStepSubmit({
        outOfWork: true,
        leavePeriods: []
      });

      await releaseEventLoop();
      wrapper.update();

      expect(submitStepMock).toHaveBeenCalled();
    });
  });

  test('step submit with skip for additional income if no claim created', async () => {
    processMock.mockReturnValue(Promise.resolve({} as IntakeSubmissionResult));

    const wrapper = mountTimeOff();

    await releaseEventLoop();
    wrapper.update();

    (wrapper
      .find('TimeOffContainer')
      .instance() as TimeOffContainer).handleStepSubmit({
      outOfWork: true,
      leavePeriods: []
    });

    await releaseEventLoop();
    wrapper.update();

    expect(submitStepMock).toHaveBeenCalledWith(
      jasmine.objectContaining({
        skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
      })
    );
  });

  test('should complete intake if no claim created', async () => {
    processMock.mockReturnValue(Promise.resolve({} as IntakeSubmissionResult));

    const wrapper = mountTimeOff();

    await releaseEventLoop();
    wrapper.update();

    (wrapper
      .find('TimeOffContainer')
      .instance() as TimeOffContainer).handleStepSubmit({
      outOfWork: true,
      leavePeriods: []
    });

    await releaseEventLoop();
    wrapper.update();

    expect(
      IntakeSubmissionService.getInstance().completeIntake
    ).toHaveBeenCalledWith(
      {
        eventOptions: [
          'PREGNANCY',
          'PREGNANCY_PRENATAL',
          'PREGNANCY_PRENATAL_CARE'
        ],
        personalDetails: {
          addressLine1: 'Main street',
          addressLine2: `Where it's at`,
          addressLine3: 'Two turn tables',
          city: 'And a microphone',
          country: 'USA',
          dateOfBirth: '1988-01-01',
          email: 'bob@dobalina.com',
          firstName: 'Jane',
          lastName: 'Doe',
          phoneNumber: {
            areaCode: '1',
            intCode: '353',
            phoneNumberType: 'Home',
            preferred: true,
            telephoneNo: '234567'
          },
          postCode: 'AB123456',
          premiseNo: '123',
          state: 'CA'
        },
        requestDetailFieldValues: new Map([
          ['EXPECTED_DELIVERY_DATE', '2019-08-05']
        ]),
        selectedTimeOff: {
          episodicLeavePeriods: [],
          outOfWork: true,
          reducedScheduleLeavePeriods: [],
          timeOffLeavePeriods: []
        },
        workDetails: {
          dateJobBegan: '2014-02-02',
          employer: 'FinCorp Ltd',
          jobTitle: 'Line Supervisor'
        }
      },
      {},
      {}
    );
  });
});
