import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow } from '../../../../../../config';
import { IncomeSourceItem } from '../../../../../../../app/modules/intake/wrap-up/additional-income-source/income-source-item';

describe('IncomeSourceItem', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <IncomeSourceItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          incomeSource={{ incomeType: 'Sick Pay' }}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <IncomeSourceItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={true}
          incomeSource={{ incomeType: 'Sick Pay' }}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <IncomeSourceItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          incomeSource={{
            incomeType: 'Sick Pay',
            amount: 10
          }}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <IncomeSourceItem
          onEdit={_noop}
          onDelete={_noop}
          isDisabled={false}
          incomeSource={{
            incomeType: 'Sick Pay',
            amount: 10,
            frequency: 'Daily'
          }}
        />
      )
    ).toMatchSnapshot();
  });
});
