import React from 'react';

import { shallow } from '../../../../../../../config';
import {
  TimeOffClaimsFormView,
  TimeOffClaimsFormViewProps
} from '../../../../../../../../app/modules/intake';

describe('TimeOffFormView', () => {
  const props = {
    isActive: true
  } as TimeOffClaimsFormViewProps;

  test('render -- SNAPSHOT', () => {
    expect(shallow(<TimeOffClaimsFormView {...props} />)).toMatchSnapshot();
  });
});
