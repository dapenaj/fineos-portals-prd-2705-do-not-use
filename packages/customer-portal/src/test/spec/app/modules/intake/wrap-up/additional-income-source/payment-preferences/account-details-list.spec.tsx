import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow, mount, DefaultContext } from '../../../../../../../config';
import { paymentPreferenceFixture } from '../../../../../../../common/fixtures';
import {
  AccountDetailsList,
  AccountDetailsFormValue
} from '../../../../../../../../app/modules/intake/wrap-up/additional-income-source/payment-preferences';

describe('EFT Accounts List Component', () => {
  const accounts: AccountDetailsFormValue[] = paymentPreferenceFixture
    .filter(preference => !!preference.accountDetails)
    .map(({ accountDetails, paymentPreferenceId }) => {
      return {
        ...accountDetails,
        paymentPreferenceId
      } as AccountDetailsFormValue;
    });

  const onEftAccountSelectedMock = jest.fn();

  const props = {
    accounts: accounts,
    isDisabled: false,
    onEftAccountSelected: onEftAccountSelectedMock
  };

  test('should call onEftAccountSelected when the selected EFT account changes', () => {
    const wrapper = mount(
      <DefaultContext>
        <AccountDetailsList {...props} />
      </DefaultContext>
    );

    expect(wrapper.find('AccountDetailsList')).toExist();

    const instance = wrapper
      .find('AccountDetailsList')
      .instance() as AccountDetailsList;
    instance.handleEftAccountChange(1);

    expect(onEftAccountSelectedMock).toHaveBeenCalledWith(accounts[1]);
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<AccountDetailsList {...props} />)).toMatchSnapshot();
  });

  test('render - no accounts -- SNAPSHOT', () => {
    expect(
      shallow(<AccountDetailsList {...props} accounts={[]} />)
    ).toMatchSnapshot();
  });

  test('render - no default EFT -- SNAPSHOT', () => {
    expect(
      shallow(<AccountDetailsList {...props} selectedAccount={accounts[2]} />)
    ).toMatchSnapshot();
  });
});
