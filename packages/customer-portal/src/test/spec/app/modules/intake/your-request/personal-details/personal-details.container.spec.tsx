import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  PersonalDetails,
  PersonalDetailsContainer,
  PersonalDetailsView,
  PersonalDetailsValue
} from '../../../../../../../app/modules/intake/your-request';
import { IntakeSectionStep } from '../../../../../../../app/shared/types';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  updateCustomerDetailsSpy,
  updateContactDetailsSpy
} from '../../../../../../common/spys';
import { appConfigFixture } from '../../../../../../common/fixtures';

describe('PersonalDetails', () => {
  const props = {
    step: { isActive: true, isComplete: false } as IntakeSectionStep<
      PersonalDetailsValue
    >,
    loading: false,
    saving: false
  };

  const updateDetailsSpy = updateCustomerDetailsSpy;
  const updateContactSpy = updateContactDetailsSpy;

  const mountPersonalDetails = () => {
    return mount(
      <DefaultContext>
        <PersonalDetails {...props} />
      </DefaultContext>
    );
  };

  test('render', () => {
    const wrapper = mountPersonalDetails();

    wrapper.update();

    expect(wrapper.find(PersonalDetailsView)).toExist();
  });

  test('step submit', async () => {
    const wrapper = mountPersonalDetails();
    const instance = wrapper
      .find(PersonalDetailsContainer)
      .instance() as PersonalDetailsContainer;

    wrapper.update();

    instance.handleStepSubmit({
      firstName: 'Test Fist Name',
      lastName: 'Test Last Name',
      dateOfBirth: 'Test Date of Birth',
      email: 'Test Email',
      phoneNumber: {
        intCode: '00',
        areaCode: '000',
        telephoneNo: '000000',
        preferred: true,
        phoneNumberType: ''
      },
      premiseNo: 'Test PremiseNo',
      addressLine1: 'Test Address Line 1',
      addressLine2: 'Test Address Line 2',
      addressLine3: 'Test Address Line 3',
      city: 'Test City',
      state: 'Test State',
      postCode: 'Test Post Code',
      country: 'Test Country'
    });

    // update customer details
    await releaseEventLoop();
    // update customer contact details
    await releaseEventLoop();

    expect(updateDetailsSpy).toHaveBeenCalled();
    expect(updateContactSpy).toHaveBeenCalled();

    wrapper.update();
  });
});
