import { LeavePeriod } from '../../../../../../../../app/modules/intake/details/time-off';
import { checkIfOverlappingLeavePeriod } from '../../../../../../../../app/modules/intake/details/time-off/form/time-off-validation';
import { TimeOffPeriodType } from '../../../../../../../../app/shared/types';

describe('Validating Overlapping Leave Periods', () => {
  test('should find overlaps with standard leave periods', () => {
    const testPeriods = [
      {
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2019-03-15',
          endDate: '2019-03-25'
        }
      },
      {
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2019-03-28',
          endDate: '2019-03-29'
        }
      },
      {
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2019-03-01',
          endDate: '2019-03-05'
        }
      }
    ] as LeavePeriod[];

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.REDUCED_SCHEDULE,
          leavePeriod: {
            startDate: '2019-03-10',
            endDate: '2019-03-20'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(true);

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.REDUCED_SCHEDULE,
          leavePeriod: {
            startDate: '2019-03-07',
            endDate: '2019-03-10'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(false);
  });

  test('should find overlaps when no end dates are provided', () => {
    const testPeriods = [
      {
        type: TimeOffPeriodType.EPISODIC,
        leavePeriod: {
          startDate: '2019-03-15'
        }
      },
      {
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2019-03-01',
          endDate: '2019-03-05'
        }
      }
    ] as LeavePeriod[];

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.EPISODIC,
          leavePeriod: {
            startDate: '2019-03-10'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(true);

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.EPISODIC,
          leavePeriod: {
            startDate: '2019-03-20'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(true);
  });

  test('should find overlaps when the current date has no end date', () => {
    const testPeriods = [
      {
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2019-03-15',
          endDate: '2019-03-25'
        }
      },
      {
        type: TimeOffPeriodType.REDUCED_SCHEDULE,
        leavePeriod: {
          startDate: '2019-03-01',
          endDate: '2019-03-05'
        }
      }
    ] as LeavePeriod[];

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.EPISODIC,
          leavePeriod: {
            startDate: '2019-03-10'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(true);

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.EPISODIC,
          leavePeriod: {
            startDate: '2019-03-20'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(true);
  });

  test('should find overlaps when the list of periods have no end dates', () => {
    const testPeriods = [
      {
        type: TimeOffPeriodType.EPISODIC,
        leavePeriod: {
          startDate: '2019-03-25'
        }
      },
      {
        type: TimeOffPeriodType.EPISODIC,
        leavePeriod: {
          startDate: '2019-03-15'
        }
      }
    ] as LeavePeriod[];

    expect(
      checkIfOverlappingLeavePeriod(
        {
          type: TimeOffPeriodType.REDUCED_SCHEDULE,
          leavePeriod: {
            startDate: '2019-03-10',
            endDate: '2019-03-20'
          }
        } as LeavePeriod,
        testPeriods
      )
    ).toBe(true);
  });
});
