import {
  updateSelectedOptions,
  updateOptions
} from '../../../../../../../app/modules/intake/your-request/initial-request/initial-request.util';
import { EventOptionId } from '../../../../../../../app/shared/types';

describe('updateSelectedOptions', () => {
  test('update the selected options', () => {
    const expected = [EventOptionId.ACCIDENT];
    const result = updateSelectedOptions(
      [EventOptionId.SICKNESS],
      EventOptionId.ACCIDENT,
      0
    );
    expect(result).toStrictEqual(expected);
  });
});

describe('updateOptions', () => {
  test('update the selected options', () => {
    const expected = [[EventOptionId.SICKNESS]];
    const result = updateOptions(
      [[EventOptionId.ACCIDENT]],
      [EventOptionId.SICKNESS],
      0
    );
    expect(result).toStrictEqual(expected);
  });
});
