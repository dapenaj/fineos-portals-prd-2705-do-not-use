import React from 'react';
import { FormikProps } from 'formik';

import { shallow } from '../../../../../config';
import { IntakeStepControls } from '../../../../../../app/modules/intake';

describe('IntakeStepControls', () => {
  const mock = jest.fn();
  const props = {
    submitting: false,
    disabled: false,
    submit: mock,
    ...({} as FormikProps<any>)
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<IntakeStepControls {...props} />)).toMatchSnapshot();
  });

  test('render - Cancel -- SNAPSHOT', () => {
    expect(
      shallow(<IntakeStepControls {...props} showCancel={true} />)
    ).toMatchSnapshot();
  });
});
