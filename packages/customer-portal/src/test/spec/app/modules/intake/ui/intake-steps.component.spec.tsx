import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow, DefaultContext, mount } from '../../../../../config';
import {
  IntakeSteps,
  yourRequestSteps,
  detailsSteps,
  wrapUpSteps
} from '../../../../../../app/modules/intake';
import {
  IntakeSectionStep,
  IntakeSectionStepId,
  Role
} from '../../../../../../app/shared/types';
import { AuthorizationService } from '../../../../../../app/shared/services';

describe('IntakeSteps', () => {
  const mock = jest.fn();
  const authorizationService = AuthorizationService.getInstance();

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<IntakeSteps steps={yourRequestSteps} selectStep={mock} />)
    ).toMatchSnapshot();
    expect(
      shallow(<IntakeSteps steps={detailsSteps} selectStep={mock} />)
    ).toMatchSnapshot();
    expect(
      shallow(<IntakeSteps steps={wrapUpSteps} selectStep={mock} />)
    ).toMatchSnapshot();
  });

  test('render - no isActive -- SNAPSHOT', () => {
    const props = {
      steps: [
        {
          id: IntakeSectionStepId.PERSONAL_DETAILS,
          isActive: false,
          isComplete: false,
          title: 'test'
        } as IntakeSectionStep<any>,
        { id: 'test' as IntakeSectionStepId } as IntakeSectionStep<any>
      ],
      selectStep: (step: IntakeSectionStep<any>) => _noop()
    };
    expect(shallow(<IntakeSteps {...props} />)).toMatchSnapshot();
  });

  test('select step', () => {
    const props = {
      steps: yourRequestSteps,
      selectStep: mock
    };

    authorizationService.authenticationService.displayRoles = [
      Role.ABSENCE_USER
    ];

    const wrapper = mount(
      <DefaultContext>
        <IntakeSteps {...props} />
      </DefaultContext>
    );

    wrapper
      .find('div[role="button"]')
      .at(1)
      .simulate('click');

    expect(mock).toHaveBeenCalled();
  });
});
