import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  WorkDetails,
  WorkDetailsView,
  WorkDetailsValue,
  WorkDetailsFormComponent,
  WorkDetailsContainer
} from '../../../../../../../app/modules/intake/your-request';
import { IntakeSectionStep } from '../../../../../../../app/shared/types';
import { releaseEventLoop } from '../../../../../../common/utils';
import { appConfigFixture } from '../../../../../../common/fixtures';

describe('WorkDetails', () => {
  const mock = jest.fn();
  const submitIntakeWorkflowStepMock = mock;

  const props = {
    step: { isActive: true, isComplete: false } as IntakeSectionStep<
      WorkDetailsValue
    >,
    conifg: appConfigFixture,
    loading: false,
    submitIntakeWorkflowStep: submitIntakeWorkflowStepMock
  };

  const mountWorkDetails = () =>
    mount(
      <DefaultContext>
        <WorkDetails {...props} />
      </DefaultContext>
    );

  test('render', async () => {
    const wrapper = mountWorkDetails();

    wrapper.update();

    expect(wrapper.find(WorkDetailsView)).toExist();
  });

  test('step submit and proceed', async () => {
    const wrapper = mountWorkDetails();
    const instance = wrapper
      .find(WorkDetailsContainer)
      .instance() as WorkDetailsContainer;

    const proceedSpy = jest.spyOn(instance, 'handleOnProceed');

    instance.handleOnSubmitStepAndProceed({
      employer: 'test',
      jobTitle: 'test',
      dateJobBegan: '2019-08-12'
    });

    await releaseEventLoop();

    expect(proceedSpy).toHaveBeenCalled();
  });
});
