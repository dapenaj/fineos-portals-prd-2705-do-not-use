import React from 'react';

import { shallow } from '../../../../../config';
import { IntakeSection } from '../../../../../../app/modules/intake';
import { IntakeSection as IntakeSectionModel } from '../../../../../../app/shared/types';

describe('IntakeSection', () => {
  const mock = jest.fn();

  const props = {
    currentSection: { isActive: true, isComplete: true } as IntakeSectionModel<
      any
    >,
    onCancel: mock,
    onSubmit: mock
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<IntakeSection {...props} />)).toMatchSnapshot();
  });
});
