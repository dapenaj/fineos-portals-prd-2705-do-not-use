import React from 'react';

import { shallow } from '../../../../../config';
import { EmptyListPlaceholder } from '../../../../../../app/modules/intake/ui/empty-list-placeholder';

describe('ItemPlaceholder', () => {
  const props = {
    icon: <span>Icon</span>,
    content: <span>Content</span>,
    button: <span>Button</span>
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<EmptyListPlaceholder {...props} />)).toMatchSnapshot();
  });
});
