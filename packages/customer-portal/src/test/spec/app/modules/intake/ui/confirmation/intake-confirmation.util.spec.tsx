import { getConfirmationType } from '../../../../../../../app/modules/intake/ui';
import { ConfirmationType } from '../../../../../../../app/shared/types';

describe('getConfirmationType', () => {
  test('returns the correct confirmation type', () => {
    expect(
      getConfirmationType({
        createdNotificationId: 'CLF-123',
        generalError: '',
        postCaseErrors: [],
        createdAbsence: {},
        createdClaim: { id: 'ABS-123' },
        isClaimRequired: false,
        isAbsenceRequired: true
      })
    ).toBe(ConfirmationType.Successful);

    expect(
      getConfirmationType({
        createdNotificationId: 'CLF-123',
        generalError: 'Error',
        postCaseErrors: [],
        createdAbsence: {},
        createdClaim: { id: 'ABS-123' },
        isClaimRequired: false,
        isAbsenceRequired: true
      })
    ).toBe(ConfirmationType.PartialSuccess);

    expect(
      getConfirmationType({
        generalError: 'Error',
        postCaseErrors: [],
        createdAbsence: {},
        createdClaim: { id: 'ABS-123' },
        isClaimRequired: false,
        isAbsenceRequired: true
      })
    ).toBe(ConfirmationType.Failure);
  });
});
