import React from 'react';
import each from 'jest-each';
import { Store } from 'redux';

import { DefaultContext, mount } from '../../../../../../config';
import {
  InitialRequest,
  InitialRequestView,
  InitialRequestValue
} from '../../../../../../../app/modules/intake/your-request';
import {
  IntakeSectionStep,
  Role,
  EventOptionId,
  IntakeSectionId,
  IntakeSectionStepId
} from '../../../../../../../app/shared/types';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  EventDetailOptionService,
  AuthorizationService
} from '../../../../../../../app/shared/services';
import { buildStore, RootState } from '../../../../../../../app/store';
import { intakeSections } from '../../../../../../../app/modules/intake';

describe('InitialRequest', () => {
  const eventDetailOptionService = EventDetailOptionService.getInstance();
  const authorizationService = AuthorizationService.getInstance();

  const startSpy = jest.spyOn(eventDetailOptionService, 'start');
  const nextSpy = jest.spyOn(eventDetailOptionService, 'next');
  const submitIntakeWorkflowStepMock = jest.fn();

  const props = {
    step: { isActive: true, isComplete: false } as IntakeSectionStep<
      InitialRequestValue
    >,
    submitIntakeWorkflowStep: submitIntakeWorkflowStepMock
  };
  const mountInitialRequest = () =>
    mount(
      <DefaultContext customStore={store}>
        <InitialRequest {...props} />
      </DefaultContext>
    );
  const DETAILS_SECTION_INDEX = intakeSections.findIndex(
    ({ id }) => id === IntakeSectionId.DETAILS
  );
  const WRAP_UP_SECTION_INDEX = intakeSections.findIndex(
    ({ id }) => id === IntakeSectionId.WRAP_UP
  );
  const SUPPORTING_EVIDENCE_STEP_INDEX = intakeSections[
    WRAP_UP_SECTION_INDEX
  ].steps.findIndex(({ id }) => id === IntakeSectionStepId.SUPPORTING_EVIDENCE);
  const ADDITIONAL_INCOME_STEP_INDEX = intakeSections[
    WRAP_UP_SECTION_INDEX
  ].steps.findIndex(
    ({ id }) => id === IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES
  );
  const REQUEST_DETAILS_STEP_INDEX = intakeSections[
    DETAILS_SECTION_INDEX
  ].steps.findIndex(({ id }) => id === IntakeSectionStepId.REQUEST_DETAILS);

  let store: Store<RootState>;

  beforeEach(() => {
    store = buildStore();
    authorizationService.authenticationService.displayRoles = [
      Role.ABSENCE_USER
    ];
  });

  test('render', async () => {
    const wrapper = mountInitialRequest();
    await releaseEventLoop();

    expect(startSpy).toHaveBeenCalled();

    expect(wrapper.find(InitialRequestView)).toExist();
  });

  test('update selected options', async () => {
    const wrapper = mountInitialRequest();
    await releaseEventLoop();

    wrapper
      .find('input[type="radio"]')
      .at(2)
      .simulate('change', { target: { checked: true } });

    expect(nextSpy).toHaveBeenCalledWith([EventOptionId.PREGNANCY]);
  });

  test('step submit', async () => {
    const wrapper = mountInitialRequest();
    await releaseEventLoop();

    wrapper.update();

    wrapper
      .find('input[type="radio"]')
      .at(2)
      .simulate('change', { target: { checked: true } });

    wrapper.update();

    expect(nextSpy).toHaveBeenCalledWith([EventOptionId.PREGNANCY]);

    wrapper.find('button[type="submit"]').simulate('click');
    await releaseEventLoop();

    wrapper.update();
  });

  each([
    [
      [
        EventOptionId.PREGNANCY,
        EventOptionId.PREGNANCY_PRENATAL,
        EventOptionId.PREGNANCY_PRENATAL_CARE
      ]
    ],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_NEWBORN]],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_ADOPTED]],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_FOSTER]]
  ]).test(
    'step submit with skipping supporting evidence step for %s',
    async (path: string[]) => {
      const wrapper = mountInitialRequest();

      await releaseEventLoop();

      wrapper.update();

      for (const pathEntry of path) {
        wrapper
          .find(`input[type="radio"][value="${pathEntry}"]`)
          .simulate('change', { target: { checked: true } });

        wrapper.update();
      }

      wrapper.find('button[type="submit"]').simulate('click');

      await releaseEventLoop();

      const {
        intake: {
          workflow: { sections: workflowSections }
        }
      } = store.getState();

      expect(
        workflowSections[WRAP_UP_SECTION_INDEX].steps[
          SUPPORTING_EVIDENCE_STEP_INDEX
        ]
      ).toEqual(
        jasmine.objectContaining({
          skip: true
        })
      );
    }
  );

  each([
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_NEWBORN]],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_ADOPTED]],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_FOSTER]]
  ]).test(
    'step submit with skipping additional income sources step for %s',
    async (path: string[]) => {
      const wrapper = mountInitialRequest();

      await releaseEventLoop();

      wrapper.update();

      for (const pathEntry of path) {
        wrapper
          .find(`input[type="radio"][value="${pathEntry}"]`)
          .simulate('change', { target: { checked: true } });

        wrapper.update();
      }

      wrapper.find('button[type="submit"]').simulate('click');

      await releaseEventLoop();

      const {
        intake: {
          workflow: { sections: workflowSections }
        }
      } = store.getState();

      expect(
        workflowSections[WRAP_UP_SECTION_INDEX].steps[
          ADDITIONAL_INCOME_STEP_INDEX
        ]
      ).toEqual(
        jasmine.objectContaining({
          skip: true
        })
      );
    }
  );

  each([
    [[EventOptionId.SICKNESS, EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE]],
    [
      [
        EventOptionId.SICKNESS,
        EventOptionId.SICKNESS_TIMEOFF_DONATION,
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD
      ]
    ],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_NEWBORN]],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_ADOPTED]],
    [[EventOptionId.CHILD_BONDING, EventOptionId.CHILD_BONDING_FOSTER]]
  ]).test(
    'step submit with skipping request details step for %s',
    async (path: string[]) => {
      const wrapper = mountInitialRequest();

      await releaseEventLoop();

      wrapper.update();

      for (const pathEntry of path) {
        wrapper
          .find(`input[type="radio"][value="${pathEntry}"]`)
          .simulate('change', { target: { checked: true } });

        wrapper.update();
      }

      wrapper.find('button[type="submit"]').simulate('click');

      await releaseEventLoop();

      const {
        intake: {
          workflow: { sections: workflowSections }
        }
      } = store.getState();

      expect(
        workflowSections[DETAILS_SECTION_INDEX].steps[
          REQUEST_DETAILS_STEP_INDEX
        ]
      ).toEqual(
        jasmine.objectContaining({
          skip: true
        })
      );
    }
  );
});
