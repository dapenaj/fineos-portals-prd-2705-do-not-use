import React from 'react';

import { shallow } from '../../../../../../config';
import { ConfirmationPartialSuccess } from '../../../../../../../app/modules/intake/ui/intake-confirmation/results';
import { ConfirmationType } from '../../../../../../../app/shared/types/confirmation.type';

describe('ConfirmationPartialSuccess', () => {
  const props = {
    content: {
      type: ConfirmationType.PartialSuccess,
      header: '',
      phoneNumber: ''
    },
    notificationId: '1232'
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<ConfirmationPartialSuccess {...props} />)
    ).toMatchSnapshot();
  });
});
