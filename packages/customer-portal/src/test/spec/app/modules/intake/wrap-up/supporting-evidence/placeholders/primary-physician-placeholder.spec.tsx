import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow } from '../../../../../../../config';
import { PrimaryPhysicianPlaceholder } from '../../../../../../../../app/modules/intake/wrap-up/supporting-evidence/placeholders';

describe('PrimaryPhysicianPlaceholder', () => {
  test('render when enabled -- SNAPSHOT', () => {
    expect(
      shallow(<PrimaryPhysicianPlaceholder onAdd={_noop} isDisabled={false} />)
    ).toMatchSnapshot();
  });

  test('render when disabled -- SNAPSHOT', () => {
    expect(
      shallow(<PrimaryPhysicianPlaceholder onAdd={_noop} isDisabled={true} />)
    ).toMatchSnapshot();
  });
});
