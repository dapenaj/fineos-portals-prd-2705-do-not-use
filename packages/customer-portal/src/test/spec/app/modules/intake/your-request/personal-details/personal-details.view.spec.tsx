import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  PersonalDetailsView,
  PersonalDetailsForm
} from '../../../../../../../app/modules/intake/your-request';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  customerFixture,
  contactFixture
} from '../../../../../../common/fixtures';

describe('PersonalDetailsView', () => {
  const mock = jest.fn();
  const submitStepMock = mock;
  const submitIntakeWorkflowStepSpy = mock;

  const props = {
    isActive: true,
    detail: customerFixture,
    contact: contactFixture,
    loading: false,
    saving: false,
    stepSubmit: submitStepMock
  };

  const mountPersonalDetailsView = () => {
    return mount(
      <DefaultContext>
        <PersonalDetailsView {...props} />
      </DefaultContext>
    );
  };

  test('render', async () => {
    const wrapper = mountPersonalDetailsView();
    await releaseEventLoop();

    expect(wrapper.find(PersonalDetailsForm)).toExist();
  });

  test('submit', async () => {
    const wrapper = mountPersonalDetailsView();

    wrapper.find('form').simulate('submit');

    await releaseEventLoop();

    expect(submitIntakeWorkflowStepSpy).toHaveBeenCalled();
  });
});
