import React from 'react';

import { shallow } from '../../../../../config';
import {
  IntakeStep,
  yourRequestSteps
} from '../../../../../../app/modules/intake';
import { IntakeSectionStepId } from '../../../../../../app/shared/types';

describe('IntakeStep', () => {
  const selectStepMock = jest.fn();

  const props = {
    ...yourRequestSteps[0],
    selectStep: selectStepMock
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<IntakeStep {...props} />)).toMatchSnapshot();
  });

  const noIsActiveProps = {
    id: IntakeSectionStepId.PERSONAL_DETAILS,
    isActive: false,
    isComplete: false,
    title: 'test',
    selectStep: selectStepMock
  };

  test('render - no isActive -- SNAPSHOT', () => {
    expect(shallow(<IntakeStep {...noIsActiveProps} />)).toMatchSnapshot();
  });
});
