import { CustomerOccupation } from 'fineos-js-api-client';

import { getCurrentCustomerOccupation } from '../../../../../../../app/modules/intake/your-request';

describe('getCurrentCustomerOccupation', () => {
  test('it returns the most current occupation', () => {
    const occupations = [
      { dateJobBegan: '2014-02-02' } as CustomerOccupation,
      { dateJobBegan: '2014-02-06' } as CustomerOccupation
    ];

    expect(getCurrentCustomerOccupation(occupations)).toBe(occupations[1]);

    const reversed = occupations.reverse();

    expect(getCurrentCustomerOccupation(reversed)).toBe(reversed[0]);

    expect(getCurrentCustomerOccupation([occupations[0]])).toBe(occupations[0]);

    expect(getCurrentCustomerOccupation(null)).toBe(null);
  });
});
