import React from 'react';

import { shallow } from '../../../../../config';
import { IntakeOverviewItem } from '../../../../../../app/modules/intake';
import { IntakeSection } from '../../../../../../app/shared/types';

describe('IntakeOverviewItem', () => {
  const mock = jest.fn();

  const props = {
    section: { isActive: true, isComplete: true } as IntakeSection<any>,
    onClick: mock
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<IntakeOverviewItem {...props} />)).toMatchSnapshot();
  });
});
