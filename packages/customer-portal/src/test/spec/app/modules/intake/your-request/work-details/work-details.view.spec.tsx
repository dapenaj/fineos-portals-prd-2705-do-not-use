import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  WorkDetailsView,
  WorkDetailsRenderer
} from '../../../../../../../app/modules/intake/your-request';
import { releaseEventLoop } from '../../../../../../common/utils';
import { customerOccupationsFixture } from '../../../../../../common/fixtures';

describe('WorkDetails', () => {
  const mock = jest.fn();
  const proceedMock = mock;
  const stepSubmitMock = mock;

  const defaultProps = {
    isActive: true,
    occupation: customerOccupationsFixture[0],
    loading: false,
    onProceed: proceedMock,
    stepSubmit: stepSubmitMock
  };

  const mountWorkDetailsView = (props?: any) =>
    mount(
      <DefaultContext>
        <WorkDetailsView {...defaultProps} {...props} />
      </DefaultContext>
    );

  test('render', async () => {
    const wrapper = mountWorkDetailsView();
    await releaseEventLoop();

    expect(wrapper.find(WorkDetailsRenderer)).toExist();
  });
});
