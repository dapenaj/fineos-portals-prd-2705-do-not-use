import React from 'react';

import Enzyme from '../../../../config/enzyme';
import { DefaultContext, mount } from '../../../../config';
import Intake, { intakeSections } from '../../../../../app/modules/intake';
import { IntakeView } from '../../../../../app/modules/intake/intake.view';
import { AuthorizationService } from '../../../../../app/shared/services';
import { Role } from '../../../../../app/shared/types';

describe('Intake', () => {
  let wrapper: Enzyme.ReactWrapper;

  const mock = jest.fn();
  const sectionClickSpy = mock;
  const submitSpy = mock;
  const cancelSpy = mock;
  const handleStepSelectSpy = mock;
  const authorizationService = AuthorizationService.getInstance();

  const props = {
    sections: intakeSections,
    currentSection: intakeSections[0],
    handleSectionClick: sectionClickSpy,
    handleSubmit: submitSpy,
    handleCancel: cancelSpy,
    handleStepSelect: handleStepSelectSpy
  };

  const mountIntake = () => {
    return mount(
      <DefaultContext>
        <Intake {...props} />
      </DefaultContext>
    );
  };

  beforeEach(() => {
    authorizationService.authenticationService.displayRoles = [
      Role.ABSENCE_USER
    ];

    wrapper = mountIntake();
  });

  test('render', async () => {
    expect(wrapper.find(IntakeView)).toExist();
  });
});
