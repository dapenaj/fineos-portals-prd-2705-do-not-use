import React from 'react';

import { shallow } from '../../../../../../../config';
import {
  RequestDetailsFormView,
  RequestDetailsFormViewProps
} from '../../../../../../../../app/modules/intake';
import {
  RequestDetailFormId,
  RequestDetailAnswerType,
  RequestDetailFieldId
} from '../../../../../../../../app/shared/types';

describe('RequestDetailsFormView', () => {
  const props = {
    forms: [
      {
        id: RequestDetailFormId.PREGNANCY_POSTPARTUM,
        layout: [
          {
            id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.NO
              },
              {
                id: RequestDetailFieldId.YES,
                targetFormId:
                  RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
              }
            ]
          },
          {
            id: RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
            answerType: RequestDetailAnswerType.DATE
          },
          {
            id: RequestDetailFieldId.NEWBORN_COUNT,
            answerType: RequestDetailAnswerType.COUNT
          },
          {
            id: RequestDetailFieldId.PREGNANCY_DELIVERY,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN
              },
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL
              },
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION
              },
              {
                id: RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE
              }
            ]
          },
          {
            id: RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
            answerType: RequestDetailAnswerType.OPTIONS,
            options: [
              {
                id: RequestDetailFieldId.NO
              },
              {
                id: RequestDetailFieldId.YES,
                targetFormId:
                  RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS
              }
            ]
          },
          {
            id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
            answerType: RequestDetailAnswerType.LONG_TEXT
          }
        ]
      }
    ],
    isActive: true
  } as RequestDetailsFormViewProps;

  test('render -- SNAPSHOT', () => {
    expect(shallow(<RequestDetailsFormView {...props} />)).toMatchSnapshot();
  });
});
