import React from 'react';

import { shallow } from '../../../../../../../config';
import { InitialRequestOptionsTitle } from '../../../../../../../../app/modules/intake';
import { getTitle } from '../../../../../../../../app/modules/intake/your-request/initial-request/ui';
import { EventOptionId } from '../../../../../../../../app/shared/types';

describe('InitialRequestOptionsTitle', () => {
  const props = {
    title: getTitle(EventOptionId.SICKNESS, 1)
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<InitialRequestOptionsTitle {...props} />)
    ).toMatchSnapshot();
  });
});
