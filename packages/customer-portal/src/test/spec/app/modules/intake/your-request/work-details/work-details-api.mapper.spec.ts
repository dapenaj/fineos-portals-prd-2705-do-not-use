import { CustomerOccupation } from 'fineos-js-api-client';

import {
  WorkDetailsFormValue,
  mapOccupation
} from '../../../../../../../app/modules/intake/your-request';
import { customerOccupationsFixture } from '../../../../../../common/fixtures';

describe('mapCustomerDetails', () => {
  test('it should map a Customer to the form', () => {
    const empty: Partial<WorkDetailsFormValue> = {
      employer: '',
      jobTitle: '',
      dateJobBegan: ''
    };

    expect(mapOccupation(null)).toStrictEqual(empty);

    expect(mapOccupation({} as CustomerOccupation)).toStrictEqual(empty);

    expect(mapOccupation(customerOccupationsFixture[0])).toStrictEqual({
      employer: 'FinCorp Ltd',
      jobTitle: 'Line Supervisor',
      dateJobBegan: '2014-02-02'
    });
  });
});
