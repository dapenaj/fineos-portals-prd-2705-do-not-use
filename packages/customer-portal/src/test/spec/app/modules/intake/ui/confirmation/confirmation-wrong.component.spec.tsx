import React from 'react';

import { shallow } from '../../../../../../config';
import { ConfirmationWrong } from '../../../../../../../app/modules/intake/ui/intake-confirmation/results';
import { ConfirmationType } from '../../../../../../../app/shared/types/confirmation.type';

describe('ConfirmationWrong', () => {
  const props = {
    content: {
      type: ConfirmationType.Failure,
      header: '',
      phoneNumber: ''
    }
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<ConfirmationWrong {...props} />)).toMatchSnapshot();
  });
});
