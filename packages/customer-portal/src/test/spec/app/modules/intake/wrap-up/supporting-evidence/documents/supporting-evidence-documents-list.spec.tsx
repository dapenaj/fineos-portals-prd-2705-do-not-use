import React from 'react';
import { noop as _noop } from 'lodash';

import { shallow } from '../../../../../../../config';
import { SupportingEvidenceDocumentsList } from '../../../../../../../../app/modules/intake/wrap-up/supporting-evidence';
import { uploadDocumentsFixture } from '../../../../../../../common/fixtures';

describe('SupportingEvidenceDocumentsList', () => {
  test('render -- SNAPSHOT', () => {
    const mock = jest.fn();
    const props = {
      loading: false,
      isActive: true,
      documents: uploadDocumentsFixture,
      onAddDocumentation: mock,
      onDeleteDocumentation: mock
    };

    expect(
      shallow(<SupportingEvidenceDocumentsList {...props} />)
    ).toMatchSnapshot();
  });
});
