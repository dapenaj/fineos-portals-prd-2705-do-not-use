import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { ClaimService } from 'fineos-js-api-client';

import { IntakeSectionStep } from '../../../../../../../app/shared/types';
import { SupportingEvidenceValue } from '../../../../../../../app/modules/intake/wrap-up/wrap-up.type';
import { DefaultContext } from '../../../../../../config';
import {
  AnotherMedicalProviderPlaceholder,
  PrimaryPhysicianPlaceholder
} from '../../../../../../../app/modules/intake/wrap-up/supporting-evidence/placeholders';
import {
  changeAnyInputValue,
  changeSelectValue,
  releaseEventLoop,
  submitForm
} from '../../../../../../common/utils';
import {
  SupportingEvidenceContainer,
  SupportingEvidenceDocuments,
  MedicalProviderItem,
  SupportingEvidenceDocumentsList
} from '../../../../../../../app/modules/intake/wrap-up/supporting-evidence';
import { IntakeSubmission } from '../../../../../../../app/shared/services';
import {
  getSupportingEvidenceSpy,
  uploadSupportingEvidenceSpy,
  uploadCaseDocumentSpy,
  markDocumentAsReadSpy
} from '../../../../../../common/spys';
import { uploadDocumentsFixture } from '../../../../../../common/fixtures';

describe('SupportingEvidence', () => {
  let submitIntakeWorkflowStepMock: jest.Mock;
  let finishIntakeWorkflow: jest.Mock;
  let wrapper: ReactWrapper;

  const mountSupportingEvidence = () => {
    submitIntakeWorkflowStepMock = jest.fn();
    finishIntakeWorkflow = jest.fn();

    const props = {
      step: { isActive: true, isComplete: false } as IntakeSectionStep<
        SupportingEvidenceValue
      >,
      submitIntakeWorkflowStep: submitIntakeWorkflowStepMock,
      finishIntakeWorkflow: finishIntakeWorkflow,
      previousSubmissionState: {
        result: {
          postCaseErrors: [],
          createdAbsence: {},
          createdClaim: { id: 'CLM-43' },
          createdNotificationId: 'NTN-4',
          isClaimRequired: true,
          isAbsenceRequired: false
        },
        model: {} as IntakeSubmission
      },
      intl: {
        formatMessage: jest.fn()
      }
    };

    return mount(
      <DefaultContext>
        <SupportingEvidenceContainer {...props} />
      </DefaultContext>
    );
  };

  const fillExpandedFormWithDefaultValues = async () => {
    changeAnyInputValue(wrapper, 'name', 'Dr John Snow');
    changeAnyInputValue(wrapper, 'phone', '1-866-4871111');
    changeAnyInputValue(wrapper, 'addressLine', 'Evergreen St. 6');
    changeAnyInputValue(wrapper, 'zipCode', '91021');
    changeAnyInputValue(wrapper, 'city', 'Austin');
    changeSelectValue(wrapper, 'state', 'TX');

    await submitForm(wrapper);
  };

  describe('Documentation', () => {
    let instance: SupportingEvidenceContainer;

    const request = {
      documentType: 'test',
      fileName: 'test',
      fileExtension: 'test',
      description: 'test',
      fileContentsText: 'test'
    };

    beforeEach(() => {
      wrapper = mountSupportingEvidence();
      instance = wrapper
        .find(SupportingEvidenceContainer)
        .instance() as SupportingEvidenceContainer;
    });

    test('should render', async () => {
      getSupportingEvidenceSpy('success');

      await releaseEventLoop();

      expect(wrapper.find(SupportingEvidenceDocuments)).toExist();
    });

    test('handleAddDocumentation', async () => {
      getSupportingEvidenceSpy('success');

      await releaseEventLoop();

      instance.handleAddDocumentation({
        ...uploadDocumentsFixture[0],
        request
      });

      await releaseEventLoop();

      expect(instance.state.documents![0].request).toBe(request);
    });

    test('handleDeleteDocumentation', async () => {
      getSupportingEvidenceSpy('success');

      await releaseEventLoop();

      instance.handleAddDocumentation({
        ...uploadDocumentsFixture[0],
        request
      });

      await releaseEventLoop();

      instance.handleDeleteDocumentation({
        ...uploadDocumentsFixture[0],
        request
      });

      await releaseEventLoop();

      expect(instance.state.documents![0].request).toBeUndefined();
    });

    test('submitDocuments', async () => {
      getSupportingEvidenceSpy('success');

      uploadCaseDocumentSpy('success');
      uploadSupportingEvidenceSpy('success');
      markDocumentAsReadSpy('success');

      await releaseEventLoop();

      instance.handleAddDocumentation({
        ...uploadDocumentsFixture[0],
        request
      });

      await releaseEventLoop();

      instance.submitDocuments();
    });
  });

  describe('MedicalProviders', () => {
    beforeEach(() => {
      wrapper = mountSupportingEvidence();
    });

    test('should render', () => {
      expect(wrapper.find(PrimaryPhysicianPlaceholder)).toExist();
      expect(wrapper.find(AnotherMedicalProviderPlaceholder)).toExist();
    });

    describe('PCP', () => {
      test('should be able to identify primary care physician information', async () => {
        wrapper
          .find('[data-test-el="add-primary-physician"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        expect(wrapper.find(PrimaryPhysicianPlaceholder)).not.toExist();

        expect(wrapper.find(MedicalProviderItem)).toExist();
        expect(
          wrapper.find(MedicalProviderItem).prop('medicalProvider')
        ).toEqual({
          name: 'Dr John Snow',
          phone: '1-866-4871111',
          addressLine: 'Evergreen St. 6',
          zipCode: '91021',
          city: 'Austin',
          state: 'TX',
          country: 'USA'
        });
      });

      test('should be able to delete primary care physician information', async () => {
        wrapper
          .find('[data-test-el="add-primary-physician"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        wrapper
          .find('[data-test-el="delete-intake-list-item"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        expect(wrapper.find(MedicalProviderItem)).not.toExist();
        expect(wrapper.find(PrimaryPhysicianPlaceholder)).toExist();
      });

      test('should be able to edit primary care physician information', async () => {
        wrapper
          .find('[data-test-el="add-primary-physician"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        wrapper
          .find('[data-test-el="edit-intake-list-item"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        changeAnyInputValue(wrapper, 'name', 'Dr John Dow');
        changeAnyInputValue(wrapper, 'phone', '2-866-4871111');
        changeAnyInputValue(wrapper, 'city', 'New-York');
        changeAnyInputValue(wrapper, 'addressLine', '');
        changeAnyInputValue(wrapper, 'zipCode', '');
        changeSelectValue(wrapper, 'state', 'NY');
        changeAnyInputValue(wrapper, 'country', 'Canada');

        await submitForm(wrapper);

        wrapper.update();

        expect(
          wrapper.find(MedicalProviderItem).prop('medicalProvider')
        ).toEqual({
          name: 'Dr John Dow',
          phone: '2-866-4871111',
          addressLine: '',
          zipCode: '',
          city: 'New-York',
          state: 'NY',
          country: 'Canada'
        });
      });

      test('allow to proceed with the step', async () => {
        expect(
          wrapper
            .find('[data-test-el="save-and-proceed-supporting-evidence"]')
            .hostNodes()
            .prop('disabled')
        ).toBeFalsy();
      });
    });

    describe('another medical provider', () => {
      test('should be able to add another medical provider information', async () => {
        wrapper
          .find('[data-test-el="add-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        expect(wrapper.find(AnotherMedicalProviderPlaceholder)).not.toExist();

        expect(wrapper.find(MedicalProviderItem).length).toBe(1);
        expect(
          wrapper.find(MedicalProviderItem).prop('medicalProvider')
        ).toEqual({
          name: 'Dr John Snow',
          phone: '1-866-4871111',
          addressLine: 'Evergreen St. 6',
          zipCode: '91021',
          city: 'Austin',
          state: 'TX',
          country: 'USA'
        });
      });

      test('should be able to add multiple medical provider information', async () => {
        wrapper
          .find('[data-test-el="add-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        wrapper
          .find('[data-test-el="add-one-more-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();
        await fillExpandedFormWithDefaultValues();

        wrapper
          .find('[data-test-el="add-one-more-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();
        await fillExpandedFormWithDefaultValues();

        expect(wrapper.find(MedicalProviderItem).length).toBe(3);
      });

      test('should be able to delete another medical provider information', async () => {
        wrapper
          .find('[data-test-el="add-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        wrapper
          .find('[data-test-el="add-one-more-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();
        await fillExpandedFormWithDefaultValues();

        expect(wrapper.find(MedicalProviderItem).length).toBe(2);

        wrapper
          .find('[data-test-el="delete-intake-list-item"]')
          .hostNodes()
          .first()
          .simulate('click');

        wrapper.update();

        expect(wrapper.find(MedicalProviderItem).length).toBe(1);

        wrapper
          .find('[data-test-el="delete-intake-list-item"]')
          .hostNodes()
          .first()
          .simulate('click');

        wrapper.update();

        expect(wrapper.find(MedicalProviderItem)).not.toExist();
        expect(wrapper.find(PrimaryPhysicianPlaceholder)).toExist();
      });

      test('should be able to edit another medical provider information', async () => {
        wrapper
          .find('[data-test-el="add-another-medical-care"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        await fillExpandedFormWithDefaultValues();

        wrapper
          .find('[data-test-el="edit-intake-list-item"]')
          .hostNodes()
          .simulate('click');

        wrapper.update();

        changeAnyInputValue(wrapper, 'name', 'Dr John Dow');
        changeAnyInputValue(wrapper, 'phone', '2-866-4871111');
        changeAnyInputValue(wrapper, 'city', 'New-York');
        changeAnyInputValue(wrapper, 'addressLine', '');
        changeAnyInputValue(wrapper, 'zipCode', '');
        changeSelectValue(wrapper, 'state', 'NY');
        changeAnyInputValue(wrapper, 'country', 'Canada');

        await submitForm(wrapper);

        wrapper.update();

        expect(
          wrapper.find(MedicalProviderItem).prop('medicalProvider')
        ).toEqual({
          name: 'Dr John Dow',
          phone: '2-866-4871111',
          addressLine: '',
          zipCode: '',
          city: 'New-York',
          state: 'NY',
          country: 'Canada'
        });
      });
    });
  });

  describe('flow', () => {
    let originalAddPrimaryCarePhysician: any;
    let originalAddAnotherMedicalProviders: any;
    let mockedAddPrimaryCarePhysician: jest.Mock;
    let mockedAddAnotherMedicalProviders: jest.Mock;

    beforeEach(async () => {
      wrapper = mountSupportingEvidence();

      const claimService = ClaimService.getInstance();
      originalAddPrimaryCarePhysician = claimService.addPrimaryCarePhysician;
      mockedAddPrimaryCarePhysician = jest.fn();
      claimService.addPrimaryCarePhysician = mockedAddPrimaryCarePhysician;

      originalAddAnotherMedicalProviders =
        claimService.addAnotherMedicalProviders;
      mockedAddAnotherMedicalProviders = jest.fn();
      claimService.addAnotherMedicalProviders = mockedAddAnotherMedicalProviders;

      wrapper
        .find('[data-test-el="add-primary-physician"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      await fillExpandedFormWithDefaultValues();

      wrapper
        .find('[data-test-el="add-another-medical-care"]')
        .hostNodes()
        .simulate('click');

      wrapper.update();

      await fillExpandedFormWithDefaultValues();
    });

    afterEach(() => {
      ClaimService.getInstance().addPrimaryCarePhysician = originalAddPrimaryCarePhysician;
      ClaimService.getInstance().addAnotherMedicalProviders = originalAddAnotherMedicalProviders;
    });

    test('should allow to proceed with save', async () => {
      let resolveAddPrimary: () => void;
      let resolveAddAnother: () => void;

      mockedAddPrimaryCarePhysician.mockReturnValueOnce(
        new Promise(f => {
          resolveAddPrimary = f;
        })
      );
      mockedAddAnotherMedicalProviders.mockReturnValueOnce(
        new Promise(f => {
          resolveAddAnother = f;
        })
      );

      wrapper
        .find('[data-test-el="save-and-proceed-supporting-evidence"]')
        .hostNodes()
        .simulate('click');

      expect(mockedAddPrimaryCarePhysician).toBeCalledWith('CLM-43', {
        name: 'Dr John Snow',
        phone: '1-866-4871111',
        addressLine: 'Evergreen St. 6',
        zipCode: '91021',
        city: 'Austin',
        state: 'TX',
        country: 'USA'
      });

      resolveAddPrimary();
      await releaseEventLoop();

      expect(mockedAddAnotherMedicalProviders).toBeCalledWith(
        'CLM-43',
        ...[
          {
            name: 'Dr John Snow',
            phone: '1-866-4871111',
            addressLine: 'Evergreen St. 6',
            zipCode: '91021',
            city: 'Austin',
            state: 'TX',
            country: 'USA'
          }
        ]
      );

      resolveAddAnother();
      await releaseEventLoop();

      expect(submitIntakeWorkflowStepMock).toBeCalled();
    });
  });

  describe('When no claim and absence is not medical related', () => {
    test('should call skipStep', async () => {
      submitIntakeWorkflowStepMock = jest.fn();
      finishIntakeWorkflow = jest.fn();

      // set up our props (with an absence, but no claim)
      const props = {
        step: { isActive: true, isComplete: false } as IntakeSectionStep<
          SupportingEvidenceValue
        >,
        submitIntakeWorkflowStep: submitIntakeWorkflowStepMock,
        finishIntakeWorkflow: finishIntakeWorkflow,
        previousSubmissionState: {
          result: {
            postCaseErrors: [],
            createdAbsence: { id: 'ABS-1', isMedicalProviderRelated: false },
            createdClaim: {},
            createdNotificationId: 'NTN-4',
            isClaimRequired: true,
            isAbsenceRequired: false
          },
          model: {} as IntakeSubmission
        },
        intl: {
          formatMessage: jest.fn()
        }
      };

      // we want this to fail
      getSupportingEvidenceSpy('failure');

      wrapper = mount(
        <DefaultContext>
          <SupportingEvidenceContainer {...props} />
        </DefaultContext>
      );

      const instance = wrapper
        .find(SupportingEvidenceContainer)
        .instance() as SupportingEvidenceContainer;

      jest.spyOn(instance, 'skipStep');

      await releaseEventLoop();

      // a blank UI is shown because our mock function doesn't change the UI
      expect(wrapper.find(SupportingEvidenceDocumentsList)).not.toExist();
      expect(wrapper.find(PrimaryPhysicianPlaceholder)).not.toExist();

      expect(instance.skipStep).toHaveBeenCalled();
      expect(submitIntakeWorkflowStepMock).toHaveBeenCalled();
    });
  });
});
