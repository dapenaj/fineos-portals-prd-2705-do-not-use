import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import {
  WorkDetailsRenderer,
  ExistingWorkDetails,
  WorkDetailsForm
} from '../../../../../../../app/modules/intake/your-request';
import { customerOccupationsFixture } from '../../../../../../common/fixtures';

describe('WorkDetails', () => {
  const mock = jest.fn();
  const submitStepMock = mock;
  const onProceedMock = mock;

  const defaultProps = {
    occupation: customerOccupationsFixture[0],
    loading: false,
    isActive: true,
    submitStep: submitStepMock,
    onProceed: onProceedMock
  };

  const mountWorkDetailsRenderer = (props?: any) => {
    return mount(
      <DefaultContext>
        <WorkDetailsRenderer {...defaultProps} {...props} />
      </DefaultContext>
    );
  };

  test('render', () => {
    const wrapper = mountWorkDetailsRenderer();

    expect(wrapper.find(ExistingWorkDetails)).toExist();
  });

  test('form states', async () => {
    const wrapper = mountWorkDetailsRenderer();

    expect(wrapper.find(ExistingWorkDetails)).toExist();

    wrapper
      .find('[data-test-el="request-change-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkDetailsForm)).toExist();

    wrapper
      .find('[data-test-el="step-cancel"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(ExistingWorkDetails)).toExist();

    wrapper
      .find('[data-test-el="new-occupation-btn"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(WorkDetailsForm)).toExist();
  });

  test('render - no occupation', () => {
    const wrapper = mountWorkDetailsRenderer({ occupation: null });

    expect(wrapper.find(WorkDetailsForm)).toExist();
  });
});
