import React from 'react';

import { shallow } from '../../../../../../config';
import { MessageList } from '../../../../../../../app/modules/my-messages/my-messages-panels';
import { messagesFixture } from '../../../../../../common/fixtures';

describe('MessageList', () => {
  const mock = jest.fn();

  const props = {
    messages: messagesFixture,
    selectedMessage: 1,
    chooseLogo: () => mock,
    onMessageSelect: () => mock
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MessageList {...props} />)).toMatchSnapshot();
  });
});
