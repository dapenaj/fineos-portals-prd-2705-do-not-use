import React from 'react';

import { shallow } from '../../../../../config';
import { MyMessagesHeader } from '../../../../../../app/modules/my-messages/my-messages-header';

describe('MyMessagesHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <MyMessagesHeader
          onSearch={() => {
            jest.fn();
          }}
          notificationCaseId="1"
        />
      )
    ).toMatchSnapshot();
  });
});
