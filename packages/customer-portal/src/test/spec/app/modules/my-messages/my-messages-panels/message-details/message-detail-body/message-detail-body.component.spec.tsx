import React from 'react';

import { shallow } from '../../../../../../../config';
import { MessageDetailBody } from '../../../../../../../../app/modules/my-messages/my-messages-panels';
import { messagesFixture } from '../../../../../../../common/fixtures';

describe('MessageDetailBody', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<MessageDetailBody message={messagesFixture[0]} />)
    ).toMatchSnapshot();
  });
});
