import React from 'react';

import { shallow } from '../../../../../../../config';
import { MessageListElement } from '../../../../../../../../app/shared/ui/message-list-element';
import { messagesFixture } from '../../../../../../../common/fixtures';

describe('MessageListElement', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <MessageListElement
          message={messagesFixture[0]}
          chooseLogo={() => {
            console.log('chooseLogo');
            return '';
          }}
        />
      )
    ).toMatchSnapshot();
  });
});
