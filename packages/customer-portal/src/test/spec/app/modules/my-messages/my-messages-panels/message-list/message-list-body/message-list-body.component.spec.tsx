import React from 'react';

import { shallow } from '../../../../../../../config';
import { MessageListBody } from '../../../../../../../../app/shared/ui/message-list-element';
import { messagesFixture } from '../../../../../../../common/fixtures';

describe('MessageListHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<MessageListBody message={messagesFixture[0]} />)
    ).toMatchSnapshot();
  });
});
