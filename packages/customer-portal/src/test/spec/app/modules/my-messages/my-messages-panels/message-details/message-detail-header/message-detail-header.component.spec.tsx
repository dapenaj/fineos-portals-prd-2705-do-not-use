import React from 'react';

import { shallow } from '../../../../../../../config';
import { MessageDetailHeader } from '../../../../../../../../app/modules/my-messages/my-messages-panels';
import { messagesFixture } from '../../../../../../../common/fixtures';

describe('MessageDetailHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <MessageDetailHeader
          message={messagesFixture[0]}
          chooseLogo={() => {
            return '';
          }}
        />
      )
    ).toMatchSnapshot();
  });
});
