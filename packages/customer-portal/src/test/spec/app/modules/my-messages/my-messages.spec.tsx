import React from 'react';

import { DefaultContext, mount } from '../../../../config';
import { releaseEventLoop } from '../../../../common/utils';
import {
  fetchMessagesSpy,
  fetchNotificationsSpy
} from '../../../../common/spys';
import MyMessages from '../../../../../app/modules/my-messages';
import { MyMessagesViewComponent } from '../../../../../app/modules/my-messages';
import { Spinner } from '../../../../../app/shared/ui';
import {
  messagesFixture,
  notificationsFixture
} from '../../../../common/fixtures';

describe('My Messages', () => {
  const props = {
    loading: false,
    notifications: notificationsFixture,
    messages: messagesFixture,
    match: {
      params: {}
    }
  };

  const mountMyMessages = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyMessages {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render loading', async () => {
    fetchNotificationsSpy();
    fetchMessagesSpy('success');

    const wrapper = mountMyMessages();

    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
  });

  test('render messages view', async () => {
    fetchNotificationsSpy();
    fetchMessagesSpy('success');

    const wrapper = mountMyMessages();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyMessagesViewComponent)).toExist();
  });

  test('filter messages', async () => {
    fetchNotificationsSpy();
    fetchMessagesSpy('success');

    const wrapper = mountMyMessages();

    await releaseEventLoop();

    wrapper.update();
    const instance = wrapper
      .find(MyMessagesViewComponent)
      .instance() as MyMessagesViewComponent;

    const resultsBySearchValue = [
      {
        caseId: 'CLM-2',
        contactTime: '2017-10-03T10:13:23.279',
        messageId: 3,
        msgOriginatesFromPortal: true,
        narrative:
          'Cras vulputate varius odio, eget aliquet tellus ultricies sit amet.',
        read: true,
        subject: 'Maecenas vitae magna neque'
      }
    ];

    const results = [
      {
        caseId: 'ABS-7',
        contactTime: '2017-10-01T09:45:23.279',
        messageId: 2,
        msgOriginatesFromPortal: false,
        narrative: 'testowyTekst Aliquam auctor a ligula non imperdiet.',
        read: true,
        subject: 'Etiam vitae est at ex feugiat condimentum'
      },
      {
        messageId: 5,
        caseId: '0',
        subject: 'Etiam vitae est at ex feugiat condimentum',
        narrative: 'testowyTekst Aliquam auctor a ligula non imperdiet.',
        contactTime: '2017-10-01T09:45:23.279',
        msgOriginatesFromPortal: false,
        read: false
      }
    ];

    expect(
      instance.filterMessagesBySearchValue(messagesFixture, ['Cras'])
    ).toEqual(resultsBySearchValue);

    expect(instance.filterMessages(true)).toEqual(results);
  });

  test('changes in state', async () => {
    fetchNotificationsSpy();
    fetchMessagesSpy('success');

    const wrapper = mountMyMessages();

    await releaseEventLoop();

    wrapper.update();
    const instance = wrapper
      .find(MyMessagesViewComponent)
      .instance() as MyMessagesViewComponent;

    instance.onSearch(['Cras']);
    expect(instance.state.selectedMessageId).toEqual(null);
    expect(instance.state.msgOriginatesFromPortal).toEqual(false);
    instance.onChange(true);
    expect(instance.state.msgOriginatesFromPortal).toEqual(true);
    instance.onMessageSelect(messagesFixture[0]);
    expect(instance.state.selectedMessageId).toEqual(
      messagesFixture[0].messageId
    );
  });

  test('render with selectedMessage', async () => {
    fetchNotificationsSpy();
    fetchMessagesSpy('success');

    const wrapper = mountMyMessages({
      match: {
        params: { notificationCaseId: 'NTN-1' }
      },
      location: { search: '?messageId=3' }
    });

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyMessagesViewComponent)).toExist();
  });
});
