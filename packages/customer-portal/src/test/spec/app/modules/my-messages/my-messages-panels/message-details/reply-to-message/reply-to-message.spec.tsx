import React from 'react';
import { mount, ReactWrapper } from 'enzyme';

import { DefaultContext } from '../../../../../../../config';
import {
  ReplyToMessage,
  ReplyToMessageComponent,
  ReplyToMessageModalForm,
  ReplyToMessageFormValue
} from '../../../../../../../../app/modules/my-messages';
import { messagesFixture } from '../../../../../../../common/fixtures';
import { addMessageSpy } from '../../../../../../../common/spys';
import { releaseEventLoop } from '../../../../../../../common/utils';

describe('Reply to Message', () => {
  let wrapper: ReactWrapper;
  const addMessageMock = jest.fn();
  const onMessageSentMock = jest.fn();

  const mountReplyToMessage = (props: any) => {
    return mount(
      <DefaultContext>
        <ReplyToMessage {...props} />
      </DefaultContext>
    );
  };

  const newMessage: ReplyToMessageFormValue = {
    subject: 'New Subject',
    narrative: 'New Narrative'
  };

  describe('Reply to Message', () => {
    let instance: ReplyToMessageComponent;

    beforeEach(() => {
      wrapper = mountReplyToMessage({
        message: messagesFixture[0],
        addMessage: addMessageMock,
        onMessageSent: onMessageSentMock
      });

      instance = wrapper
        .find(ReplyToMessageComponent)
        .instance() as ReplyToMessageComponent;
    });

    test('should render', () => {
      expect(wrapper.find(ReplyToMessageModalForm)).toExist();
    });

    test('should open the reply to message modal', async () => {
      wrapper
        .find('[data-test-el="reply-to-message-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(ReplyToMessageModalForm).prop('visible')).toBe(true);
    });

    test('should close the reply to message modal', async () => {
      wrapper
        .find('[data-test-el="reply-to-message-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(ReplyToMessageModalForm).prop('visible')).toBe(true);

      instance.handleCancel();

      wrapper.update();

      expect(wrapper.find(ReplyToMessageModalForm).prop('visible')).toBe(false);
    });

    test('submit new message', async () => {
      addMessageSpy('success');

      const addSpy = jest.spyOn(instance, 'sendMessage');

      const expected = {
        caseId: 'CLM-1',
        narrative: 'New Narrative',
        subject: 'New Subject'
      };

      instance.handleSubmit(newMessage);

      await releaseEventLoop();

      expect(addSpy).toHaveBeenCalledWith(expected);
    });
  });
});
