import React from 'react';

import { DefaultContext, mount } from '../../../../../config';
import {
  MyMessagesSearch,
  MyMessagesSearchContainer
} from '../../../../../../app/modules/my-messages';
import { showNotification } from '../../../../../../app/shared/ui/notification/notification';

describe('MyMessgesSearch', () => {
  const mountMyMessagesSearchRenderer = (props?: any) => {
    return mount(
      <DefaultContext>
        <MyMessagesSearch
          onSearch={() => {
            jest.fn();
          }}
          {...props}
        />
      </DefaultContext>
    );
  };

  test('render MyMessagesSearch', () => {
    const wrapper = mountMyMessagesSearchRenderer();

    expect(wrapper.find(MyMessagesSearch)).toExist();
  });

  test('on search', () => {
    const wrapper = mountMyMessagesSearchRenderer();
    const wrapperWithInstance = wrapper
      .find('MyMessagesSearchContainer')
      .instance() as MyMessagesSearchContainer;

    wrapperWithInstance.onSearch('test');
    expect(wrapperWithInstance.state.searchValue).toEqual('');
    expect(wrapperWithInstance.state.searchValues).toEqual(['test']);

    wrapperWithInstance.onSearch('te');
    wrapper.find(showNotification);
  });

  test('clear item', () => {
    const wrapper = mountMyMessagesSearchRenderer();
    const wrapperWithInstance = wrapper
      .find('MyMessagesSearchContainer')
      .instance() as MyMessagesSearchContainer;

    wrapperWithInstance.clearItem('test');
    expect(wrapperWithInstance.state.searchValue).toEqual('');
    expect(wrapperWithInstance.state.searchValues).toEqual([]);
  });
});
