import React from 'react';

import { DefaultContext, mount } from '../../../../../config';
import { MyMessagesPanels } from '../../../../../../app/modules/my-messages/my-messages-panels';
import { messagesFixture } from '../../../../../common/fixtures';

describe('MyMessagesPanels', () => {
  const mock = jest.fn();

  const props = {
    messages: messagesFixture,
    selectedMessage: messagesFixture[0],
    onMessageSent: () => mock,
    onClick: () => mock
  };

  const mountMyMessagesPanels = () =>
    mount(
      <DefaultContext>
        <MyMessagesPanels {...props} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountMyMessagesPanels();

    wrapper.update();

    expect(wrapper.find(MyMessagesPanels)).toExist();
  });
});
