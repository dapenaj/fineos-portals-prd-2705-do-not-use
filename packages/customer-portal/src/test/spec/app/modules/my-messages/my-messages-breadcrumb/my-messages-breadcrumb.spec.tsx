import React from 'react';

import { shallow } from '../../../../../config';
import { MyMessagesBreadcrumb } from '../../../../../../app/modules/my-messages/';

describe('MyMessagesBreadcrumb', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<MyMessagesBreadcrumb notificationCaseId={'NTN-1'} />)
    ).toMatchSnapshot();
  });
});
