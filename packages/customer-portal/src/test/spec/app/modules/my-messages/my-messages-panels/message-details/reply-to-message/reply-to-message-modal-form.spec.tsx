import React from 'react';
import { mount, ReactWrapper } from 'enzyme';

import { DefaultContext } from '../../../../../../../config';
import {
  ReplyToMessageModalForm,
  ReplyToMessageFormValue
} from '../../../../../../../../app/modules/my-messages';
import { ConfirmModal } from '../../../../../../../../app/shared/ui';

describe('Reply to Message Modal Form', () => {
  let wrapper: ReactWrapper;
  const onCancelMock = jest.fn();
  const onSubmitMock = jest.fn();

  const mountReplyToMessage = (props: any) => {
    return mount(
      <DefaultContext>
        <ReplyToMessageModalForm {...props} />
      </DefaultContext>
    );
  };

  describe('Reply to Message Form', () => {
    beforeEach(() => {
      wrapper = mountReplyToMessage({
        visible: true,
        subject: 'Test Subject',
        onCancel: () => onCancelMock,
        onSubmit: (value: ReplyToMessageFormValue) => onSubmitMock
      });
    });

    const showConfirmation = () => {
      wrapper
        .find('[data-test-el="modal-footer-cancel-btn"]')
        .hostNodes()
        .simulate('click');
    };

    test('should render', () => {
      expect(wrapper.find(ReplyToMessageModalForm)).toExist();
    });

    test('confirmation - submit', () => {
      showConfirmation();

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(true);

      wrapper
        .find('[data-test-el="confirmation-submit-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(false);
    });

    test('confirmation - cancel', () => {
      showConfirmation();

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(true);

      wrapper
        .find('[data-test-el="confirmation-cancel-button"]')
        .hostNodes()
        .simulate('click');

      expect(wrapper.find(ConfirmModal).prop('visible')).toBe(false);
    });
  });
});
