import React from 'react';

import { DefaultContext, mount } from '../../../../../../../config';
import { MyMessageSearchBadge } from '../../../../../../../../app/modules/my-messages/my-messages-search';

describe('MyMessgesSearchBadge', () => {
  const mock = jest.fn();
  const clearItem = mock;

  const defaultProps = {
    searchValues: ['test'],
    clearItem
  };

  const mountMyMessageSearchBagdeRenderer = (props?: any) => {
    return mount(
      <DefaultContext>
        <MyMessageSearchBadge {...defaultProps} {...props} />
      </DefaultContext>
    );
  };

  test('render MyMessageSearchBadge', () => {
    const wrapper = mountMyMessageSearchBagdeRenderer();

    expect(wrapper.find(MyMessageSearchBadge)).toExist();
  });

  test('click badge', async () => {
    const wrapper = mountMyMessageSearchBagdeRenderer();

    expect(wrapper.find(MyMessageSearchBadge)).toExist();

    wrapper
      .find('[data-test-el="icon-search-badge"]')
      .hostNodes()
      .simulate('click');

    wrapper.update();

    expect(wrapper.find(MyMessageSearchBadge)).toExist();
  });
});
