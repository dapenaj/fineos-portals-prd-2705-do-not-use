import React from 'react';

import { shallow } from '../../../../../../config';
import { MessageDetails } from '../../../../../../../app/modules/my-messages/my-messages-panels';
import { messagesFixture } from '../../../../../../common/fixtures';

describe('MyMessagesDetails', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <MessageDetails
          message={messagesFixture[0]}
          chooseLogo={() => {
            return '';
          }}
          onMessageSent={jest.fn()}
        />
      )
    ).toMatchSnapshot();
  });
});
