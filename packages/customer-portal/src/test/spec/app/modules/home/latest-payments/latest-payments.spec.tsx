import React from 'react';

import { mount, DefaultContext } from '../../../../../config';
import { findClaimSpy, findPaymentsSpy } from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';
import { Spinner, EmptyList } from '../../../../../../app/shared/ui';
import {
  LatestPayments,
  LatestPaymentsList
} from '../../../../../../app/modules/home/latest-payments';

describe('LatestPayments', () => {
  const mountLatestPayments = () =>
    mount(
      <DefaultContext>
        <LatestPayments />
      </DefaultContext>
    );

  test('render loading', async () => {
    findClaimSpy('success');
    findPaymentsSpy('success');

    const wrapper = mountLatestPayments();

    await releaseEventLoop();
    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
  });

  test('render success', async () => {
    findClaimSpy('success');
    findPaymentsSpy('success');

    const wrapper = mountLatestPayments();

    await releaseEventLoop();
    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(LatestPaymentsList)).toExist();
  });

  test('render failure', async () => {
    findClaimSpy('success');
    findPaymentsSpy('failure');

    const wrapper = mountLatestPayments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(EmptyList)).toExist();
  });
});
