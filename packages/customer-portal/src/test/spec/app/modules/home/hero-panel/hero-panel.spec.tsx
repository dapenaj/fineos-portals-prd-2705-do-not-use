import React from 'react';

import { DefaultContext, mount } from '../../../../../config';
import {
  HeroPanel,
  HeroPanelComponent
} from '../../../../../../app/modules/home/hero-panel';
import { releaseEventLoop } from '../../../../../common/utils';
import { appConfigFixture } from '../../../../../common/fixtures';

describe('HeroPanel', () => {
  const props = {
    config: appConfigFixture
  };

  const mountHeroPanel = () =>
    mount(
      <DefaultContext>
        <HeroPanel {...props} />
      </DefaultContext>
    );

  test('render', async () => {
    const wrapper = mountHeroPanel();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(HeroPanelComponent)).toExist();
  });
});
