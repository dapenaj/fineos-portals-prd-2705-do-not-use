import React from 'react';

import { shallow } from '../../../../../config';
import { LatestPaymentsList } from '../../../../../../app/modules/home/latest-payments';
import { paymentFixture } from '../../../../../common/fixtures';

describe('LatestPaymentsList', () => {
  const props = {
    payments: paymentFixture,
    loading: false,
    showViewAll: true
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<LatestPaymentsList {...props} />)).toMatchSnapshot();
  });

  test('render - empty -- SNAPSHOT', () => {
    expect(
      shallow(<LatestPaymentsList {...props} payments={[]} />)
    ).toMatchSnapshot();
  });

  test('render - loading -- SNAPSHOT', () => {
    expect(
      shallow(<LatestPaymentsList {...props} loading={true} />)
    ).toMatchSnapshot();
  });
});
