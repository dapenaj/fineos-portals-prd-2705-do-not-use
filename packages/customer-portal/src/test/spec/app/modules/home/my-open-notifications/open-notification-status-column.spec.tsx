import React from 'react';

import { shallow } from '../../../../../config';
import { OpenNotificationStatusColumn } from '../../../../../../app/modules/home/my-open-notifications';
import { NotificationStatusResult } from '../../../../../../app/shared/types';

describe('Open Notification', () => {
  const props = { status: NotificationStatusResult.PENDING, label: '' };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<OpenNotificationStatusColumn {...props} />)
    ).toMatchSnapshot();
  });
});
