import React from 'react';

import { shallow } from '../../../../../config';
import { OpenNotification } from '../../../../../../app/modules/home/my-open-notifications';
import { notificationsFixture } from '../../../../../common/fixtures';

describe('Open Notification', () => {
  const props = { notification: notificationsFixture[0] };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<OpenNotification {...props} />)).toMatchSnapshot();
  });
});
