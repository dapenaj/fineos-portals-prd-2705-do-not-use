import React from 'react';

import { shallow } from '../../../../../config';
import { WelcomeColumn } from '../../../../../../app/modules/home/hero-panel';
import { customerFixture } from '../../../../../common/fixtures';

describe('WelcomeColumn', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <WelcomeColumn customer={customerFixture} welcomeMessage="test" />
      )
    ).toMatchSnapshot();
  });

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<WelcomeColumn customer={null} welcomeMessage="test" />)
    ).toMatchSnapshot();
  });
});
