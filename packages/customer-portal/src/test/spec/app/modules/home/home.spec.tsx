import * as React from 'react';

import Home, { HomeView } from '../../../../../app/modules/home';
import { DefaultContext, mount } from '../../../../config';
import { AuthorizationService } from '../../../../../app/shared/services';
import { Role } from '../../../../../app/shared/types';
import { fetchNotificationsSpy } from '../../../../common/spys';
import { releaseEventLoop } from '../../../../common/utils';

describe('Home View', () => {
  const authorizationService: AuthorizationService = AuthorizationService.getInstance();

  const mountHome = () =>
    mount(
      <DefaultContext>
        <Home />
      </DefaultContext>
    );

  beforeEach(() => {
    authorizationService.authenticationService.displayRoles = [
      Role.ABSENCE_USER
    ];
  });

  test('render', async () => {
    fetchNotificationsSpy();

    const wrapper = mountHome();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(HomeView)).toExist();
  });
});
