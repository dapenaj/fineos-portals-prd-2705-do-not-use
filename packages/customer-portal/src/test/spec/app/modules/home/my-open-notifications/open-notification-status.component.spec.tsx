import React from 'react';

import { shallow } from '../../../../../config';
import { OpenNotificationStatus } from '../../../../../../app/modules/home/my-open-notifications';
import { NotificationStatusResult } from '../../../../../../app/shared/types';

describe('Open Notification Pending', () => {
  test('render - Pending - SNAPSHOT', () => {
    expect(
      shallow(
        <OpenNotificationStatus status={NotificationStatusResult.PENDING} />
      )
    ).toMatchSnapshot();
  });

  test('render - Decided - SNAPSHOT', () => {
    expect(
      shallow(
        <OpenNotificationStatus status={NotificationStatusResult.DECIDED} />
      )
    ).toMatchSnapshot();
  });

  test('render - Some Decided - SNAPSHOT', () => {
    expect(
      shallow(
        <OpenNotificationStatus
          status={NotificationStatusResult.SOME_DECIDED}
        />
      )
    ).toMatchSnapshot();
  });

  test('render - All Closed - SNAPSHOT', () => {
    expect(
      shallow(
        <OpenNotificationStatus status={NotificationStatusResult.ALL_CLOSED} />
      )
    ).toMatchSnapshot();
  });

  test('render - Undetermined - SNAPSHOT', () => {
    expect(
      shallow(
        <OpenNotificationStatus
          status={NotificationStatusResult.UNDETERMINED}
        />
      )
    ).toMatchSnapshot();
  });
});
