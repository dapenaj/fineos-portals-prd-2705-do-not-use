import React from 'react';
import {
  NotificationCaseSummary,
  AbsenceSummary,
  ClaimSummary
} from 'fineos-js-api-client';

import { shallow } from '../../../../../config';
import { OpenNotificationBody } from '../../../../../../app/modules/home/my-open-notifications';

describe('Open Notification', () => {
  const props = {
    notification: {
      absences: [] as AbsenceSummary[],
      claims: [] as ClaimSummary[]
    } as NotificationCaseSummary
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<OpenNotificationBody {...props} />)).toMatchSnapshot();
  });
});
