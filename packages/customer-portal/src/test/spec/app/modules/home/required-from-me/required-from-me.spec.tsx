import React from 'react';

import { mount, DefaultContext } from '../../../../../config';
import {
  getSupportingEvidenceSpy,
  findPaymentsSpy
} from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';
import { Spinner, RequiredFromMePanel } from '../../../../../../app/shared/ui';
import { RequiredFromMe } from '../../../../../../app/modules/home/required-from-me';
import { notificationsFixture } from '../../../../../common/fixtures';

describe('RequiredFromMe', () => {
  const props = {
    notifications: notificationsFixture
  };

  const mountMyPayments = () =>
    mount(
      <DefaultContext>
        <RequiredFromMe {...props} />
      </DefaultContext>
    );

  test('render success', async () => {
    getSupportingEvidenceSpy('success');

    const wrapper = mountMyPayments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(RequiredFromMePanel)).toExist();
  });

  test('render failure', async () => {
    getSupportingEvidenceSpy('failure');

    const wrapper = mountMyPayments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Spinner)).not.toExist();
  });
});
