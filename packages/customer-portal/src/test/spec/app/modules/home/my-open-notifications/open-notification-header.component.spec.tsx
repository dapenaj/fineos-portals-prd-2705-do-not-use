import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { shallow } from '../../../../../config';
import { OpenNotificationHeader } from '../../../../../../app/modules/home/my-open-notifications';

describe('Open Notification', () => {
  const props = {
    notification: {
      notificationCaseId: 'NTN-21',
      createdDate: '2017-08-06'
    } as NotificationCaseSummary
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<OpenNotificationHeader {...props} />)).toMatchSnapshot();
  });
});
