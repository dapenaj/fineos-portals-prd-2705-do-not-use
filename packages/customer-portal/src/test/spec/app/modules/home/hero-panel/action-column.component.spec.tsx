import React from 'react';

import { shallow } from '../../../../../config';
import { ActionColumn } from '../../../../../../app/modules/home/hero-panel';

describe('ActionColumn', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<ActionColumn title="test" message="test" />)
    ).toMatchSnapshot();
  });
});
