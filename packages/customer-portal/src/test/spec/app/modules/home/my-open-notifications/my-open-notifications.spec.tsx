import React from 'react';

import { shallow } from '../../../../../config';
import { MyOpenNotifications } from '../../../../../../app/modules/home/my-open-notifications';
import { notificationsFixture } from '../../../../../common/fixtures';

describe('MyOpenNotifications', () => {
  const props = {
    loading: false,
    notifications: notificationsFixture
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyOpenNotifications {...props} />)).toMatchSnapshot();
  });
});
