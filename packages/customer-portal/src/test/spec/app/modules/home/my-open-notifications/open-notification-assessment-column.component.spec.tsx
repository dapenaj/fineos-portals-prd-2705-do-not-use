import React from 'react';

import { shallow } from '../../../../../config';
import { OpenNotificationAssessmentColumn } from '../../../../../../app/modules/home/my-open-notifications';

describe('Open Notification', () => {
  const props = { absences: [], claims: [] };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<OpenNotificationAssessmentColumn {...props} />)
    ).toMatchSnapshot();
  });
});
