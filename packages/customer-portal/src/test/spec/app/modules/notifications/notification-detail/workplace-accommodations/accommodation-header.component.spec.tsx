import React from 'react';

import { shallow } from '../../../../../../config';
import { AccommodationHeader } from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationAccommodationsFixture } from '../../../../../../common/fixtures';

describe('AccommodationHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <AccommodationHeader
          accommodation={notificationAccommodationsFixture[0]}
          expanded={false}
        />
      )
    ).toMatchSnapshot();
  });

  test('render - expanded -- SNAPSHOT', () => {
    expect(
      shallow(
        <AccommodationHeader
          accommodation={notificationAccommodationsFixture[0]}
          expanded={true}
        />
      )
    ).toMatchSnapshot();
  });
});
