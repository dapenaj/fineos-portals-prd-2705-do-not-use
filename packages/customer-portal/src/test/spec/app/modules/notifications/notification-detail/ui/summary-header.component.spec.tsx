import React from 'react';

import { shallow } from '../../../../../../config';
import { SummaryHeader } from '../../../../../../../app/modules/notifications/notification-detail';

describe('SummaryHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <SummaryHeader title="test" icon="dollar">
          test
        </SummaryHeader>
      )
    ).toMatchSnapshot();
  });
});
