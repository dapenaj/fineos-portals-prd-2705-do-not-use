import React from 'react';

import { shallow } from '../../../../../../config';
import { DisabilityBenefits } from '../../../../../../../app/modules/notifications/notification-detail';
import { benefitsFixture } from '../../../../../../common/fixtures';

describe('DisabilityBenefits', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <DisabilityBenefits
          claimId={'CLM-1'}
          benefits={benefitsFixture}
          disabilityClaimBenefits={[]}
        />
      )
    ).toMatchSnapshot();
  });
});
