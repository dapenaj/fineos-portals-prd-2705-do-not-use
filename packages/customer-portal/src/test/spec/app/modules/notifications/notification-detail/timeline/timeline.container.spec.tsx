import React from 'react';

jest.mock('moment', () => {
  // UTC everywhere
  const moment = jest.requireActual('moment');
  return moment.utc;
});

import { shallow } from '../../../../../../config';
import {
  absencePeriodDecisionsFixture,
  absencesFixture,
  absencesWithHandlersFixture,
  benefitsFixture,
  disabilityBenefitSummariesFixture,
  notificationsFixture,
  notificationAccommodationsFixture
} from '../../../../../../common/fixtures';
import { TimelineContainer } from '../../../../../../../app/modules/notifications/notification-detail/timeline/timeline.container';
import { AuthenticationService } from '../../../../../../../app/shared/services/authentication';
import { Role } from '../../../../../../../app/shared/types';

describe('timeline.container', () => {
  const defaultProps = {
    notification: notificationsFixture[0],
    periodDecisions: absencePeriodDecisionsFixture,
    claimBenefits: [
      {
        claimId: 'CLM-1',
        benefits: benefitsFixture
      }
    ],
    absencesWithHandlers: absencesWithHandlersFixture,
    absencePaidLeaves: absencesFixture.map(absence => ({
      ...absence,
      claimBenefits: [
        {
          claimId: 'CLM-1',
          benefits: benefitsFixture
        }
      ],
      paidLeaveHandler: 'string',
      paidLeaveHandlerPhoneNumber: 'string'
    })),
    disabilityBenefitWrappers: [
      {
        claimId: 'CLM-1',
        benefitId: 0,
        disabilityBenefitSummary: disabilityBenefitSummariesFixture[0]
      }
    ]
  };

  test('render for Claim user -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.CLAIMS_USER];

    expect(shallow(<TimelineContainer {...defaultProps} />)).toMatchSnapshot();
  });

  test('render for Absences user -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];

    expect(shallow(<TimelineContainer {...defaultProps} />)).toMatchSnapshot();
  });

  test('render accommodations -- SNAPSHOT', () => {
    const accommodationProps = {
      notification: {
        ...notificationsFixture[0],
        accommodations: [
          {
            ...notificationAccommodationsFixture[0],
            accommodationDecision: 'Accommodated'
          }
        ]
      },
      periodDecisions: absencePeriodDecisionsFixture,
      claimBenefits: [
        {
          claimId: 'CLM-1',
          benefits: benefitsFixture
        }
      ],
      absencesWithHandlers: absencesWithHandlersFixture,
      absencePaidLeaves: absencesFixture.map(absence => ({
        ...absence,
        claimBenefits: [
          {
            claimId: 'CLM-1',
            benefits: benefitsFixture
          }
        ],
        paidLeaveHandler: 'string',
        paidLeaveHandlerPhoneNumber: 'string'
      })),
      disabilityBenefitWrappers: [
        {
          claimId: 'CLM-1',
          benefitId: 0,
          disabilityBenefitSummary: disabilityBenefitSummariesFixture[0]
        }
      ]
    };

    AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];

    expect(
      shallow(<TimelineContainer {...accommodationProps} />)
    ).toMatchSnapshot();
  });

  test('render for Absence user, without disabilityBenefitWrappers -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];

    const propsWithoutClaims = {
      notification: notificationsFixture[0],
      periodDecisions: absencePeriodDecisionsFixture,
      claimBenefits: [
        {
          claimId: 'CLM-1',
          benefits: benefitsFixture
        }
      ],
      absencesWithHandlers: absencesWithHandlersFixture,
      absencePaidLeaves: absencesFixture.map(absence => ({
        ...absence,
        claimBenefits: [
          {
            claimId: 'CLM-1',
            benefits: benefitsFixture
          }
        ],
        paidLeaveHandler: 'string',
        paidLeaveHandlerPhoneNumber: 'string'
      })),
      disabilityBenefitWrappers: []
    };
    expect(
      shallow(<TimelineContainer {...propsWithoutClaims} />)
    ).toMatchSnapshot();
  });

  test('render for Claim user, without disabilityBenefitWrappers -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.CLAIMS_USER];

    const propsWithoutClaims = {
      notification: notificationsFixture[0],
      periodDecisions: absencePeriodDecisionsFixture,
      claimBenefits: [
        {
          claimId: 'CLM-1',
          benefits: benefitsFixture
        }
      ],
      absencesWithHandlers: absencesWithHandlersFixture,
      absencePaidLeaves: absencesFixture.map(absence => ({
        ...absence,
        claimBenefits: [
          {
            claimId: 'CLM-1',
            benefits: benefitsFixture
          }
        ],
        paidLeaveHandler: 'string',
        paidLeaveHandlerPhoneNumber: 'string'
      })),
      disabilityBenefitWrappers: []
    };
    expect(
      shallow(<TimelineContainer {...propsWithoutClaims} />)
    ).toMatchSnapshot();
  });

  test('render for Absence user, without absencePaidLeaves -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];

    const localProps = {
      ...defaultProps,
      absencePaidLeaves: []
    };
    expect(shallow(<TimelineContainer {...localProps} />)).toMatchSnapshot();
  });

  test('render for Claim user, without absencePaidLeaves -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.CLAIMS_USER];

    const localProps = {
      ...defaultProps,
      absencePaidLeaves: []
    };
    expect(shallow(<TimelineContainer {...localProps} />)).toMatchSnapshot();
  });

  test('render for Absence user, without claimBenefits -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.ABSENCE_USER];

    const localProps = {
      ...defaultProps,
      claimBenefits: []
    };
    expect(shallow(<TimelineContainer {...localProps} />)).toMatchSnapshot();
  });

  test('render for Claim user, without claimBenefits -- SNAPSHOT', () => {
    AuthenticationService.getInstance().displayRoles = [Role.CLAIMS_USER];

    const localProps = {
      ...defaultProps,
      claimBenefits: []
    };
    expect(shallow(<TimelineContainer {...localProps} />)).toMatchSnapshot();
  });
});
