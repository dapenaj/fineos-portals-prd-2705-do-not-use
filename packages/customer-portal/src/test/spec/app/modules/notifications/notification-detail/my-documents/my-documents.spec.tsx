import React from 'react';
import { orderBy as _orderBy } from 'lodash';
import { DocumentSearch } from 'fineos-js-api-client';

import { mount, DefaultContext } from '../../../../../../config';
import {
  MyDocuments,
  MyDocumentsList,
  MyDocumentsComponent
} from '../../../../../../../app/modules/notifications/notification-detail';
import { fetchDocumentSpy } from '../../../../../../common/spys';
import { notificationsFixture } from '../../../../../../common/fixtures';
import { Spinner } from '../../../../../../../app/shared/ui';
import { releaseEventLoop } from '../../../../../../common/utils';

describe('MyDocuments', () => {
  const props = {
    notification: notificationsFixture[0]
  };

  const mountMyDocuments = () =>
    mount(
      <DefaultContext>
        <MyDocuments {...props} />
      </DefaultContext>
    );

  test('render loading', async () => {
    fetchDocumentSpy('success');

    const wrapper = mountMyDocuments();

    await releaseEventLoop();

    expect(wrapper.find('.ant-spin-nested-loading')).toExist();
  });

  test('render success', async () => {
    fetchDocumentSpy('success');

    const wrapper = mountMyDocuments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyDocumentsList)).toExist();
  });

  test('render failure', async () => {
    fetchDocumentSpy('failure');

    const wrapper = mountMyDocuments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Spinner)).not.toExist();
  });

  test('sending correct request', async () => {
    fetchDocumentSpy('success');

    const wrapper = mountMyDocuments();

    const instance = wrapper
      .find(MyDocumentsComponent)
      .instance() as MyDocumentsComponent;

    const findDocumentsSpy = jest.spyOn(instance.claimService, 'findDocuments');
    const search = new DocumentSearch();
    search.includeChildCases(true);

    await releaseEventLoop();
    expect(findDocumentsSpy).toHaveBeenCalledWith('NTN-21', search);
  });
});
