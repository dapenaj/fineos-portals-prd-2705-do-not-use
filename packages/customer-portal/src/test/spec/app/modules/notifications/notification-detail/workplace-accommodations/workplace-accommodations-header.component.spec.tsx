import React from 'react';

import { shallow } from '../../../../../../config';
import { WorkplaceAccommodationsHeader } from '../../../../../../../app/modules/notifications/notification-detail';

describe('WorkplaceAccommodationsHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<WorkplaceAccommodationsHeader />)).toMatchSnapshot();
  });
});
