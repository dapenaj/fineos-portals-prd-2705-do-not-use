import {
  getWorkplaceAccommodationStatus,
  getWorkplaceAccommodationStatusLabel
} from '../../../../../../../app/modules/notifications/notification-detail';

describe('Worplace Accommodation Utils', () => {
  it('should return the right accommodation status', () => {
    expect(getWorkplaceAccommodationStatus('Completion', 'Accommodated')).toBe(
      'Accommodated'
    );
    expect(getWorkplaceAccommodationStatus('Monitoring', 'Accommodated')).toBe(
      'Accommodated'
    );
    expect(getWorkplaceAccommodationStatus('Closed', 'Not Accommodated')).toBe(
      'Not Accommodated'
    );
    expect(
      getWorkplaceAccommodationStatus('Assessment', 'Not Accommodated')
    ).toBe('Pending');
  });

  it('should return the right accommodation status label', () => {
    expect(getWorkplaceAccommodationStatusLabel('Accommodated')).toBe(
      'WORKPLACE_ACCOMMODATION_STATUS.ACCOMMODATED'
    );

    expect(getWorkplaceAccommodationStatusLabel('Not Accommodated')).toBe(
      'WORKPLACE_ACCOMMODATION_STATUS.NOT_ACCOMMODATED'
    );

    expect(getWorkplaceAccommodationStatusLabel('Pending')).toBe(
      'WORKPLACE_ACCOMMODATION_STATUS.PENDING'
    );
  });
});
