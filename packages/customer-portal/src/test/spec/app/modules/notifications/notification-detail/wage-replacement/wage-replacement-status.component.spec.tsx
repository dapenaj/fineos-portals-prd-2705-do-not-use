import React from 'react';

import { shallow } from '../../../../../../config';
import { WageReplacementStatusIcon } from '../../../../../../../app/modules/notifications/notification-detail';

describe('WageReplacementStatusIcon', () => {
  test('render - Pending -- SNAPSHOT', () => {
    expect(
      shallow(<WageReplacementStatusIcon status="Pending" />)
    ).toMatchSnapshot();
  });

  test('render - Approved -- SNAPSHOT', () => {
    expect(
      shallow(<WageReplacementStatusIcon status="Decided" />)
    ).toMatchSnapshot();
  });

  test('render - Denied -- SNAPSHOT', () => {
    expect(
      shallow(<WageReplacementStatusIcon status="Decided" />)
    ).toMatchSnapshot();
  });

  test('render - Default -- SNAPSHOT', () => {
    expect(
      shallow(<WageReplacementStatusIcon status="Closed" count={1} />)
    ).toMatchSnapshot();
  });

  test('render - Unknown Pending -- SNAPSHOT', () => {
    expect(
      shallow(<WageReplacementStatusIcon status="Unknown" />)
    ).toMatchSnapshot();
  });
});
