import React from 'react';

import { shallow } from '../../../../../../config';
import { WageReplacementHeader } from '../../../../../../../app/modules/notifications/notification-detail';

describe('WageReplacementHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<WageReplacementHeader />)).toMatchSnapshot();
  });
});
