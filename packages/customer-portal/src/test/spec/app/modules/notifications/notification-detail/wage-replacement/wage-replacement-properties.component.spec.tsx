import React from 'react';

import { shallow } from '../../../../../../config';
import { WageReplacementProperties } from '../../../../../../../app/modules/notifications/notification-detail';
import {
  benefitsFixture,
  disabilityBenefitSummariesFixture,
  paymentFixture
} from '../../../../../../common/fixtures';

describe('WageReplacementProperties', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <WageReplacementProperties
          benefit={benefitsFixture[0]}
          disabilityBenefitSummary={disabilityBenefitSummariesFixture[0]}
          loading={false}
          paymentDetails={paymentFixture}
        />
      )
    ).toMatchSnapshot();
  });
});
