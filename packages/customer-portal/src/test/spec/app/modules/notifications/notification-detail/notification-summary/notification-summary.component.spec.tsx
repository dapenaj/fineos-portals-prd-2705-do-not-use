import React from 'react';

import { shallow } from '../../../../../../config';
import { notificationsFixture } from '../../../../../../common/fixtures';
import { NotificationSummary } from '../../../../../../../app/modules/notifications/notification-detail';

describe('NotificationSummary', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<NotificationSummary notification={notificationsFixture[0]} />)
    ).toMatchSnapshot();
  });

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<NotificationSummary notification={notificationsFixture[1]} />)
    ).toMatchSnapshot();
  });

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<NotificationSummary notification={notificationsFixture[2]} />)
    ).toMatchSnapshot();
  });
});
