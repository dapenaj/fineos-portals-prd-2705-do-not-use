import React from 'react';

import { shallow } from '../../../../../../config';
import { RecentMessagesHeader } from '../../../../../../../app/modules/notifications/notification-detail/recent-messages';
import { messagesFixture } from '../../../../../../common/fixtures';

describe('RecentMessagesHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<RecentMessagesHeader messages={messagesFixture} />)
    ).toMatchSnapshot();
  });
});
