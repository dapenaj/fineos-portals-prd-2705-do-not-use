import React from 'react';

import { shallow } from '../../../../../../config';
import { EmptyAccommodations } from '../../../../../../../app/modules/notifications/notification-detail';

describe('EmptyAccommodations', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<EmptyAccommodations />)).toMatchSnapshot();
  });
});
