import React from 'react';

import { shallow } from '../../../../../../config';
import { TimelineLegend } from '../../../../../../../app/modules/notifications/notification-detail/timeline/timeline-legend';

describe('timeline legend', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<TimelineLegend />)).toMatchSnapshot();
  });
});
