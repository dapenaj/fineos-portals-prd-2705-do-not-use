import React from 'react';

import { shallow } from '../../../../../../config';
import { BenefitHeader } from '../../../../../../../app/modules/notifications/notification-detail';
import { benefitsFixture } from '../../../../../../common/fixtures';

describe('BenefitHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<BenefitHeader benefit={benefitsFixture[0]} expanded={false} />)
    ).toMatchSnapshot();
  });

  test('render - expanded -- SNAPSHOT', () => {
    expect(
      shallow(<BenefitHeader benefit={benefitsFixture[0]} expanded={true} />)
    ).toMatchSnapshot();
  });
});
