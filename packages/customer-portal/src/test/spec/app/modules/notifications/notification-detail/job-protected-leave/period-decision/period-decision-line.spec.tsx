import React from 'react';
import { WrappedComponentProps as IntlProps } from 'react-intl';

import { shallow, mount, DefaultContext } from '../../../../../../../config';
import {
  periodDecisionsFixture,
  expectedExtractIntermittentPeriodsFromPeriodDecisionsValue
} from '../../../../../../../common/fixtures';
import { deleteTimeOffRequestSpy } from '../../../../../../../common/spys';
import { releaseEventLoop } from '../../../../../../../common/utils';
import {
  PeriodDecisionLine,
  PeriodDecisionLineComponent,
  PeriodDecisionLineState,
  PeriodDecisionDefaultProperties,
  PeriodDecisionIntermittentProperties,
  IntermittentTimeReportTable
} from '../../../../../../../../app/modules/notifications/notification-detail';
import { RequestConfirmationModal } from '../../../../../../../../app/shared/ui';

describe('PeriodDecisionLine', () => {
  const props = {
    decision: { ...periodDecisionsFixture[0], statusCategory: 'approved' }
  };

  const mountPeriodDecisionLine = () => {
    return mount(
      <DefaultContext>
        <PeriodDecisionLine {...props} />
      </DefaultContext>
    );
  };

  test('render component', () => {
    const wrapper = mountPeriodDecisionLine();

    expect(wrapper.find(PeriodDecisionLine)).toExist();

    wrapper
      .find('[data-test-el="dropdown-options"]')
      .at(0)
      .simulate('click');

    expect(wrapper.find('[data-test-el="delete-button"]')).toExist();

    wrapper
      .find('[data-test-el="delete-button"]')
      .at(0)
      .simulate('click');
  });

  test('should toggle the edit form', () => {
    const wrapper = mountPeriodDecisionLine();

    const periodDecisionLine = wrapper
      .find(PeriodDecisionLineComponent)
      .instance() as PeriodDecisionLineComponent;

    periodDecisionLine.showEditPeriodForm();

    expect((periodDecisionLine.state as PeriodDecisionLineState).isEdit).toBe(
      true
    );

    periodDecisionLine.closeForm();

    expect(
      (periodDecisionLine.state as PeriodDecisionLineState)
        .showEditLeavePeriodForm
    ).toBe(false);
  });

  test('should toggle the confirmation modal', () => {
    const wrapper = mountPeriodDecisionLine();
    const periodDecisionLine = wrapper
      .find(PeriodDecisionLineComponent)
      .instance() as PeriodDecisionLineComponent;

    expect(
      (periodDecisionLine.state as PeriodDecisionLineState).showConfirmation
    ).toBeFalsy();

    periodDecisionLine.toggleConfirmationModal();

    wrapper.update();

    expect(wrapper.find(RequestConfirmationModal)).toExist();

    expect(
      (periodDecisionLine.state as PeriodDecisionLineState).showConfirmation
    ).toBeTruthy();
  });

  test('should try to delete a period line', async () => {
    deleteTimeOffRequestSpy('success');

    const wrapper = mountPeriodDecisionLine();
    const periodDecisionLine = wrapper
      .find(PeriodDecisionLineComponent)
      .instance() as PeriodDecisionLineComponent;

    periodDecisionLine.submitDeleteRequest();

    await releaseEventLoop();
    wrapper.update();

    expect(wrapper.find(RequestConfirmationModal)).toExist();
  });

  test('render -- SNAPSHOT', () => {
    const localProps = {
      decision: {
        ...periodDecisionsFixture[0],
        statusCategory: 'approved'
      },
      intl: { formatMessage: jest.fn() },
      ...({} as IntlProps)
    };
    expect(
      shallow(<PeriodDecisionLineComponent {...localProps} />)
    ).toMatchSnapshot();
  });

  test('render - pending -- SNAPSHOT', () => {
    const localProps = {
      decision: {
        ...periodDecisionsFixture[0],
        statusCategory: 'pending',
        episodicLeavePeriodDetail: {
          frequency: 0,
          frequencyInterval: 0,
          frequencyIntervalBasis: '',
          duration: 0,
          durationBasis: ''
        }
      },
      intl: { formatMessage: jest.fn() },
      ...({} as IntlProps)
    };
    expect(
      shallow(<PeriodDecisionLineComponent {...localProps} />)
    ).toMatchSnapshot();
  });

  test('render with payment history -- SNAPSHOT', () => {
    const localProps = {
      decision: {
        ...periodDecisionsFixture[0],
        statusCategory: 'pending',
        episodicLeavePeriodDetail: {},
        leavePlanCategory: 'Paid'
      },
      intl: { formatMessage: jest.fn() },
      ...({} as IntlProps)
    };
    expect(
      shallow(<PeriodDecisionLineComponent {...localProps} />)
    ).toMatchSnapshot();
  });
});

describe('PeriodDecisionLine - Intermittent Period', () => {
  const props = {
    decision:
      expectedExtractIntermittentPeriodsFromPeriodDecisionsValue
        .intermittentPeriodDecisions[0]
  };

  const mountPeriodDecisionLine = () => {
    return mount(
      <DefaultContext>
        <PeriodDecisionLine {...props} />
      </DefaultContext>
    );
  };

  test('render component', () => {
    const wrapper = mountPeriodDecisionLine();

    expect(wrapper.find(PeriodDecisionLine)).toExist();

    expect(wrapper.find(PeriodDecisionDefaultProperties)).not.toExist();
    expect(wrapper.find(PeriodDecisionIntermittentProperties)).toExist();
    expect(wrapper.find(IntermittentTimeReportTable)).toExist();
  });

  test('render intermittent period -- SNAPSHOT', () => {
    expect(
      shallow(<PeriodDecisionLineComponent {...props} />)
    ).toMatchSnapshot();
  });

  test('render intermittent period - pending -- SNAPSHOT', () => {
    const localProps = {
      decision: {
        ...expectedExtractIntermittentPeriodsFromPeriodDecisionsValue
          .intermittentPeriodDecisions[0],
        statusCategory: 'pending'
      }
    };
    expect(
      shallow(<PeriodDecisionLineComponent {...localProps} />)
    ).toMatchSnapshot();
  });
});
