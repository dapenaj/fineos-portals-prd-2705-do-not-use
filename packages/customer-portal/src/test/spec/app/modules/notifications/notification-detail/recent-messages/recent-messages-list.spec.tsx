import React from 'react';

import { shallow } from '../../../../../../config';
import { RecentMessagesList } from '../../../../../../../app/modules/notifications/notification-detail/recent-messages';
import {
  messagesFixture,
  customerFixture
} from '../../../../../../common/fixtures';

describe('RecentMessagesList', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RecentMessagesList
          messages={messagesFixture}
          customer={customerFixture}
          notificationCaseId={'NTN-1'}
        />
      )
    ).toMatchSnapshot();
  });
});
