import React from 'react';

import { shallow } from '../../../../../../config';
import { JobProtectedLeaveStatusIcon } from '../../../../../../../app/modules/notifications/notification-detail';

describe('JobProtectedLeaveStatusIcon', () => {
  test('render - Episodic -- SNAPSHOT', () => {
    expect(
      shallow(<JobProtectedLeaveStatusIcon status="Episodic" />)
    ).toMatchSnapshot();
  });

  test('render - Reduced Schedule -- SNAPSHOT', () => {
    expect(
      shallow(<JobProtectedLeaveStatusIcon status="Reduced Schedule" />)
    ).toMatchSnapshot();
  });

  test('render - Time off period -- SNAPSHOT', () => {
    expect(
      shallow(<JobProtectedLeaveStatusIcon status="Time off period" />)
    ).toMatchSnapshot();
  });
});
