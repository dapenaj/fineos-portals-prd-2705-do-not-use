import React from 'react';

import { shallow } from '../../../../../../config';
import { MyNotificationsHeader } from '../../../../../../../app/modules/notifications/my-notifications';

describe('MyNotificationsHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyNotificationsHeader />)).toMatchSnapshot();
  });
});
