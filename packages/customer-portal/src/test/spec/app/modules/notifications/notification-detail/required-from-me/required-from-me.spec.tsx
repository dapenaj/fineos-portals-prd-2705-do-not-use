import React from 'react';

jest.mock('../../../../../../../app/shared/ui/notification');

import { mount, DefaultContext } from '../../../../../../config';
import {
  getConfirmReturnSpy,
  getSupportingEvidenceSpy,
  updateConfirmReturnEFormSpy
} from '../../../../../../common/spys';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  RequiredFromMePanel,
  showNotification
} from '../../../../../../../app/shared/ui';
import { RequiredFromMe } from '../../../../../../../app/modules/notifications/notification-detail';
import {
  confirmReturnForm,
  notificationsFixture
} from '../../../../../../common/fixtures';

describe('RequiredFromMe', () => {
  const props = {
    notification: notificationsFixture[0]
  };
  // as jest.mock used, all exported functions would have mock signature
  const showNotificationMock = showNotification as jest.Mock<
    typeof showNotification
  >;

  const mountRequireFromMe = () =>
    mount(
      <DefaultContext>
        <RequiredFromMe {...props} />
      </DefaultContext>
    );

  test('render success', async () => {
    getSupportingEvidenceSpy('success');
    getConfirmReturnSpy('success');

    const wrapper = mountRequireFromMe();

    expect(
      wrapper.find('[data-test-el="required-from-me-header"]').text()
    ).toContain('Required from me');

    await releaseEventLoop();

    wrapper.update();

    expect(
      wrapper.find('[data-test-el="required-from-me-header"]').text()
    ).toContain('Required from me (2)');
  });

  test('render failure when cannot fetch evidence', async () => {
    getSupportingEvidenceSpy('failure');

    const wrapper = mountRequireFromMe();

    await releaseEventLoop();

    wrapper.update();

    expect(showNotificationMock).toHaveBeenCalledWith({
      type: 'error',
      content: 'getSupportingEvidence failure',
      title: 'An error occurred when fetching outstanding supporting evidence.'
    });
  });

  test('render failure when cannot fetch confirm return', async () => {
    getConfirmReturnSpy('failure');

    const wrapper = mountRequireFromMe();

    await releaseEventLoop();

    wrapper.update();

    expect(showNotificationMock).toHaveBeenCalledWith({
      type: 'error',
      content: 'getConfirmReturnEForm failure',
      title:
        'An error occurred when checking if return to work confirmation required.'
    });
  });

  test('successful save confirm return', async () => {
    getSupportingEvidenceSpy('success');
    getConfirmReturnSpy('success');

    const wrapper = mountRequireFromMe();

    await releaseEventLoop();
    wrapper.update();

    const updateConfirmReturnEFormMock = updateConfirmReturnEFormSpy('success');

    wrapper.find(RequiredFromMePanel).prop('onConfirmReturnSave')!({
      ...confirmReturnForm,
      returningAsExpected: true
    });

    expect(updateConfirmReturnEFormMock).toHaveBeenCalledWith('NTN-873', {
      ...confirmReturnForm,
      returningAsExpected: true
    });

    await releaseEventLoop();
    wrapper.update();
    if (wrapper.find(RequiredFromMePanel).prop('confirmReturns')) {
      expect(
        wrapper
          .find(RequiredFromMePanel)
          .prop('confirmReturns')!
          .some(confirmReturn => confirmReturn.employeeConfirmed)
      ).toBe(true);
    }
  });

  test('failed save confirm return', async () => {
    getSupportingEvidenceSpy('success');
    getConfirmReturnSpy('success');

    const wrapper = mountRequireFromMe();

    await releaseEventLoop();
    wrapper.update();

    const updateConfirmReturnEFormMock = updateConfirmReturnEFormSpy('failure');

    wrapper.find(RequiredFromMePanel).prop('onConfirmReturnSave')!({
      ...confirmReturnForm,
      returningAsExpected: true
    });

    expect(updateConfirmReturnEFormMock).toHaveBeenCalledWith('NTN-873', {
      ...confirmReturnForm,
      returningAsExpected: true
    });

    await releaseEventLoop();
    wrapper.update();

    expect(
      wrapper
        .find(RequiredFromMePanel)
        .prop('confirmReturns')!
        .some(confirmReturn => confirmReturn.employeeConfirmed)
    ).toBe(false);
    expect(showNotificationMock).toHaveBeenCalledWith({
      type: 'error',
      content: 'updateConfirmReturnEFormSpy failure',
      title: 'An error occurred when saving return to work confirmation.'
    });
  });
});
