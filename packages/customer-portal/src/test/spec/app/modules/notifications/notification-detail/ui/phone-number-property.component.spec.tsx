import React from 'react';

import { shallow } from '../../../../../../config';
import { PhoneNumberProperty } from '../../../../../../../app/modules/notifications/notification-detail';

describe('PhoneNumberProperty', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<PhoneNumberProperty phoneNumber="test" />)
    ).toMatchSnapshot();
  });
  test('render - no phone number -- SNAPSHOT', () => {
    expect(shallow(<PhoneNumberProperty />)).toMatchSnapshot();
  });
});
