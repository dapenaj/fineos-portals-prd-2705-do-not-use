import React from 'react';

import { shallow } from '../../../../../../config';
import { WorkplaceAccommodations } from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationAccommodationsFixture } from '../../../../../../common/fixtures';

describe('WorkplaceAccommodations', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <WorkplaceAccommodations
          accommodations={notificationAccommodationsFixture}
        />
      )
    ).toMatchSnapshot();
  });
});
