import React from 'react';

import { shallow } from '../../../../../../config';
import { JobProtectedLeaveHeader } from '../../../../../../../app/modules/notifications/notification-detail';

describe('JobProtectedLeaveHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<JobProtectedLeaveHeader />)).toMatchSnapshot();
  });
});
