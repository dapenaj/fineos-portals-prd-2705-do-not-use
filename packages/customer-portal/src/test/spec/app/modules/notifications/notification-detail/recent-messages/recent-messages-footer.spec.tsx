import React from 'react';

import { shallow } from '../../../../../../config';
import { RecentMessagesFooter } from '../../../../../../../app/modules/notifications/notification-detail/recent-messages';
import { messagesFixture } from '../../../../../../common/fixtures';

describe('RecentMessagesFooter', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RecentMessagesFooter
          messages={messagesFixture}
          notificationCaseId={'NTN-21'}
        />
      )
    ).toMatchSnapshot();
  });
});
