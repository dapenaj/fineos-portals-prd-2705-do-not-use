import React from 'react';

import { shallow } from '../../../../../../config';
import { NotificationProperties } from '../../../../../../../app/modules/notifications/notification-detail';

describe('NotificationProperties', () => {
  test('render - default -- SNAPSHOT', () => {
    expect(shallow(<NotificationProperties status="" />)).toMatchSnapshot();
  });

  test('render - success -- SNAPSHOT', () => {
    expect(
      shallow(<NotificationProperties status="completion" />)
    ).toMatchSnapshot();
  });

  test('render - angle border -- SNAPSHOT', () => {
    expect(
      shallow(<NotificationProperties status="pending" type="Episodic" />)
    ).toMatchSnapshot();
  });

  test('render - halved border -- SNAPSHOT', () => {
    expect(
      shallow(
        <NotificationProperties status="declined" type="Reduced Schedule" />
      )
    ).toMatchSnapshot();
  });
});
