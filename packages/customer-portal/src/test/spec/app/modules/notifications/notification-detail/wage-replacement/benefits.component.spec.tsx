import React from 'react';

import { mount, DefaultContext, ReactWrapper } from '../../../../../../config';
import {
  Benefits,
  BenefitsComponent
} from '../../../../../../../app/modules/notifications/notification-detail';
import {
  benefitsFixture,
  disabilityBenefitSummariesFixture
} from '../../../../../../common/fixtures';
import { findPaymentsSpy } from '../../../../../../common/spys';
import { Spinner } from '../../../../../../../app/shared/ui';
import { releaseEventLoop } from '../../../../../../common/utils';

describe('Benefits', () => {
  let wrapper: ReactWrapper;

  const props = {
    benefits: benefitsFixture,
    disabilities: disabilityBenefitSummariesFixture,
    claimId: 'CLM-1'
  };

  const mountBenefits = () =>
    mount(
      <DefaultContext>
        <Benefits {...props} />
      </DefaultContext>
    );

  test('render', async () => {
    findPaymentsSpy('success');

    wrapper = mountBenefits();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(BenefitsComponent)).toExist();
  });

  test('failure', async () => {
    findPaymentsSpy('failure');

    wrapper = mountBenefits();

    await releaseEventLoop();

    expect(wrapper.find(Spinner)).not.toExist();
  });
});
