import React from 'react';

import { shallow } from '../../../../../../config';
import { TimelineView } from '../../../../../../../app/modules/notifications/notification-detail/timeline/timeline.view';
import { Panel } from '../../../../../../../app/shared/ui/panel';
import styles from '../../../../../../../app/modules/notifications/notification-detail/timeline/timeline.module.scss';

describe('timeline.view', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<TimelineView ganttGroups={[]} />)).toMatchSnapshot();
  });

  test('should be able to toggle legend', () => {
    const wrapper = shallow(<TimelineView ganttGroups={[]} />);

    expect(wrapper.find(Panel).prop('className')).not.toEqual(
      expect.stringContaining(styles.open)
    );

    wrapper.find({ name: 'legend-toggle-control' }).simulate('click');

    expect(wrapper.find(Panel).prop('className')).toEqual(
      expect.stringContaining(styles.open)
    );

    wrapper.find({ name: 'legend-toggle-control' }).simulate('click');

    expect(wrapper.find(Panel).prop('className')).not.toEqual(
      expect.stringContaining(styles.open)
    );
  });
});
