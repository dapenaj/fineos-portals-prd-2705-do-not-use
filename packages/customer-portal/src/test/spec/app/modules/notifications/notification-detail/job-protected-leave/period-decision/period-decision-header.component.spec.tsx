import React from 'react';

import { shallow } from '../../../../../../../config';
import { absencePeriodDecisionsFixture } from '../../../../../../../common/fixtures';
import { PeriodDecisionHeader } from '../../../../../../../../app/modules/notifications/notification-detail';
import { EnhancedAbsencePeriodDecision } from '../../../../../../../../app/shared/types';

describe('PeriodDecisionHeader', () => {
  const fixtures: EnhancedAbsencePeriodDecision[] = absencePeriodDecisionsFixture[0].absencePeriodDecisions.map(
    decision => ({
      ...decision,
      statusCategory: 'pending'
    })
  );

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <PeriodDecisionHeader
          name="Child Bonding"
          periodDecisions={fixtures}
          expanded={false}
        />
      )
    ).toMatchSnapshot();
  });

  test('render - expanded -- SNAPSHOT', () => {
    expect(
      shallow(
        <PeriodDecisionHeader
          name="Child Bonding"
          periodDecisions={fixtures}
          expanded={true}
        />
      )
    ).toMatchSnapshot();
  });
});
