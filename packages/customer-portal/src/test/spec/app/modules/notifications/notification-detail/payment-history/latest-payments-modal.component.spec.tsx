import React from 'react';
import { Modal } from 'antd';

import { mount, DefaultContext } from '../../../../../../config';
import { paymentFixture } from '../../../../../../common/fixtures';
import { LatestPaymentsModal } from '../../../../../../../app/modules/notifications/notification-detail';

describe('LatestPaymentsModal', () => {
  const props = {
    showPaymentHistory: true,
    caseType: 'NTN-21',
    paymentDetails: paymentFixture
  };

  const mountLatestPaymentsModal = () =>
    mount(
      <DefaultContext>
        <LatestPaymentsModal {...props} />
      </DefaultContext>
    );

  test('show/hide latest payment modal', () => {
    const wrapper = mountLatestPaymentsModal();
    const instance = wrapper
      .find('LatestPaymentsModal')
      .instance() as LatestPaymentsModal;

    wrapper.update();

    wrapper.find('button.ant-btn-link').simulate('click');
    wrapper.update();
    expect(instance.state.showModal).toBe(true);

    wrapper.find('button[aria-label="Close"]').simulate('click');
    wrapper.update();
    expect(instance.state.showModal).toBe(false);

    wrapper.find('button.ant-btn-link').simulate('click');
    wrapper.update();
    expect(instance.state.showModal).toBe(true);

    wrapper.find('.ant-modal-footer button').simulate('click');
    wrapper.update();
    expect(instance.state.showModal).toBe(false);
  });
});
