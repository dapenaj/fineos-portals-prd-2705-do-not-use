import each from 'jest-each';

import {
  getWageReplacementStatus,
  getWageReplacementStatusLabel
} from '../../../../../../../app/modules/notifications/notification-detail';
import {
  WageReplacementStatus,
  WageReplacementDisplayStatus
} from '../../../../../../../app/shared/types';

describe('getWageReplacementStatus', () => {
  each([
    ['Pending', '', 'Pending'],
    ['Closed', '', 'Closed'],
    ['Decided', 'Approved', 'Approved'],
    ['Decided', 'Denied', 'Denied'],
    ['Decided', '', 'Decided'],
    ['Unknown', 'Pending', 'Pending'],
    ['Unknown', 'Approved', 'Approved'],
    ['Unknown', 'Denied', 'Denied'],
    ['Unknown', 'Decided', 'Decided'],
    ['Unknown', '', 'Closed'],
    ['', '', 'Closed']
  ]).test(
    `when given '%s' and '%s', should return '%s'`,
    async (first: string, second: string, result: string) => {
      expect(
        getWageReplacementStatus(first as WageReplacementStatus, second)
      ).toBe(result);
    }
  );
});

describe('getWageReplacementStatusLabel', () => {
  each([
    ['Pending', 'DISABILITY_BENEFIT_STATUS.PENDING'],
    ['Approved', 'DISABILITY_BENEFIT_STATUS.APPROVED'],
    ['Denied', 'DISABILITY_BENEFIT_STATUS.DENIED'],
    ['Decided', 'DISABILITY_BENEFIT_STATUS.DECIDED'],
    ['Closed', 'DISABILITY_BENEFIT_STATUS.CLOSED']
  ]).test(
    `when given '%s', should return '%s'`,
    async (option: string, result: string) => {
      expect(
        getWageReplacementStatusLabel(option as WageReplacementDisplayStatus)
      ).toBe(result);
    }
  );
});
