import React from 'react';

import { shallow } from '../../../../../../config';
import { PendingAssessment } from '../../../../../../../app/modules/notifications/notification-detail';
import { absencePeriodDecisionsFixture } from '../../../../../../common/fixtures';

describe('PendingAssessment', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <PendingAssessment
          title="test"
          decisions={absencePeriodDecisionsFixture[0].absencePeriodDecisions}
        />
      )
    ).toMatchSnapshot();
  });

  test('render - default -- SNAPSHOT', () => {
    expect(
      shallow(
        <PendingAssessment
          decisions={absencePeriodDecisionsFixture[0].absencePeriodDecisions}
        />
      )
    ).toMatchSnapshot();
  });
});
