import React from 'react';

import { DefaultContext, mount } from '../../../../../../config';
import { clientConfigFixture } from '../../../../../../common/fixtures';
import {
  NotificationEmptyMessage,
  NotificationEmptyMessageComponent
} from '../../../../../../../app/modules/notifications/notification-detail';

describe('NotificationEmptyMessage', () => {
  const props = {
    config: clientConfigFixture
  };

  const mountNotificationEmptyMessage = (
    type: 'jobProtectedLeave' | 'wageReplacement'
  ) =>
    mount(
      <DefaultContext>
        <NotificationEmptyMessage {...props} type={type} />
      </DefaultContext>
    );

  test('render jobProtectedLeave', () => {
    const wrapper = mountNotificationEmptyMessage('jobProtectedLeave');

    wrapper.update();

    expect(wrapper.find(NotificationEmptyMessageComponent)).toExist();
  });

  test('render wageReplacement', () => {
    const wrapper = mountNotificationEmptyMessage('wageReplacement');

    wrapper.update();

    expect(wrapper.find(NotificationEmptyMessageComponent)).toExist();
  });
});
