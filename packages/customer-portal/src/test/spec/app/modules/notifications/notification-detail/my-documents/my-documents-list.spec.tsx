import React from 'react';
import { orderBy as _orderBy } from 'lodash';
import { DocumentService } from 'fineos-js-api-client';

import { mount, DefaultContext } from '../../../../../../config';
import {
  MyDocumentsList,
  MyDocumentsListComponent
} from '../../../../../../../app/modules/notifications/notification-detail';
import { markDocumentAsReadSpy } from '../../../../../../common/spys';
import { documentsFixture } from '../../../../../../common/fixtures';
import { releaseEventLoop } from '../../../../../../common/utils';
import { ExportService } from '../../../../../../../app/shared/services';

describe('MyDocumentsList', () => {
  const mock = jest.fn();

  const documentService = DocumentService.getInstance();
  const exportService = ExportService.getInstance();

  const props = {
    documents: documentsFixture,
    onDocumentDownload: mock
  };

  const mountMyDocumentsList = () =>
    mount(
      <DefaultContext>
        <MyDocumentsList {...props} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountMyDocumentsList();
    expect(wrapper.find(MyDocumentsListComponent)).toExist();
  });

  test('download a document', async () => {
    markDocumentAsReadSpy('success');
    const wrapper = mountMyDocumentsList();
    const response = {} as Response;

    const instance = wrapper
      .find('MyDocumentsListComponent')
      .instance() as MyDocumentsListComponent;

    jest
      .spyOn(documentService, 'downloadCaseDocument')
      .mockReturnValue(Promise.resolve(response));
    jest
      .spyOn(exportService, 'exportBinary')
      .mockReturnValue(Promise.resolve());

    const downloadDocumentSpy = jest.spyOn(instance, 'downloadDocument');

    instance.downloadDocument(documentsFixture[0]);

    await releaseEventLoop();
    await releaseEventLoop();

    expect(downloadDocumentSpy).toBeCalled();
  });
});
