import React from 'react';
import { FormattedMessage } from 'react-intl';

import { PropertyItem } from '../../../../../../../app/shared/ui';
import {
  buildColsFromNotification,
  createPropertyItem
} from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationsFixture } from '../../../../../../common/fixtures';

describe('Get Notification Summary', () => {
  test('createPropertyItems', () => {
    expect(createPropertyItem('NOTIFICATION.EXPECTED_DELIVERY_DATE')).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.EXPECTED_DELIVERY_DATE"
              values={{}}
            />
          }
        />
      </div>
    );
  });

  test('buildColsFromNotification for accident non-continuous', () => {
    const accidentNonContinuous = buildColsFromNotification(
      notificationsFixture[0]
    );
    expect(accidentNonContinuous).toHaveLength(2);
    expect(accidentNonContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for accident continuous', () => {
    const accidentContinuous = buildColsFromNotification(
      notificationsFixture[1]
    );
    expect(accidentContinuous).toHaveLength(4);
    expect(accidentContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
    expect(accidentContinuous[3]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.ACCIDENT_DATE" values={{}} />
          }
        >
          08-06-2017
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for pregnancy continuous', () => {
    const pregnancyContinuous = buildColsFromNotification(
      notificationsFixture[2]
    );
    expect(pregnancyContinuous).toHaveLength(5);
    expect(pregnancyContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
    expect(pregnancyContinuous[3]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.FIRST_DAY_MISSING_WORK"
              values={{}}
            />
          }
        >
          08-06-2017
        </PropertyItem>
      </div>
    );
    expect(pregnancyContinuous[4]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.EXPECTED_RETURN_TO_WORK"
              values={{}}
            />
          }
        >
          08-06-2017
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for pregnancy non-continuous', () => {
    const pregnancyNonContinuous = buildColsFromNotification(
      notificationsFixture[3]
    );
    expect(pregnancyNonContinuous).toHaveLength(3);
    expect(pregnancyNonContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
    expect(pregnancyNonContinuous[2]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.ACTUAL_DELIVERY_DATE"
              values={{}}
            />
          }
        >
          08-01-2019
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for others continuous', () => {
    const othersContinuous = buildColsFromNotification(notificationsFixture[4]);
    expect(othersContinuous).toHaveLength(3);
    expect(othersContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
    expect(othersContinuous[2]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.EXPECTED_RETURN_TO_WORK"
              values={{}}
            />
          }
        >
          08-01-2019
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for others non-continuous', () => {
    const othersNonContinuous = buildColsFromNotification(
      notificationsFixture[5]
    );
    expect(othersNonContinuous).toHaveLength(1);
    expect(othersNonContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for accident continuous without accidentDate and multipleConflictingAccidentDates=true', () => {
    const accidentContinuous = buildColsFromNotification(
      notificationsFixture[6]
    );
    expect(accidentContinuous).toHaveLength(3);
    expect(accidentContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
    expect(accidentContinuous[2]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.EXPECTED_RETURN_TO_WORK"
              values={{}}
            />
          }
        >
          08-06-2017
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for pregnancy continuous and no expectedRTWDate and multipleConflictingExpectedRTWDates=true', () => {
    const pregnancyContinuous = buildColsFromNotification(
      notificationsFixture[7]
    );
    expect(pregnancyContinuous).toHaveLength(4);
    expect(pregnancyContinuous[0]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATION.NOTIFICATION_DATE" values={{}} />
          }
        >
          08-18-2017
        </PropertyItem>
      </div>
    );
    expect(pregnancyContinuous[2]).toEqual(
      <div className="marginLeft">
        <PropertyItem
          label={
            <FormattedMessage
              id="NOTIFICATION.ACTUAL_DELIVERY_DATE"
              values={{}}
            />
          }
        >
          08-01-2019
        </PropertyItem>
      </div>
    );
  });

  test('buildColsFromNotification for no expectedRTW or multipleConflictingExpectedRTWDates but has dateFirstMissingWork', () => {
    const returnToWorkNotification = buildColsFromNotification(
      notificationsFixture[8]
    );

    expect(returnToWorkNotification).toHaveLength(3);
  });
});
