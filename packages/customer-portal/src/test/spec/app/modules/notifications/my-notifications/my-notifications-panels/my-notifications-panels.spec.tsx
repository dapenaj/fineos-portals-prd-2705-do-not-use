import React from 'react';

import { MyNotificationsPanels } from '../../../../../../../app/modules/notifications/my-notifications';
import { notificationsFixture } from '../../../../../../common/fixtures';
import { DefaultContext, mount } from '../../../../../../config';
import { releaseEventLoop } from '../../../../../../common/utils';

describe('MyNotificationsPanels', () => {
  const props = {
    notifications: notificationsFixture
  };

  const mountMyNotification = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyNotificationsPanels {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render', async () => {
    const wrapper = mountMyNotification();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyNotificationsPanels)).toExist();
  });
});
