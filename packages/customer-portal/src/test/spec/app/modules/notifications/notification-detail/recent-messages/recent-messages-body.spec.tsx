import React from 'react';

import { shallow } from '../../../../../../config';
import { RecentMessagesBody } from '../../../../../../../app/modules/notifications/notification-detail/recent-messages';
import {
  messagesFixture,
  customerFixture
} from '../../../../../../common/fixtures';

describe('RecentMessagesBody', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <RecentMessagesBody
          messages={messagesFixture}
          customer={customerFixture}
        />
      )
    ).toMatchSnapshot();
  });
});
