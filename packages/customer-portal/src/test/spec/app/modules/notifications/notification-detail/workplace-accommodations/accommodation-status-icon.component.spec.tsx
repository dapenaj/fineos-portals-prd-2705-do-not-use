import React from 'react';

import { shallow } from '../../../../../../config';
import { AccommodationStatusIcon } from '../../../../../../../app/modules/notifications/notification-detail';

describe('AccommodationStatusIcon', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<AccommodationStatusIcon status="" decision="" />)
    ).toMatchSnapshot();
  });

  test('render - Pending -- SNAPSHOT', () => {
    expect(
      shallow(<AccommodationStatusIcon status="Pending" decision="" />)
    ).toMatchSnapshot();
  });

  test('render - Accommodated -- SNAPSHOT', () => {
    expect(
      shallow(
        <AccommodationStatusIcon status="Completion" decision="Accommodated" />
      )
    ).toMatchSnapshot();
  });

  test('render - Not Accommodated -- SNAPSHOT', () => {
    expect(
      shallow(
        <AccommodationStatusIcon status="Closed" decision="Not Accommodated" />
      )
    ).toMatchSnapshot();
  });
});
