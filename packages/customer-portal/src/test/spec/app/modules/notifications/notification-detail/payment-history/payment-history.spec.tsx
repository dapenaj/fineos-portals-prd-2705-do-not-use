import React from 'react';

import { shallow } from '../../../../../../config';
import { PaymentHistory } from '../../../../../../../app/modules/notifications/notification-detail';
import { paymentFixture } from '../../../../../../common/fixtures';

describe('PaymentHistory', () => {
  const props = {
    paymentDetails: paymentFixture,
    caseType: 'Short Term Disability',
    loading: false
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PaymentHistory {...props} />)).toMatchSnapshot();
  });
});
