import React from 'react';
import { IntlProvider } from 'react-intl';
import { Modal } from 'antd';
import { EpisodicLeavePeriodDetail } from 'fineos-js-api-client';

import { shallow, mount, DefaultContext } from '../../../../../../../config';
import { expectedExtractIntermittentPeriodsFromPeriodDecisionsValue } from '../../../../../../../common/fixtures';
import {
  IntermittentTimeReports,
  IntermittentTimeReportTable,
  IntermittentTimeReportModal
} from '../../../../../../../../app/modules/notifications/notification-detail';

describe('Intemrittent Time Reports', () => {
  const props = {
    decision:
      expectedExtractIntermittentPeriodsFromPeriodDecisionsValue
        .intermittentPeriodDecisions[0]
  };

  const mountIntermittentTimeReports = () => {
    return mount(
      <DefaultContext>
        <IntermittentTimeReports {...props} />
      </DefaultContext>
    );
  };

  test('render component', () => {
    const wrapper = mountIntermittentTimeReports();

    expect(wrapper.find(IntermittentTimeReports)).toExist();

    expect(wrapper.find(IntermittentTimeReportTable)).toExist();
    expect(wrapper.find(IntermittentTimeReportModal)).toExist();
    expect(
      wrapper
        .find(IntermittentTimeReportModal)
        .find(Modal)
        .prop('visible')
    ).toBeFalsy();
  });

  test('should toggle showing the table', () => {
    const wrapper = mountIntermittentTimeReports();

    const toggleBtn = wrapper.find(
      '[data-test-el="intermittent-time-reports-toggle-button"]'
    );

    expect(wrapper.find(IntermittentTimeReportTable)).toExist();
    expect(toggleBtn).toExist();

    toggleBtn.hostNodes().simulate('click');
    wrapper.update();

    expect(wrapper.find(IntermittentTimeReportTable)).not.toExist();

    toggleBtn.hostNodes().simulate('click');
    wrapper.update();

    expect(wrapper.find(IntermittentTimeReportTable)).toExist();
  });

  test('should toggle the modal', () => {
    const wrapper = mountIntermittentTimeReports();

    const modalToggleBtn = wrapper.find(
      '[data-test-el="intermittent-time-reports-modal-button"]'
    );

    expect(modalToggleBtn).toExist();
    expect(
      wrapper
        .find(IntermittentTimeReportModal)
        .find(Modal)
        .prop('visible')
    ).toBeFalsy();

    modalToggleBtn.hostNodes().simulate('click');
    wrapper.update();

    expect(
      wrapper
        .find(IntermittentTimeReportModal)
        .find(Modal)
        .prop('visible')
    ).toBeTruthy();
    expect(
      wrapper
        .find(IntermittentTimeReportModal)
        .find(IntermittentTimeReportTable)
    ).toExist();
  });

  test('render intermittent period -- SNAPSHOT', () => {
    expect(
      shallow(
        <IntlProvider locale="en">
          <IntermittentTimeReports {...props} />
        </IntlProvider>
      )
    ).toMatchSnapshot();
  });

  test('episodic leave period empty -- SNAPSHOT', () => {
    const fixture = {
      ...expectedExtractIntermittentPeriodsFromPeriodDecisionsValue
        .intermittentPeriodDecisions[0],
      episodicLeavePeriodDetail: {} as EpisodicLeavePeriodDetail
    };

    expect(
      shallow(
        <IntlProvider locale="en">
          <IntermittentTimeReports decision={fixture} />
        </IntlProvider>
      )
    ).toMatchSnapshot();
  });
});
