import React from 'react';
import { FormattedMessage } from 'react-intl';
import each from 'jest-each';
import { AbsencePeriodDecision, Benefit } from 'fineos-js-api-client';
import moment from 'moment';

jest.mock('moment', () => {
  // UTC everywhere
  const actualMoment = jest.requireActual('moment');
  return actualMoment.utc;
});

import {
  GanttEntryColor,
  GanttEntryShape
} from '../../../../../../../app/modules/notifications/notification-detail/timeline/gantt';
import {
  absencePeriodTypeToShape,
  absenceToColor,
  accommodationStatusToColor,
  getBenefitOptions,
  jobProtectedLeaveAbsenceToTooltip,
  wageReplacementDisplayStatusToColor
} from '../../../../../../../app/modules/notifications/notification-detail/timeline/timeline.util';
import { WageReplacementDisplayStatus } from '../../../../../../../app/shared/types';

describe('timeline.util', () => {
  describe('absencePeriodTypeToShape', () => {
    each([
      ['Time off period', GanttEntryShape.FILLED],
      ['Episodic', GanttEntryShape.STRIPES],
      ['Reduced Schedule', GanttEntryShape.SEMI_FILLED]
    ]).test(
      'should map absencePeriodType "%s" to gantt %s shape',
      (absencePeriodType: string, expectedShape: GanttEntryShape) => {
        expect(absencePeriodTypeToShape(absencePeriodType)).toBe(expectedShape);
      }
    );
  });

  describe('absenceToColor', () => {
    each([
      ['Approved', '', GanttEntryColor.GREEN],
      ['Certified', '', GanttEntryColor.GREEN],
      ['Time Available', '', GanttEntryColor.GREEN],
      ['Pending', '', GanttEntryColor.YELLOW],
      ['Pending Certification', '', GanttEntryColor.YELLOW],
      ['Denied', '', GanttEntryColor.RED],
      ['', 'Cancelled', GanttEntryColor.GREY],
      ['', '', null]
    ]).test(
      'should map timeDecisionStatus "%s" and absencePeriodStatus "%s" to gantt %s color',
      (
        timeDecisionStatus: string,
        absencePeriodStatus: string,
        expectedColor: GanttEntryColor | null
      ) => {
        const absence: Partial<AbsencePeriodDecision> = {
          timeDecisionStatus,
          absencePeriodStatus,
          decisionStatus: 'Approved',
          adjudicationStatus: 'Accepted'
        };

        expect(absenceToColor(absence as AbsencePeriodDecision)).toBe(
          expectedColor
        );
      }
    );
  });

  describe('wageReplacementDisplayStatusToColor', () => {
    each([
      ['Approved', GanttEntryColor.GREEN],
      ['Pending', GanttEntryColor.YELLOW],
      ['Denied', GanttEntryColor.RED],
      ['Decided', GanttEntryColor.GREY],
      ['Closed', GanttEntryColor.GREY]
    ]).test(
      'should map wageReplacementDisplayStatus "%s" to gantt color %s',
      (
        wageReplacementDisplayStatus: WageReplacementDisplayStatus,
        expectedColor: GanttEntryColor
      ) => {
        expect(
          wageReplacementDisplayStatusToColor(wageReplacementDisplayStatus)
        ).toBe(expectedColor);
      }
    );
  });

  describe('accommodationStatusToColor', () => {
    each([
      ['Decided', GanttEntryColor.GREEN],
      ['Registration', GanttEntryColor.YELLOW],
      ['Assessment', GanttEntryColor.YELLOW],
      ['Monitoring', GanttEntryColor.YELLOW],
      ['Closed', GanttEntryColor.GREY]
    ]).test(
      'should map wageReplacementDisplayStatus "%s" to gantt color %s',
      (accommodationStatus: string, expectedColor: GanttEntryColor) => {
        expect(accommodationStatusToColor(accommodationStatus)).toBe(
          expectedColor
        );
      }
    );
  });

  describe('jobProtectedLeaveAbsenceToTooltip', () => {
    each([
      [
        {
          absencePeriodStatus: 'Approved',
          absencePeriodType: 'Episodic',
          timeEntitlement: 3,
          timeEntitlementBasis: 'days',
          timeWithinPeriodBasis: 'day',
          startDate: '2019-01-01',
          endDate: '2019-01-04',
          episodicLeavePeriodDetail: {
            frequency: 5,
            frequencyInterval: 2,
            frequencyIntervalBasis: 'Months',
            duration: 10,
            durationBasis: 'Hours'
          }
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_INTERMITTENT',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019',
            requestedTimeAmount: 10,
            requestedTimeUnit: 'Hours',
            requestedTimeDuration: 2,
            requestedTimeDurationUnit: 'Months'
          }
        }
      ],
      [
        {
          absencePeriodStatus: 'Approved',
          absencePeriodType: 'Episodic',
          timeEntitlement: 3,
          timeEntitlementBasis: 'days',
          timeWithinPeriodBasis: 'day',
          startDate: '2019-01-01',
          endDate: '2019-01-04',
          episodicLeavePeriodDetail: {
            frequency: 0,
            frequencyInterval: 0,
            frequencyIntervalBasis: '',
            duration: 0,
            durationBasis: ''
          }
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_INTERMITTENT_UNSPECIFIED',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019'
          }
        }
      ],
      [
        {
          absencePeriodStatus: 'Pending',
          absencePeriodType: 'Episodic',
          startDate: '2019-01-01',
          endDate: '2019-01-04',
          episodicLeavePeriodDetail: {
            frequency: 5,
            frequencyInterval: 2,
            frequencyIntervalBasis: 'Months',
            duration: 10,
            durationBasis: 'Hours'
          }
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_INTERMITTENT',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019',
            requestedTimeAmount: 10,
            requestedTimeUnit: 'Hours',
            requestedTimeDuration: 2,
            requestedTimeDurationUnit: 'Months'
          }
        }
      ],
      [
        {
          absencePeriodStatus: 'Approved',
          absencePeriodType: 'Time off period',
          startDate: '2019-01-01',
          endDate: '2019-01-04'
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_CONTINUOUS',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019'
          }
        }
      ],
      [
        {
          absencePeriodStatus: 'Pending',
          absencePeriodType: 'Time off period',
          startDate: '2019-01-01',
          endDate: '2019-01-04'
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_CONTINUOUS',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019'
          }
        }
      ],
      [
        {
          absencePeriodStatus: 'Approved',
          absencePeriodType: 'Reduced Schedule',
          startDate: '2019-01-01',
          endDate: '2019-01-04'
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_REDUCED',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019'
          }
        }
      ],
      [
        {
          absencePeriodStatus: 'Pending',
          absencePeriodType: 'Reduced Schedule',
          startDate: '2019-01-01',
          endDate: '2019-01-04'
        },
        {
          id:
            'NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.REQUESTED_REDUCED',
          values: {
            startDate: '01-01-2019',
            endDate: '01-04-2019'
          }
        }
      ]
    ]).test(
      'should map absence to tooltip, test case #%#',
      (absence: AbsencePeriodDecision, props: any) => {
        expect(jobProtectedLeaveAbsenceToTooltip(absence).props).toEqual(props);
      }
    );
  });

  describe('getBenefitOptions', () => {
    test('should map properly benefit.benefitRightCategory === "Recurring Benefit"', () => {
      const benefit = {
        benefitId: 0,
        benefitRightCategory: 'Recurring Benefit'
      } as Partial<Benefit>;
      const ganttEntry = getBenefitOptions(
        benefit as Benefit,
        {
          benefitId: 0,
          disabilityBenefitSummary: {
            disabilityBenefit: {
              benefitStartDate: '2019-01-02',
              benefitEndDate: '2019-01-04'
            }
          }
        } as any
      )!;

      expect(ganttEntry).toMatchObject({
        color: GanttEntryColor.GREY,
        tooltip: (
          <FormattedMessage
            id="NOTIFICATION.TIMELINE.WAGE_REPLACEMENT_TOOLTIP.CLOSED"
            values={{}}
          />
        )
      });

      expect(ganttEntry.startDate.valueOf()).toEqual(
        moment('2019-01-02').valueOf()
      );
      expect(ganttEntry.endDate.valueOf()).toEqual(
        moment('2019-01-05').valueOf()
      );
    });
    test('should map properly benefit.benefitRightCategory === "Recurring Benefit" without date', () => {
      const benefit = {
        benefitId: 0,
        benefitRightCategory: 'Recurring Benefit'
      } as Partial<Benefit>;
      const ganttEntry = getBenefitOptions(
        benefit as Benefit,
        {
          benefitId: 0,
          disabilityBenefitSummary: {
            disabilityBenefit: {
              benefitEndDate: '2019-01-04'
            }
          }
        } as any
      )!;

      expect(ganttEntry).toMatchObject({
        color: GanttEntryColor.GREY,
        tooltip: (
          <FormattedMessage
            id="NOTIFICATION.TIMELINE.WAGE_REPLACEMENT_TOOLTIP.CLOSED"
            values={{}}
          />
        )
      });

      expect(ganttEntry.endDate.valueOf()).toEqual(
        moment('2019-01-05').valueOf()
      );
    });

    test('should map properly benefit.benefitRightCategory === "Lump Sum Benefit"', () => {
      const benefit = {
        benefitId: 0,
        status: 'Pending',
        benefitRightCategory: 'Lump Sum Benefit',
        benefitIncurredDateMoment: moment('2019-01-02')
      } as Partial<Benefit>;

      const benefitOptions = getBenefitOptions(benefit as Benefit, {} as any)!;
      expect(benefitOptions).toMatchObject({
        color: GanttEntryColor.YELLOW
      });

      expect(benefitOptions.startDate.valueOf()).toEqual(
        moment('2019-01-02').valueOf()
      );
      expect(benefitOptions.endDate.valueOf()).toEqual(
        moment('2019-01-03').valueOf()
      );
    });
  });
});
