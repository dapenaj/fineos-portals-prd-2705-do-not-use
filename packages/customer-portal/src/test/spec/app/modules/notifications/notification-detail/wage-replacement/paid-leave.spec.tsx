import React from 'react';

import { mount, DefaultContext, ReactWrapper } from '../../../../../../config';
import {
  PaidLeave,
  WageReplacementProperties
} from '../../../../../../../app/modules/notifications/notification-detail';
import {
  benefitsFixture,
  disabilityBenefitSummariesFixture
} from '../../../../../../common/fixtures';
import { findPaymentsSpy } from '../../../../../../common/spys';
import { releaseEventLoop } from '../../../../../../common/utils';

describe('PaidLeave', () => {
  let wrapper: ReactWrapper;

  const props = {
    benefits: benefitsFixture,
    claimId: 'CLM-1',
    disabilities: disabilityBenefitSummariesFixture,
    paidLeaveManager: 'test',
    paidLeaveManagerPhoneNumber: 'test'
  };

  const mountPaidLeave = () =>
    mount(
      <DefaultContext>
        <PaidLeave {...props} />
      </DefaultContext>
    );

  test('render', async () => {
    findPaymentsSpy('success');

    wrapper = mountPaidLeave();

    await releaseEventLoop();

    expect(wrapper.find(WageReplacementProperties).prop('loading')).toBe(false);
  });

  test('render - failure', async () => {
    findPaymentsSpy('failure');

    wrapper = mountPaidLeave();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(WageReplacementProperties)).toExist();
  });

  test('render - success', async () => {
    findPaymentsSpy('success');

    wrapper = mountPaidLeave();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(WageReplacementProperties)).toExist();
  });
});
