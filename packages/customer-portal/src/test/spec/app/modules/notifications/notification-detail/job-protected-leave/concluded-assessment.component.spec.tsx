import React from 'react';

import { shallow } from '../../../../../../config';
import { ConcludedAssessment } from '../../../../../../../app/modules/notifications/notification-detail';
import { absencePeriodDecisionsFixture } from '../../../../../../common/fixtures';

describe('ConcludedAssessment', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <ConcludedAssessment
          title="test"
          subtitle="test"
          decisions={absencePeriodDecisionsFixture[0].absencePeriodDecisions}
        />
      )
    ).toMatchSnapshot();
  });

  test('render - default -- SNAPSHOT', () => {
    expect(
      shallow(
        <ConcludedAssessment
          decisions={absencePeriodDecisionsFixture[0].absencePeriodDecisions}
        />
      )
    ).toMatchSnapshot();
  });
});
