import React from 'react';

import { mount, DefaultContext } from '../../../../../../config';
import {
  WageReplacement,
  WageReplacementHeader,
  DisabilityBenefits,
  PaidLeaves
} from '../../../../../../../app/modules/notifications/notification-detail';
import {
  absencesFixture,
  absenceSummariesFixture,
  benefitsFixture,
  disabilityBenefitSummariesFixture
} from '../../../../../../common/fixtures';

describe('WageReplacement', () => {
  const props = {
    claimIds: ['CLM-1'],
    absenceSummaries: absenceSummariesFixture,
    claimBenefits: [
      {
        claimId: 'CLM-1',
        benefits: benefitsFixture
      }
    ],
    paidLeaves: absencesFixture.map(absence => ({
      ...absence,
      claimBenefits: [
        {
          claimId: 'CLM-1',
          benefits: benefitsFixture
        }
      ],
      paidLeaveHandler: 'string',
      paidLeaveHandlerPhoneNumber: 'string'
    })),
    disabilityBenefitWrappers: [
      {
        claimId: 'CLM-1',
        benefitId: 0,
        disabilityBenefitSummary: disabilityBenefitSummariesFixture[0]
      }
    ]
  };

  const mountWageReplacement = () =>
    mount(
      <DefaultContext>
        <WageReplacement {...props} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountWageReplacement();

    expect(wrapper.find(WageReplacementHeader)).toExist();
    expect(wrapper.find(DisabilityBenefits)).toExist();
    expect(wrapper.find(PaidLeaves)).toExist();
  });
});
