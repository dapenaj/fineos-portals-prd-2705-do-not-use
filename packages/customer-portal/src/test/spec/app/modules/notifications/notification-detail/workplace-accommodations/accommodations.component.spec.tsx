import React from 'react';

import { shallow } from '../../../../../../config';
import { Accommodations } from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationAccommodationsFixture } from '../../../../../../common/fixtures';

describe('Accommodations', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <Accommodations accommodations={notificationAccommodationsFixture} />
      )
    ).toMatchSnapshot();
  });
});
