import React from 'react';

import { shallow } from '../../../../../../config';
import { MyNotificationsStatus } from '../../../../../../../app/modules/notifications/my-notifications';
import { NotificationStatusResult } from '../../../../../../../app/shared/types';

describe('MyNotificationsStatus', () => {
  const props = {
    status: NotificationStatusResult.PENDING,
    caseProgress: 'Pregnancy, birth or related medical treatment'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyNotificationsStatus {...props} />)).toMatchSnapshot();
  });

  const propsOpen = {
    status: NotificationStatusResult.SOME_DECIDED,
    caseProgress: 'Pregnancy, birth or related medical treatment'
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyNotificationsStatus {...propsOpen} />)).toMatchSnapshot();
  });

  const propsDecided = {
    status: NotificationStatusResult.DECIDED,
    caseProgress: 'Pregnancy, birth or related medical treatment'
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<MyNotificationsStatus {...propsDecided} />)
    ).toMatchSnapshot();
  });

  const propsAllClosed = {
    status: NotificationStatusResult.ALL_CLOSED,
    caseProgress: 'Pregnancy, birth or related medical treatment'
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<MyNotificationsStatus {...propsAllClosed} />)
    ).toMatchSnapshot();
  });

  const propsUndetermined = {
    status: NotificationStatusResult.UNDETERMINED,
    caseProgress: 'Pregnancy, birth or related medical treatment'
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<MyNotificationsStatus {...propsUndetermined} />)
    ).toMatchSnapshot();
  });
});
