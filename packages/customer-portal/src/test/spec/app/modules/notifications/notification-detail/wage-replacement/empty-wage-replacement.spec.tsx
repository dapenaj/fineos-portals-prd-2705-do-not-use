import React from 'react';

import { shallow } from '../../../../../../config';
import { EmptyWageReplacement } from '../../../../../../../app/modules/notifications/notification-detail';

describe('EmptyWageReplacement', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<EmptyWageReplacement />)).toMatchSnapshot();
  });
});
