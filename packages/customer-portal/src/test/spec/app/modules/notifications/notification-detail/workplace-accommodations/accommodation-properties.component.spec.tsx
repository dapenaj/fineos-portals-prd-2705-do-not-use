import React from 'react';

import { shallow } from '../../../../../../config';
import { AccommodationProperties } from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationAccommodationsFixture } from '../../../../../../common/fixtures';

describe('AccommodationProperties', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <AccommodationProperties
          accommodation={notificationAccommodationsFixture[0]}
        />
      )
    ).toMatchSnapshot();
  });
});
