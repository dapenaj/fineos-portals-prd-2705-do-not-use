import { AbsencePeriodDecision } from 'fineos-js-api-client';

import {
  absencesWithHandlersFixture,
  expectedExtractIntermittentPeriodsFromPeriodDecisionsValue,
  extractIntermittentPeriodsFromPeriodDecisionsFixture
} from '../../../../../../common/fixtures';
import {
  getPeriodDecisionStatus,
  getStatusCategory,
  extractIntermittentPeriodsFromPeriodDecisions
} from '../../../../../../../app/modules/notifications/notification-detail';

describe('getPeriodDecisionStatus', () => {
  test('the correct status is returned', () => {
    // simple scenarios
    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Pending',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('pending');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Projected',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('pending');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'In Review',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('pending');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Cancelled',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('cancelled');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Denied',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('declined');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: '',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('none');

    // goes one level deeper
    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: '',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('none');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Rejected',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('declined');

    // goes another level deeper
    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: 'Pending',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('pending');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: 'Pending Certification',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('pending');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: 'Approved',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('approved');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: 'Certified',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('approved');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: 'Time Available',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('approved');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: 'Denied',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBe('declined');

    // a deeper level, once more we go
    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: '',
        absencePeriodStatus: 'Cancelled'
      } as AbsencePeriodDecision)
    ).toBe('cancelled');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: '',
        absencePeriodStatus: 'Episodic'
      } as AbsencePeriodDecision)
    ).toBe('approved');

    expect(
      getPeriodDecisionStatus({
        decisionStatus: 'Approved',
        adjudicationStatus: 'Accepted',
        timeDecisionStatus: '',
        absencePeriodStatus: ''
      } as AbsencePeriodDecision)
    ).toBeNull();
  });
});

describe('getStatusCategory', () => {
  test('the correct status is returned', () => {
    expect(
      getStatusCategory(
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: ''
        } as AbsencePeriodDecision,
        null,
        null
      )
    ).toBe('pending');

    // should be skipped, because it ain't consecutive
    expect(
      getStatusCategory(
        {
          decisionStatus: 'Approved',
          adjudicationStatus: 'Accepted',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-02-04',
          endDate: '2019-02-05'
        } as AbsencePeriodDecision,
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-02',
          endDate: '2019-01-03'
        } as AbsencePeriodDecision,
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-03-05',
          endDate: '2019-03-06'
        } as AbsencePeriodDecision
      )
    ).toBe('skipped');

    // should get it from the mix of prev/next
    expect(
      getStatusCategory(
        {
          decisionStatus: 'Approved',
          adjudicationStatus: 'Accepted',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-04',
          endDate: '2019-01-05'
        } as AbsencePeriodDecision,
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-02',
          endDate: '2019-01-03'
        } as AbsencePeriodDecision,
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-05',
          endDate: '2019-01-06'
        } as AbsencePeriodDecision
      )
    ).toBe('pending');

    // should get it from the prev
    expect(
      getStatusCategory(
        {
          decisionStatus: 'Approved',
          adjudicationStatus: 'Accepted',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-04',
          endDate: '2019-01-05'
        } as AbsencePeriodDecision,
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-02',
          endDate: '2019-01-03'
        } as AbsencePeriodDecision,
        null
      )
    ).toBe('pending');

    // should get it from the next
    expect(
      getStatusCategory(
        {
          decisionStatus: 'Approved',
          adjudicationStatus: 'Accepted',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-04',
          endDate: '2019-01-05'
        } as AbsencePeriodDecision,
        null,
        {
          decisionStatus: 'Pending',
          adjudicationStatus: '',
          timeDecisionStatus: '',
          absencePeriodStatus: '',
          startDate: '2019-01-05',
          endDate: '2019-01-06'
        } as AbsencePeriodDecision
      )
    ).toBe('pending');
  });
});

describe('extractIntermittentPeriodsFromPeriodDecisions', () => {
  test('should return a list of intermittent period decisions and other period decisions', () => {
    expect(
      extractIntermittentPeriodsFromPeriodDecisions(
        extractIntermittentPeriodsFromPeriodDecisionsFixture,
        absencesWithHandlersFixture[0]
      )
    ).toStrictEqual(expectedExtractIntermittentPeriodsFromPeriodDecisionsValue);
  });
});
