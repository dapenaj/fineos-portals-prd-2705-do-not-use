import React from 'react';

import { shallow } from '../../../../../../../config';
import { PeriodDecisionStatusIcon } from '../../../../../../../../app/modules/notifications/notification-detail';

describe('PeriodDecisionStatusIcon', () => {
  test('render - pending -- SNAPSHOT', () => {
    expect(
      shallow(<PeriodDecisionStatusIcon status={'pending'} count={0} />)
    ).toMatchSnapshot();
  });

  test('render - completion -- SNAPSHOT', () => {
    expect(
      shallow(<PeriodDecisionStatusIcon status={'approved'} count={1} />)
    ).toMatchSnapshot();
  });

  test('render - declined -- SNAPSHOT', () => {
    expect(
      shallow(<PeriodDecisionStatusIcon status={'declined'} count={2} />)
    ).toMatchSnapshot();
  });

  test('render - default -- SNAPSHOT', () => {
    expect(
      shallow(<PeriodDecisionStatusIcon status={'default'} count={3} />)
    ).toMatchSnapshot();
  });
});
