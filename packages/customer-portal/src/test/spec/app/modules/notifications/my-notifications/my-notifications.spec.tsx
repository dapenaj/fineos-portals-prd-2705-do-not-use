import React from 'react';

import { DefaultContext, mount } from '../../../../../config';
import { releaseEventLoop } from '../../../../../common/utils';
import { fetchNotificationsSpy } from '../../../../../common/spys';
import { MyNotifications } from '../../../../../../app/modules/notifications/my-notifications';
import { MyNotificationsView } from '../../../../../../app/modules/notifications/my-notifications/my-notifications.view';
import { Spinner } from '../../../../../../app/shared/ui';

describe('My Notifications', () => {
  const props = {
    loading: false,
    fetchNotifications: jest.fn(),
    notifications: null
  };

  const mountMyNotification = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <MyNotifications {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render loading', async () => {
    fetchNotificationsSpy();

    const wrapper = mountMyNotification({ loading: true });

    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
  });

  test('render notification detail view', async () => {
    fetchNotificationsSpy();

    const wrapper = mountMyNotification();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyNotificationsView)).toExist();
  });
});
