import React from 'react';

import { shallow } from '../../../../../../config';
import { ProposedAccommodations } from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationAccommodationsFixture } from '../../../../../../common/fixtures';

describe('ProposedAccommodations', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <ProposedAccommodations
          accommodations={
            notificationAccommodationsFixture[0].workplaceAccommodations
          }
        />
      )
    ).toMatchSnapshot();
  });
});
