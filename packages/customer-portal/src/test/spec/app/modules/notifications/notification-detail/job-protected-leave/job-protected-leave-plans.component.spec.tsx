import React from 'react';

import { shallow } from '../../../../../../config';
import { jobProtectedLeavePlanGroupsFixture } from '../../../../../../common/fixtures';
import { JobProtectedLeavePlans } from '../../../../../../../app/modules/notifications/notification-detail';
import { PeriodDecisionGroup } from '../../../../../../../app/shared/types';

describe('JobProtectedLeavePlans', () => {
  const fixture = jobProtectedLeavePlanGroupsFixture as PeriodDecisionGroup[];
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<JobProtectedLeavePlans groups={fixture} />)
    ).toMatchSnapshot();
  });

  test('render expanded -- SNAPSHOT', async () => {
    const wrapper = shallow(<JobProtectedLeavePlans groups={fixture} />);

    wrapper.setState({
      activeKey: 'My Child Bonding'
    });

    expect(wrapper).toMatchSnapshot();
  });
});
