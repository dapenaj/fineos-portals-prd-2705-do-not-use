import React from 'react';

import { shallow } from '../../../../../../config';
import { ProposedAccommodationProperties } from '../../../../../../../app/modules/notifications/notification-detail';
import { notificationAccommodationsFixture } from '../../../../../../common/fixtures';

describe('ProposedAccommodationProperties', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <ProposedAccommodationProperties
          accommodation={
            notificationAccommodationsFixture[0].workplaceAccommodations[0]
          }
        />
      )
    ).toMatchSnapshot();
  });
});
