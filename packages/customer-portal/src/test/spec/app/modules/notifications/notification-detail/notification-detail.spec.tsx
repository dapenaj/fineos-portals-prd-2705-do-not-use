import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { injectIntl } from 'react-intl';

import { DefaultContext, mount } from '../../../../../config';
import { releaseEventLoop } from '../../../../../common/utils';
import {
  notificationsFixture,
  appConfigFixture
} from '../../../../../common/fixtures';
import {
  fetchNotificationsSpy,
  findBenefitsSpy,
  getAbsenceDetailsSpy,
  getAbsencePeriodDecisionsSpy,
  readDisabilityBenefitSpy
} from '../../../../../common/spys';
import {
  JobProtectedLeave,
  NotificationDetailContainer,
  NotificationDetailView,
  Timeline,
  WageReplacement
} from '../../../../../../app/modules/notifications/notification-detail';
import { Spinner } from '../../../../../../app/shared/ui';

describe('Notification Detail', () => {
  const props = {
    notification: notificationsFixture[0],
    loading: false,
    fetchNotifications: jest.fn(),
    notifications: null,
    config: appConfigFixture,
    ...{
      match: {
        params: { id: 'NTN-21' }
      }
    }
  };
  const NotificationDetailContainerWithIntl = injectIntl(
    NotificationDetailContainer
  );

  const routerProps = {} as RouteComponentProps<{ id: string }>;

  const mountNotificationDetail = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <NotificationDetailContainerWithIntl
          {...props}
          {...routerProps}
          {...extraProps}
        />
      </DefaultContext>
    );

  test('render notification fetching loading', async () => {
    fetchNotificationsSpy();

    const wrapper = mountNotificationDetail({
      loading: true,
      notification: null
    });

    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
    expect(wrapper.find(NotificationDetailView)).not.toExist();
  });

  test('render Timeline if all data fetched', async () => {
    fetchNotificationsSpy();
    getAbsenceDetailsSpy('success');
    getAbsencePeriodDecisionsSpy('success');
    findBenefitsSpy('success');
    readDisabilityBenefitSpy('success');

    const wrapper = mountNotificationDetail();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Timeline)).toExist();
  });

  test('render JobProtectedLeave if period decisions loaded', async () => {
    fetchNotificationsSpy();
    getAbsenceDetailsSpy('success');
    getAbsencePeriodDecisionsSpy('success');

    const wrapper = mountNotificationDetail();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(JobProtectedLeave)).toExist();
  });

  test('render WageReplacement if absence details and benefits fetched', async () => {
    fetchNotificationsSpy();
    findBenefitsSpy('success');
    getAbsenceDetailsSpy('success');
    readDisabilityBenefitSpy('success');

    const wrapper = mountNotificationDetail();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(WageReplacement)).toExist();
  });
});
