import React from 'react';

import { shallow } from '../../../../../../config';
import { HandlerInfo } from '../../../../../../../app/modules/notifications/notification-detail';

describe('HandlerInfo', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<HandlerInfo name="test" phoneNumber="123" />)
    ).toMatchSnapshot();
  });

  test('render - no name -- SNAPSHOT', () => {
    expect(
      shallow(<HandlerInfo name="" phoneNumber="123" />)
    ).toMatchSnapshot();
  });

  test('render - no phoneNumber -- SNAPSHOT', () => {
    expect(
      shallow(<HandlerInfo name="test" phoneNumber="" />)
    ).toMatchSnapshot();
  });
});
