import React from 'react';

import { shallow } from '../../../../../../../config';
import { PeriodDecisionHeaderIcon } from '../../../../../../../../app/modules/notifications/notification-detail';
import { periodDecisionsFixture } from '../../../../../../../common/fixtures';
import { EnhancedAbsencePeriodDecision } from '../../../../../../../../app/shared/types/job-protected-leave.type';

describe('PeriodDecisionHeaderIcon', () => {
  test('render - Episodic -- SNAPSHOT', () => {
    expect(
      shallow(
        <PeriodDecisionHeaderIcon
          decisions={periodDecisionsFixture as EnhancedAbsencePeriodDecision[]}
        />
      )
    ).toMatchSnapshot();
  });
});
