import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import {
  notificationsFixture,
  customerFixture
} from '../../../../../../common/fixtures';
import { DefaultContext, mount } from '../../../../../../config';
import { releaseEventLoop } from '../../../../../../common/utils';
import {
  fetchEnhancedMessagesByCaseIdSpy,
  findBenefitsSpy,
  addMessageSpy,
  getCaseIdsFromNotificationSpy
} from '../../../../../../common/spys';
import {
  RecentMessages,
  RecentMessagesComponent
} from '../../../../../../../app/modules/notifications/notification-detail';

describe('RecentMessages', () => {
  let instance: RecentMessagesComponent;

  const props = {
    notification: notificationsFixture[0],
    customer: customerFixture
  };

  const mountRecentMessages = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <RecentMessages {...props} {...extraProps} />
      </DefaultContext>
    );

  test('fetching benefits failure', async () => {
    getCaseIdsFromNotificationSpy('failure');
    fetchEnhancedMessagesByCaseIdSpy('success');

    const wrapper = mountRecentMessages();

    await releaseEventLoop();

    wrapper.update();

    expect(
      wrapper
        .find('[data-test-el="recent-messages-header-unread-count"]')
        .first()
        .text()
    ).toContain('3 unread');
  });

  test('fetching benefits success', async () => {
    getCaseIdsFromNotificationSpy('success');
    fetchEnhancedMessagesByCaseIdSpy('success');

    const wrapper = mountRecentMessages();

    await releaseEventLoop();

    wrapper.update();

    expect(
      wrapper
        .find('[data-test-el="recent-messages-header-unread-count"]')
        .first()
        .text()
    ).toContain('3 unread');
  });

  test('handle add new message', async () => {
    getCaseIdsFromNotificationSpy('success');
    fetchEnhancedMessagesByCaseIdSpy('success');
    findBenefitsSpy('success');
    addMessageSpy('success');

    const wrapper = mountRecentMessages();
    instance = wrapper
      .find(RecentMessagesComponent)
      .instance() as RecentMessagesComponent;

    await releaseEventLoop();

    wrapper.update();

    instance.handleAddMessage({
      caseId: 'BEN-1',
      subject: 'test',
      narrative: 'test'
    });

    wrapper.update();
  });
});
