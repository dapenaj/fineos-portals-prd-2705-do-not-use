import React from 'react';

import { shallow } from '../../../../../../config';
import { paymentFixture } from '../../../../../../common/fixtures';
import { PaymentModalContent } from '../../../../../../../app/modules/notifications/notification-detail';

describe('PaymentModalContent', () => {
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(
        <PaymentModalContent
          caseType="Short Term Disability"
          paymentDetails={paymentFixture}
        />
      )
    ).toMatchSnapshot();
  });
});
