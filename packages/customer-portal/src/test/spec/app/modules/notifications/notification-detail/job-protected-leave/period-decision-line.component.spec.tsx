import React from 'react';
import { IntlProvider } from 'react-intl';

import {
  shallow,
  mount,
  DefaultContext,
  ReactWrapper
} from '../../../../../../config';
import { periodDecisionsFixture } from '../../../../../../common/fixtures';
import { RequestConfirmationModal } from '../../../../../../../app/shared/ui';
import {
  PeriodDecisionLineComponent,
  PeriodDecisionLine,
  PeriodDecisionLineState
} from '../../../../../../../app/modules/notifications/notification-detail';

describe('PeriodDecisionLine', () => {
  let wrapper: ReactWrapper, periodDecisionLine: PeriodDecisionLineComponent;

  beforeEach(() => {
    wrapper = mountPeriodDecisionProperties();

    periodDecisionLine = wrapper
      .find(PeriodDecisionLineComponent)
      .instance() as PeriodDecisionLineComponent;
  });

  const props = {
    decision: { ...periodDecisionsFixture[0], statusCategory: 'approved' }
  };

  const mountPeriodDecisionProperties = () => {
    return mount(
      <DefaultContext>
        <PeriodDecisionLine {...props} />
      </DefaultContext>
    );
  };

  test('render -- SNAPSHOT', () => {
    const fixture = {
      ...periodDecisionsFixture[0],
      statusCategory: 'approved'
    };
    expect(
      shallow(
        <IntlProvider locale="en">
          <PeriodDecisionLine decision={fixture} />
        </IntlProvider>
      )
    ).toMatchSnapshot();
  });

  test('render with payment history -- SNAPSHOT', () => {
    const fixture = {
      ...periodDecisionsFixture[0],
      statusCategory: 'pending',
      leavePlanCategory: 'Paid'
    };
    expect(
      shallow(
        <IntlProvider locale="en">
          <PeriodDecisionLine decision={fixture} />
        </IntlProvider>
      )
    ).toMatchSnapshot();
  });

  test('opening and closing amend leave period form', () => {
    periodDecisionLine.showEditPeriodForm();

    expect((periodDecisionLine.state as PeriodDecisionLineState).isEdit).toBe(
      true
    );

    periodDecisionLine.closeForm();

    expect(
      (periodDecisionLine.state as PeriodDecisionLineState)
        .showEditLeavePeriodForm
    ).toBe(false);
  });

  test('receiving a confirmation modal', () => {
    periodDecisionLine.toggleConfirmationModal();

    expect(
      (periodDecisionLine.state as PeriodDecisionLineState)
        .showEditLeavePeriodForm
    ).toBe(false);

    wrapper.update();

    expect(wrapper.find(RequestConfirmationModal)).toExist();
  });

  test('submiting a period leave deletion request', () => {
    periodDecisionLine.submitDeleteRequest();

    expect((periodDecisionLine.state as PeriodDecisionLineState).isEdit).toBe(
      false
    );
  });
});
