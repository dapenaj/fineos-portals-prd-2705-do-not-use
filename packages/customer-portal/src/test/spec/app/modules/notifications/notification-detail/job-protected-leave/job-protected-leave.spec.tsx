import React from 'react';
import { AbsencePeriodDecisions } from 'fineos-js-api-client';

import { DefaultContext, mount, shallow } from '../../../../../../config';
import { singleAbsencePeriodDecisionsFixture } from '../../../../../../common/fixtures/notification.fixture';
import {
  absencePeriodDecisionsFixture,
  absencesWithHandlersFixture,
  periodDecisionsIntermittentFixture
} from '../../../../../../common/fixtures';
import {
  JobProtectedLeave,
  JobProtectedLeaveHeader,
  JobProtectedLeavePlans
} from '../../../../../../../app/modules/notifications/notification-detail';

describe('JobProtectedLeave', () => {
  const props = {
    periodDecisions: absencePeriodDecisionsFixture,
    absencesWithHandlers: absencesWithHandlersFixture
  };

  const mountJobProtectedLeave = () =>
    mount(
      <DefaultContext>
        <JobProtectedLeave {...props} />
      </DefaultContext>
    );

  test('render', () => {
    const wrapper = mountJobProtectedLeave();

    expect(wrapper.find(JobProtectedLeaveHeader)).toExist();
    expect(wrapper.find(JobProtectedLeavePlans)).toExist();
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<JobProtectedLeave {...props} />)).toMatchSnapshot();
  });

  test('render - single period decision -- SNAPSHOT', () => {
    const localProps = {
      periodDecisions: singleAbsencePeriodDecisionsFixture,
      absencesWithHandlers: absencesWithHandlersFixture
    };
    expect(shallow(<JobProtectedLeave {...localProps} />)).toMatchSnapshot();
  });

  test('render - Empty -- SNAPSHOT', () => {
    expect(
      shallow(
        <JobProtectedLeave periodDecisions={null} absencesWithHandlers={null} />
      )
    ).toMatchSnapshot();
  });

  test('render - without periodDecisions -- SNAPSHOT', () => {
    expect(
      shallow(
        <JobProtectedLeave
          periodDecisions={null}
          absencesWithHandlers={absencesWithHandlersFixture}
        />
      )
    ).toMatchSnapshot();
  });
});
