import React from 'react';

import { shallow } from '../../../../../config';
import { PreferenceHandler } from '../../../../../../app/modules/my-profile';

describe('PreferenceView', () => {
  const props = {
    title: 'Personal Details',
    body: 'this is the body text',
    link: 'Click the link'
  };
  test('render -- SNAPSHOT', () => {
    expect(shallow(<PreferenceHandler {...props} />)).toMatchSnapshot();
  });
});
