import React from 'react';

import { shallow } from '../../../../../config';
import { CommunicationPreferencesComponent } from '../../../../../../app/modules/my-profile';
import {
  contactFixture,
  appConfigFixture
} from '../../../../../common/fixtures';

describe('CommunicationPreferences', () => {
  const props = {
    contact: contactFixture,
    config: appConfigFixture
  };
  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<CommunicationPreferencesComponent {...props} />)
    ).toMatchSnapshot();
  });
});
