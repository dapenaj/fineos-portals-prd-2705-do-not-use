import React from 'react';

import { mount, DefaultContext } from '../../../../../config';
import { fetchPaymentPreferencesSpy } from '../../../../../common/spys';
import { releaseEventLoop } from '../../../../../common/utils';
import { Spinner } from '../../../../../../app/shared/ui';
import {
  PaymentPreferences,
  PaymentPreferencesView
} from '../../../../../../app/modules/my-profile/payment-preferences';
import { appConfigFixture } from '../../../../../common/fixtures';

describe('PaymentPreferences', () => {
  const props = {
    config: appConfigFixture
  };

  const mountMyPayments = (extraProps?: any) =>
    mount(
      <DefaultContext>
        <PaymentPreferences {...props} {...extraProps} />
      </DefaultContext>
    );

  test('render loading', async () => {
    fetchPaymentPreferencesSpy('success');

    const wrapper = mountMyPayments();

    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
  });

  test('render success', async () => {
    fetchPaymentPreferencesSpy('success');

    const wrapper = mountMyPayments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(PaymentPreferencesView)).toExist();
  });

  test('render failure', async () => {
    fetchPaymentPreferencesSpy('failure');

    const wrapper = mountMyPayments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(Spinner)).not.toExist();
  });
});
