import React from 'react';

import { shallow } from '../../../../../config';
import {
  occupationsFixture,
  customerOccupationsFixture
} from '../../../../../common/fixtures';
import { ClaimHandler } from '../../../../../../app/modules/my-profile';

describe('ClaimsHandlerComponent', () => {
  const props = {
    occupation: occupationsFixture[0],
    customerOccupation: customerOccupationsFixture[0]
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<ClaimHandler {...props} />)).toMatchSnapshot();
  });
});
