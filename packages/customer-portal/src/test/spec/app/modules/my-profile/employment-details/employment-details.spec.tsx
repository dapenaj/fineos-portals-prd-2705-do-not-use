import React from 'react';

import { shallow } from '../../../../../config';
import { EmploymentDetailsComponent } from '../../../../../../app/modules/my-profile';
import { appConfigFixture } from '../../../../../common/fixtures';

describe('EmploymentDetails', () => {
  const props = {
    config: appConfigFixture
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<EmploymentDetailsComponent {...props} />)
    ).toMatchSnapshot();
  });
});
