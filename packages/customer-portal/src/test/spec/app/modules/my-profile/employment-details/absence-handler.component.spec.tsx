import React from 'react';

import { shallow } from '../../../../../config';
import { occupationsFixture } from '../../../../../common/fixtures';
import { AbsenceHandler } from '../../../../../../app/modules/my-profile';

describe('ClaimsHandlerComponent', () => {
  const props = {
    occupation: occupationsFixture[0]
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<AbsenceHandler {...props} />)).toMatchSnapshot();
  });
});
