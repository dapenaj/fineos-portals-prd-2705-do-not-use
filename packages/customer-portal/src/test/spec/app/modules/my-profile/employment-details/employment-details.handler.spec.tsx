import React from 'react';

import { shallow } from '../../../../../config';
import { EmploymentDetailsHandler } from '../../../../../../app/modules/my-profile';
import { AuthorizationService } from '../../../../../../app/shared/services';
import { Role } from '../../../../../../app/shared/types';
import {
  occupationsFixture,
  customerOccupationsFixture
} from '../../../../../common/fixtures';

describe('EmploymentDetailsHandler', () => {
  const authorizationService: AuthorizationService = AuthorizationService.getInstance();

  const props = {
    occupation: occupationsFixture[0],
    customerOccupation: customerOccupationsFixture[0]
  };

  beforeEach(() => {
    authorizationService.authenticationService.displayRoles = [
      Role.ABSENCE_USER
    ];
  });

  test('render -- SNAPSHOT', () => {
    expect(shallow(<EmploymentDetailsHandler {...props} />)).toMatchSnapshot();
  });
});
