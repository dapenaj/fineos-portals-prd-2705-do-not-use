import React from 'react';

import { shallow } from '../../../../../config';
import { MyProfileHeader } from '../../../../../../app/modules/my-profile';

describe('ProfileHeader', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyProfileHeader />)).toMatchSnapshot();
  });
});
