import React from 'react';

import { shallow } from '../../../../../config';
import { PaymentPreferencesViewComponent } from '../../../../../../app/modules/my-profile/payment-preferences';
import {
  paymentPreferenceFixture,
  appConfigFixture
} from '../../../../../common/fixtures';

describe('PaymentPreferences View', () => {
  const props = {
    paymentPreferences: paymentPreferenceFixture,
    config: appConfigFixture,
    message: 'hi',
    loading: true
  };

  test('render -- SNAPSHOT', () => {
    expect(
      shallow(<PaymentPreferencesViewComponent {...props} />)
    ).toMatchSnapshot();
  });
});
