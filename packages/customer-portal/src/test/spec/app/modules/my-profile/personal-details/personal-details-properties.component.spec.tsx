import React from 'react';

import { shallow } from '../../../../../config';
import { PersonalDetailsProperties } from '../../../../../../app/modules/my-profile';
import {
  customerFixture,
  contactFixture
} from '../../../../../common/fixtures';

describe('PersonalDetails', () => {
  const props = {
    customer: customerFixture,
    contact: contactFixture
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PersonalDetailsProperties {...props} />)).toMatchSnapshot();
  });
});
