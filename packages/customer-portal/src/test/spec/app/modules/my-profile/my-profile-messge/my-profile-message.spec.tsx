import React from 'react';

import { shallow } from '../../../../../config';
import { MyProfileMessage } from '../../../../../../app/modules/my-profile';

describe('CommunicationPreferences', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyProfileMessage />)).toMatchSnapshot();
  });
});
