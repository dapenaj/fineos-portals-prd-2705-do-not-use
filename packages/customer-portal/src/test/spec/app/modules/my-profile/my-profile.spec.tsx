import React from 'react';

import { shallow } from '../../../../config';
import MyProfile from '../../../../../app/modules/my-profile';

describe('MyProfile', () => {
  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyProfile />)).toMatchSnapshot();
  });
});
