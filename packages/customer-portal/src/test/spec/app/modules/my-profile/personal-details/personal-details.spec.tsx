import React from 'react';

import { shallow } from '../../../../../config';
import { PersonalDetailsComponent } from '../../../../../../app/modules/my-profile';
import { appConfigFixture } from '../../../../../common/fixtures';

describe('PersonalDetails', () => {
  const props = {
    config: appConfigFixture
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<PersonalDetailsComponent {...props} />)).toMatchSnapshot();
  });
});
