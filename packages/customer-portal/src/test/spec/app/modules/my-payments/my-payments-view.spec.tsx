import React from 'react';

import { shallow } from '../../../../config';
import { paymentFixture } from '../../../../common/fixtures';
import { MyPaymentsView } from '../../../../../app/modules/my-payments';

describe('MyPaymentsView', () => {
  test('render -- SNAPSHOT', () => {
    const path = '/my-payments';
    expect(
      shallow(<MyPaymentsView payments={paymentFixture} loading={false} />)
    ).toMatchSnapshot();
  });
});
