import React from 'react';

import { shallow } from '../../../../config';
import { paymentFixture } from '../../../../common/fixtures';
import { MyPaymentsList } from '../../../../../app/modules/my-payments';

describe('MyPaymentsList', () => {
  const props = {
    payments: paymentFixture,
    loading: false
  };

  test('render -- SNAPSHOT', () => {
    expect(shallow(<MyPaymentsList {...props} />)).toMatchSnapshot();
  });

  test('render - empty -- SNAPSHOT', () => {
    expect(
      shallow(<MyPaymentsList {...props} payments={[]} />)
    ).toMatchSnapshot();
  });

  test('render - loading -- SNAPSHOT', () => {
    expect(
      shallow(<MyPaymentsList {...props} loading={true} />)
    ).toMatchSnapshot();
  });
});
