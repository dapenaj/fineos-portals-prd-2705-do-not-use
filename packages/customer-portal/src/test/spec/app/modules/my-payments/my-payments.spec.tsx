import React from 'react';

import { mount, DefaultContext } from '../../../../config';
import MyPayments, {
  MyPaymentsList
} from '../../../../../app/modules/my-payments';
import { findClaimSpy, findPaymentsSpy } from '../../../../common/spys';
import { releaseEventLoop } from '../../../../common/utils';
import { Spinner, EmptyList } from '../../../../../app/shared/ui';

describe('MyPayments', () => {
  const mountMyPayments = () =>
    mount(
      <DefaultContext>
        <MyPayments />
      </DefaultContext>
    );

  test('render loading', async () => {
    findClaimSpy('success');
    findPaymentsSpy('success');

    const wrapper = mountMyPayments();

    await releaseEventLoop();
    await releaseEventLoop();

    expect(wrapper.find(Spinner)).toExist();
  });

  test('render success', async () => {
    findClaimSpy('success');
    findPaymentsSpy('success');

    const wrapper = mountMyPayments();

    await releaseEventLoop();
    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(MyPaymentsList)).toExist();
  });

  test('render failure', async () => {
    findClaimSpy('success');
    findPaymentsSpy('failure');

    const wrapper = mountMyPayments();

    await releaseEventLoop();

    wrapper.update();

    expect(wrapper.find(EmptyList)).toExist();
  });
});
