import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise-middleware';

import { apiResponseMiddleware } from '../../../../../app/store/middleware';

describe('api response middleware', () => {
  let store: any;

  const generateStore = () =>
    applyMiddleware(apiResponseMiddleware, promise)(createStore)(() => null);

  beforeEach(() => {
    store = generateStore();
  });

  const defaultSuccess = {
    type: 'success'
  };

  const defaultPromise = {
    ...defaultSuccess,
    payload: Promise.resolve('foo')
  };

  it('should not trigger an alert content but should return promise success', async () => {
    await store.dispatch(defaultPromise).then((response: any) => {
      expect(response.value).toEqual('foo');
    });
  });

  describe('success', () => {
    const defaultSuccessPayload = {
      ...defaultSuccess,
      meta: {
        success: { content: 'test' }
      },
      payload: Promise.resolve('foo')
    };

    const defaultSuccessTitlePayload = {
      ...defaultSuccess,
      ...defaultSuccessPayload,
      meta: {
        success: { title: 'test', content: 'test' }
      }
    };

    it('should trigger a success alert content and return promise success', async () => {
      await store.dispatch(defaultSuccessPayload).then((response: any) => {
        expect(response.value).toEqual('foo');
      });
    });

    it('should trigger a success alert content and title and return promise success', async () => {
      await store.dispatch(defaultSuccessTitlePayload).then((response: any) => {
        expect(response.value).toEqual('foo');
      });
    });
  });

  describe('error and warning', () => {
    const defaultError = {
      type: 'error'
    };

    const defaultWarning = {
      type: 'warning'
    };

    const defaultErrorPayload = {
      ...defaultError,
      meta: {
        error: { content: 'test' }
      },
      payload: Promise.reject(new Error('foo'))
    };

    const defaultWarningPayload = {
      ...defaultWarning,
      meta: {
        warning: { content: 'test' }
      },
      payload: Promise.reject(new Error('foo'))
    };

    const genericError = {
      ...defaultError,
      payload: Promise.reject({
        data: 'Error'
      })
    };

    const notFoundError = {
      ...defaultError,
      payload: Promise.reject({
        data: 'Error',
        status: 404
      })
    };

    const error403 = {
      ...defaultError,
      payload: Promise.reject({
        data: 'Error',
        status: 403
      })
    };

    const error401 = {
      ...defaultError,
      payload: Promise.reject({
        data: 'Error',
        status: 401
      })
    };

    const error422 = {
      ...defaultError,
      payload: Promise.reject({
        data: ['Error'],
        status: 422
      })
    };

    const error409 = {
      ...defaultError,
      payload: Promise.reject({
        data: 'Error',
        status: 409
      })
    };

    it('should trigger an error alert content and return promise error', async () => {
      await store.dispatch(defaultErrorPayload).catch((error: any) => {
        expect(error.message).toEqual('foo');
      });
    });

    it('should trigger an warning alert content and return promise error', async () => {
      await store.dispatch(defaultWarningPayload).catch((error: any) => {
        expect(error.message).toEqual('foo');
      });
    });

    it('should trigger an error alert content and return promise error for generic content', async () => {
      await store.dispatch(genericError).catch((error: any) => {
        expect(error.data).toEqual('Error');
      });
    });

    it('should trigger an error alert content and return promise error for 404 error', async () => {
      await store.dispatch(notFoundError).catch((error: any) => {
        expect(error.data).toEqual('Error');
      });
    });

    it('should trigger an error alert content and return promise error for 403 error', async () => {
      await store.dispatch(error403).catch((error: any) => {
        expect(error.data).toEqual('Error');
      });
    });

    it('should trigger an error alert content and return promise error for 401 error', async () => {
      await store.dispatch(error401).catch((error: any) => {
        expect(error.data).toEqual('Error');
      });
    });

    it('should trigger an error alert content and return promise error for 422 error', async () => {
      await store.dispatch(error422).catch((error: any) => {
        expect(error.data[0]).toEqual('Error');
      });
    });

    it('should trigger an error alert content and return promise error for 409 error', async () => {
      await store.dispatch(error409).catch((error: any) => {
        expect(error.data).toEqual('Error');
      });
    });
  });
});
