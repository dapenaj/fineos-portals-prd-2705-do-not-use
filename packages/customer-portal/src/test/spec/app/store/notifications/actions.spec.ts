import configureStore from 'redux-mock-store';
import { AnyAction } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import { NotificationAction } from '../../../../../app/store/notification';
import { fetchNotifications } from '../../../../../app/store/notification/actions';
import { notificationsFixture } from '../../../../common/fixtures';
import { PENDING, FULFILLED } from '../../../../../app/shared/constants';
import { NotificationService } from '../../../../../app/shared/services';

const mockStore = configureStore([thunk, promise]);
const store = mockStore();

describe('customer contact actions', () => {
  const service = NotificationService.getInstance();

  beforeEach(() => {
    store.clearActions();
    jest
      .spyOn(service, 'fetchNotifications')
      .mockReturnValue(Promise.resolve(notificationsFixture));
  });

  describe('FETCH_NOTIFICATIONS', () => {
    test('dispatch fetchNotifications', async () => {
      const expected = [
        { type: PENDING(NotificationAction.FETCH_NOTIFICATIONS) },
        {
          type: FULFILLED(NotificationAction.FETCH_NOTIFICATIONS),
          payload: notificationsFixture
        }
      ];

      await store
        .dispatch(fetchNotifications() as AnyAction)
        .then(() => expect(store.getActions()).toEqual(expected));
    });
  });
});
