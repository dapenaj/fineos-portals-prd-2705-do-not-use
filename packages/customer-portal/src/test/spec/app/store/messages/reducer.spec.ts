import { Message } from 'fineos-js-api-client';
import { PayloadMetaAction } from 'typesafe-actions';

import { messagesFixture } from '../../../../common/fixtures';
import {
  updateMessageReducer,
  MessageState,
  MessageAction
} from '../../../../../app/store/messages';
import { FULFILLED, REJECTED } from '../../../../../app/shared/constants';

describe('messages reducer', () => {
  describe('MARK_MESSAGES_AS_READ', () => {
    test('is correct', () => {
      const action: PayloadMetaAction<
        MessageAction,
        Message | undefined,
        { messageId: number; message: Message } | undefined
      > = {
        type: FULFILLED(MessageAction.MARK_MESSAGES_AS_READ) as MessageAction,
        payload: undefined,
        meta: {
          messageId: 1,
          message: new Message()
        }
      };

      const withRead = new Message();
      withRead.caseId = messagesFixture[0].caseId;
      withRead.contactTime = messagesFixture[0].contactTime;
      withRead.messageId = messagesFixture[0].messageId;
      withRead.msgOriginatesFromPortal =
        messagesFixture[0].msgOriginatesFromPortal;
      withRead.narrative = messagesFixture[0].narrative;
      withRead.read = true;
      withRead.subject = messagesFixture[0].subject;

      const initialState: MessageState = {
        fetch: { data: [messagesFixture[0]], loading: false }
      };

      const initialStateResult: MessageState = {
        fetch: { data: [withRead], loading: false }
      };

      expect(updateMessageReducer(initialState.fetch, action)).toEqual(
        initialStateResult.fetch
      );
    });

    test('test rejected', () => {
      const actionReject: PayloadMetaAction<
        MessageAction,
        Message | undefined,
        { messageId: number; message: Message } | undefined
      > = {
        type: REJECTED(MessageAction.MARK_MESSAGES_AS_READ) as MessageAction,
        payload: new Message(),
        meta: {
          messageId: 1,
          message: new Message()
        }
      };
      const expectedState: any = {
        data: [messagesFixture[0]],
        loading: false,
        error: 'error'
      };
      const initialStateError: MessageState = {
        fetch: { data: [messagesFixture[0]], loading: false, error: 'error' }
      };

      expect(
        updateMessageReducer(initialStateError.fetch, actionReject)
      ).toEqual(expectedState);
    });
  });
});
