import { MessageService } from 'fineos-js-api-client';
import configureStore from 'redux-mock-store';
import { AnyAction } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import {
  fetchMessages,
  markMessagesAsRead
} from '../../../../../app/store/messages/actions';
import { messagesFixture } from '../../../../common/fixtures';
import { MessageAction } from '../../../../../app/store/messages';
import { PENDING, FULFILLED } from '../../../../../app/shared/constants';
import { releaseEventLoop } from '../../../../common/utils';

describe('messages actions', () => {
  const service = MessageService.getInstance();
  const mockStore = configureStore([thunk, promise]);
  const createStoreWithMessageLoadingState = (loading: boolean) =>
    mockStore({
      message: {
        fetch: {
          loading
        }
      }
    });
  // redux-mock-store doesn't support redux-thunk actions type
  const fetchMessagesWithNoType = fetchMessages as any;
  let messagesServiceMethodSpy: jest.SpyInstance;

  describe('FETCH_MESSAGES', () => {
    beforeEach(() => {
      messagesServiceMethodSpy = jest
        .spyOn(service, 'getMessages')
        .mockReturnValue(Promise.resolve(messagesFixture));
    });

    afterEach(() => {
      messagesServiceMethodSpy.mockRestore();
    });

    test('should dispatch fetchNotifications', async () => {
      const store = createStoreWithMessageLoadingState(false);
      const expected = [
        { type: PENDING(MessageAction.FETCH_MESSAGES) },
        {
          type: FULFILLED(MessageAction.FETCH_MESSAGES),
          payload: messagesFixture
        }
      ];

      // redux-mock-store doesn't support redux-thunk actions
      store.dispatch(fetchMessagesWithNoType());
      await releaseEventLoop();

      expect(store.getActions()).toEqual(expected);
    });

    test('should not dispatch fetchNotifications while loading', async () => {
      const store = createStoreWithMessageLoadingState(true);

      store.dispatch(fetchMessagesWithNoType());
      await releaseEventLoop();

      expect(store.getActions()).toEqual([]);
    });
  });

  describe('MARK_MESSAGES_AS_READ', () => {
    beforeEach(() => {
      messagesServiceMethodSpy = jest
        .spyOn(service, 'markMessagesAsRead')
        .mockReturnValue(Promise.resolve());
    });

    afterEach(() => {
      messagesServiceMethodSpy.mockRestore();
    });

    test('dispatch markMessagesAsRead', async () => {
      const store = createStoreWithMessageLoadingState(false);
      const expected = [
        {
          type: PENDING(MessageAction.MARK_MESSAGES_AS_READ),
          meta: {
            messageId: 1
          }
        },
        {
          type: FULFILLED(MessageAction.MARK_MESSAGES_AS_READ),
          meta: {
            messageId: 1
          }
        }
      ];

      await store
        .dispatch(markMessagesAsRead(1) as AnyAction)
        .then(() => expect(store.getActions()).toEqual(expected));
    });
  });
});
