import configureStore from 'redux-mock-store';
import { AnyAction } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import {
  selectIntakeWorkflowSection,
  submitIntakeWorkflowStep,
  cancelIntakeWorkflow,
  selectIntakeWorkflowStep
} from '../../../../../../app/store/intake/workflow/actions';
import {
  yourRequestSteps,
  PersonalDetailsValue,
  DetailsValue,
  intakeSections
} from '../../../../../../app/modules/intake';
import {
  IntakeSectionStep,
  IntakeSection
} from '../../../../../../app/shared/types';
import { IntakeWorkflowAction } from '../../../../../../app/store/intake/workflow';
import { finishIntakeWorkflow } from '../../../../../../app/store/intake/workflow/actions';

const mockStore = configureStore([thunk, promise]);
const store = mockStore();

describe('intake workflow actions', () => {
  window.scrollTo = () => null;

  const section = intakeSections[1];
  const step = yourRequestSteps[0] as IntakeSectionStep<PersonalDetailsValue>;

  beforeEach(() => {
    store.clearActions();
  });

  describe('SELECT_INTAKE_WORKFLOW_SECTION', () => {
    test('dispatch selectIntakeWorkflowSection', () => {
      const expected = [
        {
          type: IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_SECTION,
          payload: section
        }
      ];

      store.dispatch(
        selectIntakeWorkflowSection(
          section as IntakeSection<DetailsValue>
        ) as AnyAction
      );

      expect(store.getActions()).toEqual(expected);
    });
  });

  describe('FINISH_INTAKE_WORKFLOW', () => {
    test('dispatch finishIntakeWorkflow', () => {
      const expected = [
        {
          type: IntakeWorkflowAction.FINISH_INTAKE_WORKFLOW,
          payload: step
        }
      ];

      store.dispatch(finishIntakeWorkflow(step) as AnyAction);

      expect(store.getActions()).toEqual(expected);
    });
  });

  describe('RESET_INTAKE_WORKFLOW', () => {
    test('dispatch cancelIntakeWorkflow', () => {
      const expected = [
        {
          type: IntakeWorkflowAction.RESET_INTAKE_WORKFLOW
        }
      ];

      store.dispatch(cancelIntakeWorkflow() as AnyAction);

      expect(store.getActions()).toEqual(expected);
    });
  });

  describe('SUBMIT_INTAKE_WORKFLOW_STEP', () => {
    test('dispatch submitIntakeWorkflowStep', () => {
      const expected = [
        {
          type: IntakeWorkflowAction.SUBMIT_INTAKE_WORKFLOW_STEP,
          payload: step
        }
      ];

      store.dispatch(submitIntakeWorkflowStep(step) as AnyAction);

      expect(store.getActions()).toEqual(expected);
    });
  });

  describe('SELECT_INTAKE_WORKFLOW_STEP', () => {
    test('dispatch selectIntakeWorkflowStep', () => {
      const expected = [
        {
          type: IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_STEP,
          payload: step
        }
      ];

      store.dispatch(selectIntakeWorkflowStep(step) as AnyAction);

      expect(store.getActions()).toEqual(expected);
    });
  });
});
