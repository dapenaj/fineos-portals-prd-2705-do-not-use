import { AnyAction } from 'redux';

import {
  intakeWorkflowReducer,
  IntakeWorkflowAction,
  IntakeWorkflowState
} from '../../../../../../app/store/intake/workflow';
import {
  intakeSections,
  yourRequestSteps,
  detailsSteps
} from '../../../../../../app/modules/intake';
import { IntakeSectionStepId } from '../../../../../../app/shared/types';

const initialState: IntakeWorkflowState = {
  allSectionsComplete: false,
  sections: intakeSections,
  previousSubmissionState: null,
  currentSection: intakeSections[0],
  hasValues: false
};

describe('workflow reducer', () => {
  describe('initial state', () => {
    const action: AnyAction = { type: 'test_action' };

    test('is correct', () => {
      expect(intakeWorkflowReducer(initialState, action)).toEqual(initialState);
    });
  });

  describe('SELECT_INTAKE_WORKFLOW_SECTION', () => {
    const action: AnyAction = {
      type: IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_SECTION,
      payload: intakeSections[1]
    };
    const expectedState: IntakeWorkflowState = {
      ...initialState,
      currentSection: intakeSections[1]
    };

    test('is the correct state', () => {
      expect(intakeWorkflowReducer(initialState, action)).toEqual(
        expectedState
      );
    });
  });

  describe('SUBMIT_INTAKE_WORKFLOW_STEP', () => {
    const action: AnyAction = {
      type: IntakeWorkflowAction.SUBMIT_INTAKE_WORKFLOW_STEP,
      payload: {
        ...yourRequestSteps[0],
        value: 'test',
        skipSteps: [IntakeSectionStepId.REQUEST_DETAILS]
      }
    };

    test('is the correct state', () => {
      const expectedState: IntakeWorkflowState = {
        ...initialState,
        currentSection: {
          ...intakeSections[1],
          steps: [
            {
              ...yourRequestSteps[0],
              value: 'test',
              isActive: false,
              isComplete: true
            }
          ]
        }
      };

      const reducedState = intakeWorkflowReducer(initialState, action);
      expect(reducedState.currentSection.steps[0]).toEqual(
        expectedState.currentSection.steps[0]
      );

      expect(reducedState.sections[1].steps[0]).toEqual(
        jasmine.objectContaining({
          skip: true,
          isComplete: true,
          isActive: false
        })
      );
      expect(reducedState.sections[1].steps[1]).toEqual(
        jasmine.objectContaining({
          isComplete: false,
          isActive: true
        })
      );
    });

    test('is the correct state when section completes', () => {
      // set up an initial state where all the steps for the current section are complete
      const state = {
        ...initialState,
        currentSection: {
          ...initialState.currentSection,
          steps: initialState.currentSection.steps.map(step => ({
            ...step,
            isComplete: true
          }))
        }
      };

      const expectedState: IntakeWorkflowState = {
        ...initialState,
        currentSection: {
          ...intakeSections[1],
          steps: [
            {
              ...detailsSteps[0],
              skip: true,
              isActive: false,
              isComplete: true
            }
          ]
        }
      };

      const reducedState = intakeWorkflowReducer(state, action);
      expect(reducedState.currentSection.steps[0]).toEqual(
        expectedState.currentSection.steps[0]
      );

      expect(reducedState.sections[1].steps[0]).toEqual(
        jasmine.objectContaining({
          skip: true,
          isComplete: true,
          isActive: false
        })
      );
      expect(reducedState.sections[1].steps[1]).toEqual(
        jasmine.objectContaining({
          isComplete: false,
          isActive: true
        })
      );
    });

    test('scrolls to top of page on new section', async () => {
      const spy = jest.spyOn(window, 'scrollTo');

      const state = {
        ...initialState,
        currentSection: {
          ...initialState.currentSection,
          steps: initialState.currentSection.steps.map(step => ({
            ...step,
            isComplete: true
          }))
        }
      };

      intakeWorkflowReducer(state, action);
      expect(spy).toHaveBeenCalled();
    });
  });

  describe('SELECT_INTAKE_WORKFLOW_STEP', () => {
    const action: AnyAction = {
      type: IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_STEP,
      payload: intakeSections[0].steps[1]
    };

    test('is the correct state', () => {
      expect(
        intakeWorkflowReducer(initialState, action).currentSection.steps[1]
          .isActive
      ).toEqual(true);
    });
  });

  describe('FINISH_INTAKE_WORKFLOW', () => {
    const action: AnyAction = {
      type: IntakeWorkflowAction.FINISH_INTAKE_WORKFLOW,
      payload: intakeSections[0].steps[1]
    };

    test('is the correct state', () => {
      const reducedState = intakeWorkflowReducer(initialState, action);
      reducedState.sections.forEach(section =>
        expect(section.isComplete).toEqual(true)
      );
      expect(reducedState.allSectionsComplete).toEqual(true);
    });
  });
  describe('RESET_INTAKE_WORKFLOW', () => {
    const action: AnyAction = {
      type: IntakeWorkflowAction.RESET_INTAKE_WORKFLOW
    };

    test('is the correct state', () => {
      expect(intakeWorkflowReducer(initialState, action)).toEqual(initialState);
    });
  });
});
