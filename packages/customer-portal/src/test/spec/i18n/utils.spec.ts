import * as Utils from '../../../app/i18n/utils';

describe('i18n Utils', () => {
  test('flattenMessages', () => {
    const val = {
      foo: 'bar',
      derp: {
        nerp: 'herp'
      }
    };
    expect(Utils.flattenMessages(val)).toMatchObject({
      foo: 'bar',
      'derp.nerp': 'herp'
    });
  });

  test('getMessagesForLocale', () => {
    expect(Utils.getMessagesForLocale('en')).toMatchObject({
      'HOME.HERO_ACTION': `Let's get started`
    });
  });
});
