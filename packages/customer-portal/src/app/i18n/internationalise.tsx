import React from 'react';
import { IntlProvider } from 'react-intl';

import { getMessagesForLocale } from './utils';
import { supportedLanguages } from './locales';

type InternationaliseProps = {
  language: string;
  children: React.ReactChild;
};

export const Internationalise: React.FunctionComponent<InternationaliseProps> = ({
  language,
  children
}) => {
  const locale = supportedLanguages.some(l => l === language) ? language : 'en';

  return (
    <IntlProvider
      locale={locale}
      messages={getMessagesForLocale(locale)}
      defaultLocale={locale}
    >
      {children}
    </IntlProvider>
  );
};
