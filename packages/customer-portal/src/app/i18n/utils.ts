import { availableTranslations } from './locales';

const _availableTranslations: any = availableTranslations;

export const flattenMessages = (object: any, prefix: string = '') =>
  Object.keys(object).reduce((transaltions: any, key) => {
    const value = object[key];
    const prefixedKey: any = prefix ? `${prefix}.${key}` : key;
    if (typeof value === 'string') {
      transaltions[prefixedKey] = value;
    } else {
      Object.assign(transaltions, flattenMessages(value, prefixedKey));
    }
    return transaltions;
  }, {});

export const getMessagesForLocale = (locale: string) =>
  flattenMessages(_availableTranslations[locale].default);

export const getMessageForLocale = (locale: string, text: string) =>
  getMessagesForLocale(locale)[text];
