import { ApiConfigProps } from 'fineos-js-api-client';

import * as en from './locale-data/en.json';

export const supportedLanguages = ApiConfigProps.supportedLanguages;

export const availableTranslations = { en };
