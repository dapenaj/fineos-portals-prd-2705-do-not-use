import React from 'react';
import { Collapse } from 'antd';

import {
  PeriodDecisionGroup,
  AbsenceHandlerInfo
} from '../../../../../shared/types';

import { PeriodDecisionHeader, PeriodDecisionLine } from './ui';
import './job-protected-leave-plans.scss';

const { Panel } = Collapse;

type JobProtectedLeavePlansProps = {
  groups: PeriodDecisionGroup[];
  handlerInfo?: AbsenceHandlerInfo;
};

type JobProtectedLeavePlansState = {
  activeKey: string | string[];
};

export class JobProtectedLeavePlans extends React.Component<
  JobProtectedLeavePlansProps,
  JobProtectedLeavePlansState
> {
  state = { activeKey: [] as string[] };
  render() {
    const { groups, handlerInfo } = this.props;
    const { activeKey } = this.state;

    return (
      <div data-test-el="job-protected-leave-plans">
        <Collapse
          onChange={key =>
            this.setState({
              activeKey: key
            })
          }
        >
          {groups.map(({ name, absencePeriodDecisions }) => {
            const expanded = activeKey.includes(name);
            return (
              <Panel
                header={
                  <PeriodDecisionHeader
                    expanded={expanded}
                    name={name}
                    periodDecisions={absencePeriodDecisions}
                    handlerInfo={handlerInfo}
                  />
                }
                key={name}
              >
                {expanded &&
                  absencePeriodDecisions.map((absencePeriodDecision, index) => (
                    <PeriodDecisionLine
                      key={index}
                      decision={absencePeriodDecision}
                    />
                  ))}
              </Panel>
            );
          })}
        </Collapse>
      </div>
    );
  }
}
