import React from 'react';
import { Row, Col } from 'antd';

import { ContentLayout } from '../../../../../../../shared/layouts';
import {
  EnhancedAbsencePeriodDecision,
  AbsenceHandlerInfo,
  IntermittentPeriodDecision
} from '../../../../../../../shared/types';
import { HandlerInfo } from '../../../../ui';
import { countPeriodDecisionsByStatusCategory } from '../../../job-protected-leave.util';
import { PeriodDecisionStatusIcon } from '../period-decision-status.component';

import { PeriodDecisionHeaderIcon } from './period-decision-header-icon.component';
import styles from './period-decision-header.module.scss';

type PeriodDecisionHeaderProps = {
  expanded: boolean;
  name: string;
  periodDecisions: (
    | EnhancedAbsencePeriodDecision
    | IntermittentPeriodDecision
  )[];
  handlerInfo?: AbsenceHandlerInfo;
};

export const PeriodDecisionHeader: React.FC<PeriodDecisionHeaderProps> = ({
  expanded,
  name,
  periodDecisions,
  handlerInfo
}) => (
  <div data-test-el="period-decision-header">
    {expanded ? (
      <ContentLayout direction="column" noPadding={true}>
        <Row type="flex" align="middle">
          <Col xs={24} sm={12}>
            <div className={styles.header}>
              <span data-test-el="period-decision-header-name">{name}</span>
              <PeriodDecisionHeaderIcon decisions={periodDecisions} />
            </div>
          </Col>
          <Col xs={24} sm={12}>
            <HandlerInfo
              data-test-el="period-decision-header-handler-info"
              name={handlerInfo && handlerInfo.absenceHandler}
              phoneNumber={handlerInfo && handlerInfo.absenceHandlerPhoneNumber}
            />
          </Col>
        </Row>
      </ContentLayout>
    ) : (
      <>
        <div className={styles.header}>
          <span data-test-el="period-decision-header-name">{name}</span>
          <PeriodDecisionHeaderIcon decisions={periodDecisions} />
        </div>
        <div className={styles.wrapper}>
          {countPeriodDecisionsByStatusCategory(periodDecisions).map(
            ({ status, count }, index) => (
              <PeriodDecisionStatusIcon
                key={index}
                status={status}
                count={count}
              />
            )
          )}
        </div>
      </>
    )}
  </div>
);
