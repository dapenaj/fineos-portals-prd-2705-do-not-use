import { AbsencePeriodDecision } from 'fineos-js-api-client';
import { chain as _chain, orderBy as _orderBy } from 'lodash';

import {
  PeriodDecisionStatus,
  JobProtectedLeavePlanGroup,
  AbsencePeriodDecisionStatusCount,
  EnhancedAbsencePeriodDecision,
  AbsenceWithHandler,
  IntermittentPeriodDecision
} from '../../../../shared/types';
import { parseDate } from '../../../../shared/utils';

// figure out the status category for a period decision
export const getStatusCategory = (
  decision: AbsencePeriodDecision,
  prevDecision: AbsencePeriodDecision | null,
  nextDecision: AbsencePeriodDecision | null
) => {
  let decisionStatus = getPeriodDecisionStatus(decision);
  if (!decisionStatus) {
    decisionStatus = getPeriodDecisionStatusBasedOnPreviousAndNext(
      decision,
      prevDecision,
      nextDecision
    );
  }
  return decisionStatus;
};

// get status based on AbsencePeriodDecision.decisionStatus
export const getPeriodDecisionStatus = (
  periodDecision: AbsencePeriodDecision
): PeriodDecisionStatus | null => {
  switch (periodDecision.decisionStatus) {
    case 'Pending':
    case 'Projected':
    case 'In Review':
      return 'pending';
    case 'Cancelled':
      return 'cancelled';
    case 'Denied':
      return 'declined';
    case 'Approved':
      return getAdjudicationStatus(periodDecision);
    default:
      return 'none';
  }
};

// get status based on AbsencePeriodDecision.adjudicationStatus
const getAdjudicationStatus = (
  periodDecision: AbsencePeriodDecision
): PeriodDecisionStatus | null => {
  switch (periodDecision.adjudicationStatus) {
    case 'Rejected':
      return 'declined';
    case 'Accepted':
      return getTimeDecisionStatus(periodDecision);
    default:
      return 'none';
  }
};

// get status based on AbsencePeriodDecision.timeDecisionStatus
export const getTimeDecisionStatus = (
  periodDecision: AbsencePeriodDecision
): PeriodDecisionStatus | null => {
  switch (periodDecision.timeDecisionStatus) {
    case 'Pending':
    case 'Pending Certification':
      return 'pending';
    case 'Approved':
    case 'Certified':
    case 'Time Available':
      return 'approved';
    case 'Denied':
      return 'declined';
    default:
      return getAbsencePeriodStatus(periodDecision);
  }
};

// get status based on AbsencePeriodDecision.absencePeriodStatus
const getAbsencePeriodStatus = (
  periodDecision: AbsencePeriodDecision
): PeriodDecisionStatus | null => {
  switch (periodDecision.absencePeriodStatus) {
    case 'Cancelled':
      return 'cancelled';
    case 'Episodic':
      return 'approved';
    default:
      return null;
  }
};

// Decisions are deemed as matching if they share the same leave plan name, absence reason, and absence period type
const doPeriodDecisionsMatch = (
  decisionA: AbsencePeriodDecision,
  decisionB: AbsencePeriodDecision
): boolean =>
  decisionA.leavePlanName === decisionB.leavePlanName &&
  decisionA.reasonName === decisionB.reasonName &&
  decisionA.absencePeriodType === decisionB.absencePeriodType;

// Decisions are deemed as consecutive if they are within one day of each other.
const arePeriodDecisionsConsecutive = (
  decisionA: AbsencePeriodDecision,
  decisionB: AbsencePeriodDecision
): boolean =>
  parseDate(decisionB.startDate).diff(parseDate(decisionA.endDate), 'd') <= 1;

// If a status category can't be decided on for a decision - then a status is chosen based on the previous and next decisions
const getPeriodDecisionStatusBasedOnPreviousAndNext = (
  currentDecision: AbsencePeriodDecision,
  prevDecision: AbsencePeriodDecision | null,
  nextDecision: AbsencePeriodDecision | null
): PeriodDecisionStatus => {
  if (prevDecision && nextDecision) {
    const prevStatus = getPeriodDecisionStatus(prevDecision);
    const nextStatus = getPeriodDecisionStatus(nextDecision);
    if (
      doPeriodDecisionsMatch(prevDecision, nextDecision) &&
      doPeriodDecisionsMatch(prevDecision, currentDecision) &&
      prevStatus !== null &&
      prevStatus === nextStatus &&
      arePeriodDecisionsConsecutive(prevDecision, currentDecision) &&
      arePeriodDecisionsConsecutive(currentDecision, nextDecision)
    ) {
      return prevStatus;
    }
  } else if (prevDecision) {
    const prevStatus = getPeriodDecisionStatus(prevDecision);
    if (
      doPeriodDecisionsMatch(prevDecision, currentDecision) &&
      prevStatus !== null &&
      arePeriodDecisionsConsecutive(prevDecision, currentDecision)
    ) {
      return prevStatus;
    }
  } else if (nextDecision) {
    const nextStatus = getPeriodDecisionStatus(nextDecision);
    if (
      doPeriodDecisionsMatch(nextDecision, currentDecision) &&
      nextStatus !== null &&
      arePeriodDecisionsConsecutive(currentDecision, nextDecision)
    ) {
      return nextStatus;
    }
  }

  // If we get to this point, then we can't figure out the status and it is set to "skipped"
  // This is technically a possible scenario, but extremely unlikely.
  return 'skipped';
};

export const getPeriodDecisionStatusLabel = (status: PeriodDecisionStatus) => {
  switch (status) {
    case 'pending':
      return 'ABSENCE_PERIOD_STATUS.PENDING';
    case 'declined':
      return 'ABSENCE_PERIOD_STATUS.DECLINED';
    case 'approved':
      return 'ABSENCE_PERIOD_STATUS.APPROVED';
    default:
      return 'ABSENCE_PERIOD_STATUS.CANCELLED';
  }
};

export const filterByParentPeriodId = (value: AbsencePeriodDecision[]) =>
  // skip any where parentPeriodId NOT blank and absencePeriodStatus NOT Cancelled
  value.filter(
    ({ parentPeriodId, absencePeriodStatus }) =>
      parentPeriodId === '' ||
      (parentPeriodId !== '' &&
        absencePeriodStatus.toLocaleLowerCase() !== 'cancelled')
  );

// Groups by leavePlanName - but ignores any where leavePlanName = ''
export const groupByLeavePlanName = (
  value: AbsencePeriodDecision[],
  intermittentPeriods: IntermittentPeriodDecision[]
): JobProtectedLeavePlanGroup[] =>
  _chain([...value, ...intermittentPeriods])
    .filter(absencePeriodDecision => absencePeriodDecision.leavePlanName !== '')
    .groupBy('leavePlanName')
    .map((periods, key) => ({
      name: key,
      absencePeriodDecisions: periods.filter(
        ({ absencePeriodType }) => absencePeriodType !== 'Episodic'
      ),
      intermittentPeriodDecisions: periods.filter(
        ({ absencePeriodType }) => absencePeriodType === 'Episodic'
      ) as IntermittentPeriodDecision[]
    }))
    .value();

export const groupByDecisionStatusAndEmptyPlanName = (
  value: AbsencePeriodDecision[]
): JobProtectedLeavePlanGroup[] =>
  _chain(value)
    .filter(({ leavePlanName }) => leavePlanName === '')
    .groupBy(({ decisionStatus }) =>
      ['Pending', 'Projected', 'In Review'].includes(decisionStatus)
        ? 'Pending'
        : ['Denied', 'Cancelled'].includes(decisionStatus)
        ? 'Denied'
        : 'Other'
    )
    .map((values, key) => ({
      name: key,
      absencePeriodDecisions: values
    }))
    .value();

export const orderAbsencePeriodDecisions = (
  value: AbsencePeriodDecision[],
  prop: string[],
  dir: (boolean | 'asc' | 'desc')[]
) => _orderBy(value, prop, dir);

// Counts the number of decisions for each status category
export const countPeriodDecisionsByStatusCategory = (
  value: (EnhancedAbsencePeriodDecision | IntermittentPeriodDecision)[]
): AbsencePeriodDecisionStatusCount[] =>
  _chain(value)
    .groupBy(decision => decision.statusCategory)
    .map((values, key) => ({
      status: key,
      count: values.length
    }))
    .value();

// process the preiod decisions - give them status categories, order them, merge as needed, etc.
export const processLeavePlanPeriodDecisions = (
  periodDecisions: AbsencePeriodDecision[]
): EnhancedAbsencePeriodDecision[] => {
  let decisions: AbsencePeriodDecision[] = [];

  // get the list of period decisions, ordered by date and name
  decisions = orderAbsencePeriodDecisions(
    periodDecisions,
    ['startDate', 'reasonName'],
    ['asc', 'asc']
  );

  // get the status category for each period decision and save it
  const enhancedDecisions: EnhancedAbsencePeriodDecision[] = [];
  decisions.forEach(
    (
      decision: AbsencePeriodDecision,
      index: number,
      array: AbsencePeriodDecision[]
    ) => {
      let prevDecision = null;
      let nextDecision = null;
      if (index > 0) {
        prevDecision = array[index - 1];
      }
      if (index < array.length - 1) {
        nextDecision = array[index + 1];
      }
      const statusCategory = getStatusCategory(
        decision,
        prevDecision,
        nextDecision
      );
      // ignore decisions with a status of "skipped"
      if (statusCategory !== 'skipped') {
        enhancedDecisions.push({
          ...decision,
          statusCategory
        });
      }
    }
  );

  // consecutive and matching period decisions need to be merged before returning the final array of decisions
  return _chain(enhancedDecisions)
    .groupBy(
      val =>
        `${val.leavePlanName}-${val.reasonName}-${val.absencePeriodType}-${val.statusCategory}`
    )
    .map(values => parseAndMergePeriodDecisions(values))
    .flattenDeep()
    .value();
};

const parseAndMergePeriodDecisions = (
  originalArray: EnhancedAbsencePeriodDecision[]
): EnhancedAbsencePeriodDecision[] => {
  const groupsToMerge: EnhancedAbsencePeriodDecision[][] = [];
  let tempArr: EnhancedAbsencePeriodDecision[] = [];

  // split the originalArray into smaller arrays filled with CONSECUTIVE decisions
  originalArray.forEach((current, index, array) => {
    const nextItem = index + 1 < array.length ? array[index + 1] : null;
    if (nextItem && arePeriodDecisionsConsecutive(current, nextItem)) {
      tempArr.push(current);
    } else if (nextItem) {
      tempArr.push(current);
      groupsToMerge.push([...tempArr]);
      tempArr = [];
    } else {
      const prevItem = index - 1 > -1 ? array[index - 1] : null;
      if (prevItem && arePeriodDecisionsConsecutive(prevItem, current)) {
        tempArr.push(current);
        groupsToMerge.push([...tempArr]);
      } else {
        groupsToMerge.push([...tempArr]);
        groupsToMerge.push([current]);
      }
    }
  });

  // for each group of decisions, merge them into a single decision for rendering
  const finalArray: EnhancedAbsencePeriodDecision[] = [];
  groupsToMerge.forEach(group => {
    if (group.length > 1) {
      finalArray.push(mergeConsecutiveDecisionsFromArray(group));
    } else {
      finalArray.push(...group);
    }
  });

  // return the final set of decisions
  return finalArray;
};

// merges an array of period decisions into a single decison
const mergeConsecutiveDecisionsFromArray = (
  decisionsToMerge: EnhancedAbsencePeriodDecision[]
): EnhancedAbsencePeriodDecision =>
  _orderBy(decisionsToMerge, 'startDate', 'asc').reduce((older, newer) => {
    return {
      ...older,
      ...newer,
      periodId: older.periodId,
      startDate: older.startDate,
      endDate: newer.endDate
    };
  });

// Returns an object containing an array of the intermittent period decisions and an array of the remaining period decisions
export const extractIntermittentPeriodsFromPeriodDecisions = (
  periodDecisions: AbsencePeriodDecision[],
  absenceWithHandler: AbsenceWithHandler
) => {
  // get a list of period decisions thar are not of type Episodic
  const nonEpisodicPeriodDecisions = periodDecisions.filter(
    periodDecision => periodDecision.absencePeriodType !== 'Episodic'
  );

  // get the list of episodic period decisions
  const intermittentPeriodDecisions: IntermittentPeriodDecision[] = periodDecisions
    .filter(periodDecision => periodDecision.absencePeriodType === 'Episodic')
    .map(periodDecision => {
      const {
        absence: { absencePeriods },
        handlerInfo
      } = absenceWithHandler;

      // find the absencePeriod in the absence that matches the intermittent period decision
      const matchingPeriod = absencePeriods.find(
        absencePeriod =>
          !!absencePeriod.episodicLeavePeriodDetail &&
          parseDate(periodDecision.startDate).isSameOrAfter(
            parseDate(absencePeriod.startDate)
          ) &&
          parseDate(periodDecision.endDate).isSameOrBefore(
            parseDate(absencePeriod.endDate)
          )
      );

      // find the child period decisions that belong to this intermittent period decision
      const childPeriodDecisions = nonEpisodicPeriodDecisions
        .filter(
          ({
            actualForRequestedEpisodic,
            parentPeriodId,
            absencePeriodType,
            leavePlanName,
            reasonName,
            startDate,
            endDate
          }) =>
            !!actualForRequestedEpisodic &&
            !!parentPeriodId &&
            absencePeriodType.toLocaleLowerCase() === 'time off period' &&
            leavePlanName === periodDecision.leavePlanName &&
            reasonName === periodDecision.reasonName &&
            parseDate(startDate).isSameOrAfter(
              parseDate(periodDecision.startDate)
            ) &&
            parseDate(endDate).isSameOrBefore(parseDate(periodDecision.endDate))
        )
        .map(decision => {
          const statusCategory = getPeriodDecisionStatus(decision);
          return statusCategory
            ? ({
                ...decision,
                statusCategory
              } as EnhancedAbsencePeriodDecision)
            : undefined;
        })
        .filter(decision => !!decision)
        .sort((a, b) => parseDate(b!.startDate).diff(parseDate(a!.startDate)));

      // from all of that info, combine into our IntermittentPeriodDecision
      return {
        ...periodDecision,
        statusCategory: getPeriodDecisionStatus(periodDecision),
        handlerInfo,
        childPeriodDecisions,
        episodicLeavePeriodDetail:
          matchingPeriod && matchingPeriod.episodicLeavePeriodDetail
      } as IntermittentPeriodDecision;
    });

  // filter the intermittent child period decisions from the remaining period decisions
  const jobProtectedPeriodDecisions = nonEpisodicPeriodDecisions.filter(
    ({ actualForRequestedEpisodic }) => !actualForRequestedEpisodic
  );

  // return the two arrays
  return {
    intermittentPeriodDecisions,
    jobProtectedPeriodDecisions
  };
};
