import React from 'react';
import { Icon, Popover } from 'antd';
import { FormattedMessage } from 'react-intl';

import {
  EnhancedAbsencePeriodDecision,
  IntermittentPeriodDecision
} from '../../../../../../../shared/types';

type PeriodDecisionHeaderIconProps = {
  decisions: (EnhancedAbsencePeriodDecision | IntermittentPeriodDecision)[];
};

export const PeriodDecisionHeaderIcon: React.FC<PeriodDecisionHeaderIconProps> = ({
  decisions
}) => (
  <>
    {decisions.some(
      ({ leavePlanCategory }) => leavePlanCategory === 'Paid'
    ) && (
      <Popover
        placement="right"
        overlayStyle={{
          width: '300px'
        }}
        content={<FormattedMessage id="LEAVE_PLAN_BENEFIT" />}
      >
        <Icon type="dollar" />
      </Popover>
    )}
  </>
);
