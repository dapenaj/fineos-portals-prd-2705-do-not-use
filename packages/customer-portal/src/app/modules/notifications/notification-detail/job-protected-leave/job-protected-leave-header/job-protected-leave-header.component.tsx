import React from 'react';
import { FormattedMessage } from 'react-intl';

import { SummaryHeader } from '../../ui';

import styles from './job-protected-leave-header.module.scss';
import { JobProtectedLeaveStatusIcon } from './job-protected-leave-status-icon.component';

export const JobProtectedLeaveHeader: React.FC = () => (
  <div data-test-el="job-protected-leave-header">
    <SummaryHeader
      title={<FormattedMessage id="NOTIFICATION.JOB_PROTECTED_LEAVE" />}
      icon="safety"
    >
      <div className={styles.wrapper}>
        <span
          className={styles.label}
          data-test-el="job-protected-leave-legend-item"
        >
          <JobProtectedLeaveStatusIcon status="Time off period" />
          <FormattedMessage id="NOTIFICATION.CONTINUOUS_TIME" />
        </span>
        <span
          className={styles.label}
          data-test-el="job-protected-leave-legend-item"
        >
          <JobProtectedLeaveStatusIcon status="Episodic" />
          <FormattedMessage id="NOTIFICATION.INTERMITTENT_TIME" />
        </span>
        <span
          className={styles.label}
          data-test-el="job-protected-leave-legend-item"
        >
          <JobProtectedLeaveStatusIcon status="Reduced Schedule" />
          <FormattedMessage id="NOTIFICATION.REDUCED_SCHEDULE" />
        </span>
      </div>
    </SummaryHeader>
  </div>
);
