import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Col } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';

import {
  formatDateForDisplay,
  getReasonQualifier
} from '../../../../../../shared/utils';
import { PropertyItem } from '../../../../../../shared/ui';
import {
  IntermittentPeriodDecision,
  AbsenceReason
} from '../../../../../../shared/types';

import styles from './period-decision-line.module.scss';

type PeriodDecisionLineProps = { decision: IntermittentPeriodDecision };

export const PeriodDecisionIntermittentProperties: React.FC<PeriodDecisionLineProps> = ({
  decision: {
    reasonName,
    statusCategory,
    startDate,
    qualifier1,
    endDate,
    episodicLeavePeriodDetail
  }
}) => (
  <>
    <Col xs={24} sm={12} md={6}>
      <PropertyItem
        label={<FormattedMessage id="NOTIFICATION.ABSENCE_REASON" />}
      >
        {reasonName}
        {reasonName === AbsenceReason.PREGNANCY &&
          getReasonQualifier(qualifier1)}
      </PropertyItem>
    </Col>
    <Col xs={24} sm={12} md={6}>
      <PropertyItem
        label={
          <FormattedMessage
            id={
              statusCategory === 'approved'
                ? 'NOTIFICATION.APPROVED_BETWEEN'
                : 'NOTIFICATION.REQUESTED_BETWEEN'
            }
          />
        }
      >
        <FormattedMessage
          id="NOTIFICATION.BETWEEN_DATES"
          values={{
            start: formatDateForDisplay(startDate),
            end: (
              <span className={styles.betweenDate}>
                {formatDateForDisplay(endDate)}
              </span>
            )
          }}
        />
      </PropertyItem>
    </Col>
    <Col xs={24} sm={12} md={6}>
      <PropertyItem
        label={<FormattedMessage id={'NOTIFICATION.FREQUENCY_DURATION'} />}
      >
        {!_isEmpty(episodicLeavePeriodDetail) &&
          !!episodicLeavePeriodDetail!.duration &&
          !!episodicLeavePeriodDetail!.frequency &&
          !!episodicLeavePeriodDetail!.frequencyInterval && (
            <FormattedMessage
              id={'NOTIFICATION.FREQUENCY_DURATION_VALUE'}
              values={episodicLeavePeriodDetail}
            />
          )}
      </PropertyItem>
    </Col>
  </>
);
