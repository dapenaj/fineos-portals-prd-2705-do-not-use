import React from 'react';

import {
  JobProtectedLeavePlanGroup,
  AppConfig
} from '../../../../../shared/types';
import { withAppConfig } from '../../../../../shared/contexts';

import { PendingAssessment } from './pending-assessment';
import { ConcludedAssessment } from './concluded-assessment';

type JobProtectedLeaveAssessmentProps = {
  groups: JobProtectedLeavePlanGroup[];
  config: AppConfig;
};

export const JobProtectedLeaveAssessmentContainer: React.FC<JobProtectedLeaveAssessmentProps> = ({
  groups,
  config: {
    clientConfig: {
      notification: {
        detail: {
          emptyState: { jobProtectedLeave }
        }
      }
    }
  }
}) => (
  <>
    {groups.map(({ name, absencePeriodDecisions }, index) => (
      <div key={index}>
        {['Pending'].includes(name) && (
          <PendingAssessment
            title={jobProtectedLeave.pending}
            decisions={absencePeriodDecisions}
          />
        )}
        {['Denied'].includes(name) && (
          <ConcludedAssessment
            title={jobProtectedLeave.concluded.title}
            subtitle={jobProtectedLeave.concluded.subtitle}
            decisions={absencePeriodDecisions}
          />
        )}
      </div>
    ))}
  </>
);

export default withAppConfig(JobProtectedLeaveAssessmentContainer);
