import React from 'react';
import { AbsenceService } from 'fineos-js-api-client';
import { Col, Dropdown, Menu, Button, Icon } from 'antd';
import {
  WrappedComponentProps as IntlProps,
  injectIntl,
  FormattedMessage
} from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';

import {
  showNotification,
  RequestConfirmationModal,
  RequestAmendment,
  ConfirmModal
} from '../../../../../../shared/ui';
import { NotificationProperties } from '../../../ui';
import {
  EnhancedAbsencePeriodDecision,
  IntermittentPeriodDecision
} from '../../../../../../shared/types';

import { PeriodDecisionDefaultProperties } from './period-decision-default-properties.component';
import { PeriodDecisionIntermittentProperties } from './period-decision-intermittent-properties.component';
import { PeriodDecisionStatusIcon } from './period-decision-status.component';
import { IntermittentTimeReports } from './intermittent-time-reports';
import styles from './period-decision-line.module.scss';

type PeriodDecisionLineProps = { decision: EnhancedAbsencePeriodDecision };

export type PeriodDecisionLineState = {
  showEditLeavePeriodForm: boolean;
  showConfirmation: boolean;
  isEdit: boolean;
  showModal: boolean;
};

export class PeriodDecisionLineComponent extends React.Component<
  PeriodDecisionLineProps & IntlProps,
  PeriodDecisionLineState
> {
  state: PeriodDecisionLineState = {
    showEditLeavePeriodForm: false,
    showConfirmation: false,
    isEdit: false,
    showModal: false
  };

  absenceService = AbsenceService.getInstance();

  showEditPeriodForm = () =>
    this.setState({ isEdit: true, showEditLeavePeriodForm: true });

  showConfirmModal = () =>
    this.setState({
      showModal: true
    });

  cancelConfirmModal = () => this.setState({ showModal: false });

  closeForm = () =>
    this.setState({
      showEditLeavePeriodForm: false
    });

  toggleConfirmationModal = () =>
    this.setState({ showConfirmation: !this.state.showConfirmation });

  submitDeleteRequest = async () => {
    const {
      intl: { formatMessage },
      decision: { absenceCaseId, periodId, startDate, endDate, statusCategory }
    } = this.props;

    this.setState({ isEdit: false, showModal: false });

    try {
      if (statusCategory === 'approved' || statusCategory === 'pending') {
        await this.absenceService.deleteTimeOffRequest(absenceCaseId, {
          leavePeriodId: periodId,
          startDate,
          endDate,
          deleteType:
            statusCategory === 'approved' ? 'CancelLeave' : 'WithdrawLeave'
        });
      }
      this.toggleConfirmationModal();
    } catch (error) {
      showNotification({
        title: formatMessage({
          id: 'NOTIFICATION.REQUEST_AMENDMENT.ERRORS.LEAVE_PERIOD_AMENDMENT'
        }),
        type: 'error',
        content: error
      });
    }
  };

  render() {
    const {
      showEditLeavePeriodForm,
      showConfirmation,
      isEdit,
      showModal
    } = this.state;
    const { decision } = this.props;

    const isIntermittent = decision.absencePeriodType === 'Episodic';
    return (
      <div data-test-el="period-decision-line">
        <NotificationProperties
          status={decision.statusCategory}
          type={decision.absencePeriodType}
        >
          {isIntermittent ? (
            <PeriodDecisionIntermittentProperties
              decision={decision as IntermittentPeriodDecision}
            />
          ) : (
            <PeriodDecisionDefaultProperties decision={decision} />
          )}

          <Col xs={24} sm={12} md={6}>
            <PeriodDecisionStatusIcon status={decision.statusCategory} />
            {decision.decisionStatus !== 'Denied' && (
              <Dropdown
                data-test-el="dropdown-options"
                overlay={
                  <Menu>
                    {!isIntermittent && (
                      <Menu.Item key="0">
                        <Button
                          date-test-el="amend-button"
                          onClick={this.showEditPeriodForm}
                          type="link"
                        >
                          <FormattedMessage id="NOTIFICATION.REQUEST_AMENDMENT.REQUEST_AMENDMENT" />
                        </Button>
                      </Menu.Item>
                    )}

                    <Menu.Item key="1">
                      <Button
                        type="link"
                        onClick={this.showConfirmModal}
                        data-test-el="delete-button"
                      >
                        <FormattedMessage id="NOTIFICATION.DELETE_LEAVE_PERIOD" />
                      </Button>
                    </Menu.Item>
                  </Menu>
                }
                trigger={['click']}
              >
                <Button className={styles.actions} type="link">
                  <FormattedMessage id="NOTIFICATION.ACTIONS" />
                  <Icon type="down" className={styles.icon} />
                </Button>
              </Dropdown>
            )}
          </Col>

          <ConfirmModal
            message={'NOTIFICATION.REQUEST_AMENDMENT.DELETE'}
            onSubmitConfirm={this.submitDeleteRequest}
            visible={showModal}
            onCancelConfirm={this.cancelConfirmModal}
          />

          {isIntermittent &&
            !_isEmpty(
              (decision as IntermittentPeriodDecision).childPeriodDecisions
            ) && (
              <Col xs={24}>
                <IntermittentTimeReports
                  decision={decision as IntermittentPeriodDecision}
                />
              </Col>
            )}
        </NotificationProperties>

        {showEditLeavePeriodForm && (
          <RequestAmendment
            amendment={decision}
            closeForm={this.closeForm}
            isSupervisor={false}
            renderConfirmation={this.toggleConfirmationModal}
          />
        )}
        {showConfirmation && (
          <RequestConfirmationModal
            isEdit={isEdit ? 'REQUEST_AMENDMENT' : 'REQUEST_DELETION'}
            isSupervisor={false}
            renderConfirmation={this.toggleConfirmationModal}
          />
        )}
      </div>
    );
  }
}

export const PeriodDecisionLine = injectIntl(PeriodDecisionLineComponent);
