import React from 'react';
import classnames from 'classnames';

import styles from './job-protected-leave-header.module.scss';

type JobProtectedLeaveStatus =
  | 'Time off period'
  | 'Episodic'
  | 'Reduced Schedule';
type JobProtectedLeaveStatusIconProps = { status: JobProtectedLeaveStatus };

const getBorderStyle = (status: JobProtectedLeaveStatus) => {
  switch (status) {
    case 'Episodic':
      return styles.angle;
    case 'Reduced Schedule':
      return styles.half;
    default:
      return '';
  }
};

export const JobProtectedLeaveStatusIcon: React.FC<JobProtectedLeaveStatusIconProps> = ({
  status
}) => <div className={classnames(styles.icon, getBorderStyle(status))} />;
