import React from 'react';
import { FormattedMessage } from 'react-intl';
import { AbsencePeriodDecision } from 'fineos-js-api-client';
import { Icon } from 'antd';

import { formatDateForDisplay } from '../../../../../../shared/utils';

import styles from './concluded-assessment.module.scss';

type ConcludedAssessmentProps = {
  title?: React.ReactNode;
  subtitle?: React.ReactNode;
  decisions: AbsencePeriodDecision[];
};

export const ConcludedAssessment: React.FC<ConcludedAssessmentProps> = ({
  title = (
    <FormattedMessage id="JOB_PROTECTED_LEAVE.CONCLUDED_ASSESSMENT.TITLE" />
  ),
  subtitle = (
    <FormattedMessage id="JOB_PROTECTED_LEAVE.CONCLUDED_ASSESSMENT.SUBTITLE" />
  ),
  decisions
}) => (
  <div className={styles.wrapper}>
    <div className={styles.icon}>
      <Icon type="warning" theme="filled" />
    </div>
    <div className={styles.content}>
      {title}

      <ul className={styles.list}>
        {decisions.map(decision => (
          <li key={decision.reasonName}>
            {decision.reasonName},
            <FormattedMessage id="JOB_PROTECTED_LEAVE.STARTING_ON" />
            {formatDateForDisplay(decision.startDate)}
            {decision.endDate && (
              <>
                ,
                <FormattedMessage id="JOB_PROTECTED_LEAVE.ENDING_ON" />
                {formatDateForDisplay(decision.endDate)}
              </>
            )}
          </li>
        ))}
      </ul>

      {subtitle}
    </div>
  </div>
);
