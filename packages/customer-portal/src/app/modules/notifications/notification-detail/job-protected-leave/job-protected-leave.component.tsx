import React from 'react';
import { AbsencePeriodDecisions } from 'fineos-js-api-client';

import {
  AbsenceWithHandler,
  PeriodDecisionGroup
} from '../../../../shared/types';
import { NotificationEmptyMessage } from '../ui';

import { JobProtectedLeaveAssessment } from './job-protected-leave-assessment';
import { JobProtectedLeaveHeader } from './job-protected-leave-header';
import { JobProtectedLeavePlans } from './job-protected-leave-plans';
import {
  groupByLeavePlanName,
  groupByDecisionStatusAndEmptyPlanName,
  filterByParentPeriodId,
  extractIntermittentPeriodsFromPeriodDecisions,
  processLeavePlanPeriodDecisions
} from './job-protected-leave.util';
import styles from './job-protected-leave.module.scss';

type JobProtectedLeaveProps = {
  periodDecisions: AbsencePeriodDecisions[] | null;
  absencesWithHandlers: AbsenceWithHandler[] | null;
};

export const JobProtectedLeave: React.FC<JobProtectedLeaveProps> = ({
  periodDecisions,
  absencesWithHandlers
}) => (
  <div data-test-el="job-protected-leave">
    {!!periodDecisions &&
      !!absencesWithHandlers &&
      !!periodDecisions.length &&
      periodDecisions.map(({ absencePeriodDecisions }, index) => {
        const filteredPeriodDecisions = filterByParentPeriodId(
          absencePeriodDecisions
        );

        const {
          intermittentPeriodDecisions,
          jobProtectedPeriodDecisions
        } = extractIntermittentPeriodsFromPeriodDecisions(
          filteredPeriodDecisions,
          absencesWithHandlers[index]
        );

        const periodGroupsByLeavePlan = groupByLeavePlanName(
          jobProtectedPeriodDecisions,
          intermittentPeriodDecisions
        );
        const periodGroupsWithoutLeavePlans = groupByDecisionStatusAndEmptyPlanName(
          jobProtectedPeriodDecisions
        );

        const processedPeriodGroups: PeriodDecisionGroup[] = periodGroupsByLeavePlan.map(
          group => ({
            name: group.name,
            absencePeriodDecisions: [
              ...processLeavePlanPeriodDecisions(group.absencePeriodDecisions),
              ...(group.intermittentPeriodDecisions || [])
            ]
          })
        );

        const showJobProtectedLeave =
          !!processedPeriodGroups.length ||
          !!periodGroupsWithoutLeavePlans.length;
        const jobProtectedLeaveBody = showJobProtectedLeave ? (
          <>
            <JobProtectedLeavePlans
              groups={processedPeriodGroups}
              handlerInfo={absencesWithHandlers[index].handlerInfo}
            />

            {!!periodGroupsWithoutLeavePlans.length && (
              <JobProtectedLeaveAssessment
                groups={periodGroupsWithoutLeavePlans}
              />
            )}
          </>
        ) : (
          <NotificationEmptyMessage type="jobProtectedLeave" />
        );

        return (
          <div className={styles.wrapper} key={index}>
            <JobProtectedLeaveHeader />
            {jobProtectedLeaveBody}
          </div>
        );
      })}
  </div>
);
