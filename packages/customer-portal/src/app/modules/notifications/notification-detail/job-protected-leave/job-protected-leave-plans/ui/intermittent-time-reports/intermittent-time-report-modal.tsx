import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, Modal } from 'antd';
import { EpisodicLeavePeriodDetail } from 'fineos-js-api-client';

import { EnhancedAbsencePeriodDecision } from '../../../../../../../shared/types';

import { IntermittentTimeReportTable } from './intermittent-time-report-table.component';
import styles from './intermittent-time-reports.module.scss';

type IntermittentTimeReportTableProps = {
  leavePlanName: string;
  reasonName: string;
  periodDecisions: EnhancedAbsencePeriodDecision[];
  episodicLeavePeriodDetail: EpisodicLeavePeriodDetail;
};

type IntermittentTimeReportTableState = {
  showModal: boolean;
};

export class IntermittentTimeReportModal extends React.Component<
  IntermittentTimeReportTableProps,
  IntermittentTimeReportTableState
> {
  state: IntermittentTimeReportTableState = {
    showModal: false
  };

  handleShowModal = () =>
    this.setState({
      showModal: true
    });

  handleCloseModal = () =>
    this.setState({
      showModal: false
    });

  render() {
    const {
      leavePlanName,
      reasonName,
      periodDecisions,
      episodicLeavePeriodDetail
    } = this.props;
    const { showModal } = this.state;
    return (
      <>
        <Button
          type="link"
          className={styles.buttonSpacing}
          onClick={this.handleShowModal}
          data-test-el="intermittent-time-reports-modal-button"
        >
          <FormattedMessage id="NOTIFICATION.INTERMITTENT_DECISIONS.VIEW_TIME_REPORT_HISTORY" />
        </Button>

        <Modal
          title={
            <FormattedMessage
              id="NOTIFICATION.INTERMITTENT_DECISIONS.MODAL.TITLE"
              values={{ leavePlanName, reasonName }}
            />
          }
          width={'80%'}
          visible={showModal}
          onCancel={this.handleCloseModal}
          footer={[
            <Button type="primary" onClick={this.handleCloseModal} key="close">
              <FormattedMessage id="NOTIFICATION.INTERMITTENT_DECISIONS.MODAL.CLOSE" />
            </Button>
          ]}
        >
          <div className={styles.modalBody}>
            <IntermittentTimeReportTable
              periodDecisions={periodDecisions}
              episodicLeavePeriodDetail={episodicLeavePeriodDetail}
            />
          </div>
        </Modal>
      </>
    );
  }
}
