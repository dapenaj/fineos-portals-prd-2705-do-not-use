import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Col } from 'antd';

import {
  formatDateForDisplay,
  getReasonQualifier
} from '../../../../../../shared/utils';
import { PropertyItem } from '../../../../../../shared/ui';
import { EnhancedAbsencePeriodDecision } from '../../../../../../shared/types';

type PeriodDecisionLineProps = { decision: EnhancedAbsencePeriodDecision };

export const PeriodDecisionDefaultProperties: React.FC<PeriodDecisionLineProps> = ({
  decision: { reasonName, qualifier1, startDate, endDate, statusCategory }
}) => (
  <>
    <Col xs={24} sm={12} md={6}>
      <PropertyItem
        label={<FormattedMessage id="NOTIFICATION.ABSENCE_REASON" />}
      >
        {reasonName}
        {reasonName === 'Pregnancy/Maternity' && getReasonQualifier(qualifier1)}
      </PropertyItem>
    </Col>
    <Col xs={24} sm={12} md={6}>
      <PropertyItem label={<FormattedMessage id="NOTIFICATION.LEAVE_BEGINS" />}>
        {formatDateForDisplay(startDate)}
      </PropertyItem>
    </Col>
    <Col xs={24} sm={12} md={6}>
      <PropertyItem
        label={
          <FormattedMessage
            id={
              statusCategory === 'approved'
                ? 'NOTIFICATION.APPROVED_THROUGH'
                : 'NOTIFICATION.REQUESTED_THROUGH'
            }
          />
        }
      >
        {formatDateForDisplay(endDate)}
      </PropertyItem>
    </Col>
  </>
);
