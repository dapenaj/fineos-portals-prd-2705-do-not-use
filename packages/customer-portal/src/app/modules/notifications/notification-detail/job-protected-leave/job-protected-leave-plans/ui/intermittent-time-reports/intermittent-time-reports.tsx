import React from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import { IntermittentPeriodDecision } from '../../../../../../../shared/types';

import { IntermittentTimeReportModal } from './intermittent-time-report-modal';
import { IntermittentTimeReportTable } from './intermittent-time-report-table.component';
import styles from './intermittent-time-reports.module.scss';

type IntermittentTimeReportsProps = {
  decision: IntermittentPeriodDecision;
};

type IntermittentTimeReportsState = {
  showList: boolean;
};

export class IntermittentTimeReports extends React.Component<
  IntermittentTimeReportsProps,
  IntermittentTimeReportsState
> {
  state: IntermittentTimeReportsState = {
    showList: true
  };

  toggleList = () => this.setState({ showList: !this.state.showList });

  render() {
    const {
      decision: {
        leavePlanName,
        reasonName,
        childPeriodDecisions,
        episodicLeavePeriodDetail
      }
    } = this.props;
    const { showList } = this.state;

    return (
      <>
        <Button
          type="link"
          className={styles.buttonSpacing}
          onClick={this.toggleList}
          data-test-el="intermittent-time-reports-toggle-button"
        >
          <FormattedMessage
            id={`NOTIFICATION.INTERMITTENT_DECISIONS.${
              showList ? 'HIDE_TIME_REPORTS' : 'SHOW_TIME_REPORTS'
            }`}
          />
        </Button>
        {showList && (
          <>
            <IntermittentTimeReportTable
              periodDecisions={childPeriodDecisions!.slice(0, 4)}
              episodicLeavePeriodDetail={episodicLeavePeriodDetail!}
            />
            <IntermittentTimeReportModal
              leavePlanName={leavePlanName}
              reasonName={reasonName}
              periodDecisions={childPeriodDecisions!}
              episodicLeavePeriodDetail={episodicLeavePeriodDetail!}
            />
          </>
        )}
      </>
    );
  }
}
