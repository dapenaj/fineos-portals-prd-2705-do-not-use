import React from 'react';
import { FormattedMessage } from 'react-intl';
import { AbsencePeriodDecision } from 'fineos-js-api-client';

import { formatDateForDisplay } from '../../../../../../shared/utils';

import styles from './pending-assessment.module.scss';

type PendingAssessmentProps = {
  title?: React.ReactNode;
  decisions: AbsencePeriodDecision[];
};

export const PendingAssessment: React.FC<PendingAssessmentProps> = ({
  title = (
    <FormattedMessage id="JOB_PROTECTED_LEAVE.PENDING_ASSESSMENT.TITLE" />
  ),
  decisions
}) => (
  <div className={styles.wrapper}>
    <h2 className={styles.title}>{title}</h2>
    <ul>
      {decisions.map((decision, index) => (
        <li key={`${decision.reasonName}_${index}`}>
          {decision.reasonName},
          <FormattedMessage id="JOB_PROTECTED_LEAVE.STARTING_ON" />
          {formatDateForDisplay(decision.startDate)}
          {decision.endDate && (
            <>
              ,
              <FormattedMessage id="JOB_PROTECTED_LEAVE.ENDING_ON" />
              {formatDateForDisplay(decision.endDate)}
            </>
          )}
        </li>
      ))}
    </ul>
  </div>
);
