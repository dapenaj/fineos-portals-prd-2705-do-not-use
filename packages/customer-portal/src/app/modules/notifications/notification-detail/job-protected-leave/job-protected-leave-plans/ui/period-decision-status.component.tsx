import React from 'react';
import { FormattedMessage } from 'react-intl';

import { getPeriodDecisionStatusLabel } from '../../job-protected-leave.util';
import { PeriodDecisionStatus } from '../../../../../../shared/types';
import { NotificationPanelStatus } from '../../../ui';

type PeriodDecisionStatusProps = {
  status: string | null;
  count?: number;
};

const getColorStyle = (color: string | null) => {
  switch (color) {
    case 'pending':
      return '#ffc20e';
    case 'approved':
      return '#35b558';
    case 'declined':
      return '#f04c3f';
    default:
      return '#b3b3b3';
  }
};

export const PeriodDecisionStatusIcon: React.FC<PeriodDecisionStatusProps> = ({
  status,
  count
}) => (
  <span data-test-el="period-decision-status-icon">
    <NotificationPanelStatus
      color={getColorStyle(status)}
      status={
        <>
          <FormattedMessage
            id={getPeriodDecisionStatusLabel(status as PeriodDecisionStatus)}
          />
          {count && <span className="count">({count})</span>}
        </>
      }
    />
  </span>
);
