import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Col } from 'antd';
import { EpisodicLeavePeriodDetail } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { EnhancedAbsencePeriodDecision } from '../../../../../../../shared/types';
import { Panel, Row } from '../../../../../../../shared/ui';
import { formatDateForDisplay } from '../../../../../../../shared/utils';
import { PeriodDecisionStatusIcon } from '../period-decision-status.component';

import styles from './intermittent-time-reports.module.scss';

type IntermittentTimeReportTableProps = {
  periodDecisions: EnhancedAbsencePeriodDecision[];
  episodicLeavePeriodDetail: EpisodicLeavePeriodDetail;
};

export const IntermittentTimeReportTable: React.FC<IntermittentTimeReportTableProps> = ({
  periodDecisions,
  episodicLeavePeriodDetail
}) => (
  <>
    <div className={styles.tableHead}>
      <Row className={styles.tableHeadRow}>
        <Col xs={6}>
          <FormattedMessage id="NOTIFICATION.INTERMITTENT_DECISIONS.TABLE.START_DATE" />
        </Col>
        <Col xs={6}>
          <FormattedMessage id="NOTIFICATION.INTERMITTENT_DECISIONS.TABLE.END_DATE" />
        </Col>
        <Col xs={6}>
          {!_isEmpty(episodicLeavePeriodDetail) ? (
            <FormattedMessage
              id="NOTIFICATION.INTERMITTENT_DECISIONS.TABLE.TIME_REQUESTED"
              values={episodicLeavePeriodDetail}
            />
          ) : (
            '--'
          )}
        </Col>
        <Col xs={6}>
          <FormattedMessage id="NOTIFICATION.INTERMITTENT_DECISIONS.TABLE.DECISION" />
        </Col>
      </Row>
    </div>
    <Panel className={styles.tableBody}>
      {periodDecisions.map((decision, index) => (
        <Row
          className={styles.timeReportRow}
          key={index}
          data-test-el="intermittent-time-report-table-rows"
        >
          <Col xs={6} className={styles.timeReportCol}>
            {formatDateForDisplay(decision.startDate)}
          </Col>
          <Col xs={6} className={styles.timeReportCol}>
            {formatDateForDisplay(decision.endDate)}
          </Col>
          <Col xs={6} className={styles.timeReportCol}>
            {decision.timeRequested}
          </Col>
          <Col xs={6} className={styles.timeReportCol}>
            <PeriodDecisionStatusIcon status={decision.statusCategory} />
          </Col>
        </Row>
      ))}
    </Panel>
  </>
);
