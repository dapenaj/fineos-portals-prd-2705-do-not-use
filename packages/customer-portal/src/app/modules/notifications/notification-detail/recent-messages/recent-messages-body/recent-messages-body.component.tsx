import React from 'react';
import { Customer } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';

import { MAX_RECENT_MESSAGES } from '../../../../../shared/constants';
import { EmptyState } from '../../../../../shared/ui';
import { RecentMessagesList } from '../recent-messages-list';
import { EnhancedMessage } from '../../../../../shared/types';

import styles from './recent-messages-body.module.scss';

type RecentMessagesBodyProps = {
  notificationCaseId: string;
  messages: EnhancedMessage[] | null;
  customer: Customer | null;
};

export const RecentMessagesBody: React.FC<RecentMessagesBodyProps> = ({
  notificationCaseId,
  messages,
  customer
}) => (
  <div className={styles.wrapper}>
    {!!messages && !!messages.length && (
      <RecentMessagesList
        notificationCaseId={notificationCaseId}
        messages={messages.slice(0, MAX_RECENT_MESSAGES)}
        customer={customer}
      />
    )}
    {(messages === null || !messages.length) && (
      <EmptyState>
        <FormattedMessage id="MESSAGES.EMPTY_VALUE_FOR_NOTIFICATION" />
      </EmptyState>
    )}
  </div>
);
