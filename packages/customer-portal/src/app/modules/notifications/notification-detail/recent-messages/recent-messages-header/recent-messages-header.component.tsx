import React from 'react';
import { Message } from 'fineos-js-api-client';
import { Icon, Badge } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './recent-messages-header.module.scss';

type RecentMessagesHeaderProps = {
  messages: Message[] | null;
};

export const RecentMessagesHeader: React.FC<RecentMessagesHeaderProps> = ({
  messages
}) => {
  const unread = (messages || []).filter(({ read }) => !read).length;

  return (
    <div className={styles.header} data-test-el="recent-messages-header">
      <div>
        <Badge dot={unread > 0} className={styles.icon}>
          <Icon type="mail" />
        </Badge>
        <FormattedMessage id="MESSAGES.RECENT_MESSAGES" />
      </div>
      <span
        className={styles.unreadCount}
        data-test-el="recent-messages-header-unread-count"
      >
        <FormattedMessage id="MESSAGES.UNREAD" values={{ unread }} />
      </span>
    </div>
  );
};
