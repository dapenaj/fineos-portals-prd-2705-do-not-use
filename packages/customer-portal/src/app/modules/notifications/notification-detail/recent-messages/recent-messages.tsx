import React from 'react';
import {
  Message,
  NotificationCaseSummary,
  Customer,
  NewMessage,
  MessageService
} from 'fineos-js-api-client';
import { List } from 'antd';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { showNotification } from '../../../../shared/ui';
import {
  NotificationService,
  EnhancedMessageService
} from '../../../../shared/services';
import { EnhancedMessage } from '../../../../shared/types';

import { RecentMessagesHeader } from './recent-messages-header';
import { RecentMessagesFooter } from './recent-messages-footer';
import { RecentMessagesBody } from './recent-messages-body';
import styles from './recent-messages.module.scss';

type RecentMessagesState = {
  messages: EnhancedMessage[] | null;
  requestIds: string[];
  loading: boolean;
};

type RecentMessagesProps = {
  notification: NotificationCaseSummary;
  customer: Customer | null;
} & IntlProps;

export class RecentMessagesComponent extends React.Component<
  RecentMessagesProps,
  RecentMessagesState
> {
  state: RecentMessagesState = {
    messages: null,
    requestIds: [],
    loading: false
  };

  enhancedMessageService = EnhancedMessageService.getInstance();
  messageService = MessageService.getInstance();
  notificationService = NotificationService.getInstance();

  async componentDidMount() {
    const { notification } = this.props;

    await this.getRequestIds(notification);
  }

  async getRequestIds(notification: NotificationCaseSummary) {
    const {
      intl: { formatMessage }
    } = this.props;

    this.setState({ loading: true });

    try {
      const requestIds = await this.notificationService.getCaseIdsFromNotification(
        notification
      );
      this.setState({ requestIds });
    } catch (requestIds) {
      this.setState({ requestIds });

      const title = formatMessage({ id: 'MESSAGES.FETCH_BENEFITS_ERROR' });

      showNotification({ title, type: 'error' });
    } finally {
      this.setState({ loading: false });
      this.fetchMessages();
    }
  }

  async fetchMessages() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { requestIds } = this.state;
    const {
      notification: { notificationCaseId }
    } = this.props;

    this.setState({ loading: true });

    try {
      const messages = await this.enhancedMessageService.fetchMessagesByCaseId(
        requestIds,
        notificationCaseId
      );

      this.setState({
        messages
      });
    } catch (error) {
      const title = formatMessage({
        id: 'MESSAGES.FETCH_ERROR'
      });
      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false, requestIds: [] });
    }
  }

  handleAddMessage = async (newMessage: NewMessage) => {
    const {
      intl: { formatMessage },
      notification: { notificationCaseId }
    } = this.props;
    const { messages } = this.state;

    this.setState({ loading: true });

    try {
      const message = (await this.messageService.addMessage(
        newMessage
      )) as Message;

      const enhancedMessage = { ...message, notificationCaseId };

      this.setState({
        messages: this.enhancedMessageService.sortMessages([
          ...messages!,
          enhancedMessage
        ])
      });
    } catch (error) {
      const title = formatMessage({ id: 'MESSAGES.ADD_MESSAGE_ERROR' });
      showNotification({ title, type: 'error', content: error });
    } finally {
      this.setState({ loading: false });
    }
  };

  render() {
    const { messages, loading } = this.state;
    const { notification, customer } = this.props;

    return (
      <div className={styles.wrapper} data-test-el="recent-messages">
        <List
          header={
            <RecentMessagesHeader
              messages={messages}
              data-test-el="recent-messages-header"
            />
          }
          loading={loading}
          footer={
            <RecentMessagesFooter
              notification={notification}
              onAddMessage={this.handleAddMessage}
              data-test-el="recent-messages-footer"
            />
          }
        >
          {!loading && (
            <RecentMessagesBody
              notificationCaseId={notification.notificationCaseId}
              messages={messages}
              customer={customer}
            />
          )}
        </List>
      </div>
    );
  }
}

export const RecentMessages = injectIntl(RecentMessagesComponent);
