import React from 'react';
import { Avatar, Card } from 'antd';
import { Customer } from 'fineos-js-api-client';
import { NavLink } from 'react-router-dom';

import { Initials, MessageListElement } from '../../../../../shared/ui';
import logo from '../../../../../../assets/img/logo-icon.svg';
import { EnhancedMessage } from '../../../../../shared/types';

import styles from './recent-messages-list.module.scss';

type RecentMessagesListProps = {
  notificationCaseId: string;
  messages: EnhancedMessage[];
  customer: Customer | null;
};

export const RecentMessagesList: React.FC<RecentMessagesListProps> = ({
  notificationCaseId,
  messages,
  customer
}) => {
  const chooseLogo = (msgOriginatesFromPortal: boolean) => {
    return msgOriginatesFromPortal ? (
      customer ? (
        <Initials customer={customer} />
      ) : (
        ''
      )
    ) : (
      <Avatar src={logo} />
    );
  };

  return (
    <div className={styles.wrapper} data-test-el="recent-messages-list">
      {messages.map((message, index) => (
        <NavLink
          key={index}
          className={styles.link}
          to={{
            pathname: `/my-messages/${notificationCaseId}`,
            search: `?messageId=${message.messageId}`
          }}
        >
          <Card
            bordered={false}
            className={styles.messageWrapper}
            data-test-el="recent-messages-list-element"
          >
            <MessageListElement
              showRelatesTo={false}
              message={message}
              chooseLogo={chooseLogo}
            />
          </Card>
        </NavLink>
      ))}
    </div>
  );
};
