import React from 'react';
import {
  NotificationCaseSummary,
  NewMessage as MessageRequest
} from 'fineos-js-api-client';

import { NewMessage } from '../../../../../shared/ui';

import styles from './recent-messages-footer.module.scss';

type RecentMessagesFooterProps = {
  notification: NotificationCaseSummary;
  onAddMessage: (message: MessageRequest) => void;
};

export const RecentMessagesFooter: React.FC<RecentMessagesFooterProps> = ({
  notification,
  onAddMessage
}) => (
  <div className={styles.footer}>
    <div />

    <div className={styles.button}>
      <NewMessage
        type="recent"
        notification={notification}
        onAddMessage={onAddMessage}
      />
    </div>
  </div>
);
