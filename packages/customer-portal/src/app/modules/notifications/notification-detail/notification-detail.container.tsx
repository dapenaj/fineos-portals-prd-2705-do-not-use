import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  AbsencePeriodDecisions,
  AbsenceService,
  ClaimService
} from 'fineos-js-api-client';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { flatten as _flatten, isEmpty as _isEmpty } from 'lodash';

import { RootState } from '../../../store';
import { fetchNotifications } from '../../../store/notification/actions';
import { showNotification, ContentRenderer } from '../../../shared/ui';
import {
  AbsencePaidLeave,
  AbsenceWithHandler,
  DisabilityBenefitWrapper,
  ClaimBenefit,
  BenefitRightCategory,
  AppConfig
} from '../../../shared/types';
import { withAppConfig } from '../../../shared/contexts';

import { NotificationDetailView } from './notification-detail.view';

const mapStateToProps = (
  { notification: { data, loading } }: RootState,
  {
    match: {
      params: { id }
    }
  }: RouteComponentProps<{ id: string }>
) => ({
  notifications: data,
  loading,
  notification:
    data && data.find(notification => notification.notificationCaseId === id)
});

const mapDispatchToProps = {
  fetchNotifications
};

type NotificationDetailsState = {
  absencesWithHandlers: AbsenceWithHandler[] | null;
  absencesWithHandlersLoading: boolean;
  claimBenefits: ClaimBenefit[] | null;
  absencePaidLeaves: AbsencePaidLeave[] | null;
  wageReplacementLoading: boolean;
  periodDecisions: AbsencePeriodDecisions[] | null;
  periodDecisionsLoading: boolean;
  disabilityBenefitWrappers: DisabilityBenefitWrapper[] | null;
  disabilityBenefitsLoading: boolean;
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
type NotificationDetailsProps = {
  config: AppConfig;
} & StateProps &
  DispatchProps &
  IntlProps &
  RouteComponentProps<{ id: string }>;

export class NotificationDetailContainer extends React.Component<
  NotificationDetailsProps,
  NotificationDetailsState
> {
  state: NotificationDetailsState = {
    absencesWithHandlers: null,
    absencesWithHandlersLoading: false,
    claimBenefits: null,
    absencePaidLeaves: null,
    wageReplacementLoading: false,
    periodDecisions: null,
    periodDecisionsLoading: true,
    disabilityBenefitWrappers: null,
    disabilityBenefitsLoading: false
  };

  claimService = ClaimService.getInstance();
  absenceService = AbsenceService.getInstance();

  async componentDidMount() {
    const { notification } = this.props;

    if (notification === null) {
      this.props.fetchNotifications();
    } else {
      await this.fetchRelatedData();
    }
  }

  async componentDidUpdate(prevProps: Readonly<NotificationDetailsProps>) {
    if (!prevProps.notification && this.props.notification) {
      await this.fetchRelatedData();
    }
  }

  async fetchRelatedData() {
    await this.fetchAbsencesWithHandlers();
    await this.fetchWageReplacementRelatedInfo();
    await this.fetchPeriodDecisions();
  }

  async fetchAbsencesWithHandlers() {
    const { notification } = this.props;

    if (!notification) {
      return;
    }

    const { absences: absenceSummaries } = notification;
    let absencesWithHandlers: AbsenceWithHandler[] | null = null;
    try {
      if (absenceSummaries) {
        absencesWithHandlers = await Promise.all(
          absenceSummaries.map(
            async ({
              absenceHandler,
              absenceHandlerPhoneNumber,
              absenceId
            }) => ({
              handlerInfo: {
                absenceHandler,
                absenceHandlerPhoneNumber
              },
              absence: await this.absenceService.getAbsenceDetails(absenceId)
            })
          )
        );
      }
      this.setState({
        absencesWithHandlers,
        absencesWithHandlersLoading: false
      });
    } catch (error) {
      this.setState({ absencesWithHandlersLoading: false });
    }
  }

  async fetchPeriodDecisions() {
    const absences =
      this.props.notification && this.props.notification.absences;

    if (!!absences && !!absences.length) {
      try {
        this.setState({ periodDecisionsLoading: true });

        const periodDecisions = await Promise.all(
          absences.map(({ absenceId }) =>
            this.absenceService.getAbsencePeriodDecisions(absenceId)
          )
        );

        this.setState({ periodDecisions, periodDecisionsLoading: false });
      } catch (error) {
        this.setState({ periodDecisionsLoading: false });
      }
    } else {
      this.setState({ periodDecisionsLoading: false });
    }
  }

  async fetchWageReplacementRelatedInfo() {
    const {
      notification,
      intl: { formatMessage }
    } = this.props;

    const { absencesWithHandlers } = this.state;

    if (!notification) {
      return;
    }

    const claimIds =
      notification.claims && notification.claims.map(claim => claim.claimId);

    try {
      let filteredClaimBenefits: ClaimBenefit[] | null = null;
      let filteredAbsencePaidLeaves: AbsencePaidLeave[] | null = null;

      this.setState({ wageReplacementLoading: true });

      if (absencesWithHandlers) {
        const absencePaidLeaves = await Promise.all(
          absencesWithHandlers.map(
            async ({
              absence: { financialCaseIds },
              handlerInfo: { absenceHandler, absenceHandlerPhoneNumber }
            }) => {
              const claimBenefits = await Promise.all(
                financialCaseIds.map(async claimId => ({
                  claimId,
                  benefits: await this.claimService.findBenefits(claimId)
                }))
              );

              return {
                claimBenefits: _flatten(claimBenefits),
                paidLeaveHandler: absenceHandler,
                paidLeaveHandlerPhoneNumber: absenceHandlerPhoneNumber
              };
            }
          )
        );

        filteredAbsencePaidLeaves = _flatten(
          absencePaidLeaves.map(
            ({
              claimBenefits,
              paidLeaveHandler,
              paidLeaveHandlerPhoneNumber
            }) => ({
              claimBenefits: claimBenefits.filter(
                ({ benefits }) => !!benefits && !!benefits.length
              ),
              paidLeaveHandler,
              paidLeaveHandlerPhoneNumber
            })
          )
        ).filter(
          ({ claimBenefits }) => !!claimBenefits && !!claimBenefits.length
        );

        this.setState({
          absencePaidLeaves: filteredAbsencePaidLeaves
        });
      }

      if (claimIds) {
        const claimBenefits = await Promise.all(
          claimIds.map(async claimId => ({
            claimId,
            benefits: await this.claimService.findBenefits(claimId)
          }))
        );

        filteredClaimBenefits = claimBenefits.filter(
          ({ benefits }) => !!benefits && !!benefits.length
        );

        this.setState({
          claimBenefits: filteredClaimBenefits
        });
      }

      this.fetchDisabilityBenefits(
        filteredAbsencePaidLeaves,
        filteredClaimBenefits
      );
    } catch (error) {
      const title = formatMessage({
        id: 'NOTIFICATION.WAGE_REPLACEMENT_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ wageReplacementLoading: false });
    }
  }

  async fetchDisabilityBenefits(
    absencePaidLeaves: AbsencePaidLeave[] | null,
    claimBenefits: ClaimBenefit[] | null
  ) {
    try {
      this.setState({ disabilityBenefitsLoading: true });

      let benefitRequests: Promise<DisabilityBenefitWrapper>[] = [];

      const absencePaidLeavesToRequest = !_isEmpty(absencePaidLeaves)
        ? _flatten(
            absencePaidLeaves!.map(
              absencePaidLeave => absencePaidLeave.claimBenefits
            )
          )
        : [];

      const claimBenefitsToRequest = !_isEmpty(claimBenefits)
        ? claimBenefits!
        : [];

      for (const { claimId, benefits } of [
        ...claimBenefitsToRequest,
        ...absencePaidLeavesToRequest
      ]) {
        benefitRequests = [
          ...benefitRequests,
          ...benefits
            .filter(
              benefit =>
                benefit.benefitRightCategory ===
                BenefitRightCategory.RECURRING_BENEFIT
            )
            .map(({ benefitId }) =>
              this.claimService
                .readDisabilityBenefit(claimId, String(benefitId))
                .then(disabilityBenefitSummary => ({
                  disabilityBenefitSummary,
                  claimId,
                  benefitId
                }))
            )
        ];
      }
      this.setState({
        disabilityBenefitWrappers: await Promise.all(benefitRequests),
        disabilityBenefitsLoading: false
      });
    } catch (error) {
      this.setState({ disabilityBenefitsLoading: false });
    }
  }

  render() {
    const {
      notification,
      loading,
      config: {
        customerDetail: { customer }
      }
    } = this.props;
    const {
      absencesWithHandlers,
      absencesWithHandlersLoading,
      claimBenefits,
      absencePaidLeaves,
      wageReplacementLoading,
      periodDecisions,
      periodDecisionsLoading,
      disabilityBenefitWrappers,
      disabilityBenefitsLoading
    } = this.state;

    return (
      <ContentRenderer
        loading={loading || absencesWithHandlersLoading}
        isEmpty={_isEmpty(notification)}
      >
        {!_isEmpty(notification) && (
          <NotificationDetailView
            notification={notification!}
            customer={customer}
            wageReplacementLoading={wageReplacementLoading}
            claimBenefits={claimBenefits}
            absencePaidLeaves={absencePaidLeaves}
            absencesWithHandlers={absencesWithHandlers}
            periodDecisions={periodDecisions}
            periodDecisionsLoading={periodDecisionsLoading}
            disabilityBenefitWrappers={disabilityBenefitWrappers}
            disabilityBenefitsLoading={disabilityBenefitsLoading}
          />
        )}
      </ContentRenderer>
    );
  }
}

export default injectIntl(
  withRouter(
    withAppConfig(
      connect(mapStateToProps, mapDispatchToProps)(NotificationDetailContainer)
    )
  )
);
