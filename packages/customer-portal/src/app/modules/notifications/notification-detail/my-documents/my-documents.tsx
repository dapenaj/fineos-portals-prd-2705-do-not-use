import React from 'react';
import {
  CaseDocument,
  NotificationCaseSummary,
  ClaimService,
  DocumentSearch
} from 'fineos-js-api-client';
import { orderBy as _orderBy } from 'lodash';
import { List, Icon, Badge } from 'antd';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { FormattedMessage } from 'react-intl';

import { EmptyState, showNotification } from '../../../../shared/ui';

import { MyDocumentsList } from './my-documents-list';
import styles from './my-documents.module.scss';

type MyDocumentsProps = {
  notification: NotificationCaseSummary;
} & IntlProps;

type MyDocumentsState = {
  documents: CaseDocument[] | null;
  unreadDocuments: CaseDocument[];
  loading: boolean;
};

export class MyDocumentsComponent extends React.Component<
  MyDocumentsProps,
  MyDocumentsState
> {
  state: MyDocumentsState = {
    documents: null,
    unreadDocuments: [],
    loading: false
  };

  claimService = ClaimService.getInstance();

  async componentDidMount() {
    const {
      notification,
      intl: { formatMessage }
    } = this.props;

    const search = new DocumentSearch();
    search.includeChildCases(true);

    try {
      this.setState({ loading: true });

      const documents = await this.claimService.findDocuments(
        notification.notificationCaseId,
        search
      );

      this.setState({
        documents: _orderBy(documents, 'receivedDate', 'desc')
      });

      this.handleDocumentDownload(documents);
    } catch (error) {
      const title = formatMessage({
        id: 'NOTIFICATION.DOCUMENTS_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  }

  handleDocumentDownload = (documents: CaseDocument[]) => {
    if (!!documents && !!documents.length) {
      const unreadDocuments = documents.filter(item => !item.isRead);
      this.setState({ unreadDocuments });
    }
  };

  render() {
    const { documents, unreadDocuments, loading } = this.state;

    return (
      <div className={styles.wrapper} data-test-el="my-documents">
        <List
          header={
            <div className={styles.header} data-test-el="my-documents-header">
              <div>
                <Badge dot={unreadDocuments.length > 0} className={styles.icon}>
                  <Icon type="file-text" />
                </Badge>
                <FormattedMessage
                  id="NOTIFICATION.MY_DOCUMENTS"
                  values={{ count: documents ? documents.length : 0 }}
                />
              </div>
              <div className={styles.unread}>
                <FormattedMessage
                  id="NOTIFICATION.UNREAD_DOCUMENTS"
                  values={{ unread: unreadDocuments.length }}
                />
              </div>
            </div>
          }
          loading={loading}
        >
          {!!documents && !!documents.length && (
            <MyDocumentsList
              documents={documents}
              onDocumentDownload={this.handleDocumentDownload}
            />
          )}

          {!!documents && documents.length === 0 && (
            <EmptyState>
              <FormattedMessage id="NOTIFICATION.NO_DOCUMENTS" />
            </EmptyState>
          )}
        </List>
      </div>
    );
  }
}

export const MyDocuments = injectIntl(MyDocumentsComponent);
