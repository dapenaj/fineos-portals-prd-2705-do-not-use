import React from 'react';
import { FormattedMessage } from 'react-intl';
import { List, Button } from 'antd';
import { DocumentService, CaseDocument } from 'fineos-js-api-client';
import classnames from 'classnames';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { formatDateForDisplay } from '../../../../../shared/utils';
import { showNotification, Spinner } from '../../../../../shared/ui';
import { ExportService } from '../../../../../shared/services';

import styles from './my-documents-list.module.scss';

export type MyDocumentsListProps = {
  documents: CaseDocument[];
  onDocumentDownload: (documents: CaseDocument[]) => void;
} & IntlProps;

type MyDocumentsListState = {
  documents: CaseDocument[];
  loading: boolean;
};

export class MyDocumentsListComponent extends React.Component<
  MyDocumentsListProps,
  MyDocumentsListState
> {
  state: MyDocumentsListState = {
    documents: [],
    loading: false
  };

  documentService = DocumentService.getInstance();
  exportService = ExportService.getInstance();

  componentDidMount() {
    const { documents } = this.props;

    this.setState({ documents });
  }

  downloadDocument = async (caseDocument: CaseDocument) => {
    const {
      intl: { formatMessage }
    } = this.props;

    try {
      this.setState({ loading: true });

      const downloadDocument = await this.documentService.downloadCaseDocument(
        caseDocument.caseId,
        caseDocument.documentId
      );

      this.exportService.exportBinary(
        downloadDocument,
        caseDocument.originalFilename
      );

      this.markDocumentAsRead(caseDocument);
    } catch (error) {
      const title = formatMessage({
        id: 'NOTIFICATION.DOCUMENTS_DOWNLOAD_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  };

  markDocumentAsRead = async (document: CaseDocument) => {
    const { onDocumentDownload } = this.props;
    const { documents } = this.state;

    if (!document.isRead) {
      this.setState({ loading: true });

      const readDocument = await this.documentService.markDocumentAsRead(
        document.caseId,
        document.documentId
      );

      const readDocuments = documents.map(d =>
        d.documentId === readDocument.documentId ? readDocument : d
      );

      this.setState({ loading: false, documents: readDocuments });
      onDocumentDownload(readDocuments);
    }
  };

  render() {
    const { documents, loading } = this.state;
    const { Item } = List;

    return (
      <div className={styles.scroll} data-test-el="my-documents-list">
        {!loading &&
          documents.map((document, index) => (
            <Item key={index}>
              <div className={styles.details}>
                <Button
                  data-test-el="download-document-btn"
                  className={classnames(
                    styles.document,
                    document.isRead ? styles.read : styles.unread
                  )}
                  type="link"
                  onClick={() => this.downloadDocument(document)}
                >
                  {document.name}
                  {document.fileExtension}
                </Button>
                <p className={styles.date} data-test-el="document-date">
                  <FormattedMessage
                    id={'NOTIFICATION.DATE_RECEIVED'}
                    values={{
                      date: formatDateForDisplay(document.receivedDate)
                    }}
                  />
                </p>
              </div>
            </Item>
          ))}

        {loading && <Spinner />}
      </div>
    );
  }
}

export const MyDocumentsList = injectIntl(MyDocumentsListComponent);
