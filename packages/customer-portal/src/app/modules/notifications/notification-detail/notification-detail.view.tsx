import React from 'react';
import {
  NotificationCaseSummary,
  Customer,
  AbsencePeriodDecisions
} from 'fineos-js-api-client';
import { Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { PageTitle, Row, Spinner } from '../../../shared/ui';
import { ContentLayout } from '../../../shared/layouts';
import {
  AbsencePaidLeave,
  DisabilityBenefitWrapper,
  ClaimBenefit,
  AbsenceWithHandler
} from '../../../shared/types';

import { Timeline } from './timeline';
import { NotificationSummary } from './notification-summary';
import { JobProtectedLeave } from './job-protected-leave';
import { WageReplacement } from './wage-replacement';
import { MyDocuments } from './my-documents';
import { RecentMessages } from './recent-messages';
import { WorkplaceAccommodations } from './workplace-accommodations';
import { RequiredFromMe } from './required-from-me';

type NotificationDetailViewProps = {
  notification: NotificationCaseSummary;
  customer: Customer | null;
  wageReplacementLoading: boolean;
  claimBenefits: ClaimBenefit[] | null;
  absencePaidLeaves: AbsencePaidLeave[] | null;
  absencesWithHandlers: AbsenceWithHandler[] | null;
  periodDecisionsLoading: boolean;
  periodDecisions: AbsencePeriodDecisions[] | null;
  disabilityBenefitWrappers: DisabilityBenefitWrapper[] | null;
  disabilityBenefitsLoading: boolean;
};

export const NotificationDetailView: React.FC<NotificationDetailViewProps> = ({
  notification,
  customer,
  wageReplacementLoading,
  claimBenefits,
  absencePaidLeaves,
  absencesWithHandlers,
  periodDecisions,
  periodDecisionsLoading,
  disabilityBenefitWrappers,
  disabilityBenefitsLoading
}) => {
  const claimIds =
    notification.claims && notification.claims.map(claim => claim.claimId);

  return (
    <div data-test-el="notification-detail-view">
      <ContentLayout direction="column">
        <PageTitle>
          {notification ? (
            notification.notificationCaseId
          ) : (
            <FormattedMessage id="NOTIFICATION.NO_CASE" />
          )}
        </PageTitle>
        <Row>
          <Col xs={24} lg={16}>
            <NotificationSummary notification={notification} />

            {!wageReplacementLoading &&
            !disabilityBenefitsLoading &&
            !periodDecisionsLoading ? (
              /* as timeline quite complex and use a lot of inmemory calculation it's easier to reset all via "key" */
              <Timeline
                key={notification.notificationCaseId}
                claimBenefits={claimBenefits}
                notification={notification}
                periodDecisions={periodDecisions}
                absencesWithHandlers={absencesWithHandlers}
                absencePaidLeaves={absencePaidLeaves}
                disabilityBenefitWrappers={disabilityBenefitWrappers}
              />
            ) : (
              <Spinner />
            )}

            {!periodDecisionsLoading ? (
              <JobProtectedLeave
                periodDecisions={periodDecisions}
                absencesWithHandlers={absencesWithHandlers}
              />
            ) : (
              <Spinner />
            )}

            {!wageReplacementLoading && !disabilityBenefitsLoading ? (
              <WageReplacement
                claimBenefits={claimBenefits}
                claimIds={claimIds || []}
                paidLeaves={absencePaidLeaves}
                disabilityBenefitWrappers={disabilityBenefitWrappers}
              />
            ) : (
              <Spinner />
            )}

            <WorkplaceAccommodations
              accommodations={notification.accommodations}
            />
          </Col>
          <Col xs={24} lg={8}>
            <RequiredFromMe notification={notification} />
            <MyDocuments notification={notification} />
            <RecentMessages notification={notification} customer={customer} />
          </Col>
        </Row>
      </ContentLayout>
    </div>
  );
};
