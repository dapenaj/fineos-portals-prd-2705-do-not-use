import React from 'react';
import { Button, Modal } from 'antd';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';

import { CasePayment } from '../../../../../shared/types';

import { PaymentModalContent } from './payment-modal-content.component.tsx';
import styles from './latest-payments-modal.module.scss';

const MODAL_WIDTH = '100%';

type LatestPaymentModalState = {
  showModal: boolean;
};

type LatestPaymentModalProps = {
  showPaymentHistory: boolean;
  caseType: string;
  paymentDetails: CasePayment[] | null;
};

export class LatestPaymentsModal extends React.Component<
  LatestPaymentModalProps,
  LatestPaymentModalState
> {
  state: LatestPaymentModalState = {
    showModal: false
  };

  closePaymentModal = (event: React.MouseEvent) => {
    event.stopPropagation();
    this.setState({ showModal: false });
  };

  showPaymentModal = (event: React.MouseEvent) => {
    event.stopPropagation();
    this.setState({ showModal: true });
  };

  render() {
    const { showModal } = this.state;
    const { showPaymentHistory, caseType, paymentDetails } = this.props;

    return (
      <>
        <span data-test-el="notification-payment-header">
          <FormattedMessage id="HOME.LATEST_PAYMENTS.TITLE" />
        </span>

        {showPaymentHistory && (
          <>
            <Button type="link" onClick={event => this.showPaymentModal(event)}>
              <FormattedMessage id="NOTIFICATION.VIEW_PAYMENT_HISTORY" />
            </Button>

            <Modal
              visible={showModal}
              width={MODAL_WIDTH}
              onCancel={event => this.closePaymentModal(event)}
              title={
                <FormattedMessage id="NOTIFICATION.VIEW_PAYMENT_HISTORY_MODAL" />
              }
              footer={[
                <Button
                  type="primary"
                  className={styles.button}
                  onClick={event => this.closePaymentModal(event)}
                  key="close"
                >
                  <FormattedMessage id="NOTIFICATION.CLOSE" />
                </Button>
              ]}
              className={classnames(styles.modal, styles.padding)}
            >
              <PaymentModalContent
                caseType={caseType}
                paymentDetails={paymentDetails}
              />
            </Modal>
          </>
        )}
      </>
    );
  }
}
