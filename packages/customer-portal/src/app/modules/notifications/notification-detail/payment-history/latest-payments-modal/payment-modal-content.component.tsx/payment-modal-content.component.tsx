import React from 'react';

import { CasePayment } from '../../../../../../shared/types';
import { Payment } from '../../../../../../shared/ui';

import styles from './payment-modal-content.module.scss';

type PaymentModalProps = {
  caseType: string;
  paymentDetails: CasePayment[] | null;
};

export const PaymentModalContent: React.FC<PaymentModalProps> = ({
  paymentDetails,
  caseType
}) => (
  <div className={styles.container}>
    <div className={styles.body}>
      <div className={styles.case}>{caseType}</div>
      {!!paymentDetails &&
        !!paymentDetails.length &&
        paymentDetails.map((payment, index) => (
          <Payment key={index} payment={payment} />
        ))}
    </div>
  </div>
);
