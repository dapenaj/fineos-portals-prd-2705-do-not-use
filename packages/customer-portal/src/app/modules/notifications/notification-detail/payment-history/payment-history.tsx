import React from 'react';
import { Collapse } from 'antd';
import moment from 'moment';
import { isEmpty as _isEmpty } from 'lodash';
import { FormattedMessage } from 'react-intl';

import { CasePayment } from '../../../../shared/types';
import { ContentRenderer, Payment } from '../../../../shared/ui';
import { parseDate } from '../../../../shared/utils';
import { PAYMENTS_DISPLAY_COUNT } from '../../../../shared/constants';

import { LatestPaymentsModal } from './latest-payments-modal/latest-payments-modal.component';
import styles from './payment-history.module.scss';

const { Panel } = Collapse;

type PaymentHistoryProps = {
  caseType: string;
  paymentDetails: CasePayment[];
  loading: boolean;
};

export const PaymentHistory: React.FC<PaymentHistoryProps> = ({
  caseType,
  paymentDetails,
  loading
}) => {
  const lastYearPayments = paymentDetails.filter(item =>
    parseDate(item.paymentDate).isAfter(moment().subtract(1, 'year'))
  );

  const showPaymentHistory =
    lastYearPayments.length > PAYMENTS_DISPLAY_COUNT ||
    paymentDetails.length > lastYearPayments.length;

  return (
    <div className={styles.wrapper} data-test-el="payment-history">
      <Collapse defaultActiveKey={['1']} accordion={true} bordered={false}>
        <Panel
          disabled={showPaymentHistory}
          header={
            <LatestPaymentsModal
              showPaymentHistory={showPaymentHistory}
              caseType={caseType}
              paymentDetails={paymentDetails}
            />
          }
          key="1"
        >
          <ContentRenderer
            loading={loading}
            isEmpty={_isEmpty(lastYearPayments)}
            emptyTitle={
              <FormattedMessage id={'HOME.LATEST_PAYMENTS.NO_PAYMENTS'} />
            }
          >
            {!_isEmpty(lastYearPayments) &&
              lastYearPayments
                .slice(0, PAYMENTS_DISPLAY_COUNT)
                .map((payment, index) => (
                  <Payment
                    className={styles.margin}
                    key={index}
                    payment={payment}
                  />
                ))}
          </ContentRenderer>
        </Panel>
      </Collapse>
    </div>
  );
};
