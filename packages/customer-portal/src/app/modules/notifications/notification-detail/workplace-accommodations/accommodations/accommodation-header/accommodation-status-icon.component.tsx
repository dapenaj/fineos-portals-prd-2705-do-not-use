import React from 'react';
import { FormattedMessage } from 'react-intl';

import {
  WorkplaceAccommodationStatus,
  WorkplaceAccommodationDecision
} from '../../../../../../shared/types';
import {
  getWorkplaceAccommodationStatus,
  getWorkplaceAccommodationStatusLabel
} from '../../workplace-accommodations.util';
import { NotificationPanelStatus } from '../../../ui';

type AccommodationStatusProps = {
  status: string;
  decision: string;
};

const getColorStyle = (color: string) => {
  switch (color) {
    case 'Pending':
      return '#ffc20e';
    case 'Accommodated':
      return '#35b558';
    case 'Not Accommodated':
      return '#f04c3f';
    default:
      return '#b3b3b3';
  }
};

export const AccommodationStatusIcon: React.FC<AccommodationStatusProps> = ({
  status,
  decision
}) => (
  <span data-test-el="accommodation-status-icon">
    <NotificationPanelStatus
      color={getColorStyle(
        getWorkplaceAccommodationStatus(
          status as WorkplaceAccommodationStatus,
          decision as WorkplaceAccommodationDecision
        )
      )}
      status={
        <FormattedMessage
          id={getWorkplaceAccommodationStatusLabel(
            getWorkplaceAccommodationStatus(
              status as WorkplaceAccommodationStatus,
              decision as WorkplaceAccommodationDecision
            )
          )}
        />
      }
    />
  </span>
);
