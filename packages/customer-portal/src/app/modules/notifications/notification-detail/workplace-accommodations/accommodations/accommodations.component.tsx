import React from 'react';
import { NotificationAccommodationSummary } from 'fineos-js-api-client';
import { Collapse } from 'antd';

import { AccommodationHeader } from './accommodation-header';
import { AccommodationProperties } from './accommodation-properties.component';
import styles from './accommodations.module.scss';

const { Panel } = Collapse;

type AccommodationsProps = {
  accommodations: NotificationAccommodationSummary[];
};

type AccommodationsState = {
  activeKey: string | string[];
};

export class Accommodations extends React.Component<
  AccommodationsProps,
  AccommodationsState
> {
  state = { activeKey: [] as string[] };

  render() {
    const { accommodations } = this.props;
    const { activeKey } = this.state;

    return (
      <div data-test-el="accommodations" className={styles.wrapper}>
        <Collapse
          onChange={key =>
            this.setState({
              activeKey: key
            })
          }
        >
          {accommodations.map((accommodation, index) => (
            <Panel
              key={`accommodation_${index}`}
              header={
                <AccommodationHeader
                  expanded={activeKey.includes(`accommodation_${index}`)}
                  accommodation={accommodation}
                />
              }
            >
              <AccommodationProperties accommodation={accommodation} />
            </Panel>
          ))}
        </Collapse>
      </div>
    );
  }
}
