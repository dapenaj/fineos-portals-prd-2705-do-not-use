import React from 'react';
import { NotificationAccommodationSummary } from 'fineos-js-api-client';

import { WorkplaceAccommodationsHeader } from './workplace-accommodations-header.component';
import { Accommodations } from './accommodations';
import styles from './workplace-accommodations.module.scss';

type WorkplaceAccommodationsProps = {
  accommodations?: NotificationAccommodationSummary[];
};

export const WorkplaceAccommodations: React.FC<WorkplaceAccommodationsProps> = ({
  accommodations
}) => (
  <>
    {!!accommodations && !!accommodations.length && (
      <div data-test-el="workplace-accommodations" className={styles.wrapper}>
        <WorkplaceAccommodationsHeader />
        <Accommodations accommodations={accommodations} />
      </div>
    )}
  </>
);
