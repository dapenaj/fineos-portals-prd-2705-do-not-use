import React from 'react';
import { NotificationAccommodationSummary } from 'fineos-js-api-client';
import { Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { NotificationProperties } from '../../ui';
import {
  WorkplaceAccommodationStatus,
  WorkplaceAccommodationDecision
} from '../../../../../shared/types';
import { getWorkplaceAccommodationStatus } from '../workplace-accommodations.util';
import { PropertyItem } from '../../../../../shared/ui';

import { AccommodationStatusIcon } from './accommodation-header';
import { ProposedAccommodations } from './proposed-accommodations';
import styles from './accommodations.module.scss';
import { EmptyAccommodations } from './empty-accommodations';

type AccommodationPropertiesProps = {
  accommodation: NotificationAccommodationSummary;
};

export const AccommodationProperties: React.FC<AccommodationPropertiesProps> = ({
  accommodation: {
    status,
    accommodationDecision,
    limitations,
    notifiedBy,
    workplaceAccommodations
  }
}) => (
  <div data-test-el="wage-replacement-properties">
    <NotificationProperties
      status={getWorkplaceAccommodationStatus(
        status as WorkplaceAccommodationStatus,
        accommodationDecision as WorkplaceAccommodationDecision
      ).toLowerCase()}
    >
      <Col xs={24} sm={12}>
        <PropertyItem
          label={<FormattedMessage id="NOTIFICATION.LIMITATIONS" />}
        >
          {limitations.map(({ id, limitationType }) => (
            <div key={`limitation_${id}`}> - {limitationType}</div>
          ))}
        </PropertyItem>
      </Col>
      <Col xs={24} sm={6}>
        <PropertyItem
          label={<FormattedMessage id="NOTIFICATION.NOTIFIED_BY" />}
        >
          {notifiedBy}
        </PropertyItem>
      </Col>
      <Col xs={24} sm={6}>
        <AccommodationStatusIcon
          status={status}
          decision={accommodationDecision}
        />
      </Col>

      <div className={styles.title}>
        <FormattedMessage id="NOTIFICATION.ACCOMMODATIONS_PROPOSED" />
      </div>

      {!!workplaceAccommodations && !!workplaceAccommodations.length ? (
        <ProposedAccommodations accommodations={workplaceAccommodations} />
      ) : (
        <Col xs={24}>
          <EmptyAccommodations />
        </Col>
      )}
    </NotificationProperties>
  </div>
);
