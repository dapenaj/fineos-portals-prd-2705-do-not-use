import React from 'react';
import { NotificationAccommodationSummary } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'antd';

import { formatDateForDisplay } from '../../../../../../shared/utils';
import { ContentLayout } from '../../../../../../shared/layouts';
import { InlinePropertyItem } from '../../../../../../shared/ui';
import { HandlerInfo } from '../../../ui';

import { AccommodationStatusIcon } from './accommodation-status-icon.component';

type AccommodationHeaderProps = {
  expanded: boolean;
  accommodation: NotificationAccommodationSummary;
};

export const AccommodationHeader: React.FC<AccommodationHeaderProps> = ({
  expanded,
  accommodation: {
    status,
    accommodationDecision,
    notificationDate,
    caseHandler,
    caseHandlerPhoneNumber
  }
}) => (
  <div data-test-el="accommodation-header">
    {expanded ? (
      <ContentLayout direction="column" noPadding={true}>
        <Row type="flex" align="middle">
          <Col xs={24} sm={12}>
            <div data-test-el="accommodation-header-name">
              <FormattedMessage id="NOTIFICATION.ACCOMMODATION_REQUEST" />
            </div>
            <div>
              <InlinePropertyItem
                label={<FormattedMessage id="NOTIFICATION.CREATED_ON" />}
              >
                {formatDateForDisplay(notificationDate)}
              </InlinePropertyItem>
            </div>
          </Col>
          <Col xs={24} sm={12}>
            <HandlerInfo
              data-test-el="accommodation-header-handler-info"
              name={caseHandler}
              phoneNumber={caseHandlerPhoneNumber}
            />
          </Col>
        </Row>
      </ContentLayout>
    ) : (
      <>
        <div data-test-el="accommodation-header-name">
          <FormattedMessage
            id="NOTIFICATION.ACCOMMODATION_REQUEST_DATE"
            values={{ date: formatDateForDisplay(notificationDate) }}
          />
        </div>
        <div>
          <AccommodationStatusIcon
            status={status}
            decision={accommodationDecision}
          />
        </div>
      </>
    )}
  </div>
);
