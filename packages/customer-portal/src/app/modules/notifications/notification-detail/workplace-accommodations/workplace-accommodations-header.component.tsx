import React from 'react';
import { FormattedMessage } from 'react-intl';

import { SummaryHeader } from '../ui';
import workplaceAccommodationIconSvg from '../../../../../assets/img/workplace_accommodation_icon.svg';

export const WorkplaceAccommodationsHeader: React.FC = () => (
  <div data-test-el="workplace-accommodation-header">
    <SummaryHeader
      title={<FormattedMessage id="NOTIFICATION.WORKPLACE_ACCOMMODATIONS" />}
      imgIcon={<img src={workplaceAccommodationIconSvg} alt="" />}
    />
  </div>
);
