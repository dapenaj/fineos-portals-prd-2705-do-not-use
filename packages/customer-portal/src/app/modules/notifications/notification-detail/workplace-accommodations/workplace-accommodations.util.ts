import {
  WorkplaceAccommodationDisplayStatus,
  WorkplaceAccommodationStatus,
  WorkplaceAccommodationDecision
} from '../../../../shared/types';

export const getWorkplaceAccommodationStatus = (
  status: WorkplaceAccommodationStatus,
  decision: WorkplaceAccommodationDecision
): WorkplaceAccommodationDisplayStatus => {
  switch (status) {
    case 'Completion':
    case 'Closed':
      return getWorkplaceAccommodationDecision(decision);
    case 'Monitoring':
      return 'Accommodated';
    default:
      return 'Pending';
  }
};

export const getWorkplaceAccommodationDecision = (
  decision: WorkplaceAccommodationDecision
): WorkplaceAccommodationDisplayStatus =>
  decision === 'Accommodated' ? 'Accommodated' : 'Not Accommodated';

export const getWorkplaceAccommodationStatusLabel = (
  status: WorkplaceAccommodationDisplayStatus
) => {
  switch (status) {
    case 'Accommodated':
      return 'WORKPLACE_ACCOMMODATION_STATUS.ACCOMMODATED';
    case 'Not Accommodated':
      return 'WORKPLACE_ACCOMMODATION_STATUS.NOT_ACCOMMODATED';
    default:
      return 'WORKPLACE_ACCOMMODATION_STATUS.PENDING';
  }
};
