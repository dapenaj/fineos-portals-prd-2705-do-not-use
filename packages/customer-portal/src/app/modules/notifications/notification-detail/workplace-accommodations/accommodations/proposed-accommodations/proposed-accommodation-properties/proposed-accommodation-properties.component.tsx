import React from 'react';
import { WorkPlaceAccommodationDetail } from 'fineos-js-api-client';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { ContentLayout } from '../../../../../../../shared/layouts';
import { PropertyItem } from '../../../../../../../shared/ui';
import { formatDateForDisplay } from '../../../../../../../shared/utils';

import styles from './proposed-accommodation-properties.module.scss';

type ProposedAccommodationProperties = {
  accommodation: WorkPlaceAccommodationDetail;
};

export const ProposedAccommodationProperties: React.FC<ProposedAccommodationProperties> = ({
  accommodation: {
    accommodationDescription,
    creationDate,
    implementedDate,
    accommodationEndDate
  }
}) => (
  <div
    className={styles.wrapper}
    data-test-el="proposed-accommodation-properties"
  >
    <ContentLayout direction="column" noPadding={true}>
      <Row type="flex" align="top">
        <Col xs={24} sm={8}>
          <PropertyItem
            label={<FormattedMessage id="NOTIFICATION.REQUEST_DATE" />}
          >
            {formatDateForDisplay(creationDate)}
          </PropertyItem>
        </Col>
        <Col xs={24} sm={8}>
          <PropertyItem
            label={<FormattedMessage id="NOTIFICATION.DESCRIPTION" />}
          >
            {accommodationDescription}
          </PropertyItem>
        </Col>
        <Col xs={24} sm={8}>
          <PropertyItem
            label={<FormattedMessage id="NOTIFICATION.ACCOMMODATION_DATE" />}
          >
            <>
              {formatDateForDisplay(implementedDate)}
              {!!accommodationEndDate && (
                <span> - {formatDateForDisplay(accommodationEndDate)}</span>
              )}
            </>
          </PropertyItem>
        </Col>
      </Row>
    </ContentLayout>
  </div>
);
