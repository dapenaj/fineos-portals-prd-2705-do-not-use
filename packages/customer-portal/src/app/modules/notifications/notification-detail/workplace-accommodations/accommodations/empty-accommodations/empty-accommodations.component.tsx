import React from 'react';
import { FormattedMessage } from 'react-intl';

import styles from './empty-accommodations.module.scss';

export const EmptyAccommodations: React.FC = () => (
  <div className={styles.wrapper}>
    <div className={styles.content}>
      <FormattedMessage id="ACCOMMODATIONS.EMPTY" />
    </div>
  </div>
);
