import React from 'react';
import { WorkPlaceAccommodationDetail } from 'fineos-js-api-client';
import { Collapse } from 'antd';

import { ProposedAccommodationProperties } from './proposed-accommodation-properties';
import styles from './proposed-accommodations.module.scss';

const { Panel } = Collapse;

type ProposedAccommodationsProps = {
  accommodations: WorkPlaceAccommodationDetail[];
};

export const ProposedAccommodations: React.FC<ProposedAccommodationsProps> = ({
  accommodations
}) => (
  <div data-test-el="proposed-accommodations" className={styles.wrapper}>
    <Collapse>
      {accommodations.map((accommodation, index) => (
        <Panel
          key={`proposed_accommodation_${index}`}
          header={
            <>
              {accommodation.accommodationCategory} |
              {accommodation.accommodationType}
            </>
          }
        >
          <ProposedAccommodationProperties accommodation={accommodation} />
        </Panel>
      ))}
    </Collapse>
  </div>
);
