import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';
import { Button, Collapse } from 'antd';

import { Panel } from '../../../../shared/ui/panel';

import { Gantt, GanttGroup } from './gantt';
import { TimelineLegend } from './timeline-legend';
import styles from './timeline.module.scss';

const { Panel: CollapsePanel } = Collapse;

type TimelineViewProps = {
  ganttGroups: GanttGroup[];
};

type TimelineViewState = {
  isLegendShown: boolean;
};

export class TimelineView extends React.PureComponent<
  TimelineViewProps,
  TimelineViewState
> {
  state = {
    isLegendShown: false
  };

  toggleLegend = () => {
    this.setState(({ isLegendShown }) => ({
      isLegendShown: !isLegendShown
    }));
  };

  render() {
    const { ganttGroups } = this.props;
    const { isLegendShown } = this.state;

    return (
      <div data-test-el="notification-timeline" className={styles.wrapper}>
        <Collapse defaultActiveKey={['1']}>
          <CollapsePanel
            header={<FormattedMessage id="NOTIFICATION.TIMELINE.TITLE" />}
            key="1"
          >
            <div className={styles.legendControlRow}>
              <Button
                type="link"
                name="legend-toggle-control"
                onClick={this.toggleLegend}
              >
                <FormattedMessage
                  id={
                    'NOTIFICATION.TIMELINE.' +
                    (!isLegendShown ? 'SHOW_CAPTION' : 'CLOSE_CAPTION')
                  }
                />
              </Button>
            </div>
            <div className={styles.timelineGantt}>
              <Panel
                className={classNames(
                  styles.legend,
                  isLegendShown && styles.open
                )}
              >
                <TimelineLegend />
              </Panel>

              <Gantt
                title={
                  <FormattedMessage id="NOTIFICATION.TIMELINE.GANTT_TITLE" />
                }
                groups={ganttGroups}
              />
            </div>
          </CollapsePanel>
        </Collapse>
      </div>
    );
  }
}
