import React from 'react';
import classNames from 'classnames';
import { Popover } from 'antd';

import { TIME_ENTRY_SIZE_PX } from '../gantt.variables';
import { GanttEntryColor, GanttEntryShape } from '../gantt.type';

import { colorStyles, shapeStyles } from './gantt-time-bars-styles';
import styles from './gantt-time-bars.module.scss';

type GanttEntryNormalTimeBarProps = {
  top: number;
  height: number;
  left: number;
  width: number;
  color: GanttEntryColor;
  shape: GanttEntryShape;
  tooltip?: React.ReactNode;
  disableStartProjectionLine: boolean;
  disableEndProjectionLine: boolean;
};

// internal template component
export const GanttEntryNormalTimeBar: React.FC<GanttEntryNormalTimeBarProps> = React.memo(
  ({
    top,
    height,
    left,
    width,
    tooltip,
    color,
    shape,
    disableStartProjectionLine,
    disableEndProjectionLine
  }) => {
    const topOffset = top + (height - TIME_ENTRY_SIZE_PX) / 2;
    const projectLineHeight = topOffset + (height - TIME_ENTRY_SIZE_PX) / 2;
    const timeBar = (
      <div
        className={classNames(
          styles.durationTimeBar,
          colorStyles[color],
          shapeStyles[shape]
        )}
        style={{
          top: topOffset,
          left,
          width
        }}
      />
    );

    // as duration bar may not fit in gantt view, and regular ant tooltip placement can only have fixed options
    // like left, center, right - which doesn't solve this use case.
    // ant tooltip based, on rc-tooltip, which is based on rc-trigger and has option `alignPoint: boolean` (default false)
    // setting this option to true will force showing tooltip based on mouse position, which always gonna be in
    // gantt view. unfortunately ant doesn't have proper type definition for all props which it's actually supports
    // that's why `any` type is required here
    const popoverConfig: any = {
      alignPoint: true
    };

    return (
      <>
        {!disableStartProjectionLine && (
          <div
            className={styles.projectionLine}
            style={{
              height: projectLineHeight,
              left
            }}
          />
        )}
        {tooltip ? (
          <Popover
            content={tooltip}
            overlayClassName={styles.tooltipContainer}
            {...popoverConfig}
          >
            {timeBar}
          </Popover>
        ) : (
          timeBar
        )}

        {!disableEndProjectionLine && (
          <div
            className={styles.projectionLine}
            style={{
              height: projectLineHeight,
              left: left + width
            }}
          />
        )}
      </>
    );
  }
);
