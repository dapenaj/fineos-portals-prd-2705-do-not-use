import React from 'react';
import {
  flatten as _flatten,
  uniqBy as _uniqBy,
  sortBy as _sortBy
} from 'lodash';
import { ScaleTime } from 'd3-scale';
import { Moment } from 'moment';

import { GanttGroup } from '../gantt.type';
import {
  DAY_RESERVED_SPACE_PX,
  TODAY_TOOLTIP_WIDTH_PX
} from '../gantt.variables';

import styles from './gantt-dates-marks.module.scss';

type GanttDatesMarksProps = {
  groups: GanttGroup[];
  // if today is null then it's not taken into account
  today: Moment | null;
  scale: ScaleTime<number, number>;
};

const isDayCloseToMonthEdges = (
  originalDay: Moment,
  scale: ScaleTime<number, number>
): boolean => {
  const dayClone = originalDay.clone();
  const HALF_DAY_RESERVED_SPACE_PX = DAY_RESERVED_SPACE_PX / 2;
  const originalDayOffset = Math.ceil(scale(originalDay));
  const startMonthOffset = Math.ceil(scale(dayClone.startOf('month')));
  const endMonthOffset = Math.ceil(scale(dayClone.endOf('month')));

  return (
    Math.abs(originalDayOffset - startMonthOffset) <
      HALF_DAY_RESERVED_SPACE_PX ||
    Math.abs(originalDayOffset - endMonthOffset) < HALF_DAY_RESERVED_SPACE_PX
  );
};

const isDayCloseToOtherDay = (
  originalDay: Moment,
  otherDay: Moment,
  scale: ScaleTime<number, number>
): boolean => {
  return (
    Math.abs(Math.ceil(scale(originalDay)) - Math.ceil(scale(otherDay))) <
    DAY_RESERVED_SPACE_PX
  );
};

const isCloseToToday = (
  originalDay: Moment,
  today: Moment,
  scale: ScaleTime<number, number>
): boolean => {
  return (
    Math.abs(Math.ceil(scale(originalDay)) - Math.ceil(scale(today))) <
    (TODAY_TOOLTIP_WIDTH_PX + DAY_RESERVED_SPACE_PX) / 2
  );
};

export const GanttDatesMarks: React.FC<GanttDatesMarksProps> = React.memo(
  ({ groups, scale, today }) => {
    const allTimeMarks: Moment[] = _uniqBy(
      _flatten(
        groups.map(group =>
          _flatten(
            group.rows.map(row =>
              _flatten(
                row.entries.reduce((timeEntries: Moment[][], entry) => {
                  if (!entry.timeless) {
                    timeEntries.push([entry.startDate, entry.endDate]);
                  }
                  return timeEntries;
                }, [])
              )
            )
          )
        )
      ),
      date => date.valueOf()
    );
    const allTimeMarksSorted = _sortBy(allTimeMarks, mark =>
      mark.format('YYYY-MM-DD')
    );
    const dateNodes: React.ReactNode[] = [];
    const daysHidden: Moment[] = [];

    for (let i = 0; i < allTimeMarksSorted.length; i++) {
      const timeMark = allTimeMarksSorted[i];
      const lastNotHiddenDate = allTimeMarksSorted
        .slice(0, i)
        .reverse()
        .find(previousTimeMark => !daysHidden.includes(previousTimeMark));

      const isCloseToPreviousDate =
        lastNotHiddenDate &&
        isDayCloseToOtherDay(timeMark, lastNotHiddenDate, scale);

      if (
        !isDayCloseToMonthEdges(timeMark, scale) &&
        (today === null || !isCloseToToday(timeMark, today, scale)) &&
        !isCloseToPreviousDate
      ) {
        dateNodes.push(
          <div
            key={timeMark.valueOf()}
            className={styles.day}
            style={{
              left: scale(timeMark)
            }}
          >
            {timeMark.format('D')}
          </div>
        );
      } else {
        daysHidden.push(timeMark);
      }
    }

    return <>{dateNodes}</>;
  }
);
