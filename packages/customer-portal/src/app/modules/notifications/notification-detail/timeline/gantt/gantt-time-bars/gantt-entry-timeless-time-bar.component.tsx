import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { Popover } from 'antd';

import { TIME_ENTRY_SIZE_PX } from '../gantt.variables';
import { GanttEntryColor, GanttEntryShape } from '../gantt.type';

import { colorStyles, shapeStyles } from './gantt-time-bars-styles';
import styles from './gantt-time-bars.module.scss';

type GanttEntryTimelessTimeBarProps = {
  top: number;
  height: number;
  color: GanttEntryColor;
  shape: GanttEntryShape;
  tooltip?: React.ReactNode;
  label?: React.ReactNode;
  fixedCanvasRef: React.RefObject<HTMLElement>;
};

type GanttEntryTimelessTimeBarState = {
  leftOffset: number;
};

// internal template component
export class GanttEntryTimelessTimeBar extends React.PureComponent<
  GanttEntryTimelessTimeBarProps,
  GanttEntryTimelessTimeBarState
> {
  render() {
    const {
      height,
      top,
      tooltip,
      color,
      shape,
      label,
      fixedCanvasRef
    } = this.props;
    const topOffset = top + (height - TIME_ENTRY_SIZE_PX) / 2;

    const timeBar = (
      <div
        className={classNames(
          styles.singleEntryTimeBar,
          colorStyles[color],
          shapeStyles[shape]
        )}
        style={{
          top: topOffset
        }}
      />
    );

    // Timeless time bar should be "fixed" per canvas, which means that scrolling shouldn't affect them.
    // It means that it should be rendered outside of normal canvas - the simplest way to achieve this
    // is to use React Portal
    return ReactDOM.createPortal(
      <>
        {tooltip ? (
          <Popover content={tooltip} overlayClassName={styles.tooltipContainer}>
            {timeBar}
          </Popover>
        ) : (
          timeBar
        )}

        {label && (
          <div
            className={styles.timeBarLabel}
            style={{
              top,
              left: TIME_ENTRY_SIZE_PX * 1.5
            }}
          >
            {label}
          </div>
        )}
      </>,
      fixedCanvasRef.current!
    );
  }
}
