import React from 'react';
import classNames from 'classnames';
import { Moment } from 'moment';
import { ScaleTime } from 'd3-scale';

type GanttMonthBordersProps = {
  months: Moment[];
  scale: ScaleTime<number, number>;
  monthClassName: string;
  render?: (month: Moment) => React.ReactNode;
  lastMonthClassName: string;
};

export const GanttMonths: React.FC<GanttMonthBordersProps> = React.memo(
  ({ months, scale, monthClassName, lastMonthClassName, render }) => {
    const lastMonthIndex = months.length - 1;
    const isOneMonth = lastMonthIndex === 0;
    const monthBorders = months.map((month, i) => {
      const previousOffset = i && scale(months[i - 1].clone().endOf('month'));

      return (
        <div
          className={classNames(monthClassName, {
            [lastMonthClassName]: lastMonthIndex === i
          })}
          key={month.valueOf()}
          style={{
            left: previousOffset,
            width: isOneMonth
              ? scale.range()[1]
              : scale(month.clone().endOf('month')) - previousOffset
          }}
        >
          {render && render(month)}
        </div>
      );
    });

    return <>{monthBorders}</>;
  }
);
