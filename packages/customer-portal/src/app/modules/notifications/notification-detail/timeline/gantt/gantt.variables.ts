// we cannot use TypeScript ES6-Like import for this file
// tslint:disable-next-line
const stylesVariables = require('./gantt.variables.export.scss');

export const GROUP_MARGIN_PX = parseInt(stylesVariables.rowMargin, 10);
export const ROW_HEIGHT_PX = parseInt(stylesVariables.rowHeight, 10);
export const BORDER_SIZE_PX = parseInt(stylesVariables.borderSize, 10);
export const DAY_RESERVED_SPACE_PX = parseInt(
  stylesVariables.dayReservedSpace,
  10
);
export const TODAY_TOOLTIP_WIDTH_PX = parseInt(
  stylesVariables.todayTooltipWidth,
  10
);

export const TIME_ENTRY_SIZE_PX = parseInt(stylesVariables.timeEntrySize, 10);
