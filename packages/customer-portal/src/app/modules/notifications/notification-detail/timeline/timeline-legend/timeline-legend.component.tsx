import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './timeline-legend.module.scss';

export const TimelineLegend: React.FC = () => (
  <>
    <div className={styles.legendGroup}>
      <p className={styles.legendGroupCaption}>
        <FormattedMessage id="NOTIFICATION.TIMELINE.LEGEND_COLOR" />
      </p>
      <ul className={styles.legendList}>
        <li className={classNames(styles.legendItem, styles.approved)}>
          <FormattedMessage id="NOTIFICATION.TIMELINE.LEGEND_COLOR_APPROVED" />
        </li>
        <li className={classNames(styles.legendItem, styles.pending)}>
          <FormattedMessage id="NOTIFICATION.TIMELINE.LEGEND_COLOR_PENDING" />
        </li>
        <li className={classNames(styles.legendItem, styles.declined)}>
          <FormattedMessage id="NOTIFICATION.TIMELINE.LEGEND_COLOR_DECLINED" />
        </li>
        <li className={classNames(styles.legendItem, styles.cancelled)}>
          <FormattedMessage id="NOTIFICATION.TIMELINE.LEGEND_COLOR_CANCELLED" />
        </li>
      </ul>
    </div>
    <div className={styles.legendGroup}>
      <p className={styles.legendGroupCaption}>
        <FormattedMessage id="NOTIFICATION.TIMELINE.LEGEND_GRAPHIC_PATTERN" />
      </p>
      <ul className={styles.legendList}>
        <li className={classNames(styles.legendItem, styles.continuousTime)}>
          <FormattedMessage id="NOTIFICATION.CONTINUOUS_TIME" />
        </li>
        <li className={classNames(styles.legendItem, styles.intermittentTime)}>
          <FormattedMessage id="NOTIFICATION.INTERMITTENT_TIME" />
        </li>
        <li className={classNames(styles.legendItem, styles.reducedSchedule)}>
          <FormattedMessage id="NOTIFICATION.REDUCED_SCHEDULE" />
        </li>
      </ul>
    </div>
  </>
);
