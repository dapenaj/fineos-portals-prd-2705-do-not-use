import React from 'react';
import classNames from 'classnames';
import { ScaleTime } from 'd3-scale';
import { Moment } from 'moment';

import { GanttTodayTooltipPosition } from './gantt-today-tooltip-position';
import styles from './gantt-today.module.scss';

type GanttTodayProps = {
  scale: ScaleTime<number, number>;
  today: Moment;
  tooltipPosition: GanttTodayTooltipPosition;
};

export const GanttToday: React.FC<GanttTodayProps> = ({
  today,
  scale,
  tooltipPosition
}) => (
  <>
    <div
      className={styles.today}
      style={{
        width: scale(today)
      }}
    >
      <div
        className={classNames(styles.todayTooltip, {
          [styles.bottom]: tooltipPosition === GanttTodayTooltipPosition.BOTTOM,
          [styles.top]: tooltipPosition === GanttTodayTooltipPosition.TOP
        })}
      >
        {today.format('MMM D')}
      </div>
    </div>
  </>
);
