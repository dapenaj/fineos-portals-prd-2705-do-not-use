import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  AbsencePeriodDecision,
  Benefit,
  DisabilityBenefitSummary
} from 'fineos-js-api-client';
import moment, { Moment } from 'moment';

import { getPeriodDecisionStatus } from '../job-protected-leave';
import {
  DisabilityBenefitWrapper,
  IntermittentPeriodDecision,
  WageReplacementDisplayStatus,
  WageReplacementStatus,
  EnhancedAbsencePeriodDecision,
  BenefitRightCategory
} from '../../../../shared/types';
import { getWageReplacementStatus } from '../wage-replacement';
import { formatDateForDisplay } from '../../../../shared/utils';

import { GanttEntryColor, GanttEntryNormal, GanttEntryShape } from './gantt';

const CONTINUOUS_TIME = GanttEntryShape.FILLED;
const INTERMITTENT_TIME = GanttEntryShape.STRIPES;
const REDUCED_TIME = GanttEntryShape.SEMI_FILLED;

// range would be presented as [start, end + 1]
export const timelineGanttEntryRange = (
  startDate: string | Moment,
  endDate?: string | Moment
): Pick<GanttEntryNormal, 'startDate' | 'endDate'> => {
  const startDateMapped = moment(startDate);
  const endDateMapped = endDate ? moment(endDate) : startDateMapped;

  return {
    startDate: startDateMapped,
    endDate: endDateMapped.clone().add(1, 'day')
  };
};

export const absencePeriodTypeToShape = (
  absencePeriodType: string
): GanttEntryShape => {
  switch (absencePeriodType) {
    case 'Time off period':
      return CONTINUOUS_TIME;
    case 'Episodic':
      return INTERMITTENT_TIME;
    default:
      return REDUCED_TIME;
  }
};

export const absenceToColor = (
  absence: AbsencePeriodDecision
): GanttEntryColor | null => {
  switch (getPeriodDecisionStatus(absence)) {
    case 'approved':
      return GanttEntryColor.GREEN;
    case 'pending':
      return GanttEntryColor.YELLOW;
    case 'declined':
      return GanttEntryColor.RED;
    case 'cancelled':
      return GanttEntryColor.GREY;
  }
  // not identified
  return null;
};

export const wageReplacementDisplayStatusToColor = (
  wageReplacementDisplayStatus: WageReplacementDisplayStatus
): GanttEntryColor => {
  switch (wageReplacementDisplayStatus) {
    case 'Approved':
      return GanttEntryColor.GREEN;
    case 'Denied':
      return GanttEntryColor.RED;
    case 'Pending':
      return GanttEntryColor.YELLOW;
    case 'Decided':
    case 'Closed':
      return GanttEntryColor.GREY;
  }
};

export const accommodationStatusToColor = (
  accommodationStatus: string
): GanttEntryColor | null => {
  switch (accommodationStatus) {
    case 'Decided':
      return GanttEntryColor.GREEN;
    case 'Registration':
    case 'Assessment':
    case 'Monitoring':
      return GanttEntryColor.YELLOW;
    case 'Closed':
      return GanttEntryColor.GREY;
  }

  return null;
};

const getStatusPrefix = (statusCategory: string) => {
  switch (statusCategory) {
    case 'approved':
      return 'APPROVED';
    case 'declined':
      return 'DECLINED';
    default:
      return 'REQUESTED';
  }
};

export const jobProtectedLeaveAbsenceToTooltip = (
  absence: EnhancedAbsencePeriodDecision
): React.ReactNode => {
  const params: any = {
    startDate: formatDateForDisplay(absence.startDate),
    endDate: formatDateForDisplay(absence.endDate)
  };
  const absenceStatusPrefix = getStatusPrefix(absence.statusCategory);
  let absenceTypePostfix = '';

  switch (absence.absencePeriodType) {
    case 'Episodic':
      const {
        episodicLeavePeriodDetail
      } = absence as IntermittentPeriodDecision;
      absenceTypePostfix = 'INTERMITTENT';

      if (episodicLeavePeriodDetail && episodicLeavePeriodDetail.duration) {
        params.requestedTimeAmount = episodicLeavePeriodDetail.duration;
        params.requestedTimeUnit = episodicLeavePeriodDetail.durationBasis;
        params.requestedTimeDuration =
          episodicLeavePeriodDetail.frequencyInterval;
        params.requestedTimeDurationUnit =
          episodicLeavePeriodDetail.frequencyIntervalBasis;
      } else {
        absenceTypePostfix += '_UNSPECIFIED';
      }
      break;
    case 'Time off period':
      absenceTypePostfix = 'CONTINUOUS';
      break;
    case 'Reduced Schedule':
      absenceTypePostfix = 'REDUCED';
      break;
  }

  return (
    <FormattedMessage
      id={`NOTIFICATION.TIMELINE.JOB_PROTECTED_LEAVE_ABSENCE_TOOLTIP.${absenceStatusPrefix}_${absenceTypePostfix}`}
      values={params}
    />
  );
};

export const getBenefitTooltip = (
  wageReplacementDisplayStatus: WageReplacementDisplayStatus,
  disabilityBenefitSummary: DisabilityBenefitSummary
): React.ReactNode => {
  const { disabilityBenefit } = disabilityBenefitSummary;
  switch (wageReplacementDisplayStatus) {
    case 'Approved':
      const showWithDates =
        !!disabilityBenefit.benefitStartDate &&
        !!disabilityBenefit.benefitEndDate;
      if (!showWithDates) {
        return (
          <FormattedMessage id="NOTIFICATION.TIMELINE.WAGE_REPLACEMENT_TOOLTIP.APPROVED_WITHOUT_DATE" />
        );
      }
      return (
        <FormattedMessage
          id="NOTIFICATION.TIMELINE.WAGE_REPLACEMENT_TOOLTIP.APPROVED_NOT_FROM_CLAIMS"
          values={{
            startDate: formatDateForDisplay(disabilityBenefit.benefitStartDate),
            endDate: formatDateForDisplay(disabilityBenefit.benefitEndDate)
          }}
        />
      );
    case 'Decided':
    case 'Closed':
      return (
        <FormattedMessage
          id={`NOTIFICATION.TIMELINE.WAGE_REPLACEMENT_TOOLTIP.${String(
            wageReplacementDisplayStatus
          ).toLocaleUpperCase()}`}
        />
      );
    default:
      return null;
  }
};

type BenefitOptions = Pick<
  GanttEntryNormal,
  'startDate' | 'endDate' | 'color' | 'tooltip'
>;

export const getBenefitOptions = (
  benefit: Benefit,
  disabilityBenefitWrapper: DisabilityBenefitWrapper | undefined
): BenefitOptions | null => {
  switch (benefit.benefitRightCategory) {
    case BenefitRightCategory.RECURRING_BENEFIT:
      if (disabilityBenefitWrapper) {
        const disabilityBenefitSummary =
          disabilityBenefitWrapper.disabilityBenefitSummary;
        const wageReplacementDisplayStatus = getWageReplacementStatus(
          benefit.status as WageReplacementStatus,
          benefit.stageName
        );
        return {
          ...timelineGanttEntryRange(
            disabilityBenefitSummary.disabilityBenefit.benefitStartDate ||
              disabilityBenefitSummary.disabilityBenefit.benefitEndDate,
            disabilityBenefitSummary.disabilityBenefit.benefitEndDate ||
              disabilityBenefitSummary.disabilityBenefit.benefitStartDate
          ),
          color: wageReplacementDisplayStatusToColor(
            wageReplacementDisplayStatus
          ),
          tooltip: getBenefitTooltip(
            wageReplacementDisplayStatus,
            disabilityBenefitSummary
          )
        };
      }
      break;
    case BenefitRightCategory.LUMP_SUM_BENEFIT:
      return {
        ...timelineGanttEntryRange(benefit.benefitIncurredDateMoment),
        color: wageReplacementDisplayStatusToColor(
          getWageReplacementStatus(
            benefit.status as WageReplacementStatus,
            benefit.stageName
          )
        )
      };
  }

  return null;
};
