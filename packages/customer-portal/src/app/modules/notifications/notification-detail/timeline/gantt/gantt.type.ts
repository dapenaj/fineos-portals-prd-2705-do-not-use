import React from 'react';
import { Moment } from 'moment';

export enum GanttEntryShape {
  FILLED,
  STRIPES,
  SEMI_FILLED
}

export enum GanttEntryColor {
  GREEN,
  YELLOW,
  RED,
  GREY
}

type GanttEntryBase = {
  id: any;
  color: GanttEntryColor;
  shape: GanttEntryShape;
  tooltip?: React.ReactNode;
};

export type GanttEntryTimeless = {
  timeless: true;
  label?: React.ReactNode;
} & GanttEntryBase;

export type GanttEntryNormal = {
  timeless?: false;
  startDate: Moment;
  endDate: Moment;
} & GanttEntryBase;

export type GanttEntry = GanttEntryTimeless | GanttEntryNormal;

export type GanttGroupRow = {
  id: any;
  title: React.ReactNode;
  entries: GanttEntry[];
  disabled?: boolean;
};

export type GanttGroup = {
  id: any;
  title: React.ReactNode;
  rows: GanttGroupRow[];
};

export type RowBox = {
  height: number;
  top: number;
};

export type HeightBoxesRegistry = {
  mainRowBox: RowBox;
  subRowBoxes: RowBox[];
}[];
