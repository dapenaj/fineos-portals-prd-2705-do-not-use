import React from 'react';
import {
  flatten as _flatten,
  groupBy as _groupBy,
  isEmpty as _isEmpty
} from 'lodash';
import { FormattedMessage } from 'react-intl';
import {
  AbsencePeriodDecision,
  AbsencePeriodDecisions,
  NotificationAccommodationSummary,
  NotificationCaseSummary,
  WorkPlaceAccommodationDetail
} from 'fineos-js-api-client';

import { AuthorizationService } from '../../../../shared/services/authorization';
import {
  AbsencePaidLeave,
  AbsenceWithHandler,
  ClaimBenefit,
  DisabilityBenefitWrapper,
  IntermittentPeriodDecision,
  WorkplaceAccommodationDecision
} from '../../../../shared/types';
import { getWorkplaceAccommodationDecision } from '../workplace-accommodations';
import {
  extractIntermittentPeriodsFromPeriodDecisions,
  processLeavePlanPeriodDecisions
} from '../job-protected-leave';

import {
  GanttEntry,
  GanttEntryShape,
  GanttGroup,
  GanttGroupRow
} from './gantt';
import { TimelineView } from './timeline.view';
import {
  absencePeriodTypeToShape,
  absenceToColor,
  accommodationStatusToColor,
  getBenefitOptions,
  jobProtectedLeaveAbsenceToTooltip,
  timelineGanttEntryRange
} from './timeline.util';

type TimelineContainerProps = {
  notification: NotificationCaseSummary;
  claimBenefits: ClaimBenefit[] | null;
  periodDecisions: AbsencePeriodDecisions[] | null;
  absencesWithHandlers: AbsenceWithHandler[] | null;
  absencePaidLeaves: AbsencePaidLeave[] | null;
  disabilityBenefitWrappers: DisabilityBenefitWrapper[] | null;
};

export class TimelineContainer extends React.Component<TimelineContainerProps> {
  readonly isCustomer: boolean;

  constructor(props: TimelineContainerProps) {
    super(props);

    const authorizationService = AuthorizationService.getInstance();

    this.isCustomer =
      authorizationService.isClaimsUser && !authorizationService.isAbsenceUser;
  }

  buildWageReplacement(): GanttGroup[] {
    const {
      claimBenefits,
      disabilityBenefitWrappers,
      absencePaidLeaves
    } = this.props;

    if (!_isEmpty(claimBenefits) || !_isEmpty(absencePaidLeaves)) {
      const claimBenefitsAbsencePaidLeave = absencePaidLeaves
        ? _flatten(
            absencePaidLeaves.map(
              absencePaidLeave => absencePaidLeave.claimBenefits
            )
          )
        : [];
      const benefits: ClaimBenefit[] | null = [
        ...claimBenefitsAbsencePaidLeave
      ];

      if (benefits) {
        if (claimBenefits) {
          benefits.push(...claimBenefits);
        }
        const rows: GanttGroupRow[] = [];
        if (!!disabilityBenefitWrappers && disabilityBenefitWrappers.length) {
          for (const claimBenefit of benefits) {
            for (const benefit of claimBenefit.benefits) {
              const disabilityBenefitWrapper = disabilityBenefitWrappers.find(
                wrapper => wrapper.benefitId === benefit.benefitId
              );
              const benefitOptions = getBenefitOptions(
                benefit,
                disabilityBenefitWrapper
              );
              if (benefitOptions) {
                const timeless =
                  !!disabilityBenefitWrapper &&
                  disabilityBenefitWrapper.disabilityBenefitSummary
                    .disabilityBenefit &&
                  (!disabilityBenefitWrapper.disabilityBenefitSummary
                    .disabilityBenefit.benefitStartDate ||
                    !disabilityBenefitWrapper.disabilityBenefitSummary
                      .disabilityBenefit.benefitEndDate);
                rows.push({
                  id: 'wage-replacement-disability-' + benefit.benefitCaseType,
                  title: benefit.benefitCaseType,
                  entries: [
                    {
                      id: 'benefit-' + benefit.benefitId,
                      ...benefitOptions,
                      timeless: timeless,
                      shape: GanttEntryShape.FILLED
                    } as GanttEntry
                  ],
                  disabled: timeless
                });
              }
            }
          }
        }
        if (rows.length) {
          return [
            {
              id: 'wage',
              title: <FormattedMessage id="NOTIFICATION.WAGE_REPLACEMENT" />,
              rows: rows
            }
          ];
        }
      }
    }

    return [];
  }

  buildJobProtectedLeaves(): GanttGroup[] {
    const { absencesWithHandlers, periodDecisions } = this.props;
    if (!_isEmpty(absencesWithHandlers) && !_isEmpty(periodDecisions)) {
      // For each absence child case, create a Job Protected Leave heading
      // (the expectation is that each Notification case will have one Absence case,
      // but if more than one is found then multiple Job Protected Leave headings will be displayed)
      return periodDecisions!.reduce(
        (leaves: GanttGroup[], { absencePeriodDecisions }, index) => {
          const absencesWithHandler = absencesWithHandlers![index];

          // when we extract the intermittent periods, we also remove the child period decisions (that we don't want rendered)
          const {
            intermittentPeriodDecisions,
            jobProtectedPeriodDecisions
          } = extractIntermittentPeriodsFromPeriodDecisions(
            absencePeriodDecisions,
            absencesWithHandler
          );

          const filteredAbsencePeriodDecisions: (
            | AbsencePeriodDecision
            | IntermittentPeriodDecision
          )[] = [
            ...jobProtectedPeriodDecisions,
            ...intermittentPeriodDecisions
          ];

          const absencePeriodDecisionsWithNonEmptyLeavePlanName = filteredAbsencePeriodDecisions.filter(
            periodDecision => !!periodDecision.leavePlanName
          );

          const groupedByLeavePlan = _groupBy(
            absencePeriodDecisionsWithNonEmptyLeavePlanName,
            absencePeriodDecision => absencePeriodDecision.leavePlanName
          );
          const rows: GanttGroupRow[] = [];

          for (const leavePlanName of Object.keys(groupedByLeavePlan)) {
            const absencesByLeavePlan = processLeavePlanPeriodDecisions(
              groupedByLeavePlan[leavePlanName]
            );

            if (!_isEmpty(absencesByLeavePlan)) {
              rows.push({
                id: `${absencesWithHandler.absence.absenceId}-${leavePlanName}`,
                title: leavePlanName,
                entries: absencesByLeavePlan.reduce(
                  (entries: GanttEntry[], absenceByLeavePlan) => {
                    const color = absenceToColor(absenceByLeavePlan);

                    if (color !== null) {
                      entries.push({
                        id: `${absenceByLeavePlan.leaveRequestId}-${absenceByLeavePlan.startDate}`,
                        ...timelineGanttEntryRange(
                          absenceByLeavePlan.startDate,
                          absenceByLeavePlan.endDate
                        ),
                        color,
                        shape: absencePeriodTypeToShape(
                          absenceByLeavePlan.absencePeriodType
                        ),
                        tooltip: jobProtectedLeaveAbsenceToTooltip(
                          absenceByLeavePlan
                        )
                      });
                    }
                    return entries;
                  },
                  []
                )
              });
            }
          }

          if (rows.length) {
            leaves.push({
              id:
                'job-protected-leave-' + absencesWithHandler.absence.absenceId,
              title: <FormattedMessage id="NOTIFICATION.JOB_PROTECTED_LEAVE" />,
              rows
            });
          }

          return leaves;
        },
        []
      );
    }

    return [];
  }

  buildTemporaryAccommodation = (): GanttGroup[] => {
    const {
      notification: { accommodations }
    } = this.props;

    if (!_isEmpty(accommodations)) {
      const isAccommodated = accommodations!.some(
        (accommodation: NotificationAccommodationSummary) =>
          getWorkplaceAccommodationDecision(
            accommodation.accommodationDecision as WorkplaceAccommodationDecision
          ) === 'Accommodated'
      );

      if (isAccommodated) {
        const accommodationRows: GanttGroupRow[] = [];
        const groupedAccommodations = _groupBy(
          _flatten(
            accommodations!.map(accommodation =>
              accommodation.workplaceAccommodations.map(
                (workplaceAccommodation: WorkPlaceAccommodationDetail) => ({
                  ...workplaceAccommodation,
                  status: accommodation.status
                })
              )
            )
          ),
          workplaceAccommodation =>
            `${workplaceAccommodation.accommodationCategory} | ${workplaceAccommodation.accommodationType}`
        );

        for (const groupName of Object.keys(groupedAccommodations)) {
          accommodationRows.push({
            id: 'accommodation-' + groupName,
            title: groupName,
            entries: groupedAccommodations[groupName].reduce(
              (entries: GanttEntry[], workplaceAccommodation) => {
                const color = accommodationStatusToColor(
                  workplaceAccommodation.status
                );

                if (
                  color &&
                  workplaceAccommodation.implementedDate &&
                  workplaceAccommodation.accommodationEndDate
                ) {
                  entries.push({
                    id: 'accommodation-entry-' + workplaceAccommodation.id,
                    ...timelineGanttEntryRange(
                      workplaceAccommodation.implementedDate,
                      workplaceAccommodation.accommodationEndDate
                    ),
                    color,
                    shape: GanttEntryShape.FILLED,
                    tooltip: (
                      <FormattedMessage
                        id="NOTIFICATION.TIMELINE.TEMPORARY_ACCOMMODATION_TOOLTIP"
                        values={{
                          startDate: workplaceAccommodation.implementedDate,
                          endDate: workplaceAccommodation.accommodationEndDate
                        }}
                      />
                    )
                  });
                }
                return entries;
              },
              []
            )
          });
        }

        return [
          {
            id: 'accommodation',
            title: (
              <FormattedMessage id="NOTIFICATION.TEMPORARY_ACCOMMODATION" />
            ),
            rows: accommodationRows
          }
        ];
      }
    }

    return [];
  };

  render() {
    const ganttGroups: GanttGroup[] = this.isCustomer
      ? this.buildWageReplacement()
      : [
          ...this.buildJobProtectedLeaves(),
          ...this.buildWageReplacement(),
          ...this.buildTemporaryAccommodation()
        ];
    // remove empty groups/rows
    const filledGanttGroups = ganttGroups.reduce(
      (filteredGanttGroups: GanttGroup[], potentialGanttGroup) => {
        const cleanedGanttGroupRows: GanttGroupRow[] = potentialGanttGroup.rows.filter(
          row => !_isEmpty(row.entries)
        );

        if (!_isEmpty(cleanedGanttGroupRows)) {
          filteredGanttGroups.push({
            ...potentialGanttGroup,
            rows: cleanedGanttGroupRows
          });
        }

        return filteredGanttGroups;
      },
      []
    );

    return filledGanttGroups.length ? (
      <TimelineView ganttGroups={filledGanttGroups} />
    ) : null;
  }
}
