import { GanttEntryColor, GanttEntryShape } from '../gantt.type';

import styles from './gantt-time-bars.module.scss';

export const colorStyles = {
  [GanttEntryColor.RED]: styles.redColor,
  [GanttEntryColor.YELLOW]: styles.yellowColor,
  [GanttEntryColor.GREEN]: styles.greenColor,
  [GanttEntryColor.GREY]: styles.greyColor
};

export const shapeStyles = {
  [GanttEntryShape.FILLED]: styles.filledShape,
  [GanttEntryShape.STRIPES]: styles.stripesShape,
  [GanttEntryShape.SEMI_FILLED]: styles.semiFilledShape
};
