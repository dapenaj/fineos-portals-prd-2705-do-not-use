import React from 'react';

import { HeightBoxesRegistry } from './gantt.type';
import styles from './gantt.module.scss';

type GanttGroupBordersProps = {
  heightBoxesRegistry: HeightBoxesRegistry;
  width: number;
};

export const GanttGroupBorders: React.FC<GanttGroupBordersProps> = React.memo(
  ({ heightBoxesRegistry, width }: GanttGroupBordersProps) => {
    const groupBorders = [];

    for (let i = 0; i < heightBoxesRegistry.length - 1; i++) {
      const groupHeightBox = heightBoxesRegistry[i];
      groupBorders.push(
        <div
          key={i}
          className={styles.gridGroupBorder}
          style={{
            top: groupHeightBox.mainRowBox.top,
            height:
              groupHeightBox.mainRowBox.height +
              groupHeightBox.subRowBoxes.reduce(
                (subRowsHeight, subRowBox) => subRowsHeight + subRowBox.height,
                0
              ),
            width
          }}
        />
      );
    }

    return <>{groupBorders}</>;
  }
);
