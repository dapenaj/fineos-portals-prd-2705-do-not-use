import React from 'react';
import moment, { Moment } from 'moment';
import classNames from 'classnames';
import { Button, Icon } from 'antd';
import { debounce as _debounce, flatten as _flatten } from 'lodash';
import { ScaleTime, scaleTime } from 'd3-scale';

import {
  GanttEntry,
  GanttEntryNormal,
  GanttGroup,
  HeightBoxesRegistry,
  RowBox
} from './gantt.type';
import { GanttGroupBorders } from './gantt-group-borders.component';
import { GanttToday, GanttTodayTooltipPosition } from './gantt-today';
import { GanttMonths } from './gantt-month-borders.component';
import { GanttTimeBars } from './gantt-time-bars/gantt-time-bars.component';
import { GanttDatesMarks } from './gantt-dates-mark/gantt-dates-marks.component';
import styles from './gantt.module.scss';
import {
  BORDER_SIZE_PX,
  GROUP_MARGIN_PX,
  ROW_HEIGHT_PX
} from './gantt.variables';

type GanttComponentProps = {
  title: React.ReactNode;
  groups: GanttGroup[];
};

type GroupsExpanded = {
  [index: string]: boolean;
};

type GroupsRefs = {
  mainRow: React.RefObject<HTMLDivElement>;
  subRows: React.RefObject<HTMLDivElement>[];
}[];

type GanttComponentState = {
  groupsExpanded: GroupsExpanded;
  heightBoxesRegistry: HeightBoxesRegistry;
  canvasWidth: number;
  canvasHeight: number;
  leftQuickLink: Moment | null;
  rightQuickLink: Moment | null;
};

const SIZE_NOT_SET = -1;

const RESERVED_DAYS_OFFSET = 1;

const MAX_VISIBLE_COLUMNS_COUNT = 3;

function generateMonths(startDate: Moment, endDate: Moment): Moment[] {
  const months: Moment[] = [];
  const reservedStartDate = startDate
    .clone()
    .subtract(RESERVED_DAYS_OFFSET, 'day');
  const reservedEndDate = endDate.clone().add(RESERVED_DAYS_OFFSET, 'day');
  let currMonth = reservedStartDate.startOf('month');

  while (currMonth.isBefore(reservedEndDate)) {
    months.push(currMonth);
    currMonth = currMonth.clone().add(1, 'month');
  }

  return months;
}

export class Gantt extends React.PureComponent<
  GanttComponentProps,
  GanttComponentState
> {
  // for performance reason avoid immediate resizing, it's should be debounced with maxWait
  handleScreenResize = _debounce(() => this.resize(), 100, {
    // at least 1 time per 0.5 second resize should be called
    maxWait: 500
  });

  // assume that we show fixed today
  // it means that it's not covering the case when someone left computer
  // working for 24h and see yesterday as today
  // if it's really would be needed today should be moved to component state
  readonly today: Moment;
  readonly months: Moment[];

  readonly canvasContainerRef = React.createRef<HTMLDivElement>();
  readonly fixedCanvasContainerRef = React.createRef<HTMLDivElement>();
  readonly firstDate: Moment | null;
  readonly lastDate: Moment | null;

  readonly groupsRefs: GroupsRefs;

  private timeScale: ScaleTime<number, number> | null = null;

  constructor(props: GanttComponentProps) {
    super(props);

    const today = moment();
    const entries: GanttEntry[] = _flatten(
      props.groups.map(group => _flatten(group.rows.map(row => row.entries)))
    );
    const anyNormalEntry = entries.find(entry => !entry.timeless) as
      | GanttEntryNormal
      | undefined;
    const anyDate = anyNormalEntry ? anyNormalEntry.startDate : null;

    const [firstDay, lastDay] = anyDate
      ? entries.reduce(
          ([earliestDay, latestDay]: [Moment, Moment], entry) => {
            if (entry.timeless) {
              return [earliestDay, latestDay];
            } else {
              return [
                entry.startDate.isBefore(earliestDay)
                  ? entry.startDate
                  : earliestDay,
                entry.endDate.isAfter(latestDay) ? entry.endDate : latestDay
              ];
            }
          },
          [anyDate, anyDate]
        )
      : [null, null];

    this.today = today;
    this.groupsRefs = props.groups.map(group => ({
      mainRow: React.createRef(),
      subRows: group.rows.map(() => React.createRef())
    }));
    this.state = {
      groupsExpanded: props.groups.reduce(
        (groupsExpanded: GroupsExpanded, group) => {
          groupsExpanded[group.id] = false;
          return groupsExpanded;
        },
        {}
      ),
      heightBoxesRegistry: props.groups.map((group, i) => ({
        mainRowBox: {
          top: i * ROW_HEIGHT_PX,
          height: ROW_HEIGHT_PX
        },
        subRowBoxes: group.rows.map(row => ({
          top: 0,
          height: 0
        }))
      })),
      canvasWidth: SIZE_NOT_SET,
      canvasHeight: SIZE_NOT_SET,
      leftQuickLink: null,
      rightQuickLink: null
    };

    this.firstDate = firstDay;
    this.lastDate = lastDay;

    this.months = firstDay
      ? // lastDay always would be set if firstDay exists
        generateMonths(moment.min(firstDay, today), moment.max(lastDay!, today))
      : [this.today.clone().startOf('month')];
  }

  componentDidMount() {
    // now we can run actual chart rendering as we can reading information about layout from DOM
    this.resize(true);

    window.addEventListener('resize', this.handleScreenResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleScreenResize);
  }

  handleScroll = () => {
    this.resetQuickLinks();
  };

  resetQuickLinks = () => {
    if (
      this.firstDate &&
      this.lastDate &&
      this.canvasContainerRef.current &&
      this.timeScale
    ) {
      const { current: el } = this.canvasContainerRef;
      const scrollLeft = el.scrollLeft;
      const { canvasWidth } = this.state;
      const firstDayOffset = this.timeScale(this.firstDate);
      const lastDayOffset = this.timeScale(this.lastDate);

      // even taking in account that this is PureComponent, better to skip changing state here
      // if possible, cause this method mostly called as onScroll
      if (scrollLeft > firstDayOffset) {
        if (!this.state.leftQuickLink) {
          this.setState({
            leftQuickLink: this.firstDate
          });
        }
      } else if (this.state.leftQuickLink) {
        this.setState({
          leftQuickLink: null
        });
      }

      if (scrollLeft + canvasWidth < lastDayOffset) {
        if (!this.state.rightQuickLink) {
          this.setState({
            rightQuickLink: this.lastDate
          });
        }
      } else if (this.state.rightQuickLink) {
        this.setState({
          rightQuickLink: null
        });
      }
    }
  };

  resize = (isInitial = false) => {
    const canvasContainerEl = this.canvasContainerRef.current!;
    const canvasWidth = canvasContainerEl.offsetWidth;
    const canvasHeight = canvasContainerEl.offsetHeight;

    if (isInitial) {
      this.calculateRowHeights();
    }

    this.setState({ canvasWidth, canvasHeight }, () => {
      const todayOffset = this.timeScale!(this.today);

      if (isInitial && todayOffset > canvasWidth) {
        // recenter scroll so today, always would be shown
        canvasContainerEl.scrollLeft = this.timeScale!(
          this.today.clone().startOf('month')
        );
      }

      if (this.months.length > MAX_VISIBLE_COLUMNS_COUNT) {
        this.resetQuickLinks();
      }
    });
  };

  toggleGroupExpand = (groupId: any) => {
    const { groupsExpanded } = this.state;
    this.setState(
      {
        groupsExpanded: {
          ...this.state.groupsExpanded,
          [groupId]: !groupsExpanded[groupId]
        }
      },
      this.calculateRowHeights
    );
  };

  calculateRowHeights = () => {
    const { groups } = this.props;
    const { groupsExpanded, heightBoxesRegistry } = this.state;
    const recalculatedHeightBoxesRegistry = [...heightBoxesRegistry];
    let topOffset = 0;

    for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      const group = groups[groupIndex];
      const isExpanded = groupsExpanded[group.id];
      const groupRefs = this.groupsRefs[groupIndex];
      const mainRowHeight = groupRefs.mainRow.current!.offsetHeight;
      const mainRowTop = topOffset;
      const subRowBoxes: RowBox[] = [];

      topOffset += GROUP_MARGIN_PX + mainRowHeight;

      if (isExpanded) {
        for (let rowIndex = 0; rowIndex < group.rows.length; rowIndex++) {
          const rowRef = groupRefs.subRows[rowIndex];
          const rowHeight = rowRef.current!.offsetHeight;

          subRowBoxes.push({
            top: topOffset,
            height: rowHeight
          });

          topOffset += rowHeight;
        }
      }

      recalculatedHeightBoxesRegistry[groupIndex] = {
        mainRowBox: {
          top: mainRowTop,
          height: mainRowHeight + GROUP_MARGIN_PX * 2 + BORDER_SIZE_PX
        },
        subRowBoxes
      };

      topOffset += GROUP_MARGIN_PX + BORDER_SIZE_PX;
    }

    this.setState({
      heightBoxesRegistry: recalculatedHeightBoxesRegistry
    });
  };

  goToLeftLink = () => {
    const firstDayOffset = this.timeScale!(this.firstDate!);
    this.scrollTo(firstDayOffset - this.timeScale!(this.months[1]));
  };

  goToRightLink = () => {
    const lastDayOffset = this.timeScale!(this.lastDate!);
    this.scrollTo(lastDayOffset - this.timeScale!(this.months[1]));
  };

  scrollTo = (leftOffset: number) => {
    const el = this.canvasContainerRef.current!;
    if (typeof el.scrollTo === 'function') {
      el.scrollTo({
        left: leftOffset,
        behavior: 'smooth'
      });
    } else {
      el.scrollLeft = leftOffset;
    }
  };

  buildScaledDomainRange = (): Moment[] => {
    const requiredFirstDate = this.firstDate
      ? moment.min(this.firstDate, this.today).clone()
      : this.today;
    const requiredLastDate = this.lastDate
      ? moment.max(this.lastDate, this.today).clone()
      : this.today;
    const reservedFirstDate = requiredFirstDate.clone().subtract(1, 'day');
    const reservedLastDate = requiredLastDate.clone().add(1, 'day');

    return reservedFirstDate.isSame(reservedLastDate, 'month')
      ? [reservedFirstDate, reservedLastDate]
      : [reservedFirstDate.startOf('month'), reservedLastDate.endOf('month')];
  };

  buildTimeScale = (months: Moment[], canvasWidth: number) => {
    const lastVisibleColumnIndex =
      Math.min(MAX_VISIBLE_COLUMNS_COUNT, months.length) - 1;
    const domainRange: Moment[] =
      lastVisibleColumnIndex === 0
        ? // if all dates within 1 month, scaled domain required
          this.buildScaledDomainRange()
        : [months[0], months[lastVisibleColumnIndex].clone().endOf('month')];

    return scaleTime()
      .domain(domainRange)
      .range([0, canvasWidth]);
  };

  render() {
    const { title, groups } = this.props;
    const {
      groupsExpanded,
      heightBoxesRegistry,
      canvasWidth,
      leftQuickLink,
      rightQuickLink
    } = this.state;
    const months = this.months;
    const isCanvasAvailable = canvasWidth !== SIZE_NOT_SET;
    const timeScale = this.buildTimeScale(months, canvasWidth);
    const wholeCanvasWidth = timeScale(
      months[months.length - 1].clone().endOf('month')
    );
    const isCanvasExtended = months.length > MAX_VISIBLE_COLUMNS_COUNT;
    const today = this.today;

    // allow to use it in events handlers and lifecycle hooks
    this.timeScale = timeScale;

    return (
      <div className={styles.container}>
        <div className={styles.groupsColumn}>
          <div
            className={classNames(
              styles.groupsColumnCell,
              styles.groupsColumnCellRow
            )}
          >
            {title}
          </div>

          {groups.map((group, groupIndex) => (
            <div className={styles.groupsColumnCell} key={group.id}>
              <div
                className={styles.groupsColumnCellRow}
                ref={this.groupsRefs[groupIndex].mainRow}
              >
                <Button
                  type="link"
                  onClick={() => this.toggleGroupExpand(group.id)}
                >
                  {group.title}{' '}
                  {groupsExpanded[group.id] ? (
                    <Icon type="up" />
                  ) : (
                    <Icon type="down" />
                  )}
                </Button>
              </div>
              {groupsExpanded[group.id] &&
                group.rows.map((row, rowIndex) => (
                  <div
                    key={`${row.id}-${rowIndex}`}
                    ref={this.groupsRefs[groupIndex].subRows[rowIndex]}
                    className={classNames(
                      styles.groupsColumnCellRow,
                      styles.groupsColumnCellSubRow,
                      row.disabled && styles.disabled
                    )}
                  >
                    {row.title}
                  </div>
                ))}
            </div>
          ))}
        </div>

        {isCanvasAvailable && leftQuickLink && (
          <Button
            type="link"
            className={classNames(styles.quickLink, styles.left)}
            onClick={this.goToLeftLink}
          >
            to {leftQuickLink.format('MMM D, YYYY')}
          </Button>
        )}

        {isCanvasAvailable && rightQuickLink && (
          <Button
            type="link"
            className={classNames(styles.quickLink, styles.right)}
            onClick={this.goToRightLink}
          >
            to {rightQuickLink.format('MMM D, YYYY')}
          </Button>
        )}

        <div
          className={classNames(styles.canvasContainer, {
            [styles.extended]: isCanvasExtended
          })}
          ref={this.canvasContainerRef}
          onScroll={isCanvasExtended ? this.handleScroll : undefined}
        >
          <div
            className={styles.canvasLayer}
            style={{
              width: wholeCanvasWidth
            }}
          >
            <div className={styles.timeScaleRow}>
              {isCanvasAvailable && (
                <GanttMonths
                  months={months}
                  scale={timeScale}
                  monthClassName={styles.timeScaleRowCell}
                  lastMonthClassName={styles.lastTimeScaleRowCell}
                  render={month => month.format('MMMM YYYY')}
                />
              )}
            </div>

            <div className={styles.timeScaleRow}>
              {/* this is only for grid */}
              {isCanvasAvailable && (
                <GanttMonths
                  months={months}
                  scale={timeScale}
                  monthClassName={styles.timeScaleRowCell}
                  lastMonthClassName={styles.lastTimeScaleRowCell}
                />
              )}

              {isCanvasAvailable && (
                <GanttDatesMarks
                  groups={groups}
                  today={isCanvasExtended ? today : null}
                  scale={timeScale}
                />
              )}
            </div>

            <div className={styles.timeLineCanvas}>
              {isCanvasAvailable && (
                <GanttToday
                  today={today}
                  scale={timeScale}
                  tooltipPosition={
                    isCanvasExtended
                      ? GanttTodayTooltipPosition.TOP
                      : GanttTodayTooltipPosition.BOTTOM
                  }
                />
              )}

              {isCanvasAvailable && (
                <GanttGroupBorders
                  heightBoxesRegistry={heightBoxesRegistry}
                  width={wholeCanvasWidth}
                />
              )}

              {isCanvasAvailable && (
                <GanttMonths
                  months={months}
                  scale={timeScale}
                  monthClassName={styles.gridMonthBorder}
                  lastMonthClassName={styles.lastGridMonthBorder}
                />
              )}

              {isCanvasAvailable && (
                <GanttTimeBars
                  groups={groups}
                  heightBoxesRegistry={heightBoxesRegistry}
                  scale={timeScale}
                  fixedCanvasRef={this.fixedCanvasContainerRef}
                />
              )}
            </div>
            <div
              className={styles.fixedCanvas}
              ref={this.fixedCanvasContainerRef}
            />
          </div>
        </div>
      </div>
    );
  }
}
