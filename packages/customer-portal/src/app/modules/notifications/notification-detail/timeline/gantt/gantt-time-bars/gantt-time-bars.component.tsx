import React from 'react';
import { ScaleTime } from 'd3-scale';
import { flatten as _flatten, sortBy as _sortBy } from 'lodash';

import { GanttEntry, GanttGroup, HeightBoxesRegistry } from '../gantt.type';

import { GanttEntryNormalTimeBar } from './gantt-entry-normal-time-bar.component';
import { GanttEntryTimelessTimeBar } from './gantt-entry-timeless-time-bar.component';

type GanttGroupBordersProps = {
  groups: GanttGroup[];
  heightBoxesRegistry: HeightBoxesRegistry;
  scale: ScaleTime<number, number>;
  fixedCanvasRef: React.RefObject<HTMLElement>;
};

function sortEntries(entries: GanttEntry[]): GanttEntry[] {
  return _sortBy(entries, entry => -entry.color);
}

export const GanttTimeBars: React.FC<GanttGroupBordersProps> = React.memo(
  ({ groups, heightBoxesRegistry, scale, fixedCanvasRef }) => {
    const timeBars: React.ReactNode[] = [];

    for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      const group = groups[groupIndex];
      const groupHeightBox = heightBoxesRegistry[groupIndex];
      const isExpanded = groupHeightBox.subRowBoxes.length > 0;

      if (isExpanded) {
        for (let rowIndex = 0; rowIndex < group.rows.length; rowIndex++) {
          const row = group.rows[rowIndex];
          const rowHeightBox = groupHeightBox.subRowBoxes[rowIndex];
          const orderedEntries = sortEntries(row.entries);

          for (const entry of orderedEntries) {
            if (!entry.timeless) {
              timeBars.push(
                <GanttEntryNormalTimeBar
                  key={entry.id}
                  top={rowHeightBox.top}
                  height={rowHeightBox.height}
                  left={scale(entry.startDate)}
                  width={scale(entry.endDate) - scale(entry.startDate)}
                  color={entry.color}
                  shape={entry.shape}
                  tooltip={entry.tooltip}
                  disableStartProjectionLine={entry.startDate.isSame(
                    entry.startDate.clone().startOf('month')
                  )}
                  disableEndProjectionLine={entry.endDate.isSame(
                    entry.endDate.clone().startOf('month')
                  )}
                />
              );
            } else {
              timeBars.push(
                <GanttEntryTimelessTimeBar
                  label={entry.label}
                  height={rowHeightBox.height}
                  top={rowHeightBox.top}
                  color={entry.color}
                  shape={entry.shape}
                  tooltip={entry.tooltip}
                  fixedCanvasRef={fixedCanvasRef}
                />
              );
            }
          }
        }
      } else {
        // as we assume that higher important is on top, we do reversing
        const entries = sortEntries(
          _flatten(group.rows.map(row => row.entries))
        );

        for (const entry of entries) {
          if (!entry.timeless) {
            timeBars.push(
              <GanttEntryNormalTimeBar
                key={entry.id}
                top={groupHeightBox.mainRowBox.top}
                height={groupHeightBox.mainRowBox.height}
                left={scale(entry.startDate)}
                width={scale(entry.endDate) - scale(entry.startDate)}
                color={entry.color}
                shape={entry.shape}
                tooltip={entry.tooltip}
                disableStartProjectionLine={entry.startDate.isSame(
                  entry.startDate.clone().startOf('month')
                )}
                disableEndProjectionLine={entry.endDate.isSame(
                  entry.endDate.clone().startOf('month')
                )}
              />
            );
          }
        }
      }
    }

    return <>{timeBars}</>;
  }
);
