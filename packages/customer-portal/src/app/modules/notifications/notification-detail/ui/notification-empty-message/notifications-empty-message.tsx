import React from 'react';

import { withAppConfig } from '../../../../../shared/contexts';
import { AppConfig } from '../../../../../shared/types';

import { NotificationEmptyMessageComponent } from './notification-empty-message.component';

type NotificationEmptyMessageProps = {
  type: 'jobProtectedLeave' | 'wageReplacement';
  config: AppConfig;
};

export const NotificationEmptyMessage: React.FC<NotificationEmptyMessageProps> = ({
  type,
  config: {
    clientConfig: {
      notification: {
        detail: {
          emptyState: { jobProtectedLeave, wageReplacement }
        }
      }
    }
  }
}) => (
  <NotificationEmptyMessageComponent>
    {type === 'jobProtectedLeave' && jobProtectedLeave.empty}
    {type === 'wageReplacement' && wageReplacement.pending}
  </NotificationEmptyMessageComponent>
);

export default withAppConfig(NotificationEmptyMessage);
