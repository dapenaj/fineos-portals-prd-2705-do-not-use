import React from 'react';
import { Badge } from 'antd';

import './notification-panel-status.scss';

type NotificationPanelStatusProps = {
  color: string;
  status: React.ReactNode;
};

export const NotificationPanelStatus: React.FC<NotificationPanelStatusProps> = ({
  color,
  status
}) => <Badge color={color} text={status} dot={true} />;
