import React from 'react';
import { Icon, Row, Col } from 'antd';
import classnames from 'classnames';

import { ContentLayout } from '../../../../../shared/layouts';

import styles from './summary-header.module.scss';

type SummaryHeaderProps = {
  icon?: string;
  imgIcon?: React.ReactNode;
  title: React.ReactNode;
};

export const SummaryHeader: React.FC<SummaryHeaderProps> = ({
  children,
  icon,
  imgIcon,
  title
}) => (
  <ContentLayout direction="column" noPadding={true}>
    <Row type="flex" align="middle">
      <Col sm={24} md={children ? 12 : 24}>
        <div className={classnames(styles.header, styles.content)}>
          <div className={styles.icon}>
            {icon && <Icon type={icon} />}
            {imgIcon && <>{imgIcon}</>}
          </div>
          <h2 className={styles.title}>{title}</h2>
        </div>
      </Col>
      {children && (
        <Col sm={24} md={12}>
          <div className={styles.content}>{children}</div>
        </Col>
      )}
    </Row>
  </ContentLayout>
);
