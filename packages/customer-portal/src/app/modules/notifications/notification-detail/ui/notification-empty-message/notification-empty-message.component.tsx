import React from 'react';
import { Icon } from 'antd';

import styles from './notification-empty-message.module.scss';

export const NotificationEmptyMessageComponent: React.FC = ({ children }) => (
  <div className={styles.wrapper}>
    <div className={styles.icon}>
      <Icon type="warning" theme="filled" />
    </div>
    <div className={styles.content}>
      <div className={styles.message}>{children}</div>
    </div>
  </div>
);
