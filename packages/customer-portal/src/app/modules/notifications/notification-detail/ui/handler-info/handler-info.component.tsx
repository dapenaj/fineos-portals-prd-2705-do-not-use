import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { PhoneNumberProperty } from '../..';
import { PropertyItem } from '../../../../../shared/ui/property-item';

type HandlerInfoProps = {
  name?: string;
  phoneNumber?: string;
  'data-test-el'?: string;
};

export const HandlerInfo: React.FC<HandlerInfoProps> = ({
  name,
  phoneNumber,
  ...props
}) => (
  <Row
    type="flex"
    align="middle"
    data-test-el={props['data-test-el'] || 'handler-info-properties'}
  >
    <Col xs={24} sm={12}>
      <PropertyItem label={<FormattedMessage id="NOTIFICATION.MANAGED_BY" />}>
        {!!name && name}
      </PropertyItem>
    </Col>
    <Col xs={24} sm={12}>
      <PhoneNumberProperty phoneNumber={phoneNumber} />
    </Col>
  </Row>
);
