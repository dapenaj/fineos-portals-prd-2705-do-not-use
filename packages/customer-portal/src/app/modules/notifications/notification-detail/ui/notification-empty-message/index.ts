import NotificationEmptyMessage from './notifications-empty-message';

export * from './notification-empty-message.component';
export * from './notifications-empty-message';

export { NotificationEmptyMessage };
