import React from 'react';
import { Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import { PropertyItem } from '../../../../../shared/ui';

import styles from './phone-number-property.module.scss';

type PhoneNumberPropertyProps = {
  phoneNumber?: string;
};

export const PhoneNumberProperty: React.FC<PhoneNumberPropertyProps> = ({
  phoneNumber
}) =>
  phoneNumber ? (
    <div className={styles.phone}>
      <Icon type="phone" theme="filled" />
      {phoneNumber}
    </div>
  ) : (
    <PropertyItem label={<FormattedMessage id="NOTIFICATION.PHONE_NUMBER" />} />
  );
