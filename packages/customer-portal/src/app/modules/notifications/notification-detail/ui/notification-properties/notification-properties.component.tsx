import React from 'react';
import { Row } from 'antd';
import classnames from 'classnames';

import { ContentLayout } from '../../../../../shared/layouts';

import './notification-properties.scss';

type NotificationPropertiesProps = { status: string; type?: string };

const getBorderType = (type?: string) => {
  if (type === 'Episodic') {
    return 'angled';
  } else if (type === 'Reduced Schedule') {
    return 'halved';
  } else {
    return 'full';
  }
};

const getBorderStyle = (status: string) => {
  switch (status) {
    case 'pending':
      return `warning`;
    case 'completion':
    case 'approved':
    case 'accommodated':
      return `success`;
    case 'declined':
    case 'denied':
    case 'not accommodated':
      return `danger`;
    default:
      return `default`;
  }
};

export const NotificationProperties: React.FC<NotificationPropertiesProps> = ({
  status,
  type,
  children
}) => (
  <div
    className={classnames(
      'wrapper',
      getBorderStyle(status),
      getBorderType(type)
    )}
  >
    <div className="content">
      <ContentLayout direction="column" noPadding={true}>
        <Row type="flex" align="top">
          {children}
        </Row>
      </ContentLayout>
    </div>
  </div>
);
