import React from 'react';
import { NotificationCaseSummary, DocumentService } from 'fineos-js-api-client';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { RequiredFromMePanel, showNotification } from '../../../../shared/ui';
import { UploadDocument } from '../../../../shared/types';
import { StrictEFormService } from '../../../../shared/services/eform';

import { ConfirmCaseReturn } from './confirm-case-return';

type RequiredFromMeProps = {
  notification: NotificationCaseSummary;
} & IntlProps;

type RequiredFromMeState = {
  loading: boolean;
  documents: UploadDocument[] | null;
  confirmReturns: ConfirmCaseReturn[] | null;
};

export class RequiredFromMeComponent extends React.Component<
  RequiredFromMeProps,
  RequiredFromMeState
> {
  state: RequiredFromMeState = {
    loading: false,
    documents: null,
    confirmReturns: []
  };

  documentService: DocumentService = DocumentService.getInstance();
  strictEFormService: StrictEFormService = StrictEFormService.getInstance();

  componentDidMount() {
    this.fetchRequiredFromMeData();
  }

  fetchRequiredFromMeData = async () => {
    try {
      this.setState({ loading: true });

      const [documents, confirmReturns] = await Promise.all([
        this.fetchSupportingEvidence(),
        this.fetchConfirmReturns()
      ]);

      this.setState({
        documents,
        confirmReturns
      });
    } finally {
      this.setState({ loading: false });
    }
  };

  async fetchConfirmReturns(): Promise<ConfirmCaseReturn[]> {
    const {
      notification: { absences },
      intl: { formatMessage }
    } = this.props;

    if (absences) {
      try {
        const confirmReturns: ConfirmCaseReturn[] = [];
        const eForms = await Promise.all(
          absences.map(({ absenceId }) =>
            this.strictEFormService.getConfirmReturnEForm(absenceId)
          )
        );
        for (let i = 0; i < absences.length; i++) {
          const eForm = eForms[i];

          if (eForm) {
            confirmReturns.push({
              caseId: absences[i].absenceId,
              ...eForm
            });
          }
        }

        return confirmReturns;
      } catch (error) {
        const title = formatMessage({
          id: 'RETURN_TO_WORK.FETCH_ERROR'
        });

        showNotification({
          title,
          type: 'error',
          content: error
        });
      }
    }

    return [];
  }

  async fetchSupportingEvidence(): Promise<UploadDocument[] | null> {
    const {
      notification: { notificationCaseId },
      intl: { formatMessage }
    } = this.props;

    try {
      const documents = await this.documentService.getSupportingEvidence(
        notificationCaseId
      );

      return documents.map((evidence, index) => ({
        id: index,
        caseId: evidence.uploadCaseNumber,
        evidence
      }));
    } catch (error) {
      const title = formatMessage({
        id: 'SUPPORTING_EVIDENCE.FETCH_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
      return null;
    }
  }

  handleConfirmReturnSave = async (changedConfirmReturn: ConfirmCaseReturn) => {
    const {
      intl: { formatMessage }
    } = this.props;
    const { confirmReturns } = this.state;

    try {
      await this.strictEFormService.updateConfirmReturnEForm(
        changedConfirmReturn.caseId,
        changedConfirmReturn
      );
      this.setState({
        confirmReturns: !!confirmReturns
          ? confirmReturns.map(confirmReturn => {
              if (changedConfirmReturn.caseId === confirmReturn.caseId) {
                return {
                  ...confirmReturn,
                  employeeConfirmed: true
                };
              }
              return confirmReturn;
            })
          : []
      });
    } catch (error) {
      const title = formatMessage({
        id: 'RETURN_TO_WORK.SAVE_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    }
  };

  render() {
    const { loading, documents, confirmReturns } = this.state;

    return (
      <RequiredFromMePanel
        confirmReturns={confirmReturns}
        documents={documents}
        loading={loading}
        onConfirmReturnSave={this.handleConfirmReturnSave}
        onRefreshRequiredFromMe={this.fetchRequiredFromMeData}
      />
    );
  }
}

export const RequiredFromMe = injectIntl(RequiredFromMeComponent);
