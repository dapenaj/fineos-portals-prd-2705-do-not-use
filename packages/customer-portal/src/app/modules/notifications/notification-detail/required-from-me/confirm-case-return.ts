import { ConfirmReturnEForm } from '../../../../shared/services/eform';

export type ConfirmCaseReturn = ConfirmReturnEForm & {
  caseId: string;
};
