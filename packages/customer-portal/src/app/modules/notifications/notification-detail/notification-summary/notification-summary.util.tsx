import React from 'react';
import { FormattedMessage } from 'react-intl';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { PropertyItem } from '../../../../shared/ui';
import { formatDateForDisplay } from '../../../../shared/utils';

import styles from './notification-summary.module.scss';

enum NotificationSummaryLabel {
  EXPECTED_DELIVERY_DATE = 'NOTIFICATION.EXPECTED_DELIVERY_DATE',
  ACTUAL_DELIVERY_DATE = 'NOTIFICATION.ACTUAL_DELIVERY_DATE',
  EXPECTED_RETURN_TO_WORK = 'NOTIFICATION.EXPECTED_RETURN_TO_WORK',
  ACCIDENT_DATE = 'NOTIFICATION.ACCIDENT_DATE',
  NOTIFICATION_DATE = 'NOTIFICATION.NOTIFICATION_DATE',
  FIRST_DAY_MISSING_WORK = 'NOTIFICATION.FIRST_DAY_MISSING_WORK'
}

enum NotificationReason {
  PREGNANCY = 'Pregnancy, birth or related medical treatment',
  ACCIDENT = 'Accident or treatment required for an injury'
}

const getPregnancyDeliveryDates = (
  notification: NotificationCaseSummary
): React.ReactNode[] => {
  const results = [];

  const isMiscarriage =
    notification.absences &&
    notification.absences.some(item => item.reasonQualifier2 === 'Miscarriage');

  if (
    notification.notificationReason === NotificationReason.PREGNANCY &&
    !isMiscarriage
  ) {
    results.push(
      createPropertyItem(
        NotificationSummaryLabel.EXPECTED_DELIVERY_DATE,
        notification.expectedDeliveryDate &&
          formatDateForDisplay(notification.expectedDeliveryDate)
      ),
      createPropertyItem(
        NotificationSummaryLabel.ACTUAL_DELIVERY_DATE,
        notification.actualDeliveryDate &&
          formatDateForDisplay(notification.actualDeliveryDate)
      )
    );
  }

  return results;
};

const getReturnToWorkDate = (
  notification: NotificationCaseSummary
): React.ReactNode[] => {
  const results = [];

  if (!!notification.expectedRTWDate) {
    results.push(
      createPropertyItem(
        NotificationSummaryLabel.EXPECTED_RETURN_TO_WORK,
        formatDateForDisplay(notification.expectedRTWDate)
      )
    );
  } else if (
    !notification.multipleConflictingExpectedRTWDates &&
    !!notification.dateFirstMissingWork
  ) {
    results.push(
      createPropertyItem(NotificationSummaryLabel.EXPECTED_RETURN_TO_WORK)
    );
  }

  return results;
};

const getAccidentReason = (
  notification: NotificationCaseSummary
): React.ReactNode[] => {
  const results = [];

  if (notification.accidentDate) {
    results.push(
      createPropertyItem(
        NotificationSummaryLabel.ACCIDENT_DATE,
        formatDateForDisplay(notification.accidentDate)
      )
    );
  } else if (
    notification.notificationReason === NotificationReason.ACCIDENT &&
    !notification.multipleConflictingAccidentDates
  ) {
    results.push(createPropertyItem(NotificationSummaryLabel.ACCIDENT_DATE));
  }

  return results;
};

export const createPropertyItem = (
  label: string,
  value?: React.ReactNode
): React.ReactNode => {
  return (
    <div className={styles.marginLeft}>
      <PropertyItem label={<FormattedMessage id={label} />}>
        {value}
      </PropertyItem>
    </div>
  );
};

export const buildColsFromNotification = (
  notification: NotificationCaseSummary
): React.ReactNode[] => {
  const results = [
    createPropertyItem(
      NotificationSummaryLabel.NOTIFICATION_DATE,
      formatDateForDisplay(notification.notificationDate)
    )
  ];

  results.push(...getPregnancyDeliveryDates(notification));

  if (!!notification.dateFirstMissingWork) {
    results.push(
      createPropertyItem(
        NotificationSummaryLabel.FIRST_DAY_MISSING_WORK,
        formatDateForDisplay(notification.dateFirstMissingWork)
      )
    );
  }

  results.push(...getReturnToWorkDate(notification));

  results.push(...getAccidentReason(notification));

  return results;
};
