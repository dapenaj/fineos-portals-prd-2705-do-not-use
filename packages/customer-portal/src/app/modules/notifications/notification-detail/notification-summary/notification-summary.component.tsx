import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import { Collapse, Col, Row } from 'antd';
import { FormattedMessage } from 'react-intl';

import { ContentLayout } from '../../../../shared/layouts';
import {
  getCaseProgress,
  getNotificationStatus
} from '../../../../shared/utils';
import {
  NotificationStatusResult,
  NotificationPendingStatus
} from '../../../../shared/types';

import {
  createPropertyItem,
  buildColsFromNotification
} from './notification-summary.util';
import styles from './notification-summary.module.scss';

const { Panel } = Collapse;

type NotificationSummaryProps = {
  notification: NotificationCaseSummary;
};

export const NotificationSummary: React.FC<NotificationSummaryProps> = ({
  notification
}) => (
  <div className={styles.wrapper} data-test-el="notification-summary">
    <Collapse defaultActiveKey={['1']}>
      <Panel header={<FormattedMessage id="NOTIFICATION.SUMMARY" />} key="1">
        <div
          className={styles.panel}
          data-test-el="notification-summary-properties"
        >
          <ContentLayout direction="column" noPadding={true}>
            <Row>
              <Col xs={24} sm={6}>
                {createPropertyItem(
                  'NOTIFICATION.YOUR_CASE_PROGRESS',
                  <FormattedMessage
                    id={getCaseProgress(
                      notification.status ===
                        NotificationPendingStatus.REGISTRATION
                        ? NotificationStatusResult.PENDING
                        : getNotificationStatus(
                            notification.claims,
                            notification.absences,
                            notification.accommodations
                          )
                    )}
                  />
                )}
              </Col>
              <Col xs={24} sm={18} className={styles.border}>
                <Row>
                  {buildColsFromNotification(notification).map((col, index) => {
                    return (
                      <Col
                        xs={24}
                        sm={12}
                        md={8}
                        className={styles.col}
                        key={index}
                      >
                        {col}
                      </Col>
                    );
                  })}
                </Row>
              </Col>
            </Row>
          </ContentLayout>
        </div>
      </Panel>
    </Collapse>
  </div>
);
