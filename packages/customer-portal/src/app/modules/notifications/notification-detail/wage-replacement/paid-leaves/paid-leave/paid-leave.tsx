import React from 'react';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import {
  Benefit,
  DisabilityBenefitSummary,
  ClaimService
} from 'fineos-js-api-client';
import { orderBy as _orderBy, isEmpty as _isEmpty } from 'lodash';

import { CasePayment } from '../../../../../../shared/types';
import { showNotification } from '../../../../../../shared/ui';
import { WageReplacementProperties } from '../../ui';

type PaidLeaveProps = {
  benefits: Benefit[];
  claimId: string;
  disabilities?: DisabilityBenefitSummary[] | null;
  paidLeaveManager?: string;
  paidLeaveManagerPhoneNumber?: string;
} & IntlProps;

type PaidLeaveState = {
  paymentDetails: CasePayment[] | null;
  loading: boolean;
};

export class PaidLeaveComponent extends React.Component<
  PaidLeaveProps,
  PaidLeaveState
> {
  state: PaidLeaveState = {
    paymentDetails: null,
    loading: false
  };

  claimService = ClaimService.getInstance();

  async componentDidMount() {
    const {
      claimId,
      intl: { formatMessage }
    } = this.props;

    try {
      const payments = await this.claimService.findPayments(claimId);

      const casePayments = payments.map(payment => ({
        claimId,
        ...payment
      }));

      const paymentDetails: CasePayment[] = _orderBy(
        casePayments,
        'paymentDate',
        'desc'
      );

      this.setState({
        paymentDetails
      });
    } catch (error) {
      const title = formatMessage({
        id: 'API_RESPONSES.ERRORS.FIND_PAYMENTS'
      });

      showNotification({ title, type: 'error', content: error });
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    const { benefits, disabilities } = this.props;
    const { paymentDetails, loading } = this.state;

    return (
      <>
        {benefits.map((benefit, index) => {
          const filteredPaymentDetails = !_isEmpty(paymentDetails)
            ? paymentDetails!.filter(
                item => item.benefitCaseNumber === String(benefit.benefitId)
              )
            : null;

          const disabilityBenefitSummary = !_isEmpty(disabilities)
            ? disabilities!.find(
                disability =>
                  disability.benefitSummary.benefitId === benefit.benefitId
              )
            : null;

          return (
            <WageReplacementProperties
              paymentDetails={filteredPaymentDetails}
              loading={loading}
              benefit={benefit}
              disabilityBenefitSummary={disabilityBenefitSummary}
              key={index}
            />
          );
        })}
      </>
    );
  }
}

export const PaidLeave = injectIntl(PaidLeaveComponent);
