import React from 'react';

import { WageReplacementStatusIcon } from '../ui';

type PaidLeavesStatusIconsProps = {
  statuses: { status: string; count: number }[];
};

export const PaidLeavesStatusIcons: React.FC<PaidLeavesStatusIconsProps> = ({
  statuses
}) => (
  <>
    {statuses.map(({ status, count }, index) => (
      <WageReplacementStatusIcon
        key={`${index}_WAGE_REPLACEMENT_STATUS`}
        status={status}
        count={count}
      />
    ))}
  </>
);
