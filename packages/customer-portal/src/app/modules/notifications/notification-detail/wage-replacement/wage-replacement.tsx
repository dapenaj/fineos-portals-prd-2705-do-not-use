import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';

import {
  ClaimBenefit,
  DisabilityBenefitWrapper,
  AbsencePaidLeave
} from '../../../../shared/types';

import { WageReplacementHeader } from './wage-replacement-header.component';
import { DisabilityBenefits } from './disability-benefits';
import { PaidLeaves } from './paid-leaves';
import { EmptyWageReplacement } from './ui';
import styles from './wage-replacement.module.scss';

type WageReplacementProps = {
  claimIds: string[];
  claimBenefits: ClaimBenefit[] | null;
  paidLeaves: AbsencePaidLeave[] | null;
  disabilityBenefitWrappers: DisabilityBenefitWrapper[] | null;
};

export const WageReplacement: React.FC<WageReplacementProps> = ({
  claimIds,
  claimBenefits,
  paidLeaves,
  disabilityBenefitWrappers
}) => {
  const hasPaidLeaves = !_isEmpty(paidLeaves);
  const hasClaimBenefits = !_isEmpty(claimBenefits);
  const showWageReplacementAssessment =
    !_isEmpty(claimIds) && claimBenefits && claimBenefits.length === 0;

  const showWageReplacement =
    hasPaidLeaves || hasClaimBenefits || showWageReplacementAssessment;

  return (
    <>
      {showWageReplacement && (
        <div data-test-el="wage-replacement" className={styles.wrapper}>
          <WageReplacementHeader />

          {hasClaimBenefits &&
            claimBenefits!.map(({ claimId, benefits }, index) => (
              <DisabilityBenefits
                claimId={claimId}
                benefits={benefits}
                disabilityClaimBenefits={
                  disabilityBenefitWrappers
                    ? disabilityBenefitWrappers
                        .filter(
                          disabilityBenefitWrapper =>
                            disabilityBenefitWrapper.claimId === claimId
                        )
                        .map(
                          disabilityBenefitWrapper =>
                            disabilityBenefitWrapper.disabilityBenefitSummary
                        )
                    : []
                }
                key={index}
              />
            ))}

          {hasPaidLeaves && (
            <PaidLeaves
              paidLeaves={paidLeaves!}
              disabilityClaimBenefits={disabilityBenefitWrappers}
            />
          )}

          {showWageReplacementAssessment && <EmptyWageReplacement />}
        </div>
      )}
    </>
  );
};
