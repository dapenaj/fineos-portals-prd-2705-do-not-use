import React from 'react';
import { Benefit, DisabilityBenefitSummary } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { Col } from 'antd';

import { formatDateForDisplay } from '../../../../../shared/utils';
import { PropertyItem } from '../../../../../shared/ui';
import { NotificationProperties } from '../../ui';
import { getWageReplacementStatus } from '../wage-replacement.util';
import {
  WageReplacementStatus,
  CasePayment,
  BenefitRightCategory
} from '../../../../../shared/types';
import { PaymentHistory } from '../../payment-history';

import { WageReplacementStatusIcon } from './wage-replacement-status.component';

type WageReplacementProps = {
  benefit: Benefit;
  paymentDetails: CasePayment[] | null;
  loading: boolean;
  disabilityBenefitSummary?: DisabilityBenefitSummary | null;
};

export const WageReplacementProperties: React.FC<WageReplacementProps> = ({
  benefit: { status, stageName, creationDate, benefitCaseType },
  disabilityBenefitSummary,
  paymentDetails,
  loading
}) => {
  const showPaymentHistory =
    stageName === 'Approved' && paymentDetails && paymentDetails.length > 0;

  const showSummaryDates =
    disabilityBenefitSummary &&
    disabilityBenefitSummary.benefitSummary.benefitRightCategory ===
      BenefitRightCategory.RECURRING_BENEFIT;

  return (
    <div data-test-el="wage-replacement-properties">
      <NotificationProperties
        status={getWageReplacementStatus(
          status as WageReplacementStatus,
          stageName
        ).toLowerCase()}
      >
        <Col xs={24} sm={12} md={6}>
          <PropertyItem
            label={<FormattedMessage id="NOTIFICATION.BENEFIT_START_DATE" />}
          >
            {formatDateForDisplay(creationDate)}
          </PropertyItem>
        </Col>

        <Col xs={24} sm={12} md={6}>
          <PropertyItem
            label={<FormattedMessage id="NOTIFICATION.APPROVED_THROUGH" />}
          >
            {showSummaryDates &&
              formatDateForDisplay(
                disabilityBenefitSummary!.disabilityBenefit.approvedThroughDate
              )}
          </PropertyItem>
        </Col>

        <Col xs={24} sm={12} md={6}>
          <PropertyItem
            label={<FormattedMessage id="NOTIFICATION.EXPECTED_RESOLUTION" />}
          >
            {showSummaryDates &&
              formatDateForDisplay(
                disabilityBenefitSummary!.disabilityBenefit
                  .expectedResolutionDate
              )}
          </PropertyItem>
        </Col>

        <Col xs={24} sm={12} md={6}>
          <WageReplacementStatusIcon
            status={getWageReplacementStatus(
              status as WageReplacementStatus,
              stageName
            )}
          />
        </Col>

        {showPaymentHistory && (
          <PaymentHistory
            loading={loading}
            caseType={benefitCaseType}
            paymentDetails={paymentDetails!}
          />
        )}
      </NotificationProperties>
    </div>
  );
};
