import React from 'react';
import { FormattedMessage } from 'react-intl';

import styles from './empty-wage-replacement.module.scss';

export const EmptyWageReplacement: React.FC = () => (
  <div className={styles.wrapper}>
    <div className={styles.content}>
      <FormattedMessage id="WAGE_REPLACEMENT.EMPTY.TITLE" />
    </div>
  </div>
);
