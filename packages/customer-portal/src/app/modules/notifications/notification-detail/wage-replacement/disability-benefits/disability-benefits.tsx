import React from 'react';
import { DisabilityBenefitSummary, Benefit } from 'fineos-js-api-client';

import { Benefits } from './benefits';

type DisabilityClaimBenefitsProps = {
  claimId: string;
  benefits: Benefit[];
  disabilityClaimBenefits: DisabilityBenefitSummary[];
};

export const DisabilityBenefits: React.FC<DisabilityClaimBenefitsProps> = ({
  claimId,
  benefits,
  disabilityClaimBenefits
}) => (
  <div data-test-el="disability-benefits">
    {!!benefits && (
      <Benefits
        claimId={claimId}
        disabilities={disabilityClaimBenefits}
        benefits={benefits}
      />
    )}
  </div>
);
