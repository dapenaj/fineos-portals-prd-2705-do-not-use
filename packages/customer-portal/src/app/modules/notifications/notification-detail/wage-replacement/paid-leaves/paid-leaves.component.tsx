import React from 'react';
import { Collapse } from 'antd';

import {
  AbsencePaidLeave,
  DisabilityBenefitWrapper
} from '../../../../../shared/types';

import { PaidLeavesHeader } from './paid-leaves-header';
import { PaidLeave } from './paid-leave';

const { Panel } = Collapse;

type PaidLeavesProps = {
  paidLeaves: AbsencePaidLeave[];
  disabilityClaimBenefits: DisabilityBenefitWrapper[] | null;
};

type PaidLeavesState = {
  activeKey: string | string[];
};

export class PaidLeaves extends React.Component<
  PaidLeavesProps,
  PaidLeavesState
> {
  state = { activeKey: [] as string[] };

  render() {
    const { paidLeaves, disabilityClaimBenefits } = this.props;
    const { activeKey } = this.state;

    return (
      <Collapse
        onChange={key =>
          this.setState({
            activeKey: key
          })
        }
      >
        {paidLeaves.map((paidLeave, paidLeaveIndex) => (
          <Panel
            key={`${paidLeaveIndex}_PAID_LEAVE`}
            header={
              <PaidLeavesHeader
                paidLeave={paidLeave}
                expanded={activeKey.includes(`${paidLeaveIndex}_PAID_LEAVE`)}
              />
            }
          >
            {paidLeave.claimBenefits.map(({ claimId, benefits }, index) => (
              <PaidLeave
                key={index}
                claimId={claimId}
                benefits={benefits}
                paidLeaveManager={paidLeave.paidLeaveHandler}
                paidLeaveManagerPhoneNumber={
                  paidLeave.paidLeaveHandlerPhoneNumber
                }
                disabilities={
                  disabilityClaimBenefits
                    ? disabilityClaimBenefits
                        .filter(disability => disability.claimId === claimId)
                        .map(disability => disability.disabilityBenefitSummary)
                    : []
                }
              />
            ))}
          </Panel>
        ))}
      </Collapse>
    );
  }
}
