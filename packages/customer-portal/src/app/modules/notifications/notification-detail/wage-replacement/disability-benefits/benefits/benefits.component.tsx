import React from 'react';
import {
  DisabilityBenefitSummary,
  Benefit,
  ClaimService
} from 'fineos-js-api-client';
import { Collapse } from 'antd';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { orderBy as _orderBy, isEmpty as _isEmpty } from 'lodash';

import { WageReplacementProperties } from '../../ui';
import { showNotification } from '../../../../../../shared/ui';
import { CasePayment } from '../../../../../../shared/types';

import { BenefitHeader } from './benefit-header.component';

const { Panel } = Collapse;

type BenefitsProps = {
  benefits: Benefit[];
  claimId: string;
  disabilities?: DisabilityBenefitSummary[] | null;
  paidLeaveManager?: string;
  paidLeaveManagerPhoneNumber?: string;
} & IntlProps;

type BenefitsState = {
  activeKey: string | string[];
  paymentDetails: CasePayment[] | null;
  loading: boolean;
};

export class BenefitsComponent extends React.Component<
  BenefitsProps,
  BenefitsState
> {
  claimService = ClaimService.getInstance();

  state: BenefitsState = {
    activeKey: [],
    paymentDetails: null,
    loading: false
  };

  async componentDidMount() {
    const {
      claimId,
      intl: { formatMessage }
    } = this.props;

    try {
      const payments = await this.claimService.findPayments(claimId);

      const casePayments = payments.map(payment => ({
        claimId,
        ...payment
      }));

      const paymentDetails: CasePayment[] = _orderBy(
        casePayments,
        'paymentDate',
        'desc'
      );

      this.setState({
        paymentDetails
      });
    } catch (error) {
      const title = formatMessage({
        id: 'API_RESPONSES.ERRORS.FIND_PAYMENTS'
      });

      showNotification({ title, type: 'error', content: error });
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    const {
      benefits,
      disabilities,
      paidLeaveManager,
      paidLeaveManagerPhoneNumber
    } = this.props;
    const { activeKey, paymentDetails, loading } = this.state;

    return (
      <div data-test-el="benefits">
        <Collapse
          onChange={key =>
            this.setState({
              activeKey: key
            })
          }
        >
          {benefits.map((benefit, index) => {
            const filteredPaymentDetails = !_isEmpty(paymentDetails)
              ? paymentDetails!.filter(
                  item => item.benefitCaseNumber === String(benefit.benefitId)
                )
              : null;

            const disabilityBenefitSummary = !_isEmpty(disabilities)
              ? disabilities!.find(
                  disability =>
                    disability.benefitSummary.benefitId === benefit.benefitId
                )
              : null;

            return (
              <Panel
                header={
                  <BenefitHeader
                    expanded={activeKey.includes(
                      `${benefit.benefitCaseType}_${index}`
                    )}
                    benefit={benefit}
                    paidLeaveManager={paidLeaveManager}
                    paidLeaveManagerPhoneNumber={paidLeaveManagerPhoneNumber}
                  />
                }
                key={`${benefit.benefitCaseType}_${index}`}
              >
                <WageReplacementProperties
                  paymentDetails={filteredPaymentDetails}
                  loading={loading}
                  benefit={benefit}
                  disabilityBenefitSummary={disabilityBenefitSummary}
                  key={index}
                />
              </Panel>
            );
          })}
        </Collapse>
      </div>
    );
  }
}

export const Benefits = injectIntl(BenefitsComponent);
