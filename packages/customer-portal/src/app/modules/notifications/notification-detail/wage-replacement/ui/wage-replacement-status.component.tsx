import React from 'react';
import { FormattedMessage } from 'react-intl';

import { WageReplacementStatus } from '../../../../../shared/types';
import { getWageReplacementStatusLabel } from '../wage-replacement.util';
import { NotificationPanelStatus } from '../../ui';

type WageReplacementStatusProps = {
  status: string;
  count?: number;
};

const getColorStyle = (color: string) => {
  switch (color) {
    case 'Pending':
      return '#ffc20e';
    case 'Approved':
      return '#35b558';
    case 'Denied':
      return '#f04c3f';
    default:
      return '#b3b3b3';
  }
};

export const WageReplacementStatusIcon: React.FC<WageReplacementStatusProps> = ({
  status,
  count
}) => (
  <span data-test-el="wage-replacement-status-icon">
    <NotificationPanelStatus
      color={getColorStyle(status)}
      status={
        <>
          <FormattedMessage
            id={getWageReplacementStatusLabel(status as WageReplacementStatus)}
          />
          {count && <span className="count">({count})</span>}
        </>
      }
    />
  </span>
);
