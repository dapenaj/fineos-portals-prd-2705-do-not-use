import { Benefit } from 'fineos-js-api-client';
import { chain as _chain } from 'lodash';

import { getWageReplacementStatus } from '../wage-replacement.util';
import { WageReplacementStatus } from '../../../../../shared/types';

export const countPaidLeaveBenefits = (
  value: Benefit[]
): { status: string; count: number }[] =>
  _chain(value)
    .groupBy(({ status, stageName }) =>
      getWageReplacementStatus(status as WageReplacementStatus, stageName)
    )
    .map((values, key) => ({
      status: key,
      count: values.length
    }))
    .value();
