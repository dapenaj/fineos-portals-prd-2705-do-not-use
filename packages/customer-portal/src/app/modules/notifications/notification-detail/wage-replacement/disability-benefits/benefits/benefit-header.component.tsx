import React from 'react';
import { Benefit } from 'fineos-js-api-client';
import { Row, Col } from 'antd';

import { ContentLayout } from '../../../../../../shared/layouts';
import { WageReplacementStatus } from '../../../../../../shared/types';
import { HandlerInfo } from '../../../ui';
import { WageReplacementStatusIcon } from '../../ui';
import { getWageReplacementStatus } from '../../wage-replacement.util';

type BenefitHeaderProps = {
  expanded: boolean;
  benefit: Benefit;
  paidLeaveManager?: string;
  paidLeaveManagerPhoneNumber?: string;
};

export const BenefitHeader: React.FC<BenefitHeaderProps> = ({
  expanded,
  benefit: {
    benefitCaseType,
    benefitHandler,
    benefitHandlerPhoneNo,
    status,
    stageName
  },
  paidLeaveManager,
  paidLeaveManagerPhoneNumber
}) => (
  <div data-test-el="disability-benefit-header">
    {expanded ? (
      <ContentLayout direction="column" noPadding={true}>
        <Row type="flex" align="middle">
          <Col xs={24} sm={12}>
            <div data-test-el="disability-benefit-header-name">
              {benefitCaseType}
            </div>
          </Col>
          <Col xs={24} sm={12}>
            <HandlerInfo
              data-test-el="benefit-header-handler-info"
              name={!!paidLeaveManager ? paidLeaveManager : benefitHandler}
              phoneNumber={
                !!paidLeaveManagerPhoneNumber
                  ? paidLeaveManagerPhoneNumber
                  : benefitHandlerPhoneNo
              }
            />
          </Col>
        </Row>
      </ContentLayout>
    ) : (
      <>
        <div data-test-el="disability-benefit-header-name">
          {benefitCaseType}
        </div>
        <div>
          <WageReplacementStatusIcon
            status={getWageReplacementStatus(
              status as WageReplacementStatus,
              stageName
            )}
          />
        </div>
      </>
    )}
  </div>
);
