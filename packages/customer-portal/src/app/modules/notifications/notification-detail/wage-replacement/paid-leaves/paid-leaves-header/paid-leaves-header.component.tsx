import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import { flatten as _flatten } from 'lodash';

import { ContentLayout } from '../../../../../../shared/layouts';
import { AbsencePaidLeave } from '../../../../../../shared/types';
import { HandlerInfo } from '../../../ui';
import { countPaidLeaveBenefits } from '../paid-leaves.util';
import { PaidLeavesStatusIcons } from '../paid-leaves-status-icons.component';

import styles from './paid-leaves-header.module.scss';

type PaidLeavesHeaderProps = {
  expanded: boolean;
  paidLeave: AbsencePaidLeave;
};

export const PaidLeavesHeader: React.FC<PaidLeavesHeaderProps> = ({
  expanded,
  paidLeave: { claimBenefits, paidLeaveHandlerPhoneNumber, paidLeaveHandler }
}) => (
  <div data-test-el="disability-benefit-header">
    {expanded ? (
      <ContentLayout direction="column" noPadding={true}>
        <Row type="flex" align="middle">
          <Col xs={24} sm={12}>
            <div className={styles.header}>
              <div data-test-el="disability-benefit-header-name">
                <FormattedMessage id="NOTIFICATIONS.PAID_LEAVES" />
              </div>
            </div>
          </Col>
          <Col xs={24} sm={12}>
            <HandlerInfo
              data-test-el="paid-leave-handler-info"
              name={paidLeaveHandler}
              phoneNumber={paidLeaveHandlerPhoneNumber}
            />
          </Col>
        </Row>
      </ContentLayout>
    ) : (
      <>
        <div className={styles.header}>
          <div data-test-el="disability-benefit-header-name">
            <FormattedMessage id="NOTIFICATIONS.PAID_LEAVES" />
          </div>
        </div>
        <div className={styles.wrapper}>
          <PaidLeavesStatusIcons
            statuses={countPaidLeaveBenefits(
              _flatten(claimBenefits.map(({ benefits }) => benefits))
            )}
          />
        </div>
      </>
    )}
  </div>
);
