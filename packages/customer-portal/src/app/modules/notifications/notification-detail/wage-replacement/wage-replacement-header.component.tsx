import React from 'react';
import { FormattedMessage } from 'react-intl';

import { SummaryHeader } from '../ui';

export const WageReplacementHeader: React.FC = () => (
  <div data-test-el="wage-replacement-header">
    <SummaryHeader
      title={<FormattedMessage id="NOTIFICATION.WAGE_REPLACEMENT" />}
      icon="dollar"
    />
  </div>
);
