import {
  WageReplacementDisplayStatus,
  WageReplacementStatus
} from '../../../../shared/types';

export const getWageReplacementStatus = (
  status: WageReplacementStatus,
  stageName: string
): WageReplacementDisplayStatus => {
  switch (status) {
    case 'Pending':
      return 'Pending';
    case 'Closed':
      return 'Closed';
    case 'Decided':
      return getStageNameStatus(stageName);
    default:
      return getStageNameForUnknownStatus(stageName);
  }
};

const getStageNameStatus = (
  stageName: string
): WageReplacementDisplayStatus => {
  switch (stageName) {
    case 'Approved':
      return 'Approved';
    case 'Denied':
      return 'Denied';
    default:
      return 'Decided';
  }
};

const getStageNameForUnknownStatus = (
  stageName: string
): WageReplacementDisplayStatus => {
  switch (stageName) {
    case 'Pending':
    case 'Approved':
    case 'Denied':
    case 'Decided':
      return stageName;
    default:
      return 'Closed';
  }
};

export const getWageReplacementStatusLabel = (
  status: WageReplacementDisplayStatus
) => {
  switch (status) {
    case 'Pending':
      return 'DISABILITY_BENEFIT_STATUS.PENDING';
    case 'Approved':
      return 'DISABILITY_BENEFIT_STATUS.APPROVED';
    case 'Denied':
      return 'DISABILITY_BENEFIT_STATUS.DENIED';
    case 'Decided':
      return 'DISABILITY_BENEFIT_STATUS.DECIDED';
    default:
      return 'DISABILITY_BENEFIT_STATUS.CLOSED';
  }
};
