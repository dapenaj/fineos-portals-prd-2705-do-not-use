import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';
import moment from 'moment';

import { removeClosedNotifications } from '../../../../shared/utils';
import { ContentRenderer } from '../../../../shared/ui';

import { MyNotificationPanel } from './my-notification-panel';

type MyNotificationsPanelsProps = {
  loading: boolean;
  notifications: NotificationCaseSummary[] | null;
  showClosed: boolean;
};

export const MyNotificationsPanels: React.FC<MyNotificationsPanelsProps> = ({
  loading,
  notifications,
  showClosed
}) => {
  const getFilteredNotifications = (
    summaries: NotificationCaseSummary[] | null
  ): NotificationCaseSummary[] =>
    !_isEmpty(summaries)
      ? summaries!
          .filter(
            notification =>
              showClosed ||
              removeClosedNotifications(
                notification.claims,
                notification.absences,
                notification.accommodations
              )
          )
          .sort((a, b) =>
            moment(b.notificationDate).diff(moment(a.notificationDate))
          )
      : [];

  return (
    <ContentRenderer
      loading={loading}
      isEmpty={_isEmpty(notifications)}
      emptyTitle={
        <FormattedMessage
          id={
            showClosed
              ? 'NOTIFICATIONS.EMPTY_LIST.EMPTY_ALL_TYPE_NOTIFICATIONS'
              : 'NOTIFICATIONS.EMPTY_LIST.EMPTY_OPEN_NOTIFICATIONS'
          }
        />
      }
      withBorder={true}
    >
      <div data-test-el="notifications-panels">
        {getFilteredNotifications(notifications).map((notification, key) => (
          <MyNotificationPanel key={key} notification={notification} />
        ))}
      </div>
    </ContentRenderer>
  );
};
