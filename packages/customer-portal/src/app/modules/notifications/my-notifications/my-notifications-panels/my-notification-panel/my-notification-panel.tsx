import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import { Col, Row } from 'antd';
import classnames from 'classnames';

import {
  OpenNotificationAssessmentColumn,
  OpenNotificationHeader
} from '../../../../home/my-open-notifications';
import { MyNotificationsStatus } from '../../my-notifications-status';
import { Panel } from '../../../../../shared/ui';
import {
  getNotificationStatus,
  getCaseProgress
} from '../../../../../shared/utils';
import {
  NotificationStatusResult,
  NotificationPendingStatus,
  AppConfig
} from '../../../../../shared/types';
import { withAppConfig } from '../../../../../shared/contexts';

import styles from './my-notification-panel.module.scss';

type MyNotificationPanelProps = {
  notification: NotificationCaseSummary;
  config: AppConfig;
};

const MyNotificationPanelComponent: React.FC<MyNotificationPanelProps> = ({
  notification,
  config: {
    clientConfig: {
      notification: {
        inProgressStatus: { message }
      }
    }
  }
}) => {
  const isInProgress =
    notification.status === NotificationPendingStatus.REGISTRATION;

  const status = isInProgress
    ? NotificationStatusResult.PENDING
    : getNotificationStatus(
        notification.claims,
        notification.absences,
        notification.accommodations
      );

  return (
    <Panel className={styles.panel}>
      <Row type="flex" align="middle">
        <Col xs={24} md={10}>
          <Row type="flex" align="middle">
            <Col
              xs={12}
              md={8}
              className={classnames(styles.notificationCol, styles.withBorder)}
              data-test-el="notification-case-id"
            >
              {!isInProgress ? (
                <OpenNotificationHeader notification={notification} />
              ) : (
                <div className={styles.header}>
                  {notification.notificationCaseId}
                </div>
              )}
            </Col>
            <Col
              xs={12}
              md={16}
              className={classnames(
                styles.notificationCol,
                styles.withBorder,
                styles.withPadding
              )}
            >
              <div>{notification.notificationReason}</div>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={14}>
          <Row type="flex" align="middle">
            {isInProgress ? (
              <Col
                xs={24}
                className={classnames(
                  styles.notificationCol,
                  styles.withPadding
                )}
              >
                <div className={styles.inProgress}>{message}</div>
              </Col>
            ) : (
              <>
                <Col
                  xs={12}
                  md={10}
                  className={classnames(
                    styles.notificationCol,
                    styles.withBorder,
                    styles.withPadding
                  )}
                >
                  <OpenNotificationAssessmentColumn
                    claims={notification.claims}
                    absences={notification.absences}
                    accommodations={notification.accommodations}
                    fullWidth={true}
                  />
                </Col>
                <Col xs={12} md={14} className={styles.notificationCol}>
                  <MyNotificationsStatus
                    status={status}
                    caseProgress={getCaseProgress(status)}
                  />
                </Col>
              </>
            )}
          </Row>
        </Col>
      </Row>
    </Panel>
  );
};

export const MyNotificationPanel = withAppConfig(MyNotificationPanelComponent);
