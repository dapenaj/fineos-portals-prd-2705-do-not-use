import React from 'react';
import { Col, Row, Button } from 'antd';
import { FormattedMessage } from 'react-intl';
import { NavLink } from 'react-router-dom';

import { PageTitle } from '../../../../shared/ui';

import styles from './my-notifications-header.module.scss';

export const MyNotificationsHeader: React.FC = () => (
  <PageTitle>
    <Row>
      <Col md={12} data-test-el="notifications-header">
        <FormattedMessage id="NAVBAR_LINKS.MY_NOTIFICATIONS" />
      </Col>
      <Col md={12} className={styles.buttonPosition}>
        <NavLink
          to={{ pathname: '/intake', state: { prevPath: '/notifications' } }}
        >
          <Button
            type="primary"
            data-test-el="notification-button"
            className={styles.buttonWidth}
          >
            <FormattedMessage id="NOTIFICATIONS.MAKE_NOTIFICATION" />
          </Button>
        </NavLink>
      </Col>
    </Row>
  </PageTitle>
);
