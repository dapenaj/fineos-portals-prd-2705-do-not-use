import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { ContentLayout } from '../../../shared/layouts';
import {
  MyNotificationsHeader,
  MyNotificationsPanels
} from '../my-notifications';
import { HeaderSwitch } from '../../../shared/ui';

type MyNotificationsProps = {
  notifications: NotificationCaseSummary[] | null;
  loading: boolean;
};

type MyNotificationsState = {
  showClosed: boolean;
};
export class MyNotificationsView extends React.Component<
  MyNotificationsProps,
  MyNotificationsState
> {
  state = {
    showClosed: true
  };

  onChange = (showClosed: boolean) => {
    this.setState({ showClosed });
  };

  render() {
    const { notifications, loading } = this.props;
    const { showClosed } = this.state;

    return (
      <div data-test-el="notification-wrapper">
        <ContentLayout direction="column">
          <MyNotificationsHeader />
          <HeaderSwitch
            label="NOTIFICATIONS.CLOSE_NOTIFICATIONS"
            onChange={this.onChange}
          />
          <MyNotificationsPanels
            loading={loading}
            notifications={notifications}
            showClosed={showClosed}
          />
        </ContentLayout>
      </div>
    );
  }
}
