import React from 'react';
import { connect } from 'react-redux';

import { RootState } from '../../../store';
import { fetchNotifications } from '../../../store/notification/actions';

import { MyNotificationsView } from './my-notifications.view';

const mapStateToProps = ({ notification: { data, loading } }: RootState) => ({
  notifications: data,
  loading
});
const mapDispatchToProps = {
  fetchNotifications
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
type MyNotificationsProps = StateProps & DispatchProps;

export class MyNotificationsContainer extends React.Component<
  MyNotificationsProps
> {
  componentDidMount() {
    this.props.fetchNotifications();
  }

  render() {
    const { notifications, loading } = this.props;

    return (
      <MyNotificationsView notifications={notifications} loading={loading} />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyNotificationsContainer);
