import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Icon } from 'antd';
import classnames from 'classnames';

import { NotificationStatusResult } from '../../../../shared/types';

import styles from './my-notifications-status.module.scss';

type MyNotificationsStatusProps = {
  status: NotificationStatusResult;
  caseProgress: string;
};

const getIcon = (status: NotificationStatusResult) => {
  switch (status) {
    case NotificationStatusResult.PENDING:
      return (
        <div className={classnames(styles.circle, styles.pendingIcon)}>
          <Icon type="redo" className={styles.icon} />
        </div>
      );
    case NotificationStatusResult.DECIDED:
      return (
        <div className={classnames(styles.circle, styles.decidedIcon)}>
          <Icon type="file-done" className={styles.icon} />
        </div>
      );
    case NotificationStatusResult.SOME_DECIDED:
      return (
        <div className={classnames(styles.circle, styles.decidingIcon)}>
          <Icon type="inbox" className={styles.icon} />
        </div>
      );
    case NotificationStatusResult.ALL_CLOSED:
      return (
        <div className={classnames(styles.circle, styles.pendingIcon)}>
          <Icon type="book" className={styles.icon} />
        </div>
      );

    default:
      return '';
  }
};

export const MyNotificationsStatus: React.FC<MyNotificationsStatusProps> = ({
  status,
  caseProgress
}) => (
  <div className={styles.status}>
    {getIcon(status)}
    <div className={styles.caseProgress}>
      <FormattedMessage id={caseProgress} />
    </div>
  </div>
);
