import React from 'react';
import { Button } from 'antd';

import styles from './preference-handler.module.scss';

type PreferenceHandlerProps = {
  title: React.ReactNode;
  body: React.ReactNode;
  link?: React.ReactNode;
  onClick?: () => void;
};

export const PreferenceHandler: React.FC<PreferenceHandlerProps> = ({
  title,
  body,
  link,
  // onClick functionality to be added in next pr
  onClick
}) => (
  <div className={styles.wrapper}>
    <div className={styles.title}>{title}</div>
    <div className={styles.body}>{body}</div>
    <Button onClick={onClick} className={styles.button} type="link">
      {link}
    </Button>
  </div>
);
