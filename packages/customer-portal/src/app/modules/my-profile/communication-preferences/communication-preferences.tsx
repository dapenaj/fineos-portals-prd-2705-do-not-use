import React from 'react';
import { FormattedMessage } from 'react-intl';

import { AppConfig } from '../../../shared/types';
import { withAppConfig } from '../../../shared/contexts';
import {
  formatPhoneToString,
  getCustomerPhoneNumber
} from '../../../shared/utils';
import { MyProfileHeader, MyProfileTab } from '../ui';

import { PreferenceHandler } from './preference-handler';
import styles from './communication-preferences.module.scss';

type CommunicationPreferencesContainerProps = {
  config: AppConfig;
};

export const CommunicationPreferencesComponent: React.FC<CommunicationPreferencesContainerProps> = ({
  config: {
    customerDetail: { contact },
    clientConfig: {
      myProfile: { message }
    }
  }
}) => (
  <>
    <MyProfileHeader>
      <FormattedMessage id="MY_PROFILE.COMMUNICATION_PREFERENCES" />
    </MyProfileHeader>
    <MyProfileTab message={message}>
      <>
        <PreferenceHandler
          title={<FormattedMessage id="MY_PROFILE.PHONE_CALLS" />}
          body={
            <div>
              <FormattedMessage id="MY_PROFILE.PHONE_CALLS_BODY" />
              {formatPhoneToString(
                getCustomerPhoneNumber(contact!.phoneNumbers)
              )}
            </div>
          }
        />
        <PreferenceHandler
          title={<FormattedMessage id="MY_PROFILE.ALERTS" />}
          body={<FormattedMessage id="MY_PROFILE.ALERTS_BODY" />}
        />
        <PreferenceHandler
          title={<FormattedMessage id="MY_PROFILE.WRITTEN_CORRESPONDENCE" />}
          body={
            <FormattedMessage
              id="MY_PROFILE.SET_TO_RECEIVE"
              values={{
                paper: (
                  <span className={styles.correspondence}>
                    <FormattedMessage id={'MY_PROFILE.PAPER'} />
                  </span>
                ),
                via: (
                  <span className={styles.correspondence}>
                    <FormattedMessage id={'MY_PROFILE.VIA'} />
                  </span>
                ),
                post: (
                  <span className={styles.correspondence}>
                    <FormattedMessage id={'MY_PROFILE.POST'} />
                  </span>
                )
              }}
            />
          }
        />
      </>
    </MyProfileTab>
  </>
);

export const CommunicationPreferences = withAppConfig(
  CommunicationPreferencesComponent
);
