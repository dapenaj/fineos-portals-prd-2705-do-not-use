import { MyProfile } from './my-profile.component';

export * from './communication-preferences';
export * from './employment-details';
export * from './payment-preferences';
export * from './personal-details';
export * from './ui';

export default MyProfile;
