import React from 'react';

import { MyProfileMessage } from '../my-profile-message';

import styles from './my-profile-tab.module.scss';

type MyProfileTabProps = {
  message: string;
};

export const MyProfileTab: React.FC<MyProfileTabProps> = ({
  children,
  message
}) => (
  <div className={styles.wrapper}>
    <div className={styles.content}>{children}</div>
    <div className={styles.footer}>
      <MyProfileMessage>{message}</MyProfileMessage>
    </div>
  </div>
);
