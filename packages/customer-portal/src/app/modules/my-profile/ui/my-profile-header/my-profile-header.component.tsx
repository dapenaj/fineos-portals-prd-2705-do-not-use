import React from 'react';

import styles from './my-profile-header.module.scss';

export const MyProfileHeader: React.FC = ({ children }) => (
  <div data-test-el="my-profile-details-header" className={styles.title}>
    {children}
  </div>
);
