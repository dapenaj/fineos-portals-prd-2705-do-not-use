import React from 'react';

import styles from './my-profile-message.module.scss';

export const MyProfileMessage: React.FC = ({ children }) => (
  <div data-test-el="my-profile-message" className={styles.message}>
    {children}
  </div>
);
