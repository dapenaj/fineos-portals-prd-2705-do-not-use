import React from 'react';
import { FormattedMessage } from 'react-intl';

import { AppConfig } from '../../../shared/types';
import { withAppConfig } from '../../../shared/contexts';
import { ContentLayout } from '../../../shared/layouts';
import { getCurrentCustomerOccupation } from '../../intake';
import { MyProfileHeader, MyProfileTab } from '../ui';

import { EmploymentDetailsHandler } from './employment-details-handler';

type EmploymentDetailsContainerProps = {
  config: AppConfig;
};

export const EmploymentDetailsComponent: React.FC<EmploymentDetailsContainerProps> = ({
  config: {
    clientConfig: {
      myProfile: { message }
    },
    customerDetail: { occupation, customerOccupations }
  }
}) => (
  <>
    <MyProfileHeader>
      <FormattedMessage id="MY_PROFILE.EMPLOYMENT_DETAILS" />
    </MyProfileHeader>
    <MyProfileTab message={message}>
      <ContentLayout direction="column">
        <EmploymentDetailsHandler
          occupation={occupation}
          customerOccupation={getCurrentCustomerOccupation(customerOccupations)}
        />
      </ContentLayout>
    </MyProfileTab>
  </>
);

export const EmploymentDetails = withAppConfig(EmploymentDetailsComponent);
