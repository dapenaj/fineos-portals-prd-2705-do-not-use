import React from 'react';
import { CustomerOccupation } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'antd';

import { PropertyItem } from '../../../../shared/ui';

type ClaimHandlerProps = {
  customerOccupation: CustomerOccupation;
};

export const ClaimHandler: React.FC<ClaimHandlerProps> = ({
  customerOccupation: {
    employeeId,
    dateJobBegan,
    employer,
    jobTitle,
    hoursWorkedPerWeek
  }
}) => (
  <>
    <Row type="flex">
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.CUSTOMER_ID" />}
        >
          {employeeId}
        </PropertyItem>
      </Col>
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.DATE_OF_HIRE" />}
        >
          {dateJobBegan}
        </PropertyItem>
      </Col>
    </Row>
    <Row>
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.EMPLOYER" />}
        >
          {employer}
        </PropertyItem>
      </Col>
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.JOB_TITLE" />}
        >
          {jobTitle}
        </PropertyItem>
      </Col>
    </Row>
    <Row>
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.WEEKLY_HOURS_WORKED" />}
        >
          {hoursWorkedPerWeek}
        </PropertyItem>
      </Col>
    </Row>
  </>
);
