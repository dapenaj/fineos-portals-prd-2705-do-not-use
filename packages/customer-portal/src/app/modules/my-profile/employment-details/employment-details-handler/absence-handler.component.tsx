import React from 'react';
import { Occupation } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'antd';

import { PropertyItem } from '../../../../shared/ui';

type AbsenceHandlerProps = {
  occupation: Occupation;
};

export const AbsenceHandler: React.FC<AbsenceHandlerProps> = ({
  occupation: {
    employeeId,
    jobTitle,
    workPattern,
    dateOfHire,
    department,
    hoursWorkedPerWeek,
    hoursWorkedPerYear,
    manager,
    workSite,
    workCity,
    workState
  }
}) => (
  <>
    <Row type="flex">
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.CUSTOMER_ID" />}
        >
          {employeeId}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.JOB_TITLE" />}
        >
          {jobTitle}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.WORK_PATTERN" />}
        >
          {workPattern}
        </PropertyItem>
      </Col>
    </Row>

    <Row>
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.DATE_OF_HIRE" />}
        >
          {dateOfHire}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.DEPARTMENT" />}
        >
          {department}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.WEEKLY_HOURS_WORKED" />}
        >
          {hoursWorkedPerWeek}
        </PropertyItem>
      </Col>
    </Row>

    <Row>
      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.MANAGER" />}
        >
          {manager}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.WORK_SITE" />}
        >
          {workSite}
        </PropertyItem>

        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.WORK_CITY" />}
        >
          {workCity}
        </PropertyItem>

        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.WORK_STATE" />}
        >
          {workState}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={8}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.YEARLY_HOURS_WORKED" />}
        >
          {hoursWorkedPerYear}
        </PropertyItem>
      </Col>
    </Row>
  </>
);
