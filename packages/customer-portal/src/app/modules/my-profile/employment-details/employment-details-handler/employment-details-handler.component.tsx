import React from 'react';
import { Occupation, CustomerOccupation } from 'fineos-js-api-client';

import { AuthorizationService } from '../../../../shared/services';

import { AbsenceHandler } from './absence-handler.component';
import { ClaimHandler } from './claims-handler.component';

type EmploymentDetailsHandlerProps = {
  occupation: Occupation | null;
  customerOccupation: CustomerOccupation | null;
};

export const EmploymentDetailsHandler: React.FC<EmploymentDetailsHandlerProps> = ({
  occupation,
  customerOccupation
}) => (
  <>
    {AuthorizationService.getInstance().isAbsenceUser ? (
      <AbsenceHandler occupation={occupation!} />
    ) : (
      <ClaimHandler customerOccupation={customerOccupation!} />
    )}
  </>
);
