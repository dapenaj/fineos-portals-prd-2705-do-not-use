import React from 'react';
import { Col, Tabs } from 'antd';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';

import { ContentLayout } from '../../shared/layouts';
import { PageTitle, Row } from '../../shared/ui';

import { EmploymentDetails } from './employment-details';
import { PersonalDetails } from './personal-details';
import { PaymentPreferences } from './payment-preferences';
import styles from './my-profile.module.scss';

const { TabPane } = Tabs;

type MyProfileState = {
  activeKey: string;
};

export class MyProfile extends React.Component<{}, MyProfileState> {
  state = {
    activeKey: 'personal-details'
  };

  handleTabChange = (activeKey: string) => this.setState({ activeKey });

  render() {
    const { activeKey } = this.state;

    return (
      <div data-test-el="my-profile">
        <ContentLayout direction="column">
          <PageTitle>
            <FormattedMessage id="MY_PROFILE.TITLE" />
          </PageTitle>
          <Row>
            <Col xs={24}>
              <Tabs
                defaultActiveKey="personal-details"
                tabPosition={'left'}
                onChange={this.handleTabChange}
              >
                <TabPane
                  tab={
                    <div className={styles.tab}>
                      <div
                        className={classnames(
                          styles.icon,
                          styles.personalDetail,
                          activeKey === 'personal-details' &&
                            styles.personalDetailActive
                        )}
                      />
                      <span className={styles.text}>
                        <FormattedMessage id="MY_PROFILE.PERSONAL_DETAILS" />
                      </span>
                    </div>
                  }
                  key="personal-details"
                >
                  <PersonalDetails />
                </TabPane>
                <TabPane
                  tab={
                    <div
                      data-test-el="employment-details"
                      className={styles.tab}
                    >
                      <div
                        className={classnames(
                          styles.icon,
                          styles.employmentDetails,
                          activeKey === 'employment-details' &&
                            styles.employmentDetailsActive
                        )}
                      />
                      <span className={styles.text}>
                        <FormattedMessage id="MY_PROFILE.EMPLOYMENT_DETAILS" />
                      </span>
                    </div>
                  }
                  key="employment-details"
                >
                  <EmploymentDetails />
                </TabPane>
                <TabPane
                  tab={
                    <div
                      data-test-el="payment-preferences"
                      className={styles.tab}
                    >
                      <div
                        className={classnames(
                          styles.icon,
                          styles.paymentPreferences,
                          activeKey === 'payment-preference' &&
                            styles.paymentPreferencesActive
                        )}
                      />
                      <span className={styles.text}>
                        <FormattedMessage id="MY_PROFILE.PAYMENT_PREFERENCES" />
                      </span>
                    </div>
                  }
                  key="payment-preference"
                >
                  <PaymentPreferences />
                </TabPane>
              </Tabs>
            </Col>
          </Row>
        </ContentLayout>
      </div>
    );
  }
}
