import React from 'react';
import { FormattedMessage } from 'react-intl';

import { AppConfig } from '../../../shared/types';
import { withAppConfig } from '../../../shared/contexts';
import { MyProfileHeader, MyProfileTab } from '../ui';

import { PersonalDetailsProperties } from './personal-details-properties.component';

type PersonalDetailsContainerProps = {
  config: AppConfig;
};

export const PersonalDetailsComponent: React.FC<PersonalDetailsContainerProps> = ({
  config: {
    clientConfig: {
      myProfile: { message }
    },
    customerDetail: { customer, contact }
  }
}) => (
  <>
    <MyProfileHeader>
      <FormattedMessage id="MY_PROFILE.PERSONAL_DETAILS" />
    </MyProfileHeader>
    <MyProfileTab message={message}>
      <PersonalDetailsProperties customer={customer!} contact={contact!} />
    </MyProfileTab>
  </>
);

export const PersonalDetails = withAppConfig(PersonalDetailsComponent);
