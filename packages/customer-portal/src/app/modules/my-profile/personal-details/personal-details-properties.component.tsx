import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import { Customer, Contact } from 'fineos-js-api-client';

import { ContentLayout } from '../../../shared/layouts';
import { PropertyItem, AddressRenderer } from '../../../shared/ui';
import { getCustomerAddress, formatPhoneToString } from '../../../shared/utils';

type PersonalDetailsPropertiesProps = {
  customer: Customer;
  contact: Contact;
};

export const PersonalDetailsProperties: React.FC<PersonalDetailsPropertiesProps> = ({
  customer,
  contact: { emailAddresses, phoneNumbers }
}) => (
  <ContentLayout noPadding={true} direction="column">
    <Row type="flex">
      <Col xs={24} sm={12}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.FULL_NAME" />}
        >
          {customer.firstName} {customer.lastName}
        </PropertyItem>
      </Col>

      <Col xs={24} sm={12}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.DATE_OF_BIRTH" />}
        >
          {customer.dateOfBirth}
        </PropertyItem>
      </Col>
    </Row>

    <Row>
      <Col xs={24} sm={12}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.ADDRESS" />}
        >
          <AddressRenderer address={getCustomerAddress(customer)} />
        </PropertyItem>
      </Col>

      <Col xs={24} sm={12}>
        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.EMAIL" />}
        >
          {!!emailAddresses &&
            emailAddresses.map(({ emailAddress }, index) => (
              <div key={index}>{emailAddress}</div>
            ))}
        </PropertyItem>

        <PropertyItem
          additionalPadding={true}
          label={<FormattedMessage id="MY_PROFILE.PHONE_NUMBER" />}
        >
          {!!phoneNumbers &&
            phoneNumbers.map((phone, index) => (
              <div key={index}>{formatPhoneToString(phone)}</div>
            ))}
        </PropertyItem>
      </Col>
    </Row>
  </ContentLayout>
);
