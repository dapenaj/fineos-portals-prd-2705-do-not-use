import React from 'react';
import { PaymentPreference } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';

import { PropertyItem } from '../../../../shared/ui';

import styles from './payment-preferences-properties.module.scss';

type PaymentPreferencesPropertiesProps = {
  paymentPreference: PaymentPreference;
};

export const PaymentPreferencesProperties: React.FC<PaymentPreferencesPropertiesProps> = ({
  paymentPreference: { paymentMethod, accountDetails }
}) => (
  <>
    {paymentMethod === 'Check' ? (
      <>
        <div className={styles.container}>
          <PropertyItem
            additionalPadding={true}
            label={
              <FormattedMessage id="MY_PROFILE.PAYMENT_PREFERENCE_METHOD" />
            }
          >
            {paymentMethod}
          </PropertyItem>
        </div>
      </>
    ) : (
      <>
        <div className={styles.container}>
          <PropertyItem
            additionalPadding={true}
            label={
              <FormattedMessage id="MY_PROFILE.PAYMENT_PREFERENCE_METHOD" />
            }
          >
            {paymentMethod}
          </PropertyItem>
        </div>

        <div className={styles.container}>
          <PropertyItem
            additionalPadding={true}
            label={<FormattedMessage id="MY_PROFILE.ROUTING" />}
          >
            {accountDetails && accountDetails.routingNumber}
          </PropertyItem>
          <PropertyItem
            additionalPadding={true}
            label={<FormattedMessage id="MY_PROFILE.ACCOUNT_NUMBER" />}
          >
            {accountDetails && accountDetails.accountNo}
          </PropertyItem>
          <PropertyItem
            additionalPadding={true}
            label={<FormattedMessage id="MY_PROFILE.ACCOUNT_TYPE" />}
          >
            {accountDetails && accountDetails.accountType}
          </PropertyItem>
        </div>
      </>
    )}
  </>
);
