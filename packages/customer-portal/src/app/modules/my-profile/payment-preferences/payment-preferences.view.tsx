import React from 'react';
import { FormattedMessage } from 'react-intl';
import { PaymentPreference } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { MyProfileHeader, MyProfileTab } from '../ui';
import { getDefaultPaymentPreference } from '../../../shared/utils';
import { ContentRenderer } from '../../../shared/ui';
import { withAppConfig } from '../../../shared/contexts';
import { AppConfig } from '../../../shared/types';

import { PaymentPreferencesProperties } from './payment-preferences-properties';

type PaymentPreferencesViewProps = {
  paymentPreferences: PaymentPreference[] | null;
  loading: boolean;
  config: AppConfig;
};

export const PaymentPreferencesViewComponent: React.FC<PaymentPreferencesViewProps> = ({
  paymentPreferences,
  config: {
    clientConfig: {
      myProfile: { message }
    }
  },
  loading
}) => (
  <>
    <MyProfileHeader>
      <FormattedMessage id="MY_PROFILE.PAYMENT_PREFERENCES" />
    </MyProfileHeader>
    <MyProfileTab message={message}>
      <ContentRenderer loading={loading} isEmpty={_isEmpty(paymentPreferences)}>
        {!_isEmpty(paymentPreferences) && (
          <PaymentPreferencesProperties
            paymentPreference={
              getDefaultPaymentPreference(paymentPreferences!) ||
              paymentPreferences![0]
            }
          />
        )}
      </ContentRenderer>
    </MyProfileTab>
  </>
);

export const PaymentPreferencesView = withAppConfig(
  PaymentPreferencesViewComponent
);
