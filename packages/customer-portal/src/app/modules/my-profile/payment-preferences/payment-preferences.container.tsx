import React from 'react';
import { PaymentPreference, CustomerService } from 'fineos-js-api-client';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { showNotification } from '../../../shared/ui';

import { PaymentPreferencesView } from './payment-preferences.view';

type PaymentPreferenceContainerState = {
  paymentPreferences: PaymentPreference[];
  loading: boolean;
};

type PaymentPreferenceContainerProps = IntlProps;

export class PaymentPreferenceContainer extends React.Component<
  PaymentPreferenceContainerProps,
  PaymentPreferenceContainerState
> {
  state: PaymentPreferenceContainerState = {
    paymentPreferences: [],
    loading: false
  };

  customerService: CustomerService = CustomerService.getInstance();

  async componentDidMount() {
    const {
      intl: { formatMessage }
    } = this.props;

    try {
      this.setState({ loading: true });

      const paymentPreferences = await this.customerService.findPaymentPreferences();

      this.setState({ paymentPreferences });
    } catch (error) {
      const title = formatMessage({
        id: 'MY_PROFILE.PAYMENT_PREFERENCES_ERROR_MESSAGE'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    const { loading, paymentPreferences } = this.state;

    return (
      <PaymentPreferencesView
        paymentPreferences={paymentPreferences}
        loading={loading}
      />
    );
  }
}

export const PaymentPreferences = injectIntl(PaymentPreferenceContainer);
