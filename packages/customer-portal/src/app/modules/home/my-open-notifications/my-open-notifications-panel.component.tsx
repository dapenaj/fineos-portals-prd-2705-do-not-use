import React from 'react';
import { Collapse } from 'antd';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';

import { ContentRenderer } from '../../../shared/ui';

import { OpenNotification } from './open-notification';

const { Panel } = Collapse;

type MyOpenNotificationsPanelProps = {
  loading: boolean;
  notifications: NotificationCaseSummary[] | null;
};

export const MyOpenNotificationsPanel: React.FC<MyOpenNotificationsPanelProps> = ({
  notifications,
  loading
}) => (
  <div data-test-el="my-open-notifications">
    <Collapse defaultActiveKey={['1']} accordion={true}>
      <Panel
        header={
          !_isEmpty(notifications) ? (
            <FormattedMessage
              id="HOME.MY_OPEN_NOTIFICATIONS.TITLE"
              values={{ count: notifications!.length }}
            />
          ) : (
            <FormattedMessage id="HOME.MY_OPEN_NOTIFICATIONS.TITLE_EMPTY" />
          )
        }
        key="1"
      >
        <ContentRenderer
          loading={loading}
          isEmpty={_isEmpty(notifications)}
          emptyTitle={
            <FormattedMessage id="HOME.MY_OPEN_NOTIFICATIONS.NO_NOTIFICATIONS" />
          }
        >
          {!_isEmpty(notifications) &&
            notifications!.map((notification, index) => (
              <OpenNotification key={index} notification={notification} />
            ))}
        </ContentRenderer>
      </Panel>
    </Collapse>
  </div>
);
