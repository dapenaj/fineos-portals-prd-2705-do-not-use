import React from 'react';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { FormattedMessage } from 'react-intl';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { formatDateForDisplay } from '../../../../../shared/utils';

import styles from './open-notification-header.module.scss';

type OpenNotificationHeaderProps = {
  notification: NotificationCaseSummary;
  title?: string;
  withBorder?: boolean;
};

export const OpenNotificationHeader: React.FC<OpenNotificationHeaderProps> = ({
  notification: { notificationCaseId, createdDate },
  title,
  withBorder
}) => (
  <div
    className={classnames(styles.wrapper, withBorder && styles.withBorder)}
    data-test-el="notification-link"
  >
    <div>
      <NavLink
        to={`/notifications/${notificationCaseId}`}
        className={styles.header}
      >
        {notificationCaseId}
        {!!title && <span> | {title}</span>}
      </NavLink>
    </div>
    <div>
      <span className={styles.property}>
        <FormattedMessage id="HOME.MY_OPEN_NOTIFICATIONS.NOTIFIED_ON" />
      </span>
      <span className={styles.label}>{formatDateForDisplay(createdDate)}</span>
    </div>
  </div>
);
