import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import { Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { Row } from '../../../../../shared/ui';
import { getNotificationStatus } from '../../../../../shared/utils';
import { ContentLayout } from '../../../../../shared/layouts';
import {
  NotificationStatusResult,
  NotificationPendingStatus
} from '../../../../../shared/types';

import { OpenNotificationStatusColumn } from './status-column';
import { OpenNotificationAssessmentColumn } from './assessment-column';
import styles from './open-notification-body.module.scss';

type OpenNotificationBodyProps = {
  notification: NotificationCaseSummary;
};

export const OpenNotificationBody: React.FC<OpenNotificationBodyProps> = ({
  notification: { status, claims, absences, accommodations }
}) => {
  const showLabel =
    (!!claims && !!claims.length) ||
    (!!absences && !!absences.length) ||
    (!!accommodations && !!accommodations.length);
  return (
    <ContentLayout direction="column" noPadding={true}>
      <Row>
        <div className={styles.wrapper}>
          <Col xs={24} sm={12}>
            <OpenNotificationStatusColumn
              label={
                <FormattedMessage id="HOME.MY_OPEN_NOTIFICATIONS.CASE_PROGRESS" />
              }
              status={
                status === NotificationPendingStatus.REGISTRATION
                  ? NotificationStatusResult.PENDING
                  : getNotificationStatus(claims, absences, accommodations)
              }
            />
          </Col>
          <Col xs={24} sm={12}>
            <OpenNotificationAssessmentColumn
              label={
                showLabel && (
                  <FormattedMessage id="HOME.MY_OPEN_NOTIFICATIONS.ASSESED_FOR" />
                )
              }
              claims={claims}
              absences={absences}
              accommodations={accommodations}
            />
          </Col>
        </div>
      </Row>
    </ContentLayout>
  );
};
