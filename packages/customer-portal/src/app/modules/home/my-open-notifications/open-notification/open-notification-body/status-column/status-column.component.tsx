import React from 'react';
import { FormattedMessage } from 'react-intl';

import { PropertyItem } from '../../../../../../shared/ui';
import { getCaseProgress } from '../../../../../../shared/utils';
import { NotificationStatusResult } from '../../../../../../shared/types';

import { OpenNotificationStatus } from './open-notification-status';
import styles from './status-column.module.scss';

type OpenNotificationStatusColumnProps = {
  status: NotificationStatusResult;
  label: React.ReactNode;
};

export const OpenNotificationStatusColumn: React.FC<OpenNotificationStatusColumnProps> = ({
  status,
  label
}) => (
  <div className={styles.column}>
    <OpenNotificationStatus status={status} />
    <div className={styles.progress} data-test-el="case-progress">
      <PropertyItem label={label}>
        <FormattedMessage id={getCaseProgress(status)} />
      </PropertyItem>
    </div>
  </div>
);
