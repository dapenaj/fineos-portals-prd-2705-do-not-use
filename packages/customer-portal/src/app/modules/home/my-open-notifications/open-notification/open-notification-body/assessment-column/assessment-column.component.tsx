import React from 'react';
import {
  ClaimSummary,
  AbsenceSummary,
  NotificationAccommodationSummary
} from 'fineos-js-api-client';

import { PropertyItem, CaseAssessment } from '../../../../../../shared/ui';

type OpenNotificationAssessmentColumnProps = {
  claims?: ClaimSummary[];
  absences?: AbsenceSummary[];
  accommodations?: NotificationAccommodationSummary[];
  label?: React.ReactNode;
  fullWidth?: boolean;
};

export const OpenNotificationAssessmentColumn: React.FC<OpenNotificationAssessmentColumnProps> = ({
  absences,
  claims,
  accommodations,
  label,
  fullWidth
}) => (
  <PropertyItem label={label} fullWidth={fullWidth}>
    <CaseAssessment
      claims={claims}
      absences={absences}
      accommodations={accommodations}
    />
  </PropertyItem>
);
