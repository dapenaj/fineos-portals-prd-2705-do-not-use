import React from 'react';
import classnames from 'classnames';
import { Icon } from 'antd';

import { NotificationStatusResult } from '../../../../../../../shared/types';

import styles from './open-notification-status.module.scss';

type OpenNotificationStatusProps = {
  status: NotificationStatusResult;
};

export const OpenNotificationStatus: React.FC<OpenNotificationStatusProps> = ({
  status
}) => (
  <div className={styles.wrapper}>
    <div
      className={classnames(styles.circle, {
        [styles.pendingIcon]: status === NotificationStatusResult.PENDING
      })}
    >
      {status === NotificationStatusResult.PENDING && (
        <Icon type="clock-circle" className={styles.icon} />
      )}
    </div>
    <div
      className={classnames(styles.circle, {
        [styles.decidingIcon]: status === NotificationStatusResult.SOME_DECIDED
      })}
    >
      {status === NotificationStatusResult.SOME_DECIDED && (
        <Icon type="redo" className={styles.icon} />
      )}
    </div>
    <div
      className={classnames(styles.circle, {
        [styles.decidedIcon]: status === NotificationStatusResult.DECIDED
      })}
    >
      {status === NotificationStatusResult.DECIDED && (
        <Icon type="file-done" className={styles.icon} />
      )}
    </div>
  </div>
);
