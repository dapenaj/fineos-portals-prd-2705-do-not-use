import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { OpenNotificationHeader } from './open-notification-header';
import { OpenNotificationBody } from './open-notification-body';
import styles from './open-notification.module.scss';

type OpenNotificationProps = {
  notification: NotificationCaseSummary;
};

export const OpenNotification: React.FC<OpenNotificationProps> = ({
  notification
}) => (
  <div className={styles.wrapper} data-test-el="open-notification">
    <OpenNotificationHeader
      notification={notification}
      title={notification.notificationReason}
      withBorder={true}
    />
    <OpenNotificationBody notification={notification} />
  </div>
);
