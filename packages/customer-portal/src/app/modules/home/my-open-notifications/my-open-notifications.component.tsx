import React from 'react';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { MyOpenNotificationsPanel } from './my-open-notifications-panel.component';

type MyOpenNotificationsProps = {
  notifications: NotificationCaseSummary[] | null;
  loading: boolean;
};

export const MyOpenNotifications: React.FC<MyOpenNotificationsProps> = ({
  notifications,
  loading
}) => (
  <MyOpenNotificationsPanel notifications={notifications} loading={loading} />
);
