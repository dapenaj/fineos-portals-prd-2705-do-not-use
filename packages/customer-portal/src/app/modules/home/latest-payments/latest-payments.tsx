import React from 'react';
import { ClaimService } from 'fineos-js-api-client';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { flatten as _flatten, orderBy as _orderBy } from 'lodash';
import moment from 'moment';

import { showNotification } from '../../../shared/ui';
import { CasePayment } from '../../../shared/types';
import { parseDate } from '../../../shared/utils';
import { PAYMENTS_DISPLAY_COUNT } from '../../../shared/constants';

import { LatestPaymentsList } from './latest-payments-list';

type LatestPaymentsState = {
  payments: CasePayment[] | null;
  loading: boolean;
};

type LatestPaymentsProps = IntlProps;

export class LatestPaymentsComponent extends React.Component<
  LatestPaymentsProps,
  LatestPaymentsState
> {
  claimService: ClaimService = ClaimService.getInstance();

  state: LatestPaymentsState = {
    payments: null,
    loading: false
  };

  async componentDidMount() {
    const {
      intl: { formatMessage }
    } = this.props;

    this.setState({ loading: true });

    try {
      const claims = await this.claimService.findClaims();

      if (!!claims && !!claims.length) {
        const casePayments = await Promise.all(
          claims.map(async ({ claimId }) => {
            const payments = await this.claimService.findPayments(claimId);

            return !!payments && !!payments.length
              ? payments.map(payment => ({
                  ...payment,
                  claimId
                }))
              : [];
          })
        );

        const combinePayments: CasePayment[] = _flatten(casePayments);

        this.setState({
          payments:
            !!combinePayments && !!combinePayments.length ? combinePayments : []
        });
      }
    } catch (error) {
      const title = formatMessage({ id: 'HOME.LATEST_PAYMENTS.FETCH_ERROR' });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    const { payments, loading } = this.state;

    const orderedPayments = _orderBy(
      payments,
      'paymentDate',
      'desc'
    ).filter(payment =>
      parseDate(payment.paymentDate).isAfter(moment().subtract(1, 'year'))
    );

    const showViewAll =
      orderedPayments.length > PAYMENTS_DISPLAY_COUNT ||
      (!!payments && payments.length > orderedPayments.length);

    return (
      <div data-test-el="latest-payments-wrapper">
        <LatestPaymentsList
          payments={orderedPayments}
          loading={loading}
          showViewAll={showViewAll}
        />
      </div>
    );
  }
}

export const LatestPayments = injectIntl(LatestPaymentsComponent);
