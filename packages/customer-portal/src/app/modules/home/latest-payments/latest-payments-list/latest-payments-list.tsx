import React from 'react';
import { Collapse } from 'antd';
import { FormattedMessage } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { isEmpty as _isEmpty } from 'lodash';

import { Payment, ContentRenderer } from '../../../../shared/ui';
import { CasePayment } from '../../../../shared/types';
import { PAYMENTS_DISPLAY_COUNT } from '../../../../shared/constants';

import styles from './latest-payments-list.module.scss';

const { Panel } = Collapse;

type LatestPaymentListProps = {
  payments: CasePayment[] | null;
  loading: boolean;
  showViewAll: boolean;
};

export const LatestPaymentsList: React.FC<LatestPaymentListProps> = ({
  payments,
  loading,
  showViewAll
}) => (
  <Collapse defaultActiveKey={['1']} accordion={true}>
    <Panel
      header={
        <>
          <FormattedMessage id="HOME.LATEST_PAYMENTS.TITLE" />

          <NavLink
            data-test-el="payment-view-all-selector"
            className={styles.link}
            to={`/my-payments`}
          >
            {showViewAll && (
              <FormattedMessage id="HOME.LATEST_PAYMENTS.VIEW_ALL" />
            )}
          </NavLink>
        </>
      }
      key="1"
    >
      <ContentRenderer
        loading={loading}
        isEmpty={_isEmpty(payments)}
        emptyTitle={
          <FormattedMessage id={'HOME.LATEST_PAYMENTS.NO_PAYMENTS'} />
        }
        data-test-el="latest-payments"
      >
        {!_isEmpty(payments) &&
          payments!
            .slice(0, PAYMENTS_DISPLAY_COUNT)
            .map((payment, index) => (
              <Payment
                key={index}
                payment={payment}
                showRelatesTo={true}
                className={styles.margin}
              />
            ))}
      </ContentRenderer>
    </Panel>
  </Collapse>
);
