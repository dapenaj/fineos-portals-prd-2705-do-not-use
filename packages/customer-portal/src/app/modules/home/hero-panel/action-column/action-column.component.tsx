import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from 'antd';
import { NavLink } from 'react-router-dom';

import { ElevatedCard } from '../../../../shared/ui';

import styles from './action-column.module.scss';

type ActionColumnProps = {
  title: string;
  message: string;
};

export const ActionColumn: React.FC<ActionColumnProps> = ({
  title,
  message
}) => (
  <div className={styles.wrapper}>
    <ElevatedCard>
      <div className={styles.content}>
        <div className={styles.message}>
          <h1 className={styles.title}>{title}</h1>
          <p>{message}</p>
          <NavLink
            to={{ pathname: '/intake', state: { prevPath: '/' } }}
            className={styles.action}
          >
            <Button
              className={styles.actionBtn}
              type="primary"
              block={true}
              data-test-el="action-button"
            >
              <FormattedMessage id="HOME.HERO_ACTION" />
            </Button>
          </NavLink>
        </div>
        <div className={styles.image} />
      </div>
    </ElevatedCard>
  </div>
);
