import React from 'react';

import { ContentLayout } from '../../../shared/layouts';
import { AppConfig } from '../../../shared/types';
import { withAppConfig } from '../../../shared/contexts';

import styles from './hero-panel.module.scss';
import { WelcomeColumn } from './welcome-column';
import { ActionColumn } from './action-column';

type HeroPanelProps = {
  config: AppConfig;
};

export const HeroPanelComponent: React.FC<HeroPanelProps> = ({
  config: {
    customerDetail: { customer },
    clientConfig: {
      home: { welcome, action }
    }
  }
}) => (
  <div className={styles.container}>
    <ContentLayout>
      <div className={styles.columns}>
        <WelcomeColumn customer={customer} welcomeMessage={welcome.message} />
        <ActionColumn title={action.title} message={action.message} />
      </div>
    </ContentLayout>
  </div>
);

export const HeroPanel = withAppConfig(HeroPanelComponent);
