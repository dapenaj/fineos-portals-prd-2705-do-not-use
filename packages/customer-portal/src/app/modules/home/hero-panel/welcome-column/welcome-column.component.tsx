import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Customer } from 'fineos-js-api-client';

import styles from './welcome-column.module.scss';

type WelcomeColumnProps = {
  customer: Customer | null;
  welcomeMessage: string;
};

export const WelcomeColumn: React.FC<WelcomeColumnProps> = ({
  customer,
  welcomeMessage
}) => (
  <div className={styles.wrapper}>
    <h1 className={styles.title}>
      {customer ? (
        <FormattedMessage
          id="HOME.WELCOME_MESSAGE_TITLE"
          values={{ name: customer.firstName }}
        />
      ) : (
        <FormattedMessage id="HOME.WELCOME_MESSAGE_TITLE_EMPTY" />
      )}
    </h1>
    <p>{welcomeMessage}</p>
  </div>
);
