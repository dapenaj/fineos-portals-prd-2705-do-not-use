import React from 'react';
import { Col } from 'antd';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import moment from 'moment';

import { ContentLayout } from '../../shared/layouts';
import { Row } from '../../shared/ui';
import { removeClosedNotifications } from '../../shared/utils';

import { HeroPanel } from './hero-panel';
import { MyOpenNotifications } from './my-open-notifications';
import { LatestPayments } from './latest-payments/latest-payments';
import { RequiredFromMe } from './required-from-me';

type HomeViewProps = {
  notifications: NotificationCaseSummary[] | null;
  loading: boolean;
};

export const HomeView: React.FC<HomeViewProps> = ({
  notifications,
  loading
}) => {
  const openNotifications = (!!notifications && notifications.length
    ? notifications
    : []
  )
    .filter(({ claims, absences, accommodations }) =>
      removeClosedNotifications(claims, absences, accommodations)
    )
    .sort((a, b) =>
      moment(b.notificationDate).diff(moment(a.notificationDate))
    );

  return (
    <div className="HomeView" data-test-el="home-view">
      <HeroPanel />

      <ContentLayout direction="column">
        <Row>
          <Col xs={24} lg={16}>
            <MyOpenNotifications
              notifications={openNotifications}
              loading={loading}
            />
            <LatestPayments />
          </Col>
          <Col xs={24} lg={8}>
            {!loading && <RequiredFromMe notifications={openNotifications} />}
          </Col>
        </Row>
      </ContentLayout>
    </div>
  );
};
