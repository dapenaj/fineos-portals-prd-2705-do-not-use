import React from 'react';
import { connect } from 'react-redux';

import { RootState } from '../../store';
import { fetchNotifications } from '../../store/notification/actions';

import { HomeView } from './home.view';

const mapStateToProps = ({ notification: { data, loading } }: RootState) => ({
  notifications: data,
  loading
});

const mapDispatchToProps = {
  fetchNotifications
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
type HomeProps = StateProps & DispatchProps;

export class HomeContainer extends React.Component<HomeProps> {
  componentDidMount() {
    this.props.fetchNotifications();
  }

  render() {
    const { loading, notifications } = this.props;

    return <HomeView notifications={notifications} loading={loading} />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
