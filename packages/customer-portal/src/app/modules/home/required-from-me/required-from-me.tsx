import React from 'react';
import { NotificationCaseSummary, DocumentService } from 'fineos-js-api-client';
import { flatten as _flatten } from 'lodash';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { UploadDocument } from '../../../shared/types';
import { RequiredFromMePanel, showNotification } from '../../../shared/ui';

type RequiredFromMeProps = {
  notifications: NotificationCaseSummary[] | null;
} & IntlProps;

type RequiredFromMeState = {
  documents: UploadDocument[] | null;
  loading: boolean;
  additionalCaseText: boolean;
};

export class RequiredFromMeComponent extends React.Component<
  RequiredFromMeProps,
  RequiredFromMeState
> {
  documentService: DocumentService = DocumentService.getInstance();

  state: RequiredFromMeState = {
    documents: null,
    loading: false,
    additionalCaseText: true
  };

  async componentDidMount() {
    const { notifications } = this.props;

    if (!!notifications && !!notifications.length) {
      await this.fetchSupportingEvidence();
    }
  }

  fetchSupportingEvidence = async () => {
    const {
      notifications,
      intl: { formatMessage }
    } = this.props;

    this.setState({ loading: true });

    try {
      const evidences = await Promise.all(
        notifications!.map(
          async ({ notificationCaseId }) =>
            await this.documentService.getSupportingEvidence(notificationCaseId)
        )
      );

      const documents = _flatten(evidences).map((evidence, index) => ({
        id: index,
        caseId: evidence.uploadCaseNumber,
        evidence
      }));

      this.setState({
        documents
      });
    } catch (error) {
      const title = formatMessage({
        id: 'SUPPORTING_EVIDENCE.FETCH_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  };

  render() {
    const { documents, loading, additionalCaseText } = this.state;
    const { notifications } = this.props;

    return (
      <>
        {!!notifications && !!notifications.length && (
          <RequiredFromMePanel
            loading={loading}
            documents={documents}
            confirmReturns={null}
            additionalCaseText={additionalCaseText}
            onRefreshRequiredFromMe={this.fetchSupportingEvidence}
          />
        )}
      </>
    );
  }
}

export const RequiredFromMe = injectIntl(RequiredFromMeComponent);
