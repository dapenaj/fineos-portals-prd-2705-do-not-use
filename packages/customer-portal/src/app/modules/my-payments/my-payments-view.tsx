import React from 'react';
import { Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { ContentLayout } from '../../shared/layouts';
import { PageTitle, Row } from '../../shared/ui';
import { CasePayment } from '../../shared/types';

import { MyPaymentsList } from './my-payments-list/my-payments-list';

type MyPaymentViewProps = {
  payments: CasePayment[] | null;
  loading: boolean;
};

export const MyPaymentsView: React.FC<MyPaymentViewProps> = ({
  payments,
  loading
}) => (
  <ContentLayout direction="column" data-test-el="my-payments">
    <PageTitle>
      <FormattedMessage id="NAVBAR_LINKS.MY_PAYMENTS" />
    </PageTitle>

    <Row>
      <Col xs={24}>
        <MyPaymentsList payments={payments} loading={loading} />
      </Col>
    </Row>
  </ContentLayout>
);
