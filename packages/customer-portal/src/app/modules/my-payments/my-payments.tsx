import React from 'react';
import { ClaimService } from 'fineos-js-api-client';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { flatten as _flatten, orderBy as _orderBy } from 'lodash';

import { showNotification } from '../../shared/ui';
import { CasePayment } from '../../shared/types';

import { MyPaymentsView } from './my-payments-view';

type MyPaymentsState = {
  payments: CasePayment[] | null;
  loading: boolean;
};

type MyPaymentsProps = IntlProps;

export class MyPaymentsComponents extends React.Component<
  MyPaymentsProps,
  MyPaymentsState
> {
  claimService: ClaimService = ClaimService.getInstance();

  state: MyPaymentsState = {
    payments: null,
    loading: false
  };

  async componentDidMount() {
    const {
      intl: { formatMessage }
    } = this.props;

    this.setState({ loading: true });

    try {
      const claims = await this.claimService.findClaims();

      if (!!claims && !!claims.length) {
        const casePayments = await Promise.all(
          claims.map(async ({ claimId }) => {
            const payments = await this.claimService.findPayments(claimId);

            return !!payments && !!payments.length
              ? payments.map(payment => ({
                  ...payment,
                  claimId
                }))
              : [];
          })
        );

        const combinePayments: CasePayment[] = _flatten(casePayments);

        this.setState({
          payments: combinePayments
        });
      }
    } catch (error) {
      const title = formatMessage({ id: 'HOME.LATEST_PAYMENTS.FETCH_ERROR' });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    const { payments, loading } = this.state;

    const orderedPayments: CasePayment[] = _orderBy(
      _flatten(payments),
      'paymentDate',
      'desc'
    );

    return <MyPaymentsView payments={orderedPayments} loading={loading} />;
  }
}

export const MyPayments = injectIntl(MyPaymentsComponents);
