import { MyPayments } from './my-payments';

export * from './my-payments-list';
export * from './my-payments-view';

export default MyPayments;
