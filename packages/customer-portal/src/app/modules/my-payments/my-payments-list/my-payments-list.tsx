import React from 'react';
import { FormattedMessage } from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';

import { ContentRenderer, Payment } from '../../../shared/ui';
import { CasePayment } from '../../../shared/types';

type MyPaymentListProps = {
  payments: CasePayment[] | null;
  loading: boolean;
};

export const MyPaymentsList: React.FC<MyPaymentListProps> = ({
  payments,
  loading
}) => (
  <ContentRenderer
    loading={loading}
    isEmpty={_isEmpty(payments)}
    emptyTitle={<FormattedMessage id={'PAYMENTS.NO_PAYMENTS'} />}
    withBorder={true}
    data-test-el="my-payments-list"
  >
    {!_isEmpty(payments) &&
      payments!.map((payment, index) => (
        <Payment key={index} payment={payment} showRelatesTo={true} />
      ))}
  </ContentRenderer>
);
