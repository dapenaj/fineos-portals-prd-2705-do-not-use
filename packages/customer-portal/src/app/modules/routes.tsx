import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import { Layout } from 'antd';

import { history } from '../config/history.config';
import { NavBar, Header } from '../shared/ui';

import Home from './home';
import Intake from './intake';
import NotificationDetail from './notifications/notification-detail/notification-detail.container';
import MyNotifications from './notifications/my-notifications/my-notifications.container';
import MyPayments from './my-payments';
import MyMessages from './my-messages';
import MyProfile from './my-profile';
import MyTeamLeaves from './my-team-leaves';
import styles from './routes.module.scss';

export const Routes: React.FC = () => (
  <ConnectedRouter history={history}>
    <Layout style={{ height: '100%' }}>
      <Header />

      <NavBar />

      <div className={styles.route}>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/intake" exact={true} component={Intake} />
          <Route
            path="/notifications"
            exact={true}
            component={MyNotifications}
          />
          <Route
            path="/notifications/:id"
            exact={true}
            component={NotificationDetail}
          />
          <Route path="/my-payments" exact={true} component={MyPayments} />
          <Route path="/my-messages" exact={true} component={MyMessages} />
          <Route
            path="/my-messages/:notificationCaseId"
            exact={true}
            component={MyMessages}
          />
          <Route path="/my-profile" exact={true} component={MyProfile} />
          <Route path="/my-team-leaves" exact={true} component={MyTeamLeaves} />
          <Redirect to="/" />
        </Switch>
      </div>
    </Layout>
  </ConnectedRouter>
);
