import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';
import { NotificationCaseSummary } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { PageTitle, NewMessage } from '../../../shared/ui';
import { MyMessagesBreadcrumb } from '../my-messages-breadcrumb';
import { MyMessagesSearch } from '../my-messages-search';

import styles from './my-messages-header.module.scss';

type MessageHeaderProps = {
  notificationCaseId: string | null;
  notifications: NotificationCaseSummary[] | null;
  disabled: boolean;
  onSearch: (searchValue?: string[]) => void;
  onMessageSent: () => void;
};

export const MyMessagesHeader: React.FC<MessageHeaderProps> = ({
  onSearch,
  notificationCaseId,
  notifications,
  disabled,
  onMessageSent
}) => (
  <PageTitle>
    <Row align="bottom" type="flex" gutter={16}>
      <Col
        span={16}
        lg={{ span: 8 }}
        md={{ span: 8 }}
        data-test-el="messages-header"
        className={styles.colPosition}
      >
        {!!notificationCaseId ? (
          <MyMessagesBreadcrumb notificationCaseId={notificationCaseId} />
        ) : (
          <FormattedMessage id="NAVBAR_LINKS.MY_MESSAGES" />
        )}
      </Col>

      <Col
        order={2}
        span={24}
        lg={{ span: 13 }}
        md={{ order: 1, span: 12 }}
        className={classnames(styles.search, styles.colPosition)}
      >
        <MyMessagesSearch onSearch={onSearch} disabled={disabled} />
      </Col>

      {!_isEmpty(notifications) && (
        <Col
          order={1}
          span={8}
          lg={{ span: 3 }}
          md={{ order: 2, span: 4 }}
          className={styles.colPosition}
        >
          <NewMessage
            type="all"
            notifications={notifications!}
            onMessageSent={onMessageSent}
            disabled={disabled}
          />
        </Col>
      )}
    </Row>
  </PageTitle>
);
