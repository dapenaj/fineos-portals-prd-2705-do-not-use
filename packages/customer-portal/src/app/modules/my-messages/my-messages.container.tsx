import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { isEmpty as _isEmpty } from 'lodash';

import { RootState } from '../../store';
import { parseUrl } from '../../shared/utils';
import {
  fetchMessages,
  markMessagesAsRead
} from '../../store/messages/actions';
import { fetchNotifications } from '../../store/notification/actions';
import { URLParseChar } from '../../shared/types';
import { ContentRenderer } from '../../shared/ui';

import { MyMessagesView } from './my-messages.view';

const mapStateToProps = (
  { notification, message }: RootState,
  {
    match: {
      params: { notificationCaseId }
    },
    location
  }: RouteComponentProps<{
    id: string;
    notificationCaseId: string;
    messageId: string;
  }>
) => ({
  notifications: notification.data,
  notificationsLoading: notification.loading,
  messages: message.fetch.data,
  messagesLoading: message.fetch.loading,
  notificationCaseId,
  messageId: location
    ? parseUrl(location.search, URLParseChar.QUERY_PARAM).messageId
    : null
});

const mapDispatchToProps = {
  // there is a bug in redux-connect typing, it doesn't work properly with redux-thunk actions
  fetchMessages: fetchMessages as () => void,
  markMessagesAsRead,
  fetchNotifications
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
type MyMessagesProps = StateProps & DispatchProps;

export class MyMessagesContainer extends React.Component<MyMessagesProps> {
  componentDidMount() {
    this.props.fetchNotifications();
  }

  render() {
    const {
      notificationsLoading,
      notifications,
      messages,
      messagesLoading
    } = this.props;

    const messageId = !!this.props.messageId
      ? Number(this.props.messageId)
      : null;
    const notificationCaseId = !!this.props.notificationCaseId
      ? String(this.props.notificationCaseId)
      : null;

    return (
      <ContentRenderer
        loading={notificationsLoading}
        isEmpty={_isEmpty(notifications)}
      >
        <MyMessagesView
          notifications={notifications!}
          markMessagesAsRead={this.props.markMessagesAsRead}
          selectedMessageId={Number(messageId)}
          notificationCaseId={notificationCaseId}
          messages={messages}
          loading={messagesLoading}
          fetchMessages={() => this.props.fetchMessages()}
        />
      </ContentRenderer>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyMessagesContainer);
