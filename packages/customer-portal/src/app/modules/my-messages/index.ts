import MyMessages from './my-messages.container';

export * from './my-messages.view';
export * from './my-messages-header';
export * from './my-messages-panels';
export * from './my-messages-search';
export * from './my-messages-breadcrumb';

export default MyMessages;
