import { withFormik } from 'formik';
import * as Yup from 'yup';

import { ReplyToMessageFormValue } from '../reply-to-message.type';

import { ReplyToMessageModalFormView } from './reply-to-message-modal-form.view';

type ReplyToMessageModalFormProps = {
  visible: boolean;
  subject: string;
  onCancel: () => void;
  onSubmit: (value: ReplyToMessageFormValue) => void;
};

const ReplyToMessageModalFormSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  ReplyToMessageFormValue
>> = Yup.object().shape({
  subject: Yup.string()
    .required('COMMON.VALIDATION.REQUIRED')
    .min(5, 'COMMON.VALIDATION.AT_LEAST'),
  narrative: Yup.string()
    .required('COMMON.VALIDATION.REQUIRED')
    .min(5, 'COMMON.VALIDATION.AT_LEAST')
});

const mapPropsToValues = ({ subject }: ReplyToMessageModalFormProps) => ({
  subject: `Re: ${subject}`,
  narrative: ''
});

export const ReplyToMessageModalForm = withFormik<
  ReplyToMessageModalFormProps,
  ReplyToMessageFormValue
>({
  mapPropsToValues,
  validationSchema: ReplyToMessageModalFormSchema,
  enableReinitialize: true,
  handleSubmit: (values, { props }) => props.onSubmit(values)
})(ReplyToMessageModalFormView);
