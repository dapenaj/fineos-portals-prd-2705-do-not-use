import React from 'react';
import { FormikProps, Form } from 'formik';

import {
  TextInput,
  FormLabel,
  TextAreaInput
} from '../../../../../../../shared/ui';
import { ReplyToMessageFormValue } from '../../reply-to-message.type';

import styles from './reply-to-message-form.module.scss';

type ReplyToMessageFormProps = FormikProps<ReplyToMessageFormValue>;

export const ReplyToMessageForm: React.FC<ReplyToMessageFormProps> = props => (
  <Form name="reply-to-messages-form" className={styles.form}>
    <TextInput
      name="subject"
      label={<FormLabel required={true} label="MESSAGES.SUBJECT" />}
      isDisabled={true}
      {...props}
    />

    <TextAreaInput
      name="narrative"
      label={<FormLabel required={true} label="MESSAGES.MESSAGE" />}
      {...props}
    />
  </Form>
);
