import React from 'react';
import { Card } from 'antd';
import { Message } from 'fineos-js-api-client';

import { MessageDetailHeader } from './message-detail-header';
import { MessageDetailBody } from './message-detail-body';
import styles from './message-details.module.scss';

type MyMessagesDetailsProps = {
  message: Message;
  chooseLogo: (msgOriginatesFromPortal: boolean) => React.ReactNode | string;
  onMessageSent: () => void;
};

export const MessageDetails: React.FC<MyMessagesDetailsProps> = ({
  message,
  chooseLogo,
  onMessageSent
}) => (
  <div className={styles.wrapper}>
    <Card
      className={styles.messageWrapper}
      title={<MessageDetailHeader message={message} chooseLogo={chooseLogo} />}
    >
      <MessageDetailBody onMessageSent={onMessageSent} message={message} />
    </Card>
  </div>
);
