import React from 'react';
import { Message, NewMessage } from 'fineos-js-api-client';
import {
  injectIntl,
  WrappedComponentProps as IntlProps,
  FormattedMessage
} from 'react-intl';
import { connect } from 'react-redux';
import { Button } from 'antd';

import { addMessage } from '../../../../../store/messages/actions';

import { ReplyToMessageModalForm } from './reply-to-message-modal-form';
import { ReplyToMessageFormValue } from './reply-to-message.type';

const mapDispatchToProps = {
  addMessage
};

type DispatchProps = typeof mapDispatchToProps;

type ReplyToMessageProps = {
  message: Message;
  onMessageSent: () => void;
} & IntlProps &
  DispatchProps;

type ReplyToMessageState = {
  showModal: boolean;
};

export class ReplyToMessageComponent extends React.Component<
  ReplyToMessageProps,
  ReplyToMessageState
> {
  state: ReplyToMessageState = {
    showModal: false
  };

  showReplyToMessage = () => this.setState({ showModal: true });

  handleCancel = () => this.setState({ showModal: false });

  handleSubmit = ({ subject, narrative }: ReplyToMessageFormValue) => {
    this.setState({ showModal: false });

    const {
      message: { caseId }
    } = this.props;

    this.sendMessage({
      subject,
      narrative,
      caseId
    });
  };

  sendMessage = (message: NewMessage) => {
    const {
      onMessageSent,
      intl: { formatMessage }
    } = this.props;

    const title = formatMessage({
      id: 'MESSAGES.MESSAGE_SENT'
    });

    this.props.addMessage(message, { title });

    onMessageSent();
  };

  render() {
    const {
      message: { subject }
    } = this.props;
    const { showModal } = this.state;

    return (
      <div>
        <Button
          type="primary"
          data-test-el="reply-to-message-button"
          onClick={this.showReplyToMessage}
        >
          <FormattedMessage id="MESSAGES.REPLY" />
        </Button>

        <ReplyToMessageModalForm
          visible={showModal}
          subject={subject}
          onCancel={this.handleCancel}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

export const ReplyToMessage = connect(
  null,
  mapDispatchToProps
)(injectIntl(ReplyToMessageComponent));
