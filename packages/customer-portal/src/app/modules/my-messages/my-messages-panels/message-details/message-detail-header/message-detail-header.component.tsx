import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Message } from 'fineos-js-api-client';

import { InlinePropertyItem } from '../../../../../shared/ui';
import {
  formatDateForDisplay,
  MESSAGES_DISPLAY_DATE_FORMAT
} from '../../../../../shared/utils';

import styles from './message-detail-header.module.scss';

type MessageDetailHeaderProps = {
  message: Message;
  chooseLogo: (msgOriginatesFromPortal: boolean) => React.ReactNode | string;
};

export const MessageDetailHeader: React.FC<MessageDetailHeaderProps> = ({
  message: { msgOriginatesFromPortal, subject, caseId, contactTime },
  chooseLogo
}) => (
  <div className={styles.wrapper}>
    <div className={styles.logo}>{chooseLogo(msgOriginatesFromPortal)}</div>
    <div>
      <div className={styles.subject}>{subject}</div>
      <div className={styles.relatesTo}>
        <InlinePropertyItem
          label={<FormattedMessage id="PAYMENTS.RELATES_TO" />}
        >
          {caseId}
        </InlinePropertyItem>
      </div>
    </div>
    <div className={styles.dateTime}>
      {formatDateForDisplay(contactTime, MESSAGES_DISPLAY_DATE_FORMAT)}
    </div>
  </div>
);
