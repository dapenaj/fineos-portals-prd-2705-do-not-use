export type ReplyToMessageFormValue = {
  subject: string;
  narrative: string;
};
