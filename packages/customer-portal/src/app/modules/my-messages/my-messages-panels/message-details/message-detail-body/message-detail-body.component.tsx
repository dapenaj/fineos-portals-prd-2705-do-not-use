import React from 'react';
import { Message } from 'fineos-js-api-client';

import { ReplyToMessage } from '../reply-to-message';

import styles from './message-detail-body.module.scss';

type MessageDetailBodyProps = {
  message: Message;
  onMessageSent: () => void;
};

export const MessageDetailBody: React.FC<MessageDetailBodyProps> = ({
  message,
  onMessageSent
}) => (
  <>
    <div>{message.narrative}</div>
    <div className={styles.modalWrapper} data-test-el="message-button">
      <ReplyToMessage message={message} onMessageSent={onMessageSent} />
    </div>
  </>
);
