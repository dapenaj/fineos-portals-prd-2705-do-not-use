import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Modal } from 'antd';
import { FormikProps } from 'formik';

import { ConfirmModal, ModalFooter } from '../../../../../../shared/ui';
import { ReplyToMessageFormValue } from '../reply-to-message.type';

import { ReplyToMessageForm } from './reply-to-message-form';

type ReplyToMessageModalFormViewProps = {
  visible: boolean;
  subject: string;
  onCancel: () => void;
  onSubmit: (value: ReplyToMessageFormValue) => void;
} & FormikProps<ReplyToMessageFormValue>;

type ReplyToMessageModalFormViewState = {
  showConfirmModal: boolean;
};

export class ReplyToMessageModalFormView extends React.Component<
  ReplyToMessageModalFormViewProps,
  ReplyToMessageModalFormViewState
> {
  state: ReplyToMessageModalFormViewState = {
    showConfirmModal: false
  };

  showConfirmModal = () =>
    this.setState({
      showConfirmModal: true
    });

  handleConfirmCancel = () => this.setState({ showConfirmModal: false });

  handleConfirmSubmit = () => {
    const { onCancel, resetForm } = this.props;

    this.setState({ showConfirmModal: false });
    resetForm();
    onCancel();
  };

  handleSubmit = () => {
    const { resetForm, onSubmit, values } = this.props;

    onSubmit(values);
    resetForm();
  };

  render() {
    const { visible, onCancel, onSubmit, ...formikProps } = this.props;

    const { showConfirmModal } = this.state;

    return (
      <>
        <Modal
          title={<FormattedMessage id="MESSAGES.REPLY_TO" />}
          visible={visible}
          data-test-el="new-message-modal"
          onCancel={this.showConfirmModal}
          footer={
            <ModalFooter
              submitText={'MESSAGES.SEND'}
              cancelText={'MESSAGES.CANCEL'}
              isDisabled={!formikProps.isValid}
              onCancelClick={this.showConfirmModal}
              onSubmitClick={this.handleSubmit}
            />
          }
        >
          <ReplyToMessageForm {...formikProps} />
        </Modal>

        <ConfirmModal
          visible={showConfirmModal}
          submitText={'MESSAGES.CONFIRM'}
          cancelText={'MESSAGES.CANCEL'}
          message={'MESSAGES.CANCEL_INFO'}
          onSubmitConfirm={this.handleConfirmSubmit}
          onCancelConfirm={this.handleConfirmCancel}
        />
      </>
    );
  }
}
