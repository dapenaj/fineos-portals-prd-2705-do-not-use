import React from 'react';
import { Row, Col, Avatar } from 'antd';
import { Message } from 'fineos-js-api-client';

import { Initials } from '../../../shared/ui/initials';
import logo from '../../../../assets/img/logo-icon.svg';
import { withAppConfig } from '../../../shared/contexts';
import { AppConfig } from '../../../shared/types';

import { MessageDetails } from './message-details';
import { MessageList } from './message-list';
import styles from './my-messages-panels.module.scss';

type MyMessagesPanelsProps = {
  messages: Message[];
  config: AppConfig;
  selectedMessage?: Message;
  onMessageSelect: (message: Message) => void;
  onMessageSent: () => void;
};

export const MyMessagesPanelsComponent: React.FC<MyMessagesPanelsProps> = ({
  messages,
  config: {
    customerDetail: { customer }
  },
  selectedMessage,
  onMessageSelect,
  onMessageSent
}) => {
  const chooseLogo = (msgOriginatesFromPortal: boolean) => {
    return msgOriginatesFromPortal ? (
      customer ? (
        <Initials customer={customer} />
      ) : (
        ''
      )
    ) : (
      <Avatar src={logo} />
    );
  };

  return (
    <Row data-test-el="messages-panels-wrapper" className={styles.panelWrapper}>
      <Col md={8} className={styles.listPosition}>
        <MessageList
          messages={messages}
          onMessageSelect={onMessageSelect}
          chooseLogo={chooseLogo}
          selectedMessage={selectedMessage && selectedMessage.messageId}
        />
      </Col>
      <Col md={16} className={styles.detailsPosition}>
        {!!selectedMessage && (
          <MessageDetails
            onMessageSent={onMessageSent}
            message={selectedMessage}
            chooseLogo={chooseLogo}
          />
        )}
      </Col>
    </Row>
  );
};

export const MyMessagesPanels = withAppConfig(MyMessagesPanelsComponent);
