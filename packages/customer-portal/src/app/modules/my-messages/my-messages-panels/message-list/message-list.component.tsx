import React from 'react';
import { Layout, Card } from 'antd';
import classnames from 'classnames';
import { Message } from 'fineos-js-api-client';

import { MessageListElement } from '../../../../shared/ui';

import styles from './message-list.module.scss';

const { Content } = Layout;

type MessageListProps = {
  messages: Message[];
  onMessageSelect: (message: Message) => void;
  chooseLogo: (msgOriginatesFromPortal: boolean) => React.ReactNode | string;
  selectedMessage?: number;
};

export const MessageList: React.FC<MessageListProps> = ({
  messages,
  onMessageSelect,
  chooseLogo,
  selectedMessage
}) => (
  <Content data-test-el="messages-list">
    {messages.map((message, key) => (
      <div key={key} className={styles.border}>
        <Card
          bordered={false}
          onClick={() => onMessageSelect(message)}
          className={classnames(
            styles.listWrapper,
            message.messageId === selectedMessage ? styles.selected : ''
          )}
        >
          <MessageListElement chooseLogo={chooseLogo} message={message} />
        </Card>
      </div>
    ))}
  </Content>
);
