import React from 'react';
import { Breadcrumb, Icon } from 'antd';
import { NavLink } from 'react-router-dom';

import styles from './my-messages-breadcrumb.module.scss';

type MessageHeaderBreadcrumb = {
  notificationCaseId: string;
};

export const MyMessagesBreadcrumb: React.FC<MessageHeaderBreadcrumb> = ({
  notificationCaseId
}) => (
  <Breadcrumb separator="" className={styles.header}>
    <Breadcrumb.Item key="notification">
      <NavLink
        className={styles.link}
        to={{ pathname: `/notifications/${notificationCaseId}` }}
      >
        {notificationCaseId}
      </NavLink>
    </Breadcrumb.Item>
    <Breadcrumb.Separator>
      <Icon type="right" />
    </Breadcrumb.Separator>
    <Breadcrumb.Item key="messages">Messages</Breadcrumb.Item>
  </Breadcrumb>
);
