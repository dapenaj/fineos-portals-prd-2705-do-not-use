import React from 'react';
import {
  NotificationCaseSummary,
  Message,
  MessageService
} from 'fineos-js-api-client';
import { flatten as _flatten, isEmpty as _isEmpty } from 'lodash';
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps as IntlProps
} from 'react-intl';
import moment from 'moment';

import { MyMessagesHeader, MyMessagesPanels } from '../my-messages';
import { ContentLayout } from '../../shared/layouts';
import { ContentRenderer, HeaderSwitch } from '../../shared/ui';

import styles from './my-messages.module.scss';

type MyMessagesViewProps = {
  notifications: NotificationCaseSummary[];
  markMessagesAsRead: (messageId: number) => void;
  selectedMessageId: number | null;
  notificationCaseId: string | null;
  messages: Message[] | null;
  loading: boolean;
  fetchMessages: () => void;
} & IntlProps;

type MyMessagesViewState = {
  msgOriginatesFromPortal: boolean;
  selectedMessageId: number | null;
  searchValues: string[];
};

export class MyMessagesViewComponent extends React.Component<
  MyMessagesViewProps,
  MyMessagesViewState
> {
  state: MyMessagesViewState = {
    msgOriginatesFromPortal: false,
    selectedMessageId: null,
    searchValues: []
  };

  messageService = MessageService.getInstance();

  async componentDidMount() {
    const { selectedMessageId } = this.props;

    this.props.fetchMessages();

    if (selectedMessageId) {
      this.setState({ selectedMessageId });
    }
  }

  onMessageSelect = (message: Message) => {
    if (!message.read) {
      this.props.markMessagesAsRead(message.messageId);
    }

    this.setState({
      selectedMessageId: message.messageId
    });
  };

  onChange = (msgOriginatesFromPortal: boolean) => {
    this.setState({
      msgOriginatesFromPortal,
      selectedMessageId: null
    });
  };

  onSearch = (searchValues?: string[]) => {
    this.setState({
      searchValues: searchValues ? searchValues : [],
      selectedMessageId: null
    });
  };

  filterByCaseId = (messages: Message[]) => {
    return this.props.selectedMessageId
      ? messages.filter(
          ({ messageId }) => messageId === this.props.selectedMessageId
        )
      : messages;
  };

  filterMessages = (msgOriginatesFromPortal: boolean) => {
    const { messages } = this.props;

    const filteredMessages = this.filterByCaseId(this.sortMessages(messages));

    return msgOriginatesFromPortal
      ? filteredMessages.filter(message => !message.msgOriginatesFromPortal)
      : filteredMessages;
  };

  cleanNonAlphaNumeric = (str: string): string => {
    return str.replace(/^\W*/, '').replace(/\W*$/, '');
  };

  tokenize = (str: string): string[] => {
    return str
      .split(/\s+/)
      .map(this.cleanNonAlphaNumeric)
      .filter(Boolean)
      .map(token => token.toLocaleLowerCase());
  };

  filterMessagesBySearchValue = (
    messages: Message[],
    searchValues: string[]
  ) => {
    return messages.filter(message => {
      const messageBody: string[] = [
        ...this.tokenize(message.subject),
        ...this.tokenize(message.narrative)
      ];
      const searchBody: string[] = _flatten([
        ...searchValues.map(ele => this.tokenize(ele))
      ]);

      return searchBody.every(ele => {
        return messageBody.includes(ele);
      });
    });
  };

  onMessageSent = () => this.setState({ selectedMessageId: null });

  getFilterMessages = () => {
    const { searchValues, msgOriginatesFromPortal } = this.state;

    const messagesFiltered = this.filterMessages(msgOriginatesFromPortal);

    return searchValues.length > 0
      ? this.filterMessagesBySearchValue(messagesFiltered, searchValues)
      : messagesFiltered;
  };

  sortMessages = (messages: Message[] | null): Message[] =>
    [...(!_isEmpty(messages) ? messages! : [])].sort((a, b) =>
      moment(b.contactTime).diff(moment(a.contactTime))
    );

  render() {
    const { notificationCaseId, notifications, loading } = this.props;
    const messagesFiltered = this.getFilterMessages();

    const selectedMessageIdTemp =
      this.state.selectedMessageId ||
      (messagesFiltered.length && messagesFiltered[0].messageId);

    const message = messagesFiltered.find(
      ({ messageId }) => selectedMessageIdTemp === messageId
    );

    return (
      <ContentLayout
        className={styles.container}
        direction="column"
        data-test-el="messages-wrapper"
      >
        <MyMessagesHeader
          onSearch={this.onSearch}
          notifications={notifications}
          notificationCaseId={notificationCaseId}
          onMessageSent={this.onMessageSent}
          disabled={loading}
        />
        <HeaderSwitch
          label="MESSAGES.ONLY_INCOMING"
          onChange={this.onChange}
          defaultChecked={false}
          disabled={loading}
        />

        <ContentRenderer
          loading={loading}
          isEmpty={_isEmpty(messagesFiltered)}
          emptyTitle={<FormattedMessage id="MESSAGES.EMPTY_VALUE" />}
          withBorder={true}
        >
          <MyMessagesPanels
            messages={messagesFiltered}
            selectedMessage={message}
            onMessageSelect={this.onMessageSelect}
            onMessageSent={this.onMessageSent}
          />
        </ContentRenderer>
      </ContentLayout>
    );
  }
}

export const MyMessagesView = injectIntl(MyMessagesViewComponent);
