import React from 'react';
import { Popover, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './my-messages-search-badge.module.scss';

type MyMessageSearchBadgeProps = {
  searchValues: string[];
  clearItem: (item: string) => void;
};

const popoverElem = (item: string, clearItem: (item: string) => void) => {
  return (
    <>
      {item}
      <Icon
        data-test-el="icon-search-badge"
        className={styles.iconPosition}
        type="close"
        onClick={() => clearItem(item)}
      />
    </>
  );
};

export const MyMessageSearchBadge: React.FC<MyMessageSearchBadgeProps> = ({
  searchValues,
  clearItem
}) => (
  <>
    {!!searchValues && !!searchValues.length && (
      <Popover
        content={searchValues.map((item, index) => {
          return (
            <span key={index} className={styles.searchValues}>
              {popoverElem(item, clearItem)}
            </span>
          );
        })}
        trigger="click"
      >
        <div className={styles.searchPanel}>
          <FormattedMessage id="MESSAGES.FILTERED_VALUE" />
          {searchValues.map((elem, key) => {
            return (
              <div key={key} className={styles.searchItem}>
                {popoverElem(elem, clearItem)}
              </div>
            );
          })}
        </div>
      </Popover>
    )}
  </>
);
