import React from 'react';
import { Row, Col, Input } from 'antd';
import { FormattedMessage } from 'react-intl';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { showNotification } from '../../../shared/ui/notification/notification';

import { MyMessageSearchBadge } from './ui';
import styles from './my-messages-search.module.scss';

const { Search } = Input;

type MyMessagesSearchProps = {
  disabled: boolean;
  onSearch: (searchValues?: string[]) => void;
} & IntlProps;

type MyMessagesSearchState = {
  searchValues: string[];
  searchValue: string;
};

export class MyMessagesSearchContainer extends React.Component<
  MyMessagesSearchProps,
  MyMessagesSearchState
> {
  state = {
    searchValues: [],
    searchValue: ''
  };

  onSearch = (searchValuesTemp: string) => {
    if (searchValuesTemp && searchValuesTemp.length > 2) {
      const searchValues = [...this.state.searchValues, searchValuesTemp];
      this.setState({ searchValues, searchValue: '' });
      this.props.onSearch(searchValues);
    } else {
      showNotification({
        type: 'warning',
        title: this.props.intl.formatMessage({
          id: 'MESSAGES.NOTIFICATION.WARNING.TITLE'
        }),
        content: this.props.intl.formatMessage(
          {
            id: 'MESSAGES.NOTIFICATION.WARNING.CONTENT'
          },
          { minLength: 2 }
        )
      });
    }
  };

  onChange = (searchValue: string) => {
    this.setState({ searchValue });
  };

  clearItem = (searchValue: string) => {
    const searchValues = this.state.searchValues.filter(e => e !== searchValue);
    this.setState({ searchValues });
    this.props.onSearch(searchValues);
  };

  render() {
    const { disabled } = this.props;
    const { searchValues, searchValue } = this.state;

    return (
      <Row className={styles.container} data-test-el="messages-search-wrapper">
        <Col span={16}>
          <MyMessageSearchBadge
            searchValues={searchValues}
            clearItem={this.clearItem}
          />
        </Col>
        <Col span={8}>
          <FormattedMessage id="MESSAGES.SEARCH">
            {placeholder => {
              return (
                <Search
                  name="search"
                  value={searchValue}
                  placeholder={placeholder as string}
                  className={styles.searchBorder}
                  onChange={event => this.onChange(event.target.value)}
                  onSearch={this.onSearch}
                  disabled={disabled}
                />
              );
            }}
          </FormattedMessage>
        </Col>
      </Row>
    );
  }
}

export const MyMessagesSearch = injectIntl(MyMessagesSearchContainer);
