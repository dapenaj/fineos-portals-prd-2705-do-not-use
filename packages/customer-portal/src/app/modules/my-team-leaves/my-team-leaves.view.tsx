import React from 'react';
import { TeamMember } from 'fineos-js-api-client';

import { ContentLayout } from '../../shared/layouts';
import { MyTeamLeave } from '../../shared/types';

import { MyTeamLeavesHeader } from './my-team-leaves-header';
import { TeamLeaveFilter } from './my-team-leaves-filters';
import { MyTeamLeavesList } from './my-team-leaves-list';

type MyTeamLeavesProps = {
  employees: TeamMember[] | null;
  employeesLoading: boolean;
  myTeamLeaves: MyTeamLeave[] | null;
  myTeamLeavesLoading: boolean;
  onEmployeeIdChange: (value: string) => void;
  onFilterChange: (value: TeamLeaveFilter) => void;
};

export const MyTeamLeavesView: React.FC<MyTeamLeavesProps> = ({
  employees,
  employeesLoading,
  myTeamLeaves,
  myTeamLeavesLoading,
  onEmployeeIdChange
}) => (
  <ContentLayout direction="column" data-test-el="my-team-leaves">
    <MyTeamLeavesHeader
      employees={employees}
      loading={employeesLoading || myTeamLeavesLoading}
      myTeamLeaves={myTeamLeaves}
      onEmployeeIdChange={onEmployeeIdChange}
    />
    {/* Removed until API can support them */}
    {/* <MyTeamLeavesFilters
      onFilterChange={onFilterChange}
      loading={employeesLoading || myTeamLeavesLoading}
    /> */}
    <MyTeamLeavesList
      myTeamLeaves={myTeamLeaves}
      loading={employeesLoading || myTeamLeavesLoading}
    />
  </ContentLayout>
);
