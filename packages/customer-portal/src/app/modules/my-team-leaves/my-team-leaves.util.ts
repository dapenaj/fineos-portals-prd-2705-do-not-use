import { AbsenceSummary } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { MyTeamLeaveField, MyTeamLeavesExportColumn } from '../../shared/types';
import { formatDateForDisplay, DISPLAY_DATE_FORMAT } from '../../shared/utils';

export const getAbsenceCaseHandler = (absences: AbsenceSummary[]) => {
  if (!_isEmpty(absences)) {
    const absence = absences.find(
      ({ absenceHandler }) => !_isEmpty(absenceHandler)
    );

    return absence ? absence.absenceHandler : '--';
  }

  return '--';
};

export const getCaseManagersContact = (absences: AbsenceSummary[]) => {
  if (!_isEmpty(absences)) {
    const absence = absences.find(
      ({ absenceHandlerPhoneNumber }) => !_isEmpty(absenceHandlerPhoneNumber)
    );

    return absence ? absence.absenceHandlerPhoneNumber : '--';
  }

  return '--';
};

export const getMyTeamLeavesRows = (
  columns: MyTeamLeavesExportColumn[],
  // using any because not all MyTeamLeaveField props belong to
  // SupervisedNotificationCaseSummary
  notifications: any[]
) =>
  notifications.map(notification =>
    columns.map(({ field }) =>
      getMyTeamLeaveRowValue(field, notification[field], notification.absences)
    )
  );

export const getMyTeamLeaveRowValue = (
  field: MyTeamLeaveField,
  value: any,
  absences: AbsenceSummary[]
) => {
  switch (field) {
    case MyTeamLeaveField.ABSENCE_CASE_MANAGER:
      return getAbsenceCaseHandler(absences);
    case MyTeamLeaveField.CASE_MANAGERS_CONTACT:
      return getCaseManagersContact(absences);
    case MyTeamLeaveField.CREATED_DATE:
    case MyTeamLeaveField.DATE_FIRST_MISSING_WORK:
    case MyTeamLeaveField.EXPECTED_RETURN_TO_WORK_DATE:
      return formatDateForDisplay(value, DISPLAY_DATE_FORMAT);
    default:
      return value;
  }
};
