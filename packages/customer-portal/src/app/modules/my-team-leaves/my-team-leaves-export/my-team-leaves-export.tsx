import React from 'react';
import { Button } from 'antd';
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps as IntlProps
} from 'react-intl';
import moment from 'moment';

import { ExportService } from '../../../shared/services';
import { showNotification } from '../../../shared/ui';
import {
  CsvDetails,
  MyTeamLeavesExportColumn,
  MyTeamLeave
} from '../../../shared/types';
import { EXPORT_DATE_FORMAT } from '../../../shared/utils';
import { getMyTeamLeavesRows } from '../my-team-leaves.util';
import { MY_TEAM_LEAVES_EXPORT_HEADERS } from '../my-team-leaves.constant';

import styles from './my-team-leaves-export.module.scss';

type MyTeamLeavesExportProps = {
  loading: boolean;
  myTeamLeaves: MyTeamLeave[];
} & IntlProps;

type MyTeamLeavesExportState = {
  exporting: boolean;
};

export class MyTeamLeavesExportComponent extends React.Component<
  MyTeamLeavesExportProps,
  MyTeamLeavesExportState
> {
  state: MyTeamLeavesExportState = {
    exporting: false
  };

  exportService = ExportService.getInstance();

  private exportColumns: MyTeamLeavesExportColumn[] = MY_TEAM_LEAVES_EXPORT_HEADERS.map(
    ({ header, field }) => ({
      header: this.props.intl.formatMessage({ id: header }),
      field
    })
  );

  exportMyTeamLeaves = () => {
    const {
      myTeamLeaves,
      intl: { formatMessage }
    } = this.props;
    this.setState({ exporting: true });

    try {
      const csvDetails: CsvDetails = {
        columns: this.exportColumns.map(({ header }) => header),
        rows: getMyTeamLeavesRows(this.exportColumns, myTeamLeaves),
        filename: `MY_TEAM_LEAVES_${moment().format(EXPORT_DATE_FORMAT)}`
      };

      this.exportService.exportToCsv(csvDetails);
    } catch (error) {
      const title = formatMessage({ id: 'MY_TEAM_LEAVES.EXPORT_ERROR' });

      showNotification({
        title,
        type: 'error',
        content: null
      });
    } finally {
      this.setState({ exporting: false });
    }
  };

  render() {
    const { loading } = this.props;
    const { exporting } = this.state;

    return (
      <div className={styles.wrapper}>
        <Button
          type="primary"
          disabled={loading || exporting}
          onClick={this.exportMyTeamLeaves}
          data-test-el="my-team-leaves-export-btn"
        >
          <FormattedMessage id="MY_TEAM_LEAVES.EXPORT_LIST" />
        </Button>
      </div>
    );
  }
}

export const MyTeamLeavesExport = injectIntl(MyTeamLeavesExportComponent);
