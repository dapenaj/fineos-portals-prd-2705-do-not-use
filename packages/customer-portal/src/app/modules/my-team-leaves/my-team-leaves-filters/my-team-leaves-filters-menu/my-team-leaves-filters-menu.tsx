import React, { createRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { Menu, Icon } from 'antd';
import { ClickParam } from 'antd/lib/menu';
import classnames from 'classnames';

import {
  filters,
  TeamLeaveFilterType,
  TeamLeaveFilter
} from '../my-team-leaves-filters.constant';

import styles from './my-team-leaves-filters-menu.module.scss';

const { Item } = Menu;

type MyTeamLeavesFiltersMenuProps = {
  onFilterClick: (value: TeamLeaveFilter) => void;
  onOutsideClick: () => void;
};

export class MyTeamLeavesFiltersMenu extends React.Component<
  MyTeamLeavesFiltersMenuProps
> {
  private menuRef = createRef<HTMLDivElement>();

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClick);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick);
  }

  handleClick = (event: MouseEvent) => {
    const hasRelatedTarget = !!event.target;

    const menuContainsRelatedTarget = this.menuRef.current!.contains(
      event.target as Node
    );

    if (!hasRelatedTarget || !menuContainsRelatedTarget) {
      this.props.onOutsideClick();
    }
  };

  render() {
    const { onFilterClick } = this.props;

    return (
      <div ref={this.menuRef} data-test-el="my-team-leaves-filters-menu">
        <Menu className={styles.wrapper}>
          {filters
            .filter(filter => filter.type === TeamLeaveFilterType.ALL)
            .map((filter, index) => (
              <Item
                className={styles.item}
                onClick={(param: ClickParam) => onFilterClick(filter)}
                key={`${TeamLeaveFilterType.ALL}_${index}`}
              >
                {filter.title}
              </Item>
            ))}

          <div className={styles.heading}>
            <Icon
              type="right-circle"
              theme="filled"
              className={classnames(styles.icon, styles.coming)}
            />
            <FormattedMessage id="MY_TEAM_LEAVES.COMING_BACK" />
          </div>

          {filters
            .filter(filter => filter.type === TeamLeaveFilterType.COMING_BACK)
            .map((filter, index) => (
              <Item
                className={styles.item}
                onClick={(param: ClickParam) => onFilterClick(filter)}
                key={`${TeamLeaveFilterType.COMING_BACK}_${index}`}
              >
                {filter.title}
              </Item>
            ))}

          <div className={styles.heading}>
            <Icon
              type="left-circle"
              theme="filled"
              className={classnames(styles.icon, styles.going)}
            />
            <FormattedMessage id="MY_TEAM_LEAVES.GOING_ON" />
          </div>

          {filters
            .filter(filter => filter.type === TeamLeaveFilterType.GOING_ON)
            .map((filter, index) => (
              <Item
                className={styles.item}
                onClick={(param: ClickParam) => onFilterClick(filter)}
                key={`${TeamLeaveFilterType.GOING_ON}_${index}`}
              >
                {filter.title}
              </Item>
            ))}
        </Menu>
      </div>
    );
  }
}
