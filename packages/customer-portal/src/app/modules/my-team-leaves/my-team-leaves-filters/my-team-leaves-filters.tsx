import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Dropdown, Button, Icon } from 'antd';
import classnames from 'classnames';

import {
  filters,
  TeamLeaveFilterType,
  TeamLeaveFilter
} from './my-team-leaves-filters.constant';
import { MyTeamLeavesFiltersMenu } from './my-team-leaves-filters-menu';
import styles from './my-team-leaves-filters.module.scss';

type MyTeamLeavesFiltersProps = {
  onFilterChange: (filter: TeamLeaveFilter) => void;
  loading: boolean;
};

type MyTeamLeavesFiltersState = {
  selectedFilter: TeamLeaveFilter;
  visible: boolean;
};

export class MyTeamLeavesFilters extends React.Component<
  MyTeamLeavesFiltersProps,
  MyTeamLeavesFiltersState
> {
  state: MyTeamLeavesFiltersState = {
    selectedFilter: filters[0],
    visible: false
  };

  handleFilterClick = (selectedFilter: TeamLeaveFilter) => {
    const { onFilterChange } = this.props;

    this.setState({ selectedFilter, visible: false });
    onFilterChange(selectedFilter);
  };

  handleDropdownClick = () => {
    const { visible } = this.state;
    this.setState({ visible: !visible });
  };

  handleOutsideClick = () => {
    this.setState({ visible: false });
  };

  render() {
    const {
      selectedFilter: { type, title },
      visible
    } = this.state;
    const { loading } = this.props;

    return (
      <div className={styles.wrapper} data-test-el="my-team-leaves-filter">
        <div className={styles.label}>
          <FormattedMessage id="COMMON.ACTIONS.FILTER_BY" />
        </div>

        <div className={styles.dropdown}>
          <Dropdown
            trigger={['click']}
            overlay={
              <MyTeamLeavesFiltersMenu
                onFilterClick={this.handleFilterClick}
                onOutsideClick={this.handleOutsideClick}
              />
            }
            visible={visible}
            disabled={loading}
          >
            <Button
              type="link"
              className={classnames('ant-dropdown-link', styles.button)}
              onClick={this.handleDropdownClick}
              data-test-el="my-team-leaves-filters-btn"
            >
              {type !== TeamLeaveFilterType.ALL ? (
                <>
                  <FormattedMessage id={`MY_TEAM_LEAVES.${type}`} /> {title}
                </>
              ) : (
                title
              )}
              <Icon type="down" className={styles.arrow} />
            </Button>
          </Dropdown>
        </div>
      </div>
    );
  }
}
