import React from 'react';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';

import { formatDateForApi } from '../../../shared/utils';
import { MyTeamLeaveField } from '../../../shared/types';

export enum TeamLeaveFilterType {
  ALL = 'All',
  COMING_BACK = 'COMING_BACK',
  GOING_ON = 'GOING_ON'
}

export enum TeamLeaveFilterId {
  ALL = 'ALL',
  COMING_BACK_TWO = 'COMING_BACK_TWO',
  COMING_BACK_SEVEN = 'COMING_BACK_SEVEN',
  COMING_BACK_FIFTEEN = 'COMING_BACK_FIFTEEN',
  COMING_BACK_THIRTY = 'COMING_BACK_THIRTY',
  GOING_ON_TWO = 'GOING_ON_TWO',
  GOING_ON_SEVEN = 'GOING_ON_SEVEN',
  GOING_ON_FIFTEEN = 'GOING_ON_FIFTEEN',
  GOING_ON_THIRTY = 'GOING_ON_THIRTY'
}

export type TeamLeaveFilter = {
  startDate: string;
  endDate: string;
  sort: MyTeamLeaveField;
  title: React.ReactNode;
  type: TeamLeaveFilterType;
  id: TeamLeaveFilterId;
};

const allFilters: TeamLeaveFilter[] = [
  {
    startDate: '',
    endDate: '',
    sort: MyTeamLeaveField.NOTIFICATION_CASE_ID,
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.ALL" />,
    type: TeamLeaveFilterType.ALL,
    id: TeamLeaveFilterId.ALL
  }
];

const defaultComingBackFilters = {
  sort: MyTeamLeaveField.EXPECTED_RETURN_TO_WORK_DATE,
  type: TeamLeaveFilterType.COMING_BACK
};

const comingBackInFilters: TeamLeaveFilter[] = [
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(2, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.TWO" />,
    id: TeamLeaveFilterId.COMING_BACK_TWO,
    ...defaultComingBackFilters
  },
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(7, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.SEVEN" />,
    id: TeamLeaveFilterId.COMING_BACK_SEVEN,
    ...defaultComingBackFilters
  },
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(15, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.FIFTEEN" />,
    id: TeamLeaveFilterId.COMING_BACK_FIFTEEN,
    ...defaultComingBackFilters
  },
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(30, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.THIRTY" />,
    id: TeamLeaveFilterId.COMING_BACK_THIRTY,
    ...defaultComingBackFilters
  }
];

const defaultGoingOnLeaveFilters = {
  sort: MyTeamLeaveField.DATE_FIRST_MISSING_WORK,
  type: TeamLeaveFilterType.GOING_ON
};

const goingOnLeaveFilters: TeamLeaveFilter[] = [
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(2, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.TWO" />,
    id: TeamLeaveFilterId.GOING_ON_TWO,
    ...defaultGoingOnLeaveFilters
  },
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(7, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.SEVEN" />,
    id: TeamLeaveFilterId.GOING_ON_SEVEN,
    ...defaultGoingOnLeaveFilters
  },
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(15, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.FIFTEEN" />,
    id: TeamLeaveFilterId.GOING_ON_FIFTEEN,
    ...defaultGoingOnLeaveFilters
  },
  {
    startDate: formatDateForApi(moment()),
    endDate: formatDateForApi(moment().add(30, 'd')),
    title: <FormattedMessage id="MY_TEAM_LEAVES.FILTERS.THIRTY" />,
    id: TeamLeaveFilterId.GOING_ON_THIRTY,
    ...defaultGoingOnLeaveFilters
  }
];

export const filters: TeamLeaveFilter[] = [
  ...allFilters,
  ...comingBackInFilters,
  ...goingOnLeaveFilters
];
