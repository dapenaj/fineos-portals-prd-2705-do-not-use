import React from 'react';
import { FormattedMessage } from 'react-intl';
import { TeamMember } from 'fineos-js-api-client';

import { PageTitle } from '../../../shared/ui';
import { MyTeamLeavesSearch } from '../my-team-leaves-search';
import { MyTeamLeavesExport } from '../my-team-leaves-export';
import { MyTeamLeave } from '../../../shared/types';

import styles from './my-team-leaves-header.module.scss';

type MyTeamLeavesHeaderProps = {
  employees: TeamMember[] | null;
  loading: boolean;
  myTeamLeaves: MyTeamLeave[] | null;
  onEmployeeIdChange: (value: string) => void;
};

export const MyTeamLeavesHeader: React.FC<MyTeamLeavesHeaderProps> = ({
  employees,
  loading,
  myTeamLeaves,
  onEmployeeIdChange
}) => (
  <div className={styles.wrapper} data-test-el="my-team-leaves-header">
    <PageTitle>
      <FormattedMessage id="NAVBAR_LINKS.MY_TEAM_LEAVES" />
    </PageTitle>
    <div className={styles.actions}>
      {!!employees && !!employees.length && (
        <MyTeamLeavesSearch
          employees={employees}
          onEmployeeIdChange={onEmployeeIdChange}
          loading={loading}
        />
      )}

      {!!myTeamLeaves && !!myTeamLeaves.length && (
        <MyTeamLeavesExport loading={loading} myTeamLeaves={myTeamLeaves} />
      )}
    </div>
  </div>
);
