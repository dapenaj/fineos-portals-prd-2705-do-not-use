import React from 'react';
import { Button, Popover } from 'antd';
import {
  WrappedComponentProps as IntlProps,
  FormattedMessage,
  injectIntl
} from 'react-intl';
import { AbsenceService, AbsencePeriod } from 'fineos-js-api-client';

import {
  RequestAmendment,
  RequestConfirmationModal,
  showNotification,
  ConfirmModal
} from '../../../../../../shared/ui';
import { AbsenceAmendment } from '../../../../../../shared/types';

type LeavePeriodActionsProps = {
  absenceCaseId: string;
  absencePeriod: AbsencePeriod;
  member: string;
};

type LeavePeriodActionsState = {
  showEditLeavePeriodForm: boolean;
  showAmendmentForm: boolean;
  showConfirmation: boolean;
  isEdit: boolean;
  showModal: boolean;
};

export class LeavePeriodActionsComponent extends React.Component<
  LeavePeriodActionsProps & IntlProps,
  LeavePeriodActionsState
> {
  state: LeavePeriodActionsState = {
    showEditLeavePeriodForm: false,
    showAmendmentForm: false,
    showConfirmation: false,
    isEdit: false,
    showModal: false
  };

  absenceService = AbsenceService.getInstance();

  showSupervisorEditForm = () =>
    this.setState({ isEdit: true, showAmendmentForm: true });

  toggleSupervisorConfirmation = () =>
    this.setState({
      showConfirmation: !this.state.showConfirmation
    });

  showConfirmModal = () =>
    this.setState({
      showModal: true
    });

  deleteLeavePeriod = async () => {
    this.setState({ showModal: false, isEdit: false });

    const {
      absenceCaseId,
      absencePeriod: { id, startDate, endDate, requestStatus },
      member,
      intl: { formatMessage }
    } = this.props;

    const status = requestStatus.toLowerCase();

    try {
      if (status === 'completion' || status === 'pending') {
        await this.absenceService.deleteTimeOffRequest(
          absenceCaseId,
          {
            leavePeriodId: id,
            startDate,
            endDate,
            deleteType:
              status === 'completion' ? 'CancelLeave' : 'WithdrawLeave'
          },
          member
        );
      }
      this.toggleSupervisorConfirmation();
    } catch (error) {
      showNotification({
        title: formatMessage({
          id: 'NOTIFICATION.REQUEST_AMENDMENT.ERRORS.LEAVE_PERIOD_AMENDMENT'
        }),
        type: 'error',
        content: error
      });
    }
  };

  cancelConfirmModal = () => this.setState({ showModal: false });

  closeForm = () =>
    this.setState({
      showEditLeavePeriodForm: false,
      showAmendmentForm: false
    });

  render() {
    const {
      showAmendmentForm,
      showConfirmation,
      isEdit,
      showModal
    } = this.state;
    const { absenceCaseId, absencePeriod, member } = this.props;

    const amendment: AbsenceAmendment = {
      absenceCaseId,
      periodId: absencePeriod.id,
      absencePeriodType: absencePeriod.absenceType,
      startDate: absencePeriod.startDate,
      endDate: absencePeriod.endDate,
      statusCategory: absencePeriod.requestStatus,
      reasonName: absencePeriod.reason
    };

    return (
      <div data-test-el="leave-period-actions">
        <Button
          type="link"
          onClick={this.showSupervisorEditForm}
          data-test-el="request-change"
        >
          <FormattedMessage id="MY_TEAM_LEAVES.REQUEST_A_CHANGE" />
        </Button>
        <Popover
          content={<FormattedMessage id={'MY_TEAM_LEAVES.DELETE_TOOLTIP'} />}
          trigger={'hover'}
        >
          <Button
            type="link"
            icon="delete"
            onClick={this.showConfirmModal}
            data-test-el="request-deletion"
          />
          <ConfirmModal
            message={'NOTIFICATION.REQUEST_AMENDMENT.DELETE'}
            onSubmitConfirm={() => this.deleteLeavePeriod()}
            visible={showModal}
            onCancelConfirm={this.cancelConfirmModal}
          />
        </Popover>

        {showAmendmentForm && (
          <RequestAmendment
            member={member}
            amendment={amendment}
            closeForm={this.closeForm}
            isSupervisor={true}
            renderConfirmation={this.toggleSupervisorConfirmation}
          />
        )}
        {showConfirmation && (
          <RequestConfirmationModal
            isEdit={isEdit ? 'REQUEST_AMENDMENT' : 'REQUEST_DELETION'}
            isSupervisor={true}
            renderConfirmation={this.toggleSupervisorConfirmation}
          />
        )}
      </div>
    );
  }
}

export const LeavePeriodActions = injectIntl(LeavePeriodActionsComponent);
