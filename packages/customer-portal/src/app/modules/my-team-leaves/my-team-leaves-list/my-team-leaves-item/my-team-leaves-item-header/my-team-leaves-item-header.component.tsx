import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';

import { ContentLayout } from '../../../../../shared/layouts';
import {
  formatDateForDisplay,
  DISPLAY_DATE_FORMAT
} from '../../../../../shared/utils';
import { InlinePropertyItem, PropertyItem } from '../../../../../shared/ui';
import {
  getAbsenceCaseHandler,
  getCaseManagersContact
} from '../../../my-team-leaves.util';
import { MyTeamLeavesItemProperty } from '../my-team-leaves-item-property';
import { MyTeamLeave } from '../../../../../shared/types';

import styles from './my-team-leaves-item-header.module.scss';

const COL_SIZES = {
  xs: 24,
  sm: 12,
  md: 6,
  lg: 4
};

type MyTeamLeavesItemHeaderProps = {
  myTeamLeave: MyTeamLeave;
};

export const MyTeamLeavesItemHeader: React.FC<MyTeamLeavesItemHeaderProps> = ({
  myTeamLeave: {
    member,
    notificationCaseId,
    createdDate,
    notificationReason,
    expectedRTWDate,
    absences,
    dateFirstMissingWork
  }
}) => (
  <div className={styles.wrapper} data-test-elem="my-team-leaves-item-header">
    <ContentLayout direction="column" noPadding={true}>
      <Row type="flex" align="middle">
        <Col {...COL_SIZES}>
          <MyTeamLeavesItemProperty
            label={
              <div className={styles.name}>
                {!_isEmpty(member) ? member : '--'}
              </div>
            }
          >
            <InlinePropertyItem
              label={
                <>
                  <FormattedMessage id="MY_TEAM_LEAVES.LABELS.NOTIFIED_ON" />:
                </>
              }
              padded={false}
            >
              {formatDateForDisplay(createdDate, DISPLAY_DATE_FORMAT)}
            </InlinePropertyItem>
          </MyTeamLeavesItemProperty>
        </Col>

        <Col {...COL_SIZES}>
          <MyTeamLeavesItemProperty>
            {notificationCaseId} | {notificationReason}
          </MyTeamLeavesItemProperty>
        </Col>

        <Col {...COL_SIZES}>
          <MyTeamLeavesItemProperty>
            <PropertyItem
              label={
                <FormattedMessage id="MY_TEAM_LEAVES.LABELS.FIRST_DAY_MISSING_WORK" />
              }
              padded={false}
            >
              {formatDateForDisplay(dateFirstMissingWork, DISPLAY_DATE_FORMAT)}
            </PropertyItem>
          </MyTeamLeavesItemProperty>
        </Col>

        <Col {...COL_SIZES}>
          <MyTeamLeavesItemProperty>
            <PropertyItem
              label={
                <FormattedMessage id="MY_TEAM_LEAVES.LABELS.EXPECTED_RETURN" />
              }
              padded={false}
            >
              {formatDateForDisplay(expectedRTWDate, DISPLAY_DATE_FORMAT)}
            </PropertyItem>
          </MyTeamLeavesItemProperty>
        </Col>

        <Col {...COL_SIZES}>
          <MyTeamLeavesItemProperty>
            <PropertyItem
              label={
                <FormattedMessage id="MY_TEAM_LEAVES.LABELS.ABSENCE_CASE_MANAGER" />
              }
              padded={false}
            >
              {getAbsenceCaseHandler(absences)}
            </PropertyItem>
          </MyTeamLeavesItemProperty>
        </Col>

        <Col {...COL_SIZES}>
          <MyTeamLeavesItemProperty>
            <PropertyItem
              label={
                <FormattedMessage id="MY_TEAM_LEAVES.LABELS.CASE_MANAGERS_CONTACT" />
              }
              padded={false}
            >
              {getCaseManagersContact(absences)}
            </PropertyItem>
          </MyTeamLeavesItemProperty>
        </Col>
      </Row>
    </ContentLayout>
  </div>
);
