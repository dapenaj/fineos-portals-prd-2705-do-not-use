import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'antd';
import classnames from 'classnames';
import { AbsencePeriod } from 'fineos-js-api-client';

import { ContentRenderer, PropertyItem } from '../../../../../shared/ui';
import { ContentLayout } from '../../../../../shared/layouts';
import { MyTeamLeavesItemProperty } from '../my-team-leaves-item-property';
import {
  formatDateForDisplay,
  DISPLAY_DATE_FORMAT
} from '../../../../../shared/utils';

import { LeavePeriodActions } from './leave-period-actions';
import styles from './leave-period-list.module.scss';

const DEFAULT_COL_SIZES = {
  xs: 24,
  sm: 12,
  md: 6,
  lg: 4
};

const OFFSET_COLS = {
  lg: { span: 4, offset: 4 }
};

type LeavePeriodListProps = {
  absenceCaseId: string;
  absencePeriods: AbsencePeriod[] | null;
  member: string;
};

export const LeavePeriodList: React.FC<LeavePeriodListProps> = ({
  absenceCaseId,
  absencePeriods,
  member
}) => (
  <ContentRenderer
    loading={false}
    isEmpty={_isEmpty(absencePeriods)}
    emptyTitle={<FormattedMessage id="MY_TEAM_LEAVES.EMPTY_LEAVE_PERIODS" />}
    withBorder={false}
  >
    {!_isEmpty(absencePeriods) && (
      <ContentLayout direction="column" noPadding={true}>
        {absencePeriods!.map((absencePeriod, index) => (
          <div
            key={index}
            className={classnames(
              styles.wrapper,
              index !== absencePeriods!.length && styles.border
            )}
            data-test-el="leave-period-item"
          >
            <Row type="flex" align="middle">
              <Col {...DEFAULT_COL_SIZES} {...OFFSET_COLS}>
                <MyTeamLeavesItemProperty>
                  <PropertyItem
                    label={
                      <FormattedMessage id="MY_TEAM_LEAVES.LABELS.TYPE_OF_LEAVE" />
                    }
                    padded={false}
                  >
                    {absencePeriod.absenceType}
                  </PropertyItem>
                </MyTeamLeavesItemProperty>
              </Col>

              <Col {...DEFAULT_COL_SIZES}>
                <MyTeamLeavesItemProperty>
                  <PropertyItem
                    label={
                      <FormattedMessage id="MY_TEAM_LEAVES.LABELS.LEAVE_BEGINS" />
                    }
                    padded={false}
                  >
                    {formatDateForDisplay(
                      absencePeriod.startDate,
                      DISPLAY_DATE_FORMAT
                    )}
                  </PropertyItem>
                </MyTeamLeavesItemProperty>
              </Col>

              <Col {...DEFAULT_COL_SIZES}>
                <MyTeamLeavesItemProperty>
                  <PropertyItem
                    label={
                      <FormattedMessage id="MY_TEAM_LEAVES.LABELS.REQUESTED_THROUGH" />
                    }
                    padded={false}
                  >
                    {formatDateForDisplay(
                      absencePeriod.endDate,
                      DISPLAY_DATE_FORMAT
                    )}
                  </PropertyItem>
                </MyTeamLeavesItemProperty>
              </Col>

              <Col {...DEFAULT_COL_SIZES} {...OFFSET_COLS}>
                <MyTeamLeavesItemProperty>
                  <LeavePeriodActions
                    absenceCaseId={absenceCaseId}
                    absencePeriod={absencePeriod}
                    member={member}
                  />
                </MyTeamLeavesItemProperty>
              </Col>
            </Row>
          </div>
        ))}
      </ContentLayout>
    )}
  </ContentRenderer>
);
