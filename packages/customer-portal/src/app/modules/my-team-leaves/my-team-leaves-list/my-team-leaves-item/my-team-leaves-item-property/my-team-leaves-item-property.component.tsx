import React from 'react';

import styles from './my-team-leaves-item-property.module.scss';

type MyTeamLeavesItemPropertyProps = {
  label?: React.ReactNode;
};

export const MyTeamLeavesItemProperty: React.FC<MyTeamLeavesItemPropertyProps> = ({
  label,
  children
}) => (
  <div className={styles.wrapper}>
    {label && <div>{label}</div>}
    {children}
  </div>
);
