import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import { FormattedMessage } from 'react-intl';
import { Collapse } from 'antd';

import { ContentRenderer } from '../../../shared/ui';
import { MyTeamLeave } from '../../../shared/types';

import { MyTeamLeavesItemHeader, LeavePeriodList } from './my-team-leaves-item';

const { Panel } = Collapse;

type MyTeamLeavesListProps = {
  myTeamLeaves: MyTeamLeave[] | null;
  loading: boolean;
};

export const MyTeamLeavesList: React.FC<MyTeamLeavesListProps> = ({
  loading,
  myTeamLeaves
}) => (
  <ContentRenderer
    loading={loading}
    isEmpty={_isEmpty(myTeamLeaves)}
    emptyTitle={<FormattedMessage id="MY_TEAM_LEAVES.EMPTY" />}
    withBorder={true}
    data-test-el="my-team-leaves-list"
  >
    <Collapse accordion={true}>
      {!_isEmpty(myTeamLeaves) &&
        myTeamLeaves!.map((myTeamLeave, index) => (
          <Panel
            header={<MyTeamLeavesItemHeader myTeamLeave={myTeamLeave} />}
            key={`${index}`}
          >
            <LeavePeriodList
              absenceCaseId={myTeamLeave.absenceCaseId}
              absencePeriods={myTeamLeave.absencePeriods}
              member={myTeamLeave.member}
            />
          </Panel>
        ))}
    </Collapse>
  </ContentRenderer>
);
