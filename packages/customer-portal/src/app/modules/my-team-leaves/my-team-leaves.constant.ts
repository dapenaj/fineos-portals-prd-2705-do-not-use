import {
  MyTeamLeavesExportColumn,
  MyTeamLeaveField,
  MyTeamLeaveExportHeader
} from './../../shared/types';

export const MY_TEAM_LEAVES_EXPORT_HEADERS: MyTeamLeavesExportColumn[] = [
  {
    header: MyTeamLeaveExportHeader.TEAM_MEMBER,
    field: MyTeamLeaveField.MEMBER
  },
  {
    header: MyTeamLeaveExportHeader.NOTIFIED_ON,
    field: MyTeamLeaveField.CREATED_DATE
  },
  {
    header: MyTeamLeaveExportHeader.CASE_ID,
    field: MyTeamLeaveField.NOTIFICATION_CASE_ID
  },
  {
    header: MyTeamLeaveExportHeader.REASON,
    field: MyTeamLeaveField.NOTIFICATION_REASON
  },
  {
    header: MyTeamLeaveExportHeader.FIRST_DAY_MISSING_WORK,
    field: MyTeamLeaveField.DATE_FIRST_MISSING_WORK
  },
  {
    header: MyTeamLeaveExportHeader.EXPECTED_RETURN,
    field: MyTeamLeaveField.EXPECTED_RETURN_TO_WORK_DATE
  },
  {
    header: MyTeamLeaveExportHeader.ABSENCE_CASE_MANAGER,
    field: MyTeamLeaveField.ABSENCE_CASE_MANAGER
  },
  {
    header: MyTeamLeaveExportHeader.CASE_MANAGERS_CONTACT,
    field: MyTeamLeaveField.CASE_MANAGERS_CONTACT
  }
];
