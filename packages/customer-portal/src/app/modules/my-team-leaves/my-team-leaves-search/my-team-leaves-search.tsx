import React from 'react';
import { TeamMember } from 'fineos-js-api-client';
import { Select } from 'antd';
import { FormattedMessage } from 'react-intl';

import { SelectOption } from '../../../shared/types';
import { formatTeamMemberName } from '../../../shared/utils';

import styles from './my-team-leaves-search.module.scss';

type MyTeamLeavesSearchProps = {
  employees: TeamMember[];
  loading: boolean;
  onEmployeeIdChange: (value: string) => void;
};

type MyTeamLeavesSearchState = {
  options: SelectOption[];
};

const { Option } = Select;

export class MyTeamLeavesSearch extends React.Component<
  MyTeamLeavesSearchProps,
  MyTeamLeavesSearchState
> {
  state: MyTeamLeavesSearchState = {
    options: []
  };

  componentDidMount() {
    const { employees } = this.props;

    const options: SelectOption[] = (!!employees && !!employees.length
      ? employees
      : []
    ).map(employee => ({
      text: formatTeamMemberName(employee),
      value: employee.employeeId
    }));

    this.setState({ options });
  }

  render() {
    const { options } = this.state;
    const { onEmployeeIdChange, loading } = this.props;

    return (
      <div className={styles.wrapper} data-test-el="my-team-leaves-search">
        <Select
          allowClear={true}
          showSearch={true}
          onChange={onEmployeeIdChange}
          optionFilterProp="children"
          placeholder={
            <FormattedMessage id="MY_TEAM_LEAVES.EMPLOYEE_SEARCH_PLACEHOLDER" />
          }
          className={styles.search}
          disabled={loading}
          data-test-el="employee-select"
        >
          {options.map(({ text, value }, index) => (
            <Option value={value} key={index}>
              {text}
            </Option>
          ))}
        </Select>
      </div>
    );
  }
}
