import React from 'react';
import {
  CustomerService,
  TeamMember,
  SupervisedEmployeeSearch,
  NotificationSupervisedSearch,
  Page,
  AbsenceService
} from 'fineos-js-api-client';
import { RouteComponentProps } from 'react-router-dom';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import moment from 'moment';
import { isEmpty as _isEmpty } from 'lodash';
import { StaticContext } from 'react-router';

import {
  AuthorizationService,
  NotificationService
} from '../../shared/services';
import {
  Role,
  MyTeamLeavesFilters,
  MyTeamLeave,
  MyTeamLeaveField,
  AppConfig
} from '../../shared/types';
import { showNotification } from '../../shared/ui';
import { withAppConfig } from '../../shared/contexts';

import { MyTeamLeavesView } from './my-team-leaves.view';
import { TeamLeaveFilter } from './my-team-leaves-filters';

type MyTeamLeavesProps = { config: AppConfig } & RouteComponentProps<
  {},
  StaticContext,
  { prevPath: string }
> &
  IntlProps;

type MyTeamLeavesState = {
  pageNbr: number;
  pageSize: number;
  filters: MyTeamLeavesFilters;
  employeesLoading: boolean;
  employees: TeamMember[] | null;
  myTeamLeaves: MyTeamLeave[] | null;
  myTeamLeavesLoading: boolean;
};

export class MyTeamLeavesComponent extends React.Component<
  MyTeamLeavesProps,
  MyTeamLeavesState
> {
  employeePage = new Page();

  state: MyTeamLeavesState = {
    pageNbr: 1,
    pageSize: 0,
    filters: { sort: MyTeamLeaveField.NOTIFICATION_CASE_ID },
    employeesLoading: false,
    employees: null,
    myTeamLeaves: null,
    myTeamLeavesLoading: false
  };

  authorizationService = AuthorizationService.getInstance();
  customerService = CustomerService.getInstance();
  notificationService = NotificationService.getInstance();
  absenceService = AbsenceService.getInstance();

  async componentDidMount() {
    this.checkAuthorization();

    await this.fetchEmployees();
    await this.fetchMyTeamLeaves();
  }

  async fetchEmployees() {
    const {
      config: {
        clientConfig: {
          myTeamLeaves: { employeesPageSize }
        }
      },
      intl: { formatMessage }
    } = this.props;

    const search = new SupervisedEmployeeSearch().page(
      1,
      Number(employeesPageSize)
    );

    this.setState({ employeesLoading: true });

    try {
      const { data } = await this.customerService.getSupervisedEmployees(
        search
      );

      this.setState({ employees: data });
    } catch (error) {
      const title = formatMessage({
        id: 'MY_TEAM_LEAVES.EMPLOYEES_FETCH_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ employeesLoading: false });
    }
  }

  async fetchMyTeamLeaves() {
    const {
      config: {
        clientConfig: {
          myTeamLeaves: { notificationsPageSize }
        }
      },
      intl: { formatMessage }
    } = this.props;

    const {
      filters: { startDate, endDate, sort, employeeId }
    } = this.state;

    const search = new NotificationSupervisedSearch().page(
      1,
      Number(notificationsPageSize)
    );

    if (employeeId) {
      search.employeeId(employeeId);
    }

    if (startDate && endDate) {
      search.searchPeriod(moment(startDate), moment(endDate));
    }

    if (sort) {
      search.sort(sort);
    }

    this.setState({ myTeamLeavesLoading: true });

    try {
      const notifications = await this.notificationService.fetchSupervisedNotifications(
        search
      );

      const filteredNotifications = notifications.data.filter(
        ({ absences }) => !_isEmpty(absences)
      );

      if (!!filteredNotifications && !!filteredNotifications.length) {
        const myTeamLeaves = await Promise.all(
          filteredNotifications.map(async notification => {
            try {
              const absenceCaseId = notification.absences[0].absenceId;

              const {
                member,
                absencePeriods
              } = await this.absenceService.getSupervisedAbsenceDetails(
                absenceCaseId
              );

              return {
                absenceCaseId,
                absencePeriods,
                member,
                ...notification
              };
            } catch (error) {
              return {
                absenceCaseId: '',
                absencePeriods: [],
                member: '',
                ...notification
              };
            }
          })
        );

        this.setState({
          myTeamLeaves
        });
      }
    } catch (error) {
      const title = formatMessage({ id: 'MY_TEAM_LEAVES.FETCH_ERROR' });

      showNotification({ title, type: 'error', content: error });
    } finally {
      this.setState({ myTeamLeavesLoading: false });
    }
  }

  checkAuthorization() {
    const { history } = this.props;
    const {
      push,
      location: { state }
    } = history;

    if (!this.authorizationService.hasRole(Role.ABSENCE_SUPERVISOR)) {
      push(state && state.prevPath ? state.prevPath : '/');
    }
  }

  handleEmployeeIdChange = (employeeId: string) => {
    const { filters } = this.state;

    this.setState(
      {
        filters: { ...filters, employeeId }
      },
      () => this.fetchMyTeamLeaves()
    );
  };

  handleFilterChange = ({ startDate, endDate, sort }: TeamLeaveFilter) => {
    const { filters } = this.state;

    this.setState(
      {
        filters: { ...filters, startDate, endDate, sort }
      },
      () => this.fetchMyTeamLeaves()
    );
  };

  render() {
    const {
      employees,
      employeesLoading,
      myTeamLeaves,
      myTeamLeavesLoading
    } = this.state;

    return (
      <MyTeamLeavesView
        employees={employees}
        employeesLoading={employeesLoading}
        myTeamLeaves={myTeamLeaves}
        myTeamLeavesLoading={myTeamLeavesLoading}
        onEmployeeIdChange={this.handleEmployeeIdChange}
        onFilterChange={this.handleFilterChange}
      />
    );
  }
}

export const MyTeamLeaves = withAppConfig(injectIntl(MyTeamLeavesComponent));
