import React from 'react';
import { FormattedMessage } from 'react-intl';

import { IntakeSectionStep, IntakeSectionStepId } from '../../../shared/types';

import { AdditionalIncomeValue } from './wrap-up.type';

const additionalIncomeSourceStep: IntakeSectionStep<AdditionalIncomeValue> = {
  id: IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES,
  title: (
    <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.TITLE" />
  ),
  isComplete: false,
  isActive: true
};

const supportingEvidenceStep = {
  id: IntakeSectionStepId.SUPPORTING_EVIDENCE,
  title: <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.TITLE" />,
  isComplete: false,
  isActive: false
};

export const wrapUpSteps = [additionalIncomeSourceStep, supportingEvidenceStep];
