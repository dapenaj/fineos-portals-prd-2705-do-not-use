import React from 'react';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';
import { RadioChangeEvent } from 'antd/lib/radio';
import { Radio } from 'antd';

import { Panel } from '../../../../../../../../shared/ui';
import { AccountDetailsFormValue } from '../../account-details-form';

import styles from './account-details-list-item.module.scss';

type AccountDetailsListItemProps = {
  accountDetails: AccountDetailsFormValue;
  isSelectedAccount: boolean;
  onEftAccountChange: (event: RadioChangeEvent) => void;
};

export const AccountDetailsListItem: React.FC<AccountDetailsListItemProps> = ({
  accountDetails: { accountNo, accountName, accountType },
  isSelectedAccount,
  onEftAccountChange
}) => (
  <Panel
    className={classnames(styles.panel, {
      [styles.selected]: isSelectedAccount
    })}
  >
    <Radio
      value={accountNo}
      className={styles.radio}
      onChange={onEftAccountChange}
    >
      <FormattedMessage
        id={
          !!accountName
            ? 'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.ACCOUNT_WITH_NUMBER_NAME'
            : 'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.ACCOUNT_WITH_NUMBER'
        }
        values={{
          number: accountNo,
          accountName: accountName
        }}
      />
      <div>
        <FormattedMessage
          id={`INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.${accountType.toUpperCase()}`}
        />
      </div>
    </Radio>
  </Panel>
);
