import React from 'react';
import { PaymentPreference, CustomerService } from 'fineos-js-api-client';
import {
  injectIntl,
  WrappedComponentProps as IntlProps,
  FormattedMessage
} from 'react-intl';
import { Button } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';
import moment from 'moment';

import { showNotification, ContentRenderer } from '../../../../../shared/ui';
import {
  PaymentTypeLabel,
  ElectronicPaymentMethod,
  PaymentType,
  AppConfig
} from '../../../../../shared/types';
import {
  getCheckPaymentPreference,
  ELECTRONIC_PAYMENT_METHODS
} from '../../../../../shared/utils';
import { withAppConfig } from '../../../../../shared/contexts';

import { PaymentPreferencesOptions } from './payment-preferences-options';
import {
  AccountDetails,
  AccountDetailsFormValue,
  AccountDetailsForm
} from './account-details';
import styles from './payment-preferences.module.scss';

type PaymentPreferencesProps = {
  isDisabled: boolean;
  isSubmitting: boolean;
  config: AppConfig;
  onPaymentPreferenceChange: (
    paymentMethod: string,
    paymentPreference?: PaymentPreference
  ) => void;
  onAddPaymentPreference: (
    paymentMethod: string,
    paymentPreference: PaymentPreference
  ) => void;
  onProceedClick: () => void;
} & IntlProps;

type PaymentPreferencesState = {
  paymentPreferences: PaymentPreference[] | null;
  eftAccounts: AccountDetailsFormValue[];
  loading: boolean;
  selectedPaymentMethod?: string;
  selectedEftAccount?: AccountDetailsFormValue;
};

export class PaymentPreferencesComponent extends React.Component<
  PaymentPreferencesProps,
  PaymentPreferencesState
> {
  state: PaymentPreferencesState = {
    paymentPreferences: null,
    eftAccounts: [],
    loading: true
  };

  customerService = CustomerService.getInstance();

  async componentDidMount() {
    await this.fetchPaymentPreferences();
  }

  fetchPaymentPreferences = async () => {
    const {
      intl: { formatMessage }
    } = this.props;

    this.setState({ loading: true });

    try {
      const paymentPreferences = await this.customerService.findPaymentPreferences();

      let eftAccounts: AccountDetailsFormValue[] = [];

      if (!_isEmpty(paymentPreferences)) {
        eftAccounts = paymentPreferences
          .filter(preference => !!preference.accountDetails)
          .map(({ accountDetails, paymentPreferenceId }) => {
            return {
              ...accountDetails,
              paymentPreferenceId
            } as AccountDetailsFormValue;
          });
      }

      const selectedEftAccount = this.getDefaultEftAccountDetails(
        paymentPreferences
      );

      this.setState({
        paymentPreferences,
        eftAccounts,
        selectedEftAccount
      });
    } catch (error) {
      const title = formatMessage({
        id: 'MY_PROFILE.PAYMENT_PREFERENCES_ERROR_MESSAGE'
      });
      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ loading: false });
    }
  };

  getDefaultEftAccountDetails = (
    paymentPreferences: PaymentPreference[]
  ): AccountDetailsFormValue => {
    if (!!paymentPreferences) {
      const eftPreferences = paymentPreferences.filter(({ paymentMethod }) =>
        ELECTRONIC_PAYMENT_METHODS.includes(
          paymentMethod as ElectronicPaymentMethod
        )
      );
      const defaultEft = eftPreferences.find(({ isDefault }) => isDefault);
      if (defaultEft) {
        return {
          ...defaultEft.accountDetails!,
          paymentPreferenceId: defaultEft.paymentPreferenceId
        };
      } else {
        const effectivePreferences = eftPreferences.filter(
          ({ effectiveFrom, effectiveTo }) =>
            moment().isBetween(moment(effectiveFrom), moment(effectiveTo))
        );

        if (!_isEmpty(effectivePreferences)) {
          return {
            ...effectivePreferences[0].accountDetails!,
            paymentPreferenceId: effectivePreferences[0].paymentPreferenceId
          };
        } else if (!_isEmpty(eftPreferences)) {
          return {
            ...eftPreferences[0].accountDetails!,
            paymentPreferenceId: eftPreferences[0].paymentPreferenceId
          };
        }
      }
    }
    return {} as AccountDetailsFormValue;
  };

  mapSelectedToCheckPaymentPreference = (): PaymentPreference => {
    const {
      config: {
        customerDetail: { customer }
      }
    } = this.props;

    const { paymentPreferences } = this.state;
    const checkPreference = getCheckPaymentPreference(paymentPreferences);
    return !!checkPreference
      ? ({ ...checkPreference, isDefault: true } as PaymentPreference)
      : ({
          paymentMethod: PaymentType.CHECK,
          isDefault: true,
          chequeDetails: {
            nameToPrintOnCheck: customer
              ? `${customer.firstName} ${customer.lastName}`
              : ''
          }
        } as PaymentPreference);
  };

  mapSelectedToEftPaymentPreference = (
    defaultPreference?: PaymentPreference
  ): PaymentPreference => {
    const { paymentPreferences, selectedEftAccount } = this.state;

    if (defaultPreference) {
      return { ...defaultPreference, isDefault: true };
    } else if (
      !!selectedEftAccount &&
      !!selectedEftAccount.paymentPreferenceId
    ) {
      return {
        ...paymentPreferences!.find(
          ({ paymentPreferenceId }) =>
            paymentPreferenceId === selectedEftAccount.paymentPreferenceId
        ),
        isDefault: true
      } as PaymentPreference;
    } else {
      return {
        paymentMethod: PaymentType.ELECTRONIC_FUNDS_TRANSFER,
        isDefault: true,
        accountDetails: selectedEftAccount
      } as PaymentPreference;
    }
  };

  handlePaymentPreferenceOptionChange = (
    selectedPaymentMethod: string,
    defaultPreference?: PaymentPreference
  ) => {
    const selectedPreference =
      selectedPaymentMethod === PaymentTypeLabel.CHECK
        ? this.mapSelectedToCheckPaymentPreference()
        : this.mapSelectedToEftPaymentPreference(defaultPreference);

    this.setState(
      {
        selectedPaymentMethod
      },
      () =>
        this.props.onPaymentPreferenceChange(
          this.state.selectedPaymentMethod || '',
          selectedPreference
        )
    );
  };

  handleEftAccountSelected = (selectedEftAccount: AccountDetailsFormValue) => {
    const { eftAccounts } = this.state;

    const exists = eftAccounts.find(
      ({ accountNo }) => accountNo === selectedEftAccount.accountNo
    );

    const accounts = exists
      ? [...eftAccounts]
      : [...eftAccounts, selectedEftAccount];

    this.setState(
      {
        selectedEftAccount,
        eftAccounts: accounts
      },
      () =>
        this.handlePaymentPreferenceOptionChange(
          PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
        )
    );
  };

  handleAddEftAccountDetails = (
    selectedEftAccount: AccountDetailsFormValue
  ) => {
    const { onAddPaymentPreference } = this.props;

    this.setState(
      {
        selectedPaymentMethod: PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER,
        selectedEftAccount
      },
      () =>
        onAddPaymentPreference(
          this.state.selectedPaymentMethod!,
          this.mapSelectedToEftPaymentPreference()
        )
    );
  };

  render() {
    const {
      paymentPreferences,
      eftAccounts,
      loading,
      selectedPaymentMethod,
      selectedEftAccount
    } = this.state;
    const { isDisabled, isSubmitting, onProceedClick } = this.props;

    const showEftOptions =
      selectedPaymentMethod === PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER;
    const hasEftAccounts = !_isEmpty(eftAccounts);

    // if there are no EftAccounts, and EFT is selected, then we must show the form (with own button) instead of the modal
    const showEftForm = showEftOptions && !hasEftAccounts;

    return (
      <ContentRenderer loading={loading} isEmpty={false}>
        <>
          <p
            className={styles.introText}
            data-test-el="payment-preferences-intro"
          >
            <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.INFO" />
          </p>
          <PaymentPreferencesOptions
            paymentPreferences={paymentPreferences!}
            isDisabled={isDisabled}
            onPaymentPreferenceOptionChange={
              this.handlePaymentPreferenceOptionChange
            }
          />

          {showEftForm && (
            <AccountDetailsForm
              isDisabled={isDisabled}
              hasAccounts={hasEftAccounts}
              isSubmitting={isSubmitting}
              onAddEftAccountDetails={this.handleAddEftAccountDetails}
            />
          )}
          {!showEftForm && (
            <>
              {showEftOptions && (
                <AccountDetails
                  accounts={eftAccounts}
                  isDisabled={isDisabled}
                  onEftAccountSelected={this.handleEftAccountSelected}
                  selectedAccount={selectedEftAccount}
                />
              )}

              <div className={styles.button}>
                <Button
                  type="primary"
                  data-test-el="payment-preferences-save-btn"
                  disabled={isDisabled}
                  loading={isSubmitting}
                  onClick={onProceedClick}
                >
                  <FormattedMessage id="INTAKE.CONTROLS.SAVE_AND_PROCEED" />
                </Button>
              </div>
            </>
          )}
        </>
      </ContentRenderer>
    );
  }
}

export const PaymentPreferences = withAppConfig(
  injectIntl(PaymentPreferencesComponent)
);
