import React from 'react';
import { Modal } from 'antd';
import { FormattedMessage } from 'react-intl';
import { FormikProps } from 'formik';

import { ModalFooter } from '../../../../../../../../shared/ui';
import {
  AccountDetailsFormValue,
  AccountDetailsFormView
} from '../../account-details-form';

import styles from './account-details-modal-form.module.scss';

type AccountDetailsModalFormViewProps = {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (value: AccountDetailsFormValue) => void;
} & FormikProps<AccountDetailsFormValue>;

export const AccountDetailsModalFormView: React.FC<AccountDetailsModalFormViewProps> = ({
  visible,
  onCancel,
  ...formikProps
}) => (
  <>
    <Modal
      title={
        <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.ADD_DETAIL" />
      }
      visible={visible}
      data-test-el="account-details-modal"
      onCancel={onCancel}
      footer={
        <ModalFooter
          submitText={'INTAKE.CONTROLS.ADD'}
          cancelText={'INTAKE.CONTROLS.CANCEL'}
          isDisabled={!formikProps.isValid}
          onCancelClick={onCancel}
          onSubmitClick={() => {
            const { values, onSubmit, resetForm } = formikProps;
            onSubmit(values);
            resetForm();
          }}
        />
      }
    >
      <div className={styles.form}>
        <AccountDetailsFormView
          isDisabled={false}
          hasAccounts={true}
          {...formikProps}
        />
      </div>
    </Modal>
  </>
);
