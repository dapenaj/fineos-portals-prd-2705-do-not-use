export type AccountDetailsFormValue = {
  paymentPreferenceId?: string;
  accountName?: string;
  accountNo: string;
  accountType: string;
  routingNumber: string;
};
