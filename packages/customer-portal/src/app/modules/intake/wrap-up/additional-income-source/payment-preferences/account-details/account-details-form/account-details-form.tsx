import React from 'react';
import { FormikProps, withFormik } from 'formik';

import { AccountDetailsFormValue } from './account-details-form.type';
import { AccountDetailsFormView } from './account-details-form.view';

type AccountDetailsFormOwnProps = {
  isDisabled: boolean;
  hasAccounts: boolean;
  isSubmitting: boolean;
  onAddEftAccountDetails: (value: AccountDetailsFormValue) => void;
};

export type AccountDetailsFormProps = AccountDetailsFormOwnProps &
  FormikProps<AccountDetailsFormValue>;

export const AccountDetailsForm = withFormik<
  AccountDetailsFormProps,
  AccountDetailsFormValue
>({
  mapPropsToValues: () => ({} as AccountDetailsFormValue),
  handleSubmit: (values, { props }) => props.onAddEftAccountDetails(values)
})(AccountDetailsFormView) as React.ComponentType<AccountDetailsFormOwnProps>;
