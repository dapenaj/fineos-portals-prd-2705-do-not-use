import { withFormik } from 'formik';

import { AccountDetailsFormValue } from '../../account-details-form';

import { AccountDetailsModalFormView } from './account-details-modal-form.view';

type AccountDetailsModalFormProps = {
  visible: boolean;
  onCancel: () => void;
  onSubmit: (values: AccountDetailsFormValue) => void;
};

export const AccountDetailsModalForm = withFormik<
  AccountDetailsModalFormProps,
  AccountDetailsFormValue
>({
  mapPropsToValues: props => ({} as AccountDetailsFormValue),
  handleSubmit: (values, { props }) => props.onSubmit(values),
  isInitialValid: false
})(AccountDetailsModalFormView);
