import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from 'antd';

import { AccountDetailsFormValue } from '../account-details-form';

import { AccountDetailsModalForm } from './account-details-modal-form';

type AccountDetailsModalProps = {
  isDisabled: boolean;
  onAddEftAccountDetails: (values: AccountDetailsFormValue) => void;
};

type AccountDetailsModalState = {
  showModal: boolean;
};

export class AccountDetailsModal extends React.Component<
  AccountDetailsModalProps,
  AccountDetailsModalState
> {
  state: AccountDetailsModalState = {
    showModal: false
  };

  showModal = () => this.setState({ showModal: true });

  handleCancel = () => this.setState({ showModal: false });

  handleSubmit = (values: AccountDetailsFormValue) => {
    this.props.onAddEftAccountDetails(values);
    this.handleCancel();
  };

  render() {
    const { isDisabled } = this.props;
    const { showModal } = this.state;

    return (
      <>
        <div>
          <Button
            type={'link'}
            data-test-el="account-details-modal-btn"
            onClick={this.showModal}
            disabled={isDisabled}
          >
            <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.DIFFERENT_ACCOUNT" />
          </Button>
        </div>

        <AccountDetailsModalForm
          visible={showModal}
          onCancel={this.handleCancel}
          onSubmit={this.handleSubmit}
        />
      </>
    );
  }
}
