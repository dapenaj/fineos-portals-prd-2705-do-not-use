import React from 'react';
import { connect } from 'react-redux';
import { PaymentPreference } from 'fineos-js-api-client';

import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import { IntakeSectionStep } from '../../../../shared/types';
import { RootState } from '../../../../store';
import {
  IntakeSubmissionService,
  IntakeSubmission
} from '../../../../shared/services';
import { AdditionalIncomeValue } from '../wrap-up.type';

import { AdditionalIncomeSourceView } from './additional-income-source.view';
import { IncomeSource } from './income-source.type';

const mapStateToProps = ({
  intake: {
    workflow: { previousSubmissionState }
  }
}: RootState) => ({
  // assumption: if user on this step claim had to be created before,
  // and we wouldn't get to the first section where no submission model occurred
  previousSubmissionState
});

const mapDispatchToProps = {
  submitStep: submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;
type StateProps = ReturnType<typeof mapStateToProps>;

type AdditionalIncomeSourceProps = {
  step: IntakeSectionStep<AdditionalIncomeValue>;
};

type AdditionalIncomeSourceState = {
  submitting: boolean;
};

type AdditionalIncomeSourceContainerProps = AdditionalIncomeSourceProps &
  DispatchProps &
  StateProps;
export class AdditionalIncomeSourceContainer extends React.Component<
  AdditionalIncomeSourceContainerProps,
  AdditionalIncomeSourceState
> {
  state = {
    submitting: false
  };

  intakeSubmissionService = IntakeSubmissionService.getInstance();

  handleSubmit = async (
    incomeSources: IncomeSource[],
    paymentPreference: PaymentPreference
  ) => {
    this.setState({ submitting: true });

    // assumption: previousSubmissionState had to create claim
    const { previousSubmissionState, submitStep, step } = this.props;
    const { result: submissionStateResult } = previousSubmissionState!;

    // addOtherIncomeSources has .catch and there no defined flow if it would get broken
    const incomeSourcesResult = await this.intakeSubmissionService.addOtherIncomeSources(
      submissionStateResult,
      incomeSources
    );

    // setPaymentPreference handles errors thrown by it's API calls
    const result = await this.intakeSubmissionService.setPaymentPreference(
      incomeSourcesResult,
      paymentPreference
    );

    await IntakeSubmissionService.getInstance().completeIntake(
      previousSubmissionState!.model as IntakeSubmission,
      result,
      {
        additionalDetailsValue: incomeSources
      }
    );

    try {
      submitStep({
        ...step,
        value: {
          // value would be stored in sections, but no change to submissionState - it just get passed
          incomeSources,
          submissionState: {
            ...previousSubmissionState,
            result
          }
        }
      });
    } catch (error) {
      submitStep({
        ...step,
        value: { incomeSources, submissionState: { result: error } }
      });
    } finally {
      this.setState({ submitting: false });
    }
  };

  render() {
    const { submitting } = this.state;
    const {
      step: { isActive }
    } = this.props;

    return (
      <AdditionalIncomeSourceView
        isActive={isActive}
        stepSubmit={this.handleSubmit}
        submitting={submitting}
      />
    );
  }
}
export const AdditionalIncomeSource = connect(
  mapStateToProps,
  mapDispatchToProps
)(AdditionalIncomeSourceContainer);
