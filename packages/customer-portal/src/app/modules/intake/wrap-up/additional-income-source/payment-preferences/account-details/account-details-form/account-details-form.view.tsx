import React from 'react';
import { Form, FormikProps } from 'formik';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import { PaymentPreferenceAccountType } from '../../../../../../../shared/types';
import {
  FormLabel,
  FormikTextInput,
  FormikRadioGroupInput
} from '../../../../../../../shared/ui';

import { AccountDetailsFormValue } from './account-details-form.type';
import styles from './account-details-form.module.scss';

const ACCOUNT_TYPE_OPTIONS = [
  PaymentPreferenceAccountType.CHECKING,
  PaymentPreferenceAccountType.SAVINGS
];

export const validateField = (
  field: string,
  value: any,
  hasAccounts: boolean
) => {
  switch (field) {
    case 'accountName':
      if (!hasAccounts && !value) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
      break;
    case 'routingNumber':
    case 'accountNo':
    case 'accountType':
      if (!value) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
      break;
    default:
      return undefined;
  }
};

type AccountDetailsFormViewProps = {
  isDisabled: boolean;
  hasAccounts: boolean;
  isSubmitting: boolean;
} & FormikProps<AccountDetailsFormValue>;

export const AccountDetailsFormView: React.FC<AccountDetailsFormViewProps> = ({
  hasAccounts,
  isDisabled,
  isSubmitting,
  ...props
}) => (
  <Form name="account-details" className={styles.form}>
    {!hasAccounts && (
      <FormikTextInput
        name="accountName"
        isDisabled={isDisabled}
        label={
          <FormLabel
            required={true}
            label="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.ACCOUNT_HOLDER"
          />
        }
        validate={(value: any) =>
          validateField('accountName', value, hasAccounts)
        }
      />
    )}

    <FormikTextInput
      name="routingNumber"
      isDisabled={isDisabled}
      label={
        <FormLabel
          required={true}
          label="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.ROUTING"
        />
      }
      validate={(value: any) =>
        validateField('routingNumber', value, hasAccounts)
      }
    />

    <FormikTextInput
      name="accountNo"
      isDisabled={isDisabled}
      label={
        <FormLabel
          required={true}
          label="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.ACCOUNT_NUMBER"
        />
      }
      validate={(value: any) => validateField('accountNo', value, hasAccounts)}
    />

    <FormikRadioGroupInput
      {...props}
      label={<FormLabel label="MY_PROFILE.ACCOUNT_TYPE" required={true} />}
      isDisabled={isDisabled}
      name="accountType"
      optionPrefix="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES."
      radioOptions={ACCOUNT_TYPE_OPTIONS}
      validate={(value: any) =>
        validateField('accountType', value, hasAccounts)
      }
    />

    {!hasAccounts && (
      <Button
        htmlType="submit"
        type="primary"
        data-test-el="account-details-form-view-submit-btn"
        className={styles.button}
        disabled={isDisabled || !props.isValid}
        loading={isSubmitting}
      >
        <FormattedMessage id="INTAKE.CONTROLS.SAVE_AND_PROCEED" />
      </Button>
    )}
  </Form>
);
