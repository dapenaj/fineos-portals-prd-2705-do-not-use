import React from 'react';

import { IncomeSourceViewModel } from '../income-source-form';
import { Money } from '../../../../../shared/ui/money';
import { IntakeListItem } from '../../../ui/intake-list';

type IncomeSourceProps = {
  incomeSource: IncomeSourceViewModel;
  isDisabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
};

export const IncomeSourceItem: React.FC<IncomeSourceProps> = React.memo(
  ({ incomeSource, isDisabled, onEdit, onDelete }) => (
    <IntakeListItem
      isDisabled={isDisabled}
      onEdit={onEdit}
      onDelete={onDelete}
      data-test-el="income-source-item"
    >
      <div data-test-el="income-source-item-title">
        <strong>{incomeSource.incomeType}</strong>
      </div>
      {incomeSource.amount !== null && (
        <div data-test-el="income-source-item-description">
          <Money amount={incomeSource.amount} />
          {incomeSource.frequency && ` (${incomeSource.frequency})`}
        </div>
      )}
    </IntakeListItem>
  )
);
