import React from 'react';
import { Radio } from 'antd';

import { AccountDetailsFormValue } from '../account-details-form';

import { AccountDetailsListItem } from './account-details-list-item';
import styles from './account-details-list.module.scss';

type AccountDetailsListProps = {
  accounts: AccountDetailsFormValue[];
  isDisabled: boolean;
  selectedAccount?: AccountDetailsFormValue;
  onEftAccountSelected: (account: AccountDetailsFormValue) => void;
};

type AccountDetailsListState = {
  selectedAccount?: AccountDetailsFormValue;
};

const { Group } = Radio;

export class AccountDetailsList extends React.Component<
  AccountDetailsListProps,
  AccountDetailsListState
> {
  state: AccountDetailsListState = {};

  componentDidMount() {
    const { selectedAccount } = this.props;

    this.setState({ selectedAccount });
  }

  handleEftAccountChange = (index: number) => {
    const { accounts } = this.props;
    const selectedAccount = accounts[index];

    this.setState({ selectedAccount }, () =>
      this.props.onEftAccountSelected(selectedAccount)
    );
  };

  render() {
    const { accounts, selectedAccount, isDisabled } = this.props;

    const selectedAccountIndex = !!selectedAccount
      ? accounts.findIndex(
          account => account.accountNo === selectedAccount.accountNo
        )
      : -1;

    const selectedAccountValue = !!selectedAccount
      ? selectedAccount.accountNo
      : undefined;

    return (
      <Group
        className={styles.group}
        value={selectedAccountValue}
        disabled={isDisabled}
        name="eftAccountList"
      >
        {accounts.map((account, index) => (
          <AccountDetailsListItem
            key={index}
            accountDetails={account}
            isSelectedAccount={selectedAccountIndex === index}
            onEftAccountChange={() => this.handleEftAccountChange(index)}
          />
        ))}
      </Group>
    );
  }
}
