import { IncomeSource } from '../income-source.type';
import { Omit } from '../../../../../shared/types';

export type IncomeSourceViewModel = Omit<IncomeSource, 'incomeType'> & {
  incomeType: string | null;
};
