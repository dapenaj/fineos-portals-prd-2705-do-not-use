export type IncomeSource = {
  incomeType: string;
  frequency: string | null;
  amount: number | null;
  startDate: string | null;
  endDate: string | null;
};
