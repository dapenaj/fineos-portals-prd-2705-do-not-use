import React from 'react';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import { SelectInput } from '../../../../../shared/ui/form/select-input';
import { DefaultInputProps } from '../../../../../shared/ui/form/form.type';

const FREQUENCY_OPTIONS = [
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.MONTHLY',
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.QUARTERLY',
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.HALF_YEARLY',
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.YEARLY',
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.WEEKLY',
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.BI_WEEKLY',
  'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY.ONCE'
];

type FrequencySelectProps = DefaultInputProps & IntlProps;

export const FrequencySelect = injectIntl(
  ({ intl, ...props }: FrequencySelectProps) => {
    const options = FREQUENCY_OPTIONS.map(translationId => {
      const text = intl.formatMessage({ id: translationId });
      return {
        value: text,
        text
      };
    });
    return <SelectInput {...props} options={options} />;
  }
);
