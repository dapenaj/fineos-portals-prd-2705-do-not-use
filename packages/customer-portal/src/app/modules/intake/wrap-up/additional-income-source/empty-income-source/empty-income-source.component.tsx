import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from 'antd';

import moneyBillSvg from '../../../../../../assets/img/money-bill.svg';
import {
  EmptyListImage,
  EmptyListPlaceholder
} from '../../../ui/empty-list-placeholder';

type EmptyIncomeSourceProps = {
  onAdd: () => void;
  isDisabled: boolean;
};

export const EmptyIncomeSource: React.FC<EmptyIncomeSourceProps> = ({
  onAdd,
  isDisabled
}) => (
  <EmptyListPlaceholder
    icon={<EmptyListImage imageSrc={moneyBillSvg} />}
    content={
      <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.ADDITIONAL_INCOME_PROMO" />
    }
    button={
      <Button
        type="link"
        disabled={isDisabled}
        onClick={onAdd}
        data-test-el="add-new-income"
      >
        <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.ADD_NEW_INCOME_SOURCE" />
      </Button>
    }
    data-test-el="empty-income-source"
  />
);
