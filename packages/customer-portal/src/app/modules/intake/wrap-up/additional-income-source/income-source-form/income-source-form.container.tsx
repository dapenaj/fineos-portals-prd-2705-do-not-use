import React from 'react';
import { Moment } from 'moment';
import { FormikProps, withFormik } from 'formik';
import * as Yup from 'yup';

import { MONEY_FORMAT_REGEX } from '../../../../../shared/constants';

import { IncomeSourceFormView } from './income-source-form.view';
import { IncomeSourceViewModel } from './income-source-view-model.type';

type IncomeSourceFormOwnProps = {
  incomeSource: IncomeSourceViewModel | null;
  onSave: (value: IncomeSourceViewModel) => void;
  onCancel: () => void;
};

type IncomeSourceFormProps = IncomeSourceFormOwnProps &
  FormikProps<IncomeSourceViewModel>;

const incomeSourceValidationSchema: Yup.ObjectSchema<Partial<
  IncomeSourceViewModel
>> = Yup.object().shape({
  incomeType: Yup.string()
    .nullable()
    .required('COMMON.VALIDATION.REQUIRED'),
  amount: Yup.number()
    .nullable()
    .positive('COMMON.VALIDATION.MONEY_FORMAT')
    .typeError('COMMON.VALIDATION.MONEY_FORMAT')
    .test({
      message: 'COMMON.VALIDATION.MONEY_FORMAT',
      name: 'moneyFormat',
      test(amount: number | null) {
        return amount === null || MONEY_FORMAT_REGEX.test(String(amount));
      }
    }),
  endDate: Yup.mixed()
    .nullable()
    .when('startDate', (startDate: Moment | null) =>
      Yup.mixed().test({
        message:
          'INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.VALIDATION.END_DATE_BEFORE_START',
        name: 'min',
        test(endDate: Moment | null) {
          return (
            !startDate || !endDate || endDate.valueOf() >= startDate.valueOf()
          );
        }
      })
    )
});

export const IncomeSourceFormContainer = withFormik<
  IncomeSourceFormProps,
  IncomeSourceViewModel
>({
  mapPropsToValues: props =>
    props.incomeSource || {
      incomeType: null,
      frequency: null,
      amount: null,
      startDate: null,
      endDate: null
    },
  handleSubmit: (values, { props }) => props.onSave(values),
  validationSchema: incomeSourceValidationSchema
})(IncomeSourceFormView) as React.ComponentType<IncomeSourceFormOwnProps>;
