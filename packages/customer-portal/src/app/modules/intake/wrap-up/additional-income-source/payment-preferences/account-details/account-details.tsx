import React from 'react';

import { AccountDetailsList } from './account-details-list';
import { AccountDetailsFormValue } from './account-details-form';
import { AccountDetailsModal } from './account-details-modal';

type AccountDetailsProps = {
  accounts: AccountDetailsFormValue[];
  isDisabled: boolean;
  selectedAccount?: AccountDetailsFormValue;
  onEftAccountSelected: (account: AccountDetailsFormValue) => void;
};

export const AccountDetails: React.FC<AccountDetailsProps> = ({
  selectedAccount,
  isDisabled,
  accounts,
  onEftAccountSelected
}) => (
  <>
    <AccountDetailsList
      accounts={accounts}
      isDisabled={isDisabled}
      onEftAccountSelected={onEftAccountSelected}
      selectedAccount={selectedAccount}
    />

    <AccountDetailsModal
      isDisabled={isDisabled}
      onAddEftAccountDetails={onEftAccountSelected}
    />
  </>
);
