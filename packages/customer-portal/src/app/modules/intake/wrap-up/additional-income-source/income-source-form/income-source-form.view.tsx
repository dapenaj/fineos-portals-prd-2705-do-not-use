import React from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';
import { Form, FormikProps } from 'formik';

import {
  FormikMoneyInput,
  DatePickerInput,
  FormLabel
} from '../../../../../shared/ui';

import { IncomeSourceTypeSelect } from './income-source-type-select.component';
import { FrequencySelect } from './frequency-select.component';
import { IncomeSourceViewModel } from './income-source-view-model.type';
import styles from './income-source-form.module.scss';

type IncomeSourceFormViewProps = {
  incomeSource: IncomeSourceViewModel | null;
  onCancel: () => void;
} & FormikProps<IncomeSourceViewModel>;

export const IncomeSourceFormView: React.FC<IncomeSourceFormViewProps> = props => (
  <Form name="income-source" className={styles.incomeSourceForm}>
    <IncomeSourceTypeSelect
      {...props}
      name="incomeType"
      label={
        <FormLabel
          required={true}
          label="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.INCOME_TYPE_LABEL"
        />
      }
      data-test-el="incomeType"
    />

    <FrequencySelect
      {...props}
      name="frequency"
      label={
        <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.FREQUENCY_LABEL" />
      }
      data-test-el="frequency"
    />

    <div className={styles.incomeDetailsSection}>
      <FormikMoneyInput
        {...props}
        name="amount"
        label={
          <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.AMOUNT_LABEL" />
        }
        data-test-el="amount"
      />

      <DatePickerInput
        {...props}
        name="startDate"
        label={
          <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.START_DATE_LABEL" />
        }
        data-test-el="startDate"
      />

      <DatePickerInput
        {...props}
        name="endDate"
        label={
          <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.END_DATE_LABEL" />
        }
        data-test-el="endDate"
      />
    </div>

    <Button
      htmlType="submit"
      type="primary"
      disabled={!props.isValid}
      data-test-el="income-source-form-submit-control"
    >
      {props.incomeSource !== null ? (
        <FormattedMessage id="INTAKE.CONTROLS.SAVE_CHANGES" />
      ) : (
        <FormattedMessage id="INTAKE.CONTROLS.ADD" />
      )}
    </Button>

    <Button
      onClick={() => props.onCancel()}
      type="link"
      data-test-el="income-source-form-cancel-control"
    >
      <FormattedMessage id="INTAKE.CONTROLS.CANCEL" />
    </Button>
  </Form>
);
