import React from 'react';
import { PaymentPreference } from 'fineos-js-api-client';
import { RadioChangeEvent } from 'antd/lib/radio';

import { RadioGroup, FormLabel } from '../../../../../../shared/ui';
import {
  getDefaultPaymentPreference,
  getPaymentType,
  getPaymentTypeLabel
} from '../../../../../../shared/utils';
import { PaymentTypeLabel } from '../../../../../../shared/types';

type PaymentPreferencesProps = {
  paymentPreferences: PaymentPreference[];
  isDisabled: boolean;
  onPaymentPreferenceOptionChange: (
    preference: string,
    defaultPreference?: PaymentPreference
  ) => void;
};

type PaymentPreferencesState = {
  selectedPreference?: string;
};

const PAYMENT_TYPE_OPTIONS = [
  PaymentTypeLabel.CHECK,
  PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
];

export class PaymentPreferencesOptions extends React.Component<
  PaymentPreferencesProps,
  PaymentPreferencesState
> {
  state: PaymentPreferencesState = {};

  componentDidMount() {
    const { paymentPreferences } = this.props;

    if (!!paymentPreferences && !!paymentPreferences.length) {
      const defaultPreference =
        getDefaultPaymentPreference(paymentPreferences) ||
        paymentPreferences[0];
      const paymentType = getPaymentType(defaultPreference);

      const selectedPreference = getPaymentTypeLabel(paymentType);

      this.setState({ selectedPreference }, () =>
        this.props.onPaymentPreferenceOptionChange(
          selectedPreference,
          defaultPreference
        )
      );
    } else {
      this.setState({ selectedPreference: PaymentTypeLabel.CHECK }, () =>
        this.props.onPaymentPreferenceOptionChange(PaymentTypeLabel.CHECK)
      );
    }
  }

  handleOnChange = (e: RadioChangeEvent) => {
    const selectedPreference = e.target.value as string;

    this.setState({ selectedPreference }, () =>
      this.props.onPaymentPreferenceOptionChange(selectedPreference)
    );
  };

  render() {
    const { isDisabled } = this.props;
    const { selectedPreference } = this.state;

    return (
      <RadioGroup
        label={
          <div data-test-el="payment-method">
            <FormLabel label="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES.NAME" />
          </div>
        }
        name="paymentType"
        data-test-el="payment-preferences-options-form-group"
        onChange={this.handleOnChange}
        disabled={isDisabled}
        optionPrefix="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.PAYMENT_PREFERENCES."
        options={PAYMENT_TYPE_OPTIONS}
        defaultValue={selectedPreference}
      />
    );
  }
}
