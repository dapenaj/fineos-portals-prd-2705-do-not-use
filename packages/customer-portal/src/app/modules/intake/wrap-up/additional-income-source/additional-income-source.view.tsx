import React from 'react';
import { Button } from 'antd';
import { PaymentPreference } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';

import { ContentLayout } from '../../../../shared/layouts';

import { EmptyIncomeSource } from './empty-income-source';
import { IncomeSourceViewModel, IncomeSourceForm } from './income-source-form';
import { IncomeSource } from './income-source.type';
import { IncomeSourceItem } from './income-source-item';
import { PaymentPreferences } from './payment-preferences';

type AdditionalIncomeSourceViewProps = {
  isActive: boolean;
  stepSubmit: (
    incomeSources: IncomeSource[],
    paymentPreference: PaymentPreference
  ) => void;
  submitting: boolean;
};

type AdditionalIncomeSourceViewState = {
  incomeSources: IncomeSourceViewModel[];
  expandedIncomeSourceIndex: number;
  selectedPaymentMethod: string;
  selectedPaymentPreference?: PaymentPreference;
};
export class AdditionalIncomeSourceView extends React.Component<
  AdditionalIncomeSourceViewProps,
  AdditionalIncomeSourceViewState
> {
  state = {
    incomeSources: [],
    expandedIncomeSourceIndex: -1,
    selectedPaymentMethod: ''
  } as AdditionalIncomeSourceViewState;

  handleAddingIncomeSource = () => {
    this.setState({
      expandedIncomeSourceIndex: this.state.incomeSources.length
    });
  };

  handleClosingIncomeSource = (
    incomeSource: IncomeSourceViewModel | null,
    index: number
  ) => {
    const newIncomeSources = [...this.state.incomeSources];

    if (incomeSource) {
      newIncomeSources.splice(index, 1, incomeSource);
    }

    this.setState({
      incomeSources: newIncomeSources,
      expandedIncomeSourceIndex: -1
    });
  };

  handleEditIncomeSource = (index: number) => {
    this.setState({
      expandedIncomeSourceIndex: index
    });
  };

  handleDeleteIncomeSource = (index: number) => {
    const newIncomeSources = [...this.state.incomeSources];

    newIncomeSources.splice(index, 1);

    this.setState({
      incomeSources: newIncomeSources
    });
  };

  handlePaymentPreferenceChange = (
    paymentMethod: string,
    paymentPreference?: PaymentPreference
  ) =>
    this.setState({
      selectedPaymentMethod: paymentMethod,
      selectedPaymentPreference: paymentPreference
    });

  handleAddPaymentPreference = (
    paymentMethod: string,
    paymentPreference: PaymentPreference
  ) => {
    this.setState(
      {
        selectedPaymentMethod: paymentMethod,
        selectedPaymentPreference: paymentPreference
      },
      () => this.onSave()
    );
  };

  onSave = async () => {
    const { selectedPaymentPreference } = this.state;

    // close expanded form if any
    this.setState({
      expandedIncomeSourceIndex: -1
    });

    this.props.stepSubmit(
      this.state.incomeSources as IncomeSource[],
      selectedPaymentPreference!
    );
  };

  render() {
    const { incomeSources, expandedIncomeSourceIndex } = this.state;
    const { isActive, submitting } = this.props;
    const isDisabled = !isActive || submitting;
    const isExpandedForm = expandedIncomeSourceIndex > -1;
    const editedIncomeSource = incomeSources[expandedIncomeSourceIndex] || null;

    return (
      <ContentLayout
        direction="column"
        noPadding={true}
        data-test-el="additional-income"
      >
        <p data-test-el="additional-income-title">
          <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.ADDITIONAL_INCOME_QUESTION" />
        </p>
        {isExpandedForm ? (
          <IncomeSourceForm
            incomeSource={editedIncomeSource}
            onSave={changeIncomeSource =>
              this.handleClosingIncomeSource(
                changeIncomeSource,
                expandedIncomeSourceIndex
              )
            }
            onCancel={() =>
              this.handleClosingIncomeSource(null, expandedIncomeSourceIndex)
            }
          />
        ) : (
          <>
            {incomeSources.length ? (
              <>
                {incomeSources.map((incomeSource, index) => (
                  <IncomeSourceItem
                    key={index}
                    isDisabled={isDisabled}
                    incomeSource={incomeSource}
                    onEdit={() => this.handleEditIncomeSource(index)}
                    onDelete={() => this.handleDeleteIncomeSource(index)}
                  />
                ))}
                <div>
                  <Button
                    type="link"
                    disabled={isDisabled}
                    onClick={this.handleAddingIncomeSource}
                    data-test-el="add-another-income-source"
                  >
                    <FormattedMessage id="INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.ADD_ANOTHER_INCOME_SOURCE" />
                  </Button>
                </div>
              </>
            ) : (
              <EmptyIncomeSource
                onAdd={this.handleAddingIncomeSource}
                isDisabled={isDisabled}
              />
            )}
            <PaymentPreferences
              isDisabled={isDisabled}
              isSubmitting={submitting}
              onPaymentPreferenceChange={this.handlePaymentPreferenceChange}
              onAddPaymentPreference={this.handleAddPaymentPreference}
              onProceedClick={this.onSave}
            />
          </>
        )}
      </ContentLayout>
    );
  }
}
