import { IntakeSubmissionState } from '../../../shared/services/intake/submission';

import { IncomeSource } from './additional-income-source/income-source.type';

export type SupportingEvidenceValue = {
  // physician
  submissionState: IntakeSubmissionState;
};

export type AdditionalIncomeValue = {
  incomeSources: IncomeSource[];
  submissionState: IntakeSubmissionState;
};

export type WrapUpValue = {
  additionalIncome: AdditionalIncomeValue;
  supportingEvidence: SupportingEvidenceValue;
};

export type WrapUpSubmission = {
  additionalDetailsValue?: IncomeSource[];
};
