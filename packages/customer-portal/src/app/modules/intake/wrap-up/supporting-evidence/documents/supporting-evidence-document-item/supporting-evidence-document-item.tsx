import React from 'react';
import { Button, Modal, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import { UploadDocument } from '../../../../../../shared/types';
import { DocumentUpload } from '../../../../../../shared/ui';

import styles from './supporting-evidence-document-item.module.scss';

type SupportingEvidenceDocumentItemProps = {
  document: UploadDocument;
  loading: boolean;
  isActive: boolean;
  onAddDocumentation: (document: UploadDocument) => void;
  onDeleteDocumentation: (document: UploadDocument) => void;
};

type SupportingEvidenceDocumentItemState = {
  showModal: boolean;
};

export class SupportingEvidenceDocumentItem extends React.Component<
  SupportingEvidenceDocumentItemProps,
  SupportingEvidenceDocumentItemState
> {
  state: SupportingEvidenceDocumentItemState = {
    showModal: false
  };

  showModal = () =>
    this.setState({
      showModal: true
    });

  handleSubmit = (document: UploadDocument) => {
    this.props.onAddDocumentation(document);
    this.setState({ showModal: false });
  };

  handleOnCancel = () =>
    this.setState({
      showModal: false
    });

  render() {
    const { document, loading, onDeleteDocumentation, isActive } = this.props;
    const { showModal } = this.state;

    const {
      evidence: { name },
      request
    } = document;

    return (
      <>
        <div className={styles.wrapper}>
          <div>
            <div>{name}</div>
            {request && (
              <div>
                <Icon type="check" className={styles.icon} />
                {request.fileName}
              </div>
            )}

            {!request && (
              <Button
                className={styles.button}
                disabled={!isActive}
                type="link"
                onClick={this.showModal}
                data-test-el="supporting-evidence-upload-btn"
              >
                <FormattedMessage id="COMMON.ACTIONS.UPLOAD" />
              </Button>
            )}
          </div>

          {request && (
            <div className={styles.delete}>
              <Button
                type="link"
                icon="delete"
                disabled={!isActive}
                data-test-el="supporting-evidence-delete-btn"
                onClick={() => onDeleteDocumentation(document)}
              />
            </div>
          )}
        </div>

        <Modal
          title={<FormattedMessage id="SUPPORTING_EVIDENCE.UPLOAD_DOCUMENT" />}
          visible={showModal}
          onCancel={this.handleOnCancel}
          footer={null}
        >
          <DocumentUpload
            document={document}
            loading={loading}
            onCancel={this.handleOnCancel}
            onSubmit={this.handleSubmit}
          />
        </Modal>
      </>
    );
  }
}
