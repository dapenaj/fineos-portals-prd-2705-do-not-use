import React from 'react';

import { IntakeListItem } from '../../../ui/intake-list';
import { MedicalProvider } from '../medical-provider.type';

type MedicalProviderItemProps = {
  medicalProvider: MedicalProvider;
  isDisabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
  'data-test-el'?: string;
};

function formatMedicalProvidersDetails(
  medicalProvider: MedicalProvider
): string {
  const parts = [];
  const addressDetails = [];

  if (medicalProvider.phone) {
    parts.push(medicalProvider.phone);
  }

  if (medicalProvider.addressLine) {
    addressDetails.push(medicalProvider.addressLine);
  }

  if (medicalProvider.city) {
    addressDetails.push(medicalProvider.city);
  }

  if (medicalProvider.state) {
    addressDetails.push(medicalProvider.state);
  }

  if (medicalProvider.zipCode) {
    addressDetails.push(medicalProvider.zipCode);
  }

  if (medicalProvider.country) {
    addressDetails.push(medicalProvider.country);
  }

  if (addressDetails.length) {
    parts.push(addressDetails.join(', '));
  }

  return parts.join(' | ');
}

export const MedicalProviderItem: React.FC<MedicalProviderItemProps> = React.memo(
  ({ medicalProvider, isDisabled, onEdit, onDelete, ...props }) => (
    <IntakeListItem
      isDisabled={isDisabled}
      onEdit={onEdit}
      onDelete={onDelete}
      data-test-el={props['data-test-el'] || 'medical-provider-item'}
    >
      <div data-test-el="medical-provider-title">
        <strong>{medicalProvider.name}</strong>
      </div>
      <div data-test-el="medical-provider-description">
        {formatMedicalProvidersDetails(medicalProvider)}
      </div>
    </IntakeListItem>
  )
);
