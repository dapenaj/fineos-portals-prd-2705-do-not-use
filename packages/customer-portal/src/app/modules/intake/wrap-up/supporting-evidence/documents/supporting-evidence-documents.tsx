import React from 'react';

import { Spinner } from '../../../../../shared/ui';
import { UploadDocument } from '../../../../../shared/types';

import { SupportingEvidenceDocumentsList } from './supporting-evidence-documents-list';

type SupportingEvidenceDocumentsProps = {
  loading: boolean;
  isActive: boolean;
  documents: UploadDocument[] | null;
  onAddDocumentation: (document: UploadDocument) => void;
  onDeleteDocumentation: (document: UploadDocument) => void;
};

export const SupportingEvidenceDocuments: React.FC<SupportingEvidenceDocumentsProps> = ({
  documents,
  loading,
  isActive,
  onAddDocumentation,
  onDeleteDocumentation
}) => (
  <>
    {loading && <Spinner />}
    {!!documents && !!documents.length && (
      <SupportingEvidenceDocumentsList
        loading={loading}
        isActive={isActive}
        documents={documents}
        onAddDocumentation={onAddDocumentation}
        onDeleteDocumentation={onDeleteDocumentation}
      />
    )}
  </>
);
