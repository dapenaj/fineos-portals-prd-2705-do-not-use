import React from 'react';
import { FormattedMessage } from 'react-intl';

import { SupportingEvidenceDocumentItem } from '../supporting-evidence-document-item';
import { UploadDocument } from '../../../../../../shared/types';

import styles from './supporting-evidence-documents-list.module.scss';

type SupportingEvidenceDocumentsListProps = {
  documents: UploadDocument[];
  loading: boolean;
  isActive: boolean;
  onAddDocumentation: (document: UploadDocument) => void;
  onDeleteDocumentation: (document: UploadDocument) => void;
};

export const SupportingEvidenceDocumentsList: React.FC<SupportingEvidenceDocumentsListProps> = ({
  documents,
  onAddDocumentation,
  onDeleteDocumentation,
  loading,
  isActive
}) => (
  <>
    <div className={styles.spacing}>
      <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.DOCUMENTS.TITLE" />
    </div>
    <div className={styles.spacing}>
      <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.DOCUMENTS.REQUIRED" />
    </div>
    <div className={styles.spacing}>
      {documents.map((document, index) => (
        <SupportingEvidenceDocumentItem
          key={index}
          document={document}
          loading={loading}
          isActive={isActive}
          onAddDocumentation={onAddDocumentation}
          onDeleteDocumentation={onDeleteDocumentation}
        />
      ))}
    </div>
  </>
);
