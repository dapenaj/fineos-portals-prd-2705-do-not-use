import React from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import {
  EmptyListImage,
  EmptyListPlaceholder
} from '../../../ui/empty-list-placeholder';
import ambulanceSvg from '../../../../../../assets/img/ambulance.svg';

type AnotherMedicalProviderPlaceholderProps = {
  onAdd: () => void;
  isDisabled: boolean;
};

export const AnotherMedicalProviderPlaceholder: React.FC<AnotherMedicalProviderPlaceholderProps> = ({
  onAdd,
  isDisabled
}) => (
  <EmptyListPlaceholder
    icon={<EmptyListImage imageSrc={ambulanceSvg} />}
    content={
      <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.PLACEHOLDER" />
    }
    button={
      <Button
        type="link"
        disabled={isDisabled}
        onClick={onAdd}
        data-test-el="add-another-medical-care"
      >
        <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.ADD_BUTTON" />
      </Button>
    }
    data-test-el="another-medical-provider-placeholder"
  />
);
