import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, Col } from 'antd';

import { ContentLayout } from '../../../../shared/layouts/content-layout';
import { StepSectionTitle } from '../../ui/step-section-title/step-section-title.component';
import { Row } from '../../../../shared/ui/row';
import { UploadDocument } from '../../../../shared/types';

import {
  AnotherMedicalProviderPlaceholder,
  PrimaryPhysicianPlaceholder
} from './placeholders';
import { MedicalProvider } from './medical-provider.type';
import { MedicalProviderForm } from './medical-provider-form';
import { MedicalProviderItem } from './medical-provider-item';
import { SupportingEvidenceDocuments } from './documents';
import styles from './supporting-evidence.module.scss';

export type SupportingEvidenceData = {
  primaryPhysician: MedicalProvider | null;
  anotherMedicalProviders: MedicalProvider[];
};

type SupportingEvidenceViewProps = {
  isActive: boolean;
  loading: boolean;
  documents: UploadDocument[] | null;
  showMedicalProviders: boolean;
  onAddDocumentation: (document: UploadDocument) => void;
  onDeleteDocumentation: (document: UploadDocument) => void;
  onProceed: (supportingEvidence: SupportingEvidenceData) => void;
  submitting: boolean;
};

type SupportingEvidenceViewState = {
  isPrimaryPhysicianExpanded: boolean;
  anotherMedicalProvidersExpandedIndex: number;
} & SupportingEvidenceData;

export class SupportingEvidenceView extends React.Component<
  SupportingEvidenceViewProps,
  SupportingEvidenceViewState
> {
  state = {
    primaryPhysician: null,
    isPrimaryPhysicianExpanded: false,
    anotherMedicalProviders: [],
    anotherMedicalProvidersExpandedIndex: -1
  } as SupportingEvidenceViewState;

  handleChangePrimaryPhysician = () => {
    this.setState({
      isPrimaryPhysicianExpanded: true
    });
  };

  handleAddAnotherMedicalProvider = () => {
    this.setState({
      anotherMedicalProvidersExpandedIndex: this.state.anotherMedicalProviders
        .length
    });
  };

  handleProceed = () => {
    this.props.onProceed(this.state);
  };

  handleDeletePrimaryPhysician = () => {
    this.setState({
      primaryPhysician: null
    });
  };

  handleSavePrimaryPhysician = (medicalProvider: MedicalProvider) => {
    this.setState({
      primaryPhysician: medicalProvider,
      isPrimaryPhysicianExpanded: false
    });
  };

  handleCancelPrimaryPhysician = () => {
    this.setState({
      isPrimaryPhysicianExpanded: false
    });
  };

  handleDeleteAnotherMedicalProvider = (index: number) => {
    const changedAnotherMedicalProviders = [
      ...this.state.anotherMedicalProviders
    ];

    changedAnotherMedicalProviders.splice(index, 1);

    this.setState({
      anotherMedicalProviders: changedAnotherMedicalProviders,
      anotherMedicalProvidersExpandedIndex: -1
    });
  };

  handleSaveAnotherMedicalProvider = (
    anotherMedicalProvider: MedicalProvider
  ) => {
    const {
      anotherMedicalProviders,
      anotherMedicalProvidersExpandedIndex
    } = this.state;
    const changedAnotherMedicalProviders = [...anotherMedicalProviders];

    changedAnotherMedicalProviders.splice(
      anotherMedicalProvidersExpandedIndex,
      1,
      anotherMedicalProvider
    );

    this.setState({
      anotherMedicalProviders: changedAnotherMedicalProviders,
      anotherMedicalProvidersExpandedIndex: -1
    });
  };

  handleEditAnotherMedicalProvider = (index: number) => {
    this.setState({
      anotherMedicalProvidersExpandedIndex: index
    });
  };

  handleCancelAnotherMedicalProvider = () => {
    this.setState({
      anotherMedicalProvidersExpandedIndex: -1
    });
  };

  render() {
    const {
      isPrimaryPhysicianExpanded,
      primaryPhysician,
      anotherMedicalProviders,
      anotherMedicalProvidersExpandedIndex
    } = this.state;
    const {
      isActive,
      documents,
      onAddDocumentation,
      onDeleteDocumentation,
      showMedicalProviders,
      loading,
      submitting
    } = this.props;

    const isAnotherMedicalProvidersFormExpanded =
      anotherMedicalProvidersExpandedIndex !== -1;

    const hasAnotherMedicalProviders = Boolean(anotherMedicalProviders.length);

    return (
      <ContentLayout
        direction="column"
        noPadding={true}
        data-test-el="supporting-evidence"
      >
        <SupportingEvidenceDocuments
          isActive={isActive}
          loading={loading}
          documents={documents}
          onAddDocumentation={onAddDocumentation}
          onDeleteDocumentation={onDeleteDocumentation}
        />

        {showMedicalProviders && (
          <>
            {isPrimaryPhysicianExpanded && (
              <>
                <StepSectionTitle data-test-el="supporting-evidence-primary-form-title">
                  <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.FORM_TITLE" />
                </StepSectionTitle>

                <MedicalProviderForm
                  isMainMedicalProvider={true}
                  medicalProvider={primaryPhysician}
                  onSave={this.handleSavePrimaryPhysician}
                  onCancel={this.handleCancelPrimaryPhysician}
                />
              </>
            )}

            {isAnotherMedicalProvidersFormExpanded && (
              <>
                <StepSectionTitle data-test-el="supporting-evidence-another-form-title">
                  <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.FORM_TITLE" />
                </StepSectionTitle>

                <MedicalProviderForm
                  isMainMedicalProvider={false}
                  medicalProvider={
                    anotherMedicalProviders[
                      anotherMedicalProvidersExpandedIndex
                    ]
                  }
                  onSave={this.handleSaveAnotherMedicalProvider}
                  onCancel={this.handleCancelAnotherMedicalProvider}
                />
              </>
            )}

            {!isPrimaryPhysicianExpanded &&
              !isAnotherMedicalProvidersFormExpanded && (
                <>
                  <p data-test-el="supporting-evidence-description">
                    <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.SECTION_DESCRIPTION" />
                  </p>

                  <StepSectionTitle data-test-el="supporting-evidence-primary-section-title">
                    <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.PROMO_QUESTION" />
                  </StepSectionTitle>

                  {primaryPhysician && (
                    <MedicalProviderItem
                      medicalProvider={primaryPhysician}
                      isDisabled={!isActive}
                      onEdit={this.handleChangePrimaryPhysician}
                      onDelete={this.handleDeletePrimaryPhysician}
                      data-test-el="primary-physician-item"
                    />
                  )}

                  {!primaryPhysician && (
                    <PrimaryPhysicianPlaceholder
                      onAdd={this.handleChangePrimaryPhysician}
                      isDisabled={!isActive}
                    />
                  )}

                  <StepSectionTitle
                    className={styles.sectionStart}
                    data-test-el="supporting-evidence-another-section-title"
                  >
                    <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.PROMO_QUESTION" />
                  </StepSectionTitle>

                  {hasAnotherMedicalProviders && (
                    <>
                      {anotherMedicalProviders.map(
                        (anotherMedicalProvider, index) => (
                          <MedicalProviderItem
                            key={index}
                            medicalProvider={anotherMedicalProvider}
                            isDisabled={!isActive}
                            onEdit={() =>
                              this.handleEditAnotherMedicalProvider(index)
                            }
                            onDelete={() =>
                              this.handleDeleteAnotherMedicalProvider(index)
                            }
                          />
                        )
                      )}

                      <div>
                        <Button
                          type="link"
                          disabled={!isActive}
                          onClick={this.handleAddAnotherMedicalProvider}
                          data-test-el="add-one-more-another-medical-care"
                        >
                          <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.ADD_ONE_MORE_BUTTON" />
                        </Button>
                      </div>
                    </>
                  )}

                  {!hasAnotherMedicalProviders && (
                    <AnotherMedicalProviderPlaceholder
                      onAdd={this.handleAddAnotherMedicalProvider}
                      isDisabled={!isActive}
                    />
                  )}
                </>
              )}
          </>
        )}

        <Row className={styles.sectionStart}>
          <Col>
            <Button
              type="primary"
              disabled={!isActive}
              onClick={this.handleProceed}
              data-test-el="save-and-proceed-supporting-evidence"
              loading={submitting}
            >
              <FormattedMessage id="INTAKE.CONTROLS.SAVE_AND_PROCEED" />
            </Button>
          </Col>
        </Row>
      </ContentLayout>
    );
  }
}
