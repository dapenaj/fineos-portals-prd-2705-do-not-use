import React from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import {
  EmptyListImage,
  EmptyListPlaceholder
} from '../../../ui/empty-list-placeholder';
import userMdSvg from '../../../../../../assets/img/user-md.svg';

type PrimaryPhysicianPlaceholderProps = {
  onAdd: () => void;
  isDisabled: boolean;
};

export const PrimaryPhysicianPlaceholder: React.FC<PrimaryPhysicianPlaceholderProps> = ({
  onAdd,
  isDisabled
}) => (
  <EmptyListPlaceholder
    icon={<EmptyListImage imageSrc={userMdSvg} />}
    content={
      <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.PLACEHOLDER" />
    }
    button={
      <Button
        type="link"
        disabled={isDisabled}
        onClick={onAdd}
        data-test-el="add-primary-physician"
      >
        <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.ADD_BUTTON" />
      </Button>
    }
    data-test-el="primary-physician-placeholder"
  />
);
