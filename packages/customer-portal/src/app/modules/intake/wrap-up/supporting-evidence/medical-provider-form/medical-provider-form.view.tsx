import React from 'react';
import { Form, FormikProps } from 'formik';
import { Button, Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { MedicalProvider } from '../medical-provider.type';
import { Row } from '../../../../../shared/ui/row';
import { FormSectionTitle } from '../../../../../shared/ui/form/form-section-title';
import { StateSelect } from '../../../ui/inputs';
import { FormLabel } from '../../../../../shared/ui/form/form-label';
import { TextInput } from '../../../../../shared/ui/form/text-input';

import styles from './medical-provider-form.module.scss';

type MedicalProviderFormViewProps = {
  isMainMedicalProvider: boolean;
  medicalProvider: MedicalProvider | null;
  onCancel: () => void;
  onSave: (value: MedicalProvider) => void;
} & FormikProps<MedicalProvider>;

enum Context {
  MAIN,
  ADDITIONAL
}

// context dependent translations
const contextTranslations = {
  [Context.MAIN]: {
    name: (
      <FormLabel
        label="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.NAME"
        required={true}
      />
    ),
    phone: (
      <FormLabel
        label="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.PHONE"
        required={true}
      />
    ),
    addressSectionTitle: (
      <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.MAIN.ADDRESS_SECTION_TITLE" />
    )
  },
  [Context.ADDITIONAL]: {
    name: (
      <FormLabel
        label="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.NAME"
        required={true}
      />
    ),
    phone: (
      <FormLabel
        label="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.PHONE"
        required={true}
      />
    ),
    addressSectionTitle: (
      <FormattedMessage id="INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.MEDICAL_PROVIDER.ADDITIONAL.ADDRESS_SECTION_TITLE" />
    )
  }
};

function getContextTranslations(isMainMedicalProvider: boolean) {
  return contextTranslations[
    isMainMedicalProvider ? Context.MAIN : Context.ADDITIONAL
  ];
}

export const MedicalProviderFormView: React.FC<MedicalProviderFormViewProps> = ({
  isMainMedicalProvider,
  medicalProvider,
  onCancel,
  onSave,
  ...props
}) => {
  const contextTranslation = getContextTranslations(isMainMedicalProvider);
  return (
    <Form name="medical-provider" className={styles.medicalProviderContainer}>
      <Row>
        <Col xs={24} sm={12}>
          <TextInput
            {...props}
            name="name"
            label={contextTranslation.name}
            data-test-el="name"
          />
        </Col>
      </Row>

      <Row>
        <Col xs={24} sm={12}>
          <TextInput
            {...props}
            name="phone"
            label={contextTranslation.phone}
            data-test-el="phone"
          />
        </Col>
      </Row>

      <FormSectionTitle data-test-el="address-section-title">
        {contextTranslation.addressSectionTitle}
      </FormSectionTitle>

      <Row>
        <Col md={24} xs={24}>
          <TextInput
            {...props}
            name="addressLine"
            label={<FormLabel label="COMMON.ADDRESS.LABELS.ADDRESS_LINE" />}
            data-test-el="addressLine"
          />
        </Col>
      </Row>

      <Row>
        <Col md={18} xs={16}>
          <TextInput
            {...props}
            name="city"
            label={<FormLabel label="COMMON.ADDRESS.LABELS.CITY" />}
            data-test-el="city"
          />
        </Col>

        <Col md={6} xs={8}>
          <StateSelect
            {...props}
            name="state"
            label={<FormLabel label="COMMON.ADDRESS.LABELS.STATE" />}
            data-test-el="state"
          />
        </Col>
      </Row>

      <Row>
        <Col md={6} xs={24}>
          <TextInput
            {...props}
            name="zipCode"
            label={<FormLabel label="COMMON.ADDRESS.LABELS.ZIP_CODE" />}
            data-test-el="zipCode"
          />
        </Col>

        <Col md={18} xs={24}>
          <TextInput
            {...props}
            name="country"
            label={<FormLabel label="COMMON.ADDRESS.LABELS.COUNTRY" />}
            data-test-el="country"
          />
        </Col>
      </Row>

      <Row>
        <Col>
          <Button
            htmlType="submit"
            type="primary"
            disabled={!props.isValid}
            data-test-el="medical-provider-form-submit-control"
          >
            <FormattedMessage id="INTAKE.CONTROLS.SAVE_CHANGES" />
          </Button>

          <Button
            onClick={onCancel}
            type="link"
            data-test-el="medical-provider-form-cancel-control"
          >
            <FormattedMessage id="INTAKE.CONTROLS.CANCEL" />
          </Button>
        </Col>
      </Row>
    </Form>
  );
};
