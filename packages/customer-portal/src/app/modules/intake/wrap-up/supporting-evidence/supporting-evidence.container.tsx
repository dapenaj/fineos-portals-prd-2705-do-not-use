import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { DocumentService } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { RootState } from '../../../../store';
import {
  submitIntakeWorkflowStep,
  finishIntakeWorkflow
} from '../../../../store/intake/workflow/actions';
import { IntakeSectionStep, UploadDocument } from '../../../../shared/types';
import {
  IntakeSubmissionResult,
  IntakeSubmissionService,
  SupportingEvidenceUploadService
} from '../../../../shared/services';
import { showNotification } from '../../../../shared/ui';
import { SupportingEvidenceValue } from '../wrap-up.type';

import {
  SupportingEvidenceData,
  SupportingEvidenceView
} from './supporting-evidence.view';

const mapStateToProps = ({
  intake: {
    workflow: { previousSubmissionState }
  }
}: RootState) => ({
  // assumption: if user on this step claim had to be created before,
  // and we wouldn't get to the first section where no submission model occurred
  previousSubmissionState
});

const mapDispatchToProps = {
  submitIntakeWorkflowStep,
  finishIntakeWorkflow
};

type DispatchProps = typeof mapDispatchToProps;
type StateProps = ReturnType<typeof mapStateToProps>;

type SupportingEvidenceContainerOwnProps = {
  step: IntakeSectionStep<SupportingEvidenceValue>;
};

type SupportingEvidenceContainerProps = SupportingEvidenceContainerOwnProps &
  DispatchProps &
  StateProps &
  IntlProps;

type SupportingEvidenceContainerState = {
  loading: boolean;
  documents: UploadDocument[] | null;
  submitting: boolean;
};

export class SupportingEvidenceContainer extends React.Component<
  SupportingEvidenceContainerProps,
  SupportingEvidenceContainerState
> {
  state: SupportingEvidenceContainerState = {
    documents: null,
    loading: true,
    submitting: false
  };

  intakeSubmissionService = IntakeSubmissionService.getInstance();
  documentService = DocumentService.getInstance();
  uploadService = SupportingEvidenceUploadService.getInstance();

  async componentDidMount() {
    await this.fetchSupportingEvidence();
  }

  async componentDidUpdate(prevProps: SupportingEvidenceContainerProps) {
    if (
      this.props.step.isActive &&
      this.props.step.isActive !== prevProps.step.isActive
    ) {
      await this.fetchSupportingEvidence();
    }
  }

  fetchSupportingEvidence = async () => {
    const {
      previousSubmissionState,
      intl: { formatMessage }
    } = this.props;

    const {
      result: { createdNotificationId }
    } = previousSubmissionState!;

    if (createdNotificationId) {
      this.setState({ loading: true });

      try {
        const evidences = await this.documentService.getSupportingEvidence(
          createdNotificationId
        );

        const documents: UploadDocument[] = evidences
          .filter(evidence => !evidence.docReceived)
          .map((evidence, index) => ({
            id: index,
            caseId: createdNotificationId,
            evidence
          }));

        this.setState({ documents });
      } catch (error) {
        const title = formatMessage({
          id: 'INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.DOCUMENTS.FETCH_ERROR'
        });

        showNotification({
          title,
          type: 'error',
          content: error
        });
      } finally {
        this.setState({ loading: false });
      }
    }
  };

  handleAddDocumentation = (newDocuments: UploadDocument) => {
    const { documents } = this.state;
    // Can only add documentation if documents exists
    const existing = documents!.findIndex(({ id }) => id === newDocuments.id);

    if (existing !== -1) {
      const updatedDocuments = documents!;
      updatedDocuments[existing] = newDocuments;

      this.setState({
        documents: updatedDocuments
      });
    }
  };

  handleDeleteDocumentation = (removedDocument: UploadDocument) => {
    const { documents } = this.state;
    // Can only delete documentation if documents exists
    const index = documents!.findIndex(({ id }) => id === removedDocument.id);
    const updatedEvidence = documents!;

    updatedEvidence[index] = {
      caseId: removedDocument.caseId,
      evidence: removedDocument.evidence,
      id: removedDocument.id,
      request: undefined
    };

    this.setState({
      documents: updatedEvidence
    });
  };

  handleProceed = async (supportingEvidence: SupportingEvidenceData) => {
    this.setState({ submitting: true });
    // assumption: previousSubmissionState had to create claim
    const { previousSubmissionState } = this.props;
    const { result: submissionStateResult } = previousSubmissionState!;

    const result = await this.submitMedicalProviders(
      submissionStateResult,
      supportingEvidence
    );

    await this.submitDocuments();

    try {
      this.props.submitIntakeWorkflowStep({
        ...this.props.step,
        value: {
          // value would be stored in sections, but no change to submissionState - it just get passed
          supportingEvidence,
          submissionState: {
            ...previousSubmissionState,
            result
          }
        }
      });
    } finally {
      this.setState({ submitting: false });
    }
  };

  skipStep = () => {
    const { previousSubmissionState } = this.props;
    this.props.submitIntakeWorkflowStep({
      ...this.props.step,
      value: {
        // value would be stored in sections, but no change to submissionState - it just get passed
        submissionState: {
          ...previousSubmissionState
        }
      }
    });
  };

  submitMedicalProviders = async (
    submissionStateResult: IntakeSubmissionResult,
    { primaryPhysician, anotherMedicalProviders }: SupportingEvidenceData
  ) => {
    const result = await this.intakeSubmissionService.addPrimaryCarePhysician(
      submissionStateResult,
      primaryPhysician
    );

    return this.intakeSubmissionService.addAnotherMedicalProviders(
      result,
      anotherMedicalProviders
    );
  };

  submitDocuments = async () => {
    const { documents } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;

    if (!!documents && !!documents.length) {
      const requests = documents
        .filter(document => document.request !== undefined)
        .map(({ request, caseId }) => ({
          caseId,
          request: request!
        }));

      if (!!requests && !!requests.length) {
        this.setState({ loading: true });

        try {
          requests.forEach(
            async ({ caseId, request }) =>
              await this.uploadService.uploadCaseDocument(caseId, request)
          );
        } catch (error) {
          const title = formatMessage({
            id: 'INTAKE.WRAP_UP.SUPPORTING_EVIDENCE.DOCUMENTS.FETCH_ERROR'
          });

          showNotification({
            title,
            type: 'error',
            content: error
          });
        } finally {
          this.setState({ loading: false });
        }
      }
    }
  };

  render() {
    const {
      step: { isActive },
      previousSubmissionState
    } = this.props;
    const { documents, loading, submitting } = this.state;

    const {
      result: {
        createdClaim,
        createdAbsence: { isMedicalProviderRelated }
      }
    } = previousSubmissionState!;

    const isClaimCreated = !!(createdClaim && createdClaim.id);

    if (
      !loading &&
      !isClaimCreated &&
      !isMedicalProviderRelated &&
      _isEmpty(documents)
    ) {
      this.skipStep();
    }

    return (
      <SupportingEvidenceView
        loading={loading}
        isActive={isActive}
        onProceed={this.handleProceed}
        onAddDocumentation={this.handleAddDocumentation}
        onDeleteDocumentation={this.handleDeleteDocumentation}
        showMedicalProviders={isClaimCreated || !!isMedicalProviderRelated}
        documents={documents}
        submitting={submitting}
      />
    );
  }
}
export const SupportingEvidence = connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(SupportingEvidenceContainer));
