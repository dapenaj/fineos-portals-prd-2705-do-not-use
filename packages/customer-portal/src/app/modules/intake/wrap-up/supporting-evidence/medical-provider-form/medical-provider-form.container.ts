import React from 'react';
import { FormikProps, withFormik } from 'formik';
import * as Yup from 'yup';

import { PHONE_REGEX } from '../../../../../shared/constants';
import { MedicalProvider } from '../medical-provider.type';

import { MedicalProviderFormView } from './medical-provider-form.view';

type MedicalProviderFormOwnProps = {
  isMainMedicalProvider: boolean;
  medicalProvider: MedicalProvider | null;
  onSave: (value: MedicalProvider) => void;
  onCancel: () => void;
};

type MedicalProviderFormProps = MedicalProviderFormOwnProps &
  FormikProps<MedicalProvider>;

const medicalProviderValidationSchema: Yup.ObjectSchema<Partial<
  MedicalProvider
>> = Yup.object().shape({
  name: Yup.string()
    .nullable()
    .required('COMMON.VALIDATION.REQUIRED'),
  phone: Yup.string()
    .nullable()
    .required('COMMON.VALIDATION.REQUIRED')
    .matches(PHONE_REGEX, 'COMMON.VALIDATION.PHONE_FORMAT')
});

export const MedicalProviderForm = withFormik<
  MedicalProviderFormProps,
  MedicalProvider
>({
  mapPropsToValues: props =>
    props.medicalProvider || {
      name: '',
      phone: '',
      addressLine: '',
      city: '',
      state: '',
      zipCode: '',
      country: 'USA'
    },
  handleSubmit: (values, { props }) => props.onSave(values),
  validationSchema: medicalProviderValidationSchema
})(MedicalProviderFormView) as React.ComponentType<MedicalProviderFormOwnProps>;
