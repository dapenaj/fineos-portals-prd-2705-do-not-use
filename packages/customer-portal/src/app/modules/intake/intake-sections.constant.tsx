import React from 'react';
import { FormattedMessage } from 'react-intl';

import { IntakeSection, IntakeSectionId } from '../../shared/types';

import { yourRequestSteps, YourRequestValue } from './your-request';
import { DetailsValue, detailsSteps } from './details';
import { wrapUpSteps, WrapUpValue } from './wrap-up';

const youRequestSection: IntakeSection<YourRequestValue> = {
  id: IntakeSectionId.YOUR_REQUEST,
  title: <FormattedMessage id="INTAKE.SECTION.YOUR_REQUEST" />,
  isActive: true,
  isComplete: false,
  steps: yourRequestSteps
};

const detailsSection: IntakeSection<DetailsValue> = {
  id: IntakeSectionId.DETAILS,
  title: <FormattedMessage id="INTAKE.SECTION.DETAILS" />,
  isActive: false,
  isComplete: false,
  skip: false,
  steps: detailsSteps
};

const wrapUpSection: IntakeSection<WrapUpValue> = {
  id: IntakeSectionId.WRAP_UP,
  title: <FormattedMessage id="INTAKE.SECTION.WRAP_UP" />,
  isActive: false,
  isComplete: false,
  skip: false,
  steps: wrapUpSteps
};

export const intakeSections = [
  youRequestSection,
  detailsSection,
  wrapUpSection
];
