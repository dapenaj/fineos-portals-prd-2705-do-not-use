import React from 'react';
import { FormattedMessage } from 'react-intl';

import { IntakeSectionStep, IntakeSectionStepId } from '../../../shared/types';

import {
  PersonalDetailsValue,
  WorkDetailsValue,
  InitialRequestValue
} from './your-request.type';

const personalDetailsStep: IntakeSectionStep<PersonalDetailsValue> = {
  id: IntakeSectionStepId.PERSONAL_DETAILS,
  title: <FormattedMessage id="INTAKE.YOUR_REQUEST.PERSONAL_DETAILS" />,
  isComplete: false,
  isActive: true
};

const workDetailsStep: IntakeSectionStep<WorkDetailsValue> = {
  id: IntakeSectionStepId.WORK_DETAILS,
  title: <FormattedMessage id="INTAKE.YOUR_REQUEST.WORK_DETAILS" />,
  isComplete: false,
  isActive: false
};

const initialRequestStep: IntakeSectionStep<InitialRequestValue> = {
  id: IntakeSectionStepId.INITIAL_REQUEST,
  title: <FormattedMessage id="INTAKE.YOUR_REQUEST.INITIAL_REQUEST" />,
  isComplete: false,
  isActive: false
};

export const yourRequestSteps = [
  personalDetailsStep,
  workDetailsStep,
  initialRequestStep
];
