import { Customer, Contact } from 'fineos-js-api-client';

import {
  getCustomerEmail,
  getCustomerPhoneNumber,
  getCustomerAddress
} from '../../../../../shared/utils';

export const mapCustomerDetails = (detail: Customer | null) => {
  const address = getCustomerAddress(detail);

  return detail
    ? {
        firstName: detail.firstName ? detail.firstName : '',
        lastName: detail.lastName ? detail.lastName : '',
        dateOfBirth: detail.dateOfBirth ? detail.dateOfBirth : '',
        premiseNo: address && address.premiseNo ? address.premiseNo : '',
        addressLine1:
          address && address.addressLine1 ? address.addressLine1 : '',
        addressLine2:
          address && address.addressLine2 ? address.addressLine2 : '',
        addressLine3:
          address && address.addressLine3 ? address.addressLine3 : '',
        city: address && address.addressLine4 ? address.addressLine4 : '',
        state: address && address.addressLine6 ? address.addressLine6 : '',
        country: address && address.country ? address.country : 'USA',
        postCode: address && address.postCode ? address.postCode : ''
      }
    : {
        firstName: '',
        lastName: '',
        dateOfBirth: '',
        premiseNo: '',
        addressLine1: '',
        addressLine2: '',
        addressLine3: '',
        city: '',
        state: '',
        country: 'USA',
        postCode: ''
      };
};

export const mapCustomerContactDetails = (contact: Contact | null) => {
  return contact
    ? {
        email: contact.emailAddresses
          ? getCustomerEmail(contact.emailAddresses)
          : '',
        phoneNumber: contact.phoneNumbers
          ? getCustomerPhoneNumber(contact.phoneNumbers)
          : null
      }
    : { email: '', phoneNumber: null };
};
