import React from 'react';
import { connect } from 'react-redux';
import { Customer, Contact, CustomerService } from 'fineos-js-api-client';
import { WrappedComponentProps as IntlProps, injectIntl } from 'react-intl';

import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import { PersonalDetailsValue } from '../your-request.type';
import { IntakeSectionStep, AppConfig } from '../../../../shared/types';
import { showNotification } from '../../../../shared/ui';
import { withAppConfig } from '../../../../shared/contexts';

import { PersonalDetailsView } from './personal-details.view';
import { PersonalDetailsFormValue } from './form';
import { mapToCustomer, mapToContact } from './mappers';

const mapDispatchToProps = {
  submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;

type PersonalDetailsProps = {
  step: IntakeSectionStep<PersonalDetailsValue>;
  config: AppConfig;
} & DispatchProps &
  IntlProps;

type PersonalDetailsState = {
  saving: boolean;
};

export class PersonalDetailsContainer extends React.Component<
  PersonalDetailsProps,
  PersonalDetailsState
> {
  state: PersonalDetailsState = {
    saving: false
  };

  customerService: CustomerService = CustomerService.getInstance();

  handleStepSubmit = (formValue: PersonalDetailsFormValue) => {
    const {
      config: { customerDetail }
    } = this.props;

    const detail = mapToCustomer(customerDetail.customer!, formValue);
    const contact = mapToContact(customerDetail.contact!, formValue);

    const value: PersonalDetailsValue = {
      detail,
      contact
    };

    this.updatePersonalDetails(detail, contact);

    this.props.submitIntakeWorkflowStep({
      ...this.props.step,
      value
    });
  };

  updatePersonalDetails = async (customer: Customer, contact: Contact) => {
    const {
      intl: { formatMessage }
    } = this.props;
    /**
     * This needed to change because of a limitation in the fineos api,
     * the api calls update the same table and cause an error when asynchronous
     */
    this.setState({ saving: true });

    try {
      await this.customerService.updateCustomerDetails(customer);
      await this.customerService.updateCustomerContactDetails(contact);
    } catch (error) {
      const title = formatMessage({
        id: 'INTAKE.YOUR_REQUEST.PERSONAL_DETAILS_UPDATE_ERROR'
      });

      showNotification({
        title,
        type: 'error',
        content: error
      });
    } finally {
      this.setState({ saving: false });
    }
  };

  render() {
    const {
      config: {
        customerDetail: { customer, contact }
      },
      step: { isActive }
    } = this.props;
    const { saving } = this.state;

    return (
      <PersonalDetailsView
        detail={customer}
        contact={contact}
        saving={saving}
        isActive={isActive}
        stepSubmit={this.handleStepSubmit}
      />
    );
  }
}

export const PersonalDetails = withAppConfig(
  connect(null, mapDispatchToProps)(injectIntl(PersonalDetailsContainer))
);
