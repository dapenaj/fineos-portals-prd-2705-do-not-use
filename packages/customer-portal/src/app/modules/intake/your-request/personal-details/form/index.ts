export * from './address-details';
export * from './customer-details';
export * from './form-value.type';

export { PersonalDetailsForm } from './personal-details-form.container';
export { PersonalDetailsFormView } from './personal-details-form.view';
