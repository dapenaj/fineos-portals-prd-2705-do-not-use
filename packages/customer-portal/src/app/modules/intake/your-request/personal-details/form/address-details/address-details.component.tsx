import React from 'react';
import { FormikProps } from 'formik';
import { Col } from 'antd';

import {
  Row,
  TextInput,
  FormLabel,
  AddressRenderer
} from '../../../../../../shared/ui';
import { PersonalDetailsFormProps } from '../personal-details-form.container';
import { PersonalDetailsFormValue } from '../form-value.type';
import { StateSelect } from '../../../../ui/inputs';
import { getCustomerAddress } from '../../../../../../shared/utils';

type AddressDetailsProps = PersonalDetailsFormProps &
  FormikProps<PersonalDetailsFormValue>;

export const AddressDetails: React.FC<AddressDetailsProps> = props =>
  props.outsideUSA ? (
    <AddressRenderer address={getCustomerAddress(props.detail)} />
  ) : (
    <>
      <Row>
        <Col xs={24}>
          <TextInput
            {...props}
            name="addressLine1"
            isDisabled={props.loading || !props.isActive}
            label={
              <FormLabel
                label="COMMON.ADDRESS.LABELS.ADDRESS_LINE_1"
                required={true}
              />
            }
          />
        </Col>
      </Row>

      <Row>
        <Col xs={24} sm={12}>
          <TextInput
            {...props}
            name="addressLine2"
            isDisabled={props.loading || !props.isActive}
            label={<FormLabel label="COMMON.ADDRESS.LABELS.ADDRESS_LINE_2" />}
          />
        </Col>
        <Col xs={24} sm={12}>
          <TextInput
            {...props}
            name="addressLine3"
            isDisabled={props.loading || !props.isActive}
            label={<FormLabel label="COMMON.ADDRESS.LABELS.ADDRESS_LINE_3" />}
          />
        </Col>
      </Row>

      <Row>
        <Col xs={24} sm={16}>
          <TextInput
            {...props}
            name="city"
            isDisabled={props.loading || !props.isActive}
            label={
              <FormLabel label="COMMON.ADDRESS.LABELS.CITY" required={true} />
            }
          />
        </Col>
        <Col xs={24} sm={8}>
          <StateSelect
            {...props}
            name="state"
            isDisabled={props.loading || !props.isActive}
            label={
              <FormLabel label="COMMON.ADDRESS.LABELS.STATE" required={true} />
            }
          />
        </Col>
      </Row>

      <Row>
        <Col xs={24} sm={6}>
          <TextInput
            {...props}
            name="postCode"
            isDisabled={props.loading || !props.isActive}
            label={
              <FormLabel
                label="COMMON.ADDRESS.LABELS.ZIP_CODE"
                required={true}
              />
            }
          />
        </Col>
      </Row>
    </>
  );
