import React from 'react';
import { Form, FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';

import { ContentLayout } from '../../../../../shared/layouts';
import { IntakeStepControls } from '../../../ui';

import { PersonalDetailsFormValue } from './form-value.type';
import styles from './personal-details-form.module.scss';
import { CustomerDetails } from './customer-details';
import { AddressDetails } from './address-details';
import { PersonalDetailsFormProps } from './personal-details-form.container';

type PersonalDetailsFormViewProps = PersonalDetailsFormProps &
  FormikProps<PersonalDetailsFormValue>;

export const PersonalDetailsFormView: React.FC<PersonalDetailsFormViewProps> = props => (
  <Form name="personal-details">
    <ContentLayout direction="column" noPadding={true}>
      <CustomerDetails {...props} />

      <h3 className={styles.subtitle}>
        <FormattedMessage id="INTAKE.YOUR_REQUEST.PRIMARY_ADDRESS" />
      </h3>

      <AddressDetails {...props} />

      <IntakeStepControls
        {...props}
        isDisabled={!props.isValid || !props.isActive}
      />
    </ContentLayout>
  </Form>
);
