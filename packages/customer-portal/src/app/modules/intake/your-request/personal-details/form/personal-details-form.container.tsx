import { withFormik } from 'formik';
import { Customer, Contact } from 'fineos-js-api-client';
import * as Yup from 'yup';
import moment from 'moment';
import { isEmpty as _isEmpty } from 'lodash';

import { mapCustomerDetails, mapCustomerContactDetails } from '../mappers';
import { EMAIL_REGEX, NUMERIC_REGEX } from '../../../../../shared/constants';
import { API_DATE_FORMAT } from '../../../../../shared/utils';

import { PersonalDetailsFormView } from './personal-details-form.view';
import { PersonalDetailsFormValue } from './form-value.type';

export type PersonalDetailsFormProps = {
  detail: Customer | null;
  contact: Contact | null;
  outsideUSA: boolean;
  loading: boolean;
  isActive: boolean;
  submitStep: (values: PersonalDetailsFormValue) => void;
};

const PersonalDetailSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<PersonalDetailsFormValue>
>> = Yup.object().shape({
  firstName: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  lastName: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  dateOfBirth: Yup.date()
    .max(
      moment()
        .subtract(1, 'd')
        .format(API_DATE_FORMAT),
      'COMMON.VALIDATION.DOB_MAX_DATE'
    )
    .required('COMMON.VALIDATION.REQUIRED')
    .nullable(),
  phoneNumber: Yup.object().shape({
    intCode: Yup.string()
      .max(10, 'COMMON.VALIDATION.MAX_INTL_CODE')
      .matches(NUMERIC_REGEX, 'COMMON.VALIDATION.NUMERIC_FORMAT')
      .nullable(),
    areaCode: Yup.string()
      .required('COMMON.VALIDATION.REQUIRED')
      .max(10, 'COMMON.VALIDATION.MAX_AREA_CODE')
      .matches(NUMERIC_REGEX, 'COMMON.VALIDATION.NUMERIC_FORMAT'),
    telephoneNo: Yup.string()
      .required('COMMON.VALIDATION.REQUIRED')
      .max(20, 'COMMON.VALIDATION.MAX_TELEPHONE')
      .matches(NUMERIC_REGEX, 'COMMON.VALIDATION.NUMERIC_FORMAT')
  }),
  email: Yup.string().test('email', 'COMMON.VALIDATION.EMAIL_FORMAT', email => {
    if (!_isEmpty(email)) {
      const schema = Yup.string().matches(
        EMAIL_REGEX,
        'COMMON.VALIDATION.EMAIL_FORMAT'
      );
      return schema.isValidSync(email);
    }
    return true;
  }),
  addressLine1: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  city: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  state: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  postCode: Yup.string().required('COMMON.VALIDATION.REQUIRED')
});

const mapPropsToValues = ({
  detail,
  contact
}: PersonalDetailsFormProps): PersonalDetailsFormValue => ({
  ...mapCustomerDetails(detail),
  ...mapCustomerContactDetails(contact)
});

export const PersonalDetailsForm = withFormik<
  PersonalDetailsFormProps,
  PersonalDetailsFormValue
>({
  mapPropsToValues,
  validationSchema: PersonalDetailSchema,
  // Need to validate the address details and contact info from the API
  isInitialValid: props =>
    PersonalDetailSchema.isValidSync(mapPropsToValues(props)),
  handleSubmit: (values, { props }) => props.submitStep(values)
})(PersonalDetailsFormView);
