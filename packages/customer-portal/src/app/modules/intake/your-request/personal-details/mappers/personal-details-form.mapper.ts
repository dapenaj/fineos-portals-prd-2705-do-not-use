import { Customer, Contact, CustomerAddress } from 'fineos-js-api-client';

import {
  formatStringToPhoneNumber,
  formatDateForApi,
  formatPhoneToString
} from '../../../../../shared/utils';
import { PersonalDetailsFormValue } from '../form';

export const mapToCustomer = (
  customer: Customer,
  formValue: PersonalDetailsFormValue
): Customer => ({
  ...customer,
  firstName: formValue.firstName,
  lastName: formValue.lastName,
  dateOfBirth: formatDateForApi(formValue.dateOfBirth),
  customerAddress: mapToCustomerAddress(formValue)
});

export const mapToCustomerAddress = ({
  premiseNo,
  addressLine1,
  addressLine2,
  addressLine3,
  city,
  state,
  postCode,
  country
}: PersonalDetailsFormValue): CustomerAddress => ({
  address: {
    premiseNo,
    addressLine1,
    addressLine2,
    addressLine3,
    addressLine4: city,
    addressLine5: '',
    addressLine6: state,
    addressLine7: '',
    postCode,
    country
  }
});

export const mapToContact = (
  { emailAddresses, phoneNumbers }: Contact,
  { email, phoneNumber }: PersonalDetailsFormValue
): Contact => {
  const emails = [...emailAddresses];
  if (!!email) {
    // if the email is not in the current list, then add it
    if (emailAddresses.findIndex(val => val.emailAddress === email) < 0) {
      emails.push({ emailAddress: email, preferred: false });
    }
  }

  const phones = [...phoneNumbers];
  const { areaCode, intCode, telephoneNo } = formatStringToPhoneNumber(
    formatPhoneToString(phoneNumber!)
  );
  // if the phone number is not in the current list, then add it
  if (phoneNumbers.findIndex(val => val.telephoneNo === telephoneNo) < 0) {
    phones.push({
      preferred: false,
      phoneNumberType: 'Phone',
      areaCode,
      intCode,
      telephoneNo
    });
  }

  return {
    emailAddresses: emails,
    phoneNumbers: phones
  };
};
