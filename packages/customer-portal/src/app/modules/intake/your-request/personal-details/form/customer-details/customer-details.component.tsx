import React from 'react';
import { FormikProps } from 'formik';
import { Col } from 'antd';

import {
  Row,
  TextInput,
  DatePickerInput,
  FormLabel,
  PhoneInput
} from '../../../../../../shared/ui';
import { PersonalDetailsFormValue } from '../form-value.type';
import { PersonalDetailsFormProps } from '../personal-details-form.container';

type CustomerDetailsProps = PersonalDetailsFormProps &
  FormikProps<PersonalDetailsFormValue>;

export const CustomerDetails: React.FC<CustomerDetailsProps> = props => (
  <div data-test-el="customer-details">
    <Row>
      <Col xs={24} sm={12}>
        <TextInput
          {...props}
          name="firstName"
          isDisabled={props.loading || !props.isActive}
          label={
            <FormLabel
              required={true}
              label="INTAKE.YOUR_REQUEST.LABELS.FIRST_NAME"
            />
          }
        />
      </Col>
      <Col xs={24} sm={12}>
        <TextInput
          {...props}
          name="lastName"
          isDisabled={props.loading || !props.isActive}
          label={
            <FormLabel
              required={true}
              label="INTAKE.YOUR_REQUEST.LABELS.LAST_NAME"
            />
          }
        />
      </Col>
    </Row>
    <Row>
      <Col xs={24} sm={12}>
        <DatePickerInput
          {...props}
          name="dateOfBirth"
          isDisabled={props.loading || !props.isActive}
          label={
            <FormLabel label="INTAKE.YOUR_REQUEST.LABELS.DOB" required={true} />
          }
        />
      </Col>
      <Col xs={24} sm={12}>
        <TextInput
          {...props}
          name="email"
          isDisabled={props.loading || !props.isActive}
          label={<FormLabel label="INTAKE.YOUR_REQUEST.LABELS.EMAIL" />}
        />
      </Col>
    </Row>

    <PhoneInput
      {...props}
      name="phoneNumber"
      isDisabled={props.loading || !props.isActive}
    />
  </div>
);
