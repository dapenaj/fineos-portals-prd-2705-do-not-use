import React from 'react';
import { Customer, Contact } from 'fineos-js-api-client';

import { getCustomerAddress, isOutsideUSA } from '../../../../shared/utils';

import { PersonalDetailsForm, PersonalDetailsFormValue } from './form';

type PersonalDetailsProps = {
  detail: Customer | null;
  contact: Contact | null;
  saving: boolean;
  isActive: boolean;
  stepSubmit: (formValue: PersonalDetailsFormValue) => void;
};

export const PersonalDetailsView: React.FC<PersonalDetailsProps> = ({
  detail,
  contact,
  saving,
  isActive,
  stepSubmit
}) => (
  <PersonalDetailsForm
    detail={detail}
    contact={contact}
    submitStep={stepSubmit}
    outsideUSA={isOutsideUSA(getCustomerAddress(detail))}
    loading={saving}
    isActive={isActive}
  />
);
