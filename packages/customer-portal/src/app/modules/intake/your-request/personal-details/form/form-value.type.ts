import { Phone } from 'fineos-js-api-client';

export type PersonalDetailsFormValue = {
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  email: string;
  phoneNumber: Phone | null;
  premiseNo: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  city: string;
  state: string;
  postCode: string;
  country: string;
};
