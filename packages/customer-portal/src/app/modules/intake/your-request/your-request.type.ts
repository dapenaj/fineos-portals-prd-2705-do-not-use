import { Customer, Contact, CustomerOccupation } from 'fineos-js-api-client';

import { EventOptionId } from '../../../shared/types';

export type PersonalDetailsValue = {
  detail: Customer;
  contact: Contact;
};

export type WorkDetailsValue = {
  occupation: CustomerOccupation | null;
};

export type InitialRequestValue = EventOptionId[];

export type YourRequestValue = {
  personalDetails: PersonalDetailsValue;
  workDetails: WorkDetailsValue;
  initialRequest: InitialRequestValue;
};
