import { EventOptionId } from '../../../../shared/types';

export const updateSelectedOptions = (
  options: EventOptionId[],
  selected: EventOptionId,
  index: number
): EventOptionId[] => {
  const selections = options.slice(0, index);
  selections.push(selected);

  return selections;
};

export const updateOptions = (
  currentOptions: EventOptionId[][],
  nextOptions: EventOptionId[],
  nextIndex: number
): EventOptionId[][] => {
  const options = currentOptions.slice(0, nextIndex);

  if (nextOptions.length > 0) {
    options.push(nextOptions);
  }

  return options;
};
