import { EventOptionId } from '../../../../../shared/types';

export const getNumberTextValue = (value: number) => {
  if (value === 2) {
    return 'TWO';
  }

  return 'ONE';
};

export const getTitle = (event: EventOptionId, index: number): string => {
  return `${event}_${getNumberTextValue(index)}`;
};
