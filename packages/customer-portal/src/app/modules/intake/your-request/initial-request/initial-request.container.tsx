import React from 'react';
import { connect } from 'react-redux';

import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import { EventOptionId, IntakeSectionStep } from '../../../../shared/types';
import { EventDetailOptionService } from '../../../../shared/services';
import { InitialRequestValue } from '../your-request.type';

import { InitialRequestView } from './initial-request.view';
import { updateSelectedOptions, updateOptions } from './initial-request.util';

const mapDispatchToProps = {
  submitStep: submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;

type InitialRequestProps = {
  step: IntakeSectionStep<InitialRequestValue>;
} & DispatchProps;

type InitialRequestState = {
  options: EventOptionId[][];
  selectedOptions: EventOptionId[];
  isValid: boolean;
};

export class InitialRequestContainer extends React.Component<
  InitialRequestProps,
  InitialRequestState
> {
  eventDetailOptionService = EventDetailOptionService.getInstance();
  startOptions = this.eventDetailOptionService.start();

  state = {
    options: [this.startOptions],
    selectedOptions: [] as EventOptionId[],
    isValid: false
  };

  handleOptionSelect = (selected: EventOptionId, index: number) => {
    const selectedOptions: EventOptionId[] = updateSelectedOptions(
      this.state.selectedOptions,
      selected,
      index
    );

    const options: EventOptionId[][] = updateOptions(
      this.state.options,
      this.eventDetailOptionService.next(selectedOptions),
      index + 1
    );

    this.setState({
      ...this.state,
      selectedOptions,
      options,
      isValid:
        selectedOptions.length !== 0 &&
        selectedOptions.length === options.length
    });
  };

  handleStepSubmit = () => {
    const { selectedOptions } = this.state;

    this.props.submitStep({
      ...this.props.step,
      value: selectedOptions,
      skipSteps: this.eventDetailOptionService.getSkipSteps(selectedOptions)
    });
  };

  render() {
    const { options, selectedOptions, isValid } = this.state;
    const {
      step: { isActive }
    } = this.props;

    return (
      <InitialRequestView
        options={options}
        isActive={isActive}
        optionSelect={this.handleOptionSelect}
        values={selectedOptions}
        isValid={isValid}
        submitForm={this.handleStepSubmit}
      />
    );
  }
}

export const InitialRequest = connect(
  null,
  mapDispatchToProps
)(InitialRequestContainer);
