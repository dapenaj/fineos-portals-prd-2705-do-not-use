import React from 'react';

import { EventOptionId } from '../../../../../shared/types';
import { Popover } from '../../../../../shared/ui';
import { getMessageForLocale } from '../../../../../i18n/utils';

type InitialRequestOptionPopoverProps = { content: EventOptionId };

export const InitialRequestOptionPopover: React.FC<InitialRequestOptionPopoverProps> = ({
  content
}) => (
  <>
    {getMessageForLocale('en', `POPOVER.${content}`) && (
      <Popover content={content} />
    )}
  </>
);
