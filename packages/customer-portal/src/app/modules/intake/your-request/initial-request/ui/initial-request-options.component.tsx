import React from 'react';

import { RadioGroup } from '../../../../../shared/ui';
import { EventOptionId } from '../../../../../shared/types';

import { InitialRequestOptionsTitle } from './initial-request-options-title';
import { InitialRequestOptionPopover } from './intial-request-option-popover.component';

type PersonalDetailsFormViewProps = {
  index: number;
  selections: EventOptionId[];
  options: EventOptionId[];
  isActive: boolean;
  optionSelect: (option: EventOptionId) => void;
};

export const InitialRequestOptions: React.FC<PersonalDetailsFormViewProps> = ({
  index,
  selections,
  options,
  optionSelect,
  isActive
}) => (
  <>
    <InitialRequestOptionsTitle
      title={index === 0 ? 'HOW_CAN_WE_HELP_YOU' : selections[index - 1]}
    />

    <RadioGroup
      optionPrefix="INTAKE.YOUR_REQUEST.INITIAL_REQUEST_OPTIONS."
      options={options}
      onChange={event => optionSelect(event.target.value)}
      disabled={!isActive}
      renderPopover={optionsIndex => (
        <InitialRequestOptionPopover content={options[optionsIndex]} />
      )}
    />
  </>
);
