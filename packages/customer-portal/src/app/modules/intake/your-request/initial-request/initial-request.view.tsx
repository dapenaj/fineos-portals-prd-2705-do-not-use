import React from 'react';
import { Col } from 'antd';

import { EventOptionId } from '../../../../shared/types';
import { Row } from '../../../../shared/ui';
import { ContentLayout } from '../../../../shared/layouts';

import { InitialRequestOptions, InitialRequestControls } from './ui';
import styles from './initial-request.module.scss';

type InitialRequestViewProps = {
  options: EventOptionId[][];
  values: EventOptionId[];
  isActive: boolean;
  optionSelect: (option: EventOptionId, index: number) => void;
  isValid: boolean;
  submitForm: () => void;
};

export const InitialRequestView: React.FC<InitialRequestViewProps> = ({
  isActive,
  options,
  optionSelect,
  values,
  isValid,
  submitForm
}) => (
  <ContentLayout direction="column" noPadding={true}>
    <Row>
      <Col>
        <div data-test-el="initial-request">
          {options.map(
            (children, index) =>
              children.length > 0 && (
                <div key={index} data-test-el="initial-request-option-group">
                  <InitialRequestOptions
                    index={index}
                    selections={values}
                    options={children}
                    isActive={isActive}
                    optionSelect={value => optionSelect(value, index)}
                  />
                </div>
              )
          )}

          <div className={styles.spacing}>
            <InitialRequestControls
              submitForm={submitForm}
              disabled={!isValid || !isActive}
            />
          </div>
        </div>
      </Col>
    </Row>
  </ContentLayout>
);
