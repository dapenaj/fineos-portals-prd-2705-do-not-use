import React from 'react';
import { Col, Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import { Row } from '../../../../../shared/ui';

type InitialRequestControlsProps = {
  disabled: boolean;
  submitForm: () => void;
};

export const InitialRequestControls: React.FC<InitialRequestControlsProps> = ({
  disabled,
  submitForm
}) => (
  <Row>
    <Col>
      <Button
        htmlType="submit"
        type="primary"
        disabled={disabled}
        onClick={submitForm}
      >
        <FormattedMessage id="INTAKE.CONTROLS.PROCEED" />
      </Button>
    </Col>
  </Row>
);
