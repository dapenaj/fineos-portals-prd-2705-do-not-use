import React from 'react';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';

import intialRequestStyles from '../../initial-request.module.scss';

import styles from './initial-request-options-title.module.scss';

type InitialRequestOptionsTitleProps = {
  title: string;
};

export const InitialRequestOptionsTitle: React.FC<InitialRequestOptionsTitleProps> = ({
  title
}) => (
  <h3 className={classnames(intialRequestStyles.title, styles.isRequired)}>
    <FormattedMessage
      id={`INTAKE.YOUR_REQUEST.INITIAL_REQUEST_SUBTITLES.${title}`}
    />
  </h3>
);
