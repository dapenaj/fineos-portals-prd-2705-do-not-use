import React from 'react';
import { CustomerOccupation } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { Col, Button } from 'antd';

import { Panel, Row } from '../../../../../../shared/ui';
import { formatDateForDisplay } from '../../../../../../shared/utils';

import styles from './existing-work-details.module.scss';

type ExistingWorkDetailsProps = {
  occupation: CustomerOccupation;
  isActive: boolean;
  onChangeRequest: () => void;
  onNewJobRequest: () => void;
  onProceed: () => void;
};

export const ExistingWorkDetails: React.FC<ExistingWorkDetailsProps> = ({
  occupation: { jobTitle, employer, dateJobBegan },
  isActive,
  onChangeRequest,
  onNewJobRequest,
  onProceed
}) => (
  <Row>
    <Col>
      <div data-test-el="existing-work-details">
        <Panel>
          <div className={styles.panel}>
            <h1 className={styles.title}>{employer}</h1>
            <div className={styles.details}>
              <p>
                <FormattedMessage
                  id="INTAKE.YOUR_REQUEST.WORK_DETAILS_MESSAGE"
                  values={{
                    title: jobTitle,
                    date: formatDateForDisplay(dateJobBegan)
                  }}
                />
              </p>
              <Button
                type="link"
                onClick={onChangeRequest}
                disabled={!isActive}
                data-test-el="request-change-btn"
              >
                <FormattedMessage id="INTAKE.YOUR_REQUEST.REQUEST_A_CHANGE" />
              </Button>
            </div>
          </div>
        </Panel>
        <Button
          className={styles.spacing}
          type="link"
          onClick={onNewJobRequest}
          disabled={!isActive}
          data-test-el="new-occupation-btn"
        >
          <FormattedMessage id="INTAKE.YOUR_REQUEST.I_NO_LONGER_WORK_HERE" />
        </Button>

        <div className={styles.spacing}>
          <Button
            type="primary"
            data-test-el="work-proceed-btn"
            onClick={onProceed}
            disabled={!isActive}
          >
            <FormattedMessage id="INTAKE.CONTROLS.CONFIRM_AND_PROCEED" />
          </Button>
        </div>
      </div>
    </Col>
  </Row>
);
