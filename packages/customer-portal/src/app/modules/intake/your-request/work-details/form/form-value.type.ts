export type WorkDetailsFormValue = {
  employer: string;
  jobTitle: string;
  dateJobBegan: string;
};
