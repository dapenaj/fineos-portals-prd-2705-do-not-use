import { CustomerOccupation } from 'fineos-js-api-client';
import moment from 'moment';

export const getCurrentCustomerOccupation = (
  occupations: CustomerOccupation[] | null
): CustomerOccupation | null => {
  if (!!occupations && !!occupations.length) {
    return occupations.length > 1
      ? occupations.reduce((prev, curr) =>
          moment(curr.dateJobBegan).isAfter(moment(prev.dateJobBegan))
            ? curr
            : prev
        )
      : occupations[0];
  }
  return null;
};
