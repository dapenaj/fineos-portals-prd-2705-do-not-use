import React from 'react';
import { FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';
import { Col, Form } from 'antd';

import {
  Row,
  TextInput,
  FormLabel,
  DatePickerInput
} from '../../../../../shared/ui';
import { IntakeStepControls } from '../../../ui';

import { WorkDetailsFormValue } from './form-value.type';
import { WorkDetailsProps } from './work-details';
import styles from './work-details-form.module.scss';

type WorkDetailsFormProps = WorkDetailsProps &
  FormikProps<WorkDetailsFormValue>;

export const WorkDetailsFormComponent: React.FC<WorkDetailsFormProps> = props => (
  <Form name="work-details" onSubmit={props.handleSubmit}>
    {props.formTitle && (
      <p className={styles.question}>
        <FormattedMessage id={props.formTitle} />
      </p>
    )}
    {props.showEmployer && (
      <Row>
        <Col>
          <TextInput
            {...props}
            name="employer"
            isDisabled={!props.isActive}
            label={
              <FormLabel
                required={true}
                label="INTAKE.YOUR_REQUEST.LABELS.EMPLOYER"
              />
            }
          />
        </Col>
      </Row>
    )}

    <Row>
      <Col>
        <TextInput
          {...props}
          name="jobTitle"
          isDisabled={!props.isActive}
          label={
            <FormLabel
              required={true}
              label="INTAKE.YOUR_REQUEST.LABELS.JOB_TITLE"
            />
          }
        />
      </Col>
    </Row>

    <Row>
      <Col sm={24} md={12}>
        <DatePickerInput
          {...props}
          name="dateJobBegan"
          isDisabled={!props.isActive}
          label={
            <FormLabel
              label="INTAKE.YOUR_REQUEST.LABELS.DATE_OF_HIRE"
              required={true}
            />
          }
        />
      </Col>
    </Row>

    <div className={styles.spacing}>
      <IntakeStepControls
        {...props}
        showCancel={props.showCancel}
        submitLabel={props.submitLabel}
        cancel={props.cancel}
        isDisabled={!props.isValid || !props.isActive}
      />
    </div>
  </Form>
);
