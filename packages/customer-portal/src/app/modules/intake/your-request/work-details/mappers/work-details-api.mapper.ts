import { CustomerOccupation } from 'fineos-js-api-client';

export const mapOccupation = (occupation: CustomerOccupation | null) =>
  occupation
    ? {
        employer: occupation.employer ? occupation.employer : '',
        jobTitle: occupation.jobTitle ? occupation.jobTitle : '',
        dateJobBegan: occupation.dateJobBegan ? occupation.dateJobBegan : ''
      }
    : { employer: '', jobTitle: '', dateJobBegan: '' };
