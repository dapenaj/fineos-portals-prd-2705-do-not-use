import { withFormik } from 'formik';
import { CustomerOccupation } from 'fineos-js-api-client';
import * as Yup from 'yup';
import moment from 'moment';

import { mapOccupation } from '../mappers';
import { API_DATE_FORMAT } from '../../../../../shared/utils';

import { WorkDetailsFormValue } from './form-value.type';
import { WorkDetailsFormComponent } from './work-details-form.component';

export type WorkDetailsProps = {
  occupation: CustomerOccupation | null;
  showEmployer: boolean;
  showCancel: boolean;
  submitLabel: string;
  formTitle?: string;
  isActive: boolean;
  submitStep: (values: WorkDetailsFormValue) => void;
  cancel: () => void;
};

// Validation
const WorkDetailSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<WorkDetailsFormValue>
>> = Yup.object().shape({
  employer: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  jobTitle: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  dateJobBegan: Yup.date()
    .max(
      moment().format(API_DATE_FORMAT),
      'INTAKE.YOUR_REQUEST.VALIDATION.HIRE_MAX_DATE'
    )
    .required('COMMON.VALIDATION.REQUIRED')
    .nullable()
});

// Map Api responses to form if we have them
const mapPropsToValues = ({
  occupation
}: WorkDetailsProps): WorkDetailsFormValue => mapOccupation(occupation);

// The Form
export const WorkDetailsForm = withFormik<
  WorkDetailsProps,
  WorkDetailsFormValue
>({
  mapPropsToValues,
  validationSchema: WorkDetailSchema,
  isInitialValid: props => props.occupation !== null,
  handleSubmit: (values, { props }) => props.submitStep(values),
  enableReinitialize: true
})(WorkDetailsFormComponent);
