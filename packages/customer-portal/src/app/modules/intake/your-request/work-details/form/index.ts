export * from './existing-work-details';
export * from './form-value.type';

export { WorkDetailsRenderer } from './work-details-renderer';
export { WorkDetailsForm } from './work-details';
export { WorkDetailsFormComponent } from './work-details-form.component';
