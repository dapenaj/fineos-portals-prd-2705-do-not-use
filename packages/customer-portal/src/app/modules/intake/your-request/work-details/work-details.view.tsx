import React from 'react';
import { CustomerOccupation } from 'fineos-js-api-client';

import { WorkDetailsRenderer, WorkDetailsFormValue } from './form';

type WorkDetailsProps = {
  isActive: boolean;
  occupation: CustomerOccupation | null;
  onProceed: () => void;
  onSubmitStep: (value: WorkDetailsFormValue) => void;
  onSubmitStepAndProceed: (value: WorkDetailsFormValue) => void;
};

export const WorkDetailsView: React.FC<WorkDetailsProps> = ({
  isActive,
  occupation,
  onSubmitStep,
  onSubmitStepAndProceed,
  onProceed
}) => (
  <WorkDetailsRenderer
    occupation={occupation}
    isActive={isActive}
    onSubmitStep={onSubmitStep}
    onSubmitStepAndProceed={onSubmitStepAndProceed}
    onProceed={onProceed}
  />
);
