import { CustomerOccupation } from 'fineos-js-api-client';

import { WorkDetailsFormValue } from '../form';

export const mapToCustomerOccupation = (
  occupation: CustomerOccupation | null,
  { employer, jobTitle, dateJobBegan }: WorkDetailsFormValue
): CustomerOccupation =>
  occupation
    ? {
        ...occupation,
        employer,
        jobTitle,
        dateJobBegan
      }
    : ({
        employer,
        jobTitle,
        dateJobBegan
      } as CustomerOccupation);
