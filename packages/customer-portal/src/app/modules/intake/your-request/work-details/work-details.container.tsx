import React from 'react';
import { connect } from 'react-redux';
import { isEmpty as _isEmpty } from 'lodash';
import { CustomerOccupation } from 'fineos-js-api-client';

import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import { IntakeSectionStep, AppConfig } from '../../../../shared/types';
import { withAppConfig } from '../../../../shared/contexts';
import { WorkDetailsValue } from '../your-request.type';

import { WorkDetailsView } from './work-details.view';
import { mapToCustomerOccupation } from './mappers';
import { WorkDetailsFormValue } from './form';
import { getCurrentCustomerOccupation } from './util';

const mapDispatchToProps = {
  submitStep: submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;

type WorkDetailsProps = {
  config: AppConfig;
  step: IntakeSectionStep<WorkDetailsValue>;
} & DispatchProps;

type WorkDetailsState = {
  occupation: CustomerOccupation | null;
};

export class WorkDetailsContainer extends React.Component<
  WorkDetailsProps,
  WorkDetailsState
> {
  state: WorkDetailsState = {
    occupation: null
  };

  componentDidMount() {
    const {
      config: {
        customerDetail: { customerOccupations }
      }
    } = this.props;

    if (!_isEmpty(customerOccupations)) {
      this.setState({
        occupation: getCurrentCustomerOccupation(customerOccupations)
      });
    }
  }

  handleOnSubmitStepAndProceed = (formValue: WorkDetailsFormValue) => {
    const { occupation } = this.state;

    this.setState(
      {
        occupation: mapToCustomerOccupation(occupation, formValue)
      },
      () => this.handleOnProceed()
    );
  };

  handleOnSubmitStep = (formValue: WorkDetailsFormValue) => {
    const { occupation } = this.state;

    this.setState({
      occupation: mapToCustomerOccupation(occupation, formValue)
    });
  };

  handleOnProceed = () => {
    const { submitStep, step } = this.props;
    const { occupation } = this.state;

    submitStep({
      ...step,
      value: {
        occupation
      } as WorkDetailsValue
    });
  };

  render() {
    const {
      step: { isActive },
      config: {
        customerDetail: { customerOccupations }
      }
    } = this.props;

    const { occupation } = this.state;

    return (
      <WorkDetailsView
        occupation={
          occupation || getCurrentCustomerOccupation(customerOccupations)
        }
        isActive={isActive}
        onProceed={this.handleOnProceed}
        onSubmitStep={this.handleOnSubmitStep}
        onSubmitStepAndProceed={this.handleOnSubmitStepAndProceed}
      />
    );
  }
}

export const WorkDetails = withAppConfig(
  connect(null, mapDispatchToProps)(WorkDetailsContainer)
);
