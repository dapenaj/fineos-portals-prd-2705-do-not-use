import React from 'react';
import { CustomerOccupation } from 'fineos-js-api-client';
import { FormattedMessage } from 'react-intl';
import { Col } from 'antd';

import { ContentLayout } from '../../../../../shared/layouts';
import { Row } from '../../../../../shared/ui';
import { StepSectionTitle } from '../../../ui/step-section-title/step-section-title.component';

import { WorkDetailsFormValue } from './form-value.type';
import { WorkDetailsForm } from './work-details';
import { ExistingWorkDetails } from './existing-work-details';

type WorkDetailsRendererProps = {
  occupation: CustomerOccupation | null;
  isActive: boolean;
  onSubmitStep: (value: WorkDetailsFormValue) => void;
  onSubmitStepAndProceed: (value: WorkDetailsFormValue) => void;
  onProceed: () => void;
};

type WorkDetailsRendererState = {
  showForm: boolean;
  showEmployer: boolean;
  showCancel: boolean;
  rendererTitle?: string;
  formTitle?: string;
  submitLabel: string;
};

export class WorkDetailsRenderer extends React.Component<
  WorkDetailsRendererProps,
  WorkDetailsRendererState
> {
  state: WorkDetailsRendererState = {
    showForm: this.props.occupation === null,
    showEmployer: true,
    showCancel: this.props.occupation !== null,
    rendererTitle:
      this.props.occupation === null
        ? undefined
        : 'INTAKE.YOUR_REQUEST.ARE_THESE_STILL_CORRECT.TITLE',
    formTitle: undefined,
    submitLabel: 'INTAKE.CONTROLS.SAVE_AND_PROCEED'
  };

  handleFormSubmit = (formValue: WorkDetailsFormValue) => {
    // if the user has no existing work details, then addingFirst should be true
    const addingFirst = this.props.occupation === null;

    this.setState({
      showForm: addingFirst,
      rendererTitle: addingFirst
        ? undefined
        : 'INTAKE.YOUR_REQUEST.ARE_THESE_STILL_CORRECT.TITLE'
    });

    addingFirst
      ? this.props.onSubmitStepAndProceed(formValue)
      : this.props.onSubmitStep(formValue);
  };

  render() {
    const { occupation, isActive } = this.props;
    const {
      showForm,
      showEmployer,
      showCancel,
      submitLabel,
      rendererTitle,
      formTitle
    } = this.state;

    return (
      <ContentLayout direction="column" noPadding={true}>
        <div data-test-el="work-details-renderer">
          {rendererTitle && (
            <Row>
              <Col>
                <StepSectionTitle>
                  <FormattedMessage id={rendererTitle} />
                </StepSectionTitle>
              </Col>
            </Row>
          )}

          {showForm && (
            <WorkDetailsForm
              occupation={occupation}
              submitStep={this.handleFormSubmit}
              showEmployer={showEmployer}
              submitLabel={submitLabel}
              showCancel={showCancel}
              formTitle={formTitle}
              isActive={isActive}
              cancel={() =>
                this.setState({
                  showForm: false,
                  rendererTitle:
                    'INTAKE.YOUR_REQUEST.ARE_THESE_STILL_CORRECT.TITLE'
                })
              }
            />
          )}

          {!showForm && occupation !== null && (
            <ExistingWorkDetails
              occupation={occupation}
              isActive={isActive}
              onChangeRequest={() =>
                this.setState({
                  showForm: true,
                  showEmployer: false,
                  showCancel: true,
                  rendererTitle:
                    'INTAKE.YOUR_REQUEST.SOMETHING_NOT_MATCHING.TITLE',
                  formTitle:
                    'INTAKE.YOUR_REQUEST.SOMETHING_NOT_MATCHING.SUBTITLE',
                  submitLabel: 'INTAKE.CONTROLS.OK'
                })
              }
              onNewJobRequest={() =>
                this.setState({
                  showForm: true,
                  showEmployer: true,
                  showCancel: true,
                  rendererTitle: 'INTAKE.YOUR_REQUEST.MOVING_ON.TITLE',
                  formTitle: 'INTAKE.YOUR_REQUEST.MOVING_ON.SUBTITLE',
                  submitLabel: 'INTAKE.CONTROLS.ADD_NEW_EMPLOYMENT'
                })
              }
              onProceed={() => {
                this.props.onProceed();
              }}
            />
          )}
        </div>
      </ContentLayout>
    );
  }
}
