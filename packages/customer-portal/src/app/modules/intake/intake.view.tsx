import React from 'react';

import { ContentLayout } from '../../shared/layouts';

export const IntakeView: React.FC = ({ children }) => (
  <div className="IntakeView" data-test-el="intake-view">
    <ContentLayout direction="column">{children}</ContentLayout>
  </div>
);
