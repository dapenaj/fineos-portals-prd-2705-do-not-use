import { CustomerOccupation } from 'fineos-js-api-client';

import { TimeOff, TimeOffPeriodType } from '../../shared/types';
import { getCustomerEmail, getCustomerPhoneNumber } from '../../shared/utils';

import { TimeOffFormValue } from './details';
import {
  WorkDetailsFormValue,
  PersonalDetailsValue,
  PersonalDetailsFormValue
} from './your-request';

export const buildTimeOffFromFormValue = ({
  outOfWork,
  leavePeriods
}: TimeOffFormValue): TimeOff => {
  const timeOffLeavePeriods = leavePeriods
    .filter(({ type }) => type === TimeOffPeriodType.TIME_OFF)
    .map(({ leavePeriod }) => leavePeriod);
  const reducedScheduleLeavePeriods = leavePeriods
    .filter(({ type }) => type === TimeOffPeriodType.REDUCED_SCHEDULE)
    .map(({ leavePeriod }) => leavePeriod);
  const episodicLeavePeriods = leavePeriods
    .filter(({ type }) => type === TimeOffPeriodType.EPISODIC)
    .map(({ leavePeriod }) => leavePeriod);

  return {
    outOfWork,
    timeOffLeavePeriods,
    reducedScheduleLeavePeriods,
    episodicLeavePeriods
  } as TimeOff;
};

export const buildWorkDetailsForm = (
  customerOccupation?: CustomerOccupation
): WorkDetailsFormValue | undefined => {
  if (customerOccupation) {
    return {
      employer: customerOccupation.employer,
      jobTitle: customerOccupation.jobTitle,
      dateJobBegan: customerOccupation.dateJobBegan
    };
  }
};

export const buildPersonalDetailsForm = (
  personalDetails?: PersonalDetailsValue
): PersonalDetailsFormValue | undefined => {
  if (personalDetails) {
    const {
      detail: {
        firstName,
        lastName,
        dateOfBirth,
        customerAddress: {
          address: {
            premiseNo,
            addressLine1,
            addressLine2,
            addressLine3,
            addressLine4,
            addressLine6,
            postCode,
            country
          }
        }
      },
      contact: { emailAddresses, phoneNumbers }
    } = personalDetails;

    const hasEmailAddresses = !!emailAddresses && !!emailAddresses.length;
    const email = hasEmailAddresses ? getCustomerEmail(emailAddresses) : '';

    const hasPhoneNumbers = !!phoneNumbers && !!phoneNumbers.length;
    const phoneNumber = hasPhoneNumbers
      ? getCustomerPhoneNumber(phoneNumbers)
      : null;

    return {
      firstName,
      lastName,
      dateOfBirth,
      email,
      phoneNumber,
      premiseNo,
      addressLine1,
      addressLine2,
      addressLine3,
      city: addressLine4,
      state: addressLine6,
      postCode,
      country
    };
  }
};
