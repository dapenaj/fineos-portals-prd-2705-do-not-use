import React from 'react';
import classnames from 'classnames';

import styles from './step-section-title.module.scss';

export const StepSectionTitle: React.FC<React.HTMLAttributes<
  HTMLHeadingElement
>> = ({ children, className, ...props }) => (
  <h3 {...props} className={classnames(styles.title, className)}>
    {children}
  </h3>
);
