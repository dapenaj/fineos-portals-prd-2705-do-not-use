import React from 'react';
import { Button, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './intake-list-item.module.scss';

type IncomeSourceProps = {
  isDisabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
};

export const IntakeListItem: React.FC<IncomeSourceProps> = React.memo(
  ({ children, isDisabled, onEdit, onDelete, ...props }) => (
    <div className={styles.listItem} {...props}>
      <div>{children}</div>

      <div>
        <Button
          type="link"
          disabled={isDisabled}
          onClick={onEdit}
          data-test-el="edit-intake-list-item"
        >
          <FormattedMessage id="INTAKE.CONTROLS.EDIT" />
        </Button>

        <Button
          className={styles.iconButton}
          disabled={isDisabled}
          onClick={onDelete}
          data-test-el="delete-intake-list-item"
        >
          <Icon type="delete" theme="filled" />
        </Button>
      </div>
    </div>
  )
);
