import React, { createRef } from 'react';
import classnames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './intake-step.module.scss';

type IntakeStepsProps = {
  isActive: boolean;
  title: React.ReactNode;
  selectStep: () => void;
};

export class IntakeStep extends React.Component<IntakeStepsProps> {
  private intakeScrollRef = createRef<HTMLDivElement>();

  componentDidUpdate(prevProps: IntakeStepsProps) {
    if (this.props.isActive && this.props.isActive !== prevProps.isActive) {
      this.intakeScrollRef.current!.scrollIntoView({
        behavior: 'smooth',
        block: 'center'
      });
    }
  }

  render() {
    const { isActive, title, children, selectStep } = this.props;

    return (
      <div className={classnames(styles.wrapper, isActive && styles.active)}>
        <div
          ref={this.intakeScrollRef}
          role="button"
          className={classnames(styles.header, isActive && styles.activeHeader)}
          onClick={selectStep}
        >
          {title}

          <div className={styles.asterisk}>
            <FormattedMessage id="INTAKE.ASTERISK" />
          </div>
        </div>
        <div className={styles.body}>{children}</div>
      </div>
    );
  }
}
