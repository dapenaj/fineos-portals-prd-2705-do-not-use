import React from 'react';
import { Button, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import { FormikProps } from 'formik';

import { Row } from '../../../../../shared/ui';

type IntakeStepControlsProps = {
  isDisabled?: boolean;
  submitLabel?: string;
  showCancel?: boolean;
  cancel?: () => void;
  'submit-data-test-el'?: string;
  'cancel-data-test-el'?: string;
  submitting?: boolean;
} & FormikProps<any>;

export const IntakeStepControls: React.FC<IntakeStepControlsProps> = ({
  isDisabled,
  submitLabel = 'INTAKE.CONTROLS.SAVE_AND_PROCEED',
  showCancel = false,
  cancel,
  submitting,
  ...props
}) => (
  <Row>
    <Col>
      <Button
        htmlType="submit"
        type="primary"
        disabled={isDisabled}
        data-test-el={props['submit-data-test-el'] || 'step-submit'}
        loading={submitting}
      >
        <FormattedMessage id={submitLabel} />
      </Button>

      {showCancel && (
        <Button
          type="link"
          onClick={cancel}
          data-test-el={props['cancel-data-test-el'] || 'step-cancel'}
        >
          <FormattedMessage id="INTAKE.CONTROLS.CANCEL" />
        </Button>
      )}
    </Col>
  </Row>
);
