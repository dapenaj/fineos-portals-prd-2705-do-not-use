import React from 'react';

import {
  IntakeSectionStep,
  IntakeSectionStepId
} from '../../../../shared/types';
import {
  InitialRequest,
  PersonalDetails,
  WorkDetails
} from '../../your-request';
import { RequestDetails, TimeOff } from '../../details';
import { AdditionalIncomeSource, SupportingEvidence } from '../../wrap-up';

import { IntakeStep } from './intake-step';

type IntakeStepsProps = {
  steps: IntakeSectionStep<any>[];
  selectStep: (step: IntakeSectionStep<any>) => void;
};

export const IntakeSteps: React.FC<IntakeStepsProps> = ({
  steps,
  selectStep
}) => (
  <div data-test-el="intake-steps">
    {steps.map(step => (
      <IntakeStep
        title={step.title}
        isActive={step.isActive}
        key={step.id}
        selectStep={() => selectStep(step)}
      >
        {(() => {
          switch (step.id) {
            case IntakeSectionStepId.PERSONAL_DETAILS:
              return <PersonalDetails step={step} />;
            case IntakeSectionStepId.WORK_DETAILS:
              return <WorkDetails step={step} />;
            case IntakeSectionStepId.INITIAL_REQUEST:
              return <InitialRequest step={step} />;
            case IntakeSectionStepId.REQUEST_DETAILS:
              return <RequestDetails step={step} />;
            case IntakeSectionStepId.TIME_OFF:
              return <TimeOff step={step} />;
            case IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES:
              return <AdditionalIncomeSource step={step} />;
            case IntakeSectionStepId.SUPPORTING_EVIDENCE:
              return <SupportingEvidence step={step} />;
            default:
              return <div />;
          }
        })()}
      </IntakeStep>
    ))}
  </div>
);
