import React from 'react';

import { IntakeSection } from '../../../../shared/types';
import { ContentLayout } from '../../../../shared/layouts';

import { IntakeOverviewItem } from './intake-overview-item';
import styles from './intake-overview.module.scss';

type IntakeOverviewProps = {
  sections: IntakeSection<any>[];
  onSectionClick: <T>(section: IntakeSection<T>) => void;
};

export const IntakeOverview: React.FC<IntakeOverviewProps> = ({
  sections,
  onSectionClick
}) => (
  <div className={styles.wrapper}>
    <ContentLayout>
      <div className={styles.overview}>
        {sections.map((section, index) => (
          <IntakeOverviewItem
            key={index}
            section={section}
            onClick={onSectionClick}
          />
        ))}
      </div>
    </ContentLayout>
  </div>
);
