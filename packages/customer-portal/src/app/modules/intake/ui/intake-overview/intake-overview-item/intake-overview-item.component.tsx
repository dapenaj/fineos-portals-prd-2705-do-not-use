import React from 'react';
import classnames from 'classnames';

import { IntakeSection } from '../../../../../shared/types';

import styles from './intake-overview-item.module.scss';

type IntakeOverviewItemProps = {
  section: IntakeSection<any>;
  onClick: <T>(section: IntakeSection<T>) => void;
};

export const IntakeOverviewItem: React.FC<IntakeOverviewItemProps> = ({
  section: { id, title, isActive, isComplete }
}) => (
  <div
    tabIndex={0}
    role="button"
    aria-disabled={!isActive && !isComplete}
    className={classnames(
      styles.section,
      !isActive && !isComplete && styles.disabled
    )}
    data-test-el="intake-overview-item"
  >
    <div
      data-test-el="intake-overview-number"
      className={classnames(styles.sectionIcon, isActive && styles.active)}
    >
      {id + 1}
    </div>
    <div
      data-test-el="intake-overview-title"
      className={classnames(styles.sectionTitle, isActive && styles.active)}
    >
      {title}
    </div>
  </div>
);
