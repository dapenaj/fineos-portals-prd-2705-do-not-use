import React from 'react';
import { Icon } from 'antd';

import styles from './intake-help.module.scss';

type IntakeHelpProps = {
  title: React.ReactNode;
  message: React.ReactNode;
  phoneNumber: string;
};

export const IntakeHelp: React.FC<IntakeHelpProps> = ({
  title,
  message,
  phoneNumber
}) => (
  <div className={styles.wrapper}>
    <h4 className={styles.heading}>{title}</h4>
    <p className={styles.text}>{message}</p>
    <h2 className={styles.phone}>
      <Icon type="phone" /> {phoneNumber}
    </h2>
  </div>
);
