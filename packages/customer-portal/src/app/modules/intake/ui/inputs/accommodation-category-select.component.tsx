import React from 'react';
import { FormattedMessage } from 'react-intl';

import { WORKPLACE_ACCOMMODATION_CATEGORIES } from '../../../../shared/constants';
import { SelectInput, DefaultInputProps } from '../../../../shared/ui';

type AccommodationCategorySelectProps = {
  label: React.ReactNode;
  isDisabled?: boolean;
  onChange: (value: string) => void;
  values: any;
} & DefaultInputProps;

export const AccommodationCategorySelect: React.FC<AccommodationCategorySelectProps> = ({
  label,
  isDisabled,
  onChange,
  values,
  ...props
}) => (
  <SelectInput
    {...props}
    isDisabled={isDisabled}
    label={label}
    options={WORKPLACE_ACCOMMODATION_CATEGORIES}
    onChange={onChange}
    values={values}
    placeholder={
      <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.LABELS.PLEASE_SELECT" />
    }
  />
);
