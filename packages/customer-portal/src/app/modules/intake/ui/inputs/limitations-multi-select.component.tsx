import React from 'react';

import { LIMITATIONS } from '../../../../shared/constants';
import { MultiSelectInput, DefaultInputProps } from '../../../../shared/ui';
import {} from '../../../../shared/ui/form/form.type';

type StateSelectProps = {
  label: React.ReactNode;
  isDisabled?: boolean;
} & DefaultInputProps;

export const LimitationsMultiSelect: React.FC<StateSelectProps> = ({
  label,
  isDisabled,
  ...props
}) => (
  <MultiSelectInput
    {...props}
    isDisabled={isDisabled}
    label={label}
    optionGroups={LIMITATIONS}
  />
);
