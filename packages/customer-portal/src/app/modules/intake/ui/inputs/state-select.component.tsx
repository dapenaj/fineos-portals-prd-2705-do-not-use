import React from 'react';

import { STATES } from '../../../../shared/constants';
import { SelectInput } from '../../../../shared/ui/form/select-input';
import { DefaultInputProps } from '../../../../shared/ui/form/form.type';

type StateSelectProps = {
  label: React.ReactNode;
  isDisabled?: boolean;
} & DefaultInputProps;

export const StateSelect: React.FC<StateSelectProps> = ({
  label,
  isDisabled,
  ...props
}) => (
  <SelectInput
    {...props}
    isDisabled={isDisabled}
    label={label}
    options={STATES}
  />
);
