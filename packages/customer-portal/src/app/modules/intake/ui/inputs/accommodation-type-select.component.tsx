import React from 'react';
import { FormattedMessage } from 'react-intl';

import { SelectInput, DefaultInputProps } from '../../../../shared/ui';
import { SelectOption } from '../../../../shared/types';

type AccommodationTypeSelectProps = {
  label: React.ReactNode;
  isDisabled?: boolean;
  options: SelectOption[];
  values: any;
} & DefaultInputProps;

export const AccommodationTypeSelect: React.FC<AccommodationTypeSelectProps> = ({
  label,
  isDisabled,
  options,
  values,
  ...props
}) => (
  <SelectInput
    {...props}
    isDisabled={isDisabled}
    label={label}
    options={options}
    values={values}
    placeholder={
      <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.LABELS.PLEASE_SELECT" />
    }
  />
);
