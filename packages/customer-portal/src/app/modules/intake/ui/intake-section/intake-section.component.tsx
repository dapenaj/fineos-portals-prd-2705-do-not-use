import React from 'react';
import { Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { Row } from '../../../../shared/ui';
import { IntakeHelp } from '../intake-help';

import styles from './intake-section.module.scss';

export const IntakeSection: React.FC = ({ children }) => (
  <div className={styles.wrapper}>
    <Row>
      <Col xs={24} lg={16}>
        {children}
      </Col>
      <Col xs={24} lg={8}>
        <div className={styles.help}>
          <IntakeHelp
            title={<FormattedMessage id="INTAKE.HELP.TITLE" />}
            message={<FormattedMessage id="INTAKE.HELP.MESSAGE" />}
            phoneNumber="1800 123 4567"
          />
        </div>
      </Col>
    </Row>
  </div>
);
