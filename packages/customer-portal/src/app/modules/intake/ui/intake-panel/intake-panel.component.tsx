import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from 'antd';

import { Panel } from '../../../../shared/ui';

import styles from './intake-panel.module.scss';

type IntakePanelProps = {
  title: React.ReactNode;
  disabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
};

export const IntakePanel: React.FC<IntakePanelProps> = ({
  title,
  children,
  disabled,
  onEdit,
  onDelete
}) => (
  <Panel className={styles.panel}>
    <div className={styles.wrapper} data-test-el="intake-panel-panel">
      <div className={styles.header}>
        <div className={styles.title}>{title}</div>
        <div className={styles.actions}>
          <Button
            type="link"
            disabled={disabled}
            onClick={onEdit}
            data-test-el="intake-panel-edit-btn"
          >
            <FormattedMessage id="INTAKE.DETAILS.LABELS.EDIT" />
          </Button>
          <Button
            className={styles.deleteBtn}
            type="link"
            icon="delete"
            disabled={disabled}
            onClick={onDelete}
            data-test-el="intake-panel-delete-btn"
          />
        </div>
      </div>

      <div className={styles.content}>{children}</div>
    </div>
  </Panel>
);
