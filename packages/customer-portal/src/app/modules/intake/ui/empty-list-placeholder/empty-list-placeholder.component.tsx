import React from 'react';
import { Col } from 'antd';

import { Row } from '../../../../shared/ui/row';

import styles from './empty-list-placeholder.module.scss';

interface EmptyListPlaceholderProps {
  icon: React.ReactNode;
  content: React.ReactNode;
  button: React.ReactNode;
}

// this is template component, for capturing common layout for empty list placeholder
export const EmptyListPlaceholder: React.FC<EmptyListPlaceholderProps> = ({
  icon,
  content,
  button,
  ...props
}) => (
  <div className={styles.wrapper} {...props}>
    <Row>
      <Col span={4} data-test-el="empty-list-placeholder-icon">
        <div className={styles.iconContainer}>{icon}</div>
      </Col>
      <Col span={20}>
        <div
          className={styles.description}
          data-test-el="empty-list-placeholder-content"
        >
          {content}
        </div>

        <div data-test-el="empty-list-placeholder-button">{button}</div>
      </Col>
    </Row>
  </div>
);
