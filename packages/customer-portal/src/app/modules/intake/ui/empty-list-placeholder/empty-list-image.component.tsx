import React from 'react';

import styles from './empty-list-image.module.scss';

type EmptyListImageProps = {
  imageSrc: string;
};

export const EmptyListImage: React.FC<EmptyListImageProps> = ({ imageSrc }) => (
  <img src={imageSrc} className={styles.image} alt="" />
);
