import React from 'react';
import { Button, Card, Row, Col } from 'antd';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import {
  AppConfig,
  ConfirmationCase,
  ConfirmationType
} from '../../../../shared/types';
import { ContentLayout } from '../../../../shared/layouts';
import { withAppConfig } from '../../../../shared/contexts';
import { Spinner } from '../../../../shared/ui';

import styles from './intake-confirmation.module.scss';
import { ConfirmationResult } from './results';

type IntakeConfirmationTypeProps = {
  type: ConfirmationType | null;
  createdNotificationId: string | null;
  prevPath: string;
  config: AppConfig;
};

export const IntakeConfirmationTypeComponent: React.FC<IntakeConfirmationTypeProps> = ({
  type,
  createdNotificationId,
  prevPath,
  config: {
    clientConfig: {
      intake: { wrapUp }
    }
  }
}) => {
  const confirmation =
    type && wrapUp.find((c: ConfirmationCase) => c.type === type);

  return confirmation ? (
    <div className={styles.wrapper} data-test-el="intake-confirmation-view">
      <ContentLayout className={styles.content}>
        <Row type="flex" justify="center" className={styles.row}>
          <Col span={22}>
            <Card className={styles.card}>
              <h3 className={styles.header}>{confirmation.header}</h3>

              <div data-test-el="intake-confirmation">
                <ConfirmationResult
                  notificationId={createdNotificationId}
                  confirmation={confirmation}
                />
              </div>

              <NavLink to={prevPath}>
                <Button className={styles.closeButton} type="primary">
                  <FormattedMessage id="INTAKE.CONTROLS.CLOSE" />
                </Button>
              </NavLink>
            </Card>
          </Col>
        </Row>
      </ContentLayout>
    </div>
  ) : (
    <Spinner />
  );
};

export const IntakeConfirmationType = withAppConfig(
  IntakeConfirmationTypeComponent
);
