import {
  IntakeSubmissionResult,
  getCompletedNotificationId
} from '../../../../shared/services';
import { ConfirmationType } from '../../../../shared/types';

export const getConfirmationType = (
  result: IntakeSubmissionResult
): ConfirmationType => {
  if (getCompletedNotificationId(result)) {
    return ConfirmationType.Successful;
  } else if (result.createdNotificationId) {
    return ConfirmationType.PartialSuccess;
  }

  return ConfirmationType.Failure;
};
