import React from 'react';
import { Icon } from 'antd';

import { ConfirmationCase } from '../../../../../../shared/types';

import styles from './confirmation-wrong.module.scss';

type ConfirmationWrongProps = {
  content: ConfirmationCase;
};

export const ConfirmationWrong: React.FC<ConfirmationWrongProps> = ({
  content,
  children
}) => (
  <>
    <div className={styles.wrong}>
      <div>{content.numberMessage}</div>
      <div className={styles.phone}>
        <Icon
          type="phone"
          className={styles.phonePosition}
          theme="filled"
          rotate={90}
        />
        {content.phoneNumber}
      </div>
      <div>{content.availableDays}</div>
    </div>
    {children}
  </>
);
