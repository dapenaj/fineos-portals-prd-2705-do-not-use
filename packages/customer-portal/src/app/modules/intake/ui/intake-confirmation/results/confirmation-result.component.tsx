import React from 'react';

import {
  ConfirmationCase,
  ConfirmationType
} from '../../../../../shared/types';

import { ConfirmationSuccess } from './confirmation-success';
import { ConfirmationPartialSuccess } from './confirmation-partial-success';
import { ConfirmationWrong } from './confirmation-wrong';

type ConfirmationResultProps = {
  notificationId: string | null;
  confirmation: ConfirmationCase;
};

export const ConfirmationResult: React.FC<ConfirmationResultProps> = ({
  notificationId,
  confirmation
}) => {
  if (notificationId) {
    switch (confirmation.type) {
      case ConfirmationType.Successful:
        return (
          <ConfirmationSuccess
            content={confirmation}
            notificationId={notificationId}
          />
        );
      case ConfirmationType.PartialSuccess:
        return (
          <ConfirmationPartialSuccess
            content={confirmation}
            notificationId={notificationId}
          />
        );
    }
  }

  return <ConfirmationWrong content={confirmation} />;
};
