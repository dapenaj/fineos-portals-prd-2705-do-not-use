import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { StaticContext } from 'react-router';

import { ConfirmationType } from '../../../../shared/types';

import { IntakeConfirmationType } from './intake-confirmation-type';

type IntakeConfirmationViewProps = {
  confirmationType: ConfirmationType | null;
  createdNotificationId: string | null;
} & RouteComponentProps<{}, StaticContext, { prevPath: string }>;

export const IntakeConfirmationView: React.FC<IntakeConfirmationViewProps> = ({
  history,
  confirmationType,
  createdNotificationId
}) => {
  const prevPath = history.location.state
    ? history.location.state.prevPath
    : '/';

  return (
    <IntakeConfirmationType
      type={confirmationType}
      prevPath={prevPath}
      createdNotificationId={createdNotificationId}
    />
  );
};
