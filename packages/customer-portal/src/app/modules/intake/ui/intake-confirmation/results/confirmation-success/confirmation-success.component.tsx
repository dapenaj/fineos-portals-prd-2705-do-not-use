import React from 'react';
import { Col, Row } from 'antd';

import { ConfirmationCase } from '../../../../../../shared/types';
import greenCheckIntake from '../../../../../../../assets/img/green_check_intake.svg';
import { NotificationReference } from '../notification-reference';

import styles from './confirmation-success.module.scss';

type ConfirmationSuccessProps = {
  content: ConfirmationCase;
  notificationId: string;
};

export const ConfirmationSuccess: React.FC<ConfirmationSuccessProps> = ({
  content: {
    successNotification,
    notificationReferenceMessageHeader,
    notificationReferenceMessageFooter,
    questionMessage,
    phoneNumber,
    privacyNotice
  },
  notificationId
}) => (
  <>
    <img alt="success-icon" className={styles.image} src={greenCheckIntake} />
    <div className={styles.section}>
      <Row gutter={16}>
        <Col>
          <p className={styles.header}>{successNotification}</p>
        </Col>
      </Row>
    </div>
    <div className={styles.section}>
      <Row gutter={16}>
        <Col span={16}>
          <NotificationReference
            notificationId={notificationId}
            headerText={notificationReferenceMessageHeader}
            footerText={notificationReferenceMessageFooter}
          />
        </Col>
        <Col span={8}>
          <p>{questionMessage}</p>
          <p className={styles.phoneNumber}>{phoneNumber}</p>
        </Col>
      </Row>
    </div>
    <Row gutter={16}>
      <Col>
        <p>{privacyNotice}</p>
      </Col>
    </Row>
  </>
);
