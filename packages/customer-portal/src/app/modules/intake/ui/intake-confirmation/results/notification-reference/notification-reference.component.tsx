import React from 'react';

import styles from './notification-reference.module.scss';

type NotificationReferenceProps = {
  notificationId?: string;
  headerText?: string;
  footerText?: string;
};

export const NotificationReference: React.FC<NotificationReferenceProps> = ({
  notificationId,
  headerText,
  footerText
}) => (
  <div
    data-test-el="notification-reference"
    className={styles.notificationReference}
  >
    {headerText}
    <div className={styles.referenceId}>{notificationId}</div>
    {footerText}
  </div>
);
