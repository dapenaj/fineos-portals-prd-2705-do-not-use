import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { RootState } from '../../../../store';

import { IntakeConfirmationView } from './intake-confirmation.view';
import { getConfirmationType } from './intake-confirmation.util';

const mapStateToProps = ({
  intake: {
    workflow: { previousSubmissionState }
  }
}: RootState) => {
  const submittedResult =
    previousSubmissionState && previousSubmissionState.result;

  return {
    confirmationType: submittedResult && getConfirmationType(submittedResult),
    createdNotificationId:
      (submittedResult && submittedResult.createdNotificationId) || null
  };
};

export const IntakeConfirmationContainer = withRouter(
  connect(mapStateToProps)(IntakeConfirmationView)
);
