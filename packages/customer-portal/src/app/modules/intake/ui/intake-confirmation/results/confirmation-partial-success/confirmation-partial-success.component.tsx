import React from 'react';
import { Col, Row } from 'antd';

import { ConfirmationCase } from '../../../../../../shared/types/confirmation.type';
import { ConfirmationWrong } from '../confirmation-wrong';
import { NotificationReference } from '../notification-reference';

import styles from './confirmation-partial-success.module.scss';

type ConfirmationPartialSuccessProps = {
  content: ConfirmationCase;
  notificationId: string;
};

export const ConfirmationPartialSuccess: React.FC<ConfirmationPartialSuccessProps> = ({
  content,
  notificationId
}) => (
  <ConfirmationWrong content={content}>
    <div className={styles.wrapper}>
      <div className={styles.tip}>{content.tip}</div>
      <Row>
        <Col span={12}>
          <NotificationReference
            notificationId={notificationId}
            headerText={content.notificationReferenceMessageHeader}
            footerText={content.notificationReferenceMessageFooter}
          />
        </Col>
      </Row>
    </div>
  </ConfirmationWrong>
);
