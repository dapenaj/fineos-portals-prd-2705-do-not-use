import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Prompt } from 'react-router';
import { WrappedComponentProps as IntlProps, injectIntl } from 'react-intl';

import { RootState } from '../../store';
import {
  selectIntakeWorkflowSection,
  cancelIntakeWorkflow,
  selectIntakeWorkflowStep,
  resetIntake
} from '../../store/intake/workflow/actions';

import {
  IntakeOverview,
  IntakeSection,
  IntakeSteps,
  IntakeConfirmation
} from './ui';
import { IntakeView } from './intake.view';

const mapStateToProps = ({
  intake: {
    workflow: { sections, currentSection, hasValues }
  }
}: RootState) => ({ sections, currentSection, hasValues });

const mapDispatchToProps = {
  handleSectionClick: selectIntakeWorkflowSection,
  handleCancel: cancelIntakeWorkflow,
  handleStepSelect: selectIntakeWorkflowStep,
  resetIntake
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
type IntakeProps = StateProps & DispatchProps & RouteComponentProps & IntlProps;

export class IntakeContainer extends React.Component<IntakeProps> {
  componentDidMount() {
    this.props.resetIntake();
  }

  render() {
    const {
      sections,
      currentSection,
      hasValues,
      handleSectionClick,
      handleStepSelect,
      intl
    } = this.props;

    const allSectionsComplete = sections.every(section => section.isComplete);

    return !allSectionsComplete ? (
      <IntakeView>
        <Prompt
          when={hasValues}
          message={intl.formatMessage({
            id: 'INTAKE.CONTROLS.CANCEL_INFO'
          })}
        />
        <IntakeOverview
          sections={sections}
          onSectionClick={handleSectionClick}
        />
        <IntakeSection>
          <IntakeSteps
            steps={currentSection.steps.filter(step => !step.skip)}
            selectStep={handleStepSelect}
          />
        </IntakeSection>
      </IntakeView>
    ) : (
      <IntakeConfirmation />
    );
  }
}

export const Intake = connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(IntakeContainer));
