import React from 'react';
import { FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';
import { WorkPlaceAccommodation } from 'fineos-js-api-client';
import { Row, Col, Form } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';

import {
  StepSectionTitle,
  LimitationsMultiSelect,
  IntakeStepControls
} from '../../../ui';
import {
  FormikRadioGroupInput,
  FormLabel,
  TextAreaInput
} from '../../../../../shared/ui';
import { ContentLayout } from '../../../../../shared/layouts';

import { AccommodationFormValue } from './form-value.type';
import { WorkplaceAccommodationPlaceholder } from './workplace-accommodation-placeholder';
import { WorkplaceAccommodationForm } from './workplace-accomodation-form';
import { WorkplaceAccommodationPanel } from './workplace-accommodation-panel';
import { mapToWorkPlaceAccommodation } from './mappers';
import styles from './accomodation-form.module.scss';

type AccommodationFormProps = {
  isActive: boolean;
  onProceed: (supportingEvidence: AccommodationFormValue) => void;
  submitting: boolean;
} & FormikProps<AccommodationFormValue>;

type AccommodationFormViewState = {
  showWorkplaceAccommodationForm: boolean;
  workplaceAccommodations: WorkPlaceAccommodation[];
  currentAccommodationIndex: number;
  isEdit: boolean;
};

export class AccommodationFormView extends React.Component<
  AccommodationFormProps,
  AccommodationFormViewState
> {
  state: AccommodationFormViewState = {
    showWorkplaceAccommodationForm: false,
    workplaceAccommodations: [],
    currentAccommodationIndex: 0,
    isEdit: false
  };

  handleCancelAccommodation = () => {
    const { isEdit, workplaceAccommodations } = this.state;

    if (!isEdit) {
      const accommodations = [...workplaceAccommodations];
      accommodations.pop();
      this.setState({ workplaceAccommodations: accommodations });
    }

    this.setState({
      showWorkplaceAccommodationForm: false
    });
  };

  handleAddAccommodation = () => {
    const { workplaceAccommodations } = this.state;

    this.setState({
      showWorkplaceAccommodationForm: true,
      workplaceAccommodations: [
        ...workplaceAccommodations,
        mapToWorkPlaceAccommodation({ accommodationDescription: '' })
      ],
      currentAccommodationIndex: workplaceAccommodations.length,
      isEdit: false
    });
  };

  handleSaveAccommodation = (accommodation: WorkPlaceAccommodation) => {
    const { workplaceAccommodations, currentAccommodationIndex } = this.state;
    const updatedAccommodations = [...workplaceAccommodations];

    updatedAccommodations[currentAccommodationIndex] = accommodation;

    this.setState({
      workplaceAccommodations: updatedAccommodations,
      showWorkplaceAccommodationForm: false
    });
  };

  handleEditAccommodation = (index: number) => {
    this.setState({
      showWorkplaceAccommodationForm: true,
      currentAccommodationIndex: index,
      isEdit: true
    });
  };

  handleDeleteAccommodation = (index: number) => {
    const { workplaceAccommodations } = this.state;
    const updatedAccommodations = [...workplaceAccommodations];

    updatedAccommodations.splice(index, 1);

    this.setState({
      workplaceAccommodations: updatedAccommodations
    });
  };

  handleOnSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const { workplaceAccommodations } = this.state;
    const { handleSubmit } = this.props;

    this.props.values.workPlaceAccommodations = workplaceAccommodations;

    handleSubmit(event);
  };

  render() {
    const {
      isActive,
      errors,
      values: { limitations },
      submitting
    } = this.props;
    const {
      showWorkplaceAccommodationForm,
      workplaceAccommodations,
      currentAccommodationIndex,
      isEdit
    } = this.state;

    const currentAccommodation =
      workplaceAccommodations[currentAccommodationIndex];
    const hasAccommodations =
      !!workplaceAccommodations && !!workplaceAccommodations.length;
    const hasLimitations = !!limitations && !!limitations.length;

    const hasLimitationsOrAccommodations = hasAccommodations || hasLimitations;

    const allowSubmit =
      _isEmpty(errors) && isActive && hasLimitationsOrAccommodations;

    return (
      <>
        {!showWorkplaceAccommodationForm && (
          <Form name="accommondation-form" onSubmit={this.handleOnSubmit}>
            <ContentLayout direction="column" noPadding={true}>
              <StepSectionTitle data-test-el="accommodation-form-title">
                <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.FORM_TITLE" />
              </StepSectionTitle>

              <Row>
                <Col xs={24}>
                  <FormikRadioGroupInput
                    label={
                      <FormLabel
                        required={true}
                        label="INTAKE.DETAILS.ACCOMMODATION.LABELS.PREGNANCY_RELATED"
                      />
                    }
                    isDisabled={!isActive}
                    name="pregnancyRelated"
                    optionPrefix="INTAKE.DETAILS.REQUEST_DETAILS."
                    radioOptions={['YES', 'NO']}
                    {...this.props}
                  />
                </Col>
              </Row>

              <Row>
                <Col xs={24} lg={20}>
                  <LimitationsMultiSelect
                    name="limitations"
                    isDisabled={!isActive}
                    label={
                      <FormLabel label="INTAKE.DETAILS.ACCOMMODATION.LABELS.LIMITATIONS" />
                    }
                    {...this.props}
                  />
                </Col>
              </Row>

              <StepSectionTitle
                className={styles.title}
                data-test-el="accommodation-description-title"
              >
                <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.POSSIBLE_ACCOMMODATIONS_TITLE" />
              </StepSectionTitle>

              {hasAccommodations && (
                <>
                  {workplaceAccommodations.map((accommodation, index) => (
                    <WorkplaceAccommodationPanel
                      key={index}
                      accommodation={accommodation}
                      disabled={!isActive}
                      onEdit={() => this.handleEditAccommodation(index)}
                      onDelete={() => this.handleDeleteAccommodation(index)}
                    />
                  ))}
                </>
              )}

              <WorkplaceAccommodationPlaceholder
                disabled={!isActive}
                showFull={
                  !workplaceAccommodations ||
                  workplaceAccommodations.length === 0
                }
                onClick={this.handleAddAccommodation}
              />

              <StepSectionTitle
                className={styles.title}
                data-test-el="accommodation-description-title"
              >
                <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.DESCRIPTION_TITLE" />
              </StepSectionTitle>

              <Row>
                <Col xs={24} lg={20}>
                  <TextAreaInput
                    name="additionalNotes"
                    isDisabled={!isActive}
                    label={
                      <FormLabel label="INTAKE.DETAILS.ACCOMMODATION.LABELS.DESCRIPTION" />
                    }
                    {...this.props}
                  />
                </Col>
              </Row>

              <div className={styles.controls}>
                <IntakeStepControls
                  {...this.props}
                  isDisabled={!allowSubmit}
                  submit-data-test-el="submit-accommodation"
                  submitting={submitting}
                />
              </div>
            </ContentLayout>
          </Form>
        )}

        {showWorkplaceAccommodationForm && (
          <WorkplaceAccommodationForm
            accommodation={currentAccommodation}
            isEdit={isEdit}
            onSubmit={this.handleSaveAccommodation}
            onCancel={this.handleCancelAccommodation}
          />
        )}
      </>
    );
  }
}
