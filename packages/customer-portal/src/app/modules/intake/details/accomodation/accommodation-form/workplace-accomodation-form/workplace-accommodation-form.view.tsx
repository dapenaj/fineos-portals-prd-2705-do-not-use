import React from 'react';
import { FormikProps } from 'formik';
import { WorkPlaceAccommodation } from 'fineos-js-api-client';
import { Form, Row, Col } from 'antd';

import { WORKPLACE_ACCOMMODATION_CATEGORIES } from '../../../../../../shared/constants';
import {
  AccommodationCategorySelect,
  AccommodationTypeSelect,
  IntakeStepControls
} from '../../../../ui';
import { FormLabel, TextInput } from '../../../../../../shared/ui';
import {
  SelectOption,
  AccommodationCategory,
  AccommodationType
} from '../../../../../../shared/types';
import { getAccommodationTypes } from '../../../../../../shared/utils';

import { WorkplaceAccommodationFormProps } from './workplace-accommodation-form';

type WorkplaceAccommodationFormViewProps = WorkplaceAccommodationFormProps &
  FormikProps<WorkPlaceAccommodation>;

type WorkplaceAccommodationFormViewState = {
  types: SelectOption[];
  isOther: boolean;
};

export class WorkplaceAccommodationFormView extends React.Component<
  WorkplaceAccommodationFormViewProps,
  WorkplaceAccommodationFormViewState
> {
  state: WorkplaceAccommodationFormViewState = {
    types: getAccommodationTypes(
      WORKPLACE_ACCOMMODATION_CATEGORIES[0].value as string
    ),
    isOther: false
  };

  handleCategoryChange = (value: string) => {
    const types = getAccommodationTypes(value);

    if (value === AccommodationCategory.OTHER) {
      this.setState({ types, isOther: true });
      this.props.setFieldValue('accommodationType', AccommodationType.OTHER);
    } else {
      // Reset the accommodation type
      this.props.setFieldValue('accommodationType', undefined);
      // Reset the accommodation description
      this.props.setFieldValue('accommodationDescription', '');
      this.setState({ types, isOther: false });
    }

    this.setState({
      types
    });
  };

  render() {
    const {
      onCancel,
      handleSubmit,
      isValid,
      values: { accommodationCategory }
    } = this.props;
    const { types } = this.state;

    const isOther =
      this.state.isOther ||
      accommodationCategory === AccommodationCategory.OTHER;

    return (
      <Form name="workplace-accommodation-form" onSubmit={handleSubmit}>
        <Row>
          <Col>
            <AccommodationCategorySelect
              label={
                <FormLabel
                  required={true}
                  label="INTAKE.DETAILS.ACCOMMODATION.LABELS.ACCOMMODATION_CATEGORY"
                />
              }
              name="accommodationCategory"
              onChange={this.handleCategoryChange}
              {...this.props}
            />
          </Col>
        </Row>

        <Row>
          <Col>
            {!isOther ? (
              <AccommodationTypeSelect
                label={
                  <FormLabel
                    required={true}
                    label="INTAKE.DETAILS.ACCOMMODATION.LABELS.ACCOMMODATION_TYPE"
                  />
                }
                name="accommodationType"
                options={types}
                {...this.props}
              />
            ) : (
              <TextInput
                label={
                  <FormLabel
                    required={true}
                    label="INTAKE.DETAILS.ACCOMMODATION.LABELS.ACCOMMODATION_DESCRIPTION"
                  />
                }
                name="accommodationDescription"
                {...this.props}
              />
            )}
          </Col>
        </Row>

        <IntakeStepControls
          {...this.props}
          submitLabel="INTAKE.CONTROLS.OK"
          isDisabled={!isValid}
          showCancel={true}
          cancel={onCancel}
          cancel-data-test-el="cancel-workplace-accommodation"
        />
      </Form>
    );
  }
}
