import React from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import { Panel } from '../../../../../../shared/ui';
import wheelChairSvg from '../../../../../../../assets/img/wheelchair.svg';

import styles from './workplace-accommodation-placeholder.module.scss';

type WorkplaceAccommodationPlaceholderProps = {
  showFull: boolean;
  disabled: boolean;
  onClick: () => void;
};

export const WorkplaceAccommodationPlaceholder: React.FC<WorkplaceAccommodationPlaceholderProps> = ({
  showFull,
  onClick,
  disabled
}) =>
  showFull ? (
    <Panel className={styles.wrapper}>
      <div className={styles.icon}>
        <img src={wheelChairSvg} className={styles.image} alt="" />
      </div>
      <div className={styles.content}>
        <div className={styles.message}>
          <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.ACCOMMODATION_DESCRIPTION" />
        </div>
        <Button
          type="link"
          disabled={disabled}
          onClick={onClick}
          data-test-el="add-accommodation-btn"
        >
          <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.ACTIONS.ADD" />
        </Button>
      </div>
    </Panel>
  ) : (
    <div>
      <Button
        type="link"
        disabled={disabled}
        onClick={onClick}
        data-test-el="add-another-accommodation-btn"
      >
        <FormattedMessage id="INTAKE.DETAILS.ACCOMMODATION.ACTIONS.ADD_ANOTHER" />
      </Button>
    </div>
  );
