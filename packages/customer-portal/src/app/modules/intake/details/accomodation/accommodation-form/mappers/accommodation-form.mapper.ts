import { WorkPlaceAccommodation } from 'fineos-js-api-client';

import { WorkPlaceAccommodationFormValue } from '../work-place-form-value.type';

export const mapToWorkPlaceAccommodation = ({
  accommodationCategory,
  accommodationType,
  accommodationDescription
}: WorkPlaceAccommodationFormValue): WorkPlaceAccommodation =>
  ({
    accommodationCategory,
    accommodationType,
    accommodationDescription
  } as WorkPlaceAccommodation);
