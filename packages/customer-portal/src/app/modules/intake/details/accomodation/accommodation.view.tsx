import React from 'react';

import {
  AccommodationFormValue,
  AccommodationForm
} from './accommodation-form';

type AccommodationViewProps = {
  isActive: boolean;
  stepSubmit: (values: AccommodationFormValue) => void;
  submitting: boolean;
};

export const AccommodationView: React.FC<AccommodationViewProps> = ({
  isActive,
  stepSubmit,
  submitting
}) => (
  <AccommodationForm
    isActive={isActive}
    onProceed={stepSubmit}
    submitting={submitting}
  />
);
