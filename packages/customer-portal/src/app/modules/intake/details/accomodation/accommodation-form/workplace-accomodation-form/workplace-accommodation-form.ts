import { withFormik, FormikBag } from 'formik';
import { WorkPlaceAccommodation } from 'fineos-js-api-client';
import * as Yup from 'yup';

import {
  AccommodationType,
  AccommodationCategory
} from '../../../../../../shared/types';

import { WorkplaceAccommodationFormView } from './workplace-accommodation-form.view';

export type WorkplaceAccommodationFormProps = {
  accommodation: WorkPlaceAccommodation;
  isEdit: boolean;
  onSubmit: (value: WorkPlaceAccommodation) => void;
  onCancel: () => void;
};

const handleSubmit = (
  value: WorkPlaceAccommodation,
  { props }: FormikBag<WorkplaceAccommodationFormProps, WorkPlaceAccommodation>
) => props.onSubmit(value);

const WorkplaceAccommodationSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<WorkPlaceAccommodation>
>> = Yup.object().shape({
  accommodationCategory: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  accommodationType: Yup.string().when(
    'accommodationCategory',
    (accommodationCategory: string, schema: any) =>
      accommodationCategory !== AccommodationCategory.OTHER
        ? Yup.string()
            .nullable()
            .required('COMMON.VALIDATION.REQUIRED')
        : schema
  ),
  accommodationDescription: Yup.string().when(
    'accommodationType',
    (accommodationType: string, schema: any) =>
      accommodationType === AccommodationType.OTHER
        ? Yup.string()
            .nullable()
            .required('COMMON.VALIDATION.REQUIRED')
        : schema
  )
});

export const WorkplaceAccommodationForm = withFormik<
  WorkplaceAccommodationFormProps,
  WorkPlaceAccommodation
>({
  mapPropsToValues: ({
    accommodation: {
      accommodationCategory,
      accommodationType,
      accommodationDescription
    }
  }) => ({
    accommodationCategory,
    accommodationType,
    accommodationDescription
  }),
  isInitialValid: ({
    isEdit,
    accommodation: { accommodationCategory, accommodationType }
  }) => isEdit || (!!accommodationCategory && !!accommodationType),
  validationSchema: WorkplaceAccommodationSchema,
  handleSubmit
})(WorkplaceAccommodationFormView);
