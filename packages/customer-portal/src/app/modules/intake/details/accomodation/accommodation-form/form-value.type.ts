import { WorkPlaceAccommodation } from 'fineos-js-api-client';

export type AccommodationFormValue = {
  pregnancyRelated: string;
  workPlaceAccommodations: WorkPlaceAccommodation[];
  limitations: string[];
  additionalNotes: string;
};
