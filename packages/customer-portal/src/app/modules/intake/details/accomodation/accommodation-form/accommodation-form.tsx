import { withFormik } from 'formik';
import * as Yup from 'yup';

import { AccommodationFormView } from './accommodation-form.view';
import { AccommodationFormValue } from './form-value.type';

export type AccommodationFormProps = {
  isActive: boolean;
  onProceed: (values: AccommodationFormValue) => void;
  submitting: boolean;
};

const AccommodationSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<AccommodationFormValue>
>> = Yup.object().shape({
  pregnancyRelated: Yup.string().required('COMMON.VALIDATION.REQUIRED')
});

export const AccommodationForm = withFormik<
  AccommodationFormProps,
  AccommodationFormValue
>({
  mapPropsToValues: props =>
    ({
      pregnancyRelated: '',
      limitations: [],
      workPlaceAccommodations: [],
      additionalNotes: ''
    } as AccommodationFormValue),
  validationSchema: AccommodationSchema,
  handleSubmit: (values, { props }) => props.onProceed(values)
})(AccommodationFormView);
