export type WorkPlaceAccommodationFormValue = {
  accommodationCategory?: string;
  accommodationType?: string;
  accommodationDescription: string;
};
