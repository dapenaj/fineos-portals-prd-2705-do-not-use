import React from 'react';
import { connect } from 'react-redux';
import { CustomerOccupation } from 'fineos-js-api-client';

import {
  IntakeSectionStep,
  IntakeSectionId,
  EventOptionId
} from '../../../../shared/types';
import { RootState } from '../../../../store';
import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import { AccommodationValue } from '../details.type';
import { YourRequestValue, PersonalDetailsValue } from '../../your-request';
import {
  IntakeAccommodationSubmissionService,
  IntakeSubmissionState,
  IntakeAccommodationSubmission
} from '../../../../shared/services';
import {
  buildWorkDetailsForm,
  buildPersonalDetailsForm
} from '../../intake.util';

import { AccommodationView } from './accommodation.view';
import { AccommodationFormValue } from './accommodation-form';

const mapStateToProps = ({
  intake: {
    workflow: { sections }
  }
}: RootState) => ({
  sections
});

const mapDispatchToProps = {
  submitStep: submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;
type StateProps = ReturnType<typeof mapStateToProps>;

type AccommodationProps = {
  step: IntakeSectionStep<AccommodationValue>;
} & DispatchProps &
  StateProps;

type AccommodationState = {
  submitting: boolean;
};

export class AccommodationContainer extends React.Component<
  AccommodationProps,
  AccommodationState
> {
  state = {
    submitting: false
  };

  intakeSubmissionService = IntakeAccommodationSubmissionService.getInstance();

  getPregnancyRelated = (value: string) => {
    return value === 'YES' ? 'Yes' : 'No';
  };

  handleStepSubmit = async ({
    pregnancyRelated,
    limitations,
    workPlaceAccommodations,
    additionalNotes
  }: AccommodationFormValue) => {
    const { step, submitStep, sections } = this.props;

    const yourRequest = sections.find(
      ({ id }) => id === IntakeSectionId.YOUR_REQUEST
    );
    const yourRequestValue =
      yourRequest && (yourRequest.value as YourRequestValue);
    const options = yourRequestValue && yourRequestValue.initialRequest;
    const workDetails = yourRequestValue && yourRequestValue.workDetails;
    const occupation =
      workDetails && workDetails.occupation !== null
        ? workDetails.occupation
        : undefined;

    const personalDetails =
      yourRequestValue && yourRequestValue.personalDetails;

    const accommodation: AccommodationValue = {
      pregnancyRelated: this.getPregnancyRelated(pregnancyRelated),
      limitations: limitations.map(type => ({ limitationType: type })),
      workPlaceAccommodations,
      additionalNotes
    };

    // submit the action
    try {
      this.setState({ submitting: true });

      const submissionState = await this.submitIntake(
        accommodation,
        options,
        occupation,
        personalDetails
      );

      await this.intakeSubmissionService.completeIntake(
        submissionState.model as IntakeAccommodationSubmission,
        submissionState.result
      );

      submitStep({
        ...step,
        value: { accommodation, submissionState }
      });
    } catch (error) {
      submitStep({
        ...step,
        value: { accommodation, submissionState: { result: error } }
      });
    } finally {
      this.setState({ submitting: false });
    }
  };

  submitIntake = async (
    accommodation: AccommodationValue,
    options?: EventOptionId[],
    customerOccupation?: CustomerOccupation,
    personalDetails?: PersonalDetailsValue
  ): Promise<IntakeSubmissionState> => {
    if (options) {
      const model = new IntakeAccommodationSubmission(
        options,
        accommodation,
        buildWorkDetailsForm(customerOccupation),
        buildPersonalDetailsForm(personalDetails)
      );

      const result = await this.intakeSubmissionService.process(model);
      const state: IntakeSubmissionState = {
        model,
        result
      };

      return state;
    }

    return Promise.reject({
      generalError: 'Cannot create an intake without any event options'
    });
  };

  render() {
    const {
      step: { isActive }
    } = this.props;
    const { submitting } = this.state;

    return (
      <AccommodationView
        isActive={isActive}
        stepSubmit={this.handleStepSubmit}
        submitting={submitting}
      />
    );
  }
}

export const Accommodation = connect(
  mapStateToProps,
  mapDispatchToProps
)(AccommodationContainer);
