import React from 'react';
import { WorkPlaceAccommodation } from 'fineos-js-api-client';

import { IntakePanel } from '../../../../ui';

type WorkplaceAccommodationPanelProps = {
  accommodation: WorkPlaceAccommodation;
  disabled: boolean;
  onEdit: () => void;
  onDelete: () => void;
};

export const WorkplaceAccommodationPanel: React.FC<WorkplaceAccommodationPanelProps> = ({
  accommodation: {
    accommodationCategory,
    accommodationDescription,
    accommodationType
  },
  disabled,
  onDelete,
  onEdit
}) => (
  <IntakePanel
    disabled={disabled}
    onEdit={onEdit}
    onDelete={onDelete}
    title={accommodationCategory}
  >
    {accommodationType !== 'Other'
      ? accommodationType
      : accommodationDescription}
  </IntakePanel>
);
