import { WorkPlaceAccommodation, Limitation } from 'fineos-js-api-client';

import { IntakeSubmissionState } from '../../../shared/services';
import { TimeOff } from '../../../shared/types';

export type RequestDetailsValue = {
  [key: string]: string;
};

export type AccommodationValue = {
  pregnancyRelated: string;
  workPlaceAccommodations: WorkPlaceAccommodation[];
  limitations: Limitation[];
  additionalNotes: string;
};

export type TimeOffValue = TimeOff & {
  submissionState: IntakeSubmissionState;
};

export type DetailsValue = {
  requestDetails: RequestDetailsValue | AccommodationValue;
  timeOff: TimeOffValue;
};
