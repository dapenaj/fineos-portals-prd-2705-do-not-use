import React from 'react';
import { RadioChangeEvent } from 'antd/lib/radio';
import { FormattedMessage } from 'react-intl';
import { range as _range } from 'lodash';

import {
  RequestDetailField,
  RequestDetailFieldId,
  RequestDetailAnswerType,
  SelectOption
} from '../../../../../../shared/types';
import {
  FormLabel,
  FormikDatePickerInput,
  FormikDateRangeInput,
  FormikNumberInput,
  FormikRadioGroupInput,
  FormikTextAreaInput,
  FormikTextInput,
  FormikSelectInput
} from '../../../../../../shared/ui';
import { validateField } from '../request-details-validation';

type RequestFieldProps = {
  isActive: boolean;
  field: RequestDetailField;
  onChange?: (e: RadioChangeEvent) => void;
};

const OPTIONAL_FIELDS = [
  RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
  RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
  RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS
];

export const RequestFieldInput: React.FC<RequestFieldProps> = ({
  isActive,
  field,
  onChange
}) => {
  // common props that need to passed to each type of Component
  const commonProps = {
    name: field.id,
    isDisabled: !isActive,
    label: (
      <FormLabel
        required={!OPTIONAL_FIELDS.includes(field.id)}
        label={`INTAKE.DETAILS.REQUEST_DETAILS.${field.id}`}
        hintTooltip={
          field.hasTooltip
            ? `INTAKE.DETAILS.REQUEST_DETAILS_TOOLTIP.${field.id}`
            : ''
        }
      />
    ),
    validate: (value: any) => validateField(field.id, value),
    labelCol: { xs: 48, sm: 24 },
    wrapperCol: { xs: 24, sm: 12 }
  };

  const handleOnChange = (e: any) => {
    if (onChange) {
      onChange(e);
    }
  };

  switch (field.answerType) {
    case RequestDetailAnswerType.STRING:
      return <FormikTextInput {...commonProps} />;
    case RequestDetailAnswerType.COUNT:
      return (
        <FormikNumberInput {...commonProps} step={1} precision={0} min={0} />
      );
    case RequestDetailAnswerType.CURRENCY:
    case RequestDetailAnswerType.DECIMAL_VALUE:
      return <FormikNumberInput {...commonProps} />;
    case RequestDetailAnswerType.LONG_TEXT:
      return <FormikTextAreaInput {...commonProps} />;
    case RequestDetailAnswerType.DATE:
      return <FormikDatePickerInput {...commonProps} />;
    case RequestDetailAnswerType.DATE_RANGE:
      return <FormikDateRangeInput {...commonProps} />;
    case RequestDetailAnswerType.BOOLEAN:
      return (
        <FormikRadioGroupInput
          {...commonProps}
          optionPrefix="INTAKE.DETAILS.REQUEST_DETAILS."
          radioOptions={['YES', 'NO']}
        />
      );
    case RequestDetailAnswerType.NUMBER_SELECT:
      const min = field.numberSelectRange && field.numberSelectRange.min;
      const max = field.numberSelectRange && field.numberSelectRange.max;
      let countOptions: SelectOption[] = [];

      if (min && max) {
        countOptions = _range(min, max).map(value => ({
          value,
          text: `${value}`
        }));
      }

      return (
        <FormikSelectInput
          {...commonProps}
          labelCol={{ xs: 48, sm: 24 }}
          wrapperCol={{ xs: 6, sm: 3 }}
          options={countOptions}
        />
      );
    case RequestDetailAnswerType.OPTIONS:
      return (
        <FormikRadioGroupInput
          {...commonProps}
          optionPrefix="INTAKE.DETAILS.REQUEST_DETAILS."
          radioOptions={field.options!.map(opt => opt.id)}
          onChange={(e: RadioChangeEvent) => handleOnChange(e)}
        />
      );
    default:
      return (
        <FormattedMessage
          id={`INTAKE.DETAILS.REQUEST_DETAILS.${field.id}`}
          data-test-el="request-field-default"
        />
      );
  }
};
