import React from 'react';
import { connect } from 'react-redux';

import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import {
  IntakeSectionStep,
  RequestDetailForm,
  IntakeSectionId,
  EventOptionId
} from '../../../../shared/types';
import { RequestDetailFormService } from '../../../../shared/services';
import { RequestDetailsValue, AccommodationValue } from '../details.type';
import { RootState } from '../../../../store';
import { YourRequestValue } from '../../your-request';

import { RequestDetailsView } from './request-details.view';
import { RequestDetailsFormValue } from './form';

const mapStateToProps = ({
  intake: {
    workflow: { sections }
  }
}: RootState) => ({
  sections
});

const mapDispatchToProps = {
  submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;
type StateProps = ReturnType<typeof mapStateToProps>;

type RequestDetailsProps = {
  step: IntakeSectionStep<RequestDetailsValue | AccommodationValue>;
};

type RequestDetailsContainerProps = RequestDetailsProps &
  DispatchProps &
  StateProps;

type RequestDetailsState = {
  forms: RequestDetailForm[];
  accommodationSelected: boolean;
  loading: boolean;
};
export class RequestDetailsContainer extends React.Component<
  RequestDetailsContainerProps,
  RequestDetailsState
> {
  requestDetailsFormService = RequestDetailFormService.getInstance();

  state = {
    forms: [] as RequestDetailForm[],
    accommodationSelected: false,
    loading: true
  };

  componentDidMount() {
    const { sections } = this.props;
    const yourRequest = sections.find(
      section => section.id === IntakeSectionId.YOUR_REQUEST
    )!;

    const { initialRequest } = yourRequest.value! as YourRequestValue;

    const forms = this.requestDetailsFormService.getDefaultForms(
      initialRequest
    );

    this.setState({
      forms: forms,
      accommodationSelected: initialRequest.includes(
        EventOptionId.ACCOMMODATION
      ),
      loading: false
    });
  }

  handleSubmit = (formValue: RequestDetailsFormValue) => {
    this.props.submitIntakeWorkflowStep({
      ...this.props.step,
      value: formValue
    });
  };

  render() {
    const { step } = this.props;

    const { forms, loading, accommodationSelected } = this.state;

    return (
      <RequestDetailsView
        step={step}
        forms={forms}
        loading={loading}
        stepSubmit={this.handleSubmit}
        accommodationSelected={accommodationSelected}
      />
    );
  }
}
export const RequestDetails = connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestDetailsContainer);
