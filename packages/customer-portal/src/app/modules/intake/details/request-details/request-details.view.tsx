import React from 'react';

import { RequestDetailForm, IntakeSectionStep } from '../../../../shared/types';
import { Accommodation } from '../accomodation';
import { RequestDetailsValue, AccommodationValue } from '../details.type';

import { RequestDetailsForm, RequestDetailsFormValue } from './form';

type RequestDetailsViewProps = {
  forms: RequestDetailForm[];
  loading: boolean;
  accommodationSelected: boolean;
  step: IntakeSectionStep<RequestDetailsValue | AccommodationValue>;
  stepSubmit: (formValue: RequestDetailsFormValue) => void;
};

export const RequestDetailsView: React.FC<RequestDetailsViewProps> = ({
  forms,
  loading,
  stepSubmit,
  step,
  accommodationSelected
}) => (
  <>
    {accommodationSelected ? (
      <Accommodation step={step as IntakeSectionStep<AccommodationValue>} />
    ) : (
      <RequestDetailsForm
        forms={forms}
        loading={loading}
        isActive={step.isActive}
        submitStep={stepSubmit}
      />
    )}
  </>
);
