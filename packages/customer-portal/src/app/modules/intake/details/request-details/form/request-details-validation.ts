import moment from 'moment';

import { RequestDetailFieldId } from '../../../../../shared/types';

// This method is for handling the validation of all fields in the RequestDetailsForm.
// if a field is invalid, then return a string with the error message
export const validateField = (field: string, value: any) => {
  switch (field) {
    // Accident Intake fields
    case RequestDetailFieldId.DESCRIBE_ACCIDENT:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (value.length < 5) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.MINIMUM_CHARS';
      } else if (value.length > 150) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.MAXIMUM_CHARS';
      }
      break;
    case RequestDetailFieldId.FIRST_SYMPTOM_DATE:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (moment(value).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.FIRST_SYMPTOM_DATE_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.DATE_FIRST_TREATED:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (moment(value).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.DATE_FIRST_TREATED_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.ACCIDENT_DATE:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (moment(value).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACCIDENT_DATE_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (moment(value).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACCIDENT_DATE_FIRST_TREATED_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (value && moment(value[0]).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS:
      if (value && value.length > 150) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.MAXIMUM_CHARS';
      }
      break;
    // Pregnancy Intake fields
    case RequestDetailFieldId.ACTUAL_DELIVERY_DATE:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (moment(value).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.ACTUAL_DELIVERY_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION:
      if (value && moment(value[0]).isBefore(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.EXPECTED_HOSPITAL_PAST_DATE';
      }
      break;
    case RequestDetailFieldId.EXPECTED_DELIVERY_DATE:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (moment(value).isBefore(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.EXPECTED_DELIVERY_PAST_DATE';
      }
      break;
    case RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      } else if (value && moment(value[0]).isAfter(moment.now())) {
        return 'INTAKE.DETAILS.REQUEST_DETAILS.VALIDATION.OVERNIGHT_HOSPITAL_STAY_DURATION_FUTURE_DATE';
      }
      break;
    case RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS:
      return undefined;
    default:
      if (!Boolean(value)) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
  }

  return undefined;
};
