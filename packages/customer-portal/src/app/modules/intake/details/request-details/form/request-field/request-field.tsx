import React from 'react';
import { FormikProps } from 'formik';
import { Col } from 'antd';

import { RequestDetailFormService } from '../../../../../../shared/services';
import { RequestDetailField } from '../../../../../../shared/types';
import { Row } from '../../../../../../shared/ui';
import { RequestDetailsFormValue } from '../form-value.type';

import { RequestFieldInput } from './request-field-input.component';

type RequestFieldProps = {
  isActive: boolean;
  field: RequestDetailField;
} & FormikProps<RequestDetailsFormValue>;

type RequestFieldState = {
  additionalFields?: RequestDetailField[];
};

export class RequestField extends React.Component<
  RequestFieldProps,
  RequestFieldState
> {
  requestDetailsFormService = RequestDetailFormService.getInstance();

  state = {} as RequestFieldState;

  handleRadioGroupInputChange(value: string) {
    const { field } = this.props;
    // remove all of the existing additional fields...
    this.removeFields();
    // find the selected option
    const selectedOption =
      field.options && field.options.find(opt => opt.id === value);
    // if we have a value, get the targetFormId
    if (selectedOption && selectedOption.targetFormId) {
      // get the form form using that id
      const additionalForms = this.requestDetailsFormService.getForm(
        selectedOption.targetFormId
      );
      // if we found a form for the selectedOption...
      if (additionalForms && additionalForms.layout.length) {
        // put the fields into the state
        this.setState({
          additionalFields: additionalForms.layout
        });
      }
    }
  }

  removeFields() {
    const { additionalFields } = this.state;
    const { unregisterField, values } = this.props;

    // remove the field values if set
    if (additionalFields && additionalFields.length) {
      additionalFields.forEach(field => {
        unregisterField(field.id);
        delete values[field.id];
      });
    }
    this.setState({
      additionalFields: undefined
    });
  }

  render() {
    const { field, isActive, ...props } = this.props;
    const { additionalFields } = this.state;

    return (
      <div data-test-el="request-field">
        <Row>
          <Col style={{ width: '100%' }}>
            <RequestFieldInput
              field={field}
              isActive={isActive}
              onChange={e => this.handleRadioGroupInputChange(e.target.value)}
            />
          </Col>
        </Row>
        {additionalFields &&
          additionalFields.map(additionalField => (
            <RequestField
              key={additionalField.id}
              field={additionalField}
              isActive={isActive}
              {...props}
            />
          ))}
      </div>
    );
  }
}
