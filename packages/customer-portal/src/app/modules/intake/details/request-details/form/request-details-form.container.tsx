import { withFormik } from 'formik';

import { RequestDetailForm } from '../../../../../shared/types';

import { RequestDetailsFormView } from './request-details-form.view';
import { RequestDetailsFormValue } from './form-value.type';

export type RequestDetailsFormProps = {
  forms: RequestDetailForm[];
  loading: boolean;
  isActive: boolean;
  submitStep: (values: RequestDetailsFormValue) => void;
};

export const RequestDetailsForm = withFormik<
  RequestDetailsFormProps,
  RequestDetailsFormValue
>({
  mapPropsToValues: props => ({} as RequestDetailsFormValue),
  handleSubmit: (values, { props }) => props.submitStep(values)
})(RequestDetailsFormView);
