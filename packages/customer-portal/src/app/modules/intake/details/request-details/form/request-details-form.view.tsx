import React from 'react';
import { Form, FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';

import { ContentLayout } from '../../../../../shared/layouts';
import { RequestDetailForm } from '../../../../../shared/types';
import { IntakeStepControls } from '../../../ui/intake-steps';

import styles from './request-details-form.module.scss';
import { RequestFormGroup } from './request-form-group/request-form-group.component';
import { RequestDetailsFormValue } from './form-value.type';

export type RequestDetailsFormViewProps = {
  forms: RequestDetailForm[];
  isActive: boolean;
} & FormikProps<RequestDetailsFormValue>;

export const RequestDetailsFormView: React.FC<RequestDetailsFormViewProps> = ({
  forms,
  ...props
}) => (
  <Form name="request-details">
    <ContentLayout noPadding={true} direction="column">
      <h3 className={styles.title}>
        <FormattedMessage id="INTAKE.DETAILS.REQUEST_DETAILS.TELL_US_MORE" />
      </h3>

      {forms.map(form => (
        <RequestFormGroup key={form.id} fields={form.layout} {...props} />
      ))}

      <IntakeStepControls
        {...props}
        isDisabled={!props.isValid || !props.isActive}
      />
    </ContentLayout>
  </Form>
);
