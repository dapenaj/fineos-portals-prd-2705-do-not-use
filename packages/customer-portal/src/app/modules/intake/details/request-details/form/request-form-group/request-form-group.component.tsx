import React from 'react';
import { FormikProps } from 'formik';

import { RequestDetailField } from '../../../../../../shared/types/request-details.type';
import { RequestField } from '../request-field/request-field';
import { RequestDetailsFormValue } from '../form-value.type';

type RequestFormGroupProps = {
  fields: RequestDetailField[];
  isActive: boolean;
} & FormikProps<RequestDetailsFormValue>;

export const RequestFormGroup: React.FC<RequestFormGroupProps> = ({
  fields,
  ...props
}) => (
  <>
    {fields.map(field => (
      <RequestField key={field.id} field={field} {...props} />
    ))}
  </>
);
