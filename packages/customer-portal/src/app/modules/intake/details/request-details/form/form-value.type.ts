import { RequestDetailFieldId } from '../../../../../shared/types';

export type RequestDetailsFormValue = {
  [key in RequestDetailFieldId]?: string;
};
