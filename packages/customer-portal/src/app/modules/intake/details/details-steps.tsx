import React from 'react';
import { FormattedMessage } from 'react-intl';

import { IntakeSectionStep, IntakeSectionStepId } from '../../../shared/types';

import { RequestDetailsValue } from './details.type';
import { TimeOffValue } from './details.type';

const requestDetailsStep: IntakeSectionStep<RequestDetailsValue> = {
  id: IntakeSectionStepId.REQUEST_DETAILS,
  title: <FormattedMessage id="INTAKE.DETAILS.REQUEST_DETAILS.TITLE" />,
  isComplete: false,
  isActive: true
};

const timeOffStep: IntakeSectionStep<TimeOffValue> = {
  id: IntakeSectionStepId.TIME_OFF,
  title: <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.TITLE" />,
  isComplete: false,
  isActive: false
};

export const detailsSteps = [requestDetailsStep, timeOffStep];
