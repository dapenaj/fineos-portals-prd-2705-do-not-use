import React from 'react';
import { Icon, Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import { Panel } from '../../../../../../../shared/ui';

import styles from './add-leave-period-placeholder.module.scss';

type AddLeavePeriodPlaceholderProps = {
  showFull: boolean;
  disabled: boolean;
  onClick: () => void;
};

export const AddLeavePeriodPlaceholder: React.FC<AddLeavePeriodPlaceholderProps> = ({
  showFull,
  onClick,
  disabled
}) =>
  showFull ? (
    <Panel>
      <div className={styles.wrapper}>
        <div className={styles.icon}>
          <Icon type="calendar" />
        </div>
        <div className={styles.content}>
          <div className={styles.message}>
            <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.ADD_TIME_DESCRIPTION" />
          </div>
          <Button
            type="link"
            disabled={disabled}
            onClick={onClick}
            data-test-el="add-time-off-btn"
          >
            <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.ADD_TIME_OFF" />
          </Button>
        </div>
      </div>
    </Panel>
  ) : (
    <Button
      type="link"
      disabled={disabled}
      onClick={onClick}
      data-test-el="add-another-time-off-btn"
    >
      <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.ADD_ANOTHER" />
    </Button>
  );
