import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Icon } from 'antd';

import {
  TimeOffLeavePeriod,
  TimeOffPeriodType
} from '../../../../../../../shared/types';
import { LeavePeriod } from '../../../form-value.type';
import { formatDateForDisplay } from '../../../../../../../shared/utils';
import { IntakePanel } from '../../../../../ui';

import styles from './leave-period-panel.module.scss';

type LeavePeriodPanelProps = {
  leavePeriod: LeavePeriod;
  disabled: boolean;
  isOverlapping: boolean;
  onEdit: () => void;
  onDelete: () => void;
};

export const LeavePeriodPanel: React.FC<LeavePeriodPanelProps> = ({
  leavePeriod: { type, leavePeriod },
  disabled,
  isOverlapping,
  onEdit,
  onDelete
}) => (
  <IntakePanel
    disabled={disabled}
    onEdit={onEdit}
    onDelete={onDelete}
    title={<FormattedMessage id={`INTAKE.DETAILS.TIME_OFF.${type}`} />}
  >
    <div className={styles.message} data-test-el="leave-period-panel">
      <FormattedMessage
        id="INTAKE.DETAILS.TIME_OFF.PERIOD_DESCRIPTION"
        values={{
          start: (
            <span className={styles.date}>
              {formatDateForDisplay(leavePeriod.startDate)}
            </span>
          ),
          end: (
            <span className={styles.date}>
              {leavePeriod.endDate ? (
                formatDateForDisplay(leavePeriod.endDate)
              ) : (
                <FormattedMessage id="INTAKE.DETAILS.LABELS.UNKNOWN" />
              )}
            </span>
          )
        }}
      />
    </div>
    {type === TimeOffPeriodType.TIME_OFF && (
      <div className={styles.message}>
        <FormattedMessage
          id="INTAKE.DETAILS.TIME_OFF.PERIOD_RETURN"
          values={{
            date: (
              <span className={styles.date}>
                {formatDateForDisplay(
                  (leavePeriod as TimeOffLeavePeriod).expectedReturnToWorkDate
                )}
              </span>
            )
          }}
        />
      </div>
    )}
    {isOverlapping && (
      <div className={styles.warning}>
        <div className={styles.warningIcon}>
          <Icon type="warning" theme="filled" />
        </div>
        <div className={styles.warningContent}>
          <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.OVERLAP_WARNING" />
        </div>
      </div>
    )}
  </IntakePanel>
);
