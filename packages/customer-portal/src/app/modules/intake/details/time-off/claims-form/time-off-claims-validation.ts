import * as Yup from 'yup';

import { TimeOffFormValue } from '../form-value.type';

export const TimeOffClaimsSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<TimeOffFormValue>
>> = Yup.object().shape({
  outOfWork: Yup.boolean().required('COMMON.VALIDATION.REQUIRED'),
  leavePeriods: Yup.array().of(
    Yup.object().shape({
      type: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
      leavePeriod: Yup.object().shape({
        startDate: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
        expectedReturnToWorkDate: Yup.string().required(
          'COMMON.VALIDATION.REQUIRED'
        )
      })
    })
  )
});
