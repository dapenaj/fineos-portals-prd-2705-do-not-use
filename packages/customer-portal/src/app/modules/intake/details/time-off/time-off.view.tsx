import React from 'react';

import { ContentLayout } from '../../../../shared/layouts';

import { TimeOffForm } from './form';
import { TimeOffFormValue } from './form-value.type';
import { TimeOffClaimsForm } from './claims-form';

type TimeOffViewProps = {
  isActive: boolean;
  isAbsenceUser: boolean;
  stepSubmit: (values: TimeOffFormValue) => void;
  submitting: boolean;
};

export const TimeOffView: React.FC<TimeOffViewProps> = ({
  isActive,
  isAbsenceUser,
  stepSubmit,
  submitting
}) => (
  <ContentLayout direction="column" noPadding={true} data-test-el="time-off">
    {isAbsenceUser && (
      <TimeOffForm
        isActive={isActive}
        submitStep={stepSubmit}
        submitting={submitting}
      />
    )}
    {!isAbsenceUser && (
      <TimeOffClaimsForm
        isActive={isActive}
        submitStep={stepSubmit}
        submitting={submitting}
      />
    )}
  </ContentLayout>
);
