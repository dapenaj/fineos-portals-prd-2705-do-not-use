import React from 'react';
import { FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';
import { Form, Row, Col } from 'antd';
import { isEmpty } from 'lodash';

import { BooleanInput, DatePickerInput } from '../../../../../shared/ui';
import { IntakeStepControls } from '../../../ui';
import { TimeOffProps } from '../form';
import { TimeOffFormValue } from '../form-value.type';

import styles from './time-off-claims-form.module.scss';

export type TimeOffClaimsFormViewProps = TimeOffProps &
  FormikProps<TimeOffFormValue>;

export class TimeOffClaimsFormView extends React.Component<
  TimeOffClaimsFormViewProps
> {
  render() {
    const { isActive, handleSubmit, errors, isValid, submitting } = this.props;

    const allowSubmit = isEmpty(errors) && isActive && isValid;

    return (
      <>
        <div className={styles.title}>
          <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.HOW_MUCH_TIME_OFF_CLAIMS" />
        </div>

        <Form
          name="time-off-claims"
          className={styles.form}
          onSubmit={handleSubmit}
        >
          <div className={styles.formGroup}>
            <Row>
              <Col>
                <BooleanInput
                  {...this.props}
                  isDisabled={!isActive}
                  name="outOfWork"
                  label={
                    <FormattedMessage id="INTAKE.DETAILS.LABELS.CURRENTLY_ABSENT" />
                  }
                />
              </Col>
            </Row>
          </div>
          <div className={styles.formGroup}>
            <Row gutter={32}>
              <Col>
                <DatePickerInput
                  {...this.props}
                  labelCol={{ xs: 48, sm: 24 }}
                  wrapperCol={{ xs: 24, sm: 12 }}
                  isDisabled={!isActive}
                  name="leavePeriods[0].leavePeriod.startDate"
                  label={
                    <FormattedMessage id="INTAKE.DETAILS.LABELS.FIRST_DAY_OF_ABSENCE_OUT_OF_WORK" />
                  }
                />
              </Col>
            </Row>
          </div>
          <div className={styles.formGroup}>
            <Row gutter={32}>
              <Col>
                <DatePickerInput
                  {...this.props}
                  labelCol={{ xs: 48, sm: 24 }}
                  wrapperCol={{ xs: 24, sm: 12 }}
                  isDisabled={!isActive}
                  name="leavePeriods[0].leavePeriod.expectedReturnToWorkDate"
                  label={
                    <FormattedMessage id="INTAKE.DETAILS.LABELS.RETURN_TO_WORK" />
                  }
                />
              </Col>
            </Row>
          </div>
          <div className={styles.controls}>
            <IntakeStepControls
              {...this.props}
              isDisabled={!allowSubmit}
              submitting={submitting}
            />
          </div>
        </Form>
      </>
    );
  }
}
