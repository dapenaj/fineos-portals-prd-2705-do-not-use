import { withFormik, FormikBag } from 'formik';

import {
  TimeOffLeavePeriod,
  TimeOffPeriodType
} from '../../../../../../shared/types';
import { LeavePeriod } from '../../form-value.type';
import { LeavePeriodsSchema } from '../time-off-validation';

import { LeavePeriodFormView } from './leave-period-form.view';

export type LeavePeriodFormProps = {
  leavePeriod: LeavePeriod;
  isEdit: boolean;
  isOutOfWork: boolean;
  onSubmit: (values: LeavePeriod) => void;
  onCancel: () => void;
  disabledTypeOptions?: string[];
};

const mapPropsToValues = ({ leavePeriod }: LeavePeriodFormProps) => {
  // WARNING: Special behaviour for the TIME_OFF period
  // The value of startDateFullDay needs to be inversed.
  // This is due to the required UX of the form vs the actual use of the value
  if (leavePeriod.type === TimeOffPeriodType.TIME_OFF) {
    const timeOff = leavePeriod.leavePeriod as TimeOffLeavePeriod;
    timeOff.startDateFullDay = !timeOff.startDateFullDay;
  }

  return leavePeriod;
};

const handleSubmit = (
  values: LeavePeriod,
  { props }: FormikBag<LeavePeriodFormProps, LeavePeriod>
) => {
  // WARNING: Special behaviour for the TIME_OFF period
  // The value of startDateFullDay needs to be inversed.
  // This is due to the required UX of the form vs the actual use of the value
  if (values.type === TimeOffPeriodType.TIME_OFF) {
    const timeOff = values.leavePeriod as TimeOffLeavePeriod;
    timeOff.startDateFullDay = !timeOff.startDateFullDay;
  }

  props.onSubmit(values);
};

export const LeavePeriodForm = withFormik<LeavePeriodFormProps, LeavePeriod>({
  mapPropsToValues,
  validationSchema: LeavePeriodsSchema,
  isInitialValid: props => props.isEdit,
  handleSubmit
})(LeavePeriodFormView);
