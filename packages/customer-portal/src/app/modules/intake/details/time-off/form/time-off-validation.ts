import * as Yup from 'yup';
import moment from 'moment';
import { isEqual as _isEqual, orderBy as _orderBy } from 'lodash';

import {
  TimeOffLeavePeriod,
  TimeOffPeriodType
} from '../../../../../shared/types';
import { parseDate } from '../../../../../shared/utils';
import { TimeOffFormValue, LeavePeriod } from '../form-value.type';

// This method is for handling the validation of additional fields in the LeavePeriodForm
// Ideally these validation rules would have been defined using Yup - but due to the conditional
// nature of the fields, the Yup validation was not being applied. This method works for now.
export const validateAdditionalFields = (
  field: string,
  value: any,
  { leavePeriod }: LeavePeriod
) => {
  switch (field) {
    case 'startDateOffHours':
    case 'startDateOffMinutes':
      if (
        !(leavePeriod as TimeOffLeavePeriod).startDateOffHours &&
        !(leavePeriod as TimeOffLeavePeriod).startDateOffMinutes
      ) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
      break;
    case 'endDateOffHours':
    case 'endDateOffMinutes':
      if (
        !(leavePeriod as TimeOffLeavePeriod).endDateOffHours &&
        !(leavePeriod as TimeOffLeavePeriod).endDateOffMinutes
      ) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
      break;
    case 'returnToWorkFullDate':
      if (!value) {
        return 'COMMON.VALIDATION.REQUIRED';
      }
      break;
  }
  return undefined;
};

export const checkIfOverlappingLeavePeriod = (
  currentPeriod: LeavePeriod,
  allPeriods: LeavePeriod[]
): boolean => {
  let isOverlapping = false;

  // only do comparison if we have periods to compare
  if (allPeriods.length > 1) {
    // sort the leave periods by startDate - and also remove the currentPeriod from the array
    const sortedPeriods: LeavePeriod[] = _orderBy(
      allPeriods.filter(val => !_isEqual(val, currentPeriod)),
      'startDate',
      'desc'
    );

    // check if the currentPeriod is within the dates of any of the sortedPeriods
    for (const period of sortedPeriods) {
      // Note: endDate could be undefined because type = episodic, so we will assume endDate is the end of time...

      const currentStart = parseDate(currentPeriod.leavePeriod.startDate);
      const compareStart = parseDate(period.leavePeriod.startDate);

      if (!currentPeriod.leavePeriod.endDate && !period.leavePeriod.endDate) {
        // if there is no endDate for both, then it will always be a problem (but this should never happen)
        isOverlapping = true;
        break;
      } else if (
        !currentPeriod.leavePeriod.endDate &&
        period.leavePeriod.endDate
      ) {
        // else if there is no endDate for currentPeriod...
        const compareEnd = parseDate(period.leavePeriod.endDate);
        if (
          (currentStart.isSameOrAfter(compareStart) &&
            currentStart.isSameOrBefore(compareEnd)) ||
          currentStart.isSameOrBefore(compareStart)
        ) {
          isOverlapping = true;
          break;
        }
      } else if (
        currentPeriod.leavePeriod.endDate &&
        !period.leavePeriod.endDate
      ) {
        // else if there is no endDate for comparePeriod...
        const currentEnd = parseDate(currentPeriod.leavePeriod.endDate);
        if (currentEnd.isSameOrAfter(compareStart)) {
          isOverlapping = true;
          break;
        }
      } else {
        // else there is endDate for both...
        const currentEnd = parseDate(currentPeriod.leavePeriod.endDate!);
        const compareEnd = parseDate(period.leavePeriod.endDate!);
        if (
          (currentStart.isSameOrAfter(compareStart) &&
            currentStart.isSameOrBefore(compareEnd)) ||
          (currentEnd.isSameOrAfter(compareStart) &&
            currentEnd.isSameOrBefore(compareEnd))
        ) {
          isOverlapping = true;
          break;
        }
      }
    }
  }

  return isOverlapping;
};

export const LeavePeriodsSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<LeavePeriod>
>> = Yup.object().shape({
  type: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  leavePeriod: Yup.object()
    .when('type', {
      is: type => type === TimeOffPeriodType.TIME_OFF,
      then: Yup.object().shape({
        lastDayWorked: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        startDateFullDay: Yup.boolean().required('COMMON.VALIDATION.REQUIRED'),
        startDate: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        endDate: Yup.string().when(
          'startDate',
          (startDate: string, schema: any) =>
            startDate
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(startDate).toDate(),
                    'INTAKE.DETAILS.TIME_OFF.VALIDATION.END_AFTER_START'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
        ),
        expectedReturnToWorkDate: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .when('endDate', (endDate: string, schema: any) =>
            endDate
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(endDate).toDate(),
                    'INTAKE.DETAILS.TIME_OFF.VALIDATION.ERTW_AFTER_END'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
          ),
        endDateFullDay: Yup.boolean().required('COMMON.VALIDATION.REQUIRED'),
        status: Yup.string().required('COMMON.VALIDATION.REQUIRED')
      })
    })
    .when('type', {
      is: type => type === TimeOffPeriodType.REDUCED_SCHEDULE,
      then: Yup.object().shape({
        startDate: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        endDate: Yup.string().when(
          'startDate',
          (startDate: string, schema: any) =>
            startDate
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(startDate).toDate(),
                    'INTAKE.DETAILS.TIME_OFF.VALIDATION.END_AFTER_START'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
        ),
        status: Yup.string().required('COMMON.VALIDATION.REQUIRED')
      })
    })
    .when('type', {
      is: type => type === TimeOffPeriodType.EPISODIC,
      then: Yup.object().shape({
        startDate: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        endDate: Yup.string().when(
          'startDate',
          (startDate: string, schema: any) =>
            startDate
              ? Yup.date()
                  .nullable()
                  .min(
                    moment(startDate).toDate(),
                    'INTAKE.DETAILS.TIME_OFF.VALIDATION.END_AFTER_START'
                  )
              : schema
        )
      })
    })
});

export const TimeOffSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<TimeOffFormValue>
>> = Yup.object().shape({
  outOfWork: Yup.boolean().required('COMMON.VALIDATION.REQUIRED')
});
