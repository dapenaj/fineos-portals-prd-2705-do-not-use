import React from 'react';
import { FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';
import { Form, Row, Col } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';

import {
  TimeOffLeavePeriod,
  TimeOffPeriodType
} from '../../../../../shared/types';
import { BooleanInput, FormLabel } from '../../../../../shared/ui';
import { IntakeStepControls } from '../../../ui';
import { TimeOffFormValue, LeavePeriod } from '../form-value.type';

import { LeavePeriodForm } from './leave-period-form';
import { TimeOffProps } from './time-off-form';
import { AddLeavePeriodPlaceholder, LeavePeriodPanel } from './ui';
import styles from './time-off-form.module.scss';
import { checkIfOverlappingLeavePeriod } from './time-off-validation';

export type TimeOffFormViewProps = TimeOffProps & FormikProps<TimeOffFormValue>;

type TimeOffFormViewState = {
  showForm: boolean;
  isEdit: boolean;
  leavePeriods: LeavePeriod[];
  overlappingPeriods: boolean[];
  currentPeriodIndex: number;
};

export class TimeOffFormView extends React.Component<
  TimeOffFormViewProps,
  TimeOffFormViewState
> {
  state: TimeOffFormViewState = {
    showForm: false,
    isEdit: false,
    leavePeriods: [],
    overlappingPeriods: [],
    currentPeriodIndex: 0
  };

  checkForOverlappingPeriods = (leavePeriods: LeavePeriod[]) => {
    const overlappingPeriods: boolean[] = [];
    leavePeriods.forEach(val =>
      overlappingPeriods.push(checkIfOverlappingLeavePeriod(val, leavePeriods))
    );
    return overlappingPeriods;
  };

  handleLeavePeriodCancel = () => {
    // if the form was cancelled during an Add, then remove the empty item
    if (!this.state.isEdit) {
      const leavePeriods: LeavePeriod[] = [...this.state.leavePeriods];
      leavePeriods.pop();
      this.setState({ leavePeriods });
    }

    this.setState({ showForm: false });
  };

  handleLeavePeriodSubmit = (value: LeavePeriod) => {
    const leavePeriods: LeavePeriod[] = [...this.state.leavePeriods];
    leavePeriods[this.state.currentPeriodIndex] = value;
    this.setState({
      leavePeriods,
      showForm: false,
      overlappingPeriods: this.checkForOverlappingPeriods(leavePeriods)
    });
  };

  handleAddLeavePeriod = () => {
    const leavePeriods: LeavePeriod[] = [
      ...this.state.leavePeriods,
      {
        type: TimeOffPeriodType.TIME_OFF,
        leavePeriod: { startDateFullDay: true } as TimeOffLeavePeriod
      }
    ];
    this.setState({
      leavePeriods,
      currentPeriodIndex: this.state.leavePeriods.length,
      isEdit: false,
      showForm: true
    });
  };

  handleEditLeavePeriod = (index: number) => {
    this.setState({
      currentPeriodIndex: index,
      isEdit: true,
      showForm: true
    });
  };

  handleDeleteLeavePeriod = (index: number) => {
    const leavePeriods: LeavePeriod[] = [...this.state.leavePeriods];
    leavePeriods.splice(index, 1);
    this.setState({
      leavePeriods,
      overlappingPeriods: this.checkForOverlappingPeriods(leavePeriods)
    });
  };

  handleOnSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    this.props.values.leavePeriods = this.state.leavePeriods;
    this.props.handleSubmit(event);
  };

  render() {
    const { isActive, errors, submitting } = this.props;
    const {
      showForm,
      leavePeriods,
      currentPeriodIndex,
      isEdit,
      overlappingPeriods
    } = this.state;

    const currentLeavePeriod = leavePeriods[currentPeriodIndex];

    const allowSubmit =
      _isEmpty(errors) &&
      isActive &&
      !!leavePeriods.length &&
      !overlappingPeriods.some(val => val);

    const disabledTypeOptions = leavePeriods.some(
      ({ type }) => type === TimeOffPeriodType.EPISODIC
    )
      ? [TimeOffPeriodType.EPISODIC]
      : [];

    return (
      <>
        {showForm && (
          <LeavePeriodForm
            leavePeriod={currentLeavePeriod}
            isOutOfWork={this.props.values.outOfWork}
            isEdit={isEdit}
            disabledTypeOptions={disabledTypeOptions}
            onSubmit={this.handleLeavePeriodSubmit}
            onCancel={this.handleLeavePeriodCancel}
          />
        )}
        {!showForm && (
          <Form
            name="time-off"
            className={styles.form}
            onSubmit={this.handleOnSubmit}
          >
            <div className={styles.radioGroup}>
              <Row>
                <Col>
                  <BooleanInput
                    {...this.props}
                    isDisabled={!isActive}
                    name="outOfWork"
                    label={
                      <FormLabel
                        required={true}
                        label="INTAKE.DETAILS.LABELS.CURRENTLY_ABSENT"
                      />
                    }
                  />
                </Col>
              </Row>
            </div>

            <div>
              <div className={styles.title} data-test-el="time-off-title">
                <FormattedMessage id="INTAKE.DETAILS.TIME_OFF.HOW_MUCH_TIME_OFF" />
              </div>

              {leavePeriods && leavePeriods.length > 0 && (
                <>
                  {leavePeriods.map((lp, index) => (
                    <LeavePeriodPanel
                      key={index}
                      leavePeriod={lp}
                      disabled={!isActive}
                      isOverlapping={overlappingPeriods[index]}
                      onEdit={() => this.handleEditLeavePeriod(index)}
                      onDelete={() => this.handleDeleteLeavePeriod(index)}
                    />
                  ))}
                </>
              )}

              <AddLeavePeriodPlaceholder
                disabled={!isActive}
                showFull={!leavePeriods || leavePeriods.length === 0}
                onClick={this.handleAddLeavePeriod}
              />
            </div>
            <div className={styles.controls}>
              <IntakeStepControls
                {...this.props}
                isDisabled={!allowSubmit}
                submitting={submitting}
              />
            </div>
          </Form>
        )}
      </>
    );
  }
}
