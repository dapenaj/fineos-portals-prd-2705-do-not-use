import React from 'react';
import { FormikProps } from 'formik';
import { Row, Col, Form } from 'antd';
import { FormattedMessage } from 'react-intl';
import { RadioChangeEvent } from 'antd/lib/radio';
import { Moment } from 'moment';

import {
  TimeOffLeavePeriod,
  ReducedScheduleLeavePeriod,
  EpisodicLeavePeriod,
  TimeOffPeriodType
} from '../../../../../../shared/types';
import {
  DatePickerInput,
  FormikRadioGroupInput,
  CheckboxInput,
  BooleanInput,
  FormikHoursInput,
  FormikMinutesInput,
  FormikDatePickerInput,
  FormLabel
} from '../../../../../../shared/ui';
import { formatDateForApi, parseDate } from '../../../../../../shared/utils';
import { IntakeStepControls } from '../../../../ui';
import { LeavePeriod } from '../../form-value.type';
import { TimeOffSelectionPopover } from '../ui';
import { validateAdditionalFields } from '../time-off-validation';

import { LeavePeriodFormProps } from './leave-period-form';
import styles from './leave-period-form.module.scss';

const typeOptions: TimeOffPeriodType[] = [
  TimeOffPeriodType.TIME_OFF,
  TimeOffPeriodType.REDUCED_SCHEDULE,
  TimeOffPeriodType.EPISODIC
];

type LeavePeriodFormViewProps = LeavePeriodFormProps & FormikProps<LeavePeriod>;

type LeavePeriodFormViewState = {
  type: TimeOffPeriodType;
  showStartTime: boolean;
  showEndTime: boolean;
};

export class LeavePeriodFormView extends React.Component<
  LeavePeriodFormViewProps,
  LeavePeriodFormViewState
> {
  state: LeavePeriodFormViewState = {
    type: TimeOffPeriodType.TIME_OFF,
    showStartTime: false,
    showEndTime: false
  };

  componentDidMount() {
    const { startDateFullDay, endDateFullDay } = this.props.values
      .leavePeriod as TimeOffLeavePeriod;
    this.setState({
      type: this.props.leavePeriod.type,
      showStartTime: Boolean(startDateFullDay),
      showEndTime: endDateFullDay === undefined ? false : !endDateFullDay
    });
  }

  handleTypeChange = ({ target: { value } }: RadioChangeEvent) => {
    // When the type changes, we want to strip out all of the non-relevant field based on the new type value
    const { leavePeriod } = this.props.values;
    if (value === TimeOffPeriodType.REDUCED_SCHEDULE) {
      const {
        startDate,
        endDate,
        status
      } = leavePeriod as ReducedScheduleLeavePeriod;
      this.props.setValues({
        type: value,
        leavePeriod: { startDate, endDate, status }
      });
    } else if (value === TimeOffPeriodType.EPISODIC) {
      const { startDate, endDate } = leavePeriod as EpisodicLeavePeriod;
      this.props.setValues({
        type: value,
        leavePeriod: { startDate, endDate }
      });
    }

    this.setState({ type: value, showStartTime: false, showEndTime: false });
  };

  handleLastDayChanged = (date: Moment | null, dateString: string) => {
    if (
      (this.props.values.leavePeriod as TimeOffLeavePeriod).startDateFullDay
    ) {
      // if last day is "partial day" then set the startDate to the same date
      this.props.setFieldValue(
        'leavePeriod.startDate',
        date && formatDateForApi(date)
      );
    } else {
      // otherwise startDate should always be +1 day (regardless of weekend/holidays)
      this.props.setFieldValue(
        'leavePeriod.startDate',
        date && formatDateForApi(date.add(1, 'd'))
      );
    }
  };

  handleStartDateFullDayChange = (checked: boolean) => {
    if (checked) {
      this.props.setFieldValue('leavePeriod.startDateOffHours', 0);
      this.props.setFieldValue('leavePeriod.startDateOffMinutes', 0);

      if ((this.props.values.leavePeriod as TimeOffLeavePeriod).lastDayWorked) {
        // if this is a "partial day", then set the start date to be equal to the last day worked
        this.props.setFieldValue(
          'leavePeriod.startDate',
          (this.props.values.leavePeriod as TimeOffLeavePeriod).lastDayWorked
        );
      }
    } else {
      this.props.setFieldValue('leavePeriod.startDateOffHours', undefined);
      this.props.setFieldValue('leavePeriod.startDateOffMinutes', undefined);

      if ((this.props.values.leavePeriod as TimeOffLeavePeriod).lastDayWorked) {
        // if we have a date for lastDatWorked, then set start date to be "the next day"
        this.props.setFieldValue(
          'leavePeriod.startDate',
          formatDateForApi(
            parseDate(
              (this.props.values.leavePeriod as TimeOffLeavePeriod)
                .lastDayWorked
            ).add(1, 'd')
          )
        );
      }
    }

    this.setState({
      showStartTime: checked
    });
  };

  handleEndDateFullDayChange = (checked: boolean) => {
    if (!checked) {
      this.props.setFieldValue('leavePeriod.endDateOffHours', 0);
      this.props.setFieldValue('leavePeriod.endDateOffMinutes', 0);
    } else {
      this.props.setFieldValue('leavePeriod.endDateOffHours', undefined);
      this.props.setFieldValue('leavePeriod.endDateOffMinutes', undefined);
      this.props.setFieldValue('leavePeriod.returnToWorkFullDate', undefined);
    }

    this.setState({
      showEndTime: !checked
    });
  };

  mapLabelWithTense = (translationId: string) => {
    return `${translationId}_${
      this.props.isOutOfWork ? 'OUT_OF_WORK' : 'AT_WORK'
    }`;
  };

  mapEndTimeLabelWithTense = (translationId: string) => {
    if (this.props.values.type === TimeOffPeriodType.TIME_OFF) {
      const expectedDate = (this.props.values.leavePeriod as TimeOffLeavePeriod)
        .expectedReturnToWorkDate;
      const suffix =
        !!expectedDate && parseDate(expectedDate).isAfter()
          ? 'AT_WORK'
          : 'OUT_OF_WORK';
      return `${translationId}_${suffix}`;
    } else {
      return this.mapLabelWithTense(translationId);
    }
  };

  getPeriodLabel = (type: TimeOffPeriodType, date: 'start' | 'end') => {
    switch (type) {
      case TimeOffPeriodType.REDUCED_SCHEDULE:
        return date === 'start'
          ? 'INTAKE.DETAILS.LABELS.FIRST_DAY_REDUCED_SCHEDULE'
          : 'INTAKE.DETAILS.LABELS.LAST_DAY_REDUCED_SCHEDULE';
      case TimeOffPeriodType.EPISODIC:
        return date === 'start'
          ? 'INTAKE.DETAILS.LABELS.FIRST_DAY_EPISODIC'
          : 'INTAKE.DETAILS.LABELS.LAST_DAY_EPISODIC';
      default:
        return date === 'start'
          ? this.mapLabelWithTense('INTAKE.DETAILS.LABELS.FIRST_DAY_OF_ABSENCE')
          : this.mapLabelWithTense('INTAKE.DETAILS.LABELS.LAST_DAY_OF_ABSENCE');
    }
  };

  handleReturnToWorkFullDateChange = (
    date: Moment | null,
    dateString: string
  ) => {
    this.props.setFieldValue(
      'leavePeriod.endDate',
      (this.props.values.leavePeriod as TimeOffLeavePeriod)
        .expectedReturnToWorkDate
    );
  };

  render() {
    const { onCancel, handleSubmit, isEdit, disabledTypeOptions } = this.props;
    const { type, showStartTime, showEndTime } = this.state;

    return (
      <Form
        name="leave-period"
        className={styles.LeavePeriodForm}
        onSubmit={handleSubmit}
      >
        {!isEdit && (
          <div
            className={styles.formGroup}
            data-test-el="leave-period-form-group"
          >
            <Row gutter={32}>
              <Col>
                <FormikRadioGroupInput
                  data-test-el="leave-period-type-input"
                  label={
                    <FormLabel
                      required={true}
                      label="INTAKE.DETAILS.LABELS.CIRCUMSTANCES"
                    />
                  }
                  name="type"
                  radioOptions={typeOptions}
                  optionPrefix={'INTAKE.DETAILS.TIME_OFF.OPTIONS.'}
                  renderPopover={typeIndex =>
                    disabledTypeOptions &&
                    disabledTypeOptions.some(
                      opt => opt === typeOptions[typeIndex]
                    ) ? (
                      <TimeOffSelectionPopover
                        content={`${typeOptions[typeIndex]}_DISABLED`}
                      />
                    ) : (
                      <TimeOffSelectionPopover
                        content={typeOptions[typeIndex]}
                      />
                    )
                  }
                  disabledOptions={disabledTypeOptions}
                  onChange={this.handleTypeChange}
                  {...this.props}
                />
              </Col>
            </Row>
          </div>
        )}

        {type === TimeOffPeriodType.TIME_OFF && (
          <>
            <div
              className={styles.formGroup}
              data-test-el="leave-period-form-group"
            >
              <Row gutter={32} type="flex" align="bottom">
                <Col xs={24} sm={14}>
                  <DatePickerInput
                    {...this.props}
                    labelCol={{ xs: 24, sm: 24 }}
                    wrapperCol={{ xs: 24, sm: 21 }}
                    name="leavePeriod.lastDayWorked"
                    label={
                      <FormLabel
                        required={true}
                        label={this.mapLabelWithTense(
                          'INTAKE.DETAILS.LABELS.LAST_DAY_WORKED'
                        )}
                      />
                    }
                    onChange={this.handleLastDayChanged}
                  />
                </Col>
                <Col xs={24} sm={10}>
                  <CheckboxInput
                    {...this.props}
                    name="leavePeriod.startDateFullDay"
                    label={
                      <FormattedMessage
                        id={this.mapLabelWithTense(
                          'INTAKE.DETAILS.LABELS.PARTIAL_DAY'
                        )}
                      />
                    }
                    value={undefined}
                    onChange={this.handleStartDateFullDayChange}
                  />
                </Col>
              </Row>
            </div>

            {showStartTime && (
              <div
                className={styles.formGroup}
                data-test-el="leave-period-form-group"
              >
                <Row gutter={32} type="flex" align="top">
                  <Col xs={24}>
                    <FormLabel
                      required={true}
                      label={this.mapLabelWithTense(
                        'INTAKE.DETAILS.LABELS.HOW_MUCH_TIME_ABSENT'
                      )}
                    />
                  </Col>
                  <Col xs={6} md={4}>
                    <FormikHoursInput
                      {...this.props}
                      name="leavePeriod.startDateOffHours"
                      label={
                        <FormattedMessage id="INTAKE.DETAILS.LABELS.HOURS" />
                      }
                      validate={val =>
                        validateAdditionalFields(
                          'startDateOffHours',
                          val,
                          this.props.values
                        )
                      }
                    />
                  </Col>
                  <Col xs={6} md={4}>
                    <FormikMinutesInput
                      {...this.props}
                      name="leavePeriod.startDateOffMinutes"
                      label={
                        <FormattedMessage id="INTAKE.DETAILS.LABELS.MINUTES" />
                      }
                      validate={val =>
                        validateAdditionalFields(
                          'startDateOffMinutes',
                          val,
                          this.props.values
                        )
                      }
                    />
                  </Col>
                </Row>
              </div>
            )}
          </>
        )}

        <div
          className={styles.formGroup}
          data-test-el="leave-period-form-group"
        >
          <Row gutter={32}>
            <Col>
              <DatePickerInput
                {...this.props}
                labelCol={{ xs: 48, sm: 24 }}
                wrapperCol={{ xs: 24, sm: 12 }}
                name="leavePeriod.startDate"
                isDisabled={showStartTime}
                label={
                  <FormLabel
                    required={true}
                    label={this.getPeriodLabel(type, 'start')}
                  />
                }
              />
            </Col>
          </Row>
        </div>

        <div
          className={styles.formGroup}
          data-test-el="leave-period-form-group"
        >
          <Row gutter={32}>
            <Col>
              <DatePickerInput
                {...this.props}
                labelCol={{ xs: 48, sm: 24 }}
                wrapperCol={{ xs: 24, sm: 12 }}
                name="leavePeriod.endDate"
                label={
                  <FormLabel
                    required={type !== TimeOffPeriodType.EPISODIC}
                    label={this.getPeriodLabel(type, 'end')}
                  />
                }
              />
            </Col>
          </Row>
        </div>

        {type === TimeOffPeriodType.TIME_OFF && (
          <>
            <div
              className={styles.formGroup}
              data-test-el="leave-period-form-group"
            >
              <Row gutter={32}>
                <Col>
                  <DatePickerInput
                    {...this.props}
                    labelCol={{ xs: 48, sm: 24 }}
                    wrapperCol={{ xs: 24, sm: 12 }}
                    name="leavePeriod.expectedReturnToWorkDate"
                    label={
                      <FormLabel
                        required={true}
                        label="INTAKE.DETAILS.LABELS.RETURN_TO_WORK"
                      />
                    }
                  />
                </Col>
              </Row>
            </div>
            <div
              className={styles.formGroup}
              data-test-el="leave-period-form-group"
            >
              <Row gutter={32}>
                <Col>
                  <BooleanInput
                    {...this.props}
                    name="leavePeriod.endDateFullDay"
                    label={
                      <FormLabel
                        required={true}
                        label="INTAKE.DETAILS.LABELS.NORMAL_HOURS"
                      />
                    }
                    onChange={this.handleEndDateFullDayChange}
                  />
                </Col>
              </Row>
            </div>

            {showEndTime && (
              <>
                <div
                  className={styles.formGroup}
                  data-test-el="leave-period-form-group"
                >
                  <Row gutter={32} type="flex" align="top">
                    <Col xs={24}>
                      <FormLabel
                        required={true}
                        label={this.mapEndTimeLabelWithTense(
                          'INTAKE.DETAILS.LABELS.HOW_MUCH_TIME_ABSENT'
                        )}
                      />
                    </Col>
                    <Col xs={6} md={4}>
                      <FormikHoursInput
                        {...this.props}
                        name="leavePeriod.endDateOffHours"
                        label={
                          <FormattedMessage id="INTAKE.DETAILS.LABELS.HOURS" />
                        }
                        validate={val =>
                          validateAdditionalFields(
                            'endDateOffHours',
                            val,
                            this.props.values
                          )
                        }
                      />
                    </Col>
                    <Col xs={6} md={4}>
                      <FormikMinutesInput
                        {...this.props}
                        name="leavePeriod.endDateOffMinutes"
                        label={
                          <FormattedMessage id="INTAKE.DETAILS.LABELS.MINUTES" />
                        }
                        validate={val =>
                          validateAdditionalFields(
                            'endDateOffMinutes',
                            val,
                            this.props.values
                          )
                        }
                      />
                    </Col>
                  </Row>
                </div>
                <div
                  className={styles.formGroup}
                  data-test-el="leave-period-form-group"
                >
                  <Row gutter={32}>
                    <Col xs={24}>
                      <FormikDatePickerInput
                        {...this.props}
                        labelCol={{ xs: 48, sm: 24 }}
                        wrapperCol={{ xs: 24, sm: 12 }}
                        name="leavePeriod.returnToWorkFullDate"
                        label={
                          <FormLabel
                            required={true}
                            label="INTAKE.DETAILS.LABELS.RETURN_TO_WORK_FULLTIME"
                          />
                        }
                        validate={val =>
                          validateAdditionalFields(
                            'returnToWorkFullDate',
                            val,
                            this.props.values
                          )
                        }
                        onChange={this.handleReturnToWorkFullDateChange}
                      />
                    </Col>
                  </Row>
                </div>
              </>
            )}
          </>
        )}

        {[
          TimeOffPeriodType.TIME_OFF,
          TimeOffPeriodType.REDUCED_SCHEDULE
        ].includes(type) && (
          <div
            className={styles.formGroup}
            data-test-el="leave-period-form-group"
          >
            <Row gutter={32}>
              <Col>
                <FormikRadioGroupInput
                  label={
                    <FormLabel
                      required={true}
                      label="INTAKE.DETAILS.LABELS.DATES_CONFIRMED"
                    />
                  }
                  name="leavePeriod.status"
                  radioOptions={['KNOWN', 'ESTIMATED']}
                  optionPrefix={'INTAKE.DETAILS.TIME_OFF.OPTIONS.'}
                  {...this.props}
                />
              </Col>
            </Row>
          </div>
        )}

        <div className={styles.controls}>
          <IntakeStepControls
            {...this.props}
            submitLabel="INTAKE.CONTROLS.OK"
            isDisabled={!this.props.isValid}
            showCancel={true}
            cancel={onCancel}
          />
        </div>
      </Form>
    );
  }
}
