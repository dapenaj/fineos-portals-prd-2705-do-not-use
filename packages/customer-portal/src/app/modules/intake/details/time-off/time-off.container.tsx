import React from 'react';
import { connect } from 'react-redux';
import { CustomerOccupation } from 'fineos-js-api-client';

import {
  AuthorizationService,
  IntakeSubmission,
  IntakeSubmissionService,
  IntakeSubmissionState
} from '../../../../shared/services';
import {
  EventOptionId,
  IntakeSectionId,
  IntakeSectionStep,
  IntakeSectionStepId,
  RequestDetailFieldId
} from '../../../../shared/types';
import { RootState } from '../../../../store';
import { submitIntakeWorkflowStep } from '../../../../store/intake/workflow/actions';
import { PersonalDetailsValue, YourRequestValue } from '../../your-request';
import {
  buildPersonalDetailsForm,
  buildTimeOffFromFormValue,
  buildWorkDetailsForm
} from '../../intake.util';
import { RequestDetailsValue, TimeOffValue } from '../details.type';

import { TimeOffFormValue } from './form-value.type';
import { TimeOffView } from './time-off.view';

const mapStateToProps = ({
  intake: {
    workflow: { sections, currentSection }
  }
}: RootState) => ({
  sections,
  currentSection
});

const mapDispatchToProps = {
  submitStep: submitIntakeWorkflowStep
};

type DispatchProps = typeof mapDispatchToProps;
type StateProps = ReturnType<typeof mapStateToProps>;

type TimeOffProps = {
  step: IntakeSectionStep<TimeOffValue>;
} & DispatchProps &
  StateProps;

type TimeOffState = {
  submitting: boolean;
};

export class TimeOffContainer extends React.Component<
  TimeOffProps,
  TimeOffState
> {
  state: TimeOffState = {
    submitting: false
  };

  intakeSubmissionService = IntakeSubmissionService.getInstance();
  authorizationService = AuthorizationService.getInstance();

  handleStepSubmit = async (formValue: TimeOffFormValue) => {
    const { step, submitStep, sections, currentSection } = this.props;

    // get the event details list
    const yourRequest = sections.find(
      ({ id }) => id === IntakeSectionId.YOUR_REQUEST
    );
    const wrapUp = sections.find(({ id }) => id === IntakeSectionId.WRAP_UP);
    const additionalData =
      wrapUp &&
      wrapUp.steps.find(
        ({ id }) => id === IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES
      );

    const yourRequestValue =
      yourRequest && (yourRequest.value as YourRequestValue);
    const options = yourRequestValue && yourRequestValue.initialRequest;
    const workDetails = yourRequestValue && yourRequestValue.workDetails;
    const occupation =
      workDetails && workDetails.occupation !== null
        ? workDetails.occupation
        : undefined;
    const personalDetails =
      yourRequestValue && yourRequestValue.personalDetails;

    // get the form field values
    // @TODO  - we need to change this to store it as a typed map, instead of an array
    // of unstructured id/value pairs
    const details = currentSection.steps.find(
      s => s.id === IntakeSectionStepId.REQUEST_DETAILS
    );
    const detailsValue: RequestDetailsValue = details ? details.value : {};
    const fields: Map<RequestDetailFieldId, any> = new Map();
    if (detailsValue) {
      Object.keys(detailsValue).forEach(entry => {
        fields.set(entry as RequestDetailFieldId, detailsValue[entry]);
      });
    }

    // submit the action
    try {
      this.setState({ submitting: true });

      const submissionState = await this.submitIntake(
        formValue,
        fields,
        options,
        occupation,
        personalDetails
      );
      const isClaimCreated =
        submissionState.result.createdClaim &&
        submissionState.result.createdClaim.id;
      const isAbsenceCreated =
        submissionState.result.createdAbsence &&
        submissionState.result.createdAbsence.id;

      if (
        !isClaimCreated ||
        (!isAbsenceCreated && this.authorizationService.isAbsenceUser) ||
        !additionalData ||
        additionalData.skip
      ) {
        await this.intakeSubmissionService.completeIntake(
          submissionState.model as IntakeSubmission,
          submissionState.result,
          {}
        );
      }

      submitStep({
        ...step,
        value: { formValue, submissionState },
        ...(!isClaimCreated
          ? {
              skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
            }
          : {})
      });
    } catch (error) {
      submitStep({
        ...step,
        value: { formValue, submissionState: { result: error } }
      });
    } finally {
      this.setState({ submitting: false });
    }
  };

  submitIntake = async (
    formValue: TimeOffFormValue,
    fields: Map<RequestDetailFieldId, any>,
    options?: EventOptionId[],
    customerOccupation?: CustomerOccupation,
    personalDetails?: PersonalDetailsValue
  ): Promise<IntakeSubmissionState> => {
    if (options) {
      const model = new IntakeSubmission(
        options,
        fields,
        buildTimeOffFromFormValue(formValue),
        buildWorkDetailsForm(customerOccupation),
        buildPersonalDetailsForm(personalDetails)
      );

      const result = await this.intakeSubmissionService.process(model);
      const state: IntakeSubmissionState = {
        model: model,
        result: result
      };

      return state;
    }

    return Promise.reject({
      generalError: 'Cannot create an intake without any event options'
    });
  };

  render() {
    const { step } = this.props;
    const { submitting } = this.state;

    const isActive = step.isActive && !submitting;

    return (
      <TimeOffView
        isAbsenceUser={this.authorizationService.isAbsenceUser}
        isActive={isActive}
        stepSubmit={this.handleStepSubmit}
        submitting={submitting}
      />
    );
  }
}

export const TimeOff = connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeOffContainer);
