import { withFormik } from 'formik';

import { TimeOffFormValue } from '../form-value.type';

import { TimeOffSchema } from './time-off-validation';
import { TimeOffFormView } from './time-off-form.view';

export type TimeOffProps = {
  isActive: boolean;
  submitStep: (values: TimeOffFormValue) => void;
  submitting: boolean;
};

export const TimeOffForm = withFormik<TimeOffProps, TimeOffFormValue>({
  mapPropsToValues: () => ({
    outOfWork: true,
    leavePeriods: []
  }),
  validationSchema: TimeOffSchema,
  handleSubmit: (values, { props }) => props.submitStep(values)
})(TimeOffFormView);
