import {
  TimeOffPeriodType,
  TimeOffLeavePeriod,
  ReducedScheduleLeavePeriod,
  EpisodicLeavePeriod
} from './../../../../shared/types';

export type LeavePeriod = {
  type: TimeOffPeriodType;
  leavePeriod:
    | TimeOffLeavePeriod
    | ReducedScheduleLeavePeriod
    | EpisodicLeavePeriod;
};

export type TimeOffFormValue = {
  outOfWork: boolean;
  leavePeriods: LeavePeriod[];
};
