import React from 'react';

import { Popover } from '../../../../../../shared/ui';
import { getMessageForLocale } from '../../../../../../i18n/utils';

export type TimeOffSelectionPopoverProps = {
  content: string;
};

export const TimeOffSelectionPopover: React.FC<TimeOffSelectionPopoverProps> = ({
  content
}) => (
  <>
    {getMessageForLocale('en', `POPOVER.${content}`) && (
      <Popover content={content} />
    )}
  </>
);
