import { withFormik } from 'formik';

import {
  TimeOffLeavePeriod,
  TimeOffPeriodType
} from '../../../../../shared/types';
import { TimeOffFormValue } from '../form-value.type';

import { TimeOffClaimsFormView } from './time-off-claims-form.view';
import { TimeOffClaimsSchema } from './time-off-claims-validation';

export type TimeOffClaimsProps = {
  isActive: boolean;
  submitStep: (values: TimeOffFormValue) => void;
  submitting: boolean;
};

export const TimeOffClaimsForm = withFormik<
  TimeOffClaimsProps,
  TimeOffFormValue
>({
  mapPropsToValues: () => ({
    outOfWork: true,
    // For a Claims user, they will only be allowed to create a single Time-Off period
    leavePeriods: [
      {
        type: TimeOffPeriodType.TIME_OFF,
        leavePeriod: {} as TimeOffLeavePeriod
      }
    ]
  }),
  validationSchema: TimeOffClaimsSchema,
  handleSubmit: (values, { props }) => props.submitStep(values)
})(TimeOffClaimsFormView);
