import {
  NotificationStatus,
  NotificationPendingStatus,
  NotificationDecidedStatus,
  NotificationClosedStatus
} from '../types';

export const NOTIFICATION_PENDING_STATUSES: NotificationStatus[] = [
  NotificationPendingStatus.UNKNOWN,
  NotificationPendingStatus.REGISTRATION,
  NotificationPendingStatus.ADJUDICATION,
  NotificationPendingStatus.EXTEND,
  NotificationPendingStatus.ASSESSMENT,
  NotificationPendingStatus.MONITORING,
  NotificationPendingStatus.PENDING,
  NotificationPendingStatus.NOTIFICATION_INCOMPLETE,
  NotificationPendingStatus.PENDING_CLAIM,
  NotificationPendingStatus.NOTIFICATION,
  NotificationPendingStatus.INCOMPLETE
];

export const NOTIFICATION_DECIDED_STATUSES: NotificationStatus[] = [
  NotificationDecidedStatus.DECIDED,
  NotificationDecidedStatus.COMPLETION,
  NotificationDecidedStatus.MANAGED_LEAVE
];

export const NOTIFICATION_CLOSED_STATUSES: NotificationStatus[] = [
  NotificationClosedStatus.CLOSED
];

export const MAX_RECENT_MESSAGES = 4;
