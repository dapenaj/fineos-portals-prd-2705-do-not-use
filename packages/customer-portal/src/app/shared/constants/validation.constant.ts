export const EMAIL_REGEX = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
export const PHONE_REGEX = /^\d{1,3}-\d{1,3}-\d{4,7}$/;
export const MONEY_FORMAT_REGEX = /^\d+(?:\.\d{1,2})?$/;
export const ALPHA_REGEX = /\W/;
export const NUMERIC_REGEX = /^[0-9]*$/;
