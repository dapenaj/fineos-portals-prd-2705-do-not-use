export const CLAIM_CASE_TYPE = 'Group Disability Claim';

export const MEDICAL_PROVIDER_MATCH = 'Yes - Health Care Provider';

export const DISABILITY_REQUEST_SOURCE = 'Customer Portal';
