import { ClientConfig, ConfirmationType } from '../types';

export const DEFAULT_CLIENT_CONFIG: ClientConfig = {
  notification: {
    detail: {
      emptyState: {
        jobProtectedLeave: {
          empty: '',
          pending: '',
          concluded: {
            title: '',
            subtitle: ''
          }
        },
        wageReplacement: {
          pending: ''
        }
      }
    },
    returnConfirm: {
      expectedDateConfirmation1: '',
      expectedDateConfirmation2: '',
      anotherDateConfirmation1: '',
      anotherDateConfirmation2: '',
      anotherDateConfirmation3: ''
    },
    inProgressStatus: {
      message: ''
    },
    requestConfirmation: {
      requestAmendmentMessage: '',
      requestDeletionMessage: '',
      supervisorAmendmentMessage: '',
      supervisorDeletionMessage: ''
    }
  },
  intake: {
    wrapUp: [
      {
        type: '' as ConfirmationType,
        header: '',
        phoneNumber: '',
        successNotification: '',
        notificationReferenceMessageHeader: '',
        notificationReferenceMessageFooter: '',
        privacyNotice: '',
        questionMessage: ''
      },
      {
        type: '' as ConfirmationType,
        header: '',
        phoneNumber: '',
        numberMessage: '',
        availableDays: '',
        tip: '',
        notificationReferenceMessageHeader: '',
        notificationReferenceMessageFooter: ''
      },
      {
        type: '' as ConfirmationType,
        header: '',
        phoneNumber: '',
        numberMessage: '',
        availableDays: ''
      }
    ]
  },
  header: {
    branding: {
      message: ''
    }
  },
  home: {
    welcome: {
      message: ''
    },
    action: {
      title: '',
      message: ''
    }
  },
  myProfile: {
    message: ''
  },
  myTeamLeaves: {
    notificationsPageSize: '',
    employeesPageSize: ''
  },
  documentUpload: {
    errorTitle: '',
    uploadError: '',
    receivedError: ''
  }
};
