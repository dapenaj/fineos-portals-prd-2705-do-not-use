import { OptionGroup } from '../../types';

export const LIMITATIONS: OptionGroup[] = [
  {
    group: 'LIMITATIONS.BODILY_FUNCTION',
    options: [
      {
        text: 'LIMITATIONS.BODY_ODOR',
        value: 'Body Odor'
      },
      {
        text: 'LIMITATIONS.COMMUNICABILITY_CONTAGIOUS',
        value: 'Communicability/Contagious'
      },
      {
        text: 'LIMITATIONS.COUGHING_EXCESSIVELY',
        value: 'Coughing Excessively'
      },
      {
        text: 'LIMITATIONS.DIETARY_NEEDS',
        value: 'Dietary Needs'
      },
      {
        text: 'LIMITATIONS.DROOLING',
        value: 'Drooling'
      },
      {
        text: 'LIMITATIONS.HEADACHE',
        value: 'Headache'
      },
      {
        text: 'LIMITATIONS.NAUSEA',
        value: 'Nausea'
      },
      {
        text: 'LIMITATIONS.PAIN',
        value: 'Pain'
      },
      {
        text: 'LIMITATIONS.SLEEPING_STAY_AWAKE',
        value: 'Sleeping/Stay Awake'
      },
      {
        text: 'LIMITATIONS.RESPIRATORY_DISTRESS_BREATHING_PROBLEM',
        value: 'Respiratory Distress/Breathing Problem'
      },
      {
        text: 'LIMITATIONS.SKIN_RASH_BLISTERS_SORES',
        value: 'Skin Rash/Blisters/Sores'
      },
      {
        text: 'LIMITATIONS.SUPPRESSED_IMMUNE_SYSTEM',
        value: 'Suppressed Immune System'
      },
      {
        text: 'LIMITATIONS.TEMPERATURE_SENSITIVITY',
        value: 'Temperature Sensitivity'
      },
      {
        text: 'LIMITATIONS.TOILETING_GROOMING_ISSUE',
        value: 'Toileting/Grooming Issue'
      }
    ]
  },
  {
    group: 'LIMITATIONS.COGNITION',
    options: [
      {
        text: 'LIMITATIONS.GENERAL_COGNITION',
        value: 'General Cognition'
      },
      {
        text: 'LIMITATIONS.ATTENTIVENESS_CONCENTRATION',
        value: 'Attentiveness/Concentration'
      },
      { text: 'LIMITATIONS.LEARNING', value: 'Learning' },
      { text: 'LIMITATIONS.MEMORY_LOSS', value: 'Memory Loss' },
      { text: 'LIMITATIONS.MENTAL_CONFUSION', value: 'Mental Confusion' },
      {
        text: 'LIMITATIONS.ORGANIZING_PLANNING_PRIORITIZING',
        value: 'Organizing/Planning/Prioritizing'
      },
      {
        text: 'LIMITATIONS.ORAL_VERBAL_LANGUAGE_SPEAKING',
        value: 'Oral/Verbal Language/Speaking'
      },
      { text: 'LIMITATIONS.SEIZURE_ACTIVITY', value: 'Seizure Activity' },
      {
        text: 'LIMITATIONS.TASK_SPECIFIC_LEARNING',
        value: 'Task Specific - Learning'
      },
      { text: 'LIMITATIONS.MATHEMATICS', value: 'Mathematics' },
      { text: 'LIMITATIONS.READING', value: 'Reading' },
      { text: 'LIMITATIONS.MANAGING_TIME', value: 'Managing Time' },
      { text: 'LIMITATIONS.WRITING_SPELLING', value: 'Writing/Spelling' },
      {
        text: 'LIMITATIONS.INFORMATION_PROCESSING',
        value: 'Information Processing'
      },
      { text: 'LIMITATIONS.VISUAL_PROCESSING', value: 'Visual Processing' },
      {
        text: 'LIMITATIONS.VISUAL_DISCRIMINATION',
        value: 'Visual Discrimination'
      },
      { text: 'LIMITATIONS.VISUAL_SEQUENCING', value: 'Visual Sequencing' },
      { text: 'LIMITATIONS.VISUAL_MEMORY', value: 'Visual Memory' },
      {
        text: 'LIMITATIONS.VISUAL_MOTOR_PROCESSING',
        value: 'Visual Motor Processing'
      },
      { text: 'LIMITATIONS.VISUAL_CLOSURE', value: 'Visual Closure' },
      {
        text: 'LIMITATIONS.SPATIAL_RELATIONSHIPS',
        value: 'Spatial Relationships'
      },
      { text: 'LIMITATIONS.AUDITORY_PROCESSING', value: 'Auditory Processing' },
      {
        text: 'LIMITATIONS.AUDITORY_DISCRIMINATION',
        value: 'Auditory Discrimination'
      },
      { text: 'LIMITATIONS.AUDITORY_SEQUENCING', value: 'Auditory Sequencing' },
      {
        text: 'LIMITATIONS.EXECUTIVE_FUNCTIONING_DEFICITS',
        value: 'Executive Functioning Deficit'
      }
    ]
  },
  {
    group: 'LIMITATIONS.MEDICAL_TREATMENT_DEVICE',
    options: [
      {
        text: 'LIMITATIONS.USE_OF_HEARING_DEVICES',
        value: 'Use of Hearing Devices'
      },
      {
        text: 'LIMITATIONS.USE_OF_MOBILITY_AIDS',
        value: 'Use of Mobility Aids'
      },
      { text: 'LIMITATIONS.TAKE_MEDICATION', value: 'Take Medication' },
      {
        text: 'LIMITATIONS.EFFECT_OF_RECEIVE_MEDICAL_TREATMENT',
        value: 'Effect of/Receive Medical Treatment'
      }
    ]
  },
  {
    group: 'LIMITATIONS.MOTOR_PHYSICAL',
    options: [
      {
        text: 'LIMITATIONS.VISUAL_MOTOR_PROCESSING',
        value: 'Visual Motor Processing'
      },
      { text: 'LIMITATIONS.GROSS_MOTOR', value: 'Gross Motor' },
      { text: 'LIMITATIONS.BALANCING', value: 'Balancing' },
      { text: 'LIMITATIONS.BENDING', value: 'Bending' },
      { text: 'LIMITATIONS.CARRYING', value: 'Carrying' },
      { text: 'LIMITATIONS.CLIMBING', value: 'Climbing' },
      { text: 'LIMITATIONS.KNEELING', value: 'Kneeling' },
      { text: 'LIMITATIONS.LIFTING', value: 'Lifting' },
      {
        text: 'LIMITATIONS.OPERATING_FOOT_CONTROL',
        value: 'Operating Foot Control'
      },
      { text: 'LIMITATIONS.PUSHING_PULLING', value: 'Pushing/Pulling' },
      { text: 'LIMITATIONS.REACHING', value: 'Reaching' },
      { text: 'LIMITATIONS.SITTING', value: 'Sitting' },
      { text: 'LIMITATIONS.SQUATTING', value: 'Squatting' },
      { text: 'LIMITATIONS.STANDING', value: 'Standing' },
      { text: 'LIMITATIONS.WALKING', value: 'Walking' },
      { text: 'LIMITATIONS.FINE_MOTOR', value: 'Fine Motor' },
      { text: 'LIMITATIONS.FEELING_SENSING', value: 'Feeling/Sensing' },
      { text: 'LIMITATIONS.GRASPING', value: 'Grasping' },
      { text: 'LIMITATIONS.HANDLING_FINGERING', value: 'Handling/Fingering' },
      { text: 'LIMITATIONS.OTHER_MOTOR', value: 'Other Motor' },
      { text: 'LIMITATIONS.BODY_SIZE', value: 'Body Size' },
      {
        text: 'LIMITATIONS.DECREASED_STAMINA_FATIGUE',
        value: 'Decreased Stamina/Fatigue'
      },
      {
        text: 'LIMITATIONS.OVERALL_BODY_COORDINATION',
        value: 'Overall Body Coordination'
      },
      {
        text: 'LIMITATIONS.OVERALL_BODY_WEAKNESS_STRENGTH',
        value: 'Overall Body Weakness/Strength'
      },
      {
        text: 'LIMITATIONS.SLOW_MOVEMENT_REACTION_TIME',
        value: 'Slow Movement/Reaction time'
      },
      {
        text: 'LIMITATIONS.SPASM_TIC_TREMOR_BLINKING',
        value: 'Spasm/Tic/Tremor/Blinking'
      },
      { text: 'LIMITATIONS.USE_OF_ONE_HAND_ARM', value: 'Use of One Hand/Arm' },
      {
        text: 'LIMITATIONS.USE_OF_ONE_SIDE',
        value: 'Use of One Side/Full Body'
      }
    ]
  },
  {
    group: 'LIMITATIONS.PSYCHOLOGICAL',
    options: [
      {
        text: 'LIMITATIONS.GENERAL_PSYCHOLOGICAL',
        value: 'General Psychological'
      },
      { text: 'LIMITATIONS.STRESS_INTOLERANCE', value: 'Stress Intolerance' },
      {
        text: 'LIMITATIONS.UNABLE_TO_WORK_ALONE',
        value: 'Unable to Work Alone'
      },
      { text: 'LIMITATIONS.BEHAVIOR', value: 'Behaviour' },
      {
        text: 'LIMITATIONS.CONTROL_OF_ANGER_EMOTIONS',
        value: 'Control of Anger/Emotions'
      },
      {
        text: 'LIMITATIONS.DISRUPTIVE_BEHAVIOR',
        value: 'Disruptive Behaviour'
      },
      {
        text: 'LIMITATIONS.ERRATIC_INCONSISTENT_BEHAVIOR',
        value: 'Erratic/Inconsistent Behaviour'
      },
      {
        text: 'LIMITATIONS.NON_COMPLIANT_BEHAVIOR',
        value: 'Non-compliantBehaviour'
      }
    ]
  },
  {
    group: 'LIMITATIONS.SENSORY',
    options: [
      { text: 'LIMITATIONS.VISION_IMPAIRMENT', value: 'Vision Impairment' },
      { text: 'LIMITATIONS.BLIND', value: 'Blind' },
      { text: 'LIMITATIONS.BLIND_TOTAL', value: 'Blind - Total' },
      { text: 'LIMITATIONS.BLIND_ONE_EYE', value: 'Blind - One Eye' },
      { text: 'LIMITATIONS.VISION_LOSS', value: 'Vision Loss' },
      { text: 'LIMITATIONS.LOW_VISION', value: 'Low Vision' },
      {
        text: 'LIMITATIONS.PROGRESSIVE_VISION_LOSS',
        value: 'Progressive Vision Loss'
      },
      {
        text: 'LIMITATIONS.LIMITED_VISUAL_FIELD',
        value: 'Limited Visual Field'
      },
      {
        text: 'LIMITATIONS.COLOR_VISION_DEFICIENCY',
        value: 'Color Vision Deficiency (Color Blindness)'
      },
      { text: 'LIMITATIONS.NIGHT_BLINDNESS', value: 'Night Blindness' },
      { text: 'LIMITATIONS.PHOTOSENSITIVITY', value: 'Photosensitivity' },
      { text: 'LIMITATIONS.HEARING_IMPAIRMENT', value: 'Hearing Impairment' },
      { text: 'LIMITATIONS.DEAF', value: 'Deaf' },
      { text: 'LIMITATIONS.DEAF_TOTAL', value: 'Deaf - Total' },
      { text: 'LIMITATIONS.DEAF_ONE_EAR', value: 'Deaf - One Ear' },
      { text: 'LIMITATIONS.HEARING_LOSS', value: 'Hearing Loss' },
      { text: 'LIMITATIONS.HARD_OF_HEARING', value: 'Hard of Hearing' },
      {
        text: 'LIMITATIONS.PROGRESSIVE_HEARING_LOSS',
        value: 'Progressive Hearing Loss'
      },
      {
        text: 'LIMITATIONS.FLUCTUATING_HEARING_LOSS',
        value: 'Fluctuating Hearing Loss'
      },
      { text: 'LIMITATIONS.NOISE_SENSITIVITY', value: 'Noise Sensitivity' },
      { text: 'LIMITATIONS.RINGING_IN_THE_EARS', value: 'Ringing in the Ears' },
      { text: 'LIMITATIONS.SPEECH_IMPAIRMENT', value: 'Speech Impairment' },
      { text: 'LIMITATIONS.NO_SPEECH', value: 'No Speech' },
      { text: 'LIMITATIONS.WEAK_SPEECH', value: 'Weak Speech' },
      {
        text: 'LIMITATIONS.UNINTELLIGIBLE_SPEECH',
        value: 'Unintelligible Speech'
      },
      { text: 'LIMITATIONS.SPEECH_DISFLUENCIES', value: 'Speech Disfluencies' },
      {
        text: 'LIMITATIONS.STUTTERING_SPEECH_DISFLUENCY',
        value: 'Stuttering Speech Disfluency'
      },
      {
        text: 'LIMITATIONS.NON_STUTTERING_SPEECH_DISFLUENCY',
        value: 'Non-Stuttering Speech Disfluency'
      }
    ]
  }
];
