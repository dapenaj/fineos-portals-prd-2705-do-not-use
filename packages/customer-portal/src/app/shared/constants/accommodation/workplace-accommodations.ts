import {
  SelectOption,
  WorkplaceAccommodationCategory,
  WorkplaceAccommodationTypeOption
} from '../../types';

export const WORKPLACE_ACCOMMODATION_CATEGORIES: SelectOption[] = [
  {
    text: WorkplaceAccommodationCategory.PHYSICAL_WORKPLACE_MODIFICATIONS,
    value: 'Physical workplace modifications or workstation relocation'
  },
  {
    text: WorkplaceAccommodationCategory.POLICY_MODIFICATION,
    value: 'Policy modification'
  },
  {
    text: WorkplaceAccommodationCategory.JOB_RESTRUCTURING,
    value: 'Job restructuring'
  },
  {
    text: WorkplaceAccommodationCategory.FREQUENT_BREAKS,
    value: 'Frequent breaks'
  },
  {
    text: WorkplaceAccommodationCategory.FREQUENT_CHANGE_IN_WORKING_POSITION,
    value: 'Frequent change in working position'
  },
  {
    text: WorkplaceAccommodationCategory.TELEWORK_WORK_FROM_HOME,
    value: 'Telework/work from home'
  },
  {
    text: WorkplaceAccommodationCategory.REASSIGNMENT,
    value: 'Reassignment'
  },
  {
    text: WorkplaceAccommodationCategory.FLEXIBLE_SCHEDULE,
    value: 'Flexible schedule'
  },
  {
    text: WorkplaceAccommodationCategory.WORKSTATION_EQUIPMENT,
    value: 'Workstation equipment, software, or devices change or modification'
  },
  {
    text: WorkplaceAccommodationCategory.ASSISTIVE_EQUIPMENT_MINOR,
    value: 'Assistive equipment (minor)'
  },
  {
    text: WorkplaceAccommodationCategory.ASSISTIVE_EQUIPMENT_MAJOR,
    value: 'Assistive equipment (major)'
  },
  {
    text: WorkplaceAccommodationCategory.MOBILITY_AID,
    value: 'Mobility aid'
  },
  {
    text: WorkplaceAccommodationCategory.HEARING_ASSISTANCE,
    value: 'Hearing assistance'
  },
  {
    text: WorkplaceAccommodationCategory.SIGHT_ASSISTANCE,
    value: 'Sight assistance'
  },
  {
    text: WorkplaceAccommodationCategory.ADDITIONAL_TRAINING,
    value: 'Additional training'
  },
  {
    text: WorkplaceAccommodationCategory.PROVIDE_ADMINISTRATIVE_SUPPORT,
    value: 'Provide administrative support'
  },
  {
    text: WorkplaceAccommodationCategory.OTHER_ACCOMMODATION,
    value: 'Other Accommodation'
  }
];

export const WORKPLACE_ACCOMMODATION_TYPES: WorkplaceAccommodationTypeOption[] = [
  {
    category: 'Physical workplace modifications or workstation relocation',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.WIDEN_DOORWAY',
    value: 'Widen doorway'
  },
  {
    category: 'Physical workplace modifications or workstation relocation',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CHANGE_COUNTER_HEIGHT',
    value: 'Change counter height'
  },
  {
    category: 'Physical workplace modifications or workstation relocation',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MATERIALS_WITHIN_REACH',
    value: 'Make materials and equipment within reach'
  },
  {
    category: 'Physical workplace modifications or workstation relocation',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.WORKSTATION_RELOCATION',
    value: 'Workstation relocation'
  },
  {
    category: 'Policy modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.POLICY_MODIFICATION',
    value: 'Policy modification'
  },
  {
    category: 'Policy modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ATTENDANCE',
    value: 'Attendance'
  },
  {
    category: 'Policy modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.USE_OF_FLEXIBLE_LEAVE_POLICY',
    value: 'Use of flexible leave policy'
  },
  {
    category: 'Policy modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.DRESS_CODE',
    value: 'Dress code'
  },
  {
    category: 'Job restructuring',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MOVEMENT_RESTRICTIONS',
    value: 'Lifting/bending/twisting/push/pull restrictions'
  },
  {
    category: 'Job restructuring',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.JOB_SHIFT_SWAPPING_SHARING',
    value: 'Job/shift swapping or sharing'
  },
  {
    category: 'Job restructuring',
    text:
      'WORKPLACE_ACCOMMODATION_TYPES.ELIMINATION_OF_NON_ESSENTIAL_JOB_FUNCTIONS',
    value: 'Elimination of non-essential job functions'
  },
  {
    category: 'Job restructuring',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MODIFICATION_OF_JOB_FUNCTION',
    value: 'Modification of job function'
  },
  {
    category: 'Frequent breaks',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_15_MINUTES',
    value: 'Every 15 minutes'
  },
  {
    category: 'Frequent breaks',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_30_MINUTES',
    value: 'Every 30 minutes'
  },
  {
    category: 'Frequent breaks',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_HOUR',
    value: 'Every hour'
  },
  {
    category: 'Frequent breaks',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_2_HOURS',
    value: 'Every 2 hours'
  },
  {
    category: 'Frequent breaks',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_3_HOURS',
    value: 'Every 3 hours'
  },
  {
    category: 'Frequent breaks',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_4_HOURS',
    value: 'Every 4 hours'
  },

  {
    category: 'Frequent change in working position',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_15_MINUTES',
    value: 'Every 15 minutes'
  },
  {
    category: 'Frequent change in working position',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_30_MINUTES',
    value: 'Every 30 minutes'
  },
  {
    category: 'Frequent change in working position',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_HOUR',
    value: 'Every hour'
  },
  {
    category: 'Frequent change in working position',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_2_HOURS',
    value: 'Every 2 hours'
  },
  {
    category: 'Frequent change in working position',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_3_HOURS',
    value: 'Every 3 hours'
  },
  {
    category: 'Frequent change in working position',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_4_HOURS',
    value: 'Every 4 hours'
  },
  {
    category: 'Telework/work from home',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.EVERY_DAY',
    value: 'Every day'
  },
  {
    category: 'Telework/work from home',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.1_DAY_A_WEEK',
    value: '1 day a week'
  },
  {
    category: 'Telework/work from home',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.2_DAYS_A_WEEK',
    value: '2 days a week'
  },
  {
    category: 'Telework/work from home',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.3_DAYS_A_WEEK',
    value: '3 days a week'
  },
  {
    category: 'Telework/work from home',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.4_DAYS_A_WEEK',
    value: '4 days a week'
  },
  {
    category: 'Reassignment',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.VACANT_POSITION',
    value: 'Vacant Position'
  },
  {
    category: 'Reassignment',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.NEWLY_CREATED_POSITION',
    value: 'Newly-Created Position'
  },
  {
    category: 'Reassignment',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.DIFFERENT_WORKSTATION',
    value: 'Different Workstation'
  },
  {
    category: 'Reassignment',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.DIFFERENT_WORKSITE',
    value: 'Different Worksite'
  },
  {
    category: 'Flexible schedule',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.LATE_EARLY_START_END',
    value: 'Later or earlier start or end time'
  },
  {
    category: 'Flexible schedule',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.4_10_SCHEDULE',
    value: '4 x 10 schedule'
  },
  {
    category: 'Flexible schedule',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.SHORTENED_WORKDAY_EXTEND_WORKWEEK',
    value: 'Shortened workday and extend workweek'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.KEYBOARD',
    value: 'Keyboard'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.SIT_STAND_DESK',
    value: 'Sit/stand desk'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CHAIR',
    value: 'Chair'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.STOOL',
    value: 'Stool'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.FLOOR_MAT',
    value: 'Floor mat'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ERGONOMIC_EQUIPMENT',
    value: 'Ergonomic equipment'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MONITOR',
    value: 'Monitor'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.GLARE_GOGGLES',
    value: 'Glare goggles'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.HEADSET',
    value: 'Headset'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.LIGHTING_CHANGE',
    value: 'Lighting change'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.AIR_PURIFIER',
    value: 'Air purifier'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.HUMIDIFIER',
    value: 'Humidifier'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.FAN',
    value: 'Fan'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.WHITE_NOISE_MACHINE',
    value: 'White noise machine'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ASSISTIVE_SOFTWARE',
    value: 'Assistive software'
  },
  {
    category:
      'Workstation equipment, software, or devices change or modification',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CHANGE_IN_MATERIALS',
    value: 'Change in materials or chemicals used for job'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.WRITING_AIDS',
    value: 'Writing aids'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.DICTATION_MACHINE',
    value: 'Dictation machine'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.BACK_BRACE',
    value: 'Back brace'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MEMORY_OR_TASKING_AID',
    value: 'Memory or tasking aid (scheduler/organizer)'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.LARGE_PRINT_MATERIAL',
    value: 'Large print material'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.TELEPHONE_AMPLIFICATION',
    value: 'Telephone amplification'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.TTY_ASSISTIVE_LISTENING_DEVICE',
    value: 'TTY, assistive listening device'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MAGNIFIER',
    value: 'Magnifier'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CART',
    value: 'Cart'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CAROUSAL_FILE',
    value: 'Carousal file'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ERGONOMIC_DESIGNED_TOOLS',
    value: 'Ergonomic designed tools'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.GLOVES',
    value: 'Gloves'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.VIBRATION_DAMPENING_TOOL',
    value: 'Vibration dampening tool'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.POSITIONERS',
    value: 'Positioners'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.REFRIGERATOR',
    value: 'Refrigerator'
  },
  {
    category: 'Assistive equipment (minor)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MINOR_PROTECTIVE_CLOTHING',
    value: 'Minor protective clothing or equipment'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.POWER_DOORS',
    value: 'Power doors'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ROBOTIC_TOOL',
    value: 'Robotic tool'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.RETROFITTED_AUTOMOBILE',
    value: 'Retrofitted automobile'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.WHEELCHAIR_RAMP',
    value: 'Wheelchair ramp or lift'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MATERIAL_HANDLING_LIFTS',
    value: 'Material handling lifts'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CRANES',
    value: 'Cranes'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.HOISTS',
    value: 'Hoists'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.FOOT_CONTROLS',
    value: 'Foot controls'
  },
  {
    category: 'Assistive equipment (major)',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.MAJOR_PROTECTIVE_CLOTHING',
    value: 'Major protective clothing or equipment'
  },
  {
    category: 'Mobility aid',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.SCOOTER',
    value: 'Scooter'
  },
  {
    category: 'Mobility aid',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ELEVATOR',
    value: 'Elevator'
  },
  {
    category: 'Mobility aid',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.DRIVER',
    value: 'Driver'
  },
  {
    category: 'Mobility aid',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.PARKING',
    value: 'Parking'
  },
  {
    category: 'Hearing assistance',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.SIGN_LANGUAGE_INTERPRETER',
    value: 'Sign language interpreter'
  },
  {
    category: 'Hearing assistance',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.TECHNOLOGY',
    value: 'Technology'
  },
  {
    category: 'Sight assistance',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.TECHNOLOGY',
    value: 'Technology'
  },
  {
    category: 'Sight assistance',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.BRAILLE',
    value: 'Braille'
  },
  {
    category: 'Additional training',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ON_THE_JOB_TRAINING',
    value: 'On-the-job Training'
  },
  {
    category: 'Additional training',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.OFF_SITE_TRAINING',
    value: 'Off-site Training'
  },
  {
    category: 'Additional training',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.CERTIFICATION',
    value: 'Certification'
  },
  {
    category: 'Additional training',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.PROFESSIONAL_TRAINING_OR_EDUCATION',
    value: 'Professional Training or Education'
  },
  {
    category: 'Provide administrative support',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.NOTE_TAKING',
    value: 'Note taking'
  },
  {
    category: 'Provide administrative support',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.PROOF_READING',
    value: 'Proof reading'
  },
  {
    category: 'Provide administrative support',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.ORGANIZING',
    value: 'Organizing'
  },
  {
    category: 'Provide administrative support',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.GOAL_SETTING',
    value: 'Goal setting'
  },
  {
    category: 'Other Accommodation',
    text: 'WORKPLACE_ACCOMMODATION_TYPES.OTHER',
    value: 'Other'
  }
];
