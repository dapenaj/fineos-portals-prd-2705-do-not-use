import { CustomerDetail } from '../types';

export const DEFAULT_CUSTOMER: CustomerDetail = {
  customer: null,
  contact: null,
  occupation: null,
  customerOccupations: null
};
