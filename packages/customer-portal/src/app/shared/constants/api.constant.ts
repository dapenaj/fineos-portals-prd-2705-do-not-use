export const PENDING = (type: string) => `${type}_PENDING`;
export const FULFILLED = (type: string) => `${type}_FULFILLED`;
export const REJECTED = (type: string) => `${type}_REJECTED`;
