import React from 'react';
import classnames from 'classnames';

import styles from './content-layout.module.scss';

type ContentLayoutProps = {
  direction?: 'column' | 'row';
  noPadding?: boolean;
  className?: string;
};

export const ContentLayout: React.FC<ContentLayoutProps> = ({
  direction = 'row',
  noPadding,
  children,
  className,
  ...props
}) => (
  <div
    className={classnames(
      styles.content,
      className,
      direction === 'column' ? styles.column : styles.row,
      noPadding ? noPadding : styles.padding
    )}
    {...props}
  >
    {children}
  </div>
);
