import React from 'react';
import { Alert as AlertAnt } from 'antd';
import { FormattedMessage } from 'react-intl';

import { AntType } from '../../types';

type AlertProps = {
  message: string;
  type: AntType;
  description?: string;
};

export const Alert: React.FC<AlertProps> = ({ message, type, description }) => (
  <AlertAnt
    message={<FormattedMessage id={message} />}
    type={type}
    description={description}
    closable={true}
  />
);
