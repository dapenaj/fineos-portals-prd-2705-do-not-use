import { AbsenceService } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import {
  TimeOffAmendmentType,
  AdditionalTimeOffAmendment,
  ReducedTimeOffAmendment,
  MovedTimeOffAmendment,
  AbsenceAmendment
} from '../../../shared/types';

const absenceService = AbsenceService.getInstance();

const getMovedTimeOffAmendment = (
  amendedPeriod: MovedTimeOffAmendment,
  startDate: string
): MovedTimeOffAmendment => {
  amendedPeriod.originalFirstDayAbsent = startDate;

  const hours = !_isEmpty(amendedPeriod.newLastWorkDayHours)
    ? amendedPeriod.newLastWorkDayHours
    : 0;

  const minutes = !_isEmpty(amendedPeriod.newLastWorkDayMinutes)
    ? amendedPeriod.newLastWorkDayMinutes
    : 0;

  if (!_isEmpty(amendedPeriod.newLastWorkDayHours)) {
    amendedPeriod.newLastWorkDayHours = `${hours} hr(s) ${minutes} min`;
  }

  amendedPeriod.newLastWorkDayPartial === 'YES'
    ? (amendedPeriod.newLastWorkDayPartial = 'Full Day')
    : (amendedPeriod.newLastWorkDayPartial = 'Partial Day');

  return amendedPeriod;
};

export const getAmendmentRequest = async (
  type: TimeOffAmendmentType,
  { absenceCaseId, startDate, absencePeriodType }: AbsenceAmendment,
  amendedPeriod:
    | AdditionalTimeOffAmendment
    | ReducedTimeOffAmendment
    | MovedTimeOffAmendment,
  member?: string
) => {
  switch (type) {
    case TimeOffAmendmentType.ADDITIONAL_TIME_OFF:
      return absencePeriodType === 'Continuous'
        ? await absenceService.additionalTimeOffForContinuousPeriod(
            absenceCaseId,
            amendedPeriod as AdditionalTimeOffAmendment,
            member
          )
        : await absenceService.additionalTimeOffForReducedPeriod(
            absenceCaseId,
            amendedPeriod as AdditionalTimeOffAmendment,
            member
          );
    case TimeOffAmendmentType.REDUCED_TIME_OFF:
      return absencePeriodType === 'Continuous'
        ? await absenceService.reduceTimeOffForContinuousPeriod(
            absenceCaseId,
            amendedPeriod as ReducedTimeOffAmendment,
            member
          )
        : await absenceService.reduceTimeOffForReducedPeriod(
            absenceCaseId,
            amendedPeriod as ReducedTimeOffAmendment,
            member
          );
    case TimeOffAmendmentType.MOVED_TIME_OFF:
      return absencePeriodType === 'Continuous'
        ? await absenceService.movedTimeOffForContinuousPeriod(
            absenceCaseId,
            getMovedTimeOffAmendment(
              amendedPeriod as MovedTimeOffAmendment,
              startDate
            ),
            member
          )
        : await absenceService.movedTimeOffForReducedPeriod(
            absenceCaseId,
            getMovedTimeOffAmendment(
              amendedPeriod as MovedTimeOffAmendment,
              startDate
            ),
            member
          );
  }
};
