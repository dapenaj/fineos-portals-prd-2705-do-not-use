import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Modal, Col, Row, Button } from 'antd';

import greenCheckIntake from '../../../../../assets/img/green_check_intake.svg';

import styles from './request-confirmation.module.scss';

export type RequestConfirmationViewProps = {
  isEdit: string;
  isSupervisor: boolean;
  onClose: () => void;
  messageText: React.ReactNode;
};

export class RequestConfirmationView extends React.Component<
  RequestConfirmationViewProps
> {
  render() {
    const { isEdit, isSupervisor, messageText, onClose } = this.props;
    const messagePrefix = isSupervisor
      ? 'MY_TEAM_LEAVES.REQUEST_CHANGE'
      : 'NOTIFICATION.REQUEST_AMENDMENT';

    return (
      <div>
        <Modal
          title={<FormattedMessage id={`${messagePrefix}.${isEdit}`} />}
          visible={true}
          onOk={onClose}
          onCancel={onClose}
          footer={[
            null,
            <Button key="ok" type="primary" onClick={onClose}>
              <FormattedMessage id="NOTIFICATION.REQUEST_AMENDMENT.CONTROLS.CLOSE" />
            </Button>
          ]}
          className="request-amendment-confirmation-modal"
        >
          <div className={styles.confirmation}>
            <Row>
              <Col xs={24} sm={12} md={16}>
                <p>{messageText}</p>
              </Col>
              <Col xs={24} sm={12} md={8}>
                <img alt="success-icon" src={greenCheckIntake} />
              </Col>
            </Row>
          </div>
        </Modal>
      </div>
    );
  }
}
