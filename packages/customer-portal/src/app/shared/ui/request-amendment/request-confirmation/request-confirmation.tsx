import React from 'react';

import { AppConfig } from '../../../types';
import { withAppConfig } from '../../../contexts';

import { RequestConfirmationView } from './request-confirmation.view';

type RequestConfirmationProps = {
  isEdit: string;
  isSupervisor: boolean;
  config: AppConfig;
  renderConfirmation: () => void;
};

export class RequestConfirmationComponent extends React.Component<
  RequestConfirmationProps
> {
  render() {
    const {
      isEdit,
      isSupervisor,
      renderConfirmation,
      config: {
        clientConfig: {
          notification: {
            requestConfirmation: {
              requestAmendmentMessage,
              requestDeletionMessage,
              supervisorAmendmentMessage,
              supervisorDeletionMessage
            }
          }
        }
      }
    } = this.props;

    const amendmentMessage = isSupervisor
      ? supervisorAmendmentMessage
      : requestAmendmentMessage;
    const deletionMessage = isSupervisor
      ? supervisorDeletionMessage
      : requestDeletionMessage;

    return (
      <RequestConfirmationView
        isEdit={isEdit}
        isSupervisor={isSupervisor}
        onClose={renderConfirmation}
        messageText={
          isEdit === 'REQUEST_AMENDMENT' ? amendmentMessage : deletionMessage
        }
      />
    );
  }
}

export const RequestConfirmationModal = withAppConfig(
  RequestConfirmationComponent
);
