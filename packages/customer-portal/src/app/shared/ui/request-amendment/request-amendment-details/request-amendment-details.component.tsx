import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';

import { PropertyItem } from '../../../ui';
import { AbsenceAmendment } from '../../../types';

import styles from './request-amendment-details.module.scss';

export type RequestAmendmentDetailsProps = {
  amendment: AbsenceAmendment;
};

export class RequestAmendmentDetails extends React.Component<
  RequestAmendmentDetailsProps
> {
  render() {
    const {
      amendment: { startDate, endDate, absencePeriodType }
    } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.heading}>
          <FormattedMessage id="NOTIFICATION.REQUEST_AMENDMENT.ORIGINAL_DATES" />
        </div>
        <Row gutter={32}>
          <Col xs={24} sm={12} md={8}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATION.REQUEST_AMENDMENT.LEAVE_BEGAN" />
              }
            >
              {startDate}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={8}>
            <PropertyItem
              label={<FormattedMessage id="NOTIFICATION.APPROVED_THROUGH" />}
            >
              {endDate}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={8}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATION.REQUEST_AMENDMENT.PERIOD_TYPE" />
              }
            >
              {absencePeriodType}
            </PropertyItem>
          </Col>
        </Row>
      </div>
    );
  }
}
