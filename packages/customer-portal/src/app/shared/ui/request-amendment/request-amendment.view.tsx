import React from 'react';
import { Form, Modal } from 'antd';
import {
  FormattedMessage,
  WrappedComponentProps as IntlProps
} from 'react-intl';
import { FormikProps } from 'formik';

import { RequestAmendmentDetails, ModalFooter } from '../../../shared/ui';
import {
  RequestAmendmentFormValue,
  AbsenceAmendment
} from '../../../shared/types';

import { RequestAmendmentForm } from './request-amendment-form';

export type RequestAmendmentViewProps = {
  amendment: AbsenceAmendment;
  closeForm: () => void;
  isSupervisor: boolean;
  renderConfirmation: () => void;
} & FormikProps<RequestAmendmentFormValue> &
  IntlProps;

export class RequestAmendmentView extends React.Component<
  RequestAmendmentViewProps
> {
  render() {
    const {
      amendment,
      closeForm,
      isSupervisor,
      isValid,
      ...formProps
    } = this.props;

    return (
      <Form
        name="amend-time-off"
        data-test-el="amended-time-off-form"
        onSubmit={this.props.handleSubmit}
      >
        <Modal
          title={
            <FormattedMessage
              id={
                isSupervisor
                  ? 'MY_TEAM_LEAVES.REQUEST_A_CHANGE'
                  : 'NOTIFICATION.REQUEST_AMENDMENT.REQUEST_AMENDMENT'
              }
            />
          }
          visible={true}
          onCancel={closeForm}
          footer={
            <ModalFooter
              onSubmitClick={formProps.submitForm}
              isDisabled={!isValid}
              submitText={'COMMON.ACTIONS.OK'}
              onCancelClick={closeForm}
              cancelText={'COMMON.ACTIONS.CLOSE'}
            />
          }
        >
          <RequestAmendmentDetails amendment={amendment} />
          <RequestAmendmentForm
            amendment={amendment}
            closeForm={closeForm}
            isSupervisor={isSupervisor}
            isValid={isValid}
            {...formProps}
          />
        </Modal>
      </Form>
    );
  }
}
