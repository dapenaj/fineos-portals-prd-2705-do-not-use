import { withFormik, FormikBag } from 'formik';
import { WrappedComponentProps as IntlProps, injectIntl } from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';

import { showNotification } from '../../../shared/ui';
import {
  RequestAmendmentFormValue,
  TimeOffAmendmentType,
  AdditionalTimeOffAmendment,
  AbsenceAmendment
} from '../../../shared/types';

import { RequestAmendmentView } from './request-amendment.view';
import { RequestAmendmentSchema } from './request-amendment-form-validation';
import { getAmendmentRequest } from './request-amendment-submit';

export type RequestAmendmentProps = {
  member?: string;
  amendment: AbsenceAmendment;
  closeForm: () => void;
  isSupervisor: boolean;
  renderConfirmation: () => void;
} & IntlProps;

export const handleSubmit = async (
  values: RequestAmendmentFormValue,
  { props }: FormikBag<RequestAmendmentProps, RequestAmendmentFormValue>
) => {
  const {
    intl: { formatMessage },
    isSupervisor,
    member,
    amendment,
    renderConfirmation,
    closeForm
  } = props;

  const { amendedPeriod, type } = values;

  if (!isSupervisor) {
    amendedPeriod.leavePeriodStatus = amendment.statusCategory;
  }

  if (!_isEmpty(amendedPeriod.returnHours)) {
    amendedPeriod.returnHours = `${amendedPeriod.returnHours ||
      0} hr(s) ${amendedPeriod.returnMinutes || 0} min`;
  }

  amendedPeriod.returnDay
    ? (amendedPeriod.returnDay = 'Full Day')
    : (amendedPeriod.returnDay = 'Partial Day');

  try {
    await getAmendmentRequest(type, amendment, amendedPeriod, member);

    renderConfirmation();
  } catch (error) {
    const title = formatMessage({
      id: 'NOTIFICATION.REQUEST_AMENDMENT.ERRORS.LEAVE_PERIOD_AMENDMENT'
    });

    showNotification({
      title,
      type: 'error',
      content: error
    });
  } finally {
    closeForm();
  }
};

export const RequestAmendment = injectIntl(
  withFormik<RequestAmendmentProps, RequestAmendmentFormValue>({
    mapPropsToValues: ({ amendment: { periodId, endDate } }) => ({
      type: TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
      amendedPeriod: {
        leavePeriodId: periodId,
        requestReason: 'More Time',
        originalLastDayAbsent: endDate
      } as AdditionalTimeOffAmendment
    }),
    validationSchema: RequestAmendmentSchema,
    handleSubmit
  })(RequestAmendmentView)
);
