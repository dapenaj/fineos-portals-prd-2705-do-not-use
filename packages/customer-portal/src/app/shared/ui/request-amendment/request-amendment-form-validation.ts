import * as Yup from 'yup';
import moment from 'moment';

import {
  RequestAmendmentFormValue,
  TimeOffAmendmentType
} from '../../../shared/types';

export const RequestAmendmentSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  Partial<RequestAmendmentFormValue>
>> = Yup.object().shape({
  type: Yup.string().required('COMMON.VALIDATION.REQUIRED'),
  amendedPeriod: Yup.object()
    .when('type', {
      is: type => type === TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
      then: Yup.object().shape({
        sameReason: Yup.string().when(
          'leavePeriod',
          (leavePeriod: string, schema: any) => {
            if (leavePeriod) {
              Yup.string().required('COMMON.VALIDATION.REQUIRED');
            }
          }
        ),
        newReason: Yup.string().when('sameReason', {
          is: 'NO',
          then: Yup.string().required('COMMON.VALIDATION.REQUIRED')
        }),
        newLastDayAbsent: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        newExpectedReturnDay: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable()
          .when('newLastDayAbsent', (newLastDayAbsent: string, schema: any) =>
            newLastDayAbsent
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(newLastDayAbsent).toDate(),
                    'NOTIFICATION.REQUEST_AMENDMENT.VALIDATION.RETURN_AFTER_ABSENCE'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
          ),

        returnDay: Yup.boolean().required('COMMON.VALIDATION.REQUIRED')
      })
    })
    .when('type', {
      is: type => type === TimeOffAmendmentType.REDUCED_TIME_OFF,
      then: Yup.object().shape({
        newLastDayAbsent: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        newExpectedReturnDay: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable()
          .when('newLastDayAbsent', (newLastDayAbsent: string, schema: any) =>
            newLastDayAbsent
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(newLastDayAbsent).toDate(),
                    'NOTIFICATION.REQUEST_AMENDMENT.VALIDATION.RETURN_AFTER_ABSENCE'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
          ),
        returnDay: Yup.boolean().required('COMMON.VALIDATION.REQUIRED')
      })
    })
    .when('type', {
      is: type => type === TimeOffAmendmentType.MOVED_TIME_OFF,
      then: Yup.object().shape({
        newLastWorkDay: Yup.date()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable(),
        newFirstDayAbsent: Yup.date()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable()
          .when('newLastWorkDay', (newLastWorkDay: string, schema: any) =>
            newLastWorkDay
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(newLastWorkDay).toDate(),
                    'NOTIFICATION.REQUEST_AMENDMENT.VALIDATION.ABSENCE_AFTER_WORK'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
          ),
        newLastWorkDayHours: Yup.string().when('newLastWorkDayPartial', {
          is: true,
          then: Yup.string().required('COMMON.VALIDATION.REQUIRED')
        }),
        newLastWorkDayMinutes: Yup.string().when('newLastWorkDayPartial', {
          is: true,
          then: Yup.string().required('COMMON.VALIDATION.REQUIRED')
        }),
        newLastDayAbsent: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable()
          .when('newFirstDayAbsent', (newFirstDayAbsent: string, schema: any) =>
            newFirstDayAbsent
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(newFirstDayAbsent).toDate(),
                    'NOTIFICATION.REQUEST_AMENDMENT.VALIDATION.ABSENCE_OVERLAP'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
          ),
        newExpectedReturnDay: Yup.string()
          .required('COMMON.VALIDATION.REQUIRED')
          .nullable()
          .when('newLastDayAbsent', (newLastDayAbsent: string, schema: any) =>
            newLastDayAbsent
              ? Yup.date()
                  .required('COMMON.VALIDATION.REQUIRED')
                  .nullable()
                  .min(
                    moment(newLastDayAbsent).toDate(),
                    'NOTIFICATION.REQUEST_AMENDMENT.VALIDATION.RETURN_AFTER_ABSENCE'
                  )
              : schema.required('COMMON.VALIDATION.REQUIRED').nullable()
          ),
        returnDay: Yup.boolean().required('COMMON.VALIDATION.REQUIRED')
      })
    })
});
