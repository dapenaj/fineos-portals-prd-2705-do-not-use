import React from 'react';

import { Popover } from '../../../../shared/ui';
import { getMessageForLocale } from '../../../../i18n/utils';
import { TimeOffAmendmentType } from '../../../../shared/types';

export type TimeOffAmendmentPopoverProps = { content: TimeOffAmendmentType };

export const AmendmentSelectionPopover: React.FC<TimeOffAmendmentPopoverProps> = ({
  content
}) => (
  <>
    {getMessageForLocale('en', `POPOVER.${content}`) && (
      <Popover content={content} />
    )}
  </>
);
