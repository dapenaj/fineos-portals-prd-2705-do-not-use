import React from 'react';
import { ClaimService } from 'fineos-js-api-client';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import { FormikProps } from 'formik';
import { RadioChangeEvent } from 'antd/lib/radio';
import moment, { Moment } from 'moment';

import {
  FormikRadioGroupInput,
  FormLabel,
  BooleanInput,
  DatePickerInput,
  FormikHoursInput,
  FormikMinutesInput,
  CheckboxInput,
  TextInput
} from '../../../../shared/ui';
import { formatDateForApi } from '../../../../shared/utils';
import {
  TimeOffAmendmentType,
  RequestAmendmentFormValue,
  AbsenceAmendment
} from '../../../../shared/types';

import { AmendmentSelectionPopover } from './amendment-selection-popover.component';
import styles from './request-amendment-form.module.scss';

const typeOptions: TimeOffAmendmentType[] = [
  TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
  TimeOffAmendmentType.REDUCED_TIME_OFF,
  TimeOffAmendmentType.MOVED_TIME_OFF
];

export type RequestAmendmentFormProps = {
  amendment: AbsenceAmendment;
  closeForm: () => void;
  isSupervisor: boolean;
} & FormikProps<RequestAmendmentFormValue>;

type RequestAmendmentFormState = {
  type: TimeOffAmendmentType;
  amendedLeavePeriods: RequestAmendmentFormValue[];
  disabledTypeOptions: TimeOffAmendmentType[];
  showNewReason: boolean | undefined;
  showStartTime: boolean | undefined;
  showEndTime: boolean | undefined;
  showConfirmation: boolean;
};

export class RequestAmendmentForm extends React.Component<
  RequestAmendmentFormProps,
  RequestAmendmentFormState
> {
  state: RequestAmendmentFormState = {
    type: TimeOffAmendmentType.ADDITIONAL_TIME_OFF,
    amendedLeavePeriods: [],
    disabledTypeOptions: [],
    showNewReason: false,
    showStartTime: false,
    showEndTime: false,
    showConfirmation: false
  };

  claimService = ClaimService.getInstance();

  componentDidMount() {
    if (moment().isAfter(moment(this.props.amendment.startDate))) {
      this.setState({
        disabledTypeOptions: [TimeOffAmendmentType.MOVED_TIME_OFF]
      });
    }
  }

  handleTypeChange = ({ target: { value } }: RadioChangeEvent) => {
    this.setState({
      type: value,
      showNewReason: undefined,
      showStartTime: undefined
    });

    this.props.setValues({
      type: value,
      amendedPeriod: this.props.values.amendedPeriod
    });

    switch (value) {
      case TimeOffAmendmentType.ADDITIONAL_TIME_OFF:
        this.props.setFieldValue('amendedPeriod.sameReason', undefined);
        return (this.props.values.amendedPeriod.requestReason = 'More time');
      case TimeOffAmendmentType.REDUCED_TIME_OFF:
        return (this.props.values.amendedPeriod.requestReason = 'Less time');
      case TimeOffAmendmentType.MOVED_TIME_OFF:
        this.props.setFieldValue('amendedPeriod.newLastWorkDay', undefined);
        this.props.setFieldValue(
          'amendedPeriod.newLastWorkDayPartial',
          undefined
        );
        this.props.setFieldValue(
          'amendedPeriod.newLastWorkDayHours',
          undefined
        );
        this.props.setFieldValue(
          'amendedPeriod.newLastWorkDayMinutes',
          undefined
        );
        this.props.setFieldValue('amendedPeriod.newFirstDayAbsent', undefined);
        return (this.props.values.amendedPeriod.requestReason = 'Moved time');
    }
  };

  handleReasonChange = (e: RadioChangeEvent) =>
    e.target.value === 'YES'
      ? this.setState({ showNewReason: false })
      : this.setState({ showNewReason: true });

  handleLastDayChanged = (date: Moment | null) =>
    this.props.setFieldValue('startDate', date && formatDateForApi(date));

  handleNewLastWorkDayPartialChange = (checked: boolean) => {
    if (checked) {
      this.props.setFieldValue('amendedPeriod.newLastWorkDayHours', 0);
      this.props.setFieldValue('amendedPeriod.newLastWorkDayMinutes', 0);
    } else {
      this.props.setFieldValue('amendedPeriod.newLastWorkDayHours', undefined);
      this.props.setFieldValue(
        'amendedPeriod.newLastWorkDayMinutes',
        undefined
      );
    }

    this.setState({
      showStartTime: checked
    });
  };

  handleEndDateFullDayChange = (checked: boolean) => {
    if (!checked) {
      this.props.setFieldValue('amendedPeriod.returnHours', 0);
      this.props.setFieldValue('amendedPeriod.returnMinutes', 0);
    } else {
      this.props.setFieldValue('amendedPeriod.returnHours', undefined);
      this.props.setFieldValue('amendedPeriod.returnMinutes', undefined);
    }

    this.setState({
      showEndTime: !checked
    });
  };

  render() {
    const { amendment, isSupervisor } = this.props;
    const {
      type,
      showNewReason,
      showStartTime,
      showEndTime,
      disabledTypeOptions
    } = this.state;

    const labelPrefix = isSupervisor
      ? 'MY_TEAM_LEAVES.REQUEST_CHANGE'
      : 'NOTIFICATION.REQUEST_AMENDMENT';

    return (
      <div
        className={styles.container}
        data-test-el="amend-period-form-container"
      >
        <Row gutter={32} className={styles.heading}>
          <Col>
            <FormikRadioGroupInput
              data-test-el="amended-time-off-type-input"
              labelCol={{ xs: 48, sm: 24 }}
              wrapperCol={{ xs: 24, sm: 12 }}
              label={
                <FormLabel
                  required={true}
                  label="NOTIFICATION.REQUEST_AMENDMENT.WHAT_CHANGES_TO_MAKE"
                />
              }
              name="type"
              radioOptions={typeOptions}
              optionPrefix={`${labelPrefix}.OPTIONS.`}
              defaultValue={TimeOffAmendmentType.ADDITIONAL_TIME_OFF}
              disabledOptions={disabledTypeOptions}
              onChange={this.handleTypeChange}
              renderPopover={typeIndex => (
                <AmendmentSelectionPopover content={typeOptions[typeIndex]} />
              )}
            />
          </Col>
        </Row>

        {type === TimeOffAmendmentType.ADDITIONAL_TIME_OFF && !isSupervisor && (
          <>
            <div className={styles.formGroup}>
              <Row gutter={32}>
                <Col>
                  <FormikRadioGroupInput
                    data-test-el="new-absense-reason"
                    optionPrefix="COMMON.CONFIRMATION."
                    radioOptions={['YES', 'NO']}
                    name="amendedPeriod.sameReason"
                    onChange={this.handleReasonChange}
                    labelCol={{ xs: 48, sm: 24 }}
                    wrapperCol={{ xs: 24, sm: 12 }}
                    label={
                      <FormLabel
                        required={true}
                        label={
                          'NOTIFICATION.REQUEST_AMENDMENT.SAME_ABSENCE_REASON'
                        }
                        values={amendment}
                      />
                    }
                  />
                </Col>
              </Row>
            </div>
            {showNewReason && (
              <TextInput
                {...this.props}
                name="amendedPeriod.newReason"
                label={
                  <FormLabel
                    required={true}
                    label="NOTIFICATION.REQUEST_AMENDMENT.NEW_ABSENCE_REASON"
                  />
                }
              />
            )}
          </>
        )}

        {type === TimeOffAmendmentType.MOVED_TIME_OFF && (
          <>
            <div
              className={styles.formGroup}
              data-test-el="leave-period-form-group"
            >
              <Row gutter={32} type="flex" align="bottom">
                <Col>
                  <DatePickerInput
                    {...this.props}
                    name="amendedPeriod.newLastWorkDay"
                    labelCol={{ xs: 48, sm: 24 }}
                    wrapperCol={{ xs: 24, sm: 12 }}
                    label={
                      <FormLabel
                        required={true}
                        label={`${labelPrefix}.NEW_LAST_DAY_WORKED`}
                      />
                    }
                    onChange={this.handleLastDayChanged}
                  />
                </Col>
                <Col>
                  <CheckboxInput
                    {...this.props}
                    data-test-el="new-last-day-partial"
                    name="amendedPeriod.newLastWorkDayPartial"
                    labelCol={{ xs: 48, sm: 24 }}
                    wrapperCol={{ xs: 24, sm: 12 }}
                    label={
                      <FormattedMessage id={`${labelPrefix}.PARTIAL_DAY`} />
                    }
                    value={undefined}
                    onChange={this.handleNewLastWorkDayPartialChange}
                  />
                </Col>
              </Row>

              {showStartTime && (
                <div
                  className={styles.formGroup}
                  data-test-el="leave-period-form-group"
                >
                  <Row gutter={32} type="flex" align="top">
                    <Col xs={24}>
                      <FormLabel
                        required={true}
                        label={`${labelPrefix}.HOW_MUCH_TIME_ABSENT`}
                      />
                    </Col>
                    <Col xs={6} md={4}>
                      <FormikHoursInput
                        {...this.props}
                        name="amendedPeriod.newLastWorkDayHours"
                        label={
                          <FormattedMessage id="INTAKE.DETAILS.LABELS.HOURS" />
                        }
                      />
                    </Col>
                    <Col xs={6} md={4}>
                      <FormikMinutesInput
                        {...this.props}
                        name="amendedPeriod.newLastWorkDayMinutes"
                        label={
                          <FormattedMessage id="INTAKE.DETAILS.LABELS.MINUTES" />
                        }
                      />
                    </Col>
                  </Row>
                </div>
              )}
            </div>
            <div
              className={styles.formGroup}
              data-test-el="leave-period-form-group"
            >
              <Row gutter={32}>
                <Col>
                  <DatePickerInput
                    {...this.props}
                    name="amendedPeriod.newFirstDayAbsent"
                    labelCol={{ xs: 48, sm: 24 }}
                    wrapperCol={{ xs: 24, sm: 12 }}
                    label={
                      <FormLabel
                        required={true}
                        label={`${labelPrefix}.NEW_FIRST_DAY_WORKED`}
                      />
                    }
                  />
                </Col>
              </Row>
            </div>
          </>
        )}
        <div className={styles.formGroup} data-test-el="amended-new-last-day">
          <Row gutter={32}>
            <Col>
              <DatePickerInput
                {...this.props}
                name="amendedPeriod.newLastDayAbsent"
                labelCol={{ xs: 48, sm: 24 }}
                wrapperCol={{ xs: 24, sm: 12 }}
                label={
                  <FormLabel
                    required={true}
                    label={`${labelPrefix}.LAST_ABSENCE`}
                  />
                }
              />
            </Col>
          </Row>
        </div>
        <div
          className={styles.formGroup}
          data-test-el="amended-expected-return"
        >
          <Row gutter={32}>
            <Col>
              <DatePickerInput
                {...this.props}
                name="amendedPeriod.newExpectedReturnDay"
                labelCol={{ xs: 48, sm: 24 }}
                wrapperCol={{ xs: 24, sm: 12 }}
                label={
                  <FormLabel
                    required={true}
                    label={`${labelPrefix}.RETURN_TO_WORK_DATE`}
                  />
                }
              />
            </Col>
          </Row>
        </div>
        <div className={styles.formGroup} data-test-el="amended-return-day">
          <Row gutter={32}>
            <Col>
              <BooleanInput
                {...this.props}
                name="amendedPeriod.returnDay"
                labelCol={{ xs: 48, sm: 24 }}
                wrapperCol={{ xs: 24, sm: 12 }}
                label={
                  <FormLabel
                    required={true}
                    label={`${labelPrefix}.NORMAL_RETURN_HOURS`}
                  />
                }
                onChange={this.handleEndDateFullDayChange}
              />
            </Col>
          </Row>
        </div>
        {showEndTime && (
          <>
            <div
              className={styles.formGroup}
              data-test-el="leave-period-form-group"
            >
              <Row gutter={32} type="flex" align="top">
                <Col xs={24}>
                  <FormLabel
                    required={true}
                    label={`${labelPrefix}.HOW_MUCH_TIME_ABSENT`}
                  />
                </Col>
                <Col xs={6} md={4}>
                  <FormikHoursInput
                    {...this.props}
                    name="amendedPeriod.returnHours"
                    label={
                      <FormattedMessage id="INTAKE.DETAILS.LABELS.HOURS" />
                    }
                  />
                </Col>
                <Col xs={6} md={4}>
                  <FormikMinutesInput
                    {...this.props}
                    name="amendedPeriod.returnMinutes"
                    label={
                      <FormattedMessage id="INTAKE.DETAILS.LABELS.MINUTES" />
                    }
                  />
                </Col>
              </Row>
            </div>
          </>
        )}
      </div>
    );
  }
}
