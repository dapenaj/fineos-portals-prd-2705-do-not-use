import React from 'react';
import { Menu } from 'antd';
import { NavLink, RouteComponentProps, withRouter } from 'react-router-dom';
import {
  WrappedComponentProps as IntlProps,
  injectIntl,
  FormattedMessage
} from 'react-intl';
import { MessageService, MessageSearch } from 'fineos-js-api-client';

import { NavItemLink, Role } from '../../types';
import { ContentLayout } from '../../layouts';
import { AuthorizationService } from '../../services';
import { showNotification } from '../notification';

import styles from './nav-bar.module.scss';

const { Item } = Menu;

const links: NavItemLink[] = [
  { name: 'NAVBAR_LINKS.HOME', path: '/', exact: true },
  {
    name: 'NAVBAR_LINKS.MY_NOTIFICATIONS',
    path: '/notifications',
    exact: true
  },
  { name: 'NAVBAR_LINKS.MY_PAYMENTS', path: '/my-payments', exact: true },
  {
    name: 'NAVBAR_LINKS.MY_MESSAGES',
    path: '/my-messages',
    exact: true,
    showBadge: true
  },
  {
    name: 'NAVBAR_LINKS.MY_TEAM_LEAVES',
    path: '/my-team-leaves',
    exact: true,
    role: Role.ABSENCE_SUPERVISOR
  }
];

type NavBarProps = RouteComponentProps & IntlProps;
type NavBarState = { unreadMessagesCount: number | null };

export class NavBarComponent extends React.Component<NavBarProps, NavBarState> {
  state: NavBarState = {
    unreadMessagesCount: null
  };

  readonly unlistenHistory = this.props.history.listen(() => {
    this.fetchMessages();
  });

  authorizationService = AuthorizationService.getInstance();
  messageService = MessageService.getInstance();

  componentDidMount() {
    const { unreadMessagesCount } = this.state;
    if (unreadMessagesCount === null) {
      this.fetchMessages();
    }
  }

  componentWillUnmount() {
    this.unlistenHistory();
  }

  fetchMessages = async () => {
    const {
      intl: { formatMessage }
    } = this.props;
    try {
      const search = new MessageSearch();
      search.unReadMessages(true);
      const messages = await this.messageService.getMessages(search);
      this.setState({ unreadMessagesCount: messages.length });
    } catch (error) {
      const title = formatMessage({ id: 'MESSAGES.FETCH_ERROR' });
      showNotification({ title, type: 'error', content: error });
    }
  };

  checkAuthorization = (role?: Role) =>
    role ? this.authorizationService.hasRole(role) : true;

  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { unreadMessagesCount } = this.state;

    return (
      <>
        <div className={styles.navBar}>
          <ContentLayout>
            <Menu theme="light" mode="horizontal" className={styles.menu}>
              {links.map(
                ({ name, path, exact, showBadge, role }, index) =>
                  this.checkAuthorization(role) && (
                    <Item key={index} title={formatMessage({ id: name })}>
                      <NavLink to={path} exact={exact}>
                        <FormattedMessage id={name} />
                        {showBadge && !!unreadMessagesCount && (
                          <span className={styles.badge}>
                            {unreadMessagesCount}
                          </span>
                        )}
                      </NavLink>
                    </Item>
                  )
              )}
            </Menu>
          </ContentLayout>
        </div>
      </>
    );
  }
}

export const NavBar = withRouter(injectIntl(NavBarComponent));
