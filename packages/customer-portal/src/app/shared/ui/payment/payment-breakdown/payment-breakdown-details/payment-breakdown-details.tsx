import React from 'react';
import { FormattedMessage } from 'react-intl';
import { PaymentLine } from 'fineos-js-api-client';
import { Button } from 'antd';

import { Money, InlinePropertyItem } from '../../../../../shared/ui';
import { formatDateForDisplay } from '../../../../../shared/utils';

import styles from './payment-breakdown-details.module.scss';

type PaymentBreakdownDetailsProps = {
  paymentLine: PaymentLine;
  payeeName: string;
};

type PaymentBreakdownDetailsState = {
  showDetails: boolean;
};

export class PaymentBreakdownDetails extends React.Component<
  PaymentBreakdownDetailsProps,
  PaymentBreakdownDetailsState
> {
  state = {
    showDetails: false
  };

  render() {
    const { paymentLine, payeeName } = this.props;
    const { showDetails } = this.state;

    return (
      <div className={styles.wrapper} data-test-el="payment-line">
        <div className={styles.content}>
          <div className={styles.title}>
            <div>{paymentLine.lineType}</div>
            <div>
              <Money amount={parseFloat(paymentLine.amount)} />
            </div>
          </div>
          <div className={styles.details}>
            <Button
              type="link"
              onClick={() => this.setState({ showDetails: !showDetails })}
              className={styles.detailToggleBtn}
              data-test-el="payment-line-btn"
            >
              <FormattedMessage
                id={
                  !showDetails
                    ? 'PAYMENTS.DETAILS_PLUS'
                    : 'PAYMENTS.DETAILS_MINUS'
                }
              />
            </Button>
            {showDetails && (
              <div
                className={styles.properties}
                data-test-el="payment-line-properties"
              >
                <InlinePropertyItem
                  label={<FormattedMessage id="PAYMENTS.PAID_TO" />}
                >
                  {payeeName}
                </InlinePropertyItem>
                <InlinePropertyItem
                  label={<FormattedMessage id="PAYMENTS.START_DATE" />}
                >
                  {formatDateForDisplay(paymentLine.startDate)}
                </InlinePropertyItem>
                <InlinePropertyItem
                  label={<FormattedMessage id="PAYMENTS.END_DATE" />}
                >
                  {formatDateForDisplay(paymentLine.endDate)}
                </InlinePropertyItem>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
