import React from 'react';
import { ClaimService, PaymentLine } from 'fineos-js-api-client';
import { Button } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps as IntlProps
} from 'react-intl';
import classnames from 'classnames';

import { CasePayment } from '../../../../shared/types';
import { Money, ContentRenderer } from '../../../../shared/ui';
import { showNotification } from '../../notification';

import { PaymentBreakdownDetails } from './payment-breakdown-details';
import styles from './payment-breakdown.module.scss';

type PaymentBreakdownProps = {
  payment: CasePayment;
} & IntlProps;

type PaymentBreakdownState = {
  paymentLines: PaymentLine[] | null;
  showBreakdown: boolean;
  loading: boolean;
};

export class PaymentBreakdownComponent extends React.Component<
  PaymentBreakdownProps,
  PaymentBreakdownState
> {
  claimService: ClaimService = ClaimService.getInstance();

  state: PaymentBreakdownState = {
    paymentLines: [],
    showBreakdown: false,
    loading: false
  };

  async componentDidMount() {
    const {
      payment: { claimId, paymentId },
      intl: { formatMessage }
    } = this.props;

    if (claimId && paymentId) {
      this.setState({ loading: true });

      try {
        const paymentLines = await this.claimService.getPaymentLines(
          claimId,
          paymentId
        );

        this.setState({ paymentLines });
      } catch (error) {
        const title = formatMessage({ id: 'PAYMENT_LINES.FETCH_ERROR' });

        showNotification({
          title,
          type: 'error',
          content: error
        });
      } finally {
        this.setState({ loading: false });
      }
    }
  }

  render() {
    const { showBreakdown, paymentLines, loading } = this.state;
    const {
      payment: { paymentAmount, payeeName }
    } = this.props;

    return (
      <ContentRenderer
        loading={loading}
        isEmpty={_isEmpty(paymentLines)}
        data-test-el="payment-breakdown"
      >
        {!_isEmpty(paymentLines) && (
          <div className={styles.wrapper}>
            <Button
              data-test-el="show-payment-breakdown"
              className={styles.button}
              type="link"
              onClick={() => this.setState({ showBreakdown: !showBreakdown })}
            >
              <FormattedMessage
                id={
                  !showBreakdown
                    ? 'PAYMENTS.SHOW_BREAKDOWN'
                    : 'PAYMENTS.HIDE_BREAKDOWN'
                }
              />
            </Button>

            {showBreakdown && (
              <>
                <div data-test-el="payment-breakdown-details">
                  {paymentLines!.map((paymentLine, index) => (
                    <div key={index}>
                      <PaymentBreakdownDetails
                        paymentLine={paymentLine}
                        payeeName={payeeName}
                      />
                    </div>
                  ))}
                </div>

                <div
                  className={classnames(styles.amount, styles.net)}
                  data-test-el="total-payment"
                >
                  <div>
                    <FormattedMessage id="PAYMENTS.NET_PAYMENT_AMOUNT" />
                  </div>
                  <div>
                    <Money amount={parseFloat(paymentAmount)} />
                  </div>
                </div>
              </>
            )}
          </div>
        )}
      </ContentRenderer>
    );
  }
}

export const PaymentBreakdown = injectIntl(PaymentBreakdownComponent);
