import React from 'react';
import { Collapse } from 'antd';

import { CasePayment } from '../../../shared/types';

import { PaymentBreakdown } from './payment-breakdown';
import { PaymentHeader } from './payment-header';
import { PaymentDetails } from './payment-details';
import styles from './payment.module.scss';

const { Panel } = Collapse;

type PaymentProps = {
  payment: CasePayment;
  showRelatesTo?: boolean;
  className?: string;
};

export const Payment: React.FC<PaymentProps> = ({
  payment,
  className,
  showRelatesTo
}) => (
  <div data-test-el="payment" className={className}>
    <Collapse accordion={true}>
      <Panel
        header={
          <PaymentHeader payment={payment} showRelatesTo={showRelatesTo} />
        }
        key={payment.paymentId}
      >
        <div className={styles.wrapper} data-test-el="payments-details">
          <PaymentDetails payment={payment} />
          <PaymentBreakdown payment={payment} />
        </div>
      </Panel>
    </Collapse>
  </div>
);
