import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'antd';

import { formatDateForDisplay } from '../../../utils';
import { CasePayment } from '../../../types';
import { ContentLayout } from '../../../layouts';
import { PropertyItem } from '../../property-item';
import { PaymentPopover } from '../payment-popover';

type PaymentDetailsProps = {
  payment: CasePayment;
};

enum PaymentType {
  RECURRING = 'Recurring'
}

export const PaymentDetails: React.FC<PaymentDetailsProps> = ({
  payment: {
    payeeName,
    paymentMethod,
    nominatedPayeeName,
    accountTransferInfo,
    chequePaymentInfo,
    paymentType,
    periodStartDate,
    periodEndDate,
    effectiveDate
  }
}) => (
  <ContentLayout direction="column" noPadding={true}>
    <Row type="flex">
      <Col xs={12} sm={6}>
        <PropertyItem label={<FormattedMessage id="PAYMENTS.PAID_TO" />}>
          {payeeName}
        </PropertyItem>
      </Col>

      <Col xs={12} sm={6}>
        <PropertyItem label={<FormattedMessage id="PAYMENTS.PAYMENT_METHOD" />}>
          {paymentMethod}
          <PaymentPopover
            nominatedPayeeName={nominatedPayeeName}
            accountTransferInfo={accountTransferInfo}
            chequePaymentInfo={chequePaymentInfo}
          />
        </PropertyItem>
      </Col>

      <Col xs={12} sm={6}>
        {paymentType === PaymentType.RECURRING ? (
          <PropertyItem
            label={<FormattedMessage id="PAYMENTS.PERIOD_START_DATE" />}
          >
            {formatDateForDisplay(periodStartDate)}
          </PropertyItem>
        ) : (
          <PropertyItem
            label={<FormattedMessage id="PAYMENTS.EFFECTIVE_DATE" />}
          >
            {formatDateForDisplay(effectiveDate)}
          </PropertyItem>
        )}
      </Col>

      <Col xs={12} sm={6}>
        {paymentType === PaymentType.RECURRING && (
          <PropertyItem
            label={<FormattedMessage id="PAYMENTS.PERIOD_END_DATE" />}
          >
            {formatDateForDisplay(periodEndDate)}
          </PropertyItem>
        )}
      </Col>
    </Row>
  </ContentLayout>
);
