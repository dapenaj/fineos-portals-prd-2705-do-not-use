import React from 'react';
import { Row, Col } from 'antd';
import { FormattedMessage } from 'react-intl';
import { AccountTransferInfo, ChequePaymentInfo } from 'fineos-js-api-client';

import { PropertyItem } from '../../../ui';
import { Popover } from '../../popover';

type PaymentPopoverProps = {
  nominatedPayeeName?: string;
  accountTransferInfo?: AccountTransferInfo;
  chequePaymentInfo?: ChequePaymentInfo;
};

export const PaymentPopover: React.FC<PaymentPopoverProps> = ({
  nominatedPayeeName,
  accountTransferInfo,
  chequePaymentInfo
}) => (
  <Popover
    icon="info-circle"
    overlayStyle={{ width: '250px' }}
    content={
      <Row>
        {!!nominatedPayeeName && (
          <Col>
            <PropertyItem
              label={<FormattedMessage id="PAYMENTS.PAYMENT_METHOD_PAID_TO" />}
            >
              {nominatedPayeeName}
            </PropertyItem>
          </Col>
        )}
        {!!accountTransferInfo && (
          <>
            <Col>
              <PropertyItem
                label={<FormattedMessage id="PAYMENTS.BANK_INSTITUTION" />}
              >
                {accountTransferInfo.bankInstituteName}
              </PropertyItem>
            </Col>
            <Col>
              <PropertyItem
                label={<FormattedMessage id="PAYMENTS.ACCOUNT_NUMBER" />}
              >
                {accountTransferInfo.bankAccountNumber}
              </PropertyItem>
            </Col>
          </>
        )}
        {!!chequePaymentInfo && (
          <Col>
            <PropertyItem
              label={<FormattedMessage id="PAYMENTS.CHEQUE_NUMBER" />}
            >
              {chequePaymentInfo.chequeNumber}
            </PropertyItem>
          </Col>
        )}
      </Row>
    }
  />
);
