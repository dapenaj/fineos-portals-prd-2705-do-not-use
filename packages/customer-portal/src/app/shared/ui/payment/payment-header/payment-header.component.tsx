import React from 'react';
import { FormattedMessage } from 'react-intl';
import { NavLink } from 'react-router-dom';

import { formatDateForDisplay } from '../../../utils';
import { Money } from '../../money';
import { CasePayment } from '../../../types';

import styles from './payment-header.module.scss';

type PaymentHeaderProps = {
  payment: CasePayment;
  showRelatesTo?: boolean;
};

export const PaymentHeader: React.FC<PaymentHeaderProps> = ({
  payment: { paymentAmount, paymentDate, rootCaseNumber },
  showRelatesTo
}) => (
  <div data-test-el="payment-header" className={styles.wrapper}>
    <Money amount={parseFloat(paymentAmount)} />

    <div className={styles.details}>
      {showRelatesTo && (
        <div className={styles.relatesTo}>
          <div className={styles.label}>
            <FormattedMessage id="PAYMENTS.RELATES_TO" />
          </div>
          <NavLink to={`/notifications/${rootCaseNumber}`}>
            {rootCaseNumber}
          </NavLink>
        </div>
      )}
      {formatDateForDisplay(paymentDate)}
    </div>
  </div>
);
