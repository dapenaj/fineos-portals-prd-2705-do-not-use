import React from 'react';

import styles from './page-title.module.scss';

export const PageTitle: React.FC = ({ children }) => (
  <div data-test-el="page-title" className={styles.container}>
    {children}
  </div>
);
