import React from 'react';
import { Spin } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './view-loading.module.scss';

export const ViewLoading: React.FC = () => (
  <div className={styles.loading}>
    <Spin
      tip={
        ((<FormattedMessage id="COMMON.VIEW_LOADING" />) as unknown) as string
      }
      size="large"
    />
  </div>
);
