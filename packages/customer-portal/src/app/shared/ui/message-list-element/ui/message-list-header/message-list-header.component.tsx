import React from 'react';
import classnames from 'classnames';
import { Message } from 'fineos-js-api-client';

import { formatDateForDisplay } from '../../../../utils';

import styles from './message-list-header.module.scss';

type MessageListHeaderProps = {
  message: Message;
};

export const MessageListHeader: React.FC<MessageListHeaderProps> = ({
  message
}) => (
  <div className={styles.headerListWrapper}>
    <div
      className={classnames(
        message.read ? styles.read : styles.unread,
        styles.subject
      )}
    >
      {message.subject}
    </div>
    <div className={styles.date}>
      {formatDateForDisplay(message.contactTime)}
    </div>
  </div>
);
