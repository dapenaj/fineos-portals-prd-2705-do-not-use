import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Message } from 'fineos-js-api-client';

import { InlinePropertyItem } from '../../../property-item';

type MessageListBodyProps = {
  message: Message;
  showRelatesTo?: boolean;
};

export const MessageListBody: React.FC<MessageListBodyProps> = ({
  message: { caseId },
  showRelatesTo = true
}) => (
  <>
    {showRelatesTo && (
      <InlinePropertyItem label={<FormattedMessage id="PAYMENTS.RELATES_TO" />}>
        {caseId}
      </InlinePropertyItem>
    )}
  </>
);
