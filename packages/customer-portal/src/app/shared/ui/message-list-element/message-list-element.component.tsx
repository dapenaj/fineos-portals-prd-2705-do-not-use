import React from 'react';
import { Message } from 'fineos-js-api-client';

import { MessageListHeader, MessageListBody } from './ui';
import styles from './message-list-element.module.scss';

type MessageListElementProps = {
  message: Message;
  chooseLogo: (msgOriginatesFromPortal: boolean) => React.ReactNode | string;
  showRelatesTo?: boolean;
};

export const MessageListElement: React.FC<MessageListElementProps> = ({
  message,
  chooseLogo,
  showRelatesTo
}) => (
  <div className={styles.bodyWrapper}>
    <span className={styles.icon}>
      {chooseLogo(message.msgOriginatesFromPortal)}
    </span>
    <span className={styles.body}>
      <MessageListHeader message={message} />
      <MessageListBody message={message} showRelatesTo={showRelatesTo} />
      <div className={styles.preview}>{message.narrative}</div>
    </span>
  </div>
);
