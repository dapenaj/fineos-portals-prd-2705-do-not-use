import React from 'react';
import Spin from 'antd/lib/spin';

import styles from './spinner.module.scss';

export const Spinner: React.FC = () => (
  <div className={styles.spinner}>
    <Spin size="large" />
  </div>
);
