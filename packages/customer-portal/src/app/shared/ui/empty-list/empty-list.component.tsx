import React from 'react';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';

import styles from './empty-list.module.scss';

type EmptyListProps = { withBorder?: boolean };

export const EmptyList: React.FC<EmptyListProps> = ({
  children,
  withBorder
}) => (
  <div
    className={classnames(styles.wrapper, withBorder && styles.dotted)}
    data-test-el="empty-list"
  >
    <div className={styles.title}>
      {children ? children : <FormattedMessage id="COMMON.EMPTY" />}
    </div>
  </div>
);
