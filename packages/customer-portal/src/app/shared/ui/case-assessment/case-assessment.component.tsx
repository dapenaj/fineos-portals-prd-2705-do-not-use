import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  AbsenceSummary,
  ClaimSummary,
  NotificationAccommodationSummary
} from 'fineos-js-api-client';

import { Assessment } from '../assessment/assessment.component';

import styles from './case-assessment.module.scss';

type CaseAssessmentProps = {
  claims?: ClaimSummary[];
  absences?: AbsenceSummary[];
  accommodations?: NotificationAccommodationSummary[];
};

export const CaseAssessment: React.FC<CaseAssessmentProps> = ({
  absences,
  claims,
  accommodations
}) => (
  <ul className={styles.list} data-test-el="case-assessments">
    {!!absences && !!absences.length && (
      <li>
        <Assessment
          title={<FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE" />}
        />
      </li>
    )}
    {!!claims && !!claims.length && (
      <li>
        <Assessment
          title={<FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT" />}
        />
      </li>
    )}
    {!!accommodations && !!accommodations.length && (
      <li>
        <Assessment
          title={
            <FormattedMessage id="NOTIFICATIONS.WORKPLACE_ACCOMMODATION" />
          }
        />
      </li>
    )}
  </ul>
);
