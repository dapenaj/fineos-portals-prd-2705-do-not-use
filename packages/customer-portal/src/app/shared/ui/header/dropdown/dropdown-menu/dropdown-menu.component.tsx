import React from 'react';
import { Menu, Button } from 'antd';
import { FormattedMessage } from 'react-intl';
import { NavLink } from 'react-router-dom';

// import { AuthenticationService } from '../../../../services';

import styles from './dropdown-menu.module.scss';

const { Item } = Menu;

export const DropdownMenu: React.FC = () => {
  // const logout = () => AuthenticationService.getInstance().logout();

  return (
    <Menu className={styles.menu}>
      <Item>
        <NavLink to="/my-profile">
          <Button type="link" className={styles.logoutBtn}>
            <FormattedMessage id="HEADER.MY_PROFILE" />
          </Button>
        </NavLink>
      </Item>
      {/* <Item>
        <Button type="link" onClick={logout} className={styles.logoutBtn}>
          <FormattedMessage id="HEADER.LOGOUT" />
        </Button>
      </Item> */}
    </Menu>
  );
};
