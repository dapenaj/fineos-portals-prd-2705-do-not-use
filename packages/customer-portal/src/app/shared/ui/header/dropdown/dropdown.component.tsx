import React from 'react';
import { Dropdown as AntDropdown, Icon } from 'antd';
import { Customer } from 'fineos-js-api-client';

import { DropdownMenu } from './dropdown-menu';
import styles from './dropdown.module.scss';

type DropdownProps = { customer: Customer };

export const Dropdown: React.FC<DropdownProps> = ({ customer }) => (
  <AntDropdown overlay={<DropdownMenu />} trigger={['click']}>
    <span className={styles.trigger}>
      {customer.firstName} {customer.lastName}
      <Icon className={styles.icon} type="caret-down" />
    </span>
  </AntDropdown>
);
