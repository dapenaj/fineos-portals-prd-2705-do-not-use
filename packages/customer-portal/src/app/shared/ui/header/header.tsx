import React from 'react';
import { Layout } from 'antd';

import logo from '../../../../../src/assets/img/logo.svg';
import { AppConfig } from '../../types';
import { withAppConfig } from '../../contexts';
import { ContentLayout } from '../../layouts';
import { Initials } from '../initials';

import { Dropdown } from './dropdown';
import styles from './header.module.scss';

const AntHeader = Layout.Header;

type HeaderProps = { config: AppConfig };

export const HeaderComponent: React.FC<HeaderProps> = ({
  config: {
    clientConfig: {
      header: {
        branding: { message }
      }
    },
    customerDetail: { customer }
  }
}) => (
  <div className={styles.header}>
    <ContentLayout>
      <AntHeader>
        <div className={styles.branding}>
          <img alt="logo" src={logo} />
          <div className={styles.icon} />
          <div>{message}</div>
        </div>
        {customer && (
          <div className={styles.dropdown} data-test-el="username">
            <Initials customer={customer} />
            <Dropdown customer={customer} />
          </div>
        )}
      </AntHeader>
    </ContentLayout>
  </div>
);

export const Header = withAppConfig(HeaderComponent);
