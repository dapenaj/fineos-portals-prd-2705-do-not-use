import React from 'react';
import { List } from 'antd';

import styles from './empty-state.module.scss';

const { Item } = List;

export const EmptyState: React.FC = ({ children }) => (
  <Item>
    <div className={styles.wrapper}>{children}</div>
  </Item>
);
