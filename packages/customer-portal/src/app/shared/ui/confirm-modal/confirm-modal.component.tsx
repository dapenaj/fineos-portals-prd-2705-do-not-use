import React from 'react';
import { Modal, Icon, Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './confirm-modal.module.scss';

type ConfirmModalProps = {
  visible: boolean;
  onSubmitConfirm: () => void;
  onCancelConfirm: () => void;
  message: string;
  submitText?: string;
  cancelText?: string;
};

const MODAL_WIDTH = 400;

export const ConfirmModal: React.FC<ConfirmModalProps> = ({
  visible,
  onCancelConfirm,
  message,
  onSubmitConfirm,
  submitText = 'COMMON.CONFIRMATION.YES',
  cancelText = 'COMMON.CONFIRMATION.NO'
}) => (
  <Modal
    width={MODAL_WIDTH}
    visible={visible}
    onCancel={onCancelConfirm}
    footer={null}
  >
    <div className={styles.wrapper}>
      <div className={styles.message}>
        <Icon type="question-circle" className={styles.icon} />
        <FormattedMessage id={message} />
      </div>
      <div className={styles.actions}>
        <Button
          type="danger"
          onClick={onSubmitConfirm}
          data-test-el="confirmation-submit-button"
        >
          <FormattedMessage id={submitText} />
        </Button>
        <Button
          onClick={onCancelConfirm}
          data-test-el="confirmation-cancel-button"
        >
          <FormattedMessage id={cancelText} />
        </Button>
      </div>
    </div>
  </Modal>
);
