import React from 'react';
import { notification as AntNotification } from 'antd';
import { ApiError } from 'fineos-js-api-client';

import { NotificationAlert } from '../../types';
import { ApiErrorResponse } from '../api-error-response';
import { retrieveNotificationType } from '../../utils';

const getDescription = (content: any) => {
  if (content instanceof ApiError) {
    return <ApiErrorResponse error={content} />;
  } else if (typeof content === 'string') {
    return content;
  }

  return undefined;
};

export const showNotification = ({
  title,
  type,
  content,
  duration = 5
}: NotificationAlert) => {
  AntNotification[retrieveNotificationType(content, type)]({
    message: title,
    description: getDescription(content),
    duration,
    placement: 'topRight'
  });
};
