import React from 'react';
import classnames from 'classnames';

import styles from './property-item.module.scss';

type PropertyItemProps = {
  label: React.ReactNode;
  fullWidth?: boolean;
  additionalPadding?: boolean;
  padded?: boolean;
};

export const PropertyItem: React.FC<PropertyItemProps> = ({
  label,
  children,
  fullWidth,
  additionalPadding,
  padded = true
}) => (
  <div
    className={classnames(
      { [styles.wrapper]: padded },
      { [styles.fullWidth]: fullWidth },
      { [styles.additionalPadding]: additionalPadding }
    )}
  >
    <div className={styles.label} data-test-el="property-item-label">
      {label}
    </div>
    <div className={styles.value} data-test-el="property-item-value">
      {children ? children : '--'}
    </div>
  </div>
);
