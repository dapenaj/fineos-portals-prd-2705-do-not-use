import React from 'react';
import classnames from 'classnames';

import styles from './property-item.module.scss';

type InlinePropertyItemProps = {
  label: React.ReactNode;
  padded?: boolean;
};

export const InlinePropertyItem: React.FC<InlinePropertyItemProps> = ({
  label,
  children,
  padded = true
}) => (
  <div className={classnames(padded && styles.wrapper)}>
    <span className={styles.inlineLabel} data-test-el="property-item-label">
      {label}
    </span>
    <span className={styles.value} data-test-el="property-item-value">
      {children ? children : '--'}
    </span>
  </div>
);
