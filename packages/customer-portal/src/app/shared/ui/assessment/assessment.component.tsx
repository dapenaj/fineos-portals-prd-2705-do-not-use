import React from 'react';

import styles from './assessment.module.scss';

type AssessmentProps = {
  title: React.ReactNode;
};

export const Assessment: React.FC<AssessmentProps> = ({ title }) => (
  <div className={styles.wrapper}>
    <span className={styles.status}>{title}</span>
  </div>
);
