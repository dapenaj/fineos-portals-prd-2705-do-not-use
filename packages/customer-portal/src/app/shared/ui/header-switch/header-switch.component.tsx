import React from 'react';
import { Col, Row, Switch } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './header-switch.module.scss';

type HeaderSwitchProps = {
  onChange: (checked: boolean) => void;
  label: string;
  disabled?: boolean;
  defaultChecked?: boolean;
};

export const HeaderSwitch: React.FC<HeaderSwitchProps> = ({
  onChange,
  label,
  defaultChecked = true,
  disabled
}) => (
  <Row className={styles.rowMargin}>
    <Col>
      <Switch
        defaultChecked={defaultChecked}
        onChange={onChange}
        disabled={disabled}
        data-test-el="header-switch"
      />
      <span className={styles.marginLeft}>
        <FormattedMessage id={label} />
      </span>
    </Col>
  </Row>
);
