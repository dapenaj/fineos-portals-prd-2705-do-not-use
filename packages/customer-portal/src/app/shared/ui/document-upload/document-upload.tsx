import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Upload, Button } from 'antd';
import {
  UploadFileStatus,
  UploadChangeParam,
  UploadFile,
  RcFile
} from 'antd/lib/upload/interface';

import { Spinner } from '../spinner';
import { UploadDocument } from '../../types';
import {
  getFileExtension,
  isValidFileExtension,
  VALID_FILE_EXTENSIONS
} from '../../utils';

import { UploadInput } from './upload-input';
import styles from './document-upload.module.scss';

type DocumentUploadProps = {
  document: UploadDocument;
  loading: boolean;
  onCancel: () => void;
  onSubmit: (request: UploadDocument) => void;
};

type DocumentUploadState = {
  status?: UploadFileStatus;
  type?: string;
  file?: string;
  name?: string;
  validExtension: boolean;
  fileList: UploadFile[];
};

export class DocumentUpload extends React.Component<
  DocumentUploadProps,
  DocumentUploadState
> {
  state: DocumentUploadState = {
    validExtension: true,
    fileList: []
  };

  handleOnUploadChange = ({
    file,
    fileList
  }: UploadChangeParam<UploadFile>) => {
    const { status, name, originFileObj } = file;
    const reader = new FileReader();
    const extension = getFileExtension(status === 'removed' ? '' : name);
    // remove '.'
    const type = status === 'removed' ? '' : extension.substring(1);
    // get filename without extension
    const filename = name.slice(0, name.lastIndexOf('.'));

    this.setState({
      fileList,
      status: status,
      type: status === 'removed' ? '' : type,
      name: status === 'removed' ? '' : filename,
      validExtension: extension === '' ? true : isValidFileExtension(extension)
    });

    if (status !== 'uploading' && originFileObj) {
      reader.onload = () => {
        const result = reader.result as string;
        // only want the data after the type
        const data = result.split(',')[1];

        this.setState({
          file: status === 'removed' ? '' : data
        });
      };

      reader.readAsDataURL(originFileObj);
    }
  };

  handleUpload = () => {
    const {
      document: { id, evidence }
    } = this.props;

    const { type, file, name } = this.state;

    this.props.onSubmit({
      id,
      caseId: evidence.uploadCaseNumber,
      evidence,
      request: {
        documentType: evidence.name,
        fileName: name!,
        fileExtension: type!,
        description: name!,
        fileContentsText: file!
      }
    });

    // reset the upload
    this.setState({
      status: undefined,
      type: undefined,
      file: undefined,
      name: undefined,
      fileList: []
    });
  };

  handleBeforeUpload = (file: RcFile, fileList: RcFile[]) => {
    this.setState({ status: 'done' });
    return true;
  };

  /*
   * https://stackoverflow.com/questions/51514757/action-function-is-required-with-antd-upload-control-but-i-dont-need-it
   */
  overrideAction = (response: any) =>
    setTimeout(() => {
      response.onSuccess('ok');
    }, 0);

  render() {
    const { status, name, validExtension, file, fileList } = this.state;
    const { loading, onCancel } = this.props;

    const isLoading = loading || status === 'uploading';
    const uploadDisabled = isLoading || !file || !validExtension;

    return (
      <div className={styles.wrapper}>
        {!isLoading && <div>{name}</div>}

        <Upload
          accept={VALID_FILE_EXTENSIONS.join()}
          beforeUpload={this.handleBeforeUpload}
          customRequest={this.overrideAction}
          onChange={info => this.handleOnUploadChange(info)}
          fileList={fileList}
        >
          {isLoading && <Spinner />}

          {validExtension && status !== 'done' && !isLoading && <UploadInput />}
        </Upload>

        <div className={styles.controls}>
          {!validExtension && (
            <p className={styles.error}>
              <FormattedMessage id="COMMON.VALIDATION.FILE_EXTENSION" />
            </p>
          )}

          <Button
            type="primary"
            disabled={uploadDisabled}
            onClick={this.handleUpload}
          >
            <FormattedMessage id="UPLOAD.UPLOAD_DOCUMENT" />
          </Button>

          <Button
            type="link"
            onClick={onCancel}
            disabled={loading}
            data-test-el="cancel-document-upload-btn"
          >
            <FormattedMessage id="UPLOAD.CANCEL" />
          </Button>
        </div>
      </div>
    );
  }
}
