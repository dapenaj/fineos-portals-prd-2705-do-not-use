import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from 'antd';

import styles from './upload-input.module.scss';

export const UploadInput: React.FC = () => (
  <div className={styles.upload}>
    <FormattedMessage id="UPLOAD.DRAG_DOCUMENT" />

    <div className={styles.spacing}>
      <FormattedMessage id="UPLOAD.OR" />
    </div>

    <Button type="primary" data-test-el="browse-files">
      <FormattedMessage id="UPLOAD.BROWSE_FILES" />
    </Button>
  </div>
);
