import React from 'react';
import { Modal, Button } from 'antd';
import { FormattedMessage } from 'react-intl';

import { UploadDocumentError, AppConfig } from '../../../types';
import { withAppConfig } from '../../../contexts';
import documentReceived from '../../../../../assets/img/document-received.svg';
import documentFailed from '../../../../../assets/img/document-failed.svg';

import styles from './document-upload-error-modal.module.scss';

type DocumentUploadErrorProps = {
  showModal: boolean;
  config: AppConfig;
  error?: UploadDocumentError;
  onCancelModal: () => void;
};

export const DocumentUploadErrorModal: React.FC<DocumentUploadErrorProps> = ({
  showModal,
  config: {
    clientConfig: {
      documentUpload: { errorTitle, uploadError, receivedError }
    }
  },
  error,
  onCancelModal
}) => (
  <>
    {error !== undefined && (
      <Modal
        title={errorTitle}
        visible={showModal}
        onCancel={onCancelModal}
        footer={null}
      >
        <div className={styles.wrapper}>
          <span className={styles.margin}>
            {error === 'upload' ? uploadError : receivedError}
          </span>

          <img
            alt="document-upload-error"
            src={error === 'upload' ? documentFailed : documentReceived}
          />
        </div>

        <div>
          <Button
            className={styles.button}
            type="primary"
            onClick={onCancelModal}
          >
            <FormattedMessage id="SUPPORTING_EVIDENCE.CLOSE" />
          </Button>
        </div>
      </Modal>
    )}
  </>
);

export const DocumentUploadError = withAppConfig(DocumentUploadErrorModal);
