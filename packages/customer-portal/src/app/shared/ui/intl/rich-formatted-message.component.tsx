import React from 'react';
import { FormattedMessage } from 'react-intl';

/**
 * Allow to use some rich formatted messages
 *
 * Supported tags:
 * - p - for identifying paragraphs
 *
 * Implementation details can be found here:
 * https://github.com/formatjs/react-intl/blob/master/docs/Components.md#rich-text-formatting
 */
export const RichFormattedMessage: React.FC<FormattedMessage['props']> = ({
  values,
  ...restProps
}) => (
  <FormattedMessage
    {...restProps}
    values={{
      ...(values || {}),
      p: (msg: string) => <p>{msg}</p>
    }}
  />
);
