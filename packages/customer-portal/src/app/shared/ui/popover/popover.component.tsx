import React from 'react';
import { Popover as AntPopover, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import { getMessageForLocale } from '../../../i18n/utils';

import styles from './popover.module.scss';

type PopoverProps = {
  content: string | React.ReactNode;
  icon?: string;
  overlayStyle?: React.CSSProperties;
};

export const Popover: React.FC<PopoverProps> = ({
  content,
  icon = 'question-circle',
  overlayStyle = {
    width: '400px'
  }
}) => (
  <AntPopover
    placement="right"
    overlayStyle={overlayStyle}
    content={
      typeof content === 'string' &&
      getMessageForLocale('en', `POPOVER.${content}`) ? (
        <FormattedMessage id={`POPOVER.${content}`} />
      ) : (
        content
      )
    }
  >
    <span className={styles.icon}>
      <Icon type={icon} />
    </span>
  </AntPopover>
);
