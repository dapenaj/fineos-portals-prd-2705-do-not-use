import React from 'react';

import styles from './elevated-card.module.scss';

export const ElevatedCard: React.FC = ({ children }) => (
  <div className={styles.card}>{children}</div>
);
