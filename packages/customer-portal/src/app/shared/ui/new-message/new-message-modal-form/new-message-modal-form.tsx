import { withFormik } from 'formik';
import * as Yup from 'yup';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { NewMessageFormValue, NewMessageType } from '../new-message.type';

import { NewMessageModalFormView } from './new-message-modal-form.view';

type NewMessageModalFormProps = {
  type: NewMessageType;
  visible: boolean;
  notifications?: NotificationCaseSummary[];
  notification?: NotificationCaseSummary;
  onCancel: () => void;
  onSubmit: (value: NewMessageFormValue) => void;
};

const NewMessageModalFormSchema: Yup.ObjectSchema<Yup.Shape<
  object,
  NewMessageFormValue
>> = Yup.object().shape({
  subject: Yup.string()
    .required('COMMON.VALIDATION.REQUIRED')
    .min(5, 'COMMON.VALIDATION.AT_LEAST'),
  relatesTo: Yup.string().nullable(),
  caseId: Yup.string().when('relatesTo', (relatesTo: string, schema: any) =>
    !!relatesTo
      ? Yup.string()
          .nullable()
          .required('COMMON.VALIDATION.REQUIRED')
      : schema.nullable()
  ),
  narrative: Yup.string()
    .required('COMMON.VALIDATION.REQUIRED')
    .min(5, 'COMMON.VALIDATION.AT_LEAST')
});

const mapPropsToValues = ({ type, notification }: NewMessageModalFormProps) => {
  const defaultValues: NewMessageFormValue = {
    subject: '',
    relatesTo: null,
    caseId: null,
    narrative: ''
  };

  return type === 'recent'
    ? {
        ...defaultValues,
        relatesTo: notification!.notificationCaseId
      }
    : defaultValues;
};

export const NewMessageModalForm = withFormik<
  NewMessageModalFormProps,
  NewMessageFormValue
>({
  mapPropsToValues,
  validationSchema: NewMessageModalFormSchema,
  handleSubmit: (values, { props }) => props.onSubmit(values)
})(NewMessageModalFormView);
