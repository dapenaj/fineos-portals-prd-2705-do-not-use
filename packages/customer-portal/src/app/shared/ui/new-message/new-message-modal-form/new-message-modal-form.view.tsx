import React from 'react';
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps as IntlProps
} from 'react-intl';
import { Modal } from 'antd';
import { FormikProps } from 'formik';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { SelectOption } from '../../../../shared/types';
import { ConfirmModal } from '../../confirm-modal';
import { ModalFooter } from '../../modal-footer';
import { NotificationService } from '../../../services';
import { showNotification } from '../../notification';
import { NewMessageType, NewMessageFormValue } from '../new-message.type';

import { NewMessageForm } from './new-message-form';

type NewMessageModalFormViewProps = {
  type: NewMessageType;
  visible: boolean;
  notifications?: NotificationCaseSummary[];
  notification?: NotificationCaseSummary;
  onCancel: () => void;
  onSubmit: (value: NewMessageFormValue) => void;
} & FormikProps<NewMessageFormValue> &
  IntlProps;

type NewMessageModalFormViewState = {
  showConfirmModal: boolean;
  entitlements: SelectOption[];
  loading: boolean;
  selectedNotification: string | null;
};

export class NewMessageModalFormViewComponent extends React.Component<
  NewMessageModalFormViewProps,
  NewMessageModalFormViewState
> {
  state: NewMessageModalFormViewState = {
    showConfirmModal: false,
    entitlements: [],
    loading: false,
    selectedNotification: null
  };

  notificationService = NotificationService.getInstance();

  async componentDidUpdate(prevProps: Readonly<NewMessageModalFormViewProps>) {
    const { type, notification, visible } = this.props;

    if (visible && visible !== prevProps.visible) {
      if (type === 'recent') {
        this.setState(
          {
            selectedNotification: notification!.notificationCaseId
          },
          () => this.fetchEntitlements(notification!)
        );
      }
    }
  }

  fetchEntitlements = async (notification: NotificationCaseSummary) => {
    const {
      intl: { formatMessage }
    } = this.props;

    this.setState({ loading: true });

    try {
      const entitlements = await this.notificationService.fetchNotificationEntitlements(
        notification
      );

      this.setState({ entitlements });
    } catch (error) {
      const title = formatMessage({ id: 'MESSAGES.RELATES_TO_FETCH_ERROR' });

      showNotification({ title, type: 'error', content: error });
    } finally {
      this.setState({ loading: false });
    }
  };

  showConfirmModal = () =>
    this.setState({
      showConfirmModal: true
    });

  handleConfirmCancel = () => this.setState({ showConfirmModal: false });

  handleConfirmSubmit = () => {
    const { onCancel, resetForm } = this.props;

    this.setState({ showConfirmModal: false });
    resetForm();
    onCancel();
  };

  handleSubmit = () => {
    const { resetForm, onSubmit, values } = this.props;
    const { entitlements } = this.state;

    onSubmit({ ...values, entitlements });
    resetForm();
  };

  getRelatesToOptions = (): SelectOption[] => {
    const { type, notifications, notification } = this.props;

    return type === 'all'
      ? notifications!.map(n => ({
          text: n.notificationCaseId,
          value: n.notificationCaseId
        }))
      : [
          {
            text: notification!.notificationCaseId,
            value: notification!.notificationCaseId
          }
        ];
  };

  handleRelatesToChange = (notificationCaseId: string) => {
    const { notifications } = this.props;

    this.setState({ selectedNotification: notificationCaseId });

    const notification = notifications!.find(
      n => n.notificationCaseId === notificationCaseId
    );

    if (notification) {
      this.fetchEntitlements(notification);
    }

    this.props.setFieldValue('caseId', undefined);
  };

  render() {
    const {
      visible,
      type,
      onCancel,
      onSubmit,
      notification,
      notifications,
      ...formikProps
    } = this.props;

    const {
      showConfirmModal,
      entitlements,
      loading,
      selectedNotification
    } = this.state;

    const relatesToOptions = this.getRelatesToOptions();

    return (
      <>
        <Modal
          title={<FormattedMessage id="MESSAGES.NEW_MESSAGE" />}
          visible={visible}
          data-test-el="new-message-modal"
          onCancel={this.showConfirmModal}
          footer={
            <ModalFooter
              submitText={'MESSAGES.SEND'}
              cancelText={'MESSAGES.CANCEL'}
              isDisabled={!formikProps.isValid}
              onCancelClick={this.showConfirmModal}
              onSubmitClick={this.handleSubmit}
            />
          }
        >
          <NewMessageForm
            type={type}
            entitlements={entitlements}
            loading={loading}
            relatesToOptions={relatesToOptions}
            selectedNotification={selectedNotification}
            notification={notification}
            notifications={notifications}
            onRelatesToChange={this.handleRelatesToChange}
            {...formikProps}
          />
        </Modal>

        <ConfirmModal
          visible={showConfirmModal}
          submitText={'MESSAGES.CONFIRM'}
          cancelText={'MESSAGES.CANCEL'}
          message={'MESSAGES.CANCEL_INFO'}
          onSubmitConfirm={this.handleConfirmSubmit}
          onCancelConfirm={this.handleConfirmCancel}
        />
      </>
    );
  }
}

export const NewMessageModalFormView = injectIntl(
  NewMessageModalFormViewComponent
);
