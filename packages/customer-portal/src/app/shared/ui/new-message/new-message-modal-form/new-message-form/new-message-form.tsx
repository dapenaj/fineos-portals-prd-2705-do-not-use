import React from 'react';
import { FormikProps, Form } from 'formik';
import { NotificationCaseSummary } from 'fineos-js-api-client';

import { TextInput, FormLabel, TextAreaInput } from '../../../../ui';
import { SelectOption } from '../../../../types';
import { EntitlementsSelect } from '../entitlements-select';
import { NewMessageType, NewMessageFormValue } from '../../new-message.type';

import styles from './new-message-form.module.scss';

type NewMessageFormProps = {
  type: NewMessageType;
  relatesToOptions: SelectOption[];
  entitlements: SelectOption[];
  loading: boolean;
  selectedNotification: string | null;
  notification?: NotificationCaseSummary;
  notifications?: NotificationCaseSummary[];
  onRelatesToChange: (notificationCaseId: string) => void;
} & FormikProps<NewMessageFormValue>;

export const NewMessageForm: React.FC<NewMessageFormProps> = ({
  type,
  relatesToOptions,
  notification,
  onRelatesToChange,
  selectedNotification,
  entitlements,
  loading,
  ...formikProps
}) => (
  <Form name="new-messages-form" className={styles.form}>
    <TextInput
      name="subject"
      label={<FormLabel required={true} label="MESSAGES.SUBJECT" />}
      {...formikProps}
    />

    <EntitlementsSelect
      type={type}
      relatesToOptions={relatesToOptions}
      entitlements={entitlements}
      loading={loading}
      onRelatesToChange={onRelatesToChange}
      selectedNotification={selectedNotification}
      {...formikProps}
    />

    <TextAreaInput
      name="narrative"
      label={<FormLabel required={true} label="MESSAGES.MESSAGE" />}
      {...formikProps}
    />
  </Form>
);
