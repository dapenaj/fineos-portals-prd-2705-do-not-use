import React from 'react';
import { FormikProps } from 'formik';
import { isEmpty as _isEmpty } from 'lodash';

import { SelectOption } from '../../../../types';
import { SelectInput, FormLabel } from '../../../form';
import { NewMessageType, NewMessageFormValue } from '../../new-message.type';

type EntitlementsSelectProps = {
  type: NewMessageType;
  selectedNotification: string | null;
  relatesToOptions: SelectOption[];
  entitlements: SelectOption[];
  loading: boolean;
  onRelatesToChange: (notificationCaseId: string) => void;
} & FormikProps<NewMessageFormValue>;

export const EntitlementsSelect: React.FC<EntitlementsSelectProps> = ({
  type,
  loading,
  onRelatesToChange,
  selectedNotification,
  entitlements,
  relatesToOptions,
  ...formikProps
}) => (
  <>
    {type === 'all' && (
      <SelectInput
        name="relatesTo"
        label={<FormLabel label="MESSAGES.RELATES_TO" />}
        options={!_isEmpty(relatesToOptions) ? relatesToOptions : []}
        loading={loading}
        isDisabled={loading}
        onChange={onRelatesToChange}
        allowClear={true}
        {...formikProps}
      />
    )}

    {!!selectedNotification && (
      <SelectInput
        name="caseId"
        label={
          <FormLabel
            required={true}
            label={
              type === 'all'
                ? 'MESSAGES.ENTITLEMENTS_BENEFITS'
                : 'MESSAGES.RELATES_TO'
            }
          />
        }
        options={entitlements}
        loading={loading}
        isDisabled={loading}
        {...formikProps}
      />
    )}
  </>
);
