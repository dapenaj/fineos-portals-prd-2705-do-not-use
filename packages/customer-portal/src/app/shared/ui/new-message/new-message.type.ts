import { SelectOption } from './../../types';

export type NewMessageType = 'recent' | 'all';

export type NewMessageFormValue = {
  subject: string;
  narrative: string;
  caseId: string | null;
  relatesTo: string | null;
  entitlements?: SelectOption[];
};
