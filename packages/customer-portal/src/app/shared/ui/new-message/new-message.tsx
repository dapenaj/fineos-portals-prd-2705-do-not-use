import React from 'react';
import {
  NotificationCaseSummary,
  NewMessage as NewMessageRequest
} from 'fineos-js-api-client';
import {
  injectIntl,
  WrappedComponentProps as IntlProps,
  FormattedMessage
} from 'react-intl';
import { connect } from 'react-redux';
import { Button } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';

import { addMessage } from '../../../store/messages/actions';
import {
  getNewMessageCaseId,
  getNewMessageSubject
} from '../../../shared/utils';

import { NewMessageModalForm } from './new-message-modal-form';
import { NewMessageType, NewMessageFormValue } from './new-message.type';

const mapDispatchToProps = {
  addMessage
};

type DispatchProps = typeof mapDispatchToProps;

type NewMessageProps = {
  type: NewMessageType;
  disabled?: boolean;
  notifications?: NotificationCaseSummary[];
  notification?: NotificationCaseSummary;
  onAddMessage?: (message: NewMessageRequest) => void;
  onMessageSent?: () => void;
} & IntlProps &
  DispatchProps;

type NewMessageState = {
  showModal: boolean;
};

export class NewMessageComponent extends React.Component<
  NewMessageProps,
  NewMessageState
> {
  state: NewMessageState = {
    showModal: false
  };

  showNewMessage = () => this.setState({ showModal: true });

  handleCancel = () => this.setState({ showModal: false });

  handleSubmit = ({
    subject,
    narrative,
    caseId,
    entitlements
  }: NewMessageFormValue) => {
    this.setState({ showModal: false });

    const caseEntitlements = !_isEmpty(entitlements) ? entitlements! : [];

    this.sendMessage({
      subject: getNewMessageSubject(caseEntitlements, caseId!, subject),
      narrative,
      caseId: !!caseId ? getNewMessageCaseId(caseId) : undefined
    });
  };

  sendMessage = (message: NewMessageRequest) => {
    const {
      onAddMessage,
      onMessageSent,
      intl: { formatMessage }
    } = this.props;

    if (onAddMessage) {
      onAddMessage(message);
    } else {
      const title = formatMessage({
        id: 'MESSAGES.MESSAGE_SENT'
      });

      this.props.addMessage(message, { title });

      if (onMessageSent) {
        onMessageSent();
      }
    }
  };

  render() {
    const { type, disabled, notification, notifications } = this.props;
    const { showModal } = this.state;

    return (
      <div>
        <Button
          type={type === 'recent' ? 'link' : 'primary'}
          data-test-el="new-message-button"
          onClick={this.showNewMessage}
          disabled={disabled}
        >
          <FormattedMessage id="MESSAGES.NEW_MESSAGE" />
        </Button>

        <NewMessageModalForm
          type={type}
          visible={showModal}
          notifications={notifications}
          notification={notification}
          onCancel={this.handleCancel}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

export const NewMessage = connect(
  null,
  mapDispatchToProps
)(injectIntl(NewMessageComponent));
