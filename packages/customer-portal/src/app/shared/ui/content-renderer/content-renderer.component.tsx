import React from 'react';

import { Spinner } from '../spinner';
import { EmptyList } from '../empty-list';

type ContentRendererProps = {
  loading: boolean;
  isEmpty: boolean;
  emptyTitle?: React.ReactNode;
  withBorder?: boolean;
  'data-test-el'?: string;
};

export const ContentRenderer: React.FC<ContentRendererProps> = ({
  loading,
  isEmpty,
  children,
  emptyTitle,
  withBorder,
  ...props
}) => (
  <>
    {loading && <Spinner />}

    {!loading && (
      <>
        {!isEmpty && (
          <div data-test-el={props['data-test-el'] || 'content-renderer'}>
            {children}
          </div>
        )}

        {isEmpty && emptyTitle && (
          <EmptyList withBorder={withBorder}>{emptyTitle}</EmptyList>
        )}
      </>
    )}
  </>
);
