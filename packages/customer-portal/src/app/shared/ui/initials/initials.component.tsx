import React from 'react';
import { Avatar } from 'antd';
import { Customer } from 'fineos-js-api-client';

import { formatInitial } from '../../utils';

type InitialsProps = {
  customer: Customer | null;
};
const getInitials = (customer: Customer | null) =>
  customer
    ? `${formatInitial(customer.firstName)}${formatInitial(customer.lastName)}`
    : '';

export const Initials: React.FC<InitialsProps> = ({ customer }) => (
  <Avatar size="default">{getInitials(customer)}</Avatar>
);
