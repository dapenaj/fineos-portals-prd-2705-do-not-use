import React from 'react';

import styles from './panel-footer.module.scss';

type PanelFooterProps = {
  leftAction: React.ReactNode;
  rightAction: React.ReactNode;
};

export const PanelFooter: React.FC<PanelFooterProps> = ({
  leftAction,
  rightAction
}) => (
  <div className={styles.footer}>
    <div>{leftAction}</div>
    <div>{rightAction}</div>
  </div>
);
