import React from 'react';
import { Icon, Badge } from 'antd';

import styles from './panel-header.module.scss';

type HeaderProps = {
  badgeDot?: boolean;
  iconType: string;
  title: React.ReactNode;
  notification?: React.ReactNode;
};

export const PanelHeader: React.FC<HeaderProps> = ({
  badgeDot,
  iconType,
  title,
  notification
}) => (
  <div className={styles.header}>
    <div>
      <span className={styles.badge}>
        <Badge dot={badgeDot}>
          <Icon type={iconType} className={styles.icon} />
        </Badge>
      </span>
      <span className={styles.text}>{title}</span>
    </div>
    <div>{notification}</div>
  </div>
);
