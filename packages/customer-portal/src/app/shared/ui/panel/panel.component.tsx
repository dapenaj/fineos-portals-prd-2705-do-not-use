import React from 'react';
import classnames from 'classnames';

import styles from './panel.module.scss';

type PanelProps = {
  className?: string;
};

export const Panel: React.FC<PanelProps> = ({ children, className }) => (
  <div className={classnames(styles.panel, className)}>{children}</div>
);
