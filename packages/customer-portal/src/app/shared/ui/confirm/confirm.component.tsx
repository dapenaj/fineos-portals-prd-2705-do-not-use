import React from 'react';
import { Popconfirm } from 'antd';
import { FormattedMessage } from 'react-intl';
import { PopconfirmProps } from 'antd/lib/popconfirm';

import styles from './confirm.module.scss';

export const Confirm: React.FC<PopconfirmProps> = ({
  children,
  okText,
  cancelText,
  ...props
}) => (
  <Popconfirm
    {...props}
    overlayClassName={styles.confirmTooltip}
    okText={okText || <FormattedMessage id="COMMON.CONFIRMATION.YES" />}
    cancelText={cancelText || <FormattedMessage id="COMMON.CONFIRMATION.NO" />}
  >
    {children}
  </Popconfirm>
);
