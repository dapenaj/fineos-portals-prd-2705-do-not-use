import React from 'react';
import { message } from 'antd';

import { AntType } from '../../types';

type ToastProps = {
  type: AntType;
  title: React.ReactNode;
  duration?: number;
};

export const showToast = ({ title, duration, type }: ToastProps) => {
  message[type](title, duration);
};
