import React from 'react';
import { FormattedNumber } from 'react-intl';

type MoneyProps = {
  amount: number;
};

export const Money: React.FC<MoneyProps> = ({ amount }) => (
  // for now it's not clear from where currency should come, it's definitely should be related to locale
  // so hardcode for now

  <FormattedNumber
    value={amount}
    // as here style is logical prop of FormattedNumber, we don't want to get warning about react/style-prop-object
    // eslint-disable-next-line
    style="currency"
    currency="USD"
    minimumFractionDigits={2}
    maximumFractionDigits={2}
  />
);
