import React from 'react';

import { KeyValue } from '../../../types';

type ApiErrorJsonRendererProps = { errors: KeyValue[] };

export const ApiErrorJsonRenderer: React.FC<ApiErrorJsonRendererProps> = ({
  errors
}) => (
  <ul>
    {errors.map(({ key, value }, index) => (
      <li key={index}>
        <span>
          {key && <strong>{key}:</strong>} {value}
        </span>
      </li>
    ))}
  </ul>
);
