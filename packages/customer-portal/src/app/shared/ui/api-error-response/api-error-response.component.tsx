import React from 'react';
import { ApiError } from 'fineos-js-api-client';

import { retrieveErrorMessages } from '../../utils';

import { ApiErrorJsonRenderer } from './api-error-json-renderer';

type ApiErrorResponseProps = { error: ApiError };

export const ApiErrorResponse: React.FC<ApiErrorResponseProps> = ({
  error: { json, message }
}) =>
  !json ? (
    <>{message}</>
  ) : (
    <ApiErrorJsonRenderer errors={retrieveErrorMessages(json)} />
  );
