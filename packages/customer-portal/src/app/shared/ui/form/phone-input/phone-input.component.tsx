import React from 'react';
import classnames from 'classnames';
import { Col } from 'antd';

import { TextInput } from '../text-input';
import { DefaultInputProps } from '../form.type';
import { FormLabel } from '../form-label';
import { Row } from '../../row';

import styles from './phone-input.module.scss';

export const PhoneInput: React.FC<DefaultInputProps> = ({ name, ...props }) => (
  <div className={styles.wrapper} data-test-el={props['data-test-el']}>
    <Row>
      <Col xs={24}>
        <div className={styles.title}>
          <FormLabel label="INTAKE.YOUR_REQUEST.LABELS.PHONE_NUMBER" />
        </div>
      </Col>
      <Col xs={24} sm={12} md={6}>
        <div className={classnames(styles.input, styles.smallInput)}>
          <TextInput
            {...props}
            name={`${name}.intCode`}
            label={<FormLabel label="INTAKE.YOUR_REQUEST.LABELS.INTL_CODE" />}
          />
        </div>
      </Col>
      <Col xs={24} sm={12} md={6}>
        <div className={classnames(styles.input, styles.smallInput)}>
          <TextInput
            {...props}
            name={`${name}.areaCode`}
            label={
              <FormLabel
                label="INTAKE.YOUR_REQUEST.LABELS.AREA_CODE"
                required={true}
              />
            }
          />
        </div>
      </Col>
      <Col xs={24} sm={24} md={12}>
        <div className={classnames(styles.input)}>
          <TextInput
            {...props}
            name={`${name}.telephoneNo`}
            label={
              <FormLabel
                label="INTAKE.YOUR_REQUEST.LABELS.PHONE_NUMBER"
                required={true}
              />
            }
          />
        </div>
      </Col>
    </Row>
  </div>
);
