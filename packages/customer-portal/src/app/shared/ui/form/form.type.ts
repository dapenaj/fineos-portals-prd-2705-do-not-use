import { FormikProps } from 'formik';

import { ColPropsValue } from '../../../shared/types';

export type DefaultInputProps = {
  name: string;
  label?: React.ReactNode;
  isDisabled?: boolean;
  'data-test-el'?: string;
  labelCol?: ColPropsValue;
  wrapperCol?: ColPropsValue;
  placeholder?: React.ReactNode;
} & FormikProps<any>;
