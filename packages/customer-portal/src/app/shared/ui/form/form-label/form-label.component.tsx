import React from 'react';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';
import { Icon, Popover } from 'antd';

import { RichFormattedMessage } from '../../intl';

import styles from './form-label.module.scss';

type FormLabelProps = {
  label: string;
  required?: boolean;
  values?: any;
  hintTooltip?: string;
};

export const FormLabel: React.FC<FormLabelProps> = ({
  label,
  required,
  values,
  hintTooltip
}) => (
  <div data-test-el="form-label">
    <span className={classnames(required && styles.isRequired)}>
      <FormattedMessage id={label} values={values} />
    </span>
    {hintTooltip && (
      <>
        {' '}
        <Popover
          content={<RichFormattedMessage id={hintTooltip} />}
          trigger="hover"
          overlayClassName={styles.hintOverlay}
        >
          <Icon type="question-circle" />
        </Popover>
      </>
    )}
  </div>
);
