import React from 'react';
import { Form, DatePicker } from 'antd';
import { DatePickerProps } from 'antd/lib/date-picker/interface';
import { getIn, FieldProps, Field } from 'formik';
import moment, { Moment } from 'moment';

import {
  formatValidationMessage,
  formatDateForApi,
  DISPLAY_DATE_FORMAT
} from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const { Item } = Form;

export type FormikDatePickerInputProps = DefaultFormikFieldProps &
  DatePickerProps & {
    format?: string;
    onChange?: (date: Moment, dateString: string) => void;
  };

export const FormikDatePickerInput = ({
  name,
  validate,
  label,
  isDisabled,
  onChange,
  labelCol,
  wrapperCol,
  format = DISPLAY_DATE_FORMAT,
  ...restProps
}: FormikDatePickerInputProps) => (
  <Field
    name={name}
    validate={validate}
    data-test-el="formik-date-picker-input"
  >
    {({
      field: { value },
      form: { errors, touched, setFieldValue, setFieldTouched }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);
      return (
        <div className={styles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
            labelCol={labelCol}
            wrapperCol={wrapperCol}
          >
            <DatePicker
              name={name}
              value={value ? moment(value) : undefined}
              onChange={(date, dateString) => {
                setFieldValue(name, date ? formatDateForApi(date) : null);
                if (onChange) {
                  onChange(date, dateString);
                }
              }}
              onOpenChange={isOpen => setFieldTouched(name, true)}
              format={format}
              disabled={isDisabled}
              {...restProps}
            />
          </Item>
        </div>
      );
    }}
  </Field>
);
