import * as React from 'react';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';

import {
  FormikNumberInputProps,
  FormikNumberInput
} from '../formik-number-input';

export type FormikMoneyInputProps = FormikNumberInputProps & IntlProps;

/**
 * For now, this component only supports USD $
 * Ideally the input value would be formatted (and validated) according to the user's language setting
 */
export const FormikMoneyInput = injectIntl(
  ({ intl, ...restProps }: FormikMoneyInputProps) => {
    // Need to grab the currency sumbol from react-intl
    const symbolNumberPart = intl
      .formatNumberToParts(0, { style: 'currency', currency: 'USD' })
      .find(({ type }) => type === 'currency');
    const symbol = symbolNumberPart ? symbolNumberPart.value : '';

    return (
      <FormikNumberInput
        formatter={val => `${symbol}${val}`}
        parser={val =>
          !!val ? val.replace(symbol, '').replace(/\$\s?/g, '') : 0
        }
        {...restProps}
      />
    );
  }
);
