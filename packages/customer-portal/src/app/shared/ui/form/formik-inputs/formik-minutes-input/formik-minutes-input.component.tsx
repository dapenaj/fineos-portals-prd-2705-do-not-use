import React from 'react';
import { Form, Select } from 'antd';
import { SelectProps, SelectValue } from 'antd/lib/select';
import { Field, FieldProps, getIn } from 'formik';
import { range as _range } from 'lodash';

import { SelectOption } from '../../../../types';
import { formatValidationMessage } from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const MINUTES: SelectOption[] = _range(0, 60).map(value => ({
  value,
  text: `${value}`
}));

const { Item } = Form;
const { Option } = Select;

export type FormikMinutesInputProps = DefaultFormikFieldProps & SelectProps;

export const FormikMinutesInput = ({
  name,
  validate,
  label,
  isDisabled,
  ...restProps
}: FormikMinutesInputProps) => (
  <Field name={name} validate={validate} data-test-el="formik-minutes-input">
    {({
      field: { value },
      form: { errors, touched, setFieldValue, setFieldTouched }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);
      return (
        <div className={styles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
          >
            <Select
              showSearch={true}
              value={value}
              disabled={isDisabled}
              onChange={(v: SelectValue) => setFieldValue(name, v)}
              onBlur={() => setFieldTouched(name)}
              data-test-el="form-select"
              {...restProps}
            >
              {MINUTES.map(({ value: val, text }, index) => (
                <Option key={index} value={val} className="form-select-option">
                  {text}
                </Option>
              ))}
            </Select>
          </Item>
        </div>
      );
    }}
  </Field>
);
