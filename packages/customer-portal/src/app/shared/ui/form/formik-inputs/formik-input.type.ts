import { SelectOption, ColPropsValue } from '../../../../shared/types';

export interface DefaultFormikFieldProps {
  name: string;
  validate?: (value: any) => undefined | string | Promise<any>;
  label?: React.ReactNode;
  isDisabled?: boolean;
  options?: SelectOption[];
  labelCol?: ColPropsValue;
  wrapperCol?: ColPropsValue;
}
