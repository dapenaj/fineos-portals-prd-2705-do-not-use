import React from 'react';
import { Form, DatePicker } from 'antd';
import { RangePickerProps } from 'antd/lib/date-picker/interface';
import { getIn, FieldProps, Field } from 'formik';
import moment from 'moment';

import {
  formatValidationMessage,
  formatDateForApi,
  DISPLAY_DATE_FORMAT
} from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const { Item } = Form;
const { RangePicker } = DatePicker;

export type FormikDateRangeInputProps = DefaultFormikFieldProps &
  RangePickerProps & {
    format?: string;
  };

export const FormikDateRangeInput = ({
  name,
  validate,
  label,
  isDisabled,
  format = DISPLAY_DATE_FORMAT,
  labelCol,
  wrapperCol,
  ...restProps
}: FormikDateRangeInputProps) => (
  <Field name={name} validate={validate} data-test-el="formik-date-range-input">
    {({
      field: { value },
      form: { errors, touched, setFieldValue, setFieldTouched }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);
      return (
        <div className={styles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
            labelCol={labelCol}
            wrapperCol={wrapperCol}
          >
            <RangePicker
              name={name}
              value={value ? [moment(value[0]), moment(value[1])] : undefined}
              onChange={dates => {
                setFieldValue(name, [
                  dates[0] ? formatDateForApi(dates[0]) : dates[0],
                  dates[1] ? formatDateForApi(dates[1]) : dates[1]
                ]);
                setFieldTouched(name, true);
              }}
              format={format}
              disabled={isDisabled}
              {...restProps}
            />
          </Item>
        </div>
      );
    }}
  </Field>
);
