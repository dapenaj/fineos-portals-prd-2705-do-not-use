import React from 'react';
import { Form, Select } from 'antd';
import { SelectProps, SelectValue } from 'antd/lib/select';
import { Field, FieldProps, getIn } from 'formik';

import { formatValidationMessage } from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const { Item } = Form;
const { Option } = Select;

export type FormikSelectInputProps = DefaultFormikFieldProps & {
  onChange?: (value: SelectValue) => void;
} & SelectProps;

export const FormikSelectInput = ({
  name,
  validate,
  label,
  isDisabled,
  options,
  onChange,
  wrapperCol,
  labelCol,
  ...restProps
}: FormikSelectInputProps) => (
  <Field name={name} validate={validate} data-test-el="formik-select-input">
    {({
      field: { value },
      form: { errors, touched, setFieldValue, setFieldTouched }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);
      return (
        <div className={styles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            labelCol={labelCol}
            wrapperCol={wrapperCol}
            label={label}
          >
            <Select
              showSearch={true}
              disabled={isDisabled}
              value={value}
              onChange={(v: SelectValue) => {
                if (onChange) {
                  onChange(v);
                }
                setFieldValue(name, v);
              }}
              onBlur={() => setFieldTouched(name)}
              data-test-el="form-select"
              {...restProps}
            >
              {options &&
                options.map(({ value: val, text }, index) => (
                  <Option
                    key={index}
                    value={val}
                    className="form-select-option"
                  >
                    {text}
                  </Option>
                ))}
            </Select>
          </Item>
        </div>
      );
    }}
  </Field>
);
