import * as React from 'react';
import { Radio, Form } from 'antd';
import { Group } from 'antd/lib/radio';
import { RadioGroupProps } from 'antd/lib/radio/interface';
import { Field, FieldProps, getIn } from 'formik';
import { FormattedMessage } from 'react-intl';

import { formatValidationMessage } from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import formStyles from '../../form-input.module.scss';

import styles from './formik-radio-group-input.module.scss';

const { Item } = Form;

export type FormikRadioGroupInputProps = DefaultFormikFieldProps &
  RadioGroupProps & {
    radioOptions: string[];
    disabledOptions?: string[];
    optionPrefix?: string;
    renderPopover?: (index: number) => React.ReactNode;
  };

export const FormikRadioGroupInput = ({
  name,
  validate,
  label,
  isDisabled,
  radioOptions,
  disabledOptions,
  optionPrefix,
  onChange,
  renderPopover,
  labelCol,
  wrapperCol,
  ...restProps
}: FormikRadioGroupInputProps) => (
  <Field
    name={name}
    validate={validate}
    data-test-el="formik-radio-group-input"
  >
    {({
      field: { value },
      form: { errors, touched, setFieldValue }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);
      return (
        <div className={formStyles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
            labelCol={labelCol}
            wrapperCol={wrapperCol}
          >
            <Group
              name={name}
              value={value}
              onChange={e => {
                setFieldValue(name, e.target.value);
                if (onChange) {
                  onChange(e);
                }
              }}
              disabled={isDisabled}
              size="large"
              {...restProps}
            >
              {radioOptions.map((option, index) => (
                <Radio
                  className={styles.radioInput}
                  style={{ display: 'flex' }}
                  value={option}
                  disabled={
                    disabledOptions && !!disabledOptions.find(v => v === option)
                  }
                  key={index}
                >
                  <div className={styles.radioInputLabel}>
                    <FormattedMessage id={`${optionPrefix}${option}`} />
                    {renderPopover && renderPopover(index)}
                  </div>
                </Radio>
              ))}
            </Group>
          </Item>
        </div>
      );
    }}
  </Field>
);
