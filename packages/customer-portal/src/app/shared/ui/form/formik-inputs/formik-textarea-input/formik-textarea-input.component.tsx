import * as React from 'react';
import { Form, Input } from 'antd';
import { TextAreaProps } from 'antd/lib/input';
import { Field, FieldProps, getIn } from 'formik';

import { formatValidationMessage } from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const { Item } = Form;
const { TextArea } = Input;

export type FormikTextAreaInputProps = DefaultFormikFieldProps & TextAreaProps;

export const FormikTextAreaInput = ({
  name,
  validate,
  label,
  isDisabled,
  labelCol,
  wrapperCol,
  ...restProps
}: FormikTextAreaInputProps) => (
  <Field name={name} validate={validate} data-test-el="formik-textarea-input">
    {({
      field: { value, onChange, onBlur },
      form: { errors, touched }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);

      return (
        <div className={styles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
            labelCol={labelCol}
            wrapperCol={wrapperCol}
          >
            <TextArea
              name={name}
              value={value}
              onChange={onChange}
              onBlur={onBlur}
              {...restProps}
            />
          </Item>
        </div>
      );
    }}
  </Field>
);
