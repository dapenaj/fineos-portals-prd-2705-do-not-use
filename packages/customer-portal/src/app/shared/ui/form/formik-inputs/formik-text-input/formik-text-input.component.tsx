import * as React from 'react';
import { Form, Input } from 'antd';
import { InputProps } from 'antd/lib/input';
import { Field, FieldProps, getIn } from 'formik';

import { formatValidationMessage } from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const { Item } = Form;

export type FormikTextInputProps = DefaultFormikFieldProps & InputProps;

export const FormikTextInput = ({
  name,
  validate,
  label,
  isDisabled,
  labelCol,
  wrapperCol,
  ...restProps
}: FormikTextInputProps) => (
  <Field name={name} validate={validate} data-test-el="formik-text-input">
    {({
      field: { value, onChange, onBlur },
      form: { errors, touched }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);

      return (
        <div className={styles.wrapper}>
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
            labelCol={labelCol}
            wrapperCol={wrapperCol}
          >
            <Input
              name={name}
              value={value}
              onChange={onChange}
              onBlur={onBlur}
              disabled={isDisabled}
              {...restProps}
            />
          </Item>
        </div>
      );
    }}
  </Field>
);
