import * as React from 'react';
import { omit as _omit } from 'lodash';
import { Form, InputNumber } from 'antd';
import { InputNumberProps } from 'antd/lib/input-number';
import { Field, FieldProps, getIn } from 'formik';

import { formatValidationMessage } from '../../../../utils';
import { DefaultFormikFieldProps } from '../formik-input.type';
import styles from '../../form-input.module.scss';

const { Item } = Form;

export type FormikNumberInputProps = DefaultFormikFieldProps &
  InputNumberProps & {
    'data-test-el'?: string;
  };

export const FormikNumberInput = ({
  name,
  validate,
  label,
  isDisabled,
  labelCol,
  wrapperCol,
  ...restProps
}: FormikNumberInputProps) => (
  <Field name={name} validate={validate} data-test-el="formik-number-input">
    {({
      field: { value, onBlur },
      form: { errors, touched, setFieldValue }
    }: FieldProps) => {
      const validation = getIn(errors, name);
      const wasTouched = getIn(touched, name);
      return (
        <div
          className={styles.wrapper}
          data-test-el={restProps['data-test-el']}
        >
          <Item
            hasFeedback={Boolean(validation)}
            validateStatus={wasTouched && validation && 'error'}
            help={
              wasTouched && validation && formatValidationMessage(validation)
            }
            label={label}
            labelCol={labelCol}
            wrapperCol={wrapperCol}
          >
            <InputNumber
              name={name}
              value={value}
              onChange={v => setFieldValue(name, v)}
              onBlur={onBlur}
              {..._omit(restProps, 'data-test-el')}
            />
          </Item>
        </div>
      );
    }}
  </Field>
);
