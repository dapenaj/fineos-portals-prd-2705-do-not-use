import React from 'react';
import { Form, Select } from 'antd';
import { getIn } from 'formik';

import styles from '../form-input.module.scss';
import { formatValidationMessage } from '../../../utils';
import { DefaultInputProps } from '../form.type';
import { SelectOption } from '../../../types';
import { getMessageForLocale } from '../../../../i18n/utils';

type SelectInputProps = DefaultInputProps & {
  options: SelectOption[];
  loading?: boolean;
  allowClear?: boolean;
  'data-test-el'?: string;
  onChange?: (value: any) => void;
};
const { Item } = Form;
const { Option } = Select;

export const SelectInput: React.FC<SelectInputProps> = ({
  name,
  label,
  errors,
  touched,
  values,
  setFieldValue,
  options,
  isDisabled,
  setFieldTouched,
  onChange,
  placeholder,
  loading,
  allowClear,
  ...props
}) => {
  const validation = getIn(errors, name);
  const wasTouched = getIn(touched, name);
  const inputValue = getIn(values, name);
  return (
    <div className={styles.wrapper} data-test-el={props['data-test-el']}>
      <Item
        hasFeedback={Boolean(validation)}
        validateStatus={wasTouched && validation && 'error'}
        help={
          Boolean(wasTouched && validation) &&
          formatValidationMessage(validation)
        }
        label={label}
      >
        <Select
          showSearch={true}
          value={inputValue}
          disabled={isDisabled}
          onChange={(value: any) => {
            if (onChange) {
              onChange(value);
            }
            setFieldValue(name, value);
          }}
          onBlur={() => setFieldTouched(name)}
          placeholder={placeholder}
          loading={loading}
          allowClear={allowClear}
          data-test-el="form-select"
        >
          {options.map(({ value, text }, index) => (
            <Option key={index} value={value} className="form-select-option">
              {getMessageForLocale('en', text as string)
                ? getMessageForLocale('en', text as string)
                : text}
            </Option>
          ))}
        </Select>
      </Item>
    </div>
  );
};
