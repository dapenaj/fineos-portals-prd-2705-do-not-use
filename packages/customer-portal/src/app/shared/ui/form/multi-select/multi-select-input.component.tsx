import React from 'react';
import { Form, Select } from 'antd';
import { getIn } from 'formik';
import { FormattedMessage } from 'react-intl';

import styles from '../form-input.module.scss';
import { formatValidationMessage } from '../../../utils';
import { DefaultInputProps } from '../form.type';
import { OptionGroup } from '../../../types';

type MultiSelectInputProps = DefaultInputProps & {
  optionGroups: OptionGroup[];
  'data-test-el'?: string;
};

const { Item } = Form;
const { OptGroup, Option } = Select;

export const MultiSelectInput: React.FC<MultiSelectInputProps> = ({
  name,
  label,
  errors,
  touched,
  values,
  setFieldValue,
  optionGroups,
  isDisabled,
  setFieldTouched,
  ...props
}) => {
  const validation = getIn(errors, name);
  const wasTouched = getIn(touched, name);
  const inputValue = getIn(values, name);

  return (
    <div className={styles.wrapper} data-test-el={props['data-test-el']}>
      <Item
        hasFeedback={Boolean(validation)}
        validateStatus={wasTouched && validation && 'error'}
        help={
          Boolean(wasTouched && validation) &&
          formatValidationMessage(validation)
        }
        label={label}
      >
        <Select
          mode="multiple"
          showSearch={true}
          value={inputValue}
          disabled={isDisabled}
          onChange={(value: any) => setFieldValue(name, value)}
          onBlur={() => setFieldTouched(name)}
          data-test-el="form-select"
        >
          {optionGroups.map(({ group, options }, groupIndex) => (
            <OptGroup label={<FormattedMessage id={group} />} key={groupIndex}>
              {options.map(({ value, text }, optionIndex) => (
                <Option
                  key={optionIndex}
                  value={value}
                  className="form-select-option"
                >
                  <FormattedMessage id={text as string} />
                </Option>
              ))}
            </OptGroup>
          ))}
        </Select>
      </Item>
    </div>
  );
};
