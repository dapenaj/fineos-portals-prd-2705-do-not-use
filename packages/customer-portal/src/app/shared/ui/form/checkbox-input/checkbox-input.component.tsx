import React from 'react';
import { Form, Checkbox } from 'antd';
import { getIn } from 'formik';

import { formatValidationMessage } from '../../../utils';
import styles from '../form-input.module.scss';
import { DefaultInputProps } from '../form.type';

const { Item } = Form;

export const CheckboxInput: React.FC<DefaultInputProps & {
  onChange: (checked: boolean, value?: boolean) => void;
  value?: boolean;
}> = ({
  name,
  label,
  errors,
  values,
  value,
  touched,
  setFieldValue,
  isDisabled,
  onChange
}) => {
  const validation = getIn(errors, name);
  const wasTouched = getIn(touched, name);
  const inputValue = getIn(values, name);

  return (
    <div className={styles.wrapper}>
      <Item
        hasFeedback={Boolean(validation)}
        validateStatus={wasTouched && validation && 'error'}
        help={
          Boolean(wasTouched && validation) &&
          formatValidationMessage(validation)
        }
      >
        <Checkbox
          name={name}
          disabled={isDisabled}
          value={value}
          checked={inputValue}
          onChange={event => {
            setFieldValue(name, event.target.checked);
            if (onChange) {
              onChange(event.target.checked, event.target.value);
            }
          }}
        >
          {label}
        </Checkbox>
      </Item>
    </div>
  );
};
