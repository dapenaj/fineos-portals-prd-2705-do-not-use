import React from 'react';
import { Form, DatePicker } from 'antd';
import moment, { Moment } from 'moment';
import { getIn } from 'formik';

import styles from '../form-input.module.scss';
import {
  formatValidationMessage,
  formatDateForApi,
  DISPLAY_DATE_FORMAT
} from '../../../utils';
import { DefaultInputProps } from '../form.type';

type DatePickerInputProps = DefaultInputProps & {
  format?: string;
  onChange?: (date: Moment | null, dateString: string) => void;
  'data-test-el'?: string;
};

const { Item } = Form;

export const DatePickerInput: React.FC<DatePickerInputProps> = ({
  name,
  label,
  errors,
  values,
  touched,
  onChange,
  format = DISPLAY_DATE_FORMAT,
  setFieldValue,
  setFieldTouched,
  isDisabled,
  labelCol,
  wrapperCol,
  ...props
}) => {
  const validation = getIn(errors, name);
  const wasTouched = getIn(touched, name);
  const inputValue = getIn(values, name);

  return (
    <div className={styles.wrapper} data-test-el={props['data-test-el']}>
      <Item
        hasFeedback={Boolean(validation)}
        validateStatus={wasTouched && validation && 'error'}
        help={
          Boolean(wasTouched && validation) &&
          formatValidationMessage(validation)
        }
        label={label}
        labelCol={labelCol}
        wrapperCol={wrapperCol}
      >
        <DatePicker
          name={name}
          value={inputValue ? moment(inputValue) : undefined}
          format={format}
          disabled={isDisabled}
          onChange={(date, dateString) => {
            setFieldValue(name, date ? formatDateForApi(date) : null);
            if (onChange) {
              onChange(date, dateString);
            }
          }}
          onOpenChange={isOpen => setFieldTouched(name)}
        />
      </Item>
    </div>
  );
};
