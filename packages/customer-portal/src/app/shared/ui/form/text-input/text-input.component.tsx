import React from 'react';
import { Form, Input } from 'antd';
import { getIn } from 'formik';

import { formatValidationMessage } from '../../../utils';
import styles from '../form-input.module.scss';
import { DefaultInputProps } from '../form.type';

const { Item } = Form;

export const TextInput: React.FC<DefaultInputProps> = ({
  name,
  label,
  errors,
  values,
  touched,
  setFieldTouched,
  setFieldValue,
  isDisabled,
  ...props
}) => {
  const validation = getIn(errors, name);
  const wasTouched = getIn(touched, name);
  const inputValue = getIn(values, name);

  return (
    <div className={styles.wrapper} data-test-el={props['data-test-el']}>
      <Item
        hasFeedback={Boolean(validation)}
        validateStatus={wasTouched && validation && 'error'}
        help={
          Boolean(wasTouched && validation) &&
          formatValidationMessage(validation)
        }
        label={label}
      >
        <Input
          name={name}
          value={inputValue}
          autoComplete="off"
          disabled={isDisabled}
          onChange={event => setFieldValue(name, event.target.value)}
          onBlur={() => setFieldTouched(name)}
        />
      </Item>
    </div>
  );
};
