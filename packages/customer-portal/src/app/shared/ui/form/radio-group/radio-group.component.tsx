import React from 'react';
import { Radio, Form } from 'antd';
import { FormattedMessage } from 'react-intl';
import { RadioChangeEvent } from 'antd/lib/radio';

import formStyles from '../form-input.module.scss';

import styles from './radio-group.module.scss';

const { Item } = Form;
const { Group } = Radio;

export type RadioGroupProps = {
  name?: string;
  optionPrefix?: string;
  options: string[];
  onChange?: (event: RadioChangeEvent) => void;
  disabled?: boolean;
  label?: React.ReactNode;
  renderPopover?: (index: number) => React.ReactNode;
  defaultValue?: string;
  'data-test-el'?: string;
};

export const RadioGroup: React.FC<RadioGroupProps> = ({
  name,
  optionPrefix,
  options,
  onChange,
  disabled,
  label,
  renderPopover,
  defaultValue,
  ...props
}) => {
  // The defaultValue prop doesn't seem to work. This is a workaround to set a default value.
  const defaultValueProp = defaultValue ? { value: defaultValue } : {};
  return (
    <div
      className={formStyles.wrapper}
      data-test-el={props['data-test-el'] || 'radio-group'}
    >
      <Item label={label}>
        <Group
          onChange={onChange}
          size="large"
          disabled={disabled}
          name={name}
          {...defaultValueProp}
        >
          {options.map((option, index) => (
            <Radio value={option} key={index} className={styles.radio}>
              <div className={styles.label}>
                <FormattedMessage
                  id={optionPrefix ? `${optionPrefix}${option}` : option}
                />
                {renderPopover && renderPopover(index)}
              </div>
            </Radio>
          ))}
        </Group>
      </Item>
    </div>
  );
};
