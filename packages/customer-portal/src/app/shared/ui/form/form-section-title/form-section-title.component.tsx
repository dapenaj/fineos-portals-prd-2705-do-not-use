import React from 'react';

import styles from './form-section-title.module.scss';

export const FormSectionTitle: React.FC = ({ children, ...props }) => (
  <p className={styles.subtitle} {...props}>
    {children}
  </p>
);
