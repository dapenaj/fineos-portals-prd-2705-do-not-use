import React from 'react';
import { Form, Radio } from 'antd';
import { getIn } from 'formik';
import { FormattedMessage } from 'react-intl';

import { formatValidationMessage } from '../../../utils';
import { DefaultInputProps } from '../form.type';
import formStyles from '../form-input.module.scss';
import radioStyles from '../radio-group/radio-group.module.scss';

const { Item } = Form;
const { Group } = Radio;

type BooleanInputProps = {
  onChange?: (value: boolean) => void;
  noLabel?: React.ReactElement;
} & DefaultInputProps;

export const BooleanInput: React.FC<BooleanInputProps> = ({
  name,
  label,
  errors,
  values,
  touched,
  setFieldValue,
  isDisabled,
  noLabel,
  onChange
}) => {
  const validation = getIn(errors, name);
  const wasTouched = getIn(touched, name);
  const value = getIn(values, name);

  return (
    <div className={formStyles.wrapper}>
      <Item
        hasFeedback={Boolean(validation)}
        validateStatus={wasTouched && validation && 'error'}
        help={
          Boolean(wasTouched && validation) &&
          formatValidationMessage(validation)
        }
        label={label}
      >
        <Group
          name={name}
          size="large"
          disabled={isDisabled}
          value={value}
          onChange={event => {
            setFieldValue(name, event.target.value);
            if (onChange) {
              onChange(event.target.value);
            }
          }}
        >
          <Radio value={true} className={radioStyles.radio}>
            <div className={radioStyles.label}>
              <FormattedMessage id="LABELS.YES" />
            </div>
          </Radio>

          <Radio value={false} className={radioStyles.radio}>
            <div className={radioStyles.label}>
              {noLabel || <FormattedMessage id="LABELS.NO" />}
            </div>
          </Radio>
        </Group>
      </Item>
    </div>
  );
};
