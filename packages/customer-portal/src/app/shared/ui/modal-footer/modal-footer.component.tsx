import React from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';

type ModalFooterProps = {
  submitText: string;
  cancelText: string;
  isDisabled: boolean;
  onSubmitClick: () => void;
  onCancelClick: () => void;
};

export const ModalFooter: React.FC<ModalFooterProps> = ({
  onSubmitClick,
  submitText,
  isDisabled,
  onCancelClick,
  cancelText
}) => (
  <>
    <Button
      type="primary"
      onClick={onSubmitClick}
      disabled={isDisabled}
      data-test-el="modal-footer-submit-btn"
    >
      <FormattedMessage id={submitText} />
    </Button>
    <Button onClick={onCancelClick} data-test-el="modal-footer-cancel-btn">
      <FormattedMessage id={cancelText} />
    </Button>
  </>
);
