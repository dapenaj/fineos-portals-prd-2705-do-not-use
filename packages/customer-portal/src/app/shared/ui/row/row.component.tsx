import React from 'react';
import classnames from 'classnames';
import { Row as AntRow } from 'antd';
import { RowProps } from 'antd/lib/row';

import styles from './row.module.scss';

export const Row: React.FC<RowProps> = ({
  children,
  gutter = 32,
  align = 'top',
  justify = 'start',
  type = 'flex',
  className,
  ...props
}) => (
  <AntRow
    gutter={gutter}
    className={classnames(styles.row, className)}
    align={align}
    justify={justify}
    type={type}
    {...props}
  >
    {children}
  </AntRow>
);
