import React from 'react';
import { Address } from 'fineos-js-api-client';

import styles from './address-renderer.module.scss';

type AddressRendererProps = { address: Address | null };

export const AddressRenderer: React.FC<AddressRendererProps> = ({ address }) =>
  address && (
    <div className={styles.wrapper}>
      <div>
        {address.premiseNo}
        {' ' + address.addressLine1}
      </div>
      {address.addressLine2 && <div>{address.addressLine2}</div>}
      {address.addressLine3 && <div>{address.addressLine3}</div>}
      {address.addressLine4 && <div>{address.addressLine4}</div>}
      {address.addressLine5 && <div>{address.addressLine5}</div>}
      {address.addressLine6 && <div>{address.addressLine6}</div>}
      {address.addressLine7 && <div>{address.addressLine7}</div>}
      {address.postCode && <div>{address.postCode}</div>}
      {address.country && <div>{address.country}</div>}
    </div>
  );
