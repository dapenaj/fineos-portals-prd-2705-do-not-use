import React from 'react';

import greenCheckIntake from '../../../../../../assets/img/green_check_intake.svg';
import { AppConfig } from '../../../../types';
import { ConfirmReturnEForm } from '../../../../services';
import { formatDateForDisplay } from '../../../../utils';
import { withAppConfig } from '../../../../contexts';

import styles from './finish-screen.module.scss';

type FinishScreenViewProps = {
  config: AppConfig;
  confirmReturn: ConfirmReturnEForm;
};

export const FinishScreenComponent: React.FC<FinishScreenViewProps> = ({
  config: {
    clientConfig: {
      notification: {
        returnConfirm: {
          expectedDateConfirmation1,
          expectedDateConfirmation2,
          anotherDateConfirmation1,
          anotherDateConfirmation2,
          anotherDateConfirmation3
        }
      }
    }
  },
  confirmReturn
}) => {
  const messages = (confirmReturn.returningAsExpected
    ? [expectedDateConfirmation1, expectedDateConfirmation2]
    : [
        anotherDateConfirmation1,
        anotherDateConfirmation2,
        anotherDateConfirmation3
      ]
  )
    .filter(Boolean)
    .map(stringToken =>
      stringToken!.replace(
        '{actualDate}',
        formatDateForDisplay(confirmReturn.actualRTWDate!)
      )
    );

  return (
    <div className={styles.wrapper}>
      <div className={styles.main}>
        {messages.map((messageLine, index) => (
          <p key={index}>{messageLine}</p>
        ))}
      </div>
      <div>
        <img alt="success-icon" src={greenCheckIntake} />
      </div>
    </div>
  );
};

export const FinishScreen = withAppConfig(FinishScreenComponent);
