import { withFormik } from 'formik';
import * as Yup from 'yup';
import moment from 'moment';

import { ConfirmCaseReturn } from '../../../../modules/notifications/notification-detail/required-from-me';

import { RequiredFromMeConfirmReturnFormView } from './required-from-me-confirm-return-form.view';

export type RequiredFromMeConfirmReturnFormOwnProps = {
  showModal: boolean;
  confirmReturn: ConfirmCaseReturn;
  onSave: (value: ConfirmCaseReturn) => Promise<void>;
  onClose: () => void;
  afterClose: () => void;
};

const requiredFromMeConfirmReturnFormValidationSchema: Yup.ObjectSchema<Partial<
  ConfirmCaseReturn
>> = Yup.object().shape({
  returningAsExpected: Yup.boolean().required('COMMON.VALIDATION.REQUIRED'),
  actualRTWDate: Yup.mixed().when('returningAsExpected', {
    is: false,
    then: Yup.mixed().required('COMMON.VALIDATION.REQUIRED')
  }),
  employerNotified: Yup.boolean().when('returningAsExpected', {
    is: false,
    then: Yup.boolean().required('COMMON.VALIDATION.REQUIRED')
  })
});

export const RequiredFromMeConfirmReturnFormContainer = withFormik<
  RequiredFromMeConfirmReturnFormOwnProps,
  ConfirmCaseReturn
>({
  mapPropsToValues: props => props.confirmReturn,
  enableReinitialize: true,

  // Custom sync validation
  validationSchema: requiredFromMeConfirmReturnFormValidationSchema,

  handleSubmit: (values, { props: { onSave }, setSubmitting }) => {
    onSave(
      values.returningAsExpected
        ? values
        : {
            ...values,
            actualRTWDate: values.actualRTWDate
              ? moment(values.actualRTWDate)
              : null
          }
    ).then(() => setSubmitting(false));
  },
  isInitialValid: false
})(RequiredFromMeConfirmReturnFormView);
