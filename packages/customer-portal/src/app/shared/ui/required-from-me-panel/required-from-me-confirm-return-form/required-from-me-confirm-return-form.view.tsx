import React from 'react';
import { Button, Col, Modal } from 'antd';
import { Form, FormikProps } from 'formik';
import { FormattedMessage } from 'react-intl';

import { Row } from '../../row';
import { DatePickerInput } from '../../form/date-picker-input';
import { ContentLayout } from '../../../layouts/content-layout';
import { FormLabel } from '../../form/form-label';
import { BooleanInput } from '../../form/boolean-input';
import { ModalFooter } from '../../modal-footer';
import { ConfirmCaseReturn } from '../../../../modules/notifications/notification-detail/required-from-me';

import { FinishScreen } from './finish-screen';
import styles from './required-from-me-confirm-return-form.module.scss';

type RequiredFromMeConfirmReturnFormViewProps = {
  showModal: boolean;
  onClose: () => void;
  afterClose: () => void;
} & FormikProps<ConfirmCaseReturn>;

const DATE_PICKER_LABEL_COL = { xs: 48, sm: 24 };

const DATE_PICKER_WRAPPER_COL = { xs: 24, sm: 10 };

export const RequiredFromMeConfirmReturnFormView: React.FC<RequiredFromMeConfirmReturnFormViewProps> = ({
  afterClose,
  showModal,
  onClose,
  ...formikProps
}) => {
  const isSaved = formikProps.values.employeeConfirmed;

  return (
    <Modal
      visible={showModal}
      confirmLoading={formikProps.isSubmitting}
      afterClose={afterClose}
      onCancel={() => {
        formikProps.resetForm();
        onClose();
      }}
      footer={
        <>
          <ModalFooter
            onSubmitClick={formikProps.submitForm}
            isDisabled={!formikProps.isValid || formikProps.isSubmitting}
            submitText={'COMMON.ACTIONS.OK'}
            onCancelClick={() => {
              formikProps.resetForm();
              onClose();
            }}
            cancelText={'COMMON.ACTIONS.CANCEL'}
          />

          {isSaved && (
            <Button type="primary" onClick={onClose}>
              <FormattedMessage id="COMMON.ACTIONS.CLOSE" />
            </Button>
          )}
        </>
      }
      title={
        isSaved ? (
          formikProps.values.returningAsExpected ? (
            <FormattedMessage id="RETURN_TO_WORK.CONFIRMED_DATE_TITLE" />
          ) : (
            <FormattedMessage id="RETURN_TO_WORK.NEW_DATE_TITLE" />
          )
        ) : (
          <FormattedMessage id="RETURN_TO_WORK.CONFIRM_RETURN" />
        )
      }
    >
      <div className={styles.wrapper}>
        {isSaved ? (
          <FinishScreen confirmReturn={formikProps.values} />
        ) : (
          <Form>
            <ContentLayout direction="column">
              {!!formikProps.values.expectedRTWDate && (
                <>
                  <Row>
                    <Col span={24}>
                      <DatePickerInput
                        {...formikProps}
                        labelCol={DATE_PICKER_LABEL_COL}
                        wrapperCol={DATE_PICKER_WRAPPER_COL}
                        isDisabled={true}
                        name="expectedRTWDate"
                        label={
                          <FormLabel label="RETURN_TO_WORK.EXPECTED_RETURN_LABEL" />
                        }
                      />
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      <BooleanInput
                        {...formikProps}
                        name="returningAsExpected"
                        label={
                          <FormLabel
                            required={true}
                            label="RETURN_TO_WORK.CONFIRM_QUESTION_LABEL"
                          />
                        }
                        noLabel={
                          <FormattedMessage id="RETURN_TO_WORK.NOT_CONFIRMING_LABEL" />
                        }
                      />
                    </Col>
                  </Row>
                </>
              )}

              {/* this cannot be checked just via "is it falsy" cause returningAsExpected can (and would) be undefined */}
              {formikProps.values.returningAsExpected === false && (
                <>
                  <Row>
                    <Col span={24}>
                      <DatePickerInput
                        {...formikProps}
                        labelCol={DATE_PICKER_LABEL_COL}
                        wrapperCol={DATE_PICKER_WRAPPER_COL}
                        name="actualRTWDate"
                        label={
                          <FormLabel
                            required={true}
                            label="RETURN_TO_WORK.ACTUAL_RETURN_DATE_LABEL"
                          />
                        }
                      />
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      <BooleanInput
                        {...formikProps}
                        name="employerNotified"
                        label={
                          <FormLabel
                            required={true}
                            label="RETURN_TO_WORK.EMPLOYER_NOTIFIED_LABEL"
                          />
                        }
                      />
                    </Col>
                  </Row>
                </>
              )}
            </ContentLayout>
          </Form>
        )}
      </div>
    </Modal>
  );
};
