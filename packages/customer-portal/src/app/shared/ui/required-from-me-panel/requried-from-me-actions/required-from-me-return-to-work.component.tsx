import React, { Component } from 'react';
import { Button, List } from 'antd';
import { FormattedMessage } from 'react-intl';

import { RequiredFromMeConfirmReturnForm } from '../required-from-me-confirm-return-form';
import { ConfirmCaseReturn } from '../../../../modules/notifications/notification-detail/required-from-me';
import { formatDateForDisplay } from '../../../utils';

import styles from './required-from-me-actions.module.scss';

const { Item } = List;

type RequiredFromMeReturnToWorkProps = {
  confirmReturns: ConfirmCaseReturn[];
  onSave: (confirmReturn: ConfirmCaseReturn) => Promise<void>;
  onCloseModal: () => void;
};

type RequiredFromMeReturnToWorkState = {
  showModalForCase: string | null;
};

export class RequiredFromMeReturnToWork extends Component<
  RequiredFromMeReturnToWorkProps,
  RequiredFromMeReturnToWorkState
> {
  state: RequiredFromMeReturnToWorkState = {
    showModalForCase: null
  };

  handleShowModal = (caseId: string) =>
    this.setState({
      showModalForCase: caseId
    });

  handleCloseModal = () =>
    this.setState({
      showModalForCase: null
    });

  render() {
    const { confirmReturns, onSave, onCloseModal } = this.props;
    const { showModalForCase } = this.state;
    const isMultipleConfirmations = confirmReturns.length > 1;

    return (
      <>
        {confirmReturns.map(confirmReturn => (
          <>
            {!confirmReturn.employeeConfirmed && (
              <Item key={confirmReturn.caseId}>
                <div
                  className={styles.wrapper}
                  data-test-el="required-from-me-return-to-work"
                >
                  <Button
                    className={styles.button}
                    type="link"
                    onClick={() => this.handleShowModal(confirmReturn.caseId)}
                    data-test-el="required-from-me-return-to-work-btn"
                  >
                    <FormattedMessage id="RETURN_TO_WORK.CONFIRM_RETURN" />
                    {isMultipleConfirmations && (
                      <>
                        &nbsp;-{' '}
                        {formatDateForDisplay(confirmReturn.expectedRTWDate!)}
                      </>
                    )}
                  </Button>
                </div>
              </Item>
            )}
            <RequiredFromMeConfirmReturnForm
              showModal={showModalForCase === confirmReturn.caseId}
              confirmReturn={confirmReturn}
              onSave={onSave}
              onClose={this.handleCloseModal}
              afterClose={onCloseModal}
            />
          </>
        ))}
      </>
    );
  }
}
