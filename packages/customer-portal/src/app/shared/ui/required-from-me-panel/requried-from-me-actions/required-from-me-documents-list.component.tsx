import React from 'react';
import classNames from 'classnames';
import { List, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';
import { NavLink } from 'react-router-dom';

import { RequiredFromMeDocumentModal } from '../required-from-me-document-modal';
import { InlinePropertyItem } from '../../property-item';
import { UploadDocument } from '../../../types';

import styles from './required-from-me-actions.module.scss';

type RequiredFromMeDocumentsListProps = {
  documents: UploadDocument[];
  additionalCaseText?: boolean;
  onRefreshRequiredFromMe: () => void;
};

const { Item } = List;

export const RequiredFromMeDocumentsList: React.FC<RequiredFromMeDocumentsListProps> = ({
  documents,
  additionalCaseText,
  onRefreshRequiredFromMe
}) => (
  <>
    {documents.map((document, index) => {
      const {
        evidence: { docReceived, name, rootCaseId }
      } = document;
      return (
        <Item key={index}>
          <div
            className={classNames(styles.wrapper, styles.wrapperWithDetails)}
            data-test-el="required-from-me-doc-name"
          >
            <div className={styles.details}>
              {docReceived ? (
                <>{name}</>
              ) : (
                <RequiredFromMeDocumentModal
                  document={document}
                  onRefreshRequiredFromMe={onRefreshRequiredFromMe}
                />
              )}

              {docReceived && <Icon type="check" className={styles.icon} />}
            </div>
            {additionalCaseText && (
              <InlinePropertyItem
                label={
                  <FormattedMessage id={'REQUIRED_FROM_ME.REQUIRED_FOR'} />
                }
              >
                <NavLink
                  to={`/notifications/${rootCaseId}`}
                  className={styles.link}
                >
                  {rootCaseId}
                </NavLink>
              </InlinePropertyItem>
            )}
          </div>
        </Item>
      );
    })}
  </>
);
