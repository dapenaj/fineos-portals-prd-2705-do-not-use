import React from 'react';
import { injectIntl, WrappedComponentProps as IntlProps } from 'react-intl';
import { DocumentService } from 'fineos-js-api-client';

import { UploadDocument, UploadDocumentError } from '../../../../types';
import { DocumentUpload, DocumentUploadError } from '../../../document-upload';

type RequiredFromMeUploadProps = {
  document: UploadDocument;
  onCloseModal: () => void;
  onCloseModalAndRefresh: () => void;
} & IntlProps;

type RequiredFromMeUploadState = {
  showErrorModal: boolean;
  error?: UploadDocumentError;
  loading: boolean;
};

export class RequiredFromMeUploadComponent extends React.Component<
  RequiredFromMeUploadProps,
  RequiredFromMeUploadState
> {
  documentService: DocumentService = DocumentService.getInstance();

  state: RequiredFromMeUploadState = {
    showErrorModal: false,
    loading: false
  };

  handleOnSubmit = async ({ caseId, request }: UploadDocument) => {
    this.setState({ loading: true });

    try {
      const { documentId } = await this.documentService.uploadCaseDocument(
        caseId,
        request!
      );

      await this.setDocumentAsReceived(caseId, documentId);
    } catch (error) {
      this.setState({
        loading: false,
        showErrorModal: true,
        error: 'upload'
      });

      this.props.onCloseModalAndRefresh();
    }
  };

  setDocumentAsReceived = async (caseId: string, documentId: number) => {
    try {
      await this.documentService.uploadSupportingEvidence({
        caseId,
        documentId
      });
    } catch (error) {
      this.setState({ showErrorModal: true, error: 'received' });
    } finally {
      this.setState({ loading: false });
      this.markAsRead(caseId, documentId);
    }
  };

  markAsRead = async (caseId: string, documentId: number) => {
    await this.documentService.markDocumentAsRead(caseId, documentId);
    this.props.onCloseModalAndRefresh();
  };

  handleCancelModal = () =>
    this.setState({ showErrorModal: false, error: undefined });

  render() {
    const { document, onCloseModal } = this.props;

    const { showErrorModal, error, loading } = this.state;

    return (
      <>
        <DocumentUpload
          document={document}
          loading={loading}
          onCancel={onCloseModal}
          onSubmit={this.handleOnSubmit}
        />

        <DocumentUploadError
          showModal={showErrorModal}
          error={error}
          onCancelModal={this.handleCancelModal}
        />
      </>
    );
  }
}

export const RequiredFromMeUpload = injectIntl(RequiredFromMeUploadComponent);
