import React from 'react';
import { Button, Modal } from 'antd';
import { FormattedMessage } from 'react-intl';

import { UploadDocument } from '../../../types';

import { RequiredFromMeUpload } from './required-from-me-upload';
import styles from './required-from-me-document-modal.module.scss';

type RequiredFromMeDocumentModalProps = {
  document: UploadDocument;
  onRefreshRequiredFromMe: () => void;
};

type RequiredFromMeDocumentModalState = {
  open: boolean;
};

export class RequiredFromMeDocumentModal extends React.Component<
  RequiredFromMeDocumentModalProps,
  RequiredFromMeDocumentModalState
> {
  state: RequiredFromMeDocumentModalState = {
    open: false
  };

  handleShowModal = () => this.setState({ open: true });

  handleCloseModal = () => this.setState({ open: false });

  handleCloseModalAndRefresh = () => {
    this.setState({ open: false });
    this.props.onRefreshRequiredFromMe();
  };

  render() {
    const { document } = this.props;
    const { open } = this.state;

    return (
      <>
        <Button
          className={styles.button}
          type="link"
          onClick={this.handleShowModal}
          data-test-el="required-document-upload-btn"
        >
          <FormattedMessage id="REQUIRED_FROM_ME.UPLOAD" />
          <span className={styles.name}>{document.evidence.name}</span>
        </Button>

        <Modal
          title={<FormattedMessage id="REQUIRED_FROM_ME.UPLOAD_DOCUMENT" />}
          visible={open}
          onCancel={this.handleCloseModal}
          footer={null}
        >
          <RequiredFromMeUpload
            document={document}
            onCloseModal={this.handleCloseModal}
            onCloseModalAndRefresh={this.handleCloseModalAndRefresh}
          />
        </Modal>
      </>
    );
  }
}
