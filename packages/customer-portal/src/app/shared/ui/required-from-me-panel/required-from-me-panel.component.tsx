import React from 'react';
import { List } from 'antd';
import { FormattedMessage } from 'react-intl';

import { UploadDocument } from '../../types';
import { ConfirmReturnEForm } from '../../services/eform/types';
import { EmptyState } from '../empty-state';
import { ConfirmCaseReturn } from '../../../modules/notifications/notification-detail/required-from-me';

import {
  RequiredFromMeDocumentsList,
  RequiredFromMeReturnToWork
} from './requried-from-me-actions';
import { RequiredFromMeHeader } from './required-from-me-header';
import styles from './required-from-me-panel.module.scss';

type RequiredFromMePanelProps = {
  loading: boolean;
  documents: UploadDocument[] | null;
  additionalCaseText?: boolean;
  confirmReturns: ConfirmCaseReturn[] | null;
  onConfirmReturnSave?: (confirmReturn: ConfirmCaseReturn) => Promise<void>;
  onRefreshRequiredFromMe: () => void;
};

type RequiredFromMePanelState = {
  showConfirmReturn: boolean;
};

export class RequiredFromMePanel extends React.Component<
  RequiredFromMePanelProps,
  RequiredFromMePanelState
> {
  constructor(props: RequiredFromMePanelProps) {
    super(props);

    this.state = {
      showConfirmReturn:
        Boolean(props.confirmReturns) &&
        props.confirmReturns!.some(
          ({ employeeConfirmed }) => !employeeConfirmed
        )
    };
  }

  // in this component normal data flow is broken due to a reason that after employeeConfirmed return date
  // modal still should be open, but this is not fit into logical data flow
  static getDerivedStateFromProps<ConfirmReturn extends ConfirmReturnEForm>(
    { confirmReturns }: RequiredFromMePanelProps,
    { showConfirmReturn }: RequiredFromMePanelState
  ) {
    if (
      confirmReturns &&
      confirmReturns.some(({ employeeConfirmed }) => !employeeConfirmed) &&
      !showConfirmReturn
    ) {
      return {
        showConfirmReturn: true
      };
    }

    return null;
  }

  handleCloseConfirmReturnModal = () => {
    const { confirmReturns } = this.props;

    this.setState({
      showConfirmReturn: !confirmReturns!.some(
        ({ employeeConfirmed }) => employeeConfirmed
      )
    });
  };

  render() {
    const {
      confirmReturns,
      documents,
      loading,
      additionalCaseText,
      onConfirmReturnSave,
      onRefreshRequiredFromMe
    } = this.props;
    const { showConfirmReturn } = this.state;
    const documentToUploadCount = (documents || []).filter(
      ({ evidence: { docReceived } }) => !docReceived
    ).length;
    const confirmToReturnCount = confirmReturns
      ? confirmReturns.filter(confirmReturn => !confirmReturn.employeeConfirmed)
          .length
      : 0;
    const totalRequiredActions = documentToUploadCount + confirmToReturnCount;

    return (
      <div className={styles.wrapper}>
        <List
          loading={loading}
          header={
            <RequiredFromMeHeader requiredActionsCount={totalRequiredActions} />
          }
        >
          <div className={styles.scroll} data-test-el="required-from-me-list">
            {showConfirmReturn && (
              <RequiredFromMeReturnToWork
                confirmReturns={confirmReturns!}
                onSave={onConfirmReturnSave!}
                onCloseModal={this.handleCloseConfirmReturnModal}
              />
            )}

            {!!documents && !!documents.length && (
              <RequiredFromMeDocumentsList
                documents={documents}
                additionalCaseText={additionalCaseText}
                onRefreshRequiredFromMe={onRefreshRequiredFromMe}
              />
            )}

            {totalRequiredActions === 0 && !loading && (
              <EmptyState>
                <FormattedMessage id="REQUIRED_FROM_ME.EMPTY_MESSAGE" />
              </EmptyState>
            )}
          </div>
        </List>
      </div>
    );
  }
}
