import React from 'react';
import { Icon, Badge } from 'antd';
import { FormattedMessage } from 'react-intl';

import styles from './required-from-me-header.module.scss';

type RequiredFromMeHeaderProps = {
  requiredActionsCount: number;
};

export const RequiredFromMeHeader: React.FC<RequiredFromMeHeaderProps> = ({
  requiredActionsCount
}) => (
  <div className={styles.header} data-test-el="required-from-me-header">
    <Badge dot={requiredActionsCount > 0} className={styles.icon}>
      <Icon type="bars" />
    </Badge>
    <FormattedMessage
      id="REQUIRED_FROM_ME.TITLE"
      values={{
        count: requiredActionsCount
      }}
    />
  </div>
);
