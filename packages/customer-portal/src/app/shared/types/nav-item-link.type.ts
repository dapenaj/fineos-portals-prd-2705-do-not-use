import { Role } from './role.type';

export type NavItemLink = {
  path: string;
  name: string;
  exact?: boolean;
  icon?: string;
  showBadge?: boolean;
  role?: Role;
};
