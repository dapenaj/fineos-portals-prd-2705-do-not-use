import { AbsenceStatus } from 'fineos-js-api-client';

export enum TimeOffPeriodType {
  TIME_OFF = 'TIME_OFF',
  REDUCED_SCHEDULE = 'REDUCED_SCHEDULE',
  EPISODIC = 'EPISODIC'
}

export type TimeOffLeavePeriod = {
  startDate: string;
  endDate: string;
  lastDayWorked: string;
  expectedReturnToWorkDate: string;
  startDateFullDay?: boolean;
  startDateOffHours?: number;
  startDateOffMinutes?: number;
  endDateFullDay?: boolean;
  endDateOffHours?: number;
  endDateOffMinutes?: number;
  returnToWorkFullDate?: string;
  status: AbsenceStatus;
};

export type ReducedScheduleLeavePeriod = {
  startDate: string;
  endDate: string;
  status: AbsenceStatus;
};

export type EpisodicLeavePeriod = {
  startDate: string;
  endDate?: string;
};

export type TimeOff = {
  outOfWork: boolean;
  timeOffLeavePeriods?: TimeOffLeavePeriod[];
  reducedScheduleLeavePeriods?: ReducedScheduleLeavePeriod[];
  episodicLeavePeriods?: EpisodicLeavePeriod[];
};
