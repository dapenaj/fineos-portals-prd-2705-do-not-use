import { ConfirmationCase } from './confirmation.type';

export type ClientConfig = {
  notification: NotificaitonClientConfig;
  intake: IntakeClientConfig;
  header: HeaderClientConfig;
  home: HomeClientConfig;
  myProfile: MyProfileClientConfig;
  myTeamLeaves: MyTeamLeavesConfig;
  documentUpload: DocumentUploadConfig;
};

export type DocumentUploadConfig = {
  errorTitle: string;
  uploadError: string;
  receivedError: string;
};

export type JobProtectedLeaveEmptyState = {
  empty: string;
  pending: string;
  concluded: {
    title: string;
    subtitle: string;
  };
};

export type WageReplacementEmptyState = {
  pending: string;
};

export type InProgressStatusState = {
  message: string;
};

export type RequestConfirmationState = {
  requestAmendmentMessage: string;
  requestDeletionMessage: string;
  supervisorAmendmentMessage: string;
  supervisorDeletionMessage: string;
};

export type NotificaitonDetailsEmptyState = {
  jobProtectedLeave: JobProtectedLeaveEmptyState;
  wageReplacement: WageReplacementEmptyState;
};

export type NotificaitonDetailClientConfig = {
  emptyState: NotificaitonDetailsEmptyState;
};

export type NotificationReturnConfirmClientConfig = {
  expectedDateConfirmation1: string;
  expectedDateConfirmation2?: string;
  anotherDateConfirmation1: string;
  anotherDateConfirmation2?: string;
  anotherDateConfirmation3?: string;
};

export type NotificaitonClientConfig = {
  detail: NotificaitonDetailClientConfig;
  inProgressStatus: InProgressStatusState;
  requestConfirmation: RequestConfirmationState;
  returnConfirm: NotificationReturnConfirmClientConfig;
};

export type IntakeClientConfig = {
  wrapUp: IntakeWrapUpClientConfig;
};

export type IntakeWrapUpClientConfig = ConfirmationCase[];

export type HeaderClientConfig = {
  branding: {
    message: string;
  };
};

export type HomeClientConfig = {
  welcome: { message: string };
  action: { title: string; message: string };
};

export type MyProfileClientConfig = {
  message: string;
};

export type MyTeamLeavesConfig = {
  notificationsPageSize: string;
  employeesPageSize: string;
};
