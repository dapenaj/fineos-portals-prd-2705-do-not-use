import { SelectOption } from './select-option.type';

export type OptionGroup = {
  group: string;
  options: SelectOption[];
};
