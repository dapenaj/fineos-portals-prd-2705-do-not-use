import { ApiError } from 'fineos-js-api-client';

export type ApiState<T> = {
  data: T | null;
  loading: boolean;
  error?: ApiError | string;
};
