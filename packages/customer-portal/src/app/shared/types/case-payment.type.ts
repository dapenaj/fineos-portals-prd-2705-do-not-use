import { Payment } from 'fineos-js-api-client';

export interface CasePayment extends Payment {
  claimId: string;
}
