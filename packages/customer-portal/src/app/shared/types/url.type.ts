export enum URLParseChar {
  FRAGMENT = '#',
  QUERY_PARAM = '?'
}
