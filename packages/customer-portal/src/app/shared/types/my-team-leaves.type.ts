import {
  SupervisedNotificationCaseSummary,
  AbsencePeriod
} from 'fineos-js-api-client';

export type MyTeamLeavesFilters = {
  startDate?: string;
  endDate?: string;
  statuses?: string[];
  employeeId?: string;
  sort?: MyTeamLeaveField;
};

export enum MyTeamLeaveField {
  MEMBER = 'member',
  CREATED_DATE = 'createdDate',
  NOTIFICATION_CASE_ID = 'notificationCaseId',
  NOTIFICATION_REASON = 'notificationReason',
  DATE_FIRST_MISSING_WORK = 'dateFirstMissingWork',
  EXPECTED_RETURN_TO_WORK_DATE = 'expectedRTWDate',
  ABSENCE_CASE_MANAGER = 'absenceCaseManager',
  CASE_MANAGERS_CONTACT = 'caseManagersContact'
}

export enum MyTeamLeaveExportHeader {
  TEAM_MEMBER = 'MY_TEAM_LEAVES.LABELS.TEAM_MEMBER',
  NOTIFIED_ON = 'MY_TEAM_LEAVES.LABELS.NOTIFIED_ON',
  CASE_ID = 'MY_TEAM_LEAVES.LABELS.CASE_ID',
  REASON = 'MY_TEAM_LEAVES.LABELS.REASON',
  FIRST_DAY_MISSING_WORK = 'MY_TEAM_LEAVES.LABELS.FIRST_DAY_MISSING_WORK',
  EXPECTED_RETURN = 'MY_TEAM_LEAVES.LABELS.EXPECTED_RETURN',
  ABSENCE_CASE_MANAGER = 'MY_TEAM_LEAVES.LABELS.ABSENCE_CASE_MANAGER',
  CASE_MANAGERS_CONTACT = 'MY_TEAM_LEAVES.LABELS.CASE_MANAGERS_CONTACT'
}

export type MyTeamLeavesExportColumn = {
  header: string;
  field: MyTeamLeaveField;
};

export type MyTeamLeave = {
  member: string;
  absenceCaseId: string;
  absencePeriods: AbsencePeriod[];
} & SupervisedNotificationCaseSummary;
