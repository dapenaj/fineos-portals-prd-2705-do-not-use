import {
  Customer,
  Contact,
  Occupation,
  CustomerOccupation
} from 'fineos-js-api-client';

export type CustomerDetail = {
  customer: Customer | null;
  contact: Contact | null;
  occupation: Occupation | null;
  customerOccupations: CustomerOccupation[] | null;
};
