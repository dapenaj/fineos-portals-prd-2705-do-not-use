export enum TimeOffAmendmentType {
  ADDITIONAL_TIME_OFF = 'ADDITIONAL_TIME_OFF',
  REDUCED_TIME_OFF = 'REDUCED_TIME_OFF',
  MOVED_TIME_OFF = 'MOVED_TIME_OFF'
}

export type DeleteLeavePeriodValues = {
  member?: string;
  leavePeriodId: string;
  startDate: string;
  endDate: string;
  deleteType: string;
};

export type ReducedTimeOffAmendment = {
  member?: string;
  leavePeriodId: string;
  requestReason: string;
  leavePeriodStatus?: string;
  originalLastDayAbsent: string;
  newLastDayAbsent: string;
  newExpectedReturnDay: string;
  returnDay: string;
  returnHours: string;
  returnMinutes: string;
};

export type AdditionalTimeOffAmendment = {
  sameReason?: string;
  newReason?: string;
} & ReducedTimeOffAmendment;

export type MovedTimeOffAmendment = {
  originalFirstDayAbsent: string;
  newFirstWorkDay: string;
  newLastWorkDay: string;
  newLastWorkDayPartial: string;
  newLastWorkDayHours: string;
  newLastWorkDayMinutes: string;
  newFirstDayAbsent: string;
} & ReducedTimeOffAmendment;

export type RequestAmendmentFormValue = {
  type: TimeOffAmendmentType;
  amendedPeriod:
    | AdditionalTimeOffAmendment
    | ReducedTimeOffAmendment
    | MovedTimeOffAmendment;
};
