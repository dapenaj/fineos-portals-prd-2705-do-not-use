export enum NotificationPendingStatus {
  UNKNOWN = 'Unknown',
  REGISTRATION = 'Registration',
  ADJUDICATION = 'Adjudication',
  EXTEND = 'Extend',
  ASSESSMENT = 'Assessment',
  MONITORING = 'Monitoring',
  PENDING = 'Pending',
  NOTIFICATION_INCOMPLETE = 'Notification Incomplete',
  PENDING_CLAIM = 'Pending Claim',
  NOTIFICATION = 'Notification',
  INCOMPLETE = 'Incomplete'
}

export enum NotificationDecidedStatus {
  DECIDED = 'Decided',
  COMPLETION = 'Completion',
  MANAGED_LEAVE = 'Managed Leave'
}

export enum NotificationClosedStatus {
  CLOSED = 'Closed'
}

export type NotificationStatus =
  | NotificationPendingStatus
  | NotificationDecidedStatus
  | NotificationClosedStatus;

export enum NotificationStatusResult {
  PENDING = 'Pending',
  DECIDED = 'Decided',
  ALL_CLOSED = 'Closed',
  SOME_DECIDED = 'Some Decided',
  UNDETERMINED = 'Undetermined'
}
