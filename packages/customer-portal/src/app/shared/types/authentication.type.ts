export enum AuthenticationURLParam {
  ACCESS_KEY_ID = 'AccessKeyId',
  EXPIRATION = 'Expiration',
  SECRET_KEY = 'SecretKey',
  SESSION_TOKEN = 'SessionToken',
  USER_ID = 'UserID',
  ROLE = 'Role',
  DISPLAY_USER_ID = 'DisplayUserId',
  DISPLAY_ROLE = 'DisplayRole'
}
