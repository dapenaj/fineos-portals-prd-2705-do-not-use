export enum BenefitRightCategory {
  RECURRING_BENEFIT = 'Recurring Benefit',
  LUMP_SUM_BENEFIT = 'Lump Sum Benefit'
}
