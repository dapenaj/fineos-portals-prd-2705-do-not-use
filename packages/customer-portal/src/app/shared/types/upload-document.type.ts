import {
  DocumentUploadRequest,
  SupportingEvidence
} from 'fineos-js-api-client';

export type UploadDocument = {
  id: number;
  caseId: string;
  evidence: SupportingEvidence;
  request?: DocumentUploadRequest;
};

export type UploadDocumentError = 'upload' | 'received';
