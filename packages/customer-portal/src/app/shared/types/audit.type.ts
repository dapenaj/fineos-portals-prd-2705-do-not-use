export type AuditDetails<T> = {
  label: string;
  values: T;
};

export type Audit = {
  // steps -> sections -> messages
  steps: AuditDetails<AuditDetails<AuditDetails<any>[]>[]>[];
};
