export type ColPropsValue = {
  xs?: number;
  sm?: number;
  md?: number;
};
