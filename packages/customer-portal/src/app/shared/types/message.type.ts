import { Message } from 'fineos-js-api-client';

export type EnhancedMessage = { notificationCaseId: string } & Message;

export type FetchMessagesType = 'all' | 'single';
