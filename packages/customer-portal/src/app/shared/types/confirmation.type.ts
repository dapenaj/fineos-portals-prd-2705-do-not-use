export enum ConfirmationType {
  Successful = 'successful',
  PartialSuccess = 'partial-success',
  Failure = 'failure'
}
export interface ConfirmationCase {
  type: ConfirmationType;
  header: string;
  phoneNumber: string;
  successNotification?: string;
  notificationReferenceMessageHeader?: string;
  notificationReferenceMessageFooter?: string;
  privacyNotice?: string;
  numberMessage?: string;
  availableDays?: string;
  tip?: string;
  questionMessage?: string;
}
