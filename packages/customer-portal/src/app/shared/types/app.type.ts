import { ClientConfig } from './client-config.type';
import { CustomerDetail } from './customer.type';

export type AppConfig = {
  clientConfig: ClientConfig;
  customerDetail: CustomerDetail;
};
