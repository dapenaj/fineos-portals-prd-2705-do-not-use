import {
  AbsencePeriodDecision,
  Absence,
  EpisodicLeavePeriodDetail
} from 'fineos-js-api-client';

export type PeriodDecisionStatus =
  | 'pending' // Pending/Yellow
  | 'approved' // Approved/Green
  | 'declined' // Denied/Red
  | 'cancelled' // Cancelled/Grey
  | 'skipped' // Not Rendered
  | 'none'; // Unknown (handled differently)

export type JobProtectedLeavePlanGroup = {
  name: string;
  absencePeriodDecisions: AbsencePeriodDecision[];
  intermittentPeriodDecisions?: IntermittentPeriodDecision[];
};

export type PeriodDecisionGroup = {
  name: string;
  absencePeriodDecisions: (
    | EnhancedAbsencePeriodDecision
    | IntermittentPeriodDecision
  )[];
};

export type AbsencePeriodDecisionStatusCount = {
  status: string;
  count: number;
};

export type EnhancedAbsencePeriodDecision = AbsencePeriodDecision & {
  statusCategory: string;
};

export type AbsenceWithHandler = {
  absence: Absence;
  handlerInfo: AbsenceHandlerInfo;
};

export type AbsenceHandlerInfo = {
  absenceHandler: string;
  absenceHandlerPhoneNumber: string;
};

export type IntermittentPeriodDecision = {
  episodicLeavePeriodDetail?: EpisodicLeavePeriodDetail;
  handlerInfo: AbsenceHandlerInfo;
  childPeriodDecisions?: EnhancedAbsencePeriodDecision[];
} & EnhancedAbsencePeriodDecision;

export enum AbsenceReason {
  PREGNANCY = 'Pregnancy/Maternity'
}
