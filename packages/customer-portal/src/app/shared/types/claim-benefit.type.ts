import { Benefit } from 'fineos-js-api-client';

export type ClaimBenefit = {
  claimId: string;
  benefits: Benefit[];
};
