import React from 'react';

import { AntType } from '../types';

export type NotificationOptions = {
  title: React.ReactNode;
  content?: any;
  priority?: number;
  duration?: number; // 0 to remain open
};

export type NotificationAlert = {
  type: AntType;
} & NotificationOptions;

export type NotificationAlertBuilder = (
  notification: NotificationAlert
) => NotificationAlert;
