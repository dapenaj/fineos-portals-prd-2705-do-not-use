import { Benefit, DisabilityBenefitSummary } from 'fineos-js-api-client';

import { ClaimBenefit } from './claim-benefit.type';

export type DisabilityBenefitWrapper = {
  // carry types from original definition
  claimId: ClaimBenefit['claimId'];
  benefitId: Benefit['benefitId'];
  disabilityBenefitSummary: DisabilityBenefitSummary;
};
