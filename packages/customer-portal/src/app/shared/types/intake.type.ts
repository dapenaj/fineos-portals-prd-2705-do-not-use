import React from 'react';

export type IntakeSection<T> = {
  id: IntakeSectionId;
  title: React.ReactNode;
  isActive: boolean;
  isComplete: boolean;
  skip?: boolean;
  value?: T;
  steps: IntakeSectionStep<any>[];
};

export type IntakeSectionStep<T> = {
  id: IntakeSectionStepId;
  title: React.ReactNode;
  isComplete: boolean;
  isActive: boolean;
  skip?: boolean;
  value?: T;
};

export type IntakeSectionStepSubmit<T> = {
  skipSteps?: IntakeSectionStepId[];
} & IntakeSectionStep<T>;

export enum IntakeSectionId {
  YOUR_REQUEST = 0,
  DETAILS = 1,
  WRAP_UP = 2
}

export enum IntakeSectionStepId {
  ADDITIONAL_INCOME_SOURCES = 'ADDITIONAL_INCOME_SOURCES',
  SUPPORTING_EVIDENCE = 'SUPPORTING_EVIDENCE',
  PERSONAL_DETAILS = 'PERSONAL_DETAILS',
  WORK_DETAILS = 'WORK_DETAILS',
  INITIAL_REQUEST = 'INITIAL_REQUEST',
  REQUEST_DETAILS = 'REQUEST_DETAILS',
  TIME_OFF = 'TIME_OFF'
}
