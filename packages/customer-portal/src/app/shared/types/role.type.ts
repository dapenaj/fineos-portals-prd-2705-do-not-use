export enum Role {
  ABSENCE_USER = 'absenceUser',
  ABSENCE_SUPERVISOR = 'absenceSupervisor',
  CLAIMS_USER = 'claimsUser'
}
