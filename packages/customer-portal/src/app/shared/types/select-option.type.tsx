import React from 'react';

export type SelectOption = {
  value: string | number;
  text: React.ReactNode;
};
