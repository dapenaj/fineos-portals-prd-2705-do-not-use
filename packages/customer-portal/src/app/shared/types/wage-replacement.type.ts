export enum WageReplacementStatus {
  PENDING = 'Pending',
  DECIDED = 'Decided',
  CLOSED = 'Closed'
}

export type WageReplacementDisplayStatus =
  | 'Pending'
  | 'Approved'
  | 'Denied'
  | 'Decided'
  | 'Closed';
