export type AbsenceAmendment = {
  absenceCaseId: string;
  absencePeriodType: string;
  periodId: string;
  startDate: string;
  endDate: string;
  reasonName: string;
  statusCategory?: string;
};
