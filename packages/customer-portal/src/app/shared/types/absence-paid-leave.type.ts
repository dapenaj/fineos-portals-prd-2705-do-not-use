import { ClaimBenefit } from './claim-benefit.type';

export type AbsencePaidLeave = {
  claimBenefits: ClaimBenefit[];
  paidLeaveHandler: string;
  paidLeaveHandlerPhoneNumber: string;
};
