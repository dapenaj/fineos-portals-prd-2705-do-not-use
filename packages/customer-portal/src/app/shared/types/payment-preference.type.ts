export enum PaymentType {
  CHECK = 'Check',
  ELECTRONIC_FUNDS_TRANSFER = 'Elec Funds Transfer',
  UNKNOWN = 'Unknown'
}

export enum PaymentPreferenceAccountType {
  UNKNOWN = 'UNKNOWN',
  CHECKING = 'CHECKING',
  SAVINGS = 'SAVINGS'
}

export enum CheckPaymentMethod {
  HAND_TYPED_CHECK = 'Hand Typed Check',
  INDIVIDUAL_CHECK = 'Individual Check',
  GROUP_CHECK = 'Group Check',
  RETURN_HAND_TYPED_CHECK = 'Return Hand Typed Check',
  RETURN_INDIVIDUAL_CHECK = 'Return Individual Check',
  RETURN_GROUP_CHECK = 'Return Group Check',
  CHEQUE = 'Cheque',
  CHECK = 'Check'
}

export enum ElectronicPaymentMethod {
  DIRECT_DEBIT = 'Direct Debit',
  GIRO_GMU = 'Giro - GMU',
  PREMIUM_DEPOSIT = 'Premium Deposit',
  STOCK_ACCOUNT = 'Stock Account',
  GROUP_PAYMENTS = 'Group Payments',
  STANDING_ORDER = 'Standing Order',
  CURRENT_ACCOUNT = 'Current Account',
  TRANSFER_PAYMENT = 'Transfer Payment',
  GIRO_STD = 'Giro - STD',
  DISABILITY_ELEC_FUNDS_TRANSFER = 'DISABILITY - Elec Fnds Trnsfr',
  WIRE_TRANSFER = 'Wire Transfer',
  ELEC_FUNDS_TRANSFER = 'Elec Funds Transfer'
}

export enum UnknownPaymentMethod {
  UNKNOWN = 'Unknown',
  LIFE_INTEREST_DRAFT_ACCOUNT = 'Life - Interest Draft Account',
  LIFE_CONVERSION = 'Life - Conversion',
  LIFE_LOST_POLICYHOLDER_ACCOUNT = 'Life - Lost Policyholder Acc',
  LIFE_THIRD_PARTY = 'Life - Third Party',
  LIFE_ANNUITY = 'Life - Annuity',
  LIFE_ACCELERATED_LIFE_BEN = 'Life - Accelerated Life Ben',
  LIFE_NEW_PRINCIPAL_BANK = 'Life - New Principal Bank',
  ACCOUNTING = 'Accounting',
  ESCHEATMENT = 'Escheatment',
  INTERNAL_ADJUSTMENT = 'Internal Adjustment',
  RETURN_OF_PREMIUM = 'Return of Premium',
  RETAINED_ASSET = 'Retained Asset'
}

export enum PaymentTypeLabel {
  CHECK = 'CHECK',
  ELECTRONIC_FUNDS_TRANSFER = 'ELECTRONIC_FUNDS_TRANSFER'
}
