export type PhoneNumber = {
  areaCode: string;
  intCode: string;
  telephoneNo: string;
};
