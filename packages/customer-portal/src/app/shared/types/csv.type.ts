export type CsvDetails = {
  columns: string[];
  rows: any[][];
  filename: string;
};
