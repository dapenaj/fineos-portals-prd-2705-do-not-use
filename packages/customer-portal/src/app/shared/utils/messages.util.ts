import { SelectOption } from '../types';

export const MESSAGE_CASE_ID_SPLIT_CHAR = ',';

export const getNewMessageCaseId = (value: string) =>
  value.split(MESSAGE_CASE_ID_SPLIT_CHAR)[0];

export const getNewMessageSubject = (
  entitlements: SelectOption[],
  caseId: string,
  subject: string
) => {
  const entitlement = entitlements.find(({ value }) => value === caseId);

  return !!entitlement ? `${subject} (${entitlement.text})` : subject;
};
