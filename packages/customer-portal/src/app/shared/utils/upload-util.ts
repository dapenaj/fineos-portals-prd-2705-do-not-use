export const VALID_FILE_EXTENSIONS = [
  '.xml',
  '.txt',
  '.doc',
  '.pdf',
  '.ppt',
  '.xls',
  '.jpg',
  '.jpeg',
  '.png',
  '.vsd',
  '.msg',
  '.bmp',
  '.docx',
  '.xlsx',
  '.xlsm',
  '.pptx'
];

export const isValidFileExtension = (extension: string) =>
  VALID_FILE_EXTENSIONS.includes(extension);

export const getFileExtension = (filename: string) =>
  filename.slice(filename.lastIndexOf('.'));
