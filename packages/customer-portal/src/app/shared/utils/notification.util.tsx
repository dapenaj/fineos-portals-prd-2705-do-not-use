import {
  ClaimSummary,
  AbsenceSummary,
  NotificationAccommodationSummary
} from 'fineos-js-api-client';

import { NotificationStatus, NotificationStatusResult } from '../types';
import {
  NOTIFICATION_PENDING_STATUSES,
  NOTIFICATION_DECIDED_STATUSES,
  NOTIFICATION_CLOSED_STATUSES
} from '../constants';

const getStatuses = (
  claims?: ClaimSummary[],
  absences?: AbsenceSummary[],
  accommodations?: NotificationAccommodationSummary[]
): NotificationStatus[] => {
  const claimStatuses = claims ? claims.map(claim => claim.status) : [];
  const absenceStatuses = absences
    ? absences.map(absence => absence.status)
    : [];
  const accommodationStatuses = accommodations
    ? accommodations.map(accommodation => accommodation.status)
    : [];

  return [
    ...claimStatuses,
    ...absenceStatuses,
    ...accommodationStatuses
  ] as NotificationStatus[];
};

export const getNotificationStatus = (
  claims?: ClaimSummary[],
  absences?: AbsenceSummary[],
  accommodations?: NotificationAccommodationSummary[]
): NotificationStatusResult => {
  const statuses = getStatuses(claims, absences, accommodations);

  if (allNotificationsPending(statuses)) {
    return NotificationStatusResult.PENDING;
  } else if (allNotificationsClosed(statuses)) {
    return NotificationStatusResult.ALL_CLOSED;
  } else if (allNotificationsDecidedOrClosed(statuses)) {
    return NotificationStatusResult.DECIDED;
  } else if (someNotificationsDecidedOrClosed(statuses)) {
    return NotificationStatusResult.SOME_DECIDED;
  } else {
    return NotificationStatusResult.UNDETERMINED;
  }
};

export const allNotificationsPending = (
  statuses: NotificationStatus[]
): boolean =>
  !!statuses.length &&
  statuses.every(status => NOTIFICATION_PENDING_STATUSES.includes(status));

export const allNotificationsClosed = (
  statuses: NotificationStatus[]
): boolean =>
  !!statuses.length &&
  statuses.every(status => NOTIFICATION_CLOSED_STATUSES.includes(status));

export const someNotificationsDecidedOrClosed = (
  statuses: NotificationStatus[]
): boolean =>
  !!statuses.length &&
  statuses.some(
    status =>
      NOTIFICATION_DECIDED_STATUSES.includes(status) ||
      NOTIFICATION_CLOSED_STATUSES.includes(status)
  );

export const allNotificationsDecidedOrClosed = (
  statuses: NotificationStatus[]
): boolean =>
  !!statuses.length &&
  statuses.every(
    status =>
      NOTIFICATION_DECIDED_STATUSES.includes(status) ||
      NOTIFICATION_CLOSED_STATUSES.includes(status)
  );

export const getCaseProgress = (status: NotificationStatusResult): string => {
  switch (status) {
    case NotificationStatusResult.PENDING:
      return 'NOTIFICATIONS.NOTIFICATION_STATUS.PENDING';
    case NotificationStatusResult.SOME_DECIDED:
      return 'NOTIFICATIONS.NOTIFICATION_STATUS.SOME_DECIDED';
    case NotificationStatusResult.DECIDED:
      return 'NOTIFICATIONS.NOTIFICATION_STATUS.DECIDED';
    case NotificationStatusResult.ALL_CLOSED:
      return 'NOTIFICATIONS.NOTIFICATION_STATUS.ALL_CLOSED';
    default:
      return 'NOTIFICATIONS.NOTIFICATION_STATUS.UNDETERMINED';
  }
};

export const isNotificationClosed = (statuses: NotificationStatus[]): boolean =>
  statuses.every(status => NOTIFICATION_CLOSED_STATUSES.includes(status));

export const removeClosedNotifications = (
  claims?: ClaimSummary[],
  absences?: AbsenceSummary[],
  accommodations?: NotificationAccommodationSummary[]
): boolean => {
  const statuses = getStatuses(claims, absences, accommodations);

  return statuses.length < 1 || !isNotificationClosed(statuses);
};

export const getReasonQualifier = (qualifier: string) => {
  switch (qualifier) {
    case 'Postnatal Disability':
      return ` - Postpartum disability`;
    case 'Other':
      return ` - Termination`;
    default:
      return ` - ${qualifier}`;
  }
};
