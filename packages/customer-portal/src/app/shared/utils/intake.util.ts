import { EmailAddress, Phone } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { SelectOption } from '../types';
import { WORKPLACE_ACCOMMODATION_TYPES } from '../constants';

export const getCustomerEmail = (emails: EmailAddress[]): string => {
  const preferred = emails.find(email => email.preferred);

  if (preferred) {
    return preferred.emailAddress;
  }

  return !_isEmpty(emails) ? emails[0].emailAddress : '';
};

export const getCustomerPhoneNumber = (phones: Phone[]): Phone => {
  const preferred = phones.find(phone => phone.preferred);

  if (preferred) {
    return preferred;
  }

  return phones[0];
};

export const getAccommodationTypes = (category: string): SelectOption[] =>
  WORKPLACE_ACCOMMODATION_TYPES.filter(type => type.category === category);
