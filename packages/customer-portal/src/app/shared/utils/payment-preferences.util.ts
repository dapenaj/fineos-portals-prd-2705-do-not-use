import { PaymentPreference } from 'fineos-js-api-client';
import moment from 'moment';
import { isEmpty as _isEmpty } from 'lodash';

import {
  CheckPaymentMethod,
  PaymentType,
  ElectronicPaymentMethod,
  UnknownPaymentMethod,
  PaymentTypeLabel,
  PaymentPreferenceAccountType
} from '../types';

export const getDefaultPaymentPreference = (
  paymentPreferences: PaymentPreference[]
) => {
  const defaultPaymentPreference = paymentPreferences.find(
    ({ isDefault }) => isDefault
  );
  if (defaultPaymentPreference) {
    return defaultPaymentPreference;
  } else {
    const effectivePreferences = paymentPreferences.filter(
      ({ effectiveFrom, effectiveTo }) =>
        moment().isBetween(moment(effectiveFrom), moment(effectiveTo))
    );

    if (!_isEmpty(effectivePreferences)) {
      return effectivePreferences[0];
    } else {
      return null;
    }
  }
};

export const getCheckPaymentPreference = (
  paymentPreferences: PaymentPreference[] | null
): PaymentPreference | undefined => {
  let preferenceToReturn;

  if (!_isEmpty(paymentPreferences)) {
    const checkPreferences = paymentPreferences!.filter(({ paymentMethod }) =>
      CHECK_PAYMENT_METHODS.includes(paymentMethod as CheckPaymentMethod)
    );

    preferenceToReturn = !_isEmpty(checkPreferences)
      ? checkPreferences[0]
      : undefined;

    if (!_isEmpty(checkPreferences) && checkPreferences.length > 1) {
      const effectivePreferences = paymentPreferences!.filter(
        ({ effectiveFrom, effectiveTo }) =>
          moment().isBetween(moment(effectiveFrom), moment(effectiveTo))
      );

      preferenceToReturn = !_isEmpty(effectivePreferences)
        ? effectivePreferences[0]
        : checkPreferences[0];
    }
  }

  return preferenceToReturn;
};

export const CHECK_PAYMENT_METHODS: CheckPaymentMethod[] = [
  CheckPaymentMethod.HAND_TYPED_CHECK,
  CheckPaymentMethod.INDIVIDUAL_CHECK,
  CheckPaymentMethod.GROUP_CHECK,
  CheckPaymentMethod.RETURN_HAND_TYPED_CHECK,
  CheckPaymentMethod.RETURN_INDIVIDUAL_CHECK,
  CheckPaymentMethod.RETURN_GROUP_CHECK,
  CheckPaymentMethod.CHEQUE,
  CheckPaymentMethod.CHECK
];

export const ELECTRONIC_PAYMENT_METHODS: ElectronicPaymentMethod[] = [
  ElectronicPaymentMethod.DIRECT_DEBIT,
  ElectronicPaymentMethod.GIRO_GMU,
  ElectronicPaymentMethod.PREMIUM_DEPOSIT,
  ElectronicPaymentMethod.STOCK_ACCOUNT,
  ElectronicPaymentMethod.GROUP_PAYMENTS,
  ElectronicPaymentMethod.STANDING_ORDER,
  ElectronicPaymentMethod.CURRENT_ACCOUNT,
  ElectronicPaymentMethod.TRANSFER_PAYMENT,
  ElectronicPaymentMethod.GIRO_STD,
  ElectronicPaymentMethod.DISABILITY_ELEC_FUNDS_TRANSFER,
  ElectronicPaymentMethod.WIRE_TRANSFER,
  ElectronicPaymentMethod.ELEC_FUNDS_TRANSFER
];

export const UNKNOWN_PAYMENT_METHODS: UnknownPaymentMethod[] = [
  UnknownPaymentMethod.UNKNOWN,
  UnknownPaymentMethod.LIFE_INTEREST_DRAFT_ACCOUNT,
  UnknownPaymentMethod.LIFE_CONVERSION,
  UnknownPaymentMethod.LIFE_LOST_POLICYHOLDER_ACCOUNT,
  UnknownPaymentMethod.LIFE_THIRD_PARTY,
  UnknownPaymentMethod.LIFE_ANNUITY,
  UnknownPaymentMethod.LIFE_ACCELERATED_LIFE_BEN,
  UnknownPaymentMethod.LIFE_NEW_PRINCIPAL_BANK,
  UnknownPaymentMethod.ACCOUNTING,
  UnknownPaymentMethod.ESCHEATMENT,
  UnknownPaymentMethod.INTERNAL_ADJUSTMENT,
  UnknownPaymentMethod.RETURN_OF_PREMIUM,
  UnknownPaymentMethod.RETAINED_ASSET
];

export const getPaymentType = ({
  paymentMethod
}: PaymentPreference): PaymentType => {
  if (CHECK_PAYMENT_METHODS.includes(paymentMethod as CheckPaymentMethod)) {
    return PaymentType.CHECK;
  } else if (
    ELECTRONIC_PAYMENT_METHODS.includes(
      paymentMethod as ElectronicPaymentMethod
    )
  ) {
    return PaymentType.ELECTRONIC_FUNDS_TRANSFER;
  } else if (
    UNKNOWN_PAYMENT_METHODS.includes(paymentMethod as UnknownPaymentMethod)
  ) {
    return PaymentType.UNKNOWN;
  }

  return PaymentType.CHECK;
};

export const getPaymentTypeLabel = (type: PaymentType) =>
  type === PaymentType.ELECTRONIC_FUNDS_TRANSFER
    ? PaymentTypeLabel.ELECTRONIC_FUNDS_TRANSFER
    : PaymentTypeLabel.CHECK;

export const getPaymentPreferenceAccountType = (
  type: PaymentPreferenceAccountType
) => (type === PaymentPreferenceAccountType.CHECKING ? 'Checking' : 'Savings');
