// https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem

const PERCENT_ENCODING_REGEX = /%([0-9A-F]{2})/g;

const toSolidBytes = (p1: any) => String.fromCharCode(('0x' + p1) as any);

// first we use encodeURIComponent to get percent-encoded UTF-8,
// then we convert the percent encodings into raw bytes which
// can be fed into btoa.
export const b64EncodeUnicode = (str: string) =>
  btoa(
    encodeURIComponent(str).replace(PERCENT_ENCODING_REGEX, (match, p1) =>
      toSolidBytes(p1)
    )
  );
