import { ApiError } from 'fineos-js-api-client';

import { KeyValue, AntType } from '../types';

export const getErrorMessageKeyValue = (
  property: string,
  message: any,
  json: any
): KeyValue => {
  if (message.hasOwnProperty('validationMessage')) {
    return { key: '', value: message.validationMessage };
  } else {
    return { key: property, value: json[property] };
  }
};

export const retrieveErrorMessages = (json: any): KeyValue[] => {
  const errorProps: KeyValue[] = [];

  for (const prop in json) {
    if (json.hasOwnProperty(prop)) {
      errorProps.push(getErrorMessageKeyValue(prop, json[prop], json));
    }
  }

  return errorProps;
};

export const retrieveNotificationType = (
  content: any,
  type: AntType
): AntType => {
  if (content instanceof ApiError) {
    switch (content.status) {
      case 500:
      case 503:
        return 'error';
      case 400:
      case 401:
      case 403:
      case 404:
      case 422:
        return 'warning';
      case 200:
      case 201:
        return 'success';
      default:
        return 'info';
    }
  }

  return type;
};
