import moment, { Moment } from 'moment';

import { AuthenticationURLParam, URLParseChar } from '../types';

import { API_DATE_FORMAT } from './formatting.util';

export const parseDate = (
  value: string,
  format: string = API_DATE_FORMAT
): Moment => moment(value, format);

export const parseUrl = (url: string, char: URLParseChar) =>
  url
    .substr(1)
    .split(char)
    .filter(Boolean)
    .reduce((params, pair) => {
      const [key, value] = pair.split('=');

      key === AuthenticationURLParam.DISPLAY_ROLE
        ? (params[key] = [decodeURIComponent(value)])
        : (params[key] = decodeURIComponent(value));

      return params;
    }, {} as { [key: string]: string | string[] });
