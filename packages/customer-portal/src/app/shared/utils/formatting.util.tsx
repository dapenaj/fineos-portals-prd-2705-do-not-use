import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Phone, ApiConfigProps, TeamMember } from 'fineos-js-api-client';
import moment, { Moment } from 'moment';
import { isEmpty as _isEmpty } from 'lodash';

import { PhoneNumber } from '../types';
import { ALPHA_REGEX } from '../constants';

export const DISPLAY_DATE_FORMAT = ApiConfigProps.displayDateFormat;
export const API_DATE_FORMAT = ApiConfigProps.dateOnlyFormat;
export const MESSAGES_DISPLAY_DATE_FORMAT = 'LLL';
export const EXPORT_DATE_FORMAT = 'DDMMYYYY_HH.mm';

export const formatPhoneToString = ({
  intCode,
  areaCode,
  telephoneNo
}: Phone) =>
  `${!_isEmpty(intCode) ? intCode + '-' : ''}${areaCode + '-'}${telephoneNo}`;

export const formatStringToPhoneNumber = (phone: string): PhoneNumber => {
  const values = phone.split('-');
  return {
    intCode: values[0],
    areaCode: values[1],
    telephoneNo: values[2]
  };
};

export const formatValidationMessage = (value: string): React.ReactNode => (
  <FormattedMessage id={value} />
);

export const formatDateForDisplay = (
  value: string | Moment,
  format: string = DISPLAY_DATE_FORMAT
): string =>
  moment(value).isValid() ? moment(value).format(format) : (value as string);

export const formatDateForApi = (
  value: string | Moment,
  format: string = API_DATE_FORMAT
) => moment(value).format(format);

export const formatTeamMemberName = ({ firstName, lastName }: TeamMember) =>
  `${firstName} ${lastName}`;

export const formatInitial = (value: string) =>
  value.replace(ALPHA_REGEX, '').charAt(0);
