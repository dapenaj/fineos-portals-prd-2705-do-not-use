import { Customer, Address } from 'fineos-js-api-client';

export const getCustomerAddress = (
  customer: Customer | null
): Address | null => {
  const hasAddress =
    !!customer &&
    !!customer.customerAddress &&
    !!customer.customerAddress.address;

  return hasAddress ? customer!.customerAddress.address : null;
};

export const isOutsideUSA = (address: Address | null) =>
  !!address && !!address.country && address.country !== 'USA';
