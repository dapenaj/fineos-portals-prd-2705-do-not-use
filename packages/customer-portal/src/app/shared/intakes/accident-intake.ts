import {
  StartClaimRequest,
  NewAbsenceRequest,
  AbsenceCaseNotifiedBy,
  IntakeSource
} from 'fineos-js-api-client';

import { CLAIM_CASE_TYPE } from '../constants';
import { IntakeSubmission, IntakeSubmissionResult } from '../services';
import { EventOptionId, RequestDetailFieldId } from '../types';

import { AbstractIntake, StartClaimRequestDetails } from './abstract-intake';

export class AccidentIntake extends AbstractIntake {
  static _instance: AccidentIntake;

  static getInstance(): AccidentIntake {
    return (
      AccidentIntake._instance ||
      (AccidentIntake._instance = new AccidentIntake())
    );
  }

  supports(model: IntakeSubmission): boolean {
    return model.isEventOptionSelected(EventOptionId.ACCIDENT);
  }

  protected buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest {
    const newAbsenceRequest = new NewAbsenceRequest();
    newAbsenceRequest.notifiedBy = AbsenceCaseNotifiedBy.Employee;
    newAbsenceRequest.intakeSource = IntakeSource.SelfService;
    newAbsenceRequest.reason = 'Serious Health Condition - Employee';
    newAbsenceRequest.notificationReason =
      'Accident or treatment required for an injury';
    newAbsenceRequest.additionalComments = '';

    this.setQualifiers(newAbsenceRequest, model);

    // create leave periods
    newAbsenceRequest.timeOffRequests = this.buildTimeOffLeavePeriod(
      model.getTimeOff().timeOffLeavePeriods
    );
    newAbsenceRequest.episodicRequests = this.buildEpisodicLeavePeriod(
      model.getTimeOff().episodicLeavePeriods
    );
    newAbsenceRequest.reducedScheduleRequests = this.buildReducedSchedule(
      model.getTimeOff().reducedScheduleLeavePeriods
    );

    return newAbsenceRequest;
  }

  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): StartClaimRequestDetails | undefined {
    const claimRequest = new StartClaimRequest();
    // temporary until the API can be updated to make this optional;
    claimRequest.notificationCaseId = result.createdNotificationId;
    claimRequest.notificationReason =
      'Accident or treatment required for an injury';
    claimRequest.claimIncurredDate = model.getRequestDetailFieldValue(
      RequestDetailFieldId.ACCIDENT_DATE
    );

    return {
      claimCaseType: CLAIM_CASE_TYPE,
      request: claimRequest
    };
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[] {
    return [
      // for Claim
      (r, m) => this.submitMedicalDetailsRequest(r, m),
      (r, m) => this.submitUpdateDisabilityRequest(r, m),
      (r, m) => this.submitHospitalisationRequestForClaim(r, m),

      // for Absence
      (r, m) => this.submitAdditionalConditionsRequest(r, m),
      (r, m) => this.submitHospitalisationRequestForAbsence(r, m),
      (r, m) => this.submitNotificationForMultipleTimeOffs(r, m)
    ];
  }

  /**
   * Set the values for reasonQualifier1 and reasonQualifier2
   */
  private setQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    newAbsenceRequest.reasonQualifier2 = 'Accident / Injury';
    const fieldValue = model.getRequestDetailFieldValue(
      RequestDetailFieldId.ACCIDENT_WORK_RELATED
    );
    switch (fieldValue) {
      case RequestDetailFieldId.YES:
        newAbsenceRequest.reasonQualifier1 = 'Work Related';
        break;
      case RequestDetailFieldId.NO:
        newAbsenceRequest.reasonQualifier1 = 'Not Work Related';
        break;
      default:
        throw new Error(
          `Unsupported value: ${fieldValue} for request field ${RequestDetailFieldId.ACCIDENT_WORK_RELATED}`
        );
    }
  }
}
