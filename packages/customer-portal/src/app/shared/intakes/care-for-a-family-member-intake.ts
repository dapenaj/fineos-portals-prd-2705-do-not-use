import {
  NewAbsenceRequest,
  IntakeSource,
  AbsenceCaseNotifiedBy
} from 'fineos-js-api-client';

import { IntakeSubmission, IntakeSubmissionResult } from '../services';
import { EventOptionId } from '../types';

import { AbstractIntake } from './abstract-intake';

export class CareForAFamilyMemberIntake extends AbstractIntake {
  static _instance: CareForAFamilyMemberIntake;

  static getInstance(): CareForAFamilyMemberIntake {
    return (
      CareForAFamilyMemberIntake._instance ||
      (CareForAFamilyMemberIntake._instance = new CareForAFamilyMemberIntake())
    );
  }

  supports(model: IntakeSubmission): boolean {
    return model.isEventOptionSelected(
      EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS,
      EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER
    );
  }

  protected buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest {
    const newAbsenceRequest = new NewAbsenceRequest();
    newAbsenceRequest.intakeSource = IntakeSource.SelfService;
    newAbsenceRequest.notifiedBy = AbsenceCaseNotifiedBy.Employee;
    newAbsenceRequest.reason = 'Care for a Family Member';
    newAbsenceRequest.notificationReason = 'Caring for a family member';
    newAbsenceRequest.notificationCaseId = '';
    this.setCareForAFamilyMemberReasonQualifiers(newAbsenceRequest, model);

    newAbsenceRequest.timeOffRequests = this.buildTimeOffLeavePeriod(
      model.getTimeOff().timeOffLeavePeriods
    );
    newAbsenceRequest.episodicRequests = this.buildEpisodicLeavePeriod(
      model.getTimeOff().episodicLeavePeriods
    );
    newAbsenceRequest.reducedScheduleRequests = this.buildReducedSchedule(
      model.getTimeOff().reducedScheduleLeavePeriods
    );

    return newAbsenceRequest;
  }

  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) {
    return undefined;
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[] {
    return [(r, m) => this.submitHospitalisationRequestForAbsence(r, m)];
  }

  private setCareForAFamilyMemberReasonQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    switch (model.selectedEventOption) {
      case EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS:
        newAbsenceRequest.reasonQualifier1 = 'Serious Health Condition';
        break;
      case EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREVENTIVE_CARE:
        newAbsenceRequest.reasonQualifier1 =
          'Sickness - Non-Serious Health Condition';
        break;
      case EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREGNANCY:
        newAbsenceRequest.reasonQualifier1 = 'Pregnancy Related';
        break;
      case EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_YES:
        newAbsenceRequest.reasonQualifier1 = 'Right to Leave';
        newAbsenceRequest.reasonQualifier2 = 'Medical Related';
        break;
      case EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_NO:
        newAbsenceRequest.reasonQualifier1 = 'Right to Leave';
        newAbsenceRequest.reasonQualifier2 = 'Non Medical';
        break;
    }
  }
}
