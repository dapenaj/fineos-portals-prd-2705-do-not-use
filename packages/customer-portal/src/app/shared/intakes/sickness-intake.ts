import {
  AbsenceCaseNotifiedBy,
  IntakeSource,
  NewAbsenceRequest,
  StartClaimRequest
} from 'fineos-js-api-client';

import { CLAIM_CASE_TYPE } from '../constants/intake.constant';
import { IntakeSubmission, IntakeSubmissionResult } from '../services';
import { EventOptionId, RequestDetailFieldId } from '../types';

import { AbstractIntake, StartClaimRequestDetails } from './abstract-intake';

export class SicknessIntake extends AbstractIntake {
  static _instance: SicknessIntake;

  static getInstance(): SicknessIntake {
    return (
      SicknessIntake._instance ||
      (SicknessIntake._instance = new SicknessIntake())
    );
  }

  supports(model: IntakeSubmission): boolean {
    return model.isEventOptionSelected(EventOptionId.SICKNESS);
  }

  protected buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest {
    const newAbsenceRequest = new NewAbsenceRequest();
    this.setAdditionalComments(newAbsenceRequest, model);
    newAbsenceRequest.notifiedBy = AbsenceCaseNotifiedBy.Employee;
    newAbsenceRequest.intakeSource = IntakeSource.SelfService;
    newAbsenceRequest.notificationReason =
      'Sickness, treatment required for a medical condition or any other medical procedure';
    this.setReasonAndQualifiers(newAbsenceRequest, model);

    newAbsenceRequest.timeOffRequests = this.buildTimeOffLeavePeriod(
      model.getTimeOff().timeOffLeavePeriods
    );
    newAbsenceRequest.episodicRequests = this.buildEpisodicLeavePeriod(
      model.getTimeOff().episodicLeavePeriods
    );
    newAbsenceRequest.reducedScheduleRequests = this.buildReducedSchedule(
      model.getTimeOff().reducedScheduleLeavePeriods
    );

    return newAbsenceRequest;
  }

  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): StartClaimRequestDetails {
    const claimRequest = new StartClaimRequest();

    claimRequest.notificationCaseId = result.createdNotificationId;
    claimRequest.notificationReason =
      'Sickness, treatment required for a medical condition or any other medical procedure';

    const earliestTimeOffLeavePeriod = this.findEarliestTimeOffLeavePeriod(
      model
    );

    if (earliestTimeOffLeavePeriod) {
      claimRequest.claimIncurredDate = earliestTimeOffLeavePeriod.startDate;
    }

    return {
      claimCaseType: CLAIM_CASE_TYPE,
      request: claimRequest
    };
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[] {
    return [
      (r, m) => this.submitAdditionalConditionsRequest(r, m),
      (r, m) => this.submitMedicalDetailsRequest(r, m),
      (r, m) => this.submitUpdateDisabilityRequest(r, m),
      (r, m) => this.submitHospitalisationRequestForAbsence(r, m),
      (r, m) => this.submitHospitalisationRequestForClaim(r, m),
      (r, m) => this.submitNotificationForMultipleTimeOffs(r, m)
    ];
  }

  private setAdditionalComments(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    if (
      model.selectedEventOption === EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS
    ) {
      newAbsenceRequest.additionalComments = model.getRequestDetailFieldValue(
        RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS
      );
    }

    if (model.isEventOptionSelected(EventOptionId.SICKNESS_TIMEOFF_DONATION)) {
      newAbsenceRequest.additionalComments = model.getRequestDetailFieldValue(
        RequestDetailFieldId.DESCRIBE_PROCEDURE
      );
    }
  }

  private setReasonAndQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    switch (model.selectedEventOption) {
      case EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS:
        this.setSicknessReason(newAbsenceRequest, model);
        break;
      case EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE:
        newAbsenceRequest.reason = 'Preventative Care - Employee';
        break;
      case EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD:
      case EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL:
      case EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW:
      case EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN:
        newAbsenceRequest.reason = 'Medical Donation - Employee';
        this.setDonationQualifiers(newAbsenceRequest, model);
        break;
      default:
        throw new Error('Unsupported option: ' + model.selectedEventOption);
    }
  }

  private setSicknessReason(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    if (
      model.hasRequestDetailFieldValue(
        RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY
      ) ||
      model.hasRequestDetailFieldValue(
        RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY
      )
    ) {
      if (
        model.isTrue(RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY) ||
        model.isTrue(RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY)
      ) {
        newAbsenceRequest.reason = 'Serious Health Condition - Employee';
        this.setSicknessQualifiers(newAbsenceRequest, model);
      } else {
        if (
          model.isTrue(RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT) ||
          model.isTrue(RequestDetailFieldId.IS_SICKNESS_CHRONIC) ||
          model.isTrue(RequestDetailFieldId.IS_MULTIPLE_TREATMENT)
        ) {
          newAbsenceRequest.reason = 'Serious Health Condition - Employee';
          this.setSicknessQualifiers(newAbsenceRequest, model);
        } else {
          newAbsenceRequest.reason =
            'Sickness - Non-Serious Health Condition - Employee';
        }
      }
    } else {
      newAbsenceRequest.reason =
        'Sickness - Non-Serious Health Condition - Employee';
    }
  }

  private setSicknessQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    if (
      model.hasRequestDetailFieldValue(
        RequestDetailFieldId.IS_SICKNESS_WORK_RELATED
      )
    ) {
      newAbsenceRequest.reasonQualifier2 = 'Sickness';

      if (model.isTrue(RequestDetailFieldId.IS_SICKNESS_WORK_RELATED)) {
        newAbsenceRequest.reasonQualifier1 = 'Work Related';
      } else {
        newAbsenceRequest.reasonQualifier1 = 'Not Work Related';
      }
    }
  }

  private setDonationQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    if (
      model.isEventOptionSelected(EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD)
    ) {
      newAbsenceRequest.reasonQualifier1 = 'Blood';
    } else if (
      model.isEventOptionSelected(
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL
      )
    ) {
      newAbsenceRequest.reasonQualifier1 = 'Blood Stem Cell';
    } else if (
      model.isEventOptionSelected(
        EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW
      )
    ) {
      newAbsenceRequest.reasonQualifier1 = 'Bone Marrow';
    } else {
      newAbsenceRequest.reasonQualifier1 = 'Organ';
    }
  }
}
