import {
  AccommodationRequest,
  AccommodationService
} from 'fineos-js-api-client';

import {
  IntakeAccommodationSubmission,
  IntakeSubmissionResult,
  AuthorizationService
} from '../services';
import { EventOptionId } from '../types';

import { IntakeHelpers } from './intake-helpers';

export class AccommodationIntake extends IntakeHelpers {
  static _instance: AccommodationIntake;

  private authorizationService: AuthorizationService = AuthorizationService.getInstance();
  private accommodationService: AccommodationService = AccommodationService.getInstance();

  static getInstance(): AccommodationIntake {
    return (
      AccommodationIntake._instance ||
      (AccommodationIntake._instance = new AccommodationIntake())
    );
  }

  supports(model: IntakeAccommodationSubmission): boolean {
    return model.isEventOptionSelected(EventOptionId.ACCOMMODATION);
  }

  async process(
    model: IntakeAccommodationSubmission
  ): Promise<IntakeSubmissionResult> {
    const result: IntakeSubmissionResult = {
      createdClaim: {},
      createdAbsence: {},
      createdAccommodation: {},
      postCaseErrors: [],
      isClaimRequired: false,
      isAbsenceRequired: false
    };

    return await this.requestInitialAccommodation(result, model);
  }

  protected isAbsenceUser(): boolean {
    return this.authorizationService.isAbsenceUser;
  }

  protected buildStartAccommodationRequest(
    model: IntakeAccommodationSubmission
  ): AccommodationRequest {
    const newAccommodationRequest = new AccommodationRequest();
    newAccommodationRequest.pregnancyRelated = model.getAccommodation().pregnancyRelated;
    newAccommodationRequest.limitations = model.getAccommodation().limitations;
    newAccommodationRequest.workPlaceAccommodations = model.getAccommodation().workPlaceAccommodations;
    newAccommodationRequest.additionalNotes = model.getAccommodation().additionalNotes;
    newAccommodationRequest.notificationReason =
      'Accommodation required to remain at work';
    newAccommodationRequest.notificationCaseId = '';

    return newAccommodationRequest;
  }

  /**
   * Always resolves ok, even on an error - you need to check the result parameter
   */
  private async requestInitialAccommodation(
    result: IntakeSubmissionResult,
    model: IntakeAccommodationSubmission
  ): Promise<IntakeSubmissionResult> {
    if (this.isAbsenceUser()) {
      const request = this.buildStartAccommodationRequest(model);
      if (request) {
        try {
          const accommodation = await this.accommodationService.startAccommodation(
            request
          );
          result.createdNotificationId = accommodation.notificationCaseId;
          result.createdAccommodation = {
            id: accommodation.accommodationCaseId
          };
          return result;
        } catch (error) {
          result.createdAccommodation = {
            error
          };
          return result;
        }
      }
    }
    return Promise.resolve(result);
  }
}
