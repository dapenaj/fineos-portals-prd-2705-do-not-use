import {
  AbsenceHospitalisationDetails,
  AbsenceService,
  AdditionalConditionsRequest,
  AdditionalConditionsService,
  ClaimService,
  ReducedSchedulePeriodRequest,
  RequestDate,
  TimeOffPeriodRequest,
  UpdateDisabilityDetailRequest,
  UpdateHospitalisationDetailsRequest,
  UpdateMedicalDetailsRequest
} from 'fineos-js-api-client';
import moment from 'moment';

import {
  EpisodicLeavePeriod,
  EventOptionId,
  ReducedScheduleLeavePeriod,
  RequestDetailFieldId,
  TimeOffLeavePeriod
} from '../types';
import { IntakeSubmission, IntakeSubmissionResult } from '../services';
import { parseDate, API_DATE_FORMAT } from '../utils';
import { DISABILITY_REQUEST_SOURCE } from '../constants';

/**
 * An abstract intake helper class
 *
 * Provides helper methods for:
 * - building time off periods
 * - common post case request methods
 */
export class IntakeHelpers {
  protected absenceService: AbsenceService = AbsenceService.getInstance();
  protected claimService: ClaimService = ClaimService.getInstance();
  protected additionalConditionsService = AdditionalConditionsService.getInstance();

  protected buildTimeOffLeavePeriod(
    periods?: TimeOffLeavePeriod[]
  ): TimeOffPeriodRequest[] {
    if (periods) {
      return periods.map(
        ({
          startDate,
          endDate,
          endDateOffHours,
          endDateOffMinutes,
          lastDayWorked,
          status,
          expectedReturnToWorkDate,
          startDateFullDay,
          startDateOffHours,
          startDateOffMinutes,
          endDateFullDay
        }) => {
          const request = new TimeOffPeriodRequest();
          request.startDate = new RequestDate(startDate);
          request.endDate = new RequestDate(
            endDate,
            endDateOffHours,
            endDateOffMinutes
          );
          request.status = status;

          request.lastDayWorked = lastDayWorked;
          request.expectedReturnToWorkDate = expectedReturnToWorkDate;
          request.startDateFullDay = startDateFullDay;
          request.startDateOffHours = startDateOffHours;
          request.startDateOffMinutes = startDateOffMinutes;
          request.endDateFullDay = endDateFullDay;
          request.endDateOffHours = endDateOffHours;
          request.endDateOffMinutes = endDateOffMinutes;

          return request;
        }
      );
    }
    return [];
  }

  protected buildEpisodicLeavePeriod(
    periods?: EpisodicLeavePeriod[]
  ): TimeOffPeriodRequest[] {
    if (periods) {
      return periods.map(({ startDate, endDate }) => {
        const request = new TimeOffPeriodRequest();
        request.startDate = new RequestDate(startDate);

        if (endDate) {
          request.endDate = new RequestDate(endDate);
        }

        return request;
      });
    }
    return [];
  }

  protected buildReducedSchedule(
    periods?: ReducedScheduleLeavePeriod[]
  ): ReducedSchedulePeriodRequest[] {
    if (periods) {
      return periods.map(({ startDate, endDate, status }) => {
        const request = new ReducedSchedulePeriodRequest();
        request.startDate = startDate;
        request.endDate = endDate;
        request.status = status;

        return request;
      });
    }
    return [];
  }

  protected findEarliestTimeOffLeavePeriod(
    model: IntakeSubmission
  ): TimeOffLeavePeriod | undefined {
    const timeoff = model.getTimeOff();
    if (timeoff.timeOffLeavePeriods && timeoff.timeOffLeavePeriods.length > 0) {
      const sortedByEarliest = [...timeoff.timeOffLeavePeriods].sort(
        (left, right) => {
          const leftDate = parseDate(left.startDate);
          const rightDate = parseDate(right.startDate);
          if (leftDate > rightDate) {
            return 1;
          } else if (leftDate < rightDate) {
            return -1;
          } else {
            return 0;
          }
        }
      );

      return sortedByEarliest[0];
    }
  }

  protected submitHospitalisationRequestForAbsence(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdAbsence.id) {
      const details = new AbsenceHospitalisationDetails();
      details.caseId = result.createdAbsence.id;
      // we have to send this form even if we don't stay overnight in hospital
      // we will overwrite this below if needs be
      details.ovenightAtHospital = false;

      model.ifRequestDetailFirstFieldPresent(
        [
          RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITAL_STAY,
          RequestDetailFieldId.ACCIDENT_IS_OVERNIGHT_HOSPITAL_STAY
        ],
        field => {
          // check if we are staying overnight
          details.ovenightAtHospital = model.isTrueValue(field);

          // set the dates if we have the,
          model.ifRequestDetailFirstFieldPresent(
            [
              RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION,
              RequestDetailFieldId.DONATION_OVERNIGHT_DURATION,
              RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
              RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITALIZATION_DATE,
              RequestDetailFieldId.OTHER_OVERNIGHT_HOSPITAL_STAY_DURATION
            ],
            (dates: string[]) => {
              details.startDate = dates[0];
              details.endDate = dates[1];
            }
          );
        }
      );

      // send the dates we have
      return this.absenceService.addHospitalisationDetails(details);
    }
    return Promise.resolve(result);
  }

  protected submitHospitalisationRequestForClaim(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdClaim.id) {
      const detailsRequest = new UpdateHospitalisationDetailsRequest();

      const isRequestRequired = model.ifRequestDetailFirstFieldPresent(
        [
          RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION,
          RequestDetailFieldId.DONATION_OVERNIGHT_DURATION,
          RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION
        ],
        (dates: string[]) => {
          // set the dates
          detailsRequest.startDate = dates[0];
          detailsRequest.endDateConfirmed = dates.length === 2;
          if (detailsRequest.endDateConfirmed) {
            detailsRequest.endDate = dates[1];
          }
        }
      );

      if (isRequestRequired) {
        return this.claimService.addHospitalisationDetails(
          result.createdClaim.id,
          detailsRequest
        );
      }
    }
    return Promise.resolve(result);
  }

  protected submitMedicalDetailsRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdClaim.id) {
      const request = new UpdateMedicalDetailsRequest();

      // we will only submit this if we have something to send
      let isRequestRequired = model.ifRequestDetailFirstFieldPresent(
        [
          RequestDetailFieldId.FIRST_SYMPTOM_DATE,
          RequestDetailFieldId.DONATION_DATE
        ],
        field => (request.symptomsFirstAppeared = field)
      );

      isRequestRequired =
        model.ifRequestDetailFieldPresent(
          RequestDetailFieldId.DATE_FIRST_TREATED,
          field => (request.firstDoctorVisitDate = field)
        ) || isRequestRequired;

      if (model.isEventOptionSelected(EventOptionId.PREGNANCY_TERMINATION)) {
        request.condition = 'Miscarriage';
        isRequestRequired = true;
      } else {
        isRequestRequired =
          model.ifRequestDetailFirstFieldPresent(
            [
              RequestDetailFieldId.DESCRIBE_SICKNESS,
              RequestDetailFieldId.DESCRIBE_PROCEDURE,
              RequestDetailFieldId.DESCRIBE_ACCIDENT,
              RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
              RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS
            ],
            field => {
              request.condition = field;
            }
          ) || isRequestRequired;
      }

      if (isRequestRequired) {
        return this.claimService.updateMedicalDetails(
          result.createdClaim.id,
          request
        );
      }
    }

    return Promise.resolve(result);
  }

  protected submitUpdateDisabilityRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdClaim.id) {
      const request = new UpdateDisabilityDetailRequest();

      const isAccidentIntake = model.isEventOptionSelected(
        EventOptionId.ACCIDENT
      );

      // "simple" fields
      request.eventType = isAccidentIntake ? 'Accident' : 'Sickness';
      request.claimType = 'STD';
      request.source = DISABILITY_REQUEST_SOURCE;
      request.notificationDate = moment().format(API_DATE_FORMAT);

      // fields based on time-off values
      const earliestPeriod = this.findEarliestTimeOffLeavePeriod(model);
      if (earliestPeriod) {
        request.employeeDateLastWorked = earliestPeriod.lastDayWorked;
        request.firstDayMissedWork = earliestPeriod.startDate;
        request.expectedReturnToWorkDate = earliestPeriod.expectedReturnToWorkDate
          ? earliestPeriod.expectedReturnToWorkDate
          : earliestPeriod.endDate;

        if (!isAccidentIntake) {
          request.claimIncurredDate = earliestPeriod.startDate;
          request.disabilityDateFromCustomer = earliestPeriod.startDate;
        }
      }

      // fields based on request details values
      if (isAccidentIntake) {
        request.workRelated = model.isTrueValue(
          model.getRequestDetailFieldValue(
            RequestDetailFieldId.ACCIDENT_WORK_RELATED
          )
        );
      } else {
        request.workRelated = false;
      }

      model.ifRequestDetailFieldPresent(
        RequestDetailFieldId.ACCIDENT_DATE,
        field => {
          request.claimIncurredDate = field;
          request.disabilityDateFromCustomer = field;
          request.accidentDate = field;
        }
      );

      model.ifRequestDetailFirstFieldPresent(
        [
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS
        ],
        field => (request.claimAdditionalInfo = field)
      );

      model.ifRequestDetailFirstFieldPresent(
        [
          RequestDetailFieldId.FIRST_SYMPTOM_DATE,
          RequestDetailFieldId.DONATION_DATE
        ],
        field => (request.dateSymptomsFirstAppeared = field)
      );

      return this.claimService.updateDiabilityDetails(
        result.createdClaim.id,
        request
      );
    }

    return Promise.resolve(result);
  }

  protected submitNotificationForMultipleTimeOffs(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdAbsence.id && result.createdClaim.id) {
      const periods = model.getTimeOff().timeOffLeavePeriods;
      if (periods && periods.length > 1) {
        this.claimService.indicateMultiplePeriodsInAbsence(
          result.createdClaim.id,
          result.createdAbsence.id
        );
      }
    }
    return Promise.resolve(result);
  }

  protected submitAdditionalConditionsRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdAbsence.id) {
      const additionalConditions = new AdditionalConditionsRequest();

      const isRequestRequired = model.ifRequestDetailFirstFieldPresent(
        [
          RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS
        ],
        val => {
          additionalConditions.caseId = result.createdAbsence.id!;
          additionalConditions.additionalDescription = val;
        }
      );

      if (isRequestRequired) {
        return this.additionalConditionsService.addAdditionalConditions(
          additionalConditions
        );
      }
    }
    return Promise.resolve(result);
  }
}
