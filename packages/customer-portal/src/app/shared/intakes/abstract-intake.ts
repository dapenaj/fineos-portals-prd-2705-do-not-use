import {
  StartClaimRequest,
  NewAbsenceRequest,
  AbsenceReasonSearch,
  AbsenceReasonSummary
} from 'fineos-js-api-client';

import { MEDICAL_PROVIDER_MATCH } from '../constants';
import { AuthorizationService } from '../services/authorization';
import {
  IntakeSubmission,
  IntakeSubmissionResult
} from '../services/intake/submission';

import { IntakeHelpers } from './intake-helpers';

export type StartClaimRequestDetails = {
  claimCaseType: string;
  request: StartClaimRequest;
};

/**
 * A template method for the other intake submissions.
 *
 * The implementations need to define methods for:
 * - supports - given a model, is this submission going to be implemented?
 * - buildStartAbsenceRequest - given a model, what new absence should be created
 * - buildStartClaimRequest - given a model, what new claim should be created
 * - getPostCaseRequests - what requests should be performed after the case is created - these are invoked in parallel
 *
 */
export abstract class AbstractIntake extends IntakeHelpers {
  private authorizationService: AuthorizationService = AuthorizationService.getInstance();

  /**
   * Checks if the submission supports this model
   * @param model the model
   */
  abstract supports(model: IntakeSubmission): boolean;
  /**
   *
   * @param model Process the given model
   */
  async process(model: IntakeSubmission): Promise<IntakeSubmissionResult> {
    const result: IntakeSubmissionResult = {
      createdClaim: {},
      createdAbsence: {},
      postCaseErrors: [],
      isClaimRequired: false,
      isAbsenceRequired: false
    };

    // build the case and then enrich - assuming nothing has been rejected
    await this.requestInitialCase(result, model);
    await this.checkInitialRequestsForGeneralErrors(result);
    return await this.enrichCase(result, model);
  }

  /**
   * Build a new absence (or return nothing if there's no need).
   *
   * This will only be used if the user is an absence user
   * @param model
   */
  protected abstract buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest | undefined;
  /**
   * Build a new claim (or return nothing if there's no need)
   *
   * This will only be used if the user is requesting at least one continuous time off
   * @param model
   */
  protected abstract buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): StartClaimRequestDetails | undefined;

  /**
   * Get a list of post case requests
   * @returns an array of functions that map the result/model to a promise
   */
  protected abstract getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[];

  protected isAbsenceUser(): boolean {
    return this.authorizationService.isAbsenceUser;
  }
  protected isClaimsUser(): boolean {
    return this.authorizationService.isClaimsUser;
  }

  protected isUser(): boolean {
    return this.isAbsenceUser() || this.isClaimsUser();
  }

  private async requestInitialCase(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<IntakeSubmissionResult> {
    await this.requestInitialAbsence(result, model);
    return await this.requestInitialClaim(result, model);
  }

  /**
   * Always resolves ok, even on an error - you need to check the result parameter
   */
  private async requestInitialAbsence(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<IntakeSubmissionResult> {
    if (this.isAbsenceUser()) {
      const request = this.buildStartAbsenceRequest(model);
      if (request) {
        result.isAbsenceRequired = true;
        try {
          const absence = await this.absenceService.registerAbsence(request);
          result.createdNotificationId = absence.notificationCaseId;
          result.createdAbsence = {
            id: absence.absenceId
          };
          return this.getIsMedicalRelated(result);
        } catch (error) {
          result.createdAbsence = {
            error: error
          };
          return result;
        }
      }
    }
    return Promise.resolve(result);
  }

  private async getIsMedicalRelated(
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    if (
      !!result.createdNotificationId &&
      !!result.createdAbsence &&
      !!result.createdAbsence.id
    ) {
      try {
        const absenceReasons: AbsenceReasonSummary[] = await this.absenceService.getAbsenceReasons(
          new AbsenceReasonSearch()
        );
        const absenceDetails = await this.absenceService.getAbsenceDetails(
          result.createdAbsence.id
        );

        let isMedicalProviderRelated = false;
        absenceDetails.absencePeriods.forEach(period => {
          const val = absenceReasons.find(
            ({ reason }) => period.reason === reason
          );
          if (val && val.medicalRelated === MEDICAL_PROVIDER_MATCH) {
            isMedicalProviderRelated = true;
          }
        });
        result.createdAbsence.isMedicalProviderRelated = isMedicalProviderRelated;
        return result;
      } catch (error) {
        result.createdAbsence.isMedicalProviderRelated = false;
        return result;
      }
    }
    return Promise.resolve(result);
  }

  /**
   * Always resolves ok, even on an error - you need to check the result parameter
   */
  private async requestInitialClaim(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<IntakeSubmissionResult> {
    // -  If role is customer, a claim case is always created
    // -  If role is Absence Customer or Absence Supervisor,
    //    a claim case is only created if the user has requested
    //    at least one continuous time off period.
    if (
      this.isClaimsUser() ||
      (this.isAbsenceUser() && model.isContinuousLeaveRequested)
    ) {
      const claimRequest = this.buildStartClaimRequest(result, model);

      if (claimRequest) {
        result.isClaimRequired = true;
        try {
          const claim = await this.claimService.startClaim(
            claimRequest.claimCaseType,
            claimRequest.request
          );
          result.createdClaim = {
            id: claim.claimId
          };
          // if we don't have one already from an absence, then set it here
          if (!result.createdNotificationId) {
            result.createdNotificationId = claim.notificationCaseId;
          }
          return Promise.resolve(result);
        } catch (error) {
          result.createdClaim = {
            error: error
          };
          return Promise.resolve(result);
        }
      }
    }

    return Promise.resolve(result);
  }

  /**
   * Sets a value for generalError based on the results of the initial requests.
   * @param result
   */
  private checkInitialRequestsForGeneralErrors(
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    const {
      createdAbsence,
      isAbsenceRequired,
      createdClaim,
      isClaimRequired
    } = result;

    const absenceErrors = isAbsenceRequired && createdAbsence.error;
    const claimErrors = isClaimRequired && createdClaim.error;

    if (absenceErrors && claimErrors) {
      result.generalError = 'Absence and Claim failed';
    } else if (absenceErrors) {
      result.generalError = 'Absence failed';
    } else if (claimErrors) {
      result.generalError = 'Claim failed';
    }
    return Promise.resolve(result);
  }

  private async enrichCase(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<IntakeSubmissionResult> {
    /**
     * Post Case actions now Sequential
     * This has been advised against
     */
    return await this.getPostCaseRequests().reduce(async (prev, next) => {
      await prev;
      return this.runCaseRequest(result, model, next);
    }, Promise.resolve(result));
  }

  private async runCaseRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission,
    request: (
      result: IntakeSubmissionResult,
      model: IntakeSubmission
    ) => Promise<any>
  ): Promise<IntakeSubmissionResult> {
    try {
      await request(result, model);
      return await Promise.resolve(result);
    } catch (error) {
      result.postCaseErrors.push(error);
      return Promise.resolve(result);
    }
  }
}
