import {
  StartClaimRequest,
  PregnancyService,
  NewAbsenceRequest,
  PrenatalDetailsRequest,
  PostnatalDetailsRequest,
  UpdatePregnancyDetailsRequest,
  AbsenceCaseNotifiedBy,
  IntakeSource
} from 'fineos-js-api-client';

import { CLAIM_CASE_TYPE } from '../constants';
import { IntakeSubmission, IntakeSubmissionResult } from '../services';
import { EventOptionId, RequestDetailFieldId } from '../types';

import { AbstractIntake, StartClaimRequestDetails } from './abstract-intake';

export class PregnancyIntake extends AbstractIntake {
  static _instance: PregnancyIntake;
  pregnancyService = PregnancyService.getInstance();

  static getInstance(): PregnancyIntake {
    return (
      PregnancyIntake._instance ||
      (PregnancyIntake._instance = new PregnancyIntake())
    );
  }

  supports(model: IntakeSubmission): boolean {
    return model.isEventOptionSelected(EventOptionId.PREGNANCY);
  }

  protected buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest {
    const newAbsenceRequest = new NewAbsenceRequest();
    newAbsenceRequest.additionalComments = model.getRequestDetailFieldValue(
      RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS
    );
    newAbsenceRequest.notifiedBy = AbsenceCaseNotifiedBy.Employee;
    newAbsenceRequest.intakeSource = IntakeSource.SelfService;
    newAbsenceRequest.reason = 'Pregnancy/Maternity';
    newAbsenceRequest.notificationReason =
      'Pregnancy, birth or related medical treatment';
    this.setQualifiers(newAbsenceRequest, model);

    // create these
    newAbsenceRequest.timeOffRequests = this.buildTimeOffLeavePeriod(
      model.getTimeOff().timeOffLeavePeriods
    );
    newAbsenceRequest.episodicRequests = this.buildEpisodicLeavePeriod(
      model.getTimeOff().episodicLeavePeriods
    );
    newAbsenceRequest.reducedScheduleRequests = this.buildReducedSchedule(
      model.getTimeOff().reducedScheduleLeavePeriods
    );

    return newAbsenceRequest;
  }

  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): StartClaimRequestDetails | undefined {
    // only return a request object is needed
    if (this.shouldCreateClaim(model)) {
      const claimRequest = new StartClaimRequest();
      // temporary until the API can be updated to make this optional;
      claimRequest.notificationCaseId = result.createdNotificationId;
      claimRequest.notificationReason =
        'Pregnancy, birth or related medical treatment';

      const earliestTimeOffLeavePeriod = this.findEarliestTimeOffLeavePeriod(
        model
      );

      if (earliestTimeOffLeavePeriod) {
        claimRequest.claimIncurredDate = earliestTimeOffLeavePeriod.startDate;
      }

      return {
        claimCaseType: CLAIM_CASE_TYPE,
        request: claimRequest
      };
    }
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[] {
    return [
      (r, m) => this.submitPrenatalCareRequest(r, m),
      (r, m) => this.submitPostnatalCareRequest(r, m),
      (r, m) => this.submitAdditionalConditionsRequest(r, m),
      (r, m) => this.submitMedicalDetailsRequest(r, m),
      (r, m) => this.submitUpdateDisabilityRequest(r, m),
      (r, m) => this.submitHospitalisationRequestForAbsence(r, m),
      (r, m) => this.submitHospitalisationRequestForClaim(r, m),
      (r, m) => this.submitUpdatePregnancyDetails(r, m),
      (r, m) => this.submitNotificationForMultipleTimeOffs(r, m)
    ];
  }

  private shouldCreateClaim(model: IntakeSubmission): boolean {
    return !model.isEventOptionSelected(EventOptionId.PREGNANCY_PRENATAL_CARE);
  }

  private setQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    switch (model.selectedEventOption) {
      case EventOptionId.PREGNANCY_BIRTH_DISABILITY:
      case EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY:
        newAbsenceRequest.reasonQualifier1 = 'Birth Disability';
        break;
      case EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY:
        newAbsenceRequest.reasonQualifier1 = 'Postnatal Disability';
        break;
      case EventOptionId.PREGNANCY_PRENATAL_CARE:
        newAbsenceRequest.reasonQualifier1 = 'Prenatal Care';
        break;
      case EventOptionId.PREGNANCY_PRENATAL_DISABILITY:
        newAbsenceRequest.reasonQualifier1 = 'Prenatal Disability';
        break;
      case EventOptionId.PREGNANCY_TERMINATION:
        newAbsenceRequest.reasonQualifier1 = 'Other';
        newAbsenceRequest.reasonQualifier2 = 'Miscarriage';
        break;
      default:
        throw new Error('Unsupported option: ' + model.selectedEventOption);
    }
  }

  private submitPrenatalCareRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (
      result.createdAbsence.id &&
      model.isEventOptionSelected(
        EventOptionId.PREGNANCY_PRENATAL_CARE,
        EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY,
        EventOptionId.PREGNANCY_PRENATAL_DISABILITY
      )
    ) {
      const prenatalDetailsRequest = new PrenatalDetailsRequest();

      prenatalDetailsRequest.caseId = result.createdAbsence.id;

      prenatalDetailsRequest.deliveryDate = model.getRequestDetailFieldValue(
        RequestDetailFieldId.EXPECTED_DELIVERY_DATE
      );

      if (
        model.isEventOptionSelected(EventOptionId.PREGNANCY_PRENATAL_DISABILITY)
      ) {
        model.ifRequestDetailFieldPresent(
          RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
          value => {
            prenatalDetailsRequest.deliveryType = this.mapDelivery(value);
          }
        );

        prenatalDetailsRequest.pregnancyComplications = model.isTrue(
          RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS
        );

        prenatalDetailsRequest.complicationDetails = model.getRequestDetailFieldValue(
          RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS
        );
      }

      return this.pregnancyService.addPrenatalDetails(prenatalDetailsRequest);
    }
    return Promise.resolve(result);
  }

  private submitPostnatalCareRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (
      result.createdAbsence.id &&
      model.isEventOptionSelected(
        EventOptionId.PREGNANCY_BIRTH_DISABILITY,
        EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY,
        EventOptionId.PREGNANCY_POSTNATAL
      )
    ) {
      const postnatalDetailsRequest = new PostnatalDetailsRequest();
      postnatalDetailsRequest.caseId = result.createdAbsence.id;

      postnatalDetailsRequest.deliveryDate = model.getRequestDetailFieldValue(
        RequestDetailFieldId.ACTUAL_DELIVERY_DATE
      );

      model.ifRequestDetailFieldPresent(
        RequestDetailFieldId.PREGNANCY_DELIVERY,
        value => {
          postnatalDetailsRequest.deliveryType = this.mapDelivery(value);
        }
      );
      postnatalDetailsRequest.pregnancyComplications = model.isTrue(
        RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS
      );
      postnatalDetailsRequest.complicationDetails = model.getRequestDetailFieldValue(
        RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS
      );
      postnatalDetailsRequest.numberOfNewborns = model.getRequestDetailFieldValue(
        RequestDetailFieldId.NEWBORN_COUNT
      );

      return this.pregnancyService.addPostnatalDetails(postnatalDetailsRequest);
    }
    return Promise.resolve(result);
  }

  private mapDelivery(fieldId: RequestDetailFieldId): string {
    switch (fieldId) {
      case RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL:
        return 'Vaginal';
      case RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION:
        return 'C-Section';
      case RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE:
        return 'Multiple Births';
      case RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN:
      default:
        return 'Unknown';
    }
  }

  private submitUpdatePregnancyDetails(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): Promise<any> {
    if (result.createdClaim.id) {
      const details = new UpdatePregnancyDetailsRequest();

      // set the expected/actual delivery date
      let isRequestRequired = model.ifRequestDetailFieldPresent(
        RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
        value => (details.actualDeliveryDate = value)
      );
      isRequestRequired =
        model.ifRequestDetailFieldPresent(
          RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          value => (details.expectedDeliveryDate = value)
        ) || isRequestRequired;

      // set the delivery type
      isRequestRequired =
        model.ifRequestDetailFirstFieldPresent(
          [
            RequestDetailFieldId.PREGNANCY_DELIVERY,
            RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY
          ],
          value => (details.deliveryType = this.mapDelivery(value))
        ) || isRequestRequired;

      // set if we have/or expect conplications
      isRequestRequired =
        model.ifRequestDetailFirstFieldPresent(
          [
            RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
            RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS
          ],
          value => (details.pregnancyComplications = model.isTrueValue(value))
        ) || isRequestRequired;

      // if we have anything to say, then say it
      if (isRequestRequired) {
        return this.claimService.updatePregnancyDetails(
          result.createdClaim.id,
          details
        );
      }
    }
    return Promise.resolve(result);
  }
}
