import {
  NewAbsenceRequest,
  AbsenceCaseNotifiedBy,
  IntakeSource
} from 'fineos-js-api-client';

import { EventOptionId } from '../types';
import { IntakeSubmission, IntakeSubmissionResult } from '../services';

import { AbstractIntake, StartClaimRequestDetails } from './abstract-intake';

export class OtherIntake extends AbstractIntake {
  static _instance: OtherIntake;

  static getInstance(): OtherIntake {
    return OtherIntake._instance || (OtherIntake._instance = new OtherIntake());
  }

  supports(model: IntakeSubmission): boolean {
    return model.isEventOptionSelected(EventOptionId.OTHER);
  }

  protected buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest {
    const newAbsenceRequest = new NewAbsenceRequest();

    newAbsenceRequest.intakeSource = IntakeSource.SelfService;
    newAbsenceRequest.notifiedBy = AbsenceCaseNotifiedBy.Employee;
    newAbsenceRequest.notificationReason = 'Out of work for another reason';
    newAbsenceRequest.notificationCaseId = '';

    this.setReason(newAbsenceRequest, model);
    this.setQualifiers(newAbsenceRequest, model);

    // add leave periods
    newAbsenceRequest.timeOffRequests = this.buildTimeOffLeavePeriod(
      model.getTimeOff().timeOffLeavePeriods
    );
    newAbsenceRequest.episodicRequests = this.buildEpisodicLeavePeriod(
      model.getTimeOff().episodicLeavePeriods
    );
    newAbsenceRequest.reducedScheduleRequests = this.buildReducedSchedule(
      model.getTimeOff().reducedScheduleLeavePeriods
    );

    return newAbsenceRequest;
  }

  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ): StartClaimRequestDetails | undefined {
    return undefined;
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[] {
    return [(r, m) => this.submitHospitalisationRequestForAbsence(r, m)];
  }

  private setReason(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    switch (model.selectedEventOption) {
      case EventOptionId.OTHER_EMPLOYEE_JURY_DUTY:
      case EventOptionId.OTHER_EMPLOYEE_VOTING:
      case EventOptionId.OTHER_EMPLOYEE_WITNESS:
        newAbsenceRequest.reason = 'Civic Duty';
        break;
      case EventOptionId.OTHER_EMPLOYEE_EDUCATION:
        newAbsenceRequest.reason = 'Educational Activity - Employee';
        break;
      case EventOptionId.OTHER_EMPLOYEE_MILITARY:
        newAbsenceRequest.reason = 'Military - Employee';
        break;
      case EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES:
      case EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO:
        newAbsenceRequest.reason = 'Personal - Employee';
        break;
      case EventOptionId.OTHER_EMPLOYEE_UNION:
        newAbsenceRequest.reason = 'Union Business';
        break;
      case EventOptionId.OTHER_EMPLOYEE_EMERGENCY:
        newAbsenceRequest.reason = 'Public Health Emergency - Employee';
        break;

      case EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_DONATION:
        newAbsenceRequest.reason = 'Medical Donation - Family';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE:
        newAbsenceRequest.reason = 'Preventative Care - Family Member';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_PASSED_AWAY:
        newAbsenceRequest.reason = 'Bereavement';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER:
        newAbsenceRequest.reason = 'Military Caregiver';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_COUNSELING:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_LEGAL:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_EVENT:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE:
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL:
        newAbsenceRequest.reason = 'Military Exigency Family';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_EDUCATION:
        newAbsenceRequest.reason = 'Educational Activity - Family';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_EMERGENCY:
        newAbsenceRequest.reason = 'Public Health Emergency - Family';
        break;

      default:
        throw new Error('Unsupported option: ' + model.selectedEventOption);
    }
  }

  private setQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    switch (model.selectedEventOption) {
      case EventOptionId.OTHER_EMPLOYEE_JURY_DUTY:
        newAbsenceRequest.reasonQualifier1 = 'Jury';
        break;
      case EventOptionId.OTHER_EMPLOYEE_VOTING:
        newAbsenceRequest.reasonQualifier1 = 'Voting';
        break;
      case EventOptionId.OTHER_EMPLOYEE_WITNESS:
        newAbsenceRequest.reasonQualifier1 = 'Witness';
        break;
      case EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES:
        newAbsenceRequest.reasonQualifier1 = 'Right to Leave';
        newAbsenceRequest.reasonQualifier2 = 'Medical Related';
        break;
      case EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO:
        newAbsenceRequest.reasonQualifier1 = 'Right to Leave';
        newAbsenceRequest.reasonQualifier2 = 'Non Medical';
        break;

      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE:
        newAbsenceRequest.reasonQualifier1 = 'Childcare and School Activities';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_COUNSELING:
        newAbsenceRequest.reasonQualifier1 = 'Counseling';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_LEGAL:
        newAbsenceRequest.reasonQualifier1 = 'Financial & Legal Arrangements';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_EVENT:
        newAbsenceRequest.reasonQualifier1 =
          'Military Events & Related Activities';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT:
        newAbsenceRequest.reasonQualifier1 = 'Parental Care';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT:
        newAbsenceRequest.reasonQualifier1 =
          'Post Deployment Activities - Including Bereavement';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE:
        newAbsenceRequest.reasonQualifier1 = 'Rest & Recuperation';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE:
        newAbsenceRequest.reasonQualifier1 = 'Short Notice Deployment';
        break;
      case EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL:
        newAbsenceRequest.reasonQualifier1 = 'Other Additional Activities';
        break;
    }
  }
}
