import {
  NewAbsenceRequest,
  AbsenceCaseNotifiedBy,
  IntakeSource
} from 'fineos-js-api-client';

import { EventOptionId } from '../types';
import { IntakeSubmission, IntakeSubmissionResult } from '../services';

import { AbstractIntake } from './abstract-intake';

export class ChildBondingIntake extends AbstractIntake {
  static _instance: ChildBondingIntake;

  static getInstance(): ChildBondingIntake {
    return (
      ChildBondingIntake._instance ||
      (ChildBondingIntake._instance = new ChildBondingIntake())
    );
  }

  supports(model: IntakeSubmission): boolean {
    return model.isEventOptionSelected(EventOptionId.CHILD_BONDING);
  }

  protected buildStartAbsenceRequest(
    model: IntakeSubmission
  ): NewAbsenceRequest {
    const newAbsenceRequest = new NewAbsenceRequest();
    newAbsenceRequest.intakeSource = IntakeSource.SelfService;
    newAbsenceRequest.notifiedBy = AbsenceCaseNotifiedBy.Employee;
    newAbsenceRequest.reason = 'Child Bonding';
    newAbsenceRequest.notificationCaseId = '';
    newAbsenceRequest.notificationReason =
      'Bonding with a new child (adoption/ foster care/ newborn)';
    this.setChildBondingReasonQualifiers(newAbsenceRequest, model);

    newAbsenceRequest.timeOffRequests = this.buildTimeOffLeavePeriod(
      model.getTimeOff().timeOffLeavePeriods
    );
    newAbsenceRequest.episodicRequests = this.buildEpisodicLeavePeriod(
      model.getTimeOff().episodicLeavePeriods
    );
    newAbsenceRequest.reducedScheduleRequests = this.buildReducedSchedule(
      model.getTimeOff().reducedScheduleLeavePeriods
    );

    return newAbsenceRequest;
  }

  protected buildStartClaimRequest(
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) {
    return undefined;
  }

  protected getPostCaseRequests(): ((
    result: IntakeSubmissionResult,
    model: IntakeSubmission
  ) => Promise<any>)[] {
    return [];
  }

  private setChildBondingReasonQualifiers(
    newAbsenceRequest: NewAbsenceRequest,
    model: IntakeSubmission
  ) {
    switch (model.selectedEventOption) {
      case EventOptionId.CHILD_BONDING_ADOPTED:
        newAbsenceRequest.reasonQualifier1 = 'Adoption';
        break;
      case EventOptionId.CHILD_BONDING_FOSTER:
        newAbsenceRequest.reasonQualifier1 = 'Foster Care';
        break;
      case EventOptionId.CHILD_BONDING_NEWBORN:
        newAbsenceRequest.reasonQualifier1 = 'Newborn';
        break;
    }
  }
}
