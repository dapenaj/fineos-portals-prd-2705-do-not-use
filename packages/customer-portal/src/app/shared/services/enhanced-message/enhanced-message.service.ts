import {
  MessageSearch,
  MessageService,
  NotificationCaseSummary
} from 'fineos-js-api-client';
import { flatten as _flatten, isEmpty as _isEmpty } from 'lodash';
import moment from 'moment';

import { EnhancedMessage } from '../../types/message.type';
import { NotificationService } from '../notification';

export class EnhancedMessageService {
  private static _instance: EnhancedMessageService;
  notificationService: NotificationService = new NotificationService();
  messageService: MessageService = new MessageService();

  static getInstance(): EnhancedMessageService {
    return (
      EnhancedMessageService._instance ||
      (EnhancedMessageService._instance = new EnhancedMessageService())
    );
  }

  sortMessages = (messages: EnhancedMessage[] | null): EnhancedMessage[] =>
    [...(!_isEmpty(messages) ? messages! : [])].sort((a, b) =>
      moment(b.contactTime).diff(moment(a.contactTime))
    );

  async fetchMessages(
    notifications: NotificationCaseSummary[]
  ): Promise<EnhancedMessage[]> {
    try {
      const unrelatedMessages = await this.fetchUnrelatedMessages();
      const relatedMessages = await this.fetchRelatedMessages(notifications);

      return this.sortMessages([...unrelatedMessages, ...relatedMessages]);
    } catch (error) {
      return Promise.resolve(error);
    }
  }

  async fetchMessagesByCaseId(
    caseIds: string[],
    notificationCaseId: string
  ): Promise<EnhancedMessage[]> {
    try {
      const caseMessages = await Promise.all(
        caseIds.map(async id => {
          const search = new MessageSearch();
          search.caseId(id);

          return await this.messageService.getMessages(search);
        })
      );

      const enhancedMessages: EnhancedMessage[] = _flatten(
        caseMessages
      ).map(message => ({ notificationCaseId, ...message }));

      return this.sortMessages(enhancedMessages);
    } catch (error) {
      return [];
    }
  }

  private async fetchUnrelatedMessages(): Promise<EnhancedMessage[]> {
    const messages = await this.messageService.getMessages();
    return (messages || []).filter(
      ({ caseId }) => !caseId
    ) as EnhancedMessage[];
  }

  private async fetchRelatedMessages(
    notifications: NotificationCaseSummary[]
  ): Promise<EnhancedMessage[]> {
    const messages: EnhancedMessage[][] = [];

    for (const notification of notifications) {
      let caseIds: string[] = [];

      try {
        caseIds = await this.notificationService.getCaseIdsFromNotification(
          notification
        );
      } catch (ids) {
        caseIds = ids;
      } finally {
        const caseIdMessages = await this.fetchMessagesByCaseId(
          caseIds,
          notification.notificationCaseId
        );

        messages.push(caseIdMessages);
      }
    }

    return _flatten(messages);
  }
}
