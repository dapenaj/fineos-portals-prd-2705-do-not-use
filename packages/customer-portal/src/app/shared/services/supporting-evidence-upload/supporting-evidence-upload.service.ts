import { DocumentService, DocumentUploadRequest } from 'fineos-js-api-client';

export class SupportingEvidenceUploadService {
  private static _instance: SupportingEvidenceUploadService;
  private documentService: DocumentService = DocumentService.getInstance();

  static getInstance(): SupportingEvidenceUploadService {
    return (
      SupportingEvidenceUploadService._instance ||
      (SupportingEvidenceUploadService._instance = new SupportingEvidenceUploadService())
    );
  }

  async uploadCaseDocument(caseId: string, request: DocumentUploadRequest) {
    try {
      // Upload the Case Document
      const { documentId } = await this.documentService.uploadCaseDocument(
        caseId,
        request
      );

      // Match the Supporting Evidence with the document id
      await this.documentService.uploadSupportingEvidence({
        caseId,
        documentId
      });

      // Mark the Document as read
      return await this.documentService.markDocumentAsRead(caseId, documentId);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
