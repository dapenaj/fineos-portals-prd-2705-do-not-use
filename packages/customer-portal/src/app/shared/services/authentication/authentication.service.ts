import {
  AuthenticationStrategy,
  SdkConfigProps,
  ServiceFactory,
  InsecureStrategy,
  CognitoStrategy
} from 'fineos-js-api-client';
import { isNil as _isNil } from 'lodash';
import moment from 'moment';

import { parseUrl } from '../../utils';
import { AuthenticationURLParam, URLParseChar } from '../../types';

export class AuthenticationService {
  get authStrategy(): AuthenticationStrategy {
    return SdkConfigProps.authenticationStrategy;
  }

  get hasRequiredCredentials(): boolean {
    return (
      !_isNil(this.accessKeyId) &&
      !_isNil(this.expiration) &&
      !_isNil(this.secretKey) &&
      !_isNil(this.sessionToken) &&
      !_isNil(this.userId) &&
      !_isNil(this.role)
    );
  }

  get areCredentialsValid(): boolean {
    return moment(this.expiration).isAfter(moment());
  }

  get isAuthenticated(): boolean {
    return this.authStrategy === AuthenticationStrategy.NONE
      ? true
      : this.hasRequiredCredentials && this.areCredentialsValid;
  }

  private static _instance: AuthenticationService;

  displayUserId!: string;
  displayRoles!: string[];
  accessKeyId!: string;
  expiration!: string;
  secretKey!: string;
  sessionToken!: string;
  userId!: string;
  role!: string;
  serviceFactory: ServiceFactory = ServiceFactory.getInstance();

  static getInstance(): AuthenticationService {
    return (
      AuthenticationService._instance ||
      (AuthenticationService._instance = new AuthenticationService())
    );
  }

  saveCredentials(url: string) {
    this.authStrategy === AuthenticationStrategy.NONE
      ? this.saveInsecureCredentials()
      : this.saveCognitoCredentials(url);
  }

  saveInsecureCredentials() {
    this.serviceFactory.strategy = new InsecureStrategy(
      SdkConfigProps.debugUserId
    );

    this.displayUserId = SdkConfigProps.debugDisplayUserId;
    this.displayRoles = SdkConfigProps.debugDisplayRoles;
  }

  saveCognitoCredentials(url: string) {
    const fragments = parseUrl(url, URLParseChar.FRAGMENT);

    this.accessKeyId = fragments[
      AuthenticationURLParam.ACCESS_KEY_ID
    ] as string;
    this.expiration = fragments[AuthenticationURLParam.EXPIRATION] as string;
    this.secretKey = fragments[AuthenticationURLParam.SECRET_KEY] as string;
    this.sessionToken = fragments[
      AuthenticationURLParam.SESSION_TOKEN
    ] as string;
    this.userId = fragments[AuthenticationURLParam.USER_ID] as string;
    this.role = fragments[AuthenticationURLParam.ROLE] as string;
    this.displayUserId = fragments[
      AuthenticationURLParam.DISPLAY_USER_ID
    ] as string;
    this.displayRoles = fragments[
      AuthenticationURLParam.DISPLAY_ROLE
    ] as string[];

    this.serviceFactory.strategy = new CognitoStrategy(
      this.accessKeyId,
      this.expiration,
      this.secretKey,
      this.sessionToken,
      this.userId,
      this.role
    );
  }

  logout() {
    document.location.href = SdkConfigProps.loginUrl;
  }
}
