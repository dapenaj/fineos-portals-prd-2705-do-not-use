import { DocumentService } from 'fineos-js-api-client';
import { stripIndents } from 'common-tags';

import { Audit } from '../../../types';

import { AuditBuilder } from './audit.builder';

export class AuditService {
  private static _instance: AuditService;
  documentService: DocumentService = DocumentService.getInstance();

  static getInstance(): AuditService {
    return (
      AuditService._instance || (AuditService._instance = new AuditService())
    );
  }

  /**
   * Start the audit - this creates a new audit builder
   */
  startAudit(): AuditBuilder {
    return new AuditBuilder();
  }

  /**
   * Given an audit, produce a html document.
   * @param audit the audit to render
   */
  buildAuditLog(audit: Audit): string {
    // render it as text; we strip intents to make it easier to test
    const table = this.buildTable(audit);
    return stripIndents`${table}`;
  }

  /**
   * Build a html table model for the rendering. This is just to make it look a little nice for the
   * presentation. We can't rely on CSS for this because it'll be rendered in the case management system.
   *
   * @param audit The audit
   */
  private buildTable(audit: Audit): string {
    const lines = new Array<string>();

    audit.steps.forEach(step => {
      step.values.forEach(section => {
        lines.push('-------------------------------------');
        lines.push(`${step.label} : ${section.label}`);
        lines.push('-------------------------------------');

        section.values.forEach(v =>
          lines.push(`- ${v.label}${v.values ? ': ' + v.values : ''}`)
        );
      });
    });

    return lines.join('\n');
  }
}
