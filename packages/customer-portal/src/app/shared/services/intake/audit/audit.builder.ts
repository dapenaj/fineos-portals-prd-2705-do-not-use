import { Audit, AuditDetails } from '../../../types';

// this is a factory, rather than a service
// it must be created anew every time it's used
export class AuditBuilder {
  private audit: Audit;

  constructor() {
    this.audit = {
      steps: []
    };
  }

  /**
   * Get the audt that has been built
   */
  getAudit(): Audit {
    return this.audit;
  }

  /**
   * Add a message to the audit. This will cause any message to be grouped into the given section and step.
   * The section/step will be in the order in which the messages were added.
   *
   * A section can have as many steps, and a step has many messages as you like. There's no need
   * to submit all the messages for a section at once; you can add them later on and they will still be
   * grouped together properly.
   *
   * Different sections can contain steps with the same name.
   *
   * @param sectionName  The name of the section (e.g. Your Request)
   * @param stepName  The name of the step (e.g. Personal Details or Work Details)
   * @param message The message for the audit.
   * @param value  The value for the audit message
   */
  addMessage(
    sectionName: string,
    stepName: string,
    message: string,
    value?: any
  ) {
    const section = this.findOrAdd([sectionName, stepName]);
    if (section) {
      const detail: AuditDetails<any> = {
        label: message,
        values: value
      };
      section.values.push(detail);
    } else {
      // this should never be thrown, if it is then findOrAdd wasn't coded properly
      throw Error(
        `Invariant error - couldn't add path to model: ${sectionName}, ${stepName}`
      );
    }
  }

  /**
   * Given a path (i.e. section -> step) find it. If the elements are not there, then create them
   * @param path The path
   */
  private findOrAdd(path: string[]) {
    let currentDetails: AuditDetails<any> | undefined;

    path.forEach(p => {
      const searchPath: AuditDetails<any>[] = currentDetails
        ? currentDetails.values
        : this.audit.steps;

      const existing = searchPath.find(r => r.label === p);

      if (existing) {
        currentDetails = existing;
      } else {
        const section: AuditDetails<any> = {
          label: p,
          values: []
        };
        searchPath.push(section);
        currentDetails = section;
      }
    });

    return currentDetails;
  }
}
