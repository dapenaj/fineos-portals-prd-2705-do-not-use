import { EventOptionId } from '../../../../types';
import {
  PersonalDetailsFormValue,
  WorkDetailsFormValue,
  AccommodationValue
} from '../../../../../modules/intake';
import { AbstractIntakeSubmission } from '../abstract-intake-submission';

export class IntakeAccommodationSubmission extends AbstractIntakeSubmission {
  private accommodation: AccommodationValue;

  constructor(
    eventOptions: EventOptionId[],
    accommodation: AccommodationValue,
    workDetails?: WorkDetailsFormValue,
    personalDetails?: PersonalDetailsFormValue
  ) {
    super(eventOptions, workDetails, personalDetails);
    this.accommodation = accommodation;
  }

  getAccommodation(): AccommodationValue {
    return this.accommodation;
  }
}
