import { IntakeSubmission } from './intake-submission';
import { IntakeAccommodationSubmission } from './intake-accommodation-submission';

export type IntakeSubmissionSubCaseResult = {
  id?: string;
  error?: any;
  isMedicalProviderRelated?: boolean;
};

export type IntakeSubmissionResult = {
  createdNotificationId?: string;
  generalError?: string;
  postCaseErrors: any[];
  createdAbsence: IntakeSubmissionSubCaseResult;
  createdClaim: IntakeSubmissionSubCaseResult;
  createdAccommodation?: IntakeSubmissionSubCaseResult;
  isClaimRequired: boolean;
  isAbsenceRequired: boolean;
};

export type IntakeSubmissionState = {
  model: IntakeSubmission | IntakeAccommodationSubmission;
  result: IntakeSubmissionResult;
};
