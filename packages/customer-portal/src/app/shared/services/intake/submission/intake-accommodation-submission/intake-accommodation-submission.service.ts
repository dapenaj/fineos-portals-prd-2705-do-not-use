import { AbstractIntakeSubmissionService } from '../abstract-intake-submission.service';
import { AuthorizationService } from '../../../authorization';
import { AccommodationSnapshotService } from '../../snapshot';
import { NotificationService } from '../../../notification';
import { IntakeSubmissionResult } from '../intake-submission.type';
import { getCompletedNotificationId } from '../intake-submission.utils';
import { AccommodationIntake } from '../../../../intakes';

import { IntakeAccommodationSubmission } from './intake-accommodation-submission';

/**
 * A service for handling intake accommodation submissions.
 *
 */
export class IntakeAccommodationSubmissionService extends AbstractIntakeSubmissionService {
  static _instance: IntakeAccommodationSubmissionService;

  private availableIntakes: AccommodationIntake[] = [
    AccommodationIntake.getInstance()
  ];

  private authorizationService: AuthorizationService = AuthorizationService.getInstance();
  private snapshotService = AccommodationSnapshotService.getInstance();
  private notificationService = NotificationService.getInstance();

  static getInstance(): IntakeAccommodationSubmissionService {
    return (
      IntakeAccommodationSubmissionService._instance ||
      (IntakeAccommodationSubmissionService._instance = new IntakeAccommodationSubmissionService())
    );
  }

  /**
   * The #processMethod should only ever reject if the user cannot act, or hasn't provided enough
   * information  to start a case. If _any_ case can be creatd the promise _will_ resolve, but there
   * may still be errors - the client needs to check the IntakeSubmissionResult for any errors.
   *
   * @param submission the parameters
   */
  async process(
    submission: IntakeAccommodationSubmission
  ): Promise<IntakeSubmissionResult> {
    const intake = this.selectIntake(submission);

    if (intake) {
      const result = await intake.process(submission);
      return await this.performCommonTasks(submission, result);
    } else {
      const result: IntakeSubmissionResult = {
        createdAbsence: {},
        createdClaim: {},
        postCaseErrors: [],
        generalError: 'No method is implemented to handle this request',
        isAbsenceRequired: false,
        isClaimRequired: false
      };

      return Promise.reject(result);
    }
  }

  async completeIntake(
    submission: IntakeAccommodationSubmission,
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    await this.sendSnapshot(submission, result);
    return await this.doCompletion(result);
  }

  private async sendSnapshot(
    submission: IntakeAccommodationSubmission,
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    if (result.createdNotificationId) {
      try {
        // we always submit a snapshot
        await this.snapshotService.submitSnapshot(submission, result);
        return result;
      } catch (error) {
        return await this.resolveError(result, error);
      }
    }

    return Promise.resolve(result);
  }

  private async doCompletion(
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    const completedNotificationId = getCompletedNotificationId(result);
    if (completedNotificationId) {
      try {
        await this.notificationService.completeNotification(
          completedNotificationId
        );
        return result;
      } catch (error) {
        return await this.resolveError(result, error);
      }
    }

    return Promise.resolve(result);
  }

  private selectIntake(
    submission: IntakeAccommodationSubmission
  ): AccommodationIntake | undefined {
    return this.availableIntakes.find(intakeType =>
      intakeType.supports(submission)
    );
  }
}
