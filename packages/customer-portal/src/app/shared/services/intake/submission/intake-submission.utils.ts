import { IntakeSubmissionResult } from './intake-submission.type';
/**
 * Get the id of the results notification if it was completed, or else return nothing
 * @param result the result to check
 */
export const getCompletedNotificationId = (
  result: IntakeSubmissionResult
): string | undefined => {
  if (
    // there can be no errros
    !result.generalError &&
    (!result.postCaseErrors || result.postCaseErrors.length === 0) &&
    // we must have a notification and at least one of an absence or claim or accommodation
    result.createdNotificationId &&
    (result.createdAbsence.id ||
      result.createdClaim.id ||
      (result.createdAccommodation && result.createdAccommodation.id))
  ) {
    return result.createdNotificationId;
  }
};
