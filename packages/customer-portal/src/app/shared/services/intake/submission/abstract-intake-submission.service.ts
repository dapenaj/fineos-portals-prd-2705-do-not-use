import { CustomerOccupationService } from 'fineos-js-api-client';

import { WorkDetailsFormValue } from '../../../../modules/intake';
import { formatDateForApi } from '../../../utils';

import { IntakeSubmissionResult } from './intake-submission.type';
import { AbstractIntakeSubmission } from './abstract-intake-submission';

export abstract class AbstractIntakeSubmissionService {
  private customerOccupationService = CustomerOccupationService.getInstance();

  protected async performCommonTasks(
    model: AbstractIntakeSubmission,
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    if (!!result.createdAbsence && !!result.createdAbsence.id) {
      await this.submitWorkDetails(
        result,
        result.createdAbsence.id,
        model.getWorkDetails()
      );
    }

    if (!!result.createdClaim && !!result.createdClaim.id) {
      await this.submitWorkDetails(
        result,
        result.createdClaim.id,
        model.getWorkDetails()
      );
    }

    if (!!result.createdAccommodation && !!result.createdAccommodation.id) {
      await this.submitWorkDetails(
        result,
        result.createdAccommodation.id,
        model.getWorkDetails()
      );
    }

    return result;
  }

  protected async submitWorkDetails(
    result: IntakeSubmissionResult,
    caseId?: string,
    workDetails?: WorkDetailsFormValue
  ): Promise<IntakeSubmissionResult> {
    if (caseId && workDetails) {
      try {
        await this.customerOccupationService.updateWorkDetails({
          caseId: caseId,
          employer: workDetails.employer,
          jobTitle: workDetails.jobTitle,
          dateJobBegan: formatDateForApi(workDetails.dateJobBegan)
        });
        return await Promise.resolve(result);
      } catch (error) {
        return await this.resolveError(result, error);
      }
    }

    return Promise.resolve(result);
  }

  protected resolveError(result: IntakeSubmissionResult, error: any) {
    result.postCaseErrors.push(error);
    return Promise.resolve(result);
  }
}
