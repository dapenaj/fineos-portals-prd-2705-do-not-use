import { EventOptionId } from '../../../types';
import {
  WorkDetailsFormValue,
  PersonalDetailsFormValue
} from '../../../../modules/intake';

export abstract class AbstractIntakeSubmission {
  private eventOptions: EventOptionId[];
  private workDetails?: WorkDetailsFormValue;
  private personalDetails?: PersonalDetailsFormValue;

  constructor(
    eventOptions: EventOptionId[],
    workDetails?: WorkDetailsFormValue,
    personalDetails?: PersonalDetailsFormValue
  ) {
    this.eventOptions = eventOptions;
    this.workDetails = workDetails;
    this.personalDetails = personalDetails;
  }

  getEventOptions(): EventOptionId[] {
    return [...this.eventOptions];
  }
  getWorkDetails(): WorkDetailsFormValue | undefined {
    return this.workDetails;
  }
  getPersonalDetails(): PersonalDetailsFormValue | undefined {
    return this.personalDetails;
  }

  get selectedEventOption(): EventOptionId {
    return this.eventOptions[this.eventOptions.length - 1];
  }

  /**
   * checks if any of the candidate event options are selected
   * @param candidates  candidate event options
   */
  isEventOptionSelected(...candidates: EventOptionId[]): boolean {
    const selected = candidates.find(candidate =>
      this.eventOptions.includes(candidate)
    );
    if (selected) {
      return true;
    }
    return false;
  }
}
