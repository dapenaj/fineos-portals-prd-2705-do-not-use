import {
  EventOptionId,
  RequestDetailFieldId,
  TimeOff
} from '../../../../types';
import {
  PersonalDetailsFormValue,
  WorkDetailsFormValue
} from '../../../../../modules/intake';
import { AbstractIntakeSubmission } from '../abstract-intake-submission';

export class IntakeSubmission extends AbstractIntakeSubmission {
  private requestDetailFieldValues: Map<RequestDetailFieldId, any>;
  private selectedTimeOff: TimeOff;

  constructor(
    eventOptions: EventOptionId[],
    requestDetailFieldValues: Map<RequestDetailFieldId, any>,
    selectedTimeOff: TimeOff,
    workDetails?: WorkDetailsFormValue,
    personalDetails?: PersonalDetailsFormValue
  ) {
    super(eventOptions, workDetails, personalDetails);
    this.requestDetailFieldValues = requestDetailFieldValues;
    this.selectedTimeOff = selectedTimeOff;
  }

  getTimeOff(): TimeOff {
    return this.selectedTimeOff;
  }

  get isContinuousLeaveRequested(): boolean {
    const timeoff = this.getTimeOff();

    if (timeoff.timeOffLeavePeriods) {
      return timeoff.timeOffLeavePeriods.length > 0;
    }
    return false;
  }

  /**
   * A value is true if it's defined and is boolean:true, or a case insensitive match to YES
   */
  isTrue(fieldId: RequestDetailFieldId): boolean {
    return this.isTrueValue(this.getRequestDetailFieldValue(fieldId));
  }

  isTrueValue(candidate: any): boolean {
    return (
      candidate &&
      (candidate === true ||
        RequestDetailFieldId.YES === ('' + candidate).toUpperCase())
    );
  }

  getRequestDetailFieldValue(fieldId: RequestDetailFieldId): any {
    return this.requestDetailFieldValues.get(fieldId);
  }

  ifRequestDetailFieldPresent(
    fieldId: RequestDetailFieldId,
    getValue: (value: any) => any
  ): boolean {
    if (this.hasRequestDetailFieldValue(fieldId)) {
      getValue(this.getRequestDetailFieldValue(fieldId));
      return true;
    }
    return false;
  }

  ifRequestDetailFirstFieldPresent(
    fieldIds: RequestDetailFieldId[],
    getValue: (value: any) => any
  ): boolean {
    const found = fieldIds.find(fieldId =>
      this.ifRequestDetailFieldPresent(fieldId, getValue)
    );
    if (found) {
      return true;
    }
    return false;
  }

  hasRequestDetailFieldValue(fieldId: RequestDetailFieldId): boolean {
    const selected = this.getRequestDetailFieldValue(fieldId);
    return typeof selected !== 'undefined';
  }
}
