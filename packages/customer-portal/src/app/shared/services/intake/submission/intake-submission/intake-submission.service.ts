import {
  AddIncomeSource,
  ClaimService,
  PaymentPreference,
  CustomerService
} from 'fineos-js-api-client';
import { omit as _omit } from 'lodash';

import {
  WrapUpSubmission,
  IncomeSource,
  MedicalProvider
} from '../../../../../modules/intake';
import { NotificationService } from '../../../notification';
import { parseDate } from '../../../../utils';
import { SnapshotService } from '../../snapshot';
import {
  AbstractIntake,
  AccidentIntake,
  ChildBondingIntake,
  PregnancyIntake,
  SicknessIntake,
  CareForAFamilyMemberIntake,
  OtherIntake
} from '../../../../intakes';
import { IntakeSubmissionResult } from '../intake-submission.type';
import { getCompletedNotificationId } from '../intake-submission.utils';
import { AbstractIntakeSubmissionService } from '../abstract-intake-submission.service';

import { IntakeSubmission } from './intake-submission';

/**
 * A service for handling all intake submissions.
 *
 */
export class IntakeSubmissionService extends AbstractIntakeSubmissionService {
  static _instance: IntakeSubmissionService;

  private availableIntakes: AbstractIntake[] = [
    PregnancyIntake.getInstance(),
    SicknessIntake.getInstance(),
    ChildBondingIntake.getInstance(),
    CareForAFamilyMemberIntake.getInstance(),
    AccidentIntake.getInstance(),
    OtherIntake.getInstance()
  ];

  private claimService = ClaimService.getInstance();
  private customerService = CustomerService.getInstance();
  private snapshotService = SnapshotService.getInstance();
  private notificationService = NotificationService.getInstance();

  static getInstance(): IntakeSubmissionService {
    return (
      IntakeSubmissionService._instance ||
      (IntakeSubmissionService._instance = new IntakeSubmissionService())
    );
  }
  /**
   * The #processMethod should only ever reject if the user cannot act, or hasn't provided enough
   * information  to start a case. If _any_ case can be creatd the promise _will_ resolve, but there
   * may still be errors - the client needs to check the IntakeSubmissionResult for any errors.
   *
   * @param submission the parameters
   */
  async process(submission: IntakeSubmission): Promise<IntakeSubmissionResult> {
    const intakeRequest = this.selectIntake(submission);
    if (intakeRequest) {
      const result = await intakeRequest.process(submission);
      return await this.performCommonTasks(submission, result);
    } else {
      const result: IntakeSubmissionResult = {
        createdAbsence: {},
        createdClaim: {},
        postCaseErrors: [],
        generalError: 'No method is implemented to handle this request',
        isAbsenceRequired: false,
        isClaimRequired: false
      };
      return Promise.reject(result);
    }
  }

  async addPrimaryCarePhysician(
    previousResult: IntakeSubmissionResult,
    primary: MedicalProvider | null
  ): Promise<IntakeSubmissionResult> {
    if (!!primary) {
      return this.claimService
        .addPrimaryCarePhysician(previousResult.createdClaim.id!, primary)
        .then(() => previousResult)
        .catch(error => this.resolveError(previousResult, error));
    }

    return Promise.resolve(previousResult);
  }

  async addAnotherMedicalProviders(
    previousResult: IntakeSubmissionResult,
    providers: MedicalProvider[]
  ): Promise<IntakeSubmissionResult> {
    return this.claimService
      .addAnotherMedicalProviders(previousResult.createdClaim.id!, ...providers)
      .then(() => previousResult)
      .catch(error => this.resolveError(previousResult, error));
  }

  // add income source for specific claim
  async addOtherIncomeSources(
    previousResult: IntakeSubmissionResult,
    incomeSources: IncomeSource[]
  ): Promise<IntakeSubmissionResult> {
    const addIncomeSources = incomeSources.map(incomeSource => {
      const addIncomeSource = new AddIncomeSource(incomeSource.incomeType);

      if (incomeSource.frequency) {
        addIncomeSource.frequency = incomeSource.frequency;
      }

      if (typeof incomeSource.amount === 'number') {
        addIncomeSource.amount = incomeSource.amount;
      }

      if (incomeSource.startDate) {
        addIncomeSource.startDate = parseDate(incomeSource.startDate);
      }

      if (incomeSource.endDate) {
        addIncomeSource.endDate = parseDate(incomeSource.endDate);
      }

      return addIncomeSource;
    });

    return this.claimService
      .addOtherIncomeSource(
        previousResult.createdClaim.id!,
        ...addIncomeSources
      )
      .then(() => previousResult)
      .catch(error => this.resolveError(previousResult, error));
  }

  async setPaymentPreference(
    previousResult: IntakeSubmissionResult,
    paymentPreference: PaymentPreference
  ): Promise<IntakeSubmissionResult> {
    const { paymentPreferenceId } = paymentPreference;
    return (!!paymentPreferenceId
      ? this.customerService.updatePaymentPreference(
          _omit(paymentPreference, 'paymentPreferenceId'),
          paymentPreferenceId
        )
      : this.customerService.addPaymentPreference(paymentPreference)
    )
      .then(() => previousResult)
      .catch(error => this.resolveError(previousResult, error));
  }

  /**
   * At the end of the intake, once every other form is submitted, complete the process
   *
   * This will submit a snapshot, and then complete the intake (but only if there are no errors)
   */
  async completeIntake(
    submission: IntakeSubmission,
    result: IntakeSubmissionResult,
    wrapUp: WrapUpSubmission
  ): Promise<IntakeSubmissionResult> {
    await this.sendSnapshot(submission, result, wrapUp);
    return await this.doCompletion(submission, result);
  }

  private async sendSnapshot(
    submission: IntakeSubmission,
    result: IntakeSubmissionResult,
    wrapUp: WrapUpSubmission
  ): Promise<IntakeSubmissionResult> {
    // if we created a notification then we have something to submit
    if (result.createdNotificationId) {
      try {
        // we always submit a snapshot
        await this.snapshotService.submitSnapshot(submission, result, wrapUp);
        return result;
      } catch (error) {
        return await this.resolveError(result, error);
      }
    }
    return Promise.resolve(result);
  }

  private async doCompletion(
    submission: IntakeSubmission,
    result: IntakeSubmissionResult
  ): Promise<IntakeSubmissionResult> {
    const completedNotificationId = getCompletedNotificationId(result);
    if (completedNotificationId) {
      try {
        await this.notificationService.completeNotification(
          completedNotificationId
        );
        return result;
      } catch (error) {
        return await this.resolveError(result, error);
      }
    }

    return Promise.resolve(result);
  }

  private selectIntake(
    submission: IntakeSubmission
  ): AbstractIntake | undefined {
    return this.availableIntakes.find(intakeType =>
      intakeType.supports(submission)
    );
  }
}
