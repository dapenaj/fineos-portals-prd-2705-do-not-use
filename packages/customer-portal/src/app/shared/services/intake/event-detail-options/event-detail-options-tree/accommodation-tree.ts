import {
  EventOptionEdge,
  EventOptionId,
  Role,
  IntakeSectionStepId
} from '../../../../types';

export const ACCOMMODATION_TREE: EventOptionEdge[] = [
  {
    from: EventOptionId.ORIGIN,
    to: EventOptionId.ACCOMMODATION,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR],
    skipSteps: [
      IntakeSectionStepId.TIME_OFF,
      IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  }
];
