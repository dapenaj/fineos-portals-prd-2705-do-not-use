import {
  EventOptionEdge,
  EventOptionId,
  IntakeSectionStepId,
  Role
} from '../../../../types';

/**
 * This event model defines the path to a given option in the graph
 */

export const SICKNESS_TREE: EventOptionEdge[] = [
  { from: EventOptionId.ORIGIN, to: EventOptionId.SICKNESS },
  {
    from: EventOptionId.SICKNESS,
    to: EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR]
  },
  {
    from: EventOptionId.SICKNESS,
    to: EventOptionId.SICKNESS_TIMEOFF_PREVENTIVE_CARE,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR],
    skipSteps: [IntakeSectionStepId.REQUEST_DETAILS]
  },
  {
    from: EventOptionId.SICKNESS,
    to: EventOptionId.SICKNESS_TIMEOFF_DONATION,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR]
  },
  {
    from: EventOptionId.SICKNESS_TIMEOFF_DONATION,
    to: EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD,
    skipSteps: [IntakeSectionStepId.REQUEST_DETAILS]
  },
  {
    from: EventOptionId.SICKNESS_TIMEOFF_DONATION,
    to: EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL
  },
  {
    from: EventOptionId.SICKNESS_TIMEOFF_DONATION,
    to: EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW
  },
  {
    from: EventOptionId.SICKNESS_TIMEOFF_DONATION,
    to: EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
  }
];
