import {
  EventOptionEdge,
  EventOptionId,
  IntakeSectionStepId,
  Role
} from '../../../../types';

/**
 * This event model defines the path to a given option in the graph
 */

export const PREGNANCY_TREE: EventOptionEdge[] = [
  { from: EventOptionId.ORIGIN, to: EventOptionId.PREGNANCY },
  { from: EventOptionId.PREGNANCY, to: EventOptionId.PREGNANCY_POSTNATAL },
  {
    from: EventOptionId.PREGNANCY,
    to: EventOptionId.PREGNANCY_PRENATAL
  },
  { from: EventOptionId.PREGNANCY, to: EventOptionId.PREGNANCY_TERMINATION },

  {
    from: EventOptionId.PREGNANCY_PRENATAL,
    to: EventOptionId.PREGNANCY_PRENATAL_CARE,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR],
    skipSteps: [IntakeSectionStepId.SUPPORTING_EVIDENCE]
  },
  {
    from: EventOptionId.PREGNANCY_PRENATAL,
    to: EventOptionId.PREGNANCY_PRENATAL_DISABILITY
  },
  {
    from: EventOptionId.PREGNANCY_PRENATAL,
    to: EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY
  },

  {
    from: EventOptionId.PREGNANCY_POSTNATAL,
    to: EventOptionId.PREGNANCY_BIRTH_DISABILITY
  },
  {
    from: EventOptionId.PREGNANCY_POSTNATAL,
    to: EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY
  }
];
