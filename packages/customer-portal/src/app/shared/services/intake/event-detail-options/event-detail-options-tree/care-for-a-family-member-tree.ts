import {
  EventOptionEdge,
  EventOptionId,
  IntakeSectionStepId,
  Role
} from '../../../../types';

export const CARE_FOR_A_FAMILY_MEMBER_TREE: EventOptionEdge[] = [
  {
    from: EventOptionId.ORIGIN,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR],
    skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
  },
  {
    from: EventOptionId.ORIGIN,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR]
  },
  {
    from: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREVENTIVE_CARE,
    skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
  },
  {
    from: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREGNANCY,
    skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
  },
  {
    from: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_OTHER,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_VIOLENCE
  },
  {
    from: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_VIOLENCE,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_YES,
    skipSteps: [IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES]
  },
  {
    from: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_VIOLENCE,
    to: EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_NO,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES
    ]
  }
];
