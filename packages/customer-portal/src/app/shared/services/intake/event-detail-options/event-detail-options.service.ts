import { flatten as _flatten } from 'lodash';

import {
  EventOptionId,
  EventOptionEdge,
  IntakeSectionStepId
} from '../../../types';
import { AuthorizationService } from '../../authorization';

import { EVENT_DETAIL_OPTIONS_TREE } from './event-detail-options-tree';

/**
 * Provides a simple API to get at the available options for event details.
 *
 * Usage is simple enough:
 *  - EventDetailOptionService.start() will give you the initial basic options
 *  - EventDetailOptionService.next(EventOptionId[]) will give you the children of the last element in the path
 *        This will also validate that the path is correct.
 */
export class EventDetailOptionService {
  private static _instance: EventDetailOptionService;
  authorizationService: AuthorizationService = AuthorizationService.getInstance();

  private eventOptionModel: EventOptionEdge[];

  constructor(eventOptionModel: EventOptionEdge[]) {
    this.eventOptionModel = eventOptionModel;
  }

  static getInstance(): EventDetailOptionService {
    return (
      EventDetailOptionService._instance ||
      (EventDetailOptionService._instance = new EventDetailOptionService(
        EVENT_DETAIL_OPTIONS_TREE
      ))
    );
  }

  /**
   * Get the first set of options in the event details option graph
   */
  start(): EventOptionId[] {
    return this.childrenOf(EventOptionId.ORIGIN);
  }

  /**
   * Get the next set of options from the given option path
   * @param path
   */
  next(path: EventOptionId[]): EventOptionId[] {
    // we keep popping elements from the start of this path
    // and validating they are in the allowed path
    // and then, we return the results
    const workingPath = path.filter(event => true);

    let currentChildren = this.start();
    while (workingPath.length > 0) {
      const top = workingPath.shift();
      // if we have no results in this path, then return nothing
      if (!top || currentChildren.indexOf(top) === -1) {
        return [];
      }
      currentChildren = this.childrenOf(top);
    }
    // return the current set of children
    return currentChildren;
  }

  getSkipSteps(path: EventOptionId[]): IntakeSectionStepId[] {
    return _flatten(
      this.eventOptionModel
        .filter(option => path.includes(option.to))
        .map(option => option.skipSteps || [])
    );
  }

  private childrenOf(option: EventOptionId): EventOptionId[] {
    return this.eventOptionModel
      .filter(r => this.limitByRole(r) && r.from === option)
      .map(r => r.to);
  }

  // only return options the user should see
  private limitByRole(option: EventOptionEdge) {
    return option.requiredRoles
      ? option.requiredRoles.find(r => this.authorizationService.hasRole(r))
      : true;
  }
}
