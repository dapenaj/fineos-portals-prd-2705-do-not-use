import {
  EventOptionEdge,
  EventOptionId,
  IntakeSectionStepId,
  Role
} from '../../../../types';

export const CHILD_BONDING_TREE: EventOptionEdge[] = [
  {
    from: EventOptionId.ORIGIN,
    to: EventOptionId.CHILD_BONDING,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR],
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.ADDITIONAL_INCOME_SOURCES,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.CHILD_BONDING,
    to: EventOptionId.CHILD_BONDING_NEWBORN
  },
  {
    from: EventOptionId.CHILD_BONDING,
    to: EventOptionId.CHILD_BONDING_ADOPTED
  },
  { from: EventOptionId.CHILD_BONDING, to: EventOptionId.CHILD_BONDING_FOSTER }
];
