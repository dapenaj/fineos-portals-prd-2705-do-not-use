import {
  EventOptionEdge,
  EventOptionId,
  IntakeSectionStepId,
  Role
} from '../../../../types';

/**
 * This event model defines the path to a given option in the graph
 */

export const OTHER_TREE: EventOptionEdge[] = [
  {
    from: EventOptionId.ORIGIN,
    to: EventOptionId.OTHER,
    requiredRoles: [Role.ABSENCE_USER, Role.ABSENCE_SUPERVISOR]
  },
  // Employee tree
  { from: EventOptionId.OTHER, to: EventOptionId.OTHER_EMPLOYEE },

  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_JURY_DUTY,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_VOTING,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_WITNESS,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_EDUCATION,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_MILITARY,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_VIOLENCE
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_UNION,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE,
    to: EventOptionId.OTHER_EMPLOYEE_EMERGENCY,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },

  {
    from: EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
    to: EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES
  },
  {
    from: EventOptionId.OTHER_EMPLOYEE_VIOLENCE,
    to: EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_NO,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },

  // Family Member tree
  { from: EventOptionId.OTHER, to: EventOptionId.OTHER_FAMILY_MEMBER },

  {
    from: EventOptionId.OTHER_FAMILY_MEMBER,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_DONATION
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER,
    to: EventOptionId.OTHER_FAMILY_MEMBER_PASSED_AWAY,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER,
    to: EventOptionId.OTHER_FAMILY_MEMBER_EDUCATION,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER,
    to: EventOptionId.OTHER_FAMILY_MEMBER_EMERGENCY,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },

  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CHILDCARE,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_COUNSELING,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_LEGAL,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_EVENT,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CARE_PARENT,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_POST_DEPLOYMENT,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_RR_LEAVE,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_SHORT_NOTICE,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  },
  {
    from: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY,
    to: EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_ADDITIONAL,
    skipSteps: [
      IntakeSectionStepId.REQUEST_DETAILS,
      IntakeSectionStepId.SUPPORTING_EVIDENCE
    ]
  }
];
