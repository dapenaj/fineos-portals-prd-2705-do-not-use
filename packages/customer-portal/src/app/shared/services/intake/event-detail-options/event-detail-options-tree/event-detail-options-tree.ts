import { EventOptionEdge } from '../../../../types';

import { SICKNESS_TREE } from './sickness-tree';
import { ACCIDENT_TREE } from './accident-tree';
import { PREGNANCY_TREE } from './pregnancy-tree';
import { CHILD_BONDING_TREE } from './child-bonding-tree';
import { CARE_FOR_A_FAMILY_MEMBER_TREE } from './care-for-a-family-member-tree';
import { OTHER_TREE } from './other-tree';
import { ACCOMMODATION_TREE } from './accommodation-tree';

// Note: the order of this array dictates the order they are shown in the UI
export const EVENT_DETAIL_OPTIONS_TREE: EventOptionEdge[] = [
  ...SICKNESS_TREE,
  ...ACCIDENT_TREE,
  ...PREGNANCY_TREE,
  ...CHILD_BONDING_TREE,
  ...CARE_FOR_A_FAMILY_MEMBER_TREE,
  ...OTHER_TREE,
  ...ACCOMMODATION_TREE
];
