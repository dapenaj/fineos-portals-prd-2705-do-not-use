import { EventOptionEdge, EventOptionId } from '../../../../types';

/**
 * This event model defines the path to a given option in the graph
 */

export const ACCIDENT_TREE: EventOptionEdge[] = [
  { from: EventOptionId.ORIGIN, to: EventOptionId.ACCIDENT }
];
