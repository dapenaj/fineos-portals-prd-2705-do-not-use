import { DocumentUploadRequest, DocumentService } from 'fineos-js-api-client';

import {
  IntakeSubmissionResult,
  AbstractIntakeSubmission,
  IntakeSubmissionSubCaseResult,
  IntakeSubmission,
  IntakeAccommodationSubmission
} from '../submission';
import { WrapUpSubmission } from '../../../../modules/intake';
import { AuditBuilder } from '../audit';
import { getMessageForLocale } from '../../../../i18n/utils';
import { EventOptionId, KeyValue } from '../../../types';
import { b64EncodeUnicode } from '../../../utils';

export abstract class AbstractSnapshotService {
  private documentService = DocumentService.getInstance();
  /**
   * Given the results of the intake, create a snapshot and submit it.
   * The results of this promise can be ignored, even on a failure.
   *
   * We only record them here so we can debug them
   *
   */
  protected abstract submitSnapshot(
    model: IntakeSubmission | IntakeAccommodationSubmission,
    result: IntakeSubmissionResult,
    wrapUp?: WrapUpSubmission
  ): Promise<any>;

  protected abstract buildAuditLog(
    model: IntakeSubmission | IntakeAccommodationSubmission,
    result: IntakeSubmissionResult,
    wrapUp?: WrapUpSubmission
  ): string;

  protected uploadSnapshot(result: IntakeSubmissionResult, log: string) {
    const hasErrors = result.generalError || result.postCaseErrors.length;
    const request: DocumentUploadRequest = {
      documentType: 'Customer Portal Snapshot Document',
      fileName:
        `Portal Intake Snapshot ${result.createdNotificationId}` +
        (hasErrors ? ` (Incomplete)` : ``),
      fileContentsText: b64EncodeUnicode(log),
      fileExtension: 'txt',
      description: 'Portal Intake Snapshot document'
    };

    const promises = [
      result.createdNotificationId,
      result.createdAbsence.id,
      result.createdClaim.id,
      result.createdAccommodation && result.createdAccommodation.id
    ].map(async id => {
      if (id) {
        try {
          const upload = await this.documentService.uploadCaseDocument(
            id,
            request
          );

          return await Promise.resolve(
            `Created snapshot for case ${id} as document ${upload.documentId}`
          );
        } catch (error) {
          return await Promise.resolve(
            `Could not create snapshot case ${id} because: ${error}`
          );
        }
      }
    });

    return Promise.all(promises);
  }
  /**
   * Set the general state of the intake - did it fail, what cases were created, etc
   */
  protected setGeneralSnapshot(
    builder: AuditBuilder,
    result: IntakeSubmissionResult
  ) {
    const SECTION = 'Main';
    const STEP = 'General';
    // add main doc details
    if (result.generalError) {
      builder.addMessage(STEP, SECTION, 'General Error', result.generalError);
    }

    builder.addMessage(
      STEP,
      SECTION,
      'Notification Case Id',
      result.createdNotificationId
    );

    this.addSubCaseMessages(
      builder,
      STEP,
      SECTION,
      'Absence',
      result.createdAbsence
    );

    this.addSubCaseMessages(
      builder,
      STEP,
      SECTION,
      'Claim',
      result.createdClaim
    );

    if (result.createdAccommodation) {
      this.addSubCaseMessages(
        builder,
        STEP,
        SECTION,
        'Accommodation',
        result.createdAccommodation
      );
    }

    result.postCaseErrors.forEach(error => {
      builder.addMessage(
        STEP,
        SECTION,
        'Error submitting post case request',
        error
      );
    });
  }

  /**
   * Add the personal details to the audit
   *
   * @param builder
   * @param model
   * @param result
   */
  protected setPersonalDetailsSnapshot(
    builder: AuditBuilder,
    model: AbstractIntakeSubmission
  ) {
    const personalDetails = model.getPersonalDetails();
    if (personalDetails) {
      this.addYourRequestDetailsTo('Personal Details', builder, [
        { key: 'FIRST_NAME', value: personalDetails.firstName },
        { key: 'LAST_NAME', value: personalDetails.lastName },
        { key: 'DOB', value: personalDetails.dateOfBirth },
        { key: 'PHONE_NUMBER', value: personalDetails.phoneNumber },
        { key: 'EMAIL', value: personalDetails.email },
        { key: 'PREMISE_NUMBER', value: personalDetails.premiseNo },
        { key: 'ADDRESS_LINE_1', value: personalDetails.addressLine1 },
        { key: 'ADDRESS_LINE_2', value: personalDetails.addressLine2 },
        { key: 'ADDRESS_LINE_3', value: personalDetails.addressLine3 },
        { key: 'CITY', value: personalDetails.city },
        { key: 'STATE', value: personalDetails.state },
        { key: 'ZIP_CODE', value: personalDetails.postCode }
      ]);
    }
  }

  /**
   * Add the work details to the audit
   *
   * @param builder
   * @param model
   * @param result
   */
  protected setWorkDetailsSnapshot(
    builder: AuditBuilder,
    model: AbstractIntakeSubmission
  ) {
    const workDetails = model.getWorkDetails();
    if (workDetails) {
      this.addYourRequestDetailsTo('Work Details', builder, [
        { key: 'EMPLOYER', value: workDetails.employer },
        { key: 'JOB_TITLE', value: workDetails.jobTitle },
        { key: 'DATE_OF_HIRE', value: workDetails.dateJobBegan }
      ]);
    }
  }

  /**
   * Add the selected initial request to the audit
   *
   * @param builder
   * @param model
   * @param result
   */
  protected setInitialRequestSnapshot(
    builder: AuditBuilder,
    model: AbstractIntakeSubmission
  ) {
    let prefix = 'HOW_CAN_WE_HELP_YOU';
    model.getEventOptions().forEach(option => {
      builder.addMessage(
        'Your Request',
        'Initial Request',
        this.eventOptionPrefixMessage(prefix),
        this.eventOptionMessage(option)
      );
      prefix = option;
    });
  }

  /**
   * For snapshots we only care about english - it's a log not a customer message
   * @param key the full key
   * @param defaultValue  the default value to return if there is no message, if this isn't set then the key is used
   */
  protected getEnglishMessage(key: string, defaultValue?: string) {
    return getMessageForLocale('en', key) || defaultValue || key;
  }

  /**
   * Translate the event option
   */
  protected eventOptionMessage(key: EventOptionId, defaultMessage?: string) {
    return this.getEnglishMessage(
      `INTAKE.YOUR_REQUEST.INITIAL_REQUEST_OPTIONS.${key}`,
      defaultMessage
    );
  }

  /**
   * Translate the event option
   */
  private eventOptionPrefixMessage(key: string) {
    return this.getEnglishMessage(
      `INTAKE.YOUR_REQUEST.INITIAL_REQUEST_SUBTITLES.${key}`
    );
  }

  /**
   * Translate the your request field Id
   */
  private yourRequestMessage(value: string) {
    return this.getEnglishMessage(`INTAKE.YOUR_REQUEST.LABELS.${value}`, value);
  }

  private addYourRequestDetailsTo(
    step: string,
    builder: AuditBuilder,
    values: KeyValue[]
  ) {
    values.forEach(({ key, value }) => {
      if (value) {
        builder.addMessage(
          'Your Request',
          step,
          this.yourRequestMessage(key),
          value
        );
      }
    });
  }

  private addSubCaseMessages(
    builder: AuditBuilder,
    step: string,
    section: string,
    type: string,
    result: IntakeSubmissionSubCaseResult
  ) {
    if (result.error) {
      builder.addMessage(
        step,
        section,
        `Error creating ${type} `,
        result.error
      );
    } else if (result.id) {
      builder.addMessage(step, section, `Created ${type} `, result.id);
    } else {
      builder.addMessage(step, section, `No request to create ${type}`);
    }
  }
}
