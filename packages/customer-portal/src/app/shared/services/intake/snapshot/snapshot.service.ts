import { AuditService, AuditBuilder } from '../audit';
import {
  RequestDetailForm,
  RequestDetailField,
  RequestDetailFormId,
  RequestDetailFieldId,
  KeyValue
} from '../../../types';
import { RequestDetailFormService } from '../request-detail-form';
import { IntakeSubmission, IntakeSubmissionResult } from '../submission';
import { IncomeSource, WrapUpSubmission } from '../../../../modules/intake';

import { AbstractSnapshotService } from './abstract-snapshot.service';

export class SnapshotService extends AbstractSnapshotService {
  static _instance: SnapshotService;

  static REQUEST_DETAILS = 'Request Details';

  private auditService = AuditService.getInstance();
  private requestDetailFormService = RequestDetailFormService.getInstance();
  static getInstance(): SnapshotService {
    return (
      SnapshotService._instance ||
      (SnapshotService._instance = new SnapshotService())
    );
  }

  submitSnapshot(
    model: IntakeSubmission,
    result: IntakeSubmissionResult,
    wrapUp: WrapUpSubmission
  ): Promise<any> {
    // build the audit message
    // we send the same one to any case that we created
    const log = this.buildAuditLog(model, result, wrapUp);
    return this.uploadSnapshot(result, log);
  }

  /**
   * Build and audit log
   * @param model the source model
   * @param result the result of the submission
   * @param wrapUp the result of Wrap Up section submission
   */
  buildAuditLog(
    model: IntakeSubmission,
    result: IntakeSubmissionResult,
    wrapUp: WrapUpSubmission
  ): string {
    const builder = this.auditService.startAudit();

    this.setGeneralSnapshot(builder, result);
    this.setPersonalDetailsSnapshot(builder, model);
    this.setWorkDetailsSnapshot(builder, model);
    this.setInitialRequestSnapshot(builder, model);
    this.setRequestDetailsFormSnapshot(builder, model);
    this.setTimeOffSnapshot(builder, model);
    this.setAdditionalDetailsSnapshot(builder, wrapUp.additionalDetailsValue);

    return this.auditService.buildAuditLog(builder.getAudit());
  }

  /**
   * Add the selected form details to the snapshot
   * @param builder
   * @param model
   * @param result
   */
  private setRequestDetailsFormSnapshot(
    builder: AuditBuilder,
    model: IntakeSubmission
  ) {
    // add any populated form options,
    // in the order they are defined in the configs
    this.requestDetailFormService
      .getDefaultForms(model.getEventOptions())
      .forEach(form => this.addRequestFormFields(builder, form, model));
  }

  /**
   * Add the timeoff to the snapshot.
   *
   * NOTE: this only covers continuous timeoffs at the moment
   * @param builder
   * @param model
   * @param result
   */
  private setTimeOffSnapshot(builder: AuditBuilder, model: IntakeSubmission) {
    const timeOff = model.getTimeOff();

    if (timeOff.timeOffLeavePeriods) {
      const withTense = timeOff.outOfWork
        ? (translationId: string) => `${translationId}_OUT_OF_WORK`
        : (translationId: string) => `${translationId}_AT_WORK`;
      timeOff.timeOffLeavePeriods.forEach((period, index) => {
        const timeoffType = 'Continuous';
        this.addTimeOffValuesTo(builder, timeoffType, index, [
          // These questions are rendered in the order they appear in the UI
          { key: withTense('LAST_DAY_WORKED'), value: period.lastDayWorked },
          {
            key: withTense('PARTIAL_DAY'),
            value: this.timeoffOptionMessage(
              period.startDateFullDay ? 'NO' : 'YES'
            )
          },
          {
            key: withTense('HOW_MUCH_TIME_ABSENT'),
            value: this.timeoffTimeMessage(
              period.startDateOffHours,
              period.startDateOffMinutes
            )
          },
          { key: withTense('FIRST_DAY_OF_ABSENCE'), value: period.startDate },
          { key: withTense('LAST_DAY_OF_ABSENCE'), value: period.endDate },
          {
            key: 'RETURN_TO_WORK',
            value: period.expectedReturnToWorkDate
          },
          {
            key: 'NORMAL_HOURS',
            value: this.timeoffOptionMessage(
              `${period.endDateFullDay ? 'YES' : 'NO'}`
            )
          },
          {
            key: withTense('HOW_MUCH_TIME_ABSENT'),
            value: this.timeoffTimeMessage(
              period.endDateOffHours,
              period.endDateOffMinutes
            )
          },
          {
            key: 'RETURN_TO_WORK_FULLTIME',
            value: period.returnToWorkFullDate
          },
          { key: 'DATES_CONFIRMED', value: period.status }
        ]);
      });
    }

    if (timeOff.reducedScheduleLeavePeriods) {
      timeOff.reducedScheduleLeavePeriods.forEach((period, index) => {
        const timeoffType = 'Reduced Schedule';
        this.addTimeOffValuesTo(builder, timeoffType, index, [
          // These questions are rendered in the order they appear in the UI
          { key: 'FIRST_DAY_REDUCED_SCHEDULE', value: period.startDate },
          { key: 'LAST_DAY_REDUCED_SCHEDULE', value: period.endDate },
          { key: 'DATES_CONFIRMED', value: period.status }
        ]);
      });
    }

    if (timeOff.episodicLeavePeriods) {
      timeOff.episodicLeavePeriods.forEach((period, index) => {
        const timeoffType = 'Intermittent';
        this.addTimeOffValuesTo(builder, timeoffType, index, [
          // These questions are rendered in the order they appear in the UI
          { key: 'FIRST_DAY_EPISODIC', value: period.startDate },
          {
            key: 'LAST_DAY_EPISODIC',
            value: period.endDate
              ? period.endDate
              : this.timeoffMessage('UNKNOWN')
          }
        ]);
      });
    }
  }

  /**
   * Add the additional details to the snapshot.
   *
   * @param builder
   * @param model
   * @param result
   */

  private setAdditionalDetailsSnapshot(
    builder: AuditBuilder,
    additionalDetails?: IncomeSource[]
  ) {
    if (additionalDetails) {
      additionalDetails.forEach((incomeSource, index) => {
        const incomeType = 'Additional Income Source';
        this.addAdditionalDetailsValuesTo(builder, incomeType, index, [
          { key: 'INCOME_TYPE_LABEL', value: incomeSource.incomeType },
          { key: 'FREQUENCY_LABEL', value: incomeSource.frequency },
          { key: 'AMOUNT_LABEL', value: incomeSource.amount },
          { key: 'START_DATE_LABEL', value: incomeSource.startDate },
          { key: 'END_DATE_LABEL', value: incomeSource.endDate }
        ]);
      });
    }
  }

  /**
   * Translate the form id
   */
  private formMessage(key: RequestDetailFormId) {
    return this.getEnglishMessage(
      `INTAKE.YOUR_REQUEST.INITIAL_REQUEST_FORMS.${key}`
    );
  }

  /**
   * Translate the field id
   */
  private fieldMessage(value: RequestDetailFieldId) {
    return this.getEnglishMessage(
      `INTAKE.DETAILS.REQUEST_DETAILS.${value}`,
      value
    );
  }

  /**
   * Translate the timeoff field Id
   */
  private timeoffMessage(value: string) {
    return this.getEnglishMessage(`INTAKE.DETAILS.LABELS.${value}`, value);
  }

  /**
   * Translate the timeoff option field Id
   */
  private timeoffOptionMessage(value: string) {
    return this.getEnglishMessage(
      `INTAKE.DETAILS.TIME_OFF.OPTIONS.${value}`,
      value
    );
  }

  /**
   * Translate the timeoff hours and minutes fields
   */
  private timeoffTimeMessage(hours: number = 0, minutes: number = 0) {
    if (hours || minutes) {
      return `${`${hours}`.padStart(2, '0')}:${`${minutes}`.padStart(2, '0')}`;
    } else {
      return undefined;
    }
  }

  /**
   * Translate the additionaldetails field Id
   */
  private additionalDetailsMessage(value: string) {
    return this.getEnglishMessage(
      `INTAKE.WRAP_UP.ADDITIONAL_INCOME_SOURCES.${value}`,
      value
    );
  }

  private addAdditionalDetailsValuesTo(
    builder: AuditBuilder,
    incomeType: string,
    index: number,
    keyValues: any[]
  ) {
    keyValues.forEach(keyValue => {
      const key = keyValue.key;
      const value = keyValue.value;
      if (value) {
        const message = this.additionalDetailsMessage(key);
        builder.addMessage(
          `Wrap Up`,
          `${incomeType} : ${index + 1}`,
          message,
          value
        );
      }
    });
  }

  private addTimeOffValuesTo(
    builder: AuditBuilder,
    timeoffType: string,
    index: number,
    values: KeyValue[]
  ) {
    values.forEach(({ key, value }) => {
      if (value) {
        builder.addMessage(
          'Details',
          `Time Off - ${timeoffType}: ${index + 1}`,
          this.timeoffMessage(key),
          value
        );
      }
    });
  }

  private addRequestFormFields(
    builder: AuditBuilder,
    form: RequestDetailForm,
    model: IntakeSubmission
  ): void {
    form.layout.forEach(field => {
      model.ifRequestDetailFieldPresent(field.id, value => {
        builder.addMessage(
          'Details',
          `Request Details - ${this.formMessage(form.id)}`,
          this.fieldMessage(field.id),
          this.eventOptionMessage(value, value)
        );
        this.addRequestSubFormFields(builder, model, field, value);
      });
    });
  }

  private addRequestSubFormFields(
    builder: AuditBuilder,
    model: IntakeSubmission,
    field: RequestDetailField,
    value: any
  ) {
    if (field.options) {
      const selectedOption = field.options.find(c => value === c.id);
      if (selectedOption && selectedOption.targetFormId) {
        const form = this.requestDetailFormService.getForm(
          selectedOption.targetFormId
        );
        if (form) {
          this.addRequestFormFields(builder, form, model);
        }
      }
    }
  }
}
