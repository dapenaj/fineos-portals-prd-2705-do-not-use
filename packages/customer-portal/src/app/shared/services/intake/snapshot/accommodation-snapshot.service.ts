import { WorkPlaceAccommodation, Limitation } from 'fineos-js-api-client';
import { stripIndents } from 'common-tags';

import { AuditService, AuditBuilder } from '../audit';
import {
  IntakeAccommodationSubmission,
  IntakeSubmissionResult
} from '../submission';
import { KeyValue } from '../../../types';

import { AbstractSnapshotService } from './abstract-snapshot.service';

export class AccommodationSnapshotService extends AbstractSnapshotService {
  static _instance: AccommodationSnapshotService;

  private auditService = AuditService.getInstance();
  static getInstance(): AccommodationSnapshotService {
    return (
      AccommodationSnapshotService._instance ||
      (AccommodationSnapshotService._instance = new AccommodationSnapshotService())
    );
  }

  submitSnapshot(
    model: IntakeAccommodationSubmission,
    result: IntakeSubmissionResult
  ): Promise<any> {
    // build the audit message
    // we send the same one to any case that we created
    const log = this.buildAuditLog(model, result);
    return this.uploadSnapshot(result, log);
  }

  /**
   * Build and audit log
   * @param model the source model
   * @param result the result of the submission
   * @param wrapUp the result of Wrap Up section submission
   */
  buildAuditLog(
    model: IntakeAccommodationSubmission,
    result: IntakeSubmissionResult
  ): string {
    const builder = this.auditService.startAudit();

    this.setGeneralSnapshot(builder, result);
    this.setPersonalDetailsSnapshot(builder, model);
    this.setWorkDetailsSnapshot(builder, model);
    this.setInitialRequestSnapshot(builder, model);
    this.setAccommodationSnapshot(builder, model);

    return this.auditService.buildAuditLog(builder.getAudit());
  }

  /**
   * Add the personal details to the audit
   *
   * @param builder
   * @param model
   * @param result
   */
  protected setAccommodationSnapshot(
    builder: AuditBuilder,
    model: IntakeAccommodationSubmission
  ) {
    const accommodation = model.getAccommodation();
    if (accommodation) {
      this.addAccommodation(builder, [
        {
          key: 'PREGNANCY_RELATED',
          value: accommodation.pregnancyRelated
        },
        {
          key: 'LIMITATION',
          value: accommodation.limitations
        },
        {
          key: 'WORK_PLACE_ACCOMMODATIONS',
          value: accommodation.workPlaceAccommodations
        },
        {
          key: 'ADDITIONAL_NOTES',
          value: accommodation.additionalNotes
        }
      ]);
    }
  }

  private addAccommodation(builder: AuditBuilder, values: KeyValue[]) {
    const pregnancyRelated: string = values.find(
      ({ key }) => key === 'PREGNANCY_RELATED'
    )!.value;

    builder.addMessage(
      'Details',
      'Request Details',
      this.accommodationMessage('PREGNANCY_RELATED'),
      pregnancyRelated
    );

    const description: string = values.find(
      ({ key }) => key === 'ADDITIONAL_NOTES'
    )!.value;

    builder.addMessage(
      'Details',
      'Request Details',
      this.accommodationMessage('ADDITIONAL_NOTES'),
      description
    );

    const limitations: Limitation[] = values.find(
      ({ key }) => key === 'LIMITATION'
    )!.value;

    this.addLimitations(
      builder,
      this.accommodationMessage('LIMITATION'),
      limitations
    );

    const accommodations: WorkPlaceAccommodation[] = values.find(
      ({ key }) => key === 'WORK_PLACE_ACCOMMODATIONS'
    )!.value;

    this.addAccommodations(
      builder,
      this.accommodationMessage('WORK_PLACE_ACCOMMODATIONS'),
      accommodations
    );
  }

  private accommodationMessage(value: string) {
    return this.getEnglishMessage(
      `INTAKE.DETAILS.ACCOMMODATION.LABELS.${value}`,
      value
    );
  }

  private addLimitations(
    builder: AuditBuilder,
    key: string,
    limitations: Limitation[]
  ) {
    limitations.forEach(({ limitationType }, index) =>
      builder.addMessage(
        'Details',
        `${this.accommodationMessage(key)}: ${index + 1}`,
        stripIndents`${limitationType}`
      )
    );
  }

  private addAccommodations(
    builder: AuditBuilder,
    key: string,
    accommodations: WorkPlaceAccommodation[]
  ) {
    accommodations.forEach(
      (
        { accommodationCategory, accommodationType, accommodationDescription },
        index
      ) => {
        builder.addMessage(
          'Details',
          `${this.accommodationMessage(key)}: ${index + 1}`,
          `${this.accommodationMessage(
            'ACCOMMODATION_CATEGORY'
          )}: ${accommodationCategory}`
        );
        builder.addMessage(
          'Details',
          `${this.accommodationMessage(key)}: ${index + 1}`,
          `${this.accommodationMessage(
            'ACCOMMODATION_TYPE'
          )}: ${accommodationType}`
        );
        builder.addMessage(
          'Details',
          `${this.accommodationMessage(key)}: ${index + 1}`,
          `${this.accommodationMessage('ACCOMMODATION_DESCRIPTION')}:${
            accommodationDescription ? ` ${accommodationDescription}` : ``
          }`
        );
      }
    );
  }
}
