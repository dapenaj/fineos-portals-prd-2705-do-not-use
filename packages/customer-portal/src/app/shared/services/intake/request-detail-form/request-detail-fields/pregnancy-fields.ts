import {
  RequestDetailFormUsage,
  RequestDetailFormId,
  EventOptionId,
  RequestDetailForm,
  RequestDetailFieldId,
  RequestDetailAnswerType
} from '../../../../types';

export const PREGNANCY_USAGE: RequestDetailFormUsage[] = [
  {
    formId: RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY,
    defaultFor: [EventOptionId.PREGNANCY_PRENATAL_DISABILITY]
  },
  {
    formId: RequestDetailFormId.PREGNANCY_PRENATAL_CARE,
    defaultFor: [EventOptionId.PREGNANCY_PRENATAL_CARE]
  },
  {
    formId: RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
    defaultFor: [EventOptionId.PREGNANCY_PRENATAL_BIRTH_DISABILITY]
  },
  {
    formId: RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION,
    defaultFor: [EventOptionId.PREGNANCY_TERMINATION]
  },
  {
    formId: RequestDetailFormId.PREGNANCY_POSTPARTUM,
    defaultFor: [
      EventOptionId.PREGNANCY_POSTPARTUM_DISABILITY,
      EventOptionId.PREGNANCY_BIRTH_DISABILITY
    ]
  }
];

export const PREGNANCY_FIELDS: Map<
  RequestDetailFormId,
  RequestDetailForm
> = new Map<RequestDetailFormId, RequestDetailForm>([
  [
    RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY,
    {
      id: RequestDetailFormId.PREGNANCY_PRENATAL_DISABILITY,
      layout: [
        {
          id: RequestDetailFieldId.DESCRIBE_SICKNESS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        },
        {
          id: RequestDetailFieldId.FIRST_SYMPTOM_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.PREGNANCY_MEDICAL_TREATMENT_FORM
            }
          ]
        },
        {
          id: RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_MEDICAL_TREATMENT_FORM,
    {
      id: RequestDetailFormId.MEDICAL_TREATMENT_FORM,
      layout: [
        {
          id: RequestDetailFieldId.DATE_FIRST_TREATED,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            {
              id: RequestDetailFieldId.NO
            },
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_PRENATAL_CARE,
    {
      id: RequestDetailFormId.PREGNANCY_PRENATAL_CARE,
      layout: [
        {
          id: RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          answerType: RequestDetailAnswerType.DATE
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
    {
      id: RequestDetailFormId.PREGNANCY_PRENATAL_PREFILE_BIRTH_DISABILITY,
      layout: [
        {
          id: RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.EXPECTED_OVERNIGHT_STAY_MEDICAL_TREATMENT
            }
          ]
        },
        {
          id: RequestDetailFieldId.EXPECTED_DELIVERY_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.EXPECTED_PREGNANCY_DELIVERY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN },
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL },
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION },
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE }
          ]
        },
        {
          id: RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATIONS,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION,
    {
      id: RequestDetailFormId.PREGNANCY_PRENATAL_TERMINATION,
      layout: [
        {
          id: RequestDetailFieldId.FIRST_SYMPTOM_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
            }
          ]
        },
        {
          id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_POSTPARTUM,
    {
      id: RequestDetailFormId.PREGNANCY_POSTPARTUM,
      layout: [
        {
          id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
            }
          ]
        },
        {
          id: RequestDetailFieldId.ACTUAL_DELIVERY_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.NEWBORN_COUNT,
          answerType: RequestDetailAnswerType.NUMBER_SELECT,
          numberSelectRange: {
            min: 1,
            max: 7
          }
        },
        {
          id: RequestDetailFieldId.PREGNANCY_DELIVERY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_UNKNOWN },
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_VAGINAL },
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_CSECTION },
            { id: RequestDetailFieldId.PREGNANCY_DELIVERY_MULTIPLE }
          ]
        },
        {
          id: RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATIONS,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS
            }
          ]
        },
        {
          id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS,
    {
      id: RequestDetailFormId.PREGNANCY_POSTPARTUM_COMPLICATION_DETAILS,
      layout: [
        {
          id: RequestDetailFieldId.PREGNANCY_POSTNATAL_COMPLICATION_DETAILS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS,
    {
      id: RequestDetailFormId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS,
      layout: [
        {
          id: RequestDetailFieldId.PREGNANCY_PRENATAL_COMPLICATION_DETAILS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.EXPECTED_OVERNIGHT_STAY_MEDICAL_TREATMENT,
    {
      id: RequestDetailFormId.EXPECTED_OVERNIGHT_STAY_MEDICAL_TREATMENT,
      layout: [
        {
          id: RequestDetailFieldId.EXPECTED_OVERNIGHT_HOSPITAL_STAY_DURATION,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ]
]);
