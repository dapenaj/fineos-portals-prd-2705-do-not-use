import {
  RequestDetailFormUsage,
  RequestDetailFormId,
  EventOptionId,
  RequestDetailForm,
  RequestDetailFieldId,
  RequestDetailAnswerType
} from '../../../../types';

export const SICKNESS_USAGE: RequestDetailFormUsage[] = [
  {
    formId: RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
    defaultFor: [EventOptionId.SICKNESS_TIMEOFF_FOR_SICKNESS],
    exactDefaultFor: [
      // for claimant user there generic sickness gonna be selected
      EventOptionId.SICKNESS
    ]
  },
  {
    formId: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
    defaultFor: [
      EventOptionId.SICKNESS_TIMEOFF_DONATION_BLOOD_STEMCELL,
      EventOptionId.SICKNESS_TIMEOFF_DONATION_BONE_MARROW,
      EventOptionId.SICKNESS_TIMEOFF_DONATION_ORGAN
    ]
  }
];

export const SICKNESS_FIELDS: Map<
  RequestDetailFormId,
  RequestDetailForm
> = new Map<RequestDetailFormId, RequestDetailForm>([
  // time off for sickness form

  [
    RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
    {
      id: RequestDetailFormId.TIME_OFF_FOR_SICKNESS,
      layout: [
        {
          id: RequestDetailFieldId.DESCRIBE_SICKNESS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        },
        {
          id: RequestDetailFieldId.IS_SICKNESS_WORK_RELATED,
          answerType: RequestDetailAnswerType.BOOLEAN
        },
        {
          id: RequestDetailFieldId.FIRST_SYMPTOM_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.IS_MEDICAL_TREATMENT_RECEIVED,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.MEDICAL_TREATMENT_FORM
            }
          ]
        },
        {
          id: RequestDetailFieldId.SICKNESS_ADDITIONAL_COMMENTS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.MEDICAL_TREATMENT_FORM,
    {
      id: RequestDetailFormId.MEDICAL_TREATMENT_FORM,
      layout: [
        {
          id: RequestDetailFieldId.DATE_FIRST_TREATED,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            {
              id: RequestDetailFieldId.NO,
              targetFormId:
                RequestDetailFormId.NO_OVERNIGHT_STAY_MEDICAL_TREATMENT
            },
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.NO_OVERNIGHT_STAY_MEDICAL_TREATMENT,
    {
      id: RequestDetailFormId.NO_OVERNIGHT_STAY_MEDICAL_TREATMENT,
      layout: [
        {
          id: RequestDetailFieldId.IS_N_CONSECUTIVE_DAY_TREATMENT,
          answerType: RequestDetailAnswerType.BOOLEAN
        },
        {
          id: RequestDetailFieldId.IS_SICKNESS_CHRONIC,
          answerType: RequestDetailAnswerType.BOOLEAN,
          hasTooltip: true
        },
        {
          id: RequestDetailFieldId.IS_MULTIPLE_TREATMENT,
          answerType: RequestDetailAnswerType.BOOLEAN
        }
      ]
    }
  ],
  [
    RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT,
    {
      id: RequestDetailFormId.OVERNIGHT_STAY_MEDICAL_TREATMENT,
      layout: [
        {
          id: RequestDetailFieldId.OVERNIGHT_HOSPITAL_STAY_DURATION,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ],
  // details for the medical donation form
  [
    RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
    {
      id: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE,
      layout: [
        {
          id: RequestDetailFieldId.DESCRIBE_PROCEDURE,
          answerType: RequestDetailAnswerType.LONG_TEXT
        },
        {
          id: RequestDetailFieldId.DONATION_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.REQUIRE_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY
            },
            { id: RequestDetailFieldId.NO }
          ]
        }
      ]
    }
  ],
  // details for the medical donation hospital stay
  [
    RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY,
    {
      id: RequestDetailFormId.MEDICAL_DONATION_PROCEDURE_HOSPITAL_STAY,
      layout: [
        {
          id: RequestDetailFieldId.DONATION_OVERNIGHT_DURATION,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ]
]);
