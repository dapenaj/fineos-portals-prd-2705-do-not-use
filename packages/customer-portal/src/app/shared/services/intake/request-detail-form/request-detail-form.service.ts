import { intersection as _intersection } from 'lodash';

import {
  EventOptionId,
  RequestDetailFormId,
  RequestDetailForm,
  RequestDetailFormUsage
} from '../../../types';

import {
  REQUEST_DETAIL_FORM_USAGE,
  REQUEST_DETAIL_FORM_FIELDS
} from './request-detail-fields';

/**
 * Provides a simple API to get at the available forms for request details
 *
 * Usage is simple enough:
 *  - RequestDetailFormService.getDefaultForms(path of event details options) will give you the basic default forms you need
 *  - Some forms have an option type, if that option is chosen, they may link to a subform
 *        use RequestDetailFormService.getForm() to get at that
 */
export class RequestDetailFormService {
  private static _instance: RequestDetailFormService;

  private formUsage: RequestDetailFormUsage[];
  private formModel: Map<RequestDetailFormId, RequestDetailForm>;

  constructor(
    formUsage: RequestDetailFormUsage[],
    formModel: Map<RequestDetailFormId, RequestDetailForm>
  ) {
    this.formUsage = formUsage;
    this.formModel = formModel;
  }

  static getInstance(): RequestDetailFormService {
    return (
      RequestDetailFormService._instance ||
      (RequestDetailFormService._instance = new RequestDetailFormService(
        REQUEST_DETAIL_FORM_USAGE,
        REQUEST_DETAIL_FORM_FIELDS
      ))
    );
  }

  /**
   * Given a path of options from the event details service: what default forms
   * are required to get enough information for this request?
   *
   * Returns a set of forms in the order they are to be presented.
   */
  getDefaultForms(eventDetailsPath: EventOptionId[]): RequestDetailForm[] {
    // we'll accumulate the forms we care about in here
    const result: RequestDetailForm[] = [];
    // if any forms are negated, we'll remove them from the list at the end
    const negating = new Set<RequestDetailFormId>();
    const targetEventOptionId = eventDetailsPath[eventDetailsPath.length - 1];

    this.formUsage
      // remove any that aren't needed for our path
      .filter(r => {
        const exactDefaultFor = r.exactDefaultFor || [];
        return (
          _intersection(eventDetailsPath, r.defaultFor).length > 0 ||
          exactDefaultFor.includes(targetEventOptionId)
        );
      })
      // copy the form into our list, but also keep track of any negated forms
      .forEach(usage => {
        const candidate = this.getForm(usage.formId);
        if (candidate) {
          // store any negated fields so we can remove them later on
          if (usage.negates) {
            usage.negates.forEach(n => negating.add(n));
          }
          result.push(candidate);
        }
      });

    // remove any negated forms
    return result.filter(f => !negating.has(f.id));
  }

  /**
   * Find a form by id, or return undefined if there is none
   */
  getForm(formId: RequestDetailFormId): RequestDetailForm | undefined {
    return this.formModel.get(formId);
  }
}
