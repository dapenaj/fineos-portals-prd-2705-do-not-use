import {
  RequestDetailFormUsage,
  RequestDetailFormId,
  RequestDetailForm
} from '../../../../types';

import { SICKNESS_USAGE, SICKNESS_FIELDS } from './sickness-fields';
import { ACCIDENT_USAGE, ACCIDENT_FIELDS } from './accident-fields';
import { PREGNANCY_USAGE, PREGNANCY_FIELDS } from './pregnancy-fields';
import {
  CARE_FOR_A_FAMILY_MEMBER_USAGE,
  CARE_FOR_A_FAMILY_MEMBER_FIELDS
} from './care-for-a-family-member-fields';
import { OTHER_USAGE, OTHER_FIELDS } from './other-fields';

export const REQUEST_DETAIL_FORM_USAGE: RequestDetailFormUsage[] = [
  ...SICKNESS_USAGE,
  ...ACCIDENT_USAGE,
  ...PREGNANCY_USAGE,
  ...CARE_FOR_A_FAMILY_MEMBER_USAGE,
  ...OTHER_USAGE
];

export const REQUEST_DETAIL_FORM_FIELDS: Map<
  RequestDetailFormId,
  RequestDetailForm
> = new Map();

SICKNESS_FIELDS.forEach((v, k) => REQUEST_DETAIL_FORM_FIELDS.set(k, v));
ACCIDENT_FIELDS.forEach((v, k) => REQUEST_DETAIL_FORM_FIELDS.set(k, v));
PREGNANCY_FIELDS.forEach((v, k) => REQUEST_DETAIL_FORM_FIELDS.set(k, v));
CARE_FOR_A_FAMILY_MEMBER_FIELDS.forEach((v, k) =>
  REQUEST_DETAIL_FORM_FIELDS.set(k, v)
);
OTHER_FIELDS.forEach((v, k) => REQUEST_DETAIL_FORM_FIELDS.set(k, v));
