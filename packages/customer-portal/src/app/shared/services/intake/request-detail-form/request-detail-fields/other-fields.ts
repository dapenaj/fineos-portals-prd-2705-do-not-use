import {
  RequestDetailFormUsage,
  RequestDetailFormId,
  EventOptionId,
  RequestDetailForm,
  RequestDetailFieldId,
  RequestDetailAnswerType
} from '../../../../types';

export const OTHER_USAGE: RequestDetailFormUsage[] = [
  {
    formId: RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM,
    defaultFor: [EventOptionId.OTHER_EMPLOYEE_MEDICAL_TREATMENT_YES]
  },
  {
    formId: RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM,
    defaultFor: [
      EventOptionId.OTHER_FAMILY_MEMBER_MILITARY_CAREGIVER,
      EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_PREVENTIVE,
      EventOptionId.OTHER_FAMILY_MEMBER_MEDICAL_DONATION
    ]
  }
];

export const OTHER_FIELDS: Map<
  RequestDetailFormId,
  RequestDetailForm
> = new Map<RequestDetailFormId, RequestDetailForm>([
  // Forms for EMPLOYEE reasons
  [
    RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM,
    {
      id: RequestDetailFormId.OTHER_EMPLOYEE_MEDICAL_FORM,
      layout: [
        {
          id: RequestDetailFieldId.IS_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            {
              id: RequestDetailFieldId.NO
            },
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.OTHER_OVERNIGHT_STAY_MEDICAL_TREATMENT
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.OTHER_OVERNIGHT_STAY_MEDICAL_TREATMENT,
    {
      id: RequestDetailFormId.OTHER_OVERNIGHT_STAY_MEDICAL_TREATMENT,
      layout: [
        {
          id: RequestDetailFieldId.OTHER_OVERNIGHT_HOSPITAL_STAY_DURATION,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ],

  // Forms for FAMILY MEMBER reasons
  [
    RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM,
    {
      id: RequestDetailFormId.OTHER_FAMILY_MEMBER_MEDICAL_FORM,
      layout: [
        {
          id: RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.OTHER_FAMILY_MEMBER_HOSPITALIZATION_FORM
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.OTHER_FAMILY_MEMBER_HOSPITALIZATION_FORM,
    {
      id: RequestDetailFormId.OTHER_FAMILY_MEMBER_HOSPITALIZATION_FORM,
      layout: [
        {
          id: RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITALIZATION_DATE,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ]
]);
