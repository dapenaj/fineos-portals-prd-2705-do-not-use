import {
  RequestDetailFormUsage,
  RequestDetailFormId,
  EventOptionId,
  RequestDetailForm,
  RequestDetailFieldId,
  RequestDetailAnswerType
} from '../../../../types';

export const ACCIDENT_USAGE: RequestDetailFormUsage[] = [
  {
    formId: RequestDetailFormId.ACCIDENT_FORM,
    defaultFor: [EventOptionId.ACCIDENT]
  }
];

export const ACCIDENT_FIELDS: Map<
  RequestDetailFormId,
  RequestDetailForm
> = new Map<RequestDetailFormId, RequestDetailForm>([
  [
    RequestDetailFormId.ACCIDENT_FORM,
    {
      id: RequestDetailFormId.ACCIDENT_FORM,
      layout: [
        {
          id: RequestDetailFieldId.DESCRIBE_ACCIDENT,
          answerType: RequestDetailAnswerType.LONG_TEXT
        },
        {
          id: RequestDetailFieldId.ACCIDENT_DATE,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.ACCIDENT_WORK_RELATED,
          answerType: RequestDetailAnswerType.BOOLEAN
        },
        {
          id: RequestDetailFieldId.ACCIDENT_IS_MEDICAL_TREATMENT_RECEIVED,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            {
              id: RequestDetailFieldId.YES,
              targetFormId: RequestDetailFormId.ACCIDENT_MEDICAL_TREATMENT_FORM
            },
            { id: RequestDetailFieldId.NO }
          ]
        },

        {
          id: RequestDetailFieldId.ACCIDENT_ADDITIONAL_COMMENTS,
          answerType: RequestDetailAnswerType.LONG_TEXT
        }
      ]
    }
  ],
  [
    RequestDetailFormId.ACCIDENT_MEDICAL_TREATMENT_FORM,
    {
      id: RequestDetailFormId.ACCIDENT_MEDICAL_TREATMENT_FORM,
      layout: [
        {
          id: RequestDetailFieldId.ACCIDENT_DATE_FIRST_TREATED,
          answerType: RequestDetailAnswerType.DATE
        },
        {
          id: RequestDetailFieldId.ACCIDENT_IS_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.ACCIDENT_OVERNIGHT_STAY_MEDICAL_TREATMENT_FORM
            },
            {
              id: RequestDetailFieldId.NO
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.ACCIDENT_OVERNIGHT_STAY_MEDICAL_TREATMENT_FORM,
    {
      id: RequestDetailFormId.ACCIDENT_OVERNIGHT_STAY_MEDICAL_TREATMENT_FORM,
      layout: [
        {
          id: RequestDetailFieldId.ACCIDENT_OVERNIGHT_HOSPITAL_STAY_DURATION,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ]
]);
