import {
  RequestDetailFormUsage,
  RequestDetailFormId,
  EventOptionId,
  RequestDetailForm,
  RequestDetailFieldId,
  RequestDetailAnswerType
} from '../../../../types';

export const CARE_FOR_A_FAMILY_MEMBER_USAGE: RequestDetailFormUsage[] = [
  {
    formId: RequestDetailFormId.FAMILY_MEMBER_MEDICAL_FORM,
    defaultFor: [
      EventOptionId.CARE_FOR_A_FAMILY_MEMBER_SICKNESS,
      EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREVENTIVE_CARE,
      EventOptionId.CARE_FOR_A_FAMILY_MEMBER_PREGNANCY,
      EventOptionId.CARE_FOR_A_FAMILY_MEMBER_MEDICAL_TREATMENT_YES
    ]
  }
];

export const CARE_FOR_A_FAMILY_MEMBER_FIELDS: Map<
  RequestDetailFormId,
  RequestDetailForm
> = new Map<RequestDetailFormId, RequestDetailForm>([
  [
    RequestDetailFormId.FAMILY_MEMBER_MEDICAL_FORM,
    {
      id: RequestDetailFormId.FAMILY_MEMBER_MEDICAL_FORM,
      layout: [
        {
          id: RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITAL_STAY,
          answerType: RequestDetailAnswerType.OPTIONS,
          options: [
            { id: RequestDetailFieldId.NO },
            {
              id: RequestDetailFieldId.YES,
              targetFormId:
                RequestDetailFormId.FAMILY_MEMBER_HOSPITALIZATION_FORM
            }
          ]
        }
      ]
    }
  ],
  [
    RequestDetailFormId.FAMILY_MEMBER_HOSPITALIZATION_FORM,
    {
      id: RequestDetailFormId.FAMILY_MEMBER_HOSPITALIZATION_FORM,
      layout: [
        {
          id: RequestDetailFieldId.FAMILY_MEMBER_OVERNIGHT_HOSPITALIZATION_DATE,
          answerType: RequestDetailAnswerType.DATE_RANGE
        }
      ]
    }
  ]
]);
