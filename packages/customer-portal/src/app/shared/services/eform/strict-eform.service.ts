import { CaseService, EForm, EFormAttribute } from 'fineos-js-api-client';
import moment, { Moment } from 'moment';

import { formatDateForApi } from '../../utils';

import { ConfirmReturnEForm } from './types';

const getAttribute = (
  eForm: EForm,
  attributeName: string
): EFormAttribute | undefined =>
  eForm.eFormAttributes.find(attribute => attribute.name === attributeName);

const getDateAttribute = (
  eForm: EForm,
  attributeName: string
): Moment | null => {
  const attribute = getAttribute(eForm, attributeName);
  return attribute && attribute.dateValue ? moment(attribute.dateValue) : null;
};
const getBooleanAttribute = (
  eForm: EForm,
  attributeName: string
): boolean | undefined => {
  const attribute = getAttribute(eForm, attributeName);
  return attribute && attribute.booleanValue;
};
const getStringAttribute = (
  eForm: EForm,
  attributeName: string
): string | undefined => {
  const attribute = getAttribute(eForm, attributeName);
  return attribute && attribute.stringValue;
};

export class StrictEFormService {
  static readonly CONFIRM_RETURN = 'Confirm Return to Work Details';

  private static _instance: StrictEFormService;

  caseService = CaseService.getInstance();

  static getInstance(): StrictEFormService {
    return (
      StrictEFormService._instance ||
      (StrictEFormService._instance = new StrictEFormService())
    );
  }

  async getConfirmReturnEForm(
    caseId: string
  ): Promise<ConfirmReturnEForm | undefined> {
    const eForm = await this.getEFormByType(
      caseId,
      StrictEFormService.CONFIRM_RETURN
    );

    if (eForm) {
      return {
        id: eForm.eFormId,
        expectedRTWDate: getDateAttribute(eForm, 'expectedReturnDate')!,
        actualRTWDate: getDateAttribute(eForm, 'actualReturnDate'),
        returningAsExpected: getBooleanAttribute(eForm, 'returningAsExpected')!,
        employerNotified: getBooleanAttribute(eForm, 'employerNotified'),
        employeeConfirmed: getBooleanAttribute(eForm, 'EmployeeConfirmed')!,
        rtwConfirmationNotes: getStringAttribute(eForm, 'rtwConfirmationNotes')
      };
    }
  }

  async updateConfirmReturnEForm(
    caseId: string,
    confirmReturnEForm: ConfirmReturnEForm
  ) {
    const expectedRTWDate = new EFormAttribute('expectedReturnDate');
    const actualRTWDate = new EFormAttribute('actualReturnDate');
    const returningAsExpected = new EFormAttribute('returningAsExpected');
    const employerNotified = new EFormAttribute('employerNotified');
    const employeeConfirmed = new EFormAttribute('EmployeeConfirmed');

    expectedRTWDate.date = confirmReturnEForm.expectedRTWDate
      ? formatDateForApi(confirmReturnEForm.expectedRTWDate)
      : '';
    employeeConfirmed.boolean = confirmReturnEForm.employeeConfirmed;
    returningAsExpected.boolean = confirmReturnEForm.returningAsExpected!;

    if (!confirmReturnEForm.returningAsExpected) {
      actualRTWDate.date = confirmReturnEForm.actualRTWDate
        ? formatDateForApi(confirmReturnEForm.actualRTWDate)
        : '';
      employerNotified.boolean = confirmReturnEForm.employerNotified!;
    } else {
      actualRTWDate.date = expectedRTWDate.date;
      employerNotified.boolean = true;
    }

    await this.caseService.updateEForm(caseId, confirmReturnEForm.id, [
      expectedRTWDate,
      returningAsExpected,
      actualRTWDate,
      employerNotified,
      employeeConfirmed
    ]);
  }

  private async getEFormByType(
    caseId: string,
    type: string
  ): Promise<EForm | undefined> {
    const eForms = await this.caseService.getEForms(caseId);
    const eForm = eForms.find(({ eFormType }) => eFormType === type);
    return eForm && this.caseService.readEForm(caseId, eForm.eFormId);
  }
}
