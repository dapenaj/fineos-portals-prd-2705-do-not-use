import { Moment } from 'moment';
import { EForm } from 'fineos-js-api-client';

export interface ConfirmReturnEForm {
  id: EForm['eFormId'];
  expectedRTWDate: Moment | null;

  employeeConfirmed: boolean;

  returningAsExpected: boolean | null;
  actualRTWDate: Moment | null;
  employerNotified: boolean | undefined;
  rtwConfirmationNotes: string | undefined;
}
