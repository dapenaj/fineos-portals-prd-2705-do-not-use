import {
  AbsenceService,
  ClaimService,
  NotificationCaseSummary,
  SupervisedNotificationCaseSummary,
  NotificationSupervisedSearch,
  Page,
  AbsenceSummary,
  ClaimSummary,
  NotificationAccommodationSummary
} from 'fineos-js-api-client';
import {
  uniqBy as _uniqBy,
  difference as _difference,
  isEmpty as _isEmpty,
  flatten as _flatten
} from 'lodash';

import { AuthorizationService } from '../authorization';
import { SelectOption } from '../../types';
import { formatDateForDisplay, MESSAGE_CASE_ID_SPLIT_CHAR } from '../../utils';

type EntitlementOption = {
  date?: string;
  benefitCaseType?: string;
} & SelectOption;

const ABSENCE = 'absence';
const CLAIM = 'claim';
const BENEFIT = 'benefit';
const ACCOMMODATION = 'accommodation';

/**
 * Wrapper service to the underlying notifications, so only the appropriate list of notifications is queries
 */
export class NotificationService {
  private static _instance: NotificationService;
  private authorizationService: AuthorizationService = AuthorizationService.getInstance();
  private absenceService: AbsenceService = new AbsenceService();
  private claimService: ClaimService = new ClaimService();

  static getInstance(): NotificationService {
    return (
      NotificationService._instance ||
      (NotificationService._instance = new NotificationService())
    );
  }

  /**
   * Get the notifications the user can see.
   */
  fetchNotifications(): Promise<NotificationCaseSummary[]> {
    // if we're an absence user then get the notifications from the absence service
    // these are complete notifications;
    // If we're only a claims user user, then get the limited claims notifications
    // otherwise return a promisee with an empty list
    if (
      this.authorizationService.isAbsenceSupervisor ||
      this.authorizationService.isAbsenceUser
    ) {
      return this.absenceService.findNotifications();
    } else if (this.authorizationService.isClaimsUser) {
      return this.claimService.findNotifications();
    } else {
      return Promise.resolve([]);
    }
  }

  /**
   * Get the notifications that the supervisor can see
   */
  fetchSupervisedNotifications(
    search: NotificationSupervisedSearch
  ): Promise<Page<SupervisedNotificationCaseSummary>> {
    return this.authorizationService.isAbsenceSupervisor
      ? this.absenceService.findSupervisedNotifications(search)
      : Promise.resolve({ total: 0, data: [] });
  }

  /**
   * Get the notifications the user can see.
   */
  completeNotification(
    caseId: string
  ): Promise<NotificationCaseSummary | undefined> {
    // if we're an absence user then get the notifications from the absence service
    // these are complete notifications;
    // If we're only a claims user user, then get the limited claims notifications
    // otherwise return a promise with an empty list
    if (
      this.authorizationService.isAbsenceSupervisor ||
      this.authorizationService.isAbsenceUser
    ) {
      return this.absenceService.completeIntake(caseId);
    } else if (this.authorizationService.isClaimsUser) {
      return this.claimService.completeIntake(caseId);
    } else {
      return Promise.resolve(undefined);
    }
  }

  /**
   * Get all case ids belonging to a Notification
   */
  async getCaseIdsFromNotification({
    notificationCaseId,
    absences,
    claims,
    accommodations
  }: NotificationCaseSummary): Promise<string[]> {
    const claimIds = !_isEmpty(claims)
      ? claims!.map(claim => claim.claimId)
      : [];

    const absenceIds =
      !_isEmpty(absences) && this.authorizationService.isAbsenceUser
        ? absences!.map(absence => absence.absenceId)
        : [];

    const accommodationIds =
      !_isEmpty(accommodations) && this.authorizationService.isAbsenceUser
        ? accommodations!.map(
            accommodation => accommodation.accommodationCaseId
          )
        : [];

    const notificationCaseIds = [
      notificationCaseId,
      ...claimIds,
      ...absenceIds,
      ...accommodationIds
    ];

    try {
      const benefitIds = await Promise.all(
        claimIds.map(async id => {
          const benefits = await this.claimService.findBenefits(id);
          return benefits.map(({ benefitId }) => String(benefitId));
        })
      );

      return Promise.resolve([...notificationCaseIds, ..._flatten(benefitIds)]);
    } catch (error) {
      return Promise.resolve(notificationCaseIds);
    }
  }

  /**
   * Get the Entitlements and Benefits for a Notification
   */
  async fetchNotificationEntitlements({
    absences,
    claims,
    accommodations
  }: NotificationCaseSummary): Promise<SelectOption[]> {
    try {
      const entitlementOptions = [
        ...this.fetchAbsenceEntitlements(absences),
        ...(await this.fetchClaimEntitlements(claims)),
        ...(await this.fetchAbsenceBenefitEntitlements(absences)),
        ...this.fetchAccommodationEntitlements(accommodations)
      ];

      const duplicates = _difference(
        entitlementOptions,
        _uniqBy(entitlementOptions, 'text')
      );

      const options = entitlementOptions.map(
        ({ text, value, date, benefitCaseType }) => {
          const isDuplicate = duplicates.find(
            duplicate => duplicate.text === text
          );

          return !_isEmpty(benefitCaseType)
            ? {
                value,
                text: !!isDuplicate
                  ? `${text} (${benefitCaseType} ${date})`
                  : `${text} (${benefitCaseType})`
              }
            : {
                value,
                text: !!isDuplicate ? `${text} (${date})` : `${text}`
              };
        }
      );

      return Promise.resolve(options);
    } catch (error) {
      return Promise.resolve(error);
    }
  }

  private fetchAbsenceEntitlements(
    absences?: AbsenceSummary[]
  ): EntitlementOption[] {
    if (this.authorizationService.isAbsenceUser && !_isEmpty(absences)) {
      return absences!.map(({ absenceId, createdDate }, index) => ({
        text: 'Job-protected Leave',
        value: `${absenceId}${MESSAGE_CASE_ID_SPLIT_CHAR}${index}${ABSENCE}`,
        date: formatDateForDisplay(createdDate)
      }));
    }

    return [];
  }

  private async fetchClaimEntitlements(
    claims?: ClaimSummary[]
  ): Promise<EntitlementOption[]> {
    const claimEntitlements: EntitlementOption[] = [];

    if (!_isEmpty(claims)) {
      try {
        await Promise.all(
          claims!.map(async ({ claimId }, claimIndex) => {
            const benefits = await this.claimService.findBenefits(claimId);

            if (!_isEmpty(benefits)) {
              benefits.forEach(
                (
                  { benefitCaseType, benefitId, creationDate },
                  benefitIndex
                ) => {
                  claimEntitlements.push({
                    text: 'Wage Replacement',
                    value: `${benefitId}${MESSAGE_CASE_ID_SPLIT_CHAR}${benefitIndex}${BENEFIT}`,
                    benefitCaseType,
                    date: formatDateForDisplay(creationDate)
                  });
                }
              );
            } else {
              claimEntitlements.push({
                text: 'Wage Replacement',
                value: `${claimId}${MESSAGE_CASE_ID_SPLIT_CHAR}${claimIndex}${CLAIM}`
              });
            }
          })
        );
      } catch (error) {
        return Promise.resolve(claimEntitlements);
      }
    }

    return Promise.resolve(claimEntitlements);
  }

  private async fetchAbsenceBenefitEntitlements(
    absences?: AbsenceSummary[]
  ): Promise<EntitlementOption[]> {
    const absenceBenefitEntitlements: EntitlementOption[] = [];

    if (this.authorizationService.isAbsenceUser && !_isEmpty(absences)) {
      try {
        await Promise.all(
          absences!.map(async ({ absenceId, createdDate }, index) => {
            const detail = await this.absenceService.getAbsenceDetails(
              absenceId
            );

            if (!_isEmpty(detail.financialCaseIds)) {
              absenceBenefitEntitlements.push({
                text: 'Wage Replacement (Paid Leaves)',
                value: `${absenceId}${MESSAGE_CASE_ID_SPLIT_CHAR}${index}${BENEFIT}`,
                date: formatDateForDisplay(createdDate)
              });
            }
          })
        );
        return Promise.resolve(absenceBenefitEntitlements);
      } catch (error) {
        return Promise.resolve(absenceBenefitEntitlements);
      }
    }

    return Promise.resolve(absenceBenefitEntitlements);
  }

  private fetchAccommodationEntitlements(
    accommodations: NotificationAccommodationSummary[] | undefined
  ): EntitlementOption[] {
    const accommodationEntitlements: EntitlementOption[] = [];

    if (this.authorizationService.isAbsenceUser && !_isEmpty(accommodations)) {
      accommodations!.forEach(
        ({ workplaceAccommodations, accommodationCaseId }) => {
          workplaceAccommodations.forEach(
            (
              { accommodationCategory, accommodationType, creationDate },
              accommodationIndex
            ) => {
              accommodationEntitlements.push({
                text: `${accommodationCategory} | ${accommodationType}`,
                value: `${accommodationCaseId}${MESSAGE_CASE_ID_SPLIT_CHAR}${accommodationIndex}${ACCOMMODATION}`,
                date: formatDateForDisplay(creationDate)
              });
            }
          );
        }
      );

      return accommodationEntitlements;
    }

    return accommodationEntitlements;
  }
}
