import { SdkConfigProps } from 'fineos-js-api-client';

import { AuthenticationService } from '../authentication';
import { Role } from '../../types';

export class AuthorizationService {
  private static _instance: AuthorizationService;

  authenticationService: AuthenticationService = AuthenticationService.getInstance();

  static getInstance(): AuthorizationService {
    return (
      AuthorizationService._instance ||
      (AuthorizationService._instance = new AuthorizationService())
    );
  }

  get isAbsenceUser() {
    return this.hasRole(Role.ABSENCE_USER) || this.isAbsenceSupervisor;
  }

  get isAbsenceSupervisor() {
    return this.hasRole(Role.ABSENCE_SUPERVISOR);
  }

  get isClaimsUser() {
    return this.hasRole(Role.CLAIMS_USER);
  }

  hasRole(role: Role) {
    return this.authenticationService.displayRoles.includes(
      SdkConfigProps.authorizationRole(role)
    );
  }
}
