import axios, { AxiosError } from 'axios';

import { showNotification } from '../../ui/notification/notification';
import { ClientConfig } from '../../types';

export class ClientConfigService {
  private static _instance: ClientConfigService;

  static getInstance(): ClientConfigService {
    return (
      ClientConfigService._instance ||
      (ClientConfigService._instance = new ClientConfigService())
    );
  }

  async fetchClientConfig() {
    try {
      const { data } = await axios.get<ClientConfig>(
        '/config/client-config.json'
      );

      return data;
    } catch (error) {
      const { response } = error as AxiosError;

      showNotification({
        type: 'error',
        title: 'An error has occurred.',
        content: 'Failed to fetch Client Configuration.'
      });

      return Promise.reject(response && response.data);
    }
  }
}
