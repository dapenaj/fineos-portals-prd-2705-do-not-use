import { saveAs } from 'file-saver';
import { Base64DocumentDownload } from 'fineos-js-api-client';

import { CsvDetails } from '../../types';

export class ExportService {
  private static _instance: ExportService;
  private rowDelim = '"\r\n"';
  private colDelim = '","';
  private csvType = 'data:application/csv;charset=utf-8,';
  private csvTypeIE = 'text/csv;charset=utf-8;';

  static getInstance(): ExportService {
    return (
      ExportService._instance || (ExportService._instance = new ExportService())
    );
  }

  exportToCsv({ columns, rows, filename }: CsvDetails) {
    // create csv table based on CsvDetails
    let csv = '"';
    // create table headers
    csv += columns.join(this.colDelim);
    // create table rows
    rows.forEach(row => {
      csv += this.rowDelim;
      csv += row.join(this.colDelim);
    });
    csv += '"';
    // add csv extension to filename
    filename = `${filename}.csv`;

    // IE 10+
    if (window.navigator.msSaveBlob) {
      const blob = new Blob([decodeURIComponent(encodeURI(csv))], {
        type: this.csvTypeIE
      });

      window.navigator.msSaveBlob(blob, filename);
    } else {
      const csvData = `${this.csvType}${encodeURIComponent(csv)}`;
      const link = document.createElement('a');
      link.href = csvData;
      link.setAttribute('visibility', 'hidden');
      link.download = filename;

      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  exportBinary(document: Base64DocumentDownload, defaultFileName: string) {
    try {
      const blob = this.b64toBlob(
        document.base64EncodedFileContents,
        document.contentType
      );
      const filename = document.fileName
        ? `${document.fileName}.${document.fileExtension}`
        : defaultFileName;

      saveAs(blob, filename);
    } catch (error) {
      throw new Error(error);
    }
  }

  private b64toBlob(b64Data: string, contentType: string): Blob {
    // for performance reason it's better to create blob in chunks
    const CHUNK_SIZE = 512;

    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += CHUNK_SIZE) {
      const slice = byteCharacters.slice(offset, offset + CHUNK_SIZE);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: contentType });
  }
}
