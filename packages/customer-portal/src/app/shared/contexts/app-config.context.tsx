import React from 'react';

import { Omit, AppConfig } from '../types';
import { DEFAULT_CLIENT_CONFIG, DEFAULT_CUSTOMER } from '../constants';

type AppConfigProps = {
  config: AppConfig;
};

const AppConfigContext = React.createContext<AppConfig>({
  clientConfig: DEFAULT_CLIENT_CONFIG,
  customerDetail: DEFAULT_CUSTOMER
});

export const AppConfigProvider: React.FC<AppConfigProps> = ({
  config,
  children
}) => (
  <AppConfigContext.Provider value={config}>
    {children}
  </AppConfigContext.Provider>
);

export const withAppConfig = <P extends object>(
  Component: React.ComponentType<P>
): React.FC<Omit<P, keyof AppConfigProps>> => props => (
  <AppConfigContext.Consumer>
    {config => <Component {...(props as P)} config={config} />}
  </AppConfigContext.Consumer>
);
