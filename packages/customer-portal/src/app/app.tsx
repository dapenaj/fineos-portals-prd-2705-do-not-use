import React from 'react';
import {
  CustomerService,
  Customer,
  Contact,
  Occupation,
  OccupationService,
  CustomerOccupationService,
  CustomerOccupation
} from 'fineos-js-api-client';

import {
  AuthenticationService,
  ClientConfigService,
  AuthorizationService
} from './shared/services';
import { overrideSdkAndApiConfigs } from './config';
import { AppConfigProvider } from './shared/contexts';
import { ClientConfig, CustomerDetail, AppConfig } from './shared/types';
import { DEFAULT_CLIENT_CONFIG, DEFAULT_CUSTOMER } from './shared/constants';
import { Internationalise } from './i18n/internationalise';
import { Routes } from './modules/routes';
import styles from './app.module.scss';

type AppState = {
  initialized: boolean;
  loading: boolean;
  clientConfig: ClientConfig;
  customerDetail: CustomerDetail;
};

type AppProps = {};

export class App extends React.Component<AppProps, AppState> {
  state: AppState = {
    initialized: false,
    loading: false,
    clientConfig: DEFAULT_CLIENT_CONFIG,
    customerDetail: DEFAULT_CUSTOMER
  };

  clientConfigService = ClientConfigService.getInstance();
  customerService = CustomerService.getInstance();
  occupationService = OccupationService.getInstance();
  customerOccupationService = CustomerOccupationService.getInstance();
  authenticationService = AuthenticationService.getInstance();
  authorizationService = AuthorizationService.getInstance();

  constructor(props: AppProps) {
    super(props);

    if (process.env.NODE_ENV !== 'production') {
      overrideSdkAndApiConfigs();
    }

    this.authenticationService.saveCredentials(document.location.hash);
  }

  async componentDidMount() {
    await this.fetchClientConfig();
    await this.fetchCustomerDetails();

    // at this point we can say that AppContainer initialized, but from props we would get loading: true
    // till the moment when config fully fetched
    this.setState({
      initialized: true
    });
  }

  fetchClientConfig = async () => {
    this.setState({ loading: true });

    try {
      const clientConfig = await this.clientConfigService.fetchClientConfig();
      this.setState({ clientConfig });
    } finally {
      this.setState({ loading: false });
    }
  };

  fetchCustomerDetails = async () => {
    this.setState({ loading: true });

    try {
      const customer = await this.fetchCustomer();
      const contact = await this.fetchContact();
      const occupation = this.authorizationService.isAbsenceUser
        ? await this.fetchOccupation()
        : null;
      const customerOccupations = await this.fetchCustomerOccupations();

      this.setState({
        customerDetail: {
          customer,
          contact,
          occupation,
          customerOccupations
        }
      });
    } finally {
      this.setState({ loading: false });
    }
  };

  fetchCustomer = async (): Promise<Customer | null> => {
    try {
      return await this.customerService.getCustomerDetails();
    } catch (error) {
      return null;
    }
  };

  fetchContact = async (): Promise<Contact | null> => {
    try {
      return await this.customerService.getCustomerContactDetails();
    } catch (error) {
      return null;
    }
  };

  fetchOccupation = async (): Promise<Occupation | null> => {
    try {
      return await this.occupationService.getOccupationDetails();
    } catch (error) {
      return null;
    }
  };

  fetchCustomerOccupations = async (): Promise<CustomerOccupation[] | null> => {
    try {
      return await this.customerOccupationService.getCustomerOccupations();
    } catch (error) {
      return null;
    }
  };

  render() {
    const { initialized, loading, clientConfig, customerDetail } = this.state;

    const config: AppConfig = {
      clientConfig,
      customerDetail
    };

    // app can be rendered after it's initialized and not loading anymore
    // a lot of components rely on initialized saveCredentials and fetched customer config
    return (
      initialized &&
      !loading && (
        <AppConfigProvider config={config}>
          <Internationalise language="en">
            <div className={styles.app}>
              <Routes />
            </div>
          </Internationalise>
        </AppConfigProvider>
      )
    );
  }
}
