import { RouterState } from 'connected-react-router';

import { IntakeState } from './intake';
import { NotificationState } from './notification';
import { MessageState } from './messages';

export type RootState = {
  router: RouterState;
  intake: IntakeState;
  notification: NotificationState;
  message: MessageState;
};
