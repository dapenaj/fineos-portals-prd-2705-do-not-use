import { apiReducer } from '../api-reducer';

import { NotificationState, NotificationAction } from './notification.type';

const initialState: NotificationState = { data: null, loading: false };

export const notificationReducer = apiReducer(
  initialState,
  NotificationAction.FETCH_NOTIFICATIONS
);
