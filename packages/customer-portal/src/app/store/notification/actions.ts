import { action } from 'typesafe-actions';

import { NotificationService } from '../../shared/services';

import { NotificationAction } from './notification.type';

const notificationService: NotificationService = NotificationService.getInstance();
export const fetchNotifications = () =>
  action(
    NotificationAction.FETCH_NOTIFICATIONS,
    notificationService.fetchNotifications()
  );
