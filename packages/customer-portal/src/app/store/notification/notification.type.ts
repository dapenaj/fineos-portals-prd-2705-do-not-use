import { NotificationCaseSummary } from 'fineos-js-api-client';

import { ApiState } from '../../shared/types';

export type NotificationState = ApiState<NotificationCaseSummary[]>;

export enum NotificationAction {
  FETCH_NOTIFICATIONS = '@@notification/FETCH_NOTIFICATIONS'
}
