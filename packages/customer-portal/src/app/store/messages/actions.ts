import { MessageService, Message, NewMessage } from 'fineos-js-api-client';
import { action, PayloadAction } from 'typesafe-actions';
import { ThunkAction } from 'redux-thunk';

import { NotificationOptions } from '../../shared/types';
import { RootState } from '../store.type';

import { MessageAction } from './messages.type';

const messageService: MessageService = MessageService.getInstance();

const actualFetchMessage = (): PayloadAction<
  MessageAction,
  Promise<Message[]>
> => action(MessageAction.FETCH_MESSAGES, messageService.getMessages());

export const fetchMessages = (): ThunkAction<
  void,
  RootState,
  void,
  ReturnType<typeof actualFetchMessage>
> => (dispatch, getState) => {
  const {
    message: {
      fetch: { loading }
    }
  } = getState();
  if (!loading) {
    dispatch(actualFetchMessage());
  }
};

export const markMessagesAsRead = (messageId: number) =>
  action(
    MessageAction.MARK_MESSAGES_AS_READ,
    messageService.markMessagesAsRead([messageId]),
    { messageId }
  );

export const addMessage = (
  message: NewMessage,
  successNotification: NotificationOptions
) =>
  action(MessageAction.ADD_MESSAGE, messageService.addMessage(message), {
    message,
    success: successNotification
  });
