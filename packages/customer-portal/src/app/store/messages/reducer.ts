import { combineReducers } from 'redux';
import { Message } from 'fineos-js-api-client';
import { PayloadMetaAction } from 'typesafe-actions';
import { isEmpty as _isEmpty } from 'lodash';

import { apiReducer } from '../api-reducer';
import { REJECTED, FULFILLED } from '../../shared/constants';
import { ApiState } from '../../shared/types';

import { MessageState, MessageAction } from './messages.type';

const initialState: MessageState = {
  fetch: { data: null, loading: false }
};

const fetchMessageReducer = apiReducer(
  initialState.fetch,
  MessageAction.FETCH_MESSAGES
);

export const updateMessageReducer = (
  state: ApiState<Message[]>,
  {
    type,
    payload,
    meta
  }: PayloadMetaAction<
    MessageAction,
    Message | undefined,
    { messageId: number; message: Message } | undefined
  >
) => {
  const { data } = state;

  if (meta && meta.messageId) {
    switch (type) {
      case FULFILLED(MessageAction.MARK_MESSAGES_AS_READ):
        return {
          ...state,
          data: !_isEmpty(data)
            ? data!.map(message =>
                message.messageId === meta.messageId
                  ? { ...message, read: true }
                  : message
              )
            : data
        };
      case REJECTED(MessageAction.MARK_MESSAGES_AS_READ):
        return {
          ...state,
          data: !_isEmpty(data)
            ? data!.map(message =>
                message.messageId === meta.messageId
                  ? { ...message, read: false }
                  : message
              )
            : data
        };
    }
  }

  if (
    meta &&
    meta.message &&
    type === FULFILLED(MessageAction.ADD_MESSAGE) &&
    payload
  ) {
    return {
      ...state,
      data: !_isEmpty(data)
        ? [
            ...data!,
            {
              ...payload
            }
          ]
        : data
    };
  }

  return state;
};

export const messageReducer = combineReducers<MessageState>({
  fetch: (state, action) =>
    updateMessageReducer(fetchMessageReducer(state, action), action)
});
