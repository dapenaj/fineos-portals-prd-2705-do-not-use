import { Message } from 'fineos-js-api-client';

import { ApiState } from '../../shared/types';

export type MessageState = {
  fetch: ApiState<Message[]>;
};

export enum MessageAction {
  FETCH_MESSAGES = '@@message/FETCH_MESSAGES',
  MARK_MESSAGES_AS_READ = '@@message/MARK_MESSAGES_AS_READ',
  ADD_MESSAGE = '@@message/ADD_MESSAGE'
}
