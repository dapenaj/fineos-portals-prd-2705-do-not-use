import { combineReducers } from 'redux';

import { IntakeState } from './intake.type';
import { intakeWorkflowReducer } from './workflow';

export const intakeReducer = combineReducers<IntakeState>({
  workflow: intakeWorkflowReducer
});
