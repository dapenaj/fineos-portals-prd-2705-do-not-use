import { Reducer } from 'redux';

import { intakeSections } from '../../../../app/modules/intake/intake-sections.constant';
import { IntakeSectionStep, IntakeSectionId } from '../../../shared/types';

import { IntakeWorkflowState, IntakeWorkflowAction } from './workflow.type';
import {
  updateSections,
  updateSteps,
  getAllSectionsComplete,
  getAllStepsComplete,
  applyOnSectionSkipMask,
  scrollToTop
} from './utils';

const initialState: IntakeWorkflowState = {
  allSectionsComplete: false,
  sections: intakeSections,
  previousSubmissionState: null,
  currentSection:
    intakeSections.find(section => section.isActive) || intakeSections[0],
  hasValues: false
};

export const intakeWorkflowReducer: Reducer<IntakeWorkflowState> = (
  state = initialState,
  { type, payload }
) => {
  let updatedSteps: IntakeSectionStep<any>[];

  const {
    sections,
    currentSection: { steps }
  } = state;

  switch (type) {
    case IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_SECTION:
      return {
        ...state,
        currentSection: payload,
        allSectionsComplete: getAllSectionsComplete(sections)
      };
    case IntakeWorkflowAction.FINISH_INTAKE_WORKFLOW:
      return {
        ...state,
        sections: sections.map(section => ({ ...section, isComplete: true })),
        ...(payload.value && payload.value.submissionState
          ? { previousSubmissionState: payload.value.submissionState }
          : {}),
        allSectionsComplete: true
      };
    case IntakeWorkflowAction.SUBMIT_INTAKE_WORKFLOW_STEP:
      const { skipSteps, ...stepState } = payload;
      const submissionState =
        stepState.value && stepState.value.submissionState;
      const noErrors =
        !submissionState ||
        (Boolean(submissionState.result.createdNotificationId) &&
          !submissionState.result.generalError &&
          submissionState.result.postCaseErrors.length === 0);

      updatedSteps = updateSteps(
        steps,
        { ...stepState, isComplete: true, isActive: false },
        steps.findIndex(step => step.id === stepState.id)
      );
      const isSectionComplete = updatedSteps.every(step => step.isComplete);
      const maskedSections = sections.map(section =>
        noErrors
          ? applyOnSectionSkipMask(section, skipSteps || [])
          : {
              ...section,
              isComplete: true
            }
      );

      if (isSectionComplete) {
        const currentSection = maskedSections.find(
          section => section.id === state.currentSection.id
        )!;
        const currentSectionIndex = maskedSections.indexOf(currentSection);

        const updatedSections = updateSections(
          maskedSections,
          { steps: updatedSteps, isComplete: true, isActive: false },
          maskedSections.findIndex(section => section.id === currentSection.id)
        );

        scrollToTop();

        return {
          ...state,
          sections: updatedSections,
          ...(submissionState
            ? { previousSubmissionState: submissionState }
            : {}),
          currentSection: {
            ...updatedSections[currentSectionIndex + 1],
            isActive: true
          },
          allSectionsComplete: getAllSectionsComplete(sections)
        };
      } else {
        return {
          ...state,
          sections: maskedSections,
          ...(submissionState
            ? { previousSubmissionState: submissionState }
            : {}),
          currentSection: {
            ...state.currentSection,
            steps: updatedSteps,
            isComplete: getAllStepsComplete(updatedSteps)
          },
          allSectionsComplete: noErrors
            ? getAllSectionsComplete(sections)
            : true,
          hasValues: true
        };
      }
    case IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_STEP:
      // Once the user has created a Case/Notification, then the user is not allowed to re-activate any previous steps.
      // Really this only applies to steps in the DETAILS section because users are unable to go back to other sections.
      if (
        state.currentSection.id === IntakeSectionId.DETAILS &&
        state.previousSubmissionState &&
        state.previousSubmissionState.result.createdNotificationId
      ) {
        updatedSteps = steps;
      } else {
        updatedSteps = updateSteps(
          steps,
          { ...payload, isActive: true },
          steps.findIndex(step => step.id === payload.id)
        );
      }

      return {
        ...state,
        currentSection: {
          ...state.currentSection,
          steps: updatedSteps,
          isComplete: getAllStepsComplete(steps)
        },
        allSectionsComplete: getAllSectionsComplete(sections)
      };
    case IntakeWorkflowAction.RESET_INTAKE_WORKFLOW:
      return initialState;
    default:
      return state;
  }
};
