import { IntakeSection } from '../../../shared/types';
import {
  YourRequestValue,
  PersonalDetailsValue,
  WorkDetailsValue,
  InitialRequestValue,
  DetailsValue,
  RequestDetailsValue,
  TimeOffValue,
  WrapUpValue,
  AdditionalIncomeValue,
  SupportingEvidenceValue
} from '../../../modules/intake';
import { IntakeSubmissionState } from '../../../shared/services';

export type SectionValue =
  | YourRequestValue
  | DetailsValue
  | WrapUpValue
  | undefined;

export type StepValue =
  | PersonalDetailsValue
  | WorkDetailsValue
  | InitialRequestValue
  | RequestDetailsValue
  | TimeOffValue
  | AdditionalIncomeValue
  | SupportingEvidenceValue;

export type IntakeWorkflowState = {
  allSectionsComplete: boolean;
  sections: IntakeSection<SectionValue>[];
  previousSubmissionState: IntakeSubmissionState | null;
  currentSection: IntakeSection<SectionValue>;
  hasValues: boolean;
};

export enum IntakeWorkflowAction {
  SELECT_INTAKE_WORKFLOW_SECTION = '@@intake/workflow/SELECT_INTAKE_WORKFLOW_SECTION',
  SELECT_INTAKE_WORKFLOW_STEP = '@@intake/workflow/SELECT_INTAKE_WORKFLOW_STEP',
  RESET_INTAKE_WORKFLOW = '@@intake/workflow/RESET_INTAKE_WORKFLOW',
  SUBMIT_INTAKE_WORKFLOW_STEP = '@@intake/workflow/SUBMIT_INTAKE_WORKFLOW_STEP',
  FINISH_INTAKE_WORKFLOW = '@@intake/workflow/FINISH_INTAKE_WORKFLOW'
}
