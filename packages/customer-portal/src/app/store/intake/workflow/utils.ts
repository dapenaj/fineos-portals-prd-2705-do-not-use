import {
  IntakeSection,
  IntakeSectionId,
  IntakeSectionStep,
  IntakeSectionStepId
} from '../../../shared/types';
import {
  PersonalDetailsValue,
  WorkDetailsValue,
  InitialRequestValue,
  AdditionalIncomeValue,
  SupportingEvidenceValue
} from '../../../modules/intake';
import {
  RequestDetailsValue,
  TimeOffValue
} from '../../../modules/intake/details';

import { SectionValue, StepValue } from './workflow.type';

export const updateSections = (
  sections: IntakeSection<any>[],
  {
    steps,
    isComplete,
    isActive
  }: Pick<IntakeSection<SectionValue>, 'steps' | 'isComplete' | 'isActive'>,
  currentIndex: number
) =>
  sections.map((section, index) => {
    const next = currentIndex + 1;

    if (index === currentIndex) {
      return {
        ...section,
        value: getValueFromSteps(section.id, steps),
        steps,
        isComplete,
        isActive
      };
    }

    return index === next && next <= sections.length
      ? {
          ...section,
          isActive: true,
          isComplete: getAllStepsComplete(section.steps)
        }
      : { ...section, isComplete: getAllStepsComplete(section.steps) };
  });

export const applyOnSectionSkipMask = (
  section: IntakeSection<any>,
  skipMask: IntakeSectionStepId[]
): IntakeSection<any> => {
  if (!skipMask.length) {
    return section;
  }

  const maskedSteps = [...section.steps];
  const maskedSection = {
    ...section,
    steps: maskedSteps
  };

  for (let i = 0; i < maskedSteps.length; i++) {
    const step = maskedSteps[i];

    if (skipMask.includes(step.id)) {
      if (step.isActive) {
        if (i + 1 < maskedSteps.length) {
          maskedSteps[i + 1] = { ...maskedSteps[i + 1], isActive: true };
        } else {
          // all steps complete cause last active is skipped
          maskedSection.isComplete = true;
          maskedSection.skip = true;
        }
      }

      maskedSteps[i] = {
        ...step,
        skip: true,
        isActive: false,
        isComplete: true
      };
    }
  }

  return maskedSection;
};

export const updateSteps = (
  steps: IntakeSectionStep<StepValue>[],
  { value, isActive, isComplete }: IntakeSectionStep<StepValue>,
  currentIndex: number
): IntakeSectionStep<StepValue>[] =>
  steps.map((step, index) => {
    const next = currentIndex + 1;

    if (index === currentIndex) {
      return {
        ...step,
        value,
        isComplete,
        isActive
      };
    }

    return index === next && next <= steps.length && !isActive
      ? { ...step, isActive: true }
      : { ...step, isActive: false };
  });

export const getAllStepsComplete = (steps: IntakeSectionStep<SectionValue>[]) =>
  steps.every(step => step.isComplete);

export const getAllSectionsComplete = (
  sections: IntakeSection<SectionValue>[]
) => sections.every(section => section.isComplete);

export const getValueFromSteps = (
  id: IntakeSectionId,
  steps: IntakeSectionStep<StepValue>[]
): SectionValue => {
  switch (id) {
    case IntakeSectionId.YOUR_REQUEST:
      return {
        personalDetails: steps[0].value as PersonalDetailsValue,
        workDetails: steps[1].value as WorkDetailsValue,
        initialRequest: steps[2].value as InitialRequestValue
      };
    case IntakeSectionId.DETAILS:
      return {
        requestDetails: steps[0].value as RequestDetailsValue,
        timeOff: steps[1].value as TimeOffValue
      };
    case IntakeSectionId.WRAP_UP:
      return {
        additionalIncome: steps[0].value as AdditionalIncomeValue,
        supportingEvidence: steps[1].value as SupportingEvidenceValue
      };
    default:
      return undefined;
  }
};

export const scrollToTop = () => window.scrollTo(0, 0);
