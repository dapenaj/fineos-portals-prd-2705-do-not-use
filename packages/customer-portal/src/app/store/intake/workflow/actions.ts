import { action } from 'typesafe-actions';

import {
  IntakeSection,
  IntakeSectionStep,
  IntakeSectionStepSubmit
} from '../../../shared/types';
import { history } from '../../../config';

import { IntakeWorkflowAction } from './workflow.type';

const {
  location: { state }
} = history as any;

export const selectIntakeWorkflowSection = <T>(payload: IntakeSection<T>) =>
  action(IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_SECTION, payload);

export const selectIntakeWorkflowStep = <T>(payload: IntakeSectionStep<T>) =>
  action(IntakeWorkflowAction.SELECT_INTAKE_WORKFLOW_STEP, payload);

export const submitIntakeWorkflowStep = <T>(
  payload: IntakeSectionStepSubmit<T>
) => action(IntakeWorkflowAction.SUBMIT_INTAKE_WORKFLOW_STEP, payload);

export const finishIntakeWorkflow = <T>(payload: IntakeSectionStep<T>) =>
  action(IntakeWorkflowAction.FINISH_INTAKE_WORKFLOW, payload);

export const resetIntake = () =>
  action(IntakeWorkflowAction.RESET_INTAKE_WORKFLOW);

export const cancelIntakeWorkflow = () => {
  history.push(state && state.prevPath ? state.prevPath : '/');

  return resetIntake();
};
