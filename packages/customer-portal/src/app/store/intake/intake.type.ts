import { IntakeWorkflowState } from './workflow';

export type IntakeState = {
  workflow: IntakeWorkflowState;
};
