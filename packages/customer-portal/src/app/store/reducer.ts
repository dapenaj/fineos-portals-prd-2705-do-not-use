import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { history } from '../config';

import { RootState } from './store.type';
import { intakeReducer } from './intake';
import { notificationReducer } from './notification';
import { messageReducer } from './messages';

export const rootReducer = combineReducers<RootState>({
  router: connectRouter(history),
  intake: intakeReducer,
  notification: notificationReducer,
  message: messageReducer
});
