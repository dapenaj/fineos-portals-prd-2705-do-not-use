import { AnyAction, Dispatch } from 'redux';

import { showNotification } from '../../shared/ui';
import { isPromise } from '../../shared/utils';
import { AuthenticationService } from '../../shared/services';

const authenticationService = AuthenticationService.getInstance();

export const apiResponseMiddleware = () => (next: Dispatch<AnyAction>) => (
  action: AnyAction
) => {
  if (!isPromise(action.payload)) {
    return next(action);
  }

  return next(action)
    .then((response: any) => {
      if (action.meta && action.meta.success) {
        const { title, content } = action.meta.success;

        showNotification({
          type: 'success',
          title: title ? title : 'Request successful.',
          content
        });
      }

      return Promise.resolve(response);
    })
    .catch((error: any) => {
      if (action.meta && action.meta.error) {
        const { title, content } = action.meta.error;
        showNotification({
          type: 'error',
          title: title ? title : 'An error has occurred.',
          content
        });
      } else if (action.meta && action.meta.warning) {
        const { title, content } = action.meta.warning;
        showNotification({
          type: 'error',
          title: title ? title : 'Something went wrong.',
          content
        });
      } else if (error) {
        const { data, status } = error;
        switch (status) {
          case 404:
            showNotification({
              type: 'error',
              title: 'Not found.',
              content: 'The requested resource could not be found.'
            });
            break;
          case 403:
            showNotification({
              type: 'warning',
              title: 'Unauthorised access.',
              content:
                'You do not have the required authorisation to access this resource.'
            });
            break;
          case 401:
            showNotification({
              type: 'error',
              title: 'You have been logged out.',
              content: 'We need to authenticate you, Please log in.'
            });

            authenticationService.logout();
            break;
          case 422:
            showNotification({
              type: 'warning',

              title:
                'Validation error occurred. Please resolve the following issues.',
              content: data
            });
            break;
          case 500:
          default:
            showNotification({
              type: 'error',
              title: 'An error has occurred.',
              content: 'Please try again later.'
            });
            break;
        }
      }

      return Promise.reject(error);
    });
};
