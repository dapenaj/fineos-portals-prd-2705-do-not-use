import { createStore, applyMiddleware, compose, Store } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import { history } from '../config/history.config';

import { RootState } from './store.type';
import { rootReducer } from './reducer';
import { apiResponseMiddleware } from './middleware';

export const buildStore = (): Store<RootState> => {
  const middlewares = applyMiddleware(
    routerMiddleware(history),
    apiResponseMiddleware,
    thunk,
    promise
  );

  const store = createStore(
    rootReducer,
    process.env.NODE_ENV === 'development'
      ? composeWithDevTools(middlewares)
      : compose(middlewares)
  );

  return store;
};
