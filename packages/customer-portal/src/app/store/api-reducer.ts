import { Reducer } from 'redux';

import { ApiState } from '../shared/types';
import { PENDING, REJECTED, FULFILLED } from '../shared/constants';

export type ApiReducer = <T>(
  state: ApiState<T>,
  actions: string
) => Reducer<ApiState<T>>;

export const apiReducer: ApiReducer = (initialState, action) => (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case PENDING(action):
      return {
        ...state,
        loading: true
      };
    case REJECTED(action):
      return {
        ...state,
        loading: false,
        error: payload
      };
    case FULFILLED(action):
      return {
        ...state,
        loading: false,
        data: payload
      };
    default:
      return state;
  }
};
