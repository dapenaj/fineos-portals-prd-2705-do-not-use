import {
  AuthenticationStrategy,
  SdkConfig,
  ApiConfig
} from 'fineos-js-api-client';

export const overrideSdkAndApiConfigs = () => {
  Object.assign(ApiConfig.getInstance().config, {
    enableTwilio: false
  });

  Object.assign(SdkConfig.getInstance().config, {
    authenticationStrategy: AuthenticationStrategy.NONE,
    authorizationRoles: {
      absenceUser: 'CustomerPortal_AbsenceEmployee_Role',
      absenceSupervisor: 'CustomerPortal_AbsenceSupervisor_Role',
      claimsUser: 'CustomerPortal_Claimant_Role'
    },
    apiUrls: {
      root: 'http://localhost:8888/api/v1'
    },
    debug: {
      userId: 'demof110',
      displayUserId: 'demof110@fineos.com',
      displayRoles: [
        'CustomerPortal_AbsenceSupervisor_Role',
        'CustomerPortal_AbsenceEmployee_Role'
      ]
    }
  });
};
