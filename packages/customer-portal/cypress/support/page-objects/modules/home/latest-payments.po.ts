import { PaymentLine } from '../my-payments/payment-line/payment-line';
import { PaymentDetails } from '../my-payments/payment-details/payment-details.po';

export class LatestPayments {
  paymentDetails: PaymentDetails = new PaymentDetails();
  paymentLines: PaymentLine = new PaymentLine();

  private latestPaymentsSelector = '[data-test-el="latest-payments"]';
  private paymentPanelHeaderSelector = '[data-test-el="payment-header"]';

  constructor(
    public elem: string = '[data-test-el="latest-payments-wrapper"]'
  ) {}

  get paymentsContainer() {
    return cy.get(this.latestPaymentsSelector);
  }

  getPaymentPanelHeader(index: number) {
    return cy.get(this.paymentPanelHeaderSelector).eq(index);
  }
}
