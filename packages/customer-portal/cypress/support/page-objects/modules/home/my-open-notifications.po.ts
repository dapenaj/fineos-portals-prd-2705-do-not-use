export class MyOpenNotifications {
  private notificationsSelector = '[data-test-el="open-notification"]';
  private progressSelector = '[data-test-el="case-progress"]';
  private assessmentSelector = '[data-test-el="case-assessments"]';
  private accordionSelector = '[role="tablist"]';

  constructor(public elem: string = '[data-test-el="my-open-notifications"]') {}

  get accordianHeader() {
    return cy.get(this.accordionSelector);
  }

  get notificationCount() {
    return cy.get(this.notificationsSelector);
  }

  getProgressAt(index: number) {
    return cy.get(this.progressSelector).eq(index);
  }

  getAssessmentAt(index: number) {
    return cy.get(this.assessmentSelector).eq(index);
  }
}
