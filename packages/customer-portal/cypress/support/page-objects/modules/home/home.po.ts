export class HomePage {
  private actionBtnSelector = '[data-test-el="action-button"]';

  constructor(public elem: string = '[data-test-el="home-view"]') {}

  get action() {
    return cy.get(this.actionBtnSelector);
  }

  navigateTo() {
    cy.visit('/#');
    cy.wait(300);
  }
}
