export class RequiredFromMe {
  private headerSelector = '[data-test-el="required-from-me-header"]';
  private listSelector = '[data-test-el="required-from-me-list"]';
  private nameSelector = '[data-test-el="required-from-me-doc-name"]';

  constructor(public elem: string = '[data-test-el="home-view"]') {}

  get header() {
    return cy.get(this.headerSelector);
  }

  get list() {
    return cy.get(this.listSelector);
  }

  get name() {
    return cy.get(this.nameSelector);
  }
}
