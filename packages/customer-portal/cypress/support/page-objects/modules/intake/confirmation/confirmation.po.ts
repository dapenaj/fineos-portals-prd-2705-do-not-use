export class ConfirmationPage {
  private intakeConfirmation = '[data-test-el="intake-confirmation"]';
  constructor(
    public elem: string = '[data-test-el="intake-confirmation-view"]'
  ) {}

  get container() {
    return cy.get(this.elem);
  }

  navigateToConfirmation() {
    cy.visit('/#intakeConfirmation');
    cy.wait(300);
  }

  getElement(type: string) {
    cy.get(this.intakeConfirmation).find(type);

    return this;
  }
}
