export class IntakeOverviewItem {
  private numberSelector = '[data-test-el="intake-overview-number"]';
  private titleSelector = '[data-test-el="intake-overview-title"]';

  constructor(public elem: string = '[data-test-el="intake-overview-item"]') {}

  get number() {
    return cy.get(this.numberSelector);
  }

  get title() {
    return cy.get(this.titleSelector);
  }
}
