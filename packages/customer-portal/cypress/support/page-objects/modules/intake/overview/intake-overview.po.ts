export class IntakeOverview {
  private numberSelector = '[data-test-el="intake-overview-number"]';
  private titleSelector = '[data-test-el="intake-overview-title"]';

  constructor(public elem: string = '[data-test-el="intake-overview-item"]') {}

  get count() {
    return cy.get(this.elem);
  }

  getItemAt(index: number) {
    return {
      number: cy.get(this.numberSelector).eq(index),
      title: cy.get(this.titleSelector).eq(index)
    };
  }
}
