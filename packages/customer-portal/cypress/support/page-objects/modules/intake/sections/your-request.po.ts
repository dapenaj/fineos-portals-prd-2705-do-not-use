import {
  PersonalDetailsForm,
  WorkDetailsRenderer,
  InitialRequestForm
} from '../steps';

export class YourRequest {
  personalDetailsForm: PersonalDetailsForm = new PersonalDetailsForm();
  workDetailsRenderer: WorkDetailsRenderer = new WorkDetailsRenderer();
  initialRequestForm: InitialRequestForm = new InitialRequestForm();

  fillForm() {
    cy.get(this.personalDetailsForm.elem).within(() => {
      this.personalDetailsForm.submitBtn.should('be.enabled');
      this.personalDetailsForm.submitBtn.click();
    });

    cy.get(this.workDetailsRenderer.existingWorkDetails.elem).within(() => {
      this.workDetailsRenderer.existingWorkDetails.proceedBtn.should(
        'be.enabled'
      );
      this.workDetailsRenderer.existingWorkDetails.proceedBtn.click();
    });

    cy.get(this.initialRequestForm.elem).within(() => {
      // this chooses a pregnancy workflow with lots of questions
      this.initialRequestForm.fillForm();
      this.initialRequestForm.submitBtn.should('be.enabled');
      this.initialRequestForm.submitBtn.click();
    });
  }
}
