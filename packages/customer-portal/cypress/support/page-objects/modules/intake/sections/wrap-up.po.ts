import { AdditionalIncome, SupportingEvidence } from '../steps/wrap-up';

export class WrapUp {
  additionalIncome: AdditionalIncome = new AdditionalIncome();
  supportingEvidence: SupportingEvidence = new SupportingEvidence();
}
