import { RequestDetailsForm, TimeOff } from '../steps';

export class IntakeDetail {
  requestDetailsForm: RequestDetailsForm = new RequestDetailsForm();
  timeOff: TimeOff = new TimeOff();
}
