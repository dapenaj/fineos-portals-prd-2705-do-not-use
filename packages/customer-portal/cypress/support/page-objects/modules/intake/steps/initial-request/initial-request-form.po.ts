export class InitialRequestForm {
  private optionGroupSelector = '[data-test-el="initial-request-option-group"]';
  private radioSelector = 'input[type="radio"]';
  private titleSelector = 'h3';
  private submitSelector = 'button[type="submit"]';

  constructor(public elem: string = '[data-test-el="initial-request"]') {}

  get submitBtn() {
    return cy.get(this.submitSelector);
  }

  get radioCount() {
    return cy.get(this.radioSelector);
  }

  get optionGroupCount() {
    return cy.get(this.optionGroupSelector);
  }

  checkRadioInGroupAt(group: number, radio: number) {
    return cy
      .get(this.optionGroupSelector)
      .eq(group)
      .within(() => {
        cy.get(this.radioSelector)
          .eq(radio)
          .check({ force: true });
      });
  }

  getTitleAt(index: number) {
    return cy.get(this.titleSelector).eq(index);
  }

  fillForm() {
    this.checkRadioInGroupAt(0, 2);
    this.checkRadioInGroupAt(1, 0);
    this.checkRadioInGroupAt(2, 0);
  }
}
