import { IncomeListItem } from './income-list-item.po';

export class IncomeSourceItem extends IncomeListItem {
  static readonly el: string = '[data-test-el="income-source-item"]';

  private titleSelector = '[data-test-el="income-source-item-title"]';
  private descriptionSelector =
    '[data-test-el="income-source-item-description"]';

  constructor(index: number) {
    super(`${IncomeSourceItem.el}:eq(${index})`);
  }

  get title() {
    return cy.get(`${this.el} ${this.titleSelector}`);
  }

  get description() {
    return cy.get(`${this.el} ${this.descriptionSelector}`);
  }
}
