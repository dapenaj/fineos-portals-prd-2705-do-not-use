import { IncomeListItem } from '../additional-income/income-list-item.po';

export class PrimaryPhysicianItem extends IncomeListItem {
  static readonly el: string = '[data-test-el="primary-physician-item"]';
  private titleSelector = '[data-test-el="medical-provider-title"]';
  private descriptionSelector = '[data-test-el="medical-provider-description"]';

  constructor() {
    super(PrimaryPhysicianItem.el);
  }

  get title() {
    return cy.get(`${this.el} ${this.titleSelector}`);
  }

  get description() {
    return cy.get(`${this.el} ${this.descriptionSelector}`);
  }
}
