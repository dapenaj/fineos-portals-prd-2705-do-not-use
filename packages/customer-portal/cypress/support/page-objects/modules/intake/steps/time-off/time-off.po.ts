import { TimeOffPeriodType } from '../../../../../../../src/app/shared/types/time-off.type';

import { LeavePeriodForm } from './leave-period-form.po';
import { TimeOffForm } from './time-off-form.po';

export class TimeOff {
  leavePeriodForm: LeavePeriodForm = new LeavePeriodForm();
  timeOffForm: TimeOffForm = new TimeOffForm();

  constructor(public elem: string = '[data-test-el="time-off"]') {}

  addTimeOffPeriod(asAnother?: boolean) {
    this.openForm(asAnother);
    this.leavePeriodForm.fillForm(TimeOffPeriodType.TIME_OFF);
    cy.get(this.leavePeriodForm.elem).within(() => {
      this.leavePeriodForm.submitBtn.click();
    });
  }

  addReducedSchedulePeriod(asAnother?: boolean) {
    this.openForm(asAnother);
    this.leavePeriodForm.fillForm(TimeOffPeriodType.REDUCED_SCHEDULE);
    cy.get(this.leavePeriodForm.elem).within(() => {
      this.leavePeriodForm.submitBtn.click();
    });
  }

  addEpisodicPeriod(asAnother?: boolean) {
    this.openForm(asAnother);
    this.leavePeriodForm.fillForm(TimeOffPeriodType.EPISODIC);
    cy.get(this.leavePeriodForm.elem).within(() => {
      this.leavePeriodForm.submitBtn.click();
    });
  }

  private openForm(asAnother?: boolean) {
    cy.get(this.timeOffForm.elem).within(() => {
      if (asAnother) {
        this.timeOffForm.addAnotherBtn.click();
      } else {
        this.timeOffForm.addBtn.click();
      }
    });
  }
}
