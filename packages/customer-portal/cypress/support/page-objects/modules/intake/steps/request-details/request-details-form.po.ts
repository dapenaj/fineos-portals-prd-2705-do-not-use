export class RequestDetailsForm {
  private titleSelector = 'h3';
  private fieldsSelector = '[data-test-el="request-field"]';
  private labelSelector = '[data-test-el="form-label"]';
  private submitSelector = 'button[type="submit"]';
  private radioSelector = 'input[type="radio"]';
  private inputSelector = 'input';
  private textareaSelector = 'textarea';

  constructor(public elem: string = 'form[name="request-details"]') {}

  get title() {
    return cy.get(this.titleSelector);
  }

  get fields() {
    return cy.get(this.fieldsSelector);
  }

  get submitBtn() {
    return cy.get(this.submitSelector);
  }

  getLabelAt(index: number) {
    return cy.get(this.labelSelector).eq(index);
  }

  checkRadioForField(field: Cypress.Chainable, index: number) {
    return field
      .find(this.radioSelector)
      .eq(index)
      .check({ force: true });
  }

  setDateForField(field: Cypress.Chainable) {
    field
      .find(this.inputSelector)
      .parent()
      .click();
    // "clicking" a date in the calendar no longer works
    // typing a return character in the input will auto-select today
    return field
      .parentsUntil('html')
      .find('.ant-calendar-input')
      .type('{enter}');
  }

  fillForm() {
    this.checkRadioForField(this.fields.eq(0), 0);
    this.setDateForField(this.fields.eq(1));
    this.fields
      .eq(2)
      .find(this.inputSelector)
      .clear({ force: true })
      .type('1');
    this.checkRadioForField(this.fields.eq(3), 0);
    this.checkRadioForField(this.fields.eq(4), 0);
    this.fields
      .eq(5)
      .find(this.textareaSelector)
      .clear({ force: true })
      .type('additional text');
  }
}
