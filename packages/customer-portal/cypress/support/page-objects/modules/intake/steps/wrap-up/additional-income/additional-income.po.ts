import { EmptyListPlaceholder } from '../../../../../shared';

import { IncomeSourceForm } from './income-source-form.po';
import { IncomeSourceItem } from './income-source-item.po';

export class AdditionalIncome {
  emptyIncomeSource = new EmptyListPlaceholder(
    '[data-test-el="empty-income-source"]'
  );

  incomeSourceForm = new IncomeSourceForm();

  private titleSelector: string = '[data-test-el="additional-income-title"]';
  private submitBtnSelector: string =
    '[data-test-el="payment-preferences-save-btn"]';
  private addFirstBtnSelector: string = 'button[data-test-el="add-new-income"]';
  private addAnotherBtnSelector: string =
    '[data-test-el="add-another-income-source"]';
  private paymentMethod: string = '[data-test-el="payment-method"]';
  private radioSelector = 'input[type="radio"]';
  private fieldsSelector =
    '[data-test-el="payment-preferences-options-form-group"]';
  private eftAccount = '[data-test-el="account-details-modal-btn"]';

  constructor(public elem: string = '[data-test-el="additional-income"]') {}

  checkRadioForField(field: Cypress.Chainable, index: number) {
    return field
      .find(this.radioSelector)
      .eq(index)
      .check({ force: true });
  }

  fillRadio() {
    this.checkRadioForField(this.fields.eq(0), 1);
  }

  get title() {
    return cy.get(this.titleSelector);
  }

  get addFirstBtn() {
    return cy.get(this.addFirstBtnSelector);
  }

  get addAnotherBtn() {
    return cy.get(this.addAnotherBtnSelector);
  }

  get submitBtn() {
    return cy.get(this.submitBtnSelector);
  }

  getIncomeSourceItemAt(index: number): IncomeSourceItem {
    return new IncomeSourceItem(index);
  }

  get incomeSourceItems() {
    return cy.get(IncomeSourceItem.el);
  }

  get paymentMethodLabel() {
    return cy.get(this.paymentMethod);
  }

  get fields() {
    return cy.get(this.fieldsSelector);
  }

  get eftAccountButton() {
    return cy.get(this.eftAccount);
  }
}
