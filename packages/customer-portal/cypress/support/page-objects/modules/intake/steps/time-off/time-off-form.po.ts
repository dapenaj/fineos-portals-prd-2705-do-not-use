export class TimeOffForm {
  private titleSelector = '[data-test-el="time-off-title"]';
  private addBtnSelector = '[data-test-el="add-time-off-btn"]';
  private addAnotherBtnSelector = '[data-test-el="add-another-time-off-btn"]';
  private leavePeriodSelector = '[data-test-el="leave-period-panel"]';
  private submitSelector = 'button[type="submit"]';

  constructor(public elem: string = 'form[name="time-off"]') {}

  get title() {
    return cy.get(this.titleSelector);
  }

  get addBtn() {
    return cy.get(this.addBtnSelector);
  }

  get addAnotherBtn() {
    return cy.get(this.addAnotherBtnSelector);
  }

  get periods() {
    return cy.get(this.leavePeriodSelector);
  }

  get submitBtn() {
    return cy.get(this.submitSelector);
  }
}
