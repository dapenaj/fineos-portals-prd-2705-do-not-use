import { noop as _noop } from 'lodash';

interface FormValue {
  firstName: string;
  lastName: string;
  phoneNumber: { int: string; area: string; tele: string };
  email?: string;
}

export class CustomerDetails {
  private firstNameSelector = 'input[name="firstName"]';
  private lastNameSelector = 'input[name="lastName"]';
  private dobSelector = 'input[name="dateOfBirth"]';
  private intCodeSelector = 'input[name="phoneNumber.intCode"]';
  private areaCodeSelector = 'input[name="phoneNumber.areaCode"]';
  private telephoneNoSelector = 'input[name="phoneNumber.telephoneNo"]';
  private emailSelector = 'input[name="email"]';

  constructor(public elem: string = '[data-test-el="customer-detials"]') {}

  get firstNameInput() {
    return cy.get(this.firstNameSelector);
  }

  get lastNameInput() {
    return cy.get(this.lastNameSelector);
  }

  get dateOfBirthInput() {
    return cy.get(this.dobSelector);
  }

  get intCodeInput() {
    return cy.get(this.intCodeSelector);
  }

  get areaCodeInput() {
    return cy.get(this.areaCodeSelector);
  }

  get telephoneNoInput() {
    return cy.get(this.telephoneNoSelector);
  }

  get emailInput() {
    return cy.get(this.emailSelector);
  }

  clearAndPopulate({
    firstName,
    lastName,
    phoneNumber: { int, area, tele },
    email
  }: FormValue) {
    this.firstNameInput.clear().type(firstName);
    this.lastNameInput.clear().type(lastName);
    this.intCodeInput.clear().type(int);
    this.areaCodeInput.clear().type(area);
    this.telephoneNoInput.clear().type(tele);
    email ? this.emailInput.clear().type(email) : _noop();
  }

  populate({
    firstName,
    lastName,
    phoneNumber: { int, area, tele },
    email
  }: FormValue) {
    this.firstNameInput.type(firstName);
    this.lastNameInput.type(lastName);
    this.intCodeInput.type(int);
    this.areaCodeInput.type(area);
    this.telephoneNoInput.type(tele);
    email ? this.emailInput.type(email) : _noop();
  }
}
