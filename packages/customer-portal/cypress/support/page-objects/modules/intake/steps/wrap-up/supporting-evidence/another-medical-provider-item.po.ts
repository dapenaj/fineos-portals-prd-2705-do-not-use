import { IncomeListItem } from '../additional-income/income-list-item.po';

export class AnotherMedicalProviderItem extends IncomeListItem {
  static readonly el: string = '[data-test-el="medical-provider-item"]';

  private titleSelector = '[data-test-el="medical-provider-title"]';
  private descriptionSelector = '[data-test-el="medical-provider-description"]';

  constructor(index: number) {
    super(`${AnotherMedicalProviderItem.el}:eq(${index})`);
  }

  get title() {
    return cy.get(`${this.el} ${this.titleSelector}`);
  }

  get description() {
    return cy.get(`${this.el} ${this.descriptionSelector}`);
  }
}
