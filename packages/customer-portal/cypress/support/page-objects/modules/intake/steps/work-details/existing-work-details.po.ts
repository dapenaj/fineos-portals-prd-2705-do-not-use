export class ExistingWorkDetails {
  private titleSelector = 'h1';
  private detailsSelector = 'p';
  private requestChangeSelector = '[data-test-el="request-change-btn"]';
  private newOccupationSelector = '[data-test-el="new-occupation-btn"]';
  private proceedSelector = '[data-test-el="work-proceed-btn"]';

  constructor(public elem: string = '[data-test-el="existing-work-details"]') {}

  get title() {
    return cy.get(this.titleSelector);
  }

  get details() {
    return cy.get(this.detailsSelector);
  }

  get requestChangeBtn() {
    return cy.get(this.requestChangeSelector);
  }

  get newOccupationBtn() {
    return cy.get(this.newOccupationSelector);
  }

  get proceedBtn() {
    return cy.get(this.proceedSelector);
  }
}
