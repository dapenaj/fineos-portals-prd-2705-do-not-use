import { CustomerDetails } from './customer-details.po';
import { AddressDetails } from './address-details.po';

export class PersonalDetailsForm {
  customerDetails: CustomerDetails = new CustomerDetails();
  addressDetails: AddressDetails = new AddressDetails();

  private submitSelector = 'button[type="submit"]';

  constructor(public elem: string = 'form[name="personal-details"]') {}

  get submitBtn() {
    return cy.get(this.submitSelector);
  }
}
