import {
  DatePickerField,
  InputField,
  SelectField
} from '../../../../../shared/form';

export interface IncomeSourceFormValues {
  incomeType: string;
  frequency: string;
  amount: string;
  startDate: string;
  endDate: string;
}

export class IncomeSourceForm {
  private incomeTypeSelector = '[data-test-el="incomeType"]';
  private frequencySelector = '[data-test-el="frequency"]';
  private amountSelector = '[data-test-el="amount"]';
  private startDateSelector = '[data-test-el="startDate"]';
  private endDateSelector = '[data-test-el="endDate"]';
  private submitSelector = 'button[type="submit"]';

  constructor(public el: string = 'form[name="income-source"]') {}

  get incomeType(): SelectField {
    return new SelectField(`${this.el} ${this.incomeTypeSelector}`);
  }

  get frequency(): SelectField {
    return new SelectField(`${this.el} ${this.frequencySelector}`);
  }

  get amount(): InputField {
    return new InputField(`${this.el} ${this.amountSelector}`);
  }

  get startDate(): DatePickerField {
    return new DatePickerField(`${this.el} ${this.startDateSelector}`);
  }

  get endDate() {
    return new DatePickerField(`${this.el} ${this.endDateSelector}`);
  }

  get submitBtn() {
    return cy.get(this.submitSelector);
  }

  fillWithValues(
    values: Partial<IncomeSourceFormValues> = {
      incomeType: 'State Disability',
      frequency: 'Monthly',
      amount: '235',
      startDate: '05-06-2019',
      endDate: '06-06-2019'
    }
  ) {
    if (values.incomeType) {
      this.incomeType.setValue(values.incomeType);
    }
    if (values.frequency) {
      this.frequency.setValue(values.frequency);
    }
    if (values.amount) {
      this.amount.setValue(values.amount);
    }
    if (values.startDate) {
      this.startDate.setValue(values.startDate);
    }
    if (values.endDate) {
      this.endDate.setValue(values.endDate);
    }
  }
}
