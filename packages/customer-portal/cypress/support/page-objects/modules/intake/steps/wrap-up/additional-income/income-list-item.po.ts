export class IncomeListItem {
  private editBtnSelector = '[data-test-el="edit-intake-list-item"]';
  private deleteBtnSelector = '[data-test-el="delete-intake-list-item"]';

  constructor(public el: string) {}

  get editBtn() {
    return cy.get(`${this.el} ${this.editBtnSelector}`);
  }

  get deleteBtn() {
    return cy.get(`${this.el} ${this.deleteBtnSelector}`);
  }
}
