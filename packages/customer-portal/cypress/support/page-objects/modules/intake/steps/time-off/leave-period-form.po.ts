import { TimeOffPeriodType } from '../../../../../../../src/app/shared/types/time-off.type';

export class LeavePeriodForm {
  private fieldsSelector = '[data-test-el="leave-period-form-group"]';
  private labelSelector = '.ant-form-item-label';
  private submitSelector = 'button[type="submit"]';
  private radioSelector = 'input[type="radio"]';
  private inputSelector = 'input';
  private selectedDaySelector = '.ant-calendar-today';

  constructor(public elem: string = 'form[name="leave-period"]') {}

  get fields() {
    return cy.get(this.fieldsSelector);
  }

  get submitBtn() {
    return cy.get(this.submitSelector);
  }

  getLabelAt(index: number) {
    return cy.get(this.labelSelector).eq(index);
  }

  checkRadioForField(field: Cypress.Chainable, index: number) {
    return field
      .find(this.radioSelector)
      .eq(index)
      .check({ force: true });
  }

  setDateForField(field: Cypress.Chainable, dateStr: string = 'Dec 09, 2019') {
    field
      .find(this.inputSelector)
      .first()
      .parent()
      .click();
    // "clicking" a date in the calendar no longer works
    // typing a date string and hitting return in the input will select a date
    return field
      .parentsUntil('html')
      .find('.ant-calendar-input')
      .clear()
      .type(dateStr)
      .type('{enter}');
  }

  fillForm(type: TimeOffPeriodType) {
    switch (type) {
      case TimeOffPeriodType.REDUCED_SCHEDULE:
        this.checkRadioForField(this.fields.eq(0), 1);
        this.setDateForField(this.fields.eq(1));
        this.setDateForField(this.fields.eq(2));
        this.checkRadioForField(this.fields.eq(3), 0);
        break;
      case TimeOffPeriodType.EPISODIC:
        this.checkRadioForField(this.fields.eq(0), 2);
        this.setDateForField(this.fields.eq(1));
        this.setDateForField(this.fields.eq(2));
        break;
      default:
        this.setDateForField(this.fields.eq(1));
        this.setDateForField(this.fields.eq(2));
        this.setDateForField(this.fields.eq(3));
        this.setDateForField(this.fields.eq(4));
        this.checkRadioForField(this.fields.eq(5), 0);
        this.checkRadioForField(this.fields.eq(6), 0);
        break;
    }
  }
}
