import { InputField, SelectField } from '../../../../../shared/form';

export interface MedicalProviderFormValues {
  name: string;
  phone: string;
  city: string;
  addressLine: string;
  state: string;
  zipCode: string;
}

export class MedicalProviderForm {
  private addressSectionTitleSelector: string =
    '[data-test-el="address-section-title"]';
  private nameSelector: string = '[data-test-el="name"]';
  private phoneSelector: string = '[data-test-el="phone"]';
  private addressLineSelector: string = '[data-test-el="addressLine"]';
  private citySelector: string = '[data-test-el="city"]';
  private stateSelector: string = '[data-test-el="state"]';
  private zipCodeSelector: string = '[data-test-el="zipCode"]';
  private countrySelector: string = '[data-test-el="country"]';
  private saveBtnSelector: string = 'button[type="submit"]';

  constructor(public el: string = 'form[name="medical-provider"]') {}

  get addressSectionTitle() {
    return cy.get(`${this.el} ${this.addressSectionTitleSelector}`);
  }

  get name() {
    return new InputField(`${this.el} ${this.nameSelector}`);
  }

  get phone() {
    return new InputField(`${this.el} ${this.phoneSelector}`);
  }

  get addressLine() {
    return new InputField(`${this.el} ${this.addressLineSelector}`);
  }

  get city() {
    return new InputField(`${this.el} ${this.citySelector}`);
  }

  get state() {
    return new SelectField(`${this.el} ${this.stateSelector}`);
  }

  get zipCode() {
    return new InputField(`${this.el} ${this.zipCodeSelector}`);
  }

  get country() {
    return new InputField(`${this.el} ${this.countrySelector}`);
  }

  get saveBtn() {
    return cy.get(`${this.el} ${this.saveBtnSelector}`);
  }

  fillWithValues(
    values: Partial<MedicalProviderFormValues> = {
      name: 'Dr Jones',
      phone: '1-234-567890',
      city: 'Austin',
      addressLine: 'Street 1',
      state: 'Texas',
      zipCode: '91021'
    }
  ) {
    if (values.name) {
      this.name.setValue(values.name);
    }
    if (values.phone) {
      this.phone.setValue(values.phone);
    }
    if (values.city) {
      this.city.setValue(values.city);
    }
    if (values.addressLine) {
      this.addressLine.setValue(values.addressLine);
    }
    if (values.state) {
      this.state.setValue(values.state);
    }
    if (values.zipCode) {
      this.zipCode.setValue(values.zipCode);
    }
  }
}
