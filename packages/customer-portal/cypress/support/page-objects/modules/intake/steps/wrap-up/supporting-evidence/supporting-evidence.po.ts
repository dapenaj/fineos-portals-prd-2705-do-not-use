import { EmptyListPlaceholder } from '../../../../../shared';

import { AnotherMedicalProviders } from './another-medical-providers.po';
import { MedicalProviderForm } from './medical-provider-form.po';
import { PrimaryPhysicianItem } from './primary-physician-item.po';
import { AnotherMedicalProviderItem } from './another-medical-provider-item.po';

export class SupportingEvidence {
  anotherMedicalProviders: AnotherMedicalProviders = new AnotherMedicalProviders();
  medicalProviderForm: MedicalProviderForm = new MedicalProviderForm();

  primaryPhysicianItem: PrimaryPhysicianItem = new PrimaryPhysicianItem();

  primaryPhysicianPlaceholder = new EmptyListPlaceholder(
    '[data-test-el="primary-physician-placeholder"]'
  );
  anotherMedicalProviderPlaceholder = new EmptyListPlaceholder(
    '[data-test-el="another-medical-provider-placeholder"]'
  );

  private descriptionSelector: string =
    '[data-test-el="supporting-evidence-description"]';
  private primaryPhysicianSectionTitleSelector: string =
    '[data-test-el="supporting-evidence-primary-section-title"]';
  private anotherMedicalProvidersSectionTitleSelector: string =
    '[data-test-el="supporting-evidence-another-section-title"]';
  private saveAndProceedBtnSelector: string =
    '[data-test-el="save-and-proceed-supporting-evidence"]';
  private addAnotherMedicalProviderBtnSelector: string =
    '[data-test-el="add-one-more-another-medical-care"]';

  constructor(public elem: string = '[data-test-el="supporting-evidence"]') {}

  get description() {
    return cy.get(this.descriptionSelector);
  }

  get primaryPhysicianSectionTitle() {
    return cy.get(this.primaryPhysicianSectionTitleSelector);
  }

  get anotherMedicalProvidersSectionTitle() {
    return cy.get(this.anotherMedicalProvidersSectionTitleSelector);
  }

  get saveAndProceedBtn() {
    return cy.get(this.saveAndProceedBtnSelector);
  }

  get addAnotherMedicalProviderBtn() {
    return cy.get(this.addAnotherMedicalProviderBtnSelector);
  }

  get anotherMedicalProvideItems() {
    return cy.get(AnotherMedicalProviderItem.el);
  }

  getAnotherMedicalProviderItemAt(index: number): AnotherMedicalProviderItem {
    return new AnotherMedicalProviderItem(index);
  }
}
