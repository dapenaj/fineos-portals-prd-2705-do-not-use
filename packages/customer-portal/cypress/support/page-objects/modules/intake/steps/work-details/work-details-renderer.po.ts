import { ExistingWorkDetails } from './existing-work-details.po';
import { WorkDetailsForm } from './work-details-form.po';

export class WorkDetailsRenderer {
  existingWorkDetails: ExistingWorkDetails = new ExistingWorkDetails();
  workDetailsForm: WorkDetailsForm = new WorkDetailsForm();

  private titleSelector = 'h3';

  constructor(public elem: string = '[data-test-el="work-details-renderer"]') {}

  get title() {
    return cy.get(this.titleSelector);
  }
}
