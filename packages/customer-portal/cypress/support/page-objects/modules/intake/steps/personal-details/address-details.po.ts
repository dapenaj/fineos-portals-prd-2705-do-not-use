import { noop as _noop } from 'lodash';

import { searchSelectOption } from '../../../../../utils';

interface FormValue {
  addressLine1: string;
  addressLine2?: string;
  addressLine3?: string;
  city: string;
  state: { search: string; option: string };
  postCode: string;
}

export class AddressDetails {
  private addressLine1Selector = 'input[name="addressLine1"]';
  private addressLine2Selector = 'input[name="addressLine2"]';
  private addressLine3Selector = 'input[name="addressLine3"]';
  private citySelector = 'input[name="city"]';
  private stateSelector = '[data-test-el="form-select"]';
  private postCodeSelector = 'input[name="postCode"]';

  constructor(public elem: string = '[data-test-el="address-detials"]') {}

  get addressLine1Input() {
    return cy.get(this.addressLine1Selector);
  }

  get addressLine2Input() {
    return cy.get(this.addressLine2Selector);
  }

  get addressLine3Input() {
    return cy.get(this.addressLine3Selector);
  }

  get cityInput() {
    return cy.get(this.citySelector);
  }

  get state() {
    return cy.get(this.stateSelector);
  }

  get stateSearchInput() {
    return cy.get('.ant-select-search__field');
  }

  get postCodeInput() {
    return cy.get(this.postCodeSelector);
  }

  clearAndPopulate({
    addressLine1,
    addressLine2,
    addressLine3,
    city,
    state,
    postCode
  }: FormValue) {
    this.addressLine1Input.clear().type(addressLine1);
    addressLine2 ? this.addressLine2Input.clear().type(addressLine2) : _noop();
    addressLine3 ? this.addressLine3Input.clear().type(addressLine3) : _noop();
    this.cityInput.clear().type(city);
    searchSelectOption(this.stateSearchInput, state.search, state.option);
    this.postCodeInput.clear().type(postCode);
  }
}
