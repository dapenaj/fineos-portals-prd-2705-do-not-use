interface FormValue {
  employer: string;
  jobTitle: string;
}

export class WorkDetailsForm {
  private submitSelector = 'button[type="submit"]';
  private employerSelector = 'input[name="employer"]';
  private jobTitleSelector = 'input[name="jobTitle"]';
  private titleSelector = 'p';
  private cancelSelector = '[data-test-el="step-cancel"]';

  constructor(public elem: string = 'form[name="work-details"]') {}

  get title() {
    return cy.get(this.titleSelector);
  }

  get submitBtn() {
    return cy.get(this.submitSelector);
  }

  get cancelBtn() {
    return cy.get(this.cancelSelector);
  }

  get employerInput() {
    return cy.get(this.employerSelector);
  }

  get jobTitleInput() {
    return cy.get(this.jobTitleSelector);
  }

  clearAndPopulate({ employer, jobTitle }: FormValue) {
    this.employerInput.clear().type(employer);
    this.jobTitleInput.clear().type(jobTitle);
  }
}
