import { IntakeOverview } from './overview';
import { YourRequest, IntakeDetail } from './sections';
import { WrapUp } from './sections/wrap-up.po';

export class IntakePage {
  overview: IntakeOverview = new IntakeOverview();
  yourRequest: YourRequest = new YourRequest();
  detail: IntakeDetail = new IntakeDetail();
  wrapUp: WrapUp = new WrapUp();

  constructor(public elem: string = '[data-test-el="intake-view"]') {}

  navigateTo() {
    cy.visit('/#intake');
    cy.wait(300);
  }
}
