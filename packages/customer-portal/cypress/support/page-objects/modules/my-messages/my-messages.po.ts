import { EmptyList } from '../../shared';

export class MyMessagesPage {
  empty = new EmptyList();

  private headerSelector = '[data-test-el="messages-header"]';
  private searchSelector = '[data-test-el="messages-search-wrapper"]';
  private panelsSelector = '[data-test-el="messages-panels-wrapper"]';
  private searchInputSelector = 'input[name="search"]';
  private messagesListSelector = '[data-test-el="messages-list"]';
  private actionsSelector = '[data-test-el="header-switch"]';
  private iconCloseBadge = '[data-test-el="icon-search-badge"]';
  private newMessageSelector = '[data-test-el="new-message-button"]';

  constructor(public elem: string = '[data-test-el="messages-wrapper"]') {}

  navigateTo() {
    cy.visit('/#my-messages');
    cy.wait(300);
  }

  get messagesHeader() {
    return cy.get(this.headerSelector);
  }

  get search() {
    return cy.get(this.searchSelector);
  }

  get searchInput() {
    return cy.get(this.searchInputSelector);
  }

  get panels() {
    return cy.get(this.panelsSelector);
  }

  get messageList() {
    return cy.get(this.messagesListSelector);
  }

  get newMessage() {
    return cy.get(this.newMessageSelector);
  }

  get actions() {
    return cy.get(this.actionsSelector);
  }

  get getIconCloseBadge() {
    return cy.get(this.iconCloseBadge);
  }

  searchMessages(search: string) {
    this.searchInput.type(search).type('{enter}');
  }
}
