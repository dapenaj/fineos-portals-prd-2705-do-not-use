export class MyDocuments {
  private headerSelector = '[data-test-el="my-documents-header"]';
  private typeSelector = '[data-test-el="download-document-btn"]';
  private dateSelector = '[data-test-el="document-date"]';

  constructor(public elem: string = '[data-test-el="my-documents"]') {}

  get header() {
    return cy.get(this.headerSelector);
  }

  getTypeAt(index: number) {
    return cy.get(this.typeSelector).eq(index);
  }

  getDateAt(index: number) {
    return cy.get(this.dateSelector).eq(index);
  }
}
