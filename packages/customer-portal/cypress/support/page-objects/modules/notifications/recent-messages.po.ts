export class RecentMessages {
  private headerSelector = '[data-test-el="recent-messages-header"]';
  private messagesSelector = '[data-test-el="recent-messages-list"]';
  private newMessageSelector = '[data-test-el="new-message-button"]';
  private messageModalSelector = '[data-test-el="message-modal"]';

  constructor(public elem: string = '[data-test-el="recent-messages"]') {}

  get header() {
    return cy.get(this.headerSelector);
  }

  get messagesList() {
    return cy.get(this.messagesSelector);
  }

  get newMessage() {
    return cy.get(this.newMessageSelector);
  }

  get messageModal() {
    return cy.get(this.messageModalSelector);
  }

  openMessage(index: number) {
    cy.get(this.messagesSelector)
      .get('[data-test-el="recent-messages-list-element"]')
      .eq(index)
      .click();
  }
}
