export class RequiredFromMe {
  private headerSelector = '[data-test-el="required-from-me-header"]';
  private listSelector = '[data-test-el="required-from-me-list"]';
  private nameSelector = '[data-test-el="required-from-me-doc-name"]';
  private confirmReturnBtnSelector =
    '[data-test-el="required-from-me-return-to-work-btn"]';

  constructor(
    public elem: string = '[data-test-el="notification-detail-view"]'
  ) {}

  get header() {
    return cy.get(this.headerSelector);
  }

  get list() {
    return cy.get(this.listSelector);
  }

  get name() {
    return cy.get(this.nameSelector);
  }

  get confirmReturnBtn() {
    return cy.get(this.confirmReturnBtnSelector);
  }
}
