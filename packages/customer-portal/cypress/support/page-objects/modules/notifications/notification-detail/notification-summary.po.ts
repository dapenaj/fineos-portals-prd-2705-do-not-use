import { Collapse } from '../../../shared/collapse.po';
import { PropertyItem } from '../../../shared/property-item.po';

export class NotificationSummary {
  collapse = new Collapse(this.elem);
  properties = new PropertyItem();

  constructor(public elem: string = '[data-test-el="notification-summary"]') {}
}
