export class PaymentHistory {
  private headerSelector = '[data-test-el="notification-payment-header"]';
  private paymentSelector = '[data-test-el="payment"]';

  constructor(public elem: string = '[data-test-el="payment-history]') {}

  get paymentHeader() {
    return cy.get(this.headerSelector);
  }

  get payment() {
    return cy.get(this.paymentSelector);
  }
}
