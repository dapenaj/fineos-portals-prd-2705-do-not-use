import { BenefitHeader } from './benefit-header.po';
import { Benefit } from './benefit.po';

export class Benefits {
  benefitHeader = new BenefitHeader();
  benefit = new Benefit();

  constructor(public elem: string = '[data-test-el="benefits"]') {}
}
