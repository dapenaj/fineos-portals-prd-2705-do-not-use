import { NotificationSummary } from './notification-summary.po';
import { JobProtectedLeave } from './job-protected-leave';
import { WageReplacement, PaymentHistory } from './wage-replacement';
import { NotificationTimeline } from './notification-timeline.po';

export class NotificationDetail {
  summary: NotificationSummary = new NotificationSummary();
  timeline: NotificationTimeline = new NotificationTimeline();
  jobProtectedLeave: JobProtectedLeave = new JobProtectedLeave();
  wageReplacement: WageReplacement = new WageReplacement();
  paymentHistory: PaymentHistory = new PaymentHistory();

  private confirmationModalSelector = '.request-amendment-confirmation-modal';

  constructor(
    public elem: string = '[data-test-el="notification-detail-view"]'
  ) {}

  navigateTo(id: string = 'NTN-21') {
    cy.visit(`/#notifications/${id}`);
    cy.wait(300);
  }

  get confirmationModal() {
    return cy.get(this.confirmationModalSelector);
  }
}
