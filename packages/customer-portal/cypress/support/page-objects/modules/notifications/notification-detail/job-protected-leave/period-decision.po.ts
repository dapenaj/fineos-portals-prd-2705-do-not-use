import { PropertyItem } from '../../../../shared';

export class PeriodDecision {
  properties = new PropertyItem();

  private statusSelector = '[data-test-el="period-decision-status-icon"]';

  constructor(public elem: string = '[data-test-el="period-decision-line"]') {}

  getPeriodDecisionAt(index: number) {
    return cy.get(this.elem).eq(index);
  }

  getStatusAt(index: number) {
    return cy.get(this.statusSelector).eq(index);
  }
}
