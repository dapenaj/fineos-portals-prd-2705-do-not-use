import { PeriodDecisionHeader } from './period-decision-header.po';
import { PeriodDecision } from './period-decision.po';

export class JobProtectedLeavePlan {
  periodDecisionHeaders = new PeriodDecisionHeader();
  periodDecisions = new PeriodDecision();

  constructor(
    public elem: string = '[data-test-el="job-protected-leave-plans"]'
  ) {}
}
