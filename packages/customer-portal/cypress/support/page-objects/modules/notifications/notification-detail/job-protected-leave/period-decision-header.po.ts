import { PropertyItem } from '../../../../shared';

export class PeriodDecisionHeader {
  properties = new PropertyItem();

  private nameSelector = '[data-test-el="period-decision-header-name"]';
  private statusSelector = '[data-test-el="period-decision-status-icon"]';

  constructor(public elem: string = '.ant-collapse-item') {}

  getNameAt(index: number) {
    return cy
      .get(this.elem)
      .eq(index)
      .get(this.nameSelector);
  }

  toggleAt(index: number) {
    cy.get(`${this.elem} [role="button"]`)
      .eq(index)
      .click();
  }

  getStatusAt(index: number, statusIndex: number) {
    return cy
      .get(this.elem)
      .eq(index)
      .get(this.statusSelector)
      .eq(statusIndex);
  }

  getPeriodDecisionHeaderAt(index: number) {
    return cy.get(this.elem).eq(index);
  }
}
