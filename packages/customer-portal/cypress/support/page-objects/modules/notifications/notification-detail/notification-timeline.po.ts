import { Collapse } from '../../../shared/collapse.po';

export class NotificationTimeline {
  collapse = new Collapse(this.elem);

  constructor(public elem: string = '[data-test-el="notification-timeline"]') {}
}
