import { Benefits } from './benefits.po';

export class WageReplacement {
  benefits = new Benefits();

  private headerSelector = '[data-test-el="wage-replacement-header"]';

  constructor(public elem: string = '[data-test-el="wage-replacement"]') {}

  get header() {
    return cy.get(this.headerSelector);
  }
}
