import { JobProtectedLeavePlan } from './job-protected-leave-plan.po';

export class JobProtectedLeave {
  jobProtectedLeavePlan = new JobProtectedLeavePlan();

  private headerSelector = '[data-test-el="job-protected-leave-header"]';
  private legendItemSelector =
    '[data-test-el="job-protected-leave-legend-item"]';

  constructor(public elem: string = '[data-test-el="job-protected-leave"]') {}

  get header() {
    return cy.get(this.headerSelector);
  }

  getLegendItemAt(index: number) {
    return cy.get(this.legendItemSelector).eq(index);
  }
}
