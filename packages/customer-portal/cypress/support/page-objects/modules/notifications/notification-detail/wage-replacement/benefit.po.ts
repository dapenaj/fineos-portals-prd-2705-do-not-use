import { PropertyItem } from '../../../../shared';

export class Benefit {
  properties = new PropertyItem();

  private statusSelector = '[data-test-el="wage-replacement-status-icon"]';

  constructor(
    public elem: string = '[data-test-el="wage-replacement-properties"]'
  ) {}

  getWageReplacementAt(index: number) {
    return cy.get(this.elem).eq(index);
  }

  getStatusAt(index: number) {
    return cy.get(this.statusSelector).eq(index);
  }
}
