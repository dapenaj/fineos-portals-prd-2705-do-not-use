export class RequestAmendmentForm {
  private amendPeriodFormContainer =
    '[data-test-el="amend-period-form-container"]';

  constructor(public elem: string = '[data-test-el="amended-time-off-form"]') {}

  get amendFormContainer() {
    return cy.get(this.amendPeriodFormContainer);
  }
}
