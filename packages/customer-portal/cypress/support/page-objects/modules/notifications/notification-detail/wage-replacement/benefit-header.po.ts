import { PropertyItem } from '../../../../shared';

export class BenefitHeader {
  properties = new PropertyItem();

  private nameSelector = '[data-test-el="disability-benefit-header-name"]';
  private statusSelector = '[data-test-el="wage-replacement-status-icon"]';

  constructor(public elem: string = '.ant-collapse-item') {}

  get name() {
    return cy.get(this.nameSelector);
  }

  toggleAt(index: number) {
    cy.get(`${this.elem} [role="button"]`)
      .eq(index)
      .click();
  }

  getStatusAt(index: number) {
    return cy.get(this.statusSelector).eq(index);
  }
}
