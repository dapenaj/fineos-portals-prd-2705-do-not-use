export class MyNotificationsPage {
  private headerSelector = '[data-test-el="notifications-header"]';
  private actionBtnSelector = '[data-test-el="notification-button"]';
  private panelsSelector = '[data-test-el="notifications-panels"]';
  private actionsSelector = '[data-test-el="header-switch"]';
  private notificationCaseId = '[data-test-el="notification-case-id"]';

  constructor(public elem: string = '[data-test-el="notification-wrapper"]') {}

  get action() {
    return cy.get(this.actionBtnSelector);
  }

  get notificationsHeader() {
    return cy.get(this.headerSelector);
  }

  get notificationsPanels() {
    return cy.get(this.panelsSelector);
  }

  get notificationsActions() {
    return cy.get(this.actionsSelector);
  }

  get notificationCaseIds() {
    return cy.get(this.notificationCaseId);
  }

  navigateTo() {
    cy.visit('/#notifications');
    cy.wait(300);
  }
}
