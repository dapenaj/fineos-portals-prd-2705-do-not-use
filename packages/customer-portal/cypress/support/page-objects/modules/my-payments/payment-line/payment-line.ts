import { PropertyItem } from '../../../shared/property-item.po';

export class PaymentLine {
  properties = new PropertyItem();

  private paymentLineBtnSelector = '[data-test-el="payment-line-btn"]';

  constructor(public elem: string = '[data-test-el="payment-line"]') {}

  getPaymentLineBtn(index: number) {
    return cy.get(this.paymentLineBtnSelector).eq(index);
  }
}
