import { PropertyItem } from '../../../shared/property-item.po';

export class PaymentDetails {
  properties = new PropertyItem();

  private showBtnSelector = '[data-test-el="show-payment-breakdown"]';
  private paymentBreakdownDetailsSelector =
    '[data-test-el="payment-breakdown-details"]';

  constructor(public elem: string = '[data-test-el="payment-breakdown"]') {}

  get showBreakdownBtn() {
    return cy.get(this.showBtnSelector);
  }

  get breakdownDetails() {
    return cy.get(this.paymentBreakdownDetailsSelector);
  }
}
