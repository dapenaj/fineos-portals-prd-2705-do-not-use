import { PaymentDetails } from './payment-details';
import { PaymentLine } from './payment-line';

export class MyPayments {
  paymentDetails: PaymentDetails = new PaymentDetails();
  paymentLines: PaymentLine = new PaymentLine();

  private myPaymentsHeaderSelector = '[data-test-el="page-title"]';
  private paymentPanelSelector = '[data-test-el="payment"]';
  private myPaymentsListSelector = '[data-test-el="my-payments-list"]';

  constructor(public elem: string = '[data-test-el="my-payments"]') {}

  get paymentHeader() {
    return cy.get(this.myPaymentsHeaderSelector);
  }

  get paymentsList() {
    return cy.get(this.myPaymentsListSelector);
  }

  getPaymentPanel(index: number) {
    return cy.get(this.paymentPanelSelector).eq(index);
  }

  navigateTo() {
    cy.visit(`/#my-payments`);
    cy.wait(300);
  }
}
