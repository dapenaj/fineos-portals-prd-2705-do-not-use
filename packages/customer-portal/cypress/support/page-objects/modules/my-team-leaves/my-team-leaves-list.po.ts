import { PropertyItem } from '../../shared';

export class MyTeamLeavesList {
  properties = new PropertyItem();

  private myTeamLeaveSelector = '[role="tab"]';
  private leavePeriodSelector = '[data-test-el="leave-period-item"]';
  private leavePeriodActionsSelector = '[data-test-el="leave-period-actions"]';
  private deletePeriodSelector = '[data-test-el="request-deletion"]';
  private requestAmendmentFormSelector =
    '[data-test-el="amend-period-form-container"]';
  private confirmationModalSelector = '.request-amendment-confirmation-modal';

  constructor(public elem: string = '[data-test-el="my-team-leaves-list"]') {}

  get myTeamLeaves() {
    return cy.get(this.myTeamLeaveSelector);
  }

  get leavePeriods() {
    return cy.get(this.leavePeriodSelector);
  }

  get leavePeriodActions() {
    return cy.get(this.leavePeriodActionsSelector);
  }

  get deletePeriod() {
    return cy.get(this.deletePeriodSelector);
  }

  get requestAmendmentForm() {
    return cy.get(this.requestAmendmentFormSelector);
  }

  get confirmationModal() {
    return cy.get(this.confirmationModalSelector);
  }

  selectMyTeamLeaveAt(index: number) {
    cy.get(this.myTeamLeaveSelector)
      .eq(index)
      .click();
  }

  selectLeavePeriodAt(index: number) {
    cy.get(this.leavePeriodSelector).eq(index);
  }
}
