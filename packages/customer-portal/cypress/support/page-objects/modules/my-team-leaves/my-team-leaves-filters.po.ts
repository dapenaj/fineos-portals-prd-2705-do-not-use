export class MyTeamLeavesFilters {
  private filtersBtnSelector = '[data-test-el="my-team-leaves-filters-btn"]';
  private filtersMenuSelector = '[data-test-el="my-team-leaves-filters-menu"]';
  private filterItemSelector = '[role="menuitem"]';

  constructor(
    public elem: string = '[data-test-el="my-team-leaves-filters"]'
  ) {}

  get filtersBtn() {
    return cy.get(this.filtersBtnSelector);
  }

  get filtersMenu() {
    return cy.get(this.filtersMenuSelector);
  }

  get filterMenuItems() {
    return cy.get(this.filterItemSelector);
  }

  selectFilterAt(index: number) {
    return cy
      .get(this.filterItemSelector)
      .eq(index)
      .click({ force: true });
  }
}
