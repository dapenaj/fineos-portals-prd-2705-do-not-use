import { MyTeamLeavesHeader } from './my-team-leaves-header.po';
import { MyTeamLeavesFilters } from './my-team-leaves-filters.po';
import { MyTeamLeavesList } from './my-team-leaves-list.po';

export class MyTeamLeaves {
  header = new MyTeamLeavesHeader();
  filters = new MyTeamLeavesFilters();
  list = new MyTeamLeavesList();

  constructor(public elem: string = '[data-test-el="my-team-leaves"]') {}

  navigateTo() {
    cy.visit('/#my-team-leaves');
    cy.wait(300);
  }
}
