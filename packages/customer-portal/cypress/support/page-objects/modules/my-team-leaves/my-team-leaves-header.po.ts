import { SelectField } from '../../shared/form';

export class MyTeamLeavesHeader {
  private titleSelector = '[data-test-el="page-title"]';
  private exportSelector = '[data-test-el="my-team-leaves-export-btn"]';
  private employeeSelector = '[data-test-el="my-team-leaves-search"]';

  constructor(public elem: string = '[data-test-el="my-team-leaves-header"]') {}

  get title() {
    return cy.get(this.titleSelector);
  }

  get exportBtn() {
    return cy.get(this.exportSelector);
  }

  get employeeSelect(): SelectField {
    return new SelectField(this.employeeSelector);
  }

  get employee() {
    return cy.get(this.employeeSelector);
  }
}
