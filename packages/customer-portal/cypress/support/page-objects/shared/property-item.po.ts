export class PropertyItem {
  private propertyItemLabelSelector = '[data-test-el="property-item-label"]';
  private propertyItemValueSelector = '[data-test-el="property-item-value"]';

  hasLabel(index: number, label: string) {
    return cy
      .get(this.propertyItemLabelSelector)
      .eq(index)
      .should('have.text', label);
  }

  hasValue(index: number, value: string) {
    return cy
      .get(this.propertyItemValueSelector)
      .eq(index)
      .should('have.text', value);
  }
}
