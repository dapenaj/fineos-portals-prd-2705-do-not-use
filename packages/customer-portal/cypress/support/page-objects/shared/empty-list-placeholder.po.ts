export class EmptyListPlaceholder {
  private iconSelector: string = '[data-test-el="empty-list-placeholder-icon"]';
  private contentSelector: string =
    '[data-test-el="empty-list-placeholder-content"]';
  private buttonSelector: string =
    '[data-test-el="empty-list-placeholder-button"] button';

  constructor(public elem: string) {}

  get icon() {
    return cy.get(`${this.elem} ${this.iconSelector}`);
  }

  get content() {
    return cy.get(`${this.elem} ${this.contentSelector}`);
  }

  get button() {
    return cy.get(`${this.elem} ${this.buttonSelector}`);
  }
}
