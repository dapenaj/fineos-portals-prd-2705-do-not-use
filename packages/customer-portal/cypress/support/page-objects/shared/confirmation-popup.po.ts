export class ConfirmationPopup {
  private yesBtnSelector: string = 'button:contains("Yes")';

  constructor(public el: string = '.ant-popover[class*="confirm"]') {}

  get yesBtn() {
    return cy.get(`${this.el} ${this.yesBtnSelector}`);
  }
}
