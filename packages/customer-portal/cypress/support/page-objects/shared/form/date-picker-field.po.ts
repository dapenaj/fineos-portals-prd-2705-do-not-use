export class DatePickerField {
  private labelSelector = 'label';
  private inlineInputSelector = 'input';
  private containerSelector = '.ant-calendar-picker-container';

  constructor(public el: string) {}

  get label() {
    return cy.get(`${this.el} ${this.labelSelector}`);
  }

  setValue(dateStr: string) {
    // force click cause here input used only internally by datepciker component
    cy.get(`${this.el} ${this.inlineInputSelector}`).click({ force: true });
    cy.get(`${this.containerSelector} ${this.inlineInputSelector}`).type(
      dateStr
    );
    // close date picker
    cy.get('body').click();
  }
}
