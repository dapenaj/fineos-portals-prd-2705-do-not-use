export class SelectField {
  private labelSelector = 'label';
  private inlineInputSelector = 'input';

  private containerSelector =
    '.ant-select-dropdown:not(.ant-select-dropdown-hidden)';
  private itemSelector = 'li[role="option"]';

  constructor(public el: string) {}

  get label() {
    return cy.get(`${this.el} ${this.labelSelector}`);
  }

  setValue(itemText: string) {
    cy.get(`${this.el} ${this.inlineInputSelector}`).click({ force: true });
    cy.get(
      `${this.containerSelector} ${this.itemSelector}:contains('${itemText}')`
    )
      .first()
      .click();
  }
}
