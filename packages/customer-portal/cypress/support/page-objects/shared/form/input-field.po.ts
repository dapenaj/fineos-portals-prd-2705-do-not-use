export class InputField {
  private labelSelector = 'label';
  private inlineInputSelector = 'input';

  constructor(public el: string) {}

  get label() {
    return cy.get(`${this.el} ${this.labelSelector}`);
  }

  get input() {
    return cy.get(`${this.el} ${this.inlineInputSelector}`);
  }

  setValue(value: string) {
    this.input.clear().type(value);
  }
}
