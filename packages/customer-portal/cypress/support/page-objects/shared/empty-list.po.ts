export class EmptyList {
  private emptyListSelector = '[data-test-el="empty-list"]';

  get emptyList() {
    return cy.get(this.emptyListSelector);
  }
}
