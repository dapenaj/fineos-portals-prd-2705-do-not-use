export class Collapse {
  private headerSelector = '[role="button"]';

  constructor(public elem: string) {}

  get header() {
    return cy.get(this.headerSelector);
  }

  toggle() {
    cy.get(this.headerSelector).click();
  }
}
