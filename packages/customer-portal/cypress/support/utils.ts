export const searchSelectOption = (
  elem: Cypress.Chainable<JQuery<HTMLElement>>,
  search: string,
  option: string
) => {
  elem
    .first()
    .click({ force: true })
    .type(search, { force: true })
    .get('li[role="option"]')
    .contains(option)
    .click({ force: true });
};

export const clickSelectOption = (
  elem: Cypress.Chainable<JQuery<HTMLElement>>,
  option: string
) => {
  elem
    .first()
    .click({ force: true })
    .get('li[role="option"]')
    .contains(option)
    .click();
};
