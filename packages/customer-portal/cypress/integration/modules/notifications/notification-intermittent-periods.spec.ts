import { PropertyItem } from '../../../support/page-objects/shared';
import { NotificationDetail } from '../../../support/page-objects/modules/notifications';

describe('Notification Detail Page - Intermittent Periods', () => {
  const page = new NotificationDetail();
  const jobProtectedLeave = page.jobProtectedLeave;
  const jobProtectedLeavePlan = jobProtectedLeave.jobProtectedLeavePlan;

  it('should display job protected leave for intermittent periods', () => {
    const intermittentPeriodDecisionProperties = [
      { label: 'Absence reason', value: 'Care for a Family Member' },
      { label: 'Approved between', value: 'Aug 05, 2019 - Aug 24, 2019' },
      {
        label: 'Frequency and duration',
        value: '5 episodes of 10 Hours every 2 Months'
      }
    ];

    page.navigateTo('NTN-4');
    cy.url().should('include', '/#notifications/NTN-4');

    // check the job protected leave plans/decisions
    cy.get(jobProtectedLeave.elem).within(() => {
      jobProtectedLeave.header.should('be', 'Job-protected Leave');
    });

    cy.get(jobProtectedLeavePlan.elem).within(() => {
      cy.get(jobProtectedLeavePlan.periodDecisionHeaders.elem).should(
        'have.length',
        1
      );

      jobProtectedLeavePlan.periodDecisionHeaders
        .getNameAt(0)
        .should('be', 'My Child Bonding Plan');
      jobProtectedLeavePlan.periodDecisionHeaders
        .getStatusAt(0, 0)
        .should('be', 'Approved (2)');

      jobProtectedLeavePlan.periodDecisionHeaders.toggleAt(0);
    });

    cy.wait(2000);

    cy.get(jobProtectedLeavePlan.elem).within(() => {
      cy.get('[data-test-el="period-decision-line"]')
        .eq(1)
        .within(() => {
          const propertyItems = new PropertyItem();
          intermittentPeriodDecisionProperties.map(
            ({ label, value }, index) => {
              propertyItems.hasLabel(index, label);
              propertyItems.hasValue(index, value);
            }
          );
        });

      // check table is shown
      const tableRows = cy.get(
        '[data-test-el="intermittent-time-report-table-rows"]'
      );
      tableRows.should('have.length', 4);

      const rowValues = ['Aug 13, 2019', 'Aug 13, 2019', '5', 'Declined'];
      tableRows
        .eq(0)
        .children()
        .each((el, index) => {
          cy.wrap(el.text()).should('equal', rowValues[index]);
        });
    });
  });
});
