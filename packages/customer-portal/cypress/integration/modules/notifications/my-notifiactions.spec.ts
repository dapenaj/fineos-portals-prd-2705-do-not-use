import { MyNotificationsPage } from '../../../support/page-objects/modules/notifications';

describe('My notifications Page', () => {
  let myNotifications: MyNotificationsPage;
  const openCases = [
    { label: 'NTN-21' },
    { label: 'NTN-22' },
    { label: 'NTN-23' },
    { label: 'NTN-25' },
    { label: 'NTN-4' },
    { label: 'NTN-26' },
    { label: 'NTN-27' },
    { label: 'NTN-28' },
    { label: 'NTN-873' }
  ];

  beforeEach(() => {
    myNotifications = new MyNotificationsPage();
    myNotifications.navigateTo();
  });

  it('should display header', () => {
    cy.get(myNotifications.elem).within(() => {
      cy.url().should('be', '/notifications');

      myNotifications.notificationsHeader.should(
        'have.text',
        `My Notifications`
      );
    });
  });

  it('should display action button', () => {
    cy.get(myNotifications.elem).within(() => {
      myNotifications.action.should('have.text', `Make a notification`);

      myNotifications.action.click();
      cy.url().should('include', '/#intake');
    });
  });

  it('should display all notifications', () => {
    cy.get(myNotifications.elem).within(() => {
      myNotifications.notificationsPanels.should($notifications => {
        expect($notifications.children()).to.have.length(9);
      });
    });
  });

  it('should display only open notifications', () => {
    cy.get(myNotifications.elem).within(() => {
      myNotifications.notificationsActions.click();

      myNotifications.notificationCaseIds.should($notifications => {
        expect($notifications).to.have.length(openCases.length);
      });

      openCases.forEach(openCase => {
        myNotifications.notificationCaseIds.contains(openCase.label);
      });
    });
  });
});
