import {
  NotificationDetail,
  MyDocuments,
  RecentMessages,
  RequiredFromMe,
  RequestAmendmentForm
} from '../../../support/page-objects/modules/notifications';

describe('Notification Detail Page', () => {
  const page = new NotificationDetail();
  const documents = new MyDocuments();
  const messages = new RecentMessages();
  const requestAmendmentForm = new RequestAmendmentForm();
  const requiredFromMe = new RequiredFromMe();
  const summary = page.summary;
  const timeline = page.timeline;
  const jobProtectedLeave = page.jobProtectedLeave;
  const jobProtectedLeavePlan = jobProtectedLeave.jobProtectedLeavePlan;
  const periodDecisionHeader = jobProtectedLeavePlan.periodDecisionHeaders;
  const periodDecision = jobProtectedLeavePlan.periodDecisions;
  const wageReplacement = page.wageReplacement;
  const benefits = wageReplacement.benefits;
  const paymentHistory = page.paymentHistory;

  const summaryProperties = [
    { label: 'Your case progress', value: 'We are considering your request' },
    { label: 'Notification date', value: 'Aug 18, 2017' },
    { label: 'Expected delivery date', value: 'Aug 01, 2019' },
    { label: 'Actual delivery date', value: 'Aug 01, 2019' },
    { label: 'First day missing work', value: 'Aug 16, 2017' },
    { label: 'Expected return to work', value: 'Sep 02, 2019' }
  ];

  const periodDecisionHeaderProperties = [
    { label: 'Managed by', value: 'Sammy Okamura' }
  ];

  const periodDecisionProperties = [
    { label: 'Absence reason', value: 'Pregnancy/Maternity - Prenatal care' },
    { label: 'Leave begins', value: 'Jan 01, 2019' },
    { label: 'Approved through', value: 'Jan 11, 2019' }
  ];

  const periodDecisionLines = [
    {
      name: 'My Child Bonding Plan',
      statuses: ['Approved (2)', 'Pending (1)']
    },
    { name: 'My Child Bonding Plan 2', statuses: ['Approved (1)'] },
    {
      name: 'My Child Bonding Plan 3',
      statuses: ['Approved (2)', 'Declined (1)']
    },
    { name: 'My Child Bonding Plan 4', statuses: ['Approved (2)'] },
    { name: 'My Child Bonding Plan 5', statuses: ['Approved (1)'] }
  ];

  const wageReplacementHeaderProperties = [
    { label: 'Managed by', value: 'Benny Smith' }
  ];

  const wageReplacementProperties = [
    { label: 'Benefit start date', value: 'Jul 10, 2019' },
    { label: 'Approved through', value: 'Jul 13, 2019' },
    { label: 'Expected Resolution', value: 'Jul 13, 2019' }
  ];

  const uploadDocumentsList = [
    { name: 'Document 1' },
    { name: 'Upload Document 2' }
  ];

  beforeEach(() => {
    page.navigateTo();
  });

  it('should display the notification details', () => {
    cy.url().should('include', '/#notifications/NTN-21');

    // check the summary
    cy.get(summary.elem).within(() => {
      summary.collapse.header.contains('Summary');

      summaryProperties.map(({ label, value }, index) => {
        summary.properties.hasLabel(index, label);
        summary.properties.hasValue(index, value);
      });
    });

    // check the timeline
    cy.get(timeline.elem).within(() => {
      timeline.collapse.header.contains('Timeline');
    });

    // check the job protected leave plans/decisions
    cy.get(jobProtectedLeave.elem).within(() => {
      jobProtectedLeave.header.should('be', 'Job-protected Leave');
      jobProtectedLeave.getLegendItemAt(0).should('be', 'Fixed Time');
      jobProtectedLeave.getLegendItemAt(1).should('be', 'Intermittent time');
      jobProtectedLeave.getLegendItemAt(2).should('be', 'Reduced Schedule');
    });

    cy.get(jobProtectedLeavePlan.elem).within(() => {
      cy.get(jobProtectedLeavePlan.periodDecisionHeaders.elem).should(
        'have.length',
        periodDecisionLines.length
      );

      periodDecisionLines.forEach(({ name, statuses }, index) => {
        jobProtectedLeavePlan.periodDecisionHeaders
          .getNameAt(index)
          .should('be', name);

        statuses.forEach((status, statusIndex) => {
          jobProtectedLeavePlan.periodDecisionHeaders
            .getStatusAt(index, statusIndex)
            .should('be', status);
        });
      });

      jobProtectedLeavePlan.periodDecisionHeaders.toggleAt(0);

      jobProtectedLeavePlan.periodDecisionHeaders
        .getPeriodDecisionHeaderAt(0)
        .within(() => {
          periodDecisionHeaderProperties.map(({ label, value }, index) => {
            jobProtectedLeavePlan.periodDecisionHeaders.properties.hasLabel(
              index,
              label
            );
            jobProtectedLeavePlan.periodDecisionHeaders.properties.hasValue(
              index,
              value
            );
          });
        });

      jobProtectedLeavePlan.periodDecisions
        .getPeriodDecisionAt(0)
        .within(() => {
          periodDecisionProperties.map(({ label, value }, index) => {
            jobProtectedLeavePlan.periodDecisions.properties.hasLabel(
              index,
              label
            );
            jobProtectedLeavePlan.periodDecisions.properties.hasValue(
              index,
              value
            );
          });
        });
    });

    // check the wage replacements
    cy.get(wageReplacement.elem).within(() => {
      wageReplacement.header.should('be', 'Wage Replacement');
    });

    // check benefits
    cy.get(benefits.elem).within(() => {
      benefits.benefitHeader.name.should('be', 'My Child Bonding Plan');
      benefits.benefitHeader.getStatusAt(0).should('be', 'Pending');

      benefits.benefitHeader.toggleAt(0);

      wageReplacementHeaderProperties.map(({ label, value }, index) => {
        benefits.benefitHeader.properties.hasLabel(index, label);
        benefits.benefitHeader.properties.hasValue(index, value);
      });

      benefits.benefit.getWageReplacementAt(0).within(() => {
        wageReplacementProperties.map(({ label, value }, index) => {
          benefits.benefit.properties.hasLabel(index, label);
          benefits.benefit.properties.hasValue(index, value);
        });

        benefits.benefit.getStatusAt(0).should('be', 'Pending');
      });
    });

    cy.get(benefits.elem).within(() => {
      benefits.benefitHeader.name.should('be', 'My Child Bonding Plan');
      benefits.benefitHeader.getStatusAt(1).should('be', 'Approved');

      benefits.benefitHeader.toggleAt(1);

      // check that some of the decisions can show payment history

      benefits.benefit.getWageReplacementAt(1).within(() => {
        paymentHistory.paymentHeader.contains('Latest Payments');

        paymentHistory.payment.should($paymentHistory => {
          expect($paymentHistory.children()).to.have.length(1);
        });
      });
    });
  });

  it(`should display ammend period form`, () => {
    page.navigateTo('NTN-21');

    cy.get(jobProtectedLeave.elem);

    cy.get(jobProtectedLeavePlan.elem).within(() => {
      cy.get(periodDecisionHeader.elem)
        .first()
        .click();
    });

    cy.get(periodDecision.elem)
      .first()
      .click();

    cy.get(periodDecision.elem)
      .contains('Actions')
      .click();

    cy.contains('Request an amendment').click();

    cy.get(requestAmendmentForm.elem);
  });

  it(`should request to delete period`, () => {
    cy.get(jobProtectedLeave.elem);

    cy.get(jobProtectedLeavePlan.elem).within(() => {
      cy.get(periodDecisionHeader.elem)
        .first()
        .click();
    });

    cy.get(periodDecision.elem)
      .first()
      .click();

    cy.get(periodDecision.elem)
      .contains('Actions')
      .click();

    cy.contains('Delete leave period').click();

    cy.get('[data-test-el="confirmation-submit-button"]').click();

    page.confirmationModal.contains('Request deletion of leave request');
  });

  it(`should display a user's documents`, () => {
    cy.get(documents.elem).within(() => {
      documents.header.should('be', `My Documents (5)`);

      documents.getTypeAt(1).contains(`Employee Statement.xml`);
      documents.getDateAt(1).contains(`Aug 06, 2017`);
    });
  });

  it('should display the required from me section', () => {
    page.navigateTo('NTN-21');
    cy.get(requiredFromMe.elem).within(() => {
      requiredFromMe.header.should('be', 'Required from me (2)');

      requiredFromMe.list.should($documentList => {
        expect($documentList.children()).to.have.length(3);
      });

      requiredFromMe.confirmReturnBtn.should(
        'be',
        'Confirm the day you return to work'
      );

      uploadDocumentsList.map(({ name }, index) => {
        requiredFromMe.name.eq(index).should('be', name);
      });
    });
  });

  it(`should display a messages`, () => {
    cy.get(messages.elem).within(() => {
      messages.header.should('be', `Recent Messages`);

      messages.messagesList.should($messages => {
        expect($messages.children()).to.have.length(4);
      });

      messages.newMessage.click();
      messages.newMessage.should('have.text', `New Message`);
    });
  });

  it(`should display a my messages page with selected notification and message`, () => {
    cy.get(messages.elem).within(() => {
      messages.openMessage(1);
      cy.url().should('include', '8');
    });
  });
});
