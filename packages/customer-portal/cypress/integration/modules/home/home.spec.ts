import {
  HomePage,
  MyOpenNotifications,
  LatestPayments,
  RequiredFromMe
} from '../../../support/page-objects/modules/home';

describe('Home Page', () => {
  const page = new HomePage();
  const openNotifications = new MyOpenNotifications();

  const latestPayments = new LatestPayments();
  const paymentDetails = latestPayments.paymentDetails;
  const paymentLines = latestPayments.paymentLines;

  const requiredFromMe = new RequiredFromMe();

  const openCases = [
    { label: 'NTN-21' },
    { label: 'NTN-22' },
    { label: 'NTN-23' },
    { label: 'NTN-25' },
    { label: 'NTN-4' },
    { label: 'NTN-26' },
    { label: 'NTN-27' },
    { label: 'NTN-28' },
    { label: 'NTN-873' }
  ];
  const paymentsCases = [{ label: '$1,000.00' }];

  const detailsProperties = [
    { label: 'Paid to:', value: 'Jon Snow' },
    { label: 'Payment Method', value: 'Electronic funds transfer' },
    { label: 'Period Start Date', value: 'Jun 06, 2019' },
    { label: 'Period End Date', value: 'Jun 28, 2019' }
  ];
  const lineProperties = [
    { label: 'Paid to:', value: 'Jon Snow' },
    { label: 'Start Date:', value: 'Jul 26, 2019' },
    { label: 'End Date:', value: 'Aug 26, 2019' }
  ];

  const uploadDocumentsList = [
    { name: 'Document 1', caseDetails: 'Required for: NTN-21' },
    { name: 'Upload Document 2', caseDetails: 'Required for: NTN-21' },
    { name: 'Document 3', caseDetails: 'Required for: NTN-22' },
    { name: 'Upload Document 4', caseDetails: 'Required for: NTN-23' }
  ];

  beforeEach(() => {
    page.navigateTo();
    cy.wait(2000);
  });

  it('should allow the user to navigate to the intake form', () => {
    cy.get(page.elem).within(() => {
      cy.url().should('be', '/');
      page.action.should('have.text', `Let's get started`);
      page.action.click();
      cy.url().should('include', '/#intake');
    });
  });

  it(`should display the user's open notifications`, () => {
    cy.get(openNotifications.elem).within(() => {
      openNotifications.accordianHeader.contains(
        `My Open Notifications (${openCases.length})`
      );

      openNotifications.notificationCount.should($notifications => {
        expect($notifications).to.have.length(openCases.length);
      });

      openCases.forEach(openCase => {
        openNotifications.notificationCount.contains(openCase.label);
      });

      openNotifications
        .getProgressAt(1)
        .contains(`We've made some decisions on your case`);

      openNotifications.getAssessmentAt(1).contains('Wage Replacement');
    });
  });

  it('should display all payments', () => {
    cy.get(latestPayments.elem).within(() => {
      latestPayments.paymentsContainer.should($payments => {
        expect($payments.children()).to.have.length(1);
      });
      paymentsCases.forEach(paymentsCase => {
        latestPayments.paymentsContainer.contains(paymentsCase.label);
      });
    });
  });

  it('should display the users payments from the past year', () => {
    latestPayments.getPaymentPanelHeader(0).click();

    cy.get(latestPayments.elem).within(() => {
      detailsProperties.map(({ label, value }, index) => {
        paymentDetails.properties.hasLabel(index, label);
        paymentDetails.properties.hasValue(index, value);
      });
    });

    cy.get(latestPayments.elem).within(() => {
      paymentDetails.showBreakdownBtn.click();

      paymentLines.getPaymentLineBtn(0).click();

      cy.get(paymentLines.elem).within(() => {
        lineProperties.map(({ label, value }, index) => {
          paymentLines.properties.hasLabel(index, label);
          paymentLines.properties.hasValue(index, value);
        });
      });
    });
  });

  it('should display the required from me section', () => {
    cy.get(requiredFromMe.elem).within(() => {
      requiredFromMe.header.should('be', 'Required from me (2)');

      requiredFromMe.list.should($documentList => {
        expect($documentList.children()).to.have.length(4);
      });

      uploadDocumentsList.map(({ name, caseDetails }, index) => {
        requiredFromMe.name.eq(index).should('be', name);
        requiredFromMe.name.eq(index).should('be', caseDetails);
      });
    });
  });
});
