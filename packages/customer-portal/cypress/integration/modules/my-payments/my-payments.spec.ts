import { MyPayments } from '../../../support/page-objects/modules/my-payments';

describe('My Payments Page', () => {
  const myPayments = new MyPayments();
  const paymentDetails = myPayments.paymentDetails;
  const paymentLines = myPayments.paymentLines;

  const detailsProperties = [
    { label: 'Paid to:', value: 'Jon Snow' },
    { label: 'Payment Method', value: 'Electronic funds transfer' },
    { label: 'Period Start Date', value: 'Jun 06, 2019' },
    { label: 'Period End Date', value: 'Jun 28, 2019' }
  ];
  const lineProperties = [
    { label: 'Paid to:', value: 'Jon Snow' },
    { label: 'Start Date:', value: 'Jul 26, 2019' },
    { label: 'End Date:', value: 'Aug 26, 2019' }
  ];

  beforeEach(() => {
    myPayments.navigateTo();
  });

  it('should test the my-payments page', () => {
    cy.get(myPayments.elem).within(() => {
      cy.url().should('be', '/my-payments');
      myPayments.paymentHeader.contains('My Payments');
    });
  });

  it('should display all payments', () => {
    cy.get(myPayments.elem).within(() => {
      myPayments.paymentsList.should($payments => {
        expect($payments.children()).to.have.length(3);
      });
    });
  });

  it('should open a payment panel', () => {
    myPayments.getPaymentPanel(0).click();

    cy.get(myPayments.elem).within(() => {
      detailsProperties.map(({ label, value }, index) => {
        paymentDetails.properties.hasLabel(index, label);
        paymentDetails.properties.hasValue(index, value);
      });
    });

    cy.get(myPayments.elem).within(() => {
      paymentDetails.showBreakdownBtn.click();

      paymentLines.getPaymentLineBtn(0).click();

      cy.get(paymentLines.elem).within(() => {
        lineProperties.map(({ label, value }, index) => {
          paymentLines.properties.hasLabel(index, label);
          paymentLines.properties.hasValue(index, value);
        });
      });
    });
  });
});
