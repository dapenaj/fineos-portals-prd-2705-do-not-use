import { MyMessagesPage } from '../../../support/page-objects/modules/my-messages';

describe('My messages Page', () => {
  let myMessages: MyMessagesPage;

  beforeEach(() => {
    myMessages = new MyMessagesPage();
    myMessages.navigateTo();
  });

  it('should display page name', () => {
    cy.get(myMessages.elem).within(() => {
      cy.url().should('be', '/my-messages');

      myMessages.messagesHeader.should('have.text', `My Messages`);
    });
  });

  it('switch to incoming messages', () => {
    cy.get(myMessages.elem).within(() => {
      myMessages.messageList.should($messages => {
        expect($messages.children()).to.have.length(9);
      });
      myMessages.actions.click();
      myMessages.messageList.should($messages => {
        expect($messages.children()).to.have.length(4);
      });
    });
  });

  it('should display search', () => {
    cy.get(myMessages.elem).within(() => {
      myMessages.searchMessages('test');
      myMessages.empty.emptyList.should(
        'have.text',
        `You don't have any messages.`
      );
      myMessages.getIconCloseBadge.click();
      myMessages.searchMessages('Cras');
      myMessages.messageList.should($messages => {
        expect($messages.children()).to.have.length(3);
      });
    });
  });

  it('new message', () => {
    cy.get(myMessages.elem).within(() => {
      myMessages.newMessage
        .first()
        .should('have.text', 'New Message')
        .click();
    });
  });
});
