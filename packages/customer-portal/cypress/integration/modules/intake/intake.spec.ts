import {
  ConfirmationPage,
  IntakePage
} from '../../../support/page-objects/modules/intake';

describe('Intake Page', () => {
  const page = new IntakePage();

  const yourRequest = page.yourRequest;

  const personalDetailsForm = yourRequest.personalDetailsForm;
  const customerDetails = personalDetailsForm.customerDetails;
  const addressDetails = personalDetailsForm.addressDetails;

  const workDetailsRenderer = yourRequest.workDetailsRenderer;
  const existingWorkDetails = workDetailsRenderer.existingWorkDetails;
  const workDetailsForm = workDetailsRenderer.workDetailsForm;

  const initialRequest = yourRequest.initialRequestForm;

  const detail = page.detail;

  const wrapUp = page.wrapUp;

  beforeEach(() => {
    page.navigateTo();
    cy.wait(2000);
  });

  it('should be on the intake page', () => {
    cy.get(page.elem).within(() => {
      cy.url().should('include', '/#intake');

      page.overview.count.should('have.length', 3);

      page.overview.getItemAt(0).number.should('be', '1');
      page.overview.getItemAt(0).number.should('be', 'Your Request');
      page.overview.getItemAt(1).number.should('be', '2');
      page.overview.getItemAt(1).number.should('be', 'Details');
      page.overview.getItemAt(2).number.should('be', '3');
      page.overview.getItemAt(2).number.should('be', 'Wrap-up');
    });
  });

  describe('Your Request', () => {
    describe('Personal Details', () => {
      it('form is populated and can be submitted', () => {
        cy.get(personalDetailsForm.elem).within(() => {
          customerDetails.firstNameInput.should('have.value', 'Jane');
          customerDetails.lastNameInput.should('have.value', 'Doe');
          customerDetails.dateOfBirthInput.should('have.value', 'Jan 01, 1988');
          customerDetails.intCodeInput.should('have.value', '353');
          customerDetails.areaCodeInput.should('have.value', '1');
          customerDetails.telephoneNoInput.should('have.value', '234567');
          customerDetails.emailInput.should('have.value', 'bob@dobalina.com');

          addressDetails.addressLine1Input.should('have.value', 'Main street');
          addressDetails.addressLine2Input.should(
            'have.value',
            `Where it's at`
          );
          addressDetails.addressLine3Input.should(
            'have.value',
            'Two turn tables'
          );
          addressDetails.cityInput.should('have.value', 'And a microphone');
          addressDetails.postCodeInput.should('have.value', 'AB123456');

          personalDetailsForm.submitBtn.should('be.enabled');
        });
      });

      it('can be updated and submitted', () => {
        customerDetails.clearAndPopulate({
          firstName: 'Test First Name',
          lastName: 'Test Last Name',
          phoneNumber: { int: '353', area: '01', tele: '456765' },
          email: 'test@test.com'
        });

        addressDetails.clearAndPopulate({
          addressLine1: 'Fake Street',
          city: 'Springfield',
          state: { option: 'Alabama', search: 'al' },
          postCode: 'Test'
        });

        addressDetails.addressLine2Input.clear();
        addressDetails.addressLine3Input.clear();

        cy.get(personalDetailsForm.elem).within(() => {
          personalDetailsForm.submitBtn.should('be.enabled');
          personalDetailsForm.submitBtn.click().then(() => {
            customerDetails.firstNameInput.should(
              'have.value',
              'Test First Name'
            );
            customerDetails.lastNameInput.should(
              'have.value',
              'Test Last Name'
            );

            customerDetails.intCodeInput.should('have.value', '353');
            customerDetails.areaCodeInput.should('have.value', '01');
            customerDetails.telephoneNoInput.should('have.value', '456765');
            customerDetails.emailInput.should('have.value', 'test@test.com');

            addressDetails.addressLine1Input.should(
              'have.value',
              'Fake Street'
            );
            addressDetails.addressLine2Input.should('have.value', '');
            addressDetails.addressLine3Input.should('have.value', '');
            addressDetails.cityInput.should('have.value', 'Springfield');
            addressDetails.postCodeInput.should('have.value', 'Test');
          });
        });
      });
    });

    describe('Work Details', () => {
      beforeEach(() => {
        cy.get(personalDetailsForm.elem).within(() => {
          personalDetailsForm.submitBtn.should('be.enabled');
          personalDetailsForm.submitBtn.click();
        });
      });

      it('displays an existing occupation', () => {
        cy.get(workDetailsRenderer.elem).within(() => {
          workDetailsRenderer.title.should(
            'have.text',
            'Are these still correct?'
          );
          existingWorkDetails.title.should('have.text', 'FinCorp Ltd');
          existingWorkDetails.details.should(
            'have.text',
            'Working as a Line Supervisor, hired on Feb 02, 2014'
          );
        });
      });

      it('should allow for an occupation to be changed', () => {
        cy.get(workDetailsRenderer.elem).within(() => {
          existingWorkDetails.details.should(
            'have.text',
            'Working as a Line Supervisor, hired on Feb 02, 2014'
          );

          existingWorkDetails.requestChangeBtn.click().then(() => {
            workDetailsRenderer.title.should(
              'have.text',
              'Something not matching?'
            );

            workDetailsForm.title.should(
              'have.text',
              'If you are noticing some details are not right, you can request a change in your employment record:'
            );

            workDetailsForm.submitBtn.should('have.text', 'Ok');

            workDetailsForm.jobTitleInput.clear().type('New Job Title');
            workDetailsForm.submitBtn.click().then(() => {
              existingWorkDetails.details.should(
                'have.text',
                'Working as a New Job Title, hired on Feb 02, 2014'
              );
            });
          });
        });
      });

      it('should allow for a new occupation to be entered', () => {
        cy.get(workDetailsRenderer.elem).within(() => {
          existingWorkDetails.newOccupationBtn.click().then(() => {
            workDetailsRenderer.title.should('have.text', 'Moving on?');

            workDetailsForm.title.should(
              'have.text',
              'If you have a new employment, you can add it here:'
            );

            workDetailsForm.submitBtn.should('have.text', 'Add new employment');
          });
        });
      });

      it('should allow the the user to cancel out form the occupation form', () => {
        cy.get(workDetailsRenderer.elem).within(() => {
          existingWorkDetails.requestChangeBtn.click();
          workDetailsForm.cancelBtn.click().then(() => {
            workDetailsRenderer.title.should(
              'have.text',
              'Are these still correct?'
            );

            existingWorkDetails.newOccupationBtn.click();
            workDetailsForm.cancelBtn.click().then(() => {
              workDetailsRenderer.title.should(
                'have.text',
                'Are these still correct?'
              );
            });
          });
        });
      });

      it('should be possible to proceed with the existing occupation', () => {
        cy.get(workDetailsRenderer.elem).within(() => {
          workDetailsRenderer.title.should(
            'have.text',
            'Are these still correct?'
          );

          existingWorkDetails.proceedBtn.should('be.enabled');
          existingWorkDetails.proceedBtn.click();
        });
      });
    });

    describe('Initial Request', () => {
      beforeEach(() => {
        cy.get(personalDetailsForm.elem).within(() => {
          personalDetailsForm.submitBtn.should('be.enabled');
          personalDetailsForm.submitBtn.click();
        });

        cy.get(existingWorkDetails.elem).within(() => {
          existingWorkDetails.proceedBtn.should('be.enabled');
          existingWorkDetails.proceedBtn.click();
        });
      });

      it('display the start initial request events', () => {
        cy.get(initialRequest.elem).within(() => {
          initialRequest.optionGroupCount.should('have.length', 1);
          initialRequest.radioCount.should('have.length', 8);
          initialRequest.submitBtn.should('be.disabled');
          initialRequest
            .getTitleAt(0)
            .should('have.text', 'How can we help you?');
        });
      });

      it('should be possible to select initial request events', () => {
        cy.get(initialRequest.elem).within(() => {
          initialRequest.checkRadioInGroupAt(0, 0);
          initialRequest.optionGroupCount.should('have.length', 2);
          initialRequest
            .getTitleAt(1)
            .should(
              'have.text',
              'Mark the option which better describes why you need time off:'
            );
          initialRequest.checkRadioInGroupAt(1, 2);
          initialRequest.optionGroupCount.should('have.length', 3);
          initialRequest.radioCount.should('have.length', 15);
          initialRequest
            .getTitleAt(2)
            .should('have.text', 'What are you donating?');
          initialRequest.checkRadioInGroupAt(0, 2);
          initialRequest.optionGroupCount.should('have.length', 2);
          initialRequest.radioCount.should('have.length', 11);
        });
      });

      it('be possible to proceed with initial request', () => {
        cy.get(initialRequest.elem).within(() => {
          initialRequest.fillForm();
          initialRequest.submitBtn.should('be.enabled');
          initialRequest.submitBtn.click();
        });
      });
    });
  });

  describe('Details', () => {
    const requestDetailsForm = detail.requestDetailsForm;
    const timeOff = detail.timeOff;

    beforeEach(() => {
      yourRequest.fillForm();
    });

    describe('Request Details', () => {
      it('display a form based on request', () => {
        const initialLabels = [
          'Did you stay overnight in hospital?',
          'When was the delivery date?',
          'How many new-borns were delivered?',
          'What was the pregnancy delivery method?',
          'Were there pregnancy or delivery-related complications?',
          'Anything else you wish to tell us which will help us understand what happened to you?'
        ];

        cy.get(requestDetailsForm.elem).within(() => {
          requestDetailsForm.title.should(
            'have.text',
            'Can you tell us more about the situation?'
          );

          requestDetailsForm.fields.should('have.length', 6);
          requestDetailsForm.fields.each((el, index) => {
            requestDetailsForm
              .getLabelAt(index)
              .should('have.text', initialLabels[index]);
          });

          requestDetailsForm.submitBtn.should('be.disabled');
        });
      });

      it('should add and remove questions based on answers', () => {
        const firstLabelText = 'Did you stay overnight in hospital?',
          secondLabelText = 'When was the delivery date?',
          extraLabelText = 'What dates did you stay in hospital?';

        cy.get(requestDetailsForm.elem).within(() => {
          // original questions are shown
          requestDetailsForm.fields.should('have.length', 6);
          requestDetailsForm.getLabelAt(0).should('have.text', firstLabelText);
          requestDetailsForm.getLabelAt(1).should('have.text', secondLabelText);

          // select an option in the first radio group
          requestDetailsForm.checkRadioForField(
            requestDetailsForm.fields.first(),
            1
          );

          // now there is an extra questions between the original first 2
          requestDetailsForm.fields.should('have.length', 7);
          requestDetailsForm.getLabelAt(0).should('have.text', firstLabelText);
          requestDetailsForm.getLabelAt(1).should('have.text', extraLabelText);
          requestDetailsForm.getLabelAt(2).should('have.text', secondLabelText);

          // select a different option in the first radio group
          requestDetailsForm.checkRadioForField(
            requestDetailsForm.fields.first(),
            0
          );

          // questions go back to the original set
          requestDetailsForm.fields.should('have.length', 6);
          requestDetailsForm.getLabelAt(0).should('have.text', firstLabelText);
          requestDetailsForm.getLabelAt(1).should('have.text', secondLabelText);
        });
      });

      it('can be updated and submitted', () => {
        cy.get(requestDetailsForm.elem).within(() => {
          requestDetailsForm.submitBtn.should('be.disabled');

          requestDetailsForm.fillForm();

          requestDetailsForm.submitBtn.should('be.enabled');
          requestDetailsForm.submitBtn.click();
        });
      });

      it('can disable/enable the submit button when additional fields are added/removed', () => {
        cy.get(requestDetailsForm.elem).within(() => {
          requestDetailsForm.fields.should('have.length', 6);
          requestDetailsForm.submitBtn.should('be.disabled');
          requestDetailsForm.fillForm();
          requestDetailsForm.submitBtn.should('be.enabled');

          // change the radio group value to show another question
          requestDetailsForm.checkRadioForField(
            requestDetailsForm.fields.eq(0),
            1
          );
          requestDetailsForm.fields.should('have.length', 7);
          requestDetailsForm.submitBtn.should('be.disabled');

          // change the radio group value back
          requestDetailsForm.checkRadioForField(
            requestDetailsForm.fields.eq(0),
            0
          );
          requestDetailsForm.fields.should('have.length', 6);
          requestDetailsForm.submitBtn.should('be.enabled');
        });
      });
    });

    describe('Time-off', () => {
      const timeOffForm = timeOff.timeOffForm;
      const leavePeriodForm = timeOff.leavePeriodForm;

      const timeOffLabels = [
        'Which of these better fit your circumstances?',
        'When was the last day you worked?',
        'When was the first day of your absence?',
        'When is the last day of your absence?',
        'When do you expect to return to work?',
        'Will you be working normal hours the day you return?',
        'Have these dates been confirmed?'
      ];
      const reducedScheduleLabels = [
        'Which of these better fit your circumstances?',
        'When is/was your first day of work under a reduced schedule?',
        'When is your last day of work under a reduced schedule?',
        'Have these dates been confirmed?'
      ];
      const episodicLabels = [
        'Which of these better fit your circumstances?',
        'When does your intermittent leave period start?',
        'If you are requesting intermittent leave for a limited period of time, you can enter here the last day of this period:'
      ];

      beforeEach(() => {
        cy.get(requestDetailsForm.elem).within(() => {
          requestDetailsForm.fillForm();
          requestDetailsForm.submitBtn.click();
        });
      });

      it('should display', () => {
        cy.get(timeOffForm.elem).within(() => {
          timeOffForm.title.should(
            'have.text',
            'How much time do you need to be off-work?'
          );
          timeOffForm.addBtn.should('have.text', 'Add time-off');

          timeOffForm.submitBtn.should('be.disabled');
        });
      });

      it('should display a form for adding leave periods', () => {
        cy.get(timeOffForm.elem).within(() => {
          timeOffForm.addBtn.click();
        });

        cy.get(leavePeriodForm.elem).within(() => {
          leavePeriodForm.fields.should('have.length', timeOffLabels.length);
          leavePeriodForm.fields.each((el, index) => {
            leavePeriodForm
              .getLabelAt(index)
              .should('have.text', timeOffLabels[index]);
          });

          leavePeriodForm.submitBtn.should('be.disabled');
        });
      });

      it('should add or remove questions based on answers', () => {
        cy.get(timeOffForm.elem).within(() => {
          timeOffForm.addBtn.click();
        });

        cy.get(leavePeriodForm.elem).within(() => {
          leavePeriodForm.fields.should('have.length', timeOffLabels.length);
          leavePeriodForm.fields.each((el, index) => {
            leavePeriodForm
              .getLabelAt(index)
              .should('have.text', timeOffLabels[index]);
          });

          // select reduced schedule type
          leavePeriodForm.checkRadioForField(leavePeriodForm.fields.first(), 1);
          leavePeriodForm.fields.should(
            'have.length',
            reducedScheduleLabels.length
          );
          leavePeriodForm.fields.each((el, index) => {
            leavePeriodForm
              .getLabelAt(index)
              .should('have.text', reducedScheduleLabels[index]);
          });

          // select episodic type
          leavePeriodForm.checkRadioForField(leavePeriodForm.fields.first(), 2);
          leavePeriodForm.fields.should('have.length', episodicLabels.length);
          leavePeriodForm.fields.each((el, index) => {
            leavePeriodForm
              .getLabelAt(index)
              .should('have.text', episodicLabels[index]);
          });
        });
      });

      it('should show a list of leave periods', () => {
        timeOff.addTimeOffPeriod();
        timeOff.addReducedSchedulePeriod(true);
        timeOff.addEpisodicPeriod(true);
        timeOff.addTimeOffPeriod(true);
        timeOff.addReducedSchedulePeriod(true);

        cy.get(timeOffForm.elem).within(() => {
          timeOffForm.periods.should('have.length', 5);
        });
      });

      it('can be submitted', () => {
        timeOff.addTimeOffPeriod();

        cy.get(timeOffForm.elem).within(() => {
          timeOffForm.submitBtn.should('be.enabled');
          timeOffForm.submitBtn.click();
        });
      });
    });
  });

  describe('Wrap-Up', () => {
    const requestDetailsForm = detail.requestDetailsForm;
    const timeOff = detail.timeOff;

    beforeEach(() => {
      const timeOffForm = timeOff.timeOffForm;
      yourRequest.fillForm();

      cy.get(requestDetailsForm.elem).within(() => {
        requestDetailsForm.fillForm();
        requestDetailsForm.submitBtn.click();
      });

      timeOff.addTimeOffPeriod();

      cy.get(timeOffForm.elem).within(() => {
        timeOffForm.submitBtn.click();
      });

      // wait an arbitrary amount of time before clicking the button
      // it's not ideal, but for now it works.
      cy.wait(2000);
    });

    describe('Additional Income', () => {
      const additionalIncome = wrapUp.additionalIncome;

      it('should display', () => {
        cy.get(additionalIncome.elem).within(() => {
          additionalIncome.title.should(
            'have.text',
            'Do you have any additional sources of income?'
          );
          additionalIncome.emptyIncomeSource.content.should(
            'have.text',
            'Add here any regular payment you receive other than your job salary. ' +
              'Some examples of additional income sources are allowances, bonuses, and commissions.'
          );
          additionalIncome.emptyIncomeSource.button.should(
            'have.text',
            'Add additional source of income'
          );

          additionalIncome.submitBtn.should('be.enabled');
        });
      });

      it('should display a form for adding income sources', () => {
        additionalIncome.addFirstBtn.click();

        additionalIncome.incomeSourceForm.incomeType.label.should(
          'have.text',
          'Income type'
        );
        additionalIncome.incomeSourceForm.frequency.label.should(
          'have.text',
          'How often are you paid?'
        );

        additionalIncome.incomeSourceForm.amount.label.should(
          'have.text',
          'How much are you paid?'
        );

        additionalIncome.incomeSourceForm.startDate.label.should(
          'have.text',
          'When did you first receive payment for this?'
        );

        additionalIncome.incomeSourceForm.endDate.label.should(
          'have.text',
          'When will you receive your last payment for this?'
        );

        additionalIncome.incomeSourceForm.submitBtn.should('be.disabled');
      });

      it('should add income source', () => {
        additionalIncome.addFirstBtn.click();

        additionalIncome.incomeSourceForm.fillWithValues();

        additionalIncome.incomeSourceForm.submitBtn.click();

        const firstItem = additionalIncome.getIncomeSourceItemAt(0);
        firstItem.title.should('have.text', 'State Disability');
        firstItem.description.should('have.text', '$235.00 (Monthly)');
      });

      it('should delete income source', () => {
        additionalIncome.addFirstBtn.click();

        additionalIncome.incomeSourceForm.fillWithValues();

        additionalIncome.incomeSourceForm.submitBtn.click();

        additionalIncome.incomeSourceItems.should('have.length', 1);

        additionalIncome.addAnotherBtn.click();
        additionalIncome.incomeSourceForm.fillWithValues();
        additionalIncome.incomeSourceForm.submitBtn.click();

        additionalIncome.incomeSourceItems.should('have.length', 2);

        additionalIncome.getIncomeSourceItemAt(0).deleteBtn.click();

        additionalIncome.incomeSourceItems.should('have.length', 1);
      });

      it('should edit income source', () => {
        additionalIncome.addFirstBtn.click();

        additionalIncome.incomeSourceForm.fillWithValues();

        additionalIncome.incomeSourceForm.submitBtn.click();

        const firstItem = additionalIncome.getIncomeSourceItemAt(0);

        firstItem.editBtn.click();

        additionalIncome.incomeSourceForm.fillWithValues({
          incomeType: 'Sick Pay',
          frequency: 'Weekly',
          amount: '135',
          startDate: '05-06-2019',
          endDate: '06-06-2019'
        });
        additionalIncome.incomeSourceForm.submitBtn.click();

        firstItem.title.should('have.text', 'Sick Pay');
        firstItem.description.should('have.text', '$135.00 (Weekly)');
      });

      it('should lead to next step after submit', () => {
        additionalIncome.addFirstBtn.click();

        additionalIncome.incomeSourceForm.fillWithValues();
        additionalIncome.incomeSourceForm.submitBtn.click();

        additionalIncome.submitBtn.click();

        additionalIncome.submitBtn.should('be.disabled');
      });

      it('should show Payment preferences', () => {
        cy.get(additionalIncome.elem).within(() => {
          additionalIncome.paymentMethodLabel.should(
            'have.text',
            'Payment preferences'
          );
          additionalIncome.fillRadio();
          additionalIncome.eftAccountButton.should('be.enabled');
          additionalIncome.eftAccountButton.should(
            'have.text',
            'I want to use a different account'
          );
        });
      });
    });

    describe('Supporting Evidence', () => {
      const supportingEvidence = wrapUp.supportingEvidence;

      beforeEach(() => {
        cy.get(wrapUp.additionalIncome.elem).within(() => {
          wrapUp.additionalIncome.submitBtn.click();
        });
      });

      it('should display', () => {
        supportingEvidence.description.should(
          'have.text',
          'We need to capture the contact information of who gave or is giving you medical treatment and supervision.'
        );

        supportingEvidence.primaryPhysicianSectionTitle.should(
          'have.text',
          'Who is your primary care physician (PCP)?'
        );

        supportingEvidence.anotherMedicalProvidersSectionTitle.should(
          'have.text',
          'Do you wish to add another medical provider?'
        );

        supportingEvidence.primaryPhysicianPlaceholder.content.should(
          'have.text',
          'A primary care physician (PCP) is a health care practitioner ' +
            'who sees people that have common medical problems. ' +
            'This person is most often a doctor, a physical assistant or a nurse practitioner.'
        );

        supportingEvidence.anotherMedicalProviderPlaceholder.content.should(
          'have.text',
          'For example, if you were admitted in the emergency room, ' +
            'you might have been attended by someone other than your PCP.'
        );

        supportingEvidence.saveAndProceedBtn.should('be.enabled');
      });

      it('should display a form for adding primary physician', () => {
        supportingEvidence.primaryPhysicianPlaceholder.button.click();

        supportingEvidence.medicalProviderForm.addressSectionTitle.should(
          'have.text',
          `Enter your primary care physician's address if you have it`
        );

        supportingEvidence.medicalProviderForm.name.label.should(
          'have.text',
          'Name of your primary care physician'
        );
        supportingEvidence.medicalProviderForm.phone.label.should(
          'have.text',
          'Contact number of your PCP'
        );

        supportingEvidence.medicalProviderForm.addressLine.label.should(
          'have.text',
          'Address line (street, number, complement)'
        );

        supportingEvidence.medicalProviderForm.city.label.should(
          'have.text',
          'City'
        );

        supportingEvidence.medicalProviderForm.zipCode.label.should(
          'have.text',
          'Zip Code'
        );

        supportingEvidence.medicalProviderForm.country.label.should(
          'have.text',
          'Country'
        );

        supportingEvidence.medicalProviderForm.saveBtn.should('be.disabled');
      });

      it('should display a form for another medical provider', () => {
        supportingEvidence.anotherMedicalProviderPlaceholder.button.click();

        supportingEvidence.medicalProviderForm.addressSectionTitle.should(
          'have.text',
          `Enter your medical provider's address if you have it`
        );

        supportingEvidence.medicalProviderForm.name.label.should(
          'have.text',
          'Name of your medical provider'
        );
        supportingEvidence.medicalProviderForm.phone.label.should(
          'have.text',
          'Contact number of your medical provider'
        );

        supportingEvidence.medicalProviderForm.addressLine.label.should(
          'have.text',
          'Address line (street, number, complement)'
        );

        supportingEvidence.medicalProviderForm.city.label.should(
          'have.text',
          'City'
        );

        supportingEvidence.medicalProviderForm.zipCode.label.should(
          'have.text',
          'Zip Code'
        );

        supportingEvidence.medicalProviderForm.country.label.should(
          'have.text',
          'Country'
        );

        supportingEvidence.medicalProviderForm.country.input.should(
          'have.value',
          'USA'
        );

        supportingEvidence.medicalProviderForm.saveBtn.should('be.disabled');
      });

      it('should add primary physician', () => {
        supportingEvidence.primaryPhysicianPlaceholder.button.click();

        supportingEvidence.medicalProviderForm.fillWithValues();

        supportingEvidence.medicalProviderForm.saveBtn.click();

        const primaryPhysicianItem = supportingEvidence.primaryPhysicianItem;
        primaryPhysicianItem.title.should('have.text', 'Dr Jones');
        primaryPhysicianItem.description.should(
          'have.text',
          '1-234-567890 | Street 1, Austin, TX, 91021, USA'
        );
      });

      it('should delete primary physician', () => {
        supportingEvidence.primaryPhysicianPlaceholder.button.click();
        supportingEvidence.medicalProviderForm.fillWithValues();
        supportingEvidence.medicalProviderForm.saveBtn.click();

        supportingEvidence.primaryPhysicianItem.deleteBtn.click();
        supportingEvidence.primaryPhysicianItem.title.should('have.length', 0);
      });

      it('should edit primary physician', () => {
        supportingEvidence.primaryPhysicianPlaceholder.button.click();
        supportingEvidence.medicalProviderForm.fillWithValues();
        supportingEvidence.medicalProviderForm.saveBtn.click();

        supportingEvidence.primaryPhysicianItem.editBtn.click();
        supportingEvidence.medicalProviderForm.fillWithValues({
          name: 'Dr Bones',
          phone: '2-234-567890',
          city: 'Dallas',
          addressLine: 'Guarden 3',
          state: 'Alabama',
          zipCode: '81021'
        });
        supportingEvidence.medicalProviderForm.saveBtn.click();

        const primaryPhysicianItem = supportingEvidence.primaryPhysicianItem;
        primaryPhysicianItem.title.should('have.text', 'Dr Bones');
        primaryPhysicianItem.description.should(
          'have.text',
          '2-234-567890 | Guarden 3, Dallas, AL, 81021, USA'
        );
      });

      it('should add another medical provider', () => {
        supportingEvidence.anotherMedicalProviderPlaceholder.button.click();

        supportingEvidence.medicalProviderForm.fillWithValues();

        supportingEvidence.medicalProviderForm.saveBtn.click();

        const anotherMedicalProviderItem = supportingEvidence.getAnotherMedicalProviderItemAt(
          0
        );
        anotherMedicalProviderItem.title.should('have.text', 'Dr Jones');
        anotherMedicalProviderItem.description.should(
          'have.text',
          '1-234-567890 | Street 1, Austin, TX, 91021, USA'
        );
      });

      it('should delete another medical provider', () => {
        supportingEvidence.anotherMedicalProviderPlaceholder.button.click();

        supportingEvidence.medicalProviderForm.fillWithValues();
        supportingEvidence.medicalProviderForm.saveBtn.click();

        supportingEvidence.addAnotherMedicalProviderBtn.click();

        supportingEvidence.medicalProviderForm.fillWithValues();
        supportingEvidence.medicalProviderForm.saveBtn.click();

        supportingEvidence.anotherMedicalProvideItems.should('have.length', 2);
        supportingEvidence.getAnotherMedicalProviderItemAt(0).deleteBtn.click();
        supportingEvidence.anotherMedicalProvideItems.should('have.length', 1);
      });

      it('should edit another medical provider', () => {
        supportingEvidence.anotherMedicalProviderPlaceholder.button.click();
        supportingEvidence.medicalProviderForm.fillWithValues();
        supportingEvidence.medicalProviderForm.saveBtn.click();

        supportingEvidence.getAnotherMedicalProviderItemAt(0).editBtn.click();
        supportingEvidence.medicalProviderForm.fillWithValues({
          name: 'Dr Bones',
          phone: '2-234-567890',
          city: 'Dallas',
          addressLine: 'Guarden 3',
          state: 'Alabama',
          zipCode: '81021'
        });
        supportingEvidence.medicalProviderForm.saveBtn.click();

        const anotherMedicalProvider = supportingEvidence.getAnotherMedicalProviderItemAt(
          0
        );
        anotherMedicalProvider.title.should('have.text', 'Dr Bones');
        anotherMedicalProvider.description.should(
          'have.text',
          '2-234-567890 | Guarden 3, Dallas, AL, 81021, USA'
        );
      });

      it('should lead to confirmation page after submit', () => {
        supportingEvidence.primaryPhysicianPlaceholder.button.click();
        supportingEvidence.medicalProviderForm.fillWithValues();
        supportingEvidence.medicalProviderForm.saveBtn.click();

        supportingEvidence.saveAndProceedBtn.click();

        const confirmationPage = new ConfirmationPage();

        confirmationPage.container.should('have.length', 1);
      });
    });
  });
});
