import { MyTeamLeaves } from '../../../support/page-objects/modules/my-team-leaves';

describe('My Team Leaves', () => {
  const { navigateTo, elem, header, filters, list } = new MyTeamLeaves();

  const myTeamLeaveProperties = [
    { label: 'Notified on:', value: 'Aug 23, 2019' },
    { label: 'First day missing work', value: 'Aug 01, 2019' },
    { label: 'Expected return', value: 'Sep 02, 2019' },
    { label: 'Absence case manager', value: 'Bob Dobalina' },
    { label: `Case manager's contact`, value: '01 23 45678' }
  ];

  const leavePlanProperties = [
    { label: 'Type of leave', value: 'Continuous' },
    { label: 'Leave begins', value: 'Aug 06, 2017' },
    { label: 'Requested through', value: 'Aug 18, 2017' }
  ];

  const employee = 'Arch Stanton';

  const menuItems = [
    'All team members',
    'In the next 2 days',
    'In the next 7 days',
    'In the next 15 days',
    'In the next 30 days',
    'In the next 2 days',
    'In the next 7 days',
    'In the next 15 days',
    'In the next 30 days'
  ];

  beforeEach(() => {
    navigateTo();
    cy.wait(2000);
  });

  it('should display my team leaves', () => {
    cy.url().should('include', '/#my-team-leaves');

    cy.get(elem).within(() => {
      header.title.should('be', 'My Team Leaves');
      header.exportBtn.should('have.text', 'Export List');
    });

    cy.get(list.elem).within(() => {
      list.myTeamLeaves.should($leaves => {
        expect($leaves).to.have.length(2);
      });

      list.myTeamLeaves.within(() => {
        myTeamLeaveProperties.map(({ label, value }, index) => {
          list.properties.hasLabel(index, label);
          list.properties.hasValue(index, value);
        });
      });

      list.selectMyTeamLeaveAt(0);

      list.leavePeriods.should($period => {
        expect($period).to.have.length(2);
      });

      list.leavePeriods.within(() => {
        leavePlanProperties.map(({ label, value }, index) => {
          list.properties.hasLabel(index, label);
          list.properties.hasValue(index, value);
        });
      });
    });
  });

  it('should allow for an employee to be selected', () => {
    header.employee.should('contain', 'Search by employee name');

    header.employeeSelect.setValue(employee);

    header.employee.should('contain', employee);
  });

  it(`should display amend period form`, () => {
    list.selectMyTeamLeaveAt(0);

    list.leavePeriodActions.within(() =>
      cy
        .get('[data-test-el="request-change"]')
        .first()
        .click()
    );

    list.requestAmendmentForm.contains(
      'What kind of change do you want to make?'
    );
  });

  it(`should request to delete period`, () => {
    list.selectMyTeamLeaveAt(0);

    list.deletePeriod.first().click();

    cy.get('[data-test-el="confirmation-submit-button"]').click();

    list.confirmationModal.contains('Request for deletion received');
  });

  // Remove until filtering is allowed in the api
  // it('should allow for filters to be applied', () => {
  //   cy.wait(300);

  //   filters.filtersBtn.should('have.text', 'All team members');

  //   filters.filtersBtn.click();

  //   filters.filtersMenu.within(() => {
  //     filters.filterMenuItems.should($items => {
  //       expect($items).to.have.length(menuItems.length);
  //     });

  //     menuItems.forEach(item => {
  //       filters.filterMenuItems.contains(item);
  //     });

  //     filters.selectFilterAt(3);
  //   });

  //   filters.filtersBtn.should('contain', menuItems[3]);
  // });
});
