const puppeteer = require('puppeteer').executablePath();
const {execSync} = require('child_process');
const https = require('https');

function getRequest(domain, endPoint) {
    return new Promise((fulfil, reject) => {
        const req = https.request({
            hostname: domain,
            method: 'GET',
            path: endPoint
        }, res => {
            res.on('data', d => {
                fulfil(d.toString().trim());
            });
        });
        req.on('error', reject);
        req.end();
    });
}

const fullVersion = execSync(`${puppeteer} --version`, {encoding: 'utf8'});
const [majorVersion] = fullVersion.split(/\D/).filter(Boolean);
getRequest('chromedriver.storage.googleapis.com', `/LATEST_RELEASE_${majorVersion}`)
    // eslint-disable-next-line no-console
    .then(r => console.log(r))
    .catch(err => {
        // eslint-disable-next-line no-console
        console.error(err);
        // eslint-disable-next-line no-process-exit
        process.exit(1);
    });
