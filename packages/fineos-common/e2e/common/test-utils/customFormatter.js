/* eslint-disable*/
/* *
 * THIS FORMATTER IS UNDER CONSTRUCTION - REQUIRES WORK
 * */
const {format} = require('assertion-error-formatter');
const moment = require('moment');
const {SummaryFormatter} = require('cucumber');
const sharedConfig = require('../../config/sharedConfig');
const fs = require('fs');
const OUTPUT_DIR = sharedConfig.config.cucumberOpts.jsonDir;
const _ = require('lodash');
const {getApiCallRecords} = require('./apiCallsDetailsStorage');
const {GherkinDocumentParser, PickleParser} = require('cucumber/lib/formatter/helpers');
const {buildStepArgumentIterator} = require('cucumber/lib/step_arguments');

const {
    getStepLineToKeywordMap,
    getScenarioLineToDescriptionMap
} = GherkinDocumentParser;

const {
    getScenarioDescription,
    getStepLineToPickledStepMap,
    getStepKeyword
} = PickleParser;

class CustomFormatter extends SummaryFormatter {

    constructor(options) {
        super(options);
        this.countScenarios = 0;
        this.countBeforeHooks = 0;
        this.scenarios = [];
        this.features = [];
        this.stepsFailedOrUndefined = false;
        options.eventBroadcaster
            .on('test-step-finished', this.onTestStepFinished.bind(this))
            .on('test-case-started', this.onTestCaseStarted.bind(this))
            .on('test-run-finished', this.onTestRunFinished.bind(this))
            .on('attachment', this.onAttachment.bind(this));
    }

    onAttachment(data) {
        console.log(`\x1b[31mErrors in feature: ${data.source.uri}\x1b[0m`);
    }

    onTestStepFinished(data) {
        if (this.scenarios.length === 0) {
            const {scenarios, features} = this.getScenariosFeatures();
            this.scenarios = scenarios;
            this.features = features;
        }
        const isFailedStatus = data.result.status === 'failed';
        const isUndefinedStatus = data.result.status === 'undefined';
        const pickleSteps = this.scenarios[this.countScenarios].pickle.steps;
        const testCaseSteps = this.scenarios[this.countScenarios].testCase.steps;
        const feature = this.features.find(feature => feature.uri.includes(data.testCase.sourceLocation.uri));
        const scenarioIndex = feature.elements.findIndex(i => (i.line === data.testCase.sourceLocation.line));
        const featureSteps = feature.elements[scenarioIndex].steps;
        let stepText = 'Before Hook';
        let isAfterHook = false;
        let isBeforeHook = false;
        if (testCaseSteps[data.index].sourceLocation) {
            stepText = `${featureSteps[data.index].keyword}${pickleSteps[data.index - this.countBeforeHooks].text}`;
        } else if (data.index - this.countBeforeHooks > pickleSteps.length - 1) {
            stepText = 'After Hook';
            isAfterHook = true;
            if (data.index === testCaseSteps.length - 1) {
                this.countScenarios++;
            }
        } else {
            this.countBeforeHooks++;
            isBeforeHook = true;
        }

        let statusIndicator = !this.stepsFailedOrUndefined || isAfterHook ? '\x1b[32m\u2714' : '\x1b[36m-';
        if (isFailedStatus || isUndefinedStatus) {
            global.problematicSteps.push(stepText);
            statusIndicator = `\x1b[3${isFailedStatus ? '1m\u2716' : '3m?'}`;
            this.stepsFailedOrUndefined = true;
        }
        if (!isAfterHook && !isBeforeHook) {
            console.log(`${statusIndicator} ${stepText}\x1b[0m`);
        }
    }

    onTestCaseStarted() {
        this.stepsFailedOrUndefined = false;
        this.countBeforeHooks = 0;
        this.scenarios = [];
        global.problematicSteps = [];
    }

    convertNameToId(obj) {
        return obj.name.replace(/ /g, '-').toLowerCase();
    }

    formatDataTable(dataTable) {
        return {
            rows: dataTable.rows.map(row => ({cells: _.map(row.cells, 'value')}))
        };
    }

    formatDocString(docString) {
        return {
            content: docString.content,
            line: docString.location.line
        };
    }

    formatStepArguments(stepArguments) {
        const iterator = buildStepArgumentIterator({
            dataTable: this.formatDataTable.bind(this),
            docString: this.formatDocString.bind(this)
        });
        return _.map(stepArguments, iterator);
    }

    getTCLine(pickle, line) {
        const pattern = browser.params.tcPattern;
        let testCase = '';
        for (const tag of pickle.tags) {
            const tagName = tag.name;
            if (pattern.test(tagName)) {
                testCase = tagName.replace('@', '');
            }
        }
        return `${testCase}:${line}`;
    }

    getScenariosFeatures() {
        const groupedTestCases = {};
        _.each(this.eventDataCollector.testCaseMap, testCase => {
            const {sourceLocation: {uri}} = testCase;
            if (!groupedTestCases[uri]) {
                groupedTestCases[uri] = [];
            }
            groupedTestCases[uri].push(testCase);
        });
        const scenarios = [];
        const features = _.map(groupedTestCases, (group, uri) => {
            const gherkinDocument = this.eventDataCollector.gherkinDocumentMap[uri];
            const featureData = this.getFeatureData(gherkinDocument.feature, uri);
            const stepLineToKeywordMap = getStepLineToKeywordMap(gherkinDocument);
            const scenarioLineToDescriptionMap = getScenarioLineToDescriptionMap(
                gherkinDocument
            );
            featureData.executionTime = moment(new Date()).format('MMMM Do YYYY, h:mm:ss a');
            featureData.elements = group.map(testCase => {
                const {pickle} = this.eventDataCollector.getTestCaseData(
                    testCase.sourceLocation
                );
                const scenarioData = this.getScenarioData({
                    featureId: featureData.id,
                    uri,
                    pickle,
                    scenarioLineToDescriptionMap
                });
                const stepLineToPickledStepMap = getStepLineToPickledStepMap(pickle);
                let isBeforeHook = true;
                scenarioData.steps = testCase.steps.map(testStep => {
                    isBeforeHook = isBeforeHook && !testStep.sourceLocation;
                    return this.getStepData({
                        isBeforeHook,
                        pickle,
                        stepLineToKeywordMap,
                        stepLineToPickledStepMap,
                        testCase,
                        testStep
                    });
                });
                scenarios.push({pickle, testCase});
                return scenarioData;
            });

            return featureData;
        });
        return {features, scenarios};
    }

    onTestRunFinished() {
        const {features} = this.getScenariosFeatures();
        const randomNumber = Math.floor((Math.random() * 100) + 1).toString();
        const featureName = features.length > 0
                            ? features[0].uri.replace(/^([\w.\/\\-]+?)([\s\w.\-–]+)\.feature$/, '$2').replace(/\s/g, '')
                            : '';
        browser.params.reportPath = `${OUTPUT_DIR}/dailyTestReport.${new Date().getTime()}_${randomNumber}__${featureName}.json`;
        try {
            console.log(`Trying to write json file: ${browser.params.reportPath}`);
            fs.writeFileSync(browser.params.reportPath, JSON.stringify(features, null, 2));
            console.log(`Wrote json file: ${browser.params.reportPath}`);
        } catch (error) {
            console.log(`Error writing file: ${browser.params.reportPath}\nFailed with message: ${error.message}`);
        }
    }

    getFeatureData(feature, uri) {
        return {
            description: feature.description,
            id: this.convertNameToId(feature),
            keyword: feature.keyword,
            line: feature.location.line,
            name: feature.name,
            tags: this.getTags(feature),
            uri
        };
    }

    getScenarioData({featureId, pickle, uri, scenarioLineToDescriptionMap}) {
        const description = getScenarioDescription({
            pickle,
            scenarioLineToDescriptionMap
        });
        const apiCallsData = getApiCallRecords(JSON.stringify(pickle) + ' ' + uri);
        return {
            description: apiCallsData.length
              ? (description ? description + '\n\n' : '')
                + 'Failed calls: \n' + apiCallsData.reduce(
                  (acc, curr) => acc + JSON.stringify(curr) + '\n',
                  ''
                )
              : description,
            id: `${featureId};${this.convertNameToId(pickle)}`,
            keyword: 'Scenario',
            line: pickle.locations[0].line,
            name: pickle.name,
            tags: this.getTags(pickle),
            type: 'scenario'
        };
    }

    getStepData(
        {
            isBeforeHook,
            stepLineToKeywordMap,
            stepLineToPickledStepMap,
            testStep,
            testCase,
            pickle
        }) {
        const data = {};
        if (testStep.sourceLocation) {
            const {line} = testStep.sourceLocation;
            const pickleStep = stepLineToPickledStepMap[line];
            data.arguments = this.formatStepArguments(pickleStep.arguments);
            data.keyword = getStepKeyword({pickleStep, stepLineToKeywordMap});
            data.line = line;
            data.name = pickleStep.text;
            data.match = {
                location: data.name
            };
        } else {
            data.keyword = isBeforeHook ? 'Before' : 'After';
            data.hidden = true;
        }

        if (testStep.result) {
            const {status} = testStep.result;
            data.result = {
                status
            };
            if (testStep.result.duration) {
                data.result.duration = testStep.result.duration * 1000000;
            }
            const {exception} = testStep.result;
            if (status === 'failed' && exception) {
                data.result.error_message = format(exception);
            }
        }
        if (_.size(testStep.attachments) > 0) {
            testStep.attachments = testStep.attachments.map(
                attachment => {
                    attachment.line = this.getTCLine(pickle, testCase.sourceLocation.line);
                    return attachment;
                }
            );
            data.embeddings = testStep.attachments;
        }
        return data;
    }

    getTags(obj) {
        return _.map(obj.tags, tagData => ({line: tagData.location.line, name: tagData.name}));
    }
}

module.exports = CustomFormatter;

