const apiCallsDetailsStorage = new Map();

function addApiCallRecords(uri, records) {
    apiCallsDetailsStorage.set(uri, records);
}

function getApiCallRecords(uri) {
    return apiCallsDetailsStorage.get(uri) || [];
}

module.exports = {addApiCallRecords, getApiCallRecords};
