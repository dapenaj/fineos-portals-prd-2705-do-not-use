const moment = require('moment');

class DatePickerHelper {
    /**
     * This method get number of months of given date
     *
     * @param {date} date - date used to calculate number of months
     * @return {Number} number of months
     */
    getAbsoluteMonths(date) {
        const months = Number(date.format('MM'));
        const years = Number(date.format('YYYY'));
        return months + (years * 12);
    }

    /**
     * This method get number of months of given date
     *
     * @param {date} firstDate - first date used to calculate number difference
     * @param {date} secondDate - second date used to calculate number difference
     * @return {Number} number of months difference
     */
    getMonthsDifferenceBetweenDates(firstDate, secondDate) {
        return this.getAbsoluteMonths(firstDate) - this.getAbsoluteMonths(secondDate);
    }

    /**
     * This method get number of months of given date
     *
     * @param {date} firstDate - first date used to calculate number difference
     * @param {date} secondDate - second date used to calculate number difference
     * @return {Number} number of months difference
     */
    getYearsDifferenceBetweenDates(firstDate, secondDate) {
        return Number(firstDate.format('YYYY')) - Number(secondDate.format('YYYY'));
    }

    /**
     * This method read actual date from date picker
     * @return {date} actual date taken from date picker
     */
    async readActualDate() {
        let actualMonth = (await pages.erp.datePicker.actualMonthYearCards().first().getText()).toString();
        actualMonth = moment().month(actualMonth).format('MM');
        const actualYear = (await pages.erp.datePicker.actualMonthYearCards().last().getText()).toString();

        return moment(moment(`${actualYear}-${actualMonth}-01`).format(constants.dateFormats.isoDate));
    }

    /**
     * This method navigate date given as parameter using next/prev month date picker buttons
     *
     * @param {date} finalDate - date to be found
     * @return {Number} number of months difference
     */
    async navigateDate(finalDate) {
        const actualDate = await this.readActualDate();
        const monthsDifference = this.getMonthsDifferenceBetweenDates(finalDate, actualDate);

        if (monthsDifference) {
            const direction = (monthsDifference > 0) ? await pages.erp.datePicker.nextMonth() : await pages.erp.datePicker.prevMonth();

            for (let i = 0; i < Math.abs(monthsDifference); i++) {
                await direction.Click({waitForVisibility: true});
            }
        }

        return monthsDifference;
    }

    /**
     * This method navigate date given as parameter using next/prev month date picker buttons
     *
     * @param {date} date - date to be found
     * @return {Number} number of months difference
     */
    async navigateYear(date) {
        const actualDate = await this.readActualDate();
        const yearsDifference = this.getYearsDifferenceBetweenDates(date, actualDate);

        if (yearsDifference) {
            const direction = (yearsDifference > 0) ? await pages.erp.datePicker.nextYear() : await pages.erp.datePicker.prevYear();

            for (let i = 0; i < Math.abs(yearsDifference); i++) {
                await direction.Click({waitForVisibility: true});
            }
        }

        return yearsDifference;
    }
}

module.exports = DatePickerHelper;
