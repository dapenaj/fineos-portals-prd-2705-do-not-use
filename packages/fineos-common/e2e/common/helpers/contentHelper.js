export const getContentDescribedById = translationId => {
    let el = document.querySelector(`[data-test-el=${translationId}]`);
    if (el) {
        const stack = el.id ? [el.id] : [];
        while (el && el.parentElement) {
            el = el.parentElement;
            if (Boolean(el) && el.id) {
                stack.push(el.id);
            }
        }

        while (stack.length) {
            const current = stack.pop();
            const content = document.querySelector(`[aria-describedby='${current}']`);

            if (content) {
                return content;
            }
        }
    }
    return null;
};
