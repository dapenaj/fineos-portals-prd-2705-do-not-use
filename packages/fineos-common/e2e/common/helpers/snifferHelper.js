const {runSyncJS} = require('./jsHelper');

function installSniffer() {
    return runSyncJS(function () {
        function parseJSON(json) {
            if (json) {
                try {
                    return JSON.parse(json);
                } catch (e) {
                    return json;
                }
            }

            return json;
        }

        if (!Array.isArray(window.__$$FINEOS_SNIFFER)) {
            window.__$$FINEOS_SNIFFER = [];
        }
        const storage = window.__$$FINEOS_SNIFFER;
        const actualFetch = window.fetch;

        class RequestSniffed extends Request {

            /**
             *
             * @type {null | object}
             * @private
             */
            _recordedBody = null;

            /**
             *
             * @param url {string}
             * @param init {RequestInit}
             */
            constructor(url, init) {
                super(url, init);
                this._recordedBody = init.body;
            }
        }

        window.Request = RequestSniffed;

        /**
         *
         * @param requestInfo {RequestSniffed}
         */
        window.fetch = function fetch(requestInfo) {
            const apiCall = {
                body: parseJSON(requestInfo._recordedBody),
                method: requestInfo.method,
                url: requestInfo.url
            };

            storage.push(apiCall);

            return actualFetch(requestInfo)
                .then(function (res) {
                    const textParser = res.text;
                    const jsonParser = res.json;

                    apiCall.status = res.ok ? 'SUCCESS' : 'FAIL';
                    apiCall.httpCode = res.status;

                    function recordSuccess(data) {
                        apiCall.data = data;
                        return data;
                    }

                    res.text = function () {
                        return textParser.call(res).then(recordSuccess);
                    };

                    res.json = function () {
                        return jsonParser.call(res).then(recordSuccess);
                    };

                    return res;
                })
                .catch(function (err) {
                    apiCall.httpCode = 0;
                    apiCall.status = 'FAIL';
                    apiCall.error = err.toString();

                    return Promise.reject(err);
                });
        };
    });
}

/**
 * @typedef {Object} ApiCall
 * @property {'GET' | 'POST'} method - request method
 * @property {string} url - the full URL
 * @property {'SUCCESS' | 'FAIL'} status - status of the request, is it success of fail
 * @property {*} data - response data if request finished successfully
 * @property {string} error - error message if request failed
 */

/**
 *
 * @param urlPattern {string | RegExp} - url pattern by which lookup would be performed, supports (*) wildcard,
 * and it's case-sensitive
 * @param method {'GET' | 'POST'} [method=GET]
 * @return {promise.Promise<Array<ApiCall>>}
 */
function lookupForRequests(urlPattern, method = 'GET') {
    return runSyncJS(function (patternType, urlPattern, method) {
        const testByPattern = (function () {
            if (patternType === 'string') {
                const pattern = urlPattern.split('*');
                /**
                 * @param url {string}
                 * @return {boolean}
                 */
                return function testByPattern(url) {
                    let indexOffset = 0;

                    for (const patternEntry of pattern) {
                        const patternEntryIndex = url.indexOf(patternEntry, indexOffset);

                        if (patternEntryIndex < 0) {
                            return false;
                        }

                        indexOffset = patternEntryIndex + patternEntry.length;
                    }

                    return true;
                };
                // eslint-disable-next-line no-else-return
            } else {
                const lastSlashIndex = urlPattern.lastIndexOf('/');
                const regexExpStr = urlPattern.substring(1, lastSlashIndex);
                const modes = urlPattern.substring(lastSlashIndex + 1);
                const regExp = modes ? new RegExp(regexExpStr, modes) : new RegExp(regexExpStr);

                return function testByRexExp(url) {
                    return regExp.test(url);
                };
            }
        }());
        const results = window.__$$FINEOS_SNIFFER.filter(apiCall => apiCall.method === method && testByPattern(apiCall.url)
        );

        results.reverse();

        return results;
    }, typeof urlPattern === 'string' ? 'string' : 'regexp', urlPattern.toString(), method);
}

/**
 *
 * @param urlPattern {string | RegExp} - url pattern by which lookup would be performed, supports (*) wildcard,
 * and it's case-sensitive
 * @param jsonPath {string} - path to json field
 * @param jsonRoot {string} - root of json, default is data
 * @return {promise.Promise<array>}
 */
async function getRequestDataArray(urlPattern, jsonPath, jsonRoot = 'data') {
    const data = [];
    const requestData = testDataHelpers.getJsonFieldValue((await lookupForRequests(urlPattern))[0], jsonRoot);

    for (const rD of requestData) {
        data.push(testDataHelpers.getJsonFieldValue(rD, jsonPath));
    }
    return data;
}

/**
 *
 * @param urlPattern {string | RegExp} - url pattern by which lookup would be performed, supports (*) wildcard,
 * and it's case-sensitive
 * @param jsonPath {string} - path to json field
 * @return {promise.Promise<array>}
 */
async function getAllRequestsDataIntoArray(urlPattern, jsonPath) {
    const data = [];
    const apiDataResponses = (await lookupForRequests(urlPattern));

    for (const response of apiDataResponses) {
        data.push(await testDataHelpers.getJsonFieldValue(response, jsonPath));
    }

    return data;
}

/**
 *
 * @param urlPattern {string | RegExp} - url pattern by which lookup would be performed, supports (*) wildcard,
 * and it's case-sensitive
 * @param jsonPath {string} - path to json field
 * @return {promise.Promise<string>}
 */
async function getRequestData(urlPattern, jsonPath) {
    const requestData = (await lookupForRequests(urlPattern))[0].data;
    return testDataHelpers.getJsonFieldValue(requestData, jsonPath);
}

/**
 * Lookup through all requests, and return logged failed requests
 *
 * @return {promise.Promise<Array<ApiCall>>}
 */
function getAllFailedRequests() {
    return runSyncJS(function () {
        return Array.isArray(window.__$$FINEOS_SNIFFER)
            ? window.__$$FINEOS_SNIFFER.filter(apiCall => apiCall.status === 'FAIL')
            : [];
    });
}

module.exports = {getAllFailedRequests, getAllRequestsDataIntoArray, getRequestData, getRequestDataArray, installSniffer, lookupForRequests};
