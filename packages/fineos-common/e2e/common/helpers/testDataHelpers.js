const faker = require('faker');
const path = require('path');

class TestDataHelpers {
    /**
     * This method convert number to currency format
     *
     * @param {string} locale - locale name
     * @param {string} currencyName - name of currency, e.g USD
     * @param {string} number to be converted
     *
     * @return converted number to currency format
     */
    formatCurrency(locale, currencyName, amount) {
        const formatter = new Intl.NumberFormat(locale, {
            currency: currencyName,
            style: 'currency'
        });

        return formatter.format(amount);
    }

    /**
     * This method get random element of given array
     *
     * @param {array} array - array used to get random element
     *
     * @return random element of given array
     */
    getRandomArrayElement(array) {
        return faker.random.arrayElement(array);
    }

    /**
     * This method get random index of given array
     *
     * @param {array} array - array used to get random element
     *
     * @return random index of given array
     */
    getRandomArrayElementIndex(array) {
        return faker.random.number(array.length - 1);
    }

    /**
     * This method replace characters in given string
     *
     * @param {string} originalText - original string with characters to be replaced
     * @param {RegExp} searchValue - characters to be replaced
     * @param {string} replaceValue - replacement characters
     *
     * @return string with replaced characters
     */
    replaceCharsInString(originalText, searchValue, replaceValue) {
        return originalText.replace(searchValue, replaceValue);
    }

    /**
     * This method divides give array into smaller chunks with given size
     *
     * @param {array} arr - original array to be divided into chunks
     * @param {int} chunkSize - size of chunk
     *
     * @return {array} array of chunks
     */
    chunkArray(arr, chunkSize) {
        if (chunkSize <= 0) {
            throw new Error(`Chunk size must be > than zero. ${chunkSize} was given.`);
        }

        const chunks = [];
        for (let i = 0, len = arr.length; i < len; i += chunkSize) {
            chunks.push(arr.slice(i, i + chunkSize));
        }
        return chunks;
    }

    /**
     * This can be used to mix two rgb colors
     *
     * @param {array} arr - original array to be divided into chunks
     * @param {int} chunkSize - size of chunk
     *
     * @return {array} array of chunks
     */
    mixRgbColors(weight, rgbColor1, rgbColor2) {
        const w = 2 * weight - 1;
        const w1 = (w + 1) / 2;
        const w2 = 1 - w1;
        const rgb = [
            Math.round(w1 * rgbColor1[0] + w2 * rgbColor2[0]),
            Math.round(w1 * rgbColor1[1] + w2 * rgbColor2[1]),
            Math.round(w1 * rgbColor1[2] + w2 * rgbColor2[2])
        ];

        return rgb;
    }

    /**
     * Converts rgb color to array
     *
     * @param {string} rgbColor - for instance rgb(255,255,255)
     * @return {array} color array
     */
    convertRgbToArray(rgbColor) {
        return rgbColor.toString().replace(constants.regExps.digits, '').split(',');
    }

    /**
     *
     * This method get json field value by given json path.
     * @param {json} json - json with field value to be taken
     * @param {string} jsonPath - path to json field e.g. fieldA.fieldB.fieldC
     *
     */
    getJsonFieldValue(json, jsonPath) {
        jsonPath.split('.').forEach(field => {
            if (json.hasOwnProperty(field)) {
                json = json[field];
            } else {
                json = null;
            }
        });

        return json;
    }

    /**
     *
     * This method uploads file given in fileName parameter
     * @param {string} fileName - file name with extensiion e.g. cat.png (choose file available in
     *     /packages/fineos-common/e2e/employer-portal/testData/filesToUpload/ If file you need to upload is missing,
     *     add it to mentioned directory.
     */
    async uploadDocument(fileName) {
        const fileToUploadPath = `${path.resolve(process.cwd(), '../../..') + constants.filesToUploadDir}${fileName}`;
        const fileInput = await waitHelpers.waitForElementToBePresent(pages.erp.sharedModal.uploadDocumentInput());
        fileInput.sendKeys(fileToUploadPath);
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     *
     * This method checks if given num string is numeric
     * @param {string} num - string to be verified
     * @returns {boolean}
     */
    isNumeric(num) {
        return !isNaN(num);
    }
}

module.exports = TestDataHelpers;
