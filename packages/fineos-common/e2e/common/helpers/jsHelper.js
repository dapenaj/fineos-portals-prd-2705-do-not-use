/**
 *
 * @param fn {Function}
 * @param args {any}
 * @return {promise.Promise<any>}
 */
module.exports.runSyncJS = function runSyncJS(fn, ...args) {
    return browser.executeScript(`return (${fn.toString()})(...arguments)`, ...args);
};
