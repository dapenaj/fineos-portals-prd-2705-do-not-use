by.addLocator(
    'labelText',
    function labelText(text, opt_parentElement, opt_rootSelector) {
        const using = opt_parentElement || document;
        const labels = [];
        const els = using.querySelectorAll('*:not(:empty)');
        for (let i = 0; i < els.length; i++) {
            const el = els[i];
            const innerText = el.innerText ? el.innerText.trim() : '';
            if (innerText === text) {
                const label =
                    el.tagName.toLowerCase() === 'label'
                        ? el
                        : el.closest('label');
                if (label && labels.indexOf(label) === -1) {
                    labels.push(label);
                }
            }
        }
        return labels;
    }
);

by.addLocator(
    'spanText',
    function spanText(text, opt_parentElement, opt_rootSelector) {
        const using = opt_parentElement || document;
        const spans = [];
        const els = using.querySelectorAll('*:not(:empty)');
        for (let i = 0; i < els.length; i++) {
            const el = els[i];
            const innerText = el.innerText ? el.innerText.trim() : '';
            if (innerText === text) {
                const span =
                    el.tagName.toLowerCase() === 'span'
                        ? el
                        : el.closest('span');
                if (span && spans.indexOf(span) === -1) {
                    spans.push(span);
                }
            }
        }
        return spans;
    }
);

by.addLocator(
    'labelTextInput',
    function labelTextInput(inputText, opt_parentElement, opt_rootSelector) {
        const using = opt_parentElement || document;
        const inputs = [];
        const els = using.querySelectorAll('*:not(:empty)');
        for (let i = 0; i < els.length; i++) {
            const el = els[i];
            const innerText = el.innerText ? el.innerText.trim() : '';
            if (innerText === inputText) {
                const label =
                    el.tagName.toLowerCase() === 'label'
                        ? el
                        : el.closest('label');
                if (label && label.hasAttribute('for')) {
                    const input = document.getElementById(label.getAttribute('for'));
                    if (input && inputs.indexOf(input) === -1) {
                        inputs.push(input);
                    }
                }
            }
        }
        return inputs;
    }
);
