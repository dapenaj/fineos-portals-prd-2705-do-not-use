const moment = require('moment');

class ExpectHelpers {

    /**
     * Assertion expecting given text to be contained in specified element text
     * @param {ElementFinder} webElement - web element
     * @param {string} text - phrase to look for
     * @param {string} message - custom error message when assertion not satisfied
     * @param {boolean} [caseSensitive=true] - is check case sensitive
     * @returns {Promise<void>}
     */
    async expectElementContainText(webElement, text, message, caseSensitive = true) {
        const actual = await webElement.getText();
        expect(caseSensitive ? actual : actual.toLocaleLowerCase())
            .to.contain(caseSensitive ? text : text.toLocaleLowerCase(), message);
    }

    /**
     * Assertion expecting given attribute value to be contained in specified element text
     * @param {ElementFinder} webElement - web element
     * @param {string} attributeName - name of given attribute
     * @param {string} attributeValue - expected value of given attribute
     * @param {string} message - custom error message when assertion not satisfied
     * @param {boolean} [caseSensitive=true] - is check case sensitive
     * @returns {Promise<void>}
     */
    async expectElementContainAttributeValue(webElement, attributeName, attributeValue, message, caseSensitive = true) {
        const actual = await webElement.getAttribute(attributeName);
        expect(caseSensitive ? actual : actual.toLocaleLowerCase())
            .to.contain(caseSensitive ? attributeValue : attributeValue.toLocaleLowerCase(), message);
    }

    /**
     * Assertion expecting given attribute value to be contained in specified element text
     * @param {ElementFinder} webElement - web element
     * @param {string} attributeName - name of given attribute
     * @param {string} attributeValue - expected value of given attribute
     * @param {string} message - custom error message when assertion not satisfied
     * @param {boolean} [caseSensitive=true] - is check case sensitive
     * @returns {Promise<void>}
     */
    async expectElementAttributeValueEquals(webElement, attributeName, attributeValue, message, caseSensitive = true) {
        const actual = await webElement.getAttribute(attributeName);
        expect(caseSensitive ? actual : actual.toLocaleLowerCase())
            .equal(caseSensitive ? attributeValue : attributeValue.toLocaleLowerCase(), message);
    }

    /**
     * Assertion expecting given css value to be equal in specified element
     * @param {ElementFinder} webElement - web element
     * @param {string} attributeName - name of given attribute
     * @param {string} attributeValue - expected value of given attribute
     * @param {string} message - custom error message when assertion not satisfied
     * @param {boolean} [caseSensitive=true] - is check case sensitive
     * @returns {Promise<void>}
     */
    async expectElementCssValueEquals(webElement, cssName, cssValue, message, caseSensitive = true) {
        const actual = await webElement.getCssValue(cssName);
        expect(caseSensitive ? actual : actual.toLocaleLowerCase())
            .to.equal(caseSensitive ? cssValue : cssValue.toLocaleLowerCase(), message);
    }

    /**
     * Assertion expecting given text to be contained in elements list
     * @param {ElementArrayFinder} webElementsList - web element
     * @param {string} text - phrase to look for
     * @param {string} message - custom error message when assertion not satisfied
     * @returns {Promise<void>}
     */
    async expectElementsListContainText(webElementsList, text, message) {
        const listOfStrings = await webElementsList.map(async el => await el.getText());
        expect(listOfStrings.toString()).to.contain(text, message);
    }

    /**
     * Assertion expecting given text to be contained in list element
     * @param {ElementArrayFinder} webElementsList - web element
     * @param {string} text - phrase to look for
     * @param {string} message - custom error message when assertion not satisfied
     * @returns {Promise<void>}
     */
    async expectEachListElementContainText(webElementsList, text, message) {
        await webElementsList.each(async el => {
            const elTxt = await el.getText();
            expect(elTxt.toString()).contains(text.toString());
        });
    }

    /**
     * Assertion expecting given text to be same as in specified element
     * @param {ElementFinder} webElement - web element
     * @param {string} text - phrase to look for
     * @param {string} message - custom error message when assertion not satisfied
     * @param {boolean} [caseSensitive=true] - is check case sensitive
     * @returns {Promise<void>}
     */
    async expectElementTextEqual(webElement, text, message, caseSensitive = true) {
        const actual = await webElement.getText();
        expect(caseSensitive ? actual : actual.toLocaleLowerCase())
            .to.equal(caseSensitive ? text : text.toLocaleLowerCase(), message);
    }

    /**
     * Assert on condition -  element is visible
     * @param {ElementFinder} webElement - element to check
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async expectElementToBeVisible(webElement, customMessage = '') {
        const selector = webElement.locator().value;
        await expect(webElement.isPresent()).to.eventually.equal(
            true,
            `expectElementToBeVisible: Element [${selector}] is not present\n${customMessage}`
        );
        await expect(webElement.isDisplayed()).to.eventually.equal(
            true,
            `expectElementToBeVisible: Element [${selector}] is not visible\n${customMessage}`
        );
        return webElement;
    }

    /**
     * Assert on condition -  elements from list are visible
     * @param {Array<ElementFinder>} webElementsList - element to check
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<void>}
     */
    async expectAllElementsToBeVisible(webElementsList, customMessage = '') {
        for (const webElement of webElementsList) {
            await this.expectElementToBeVisible(webElement, customMessage);
        }
    }

    /**
     * Assert on condition -  elements from list are NOT visible
     * @param {Array<ElementFinder>} webElementsList - element to check
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<void>}
     */
    async expectAllElementsToBeNotVisible(webElementsList, customMessage = '') {
        for (const webElement of webElementsList) {
            await this.expectElementToBeNotVisible(webElement, customMessage);
        }
    }

    /**
     * Assert on condition - element is present
     * @param {ElementFinder} webElement - element to check
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async expectElementToBePresent(webElement, customMessage = '') {
        const selector = webElement.locator().value;
        await expect(webElement.isPresent()).to.eventually.equal(
            true,
            `expectElementToBePresent: Element [${selector}] is not present\n${customMessage}`
        );
        return webElement;
    }

    /**
     * Assert on condition -  element is not visible
     * @param {ElementFinder} webElement - element to check
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async expectElementToBeNotVisible(webElement, customMessage = '') {
        const selector = webElement.locator().value;
        await expect(webElement.isPresent()).to.eventually.equal(
            false,
            `expectElementToBeNotVisible: Element [${selector}] is visible\n${customMessage}`
        );
        return webElement;
    }

    /**
     * @param {ElementFinder} webElement - element to check
     * @param {string} translationPath - path to json field key with translation
     * @returns {Promise<void>}
     */
    async checkTranslation(webElement, translationPath) {
        const labelText = await webElement.getText();
        const translation = translationsHelper.getTranslation(translationPath);
        const errorMessage = `The label [${labelText}] is not equal to translation pattern [${translation}] for key [${translationPath}]`;
        expect(labelText).is.equal(translation, errorMessage);
    }

    /**
     * @param {string} expectedDateFormat - date format e.g. MMM DD, YYYY
     * @param {ElementFinder} webElement - element with text date to be verified
     * @returns {Promise<void>}
     */
    async checkDateFormat(expectedDateFormat, webElement) {
        this.checkStringDateFormat(expectedDateFormat, await webElement.getText());
    }

    /**
     * This method converts string date to date object
     * @param {string} expectedDateFormat - date format e.g. MMM DD, YYYY
     * @param {string} stringDate to be converted date
     * @returns {Date}
     */
    covertStringToDate(stringDate, expectedDateFormat) {
        return moment(stringDate, expectedDateFormat, true);
    }

    /**
     * This method checks if dates array is sorted in descending or ascending order
     * @param {Date[]} datesArray - array with date objects
     * @param {boolean} isDescending - expected sorting type
     */
    checkIfArrayDateElementsAreSorted(datesArray, isDescending) {
        const errorMessage = `Array elements '${datesArray.toString()}' are not sorted in expected order (Descending:${isDescending})`;
        expect(datesArray, errorMessage).to.be.sorted({descending: isDescending});
    }

    /**
     * @param {string} expectedDateFormat - date format e.g. MMM DD, YYYY
     * @param {string} dateString - element with text date to be verified
     */
    checkStringDateFormat(expectedDateFormat, dateString) {
        const errorMessage = `Expected the date [${dateString}] to be in following format: ${expectedDateFormat}`;
        expect(moment(dateString, expectedDateFormat, true).isValid()).to.be.equal(true, errorMessage);
    }

    /**
     * Checks if an array of string formatted dates is sorted properly
     * @param {Array<string>} datesArray - string formatted dates
     * @param {string} [dateFormat=constants.dateFormats.shortDate] - format of date used by parser
     * @param {boolean} [mostRecentFirst = true] - type of sorting
     */
    expectDatesListToBeSorted(datesArray, dateFormat = constants.dateFormats.shortDate, mostRecentFirst = true) {
        const sortType = mostRecentFirst ? 'most recent first' : 'most recent last';
        const errorMessage = `Given dates [${datesArray.join(',')}] are not sorted "${sortType}"`;
        const momentDatesArray = datesArray.map(date => moment(date, dateFormat));
        let expectedOrder = mostRecentFirst ? momentDatesArray.sort((a, b) => b.unix() - a.unix()) : momentDatesArray.sort(
            (a, b) => a.unix() - b.unix());
        expectedOrder = expectedOrder.map(date => moment(date).format(dateFormat));
        expect(datesArray).to.have.ordered.members(expectedOrder, errorMessage);
    }
}

module.exports = ExpectHelpers;
