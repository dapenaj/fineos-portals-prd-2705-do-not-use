const fs = require('fs');
const path = require('path');

class TranslationsHelper {

    /**
     * This method can be used to read and parse data from json file
     *
     * @param {string} fileUrl
     * @returns {Object}
     */
    readJsonFile(fileUrl) {
        if (fs.existsSync(fileUrl)) {
            const fileContents = fs.readFileSync(fileUrl, 'utf8');
            try {
                const data = JSON.parse(fileContents);
                return data;
            } catch (err) {
                throw new Error(`CRITICAL ERROR: Failed to parse configuration JSON ${fileUrl}\n${err}`);
            }
        } else {
            throw new Error(`CRITICAL ERROR: JSON file was not found: ${fileUrl}`);
        }
    }

    /**
     * This method can be used to get translations strings. Method reads jsons with translations and fetch string using
     * given json path
     *
     * @param {string} jsonFieldPath - path to json field key  (e.g. key1.key2)
     * @returns {string}
     */
    getTranslation(jsonFieldPath) {
        const employerPortalTranslationsUrl = `${path.resolve(
            process.cwd(),
            '../../..'
        ) + constants.employerPortalTranslationsDir + language}.json`;
        let translation = this.readJsonFile(employerPortalTranslationsUrl);

        jsonFieldPath.split('.').forEach(field => {
            if (translation.hasOwnProperty(field)) {
                translation = translation[field];
            } else {
                const fineosCommonTranslationsUrl = `${path.resolve(
                    process.cwd(),
                    '../../..'
                ) + constants.fineosCommonTranslationsDir + language}.json`;

                translation = this.readJsonFile(fineosCommonTranslationsUrl);

                if (translation.hasOwnProperty(field)) {
                    translation = translation[field];
                } else {
                    throw new Error(`CRITICAL ERROR: JSON property [${field}] was not found in JSON path [${jsonFieldPath}]`);
                }
            }
        });
        return translation.replace(constants.regExps.htmlTags, '');
    }

    /**
     * This method get translation using given jsonFieldPath and replace all variables {...} with values given in
     * variablesValues array
     *
     * @param {string} jsonFieldPath - path to json field key  (e.g. key1.key2)
     * @param {string[]} variablesValues - values of variables to be replaced in translation string
     * @returns {string} - translation with replaced variables values
     */
    mapTranslationVariables(jsonFieldPath, variablesValues) {
        let translation = this.getTranslation(jsonFieldPath);

        for (const variable of variablesValues) {
            translation = translation.replace(constants.regExps.wordsPlaceholders, variable);
        }

        return translation;
    }
}

module.exports = TranslationsHelper;
