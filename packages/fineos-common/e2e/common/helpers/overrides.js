/* eslint-disable*/
const {ElementFinder} = require('protractor');

/**
 * aligns viewport according to element
 * @param {string} [scrollPosition='center'] - viewport alignment for scroll.
 * Accepts: start, center, nearest, end
 * @returns {Promise<void>}
 */
async function scrollTo(scrollPosition = 'center') {
    await browser.executeScript(`arguments[0].scrollIntoView({block: "${scrollPosition}"})`, this.getWebElement());
}

/**
 * concat error stacks and throws an error
 * @param {Object} err - error object to be thrown
 */
function throwError(err) {
    err.stack += `\n Locator: ${this.locator()}\n STACK: ${this.Stack}`;
    throw err;
}

/**
 * Performs additional checks and waits (depending on passed options) before executing click action on web element.
 * @param {Object} [options] - options
 * @param {boolean} [options.mobile = false] - specifies if click or tap should be performed
 * @param {boolean} [options.waitForVisibility = false] - specifies to wait for element visibility or presence
 * @param {number} [options.timeout = timeouts.T10] - amount of time to wait before throwing an error for every wait
 *     function
 * @returns {Promise<ElementFinder>}
 */
ElementFinder.prototype.Click = async function ({mobile = false, waitForVisibility = false, timeout = timeouts.T30} = {}) {
    const errorHandler = throwError.bind(this);
    this.Stack = new Error().stack;
    await waitHelpers.waitForElementToBePresent(this, timeout).then(undefined, errorHandler);
    if (waitForVisibility) {
        await waitHelpers.waitForElementToBeVisible(this, timeout).then(undefined, errorHandler);
    }
    await scrollTo.call(this, 'center').then(undefined, errorHandler);
    await waitHelpers.waitForElementToBeStatic(this, timeout).then(undefined, errorHandler);
    if (mobile) {
        await browser.touchActions().tap(this.getWebElement()).perform().then(undefined, errorHandler);
    } else {
        await this.click().then(undefined, errorHandler);
    }
    return new ElementFinder(this.browser_, this.parentElementArrayFinder);
};

/**
 * Performs additional checks and waits (depending on passed options) before executing click action on web element.
 * @param {string} text - text to be sent
 * @param {number} [timeout = timeouts.T10] - amount of time to wait before throwing an error for every wait function
 * @returns {Promise<ElementFinder>}
 */
ElementFinder.prototype.SendText = async function (text, timeout = timeouts.T10) {
    const errorHandler = throwError.bind(this);
    this.Stack = new Error().stack;
    await waitHelpers.waitForElementToBePresent(this, timeout).then(undefined, errorHandler);
    await this.clear().then(undefined, errorHandler);
    await this.sendKeys(text).then(undefined, errorHandler);
    return new ElementFinder(this.browser_, this.parentElementArrayFinder);
};

/**
 * Aligns the viewport with the web element
 * @param {number} [timeout = timeouts.T10] - amount of time to wait before throwing an error for every wait function
 * @returns {Promise<ElementFinder>}
 */
ElementFinder.prototype.Scroll = async function (timeout = timeouts.T10) {
    const errorHandler = throwError.bind(this);
    this.Stack = new Error().stack;
    await waitHelpers.waitForElementToBePresent(this, timeout).then(undefined, errorHandler);
    await scrollTo.call(this, 'center').then(undefined, errorHandler);
    return new ElementFinder(this.browser_, this.parentElementArrayFinder);
};

/**
 * This methods clear text/value of input elements. Can be uses as WO when Selenium native method clear() is not
 * working.
 * @returns {Promise<ElementFinder>}
 */
ElementFinder.prototype.Clear = async function () {
    await this.Click({waitForVisibility: true});
    let keys = protractor.Key.SHIFT + protractor.Key.HOME + protractor.Key.BACK_SPACE + protractor.Key.NULL;
    keys += protractor.Key.SHIFT + protractor.Key.END + protractor.Key.BACK_SPACE + protractor.Key.NULL;
    await this.sendKeys(keys);
    return new ElementFinder(this.browser_, this.parentElementArrayFinder);
};

/**
 * This methods clear text/value of input elements and set text. Can be uses as WO when Selenium native method clear()
 * is not working.
 *
 * @param {string} text - text to be set in input element
 * @returns {Promise<void>}
 */
ElementFinder.prototype.SetText = async function (text) {
    await this.Clear();
    await this.sendKeys(text);
};

/**
 * This methods hoovers over element.
 *
 * @returns {Promise<void>}
 */
ElementFinder.prototype.HooverOverElement = async function () {
    await browser.actions().mouseMove(this).perform();
};
