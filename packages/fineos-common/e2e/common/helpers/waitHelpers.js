class WaitHelpers {

    /**
     * Wait until element is visible
     * @param {ElementFinder} webElement - element to check
     * @param {number} [timeout=timeouts.T30] - timeout for the wait
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async waitForElementToBeVisible(webElement, timeout = timeouts.T60, customMessage = '') {
        const selector = webElement.locator().value;
        await browser.wait(EC.visibilityOf(webElement), timeout,
            `waitForElementToBeVisible: Element [${selector}] is not visible\n${customMessage}`
        );
        return webElement;
    }

    /**
     * Wait until element is present
     * @param {ElementFinder} webElement - element to check
     * @param {number} [timeout=timeouts.T30] - timeout for the wait
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async waitForElementToBePresent(webElement, timeout = timeouts.T30, customMessage = '') {
        const selector = webElement.locator().value;
        await browser.wait(EC.presenceOf(webElement), timeout,
            `waitForElementToBePresent: Element [${selector}] is not present\n${customMessage}`
        );
        return webElement;
    }

    /**
     * Wait until element is not visible
     * @param {ElementFinder} webElement - element to check
     * @param {number} [timeout=timeouts.T30] - timeout for the wait
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async waitForElementToBeNotVisible(webElement, timeout = timeouts.T30, customMessage = '') {
        const selector = webElement.locator().value;
        await browser.wait(EC.invisibilityOf(webElement), timeout,
            `waitForElementToBeNotVisible: Element [${selector}] is still visible\n${customMessage}`
        );
        return webElement;
    }

    /**
     * Wait until element is visible
     * @param {ElementFinder} webElement - element to check
     * @param {number} [timeout=timeouts.T30] - timeout for the wait
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<ElementFinder>}
     */
    async waitForElementToBeClickable(webElement, timeout = timeouts.T30, customMessage = '') {
        const selector = webElement.locator().value;
        await browser.wait(EC.elementToBeClickable(webElement), timeout,
            `waitForElementToBeClickable: Element [${selector}] is not clickable\n${customMessage}`
        );
        return webElement;
    }

    /**
     *
     * @param {ElementFinder} webElement - element to check
     * @param {string} text - expected text
     * @param {number} [timeout=timeouts.T30] - timeout for the wait
     * @param {string} [customMessage=''] - message to print on failure
     * @returns {Promise<void>}
     */
    async waitForElementTextEqual(webElement, text, timeout = timeouts.T30, customMessage = '') {
        await browser.wait(EC.textToBePresentInElement(webElement, text), timeout, customMessage);
    }

    /**
     * Assertion waits until given css value to be equal in specified element
     * @param {ElementFinder} webElement - web element
     * @param {string} attributeName - name of given attribute
     * @param {string} attributeValue - expected value of given attribute
     * @param {string} message - custom error message when assertion not satisfied
     * @param {boolean} [caseSensitive=true] - is check case sensitive
     * @returns {Promise<void>}
     */
    async waitElementCssValueEquals(webElement, cssName, cssValue, message, caseSensitive = true) {
        const pollPeriod = timeouts.T01;
        let elapsedTime = 0;
        let actual = await webElement.getCssValue(cssName);

        while (actual.toLowerCase() !== cssValue.toLowerCase() && elapsedTime < timeouts.T10) {
            await browser.sleep(pollPeriod);
            actual = await webElement.getCssValue(cssName);
            elapsedTime += pollPeriod;
        }

        expect(caseSensitive ? actual : actual.toLocaleLowerCase())
            .to.equal(caseSensitive ? cssValue : cssValue.toLocaleLowerCase(), message);
    }

    /**
     * Wait until element is not in the move (animation)
     * @param {ElementFinder} webElement - element to check
     * @param {number} [timeout=timeouts.T15] - timeout for the wait
     * @returns {Promise<ElementFinder>}
     */
    async waitForElementToBeStatic(webElement, timeout = timeouts.T45) {
        const prevLocation = {x: 0, y: 0};
        await browser.wait(
            async () => {
                const location = await webElement.getLocation();
                if (prevLocation.y === location.y && prevLocation.x === location.x) {
                    return true;
                }
                prevLocation.y = location.y;
                prevLocation.x = location.x;
                return await browser.sleep(500).then(() => false);
            }, timeout,
            `waitForElementToBeStatic: Element ${webElement.locator().value} is not static\n ${new Error().stack}`
        );
        await this.waitForElementToBeClickable(webElement, timeout);
        return webElement;
    }

    /**
     * @param {number} [timeout=timeouts.T15] - timeout for the wait
     * @returns {Promise<void>}
     */
    async waitForSpinnersToDisappear(timeout = timeouts.T60) {
        const spinners = pages.erp.notificationsWithOutstandingReqPage.pageSpinners();
        await browser.wait(
            async () => ((await spinners.count()) === 0), timeout,
            `waitForSpinnerToDisappear: Spinners are still visible after given timeout '${timeout}'`
        );
    }

    /**
     * @param {number} [timeout=timeouts.T45] - timeout for the wait
     * @returns {Promise<void>}
     */
    async waitForIntakePageEmploymentButtonLoadingToDisappear(timeout = timeouts.T45) {
        const spinners = pages.erp.intakeEmployeeDetailsPage.buttonLoadingIcon();
        await browser.wait(
            async () => ((await spinners.count()) === 0), timeout,
            `waitForButtonLoadingToDisappear: Button loading is still visible after given timeout '${timeout}'`
        );
    }
}

module.exports = WaitHelpers;
