/**
 * Get random element from array
 * @param {Array<*>}list - list of items
 * @returns {*}
 */
module.exports.getRandomArrayItem = function (list) {
    return list[Math.floor((Math.random() * list.length))];
};

/**
 * Get random element from array
 * @param {Array<*>}list - list of items
 * @returns {*}
 */
module.exports.getRandomArrayIndex = function (list) {
    return Math.floor((Math.random() * list.length));
};
