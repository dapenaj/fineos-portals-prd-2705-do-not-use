/**
 * Mapping page objects into global scope
 */
class DemoPages {

    constructor() {
        const HomePage = require('../demo/pages/homePage.js');
        this.homePage = new HomePage();

        const ResultsListPage = require('../demo/pages/resultsListPage.js');
        this.resultsListPage = new ResultsListPage();
    }
}

class ErpPages {

    constructor() {
        const LoginPage = require('../employer-portal/pages/loginPage.js');
        this.loginPage = new LoginPage();

        const LogOutPopUpPage = require('../employer-portal/pages/logOutPopUpPage.js');
        this.logOutPopUpPage = new LogOutPopUpPage();

        const NotificationsWithOutstandingReqPage = require(
            '../employer-portal/pages/notificationsWithOutstandingReqPage.js');
        this.notificationsWithOutstandingReqPage = new NotificationsWithOutstandingReqPage();

        const MyDashboardPage = require('../employer-portal/pages/myDashboardPage.js');
        this.myDashboardPage = new MyDashboardPage();

        const SearchPage = require('../employer-portal/pages/searchPage.js');
        this.searchPage = new SearchPage();

        const NotificationPage = require('../employer-portal/pages/notificationPage.js');
        this.notificationPage = new NotificationPage();

        const EmployeeProfilePage = require('../employer-portal/pages/employeeProfilePage.js');
        this.employeeProfilePage = new EmployeeProfilePage();

        const EmployeePersonalDetailsPage = require('../employer-portal/pages/employeePersonalDetailsPage.js');
        this.employeePersonalDetailsPage = new EmployeePersonalDetailsPage();

        const EmployeeEmailsPage = require('../employer-portal/pages/employeeEmailsPage.js');
        this.employeeEmailsPage = new EmployeeEmailsPage();

        const EmployeePhoneNumbersPage = require('../employer-portal/pages/employeePhoneNumbersPage.js');
        this.employeePhoneNumbersPage = new EmployeePhoneNumbersPage();

        const EditCurrentOccupationPage = require('../employer-portal/pages/editCurrentOccupationPage.js');
        this.editCurrentOccupationPage = new EditCurrentOccupationPage();

        const EditEligibilityCriteriaPage = require('../employer-portal/pages/editEligibilityCriteriaPage.js');
        this.editEligibilityCriteriaPage = new EditEligibilityCriteriaPage();

        const EditWorkPatternPage = require('../employer-portal/pages/editWorkPatternPage.js');
        this.editWorkPatternPage = new EditWorkPatternPage();

        const ControlUserAccessPage = require('../employer-portal/pages/controlUserAccessPage.js');
        this.controlUserAccessPage = new ControlUserAccessPage();

        const PaymentHistoryPage = require('../employer-portal/pages/paymentHistoryPage.js');
        this.paymentHistoryPage = new PaymentHistoryPage();

        const CancelWithDrawRequest = require('../employer-portal/pages/cancelWithDrawRequestPage.js');
        this.cancelWithDrawRequestPage = new CancelWithDrawRequest();

        const IntakeEmployeeDetailsPage = require('../employer-portal/pages/intakeEmployeesDetailsPage.js');
        this.intakeEmployeeDetailsPage = new IntakeEmployeeDetailsPage();

        const IntakeEditEmployeeDetailsPage = require('../employer-portal/pages/intakeEditEmployeeDetailsPage.js');
        this.intakeEditEmployeeDetailsPage = new IntakeEditEmployeeDetailsPage();

        const IntakeOccupationAndEarnings = require('../employer-portal/pages/intakeOccupationAndEarnings.js');
        this.intakeOccupationAndEarnings = new IntakeOccupationAndEarnings();

        const InitialRequestPage = require('../employer-portal/pages/initialRequestPage.js');
        this.initialRequestPage = new InitialRequestPage();

        const IntakeAcknowledgmentScreen = require('../employer-portal/pages/intakeAcknowledgmentScreen.js');
        this.intakeAcknowledgmentScreen = new IntakeAcknowledgmentScreen();

        const UploadDocumentPage = require('../employer-portal/pages/uploadDocumentPage.js');
        this.uploadDocumentPage = new UploadDocumentPage();

        const PhoneCallsModal = require('../employer-portal/pages/phoneCallsModal.js');
        this.phoneCallsModal = new PhoneCallsModal();

        const AddNewPhoneNumberModal = require('../employer-portal/pages/addNewPhoneNumberModal.js');
        this.addNewPhoneNumberModal = new AddNewPhoneNumberModal();

        const AddNewEmailModal = require('../employer-portal/pages/addNewEmailModal.js');
        this.addNewEmailModal = new AddNewEmailModal();

        const CommunicationPreferencesPage = require('../employer-portal/pages/communicationPreferencesPage.js');
        this.communicationPreferencesPage = new CommunicationPreferencesPage();

        const AlertsAndUpdatesModal = require('../employer-portal/pages/alertsAndUpdatesModal.js');
        this.alertsAndUpdatesModal = new AlertsAndUpdatesModal();

        const ShowHidePanelsModal = require('../employer-portal/pages/showHidePanelsModal.js');
        this.showHidePanelsModal = new ShowHidePanelsModal();

        const WrittenCorrespondenceModal = require('../employer-portal/pages/writtenCorrespondenceModal.js');
        this.writtenCorrespondenceModal = new WrittenCorrespondenceModal();

        const SharedModal = require('../employer-portal/pages/sharedModal.js');
        this.sharedModal = new SharedModal();

        const ConcurrencyErrorModal = require('../employer-portal/pages/concurrencyErrorModal.js');
        this.concurrencyErrorModal = new ConcurrencyErrorModal();

        const IntermittentTimeModal = require('../employer-portal/pages/intermittentTimeModal.js');
        this.intermittentTimeModal = new IntermittentTimeModal();

        const DatePicker = require('../employer-portal/pages/datePicker.js');
        this.datePicker = new DatePicker();

        const IntakeHowDoYouWantToProceedModal = require('../employer-portal/pages/intakeHowDoYouWantToProceedModal.js');
        this.intakeHowDoYouWantToProceedModal = new IntakeHowDoYouWantToProceedModal();

        const RequestForMoreTimeModal = require('../employer-portal/pages/requestForMoreTimeModal.js');
        this.requestForMoreTimeModal = new RequestForMoreTimeModal();

        const NewMessageModal = require('../employer-portal/pages/newMessageModal.js');
        this.newMessageModal = new NewMessageModal();

        const WebMessagesPage = require('../employer-portal/pages/webMessagesPage.js');
        this.webMessagesPage = new WebMessagesPage();

        const TimeTakenPage = require('../employer-portal/pages/timeTakenPage.js');
        this.timeTakenPage = new TimeTakenPage();

        const ErrorPage = require('../employer-portal/pages/errorPage.js');
        this.errorPage = new ErrorPage();

        const WebMessagesDrawer = require('../employer-portal/pages/webMessagesDrawer.js');
        this.webMessagesDrawer = new WebMessagesDrawer();
    }

    applicationRoot() {
        return $('#root');
    }
}

module.exports = {
    demo: new DemoPages(),
    erp: new ErpPages()
};
