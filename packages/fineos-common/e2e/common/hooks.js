/* eslint-disable*/
const {getAllFailedRequests} = require('./helpers/snifferHelper');
const {setDefaultTimeout, Before, After} = require('cucumber');
const fs = require('fs');
const path = require('path');
const {addApiCallRecords} = require('./test-utils/apiCallsDetailsStorage');
setDefaultTimeout(300 * 1000);
const scenarioLogColor = '\x1b[34m';
const errorLogColor = '\x1B[31m';
const normalLogColor = '\x1b[0m';

/**
 * Before each test run this code (logger code to be moved to reporter)
 */
Before(async function (testCase) {
    await browser.waitForAngularEnabled(false);
    const {tags} = testCase.pickle;
    const specId = getSpecId(tags);
    const path = testCase.sourceLocation.uri;
    const name = testCase.pickle.name;
    console.log(`\n${scenarioLogColor}${specId || path} : ln ${testCase.sourceLocation.line}\nScenario: ${name}${normalLogColor}`);
    global.testVars = Object.create(constants.testVarsInitObject);
});

const EXECUTION_TIME = new Date().toISOString();

async function collectErrors(pickle, sourceLocation) {
    const handles = await browser.getAllWindowHandles();
    let failedRequests = [];

    // if we have more than one browser tab, we need to collect details from all
    for (const handle of handles) {
        await browser.switchTo().window(handle);
        failedRequests = failedRequests.concat(await getAllFailedRequests());
    }

    if (failedRequests.length) {
        const testCaseDetails = pickle.name + ' ' + sourceLocation.uri;
        const errorLogPath = path.resolve(__filename, '..', '..', 'reports', `error-${EXECUTION_TIME}.log`);
        console.warn(`Failed API calls in ${JSON.stringify(testCaseDetails)} detected, please check ${errorLogPath} for details`);
        console.warn('Failed API calls details: ' + JSON.stringify(failedRequests, null, 1))

        addApiCallRecords(JSON.stringify(pickle) + ' ' + sourceLocation.uri, failedRequests);
        fs.appendFileSync(errorLogPath, `Failed API calls in ${JSON.stringify(testCaseDetails)}, details:\n${JSON.stringify(failedRequests, null, 1)}`);
    }
}

After(async function (testCase) {
    await collectErrors(testCase.pickle, testCase.sourceLocation);
});

/**
 * basing on tags in scenario - extract TC ID comparing to defined regex patten
 */
function getSpecId(tags) {
    const pattern = browser.params.tcPattern;
    for (const tag of tags) {
        const tagName = tag.name;
        if (pattern.test(tagName)) {
            return tagName.replace('@', '');
        }
    }
}

/**
 * After each test run this code (logger code to be moved to reporter)
 */
After({timeout: timeouts.T30}, async function (testCase) {
    const {tags} = testCase.pickle;
    const specId = getSpecId(tags);
    const path = testCase.sourceLocation.uri;
    const isFailed = testCase.result.status === 'failed';
    if (isFailed) {
        const {exception} = testCase.result;
        const failDescription = exception.stack || exception;
        console.log('*************************************************');
        try {
            const screenShot = await browser.takeScreenshot();
            const buffer = Buffer.from(screenShot, 'base64');
            this.attach(buffer, 'image/png');
        } catch (error) {
            console.log(`${errorLogColor}! Error trying to take screen shot !`);
            console.log(error);
        }
        console.log(`${errorLogColor}Error in scenario:${normalLogColor} ${specId || path} : ln ${testCase.sourceLocation.line}`);
        console.log(`${errorLogColor}Failure Description:${normalLogColor} ${failDescription}`);
        console.log('*************************************************\n');
    }
});
