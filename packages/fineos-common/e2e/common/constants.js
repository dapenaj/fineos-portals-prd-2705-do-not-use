const employerPortalTranslationsDir = '/packages/fineos-common/e2e/employer-portal/testData/translations/employer-portal/';
const fineosCommonTranslationsDir = '/packages/fineos-common/e2e/employer-portal/testData/translations/fineos-common/';
const clientConfigDir = '/packages/fineos-common/e2e/employer-portal/testData/translations/';
const filesToUploadDir = '/packages/fineos-common/e2e/employer-portal/testData/filesToUpload/';
const dateFormats = {
    isoDate: 'YYYY-MM-DD',
    shortDate: 'MMM DD, YYYY'
};
const timeouts = {
    T01: 100,
    T1: 1000,
    T3: 3000,
    T5: 5000,
    T10: 10000,
    T15: 15000,
    T30: 30000,
    T45: 45000,
    T60: 60000
};
const searchModes = {
    EMPLOYEE: 'Employee search',
    NOTIFICATION: 'Notification search'
};
const searchTargets = {
    EMPLOYEE_FIRST_NAME: 'Employee First Name',
    EMPLOYEE_ID: 'Employee ID',
    EMPLOYEE_LAST_NAME: 'Employee Last Name',
    EMPLOYEE_PROFILE: 'Employee Profile',
    NOTIFICATION_DETAILS: 'Notification Details',
    NOTIFICATION_ID: 'Notification ID'
};
const fieldNames = {
    ACCIDENT_DATE: 'Accident date',
    DOCUMENTS: 'Documents',
    EMPLOYEE_NAME_ID: 'Employee Name - Employee Id',
    FIRST_DAY_MISSING_WORK: 'First day missing work',
    JOB_TITLE: 'Job Title',
    NOTIFICATION_CREATION_DATE: 'Notification Creation Date',
    NOTIFICATION_ID: 'Notification ID',
    NOTIFICATION_REASON: 'Notification Reason',
    ORGANIZATION_UNIT: 'Organization Unit',
    VIEW_ACTIONS: 'View Actions',
    WORK_SITE: 'Work site'
};
const stepWording = {
    APPLY: 'apply',
    CAN: 'can',
    CHECK: 'check',
    CONFIRM: 'confirm',
    ENABLED: 'enabled',
    HAVE: 'have',
    ON: 'on',
    VISIBLE: 'visible',
    WITH: 'with'
};
const sortingTypes = {
    ASCENDING: 'ascending',
    CANCEL: 'cancel',
    DESCENDING: 'descending'
};
const notificationCards = {
    JOB_PROTECTED_LEAVE: 'job-protected-leave',
    NOTIFICATION_SUMMARY: 'notification-summary',
    TIMELINE: 'timeline',
    WAGE_REPLACEMENT: 'wage-replacement',
    WORKPLACE_ACCOMMODATION: 'workplace-accommodation-panel'
};
const statusColors = {
    GREEN: 'rgba(53, 181, 88, 1)',
    GREY: 'rgba(179, 179, 179, 1)',
    RED: 'rgba(240, 76, 63, 1)',
    WHITE: 'rgba(255, 255, 255, 1)',
    YELLOW: 'rgba(255, 194, 14, 1)'
};
const gradients = {
    HORIZONTAL_SPLIT_LINE: 'rgba(0, 0, 0, 0) linear-gradient({lightColor}, {lightColor} 50%, {strongColor} 50%, {strongColor}) repeat scroll 0% 0% / auto padding-box border-box',
    NO_GRADIENT: 'rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box',
    SOLID_LINE: '{strongColor} none repeat scroll 0% 0% / auto padding-box border-box',
    STRIPED_LINE: 'rgba(0, 0, 0, 0) repeating-linear-gradient(-45deg, {lightColor}, {lightColor} 12px, {strongColor} 12px, {strongColor} 25px) repeat scroll 0% 0% / auto padding-box border-box'
};
const gradientColors = {
    GREEN: 'rgb(53, 181, 88)',
    RED: 'rgb(240, 76, 63)',
    YELLOW: 'rgb(255, 194, 14)'
};
const stateColors = {
    ACTIVE: 'rgb(0, 123, 194)',
    ACTIVE_1: 'rgba(24, 144, 255, 1)',
    ACTIVE_2: 'rgba(0, 123, 194, 1)',
    HIGHLIGHTED: 'rgba(244, 234, 240, 1)',
    HIGHLIGHTED_2: 'rgba(255, 237, 183, 1)',
    INACTIVE: 'rgb(102, 102, 102)',
    INACTIVE_1: 'rgba(191, 191, 191, 1)',
    INACTIVE_2: 'rgba(0, 0, 0, 0.25)'
};
const colors = {
    GREY: 'rgba(210, 208, 213, 1)',
    LIGHT_YELLOW: 'rgba(255, 240, 203, 1)',
    PURPLE: 'rgba(147, 43, 106, 1)',
    TRANSPARENT: 'rgba(0, 0, 0, 0)'
};
const jobAbsenceLegend = {
    CONTINUOUS_TIME: 'full',
    INTERMITTENT_TIME: 'angled',
    REDUCED_SCHEDULE: 'halved'
};
const periodTypes = {
    CONTINUOUS_TIME: 'Continuous time',
    INTERMITTENT_TIME: 'Intermittent time',
    REDUCED_SCHEDULE: 'Reduced Schedule'
};
const userTypes = {
    ADMIN_USER: 'HR admin user',
    HR_TEST_USER: 'HR test user'
};
const htmlAttributes = {
    CLASS: 'class',
    DATA_ROW_KEY: 'data-row-key',
    DATA_TEST_EL: 'data-test-el'
};
const notificationSummaryActions = {
    ADD_REQUEST: {key: 'request for more time', value: 'REQUEST_FOR_MORE_TIME'},
    REMOVE_REQUEST: {key: 'request a leave to be removed', value: 'LEAVE_PERIOD_CHANGE'}
};
const popUpActions = {
    ACCEPT: 'accept',
    CANCEL: 'cancel',
    CLOSE: 'close',
    SEND: 'send',
    YES: 'yes'
};
const colorClasses = {
    APPROVED: 'success',
    CANCELLED: 'neutral',
    DENIED: 'danger',
    PENDING: 'warning'
};
const colorNames = {
    APPROVED: 'Green',
    CANCELLED: 'Grey',
    DENIED: 'Red',
    PENDING: 'Yellow'
};
const highContrastModeColors = {
    notificationsCount: {highContrastMode: 'rgba(176, 94, 17, 1)', normalMode: 'rgba(226, 120, 22, 1)'}
};
const testVarsInitObject = {
    employeeJobTitle: {},
    employeeOrganisationUnit: {},
    employeePersonalDetails: {},
    employeeWorkSite: {},
    employmentDetails: {},
    endDateJobProtected: {},
    expandedLeaveCards: {},
    filtering: {},
    intake: {},
    leavePlanNameFirst: {},
    leavePlanNameLast: {},
    leavesDecidedOnDate: {},
    lessRecentNotification: {},
    moreRecentNotification: {},
    newMessage: {},
    payments: {},
    searchQuery: null,
    searchTarget: null,
    startDateJobProtected: {},
    workPattern: {}
};
const selectOptions = {
    canadaProvinces: 'AB,BC,MB,NB,NL,NT,NS,NU,ON,PE,QC,SK,YT',
    countries: 'United States,Canada,Netherlands,Ireland,Sweden,Australia,New Zealand,Mexico,Singapore,Hong Kong,Japan',
    usStates: 'AK,AL,AR,AZ,CA,CO,CT,DC,DE,FL,GA,HI,IA,ID,IL,IN,KS,KY,LA,MA,MD,ME,MI,MN,MO,MS,MT,NC,ND,NE,NH,NJ,NM,NV,NY,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VA,VT,WA,WI,WV,WY,AA,AE,AP,AS,FM,GU,MH,MP,PR,PW,VI,UM'
};
const regExps = {
    address: {
        notUs: /^.+\n(.+\n)?(.+\n)?(.+\n)?(.+\n)?(.+\n)?(.+\n)?.+\n.+\n.*$/,
        us: /^.+\n(.+\n)?(.+\n)?.+,.*\n.+(-.+)?\n.*$/
    },
    blankLine: /\n(s+)?\n/,
    coma: /,/g,
    digits: /[^\d,]/g,
    htmlTags: /(<([^>]+)>)/gi,
    newLine: /(\r\n|\n|\r)/gm,
    nonSpaces: /\S+/g,
    phoneNumber: /(\d+-)?\d+-\d+/,
    range: /[\d]+/g,
    spaces: /\s+/g,
    wordsPlaceholders: /\{\w+\}/
};
const endPoints = {
    allWebMessages: '?pageIndex={pageNo}&pageSize=20',
    documents: '/documents',
    employeesApprovedForLeave: '/employees-approved-leaves?startDate',
    events: '/events?eventDate',
    filteredNotifications: '/notifications?',
    getPayments: /payments$/,
    incomingWebMessages: '?msgOriginatesFromPortal=false&pageIndex={pageNo}&pageSize=20',
    notification: '/notifications/',
    outgoingWebMessages: '?msgOriginatesFromPortal=true&pageIndex={pageNo}&pageSize=20',
    readDisabilityDetails: '/readDisabilityDetails',
    unreadWebMessage: '?pageIndex={pageNo}&pageSize=20&readByGroupClient=false',
    webMessages: '/web-messages'

};
const addressLines = {
    addressLines: []
};
const employmentDetailsFields = new Map();
employmentDetailsFields.set(
    'youremployeeholdsthejobtitleof',
    {dataTestEl: ['CURRENT_OCCUPATION_CARD.JOB_TITLE_WITH_START_DATE_AND_END_DATE']}
);
employmentDetailsFields.set('theiremploymenttypeis:', {dataTestEl: ['CURRENT_OCCUPATION_CARD.EMPLOYMENT_TYPE']});
employmentDetailsFields.set(
    'theiradjusteddateofhireis',
    {dataTestEl: ['CURRENT_OCCUPATION_CARD.ADJUSTED_DATE_OF_HIRE']}
);
employmentDetailsFields.set('theyworkon', {dataTestEl: ['CURRENT_OCCUPATION_CARD.WORK_BASIS']});
employmentDetailsFields.set(
    'theiraveragehoursworkedperweekare:',
    {dataTestEl: ['CURRENT_OCCUPATION_CARD.HOURS_PER_WEEK']}
);
employmentDetailsFields.set(
    'theircontractualearningsare:',
    {dataTestEl: ['CURRENT_OCCUPATION_CARD.CONTRACTUAL_EARNINGS_AMOUNT', 'EARNINGS_FREQUENCIES']}
);
employmentDetailsFields.set('theirusworkstateis', {dataTestEl: ['ELIGIBILITY_CRITERIA_CARD.WORK_STATE']});
employmentDetailsFields.set(
    'theyareunderthecollectivebargainingagreement(cba)of',
    {dataTestEl: ['ELIGIBILITY_CRITERIA_CARD.CBA_NUMBER']}
);
employmentDetailsFields.set('they\'veworkedatotalof', {dataTestEl: ['ELIGIBILITY_CRITERIA_CARD.HOURS_PER_YEAR']});

module.exports.employerPortalTranslationsDir = employerPortalTranslationsDir;
module.exports.fineosCommonTranslationsDir = fineosCommonTranslationsDir;
module.exports.filesToUploadDir = filesToUploadDir;
module.exports.dateFormats = dateFormats;
module.exports.clientConfigDir = clientConfigDir;
module.exports.timeouts = timeouts;
module.exports.searchModes = searchModes;
module.exports.fieldNames = fieldNames;
module.exports.stepWording = stepWording;
module.exports.sortingTypes = sortingTypes;
module.exports.searchTargets = searchTargets;
module.exports.notificationCards = notificationCards;
module.exports.jobAbsenceLegend = jobAbsenceLegend;
module.exports.userTypes = userTypes;
module.exports.htmlAttributes = htmlAttributes;
module.exports.notificationSummaryActions = notificationSummaryActions;
module.exports.popUpActions = popUpActions;
module.exports.statusColors = statusColors;
module.exports.stateColors = stateColors;
module.exports.colors = colors;
module.exports.gradients = gradients;
module.exports.gradientColors = gradientColors;
module.exports.periodTypes = periodTypes;
module.exports.colorClasses = colorClasses;
module.exports.colorNames = colorNames;
module.exports.highContrastModeColors = highContrastModeColors;
module.exports.testVarsInitObject = testVarsInitObject;
module.exports.selectOptions = selectOptions;
module.exports.regExps = regExps;
module.exports.employmentDetailsFields = employmentDetailsFields;
module.exports.endPoints = endPoints;
module.exports.addressLines = addressLines;
