/**
 * Mapping page actions into global scope
 */

class DemoActions {

    constructor() {
        const HomeActions = require('../demo/actions/homeActions.js');
        this.homeActions = new HomeActions();

        const ResultListActions = require('../demo/actions/resultListActions.js');
        this.resultListActions = new ResultListActions();
    }
}

class ErpActions {

    constructor() {
        const LoginActions = require('../employer-portal/actions/loginActions');
        this.loginActions = new LoginActions();

        const SearchActions = require('../employer-portal/actions/searchActions');
        this.searchActions = new SearchActions();

        const EmployeeProfileActions = require('../employer-portal/actions/employeeProfileActions');
        this.employeeProfileActions = new EmployeeProfileActions();

        const EditWorkPatternActions = require('../employer-portal/actions/editWorkPatternActions');
        this.editWorkPatternActions = new EditWorkPatternActions();

        const CurrentOccupationActions = require('../employer-portal/actions/currentOccupationActions');
        this.currentOccupationActions = new CurrentOccupationActions();

        const EligibilityCriteriaActions = require('../employer-portal/actions/eligibilityCriteriaActions');
        this.eligibilityCriteriaActions = new EligibilityCriteriaActions();

        const ControlUserAccessActions = require('../employer-portal/actions/controlUserAccessActions');
        this.controlUserAccessActions = new ControlUserAccessActions();

        const NotificationsWithOutstandingReqActions = require('../employer-portal/actions/notificationsWithOutstandingReqActions');
        this.notificationsWithOutstandingReqActions = new NotificationsWithOutstandingReqActions();

        const NotificationActions = require('../employer-portal/actions/notificationActions');
        this.notificationActions = new NotificationActions();

        const CancelWithdrawRequestActions = require('../employer-portal/actions/cancelWithdrawRequestActions');
        this.cancelWithdrawRequestActions = new CancelWithdrawRequestActions();

        const IntakeActions = require('../employer-portal/actions/intakeActions');
        this.intakeActions = new IntakeActions();

        const IntermittentTimeActions = require('../employer-portal/actions/intermittentTimeActions');
        this.intermittentTimeActions = new IntermittentTimeActions();

        const RequestForMoreTimeActions = require('../employer-portal/actions/requestForMoreTimeActions');
        this.requestForMoreTimeActions = new RequestForMoreTimeActions();

        const NewMessagesActions = require('../employer-portal/actions/newMessagesActions');
        this.newMessagesActions = new NewMessagesActions();

        const WebMessagesActions = require('../employer-portal/actions/webMessagesActions');
        this.webMessagesActions = new WebMessagesActions();

        const ErrorPageActions = require('../employer-portal/actions/errorPageActions');
        this.errorPageActions = new ErrorPageActions();

        const MyDashboardActions = require('../employer-portal/actions/myDashboardActions');
        this.myDashboardActions = new MyDashboardActions();
    }
}

module.exports = {
    demo: new DemoActions(),
    erp: new ErpActions()
};
