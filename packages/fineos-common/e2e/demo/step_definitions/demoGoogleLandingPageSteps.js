Given(/^I load homepage$/, async function () {
    await actions.demo.homeActions.navigate();
});

When(/^I search for "([^"]*)"$/, async function (phrase) {
    await actions.demo.homeActions.searchForPhrase(phrase);
});
