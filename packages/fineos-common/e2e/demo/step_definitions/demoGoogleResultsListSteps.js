Then(/^I should see results list$/, async function () {
    await actions.demo.resultListActions.waitForResultsList();
});

Then(/^I verify that the phrase "([^"]*)" is contained in the first result$/, async function (phrase) {
    await actions.demo.resultListActions.waitForResultsList();
    await expectHelpers.expectElementContainText(
        pages.demo.resultsListPage.listResults().first(),
        phrase,
        `I should see phrase ${phrase} in first search result`,
        false
    );
});

When(/^I click on first search result$/, async function () {
    await pages.demo.resultsListPage.resultLink(0).Click({waitForVisibility: true});
});

Then(/^I should be redirected from search result page$/, async function () {
    await waitHelpers.waitForElementToBeNotVisible(
        await pages.demo.resultsListPage.listResults().first(),
        timeouts.T10,
        'List of results shouldn\'t be shown after clicking result link'
    );
});
