@demo
Feature: Google redirection feature

After clicking on searched link I should be taken to correct page

  @search @redirect @TC01
  Scenario: Checking redirection on results list
    Given I load homepage
    And I search for "Protractor"
    When I click on first search result
    Then I should be redirected from search result page
