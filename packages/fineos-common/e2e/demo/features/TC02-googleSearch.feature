@demo
Feature: Google search feature

  Test case contains 3 examples of which first two should pass and 3 should fail on different assertions.

  @search @desktop @TC02
  Scenario Outline: Searching results check
    Given I load homepage
    When I search for "<phrase>"
    Then I should see results list
    And I verify that the phrase "<phrase>" is contained in the first result


    Examples:
      | phrase      |
   #   | Protractor  |
      | Jam Session |
