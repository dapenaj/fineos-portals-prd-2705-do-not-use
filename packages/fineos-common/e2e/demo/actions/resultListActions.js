class ResultsListActions {

    /**
     * Waits for results list (at least one result should be shown to satisfy assertion)
     * @returns {Promise<void>}
     */
    async waitForResultsList() {
        await waitHelpers.waitForElementToBeVisible(
            pages.demo.resultsListPage.listResults().first(),
            timeouts.T5,
            'I should see at least one search result'
        );
    }
}

module.exports = ResultsListActions;
