class HomeActions {

    /**
     * Navigates to homepage - urls defined per env type
     * @returns {Promise<void>}
     */
    async navigate() {
        await browser.get(browser.baseUrl);
        await waitHelpers.waitForElementToBeVisible(
            pages.demo.homePage.inputSearch(),
            timeouts.T10,
            'I should see search input'
        );
    }

    /**
     * Search for given phrase
     * @param {string} phrase - phrase to look for
     * @returns {Promise<void>}
     */
    async searchForPhrase(phrase) {
        await pages.demo.homePage.inputSearch().SendText(phrase + protractor.Key.ENTER);
    }
}

module.exports = HomeActions;
