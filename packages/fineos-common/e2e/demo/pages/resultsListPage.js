class ResultsListPage {

    listResults() {
        return element.all(by.css('.g .rc'));
    }

    resultLink(index) {
        return this.listResults().get(index).element(by.css('.r>a h3'));
    }
}

module.exports = ResultsListPage;
