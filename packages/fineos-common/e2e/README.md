__________________________________________________________________________________________

#First time installation:
__________________________________________________________________________________________
1. Install NodeJS for your OS, you can download it [here](https://nodejs.org/en/download/current)
. 
2. Once installation is complete, you should be able to check version of Node & NPM (Node Package Manager) from command line by typing:
    +	node --version
    +	npm --version

3. It is time to install [GIT](https://git-scm.com/download). If you have git already installed and configured you can jump to the point 6.

4. _(Optional)_ Install your favourite interface for GIT ([TortoiseGit](https://tortoisegit.org) , [SourceTree](https://www.sourcetreeapp.com) , etc... )

5. _(Optional-Skip if repo is not secured)_ Generate SSH keys to access repository:
    + Run Git Bash
    + Enter command: ssh-keygen
    + Hit enter when asked for the filename (git will use default one)
    + Hit enter when asked for the password for no password key pair, or set own password and remember it for future
    + In user home dir there would be created directory with 2 files id_rsa (private key - this stay on your PC) and id_rsa.pub (public key - this should be added to the repo in order to get access to)

6. Clone repository + switch to test branch
    + git clone git@bitbucket.org:fineoshackers/fineos-portals.git
    + git checkout develop

7. Install dependencies
    + open command line, go to [root-directory]/packages/fineos-common/e2e
	+ npm install
	+ npm install webdriver-manager -g
	+ npm run webdriver-update

8. Run tests using command
	+ npm run demo
__________________________________________________________________________________________

#Programming Environment
__________________________________________________________________________________________
It is recommended to use [WebStorm](https://www.jetbrains.com/webstorm/download/download-thanks.html), 
but any programming environment supporting JavaScript would be good.

__________________________________________________________________________________________

#Preparing WebStorm
__________________________________________________________________________________________

Once you decide to use WebStorm, follow those steps to set your test environment up:

> Before you begin -> open project in WebStorm (File - Open... - <Select Repo Folder> - OK) 

1. Open WebStorm Settings(CTRL+ALT+S)
2. Go to **Languages & Frameworks** -> **JavaScript** and set JavaScript version to ECMAScript6
3. Go to sub-menu **Libraries** and **Download** following libs:
    + chai
    + chai-as-promised
    + protractor-helpers
    + protractor-browser-logs
4. Close settings window
5. Using top menu select **Run** -> **Edit Configurations...**
6. Press (ALT+Insert) and chose Node.js configuration
7. Set following fields:
    + **Name**: Run tests
    + **Node interpreter**: _should be filled by default, no need to change it_
    + **Node parameters**: _leave empty_
    + **Working directory**: fineos-portals\packages\fineos-common\e2e
    + **JavaScript file**: node_modules\protractor\built\cli.js
    + **Application parameters**: config\localConfig.js --env=erp_4 --tags="@smoke and @automated"
8. Press OK button
9. From now you should be able to run *(SHIFT+F10)* or debug *(SHIFT+F9)* tests directly using WebStorm
__________________________________________________________________________________________
#Executing tests on 1 or more threads. Features are executed in parallel
__________________________________________________________________________________________
1. IMPORTANT: test data is stored in dedicated repository 
on develop branch git@bitbucket.org:fineoshackers/fineos-portals-test-data.git
Before you execute any test you need to open terminal, navigate /fineos-portals/packages/fineos-common/e2e 
and run "npm run fetch-test-data" command. The script will clone the test data from fineos-portals-test-data repository. 
Script is cloning repository using ssh protocol, be sure you have ssh keys configured.
When the script is finished with success, new directory "generatedTestData" should be created 
in /fineos-portals/packages/fineos-common/e2e/employer-portal/testData/ with all the test data inside. The script should 
be executed each time new test data is added to fineos-portals-test-data repository. Latest version of test data will 
be fetched in result.
2. To execute one test specify the test tag in the parameters: config/localConfig.js --env=erp_8 --tags="@C115-F02-01"
3. When user wants to execute 1 feature with multiple tests, the tests will be executed in 1 thread one after another.
4. To execute more than one test on more than 1 thread specify the test set tag for example @smoke 
config/localConfig.js --env=ENV --tags="@smoke"
5. For multiple threads feature files are divided between them. For example the default value of threads is set to 4.
So if there are 4 features with smoke tag, one feature will be executed per thread.
6. The default thread value is set to 4. This value can be found in sharedConfig.js:
    .option('--threads <threads>', 'browser parallel instances', x => parseInt(x, 10), 4);
    
    But when one test is sent as a parameter only one browser thread will be opened according to the algorithm:
    
    const bucketSize = availableFeatures.length / Math.min(program.threads, availableFeatures.length);
__________________________________________________________________________________________
#Other

[see also config/help.md](./config/help.md)
