const notifications = require('./../constants/notifications');
const employees = require('./../constants/employees');

class SearchActions {

    /**
     * Selects search mode
     * @param {string} mode - search mode to be selected, supported values constants.searchModes.*
     * @returns {Promise<void>}
     */
    async setSearchMode(mode) {
        const selection = await pages.erp.searchPage.textSelectedSearchMode().getText();
        if (selection !== mode) {
            await pages.erp.searchPage.dropdownSearchMode().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.searchPage.buttonDropdownOptionByText(mode));
            await pages.erp.searchPage.buttonDropdownOptionByText(mode).Click({waitForVisibility: true});
            await expectHelpers.expectElementTextEqual(
                pages.erp.searchPage.textSelectedSearchMode(),
                mode,
                `Expected to see mode [${mode}]`
            );
        }
    }

    /**
     * Opens notification
     * @param {string} id - notification ID to be opened
     * @returns {Promise<void>}
     */
    async openNotification(id) {
        testVars.searchQuery = id;
        await this.setSearchMode(constants.searchModes.NOTIFICATION);
        await pages.erp.searchPage.inputSearch().SendText(id.replace('NTN-', ''));
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResults(),
            timeouts.T30,
            `Expected to see search results for [${id}]`
        );
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResultLink(0),
            timeouts.T30,
            `Expected to see at least one search result for [${id}]`
        );
        await pages.erp.searchPage.sectionSearchResultLink(0).Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.sectionNotificationDetails(),
            timeouts.T30,
            `Expected to see notification details for [${id}]`
        );
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Search for random item by search target
     * @param {string} searchTarget - employee or notification, supported values: constants.searchTargets.*
     * @param {boolean} isPartial - if set as true it will enter only part of search query
     * @returns {Promise<void>}
     */
    async searchForRandomTarget(searchTarget, isPartial = false) {
        if (searchTarget === constants.searchTargets.NOTIFICATION_ID) {
            await actions.erp.searchActions.setSearchMode(constants.searchModes.NOTIFICATION);
            testVars.searchQuery = notifications.getRandomNotification().id;
            await pages.erp.searchPage.inputSearch().SendText(testVars.searchQuery.replace('NTN-', ''));
        } else {
            const employee = employees.getRandomEmployee();
            switch (searchTarget) {
                case constants.searchTargets.EMPLOYEE_FIRST_NAME:
                    testVars.searchQuery = employee.firstName;
                    await pages.erp.searchPage.inputSearch().SendText(employee.firstName);
                    break;
                case constants.searchTargets.EMPLOYEE_LAST_NAME:
                    testVars.searchQuery = employee.lastName;
                    await pages.erp.searchPage.inputSearch().SendText(employee.lastName);
                    break;
                case constants.searchTargets.EMPLOYEE_ID:
                    testVars.searchQuery = `${employee.firstName} ${employee.lastName}`;
                    await pages.erp.searchPage.inputSearch().SendText(employee.EmployeeId);
                    break;
                default:
                    throw new Error(`search target [${searchTarget}] is not supported`);
            }
        }
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Search for given item by search target
     * @param {string} searchTarget - employee or notification, supported values: constants.searchTargets.*
     * @param {string} descriptor - employee or notification descriptor
     * @param {boolean} isPartial - if set as true it will enter only part of search query
     * @returns {Promise<void>}
     */
    async searchForGivenTarget(searchTarget, descriptor, isPartial = false) {
        if (searchTarget === constants.searchTargets.NOTIFICATION_ID) {
            await actions.erp.searchActions.setSearchMode(constants.searchModes.NOTIFICATION);

            if (isPartial) {
                testVars.randomNotificationId = testVars.randomNotificationId || notifications.getRandomNotification(
                    /NTN-\d{2,4}/g).id;

                if (descriptor.toLowerCase().includes('continue')) {
                    testVars.searchQuery = testVars.randomNotificationId.substr(-1);
                } else {
                    testVars.searchQuery = testVars.randomNotificationId.slice(0, -1);
                }
            } else {
                testVars.searchQuery = notifications.getNotification(descriptor).id;
            }

            await pages.erp.searchPage.inputSearch().SendText(testVars.searchQuery.replace('NTN-', ''));
        } else {
            const employee = employees.getEmployee(descriptor);
            switch (searchTarget) {
                case constants.searchTargets.EMPLOYEE_FIRST_NAME:
                    testVars.searchQuery = employee.firstName;
                    await pages.erp.searchPage.inputSearch().SendText(employee.firstName);
                    break;
                case constants.searchTargets.EMPLOYEE_LAST_NAME:
                    testVars.searchQuery = employee.lastName;
                    await pages.erp.searchPage.inputSearch().SendText(employee.lastName);
                    break;
                case constants.searchTargets.EMPLOYEE_ID:
                    testVars.searchQuery = `${employee.lastName}`;
                    await pages.erp.searchPage.inputSearch().SendText(employee.EmployeeId);
                    break;
                default:
                    throw new Error(`search target [${searchTarget}] is not supported`);
            }
        }
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Open employee details page
     * @param {string} employeeSearchTarget - field used to find employee, e.g lastName, EmployeeId
     * @param {string} descriptor - select employee with notifications
     * @returns {Promise<void>}
     */
    async openEmployeeDetailsPage(descriptor, employeeSearchTarget) {
        await this.setSearchMode(constants.searchModes.EMPLOYEE);
        testVars.searchTarget = employees.getEmployee(descriptor);
        await pages.erp.searchPage.inputSearch().SendText(testVars.searchTarget[employeeSearchTarget]);
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResults(),
            timeouts.T60,
            'Expected to see search results for employee'
        );
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResultLink(0),
            timeouts.T30,
            'Expected to see at least one search result for employee'
        );
        await pages.erp.searchPage.sectionSearchResultLink(0).Click({waitForVisibility: true});

        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.actionsLink(),
            timeouts.T60,
            'Expected to see employee profile page'
        );
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Open results page
     * @param {string} searchTarget - open results page based on the search query
     * @returns {Promise<void>}
     */
    async openResultsPage(searchTarget) {
        await this.searchForRandomTarget(searchTarget);
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResults(),
            timeouts.T60,
            'Expected to see search results'
        );
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResultLink(0),
            timeouts.T60,
            'Expected to see at least one search result'
        );
    }

    /**
     * Clicks on the first item from the results page
     * @returns {Promise<void>}
     */
    async clickOnTheFirstSearchResult() {
        const firstSearchResult = await pages.erp.searchPage.listSearchResults().first();
        testVars.firstSearchResultTitle = await pages.erp.searchPage.searchResultItemTitle(firstSearchResult).getText();
        testVars.firstSearchResultData = await pages.erp.searchPage.searchResultItemData(firstSearchResult).getText();
        testVars.actualLinkData = testVars.firstSearchResultTitle;
        await pages.erp.searchPage.sectionSearchResultLink(0).Click({waitForVisibility: true});
    }

    /**
     * Clicks on the first item from the results page
     * @returns {Promise<void>}
     * @param {boolean} descriptor - existing or not
     * @param {string} employeeSearchTarget - last name, employee id or first name
     */
    async searchForEmployee(descriptor, employeeSearchTarget) {
        await this.setSearchMode(constants.searchModes.EMPLOYEE);
        testVars.searchTarget = employees.getEmployee(descriptor);
        await pages.erp.searchPage.inputSearch().SendText(testVars.searchTarget[employeeSearchTarget]);
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.searchPage.sectionSearchResults(),
            timeouts.T60,
            'Expected to see search results for employee'
        );
    }

    /**
     * Opens intake creator for existing or not existing employee
     * @param {boolean} existingOrNot - existing or not existing employee
     * @returns {Promise<void>}
     */
    async openIntakeCreatorForExistingOrNotEmployee(existingOrNot) {
        await actions.erp.searchActions.searchForEmployee(existingOrNot, 'EmployeeId');

        if (existingOrNot === 'not existing') {
            await pages.erp.searchPage.startNotificationOnBehalfLink().Click({waitForVisibility: true});
        } else {
            await browser.actions().mouseMove(pages.erp.searchPage.employeeSearchResult()).perform();
            await pages.erp.searchPage.makeNotificationLink().Click();
        }

        await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.employeeDetailsFormHeader().first());
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Opens intake creator for existing or not existing employee
     * @param {string} descriptor - employee id
     * @returns {Promise<void>}
     */
    async openIntakeCreatorForUserWithEmployeeId(descriptor) {
        await actions.erp.searchActions.searchForEmployee(descriptor, 'EmployeeId');
        await browser.actions().mouseMove(pages.erp.searchPage.employeeSearchResult()).perform();
        await pages.erp.searchPage.makeNotificationLink().Click();
        await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.employeeDetailsFormHeader().first());
    }
}

module.exports = SearchActions;
