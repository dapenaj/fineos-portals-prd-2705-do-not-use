const faker = require('faker');

class EmployeeProfileActions {

    /**
     * Returns array with Strings
     * @param objectsList - list of WebElements
     * @returns {Promise<Array<String>>}
     */
    async mapPageObjectListToStringArray(objectsList) {
        objectsList = objectsList.filter(async arrayEl => await arrayEl.getText() !== '');

        await waitHelpers.waitForElementToBeVisible(objectsList.first());
        return await objectsList.map(async item => await item.getText());
    }

    /**
     * Returns labels of user data
     * @returns {Promise<Array<String>>}
     */
    async getEmployeeDetailsDataLabels() {
        return await this.mapPageObjectListToStringArray(
            pages.erp.employeeProfilePage.employeeDataLabels()
        );
    }

    /**
     * Returns labels of employment details data
     * @param {string} cardName
     * @returns {Promise<Array<String>>}
     */
    async getEmploymentDetailsCardFields(cardName) {
        return await this.mapPageObjectListToStringArray(
            pages.erp.employeeProfilePage.employmentDetailsCardFields(cardName)
        );
    }

    /**
     * Returns values of user data
     * @returns {Promise<Array<String>>}
     */
    async getEmployeeDetailsDataValues() {
        return await this.mapPageObjectListToStringArray(
            pages.erp.employeeProfilePage.employeeDetailsDataValues()
        );
    }

    /**
     * Returns text corresponding to given fieldName
     * @param {string} fieldName - text of field
     * @returns {string}
     */
    async getEmploymentDetailsElementsText(fieldName) {
        const actualData = [];
        const key = fieldName.replace(constants.regExps.spaces, '').toLowerCase();
        const dataTestElTags = constants.employmentDetailsFields.get(key).dataTestEl;

        for (const tag of dataTestElTags) {
            actualData.push(await pages.erp.employeeProfilePage.employmentDetailsField(tag).getText());
        }

        return actualData.join(' ');
    }

    /**
     * Returns expected test data in correct format
     * @param {string} dataType - field name used to get formatted test data
     * @returns {string} - expected test data in correct format
     */
    getTestDataInCorrectFormat(dataType) {
        const employee = testVars.searchTarget;
        const data = dataType.replace(constants.regExps.spaces, '').toLowerCase();
        let translationPath;

        switch (data) {
            case 'theircontractualearningsare:':
                translationPath = 'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.CONTRACTUAL_EARNINGS';
                return translationsHelper.mapTranslationVariables(
                    translationPath,
                    [employee.earnings.amount, employee.earnings.frequency]
                );
            case 'youremployeeholdsthejobtitleof':
                translationPath = 'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.JOB_TITLE_WITH_START_DATE_AND_END_DATE';
                return translationsHelper.mapTranslationVariables(
                    translationPath,
                    [employee.employmentDetails.jobTitle, employee.employmentDetails.jobStartDate, employee.employmentDetails.jobEndDate]
                );
            case 'theirusworkstateis':
                translationPath = 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORK_STATE';
                return translationsHelper.mapTranslationVariables(
                    translationPath,
                    [employee.employmentDetails.state]
                );
            default:
                return `Test data was not formatted for unsupported data type ${dataType}`;
        }
    }

    /**
     * Returns titles of preferences
     * @returns {Promise<Array<String>>}
     */
    async getPreferencesCardsTitles() {
        return await this.mapPageObjectListToStringArray(
            pages.erp.employeeProfilePage.preferencesTitles()
        );
    }

    /**
     * Function generates faked test data depending on given data type
     *
     * @param {string} dataType - firstName, lastName, email, etc.
     * @param {[string]} statesArray = array with available stated abbreviations, etc. ['CA', 'NY']
     * @returns {string} generated test data
     */
    // eslint-disable-next-line complexity
    generatePersonalDetailsData(dataType, statesArray = []) {
        switch (dataType) {
            case 'firstname':
                return `${faker.name.firstName('mixed')}Auto`;
            case 'lastname':
                return `${faker.name.lastName('mixed')}Auto`;
            case 'dateofbirth':
                return `${faker.date.month().slice(0, 3)} ${faker.random.number({
                    max: 28,
                    min: 10
                })}, ${faker.random.number({max: 2019, min: 1920})}`;
            case 'addressline1':
            case 'addressline2':
            case 'addressline3':
            case 'addressline4':
            case 'addressline5':
            case 'addressline6':
            case 'addressline7':
                return faker.address.streetName().toString().substring(0, 39).trim();
            case 'city':
                return faker.address.city();
            case 'state':
            case 'province':
            case 'country':
                return faker.random.arrayElement(statesArray);
            case 'zipcode':
                return faker.address.zipCode();
            case 'postalcode':
                return faker.random.arrayElement(['B2C5D0', 'W1B8J5', 'O0P9Y7', 'G2F7B0', 'U8O0P3']);
            case 'email':
                return faker.internet.email();
            case 'countrycode':
                return faker.random.number(99);
            case 'areacode':
                return faker.random.number(999);
            case 'phonenumber':
                return faker.random.number({max: 9999999999, min: 10000000});
            case 'phonetype':
                return faker.random.arrayElement(['Landline', 'Cell phone']);
            default:
                return `Test data was not generated for unsupported data type ${dataType}`;
        }
    }

    /**
     * This method scrolls up state options list until it finds option with given name
     * @param {int} maxScrolls - max number of Scroll actions
     *
     * @returns {string} generated test data
     */
    async scrollUpToFirstSelectOption(maxScrolls = 15) {
        let numberOfScrolls = 0;
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectOptions().first());

        while (numberOfScrolls < maxScrolls) {
            const firstOption = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectOptions().first());
            await firstOption.Scroll();
            numberOfScrolls++;
        }
    }

    /**
     * This method scrolls down state options list until it finds option with given name
     * @param {string} untilElement - name of select option, scroll action will be repeated until this element
     *     occurs.
     * @param {int} maxScrolls - max number of Scroll actions
     *
     * @returns {string} generated test data
     */
    async getAllSelectOptions(untilElement = 'um', maxScrolls = 15) {
        let i = 0;
        let lastOption = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectOptions().last());

        await this.scrollUpToFirstSelectOption();

        let allAvailableOptions = (await pages.erp.employeePersonalDetailsPage.selectOptions().getText()).toString().split(
            ',');

        while ((await lastOption.getText()).toString().toLowerCase() !== untilElement.toLowerCase()) {
            await pages.erp.employeePersonalDetailsPage.selectOptions().last().Scroll();
            lastOption = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectOptions().last());
            const availableStates = await pages.erp.employeePersonalDetailsPage.selectOptions().getText();

            if (++i > maxScrolls) {
                break;
            }

            allAvailableOptions = allAvailableOptions.concat(availableStates.toString().split(','));
        }

        return [...new Set(allAvailableOptions)];
    }

    /**
     * This method scrolls down state options list until it finds option with given name
     * @returns {array} generated test data
     */
    async getAllPhoneNumbers() {
        const actualPhoneNumbers = [];

        await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().each(async phoneCard => {
            let phoneNumber = (await pages.erp.employeePhoneNumbersPage.phoneNumberAllInputs(phoneCard).getAttribute(
                'value')).toString().replace(constants.regExps.coma, '-');

            if (phoneNumber.toString() === '--') {
                phoneNumber = '';
            } else if (phoneNumber.startsWith('-')) {
                phoneNumber = phoneNumber.toString().substring(1);
            }

            actualPhoneNumbers.push(phoneNumber);
        });

        return actualPhoneNumbers;
    }

    /**
     * This method checks if all add/edit email pop up elements are visible
     * @param {string} action - add or edit (email)
     */
    async checkAddEditEmailPopupElements(action) {
        if (action === 'Edit') {
            const title = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.editEmailsPopupTitle());
            await expectHelpers.checkTranslation(title, 'PROFILE.PERSONAL_DETAILS_TAB.EDIT_EMAILS');
            await waitHelpers.waitForElementToBeStatic(pages.erp.employeeEmailsPage.editEmailsPopupTitle());
            expect(constants.stateColors.ACTIVE_2).is.equal(await pages.erp.employeeEmailsPage.addEmailButton().getCssValue(
                'color'));
        } else {
            const title = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.addEmailPopupTitle());
            await expectHelpers.checkTranslation(title, 'PROFILE.PERSONAL_DETAILS_TAB.ADD_EMAIL');
            await waitHelpers.waitForElementToBeStatic(pages.erp.employeeEmailsPage.addEmailButton());
            expect(constants.stateColors.INACTIVE_2).is.equal(await pages.erp.employeeEmailsPage.addEmailButton().getCssValue(
                'color'));
        }

        const primaryAddress = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.emailsListTitle(),);
        await expectHelpers.checkTranslation(primaryAddress, 'PROFILE.PERSONAL_DETAILS_TAB.EMAILS');

        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.employeeEmailsPage.okButton(),
            pages.erp.sharedModal.cancelButton(),
            pages.erp.sharedModal.closeXButton()
        ]);
    }

    /**
     * This method checks if all add/edit phone numbers pop up elements are visible
     * @param {string} action - add or edit (email)
     */
    async checkAddEditPhoneNumbersPopupElements(action) {
        if (action === 'Edit') {
            const title = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.editPhoneNumbersTitle());
            await expectHelpers.checkTranslation(title, 'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PHONE_NUMBERS');
        } else {
            const title = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.addPhoneNumbersTitle());
            await expectHelpers.checkTranslation(title, 'PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE');
        }

        const primaryAddress = await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeePhoneNumbersPage.phoneNumbersListTitle(),
            timeouts.T30,
            'Expect to see popup Phone number(s) label'
        );
        await expectHelpers.checkTranslation(primaryAddress, 'PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS');

        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.employeePhoneNumbersPage.okButton(),
            pages.erp.employeePhoneNumbersPage.cancelButton(),
            pages.erp.employeePhoneNumbersPage.closeButton()
        ]);
    }

    /**
     * This method checks if fieldsLabels of given popup are correct
     * @param {string} fieldLabels - labels to be checked
     * @param {string} sectionName (employmentDetails, employeePersonalDetails)
     */
    async checkFieldsContent(fieldLabels, sectionName) {
        fieldLabels = fieldLabels.split(',').map(item => item.trim());
        let formField;
        for (const label of fieldLabels) {
            if (sectionName.toString().includes('employment')) {
                formField = await pages.erp.employeePersonalDetailsPage.editCurrentOccupationInput(label);
            } else {
                formField = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(label);
            }

            const dataType = label.trim().replace(constants.regExps.spaces, '').toLowerCase();
            const expectedFieldContent = testVars[sectionName][dataType];
            let actualFieldContent = await formField.getAttribute('value');

            if (testDataHelpers.isNumeric(expectedFieldContent)) {
                actualFieldContent = Number(actualFieldContent);
            }

            expect(actualFieldContent.toString().trim()).is.equal(expectedFieldContent.toString().trim());
        }
    }

    /**
     * This method adds random phone number in Employee Personal Details
     * @param {array} phoneFieldsArray array with phone number subfields
     */
    async addRandomPhoneNumber(phoneFieldsArray = ['Country code', 'Area code', 'Phone number']) {
        for (const field of phoneFieldsArray) {
            const dataType = field.trim().replace(constants.regExps.spaces, '').toLocaleLowerCase();

            testVars.employeePersonalDetails[dataType] = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
                dataType);

            const formField = await pages.erp.employeePhoneNumbersPage.editNumbersInputs(field).last();
            await formField.SetText(testVars.employeePersonalDetails[dataType]);

            expect(await formField.getAttribute('value')).is.equal(testVars.employeePersonalDetails[dataType].toString());
        }
    }

    /**
     * This allows to choose phone number type in Employee Personal Details
     * @param {string} phoneNumberType (Landline, Cell Phone)
     */
    async choosePhoneType(phoneNumberType) {
        const lastPhoneDetailsCard = await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().last();
        await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().last().Click({waitForVisibility: true});

        if (phoneNumberType === 'Landline') {
            await pages.erp.employeePhoneNumbersPage.landLineSelectOption(lastPhoneDetailsCard).Click({waitForVisibility: true});
        } else {
            await pages.erp.employeePhoneNumbersPage.cellPhoneSelectOption(lastPhoneDetailsCard).Click({waitForVisibility: true});
        }
    }
}

module.exports = EmployeeProfileActions;


