class MyDashboardActions {
    /**
     * This method get my dashboard widget by its name.
     * @param {string} widgetName
     * @returns {ElementFinder} - widgetElement
     */
    async getWidgetByName(widgetName) {
        let widget;
        switch (widgetName) {
            case 'Employees expected to return to work':
                widget = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.employeesExpectedToReturnToWorkWidgetDayFilter());
                break;
            case 'Notifications created':
                widget = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.notificationsCreatedWidgetDayFilter());
                break;
            case 'Leaves decided':
                widget = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidgetDayFilter());
                break;
            case 'Employees approved for leave':
                widget = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetDayFilter());
                break;
            default:
                throw new Error(`widgetName [${widgetName}] is not supported`);
        }
        return widget;
    }

    /**
     * This method get actual state of high contrast mode switch
     * @returns {string} - actual state of high contrst mode
     */
    async getHighContrastModeState() {
        const actualState = await pages.erp.myDashboardPage.highContrastModeSwitch().getAttribute('aria-checked');
        return (actualState === 'true') ? 'on' : 'off';
    }

    /**
     * This method if high contrast mode modal elements are visible
     *  @param {boolean} visibility
     */
    async isHighContrastModeModalVisible(visibility) {
        if (visibility) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.highContrastModeSwitch());
            const label = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.highContrastModeLabel());
            const description = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.highContrastModeDescription());

            await expectHelpers.checkTranslation(label, 'FINEOS_COMMON.HIGH_CONTRAST_MODE.SWITCH_LABEL');
            await expectHelpers.checkTranslation(description, 'FINEOS_COMMON.HIGH_CONTRAST_MODE.DESCRIPTION');
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.highContrastModeSwitch());
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.highContrastModeLabel());
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.highContrastModeDescription());
        }
    }
}

module.exports = MyDashboardActions;
