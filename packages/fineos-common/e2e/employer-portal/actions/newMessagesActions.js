const faker = require('faker');
const moment = require('moment');
const {getRequestDataArray} = require('../../common/helpers/snifferHelper');
const {lookupForRequests} = require('../../common/helpers/snifferHelper');

class NewMessagesActions {
    /**
     * Function generates faked test data depending on given data type
     *
     * @param {string} dataType - firstName, lastName, email, etc.
     * @param {[string]} statesArray = array with available stated abbreviations, etc. ['CA', 'NY']
     * @returns {string} generated test data
     */
    generateNewMessageData(dataType, statesArray = []) {
        switch (dataType.toLowerCase()) {
            case 'subject':
                testVars.newMessage[dataType.toLowerCase()] = faker.company.catchPhrase();
                break;
            case 'message':
                testVars.newMessage[dataType.toLowerCase()] = faker.lorem.paragraph();
                break;
            default:
                return `Test data was not generated for unsupported data type ${dataType}`;
        }
        return testVars.newMessage[dataType.toLowerCase()];
    }

    /**
     * This method allows to get notifications web messages using api GET request
     *
     * @param jsonPath {string} - path to json field
     * @returns {array} array messages objects
     */
    getNotificationWebMessages(jsonPath) {
        const actualNotificationId = testVars.searchTarget.id;
        return getRequestDataArray(
            `/${actualNotificationId}/web-messages?_includeWebMessagesFromSubCases=true`,
            jsonPath,
            'data.elements'
        );
    }

    /**
     * This method allows to get notifications web messages using api GET request sorted by  v field
     *
     * @param sortOrder {string} - sorting order ascending, descending
     * @returns {array} array of messages objects sorted by contactDateTime
     */
    async getNotificationWebMessagesSortedByDate(sortOrder = 'descending') {
        const actualNotificationId = testVars.searchTarget.id;
        const listOfMessages = testDataHelpers.getJsonFieldValue(
            (await lookupForRequests(`/${actualNotificationId}/web-messages?_includeWebMessagesFromSubCases=true`))[0],
            'data.elements'
        );

        listOfMessages.sort((a, b) => {
            const dateTimeA = moment(a.contactDateTime).valueOf();
            const dateTimeB = moment(b.contactDateTime).valueOf();

            return (sortOrder.toLowerCase() === 'descending') ? dateTimeB - dateTimeA : dateTimeA - dateTimeB;
        });

        return listOfMessages;
    }
}

module.exports = NewMessagesActions;
