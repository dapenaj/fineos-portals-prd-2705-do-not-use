const {getRequestDataArray} = require('../../common/helpers/snifferHelper');
const {getRequestData} = require('../../common/helpers/snifferHelper');

class WebMessagesActions {

    /**
     * This method takes all available api web messages from all available pages using api GET request. Taken messages
     * are in ascending or descending order.
     * @param {string} endpointQuery - query of endpoint used to take filtered/sorted messages
     * @param {string} sortQuery - decides which field will be taken while sorting and if messages should be in
     *     ascending or descending order (default is '-contactDateTime' - descending)
     * @param {number} pageSize - number of messages on the size, default is 20
     * @returns {array} - list of subjects of taken web messages in ascending or descending order
     */
    async getApiMessages(endpointQuery, sortQuery = '-contactDateTime', pageSize = 20) {
        let allApiMessages = [];
        const numberOfAllMessages = await getRequestData(
            constants.endPoints.webMessages,
            'totalSize'
        );
        const totalNumberOfPages = Math.ceil(numberOfAllMessages / pageSize);

        for (let pageNo = 0; pageNo < totalNumberOfPages; pageNo++) {
            const actualPageMessages = await getRequestDataArray(
                `${constants.endPoints.webMessages}${endpointQuery.replace(/{pageNo}/gi, pageNo)}&sort=${sortQuery}`,
                'subject',
                'data.elements'
            );
            allApiMessages = allApiMessages.concat(actualPageMessages);
        }

        return allApiMessages;
    }

    /**
     * This method takes actual filter option set on UI. It takes data-test-el of selected option and extracts filter
     * option (ALL, INCOMING, OUTGOING, UNREAD)
     * @returns {string} - actually used filter option (ALL, INCOMING, OUTGOING or UNREAD)
     */
    async getActualFilterOption() {
        const actualOptionDataTeslElAttr = await pages.erp.webMessagesPage.filterSelectedOption().getAttribute(
            'data-test-el');
        return actualOptionDataTeslElAttr.toString().split('.').slice(-1);
    }

    /**
     * This method takes actual sorting option set on UI. It takes data-test-el of selected option and extracts sorting
     * option (ASC, DESC)
     * @returns {string} - actually used sorting option (ASC, DESC)
     */
    async getActualSortingOption() {
        const actualOptionDataTeslElAttr = await pages.erp.webMessagesPage.sortSelectedOption().getAttribute(
            'data-test-el');
        return actualOptionDataTeslElAttr.toString().split('.').slice(-1);
    }

    /**
     * This method scrolls to last available web message on the list. It makes all messages are displayed.
     */
    async showAllUiMessages() {
        let actualSpinners = await pages.erp.notificationsWithOutstandingReqPage.pageSpinners();

        while (actualSpinners.length > 0) {
            await pages.erp.notificationsWithOutstandingReqPage.pageSpinners().last().Scroll().then(null, function (err) {
                // eslint-disable-next-line no-console
                console.log(`The error occurred is: ${err.name}`);
            });

            actualSpinners = await pages.erp.notificationsWithOutstandingReqPage.pageSpinners();
        }
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * This method get all available API messages using api request, it takes also all available UI messages using
     * locator and compares both lists.
     */
    async getAndCompareApiAndUiMessages(filterOptionName, sortQuery) {
        const endpointQuery = this.getWebMessageApiRequestQuery(filterOptionName);
        let allApiMessages = await this.getApiMessages(endpointQuery, sortQuery);
        allApiMessages = allApiMessages.toString().replace(/\s+/gi, ' ').trim();

        let allUiMessages = await pages.erp.webMessagesPage.messagesSubjectsList().getText();
        allUiMessages = allUiMessages.toString().replace(/\s+,/gi, ',').trim();

        expect(allApiMessages).is.equal(allUiMessages);
    }

    /**
     * This method maps API request endpoint query depending on the filter option
     */
    getWebMessageApiRequestQuery(option) {
        switch (option.toString().toLowerCase()) {
            case 'all':
                return constants.endPoints.allWebMessages;
            case 'incoming':
                return constants.endPoints.incomingWebMessages;
            case 'outgoing':
                return constants.endPoints.outgoingWebMessages;
            case 'unread':
                return constants.endPoints.unreadWebMessage;
            default:
                throw new Error(`option [${option}] is not supported`);
        }
    }
}

module.exports = WebMessagesActions;
