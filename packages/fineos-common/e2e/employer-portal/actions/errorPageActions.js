
class ErrorPageActions {
    /**
     * Navigates to errorPage
     * @param {string} urlSuffix that is added to the baseUrl string
     * @returns {Promise<void>}
     */
    async navigateToErrorPage(urlSuffix = 'errorPage') {
        const currentUrl = await browser.getCurrentUrl();
        const url = `${currentUrl}${urlSuffix}`;
        await browser.get(url);
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Return the browser title
     * @returns {string} browser title
     */
    async getBrowserTitle() {
        const browserTitle = await browser.getTitle();
        return browserTitle;
    }
}

module.exports = ErrorPageActions;
