/* eslint-disable prefer-destructuring */
const {getRandomArrayItem} = require('../../common/helpers/mathHelper');

class CancelWithdrawRequestActions {
    /**
     * This method basing on test descriptor finds notification and filters out absence periods with status
     * different than cancelled, then it removes duplicated periods with same start date, end date, period type,
     * absence reason
     * @param {string} testDescriptor
     * @returns {Array} list of unique periods
     */
    async getCancellablePeriods() {
        const _ = require('lodash');
        const plansWithAllData = await this.plansWitAllData();
        const plansWithEmptyEndDates = await this.plansWithEmptyEndDate();
        const firstFilter = _.uniqBy(plansWithAllData);
        const secondFilter = _.uniqBy(plansWithEmptyEndDates);
        let filteredData = _.concat(firstFilter, secondFilter);
        let merged = [];
        const temp = [];

        if (filteredData.length === 2) {
            if (_.isEqual(filteredData[0], filteredData[1])) {
                merged = _.merge(filteredData[0], filteredData[1]);
                temp.push(merged);
                filteredData = temp;
            }
        }

        return filteredData;
    }

    /**
     * This method removes duplicates in provided array
     * @param {(string|promise.Promise<string>)[]} arrayOfStrings
     * @returns {any[]}
     */
    removeDuplicatesInArray(arrayOfStrings) {
        return [...new Set(arrayOfStrings)];
    }

    /**
     * This method returns plans with empty end date
     * @returns {plansWithEmptyEndDate[]}
     */
    async plansWithEmptyEndDate() {
        const plansWithEmptyEndDate = [];
        for (let i = 0; i < testVars.expandedLeaveCards.length; i++) {
            if (await pages.erp.notificationPage.leaveCardStatus(await testVars.expandedLeaveCards[i]).get(0).getText() !== 'Cancelled' &&
                await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).count() === 2) {
                const planName = await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).get(
                    0).getText();
                const requestThrough = await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).get(
                    1).getText();
                await plansWithEmptyEndDate.push([planName, requestThrough]);
            }
        }
        return plansWithEmptyEndDate;
    }

    /**
     * This method returns plans with all data
     * @returns {plansWithAllData[]}
     */
    async plansWitAllData() {
        const plansWithAllData = [];
        for (let i = 0; i < testVars.expandedLeaveCards.length; i++) {
            if (await pages.erp.notificationPage.leaveCardStatus(await testVars.expandedLeaveCards[i]).get(0).getText() !== 'Cancelled' &&
                await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).count() === 3) {
                const planName = await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).get(
                    0).getText();
                const starDate = await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).get(
                    1).getText();
                const endDate = await pages.erp.notificationPage.leaveCardValues(await testVars.expandedLeaveCards[i]).get(
                    2).getText();
                await plansWithAllData.push([planName, starDate, endDate]);
            }
        }
        return plansWithAllData;
    }

    /**
     * This method verifies the correct number of filtered periods between cancel withdraw request and notification page
     * @returns {Promise<void>}
     */
    async verifyPeriodsList() {
        const filteredData = await this.getCancellablePeriods();
        const numberOfCards = await pages.erp.cancelWithDrawRequestPage.leaveReasonCards().count();
        const leaveCards = [];

        expect(numberOfCards).is.greaterThan(0);
        expect(filteredData.length).to.equal(numberOfCards);

        for (let i = 0; i < filteredData.length; i++) {
            leaveCards.push(await pages.erp.cancelWithDrawRequestPage.leaveReasonCards().get(i).getText());
        }

        for (let i = 0; i < filteredData.length; i++) {
            const cards = [];
            for (let i = 0; i < await pages.erp.cancelWithDrawRequestPage.leaveReasonCards().length; i++) {
                cards.push(await pages.erp.cancelWithDrawRequestPage.leaveReasonCards().get(i).getText());
            }

            const occupation = filteredData[i][0];
            const date = filteredData[i][1];

            expect(leaveCards.some(el => el.includes(occupation))).to.be.true;
            expect(leaveCards.some(el => el.includes(date))).to.be.true;
        }
    }

    /**
     * This method selects reason for cancel withdraw request
     * @returns {Promise<void>}
     */
    async selectRandomCancelReason() {
        await pages.erp.cancelWithDrawRequestPage.selectCancelReasonInput().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.cancelReasonsList().first());
        const randomItem = getRandomArrayItem(await pages.erp.cancelWithDrawRequestPage.cancelReasonsList());
        await randomItem.Click();
    }
}

module.exports = CancelWithdrawRequestActions;
