class NotificationActions {

    /**
     * Gets string value of field located by translation key (label)
     * @param {string} translationId - translation key
     * @param {ElementFinder} parentElement - optional parent element to search in reduced scope
     * @param {number} [maxDepth=3] - max number of jumps-up in the DOM tree should we make in order to find value
     * @returns {Promise<null|string>}
     */
    async getValueByTranslationId(translationId, parentElement = null, maxDepth = 3) {
        const labelQuery = `[data-test-el*="${translationId}"]`;
        const valueQuery = '[data-test-el="property-item-value"]';
        const results = parentElement ? parentElement.$$(labelQuery) : $$(labelQuery);
        const labelElement = (await results.count()) > 0 ? results.first() : null;
        if (labelElement) {
            let parent = labelElement;
            for (let depth = 0; depth < maxDepth; depth++) {
                parent = parent.element(by.xpath('..'));
                const values = await parent.$$(valueQuery).count();
                if (values === 1) {
                    return parent.$(valueQuery).getText();
                }
            }
        }
    }

    /**
     * Get period type basing on css style of the left bar indicator
     * @param {ElementFinder} periodCardElement - web locator of period section
     * @returns {Promise<string|null>}
     */
    async getPeriodTypeFromBar(periodCardElement) {
        const attributes = await periodCardElement.getAttribute(constants.htmlAttributes.CLASS);
        if (attributes.includes(constants.jobAbsenceLegend.REDUCED_SCHEDULE)) {
            return constants.periodTypes.REDUCED_SCHEDULE;
        } else if (attributes.includes(constants.jobAbsenceLegend.INTERMITTENT_TIME)) {
            return constants.periodTypes.INTERMITTENT_TIME;
        } else if (attributes.includes(constants.jobAbsenceLegend.CONTINUOUS_TIME)) {
            return constants.periodTypes.CONTINUOUS_TIME;
        }
        throw new Error('Given element doesnt have any known period types');
    }

    /**
     * Map item colour basing on css class names
     * @param {ElementFinder} colorElement - web element containing color class attribute (works with both dots and
     *     bars)
     * @returns {Promise<string|null>}
     */
    async mapItemColor(colorElement) {
        const attributes = await colorElement.getAttribute(constants.htmlAttributes.CLASS);
        if (attributes.includes(constants.colorClasses.APPROVED)) {
            return constants.colorNames.APPROVED;
        } else if (attributes.includes(constants.colorClasses.PENDING)) {
            return constants.colorNames.PENDING;
        } else if (attributes.includes(constants.colorClasses.DENIED)) {
            return constants.colorNames.DENIED;
        } else if (attributes.includes(constants.colorClasses.CANCELLED)) {
            return constants.colorNames.CANCELLED;
        }
        throw new Error('Given element doesnt have any known colour classes');
    }

    /**
     * Grab all data from existing job protected cards, store it in testVars and execute grab operation only when
     * no previous data stored
     * @returns {Promise<Array<Object>>}
     */
    async getJobProtectedData() {
        if (testVars.jobProtectedData) {
            return testVars.jobProtectedData;
        }
        const job = constants.notificationCards.JOB_PROTECTED_LEAVE;
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(job).first());
        const leavePlans = pages.erp.notificationPage.collapsibleCards(job);
        const result = [];

        for (let index = 0; index < (await leavePlans.count()); index++) {
            const leavePlan = leavePlans.get(index);
            const leavePlanName = await pages.erp.notificationPage.jobLeavePlanHeader(leavePlan).getText();
            const periodsList = pages.erp.notificationPage.jobPeriodsList(leavePlan);
            await leavePlan.Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeStatic(periodsList.first());
            const managedBy = await this.getValueByTranslationId('HANDLER_INFO.MANAGED_BY', leavePlan);
            const phoneNumber = await this.getValueByTranslationId('HANDLER_INFO.PHONE_NUMBER', leavePlan);
            const periodsCount = await periodsList.count();
            const planDetails = {leavePlanName, managedBy, periods: [], phoneNumber};
            for (let i = 0; i < periodsCount; i++) {
                const scope = periodsList.get(i);
                await scope.Scroll();
                const period = {};
                period.startDate = await this.getValueByTranslationId('_BEGINS', scope) ||
                    (await this.getValueByTranslationId('_BETWEEN', scope)).split(' - ')[0];
                period.endDate = await this.getValueByTranslationId('_THROUGH', scope) ||
                    (await this.getValueByTranslationId('_BETWEEN', scope)).split(' - ')[1];
                period.absenceReason = await this.getValueByTranslationId('ABSENCE_REASON', scope);
                period.status = await pages.erp.notificationPage.caseStatusText(scope).getText();
                period.type = await this.getPeriodTypeFromBar(scope);
                period.barColor = await this.mapItemColor(scope);
                period.dotColor = await this.mapItemColor(pages.erp.notificationPage.statusIndicatorField(scope));
                period.fieldNames = await pages.erp.notificationPage.genericLabelsList(scope)
                    .map(async label => await label.getText());
                planDetails.periods.push(period);
            }
            result.push(planDetails);
        }
        testVars.jobProtectedData = result;
        return result;
    }

    /**
     * Gets all periods from job protected cards - regardless of leave period names - squash all periods to one array
     * @returns {Promise<Array<Object>>}
     */
    async getAllJobPeriods() {
        const jobProtectedData = await this.getJobProtectedData();
        const allPeriods = [];
        for (const plan of jobProtectedData) {
            allPeriods.push(...plan.periods);
        }
        return allPeriods;
    }

    /**
     * Gets all benefits elements text headers from all Wage Replacement cards.
     *
     * @param {string} cardName - notification card name e.g. Wage Replacement, etc.
     * @returns {Promise<Array<String>>}
     */
    async mergeWageReplacementBenefits(cardName) {
        const benefits = [];
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(cardName).first());
        await pages.erp.notificationPage.cards(cardName).each(async card => {
            await pages.erp.notificationPage.collapsibleCardsOfParent(card).each(async item => {
                benefits.push((await item.getText()).split('\n')[0].trim());
            });
        });

        return benefits;
    }

    /**
     * Gets all Job Protected Leave elements texts headers.
     *
     * @param {string} cardName - notification card name e.g. Wage Replacement, etc.
     * @returns {Promise<Array<String>>}
     */
    async getJobProtectedLeaveCards(cardName) {
        const cardItems = [];
        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.collapsibleCards(cardName).first());
        await pages.erp.notificationPage.collapsibleCards(cardName).each(async item => {
            cardItems.push((await item.getText()).split('\n')[0].trim());
        });

        return cardItems;
    }

    /**
     * Gets all Temporary Accommodation elements texts headers.
     *
     * @param {string} cardName - notification card name e.g. Wage Replacement, etc.
     * @returns {Promise<Array<String>>}
     */
    async filterTemporaryAccommodations(cardName) {
        const cardItems = [];
        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.collapsibleCards(cardName).first());

        for (let i = 0; i < await pages.erp.notificationPage.collapsibleCards(cardName).count(); i++) {
            const item = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(
                cardName).get(i));

            if ((await item.getText()).toLowerCase().includes('\naccommodated')) {
                await item.Click({waitForVisibility: true});
                await waitHelpers.waitForElementToBeStatic(await pages.erp.notificationPage.proposedAccommodations(
                    item).first());

                await pages.erp.notificationPage.proposedAccommodations(item).each(async subItem => {
                    const actualAccommodation = await subItem.getText();
                    await subItem.Click({waitForVisibility: true});

                    const obtainedDate = await waitHelpers.waitForElementToBeStatic(await pages.erp.notificationPage.caseDetailsFields(
                        subItem,
                        'Accommodation date'
                    ).last());
                    if ((await obtainedDate.getText()).includes('-')) {
                        cardItems.push(actualAccommodation);
                    }
                });
            }
        }

        return cardItems;
    }

    /**
     * Get all payment adjustments and sum amount and return it
     *
     * @param {ElementFinder} payment - payment element with adjustments
     * @returns {Number} - sum of all adjustments
     */
    async sumAdjustmentsAmounts(payment) {
        let sum = 0;

        await pages.erp.notificationPage.adjustmentHeaders(payment).each(async singleAdjustment => {
            // eslint-disable-next-line prefer-destructuring
            let actualAmount = (await singleAdjustment.getText()).split(/\n/)[1];

            if (actualAmount.startsWith('-')) {
                actualAmount = `-${actualAmount.slice(2)}`;
            } else {
                actualAmount = actualAmount.substring(1);
            }

            sum += Number(actualAmount);
        });

        return sum;
    }

    /**
     * Get last payment element
     *
     * @returns {ElementFinder} - last payment element
     */
    async getLastPayment() {
        const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
        const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));
        return await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsiblePayments(
            latestPaymentsSection).first());
    }

    /**
     * Find and return notification card item with expected name
     * @param {string} cardName - notification card name e.g Wage Replacement, etc.
     * @param {string} itemName - card item name e.g LTD Benefit, etc.
     *
     * @returns {ElementFinder|null} - item with expected name
     */
    async getItemByName(cardName, itemName) {
        let found = false;
        let foundItem = null;
        await pages.erp.notificationPage.cards(cardName).each(async card => {
            await pages.erp.notificationPage.collapsibleCardsOfParent(card).each(async item => {
                if ((await item.getText()).includes(itemName) && found === false) {
                    foundItem = item;
                    found = true;
                }
            });
        });
        return foundItem;
    }

    /**
     * This method returns filtered array of outstanding items
     * @param {string} expectedStatus - outstanding item status (provided, not-provided)
     *
     * @return {ElementArrayFinder} - array with filtered elements
     */
    async filterOutstandingItems(expectedStatus) {
        return await pages.erp.notificationPage.allOutstandingItems().filter(async item => (await item.getText()).includes(
            expectedStatus));
    }

    /**
     * This method returns filtered array of read/unread documents
     * @param {string} expectedStatus - outstanding item status (read, unread)
     *
     * @return {ElementArrayFinder} - array with filtered elements
     */
    async filterDocumentsByStatus(expectedStatus) {
        return await pages.erp.notificationPage.documentsList().filter(async item => (await pages.erp.notificationPage.documentItemIcon(
            item).getAttribute(
            'class'
        )).includes(
            ` ${expectedStatus}`));
    }

    /**
     * This method reads las document if not read already
     */
    async readLastDocument() {
        const lastDocument = await pages.erp.notificationPage.documentsList().last();
        const actualColor = await pages.erp.notificationPage.documentItemIcon(lastDocument).getCssValue('color');

        if (actualColor === constants.colors.PURPLE) {
            const lastDocumentButton = pages.erp.notificationPage.documentsButtonsList().last();
            await lastDocumentButton.Scroll();
            await lastDocumentButton.Click({waitForVisibility: true});

            await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.documentItemUnreadIcon(
                lastDocument));
            await waitHelpers.waitForSpinnersToDisappear();
        }
    }

    /**
     * This method returns all not provided outstanding items
     *
     * @return {ElementArrayFinder} - array with not provided outstanding items
     */
    async getNotProvidedOutstandingItems() {
        const status = translationsHelper.getTranslation('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD');
        const notProvidedItems = await this.filterOutstandingItems(status.split(':')[0]);

        return notProvidedItems;
    }

    /**
     * This method returns all provided outstanding items
     *
     * @return {ElementArrayFinder} - array with not provided outstanding items
     */
    async getProvidedOutstandingItems() {
        const status = translationsHelper.getTranslation('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED');
        const providedItems = await this.filterOutstandingItems(status);

        return providedItems;
    }

    /**
     * This method maps option name used in ui to name used in data-test-el
     * @param {string} optionName - actions dropdown option name to be mapped
     * @return {string} - mapped option name
     */

    mapActionsDropdownOptions(optionName) {
        switch (optionName.trim().toLowerCase()) {
            case constants.notificationSummaryActions.ADD_REQUEST.key:
                return constants.notificationSummaryActions.ADD_REQUEST.value;
            case constants.notificationSummaryActions.REMOVE_REQUEST.key:
                return constants.notificationSummaryActions.REMOVE_REQUEST.value;
            default:
                throw new Error(`given option name [${optionName}] is not supported`);
        }
    }
}

module.exports = NotificationActions;
