const moment = require('moment');

class IntermittentTimeActions {
    /**
     * This method checks if the date list of intermittent time episodes is sorted ascending or descenging
     * @returns {Promise<void>}
     */
    async verifyIntermittentTimeDateSorting(ascendingOrDescending) {
        const episodeDates = [];
        await waitHelpers.waitForElementToBeVisible(pages.erp.intermittentTimeModal.episodeDates().last());
        await pages.erp.intermittentTimeModal.episodeDates().each(async singleDate => {
            const actualDateString = await singleDate.getText();
            const actualDate = moment(actualDateString, constants.dateFormats.shortDate, true).toDate();
            episodeDates.push(moment(actualDate, constants.dateFormats.shortDate, true));
        });
        expect(episodeDates.length).is.greaterThan(0);
        ascendingOrDescending === 'ascending' ? expect(episodeDates).to.be.sorted({ascending: true}) : expect(episodeDates).to.be.sorted(
            {descending: true});
    }

    /**
     * This method checks if the provided status is properly filtered for all episodes
     * @returns {Promise<void>}
     * @param  {string} status
     */
    async verifyAllFilteredStatuses(status) {
        const statusesList = [];
        await waitHelpers.waitForElementToBeVisible(pages.erp.intermittentTimeModal.decisionStatuses().last());
        await pages.erp.intermittentTimeModal.decisionStatuses().each(async status => {
            const statusString = await status.getText();
            statusesList.push(statusString);
        });

        for (let i = 0; i < statusesList.length; i++) {
            expect(statusesList[i]).to.eq(status);
        }
    }

    /**
     * This method checks if the each cell in intermittent time list is filled with data
     * @returns {Promise<void>}
     */
    async verifyDatesDurationDecisionIsNotEmptyOnTheList() {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.intermittentTimeDates().last());
        await pages.erp.notificationPage.intermittentTimeDates().each(async date => {
            expect(await date.getText()).to.be.not.empty;
        });
        await pages.erp.notificationPage.intermittentTimeDuration().each(async duration => {
            expect(await duration.getText()).to.be.not.empty;
        });
        await pages.erp.notificationPage.intermittentTimeStatus().each(async status => {
            expect(await status.getText()).to.be.not.empty;
        });
    }

    /**
     * This method verifies if each date cell contains a date in a correct format displayed in the pop-up
     * @returns {Promise<void>}
     */
    async verifyDateFormatInThePopUp() {
        await this.verifyDateFormatOfTheListOfElements(pages.erp.intermittentTimeModal.episodeDates());
    }

    /**
     * This method verifies if each date cell contains a date in a correct format displayed in the pop-up
     * @returns {Promise<void>}
     */
    async verifyDateFormatOnTheList() {
        await this.verifyDateFormatOfTheListOfElements(pages.erp.notificationPage.intermittentTimeDates());
    }

    /**
     * This method verifies if each date cell contains a date in a correct format displayed
     * @returns {Promise<void>}
     * @param {ElementArrayFinder} listElements - list with elements storing dates
     */
    async verifyDateFormatOfTheListOfElements(listOfElements) {
        await waitHelpers.waitForElementToBeVisible(listOfElements.last());
        await listOfElements.each(async singleDate => {
            const actualDateText = await singleDate.getText();
            const actualDate = moment(actualDateText, constants.dateFormats.shortDate, true).toDate();
            moment(moment(actualDate).format(constants.dateFormats.shortDate), constants.dateFormats.shortDate, true).isValid();
        });
    }
}

module.exports = IntermittentTimeActions;
