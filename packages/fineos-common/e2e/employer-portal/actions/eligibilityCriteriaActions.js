const faker = require('faker');

class EligibilityCriteriaActions {

    /**
     * Function get checkbox element using its name
     *
     * @param {string} checkboxName
     * @param {string} actualCheckboxState actual checkbox state - true, false
     * @returns {ElementFinder} - checkbox element
     */
    getCheckboxElementByName(checkboxName, actualCheckboxState) {
        switch (checkboxName) {
            case 'Within FMLA radius':
                return pages.erp.employeeProfilePage.fmlaCheckboxCheckbox(actualCheckboxState.toString().toUpperCase());
            case 'Works at home':
                return pages.erp.employeeProfilePage.worksAtHomeCheckbox(actualCheckboxState.toString().toUpperCase());
            case 'Key employee':
                return pages.erp.employeeProfilePage.keyEmployeeCheckbox(actualCheckboxState.toString().toUpperCase());
            default:
                throw new Error(`given checkbox name [${checkboxName}] is not supported`);
        }
    }

    /**
     * Function get checkbox translation using its name
     *
     * @param {string} checkboxName
     * @param {string} actualCheckboxState actual checkbox state - true, false
     * @returns {string} - checkbox translation
     */
    getCheckboxTranslationByName(checkboxName, actualCheckboxState) {
        switch (checkboxName) {
            case 'Within FMLA radius':
                return translationsHelper.getTranslation(
                    `PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_WITH_FMLA_${actualCheckboxState.toString().toUpperCase()}`);
            case 'Works at home':
                return translationsHelper.getTranslation(
                    `PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_AT_HOME_${actualCheckboxState.toString().toUpperCase()}`);
            case 'Key employee':
                return translationsHelper.getTranslation(
                    `PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.IS_KEY_EMPLOYEE_${actualCheckboxState.toString().toUpperCase()}`);
            default:
                throw new Error(`given checkbox name [${checkboxName}] is not supported`);
        }
    }

    /**
     * Function generates faked test data depending on given data type
     *
     * @param {string} dataType - actualworkedhours, cba, workstate
     * @param {[string]} testDataArray = array with available stated abbreviations, etc. ['Full time',
     *     'Salaried']
     * @returns {string} generated test data
     */
    generateEligibilityCriteriaData(dataType, testDataArray = []) {
        switch (dataType) {
            case 'actualhoursworked':
                return faker.random.number({max: 8760, min: 0});
            case 'cba':
                return faker.random.number({max: 9999999999999999999, min: 0});
            case 'workstate':
                return faker.random.arrayElement(testDataArray);
            case 'withinfmlaradius':
            case 'worksathome':
            case 'keyemployee':
                return faker.random.arrayElement([true, false]);
            default:
                return `Test data was not generated for unsupported data type ${dataType}`;
        }
    }
}

module.exports = EligibilityCriteriaActions;
