const faker = require('faker');

class EditWorkPatternActions {

    /**
     * Returns input element by its name
     * @param {string} fieldName - list available field names are available in switch cases
     * @returns {Promise<ElementFinder>}
     */
    async getWorkPatternInputField(fieldName) {
        switch (fieldName) {
            case 'pattern description':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.additionalInformationTextArea());
            case 'number of weeks':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.numberOfWeeksInput());
            case 'day length':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.dayLengthInput());
            default:
                throw new Error(`given field name [${fieldName}] is not supported`);
        }
    }

    /**
     * Returns switch element by its name
     * @param {string} switchName - list available field names are available in switch cases
     * @returns {Promise<ElementFinder>}
     */
    async getWorkPatternSwitch(switchName) {
        switch (switchName) {
            case 'pattern repeats':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatterRepeatsSwitch());
            case 'day length same':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.theSameLengthDaySwitch());
            case 'non standard working week':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.nonStandardWorkingWeekSwitch());
            case 'include weekend':
                return await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.includeWeekendSwitchOff());
            default:
                throw new Error(`given switch name [${switchName}] is not supported`);
        }
    }

    /**
     * Returns error name by its type
     * @param {string} errorType - list available field names are available in switch cases
     * @returns {string} - error name
     */
    getValidationErrorName(errorType) {
        switch (errorType) {
            case 'not provided information':
                return 'WORK_PATTERN.VALIDATION.ADDITIONAL_INFORMATION_VALIDATION';
            case 'required':
                return 'FINEOS_COMMON.VALIDATION.REQUIRED';
            case 'invalid time format':
                return 'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT';
            case 'at least one filled':
                return 'FINEOS_COMMON.VALIDATION.AT_LEAST_ONE_FILLED';
            default:
                throw new Error(`given error type [${errorType}] is not supported`);
        }
    }

    /**
     * Function generates faked test data depending on given data type
     *
     * @param {string} dataType - workingdays.
     * @param {[string]} statesArray = array with available stated abbreviations, etc. ['CA', 'NY']
     * @returns {string} generated test data
     */
    generateWorkPatternData(dataType, statesArray = []) {
        switch (dataType) {
            case 'workingdays':
                return faker.random.arrayElement(['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY']);
            case 'weekenddays':
                return faker.random.arrayElement(['SATURDAY', 'SUNDAY']);
            case 'hours':
                // eslint-disable-next-line no-case-declarations
                const hours = (`0${faker.random.number(23)}`).slice(-2);
                // eslint-disable-next-line no-case-declarations
                const minutes = (`0${faker.random.number(59)}`).slice(-2);

                return `${hours}:${minutes}`;
            default:
                throw new Error(`Test data was not generated for unsupported data type ${dataType}`);
        }
    }

    /**
     * Function records work pattern data in edit mode before saved.
     */
    async recordEditedWorkPatternData() {
        const actualWorkPatternType = (await pages.erp.editWorkPatternPage.workPatternTypeSelect().getText()).toString().toLowerCase();
        const actualWorkingHours = (await pages.erp.editWorkPatternPage.allDaysInputs().getAttribute('value')).toString();

        testVars.workPattern.editedWorkPatternType = actualWorkPatternType;
        testVars.workPattern.editedWorkingHours = actualWorkingHours;
    }

    /**
     * Function makes following time conversion e.g. 08:12 to 8h 12m
     * @param {array} array with work times to be converted
     *
     * @returns {array}
     */
    convertWorkingTime(workingTimesArray) {
        workingTimesArray.forEach((workingTime, index) => {
            if (workingTime.includes(':')) {
                workingTime.split(':').forEach(el => {
                    if (el.startsWith('0')) {
                        workingTime = workingTime.replace(el, parseInt(el, 10));
                    }
                });

                workingTimesArray[index] = `${workingTime.replace(':', 'h ')}m`;
            }
        });

        return workingTimesArray;
    }
}

module.exports = EditWorkPatternActions;


