const users = require('./../constants/users');

class ControlUserAccessActions {

    /**
     * Opens user access page
     * @returns {Promise<void>}
     */
    async openPage() {
        await pages.erp.searchPage.userMenu().Click({waitForVisibility: true});
        await pages.erp.searchPage.optionControlUserAccess().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.controlUserAccessPage.pageHeader(),
            timeouts.T60,
            'I should see Control User Access page'
        );
    }

    /**
     * Gets index of user on users list
     * @param {string} userDescriptor
     * @returns {Promise<number>}
     */
    async getIndexOfUser(userDescriptor) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.controlUserAccessPage.usersList().first(),
            timeouts.T60,
            'I should see portal users list'
        );
        const testUser = users.getUser(userDescriptor);
        const usersOnTheList = await pages.erp.controlUserAccessPage.usersList()
            .map(async item => await pages.erp.controlUserAccessPage.userName(item).getText());
        return usersOnTheList.findIndex(i => i.indexOf(testUser.displayName) > -1);
    }

    /**
     * Enable given user
     * @param {string} [userDescriptor=constants.userTypes.ENABLE_DISABLE_USER] - user descriptor
     * Accepted values = constants.userTypes.*
     * @returns {Promise<void>}
     */
    async enableUser(userDescriptor = constants.userTypes.HR_TEST_USER) {
        const testUser = users.getUser(userDescriptor).displayName;
        const userIndex = await this.getIndexOfUser(userDescriptor);
        expect(userIndex).to.be.gt(-1, `Expected to see [${testUser}] on the portal users list`);
        const userBar = pages.erp.controlUserAccessPage.usersList().get(userIndex);
        const userEnabled = await pages.erp.controlUserAccessPage.iconEnabled(userBar).isPresent();
        if (!userEnabled) {
            await pages.erp.controlUserAccessPage.actionButton(userBar).Click();
            await waitHelpers.waitForElementToBeVisible(
                pages.erp.controlUserAccessPage.iconEnabled(userBar),
                timeouts.T60,
                `User [${testUser}] should be enabled`
            );
            await waitHelpers.waitForSpinnersToDisappear();
        }
    }

    /**
     * Disable given user
     * @param {string} [userDescriptor=constants.userTypes.ENABLE_DISABLE_USER] - user descriptor
     * Accepted values = constants.userTypes.*
     * @param {boolean} [confirmPopup=true] - confirm popup ?
     * @returns {Promise<void>}
     */
    async disableUser(userDescriptor = constants.userTypes.HR_TEST_USER, confirmPopup = true) {
        const testUser = users.getUser(userDescriptor).displayName;
        const userIndex = await this.getIndexOfUser(userDescriptor);
        expect(userIndex).to.be.gt(-1, `Expected to see [${testUser}] on the portal users list`);
        const userBar = pages.erp.controlUserAccessPage.usersList().get(userIndex);
        const userDisabled = await pages.erp.controlUserAccessPage.iconDisabled(userBar).isPresent();
        if (!userDisabled) {
            await pages.erp.controlUserAccessPage.actionButton(userBar).Click();
            await waitHelpers.waitForElementToBeVisible(
                pages.erp.controlUserAccessPage.confirmationPopover(),
                timeouts.T60,
                'I should see confirmation dialog'
            );
            if (confirmPopup) {
                await pages.erp.controlUserAccessPage.buttonYesOnConfirmationPopover().Click();
                await waitHelpers.waitForElementToBeVisible(
                    pages.erp.controlUserAccessPage.iconDisabled(userBar),
                    timeouts.T60,
                    `User [${testUser}] should be disabled`
                );
            }
        }
    }

    /**
     * Checks if given user gets enabled in given time
     * @param {string} state - user state - enabled or disabled
     * @param {string} [userDescriptor=constants.userTypes.ENABLE_DISABLE_USER] - user descriptor
     * Accepted values = constants.userTypes.*
     * @returns {Promise<boolean>}
     */
    async waitForState(state, userDescriptor) {
        const userIndex = await this.getIndexOfUser(userDescriptor);
        const userBar = pages.erp.controlUserAccessPage.usersList().get(userIndex);
        const statusIndicator = (state === constants.stepWording.ENABLED)
            ? pages.erp.controlUserAccessPage.iconEnabled(userBar)
            : pages.erp.controlUserAccessPage.iconDisabled(userBar);
        await waitHelpers.waitForElementToBeVisible(
            statusIndicator,
            timeouts.T60,
            `User [${userDescriptor}] should be in ${state} state`
        );
    }

    /**
     * This method get random user from users list
     * @returns {string} existing random user
     */
    async getExistingRandomUserName() {
        const disabled = `(${translationsHelper.getTranslation('CONTROL_USER_ACCESS.DISABLED')})`;
        const actualUsers = await pages.erp.controlUserAccessPage.userNamesList().getText();
        const existingRandomUser = testDataHelpers.getRandomArrayElement(actualUsers.toString().split(',')).toString();

        return existingRandomUser.toString().replace(disabled, '');
    }
}

module.exports = ControlUserAccessActions;
