const notifications = require('./../constants/notifications');
const employees = require('./../constants/employees');
const moment = require('moment');
const faker = require('faker');
const {getRandomArrayItem} = require('../../common/helpers/mathHelper');
const {getRequestDataArray} = require('../../common/helpers/snifferHelper');
const {getAllRequestsDataIntoArray} = require('../../common/helpers/snifferHelper');

class NotificationsWithOutstandingReqActions {

    /**
     * Gets web element basing on field nme and notification ID
     * @param {string} name - field name
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    getFieldByName(name, notificationId) {
        let testElement;
        switch (name) {
            case constants.fieldNames.NOTIFICATION_ID:
            case constants.fieldNames.NOTIFICATION_REASON:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellNotification(
                    notificationId);
                break;
            case constants.fieldNames.EMPLOYEE_NAME_ID:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellEmployee(notificationId);
                break;
            case constants.fieldNames.JOB_TITLE:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellEmployeeJobTitle(
                    notificationId);
                break;
            case constants.fieldNames.ORGANIZATION_UNIT:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellEmployeeOrganizationUnit(
                    notificationId);
                break;
            case constants.fieldNames.WORK_SITE:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellEmployeeWorkSite(
                    notificationId);
                break;
            case constants.fieldNames.NOTIFICATION_CREATION_DATE:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellNotificationsDate(
                    notificationId);
                break;
            case constants.fieldNames.VIEW_ACTIONS:
                testElement = pages.erp.notificationsWithOutstandingReqPage.tableCellActions(
                    notificationId);
                break;
            default:
                throw new Error(`The field [${name}] is not mapped in this action`);
        }
        return testElement;
    }

    /**
     * Calculate number of all notifications available for user. Pagination is used to calculate it.
     *
     * @return {Number} - number of all notifications
     */
    async countNumberOfAllNotifications() {
        const RADIX = 10;
        let numberOfNotifications = -1;
        await waitHelpers.waitForSpinnersToDisappear();

        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().last(),
            timeouts.T1
        ).then(async () => {
            await waitHelpers.waitForElementToBeStatic(pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().last());
            const paginationPages = await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().last().getAttribute(
                'title');
            await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().last().Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();

            const pageSize = (await pages.erp.notificationsWithOutstandingReqPage.paginationOptionsButton().getText()).split(
                '/')[0].trim();
            const lastPageNotifications = await pages.erp.notificationsWithOutstandingReqPage.notificationRows().count();

            await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().first().Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();

            numberOfNotifications = Number.parseInt(
                pageSize,
                RADIX
            ) * (Number.parseInt(paginationPages, RADIX) - 1) + lastPageNotifications;
        }, async () => {
            numberOfNotifications = Number.parseInt(
                await pages.erp.notificationsWithOutstandingReqPage.notificationRows().count(),
                RADIX
            );
        });
        return numberOfNotifications;
    }

    /**
     * Calculate expected number of pages with notifications. Pagination is used for calculations.
     *
     * @return - number expected number of pages with notifications
     */
    async countNumberOfPages() {
        const displayedNumberOfNotifications = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.numberOfNotifications().getText());
        const actualPageSize = (await pages.erp.notificationsWithOutstandingReqPage.paginationOptionsButton().getText()).split(
            '/')[0].trim();
        const expectedNumberOfPages = Number.parseInt(displayedNumberOfNotifications.toString(), 10) / Number.parseInt(
            actualPageSize,
            10
        );

        return Math.ceil(expectedNumberOfPages);
    }

    /**
     * Filter data on Home page using Notification Id
     *
     * @param {string} testDescriptor - descriptor of test data taken from notifications.json
     */
    async filterNotificationsById(testDescriptor) {
        const notificationId = notifications.getNotification(testDescriptor).id;
        await pages.erp.notificationsWithOutstandingReqPage.searchNotificationButton().Click({waitForVisibility: true});
        await pages.erp.notificationsWithOutstandingReqPage.inputSearchNotification().Clear();
        await pages.erp.notificationsWithOutstandingReqPage.inputSearchNotification().Click({waitForVisibility: true});
        await pages.erp.notificationsWithOutstandingReqPage.inputSearchNotification().SendText(notificationId);
        testVars.searchQuery = notificationId;
    }

    /**
     * Filter data on Home page using Employee First Name or Employee Last Name
     *
     * @param {string} searchTarget - name of field used to filter data etc. "Employee First Name", "Employee Last Name"
     * @param {string} testDescriptor - descriptor of test data taken from notifications.json
     */
    async filterNotificationsByEmployee(filterName, testDescriptor) {
        const employee = employees.getEmployee(testDescriptor);
        await pages.erp.notificationsWithOutstandingReqPage.searchEmployeeNameButton().Click({waitForVisibility: true});

        if (filterName === constants.searchTargets.EMPLOYEE_FIRST_NAME) {
            await pages.erp.notificationsWithOutstandingReqPage.inputSearchFirstName().Click({waitForVisibility: true});
            await pages.erp.notificationsWithOutstandingReqPage.inputSearchFirstName().Clear();
            await pages.erp.notificationsWithOutstandingReqPage.inputSearchFirstName().SendText(employee.firstName);
            testVars.searchQuery = employee.firstName;
        } else {
            await pages.erp.notificationsWithOutstandingReqPage.inputSearchLastName().Click({waitForVisibility: true});
            await pages.erp.notificationsWithOutstandingReqPage.inputSearchLastName().Clear();
            await pages.erp.notificationsWithOutstandingReqPage.inputSearchLastName().SendText(employee.lastName);
            testVars.searchQuery = employee.lastName;
        }
    }

    /**
     * Get actual notifications sorting
     *
     * @return {string} - actual notifications sorting state
     */
    async getActualSorting() {
        let actualState = null;
        const upArrowState = await pages.erp.notificationsWithOutstandingReqPage.sortButton('up').getCssValue(
            'color') === constants.stateColors.ACTIVE_1;
        const downArrowState = await pages.erp.notificationsWithOutstandingReqPage.sortButton('down').getCssValue(
            'color') === constants.stateColors.ACTIVE_1;

        if (upArrowState === false && downArrowState === false) {
            actualState = constants.sortingTypes.CANCEL;
        } else if (upArrowState === false && downArrowState === true) {
            actualState = constants.sortingTypes.DESCENDING;
        } else if (upArrowState === true && downArrowState === false) {
            actualState = constants.sortingTypes.ASCENDING;
        }

        return actualState.toString();
    }

    /**
     *
     * Set notifications sorting - this method sorts notification in ascending/descending order
     */
    async setNotificationsSorting(columnName, actualSortingState, sortingOrder) {
        const sortButton = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedColumnHeader());

        switch (sortingOrder) {
            case constants.sortingTypes.ASCENDING:
                if (actualSortingState === constants.sortingTypes.DESCENDING) {
                    await sortButton.Click({waitForVisibility: true});
                    await pages.erp.notificationsWithOutstandingReqPage.columnHeader(columnName).Click({waitForVisibility: true});
                } else if (actualSortingState === constants.sortingTypes.CANCEL) {
                    await sortButton.Click({waitForVisibility: true});
                }
                break;
            case constants.sortingTypes.DESCENDING:
                if (actualSortingState === constants.sortingTypes.ASCENDING) {
                    await sortButton.Click({waitForVisibility: true});
                } else if (actualSortingState === constants.sortingTypes.CANCEL) {
                    await sortButton.Click({waitForVisibility: true});
                    await sortButton.Click({waitForVisibility: true});
                }
                break;
            default:
                throw new Error(`given sorting order [${sortingOrder}] is not supported`);
        }
        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Check if "Notifications with outstanding requirements" drawer elements are visible
     */
    async checkIfDrawerElementsAreVisible() {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.notificationRows().last());
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.downloadListButton());
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.searchEmployeeNameButton());
        await waitHelpers.waitForElementToBeVisible(pages.erp.sharedModal.closeDrawerButton());
        await waitHelpers.waitForElementToBeVisible(pages.erp.showHidePanelsModal.closeXButton());
    }

    /**
     * Check if "Notifications with outstanding requirements" drawer elements are not visible
     */
    async checkIfDrawerElementsAreNotVisible() {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.notificationRows().last());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.paginationPreviousButton());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.paginationNextButton());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().last());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.paginationOptionsButton());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.searchEmployeeNameButton());
    }

    getFilteringDateRange() {
        const dates = [];
        const actualDateRange = testVars.filtering.dateRange.match(/\(.+\)/).toString().slice(1, -1);
        actualDateRange.split('-').map(date => {
            let actualDate = `${date.trim()}, ${moment().year()}`;
            actualDate = moment(actualDate, constants.dateFormats.shortDate, true).toDate();
            dates.push(actualDate);
        });

        return {
            endDate: dates[1],
            startDate: dates[0]
        };
    }

    async getColumnFilteringData(dataType, columnName) {
        let apiFieldName;
        let filteringData;

        if (dataType === 'leaves') {
            const dR = actions.erp.notificationsWithOutstandingReqActions.getFormattedDateRange(constants.dateFormats.isoDate);
            await browser.sleep(2000);
            apiFieldName = this.mapLeavesFilteringHeaderToApiRequestField(columnName);
            filteringData = await getRequestDataArray(
                `${constants.endPoints.events}._ge=${dR.startDate}&eventDate._le=${dR.endDate}`,
                apiFieldName,
                'data.elements'
            );
        } else {
            apiFieldName = this.mapFilteringHeaderToApiRequestField(columnName);
            filteringData = await getRequestDataArray(
                constants.endPoints.filteredNotifications,
                apiFieldName,
                'data.elements'
            );
        }

        filteringData = testDataHelpers.getRandomArrayElement(filteringData);
        filteringData = filteringData.toString().substr(
            0,
            faker.random.number({max: filteringData.length, min: 2})
        );

        return filteringData;
    }

    mapFilteringHeaderToApiRequestField(columnName) {
        switch (columnName) {
            case 'Employee First Name':
                return 'customer.firstName';
            case 'Employee Last Name':
                return 'customer.lastName';
            case 'Job title':
                return 'customer.jobTitle';
            case 'Group':
                return 'customer.workSite';
            case 'Case':
                return 'caseNumber';
            default:
                return `Invalid column name ${columnName}`;
        }
    }

    mapLeavesFilteringHeaderToApiRequestField(columnName) {
        switch (columnName) {
            case 'Employee First Name':
                return 'employee.firstName';
            case 'Employee Last Name':
                return 'employee.lastName';
            case 'Group':
                return 'adminGroup';
            case 'Case':
                return 'notificationCase.caseReference';
            default:
                return `Invalid column name ${columnName}`;
        }
    }

    /**
     *
     * @return {string}
     */
    async findNotificationWithHyperlink() {
        await waitHelpers.waitForSpinnersToDisappear();
        await browser.sleep(60000); // Bug FEP-2255
        const expectedState = translationsHelper.getTranslation('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS').toLowerCase();
        let randomNotification = getRandomArrayItem(await pages.erp.notificationsWithOutstandingReqPage.notificationRows());
        let randomNotificationId = await randomNotification.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);
        let actualState = (await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(
            randomNotificationId).getText()).toString().toLowerCase();

        if (actualState === expectedState) {
            let iterations = 0;
            await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(randomNotificationId).Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();

            let hyperLinks = await getRequestDataArray(
                `${randomNotificationId}/outstanding-information`,
                'infoReceived'
            );

            while (!hyperLinks.includes(false) && iterations < 10) {
                iterations++;
                randomNotification = getRandomArrayItem(await pages.erp.notificationsWithOutstandingReqPage.notificationRows());
                randomNotificationId = await randomNotification.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);
                actualState = (await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(
                    randomNotificationId).getText()).toString().toLowerCase();

                if (actualState === expectedState) {
                    await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(randomNotificationId).Click(
                        {waitForVisibility: true});
                    await waitHelpers.waitForSpinnersToDisappear();
                    hyperLinks = await getRequestDataArray(
                        `${randomNotificationId}/outstanding-information`,
                        'infoReceived'
                    );
                }
            }
        }
        testVars.randomNotificationId = randomNotificationId;

        return randomNotificationId;
    }

    /**
     * @return {string}
     */
    getFormattedDateRange(dateFormat) {
        const dR = actions.erp.notificationsWithOutstandingReqActions.getFilteringDateRange();
        const dRStartDate = moment(
            dR.startDate,
            constants.dateFormats.shortDate,
            true
        ).format(dateFormat);
        const dREndDate = moment(
            dR.endDate,
            constants.dateFormats.shortDate,
            true
        ).format(dateFormat);

        return {
            endDate: dREndDate,
            startDate: dRStartDate
        };
    }

    /**
     * @return {string}
     */
    async getNumberOfAllApiLeavesDecided() {
        const dR = this.getFormattedDateRange(constants.dateFormats.isoDate);
        await browser.sleep(2000);
        let numberOfApiSortedItems = await getAllRequestsDataIntoArray(
            `${constants.endPoints.events}._ge=${dR.startDate}&eventDate._le=${dR.endDate}`,
            'data.totalSize',
        );
        numberOfApiSortedItems = numberOfApiSortedItems.reduce((total, amount) => total + amount);

        return numberOfApiSortedItems;
    }

    /**
     * @param {string} widgetType
     */
    async setTotalNumberOfWidget(widgetType) {
        if (widgetType.toString().toLowerCase().includes('employees') && !widgetType.toString().toLowerCase().includes('approved')) {
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.viewEmployeesExpectedToReturnToWorkPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.employeesExpectedToReturnToWorkWidgetDayFilter().getText();
        } else if (widgetType.toString().toLowerCase().includes('notifications')) {
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.notificationsCreatedWidgetPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.notificationsCreatedWidgetDayFilter().getText();
        } else if (widgetType.toString().toLowerCase().includes('leaves')) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidgetPieChartTotalResults());
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.leavesDecidedWidgetPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.leavesDecidedWidgetDayFilter().getText();
        } else if (widgetType.toString().toLowerCase().includes('approved')) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetPieChartTotalResults());
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetDayFilter().getText();
        }
    }
}

module.exports = NotificationsWithOutstandingReqActions;
