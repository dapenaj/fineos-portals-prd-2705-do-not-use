const moment = require('moment');

class RequestForMoreTimeActions {
    /**
     * Find expectedPeriodStartDate in calendar (date picker)
     * Get existing period start date from ui.
     * @param {string} expectedPeriodStartDate - string date to be found in calendar
     * @returns {string} existing period start date
     */
    async findPeriodStartDate(expectedPeriodStartDate) {
        await datePickerHelper.navigateDate(moment(expectedPeriodStartDate, constants.dateFormats.isoDate, true));
        const actualPeriodStartDate = await waitHelpers.waitForElementToBeVisible(pages.erp.requestForMoreTimeModal.existingAbsencePeriodStartDate());
        return actualPeriodStartDate.getAttribute('title');
    }

    /**
     * Find expectedPeriodStartDate in calendar (date picker)
     * Get existing period end date from ui.
     * @param {string} expectedPeriodStartDate - string date to be found in calendar
     * @returns {string} existing period start date
     */
    async findPeriodEndDate(expectedPeriodEndDate) {
        await datePickerHelper.navigateDate(moment(expectedPeriodEndDate, constants.dateFormats.isoDate, true));
        const actualPeriodEndDate = await waitHelpers.waitForElementToBeVisible(pages.erp.requestForMoreTimeModal.existingAbsencePeriodEndDate());
        return actualPeriodEndDate.getAttribute('title');
    }

    /**
     * This method maps gradient with given color and gradient type
     * @param {string} gradientType - type of gradient
     * @param {string} strongColor - strong color used in gradient
     * @param {string} lightColor - strong color used in gradient
     *
     * @returns {string} existing period start date
     */
    mapGradientWithTypeAndColor(gradientType, strongColor, lightColor) {
        const gradient = gradientType.toUpperCase().replace(constants.regExps.spaces, '_');

        return constants.gradients[gradient].replace(/{strongColor}/ig, strongColor).replace(
            /{lightColor}/ig,
            lightColor
        );
    }

    /**
     * This method verifies if time bar days have correct color and gradient
     *
     * @param {string} expectedColor - expected time bar color (red, green, yellow)
     * @param {string} expectedGradientType - type of gradient, solid line, horizontal split line, striped line
     * @param {string} timeBarStartDate - time bar start date
     * @param {string} timeBarEndDate - time bar end date
     */
    async verifyTimeBarGradientAndColor(expectedColor, expectedGradientType, timeBarStartDate, timeBarEndDate) {
        await waitHelpers.waitForElementToBeStatic(pages.erp.datePicker.daysList().first());
        await pages.erp.datePicker.daysList().each(async el => {
            const actualGradient = await pages.erp.datePicker.dayCell(el).getCssValue('background');
            const actualDate = await el.getAttribute('title');

            if (actualDate >= timeBarStartDate && actualDate <= timeBarEndDate) {
                const lightColor = this.calculateGradientLightColor(constants.gradientColors[expectedColor.toString().toUpperCase()]);
                const strongColor = constants.gradientColors[expectedColor.toString().toUpperCase()];
                const expectedGradient = this.mapGradientWithTypeAndColor(
                    expectedGradientType,
                    strongColor,
                    lightColor
                );
                expect(actualGradient).is.equal(expectedGradient);
            } else {
                expect(actualGradient).is.equal(constants.gradients.NO_GRADIENT);
            }
        });
    }

    /**
     * This method verifies if time bar days are disabled
     *
     * @param {string} timeBarStartDate - time bar start date
     * @param {string} timeBarEndDate - time bar end date
     */
    async verifyTimeBarElementsDisability(timeBarStartDate, timeBarEndDate) {
        let timeBarFound = false;
        await waitHelpers.waitForElementToBeStatic(pages.erp.datePicker.daysList().first());
        await pages.erp.datePicker.daysList().each(async el => {
            const actualDate = await el.getAttribute('title');

            if (actualDate >= timeBarStartDate && actualDate <= timeBarEndDate) {
                const isDisabled = await el.getAttribute('class');
                expect(isDisabled.toString().toLowerCase()).includes('disabled');
                timeBarFound = true;
            }
        });

        if (!timeBarFound) {
            throw new Error(`Time bar does not exists. Period start data is: [${timeBarStartDate}] and period and date is: [${timeBarEndDate}]`);
        }
    }

    /**
     * This method can be used to calculate time bar gradient light color. Base color will be mixed with white color
     * using given weight value.
     *
     * @param {string} baseColor - e.g. rgb(255,240,14)
     * @param {number} weight - number between 0 and 1.
     *
     * @return {string} gradient light color (mixed base color with white)
     */
    calculateGradientLightColor(baseColor, weight = 0.75) {
        const WHITE = [255, 255, 255];
        const rgbColor = testDataHelpers.convertRgbToArray(baseColor);

        return `rgb(${testDataHelpers.mixRgbColors(weight, WHITE, rgbColor).join(', ')})`;
    }
}

module.exports = RequestForMoreTimeActions;
