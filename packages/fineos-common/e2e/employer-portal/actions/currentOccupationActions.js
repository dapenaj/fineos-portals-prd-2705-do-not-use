const faker = require('faker');

class CurrentOccupationActions {

    /**
     * Function generates faked test data depending on given data type
     *
     * @param {string} dataType - firstName, lastName, email, etc.
     * @param {[string]} testDataArray = array with available stated abbreviations, etc. ['Full time',
     *     'Salaried']
     * @returns {string} generated test data
     */
    generateCurrentOccupationData(dataType, testDataArray = []) {
        switch (dataType) {
            case 'jobtitle':
                return faker.company.bs();
            case 'adjusteddateofhire':
            case 'jobstartdate':
                return `${faker.date.month().slice(0, 3)} ${faker.random.number({
                    max: 28,
                    min: 10
                })}, ${faker.random.number({max: 2019, min: 2010})}`;
            case 'jobenddate':
                return `${faker.date.month().slice(0, 3)} ${faker.random.number({
                    max: 28,
                    min: 10
                })}, ${faker.random.number({max: 2045, min: 2030})}`;
            case 'employmenttype':
            case 'full/parttime':
            case 'earningsfrequency':
                return faker.random.arrayElement(testDataArray);
            case 'earnings':
                return faker.random.arrayElement(['1', '10', '10000', '19809809809809', '100.5', '99.99']);
            case 'hoursworkedperweek':
                return faker.random.arrayElement(['6', '90', '1.5', '99.9']);
            default:
                return `Test data was not generated for unsupported data type ${dataType}`;
        }
    }
}

module.exports = CurrentOccupationActions;
