const users = require('../constants/users');
const {installSniffer} = require('../../common/helpers/snifferHelper');

class LoginActions {

    /**
     * Navigates to homepage - urls defined per env type
     * @param {string} [userDescriptor = constants.userTypes.ADMIN_USER] user to log in, by default admin will log in
     */
    async fillInCredentials(userDescriptor = constants.userTypes.ADMIN_USER) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.loginPage.panelAppSelector(),
            timeouts.T60,
            'I should see app selector panel'
        ).then(null, async err => {
            // eslint-disable-next-line no-console
            console.log(`problem with Fineos Cloud IDP in: ${err}. Retry login!`);
            await browser.get(browser.baseUrl);

            await waitHelpers.waitForElementToBeVisible(
                pages.erp.loginPage.panelAppSelector(),
                timeouts.T60,
                'I should see app selector panel'
            );
        });

        await pages.erp.loginPage.radioAppSelect().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeClickable(pages.erp.loginPage.dropDownAppSelect());
        await pages.erp.loginPage.dropDownAppSelect().sendKeys(browser.params.env.appName);
        await pages.erp.loginPage.buttonSignIn().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(pages.erp.loginPage.panelLoginForm(), timeouts.T60)
            .then(async () => {
                const user = users.getUser(userDescriptor);
                await pages.erp.loginPage.inputUserName().sendKeys(user.username);
                await pages.erp.loginPage.inputPassword().sendKeys(user.password);
                await pages.erp.loginPage.buttonSubmit().Click({waitForVisibility: true});
            }, () => {
                // ignoring promise rejection - in case user is already logged
            });
    }

    async createNewWindowAndSwitchTo(userDescriptor = constants.userTypes.HR_TEST_USER) {
        await browser.executeScript('window.open()').then(function () {
            browser.getAllWindowHandles().then(async function (handles) {
                await browser.switchTo().window(handles[1]).then(function () {
                    return browser.get(browser.baseUrl);
                });
            });
        });
    }

    async switchToWindow(windowIndex) {
        await browser.getAllWindowHandles().then(async function (handles) {
            await browser.switchTo().window(handles[windowIndex]);
        });
    }

    /**
     * Navigates to homepage - urls defined per env type
     * @param {string} [userDescriptor = constants.userTypes.ADMIN_USER] user to log in, by default admin will log in
     * @returns {Promise<void>}
     */
    async logIn(userDescriptor = constants.userTypes.ADMIN_USER) {
        await browser.get(browser.baseUrl);

        if (browser.params.env.name !== 'localhost') {
            await this.fillInCredentials(userDescriptor);

            await waitHelpers.waitForElementToBeVisible(
                pages.erp.applicationRoot(),
                timeouts.T60,
                'I should see application root in DOM'
            ).then(null, async err => {
                // eslint-disable-next-line no-console
                console.log(`problem with sign in: ${err}. Retry login!`);
                await browser.get(browser.baseUrl);
                await this.signOut();
                await this.fillInCredentials(userDescriptor);
            });
        }

        await waitHelpers.waitForElementToBeVisible(
            pages.erp.applicationRoot(),
            timeouts.T60,
            'I should see application root in DOM'
        );

        if (browser.params.env.type === 'local') {
            const currentSignatureCookie = await browser.manage().getCookie('portal-cookie');

            await browser.get(browser.params.env.localUrl.toString());

            await browser.manage().addCookie({
                domain: browser.params.env.localUrl.host,
                name: currentSignatureCookie.name,
                path: browser.params.env.localUrl.pathname,
                value: currentSignatureCookie.value
            });

            // to ensure that our script gonna use our cookie
            await browser.refresh();
        }

        await installSniffer();

        await waitHelpers.waitForElementToBeVisible(
            pages.erp.myDashboardPage.panelMainLayout(),
            timeouts.T60,
            'I should see application root in DOM'
        ).then(null, async err => {
            // eslint-disable-next-line no-console
            console.log(`problem with log in: ${err}. Retry login!`);
            await browser.get(browser.baseUrl);
            await this.signOut();
            await this.fillInCredentials(userDescriptor);
        });

        await waitHelpers.waitForElementToBeVisible(
            pages.erp.myDashboardPage.panelMainLayout(),
            timeouts.T60,
            'I should see home screen'
        );

        await waitHelpers.waitForSpinnersToDisappear();
    }

    /**
     * Log out from application, clear cache to remove stored user
     * @returns {Promise<void>}
     */
    async logOut() {
        await pages.erp.searchPage.logOutButton().Click({waitForVisibility: true});
    }

    /**
     * Sign out from currently signed in site on Fineoscloud IDP
     * @returns {Promise<void>}
     */
    async signOut() {
        const signInStatus = await waitHelpers.waitForElementToBeVisible(pages.erp.loginPage.signInStatusLabel());

        if (await signInStatus.getText() === 'You are signed in.') {
            await pages.erp.loginPage.signOutCurrentSiteRadioButton().Click({waitForVisibility: true});
            await pages.erp.loginPage.buttonSignOut().Click({waitForVisibility: true});
        }
    }
}

module.exports = LoginActions;
