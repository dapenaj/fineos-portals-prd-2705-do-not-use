const {getRandomArrayItem} = require('../../common/helpers/mathHelper');
const faker = require('faker');

class IntakeActions {
    /**
     * This method selects reason for cancel withdraw request
     * @returns {Promise<void>}
     */
    async selectRandomStateOrProvince(provinceOrState) {
        await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeEmployeeDetailsPage.stateProvinceDropDownArrow());

        if (provinceOrState === 'State') {
            await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.stateDropDown());
            await waitHelpers.waitForElementToBePresent(await pages.erp.intakeEmployeeDetailsPage.stateDropDown());
            await pages.erp.intakeEmployeeDetailsPage.stateDropDown().Click({waitForVisibility: true});
        } else {
            await pages.erp.intakeEmployeeDetailsPage.provinceDropDown().Click({waitForVisibility: true});
        }

        const selectedStateOrProvince = await pages.erp.intakeEmployeeDetailsPage.statesList().last();
        await selectedStateOrProvince.Scroll();
        await selectedStateOrProvince.Click({waitForVisibility: true});
    }

    /**
     * This method selects random phone number
     * @returns {Promise<void>}
     */
    async selectRandomPhoneNumber() {
        await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.phoneNumbers().first());
        const randomItem = getRandomArrayItem(await pages.erp.intakeEmployeeDetailsPage.phoneNumbers());
        await randomItem.Click();
    }

    /**
     * This method closes acknowledgment screen
     * @returns {Promise<void>}
     */
    async closeAcknowledgmentScreen() {
        await pages.erp.intakeAcknowledgmentScreen.closeBtn().Click({waitForVisibility: true});
    }

    /**
     * This method returns random date
     * @returns {string}
     */
    generateLastDay() {
        return `${faker.date.month().slice(0, 3)} ${faker.random.number({
            max: 28,
            min: 10
        })}, ${faker.random.number({max: 2040, min: 2035})}`;
    }

    /**
     * This method returns random date
     * @returns {string}
     */
    generateDateOfHire() {
        return `${faker.date.month().slice(0, 3)} ${faker.random.number({
            max: 28,
            min: 10
        })}, ${faker.random.number({max: 2018, min: 2018})}`;
    }

    /**
     * This method fills the fields based on provided fieldsList and postalCode number
     * @param {string} fieldsList
     * @param {string} postalCode
     */
    async fillMandatoryAddressFieldsOnEditPage(fieldsList, postalCode) {
        await waitHelpers.waitForSpinnersToDisappear();
        const addressFields = fieldsList.split(',');

        if (testVars.searchTarget.selectedCountry === 'United States' || testVars.searchTarget.selectedCountry === 'Canada') {
            for (let i = 0; i < addressFields.length; i++) {
                if (addressFields[i].includes('Zip code') || addressFields[i].includes('Postal code')) {
                    await pages.erp.intakeEditEmployeeDetailsPage.postalCodeInput().SetText(postalCode);
                } else if (addressFields[i].includes('State') || addressFields[i].includes('Province')) {
                    await actions.erp.intakeActions.selectRandomStateOrProvince(addressFields[i]);
                } else {
                    await pages.erp.intakeEmployeeDetailsPage.getInputByLabel(addressFields[i]).SetText(faker.address.streetName(1));
                }
                await this.saveFieldsData(addressFields, i);
            }
        } else {
            for (let i = 0; i < addressFields.length; i++) {
                if (addressFields[i].includes('Postal code')) {
                    await pages.erp.intakeEditEmployeeDetailsPage.postalCodeInput().SetText(postalCode);
                } else if (addressFields[i].includes('State')) {
                    await actions.erp.intakeActions.selectRandomStateOrProvince(addressFields[i].getText());
                } else {
                    await pages.erp.intakeEmployeeDetailsPage.getInputByLabel(addressFields[i]).SetText(faker.address.streetName(1));
                }
                await this.saveFieldsData(addressFields, i);
            }
        }
    }

    /**
     * This method checks if the addressFields contain the State ot Province field and saved this field value
     * also this method saved all fields values entered by the automation test
     * @param {string} addressFields
     * @param {int} iterator
     */
    async saveFieldsData(addressFields, iterator) {
        if (addressFields[iterator].includes('State') || addressFields[iterator].includes('Province')) {
            constants.addressLines[iterator] = await pages.erp.intakeEditEmployeeDetailsPage.selectedStateOrProvince().getAttribute(
                'title');
        } else {
            constants.addressLines[iterator] = await pages.erp.intakeEditEmployeeDetailsPage.getInputByLabel(
                addressFields[iterator]).getAttribute('value');
        }
    }

    /**
     * This method fills the new phone number data
     */
    async fillNewPhoneNumber() {
        await pages.erp.intakeEditEmployeeDetailsPage.useDifferentPhoneNumber().Click({waitForVisibility: true});
        await pages.erp.intakeEmployeeDetailsPage.getInputByLabel('Country code').SetText((Math.floor(Math.random() * 90 + 10)).toString());
        await pages.erp.intakeEmployeeDetailsPage.getInputByLabel('Area code').SetText((Math.floor(Math.random() * 90 + 10)).toString());
        await pages.erp.intakeEmployeeDetailsPage.getInputByLabel('Phone number').SetText((Math.floor(Math.random() * 1000000)).toString());
    }

    /**
     * This method selects country from the dropdown
     * @param {string} countryName
     */
    async selectCountryFromDropDown(countryName) {
        await this.displayCountriesDropdown();
        const element = await pages.erp.intakeEditEmployeeDetailsPage.country(countryName).Scroll();
        await element.Click({waitForVisibility: true});
    }

    /**
     * This method selects last country from the dropdown
     */
    async selectLastCountryFromDropDown() {
        await this.displayCountriesDropdown();
        const element = await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEditEmployeeDetailsPage.countriesList().last());
        await element.Click({waitForVisibility: true});
    }

    /**
     * This method displays countries dropdown
     */
    async displayCountriesDropdown() {
        await waitHelpers.waitForElementToBeClickable(await pages.erp.intakeEditEmployeeDetailsPage.countryDropDown());
        await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeEditEmployeeDetailsPage.countryDropDownArrow());
        await waitHelpers.waitForElementToBeClickable(await pages.erp.intakeEditEmployeeDetailsPage.countryDropDownArrow());
        await pages.erp.intakeEditEmployeeDetailsPage.countryDropDown().Click({waitForVisibility: true});
    }
}

module.exports = IntakeActions;
