Then(/^the 'Edit personal details' popup opens$/, async () => {
    const title = await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeePersonalDetailsPage.editPersonalDetailsTitle(),
        timeouts.T30,
        'Expect to see form title'
    );
    await expectHelpers.checkTranslation(title, 'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS');

    const primaryAddress = await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeePersonalDetailsPage.primaryAddressLabel(),
        timeouts.T30,
        'Expect to see form Primary Address Label'
    );
    await expectHelpers.checkTranslation(primaryAddress, 'PROFILE.PERSONAL_DETAILS_TAB.PRIMARY_ADDRESS');

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.employeePersonalDetailsPage.okButton(),
        pages.erp.employeePersonalDetailsPage.cancelButton(),
        pages.erp.employeePersonalDetailsPage.closeButton()
    ]);
});

Then(/^the 'Edit personal details' popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(
        pages.erp.employeePersonalDetailsPage.editPersonalDetailsTitle(),
        timeouts.T30,
        'Expect to not see form title'
    );
});

Then(
    /^the 'Edit personal details' popup contains following "(editable|not editable)" fields "([^"]*)"$/,
    async (isEditable, expectedFields) => {
        const fields = expectedFields.split(',').map(item => item.trim());
        if (isEditable === 'editable') {
            for (const fieldLabel of fields) {
                const input = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(
                    fieldLabel));

                if (await input.getAttribute('role') === 'combobox') {
                    await waitHelpers.waitForElementToBeVisible(await pages.erp.employeePersonalDetailsPage.selectArrow(
                        'country'));
                    await waitHelpers.waitForElementToBeClickable(await pages.erp.employeePersonalDetailsPage.selectArrow(
                        'country'));
                }
                await waitHelpers.waitForElementToBeVisible(await pages.erp.employeePersonalDetailsPage.selectArrow(
                    'country'));
                expect(await input.isEnabled()).is.equal(true);
            }
        } else {
            const addressFields = await pages.erp.employeePersonalDetailsPage.addressFields();
            expect(fields.length).is.equal(addressFields.length);
        }
    }
);

When(/^I type "([^"]*)" value into "([^"]*)" field$/, async (fieldValue, fieldLabel) => {
    const dataType = fieldLabel.trim().replace(/\s+/g, '').toLowerCase();
    const formField = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(fieldLabel);
    testVars.employeePersonalDetails.historicalData[dataType] = await formField.getAttribute('value');

    await formField.Clear();
    await formField.SendText(fieldValue);
    expect(await formField.getAttribute('value')).is.equal(fieldValue);

    if (await formField.getAttribute('data-test-el') === 'date-picker') {
        await formField.SendText(protractor.Key.ENTER);
    }
});

When(/^I clear "([^"]*)" field$/, async fieldLabel => {
    const formField = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(fieldLabel);

    if (fieldLabel.toString().toLowerCase().includes('date')) {
        await pages.erp.employeePersonalDetailsPage.clearDateOfBirthButton().HooverOverElement();
        await pages.erp.employeePersonalDetailsPage.clearDateOfBirthButton().Click({waitForVisibility: true});
    } else {
        await formField.Clear();
    }

    expect(await formField.getAttribute('value')).is.empty;
});

Then(
    /^I see "([^"]*)" error message for the invalid field values of "Postal code" on the Edit personal details popup$/,
    async expectedErrorMessage => {
        const errorMessage = await (await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.canadianPostalCodeErrorMessage())).getText();
        expect(errorMessage.toString()).is.equal(expectedErrorMessage);
    }
);

Then(
    /^the "([^"]*)" error is "(visible|not visible)" under the "([^"]*)" field$/,
    async (expectedErrorMessage, visibility, inputName) => {
        if (visibility === constants.stepWording.VISIBLE) {
            const errorMessage = await (await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.inputRequiredErrorMessage(
                inputName))).getText();
            expect(errorMessage.toString()).is.equal(expectedErrorMessage);
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.employeePersonalDetailsPage.inputRequiredErrorMessage(
                inputName));
        }
    }
);

When(/^I see "([^"]*)" field was not changed$/, async fieldLabel => {
    const dataType = fieldLabel.trim().replace(/\s+/g, '').toLowerCase();
    const formField = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(fieldLabel);

    expect(await formField.getAttribute('value')).is.equal(testVars.employeePersonalDetails.historicalData[dataType].toString());
});

Then(/^I enter (the same random|random) test data in the "([^"]*)" field$/, async (mode, fieldLabel) => {
    const dataType = fieldLabel.trim().replace(/\s+/g, '').toLowerCase();

    if (mode === 'random') {
        testVars.employeePersonalDetails[dataType] = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
            dataType);
    }

    const formField = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(fieldLabel);
    await formField.Clear();

    if (await formField.getAttribute('data-test-el') === 'date-picker') {
        await formField.SendText(testVars.employeePersonalDetails[dataType] + protractor.Key.ENTER);
    } else {
        await formField.SendText(testVars.employeePersonalDetails[dataType]);
    }

    expect(await formField.getAttribute('value')).is.equal(testVars.employeePersonalDetails[dataType].toString());
});

Then(/^I enter "([^"]*)" test data in the "([^"]*)" field$/, async (txt, fieldLabel) => {
    const formField = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(fieldLabel);
    await formField.Clear();

    if (await formField.getAttribute('data-test-el') === 'date-picker') {
        await formField.SendText(txt + protractor.Key.ENTER);
    } else {
        await formField.SendText(txt);
    }

    expect(await formField.getAttribute('value')).is.equal(txt);
});

Then(/^"Personal Details" "([^"]*)" fields were refreshed correctly$/, async fieldLabels => {
    await actions.erp.employeeProfileActions.checkFieldsContent(fieldLabels, 'employeePersonalDetails');
});

Then(/^I see "Personal details" "([^"]*)" fields were refreshed with data set in intake flow$/, async fieldLabels => {
    fieldLabels = fieldLabels.split(',').map(item => item.trim());

    for (const label of fieldLabels) {
        const dataType = label.trim().replace(constants.regExps.spaces, '').toLowerCase();
        const expectedAddress = testVars.intake[dataType];
        const personalDetailsField = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.editPersonalDetailsInput(
            label));
        const actualAddress = await personalDetailsField.getAttribute('value');

        expect(expectedAddress).is.equal(actualAddress);
    }
});

When(/^I click "(Country|Province|State)" field$/, async selectName => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectArrow(selectName));
    await pages.erp.employeePersonalDetailsPage.select(selectName).Click({waitForVisibility: true});
});

When(
    /^I see a dropdown list of "(\d+)" "(Province|State)" codes with a 2 characters abbreviation$/,
    async (numberOfOptions, selectName) => {
        const allAvailableOptions = await actions.erp.employeeProfileActions.getAllSelectOptions();

        if (selectName === 'State') {
            expect(constants.selectOptions.usStates).is.equal(allAvailableOptions.toString());
        } else {
            expect(constants.selectOptions.canadaProvinces).is.equal(allAvailableOptions.toString());
        }

        expect(allAvailableOptions.length).is.equal(Number.parseInt(numberOfOptions, 10));
    }
);

Then(/^I select "(.*)" test data in the "(Country|Province|State)" field$/, async (optionName, selectName) => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectArrow(selectName));
    await pages.erp.employeePersonalDetailsPage.select(selectName).Click({waitForVisibility: true});
    await pages.erp.employeePersonalDetailsPage.selectOption(optionName).Scroll();
    await pages.erp.employeePersonalDetailsPage.selectOption(optionName).Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
});

Then(
    /^I select (the same random|random) test data in the "(Country|Province|State)" field$/,
    async (mode, selectName) => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectArrow(selectName));
        await pages.erp.employeePersonalDetailsPage.select(selectName).Click({waitForVisibility: true});

        let availableStates = await pages.erp.employeePersonalDetailsPage.selectOptions().getText();
        availableStates = availableStates.toString().split(',').filter(Boolean);

        if (mode === 'random') {
            testVars.employeePersonalDetails[selectName.toString().toLowerCase()] = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
                selectName.toString().toLowerCase(),
                availableStates.toString().split(',')
            );
        }

        await pages.erp.employeePersonalDetailsPage.selectOption(testVars.employeePersonalDetails[selectName.toString().toLowerCase()]).Click(
            {waitForVisibility: true});
        expect(await pages.erp.employeePersonalDetailsPage.select(selectName).getText()).is.equal(
            testVars.employeePersonalDetails[selectName.toString().toLowerCase()]);
    }
);

Then(
    /^I select "([^"]*)" option in the "(Country|Province|State)" field$/,
    async (option, selectName) => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeePersonalDetailsPage.selectArrow(selectName));
        await pages.erp.employeePersonalDetailsPage.select(selectName).Click({waitForVisibility: true});
        await pages.erp.employeePersonalDetailsPage.selectOption(option).Click({waitForVisibility: true});

        expect(await pages.erp.employeePersonalDetailsPage.select(selectName).getText()).is.equal(
            testVars.employeePersonalDetails[selectName.toString().toLowerCase()]);
    }
);

When(/^I "(accept|cancel|close X)" 'Edit Personal Details' popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.employeePersonalDetailsPage.okButton().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.employeePersonalDetailsPage.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.employeePersonalDetailsPage.closeButton().Click({waitForVisibility: true});
    }
});

Then(/^I verify that fields on Edit Personal Details page are not updated$/, async () => {
    const actualData = await pages.erp.employeeProfilePage.personalDetailsData().getText();
    expect(actualData.toString()).is.equal(testVars.employeePersonalDetails.historicalData.toString());
});
