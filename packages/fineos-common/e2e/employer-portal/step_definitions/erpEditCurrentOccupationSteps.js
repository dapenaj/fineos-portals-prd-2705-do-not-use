const {Then} = require('cucumber');

Then(/^the 'Edit Current occupation' popup opens$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.unauthorizedAlert());
    const title = await waitHelpers.waitForElementToBeVisible(
        pages.erp.editCurrentOccupationPage.editCurrentOccupationTitle(),
        timeouts.T30,
        'Expect to see form title'
    );
    await expectHelpers.checkTranslation(title, 'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.OCCUPATION_TITLE_EDIT');

    await waitHelpers.waitForElementToBeClickable(
        await pages.erp.editCurrentOccupationPage.employmentTypeSelectArrow()
    );
    await waitHelpers.waitForElementToBeStatic(
        await pages.erp.editCurrentOccupationPage.employmentTypeSelectArrow()
    );

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.editCurrentOccupationPage.okButton(),
        pages.erp.editCurrentOccupationPage.cancelButton(),
        pages.erp.editCurrentOccupationPage.closeButton()
    ]);
});

Then(/^the 'Edit Current occupation' popup closes$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeNotVisible(
        pages.erp.editCurrentOccupationPage.editCurrentOccupationTitle(),
        timeouts.T30,
        'Expect to not see form title'
    );
});

Then(/^the 'Edit Current occupation' popup contains the editable fields "([^"]*)"$/, async fieldsLabels => {
    const fieldsLabelsArray = fieldsLabels.split(',').map(field => field.trim().replace(
        constants.regExps.spaces,
        '_'
    ).toUpperCase());

    for (const label of fieldsLabelsArray) {
        expect(await pages.erp.editCurrentOccupationPage.formInput(label).isEnabled()).equal(true);
    }
});

Then(
    /^the 'Edit Current occupation' popup labels "(have|do not have)" '\*' on the fields "([^"]*)"$/,
    async (isRequired, fieldsLabels) => {
        const fieldsLabelsArray = fieldsLabels.split(',').map(field => field.trim().replace(
            constants.regExps.spaces,
            '_'
        ).toUpperCase());

        if (isRequired === constants.stepWording.HAVE) {
            for (const label of fieldsLabelsArray) {
                expect(await pages.erp.editCurrentOccupationPage.formFieldLabel(label).getText()).contains('*');
            }
        } else {
            for (const label of fieldsLabelsArray) {
                expect(await pages.erp.editCurrentOccupationPage.formFieldLabel(label).getText()).not.contains('*');
            }
        }
    }
);

Then(/^I "(check|uncheck)" "Adjusted date of hire" switch$/, async switchAction => {
    const adjustedDateOfHireSwitch = await waitHelpers.waitForElementToBeVisible(pages.erp.editCurrentOccupationPage.adjustedDateOfHireSwitch());
    const actualSwitchState = await adjustedDateOfHireSwitch.getAttribute('aria-checked');

    if (constants.stepWording.CHECK === switchAction && (actualSwitchState === 'false' || actualSwitchState === null)) {
        await adjustedDateOfHireSwitch.Click({waitForVisibility: true});
    } else if (constants.stepWording.CHECK !== switchAction && actualSwitchState === 'true') {
        await adjustedDateOfHireSwitch.Click({waitForVisibility: true});
    }
});

Then(/^I see 'Current occupation' data in edit mode is the same like in display mode$/, async () => {
    const displayModeData = await pages.erp.employeeProfilePage.currentOccupationData().getText();
    displayModeData.push(await pages.erp.employeeProfilePage.paymentFrequency().getText());
    testVars.employmentDetails.historicalData = displayModeData;
    const editEmploymentFormFields = await pages.erp.editCurrentOccupationPage.editCurrentOccupationData();
    const editModeData = [];

    for (const editField of editEmploymentFormFields) {
        let dataValue = await editField.getAttribute('value');
        const fieldName = await editField.getAttribute('name');

        if (dataValue !== '') {
            if (dataValue === null) {
                dataValue = await editField.getText();
            }

            if (fieldName !== null && fieldName.includes('earnings')) {
                dataValue = testDataHelpers.formatCurrency('en-US', 'USD', dataValue);
            }

            editModeData.push(dataValue);
        }
    }

    expect(editModeData.length).is.within(3, 9);
    editModeData.forEach(item => {
        if (item.trim().endsWith('.0')) {
            item = item.replace('.0', '');
        }
        if (item.toString().toLowerCase() === 'please select') {
            item = item.replace(/please select/ig, '-');
        }
        expect(displayModeData.toString()).contains(item.trim());
    });
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.unauthorizedAlert());
});

Then(
    /^I enter (random|the same random) test data in the 'Current occupation' popup "([^"]*)" field$/,
    async (mode, fieldLabel) => {
        const dataType = fieldLabel.trim().replace(constants.regExps.spaces, '').toLowerCase();

        if (mode === 'random') {
            testVars.employmentDetails[dataType] = await actions.erp.currentOccupationActions.generateCurrentOccupationData(
                dataType);
        }
        const formField = await pages.erp.editCurrentOccupationPage.formInput(fieldLabel.trim().replace(
            constants.regExps.spaces,
            '_'
        ).toUpperCase());
        await formField.Clear();

        if ((await formField.getAttribute('data-test-el')).toString().includes('date-picker')) {
            await formField.SendText(testVars.employmentDetails[dataType] + protractor.Key.ENTER);
        } else {
            await formField.SendText(testVars.employmentDetails[dataType]);
        }

        expect(await formField.getAttribute('value')).is.equal(testVars.employmentDetails[dataType].toString());
    }
);

Then(/^I choose (random|the same random) test data in the "([^"]*)" select field$/, async (mode, selectName) => {
    const name = selectName.replace(constants.regExps.spaces, '').toLowerCase();
    await waitHelpers.waitForElementToBeStatic(pages.erp.editCurrentOccupationPage.dropdownInput(name));
    await pages.erp.editCurrentOccupationPage.dropdownInput(name).Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(pages.erp.editCurrentOccupationPage.dropdownListOptions(name).first());
    const availableOptions = await pages.erp.editCurrentOccupationPage.dropdownListOptions(name).getText();

    if (mode === 'random') {
        testVars.employmentDetails[name] = await actions.erp.currentOccupationActions.generateCurrentOccupationData(
            name,
            availableOptions.toString().split(',').splice(1)
        );
    }

    await pages.erp.editCurrentOccupationPage.dropdownListOption(testVars.employmentDetails[name]).Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.editCurrentOccupationPage.dropdownListOption(testVars.employmentDetails[name]));
    expect(await pages.erp.editCurrentOccupationPage.dropdownInput(name).getText()).is.equal(testVars.employmentDetails[name]);
});

Then(/^I choose "([^"]*)" option in the "([^"]*)" select field$/, async (option, selectName) => {
    const name = selectName.replace(constants.regExps.spaces, '').toLowerCase();
    await waitHelpers.waitForElementToBeStatic(pages.erp.editCurrentOccupationPage.dropdownInput(name));
    await pages.erp.editCurrentOccupationPage.dropdownInput(name).Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(pages.erp.editCurrentOccupationPage.dropdownListOptions(name).first());

    await pages.erp.editCurrentOccupationPage.dropdownListOption(option).Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.editCurrentOccupationPage.dropdownListOption(option));
    expect(await pages.erp.editCurrentOccupationPage.dropdownInput(name).getText()).is.equal(option);
});

Then(/^I verify "([^"]*)" select has following "([^"]*)" options$/, async (selectName, selectOptions) => {
    const name = selectName.replace(constants.regExps.spaces, '').toLowerCase();
    await waitHelpers.waitForElementToBeStatic(pages.erp.editCurrentOccupationPage.dropdownInput(name));
    await pages.erp.editCurrentOccupationPage.dropdownInput(name).Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(pages.erp.editCurrentOccupationPage.dropdownListOptions(name).first());
    const actualOptions = (await pages.erp.editCurrentOccupationPage.dropdownListOptions(name).getText()).toString();

    expect(actualOptions).is.equal(selectOptions);
});

When(/^I "(accept|cancel|close X)" 'Edit Current occupation' popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.editCurrentOccupationPage.okButton().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.editCurrentOccupationPage.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.editCurrentOccupationPage.closeButton().Click({waitForVisibility: true});
    }
});

When(/^I clear the "(Job title|Job start date)" field$/, async fieldName => {
    if (fieldName === 'Job title') {
        await pages.erp.editCurrentOccupationPage.jobTitleInput().Clear();
    } else {
        await pages.erp.editCurrentOccupationPage.jobStartDateDatePicker().Clear();
    }
});

Then(/^I see the OK button is enabled$/, async () => {
    const okButton = await waitHelpers.waitForElementToBeVisible(pages.erp.editCurrentOccupationPage.okButton());
    expect(await okButton.getCssValue('border-color')).is.equal(constants.stateColors.ACTIVE);
});

When(/^I enter "([^"]*)" in the "([^"]*)" field$/, async (value, fieldLabel) => {
    const formField = await pages.erp.editCurrentOccupationPage.formInput(fieldLabel.trim().replace(
        constants.regExps.spaces,
        '_'
    ).toUpperCase());
    await formField.Clear();

    if ((await formField.getAttribute('data-test-el')).toString().includes('date-picker')) {
        await formField.SendText(value + protractor.Key.ENTER);
    } else {
        await formField.SendText(value);
    }

    expect(await formField.getAttribute('value')).is.equal(value);
    await pages.erp.editCurrentOccupationPage.editCurrentOccupationTitle().Click({waitForVisibility: true});
});

When(/^I type "([^"]*)" in the "Email" input field$/, async value => {
    const formField = await waitHelpers.waitForElementToBeVisible(pages.erp.addNewEmailModal.emailAddressInput());
    await formField.Clear();
    await formField.SendText(value);

    expect(await formField.getAttribute('value')).is.equal(value);
});

Then(/^I see on the Alerts and updates popup list of emails has not been included "([^"]*)"$/, async newEmail => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.addNewEmailModal.radioButtons().first());
    await pages.erp.addNewEmailModal.radioButtons().each(item => {
        item.getText().then(previousEmail => {
            expect(previousEmail).to.not.equal(newEmail);
        });
    });
});

Then(/^I see an invalid number error message for the "([^"]*)" field$/, async fieldName => {
    await expectHelpers.checkTranslation(
        await pages.erp.editCurrentOccupationPage.hoursPerWeekInvalidNumber(),
        'FINEOS_COMMON.' +
        'VALIDATION.INVALID_NUMBER_VALUE'
    );
});

Then(
    /^I see an invalid non-numeric error message for the "(Contractual earnings|Hours worked per week)" field$/,
    async fieldName => {
        if (fieldName === 'Hours worked per week') {
            await waitHelpers.waitForElementToBeVisible(await pages.erp.editCurrentOccupationPage.hoursPerWeekNonNumeric());
            await expectHelpers.checkTranslation(
                await pages.erp.editCurrentOccupationPage.hoursPerWeekNonNumeric(),
                'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
            );
        } else {
            await waitHelpers.waitForElementToBeVisible(await pages.erp.editCurrentOccupationPage.earningsNotNumericValidation());
            await expectHelpers.checkTranslation(
                await pages.erp.editCurrentOccupationPage.earningsNotNumericValidation(),
                'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
            );
        }
    }
);

Then(
    /^I do not see an invalid data error message for the "(Contractual earnings|Hours worked per week)" field$/,
    async fieldName => {
        if (fieldName === 'Hours worked per week') {
            await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.hoursPerWeekNonNumeric());
            await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.hoursPerWeekInvalidNumber());
        } else {
            await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.earningsNotNumericValidation());
            await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.valueGreaterThanZero());
        }
    }
);

When(/^I see an Value must be greater than zero error message for the "([^"]*)" field$/, async field => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.editCurrentOccupationPage.valueGreaterThanZero());
    await expectHelpers.checkTranslation(
        await pages.erp.editCurrentOccupationPage.valueGreaterThanZero(),
        'FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO'
    );
});

Then(/^"Current Occupation" "([^"]*)" fields were refreshed correctly$/, async fieldLabels => {
    await actions.erp.employeeProfileActions.checkFieldsContent(fieldLabels, 'employmentDetails');
});

Then(/^"Current Occupation" "([^"]*)" select fields were refreshed correctly$/, async selectNames => {
    selectNames = selectNames.split(',').map(item => item.trim());

    for (const selectName of selectNames) {
        const name = selectName.replace(constants.regExps.spaces, '').toLowerCase();
        await waitHelpers.waitForElementToBeStatic(pages.erp.editCurrentOccupationPage.dropdownInput(name));

        const actualFieldContent = await pages.erp.editCurrentOccupationPage.selectedOption(name).getText();
        const expectedFieldContent = testVars.employmentDetails[name];

        expect(actualFieldContent.toString().trim()).is.equal(expectedFieldContent.toString().trim());
    }
});
