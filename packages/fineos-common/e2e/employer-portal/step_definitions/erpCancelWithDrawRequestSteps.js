Then(/^I can see mandatory field Reason for removal$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.reasonForRemovalLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.leaveReasonSelect());
});

Then(/^I can see optional field "Any additional details you want to give us\?"$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.additionalDetailsLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.additionalDetailsInputField());
    await expectHelpers.checkTranslation(
        pages.erp.cancelWithDrawRequestPage.additionalDetailsLabel(),
        'LEAVE_PERIOD_CHANGE.DETAILS_LABEL'
    );
});

Then(/^I verify that the Actions menu is not visible$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.actionsLink());
});

When(/^I select cancel button on "([^"]*)" popup$/, async requestString => {
    await pages.erp.cancelWithDrawRequestPage.cancelRequestToLeaveButton().Click({waitForVisibility: true});
});

Then(/^I should be land on notification page$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.sectionNotificationDetails(),
        timeouts.T15,
        'Expected to see notification details page'
    );
});

Then(/^I shouldn't be able to submit request \(OK button disabled\)$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.disabledOkButton());
});

Then(/^I can see the OK button enabled$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.enabledOkButton());
});

When(/^I select first period "([^"]*)"$/, async fieldName => {
    await pages.erp.cancelWithDrawRequestPage.periodsCheckboxes().first().Click({waitForVisibility: true});
});

Given(/^I select "(single period|multiple periods)" to remove$/, async numberOfPeriods => {
    if (numberOfPeriods === 'single period') {
        await waitHelpers.waitForSpinnersToDisappear();
        await waitHelpers.waitForElementToBeStatic(pages.erp.cancelWithDrawRequestPage.periodsCheckboxes().first());
        await pages.erp.cancelWithDrawRequestPage.periodsCheckboxes().first().Click({waitForVisibility: true});
    } else {
        await waitHelpers.waitForSpinnersToDisappear();
        await pages.erp.cancelWithDrawRequestPage.selectAllPeriodsCheckboxes().Click({waitForVisibility: true});
    }
});

When(/^I complete cancellation request and submit$/, async () => {
    await actions.erp.cancelWithdrawRequestActions.selectRandomCancelReason();
    await pages.erp.cancelWithDrawRequestPage.additionalDetailsInputField().sendKeys('Additional information');
    await pages.erp.cancelWithDrawRequestPage.enabledOkButton().Click({waitForVisibility: true});
});

Then(/^I should see Success screen$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.successModal());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.successModalCloseBtn());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.confirmationImage());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.successModalCloseIcon());
    await expectHelpers.checkTranslation(
        pages.erp.cancelWithDrawRequestPage.successModalMessage(),
        'LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_MESSAGE'
    );
    await expectHelpers.checkTranslation(
        pages.erp.cancelWithDrawRequestPage.successModalHeader(),
        'LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_HEAD'
    );
});

When(/^I close confirmation screen$/, async () => {
    await pages.erp.cancelWithDrawRequestPage.successModalCloseBtn().Click({waitForVisibility: true});
});

Then(/^I verify that the menu option request a leave to be removed is not visible$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.requestLeaveToRemove());
});

Then(
    /^I verify that the menu action "(Request a leave to be removed|Request for more time)" is "(visible|not visible)" in the dropdown$/,
    async (optionName, visibleOrNot) => {
        const mappedOption = actions.erp.notificationActions.mapActionsDropdownOptions(optionName);
        const option = await pages.erp.notificationPage.notificationSummaryActionsDropDown(mappedOption);

        if (visibleOrNot === 'visible') {
            await waitHelpers.waitForElementToBeVisible(option);
        } else {
            await waitHelpers.waitForElementToBeNotVisible(option);
        }
    }
);

Then(/^message displayed after a successful request should be customizable$/, async () => {
    await expectHelpers.checkTranslation(
        pages.erp.cancelWithDrawRequestPage.successModalMessage(),
        'LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_MESSAGE'
    );
    await expectHelpers.checkTranslation(
        pages.erp.cancelWithDrawRequestPage.successModalHeader(),
        'LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_HEAD'
    );
});

Then(
    /^I verify that only periods with status different than "([^"]*)" considered on the list and test data "([^"]*)"$/,
    async (status, testDataDescriptor) => {
        await actions.erp.cancelWithdrawRequestActions.verifyPeriodsList();
    }
);

Then(/^I verify I can see "([^"]*)" item\(s\) on periods list$/, async numberOfPeriods => {
    const numberOfCards = await pages.erp.cancelWithDrawRequestPage.leaveReasonCards().count();
    expect(numberOfCards).to.equal(parseInt(numberOfPeriods, 10));
});

Then(
    /^I verify that each period on the list contains correct start & end date, period type and absence reason for test data "([^"]*)"$/,
    async testDescriptor => {
        await actions.erp.cancelWithdrawRequestActions.verifyPeriodsList();
    }
);

Then(/^I verify that each period on the list contains "([^"]*)" fields$/, async fields => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.cancelWithDrawRequestPage.requestLeaveForm()
        , timeouts.T30
        , 'Request leave form was not found was not found.');
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.reasonForRemovalLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.leaveReasonSelect());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.leaveReasonDateFrom().first());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.leaveReasonDateTo().first());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.leaveReason().first());
    await waitHelpers.waitForElementToBeVisible(pages.erp.cancelWithDrawRequestPage.additionalDetailsLabel());
});
