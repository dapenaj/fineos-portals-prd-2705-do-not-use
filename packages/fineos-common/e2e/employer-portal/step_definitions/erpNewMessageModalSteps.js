Then(/^the "New Message" popup opens$/, async () => {
    const title = await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.newMessageModalTitle());
    await expectHelpers.checkTranslation(title, 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE');
    await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.sendButton());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.subjectLabel());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.subjectInput());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.messageLabel());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.messageInput());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.remainingCharactersInfo());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.closeXButton());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.mandatoryFieldsLabel());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.cancelButton());
});

Then(/^the "New Message" popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.newMessageModal.newMessageModalTitle());
});

Then(/^"Message sent" alert is visible$/, async () => {
    const messageSentAlert = await waitHelpers.waitForElementToBeVisible(await pages.erp.newMessageModal.messageSentAlert());
    await expectHelpers.checkTranslation(messageSentAlert, 'NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_SENT');
});

Then(
    /^the "New Message" popup labels "(have|do not have)" "(\*)" on the fields "([^"]*)"$/,
    async (isRequired, mandatoryCharacter, fieldLabels) => {
        const newMessageFieldsArray = fieldLabels.split(',').map(f => f.trim());
        expect(newMessageFieldsArray).is.not.empty;

        for (const field of newMessageFieldsArray) {
            const newMessageModalContent = await pages.erp.newMessageModal.newMessageModal().getText();

            if (isRequired === constants.stepWording.HAVE) {
                expect(newMessageModalContent).contains(field + mandatoryCharacter);
            } else {
                expect(newMessageModalContent).contains(`${field}\n`);
            }
        }
    }
);

Then(/^the "New Message" popup shows text info about message remaining characters$/, async () => {
    const maxNumberOfCharacters = 4000;
    const actualMessage = await pages.erp.newMessageModal.messageInput().getAttribute('value');
    const actualNumberOfCharacters = await pages.erp.newMessageModal.remainingCharactersInfo().getText();
    const expectedNumberOfCharacters = translationsHelper.mapTranslationVariables(
        'FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS',
        [(maxNumberOfCharacters - actualMessage.length).toString()]
    );

    expect(expectedNumberOfCharacters).is.equal(actualNumberOfCharacters);
});

When(/^I enter "([^"]*)" text in the "(Subject|Message)" "New Message" field$/, async (value, fieldLabel) => {
    if (value.toString().toLowerCase().trim() === 'random') {
        value = actions.erp.newMessagesActions.generateNewMessageData(fieldLabel);
        testVars.newMessage[fieldLabel.toString().toLowerCase()] = value;
    }

    if (fieldLabel === 'Subject') {
        const subject = await pages.erp.newMessageModal.subjectInput();
        await subject.SetText(value);
        expect(await subject.getAttribute('value')).is.equal(value);
    } else {
        const message = await pages.erp.newMessageModal.messageInput();
        await message.SetText(value);
        expect(await message.getText()).is.equal(value);
    }
});

When(
    /^I type "([^"]*)" random characters in the "(Subject|Message)" "New Message" field$/,
    async (numberOfChars, fieldLabel) => {
        const value = new Array(parseInt(numberOfChars, 10) + 1).join('a');
        testVars.newMessage[fieldLabel.toString().toLowerCase()] = value;

        if (fieldLabel === 'Subject') {
            const subject = await pages.erp.newMessageModal.subjectInput();
            await subject.SetText(value);

            expect(await subject.getAttribute('value')).is.equal(value);
        } else {
            const message = await pages.erp.newMessageModal.messageInput();
            await message.SetText(value);

            expect(await message.getText()).is.equal(value);
        }
    }
);

When(
    /^I remove "([^"]*)" character(?:s)? from the "(Subject|Message)" "New Message" field$/,
    async (numberOfChars, fieldLabel) => {
        const keys = protractor.Key.END + protractor.Key.BACK_SPACE + protractor.Key.NULL;
        const field = (fieldLabel === 'Subject') ? await pages.erp.newMessageModal.subjectInput() : await pages.erp.newMessageModal.messageInput();

        for (let i = 0; i < parseInt(numberOfChars, 10); i++) {
            await field.sendKeys(keys);
        }

        testVars.newMessage[fieldLabel.toString().toLowerCase()] = (fieldLabel === 'Subject') ? await field.getAttribute(
            'value') : await field.getText();
    }
);

Then(/^I verify the "(Subject|Message)" field contains the text "([^"]*)"$/, async (fieldLabel, value) => {
    if (fieldLabel === 'Subject') {
        const subject = await pages.erp.newMessageModal.subjectInput();
        expect(await subject.getAttribute('value')).is.equal(value);
    } else {
        const message = await pages.erp.newMessageModal.messageInput();
        expect(await message.getText()).is.equal(value);
    }
});

Then(/^I verify under the "Message" field shows "(.*) characters remaining"$/, async numberOfRemainingChars => {
    const actualMessage = await (await waitHelpers.waitForElementToBeVisible(pages.erp.newMessageModal.remainingCharsCounter())).getText();
    const expectedMessage = translationsHelper.mapTranslationVariables(
        'FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS',
        [numberOfRemainingChars]
    );

    expect(expectedMessage).equals(actualMessage);
});

When(
    /^I "(can|can't)" see the validation error "Please enter a maximum of (?:196|4000) characters to send this message" under the "(Subject|Message)" field$/,
    async (canOrNot, fieldLabel) => {
        if (constants.stepWording.CAN === canOrNot) {
            const fieldEl = (fieldLabel === 'Subject') ? await waitHelpers.waitForElementToBeVisible(pages.erp.newMessageModal.subjectMaxLengthAlert()) : await waitHelpers.waitForElementToBeVisible(
                pages.erp.newMessageModal.messageMaxLengthAlert());
            const expectedAlertTranslationPath = (fieldLabel === 'Subject')
                ? 'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MAX_SUBJECT_LENGTH'
                : 'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MAX_LENGTH';
            await expectHelpers.checkTranslation(fieldEl, expectedAlertTranslationPath);
        } else {
            (fieldLabel === 'Subject') ? await waitHelpers.waitForElementToBeNotVisible(pages.erp.newMessageModal.subjectMaxLengthAlert()) : await waitHelpers.waitForElementToBeNotVisible(
                pages.erp.newMessageModal.messageMaxLengthAlert());
        }
    }
);

When(
    /^I "(can|can't)" see the validation error "Please enter at least 5 characters" under the "(Subject|Message)" field$/,
    async (canOrNot, fieldLabel) => {
        if (constants.stepWording.CAN === canOrNot) {
            const fieldEl = (fieldLabel === 'Subject') ? await waitHelpers.waitForElementToBeVisible(pages.erp.newMessageModal.subjectMinLengthAlert()) : await waitHelpers.waitForElementToBeVisible(
                pages.erp.newMessageModal.messageMinLengthAlert());
            const expectedAlertTranslationPath = (fieldLabel === 'Subject')
                ? 'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MIN_SUBJECT_LENGTH'
                : 'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MIN_LENGTH';
            await expectHelpers.checkTranslation(fieldEl, expectedAlertTranslationPath);
        } else {
            (fieldLabel === 'Subject') ? await waitHelpers.waitForElementToBeNotVisible(pages.erp.newMessageModal.subjectMinLengthAlert()) : await waitHelpers.waitForElementToBeNotVisible(
                pages.erp.newMessageModal.messageMinLengthAlert());
        }
    }
);

Then(/^I "(send|cancel|close|close X)" "(?:[^"]*)" (modal|drawer)$/, async (button, windowType) => {
    if (button === constants.popUpActions.SEND) {
        await pages.erp.newMessageModal.sendButton().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CLOSE) {
        if (windowType === 'modal') {
            await pages.erp.sharedModal.closeButton().Click({waitForVisibility: true});
        } else {
            await pages.erp.sharedModal.closeDrawerButton().Click({waitForVisibility: true});
        }
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.sharedModal.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.showHidePanelsModal.closeXButton().Click({waitForVisibility: true});
    }
});

Then(/^I verify the confirm "cancel" popup opens with customizable text from Theme Configurator$/, async () => {
    const popUp = await waitHelpers.waitForElementToBeVisible(pages.erp.newMessageModal.cancelSendingPopUpTxt());
    await expectHelpers.checkTranslation(popUp, 'NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING');
});

Then(/^I verify the confirm "cancel" popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.newMessageModal.cancelSendingPopUpTxt());
});

When(/^I add up to "(\d+)" web messages$/, async expectedNumberOfWebMessages => {
    const actualNumberOfWebMessages = (await actions.erp.newMessagesActions.getNotificationWebMessages('subject')).length;

    if (actualNumberOfWebMessages < expectedNumberOfWebMessages) {
        const numberOfMissingMessages = expectedNumberOfWebMessages - actualNumberOfWebMessages;

        for (let i = 0; i < numberOfMissingMessages; i++) {
            await pages.erp.notificationPage.newMessageButton().Click({waitForVisibility: true});
            await pages.erp.newMessageModal.subjectInput().SetText(actions.erp.newMessagesActions.generateNewMessageData(
                'subject'));
            await pages.erp.newMessageModal.messageInput().SetText(actions.erp.newMessagesActions.generateNewMessageData(
                'message'));
            await pages.erp.newMessageModal.sendButton().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(await pages.erp.newMessageModal.newMessageModalTitle());
            await waitHelpers.waitForSpinnersToDisappear();
        }
    }
});

Then(
    /^I verify if messages are sorted correctly by contact date time in "(ascending|descending)" order$/,
    async sortType => {
        const sortedApiMessages = await actions.erp.newMessagesActions.getNotificationWebMessagesSortedByDate(sortType);
        const expectedApiMessages = [];
        sortedApiMessages.forEach(m => expectedApiMessages.push(m.subject));

        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.messagesSubjectsList().first());
        const actualUiMessages = await pages.erp.notificationPage.messagesSubjectsList().getText();

        expect(actualUiMessages.toString()).is.equal(expectedApiMessages.toString());
    }
);


