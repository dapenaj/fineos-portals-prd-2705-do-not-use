const notifications = require('./../constants/notifications');

When(/^I open notification page$/, async () => {
    testVars.searchTarget = notifications.getRandomNotification();
    await actions.erp.searchActions.openNotification(testVars.searchTarget.id);
});

When(/^I open notification with "([^"]*)" and test data "([^"]*)"$/, async (caseType, testDataDescriptor) => {
    testVars.searchTarget = notifications.getNotification(testDataDescriptor);
    await actions.erp.searchActions.openNotification(
        testVars.searchTarget.id
    );
});

Then(/^I "(can|can't)" see search section located on the Top Bar$/, async canOrNot => {
    if (canOrNot === constants.stepWording.CAN) {
        await expectHelpers.expectElementToBeVisible(
            pages.erp.searchPage.sectionSearch(),
            'I should see search widget'
        );
    } else {
        await expectHelpers.expectElementToBeNotVisible(
            pages.erp.searchPage.sectionSearch(),
            'I shouldn\'t see search widget'
        );
    }
});

Then(/^I see that search mode is set to Employee by default$/, async () => {
    await expectHelpers.expectElementTextEqual(
        pages.erp.searchPage.textSelectedSearchMode(),
        constants.searchModes.EMPLOYEE,
        'Default search mode should be set to Employee'
    );
});

When(/^I switch quick search mode to "(Employee search|Notification search)"$/, async mode => {
    await actions.erp.searchActions.setSearchMode(mode);
});

Then(/^I can switch between search modes: "([^"]*)"$/, async modesString => {
    const searchModes = modesString.split(',').map(mode => mode.trim());
    await pages.erp.searchPage.dropdownSearchMode().Click();
    await waitHelpers.waitForElementToBeVisible(pages.erp.searchPage.sectionDropdownOptions().get(0));

    for (const mode of searchModes) {
        await expectHelpers.expectElementContainText(
            await pages.erp.searchPage.sectionDropdownList(),
            mode,
            `Expect [${mode}] to be in search option`
        );
    }
});

Then(/^I can see "([^"]*)" prefix in quick search input field$/, async prefix => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.searchPage.inputSearch());
    await expectHelpers.expectElementContainAttributeValue(
        pages.erp.searchPage.inputSearch(),
        'value',
        prefix,
        `I should see [${prefix}] prefix in search box`
    );
});

Then(/^I can type notification ID$/, async () => {
    const ntnId = notifications.getRandomNotification().id;
    await pages.erp.searchPage.inputSearch().SendText(ntnId.replace('NTN-', ''));
    testVars.searchQuery = ntnId;
});

Then(/^I can see a list of items that match my search query$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.searchPage.listSearchResults().get(0));
    await expectHelpers.expectEachListElementContainText(
        pages.erp.searchPage.listSearchResults(),
        testVars.searchQuery,
        'Search results should contain search query'
    );
});

When(/^I partially type search query for "([^"]+)"$/, async searchTarget => {
    await actions.erp.searchActions.searchForRandomTarget(searchTarget, true);
});

When(/^I partially type search query for "([^"]+)" and test data "([^"]+)"$/, async (searchTarget, descriptor) => {
    await actions.erp.searchActions.searchForGivenTarget(searchTarget, descriptor, true);
});

When(/^I incorrectly type search query for "([^"]+)" and test data "([^"]+)"$/, async (searchTarget, descriptor) => {
    await actions.erp.searchActions.searchForGivenTarget(searchTarget, descriptor);
});

When(/^I can see empty results list$/, async () => {
    expect(await pages.erp.searchPage.listSearchResults().count()).is.equal(0);
});

When(
    /^The result list contains message indicating that no records are returned by the "([^"]+)" search$/,
    async searchTarget => {
        if (searchTarget === constants.searchTargets.EMPLOYEE_ID) {
            await waitHelpers.waitForElementToBeVisible(await pages.erp.searchPage.noEmployeeFoundMessage());
            await expectHelpers.checkTranslation(
                await pages.erp.searchPage.noEmployeeFoundMessage(),
                'HEADER.SEARCH.NO_EMPLOYEE_FOUND'
            );
        } else {
            await waitHelpers.waitForElementToBeVisible(await pages.erp.searchPage.noNotificationFoundMessage());
            await expectHelpers.checkTranslation(
                await pages.erp.searchPage.noNotificationFoundMessage(),
                'HEADER.SEARCH.NO_NOTIFICATION_FOUND'
            );
        }
    }
);

Then(/^Search is started once I stop typing \(with a \d+ ms delay\)$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.searchPage.sectionSearchResults(),
        timeouts.T60,
        'I should see search results'
    );
});

When(
    /^I can continue typing or change the search string for "([^"]+)" and test data "([^"]+)"$/,
    async (searchTarget, descriptor) => {
        const initialSearchQuery = testVars.searchQuery;
        await actions.erp.searchActions.searchForGivenTarget(searchTarget, descriptor, true);
        testVars.searchQuery = initialSearchQuery + testVars.searchQuery;
    }
);

When(/^I open Employee details page "([^"]+)"$/, async employeeDescriptor => {
    await actions.erp.searchActions.openEmployeeDetailsPage(employeeDescriptor, 'lastName');
});

When(/^I open Employee details page "([^"]+)" using EmployeeId$/, async employeeDescriptor => {
    await actions.erp.searchActions.openEmployeeDetailsPage(employeeDescriptor, 'EmployeeId');
});

When(/^I open "([^"]+)" of employee with defined "([^"]+)"$/, async (selectedTab, descriptor) => {
    await actions.erp.searchActions.openEmployeeDetailsPage('withNotifications', 'lastName');
    await pages.erp.employeeProfilePage.employeeProfileTab('Employee Details').click();
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.activeEmployeeDetailsTab(),
        timeouts.T15,
        'I should see notifications tab active'
    );
    await pages.erp.employeeProfilePage.communicationDetailsTab().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.activePreferencesTab(),
        timeouts.T15,
        'Is communication preferences tab active'
    );
    await waitHelpers.waitForSpinnersToDisappear();
});

When(/^I type search query for "(Employee Last Name|Notification ID)"$/, async searchTarget => {
    await actions.erp.searchActions.openResultsPage(searchTarget);
});

Then(/^I should see results list for selected target$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.searchPage.sectionSearchResultLink(0),
        timeouts.T1,
        'Expected to see at least one search result for selected query'
    );
});

When(/^I click on first search result on results page$/, async () => {
    await actions.erp.searchActions.clickOnTheFirstSearchResult();
});

When(/^I verify that correct "(Employee Profile|Notification Details)" data was loaded$/, async searchTarget => {
    if (searchTarget === constants.searchTargets.EMPLOYEE_PROFILE) {
        expect(await pages.erp.employeeProfilePage.userNameField().getText()).is.equal(testVars.firstSearchResultTitle);
        expect(await pages.erp.employeeProfilePage.personalDataField().getText()).is.equal(testVars.firstSearchResultData);
    } else {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.cards(constants.notificationCards.NOTIFICATION_SUMMARY).first());

        expect(await pages.erp.notificationPage.textNotificationId().getText()).is.equal(testVars.firstSearchResultTitle);
        expect(testVars.firstSearchResultData).contains(await pages.erp.notificationPage.textFullName().getText());
        expect(await pages.erp.notificationPage.textNotificationReason().getText()).contains(
            testVars.firstSearchResultData.split('|')[1].trim());
    }
});

Then(/^Each "([^"]+)" search result contains "([^"]+)" fields/, async (searchResult, fields) => {
    await pages.erp.searchPage.listSearchResults().each(async result => {
        expect(await pages.erp.searchPage.searchResultItemTitle(result).getText()).contains(testVars.searchQuery);

        if (searchResult.toLowerCase().startsWith('employee')) {
            expect(await pages.erp.searchPage.searchResultItemData(result).getText()).to.match(
                /^.+\|.+\|.+$/);
        } else {
            expect(await pages.erp.searchPage.searchResultItemData(result).getText()).to.match(
                /^.+\|.+$/);
        }
    });
});

Then(/^I can see results list is limited to max 5 results$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.searchPage.listSearchResults().get(0));
    expect(await pages.erp.searchPage.listSearchResults().count()).to.be.within(1, 5);
});

Then(/^The result list contains message indicating that too many records are returned by the search$/, async () => {
    if (await pages.erp.searchPage.listSearchResults().count() >= 5) {
        let translationMessage = translationsHelper.getTranslation('HEADER.SEARCH.TOO_MANY_RESULTS');
        translationMessage = translationMessage.replace('{query}', testVars.searchQuery);
        translationMessage = translationMessage.replace('{maxTotalMatches}', '5');

        expect(await pages.erp.searchPage.tooManyResultsMessage().getText()).is.equal(translationMessage);
    } else {
        await waitHelpers.waitForElementToBeNotVisible(await pages.erp.searchPage.tooManyResultsMessage());
    }
});

When(/^I click the "(My Dashboard|Messages)" page link$/, async pageName => {
    if (pageName === 'My Dashboard') {
        await pages.erp.searchPage.myDashboardButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.searchPage.messagesButton().Click({waitForVisibility: true});
    }
});

When(/^I open intake creator for "([^"]*)" employee$/, async existingOrNot => {
    await actions.erp.searchActions.openIntakeCreatorForExistingOrNotEmployee(existingOrNot);
});

When(/^I open intake creator for "([^"]*)" employee with all personal details defined$/, async existingOrNot => {
    await actions.erp.searchActions.openIntakeCreatorForExistingOrNotEmployee(existingOrNot);
});

When(/^I open intake creator "([^"]*)" using employee id$/, async descriptor => {
    await actions.erp.searchActions.openIntakeCreatorForUserWithEmployeeId(descriptor);
});

Given(/^I open personal details of employee with at least "([^"]*)"$/, async descriptor => {
    await actions.erp.searchActions.openEmployeeDetailsPage(descriptor, 'EmployeeId');
});

When(/^I open notification with "([^"]*)"$/, async descriptor => {
    testVars.searchTarget = notifications.getNotification(descriptor);
    await actions.erp.searchActions.openNotification(testVars.searchTarget.id);
});

When(/^I record the "Messages" navigation menu name badges count$/, async () => {
    testVars.newMessage.allMessagesCounter = await (await waitHelpers.waitForElementToBeVisible(pages.erp.searchPage.unreadMessagesCounter())).getText();
});

Then(/^I verify the "Messages" navigation menu name badge (decreases|increases) by (.*)$/, async (direction, delta) => {
    const historicalCounterValue = parseInt(testVars.newMessage.allMessagesCounter, 10);
    delta = parseInt(delta, 10);
    let actualCounterValue = await (await waitHelpers.waitForElementToBeVisible(pages.erp.searchPage.unreadMessagesCounter())).getText();
    actualCounterValue = parseInt(actualCounterValue, 10);
    const expectedCounterValue = direction === 'decreases' ? historicalCounterValue - delta : historicalCounterValue + delta;
    expect(expectedCounterValue).equals(actualCounterValue);
});
