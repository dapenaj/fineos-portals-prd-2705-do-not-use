const {lookupForRequests} = require('../../common/helpers/snifferHelper');
const {getAllRequestsDataIntoArray} = require('../../common/helpers/snifferHelper');
const moment = require('moment');
const notifications = require('./../constants/notifications');
const {Then} = require('cucumber');

Then(
    /^the notification header contains: 'Notification ID, Employee Name, Job Title, Organisation Unit, Work Site'$/,
    async () => {
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.textFullName(),
            pages.erp.notificationPage.textNotificationId(),
            pages.erp.notificationPage.textJobTitle(),
            pages.erp.notificationPage.textOrganisation(),
            pages.erp.notificationPage.textWorkSite()
        ]);
    }
);

When(/^I click on the Employee Name in notification header$/, async () => {
    await pages.erp.notificationPage.textFullName().Click();
});

Then(
    /^the notification summary card contains 'Case Progress, Notification Reason, Notified on' fields$/,
    async () => {
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.textNotificationReason(),
            pages.erp.notificationPage.textNotificationDate(),
            pages.erp.notificationPage.textCaseProgress()
        ]);
    }
);

Then(/^the notification summary Notified on date has "(MMM DD, YYYY|YYYY-MM-DD)" format$/, async expectedDateFormat => {
    const notifiedOnDate = await (await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.textNotificationDate())).getText();
    const actualDate = moment(notifiedOnDate, expectedDateFormat, true).toDate();
    moment(moment(actualDate).format(expectedDateFormat), expectedDateFormat, true).isValid();
});

Then(/^the notification summary card "([^"]*)" field text contains "([^"]*)"$/, async (fieldName, expectedTxt) => {
    const field = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.textFieldByName(fieldName));
    expect(await field.getText()).contain(expectedTxt);
});

When(/^I record the "Messages" tab badges count$/, async () => {
    testVars.newMessage.notificationMessagesCounter = await (await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.unreadMessagesCounter())).getText();
});

Then(/^I verify the "Messages" tab badge (decreases|increases) by (.*)$/, async (direction, delta) => {
    const historicalCounterValue = parseInt(testVars.newMessage.notificationMessagesCounter, 10);
    delta = parseInt(delta, 10);

    let actualCounterValue = await (await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.unreadMessagesCounter())).getText();
    actualCounterValue = parseInt(actualCounterValue, 10);

    const expectedCounterValue = direction === 'decreases' ? historicalCounterValue - delta : historicalCounterValue + delta;
    expect(expectedCounterValue).equals(actualCounterValue);
});

Then(/^the notification summary card contains additional field "([^"]*)"$/, async field => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.textFieldByName(field),
        timeouts.T15,
        `I should see [${field}] field on notification summary card`
    );
});

Then(/^the field "(Expected return to work)" field is highlighted$/, async field => {
    const markedField = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.markedTextFieldByName(
        field));
    const expectedColour = await markedField.getCssValue('background-color');

    expect(expectedColour).is.equal(constants.stateColors.HIGHLIGHTED_2);
});

Then(/^the notification summary card does not contain additional field "([^"]*)"$/, async field => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.textValueByFieldName(field));
});

Then(
    /^the field "(Accident date|Expected return to work|First day missing work)" shows "([^"]*)"(?: when "[^"]*" dates available)?$/,
    async (field, expectedDate) => {
        const actualDate = await pages.erp.notificationPage.textValueByFieldName(field).getText();
        const endpoint = (field === 'Expected return to work') ? constants.endPoints.notification + testVars.searchTarget.id : constants.endPoints.readDisabilityDetails;
        // eslint-disable-next-line no-nested-ternary
        field = (field === 'Accident date') ? 'disabilityClaim.accidentDate' : (field === 'Expected return to work') ? 'expectedRTWDate' : 'disabilityClaim.firstDayMissedWork';

        let apiDates = await getAllRequestsDataIntoArray(
            endpoint,
            `data.${field}`
        );

        apiDates = apiDates.sort(function (a, b) {
            return new Date(a) - new Date(b);
        });

        if (expectedDate !== '-') {
            expectedDate = moment(
                apiDates[0],
                constants.dateFormats.isoDate,
                true
            ).format(constants.dateFormats.shortDate);
        }

        expect(expectedDate).is.equal(actualDate);
    }
);

Then(
    /^the field "([^"]*)" shows the start date of "[^"]*" job protected leave period when "[^"]*" periods available$/,
    async field => {
        const actualDate = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.textValueByFieldName(
            field).getText());
        const cardName = constants.notificationCards.JOB_PROTECTED_LEAVE;
        await pages.erp.notificationPage.cards(cardName).first().Scroll();
        const firstLeavePeriod = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(
            cardName).first());
        await firstLeavePeriod.Click({waitForVisibility: true});

        const leaveBegins = translationsHelper.getTranslation('NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEAVE_BEGINS');
        let expectedDate = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.caseDetailsField(
            firstLeavePeriod,
            leaveBegins
        ));
        expectedDate = await expectedDate.getText();

        expect(expectedDate).is.equal(actualDate);
    }
);

Then(/^I verify that the 'Wage Replacement' card is "(visible|not visible)"$/, async visibility => {
    const expectedCardTitle = translationsHelper.getTranslation('NOTIFICATIONS.WAGE_REPLACEMENT.PANEL_TITLE');
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;

    if (visibility === constants.stepWording.VISIBLE) {
        await pages.erp.notificationPage.cards(wageReplacement).first().Scroll();
        await expectHelpers.expectElementTextEqual(
            pages.erp.notificationPage.nonCollapsibleHeader(wageReplacement),
            expectedCardTitle,
            `Wage Replacement card with title '${expectedCardTitle}' was not found`,
            true
        );
        await expectHelpers.expectElementToBeVisible(pages.erp.notificationPage
            .caseDollarIcon(pages.erp.notificationPage.cards(wageReplacement).first()));
        expect(await pages.erp.notificationPage.collapsibleCards(wageReplacement))
            .to.be.an('array').that.is.not.empty;
    } else {
        await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.cards(wageReplacement).first());
    }
});

Then(/^I verify that the 'Wage Replacement' empty placeholder is "(visible|not visible)"$/, async visibility => {
    if (visibility === constants.stepWording.VISIBLE) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.wageReplacementEmptyPlaceholder());
        await expectHelpers.checkTranslation(
            pages.erp.notificationPage.wageReplacementEmptyPlaceholder(),
            'NOTIFICATIONS.WAGE_REPLACEMENT.EMPTY'
        );
    } else {
        await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.wageReplacementEmptyPlaceholder());
    }
});

Then(/^I "(can|can't)" see list of collapsed items in "([^"]*)" card$/, async (canOrNot, cardName) => {
    const name = notifications.mapNotificationCardsNames(cardName);

    if (canOrNot === constants.stepWording.CAN) {
        await pages.erp.notificationPage.cards(name).first().Scroll();
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(name).first());
    } else {
        expect(await pages.erp.notificationPage.collapsibleCards(name)).to.be.an('array').that.is.empty;
    }
});

Then(/^each collapsed item on 'Wage Replacement' card has "([^"]*)" fields$/, async caseFields => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;

    expect(await pages.erp.notificationPage.collapsibleCards(wageReplacement)).to.be.an('array').that.is.not.empty;
    await pages.erp.notificationPage.collapsibleCards(wageReplacement).each(async singleBenefitCard => {
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.caseStatusText(singleBenefitCard),
            pages.erp.notificationPage.caseStatus(singleBenefitCard),
            pages.erp.notificationPage.wageReplacementCaseHeader(singleBenefitCard),
            pages.erp.notificationPage.caseArrowIcon(singleBenefitCard)
        ]);
    });
});

When(/^I expand "([^"]*)" details for all available items$/, async cardName => {
    const name = notifications.mapNotificationCardsNames(cardName);

    await pages.erp.notificationPage.cards(name).first().Scroll();
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(name).first());
    await pages.erp.notificationPage.collapsibleCards(name).each(async cardItem => {
        await waitHelpers.waitForElementToBeClickable(cardItem);
        await cardItem.Click({waitForVisibility: true});
        await waitHelpers.waitForSpinnersToDisappear();
    });
});

When(/^I see the "([^"]*)" items list is sorted by "([^"]*)"$/, async (cardName, sortingField) => {
    const name = notifications.mapNotificationCardsNames(cardName);
    const actualDates = [];
    const card = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.cards(name).first());

    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.notificationPage.caseDetailsFields(card, sortingField).each(async date => {
        await waitHelpers.waitForElementToBeVisible(date);
        const actualDate = expectHelpers.covertStringToDate(await date.getText(), constants.dateFormats.shortDate);
        actualDates.push(actualDate);
    });

    expectHelpers.checkIfArrayDateElementsAreSorted(actualDates, true);
});

When(/^I expand first 'Proposed Accommodation'$/, async () => {
    const workplaceAccommodation = constants.notificationCards.WORKPLACE_ACCOMMODATION;
    const accommodationItem = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(
        workplaceAccommodation).first());
    await pages.erp.notificationPage.proposedAccommodations(accommodationItem).first().Click({visibility: true});
});

Then(/^I verify Accommodation Date field contains "(single|range)" date$/, async dateType => {
    const workplaceAccommodation = constants.notificationCards.WORKPLACE_ACCOMMODATION;
    const cardItem = await pages.erp.notificationPage.collapsibleCards(workplaceAccommodation).first();
    const obtainedDate = await pages.erp.notificationPage.caseDetailsField(cardItem, 'Accommodation date');

    if (dateType === 'single') {
        await expectHelpers.checkDateFormat(constants.dateFormats.shortDate, obtainedDate);
    } else {
        const rangeDates = await obtainedDate.getText();
        expect(rangeDates.split('-').length).is.equal(2);

        rangeDates.split('-').forEach(date => {
            expectHelpers.checkStringDateFormat(constants.dateFormats.shortDate, date.trim());
        });
    }
});

Then(/^I see all expanded "([^"]*)" items have "([^"]*)" details$/, async (cardName, expectedFields) => {
    const name = notifications.mapNotificationCardsNames(cardName);
    const expectedFieldsArray = expectedFields.split(',');

    expect(await pages.erp.notificationPage.collapsibleCards(name)).to.be.an('array').that.is.not.empty;
    await pages.erp.notificationPage.collapsibleCards(name).each(async cardItem => {
        for (const field of expectedFieldsArray) {
            await waitHelpers.waitForElementToBeVisible(
                pages.erp.notificationPage.caseDetailsField(
                    cardItem,
                    field.trim()
                )
                , timeouts.T30
                , `Page element with label '${field.trim()}' was not found.`);
        }
    });
});

Then(/^I see first expanded "([^"]*)" item has "([^"]*)" details$/, async (cardName, expectedFields) => {
    const name = notifications.mapNotificationCardsNames(cardName);
    const expectedFieldsArray = expectedFields.split(',');
    const detailsItem = await pages.erp.notificationPage.collapsibleCards(name).first();

    for (const field of expectedFieldsArray) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.caseDetailsField(
                detailsItem,
                field.trim()
            )
            , timeouts.T30
            , `Page element with label '${field.trim()}' was not found.`);
    }
});

Then(
    /^I see all expanded "([^"]*)" items have 'status text' and 'graphical status indicator'$/,
    async cardName => {
        const name = notifications.mapNotificationCardsNames(cardName);

        expect(await pages.erp.notificationPage.collapsibleCards(name)).to.be.an('array').that.is.not.empty;
        await pages.erp.notificationPage.collapsibleCards(name).each(async cardItem => {
            await expectHelpers.expectAllElementsToBeVisible([
                pages.erp.notificationPage.caseStatusText(cardItem),
                pages.erp.notificationPage.caseStatus(cardItem)
            ]);
        });
    }
);

Then(
    /^I see first expanded "([^"]*)" item has 'status text' and 'graphical status indicator'$/,
    async cardName => {
        const name = notifications.mapNotificationCardsNames(cardName);
        const detailsItem = await pages.erp.notificationPage.collapsibleCards(name).first();

        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.caseStatusText(detailsItem),
            pages.erp.notificationPage.caseStatus(detailsItem)
        ]);
    }
);

Then(/^I verify that the 'Timeline' card is "(visible|not visible)"$/, async visibility => {
    const expectedContainerTitle = translationsHelper.getTranslation('NOTIFICATIONS.TIMELINE.TITLE');
    const timeline = constants.notificationCards.TIMELINE;

    if (visibility === constants.stepWording.VISIBLE) {
        await pages.erp.notificationPage.cards(timeline).first().Scroll();
        await expectHelpers.expectElementTextEqual(
            pages.erp.notificationPage.collapsibleHeader(timeline),
            expectedContainerTitle,
            `Timeline container with title '${expectedContainerTitle}' was not found`,
            true
        );
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.timelineLegendButton(),
            pages.erp.notificationPage.timelineCalendar(),
            pages.erp.notificationPage.caseArrowIcon(pages.erp.notificationPage.cards(timeline).first())
        ]);
    } else {
        await expectHelpers.expectElementToBeNotVisible(
            pages.erp.notificationPage.cards(timeline).first());
    }
});

Then(/^I "(can|can't)" see "([^"]*)" section on the timeline$/, async (canOrNot, timelineCaseTitle) => {
    const timeline = constants.notificationCards.TIMELINE;

    if (canOrNot === constants.stepWording.CAN) {
        await pages.erp.notificationPage.cards(timeline).first().Scroll();
        await expectHelpers.expectElementToBeVisible(
            pages.erp.notificationPage.timelineSections(
                pages.erp.notificationPage.cards(timeline).first(),
                timelineCaseTitle.trim()
            ).first(),
            `Page element with text '${timelineCaseTitle.trim()}' was not found.`
        );
    } else {
        await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage
            .timelineSections(pages.erp.notificationPage.cards(timeline).first(), timelineCaseTitle).first()
        );
    }
});

Then(/^I verify that the 'Workplace Accommodation' card is "(visible|not visible)"$/, async visibility => {
    const expectedCardTitle = translationsHelper.getTranslation('NOTIFICATIONS.ACCOMMODATIONS.PANEL_TITLE');
    const accommodation = constants.notificationCards.WORKPLACE_ACCOMMODATION;

    if (visibility === constants.stepWording.VISIBLE) {
        await pages.erp.notificationPage.cards(accommodation).first().Scroll();
        await expectHelpers.expectElementTextEqual(
            pages.erp.notificationPage.nonCollapsibleHeader(accommodation),
            expectedCardTitle,
            `Accommodation card with title '${expectedCardTitle}' was not found`,
            true
        );
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.accommodationCardIcon(
            pages.erp.notificationPage.cards(accommodation).first()));
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(accommodation).first());
    } else {
        await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.cards(accommodation).first());
    }
});

Then(/^each collapsed item on 'Workplace Accommodation' card has "([^"]*)" fields$/, async caseFields => {
    const accommodation = constants.notificationCards.WORKPLACE_ACCOMMODATION;

    expect(await pages.erp.notificationPage.collapsibleCards(accommodation)).to.be.an('array').that.is.not.empty;
    await pages.erp.notificationPage.collapsibleCards(accommodation).each(async collapsedCase => {
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.caseArrowIcon(collapsedCase),
            pages.erp.notificationPage.accommodationCaseHeader(collapsedCase),
            pages.erp.notificationPage.caseStatusText(collapsedCase),
            pages.erp.notificationPage.caseStatus(collapsedCase)
        ]);

        const accommodationHeader = await pages.erp.notificationPage.accommodationCaseDate(collapsedCase).getText();
        const accommodationDate = accommodationHeader.slice(accommodationHeader.lastIndexOf('|') + 1).trim();
        expect(moment(accommodationDate, 'MMM DD, YYYY', true).isValid()).to.be.equal(true);
    });
});

When(/^I expand "([^"]*)" details for first item$/, async cardName => {
    const name = notifications.mapNotificationCardsNames(cardName);

    await pages.erp.notificationPage.cards(name).first().Scroll();
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(name).first());

    const collapsibleItem = await pages.erp.notificationPage.collapsibleCards(name).first();
    await collapsibleItem.Click();
});

Then(
    /^hover message on the "Entitlement Begins" label is: "From this date, your employee is entitled to receive proportional pay for the periods they've been approved to be out of work"$/,
    async () => {
        const name = notifications.mapNotificationCardsNames('Wage Replacement');
        const parentElement = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(
            name).first());
        await pages.erp.notificationPage.entitlementBeginsTooltip(parentElement).Click({waitForVisibility: true});
        const label = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.entitlementBeginsTooltipInfo());
        await expectHelpers.checkTranslation(label, 'NOTIFICATIONS.WAGE_REPLACEMENT.ENTITLEMENTS_TOOLTIP');
    }
);

When(/^I expand "([^"]*)" details for first "([^"]*)" item$/, async (cardName, itemName) => {
    const name = notifications.mapNotificationCardsNames(cardName);
    await pages.erp.notificationPage.cards(name).first().Scroll();
    const foundItem = await actions.erp.notificationActions.getItemByName(name, itemName);
    await foundItem.Click({waitForVisibility: true});
});

When(/^I can see placeholder for accommodations list$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.proposedAccommodationsPlaceholder());
    await expectHelpers.checkTranslation(
        await pages.erp.notificationPage.proposedAccommodationsPlaceholder(),
        'NOTIFICATIONS.ACCOMMODATIONS.EMPTY'
    );
});

When(/^I expand "([^"]*)" details for first item with "([^"]*)" status$/, async (cardName, expectedStatus) => {
    let found = false;
    const name = notifications.mapNotificationCardsNames(cardName);
    await pages.erp.notificationPage.cards(name).first().Scroll();

    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(name).first());

    for (const collapsedCase of await pages.erp.notificationPage.collapsibleCards(name)) {
        const caseStatus = await pages.erp.notificationPage.caseStatusText(collapsedCase).getText();

        if (caseStatus.toLowerCase() === expectedStatus.toLowerCase()) {
            await collapsedCase.Click();
            found = true;
            break;
        }
    }
    expect(found).is.equal(true, `${cardName} card with status ${expectedStatus} was not found!`);
});

When(
    /^I see "([^"]*)" item with status "([^"]*)" and "(Red|Green|Yellow|White|Grey)" indicator color$/,
    async (cardName, expectedStatus, expectedColor) => {
        let found = false;
        const name = notifications.mapNotificationCardsNames(cardName);
        await pages.erp.notificationPage.cards(name).first().Scroll();
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(name).first());

        for (const collapsedCase of await pages.erp.notificationPage.collapsibleCards(name)) {
            const caseStatus = await pages.erp.notificationPage.caseStatusText(collapsedCase).getText();

            if (caseStatus.toLowerCase().startsWith(expectedStatus.toLowerCase())) {
                found = true;
                const obtainedColor = await pages.erp.notificationPage.caseStatus(collapsedCase).getCssValue(
                    'background-color');
                expect(obtainedColor).is.equal(constants.statusColors[expectedColor.toUpperCase()]);
                break;
            }
        }
        expect(found).is.equal(true, `${cardName} card with status ${expectedStatus} was not found!`);
    }
);

Then(/^I can see '(Show|Hide) reason' button$/, async action => {
    if (action === 'Show') {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.showReasonButton());
    } else {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.hideReasonButton());
    }
});

Then(/^I (can|can't) see expanded reasons list$/, async canOrNot => {
    if (canOrNot === constants.stepWording.CAN) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.reasonsList().first());
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.reasonsList().first());
    }
});

When(/^I show reasons list using 'Show reason' button$/, async () => {
    await pages.erp.notificationPage.showReasonButton().Click({waitForVisibility: true});
});

When(/^I hide reasons list using 'Hide reason' button$/, async () => {
    await pages.erp.notificationPage.hideReasonButton().Click({waitForVisibility: true});
});

Then(/^I can see first details item has list of collapsed items in 'Accommodations Proposed' section$/, async () => {
    const accommodation = constants.notificationCards.WORKPLACE_ACCOMMODATION;

    await pages.erp.notificationPage.cards(accommodation).first().Scroll();
    const detailsItem = await pages.erp.notificationPage.collapsibleCards(accommodation).first();
    expect(await pages.erp.notificationPage.proposedAccommodations(detailsItem))
        .to.be.an('array').that.is.not.empty;
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.proposedAccommodationsTitle(detailsItem));
});

Then(/^each collapsed accommodation proposal has 'Accommodation Category | Accommodation Type'$/, async () => {
    const accommodation = constants.notificationCards.WORKPLACE_ACCOMMODATION;

    await pages.erp.notificationPage.cards(accommodation).first().Scroll();
    const detailsItem = await pages.erp.notificationPage.collapsibleCards(accommodation).first();

    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.proposedAccommodations(detailsItem).first());
    await pages.erp.notificationPage.proposedAccommodations(detailsItem).each(async accommodationProposal => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.caseArrowIcon(accommodationProposal));
        expect(await accommodationProposal.getText()).to.match(
            /.+[|].+/,
            'Obtained text dos not have expected pattern.'
        );
    });
});

Then(/^I verify that the 'Job Protected Leave' card is "(visible|not visible)"$/, async visibility => {
    const expectedCardTitle = translationsHelper.getTranslation('NOTIFICATIONS.JOB_PROTECTED_LEAVE.PANEL_TITLE');
    const job = constants.notificationCards.JOB_PROTECTED_LEAVE;

    if (visibility === constants.stepWording.VISIBLE) {
        await pages.erp.notificationPage.cards(job).first().Scroll();
        await expectHelpers.expectElementContainText(
            pages.erp.notificationPage.nonCollapsibleHeader(job),
            expectedCardTitle,
            `Job protected leave card with title '${expectedCardTitle}' was not found`,
            true
        );
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.jobCardIcon(
            pages.erp.notificationPage.cards(job).first()));
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(job).first());
    } else {
        await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.cards(job).first());
    }
});

Then(/^each collapsed case on 'Job Protected Leave' card has "([^"]*)" fields$/, async caseFields => {
    const job = constants.notificationCards.JOB_PROTECTED_LEAVE;

    expect(await pages.erp.notificationPage.collapsibleCards(job)).to.be.an('array').that.is.not.empty;
    await pages.erp.notificationPage.collapsibleCards(job).each(async collapsedCase => {
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.caseArrowIcon(collapsedCase),
            pages.erp.notificationPage.jobCaseHeader(collapsedCase),
            pages.erp.notificationPage.caseStatusText(collapsedCase),
            pages.erp.notificationPage.caseStatus(collapsedCase)
        ]);
    });
});

Then(/^I can see a 'Job Protected Leave' card legend presenting graphical patterns: "([^"]*)"$/, async legendItems => {
    const job = constants.notificationCards.JOB_PROTECTED_LEAVE;
    const legendItemsArray = legendItems.split(',').map(item => item.trim());

    await pages.erp.notificationPage.cards(job).first().Scroll();
    const legendContent = await pages.erp.notificationPage.jobLegend(pages.erp.notificationPage.cards(job).first()).getText();

    for (const legendItem of legendItemsArray) {
        expect(legendContent).to.have.string(legendItem);
    }
});

Then(/^I see Job Protected Leave period of type “([^"]*)" has “([^"]*)" fields$/, async (leaveType, expectedFields) => {
    const expectedFieldsArray = expectedFields.split(',').map(field => field.trim());
    const allPeriods = await actions.erp.notificationActions.getAllJobPeriods();
    const testPeriod = allPeriods.filter(period => period.type.toLowerCase() === leaveType.toLowerCase());
    expect(testPeriod.length).to.be.gt(0, `This test requires notification with ${leaveType} leave period`);
    expect(testPeriod[0].fieldNames).to.have.members(expectedFieldsArray);
});

Then(
    /^I see first expanded "([^"]*)" item with leave period type "([^"]*)" has "([^"]*)" fields$/,
    async (cardName, leaveType, expectedFields) => {
        const name = notifications.mapNotificationCardsNames(cardName);
        const expectedFieldsArray = expectedFields.split(',');
        const detailsItem = await pages.erp.notificationPage.collapsibleCards(name).first();
        const mappedLeaveType = notifications.mapLeaveTypeNames(leaveType);
        const leaveItem = await pages.erp.notificationPage.absenceCard(detailsItem, mappedLeaveType).first();
        for (const field of expectedFieldsArray) {
            await waitHelpers.waitForElementToBeVisible(
                pages.erp.notificationPage.caseDetailsField(
                    leaveItem,
                    field.trim()
                )
                , timeouts.T30
                , `Page element with label '${field.trim()}' was not found.`);
        }
    }
);

Then(
    /^I see Job Protected Leave period of type “([^"]*)" has 'status text' and 'graphical status indicator'$/,
    async leaveType => {
        const job = constants.notificationCards.JOB_PROTECTED_LEAVE;
        const detailsItem = pages.erp.notificationPage.collapsibleCards(job).first();
        const mappedLeaveType = notifications.mapLeaveTypeNames(leaveType);
        const leaveItem = pages.erp.notificationPage.absenceCard(detailsItem, mappedLeaveType).first();
        await leaveItem.Scroll();
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.caseStatusText(leaveItem),
            pages.erp.notificationPage.caseStatus(leaveItem)
        ]);
    }
);

Then(/^I verify the list of "(Documents|Outstanding Items)" is not empty$/, async tabName => {
    if (tabName === constants.fieldNames.DOCUMENTS) {
        await pages.erp.notificationPage.rightArrows().last().Click({waitForVisibility: true});
        await pages.erp.notificationPage.documentsTab().Click({waitForVisibility: true});
        const itemsCount = await pages.erp.notificationPage.documentsList().count();
        expect(itemsCount).to.be.gt(0);
    } else {
        await pages.erp.notificationPage.outstandingTab().click();
        const itemsCount = await pages.erp.notificationPage.outstandingListItems().count();
        expect(itemsCount).to.be.gt(0);
    }
});

Then(/^I click "(Documents|Messages|Outstanding Items)" tab$/, async tabName => {
    if (tabName === 'Documents') {
        await pages.erp.notificationPage.documentsTab().Click({waitForVisibility: true});
    } else if (tabName === 'Outstanding Items') {
        await pages.erp.notificationPage.outstandingTab().Click({waitForVisibility: true});
    } else {
        await pages.erp.notificationPage.messagesTab().Click({waitForVisibility: true});
        testVars.newMessage.savedMessagesList = await pages.erp.notificationPage.messagesSubjectsList().getText();
    }
});

Then(/^I verify no web message is sent to the server$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    const expectedMessagesList = testVars.newMessage.savedMessagesList;
    const actualMessagesList = await pages.erp.notificationPage.messagesSubjectsList().getText();

    expect(expectedMessagesList.toString()).is.equal(actualMessagesList.toString());
});

When(/^I click the "New Message" button$/, async () => {
    await pages.erp.notificationPage.newMessageButton().Click({waitForVisibility: true});
});

Then(
    /^I verify the list of web messages is refreshed using an animation with the new message highlighted in yellow temporary$/,
    async () => {
        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationsWithOutstandingReqPage.pageSpinners().first());
        await waitHelpers.waitForSpinnersToDisappear();

        const firstMessageBackground = await (await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.newMessagesList().first())).getCssValue(
            'background-color');
        expect(firstMessageBackground).equals(constants.colors.LIGHT_YELLOW);

        const firstMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.newMessagesList().first());
        await waitHelpers.waitElementCssValueEquals(firstMessage, 'background-color', constants.colors.TRANSPARENT);
    }
);

When(/^I can see new read message with correct content is first one on the list$/, async () => {
    const todayDate = moment().format(constants.dateFormats.shortDate);
    await waitHelpers.waitForSpinnersToDisappear();

    const firstMessage = await waitHelpers.waitForElementToBeStatic(pages.erp.notificationPage.newMessagesList().first());
    const firstMessageText = await firstMessage.getText();
    expect(firstMessageText).contains(testVars.newMessage.subject);
    expect(firstMessageText).contains(testVars.newMessage.message);
    expect(firstMessageText).contains(todayDate);

    const firstMessageBorderLeftColor = await firstMessage.getCssValue('border-left-color');
    const firstMessageIconColor = await (await waitHelpers.waitForElementToBeStatic(pages.erp.notificationPage.newMessagesReadIcons().first())).getCssValue(
        'color');
    expect(firstMessageBorderLeftColor).contains(constants.colors.TRANSPARENT);
    expect(firstMessageIconColor).contains(constants.colors.GREY);
});

Then(/^I verify that the "(Outstanding|Documents)" tab shows empty message$/, async tabName => {
    if (tabName === 'Outstanding') {
        const actualMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.outstandingEmptyMessage());
        await expectHelpers.checkTranslation(actualMessage, 'NOTIFICATIONS.ALERTS.OUTSTANDING.EMPTY');
    } else {
        const actualMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.documentsEmptyMessage());
        await expectHelpers.checkTranslation(actualMessage, 'NOTIFICATIONS.ALERTS.DOCUMENTS.EMPTY');
    }
});

Then(/^Uploaded document is (visible|not visible) on the documents list$/, async isVisible => {
    const availableDocuments = await pages.erp.notificationPage.documentsButtonsList().getText();

    if (isVisible === 'visible') {
        expect(availableDocuments.toString()).contains(testVars.outstandingItemTitle);
    } else {
        expect(availableDocuments.toString()).not.contains(testVars.outstandingItemTitle);
    }
});

Then(/^each 'not-provided' item contains a coloured bar to the left of the card$/, async () => {
    const notProvidedItems = await actions.erp.notificationActions.getNotProvidedOutstandingItems();
    expect(notProvidedItems.length).is.greaterThan(0);

    for (const item of notProvidedItems) {
        await item.Scroll();
        await expectHelpers.expectElementCssValueEquals(item, 'border-left-color', constants.colors.PURPLE, '');
    }
});

Then(/^each 'not-provided' item contains 'orange dot' icon$/, async () => {
    const notProvidedItems = await actions.erp.notificationActions.getNotProvidedOutstandingItems();
    expect(notProvidedItems.length).is.greaterThan(0);

    for (const item of notProvidedItems) {
        await item.Scroll();
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.notProvidedOutstandingItemIcon(item));
    }
});

Then(/^each provided item contains the name of the document$/, async () => {
    const providedItems = await actions.erp.notificationActions.getProvidedOutstandingItems();
    expect(providedItems.length).is.greaterThan(0);

    for (const item of providedItems) {
        await item.Scroll();
        const documentName = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.nameOfProvidedDocument(
            item));

        expect(documentName).is.not.empty;
    }
});

Then(/^each provided item contains 'green tick' icon$/, async () => {
    const providedItems = await actions.erp.notificationActions.getProvidedOutstandingItems();
    expect(providedItems.length).is.greaterThan(0);

    for (const item of providedItems) {
        await item.Scroll();
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.providedOutstandingItemIcon(item));
    }
});

Then(/^each provided item contains text 'Provided and awaiting verification'$/, async () => {
    const expectedStatus = translationsHelper.getTranslation('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED');
    const providedItems = await actions.erp.notificationActions.getProvidedOutstandingItems();
    expect(providedItems.length).is.greaterThan(0);

    for (const item of providedItems) {
        await item.Scroll();
        expect(await pages.erp.notificationPage.providedOutstandingItem(item).getText()).include(expectedStatus);
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.uploadDocumentLink(item));
    }
});

Then(/^each not-provided item contains information on the source of the item$/, async () => {
    const expectedStatus = translationsHelper.getTranslation('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD');
    const notProvidedItems = await actions.erp.notificationActions.getNotProvidedOutstandingItems();
    expect(notProvidedItems.length).is.greaterThan(0);

    for (const item of notProvidedItems) {
        await item.Scroll();
        expect(await pages.erp.notificationPage.notProvidedOutstandingItem(item).getText()).include(expectedStatus.split(
            ':')[0]);
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.uploadDocumentLink(item));
    }
});

Then(/^I see that outstanding item is (not marked|marked) as provided in alerts section$/, async isMarked => {
    if (isMarked === 'marked') {
        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.uploadedOutstandingItemIcon(
            testVars.outstandingItemTitle
        ));
    } else {
        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.notUploadedOutstandingItemIcon(
            testVars.outstandingItemTitle
        ));
    }
});

Then(/^only provided items are included in the orange count in 'Outstanding' tab$/, async () => {
    const missingItemsNumber = await (await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.notProvidedItemsNumber())).getText();
    const notProvidedItems = await actions.erp.notificationActions.getNotProvidedOutstandingItems();
    const providedItems = await actions.erp.notificationActions.getProvidedOutstandingItems();
    const allItems = await pages.erp.notificationPage.allOutstandingItems();

    expect(Number.parseInt(missingItemsNumber, 10)).is.equal(allItems.length - providedItems.length);
    expect(notProvidedItems.length).is.equal(allItems.length - providedItems.length);
});

Then(/^I can see at least one item waiting "(to be provided|for verification)" with all fields$/, async itemType => {
    let expectedStatus;

    if (itemType === 'to be provided') {
        expectedStatus = translationsHelper.getTranslation('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD');
        const notProvidedItems = await actions.erp.notificationActions.filterOutstandingItems(expectedStatus.split(':')[0]);
        expect(notProvidedItems.length).is.greaterThan(0);

        for (const item of notProvidedItems) {
            await item.Scroll();
            expect(await pages.erp.notificationPage.notProvidedOutstandingItem(item).getText()).include(expectedStatus.split(
                ':')[0]);
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.notProvidedOutstandingItemIcon(item));
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.uploadDocumentLink(item));
        }
    } else {
        expectedStatus = translationsHelper.getTranslation('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED');
        const providedItems = await actions.erp.notificationActions.filterOutstandingItems(expectedStatus);
        expect(providedItems.length).is.greaterThan(0);

        for (const item of providedItems) {
            await item.Scroll();
            expect(await pages.erp.notificationPage.providedOutstandingItem(item).getText()).equal(expectedStatus);
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.providedOutstandingItemIcon(item));
            await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.uploadDocumentLink(item));
        }
    }
});

When(/^I read last document$/, async () => {
    await actions.erp.notificationActions.readLastDocument();
});

Then(/^each "(read|unread)" document contains appropriate icon reflecting its state$/, async state => {
    let expectedColor = constants.colors.GREY;
    const documents = await actions.erp.notificationActions.filterDocumentsByStatus(state);
    expect(documents.length).is.greaterThan(0);

    for (const document of documents) {
        await document.Scroll();

        if (state === 'unread') {
            expectedColor = constants.colors.PURPLE;
        }

        await expectHelpers.expectElementCssValueEquals(
            await pages.erp.notificationPage.documentItemIcon(document),
            'color',
            expectedColor,
            ''
        );
    }
});

Then(/^each document contains date on which it was linked to the case$/, async () => {
    const allDocuments = await pages.erp.notificationPage.documentsList();

    for (const document of allDocuments) {
        const actualDate = await pages.erp.notificationPage.documentItemDate(document).getText();
        expect(moment(actualDate, constants.dateFormats.shortDate, true).isValid() || actualDate === '-').to.be.equal(
            true);
    }
});

Then(
    /^each "(read|unread)" document "(contains|doesn't contain)" coloured bar to the left of the card$/,
    async (state, contains) => {
        let expectedColor = constants.colors.PURPLE;
        const documents = await actions.erp.notificationActions.filterDocumentsByStatus(state);

        for (const document of documents) {
            if (contains === 'doesn\'t contain') {
                expectedColor = constants.colors.TRANSPARENT;
            }

            await expectHelpers.expectElementCssValueEquals(
                document,
                'border-left-color',
                expectedColor,
                ''
            );
        }
    }
);

Then(/^I verify that the payments section is "(visible|not visible)"$/, async visibility => {
    const expectedTitle = translationsHelper.getTranslation('PAYMENTS.LATEST_PAYMENTS');
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;

    if (visibility === constants.stepWording.VISIBLE) {
        const detailsItem = await pages.erp.notificationPage.cards(wageReplacement).first().Scroll();

        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement),
            timeouts.T30,
            'Payment collapsible card was not found.'
        );
        await expectHelpers.expectElementContainText(
            pages.erp.notificationPage.latestPaymentsHeader(detailsItem),
            expectedTitle,
            `Latest Payment card with title '${expectedTitle}' was not found`,
            true
        );
    } else {
        await waitHelpers.waitForElementToBeNotVisible(
            pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement),
            timeouts.T30,
            'Payment card was found, but it shouldn\'t.'
        );
    }
});

When(/^I expand 'Latest Payments' section$/, async () => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));
    await pages.erp.notificationPage.caseArrowIcon(latestPaymentsSection).Click();
});

When(/^I expand 'Latest Payments' section for first "([^"]*)" item$/, async itemName => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const foundItem = await actions.erp.notificationActions.getItemByName(wageReplacement, itemName);
    await pages.erp.notificationPage.latestPaymentsHeader(foundItem).Click({waitForVisibility: true});
});

When(/^I expand last payment$/, async () => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));
    await pages.erp.notificationPage.collapsiblePayments(latestPaymentsSection).first().Click();
});

Then(/^latest payments section is collapsed by default$/, async () => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const parentElement = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsiblePaymentsCard(
        wageReplacement));
    expect(await pages.erp.notificationPage.collapsibleContainer(parentElement).getAttribute('aria-expanded')).to.be.equal(
        'false');
});

When(/^I open 'Latest Payments' history$/, async () => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));
    await pages.erp.notificationPage.latestPaymentsHistoryButton(latestPaymentsSection).Click();
});

When(/^'View payment history' button is (visible|not visible)$/, async visibility => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));

    if (visibility === constants.stepWording.VISIBLE) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.latestPaymentsHistoryButton(
            latestPaymentsSection));
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationPage.latestPaymentsHistoryButton(
            latestPaymentsSection));
    }
});

When(/^I hover on 'i' icon located in Payment Method field$/, async () => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    await pages.erp.notificationPage.paymentMethodTooltipIcon(lastPayment).HooverOverElement();
});

When(/^I can see payment method popover with following text "([^"]*)"$/, async tooltipFields => {
    const tooltipFieldsArray = tooltipFields.split(',').map(f => f.trim().replace(/\s+/g, '_').toUpperCase());

    for (const field of tooltipFieldsArray) {
        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.paymentMethodTooltip(
            field));
    }
});

When(/^I show 'Payments Breakdown'$/, async () => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    await pages.erp.notificationPage.paymentBreakdownButton(lastPayment).Click({waitForVisibility: true});
});

When(/^I can expand details for adjustment with payee specified$/, async () => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    await waitHelpers.waitForElementToBeStatic(pages.erp.notificationPage.adjustmentDetailsButton(lastPayment).first());
    await pages.erp.notificationPage.adjustmentDetailsButton(lastPayment).first().Click({waitForVisibility: true});
});

Then(/^I see a list of adjustments used to calculate the 'Net Payment Amount'$/, async () => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    const sum = await actions.erp.notificationActions.sumAdjustmentsAmounts(lastPayment);

    expect(await pages.erp.notificationPage.adjustmentSummaryPayment(lastPayment).first().getText()).contains(sum.toString());
});

Then(/^the 'Net Payment Amount' is the payment amount used for the payment line$/, async () => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    const netPayment = await pages.erp.notificationPage.adjustmentSummaryPayment(lastPayment).first().getText();
    const paymentHeader = await pages.erp.notificationPage.paymentHeaderDetails(lastPayment).getText();

    expect(netPayment.split('\n')[1]).is.equal(paymentHeader.split('\n')[0]);
});

Then(/^I verify that payments are sorted basing on payment date in descending order$/, async () => {
    const paymentsDates = [];
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));

    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.collapsiblePayments(
        latestPaymentsSection).first());

    await pages.erp.notificationPage.collapsiblePayments(latestPaymentsSection).each(async payment => {
        const actualDate = (await payment.getText()).split('\n')[1].toString();
        expect(moment(actualDate, constants.dateFormats.shortDate, true).isValid()).to.be.equal(true);
        paymentsDates.push(moment(actualDate, constants.dateFormats.shortDate, true));
    });

    expect(paymentsDates.length).is.greaterThan(0);
    expect(paymentsDates).to.be.descending;
});

Then(
    /^I verify that "([^"]*)" benefit payments are sorted basing on payment date in descending order$/,
    async itemName => {
        const paymentsDates = [];
        const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
        const foundItem = await actions.erp.notificationActions.getItemByName(wageReplacement, itemName);
        const latestPaymentsSection = await pages.erp.notificationPage.latestPayments(foundItem);

        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.collapsiblePayments(
            latestPaymentsSection).first());

        await pages.erp.notificationPage.collapsiblePayments(latestPaymentsSection).each(async payment => {
            const actualDate = (await payment.getText()).split('\n')[1].toString();
            expect(moment(actualDate, constants.dateFormats.shortDate, true).isValid()).to.be.equal(true);
            paymentsDates.push(moment(actualDate, constants.dateFormats.shortDate, true));
        });

        expect(paymentsDates.length).is.greaterThan(0);
        expect(paymentsDates).to.be.descending;
    }
);

Then(/^I can see 4 most recent payments$/, async () => {
    const paymentsDates = [];
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement));
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.collapsiblePayments(
        latestPaymentsSection).first());

    await pages.erp.notificationPage.collapsiblePayments(latestPaymentsSection).each(async payment => {
        paymentsDates.push(await payment.getText());
    });

    expect(paymentsDates.length).is.equal(4);
    expect(paymentsDates.toString()).is.equal(testVars.payments.paymentsHistory.splice(0, 4).toString());
});

Then(/^I verify that the 'Timeline' card is (expanded|collapsed)$/, async cardState => {
    const timeline = constants.notificationCards.TIMELINE;
    const timelineCard = await pages.erp.notificationPage.cards(timeline).first();
    const timelineCollapsibleCard = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleContainer(
        timelineCard));

    if (cardState === 'expanded') {
        expect(await timelineCollapsibleCard.getAttribute('aria-expanded')).to.be.equal('true');
    } else {
        expect(await timelineCollapsibleCard.getAttribute('aria-expanded')).to.be.equal('false');
    }
});

Then(/^I verify that the timeline card is showing current date$/, async () => {
    const timelineTodayDate = await pages.erp.notificationPage.timelineTodayDate().getText();
    expect(moment().format('MMM D')).is.equal(timelineTodayDate);
});

Then(/^I can display legend by clicking 'Show caption' button$/, async () => {
    const timelineLegend = await pages.erp.notificationPage.timelineLegendButton();
    const legendActualState = await timelineLegend.getAttribute('data-test-el');

    if (legendActualState.includes('SHOW')) {
        timelineLegend.click({waitForVisibility: true});
    }
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.timelineLegendColors().first());

    expect(await pages.erp.notificationPage.timelineLegendButton().getAttribute('data-test-el')).contains('CLOSE');
    expect(await pages.erp.notificationPage.timelineLegendColors().count()).is.equal(4);
    expect(await pages.erp.notificationPage.timelineLegendGraphicPatterns().count()).is.equal(3);
});

Then(/^I can hide legend by clicking 'Hide caption' button$/, async () => {
    const timelineLegend = await pages.erp.notificationPage.timelineLegendButton();
    const legendActualState = await timelineLegend.getAttribute('data-test-el');

    if (legendActualState.includes('CLOSE')) {
        timelineLegend.click({waitForVisibility: true});
    }
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.notificationPage.timelineLegendColors().first());
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.notificationPage.timelineLegendGraphicPatterns().first());

    expect(await pages.erp.notificationPage.timelineLegendButton().getAttribute('data-test-el')).contains('SHOW');
});

When(/^I (expand|collapse) 'Timeline' card$/, async cardState => {
    const timeline = constants.notificationCards.TIMELINE;
    const timelineCard = await pages.erp.notificationPage.cards(timeline).first();
    const timelineCollapsibleCard = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleContainer(
        timelineCard));
    const actualState = await timelineCollapsibleCard.getAttribute('aria-expanded');

    if ((actualState === 'true' && cardState === 'collapse') || (actualState === 'false' && cardState === 'expand')) {
        await timelineCollapsibleCard.Click({waitForVisibility: true});
    }
});

Then(/^number of "([^"]*)" sections is equal to number of absence cases$/, async sectionName => {
    const timeline = constants.notificationCards.TIMELINE;
    const timelineCard = await pages.erp.notificationPage.cards(timeline).first();
    const numberOfTimelineSections = await pages.erp.notificationPage.timelineSections(
        timelineCard,
        sectionName
    ).count();
    const numberOfCards = await pages.erp.notificationPage.cards(notifications.mapNotificationCardsNames(
        sectionName)).count();

    expect(numberOfTimelineSections).is.equal(numberOfCards);
});

When(/^I expand 'Timeline' "([^"]*)" section$/, async sectionName => {
    const timeline = constants.notificationCards.TIMELINE;
    const timelineCard = await pages.erp.notificationPage.cards(timeline).first();

    await pages.erp.notificationPage.timelineSections(
        timelineCard,
        sectionName
    ).first().Click({waitForVisibility: true});
});

Then(
    /^text of lines in expanded "([^"]*)" section should match text of leave plans in the absence case$/,
    async sectionName => {
        let cardItems;
        const timelineSectionItems = [];
        const cardName = notifications.mapNotificationCardsNames(sectionName);
        const timelineCard = await pages.erp.notificationPage.cards(constants.notificationCards.TIMELINE).first();
        const timelineSection = await pages.erp.notificationPage.timelineSections(timelineCard, sectionName).first();

        await pages.erp.notificationPage.timelineSectionCases(timelineSection, sectionName).each(async sectionsCase => {
            timelineSectionItems.push(await sectionsCase.getText());
        });

        await pages.erp.notificationPage.cards(cardName).first().Scroll();

        if (sectionName === 'Wage Replacement') {
            cardItems = await actions.erp.notificationActions.mergeWageReplacementBenefits(cardName);
        } else if (sectionName === 'Temporary accommodations') {
            cardItems = await actions.erp.notificationActions.filterTemporaryAccommodations(cardName);
        } else {
            cardItems = await actions.erp.notificationActions.getJobProtectedLeaveCards(cardName);
        }

        expect(cardItems.length).is.equal(timelineSectionItems.length);
        expect(cardItems.length).is.greaterThan(0);
        expect(cardItems.sort().toString()).to.equal(timelineSectionItems.sort().toString().replace(
            constants.regExps.newLine,
            ''
        ));
    }
);

Then(/^the label shown for each line is amalgamation "Accommodation Category | Accommodation Type"$/, async () => {
    const timelineCard = await pages.erp.notificationPage.cards(constants.notificationCards.TIMELINE).first();
    const timelineSection = await pages.erp.notificationPage.timelineSections(
        timelineCard,
        'Temporary accommodations'
    ).first();

    await pages.erp.notificationPage.timelineSectionCases(
        timelineSection,
        'Temporary accommodations'
    ).each(async sectionCase => {
        expect(await sectionCase.getText()).to.match(
            /.+[|].+/,
            'Obtained text dos not have expected pattern.'
        );
    });
});

Then(/^each payment is collapsed by default and has 'Payment Amount, Payment Date' fields$/, async () => {
    const wageReplacement = constants.notificationCards.WAGE_REPLACEMENT;
    const latestPaymentsSection = pages.erp.notificationPage.collapsiblePaymentsCard(wageReplacement);

    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsiblePayments(latestPaymentsSection).first());
    await pages.erp.notificationPage.collapsiblePayments(latestPaymentsSection).each(async singlePayment => {
        expect(await pages.erp.notificationPage.collapsibleContainer(singlePayment)
            .getAttribute('aria-expanded')).to.be.equal('false');

        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.notificationPage.caseArrowIcon(singlePayment),
            pages.erp.notificationPage.paymentHeaderDetails(singlePayment)
        ]);

        const paymentHeader = await pages.erp.notificationPage.paymentHeaderDetails(singlePayment).getText();
        expect(moment(paymentHeader.split(/\n/)[1], 'MMM DD, YYYY', true).isValid()).to.be.equal(true);
    });
});

Then(/^latest payment has "([^"]*)" fields$/, async paymentFields => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    const paymentFieldsArray = paymentFields.split(',').map(f => f.trim());

    for (const field of paymentFieldsArray) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.caseDetailsField(
                lastPayment,
                field
            )
            , timeouts.T30
            , `Page element with label '${field}' was not found.`);
    }
});

When(
    /^I check that notification has "([^"]*)", "([^"]*)", "([^"]*)"$/,
    async (jobTitle, organisationUnit, workSite) => {
        testVars.employeeJobTitle = await pages.erp.notificationPage.employeeNotificationDetailHeader(jobTitle).getText();
        testVars.employeeOrganisationUnit = await pages.erp.notificationPage.employeeNotificationDetailHeader(
            organisationUnit).getText();
        testVars.employeeWorkSite = await pages.erp.notificationPage.employeeNotificationDetailHeader(workSite).getText();
    }
);

Then(/^All fields in notification header are populated with corresponding data$/, async () => {
    const apiDataResponse = await lookupForRequests(`/groupClient/notifications/${testVars.searchTarget.id}`);
    expect(testVars.employeeJobTitle).to.equal(apiDataResponse[0].data.customer.jobTitle);
    expect(testVars.employeeOrganisationUnit).to.equal(apiDataResponse[0].data.customer.organisationUnit);
    expect(testVars.employeeWorkSite).to.equal(apiDataResponse[0].data.customer.workSite);
});

Then(/^each Adjustment has Adjustment Type Name and Adjustment Type Amount$/, async () => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();

    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.adjustmentHeaders(
        lastPayment).first());

    await pages.erp.notificationPage.adjustmentHeaders(lastPayment).each(async adjustment => {
        const adjustmentHeaderFields = (await adjustment.getText()).split('\n');

        expect(adjustmentHeaderFields.length).is.equal(2);
        expect(adjustmentHeaderFields[0]).is.not.empty;
        expect(adjustmentHeaderFields[1].trim()).to.match(/^(-)?\$\d+\.\d{2}$/g);
    });
});

Then(/^Adjustment details contains following "([^"]*)" fields$/, async detailsFields => {
    const lastPayment = await actions.erp.notificationActions.getLastPayment();
    const detailsFieldsArray = detailsFields.split(',').map(f => f.trim());
    const adjustment = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.adjustmentDetails(
        lastPayment).first());

    for (const field of detailsFieldsArray) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.caseDetailsField(
                adjustment,
                field
            )
            , timeouts.T30
            , `Page element with label '${field}' was not found.`);
    }
});

When(/^I select "([^"]*)" from Actions menu on the 'Notification Summary' card$/, async optionName => {
    await pages.erp.notificationPage.notificationSummaryActionsButton().Click({waitForVisibility: true});
    const mappedOption = actions.erp.notificationActions.mapActionsDropdownOptions(optionName);
    await pages.erp.notificationPage.notificationSummaryActionsDropDown(mappedOption)
        .Click({waitForVisibility: true});
});

Then(/^I can see the cancelled periods in the 'Job-protected Leave' drawer$/, async () => {
    await pages.erp.notificationPage.leaveCardStatusJobProtection().each(async item => {
        const expectedStatus = await translationsHelper.getTranslation('DECISION_STATUS_CATEGORY.CANCELLED');
        await item.getText().then(async status => {
            if (status === expectedStatus) {
                testVars.startDateJobProtected = await pages.erp.notificationPage.textValueJobProtected('Leave begins').first().getText();
                testVars.endDateJobProtected = await pages.erp.notificationPage.textValueJobProtected(
                    'Requested through').first().getText();
            }
        });
    });
});

Then(/^I check cancelled period does not appear in the date picker$/, async () => {
    const startDateIso = moment(
        testVars.startDateJobProtected.toString(),
        constants.dateFormats.shortDate,
        true
    ).format(constants.dateFormats.isoDate);
    const endDateIso = moment(testVars.endDateJobProtected.toString(), constants.dateFormats.shortDate, true).format(
        constants.dateFormats.isoDate);
    await pages.erp.requestForMoreTimeModal.absenceFormStartDate().Click({waitForVisibility: true});
    await pages.erp.requestForMoreTimeModal.absenceFormStartDate().SendText(testVars.startDateJobProtected);

    let dayCancelledPeriod = startDateIso;
    await datePickerHelper.navigateDate(moment(dayCancelledPeriod));
    while (dayCancelledPeriod < endDateIso) {
        dayCancelledPeriod = moment(dayCancelledPeriod).add(1, 'days').format(constants.dateFormats.isoDate);
        const colorCancelledDatePicker = await pages.erp.requestForMoreTimeModal.dayMonthDatePicker(dayCancelledPeriod).getCssValue(
            'background-color');
        expect(colorCancelledDatePicker).equals(constants.colors.TRANSPARENT);
    }
});

Then(/^I should see popup informing that additional time might not be the applicable$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.requestMoreTimePopupCancelButton(),
        timeouts.T30,
        'Expected to see  pop up cancel button'
    );
    await expectHelpers.expectElementToBeVisible(pages.erp.notificationPage.requestMoreTimePopupAcceptButton());

    await expectHelpers.checkTranslation(
        pages.erp.notificationPage.requestMoreTimePopupHeader(),
        'REQUEST_FOR_MORE_TIME.POPOVER_HEADER'
    );

    const notificationReason = await pages.erp.notificationPage.textNotificationReason().getText();
    const translation = translationsHelper.mapTranslationVariables(
        'REQUEST_FOR_MORE_TIME.POPOVER_BODY',
        [notificationReason]
    );
    expect(await pages.erp.notificationPage.requestMoreTimePopupBody().getText()).is.equal(translation);
});

Then(/^I "(accept|cancel)" additional time popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.notificationPage.requestMoreTimePopupAcceptButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.notificationPage.requestMoreTimePopupCancelButton().Click({waitForVisibility: true});
    }
});

Then(/^I should see acknowledgment screen$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.absenceRequestPopUpCancelButton(),
        timeouts.T30,
        'Absence request pop up is not visible.'
    );

    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.absenceRequestPopUpHeader());
    await expectHelpers.checkTranslation(
        pages.erp.notificationPage.absenceRequestPopUpHeader(),
        'REQUEST_FOR_MORE_TIME.SUCCESS_HEAD'
    );

    await expectHelpers.checkTranslation(
        pages.erp.notificationPage.absenceRequestPopUpMessage(),
        'REQUEST_FOR_MORE_TIME.SUCCESS_MESSAGE'
    );
});

When(/^I open "([^"]*)" popup$/, async requestString => {
    await pages.erp.notificationPage.actionsLink().Click({waitForVisibility: true});
    await pages.erp.notificationPage.requestLeaveToRemove().Click({waitForVisibility: true});
});

Then(/^I verify that only one period can be removed$/, async () => {
    const count = await pages.erp.notificationPage.periodToBeRemoved().count();
    expect(count).to.equal(1);
});

When(/^I click on the actions menu options on the notification page$/, async () => {
    await pages.erp.notificationPage.actionsLink().Click({waitForVisibility: true});
});

Then(/^I verify that the periods are organized by Leave Plan grouping$/, async () => {
    const job = constants.notificationCards.JOB_PROTECTED_LEAVE;
    const card = pages.erp.notificationPage.cards(job).first();
    const [jobProtectedCard] = testVars.searchTarget.jobProtectedLeaveCards;
    const expectedLeavePlans = jobProtectedCard.leavePlans.map(x => x.leavePlanName);
    await card.Scroll();
    const leavePlans = await pages.erp.notificationPage.jobLeavePlansList(card).map(x => x.getText());
    expect(expectedLeavePlans).to.have.members(leavePlans, 'List of Leave Plans is not as expected');
});

Then(/^leaves within each Leave Plan are sorted by Start Date then by the Absence Reason$/, async () => {
    const leavePlans = await actions.erp.notificationActions.getJobProtectedData();
    for (const leavePlan of leavePlans) {
        const startDates = leavePlan.periods.map(period => period.startDate);
        expectHelpers.expectDatesListToBeSorted(startDates);
    }
});

Then(/^I should see job protection card in empty state with leaves list$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.jobNoLeavePlansSection());
    await expectHelpers.expectElementToBeVisible(pages.erp.notificationPage.jobNoLeavePlansList().first());
});

Then(/^leaves without Leave Plan are sorted by Decision Status$/, async () => {
    const decisionList = await pages.erp.notificationPage.jobNoLeavePlansList()
        .map(async item => await pages.erp.notificationPage.jobNoLeavePlansDecisionsList(item).getText());
    expect(decisionList).to.be.ascending;
});

Then(
    /^each leave with end date "([^"]*)" should be formatted as follows "([^"]*)"$/,
    async (endDateAvailability, textFormat) => {
        const leaveList = await pages.erp.notificationPage.jobNoLeavePlansList().map(async item => await item.getText());
        const regex = new RegExp(`^${textFormat.replace(/-[a-zA-Z]+-/g, '.+')}$`);
        for (const leave of leaveList) {
            expect(leave).to.match(regex);
        }
    }
);

Then(/^I can customize messages on no leave plans section$/, async () => {
    const header = 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ASSESSMENT_CONCLUDED_TITLE';
    const footer = 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ASSESSMENT_CONCLUDED_FOOTER';
    await expectHelpers.checkTranslation(
        pages.erp.notificationPage.translationElement(header),
        header
    );
    await expectHelpers.checkTranslation(
        pages.erp.notificationPage.translationElement(footer),
        footer
    );
});

Then(/^I verify that the periods are merged into one leave$/, async () => {
    const jobProtectedData = await actions.erp.notificationActions.getJobProtectedData();
    for (const leavePlan of jobProtectedData) {
        expect(leavePlan.periods.length).to.eq(1);
    }
});

Then(/^start date is the start date of the first period and end date is the end date of the last period$/, async () => {
    const apiDataResponse1 = (await lookupForRequests('/absences/absence-period-decisions*'));
    const jobProtectedData = await actions.erp.notificationActions.getJobProtectedData();
    const expectedLeavePlanName = apiDataResponse1[0].data.decisions[0].period.leavePlan.name;
    const [actualPeriod] = jobProtectedData.find(plan => plan.leavePlanName === expectedLeavePlanName).periods;
    const dateFormat = constants.dateFormats.shortDate;
    const date = moment(actualPeriod.startDate, dateFormat, true).add(2, 'hour').toString();
    expect(new Date(apiDataResponse1[0].data.startDate).toString()).to.include(date.toString());
});

Then(/^I can see the Paid Leave Plan Indicator next to plan name$/, async () => {
    const wageReplacementCards = [];

    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.cards(constants.notificationCards.WAGE_REPLACEMENT).last());
    await pages.erp.notificationPage.cards(constants.notificationCards.WAGE_REPLACEMENT).each(async wRCard => {
        await wRCard.Scroll();
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCardsOfParent(wRCard).last());
        await pages.erp.notificationPage.collapsibleCardsOfParent(wRCard).each(async wRSubCard => {
            // eslint-disable-next-line prefer-destructuring
            const actualWageReplacement = (await wRSubCard.getText()).toString().split(constants.regExps.newLine)[0];
            wageReplacementCards.push(actualWageReplacement.trim());
        });
    });

    expect(wageReplacementCards).is.not.empty;

    let wasFound = false;
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.cards(constants.notificationCards.JOB_PROTECTED_LEAVE).last());
    await pages.erp.notificationPage.cards(constants.notificationCards.JOB_PROTECTED_LEAVE).each(async jPLCard => {
        await jPLCard.Scroll();
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCardsOfParent(jPLCard).last());
        await pages.erp.notificationPage.collapsibleCardsOfParent(jPLCard).each(async jPLSubCard => {
            // eslint-disable-next-line prefer-destructuring
            const actualJobProtectedLeave = (await jPLSubCard.getText()).toString().split(constants.regExps.newLine)[0];

            if (wageReplacementCards.toString().includes(actualJobProtectedLeave)) {
                await expectHelpers.expectElementToBeVisible(pages.erp.notificationPage.jobPaidLeaveIndicator(jPLSubCard));
                wasFound = true;
            }
        });
    });

    expect(wasFound).is.true;
});

When(/^I hover on the Paid Leave Plan Indicator$/, async () => {
    const jobProtectedLeaveCard = await pages.erp.notificationPage.cards(constants.notificationCards.JOB_PROTECTED_LEAVE).first();
    const indicator = pages.erp.notificationPage.jobPaidLeaveIndicator(jobProtectedLeaveCard);
    await browser.actions().mouseMove(indicator).perform();
});

Then(/^I should see popover with message: "([^"]*)"$/, async expectedText => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.notificationPage.jobPaidLeaveIndicatorTooltip(),
        timeouts.T3,
        'I should see tooltip next to paid leave indicator'
    );
    const tooltipText = await pages.erp.notificationPage.jobPaidLeaveIndicatorTooltip().getText();
    expect(tooltipText).to.eq(expectedText, 'Tooltip text is not as expected');
});

Then(/^I can customize message on Paid Leave Plan Indicator popover$/, async () => {
    const translationKey = 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.PAID_LEAVE_TOOLTIP';
    await expectHelpers.checkTranslation(
        pages.erp.notificationPage.translationElement(translationKey),
        translationKey
    );
});

Then(
    /^status bar at the side of "([^"]*)" leave section has "([^"]*)" color and gradient indicator for "([^"]*)" period type$/,
    async (status, color, leaveType) => {
        const allPeriods = await actions.erp.notificationActions.getAllJobPeriods();
        const testPeriod = allPeriods.filter(
            period => period.type.toLowerCase() === leaveType.toLowerCase() && period.status === status
        );
        expect(testPeriod.length).to.be.gt(0, `This test requires notification with ${status}, ${leaveType} period`);
        expect(testPeriod[0].barColor).to.eq(color, 'Color of the left bar does not match expectation');
        expect(testPeriod[0].type.toLowerCase()).to.eq(
            leaveType,
            'Gradient the left bar does not match expectation'
        );
    }
);
Then(/^the status indicator of "([^"]*)" "([^"]*)" leave has "([^"]*)" color$/, async (status, leaveType, color) => {
    const allPeriods = await actions.erp.notificationActions.getAllJobPeriods();
    const testPeriod = allPeriods.filter(
        period => period.type.toLowerCase() === leaveType.toLowerCase() && period.status === status
    );
    expect(testPeriod[0].dotColor).to.eq(color, 'Color of the left bar does not match expectation');
});

When(/^I expand all collapsible cards$/, async () => {
    const cards = await pages.erp.notificationPage.jobProtectedLeaveCards();
    cards.forEach(el => el.Click({waitForVisibility: true}));

    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.expandedCards().first());
    await waitHelpers.waitForElementToBeStatic(await pages.erp.notificationPage.expandedCards().first());

    testVars.expandedLeaveCards = await pages.erp.notificationPage.expandedCards();
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.absenceReason().first());
});

When(/^I click "([^"]*)" link on an intermittent period$/, async btnName => {
    await pages.erp.notificationPage.showLatestEpisodesButton().Click({waitForVisibility: true});
});

When(/^I expand job protected leave card period "([^"]*)" actuals$/, async number => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.jobProtectedLeaveCards().first());
    await pages.erp.notificationPage.jobProtectedLeaveCards().each(async card => {
        await card.Click({waitForVisibility: true});
    });
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.expandedCards().first());
});

Then(/^I see a list of actual time taken$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.hideLatestEpisodesButton());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.actualTimeTakenList());
});

When(/^I click "(Show|Hide)" latest episodes link on an intermittent period$/, async label => {
    if (label === 'Show') {
        await pages.erp.notificationPage.showLatestEpisodesButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.notificationPage.hideLatestEpisodesButton().Click({waitForVisibility: true});
    }
});

When(/^I click 'Hide latest episodes' link on an intermittent period$/, async () => {
    await pages.erp.notificationPage.hideLatestEpisodesButton().Click({waitForVisibility: true});
});

Then(/^I see the list of actual time taken is collapsed and hidden$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.notificationPage.hideLatestEpisodesButton());
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.notificationPage.actualTimeTakenList());
});

Then(/^the link name is changed from 'Hide latest episodes' to 'Show latest episodes'$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.notificationPage.hideLatestEpisodesButton());
    const label = await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.showLatestEpisodesButton());
    await expectHelpers.checkTranslation(label, 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON');
});

When(/^I click View more link under the list$/, async () => {
    await pages.erp.notificationPage.viewMoreEpisodes().Click({waitForVisibility: true});
});

Then(/^a pop-up is displayed with a list of all the actual time taken for that period$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.intermittentTimeModal());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.modalTitle());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.episodeDatesColumnName());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.filterIcon());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.decisionStatuses().first());
});

Then(/^I "(can|can't)" see the 'View more' link under the list$/, async canOrNot => {
    canOrNot === 'can' ? await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationPage.viewMoreEpisodes()) : await waitHelpers.waitForElementToBeNotVisible(
        await pages.erp.notificationPage.viewMoreEpisodes());
});

Then(/^the title of the pop-up is in the form \[leave plan name\] - \[absence reason\]$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.modalTitle());
    const modalTitle = await pages.erp.intermittentTimeModal.modalTitle().getText();
    expect(modalTitle.toString().split('-')[0]).to.be.not.empty;
    expect(modalTitle.toString().split('-')[1]).to.be.not.empty;
});

When(/^I click the Episode date\(s\) column name$/, async () => {
    await pages.erp.intermittentTimeModal.episodeDatesColumnName().Click({waitForVisibility: true});
});

Then(
    /^the list of items is sorted by Episode date\(s\) in "(descending|ascending)" order(?: by default)?$/,
    async ascendingOrNot => {
        await actions.erp.intermittentTimeActions.verifyIntermittentTimeDateSorting(ascendingOrNot);
    }
);

When(/^I click the filter icon next to the Decision column name$/, async () => {
    await pages.erp.intermittentTimeModal.filterIcon().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intermittentTimeModal.filterDropDown());
});

Then(/^I can filter the items by the "(Approved|Pending|Declined)" Decision$/, async option => {
    await pages.erp.intermittentTimeModal.filterOptions(option).Click({waitForVisibility: true});
    await pages.erp.intermittentTimeModal.filterDropDownOkBtn().Click({waitForVisibility: true});
});

Then(/^I verify the results on the list are correctly filtered by "(Approved|Pending|Declined)"$/, async option => {
    await actions.erp.intermittentTimeActions.verifyAllFilteredStatuses(option);
});

Then(/^for each item on the list I see Episode date\(s\), Episode duration, Decision values$/, async () => {
    await actions.erp.intermittentTimeActions.verifyDatesDurationDecisionIsNotEmptyOnTheList();
});

Then(/^the Episode date\(s\) is displayed in format mmm dd, yyyy on the list$/, async () => {
    await actions.erp.intermittentTimeActions.verifyDateFormatOnTheList();
});

Then(/^the Episode date\(s\) is displayed in format mmm dd, yyyy in the pop-up$/, async () => {
    await actions.erp.intermittentTimeActions.verifyDateFormatInThePopUp();
});

Then(
    /^I see the Decision on the list contains a "([^"]*)" icon representing the "([^"]*)" status$/,
    async (colour, status) => {
        await pages.erp.notificationPage.intermittentTimeIndicators(colour).each(async indicator => {
            expect(await indicator.getText()).to.contain(status.toString());
        });
    }
);

Then(/^for each item in the pop-up I see Episode date\(s\), Episode duration, Decision values$/, async () => {
    await actions.erp.intermittentTimeActions.verifyDateFormatInThePopUp();
});

Then(
    /^I see the Decision in the pop-up contains a "([^"]*)" icon representing the "([^"]*)" status$/,
    async (colour, status) => {
        await pages.erp.intermittentTimeModal.intermittentTimeIndicators(colour).each(async indicator => {
            expect(await indicator.getText()).to.contain(status.toString());
        });
    }
);
