const users = require('../../employer-portal/constants/users');
const faker = require('faker');

Given(/^I navigate to Control Access page$/, async () => {
    await actions.erp.controlUserAccessActions.openPage();
});

Given(/^I make sure that "([^"]*)" is "(disabled|enabled)"$/, async (userDescriptor, userStatus) => {
    if (userStatus === 'enabled') {
        await actions.erp.controlUserAccessActions.enableUser(userDescriptor);
    } else {
        await actions.erp.controlUserAccessActions.disableUser(userDescriptor);
    }
    await waitHelpers.waitForSpinnersToDisappear();
});

Given(/^I click 'disable user' on a "([^"]*)"$/, async userDescriptor => {
    await actions.erp.controlUserAccessActions.disableUser(userDescriptor, false);
});

Given(/^I "(disable|enable)" "([^"]*)" user$/, async (userStatus, userDescriptor) => {
    if (userStatus === 'disable') {
        await actions.erp.controlUserAccessActions.disableUser(userDescriptor, true);
    } else {
        await actions.erp.controlUserAccessActions.enableUser(userDescriptor);
    }
});

Given(/^I can view the portal users belonging to Organisation$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.controlUserAccessPage.usersList().first());

    await pages.erp.controlUserAccessPage.usersList().each(async user => {
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.controlUserAccessPage.userName(user),
            pages.erp.controlUserAccessPage.actionButton(user),
            pages.erp.controlUserAccessPage.userRole(user)
        ]);
    });
});

Then(/^I see user item consist of 'User name, Role and modify status link'$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.controlUserAccessPage.usersList().first());

    await pages.erp.controlUserAccessPage.usersList().each(async user => {
        const userName = await expectHelpers.expectElementToBeVisible(pages.erp.controlUserAccessPage.userName(user));
        expect(await userName.getText()).is.not.empty;

        const userRole = await expectHelpers.expectElementToBeVisible(pages.erp.controlUserAccessPage.userRole(user));
        expect(await userRole.getText()).is.not.empty;

        const userStatus = await expectHelpers.expectElementToBeVisible(pages.erp.controlUserAccessPage.actionButton(
            user));

        if ((await userName.getText()).toString().includes('disabled')) {
            await expectHelpers.checkTranslation(userStatus, 'CONTROL_USER_ACCESS.ENABLE_USER');
        } else {
            await expectHelpers.checkTranslation(userStatus, 'CONTROL_USER_ACCESS.DISABLE_USER');
        }

        expect(await userStatus.getText()).is.not.empty;
    });
});

When(/^I use filter to search "([^"]+|logged in|existing)" user name or its part$/, async userName => {
    let searchQuery = userName;

    await waitHelpers.waitForElementToBeVisible(pages.erp.controlUserAccessPage.usersList().first());
    testVars.usersList = (await pages.erp.controlUserAccessPage.usersList().getText()).toString();

    if (userName === 'logged in') {
        const loggedInUser = (await pages.erp.searchPage.userMenu().getText()).toString();
        searchQuery = loggedInUser.toUpperCase().replace(/\s+/g, '.');
    } else if (userName === 'existing') {
        searchQuery = await actions.erp.controlUserAccessActions.getExistingRandomUserName();
        searchQuery = searchQuery.substr(
            faker.random.number(10),
            faker.random.number({max: searchQuery.length, min: 11})
        );
    }
    testVars.userSearchQuery = searchQuery;
    await pages.erp.controlUserAccessPage.searchUserInput().SendText(searchQuery);
});

Then(/^I can clear the filter to reset list to original view$/, async () => {
    await pages.erp.controlUserAccessPage.resetFilterButton().Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();

    const expectedUsersList = testVars.usersList;
    const actualUsersList = (await pages.erp.controlUserAccessPage.usersList().getText()).toString();

    expect(expectedUsersList).is.equal(actualUsersList);
});

Then(/^I can see "(correctly filtered|empty)" list of users$/, async listState => {
    if (listState === 'empty') {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.controlUserAccessPage.usersList().first());
    } else {
        await waitHelpers.waitForElementToBeVisible(pages.erp.controlUserAccessPage.usersList().first());
        await pages.erp.controlUserAccessPage.usersList().each(async user => {
            const userName = await waitHelpers.waitForElementToBeVisible(pages.erp.controlUserAccessPage.userName(user));
            expect((await userName.getText()).toString().toLowerCase()).contain(testVars.userSearchQuery.toString().toLowerCase());
        });
    }
});

When(/^I "(confirm|cancel)" popup to disable user$/, async decision => {
    if (decision === constants.stepWording.CONFIRM) {
        await pages.erp.controlUserAccessPage.buttonYesOnConfirmationPopover().Click();
    } else {
        await pages.erp.controlUserAccessPage.buttonNoOnConfirmationPopover().Click();
    }
});

When(/^the popup to disable user opens$/, async () => {
    const popUpTxt = await waitHelpers.waitForElementToBeVisible(pages.erp.controlUserAccessPage.confirmationPopoverBody());
    await expectHelpers.checkTranslation(popUpTxt, 'CONTROL_USER_ACCESS.CHANGE_PERMISSIONS');

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.controlUserAccessPage.buttonYesOnConfirmationPopover(),
        pages.erp.controlUserAccessPage.buttonNoOnConfirmationPopover()
    ]);
});

Then(
    /^I "(can|cannot)" see alert confirming "([^"]*)" access was "(disabled|enabled)"$/,
    async (canOrNot, userDescriptor, status) => {
        const displayedName = users.getUser(userDescriptor).displayName;
        const checker = canOrNot === constants.stepWording.CAN ? waitHelpers.waitForElementToBeVisible.bind(waitHelpers) : waitHelpers.waitForElementToBeNotVisible.bind(
            waitHelpers);
        const object = status === constants.stepWording.ENABLED ? pages.erp.controlUserAccessPage.alertUserEnabled(
            displayedName) : pages.erp.controlUserAccessPage.alertUserDisabled(displayedName);
        await checker(object, timeouts.T60, `Expected I ${canOrNot} see alert for ${status} access`);

        if (canOrNot === 'can') {
            const translation = translationsHelper.mapTranslationVariables(
                `CONTROL_USER_ACCESS.${status.toUpperCase()}_USER_INFO`,
                [displayedName]
            );
            expect(await object.getText()).is.equal(translation);
        }
    }
);

Then(/^I can see "(disabled|enabled)" access for chosen "([^"]*)" on users list$/, async (status, userDescriptor) => {
    await actions.erp.controlUserAccessActions.waitForState(status, userDescriptor);
});

Then(/^I verify HR test user "(can|can't)" log in to the portal$/, async canOrNot => {
    const userDescriptor = constants.userTypes.HR_TEST_USER;
    await actions.erp.loginActions.logOut();
    await actions.erp.loginActions.logIn(userDescriptor);
    if (canOrNot === constants.stepWording.CAN) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.myDashboardPage.panelMainLayout(),
            timeouts.T15,
            'I should see home screen'
        );
    } else {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.myDashboardPage.errorPage(),
            timeouts.T15,
            'I should see error page'
        );
    }
});
