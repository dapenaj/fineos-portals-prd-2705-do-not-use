const moment = require('moment');
const {getRequestData} = require('../../common/helpers/snifferHelper');

Then(/^I verify that Additional Absence form is "(visible|not visible)"$/, async visibility => {
    if (visibility === constants.stepWording.VISIBLE) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.requestForMoreTimeModal.absenceFormTitle());
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.requestForMoreTimeModal.absenceFormStartDate(),
            pages.erp.requestForMoreTimeModal.absenceFormEndDate(),
            pages.erp.requestForMoreTimeModal.absenceFormTextArea(),
            pages.erp.sharedModal.okButton().first(),
            pages.erp.sharedModal.cancelButton(),
            pages.erp.sharedModal.closeXButton()
        ]);
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.requestForMoreTimeModal.absenceFormTitle());
    }
});

Then(/^I should see all mandatory and optional fields of 'Additional Absence' form$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.requestForMoreTimeModal.absenceFormTitle(),
        timeouts.T30,
        'Expect to see absence for title'
    );

    const datePickerLabel = await waitHelpers.waitForElementToBeVisible(
        pages.erp.requestForMoreTimeModal.absenceFormLabels(
            translationsHelper.getTranslation('REQUEST_FOR_MORE_TIME.EXTRA_TIME_LABEL')));
    const reasonInputTextLabel = await waitHelpers.waitForElementToBeVisible(
        pages.erp.requestForMoreTimeModal.absenceFormLabels(
            translationsHelper.getTranslation('REQUEST_FOR_MORE_TIME.DESCRIPTION_LABEL')));

    expect(await datePickerLabel.getText()).contains('*');
    expect(await reasonInputTextLabel.getText()).not.contains('*');
});

When(/^I type following text "([^"]*)" in the "Time off reason description" field$/, async txt => {
    const timeOffReasonTextArea = await pages.erp.requestForMoreTimeModal.absenceFormTextArea();
    await timeOffReasonTextArea.SetText(txt);
    await expectHelpers.expectElementAttributeValueEquals(timeOffReasonTextArea, 'value', txt, '', true);
});

Then(/^"required period validation" error message is visible$/, async () => {
    const errorMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.requestForMoreTimeModal.datePeriodsValidation());
    await expectHelpers.checkTranslation(errorMessage, 'REQUEST_FOR_MORE_TIME.REQUIRED_PERIOD_VALIDATION');
});

Then(/^"Request for more time" confirmation popup is "(visible|not visible)"$/, async visibility => {
    if (visibility === constants.stepWording.VISIBLE) {
        const header = await waitHelpers.waitForElementToBeVisible(pages.erp.requestForMoreTimeModal.confirmationHeader());
        await expectHelpers.checkTranslation(header, 'REQUEST_FOR_MORE_TIME.SUCCESS_HEAD');

        const message = await waitHelpers.waitForElementToBeVisible(pages.erp.requestForMoreTimeModal.confirmationMessage());
        await expectHelpers.checkTranslation(message, 'REQUEST_FOR_MORE_TIME.SUCCESS_MESSAGE');

        await waitHelpers.waitForElementToBeVisible(pages.erp.sharedModal.confirmationSuccessImage());
        await expectHelpers.expectAllElementsToBeVisible([
            pages.erp.sharedModal.closeXButton(),
            pages.erp.sharedModal.closeButton()
        ]);
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.requestForMoreTimeModal.confirmationHeader());
    }
});

When(/^I select additional time in not occupied time slot$/, async () => {
    await pages.erp.requestForMoreTimeModal.absenceFormStartDate().Click({waitForVisibility: true});
    let startDate = await pages.erp.notificationPage.notOccupiedLeaveDays().first().getAttribute('title');
    startDate = moment(startDate, constants.dateFormats.isoDate, true).format(constants.dateFormats.shortDate);
    await pages.erp.requestForMoreTimeModal.absenceFormStartDate().SendText(startDate + protractor.Key.ENTER);

    await pages.erp.requestForMoreTimeModal.absenceFormEndDate().Click({waitForVisibility: true});
    const endDate = moment(startDate, constants.dateFormats.shortDate, true).add(
        5,
        'days'
    ).format(constants.dateFormats.shortDate);
    await pages.erp.requestForMoreTimeModal.absenceFormEndDate().SendText(endDate + protractor.Key.ENTER);
});

When(/^I open date range picker$/, async () => {
    await pages.erp.requestForMoreTimeModal.absenceFormStartDate().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(pages.erp.datePicker.actualMonthYearCards().first());
});

Then(/^I verify I cannot add additional absence that overlaps existing one$/, async () => {
    const expectedTimeBarStartDate = await getRequestData('absence-period-decisions?absenceId', 'startDate');
    const expectedTimeBarEndDate = await getRequestData('absence-period-decisions?absenceId', 'endDate');

    if (expectedTimeBarStartDate && expectedTimeBarEndDate) {
        const actualPeriodStartDate = await actions.erp.requestForMoreTimeActions.findPeriodStartDate(
            expectedTimeBarStartDate);
        expect(expectedTimeBarStartDate.toString()).is.equal(actualPeriodStartDate.toString());

        await actions.erp.requestForMoreTimeActions.verifyTimeBarElementsDisability(
            expectedTimeBarStartDate,
            expectedTimeBarEndDate
        );

        const actualPeriodEndDate = await actions.erp.requestForMoreTimeActions.findPeriodEndDate(
            expectedTimeBarEndDate);
        expect(expectedTimeBarEndDate.toString()).is.equal(actualPeriodEndDate.toString());
    } else {
        throw new Error(`Absence period does not exists. Period start data is: [${expectedTimeBarStartDate}] and period and date is: [${expectedTimeBarEndDate}]`);
    }
});

Then(
    /^existing period should be represented as "(yellow|red|green)" "(solid line|horizontal split line|striped line)" time bar displayed between correct start & end dates$/,
    async (color, gradientType) => {
        const expectedTimeBarStartDate = await getRequestData('absence-period-decisions?absenceId', 'startDate');
        const expectedTimeBarEndDate = await getRequestData('absence-period-decisions?absenceId', 'endDate');

        if (expectedTimeBarStartDate && expectedTimeBarEndDate) {
            const actualPeriodStartDate = await actions.erp.requestForMoreTimeActions.findPeriodStartDate(
                expectedTimeBarStartDate);
            expect(expectedTimeBarStartDate.toString()).is.equal(actualPeriodStartDate.toString());

            await actions.erp.requestForMoreTimeActions.verifyTimeBarGradientAndColor(
                color,
                gradientType,
                expectedTimeBarStartDate,
                expectedTimeBarEndDate
            );

            const actualPeriodEndDate = await actions.erp.requestForMoreTimeActions.findPeriodEndDate(
                expectedTimeBarEndDate);
            expect(expectedTimeBarEndDate.toString()).is.equal(actualPeriodEndDate.toString());
        } else {
            throw new Error(`Absence period does not exists. Period start data is: [${expectedTimeBarStartDate}] and period and date is: [${expectedTimeBarEndDate}]`);
        }
    }
);

Then(/^by default I see the current month when the calendar is shown$/, async () => {
    const actualMonth = (await pages.erp.datePicker.actualMonthYearCards().first().getText()).toString();
    const expectedMonth = moment().format('MMM');
    expect(expectedMonth).is.equal(actualMonth);

    const actualYear = (await pages.erp.datePicker.actualMonthYearCards().last().getText()).toString();
    const expectedYear = moment().format('YYYY');
    expect(expectedYear).is.equal(actualYear);
});

Then(
    /^I verify I am able to navigate to the "(\d+)" "(previous|next)" months from today’s date$/,
    async (numberOfMonths, dir) => {
        const finalDate = (dir === 'next') ? moment().add(Number(numberOfMonths), 'months') : moment().subtract(Number(
            numberOfMonths), 'months');
        const expectedMonth = finalDate.format('MMM');
        const expectedYear = finalDate.format('YYYY');

        await datePickerHelper.navigateDate(moment(finalDate, constants.dateFormats.isoDate, true));
        const actualMonth = (await pages.erp.datePicker.actualMonthYearCards().first().getText()).toString();
        const actualYear = (await pages.erp.datePicker.actualMonthYearCards().last().getText()).toString();

        expect(expectedMonth).is.equal(actualMonth);
        expect(expectedYear).is.equal(actualYear);
    }
);

Then(
    /^I verify I am able to navigate to the "(\d+)" "(previous|next)" years from today’s date$/,
    async (numberOfYears, dir) => {
        const finalDate = (dir === 'next') ? moment().add(Number(numberOfYears), 'years') : moment().subtract(Number(
            numberOfYears), 'years');
        const expectedYear = finalDate.format('YYYY');

        await datePickerHelper.navigateYear(moment(finalDate, constants.dateFormats.isoDate, true));
        const actualYear = (await pages.erp.datePicker.actualMonthYearCards().last().getText()).toString();

        expect(expectedYear).is.equal(actualYear);
    }
);
