const notifications = require('./../constants/notifications');
const moment = require('moment');
const {Then, When} = require('cucumber');

Then(/^the system opens the Employee Details page$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.actionsLink(),
        timeouts.T15,
        'I should see employee profile'
    );
});

Then(/^Top section of employee profile page contains "([^"]*)" fields$/, async firstNameLastName => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.userNameField(),
        timeouts.T15,
        `I should see [${firstNameLastName}] field on employee profile page`
    );
    const expectedEmployee = `${testVars.searchTarget.firstName} ${testVars.searchTarget.lastName}`;
    await expectHelpers.expectElementContainText(
        pages.erp.employeeProfilePage.userNameField(),
        expectedEmployee,
        `I should see name ${expectedEmployee} on employee profile page`
    );
});

Then(/^Notifications tab is selected by default$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.activeNotificationsTab(),
        timeouts.T15,
        'I should see notifications tab active'
    );
});

Then(/^Each notification item contains "([^"]*)" fields$/, async fields => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.employeeProfilePage.notificationsList().first());
    await pages.erp.employeeProfilePage.notificationsList().each(async notification => {
        await expectHelpers.expectElementToBeVisible(await pages.erp.employeeProfilePage.notificationReason(notification));

        expect(await pages.erp.employeeProfilePage.notificationLink(notification).getText()).match(/NTN-\d+/);

        await expectHelpers.checkDateFormat(
            constants.dateFormats.shortDate,
            await pages.erp.employeeProfilePage.notificationDate(notification)
        );
    });
});

Then(
    /^The notifications list is sorted by "Notified on date" with "(ascending|descending)" order(?: by default)?$/,
    async sortingOrder => {
        const notificationsDates = [];
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.notificationDates().first());

        await pages.erp.employeeProfilePage.notificationDates().each(async date => {
            const actualDate = (await date.getText()).toString();
            expect(moment(actualDate, constants.dateFormats.shortDate, true).isValid()).to.be.equal(true);
            notificationsDates.push(moment(actualDate, constants.dateFormats.shortDate, true));
        });

        if (sortingOrder === constants.sortingTypes.ASCENDING) {
            expect(notificationsDates).to.be.ascending;
        } else {
            expect(notificationsDates).to.be.descending;
        }
    }
);

When(/^I select "(Newest first|Oldest first)" option in the "Sorting by" select$/, async optionName => {
    optionName = (optionName === 'Newest first') ? 'DESC' : 'ASC';
    await pages.erp.employeeProfilePage.notificationsSortSelect().Click({waitForVisibility: true});
    await pages.erp.webMessagesPage.sortOption(optionName.toString().toUpperCase()).Click({waitForVisibility: true});

    await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.notificationsList().last());
});

Then(/^I can't see the "Sorting by" select$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.employeeProfilePage.notificationsSortSelect());
});

When(/^I click first notification link$/, async () => {
    const firstNotification = await pages.erp.employeeProfilePage.notificationsList().first();
    testVars.firstSearchResultTitle = await pages.erp.employeeProfilePage.notificationLink(firstNotification).getText();
    testVars.firstSearchResultData = `${await pages.erp.employeeProfilePage.userNameField()
        .getText()}|${await pages.erp.employeeProfilePage.notificationReason(firstNotification).getText()}`;
    testVars.actualLinkData = await firstNotification.getText();
    await pages.erp.employeeProfilePage.notificationLink(firstNotification).Click({waitForVisibility: true});
});

Then(/^I can see information that no notification is available for the user$/, async () => {
    await expectHelpers.checkTranslation(
        await pages.erp.employeeProfilePage.lackOfNotificationsMessage(),
        'PROFILE.NO_NOTIFICATIONS'
    );
});

Then(/^I can see "([^"]*)" on notification containing "([^"]*)"$/, async (descriptor, expectedLabel) => {
    const givenNotificationId = notifications.getNotification(descriptor).id;
    let wasFound = false;

    await waitHelpers.waitForElementToBeVisible(await pages.erp.employeeProfilePage.notificationsList().first());
    await pages.erp.employeeProfilePage.notificationsList().each(async notification => {
        const notificationId = await pages.erp.employeeProfilePage.notificationLink(notification).getText();

        if (notificationId === givenNotificationId) {
            wasFound = true;

            if (expectedLabel === '') {
                await expectHelpers.expectElementToBeNotVisible(await pages.erp.employeeProfilePage.notificationType(
                    notification).first());
                await pages.erp.employeeProfilePage.notificationLink(notification).Click({waitForVisibility: true});
                await waitHelpers.waitForSpinnersToDisappear();
                await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.cards(
                    constants.notificationCards.WAGE_REPLACEMENT).first());
                await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.cards(
                    constants.notificationCards.JOB_PROTECTED_LEAVE).first());
                await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationPage.cards(
                    constants.notificationCards.WORKPLACE_ACCOMMODATION).first());
            } else {
                expect(await pages.erp.employeeProfilePage.notificationType(notification).getText()).contains(
                    expectedLabel);
                await pages.erp.employeeProfilePage.notificationLink(notification).Click({waitForVisibility: true});

                const name = notifications.mapNotificationCardsNames(expectedLabel);
                const card = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.cards(name).first());
                await card.Scroll();
                await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.collapsibleCards(name).first());
            }
        }
    });
    expect(wasFound, `Current Employee does not have notification ${givenNotificationId}`).is.true;
});

When(/^I open "(Employee Details|Notifications|Time Taken)" tab$/, async tabName => {
    await pages.erp.employeeProfilePage.employeeProfileTab(tabName).click();
    await waitHelpers.waitForSpinnersToDisappear();
    if (tabName === 'Notifications') {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.activeNotificationsTab(),
            timeouts.T15,
            `I should see name ${tabName} on employee profile page`
        );
    } else if (tabName === 'Employee Details') {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.activeEmployeeDetailsTab(),
            timeouts.T15,
            `I should see name ${tabName} on employee profile page`
        );
    } else if (tabName === 'Time taken') {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.activeTimeTakenTab(),
            timeouts.T15,
            `I should see name ${tabName} on employee profile page`
        );
    }

    await waitHelpers.waitForSpinnersToDisappear();
});

When(/^I open Notifications tab$/, async () => {
    await pages.erp.employeeProfilePage.employeeNotificationsTab();
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.activeNotificationsTab(),
        timeouts.T15,
        'I should see active Notifications tab'
    );
});

When(/^I check the notifications ordered as more recent notification$/, async () => {
    // await pages.erp.notificationPage.notificationLinkEmployee().each(async notification => {
    const notificationIds = await pages.erp.notificationPage.notificationLinkEmployee().map(x => x.getText());
    // eslint-disable-next-line
    testVars.moreRecentNotification = notificationIds[1];
    // eslint-disable-next-line
    testVars.lessRecentNotification = notificationIds[0];
});

Then(/^I see "2" leave plans for which the employee has reported time off$/, async () => {
    testVars.leavePlanNameFirst = await pages.erp.timeTakenPage.leavePlanTitle().first().getText();
    testVars.leavePlanNameLast = await pages.erp.timeTakenPage.leavePlanTitle().last().getText();
});

Then(/^The list of Leave Plans is ordered by the plan with the most recent leave requests$/, async () => {
    const job = constants.notificationCards.JOB_PROTECTED_LEAVE;
    await actions.erp.searchActions.openNotification(testVars.moreRecentNotification);
    const cardMoreRecent = await pages.erp.notificationPage.cards(job).first();
    await cardMoreRecent.Scroll();
    const leavePlanTitleMoreRecent = await pages.erp.notificationPage.jobLeavePlansList(cardMoreRecent).first().getText();
    await actions.erp.searchActions.openNotification(testVars.lessRecentNotification);
    const cardLessRecent = await pages.erp.notificationPage.cards(job).first();
    await cardLessRecent.Scroll();
    const leavePlanTitleLessRecent = await pages.erp.notificationPage.jobLeavePlansList(cardLessRecent).first().getText();
    expect(leavePlanTitleMoreRecent, testVars.leavePlanNameFirst).to.be.equal;
    expect(leavePlanTitleLessRecent, testVars.leavePlanNameLast).to.be.equal;
});

// eslint-disable-next-line
Then(/^The list of leave plans is ordered alphabetically$/, async () => {
    expect(testVars.leavePlanNameFirst < testVars.leavePlanNameLast).to.be.true;
});

Then(/^I can see "([^"]*)" fields on Personal Details section$/, async listOfElements => {
    const labels = listOfElements.split(',').map(item => item.trim().toLowerCase());
    const labelsList = await actions.erp.employeeProfileActions.getEmployeeDetailsDataLabels();
    labels.forEach(item => expect(labelsList.join(',').toLowerCase()).to.include(item));
});

Then(/^the Employee’s Full Name is displayed in the format \[First Name] \[Last Name]$/, async () => {
    const employee = testVars.searchTarget;
    const valuesList = await actions.erp.employeeProfileActions.getEmployeeDetailsDataValues();
    const [user] = valuesList;
    expect(user).to.include(`${employee.firstName} ${employee.lastName}`);
});

Then(
    /^I should see all "([^"]*)" sections populated correctly with "([^"]*)"$/,
    async (preferencesHeader, preferencesList) => {
        const preferences = preferencesList.split(',').map(item => item.trim());
        const [phoneCalls, alertUpdates, reminders, writtenCorrespondence] = preferences;
        await waitHelpers.waitForElementToBeStatic(
            pages.erp.employeeProfilePage.communicationPreferencesTabHeader()
        );
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.preferencesTitles().first(),
            timeouts.T30,
            'I should see notifications tab active'
        );
        const communicationsHeader = await pages.erp.employeeProfilePage.communicationPreferencesTabHeader().getText();
        const preferencesCards = await actions.erp.employeeProfileActions.getPreferencesCardsTitles();
        expect(preferencesHeader).to.include(communicationsHeader);
        expect(preferencesCards[0]).to.include(phoneCalls);
        expect(preferencesCards[1]).to.include(`${alertUpdates}, ${reminders}`);
        expect(preferencesCards[2]).to.include(writtenCorrespondence);
    }
);

When(/^I open "(Personal Details|Employment Details|Communication Preferences)" tab$/, async tabName => {
    const tab = tabName.trim().replace(/\s+/, '_').toUpperCase();
    await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.employeeTab(tab));
    await pages.erp.employeeProfilePage.employeeTab(tab).Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
});

When(/^I click the "Edit" link under the "Current occupation" section$/, async () => {
    await pages.erp.employeeProfilePage.editCurrentOccupationButton().Click({waitForVisibility: true});
});

Then(/^the Edit "(?:[^"]*)" popup opens$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.editCurrentOccupationTitle(),
        timeouts.T30,
        'Edit current occupation popup opens'
    );
});

Then(/^I switch "([^"]*)" add adjusted date of hire$/, async toggle => {
    const status = await pages.erp.employeeProfilePage.toggleAddDateHire().getAttribute('aria-checked');
    if (((toggle === 'on') && (status !== 'true')) || ((toggle === 'off') && (status === 'true'))) {
        await pages.erp.employeeProfilePage.toggleAddDateHire().Click({waitForVisibility: true});
    }
});

Then(/^I "(can|canNot)" see "(?:[^"]*)" field$/, async field => {
    if (field === 'can') {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.adjustedDateOfHireField(),
            timeouts.T30,
            'Able to enter Adjusted date of Hire'
        );
    } else {
        await waitHelpers.waitForElementToBeNotVisible(
            pages.erp.employeeProfilePage.adjustedDateOfHireField(),
            timeouts.T30,
            'Unable to enter Adjusted date of Hire'
        );
    }
});

When(/^I enter a future date after today to "(?:[^"]*)" field$/, async () => {
    const finalDate = moment().add(1, 'months').format(constants.dateFormats.shortDate);
    await pages.erp.employeeProfilePage.datePickerAdjustedDate().Click({waitForVisibility: true});
    await pages.erp.employeeProfilePage.datePickerAdjustedDate().SetText(finalDate + protractor.Key.ENTER);
});

Then(/^I see the message Adjusted date of hire "(afterToday|afterJobEndDate)" warning$/, async warningMessage => {
    if (warningMessage === 'afterToday') {
        const labelToday = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.adjustedDateAfterTodayWarning());
        await expectHelpers.checkTranslation(labelToday, 'ADJUSTED_DATE_OF_HIRE_AFTER_TODAY_WARNING');
    } else {
        const labelJobEndDate = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.adjustedDateAfterJobEndDateWarning());
        await expectHelpers.checkTranslation(labelJobEndDate, 'JOB_END_DATE_AFTER_ADJUSTED_DATE_WARNING');
    }
});

Then(/^I enter a date after the "(?:[^"]*)" to "(?:[^"]*)" field$/, async () => {
    const referenceDate = await pages.erp.employeeProfilePage.jobEndDate().getAttribute('value');
    const finalAdjustedDate = moment(referenceDate).add(1, 'years').format(constants.dateFormats.shortDate);
    await pages.erp.employeeProfilePage.datePickerAdjustedDate().SetText(finalAdjustedDate + protractor.Key.ENTER);
});

When(/^I hover a help icon next to "([^"]*)" field$/, async jobDate => {
    if (jobDate === 'Job start date') {
        await pages.erp.employeeProfilePage.tooltipJobDates().get(0).Click({waitForVisibility: true});
    } else if (jobDate === 'Job end date') {
        await pages.erp.employeeProfilePage.tooltipJobDates().get(1).Click({waitForVisibility: true});
    } else {
        await pages.erp.employeeProfilePage.tooltipJobDates().get(2).Click({waitForVisibility: true});
    }
});

Then(
    /^I see a popover with customizable text from the theme configurator with information regarding "(Job start date|Job end date|Adjusted date of hire)"$/,
    async information => {
        if (information === 'Job start date') {
            const infoStartDate = await waitHelpers.waitForElementToBeVisible(
                pages.erp.employeeProfilePage.infoTooltipJobStartDate(),
                timeouts.T30,
                'Tooltip information about start date of hire'
            );
            await expectHelpers.checkTranslation(infoStartDate, 'EDIT_EMPLOYMENT_DETAILS.JOB_START_DATE.POPOVER');
        } else if (information === 'Job end date') {
            const infoEndDate = await waitHelpers.waitForElementToBeVisible(
                pages.erp.employeeProfilePage.infoTooltipJobEndDate(),
                timeouts.T30,
                'Tooltip information about start date of hire'
            );
            await expectHelpers.checkTranslation(infoEndDate, 'EDIT_EMPLOYMENT_DETAILS.JOB_END_DATE.POPOVER');
        } else {
            const adjustedDateHire = await waitHelpers.waitForElementToBeVisible(
                pages.erp.employeeProfilePage.infoTooltipAdjustedDateHire(),
                timeouts.T30,
                'Tooltip information about start date of hire'
            );
            await expectHelpers.checkTranslation(
                adjustedDateHire,
                'EDIT_EMPLOYMENT_DETAILS.ADJUSTED_HIRE_DATE.POPOVER'
            );
        }
    }
);

Then(
    /^the preferred contact phone number is (not set|set) in "Phone calls" section with correct text (?:and format)?$/,
    async expectedState => {
        if (expectedState === 'set') {
            let actualPhoneCallsSectionTxt = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.communicationPreferencesPhones());
            actualPhoneCallsSectionTxt = await actualPhoneCallsSectionTxt.getText();

            let expectedPhoneCallsSectionRegexp = await translationsHelper.mapTranslationVariables(
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.PHONE',
                ['(\\d+-)?\\d+-\\d+']
            );
            expectedPhoneCallsSectionRegexp = new RegExp(expectedPhoneCallsSectionRegexp.toString());

            expect(actualPhoneCallsSectionTxt).to.match(expectedPhoneCallsSectionRegexp);
        } else {
            const actualPhoneCallsSectionTxt = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.communicationPreferencesPhonesEmpty());
            await expectHelpers.checkTranslation(
                actualPhoneCallsSectionTxt,
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.EMPTY'
            );
        }
    }
);

Then(
    /^the preferred contact (via email||via SMS|via email and SMS) is (not set|set) in "Alerts" section with correct text (?:and format)?$/,
    async (channel, expectedState) => {
        const phoneNumberTemplate = '(\\d+-)?\\d+-\\d+';
        const emailTemplate = '[\\w!#$%&\'*+\\/=?`{|}~^-]+(?:\\.[\\w!#$%&\'*+\\/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}';

        if (expectedState === 'set') {
            let translationVariables;
            let actualAlertsSectionTxt = [];
            let expectedAlertsRegexp = '';

            await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.alertsAndUpdatesTxt().first());
            await pages.erp.communicationPreferencesPage.alertsAndUpdatesTxt().each(async message => {
                actualAlertsSectionTxt.push(await message.getText());
                expectedAlertsRegexp += translationsHelper.getTranslation(
                    'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.MESSAGE');
            });
            actualAlertsSectionTxt = actualAlertsSectionTxt.join('');

            if (channel === 'via email') {
                translationVariables = ['email', emailTemplate];
            } else if (channel === 'via SMS') {
                translationVariables = ['SMS', phoneNumberTemplate];
            } else {
                translationVariables = ['email', emailTemplate, 'SMS', phoneNumberTemplate];
            }

            for (const variable of translationVariables) {
                expectedAlertsRegexp = expectedAlertsRegexp.replace(constants.regExps.wordsPlaceholders, variable);
            }

            expect(actualAlertsSectionTxt).to.match(new RegExp(expectedAlertsRegexp.toString()));
        } else {
            const actualPhoneCallsSectionTxt = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.communicationPreferencesPhonesEmpty());
            await expectHelpers.checkTranslation(
                actualPhoneCallsSectionTxt,
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.EMPTY'
            );
        }
    }
);

Then(
    /^the "Written correspondence" section subscribed "(via post|via email|via SMS)" has correct text$/,
    async channel => {
        const phoneNumberTemplate = '(\\d+-)?\\d+-\\d+';
        const emailTemplate = '[\\w!#$%&\'*+\\/=?`{|}~^-]+(?:\\.[\\w!#$%&\'*+\\/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}';
        let actualWrittenCorrespondenceSection = '';
        let expectedWrittenCorrespondenceSectionTxt = '';

        if (channel === 'via post') {
            actualWrittenCorrespondenceSection = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.writtenCorrespondenceEmpty());
            expectedWrittenCorrespondenceSectionTxt = translationsHelper.mapTranslationVariables(
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.EMPTY',
                ['paper', channel]
            );
        } else if (channel === 'via email') {
            actualWrittenCorrespondenceSection = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.writtenCorrespondenceEmail());
            expectedWrittenCorrespondenceSectionTxt = translationsHelper.mapTranslationVariables(
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.NOTIFIED_BY',
                ['email', emailTemplate]
            );
        } else if (channel === 'via SMS') {
            actualWrittenCorrespondenceSection = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.writtenCorrespondencePhone());
            expectedWrittenCorrespondenceSectionTxt = translationsHelper.mapTranslationVariables(
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.NOTIFIED_BY',
                ['SMS', phoneNumberTemplate]
            );
        }
        expect(await actualWrittenCorrespondenceSection.getText()).to.match(new RegExp(
            expectedWrittenCorrespondenceSectionTxt.toString()));
    }
);

Then(
    /^I can see correct message for "(empty|fixed|rotating|variable) work pattern" in "Work pattern" section$/,
    async workPatternType => {
        workPatternType = workPatternType.toString().toUpperCase();

        if (workPatternType === 'VARIABLE') {
            workPatternType = `WORK_PATTERN.${workPatternType}_WORK_PATTERN`;
        } else if (workPatternType === 'EMPTY') {
            workPatternType = 'WORK_PATTERN.EMPTY';
        } else {
            workPatternType = `WORK_PATTERN.EMPLOYEE_WORK_PATTERN_${workPatternType}`;
        }

        const expectedMessage = translationsHelper.getTranslation(workPatternType);
        const actualWorkPattern = await pages.erp.employeeProfilePage.workPatternCard().getText();

        expect(actualWorkPattern).contains(expectedMessage);
    }
);

Then(
    /^I can see the "Work pattern" section with "(1|2|3|4)" row(?:s)? of a week(?: each)? with row title "Week " plus row number$/,
    async expectedNumberOfWeeks => {
        const actualWeeksLabels = await pages.erp.employeeProfilePage.workPatternWeeksLabels();
        expect(actualWeeksLabels.length).equals(Number.parseInt(expectedNumberOfWeeks, 10));

        await pages.erp.employeeProfilePage.workPatternWeeksLabels().each(async (wL, wI) => {
            const expectedLabel = translationsHelper.mapTranslationVariables(
                'WORK_PATTERN.ROTATING_WEEK_HEADER',
                [(++wI).toString()]
            );
            const actualLabel = await wL.getText();

            expect(expectedLabel).equals(actualLabel);
        });
    }
);

Then(/^I can see the "Work pattern" week without title "Week " plus row number$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.employeeProfilePage.workPatternWeeksLabels().first());
});

Then(
    /^I can see the "(1|2|3|4)" row(?:s)? of a week(?: each)? with following times "([^"]*)"$/,
    async (expectedNumberOfWeeks, expectedWeekTimes) => {
        const actualWeeksTimes = await pages.erp.employeeProfilePage.workPatternWeeksTimes();
        expect(actualWeeksTimes.length).equals(Number.parseInt(expectedNumberOfWeeks, 10));

        await pages.erp.employeeProfilePage.workPatternWeeksTimes().each(async w => {
            const actualWeekTimes = (await w.getText()).toString().replace(constants.regExps.newLine, ' ');
            expect(expectedWeekTimes).equals(actualWeekTimes);
        });
    }
);

Then(
    /^I can see the "Work pattern" section with "(1|2|3|4)" row(?:s)? of a week(?: each)? with columns "Mon, Tue, Wed, Thu, Fri, Sat, Sun"$/,
    async expectedNumberOfWeeks => {
        const days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
        const expectedTitle = days.map(d => translationsHelper.getTranslation(`WORK_PATTERN.WEEK_DAYS_SHORTCUT.${d}`)).join(
            ' ');
        const actualWeeks = await pages.erp.employeeProfilePage.workPatternWeeks();
        expect(actualWeeks.length).equals(Number.parseInt(expectedNumberOfWeeks, 10));

        await pages.erp.employeeProfilePage.workPatternWeeksHeaders().each(async w => {
            expect(expectedTitle).equals(await w.getText());
        });
    }
);

When(
    /^I edit "(Current occupation|Eligibility criteria|Work pattern)" card in 'Employment Details'$/,
    async cardName => {
        switch (cardName.toString().toLowerCase()) {
            case 'current occupation':
                await pages.erp.employeeProfilePage.editCurrentOccupationButton().Click({waitForVisibility: true});
                break;
            case 'eligibility criteria':
                await pages.erp.employeeProfilePage.editEligibilityCriteriaButton().Click({waitForVisibility: true});
                break;
            case 'work pattern':
                await waitHelpers.waitForElementToBeVisible(await pages.erp.employeeProfilePage.editWorkPatternButton());
                testVars.employmentDetails.historicalWorkPattern = (await pages.erp.employeeProfilePage.workPatternCard().getText()).toString();

                await pages.erp.employeeProfilePage.editWorkPatternButton().Click({waitForVisibility: true});
                break;
            default:
                throw new Error(`given card name [${cardName}] is not supported`);
        }
    }
);

When(/^I add "Work pattern" card in 'Employment Details'$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.employeeProfilePage.addWorkPatternButton());
    testVars.employmentDetails.historicalWorkPattern = (await pages.erp.employeeProfilePage.workPatternCard().getText()).toString();

    await pages.erp.employeeProfilePage.addWorkPatternButton().Click({waitForVisibility: true});
});

Then(/^I verify that "Employment Details" section is shown$/, async () => {
    await expectHelpers.checkTranslation(
        pages.erp.employeeProfilePage.employeeIdLabel(),
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EMPLOYEE_ID'
    );
});

Then(
    /^the Employment Details "(Current occupation|Eligibility criteria|Work pattern)" card should "(have|not have)" following fields "([^"]*)"$/,
    async (cardName, isRequired, expectedFieldsList) => {
        const expectedFields = expectedFieldsList.split(',').map(item => item.trim().toLowerCase());
        const actualFieldsList = await actions.erp.employeeProfileActions.getEmploymentDetailsCardFields(cardName);

        if (constants.stepWording.HAVE === isRequired) {
            expectedFields.forEach(field => expect(actualFieldsList.join(',').toLowerCase()).to.include(field));
        } else {
            expectedFields.forEach(field => expect(actualFieldsList.join(',').toLowerCase()).to.not.include(field));
        }
    }
);

Then(/^I can see the Employment Details tab is updated with changes to the work pattern section$/, async () => {
    let expectedWorkingHours = testVars.workPattern.editedWorkingHours.replace(/00:00/ig, '–').split(',');
    expectedWorkingHours = actions.erp.editWorkPatternActions.convertWorkingTime(expectedWorkingHours);

    let actualWorkPatternData = (await pages.erp.employeeProfilePage.workPatternCard().getText()).toString();
    actualWorkPatternData = actualWorkPatternData.replace(constants.regExps.newLine, ',');
    testDataHelpers.chunkArray(expectedWorkingHours, 7).forEach(weekDays => {
        expect(actualWorkPatternData.toString().replace(/–/g, '').trim()).contains(weekDays.toString().replace(
            /–/g,
            ''
        ).trim());
    });
    expect(expectedWorkingHours).is.not.empty;
});

Then(/^I can see the "Work pattern" section is not updated$/, async () => {
    const expectedWorkPatternData = testVars.employmentDetails.historicalWorkPattern;
    const actualWorkPatternData = (await pages.erp.employeeProfilePage.workPatternCard().getText()).toString();

    expect(expectedWorkPatternData).is.equal(actualWorkPatternData);
});

Then(
    /^the "(Address)" of employee "(from USA|not from USA)" should be presented in the following format "(?:[^"]*)"$/,
    async (fieldTxt, employeeOrigin) => {
        const actualData = await pages.erp.employeeProfilePage.employmentDetailsField(fieldTxt.toLowerCase()).getText();
        employeeOrigin = (employeeOrigin === 'from USA') ? 'us' : 'notUs';
        expect(actualData).to.match(constants.regExps.address[employeeOrigin]);
    }
);

Then(/^the "(Address)" do not have blank lines$/, async fieldTxt => {
    const actualData = await pages.erp.employeeProfilePage.employmentDetailsField(fieldTxt.toLowerCase()).getText();
    expect(actualData).to.not.match(constants.regExps.blankLine);
});

Then(/^the "([^"]*)" data should be presented in format: "(?:[^"]*)"$/, async fieldTxt => {
    const actualData = await actions.erp.employeeProfileActions.getEmploymentDetailsElementsText(fieldTxt);
    const expectedData = await actions.erp.employeeProfileActions.getTestDataInCorrectFormat(fieldTxt);
    expect(actualData).to.be.equal(expectedData);
});

When(/^I click the "Edit" link under the "Name, Date of Birth, Address" section$/, async () => {
    await pages.erp.employeeProfilePage.editPersonalDetailsButton().Click({waitForVisibility: true});
});

When(/^I click the "(Add|Edit)" link under the "Emails" section$/, async action => {
    if (action === 'Edit') {
        await pages.erp.employeeProfilePage.editEmailsButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.employeeProfilePage.addEmailsButton().Click({waitForVisibility: true});
    }
});

When(/^I click the "(Add|Edit)" link under the "Phone Numbers" section$/, async action => {
    if (action === 'Edit') {
        await pages.erp.employeeProfilePage.editPhoneNumbersButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.employeeProfilePage.addPhoneNumbersButton().Click({waitForVisibility: true});
    }
});

Then(
    /^I see that generated "([^"]*)" data is saved in the 'Personal Details' tab in the "([^"]*)" field$/,
    async (expectedTextPattern, fieldName) => {
        let expectedTestData = '';
        expectedTextPattern.split(',').forEach(item => {
            item = item.trim().replace(/\s+/g, '').toLocaleLowerCase();
            expectedTestData += `${testVars.employeePersonalDetails[item]} `;
        });

        await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.personalDetailsField(fieldName));
        const obtainedText = await pages.erp.employeeProfilePage.personalDetailsField(fieldName).getText();
        expect(testDataHelpers.replaceCharsInString(
            obtainedText,
            constants.regExps.newLine,
            ' '
        )).contains(expectedTestData.trim());
    }
);

Then(
    /^I see that "([^"]*)" data is saved in the 'Personal Details' tab in the "([^"]*)" field$/,
    async (expectedFieldValue, fieldName) => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.personalDetailsField(fieldName));
        const obtainedText = await pages.erp.employeeProfilePage.personalDetailsField(fieldName).getText();
        expect(testDataHelpers.replaceCharsInString(
            obtainedText,
            constants.regExps.newLine,
            ' '
        )).contains(expectedFieldValue.trim());
    }
);

Then(
    /^I see the deleted email address is removed from the list of "(Email\(s\))" on the 'Personal Details' tab$/,
    async fieldName => {
        const expectedEmails = testVars.employeePersonalDetails.finalEmails;
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.personalDetailsField(fieldName));
        const actualEmails = await pages.erp.employeeProfilePage.personalDetailsField(fieldName).getText();

        expect(testDataHelpers.replaceCharsInString(
            actualEmails,
            constants.regExps.newLine,
            ','
        )).contains(expectedEmails);
    }
);

Then(/^I see 'Personal details' data in edit mode is the same like in display mode$/, async () => {
    const personalProfileData = await pages.erp.employeeProfilePage.personalDetailsData().getText();
    testVars.employeePersonalDetails.historicalData = personalProfileData;
    const personalDetailsEditFields = await pages.erp.employeePersonalDetailsPage.editPersonalDetailsFields();
    const personalDetailsEditData = [];

    await waitHelpers.waitForElementToBeVisible(await pages.erp.employeePersonalDetailsPage.selectArrow('country'));

    for (const editField of personalDetailsEditFields) {
        let dataValue = await editField.getAttribute('value');
        if (dataValue !== '') {
            if (dataValue === null) {
                dataValue = await editField.getText();
            }
            personalDetailsEditData.push(dataValue);
        }
    }
    expect(personalDetailsEditData.length).is.greaterThan(5);

    personalDetailsEditData.forEach(item => {
        expect(personalProfileData.toString()).contains(item.trim());
    });
});

Given(/^I open Communication Preferences of the same employee$/, async () => {
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.activeEmployeeDetailsTab(),
        timeouts.T15,
        'I should see notifications tab active'
    );
    await pages.erp.employeeProfilePage.communicationDetailsTab().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(
        pages.erp.employeeProfilePage.activePreferencesTab(),
        timeouts.T15,
        'Is communication preferences tab active'
    );
    await waitHelpers.waitForSpinnersToDisappear();
});

Given(/^I record the Phone Numbers field value on the Personal Details tab for comparison$/, async () => {
    testVars.searchTarget.personalDetailsTabPhones = await pages.erp.employeeProfilePage.personalDetailsPhones().getText();
});

Given(/^I record the phone number in the "([^"]*)" section for comparison$/, async tst => {
    testVars.searchTarget.communicationTabPhones = await pages.erp.communicationPreferencesPage.communicationPreferencesPhones().getText();
});

When(/^I click the "(Add phone number|Edit preference)" link in the Phone calls section$/, async linkName => {
    if (linkName === 'Edit preference') {
        await pages.erp.communicationPreferencesPage.phoneCallsEditLink().Click({waitForVisibility: true});
    } else {
        await pages.erp.communicationPreferencesPage.phoneCallsAddLink().Click({waitForVisibility: true});
    }
});

When(/^I set random "Phone number" in the Phone calls section if not set$/, async () => {
    let actualLink = await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.phoneCallsLink());
    actualLink = await actualLink.getAttribute('data-test-el');

    if (actualLink.toString().toLowerCase().includes('add')) {
        await pages.erp.communicationPreferencesPage.phoneCallsAddLink().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(pages.erp.communicationPreferencesPage.availablePhoneNumbers().first());

        const availablePhoneNumbers = await pages.erp.communicationPreferencesPage.availablePhoneNumbers();
        await testDataHelpers.getRandomArrayElement(availablePhoneNumbers).Click({waitForVisibility: true});
        await pages.erp.sharedModal.okButton().last().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeNotVisible(await pages.erp.addNewPhoneNumberModal.modalHeader());
    }
});

Then(/^the "(Phone calls|Alerts and updates|Written correspondence)" popup opens$/, async modalName => {
    let title;
    let translation;

    if (modalName === 'Phone calls') {
        title = await waitHelpers.waitForElementToBeVisible(await pages.erp.phoneCallsModal.modalHeader());
        translation = 'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.TITLE';
        await expectHelpers.checkTranslation(title, 'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.TITLE');
    } else if (modalName === 'Alerts and updates') {
        title = await waitHelpers.waitForElementToBeVisible(await pages.erp.alertsAndUpdatesModal.modalHeader());
        translation = 'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.TITLE';
    } else if (modalName === 'Written correspondence') {
        title = await waitHelpers.waitForElementToBeVisible(await pages.erp.writtenCorrespondenceModal.modalHeader());
        translation = 'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.TITLE';
    }

    await expectHelpers.checkTranslation(title, translation);
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.okButton().first());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.cancelButton());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.closeXButton());
});

Then(/^the Phone Calls popup should contain the "([^"]*)" link$/, async linkType => {
    if (linkType.toString().toLowerCase().includes('email')) {
        await expectHelpers.checkTranslation(
            await pages.erp.sharedModal.useDifferentEmailButton(),
            'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON'
        );
    } else {
        await expectHelpers.checkTranslation(
            await pages.erp.sharedModal.useDifferentPhoneNumberButton(),
            'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON'
        );
    }
});

Then(/^the Phone Calls popup should contain the label "([^"]*)" on the list of phone numbers$/, async title => {
    await pages.erp.phoneCallsModal.selectPhoneNumberModalHeader();
    expect(await pages.erp.phoneCallsModal.selectPhoneNumberModalHeader().getText()).contain(title);
});

Then(/^the Phone Calls popup should contain the buttons "([^"]*)"$/, async modalButtons => {
    const buttons = modalButtons.split(',');
    expect(await pages.erp.sharedModal.okButton().getText()).to.contain(buttons[0].trim());
    expect(await pages.erp.sharedModal.cancelButton().getText()).to.contain(buttons[1].trim());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.closeXButton());
});

Then(
    /^the Phone Calls popup should contain list of radio buttons with phone numbers in format "\[International Code\]-\[Area Code\]-\[Telephone Number\]"$/,
    async () => {
        await waitHelpers.waitForSpinnersToDisappear();
        await waitHelpers.waitForElementToBeVisible(await pages.erp.phoneCallsModal.registeredPhoneNumbers().first());
        const phones1 = await pages.erp.phoneCallsModal.registeredPhoneNumbers().first().getText();
        const phones2 = phones1.split('-');
        expect(phones2.length).to.equal(3);
    }
);

Then(/^the Phone Calls popup should contain the title "(?:[^"]*)"$/, async () => {
    const actualTitle = await pages.erp.phoneCallsModal.modalHeader();
    await expectHelpers.checkTranslation(actualTitle, 'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.TITLE');
});

Then(
    /^the Phone Calls popup should show (selected|not selected) phone number in the list of phone numbers$/,
    async selected => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.phoneCallsModal.radioButtonPhoneList().first());

        if (selected === 'selected') {
            await pages.erp.phoneCallsModal.radioButtonPhoneList().each(async phoneNumber => {
                expect(await phoneNumber.getAttribute('data-test-el')).contain('selected');
            });
        } else {
            await pages.erp.phoneCallsModal.radioButtonPhoneList().each(async phoneNumber => {
                expect(await phoneNumber.getAttribute('data-test-el')).not.contain('selected');
            });
        }
    }
);

Then(
    /^the Phone Calls popup should contain the recorded phone numbers of the personal details in the list of phone numbers$/,
    async () => {
        if (await pages.erp.phoneCallsModal.radioButtonPhoneList().count() === 0) {
            throw new Error('Radio button list of phone numbers is empty');
        } else {
            await pages.erp.phoneCallsModal.radioButtonPhoneList().each(async radioBtn => {
                const elTxt = await radioBtn.getText();
                expect(testVars.searchTarget.personalDetailsTabPhones).contains(elTxt.toString());
            });
        }
    }
);

Then(
    /^the Add new phone number popup opens with elements "Add new phone number title, Phone Contact Type, Country code, Area code, Phone number, OK, Cancel, Close X"$/,
    async () => {
        const title = await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.modalHeader());
        await expectHelpers.checkTranslation(
            title,
            'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.ADD_PHONE_NUMBER'
        );

        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.mandatoryFieldsLabel());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.modalSubtitle());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.phoneTypeDropdown());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.countryCodeLabel());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.countryCodeInput());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.areaCodeLabel());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.areaCodeInput());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.phoneNumberLabel());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.phoneNumberInput());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.okButton());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.cancelButton());
        await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.closeXButton());
    }
);

Then(
    /^Add new phone number popup labels "(have|do not have)" "(\*)" on the field "(Country code|Area code|Phone no)"$/,
    async (isRequired, mandatoryCharacter, label) => {
        label = label.replace(constants.regExps.spaces, '_').toUpperCase();

        let expectedLabel = await translationsHelper.getTranslation(`COMMON.PHONE.${label}`);

        if (isRequired === constants.stepWording.HAVE) {
            expectedLabel += mandatoryCharacter;
        }

        const actualLabel = await pages.erp.addNewPhoneNumberModal.formLabel(label).getText();
        expect(expectedLabel).is.equal(actualLabel);
    }
);

Then(/^the "Add new email" popup opens$/, async () => {
    const title = await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewEmailModal.modalHeader());
    await expectHelpers.checkTranslation(title, 'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.TITLE');

    await expectHelpers.expectAllElementsToBeVisible([
        await pages.erp.addNewEmailModal.mandatoryFieldsLabel(),
        await pages.erp.addNewEmailModal.okButton(),
        await pages.erp.addNewEmailModal.cancelButton(),
        await pages.erp.addNewEmailModal.closeXButton(),
        await pages.erp.addNewEmailModal.emailAddressInput(),
        await pages.erp.addNewEmailModal.emailAddressLabel()
    ]);
});

Then(/^the "Add new email" popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.addNewEmailModal.modalHeader());
});

When(/^I "(accept|cancel|close X)" "Add new email" popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.addNewEmailModal.okButton().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.addNewEmailModal.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.addNewEmailModal.closeXButton().Click({waitForVisibility: true});
    }
});

When(/^I enter (random|[\w\d\W]+) number in the "([^"]*)" field$/, async (dataType, numberType) => {
    if (dataType === 'random') {
        const data = numberType.trim().replace(/\s+/g, '').toLowerCase();
        testVars.employeePersonalDetails[numberType] = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
            data);
    } else {
        testVars.employeePersonalDetails[numberType] = dataType;
    }

    switch (numberType.toString().toLowerCase()) {
        case 'country code':
            await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.countryCodeLabel());
            await pages.erp.addNewPhoneNumberModal.countryCodeInput().SetText(testVars.employeePersonalDetails[numberType]);
            break;
        case 'area code':
            await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.areaCodeLabel());
            await pages.erp.addNewPhoneNumberModal.areaCodeInput().SetText(testVars.employeePersonalDetails[numberType]);
            break;
        case 'phone number':
            await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.phoneNumberLabel());
            await pages.erp.addNewPhoneNumberModal.phoneNumberInput().SetText(testVars.employeePersonalDetails[numberType]);
            break;
        default:
            throw new Error('Incorrect data provided');
    }
});

When(/^I click the OK button$/, async () => {
    await pages.erp.addNewPhoneNumberModal.okButton().Click({waitForVisibility: true});
});

Then(/^the Add new phone number popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.addNewPhoneNumberModal.modalHeader());
});

Given(/^I record the list of phone numbers for comparison$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(await pages.erp.phoneCallsModal.registeredPhoneNumbers().first());
    testVars.searchTarget.phoneCallsModal = await pages.erp.phoneCallsModal.registeredPhoneNumbers().getText();
});

Given(/^I record the list of emails for comparison$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(await pages.erp.writtenCorrespondenceModal.registeredEmails().first());
    testVars.searchTarget.emails = await pages.erp.writtenCorrespondenceModal.registeredEmails().getText();
});

Given(
    /^I see the phone number "([^"]*)" is selected and added to the recorded list of phone numbers in the Phone Calls popup$/,
    async dataType => {
        const data = dataType.split(',');
        await waitHelpers.waitForElementToBeVisible(await pages.erp.phoneCallsModal.registeredPhoneNumbers().first());
        for (let i = 0; i < data.length; i++) {
            expect(await pages.erp.phoneCallsModal.registeredPhoneNumbers().first().getText()).to.contain(testVars.employeePersonalDetails[data[i]]);
        }
    }
);

When(/^I select (Landline|Cell Phone) in the Phone Contact Type field$/, async phoneNumberType => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.dropDownArrow());
    await waitHelpers.waitForElementToBeStatic(await pages.erp.addNewPhoneNumberModal.dropDownArrow());
    await pages.erp.addNewPhoneNumberModal.phoneTypeDropdown().Click({waitForVisibility: true});
    if (phoneNumberType === 'Landline') {
        await pages.erp.addNewPhoneNumberModal.landLineOption().Click({waitForVisibility: true});
    } else {
        await pages.erp.addNewPhoneNumberModal.cellPhoneOption().Click({waitForVisibility: true});
    }
});

Then(/^I see "(Landline|Cell Phone)" option is selected in Phone Contact Type field$/, async phoneNumberType => {
    phoneNumberType = (phoneNumberType === 'Landline') ? 'LANDLINE' : 'CELL_PHONE';

    await waitHelpers.waitForElementToBeVisible(await pages.erp.addNewPhoneNumberModal.dropDownArrow());
    await waitHelpers.waitForElementToBeStatic(await pages.erp.addNewPhoneNumberModal.dropDownArrow());

    await expectHelpers.checkTranslation(
        await pages.erp.addNewPhoneNumberModal.phoneTypeDropdown(),
        `PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.${phoneNumberType}`
    );
});

Then(/^I see Phone Contact Type select is "(disabled|enabled)"$/, async isEnabled => {
    await pages.erp.addNewPhoneNumberModal.phoneTypeDropdown().Click({waitForVisibility: true});

    if (isEnabled === 'enabled') {
        await waitHelpers.waitForElementToBeVisible(pages.erp.addNewPhoneNumberModal.phoneTypeOptions().first());
        await pages.erp.addNewPhoneNumberModal.phoneTypeDropdown().Click({waitForVisibility: true});
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.addNewPhoneNumberModal.phoneTypeOptions().first());
    }
});

Then(/^the Alerts and updates popup should contain the buttons "([^"]*)"$/, async modalButtons => {
    const buttons = modalButtons.split(',');
    expect(await pages.erp.sharedModal.okButton().getText()).to.contain(buttons[0].trim());
    expect(await pages.erp.sharedModal.cancelButton().getText()).to.contain(buttons[1].trim());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.closeXButton());
});

Given(/^I open personal details of employee "([^"]*)"(?: and )?(?:[^"]*)$/, async descriptor => {
    await actions.erp.searchActions.openEmployeeDetailsPage(descriptor, 'EmployeeId');
});

Given(
    /^I record the list of email address for comparison of the Emails section of the personal details tab$/,
    async () => {
        testVars.searchTarget.emails = await pages.erp.employeeProfilePage.personalDetailsEmail().getText();
    }
);

Given(
    /^I record the list of phone numbers for comparison of the Phone numbers section of the personal details tab$/,
    async () => {
        testVars.searchTarget.phoneNumbers = await pages.erp.employeeProfilePage.personalDetailsPhones().getText();
    }
);

Given(
    /^I record the list of (Cell Phone|Landline) phone numbers for comparison of the Phone numbers section of the personal details tab$/,
    async phoneType => {
        const numbers = [];

        phoneType = translationsHelper.getTranslation(`PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.${phoneType.toString().replace(
            constants.regExps.spaces,
            '_'
        ).toUpperCase()}`);

        await waitHelpers.waitForElementToBeVisible(await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().first());
        await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().each(async phoneCard => {
            if ((await phoneCard.getText()).toString().includes(phoneType)) {
                const number = (await pages.erp.employeePhoneNumbersPage.numberInputs(phoneCard).getAttribute('value')).filter(
                    Boolean).join('-');
                numbers.push(number);
            }
        });

        phoneType.toString().toLowerCase() === 'landline' ? testVars.searchTarget.landlinePhoneNumbers = numbers : testVars.searchTarget.cellPhoneNumbers = numbers;
    }
);

Then(/^I can see the list of "(email addresses|phone numbers)" registered for my employee$/, async contactType => {
    const actualList = (contactType === 'email addresses') ? await pages.erp.employeeProfilePage.personalDetailsEmail().getText()
        : await pages.erp.employeeProfilePage.personalDetailsPhones().getText();

    expect(actualList.trim()).is.not.empty;
});

Then(/^each number should be presented in following format "(?:[^"]*)"$/, async () => {
    const actualPhoneNumbers = await pages.erp.employeeProfilePage.personalDetailsPhones().getText();
    actualPhoneNumbers.toString().split(/\n/g).forEach(number => {
        expect(number.trim()).to.match(constants.regExps.phoneNumber);
    });
});

Then(
    /^the Edit emails should contain list of email addresses from the recorded values in editable "Email" fields$/,
    async () => {
        const recordedEmailAddress = testVars.searchTarget.emails.toString().replace(/(\r\n|\n|\r)/gm, ',');
        const actualEmailAddresses = await pages.erp.employeeEmailsPage.emailInputs().getAttribute('value');
        expect(recordedEmailAddress.toString()).is.equal(actualEmailAddresses.toString());
    }
);

When(
    /^I record the preferred (?:email address|phone number) in the Alerts and updates section for comparison$/,
    async () => {
        await waitHelpers.waitForSpinnersToDisappear();
        testVars.searchTarget.preferredNumberEmail = await pages.erp.communicationPreferencesPage.alertsAndUpdatesTxt().first().getText();
    }
);

When(/^I click the "(Edit preference)" link in Alerts, updates and reminders section$/, async linkName => {
    expect(await pages.erp.communicationPreferencesPage.alertsAndUpdatesEditLink().getText()).to.equal(
        linkName);
    await pages.erp.communicationPreferencesPage.alertsAndUpdatesEditLink().Click({waitForVisibility: true});
});

When(/^I click the "(They want to receive alerts)" link in Alerts, updates and reminders section$/, async linkName => {
    await pages.erp.communicationPreferencesPage.theyWantToReceiveAlertsLink().Click({waitForVisibility: true});
});

Then(
    /^the Alerts and updates popup should contain the selected checkbox "(Send them an email|Send them an SMS)"$/,
    async checkBoxLabel => {
        await waitHelpers.waitForSpinnersToDisappear();
        if (checkBoxLabel === 'Send them an email') {
            const emailLabel = await waitHelpers.waitForElementToBeVisible(await pages.erp.alertsAndUpdatesModal.sendThemEmailCheckBox());
            await expectHelpers.checkTranslation(
                emailLabel,
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_EMAIL'
            );
            expect(await pages.erp.alertsAndUpdatesModal.modalCheckBoxSelected().getAttribute('value')).to.contain(
                'EMAIL');
        } else if (checkBoxLabel === 'Send them an SMS') {
            const smsLabel = await waitHelpers.waitForElementToBeVisible(await pages.erp.alertsAndUpdatesModal.sendThemSmsCheckBox());
            await expectHelpers.checkTranslation(
                smsLabel,
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_SMS'
            );
            expect(await pages.erp.alertsAndUpdatesModal.modalCheckBoxSelected().getAttribute('value')).to.contain('SMS');
        }
    }
);

Then(
    /^I "(select|deselect)" the "(Send them an email|Send them an SMS)" checkbox in "Alerts and updates"$/,
    async (action, checkBoxLabel) => {
        await waitHelpers.waitForSpinnersToDisappear();

        const checkbox = (checkBoxLabel === 'Send them an email') ? await waitHelpers.waitForElementToBeVisible(pages.erp.alertsAndUpdatesModal.sendThemEmailCheckBox()) : await waitHelpers.waitForElementToBeVisible(
            pages.erp.alertsAndUpdatesModal.sendThemSmsCheckBox());
        const isCheckboxSelected = Boolean((await checkbox.getAttribute('class')).toString().includes('checked'));

        if ((action === 'select' && !isCheckboxSelected) || (action === 'deselect' && isCheckboxSelected)) {
            await checkbox.Click({waitForVisibility: true});
        }
    }
);

When(/^I select the "first" "(phone number|email address)" radio button$/, async channelType => {
    if (channelType === 'email address') {
        await pages.erp.alertsAndUpdatesModal.emailsList().first().Click({waitForVisibility: true});
    } else {
        await pages.erp.alertsAndUpdatesModal.phoneNumbersList().first().Click({waitForVisibility: true});
    }
});

Then(/^the list of "(phone number|email address)" is hidden$/, async channelType => {
    if (channelType === 'email address') {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.alertsAndUpdatesModal.emailsList().first());
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.alertsAndUpdatesModal.phoneNumbersList().first());
    }
});

Then(
    /^I select the "(Send them an email|Send them an SMS)" checkbox in Written correspondence$/,
    async checkBoxLabel => {
        await waitHelpers.waitForSpinnersToDisappear();

        if (checkBoxLabel === 'Send them an email') {
            await pages.erp.writtenCorrespondenceModal.sendThemAnEmailCheckbox().Click({waitForVisibility: true});
        } else if (checkBoxLabel === 'Send them an SMS') {
            await pages.erp.writtenCorrespondenceModal.sendThemAnSmsCheckbox().Click({waitForVisibility: true});
        }
    }
);

Then(
    /^I see the preferred "(email address|phone number)" is "(marked|not marked)" in "(?:Alerts and updates|Written correspondence)"$/,
    async (channelType, isMarked) => {
        if (isMarked === 'marked') {
            const preferredListElement = (channelType === 'email address') ? await pages.erp.alertsAndUpdatesModal.preferredEmail() : await pages.erp.alertsAndUpdatesModal.preferredPhoneNumber();
            const expectedPreferred = (channelType === 'email address') ? testVars.searchTarget.preferredEmail : testVars.searchTarget.preferredPhoneNumber;
            const actualPreferredEmail = await preferredListElement.getText();

            expect(expectedPreferred).contains(actualPreferredEmail);
        } else if (isMarked === 'not marked') {
            if (channelType === 'email address') {
                await waitHelpers.waitForElementToBeNotVisible(pages.erp.alertsAndUpdatesModal.preferredEmail());
            } else {
                await waitHelpers.waitForElementToBeNotVisible(pages.erp.alertsAndUpdatesModal.preferredPhoneNumber());
            }
        }
    }
);

Then(
    /^I see the the recorded list of "(emails|phone numbers)" from the personal details tab$/,
    async communicationType => {
        await waitHelpers.waitForSpinnersToDisappear();

        if (communicationType === 'emails') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.alertsAndUpdatesModal.emailsList().first());
            const actualEmails = (await pages.erp.alertsAndUpdatesModal.emailsList().getText()).toString();
            const expectedEmails = testVars.searchTarget.emails.replace(/\n+/gm, ',');

            expect(expectedEmails).is.equal(actualEmails);
        } else {
            await waitHelpers.waitForElementToBeVisible(pages.erp.alertsAndUpdatesModal.phoneNumbersList().first());
            const actualPhoneNumbers = (await pages.erp.alertsAndUpdatesModal.phoneNumbersList().getText()).toString();
            const expectedPhoneNumbers = testVars.searchTarget.personalDetailsTabPhones.replace(/\n+/gm, ',');

            expect(expectedPhoneNumbers).contains(actualPhoneNumbers);
        }
    }
);

Then(
    /^the Alerts and updates popup should contain the label "(?:[^"]*)" on the list of (emails|phone numbers)$/,
    async channel => {
        if (channel === 'emails') {
            const actualEmailLabel = await pages.erp.alertsAndUpdatesModal.selectEmailLabel().getText();
            const expectedEmailLabel = translationsHelper.mapTranslationVariables(
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.LABEL',
                ['Select']
            );

            expect(actualEmailLabel).is.equal(expectedEmailLabel);
        } else {
            const actualSmsLabel = await pages.erp.alertsAndUpdatesModal.selectPhoneNumberLabel().getText();
            const expectedSmsLabel = translationsHelper.mapTranslationVariables(
                'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL',
                ['Select']
            );

            expect(actualSmsLabel).is.equal(expectedSmsLabel);
        }
    }
);

Then(
    /^the Alerts and updates popup should contain the not selected checkbox "(Send them an email|Send them an SMS)"$/,
    async checkBoxLabel => {
        if (checkBoxLabel === 'Send them an email') {
            expect(await pages.erp.alertsAndUpdatesModal.sendThemEmailLabel().getText()).to.contain(checkBoxLabel);
            expect(await pages.erp.alertsAndUpdatesModal.modalCheckBoxNotSelected().getAttribute('value')).to.contain(
                'EMAIL');
        } else if (checkBoxLabel === 'Send them an SMS') {
            expect(await pages.erp.alertsAndUpdatesModal.modalCheckBoxNotSelected().getAttribute('value')).to.contain(
                'SMS');
            expect(await pages.erp.alertsAndUpdatesModal.sendThemSmsLabel().getText()).to.contain(checkBoxLabel);
        }
    }
);

Then(
    /^the Alerts and updates popup should contain the recorded list of emails(?: with the recorded selected preferred email address)?$/,
    async () => {
        expect(testVars.searchTarget.emails).to.contain(await pages.erp.alertsAndUpdatesModal.selectedEmail().getText());
    }
);

Then(
    /^the Alerts and updates popup should contain the recorded list of mobile phone numbers(?: with the recorded selected preferred mobile phone number)?$/,
    async () => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.alertsAndUpdatesModal.preferredPhoneNumber());
        expect(testVars.searchTarget.phoneNumbers).to.contain(await pages.erp.alertsAndUpdatesModal.preferredPhoneNumber().getText());
    }
);

Then(/^the Alerts and updates popup should not contain the recorded list of landline phone numbers$/, async () => {
    const actualPhoneNumbers = await pages.erp.alertsAndUpdatesModal.phoneNumbersList().getText();

    expect(actualPhoneNumbers).is.eql(testVars.searchTarget.cellPhoneNumbers);
    expect(actualPhoneNumbers).to.not.have.members(testVars.searchTarget.landlinePhoneNumbers);
});

Then(
    /^I see on the Alerts and updates popup "([^"]*)" added to the list of emails from the recorded values$/,
    async newEmailAddress => {
        const historicalEmails = testVars.searchTarget.emails.toString();
        const expectedEmailsList = `${newEmailAddress},${historicalEmails}`;
        const actualEmailsList = (await pages.erp.writtenCorrespondenceModal.registeredEmails().getText()).toString();

        expect(expectedEmailsList).to.be.equal(actualEmailsList);
    }
);

When(/^I set communication via "(email|post|sms)"$/, async communicationChannel => {
    const actualSettings = await pages.erp.communicationPreferencesPage.writtenCorrespondenceChannel().getText();

    if (!actualSettings.toString().toLowerCase().includes(communicationChannel)) {
        await pages.erp.communicationPreferencesPage.writtenCorrespondenceEditLink().Click({waitForVisibility: true});

        switch (communicationChannel) {
            case 'email':
            case 'sms':
                await pages.erp.writtenCorrespondenceModal.sendThemCorrespondenceViaPortalCheckbox().Click({waitForVisibility: true});

                if (communicationChannel === 'email') {
                    await pages.erp.writtenCorrespondenceModal.sendThemAnEmailCheckbox().Click({waitForVisibility: true});
                } else {
                    await pages.erp.writtenCorrespondenceModal.sendThemAnSmsCheckbox().Click({waitForVisibility: true});
                }
                break;
            case 'post':
                await pages.erp.writtenCorrespondenceModal.sendThemCorrespondenceViaPostCheckbox().Click({waitForVisibility: true});
                break;
            default:
                throw new Error(`given channel [${communicationChannel}] is not supported`);
        }

        await pages.erp.writtenCorrespondenceModal.okButton().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeNotVisible(await pages.erp.writtenCorrespondenceModal.okButton());
    }
});

When(/^I click the "Edit preference" link in Written Correspondence section$/, async () => {
    await expectHelpers.checkTranslation(
        await pages.erp.communicationPreferencesPage.writtenCorrespondenceEditLink(),
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT'
    );
    await pages.erp.communicationPreferencesPage.writtenCorrespondenceEditLink().Click({waitForVisibility: true});
});

Given(/^I record the preferred email address in the Written Correspondence for comparison$/, async () => {
    const prefEmail = (await pages.erp.communicationPreferencesPage.writtenCorrespondenceEmail().getText()).toString();
    testVars.searchTarget.preferredEmail = prefEmail.substring(prefEmail.lastIndexOf(' '), prefEmail.length - 1).trim();
});

Given(/^I open personal details of employee who set written correspondence option "([^"]*)"$/, async descriptor => {
    await actions.erp.searchActions.openEmployeeDetailsPage(descriptor, 'lastName');
});

Then(/^the Written Correspondence popup should contain the buttons "([^"]*)"$/, async modalButtons => {
    const buttons = modalButtons.split(',');
    expect(await pages.erp.sharedModal.okButton().getText()).to.contain(buttons[0].trim());
    expect(await pages.erp.sharedModal.cancelButton().getText()).to.contain(buttons[1].trim());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.closeXButton());
});

Then(
    /^the Written Correspondence popup should contain the "(selected|not selected)" radio button "(Send them an SMS|Send them paper correspondence via post|Send them an email|Send them correspondence only through the portal)"$/,
    async (expectedState, checkBoxLabel) => {
        let isChecked;
        await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.okButton().first());
        if (checkBoxLabel === 'Send them paper correspondence via post') {
            isChecked = await pages.erp.writtenCorrespondenceModal.sendThemCorrespondenceViaPostCheckbox().getAttribute(
                'class');
        } else if (checkBoxLabel === 'Send them an SMS') {
            isChecked = await pages.erp.writtenCorrespondenceModal.sendThemAnSmsCheckbox().getAttribute('class');
        } else if (checkBoxLabel === 'Send them an email') {
            isChecked = await pages.erp.writtenCorrespondenceModal.sendThemAnEmailCheckbox().getAttribute('class');
        } else if (checkBoxLabel === 'Send them correspondence only through the portal') {
            isChecked = await pages.erp.writtenCorrespondenceModal.sendThemCorrespondenceViaPortalCheckbox().getAttribute(
                'class');
        }

        if (expectedState === 'selected') {
            expect(isChecked.toString()).contains('checked');
        } else {
            expect(isChecked.toString()).not.contains('checked');
        }
    }
);

Then(
    /^I select the "(Send them correspondence only through the portal|Send them paper correspondence via post)" radio button$/,
    async checkBoxLabel => {
        await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.okButton().first());
        if (checkBoxLabel === 'Send them correspondence only through the portal') {
            await pages.erp.writtenCorrespondenceModal.sendThemCorrespondenceViaPortalCheckbox().Click({waitForVisibility: true});
        } else if (checkBoxLabel === 'Send them paper correspondence via post') {
            await pages.erp.writtenCorrespondenceModal.sendThemCorrespondenceViaPostCheckbox().Click({waitForVisibility: true});
        }
    }
);

When(/^I record the preferred phone number in the Written Correspondence for comparison$/, async () => {
    testVars.searchTarget.preferredPhoneNumber = await pages.erp.communicationPreferencesPage.writtenCorrespondencePhone().getText();
});

Then(/^the Written Correspondence popup should contain the recorded selected preferred phone number$/, async () => {
    expect(testVars.searchTarget.preferredPhoneNumber).to.contain(await pages.erp.writtenCorrespondenceModal.selectedPreferredNumber().getText());
});

Then(
    /^the Written Correspondence popup should contain all registered mobile phone numbers from the personal contact tab Phone numbers section$/,
    async () => {
        await pages.erp.writtenCorrespondenceModal.registeredMobilePhones().each(async phone => {
            const elTxt = await phone.getText();
            expect(testVars.searchTarget.personalDetailsTabPhones).contains(elTxt.toString());
        });
    }
);

Then(
    /^the Written Correspondence popup should contain no registered landline phone numbers from the personal contact tab Phone numbers section$/,
    async () => {
        const actualPhoneNumbers = await pages.erp.writtenCorrespondenceModal.registeredMobilePhones().getText();

        expect(actualPhoneNumbers).is.eql(testVars.searchTarget.cellPhoneNumbers);
        expect(actualPhoneNumbers).to.not.have.members(testVars.searchTarget.landlinePhoneNumbers);
    }
);

Then(/^the Written Correspondence popup should contain the recorded selected preferred email address$/, async () => {
    await pages.erp.writtenCorrespondenceModal.selectedPreferredEmail();
    expect(testVars.searchTarget.emails).to.contain(await pages.erp.writtenCorrespondenceModal.selectedPreferredEmail().getText());
});

Then(
    /^the Written Correspondence popup should contain all registered emails addresses from the personal contact tab Emails section$/,
    async () => {
        await pages.erp.writtenCorrespondenceModal.registeredEmails().each(async email => {
            const elTxt = await email.getText();
            expect(testVars.searchTarget.emails).contains(elTxt.toString());
        });
    }
);

Then(
    /^the Phone Calls popup should show the recorded phone number of the Phone Calls as selected in the list of phone numbers$/,
    async () => {
        expect(testVars.searchTarget.communicationTabPhones).to.contain(await pages.erp.phoneCallsModal.radioButtonPhoneList().first().getText());
    }
);

When(/^I click the Edit link$/, async () => {
    await pages.erp.employeeProfilePage.editCurrentOccupationButton().Click({waitForVisibility: true});
});

Then(/^I see on the Employment Details tab "([^"]*)" in the "([^"]*)" field$/, async (expectedValue, fieldName) => {
    await waitHelpers.waitForSpinnersToDisappear();
    const actualValue = await actions.erp.employeeProfileActions.getEmploymentDetailsElementsText(fieldName);
    await expect(actualValue).contains(expectedValue.trim());
});

Then(
    /^I see that generated "([^"]*)" data is saved in the 'Employment Details' tab in the "([^"]*)" field$/,
    async (expectedTextPattern, fieldName) => {
        let expectedTestData = testVars.employmentDetails[expectedTextPattern.trim().replace(
            constants.regExps.spaces, '').toLowerCase()];
        const actualTestData = await actions.erp.employeeProfileActions.getEmploymentDetailsElementsText(fieldName);

        if (expectedTextPattern.toString().toLowerCase() === 'earnings') {
            expectedTestData = testDataHelpers.formatCurrency(
                'en-US',
                'USD',
                Number.parseFloat(expectedTestData.toString().trim())
            );
        }
        await expect(actualTestData).contains(expectedTestData.toString().trim());
    }
);

Then(
    /^I see that "(Within FMLA radius|Works at home|Key employee)" checkbox is saved on the "Eligibility criteria" section$/,
    async checkboxName => {
        const expectedCheckboxState = testVars.employmentDetails[checkboxName.trim().replace(
            constants.regExps.spaces, '').toLowerCase()];
        const actualCheckboxText = await actions.erp.eligibilityCriteriaActions.getCheckboxElementByName(
            checkboxName,
            expectedCheckboxState
        ).getText();
        const expectedCheckboxText = await actions.erp.eligibilityCriteriaActions.getCheckboxTranslationByName(
            checkboxName,
            expectedCheckboxState
        );
        expect(actualCheckboxText).is.equal(expectedCheckboxText);
    }
);

Then(/^I verify that fields on Employment Details page are not updated$/, async () => {
    const actualData = await pages.erp.employeeProfilePage.currentOccupationData().getText();
    actualData.push(await pages.erp.employeeProfilePage.paymentFrequency().getText());
    expect(actualData.toString()).is.equal(testVars.employmentDetails.historicalData.toString());
});

Then(/^I (can|can't) see a (email|phone) validation message$/, async (isVisible, errorType) => {
    if (isVisible === 'can') {
        const errorMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.writtenCorrespondenceModal.errorMessage(
            errorType.toString().toUpperCase()));

        await expectHelpers.checkTranslation(
            errorMessage,
            `PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.VALIDATION.${errorType.toString().toUpperCase()}`
        );
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.writtenCorrespondenceModal.errorMessage(
            errorType.toString().toUpperCase()));
    }
});

