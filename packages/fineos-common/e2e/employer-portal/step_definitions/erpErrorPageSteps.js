Then(/^the error page web title is displayed$/, async () => {
    expect(await actions.erp.errorPageActions.getBrowserTitle()).to.contain('Error | Employer Portal');
});

Then(/^the error message title is displayed$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.errorPage.errorPageLine1());
    await expectHelpers.checkTranslation(await pages.erp.errorPage.errorPageLine1(), 'ERROR_PAGE.LINE_1');
});

Then(/^the error message text is displayed$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.errorPage.errorPageLine2());
    const translation = translationsHelper.getTranslation('ERROR_PAGE.LINE_2').replace(constants.regExps.htmlTags, '');
    expect(await pages.erp.errorPage.errorPageLine2().getText()).to.contain(translation);
});

Then(/^the error image is displayed$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.errorPage.errorImage());
});

Then(/^the error page header is displayed$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.errorPage.errorPageHeader());
    await expectHelpers.checkTranslation(await pages.erp.errorPage.errorPageHeader(), 'ERROR_PAGE.HEADER');
});

Given(/^I open error page$/, async () => {
    await actions.erp.errorPageActions.navigateToErrorPage();
    await waitHelpers.waitForElementToBeVisible(pages.erp.errorPage.errorPageContainer());
});
