Then(/^I verify that each Leave Plan section has following fields "([^"]*)"$/, async fields => {
    fields = fields.split(',').map(f => f.trim());
    await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.leavePlans().last());

    await pages.erp.timeTakenPage.leavePlans().each(async leavePlan => {
        for (const field of fields) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.leavePlanElement(leavePlan, field));
        }
    });
});

Then(/^I verify that each Leave Plan section has "Leave Plan Name" and "Leave Plan description"$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.leavePlans().last());

    await pages.erp.timeTakenPage.leavePlans().each(async leavePlan => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.leavePlanName(leavePlan));
        await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.leavePlanDescription(leavePlan));
    });
});

When(
    /^I hover over the help icon next to the "(Calendar Year|Fixed Year|Rolling Back|Rolling Forward)" calculation method$/,
    async calculationMethod => {
        let wasFound = false;
        await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.leavePlans().last());

        await pages.erp.timeTakenPage.leavePlans().each(async leavePlan => {
            if ((await leavePlan.getText()).toString().includes(calculationMethod)) {
                const tooltipIcon = await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.calculationMethodHelpIcon(
                    leavePlan));
                await tooltipIcon.HooverOverElement();
                wasFound = true;
            }
        });
        expect(wasFound).is.true;
    }
);

Then(
    /^I expect to see an calculation explanation for "(Calendar Year|Fixed Year|Rolling Back|Rolling Forward)" calculation method$/,
    async calculationMethod => {
        calculationMethod = calculationMethod.replace(constants.regExps.spaces, '_').toUpperCase();

        const tooltip = await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.calculationMethodTooltip(
            calculationMethod));

        await expectHelpers.checkTranslation(
            tooltip,
            `PROFILE.TIME_TAKEN_TAB.CALCULATION_METHOD_TYPE.${calculationMethod}`
        );
    }
);

Then(/^I should see empty state placeholder$/, async () => {
    const emptyStatePlaceholder = await waitHelpers.waitForElementToBeVisible(pages.erp.timeTakenPage.emptyLeavePlanPlaceholder());
    await expectHelpers.checkTranslation(emptyStatePlaceholder, 'PROFILE.TIME_TAKEN_TAB.EMPTY_STATE');
});
