const faker = require('faker');
const {getRandomArrayIndex} = require('../../common/helpers/mathHelper');

Then(
    /^the "(Add|Edit)" "Email(?:s)?" popup opens with "Email\(s\), OK, Cancel, Close X" fields and correct title$/,
    async action => {
        await actions.erp.employeeProfileActions.checkAddEditEmailPopupElements(action);
    }
);

Then(/^the "(Add|Edit)" "Email(?:s)?" popup is still visible$/, async action => {
    await actions.erp.employeeProfileActions.checkAddEditEmailPopupElements(action);
});

Then(/^I click the delete icon beside random email address$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.emailInputs().first());
    const originalEmails = await pages.erp.employeeEmailsPage.emailInputs().getAttribute(
        'value');
    const randomEmailIndex = getRandomArrayIndex(await pages.erp.employeeEmailsPage.deleteEmailButtons());
    await pages.erp.employeeEmailsPage.deleteEmailButtons().get(randomEmailIndex).Click({waitForVisibility: true});

    testVars.employeePersonalDetails.finalEmails = originalEmails.toString().split(',').filter(
        (e, i) => i !== randomEmailIndex);
});

Then(/^I see the deleted email address is removed from the list of emails on the Edit emails popup$/, async () => {
    const expectedEmails = testVars.employeePersonalDetails.finalEmails;
    const actualEmails = await pages.erp.employeeEmailsPage.emailInputs().getAttribute('value');

    expect(expectedEmails.toString()).is.equal(actualEmails.toString());
});

Then(/^the Employee has "(at least|exactly)" (\d+) "Email(?:s)?"$/, async (comparisonMode, expectedNumberOfEmails) => {
    const initialNumberOfEmails = await pages.erp.employeeEmailsPage.emailInputs().count();

    if (initialNumberOfEmails < expectedNumberOfEmails) {
        let generatedEmail = '';
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.emailInputs().first());
        const firstEmail = await pages.erp.employeeEmailsPage.emailInputs().first().getAttribute('value');
        let initialIndex = 0;

        if (firstEmail.toString() !== '') {
            initialIndex = initialNumberOfEmails;
            await pages.erp.employeeEmailsPage.addEmailButton().Click({waitForVisibility: true});
        }

        for (let i = initialIndex; i < expectedNumberOfEmails; i++) {
            generatedEmail = await actions.erp.employeeProfileActions.generatePersonalDetailsData('email');
            await pages.erp.employeeEmailsPage.emailInputs().get(i).SetText(generatedEmail);

            if (i + 1 === Number.parseInt(expectedNumberOfEmails, 10)) {
                break;
            }
            await pages.erp.employeeEmailsPage.addEmailButton().Click({waitForVisibility: true});
        }
    }

    const actualNumberOfEmails = await pages.erp.employeeEmailsPage.emailInputs().filter(async email => (await email.getAttribute(
        'value') !== '')).count();
    testVars.employeePersonalDetails.emailsInputs = await pages.erp.employeeEmailsPage.emailInputs();

    if (comparisonMode === 'exactly') {
        expect(actualNumberOfEmails).is.equal(Number.parseInt(expectedNumberOfEmails, 10));
    } else {
        expect(actualNumberOfEmails).is.gte(Number.parseInt(expectedNumberOfEmails, 10));
    }
});

When(/^I enter (random|the same random) test data in (random|last) email text box$/, async (mode, emailInput) => {
    let email;

    if (mode === 'random') {
        testVars.employeePersonalDetails.email = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
            'email');
    }

    if (emailInput === 'random') {
        const randomEmailIndex = faker.random.number(await pages.erp.employeeEmailsPage.emailInputs().count() - 1);
        email = await pages.erp.employeeEmailsPage.emailInputs().get(randomEmailIndex);
    } else {
        email = await pages.erp.employeeEmailsPage.emailInputs().last();
    }

    await email.SetText('');
    await email.SetText(testVars.employeePersonalDetails.email);

    expect(testVars.employeePersonalDetails.email).is.equal(await email.getAttribute('value'));
});

When(/^I enter "([^"]*)" test data in last email text box$/, async emailAddress => {
    const lastEmailInput = await pages.erp.employeeEmailsPage.emailInputs().last();

    await lastEmailInput.Clear();
    await lastEmailInput.SendText(emailAddress);

    expect(emailAddress).is.equal(await pages.erp.employeeEmailsPage.emailInputs().last().getAttribute('value'));
});

Then(/^I verify email address inputs are refreshed$/, async () => {
    const actual = await pages.erp.employeeEmailsPage.emailInputs().last().getAttribute('value');
    const expected = testVars.employeePersonalDetails.email;

    expect(expected).is.equal(actual);
});

Then(/^I see the "\+Add another" link is "enabled" under the email text box field$/, async () => {
    const addAnotherEmailButton = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.addEmailButton());
    await expectHelpers.checkTranslation(addAnotherEmailButton, 'PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_EMAIL');
});

When(/^I click the "\+Add another" email link$/, async () => {
    await pages.erp.employeeEmailsPage.addEmailButton().Click({waitForVisibility: true});
});

When(/^I see another the email text box field added to the list of emails$/, async () => {
    const originalEmailsNumber = testVars.employeePersonalDetails.emailsInputs.toString().split(',').length;
    const actualEmailsNumber = await pages.erp.employeeEmailsPage.emailInputs().count();
    expect(actualEmailsNumber - 1).is.equal(originalEmailsNumber);

    const lastEmailInput = await waitHelpers.waitForElementToBeVisible(pages.erp.employeeEmailsPage.emailInputs().last());
    expect(await lastEmailInput.getAttribute('value')).is.empty;
});

When(/^I "(accept|cancel|close X)" 'Edit emails' popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.employeeEmailsPage.okButton().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.sharedModal.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.sharedModal.closeXButton().Click({waitForVisibility: true});
    }
    await waitHelpers.waitForSpinnersToDisappear();
});

Then(/^I verify that Email fields on Edit Personal Details page are not updated$/, async () => {
    const actualData = await pages.erp.employeeProfilePage.personalDetailsEmail().getText();
    expect(actualData.toString()).is.equal(testVars.searchTarget.emails.toString());
});

Then(/^the 'Edit emails' popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(
        pages.erp.employeeEmailsPage.editEmailsPopupTitle(),
        timeouts.T30,
        'Expect to not see form title'
    );
});

Then(/^Invalid email error messages is "(visible|not visible)"$/, async visibility => {
    if (visibility === constants.stepWording.VISIBLE) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeEmailsPage.emailFormatErrorMessage(),
            timeouts.T30,
            'Expect to see form email format error'
        );

        await expectHelpers.checkTranslation(
            await pages.erp.employeeEmailsPage.emailFormatErrorMessage(),
            'FINEOS_COMMON.VALIDATION.NOT_EMAIL'
        );
    } else {
        await waitHelpers.waitForElementToBeNotVisible(
            pages.erp.employeeEmailsPage.emailFormatErrorMessage(),
            timeouts.T30,
            'Expect to see form email format error'
        );
    }
});
