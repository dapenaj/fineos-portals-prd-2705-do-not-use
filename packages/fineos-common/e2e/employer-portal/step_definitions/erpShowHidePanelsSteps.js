Then(/^the "Show\/hide panels" modal opens$/, async () => {
    const title = await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.modalTitle());
    await expectHelpers.checkTranslation(title, 'WIDGET.SHOW_HIDE_PANELS');

    const employeesExpectedToRTWLabel = await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.panelLabel(
        'EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK'));
    await expectHelpers.checkTranslation(employeesExpectedToRTWLabel, 'WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK');

    const notificationsCreatedLabel = await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.panelLabel(
        'NOTIFICATIONS_CREATED'));
    await expectHelpers.checkTranslation(notificationsCreatedLabel, 'WIDGET.NOTIFICATIONS_CREATED');

    await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.employeesExpectedToRTWSwitch());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.notificationsCreatedSwitch());

    await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.closeXButton());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.cancelButton());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.sharedModal.saveChangesButton());
});

Then(
    /^the "(Employees returning to work|Notifications created)" switch is (disabled|enabled)$/,
    async (panelName, expectedSwitchState) => {
        let actualSwitchState;
        expectedSwitchState = (expectedSwitchState === 'enabled');
        await waitHelpers.waitForElementToBeVisible(await pages.erp.showHidePanelsModal.modalTitle());

        if (panelName === 'Employees returning to work') {
            actualSwitchState = await pages.erp.showHidePanelsModal.employeesExpectedToRTWSwitch().getAttribute(
                'aria-checked');
        } else {
            actualSwitchState = await pages.erp.showHidePanelsModal.notificationsCreatedSwitch().getAttribute(
                'aria-checked');
        }

        expect(expectedSwitchState).is.equal(Boolean(actualSwitchState));
    }
);
