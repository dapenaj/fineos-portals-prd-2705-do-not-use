const {getRequestData} = require('../../common/helpers/snifferHelper');

Then(/^My Dashboard page (with|without) available notifications is loaded$/, async isNotificationsList => {
    await waitHelpers.waitForSpinnersToDisappear();

    await expectHelpers.expectElementToBeVisible(
        pages.erp.searchPage.sectionSearch(),
        'I should see search widget'
    );

    const title = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.pageTitle());
    await expectHelpers.checkTranslation(title, 'PAGE_TITLES.MY_DASHBOARD');

    if (isNotificationsList === constants.stepWording.WITH) {
        const numberOfNotificationsLabel = (await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsCount().getText())).toString();
        await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsLabel());
        await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.viewNotificationsDetailsButton());

        expect(Number.parseInt(numberOfNotificationsLabel, 10)).is.greaterThan(0);
    } else {
        const emptyStateLabel = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.emptyStateLabel());
        await expectHelpers.checkTranslation(emptyStateLabel, 'OUTSTANDING_NOTIFICATIONS.EMPTY_STATE');

        await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsCount());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsLabel());
    }
});

Then(/^My Dashboard page with available leaves decided is loaded$/, async () => {
    const title = await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.pageTitle());
    await expectHelpers.checkTranslation(title, 'PAGE_TITLES.MY_DASHBOARD');
    const numberOfLeavesDecided = (await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidgetPieChartTotalResults().getText())).toString();
    await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.viewLeavesDecidedDetailsButton());
    expect(Number.parseInt(numberOfLeavesDecided, 10)).is.greaterThan(0);
});

Then(/^I open the grouping criteria selector in the "View leaves decided" panel$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedFilterCriteria());
    await pages.erp.myDashboardPage.leavesDecidedFilterCriteria().Click({waitForVisibility: true});
});

Then(/^I select a "(Decision|Reason|Group)" grouping criteria the "View leaves decided" panel$/, async criteria => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidgetCriteriaFilterOption(criteria.toUpperCase()));
    await pages.erp.myDashboardPage.leavesDecidedWidgetCriteriaFilterOption(criteria.toUpperCase()).Click({waitForVisibility: true});
});

Then(/^I verify the graph total number fits to the sum of particular slices amounts$/, async () => {
    const numberTotalResults = (await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidgetPieChartTotalResults().getText())).toString();
    await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidgetPieChartPartialResults().first());
    const numberPartialResults = await pages.erp.myDashboardPage.leavesDecidedWidgetPieChartPartialResults().map(item => item.getText());
    for (let i = 0; i < numberPartialResults.length; i++) {
        numberPartialResults[i] = parseInt(numberPartialResults[i], 10);
    }
    const totalNumberPartialResults = numberPartialResults.reduce((a, b) => a + b, 0);
    expect(parseInt(numberTotalResults, 10)).to.equal(totalNumberPartialResults);
});

Then(
    /^"(Employees returning to work|Notifications created|Leaves decided)" widget is "(visible|not visible)"$/,
    async (widgetName, isVisible) => {
        if (widgetName === 'Employees returning to work') {
            if (isVisible === 'visible') {
                await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.expectedToReturnToWorkWidget());
            } else {
                await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.expectedToReturnToWorkWidget());
            }
        } else if (widgetName === 'Notifications created') {
            if (isVisible === 'visible') {
                await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.notificationsCreatedWidget());
            } else {
                await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.notificationsCreatedWidget());
            }
        } else if (widgetName === 'Leaves decided') {
            if (isVisible === 'visible') {
                await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.leavesDecidedWidget());
            } else {
                await waitHelpers.waitForElementToBeNotVisible(pages.erp.myDashboardPage.leavesDecidedWidget());
            }
        }
    }
);

Then(/^I click the "Show\/hide panels" button$/, async () => {
    await pages.erp.myDashboardPage.showHidePanels().Click({waitForVisibility: true});
});

Then(/^I click "View details" to see notifications list with outstanding requirements$/, async () => {
    await pages.erp.myDashboardPage.viewNotificationsDetailsButton().Click({waitForVisibility: true});
});

Then(
    /^I click "View details" on "(Employees expected to return to work|Notifications created|Notifications with outstanding requirements|Leaves decided|Employees approved for leave)" widget$/,
    async widgetName => {
        if (widgetName === 'Employees expected to return to work') {
            await pages.erp.myDashboardPage.viewEmployeesExpectedToReturnToWorkButton().Click({waitForVisibility: true});
        } else if (widgetName === 'Notifications created') {
            await pages.erp.myDashboardPage.viewNotificationsCreatedButton().Click({waitForVisibility: true});
        } else if (widgetName === 'Notifications with outstanding requirements') {
            await pages.erp.myDashboardPage.viewNotificationsDetailsButton().Click({waitForVisibility: true});
        } else if (widgetName === 'Leaves decided') {
            await pages.erp.myDashboardPage.viewLeavesDecidedDetailsButton().Click({waitForVisibility: true});
        } else {
            await pages.erp.myDashboardPage.viewEmployeesApprovedForLeaveDetailsButton().Click({waitForVisibility: true});
        }
    }
);

Then(
    /^I select "(Employees expected to return to work|Notifications created|Leaves decided|Employees approved for leave)" date period with available results$/,
    async widgetName => {
        await (await actions.erp.myDashboardActions.getWidgetByName(widgetName)).Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.datePeriodFilterOptions().first());
        const filterOptions = await pages.erp.myDashboardPage.datePeriodFilterOptions();

        for (const option of filterOptions.reverse()) {
            await option.Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();

            const numberOfApiSortedItems = await getRequestData(
                constants.endPoints.filteredNotifications,
                'totalSize',
            );

            if (Number.parseInt(numberOfApiSortedItems, 10) > 0) {
                await actions.erp.notificationsWithOutstandingReqActions.setTotalNumberOfWidget(widgetName);
                break;
            }
            await (await actions.erp.myDashboardActions.getWidgetByName(widgetName)).Click({waitForVisibility: true});
        }
    }
);

Then(
    /^I select "(today|this week|next week|week after next|last week|week before last)" on "(Employees expected to return to work|Notifications created|Leaves decided|Employees approved for leave)" widget$/,
    async (filterOption, widgetName) => {
        let filteringOption;

        if (widgetName === 'Employees expected to return to work') {
            await pages.erp.myDashboardPage.employeesExpectedToReturnToWorkWidgetDayFilter().Click({waitForVisibility: true});
            filteringOption = await pages.erp.myDashboardPage.employeesExpectedToReturnToWorkWidgetFilterOption(
                filterOption);
            await filteringOption.Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.viewEmployeesExpectedToReturnToWorkPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.employeesExpectedToReturnToWorkWidgetDayFilter().getText();
        } else if (widgetName === 'Notifications created') {
            await pages.erp.myDashboardPage.notificationsCreatedWidgetDayFilter().Click({waitForVisibility: true});
            filteringOption = await pages.erp.myDashboardPage.notificationsCreatedWidgetDayFilterOption(
                filterOption);
            await filteringOption.Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.notificationsCreatedWidgetPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.notificationsCreatedWidgetDayFilter().getText();
        } else if (widgetName === 'Leaves decided') {
            await pages.erp.myDashboardPage.leavesDecidedWidgetDayFilter().Click({waitForVisibility: true});
            filteringOption = await pages.erp.myDashboardPage.leavesDecidedWidgetDayFilterOption(
                filterOption);
            await filteringOption.Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.leavesDecidedWidgetPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.leavesDecidedWidgetDayFilter().getText();
        } else if (widgetName === 'Employees approved for leave') {
            await pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetDayFilter().Click({waitForVisibility: true});
            filteringOption = await pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetDayFilterOption(
                filterOption);
            await filteringOption.Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();
            testVars.filtering.totalResults = await pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetPieChartTotalResults().getText();
            testVars.filtering.dateRange = await pages.erp.myDashboardPage.employeesApprovedForLeaveWidgetDayFilter().getText();
        }
    }
);

Then(/^I can see user is "(disabled|enabled)"$/, async userStatus => {
    if (userStatus === 'disabled') {
        await expectHelpers.checkTranslation(
            await pages.erp.myDashboardPage.welcomeMessageTitle(),
            'WELCOME_MESSAGE.TITLE'
        );
        await expectHelpers.checkTranslation(
            await pages.erp.myDashboardPage.welcomeMessageSubtitle(),
            'WELCOME_MESSAGE.SUBTITLE'
        );
        await expectHelpers.expectElementToBeNotVisible(pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsCount());
        await expectHelpers.expectElementToBeNotVisible(pages.erp.searchPage.sectionSearch());
    } else {
        await expectHelpers.expectElementToBeNotVisible(pages.erp.myDashboardPage.welcomeMessageTitle());
        await expectHelpers.expectElementToBeNotVisible(pages.erp.myDashboardPage.welcomeMessageSubtitle());
        await expectHelpers.expectElementToBeVisible(pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsCount());
        await expectHelpers.expectElementToBeVisible(pages.erp.searchPage.sectionSearch());
    }
});

When(/^set high contrast mode is "(on|off)" if not set$/, async action => {
    await pages.erp.myDashboardPage.highContrastModeIcon().Click({waitForVisibility: true});
    await actions.erp.myDashboardActions.isHighContrastModeModalVisible(true);

    let actualState = await actions.erp.myDashboardActions.getHighContrastModeState();

    if (actualState !== action) {
        await pages.erp.myDashboardPage.highContrastModeSwitch().Click({waitForVisibility: true});
    }

    actualState = await actions.erp.myDashboardActions.getHighContrastModeState();
    expect(action).is.equal(actualState);

    await pages.erp.myDashboardPage.highContrastModeIcon().Click({waitForVisibility: true});
    await actions.erp.myDashboardActions.isHighContrastModeModalVisible(false);
});

When(/^I turn "(on|off)" high contrast mode using (enter|mouse click)$/, async (action, actionType) => {
    const actualState = await actions.erp.myDashboardActions.getHighContrastModeState();

    if (actualState !== action) {
        if (actionType === 'enter') {
            await pages.erp.myDashboardPage.highContrastModeSwitch().sendKeys(protractor.Key.ENTER);
        } else {
            await pages.erp.myDashboardPage.highContrastModeSwitch().Click({waitForVisibility: true});
        }
    }
});

When(/^I (click|press escape button|tab to)(?: high contrast mode icon)?(?: and press enter)?$/, async action => {
    if (action === 'click') {
        await pages.erp.myDashboardPage.highContrastModeIcon().Click({waitForVisibility: true});
    } else if (action === 'press escape button') {
        await pages.erp.myDashboardPage.highContrastModeSwitch().sendKeys(protractor.Key.ESCAPE);
    } else {
        await pages.erp.searchPage.inputSearch().Click({waitForVisibility: true});
        await pages.erp.searchPage.inputSearch().sendKeys(protractor.Key.TAB + protractor.Key.TAB +
            protractor.Key.ENTER);
    }
});

Then(/^The high contrast mode modal (closes|opens)$/, async expectedState => {
    if (expectedState === 'opens') {
        await actions.erp.myDashboardActions.isHighContrastModeModalVisible(true);
    } else {
        await actions.erp.myDashboardActions.isHighContrastModeModalVisible(false);
    }
});

Then(/^The high contrast mode switch is "(on|off)"$/, async expectedState => {
    const actualState = await actions.erp.myDashboardActions.getHighContrastModeState();
    expect(expectedState).is.equal(actualState);
});

Then(/^I see the accessibility icon visible on the right side of the Top Bar$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.highContrastModeIcon());
});

Then(
    /^I see a total number of notifications with outstanding requirements with high contrast color "(on|off)"$/,
    async expectedState => {
        let expectedColor;

        await waitHelpers.waitForElementToBeVisible(pages.erp.myDashboardPage.highContrastModeIcon());
        const actualColor = await pages.erp.myDashboardPage.notificationsWithOutstandingRequirementsCount().getCssValue(
            'background-color');

        if (expectedState === 'on') {
            expectedColor = constants.highContrastModeColors.notificationsCount.highContrastMode;
        } else {
            expectedColor = constants.highContrastModeColors.notificationsCount.normalMode;
        }

        expect(expectedColor).is.equal(actualColor);
    }
);
