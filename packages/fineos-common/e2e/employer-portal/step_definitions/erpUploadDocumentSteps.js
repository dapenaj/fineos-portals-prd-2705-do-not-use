When(/^the "Upload Document" popup is (visible|still visible)$/, async mode => {
    const pageTitle = await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.pageTitle());
    await expectHelpers.checkTranslation(pageTitle, 'NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD_DOCUMENT');

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.uploadDocumentPage.formName(),
        pages.erp.uploadDocumentPage.validFileExtensionsLabel(),
        pages.erp.uploadDocumentPage.maxFileSizeLabel(),
        pages.erp.uploadDocumentPage.cancelUploadDocumentButton(),
        pages.erp.uploadDocumentPage.uploadDocumentButton(),
        pages.erp.uploadDocumentPage.closeUploadPageButton()
    ]);

    if (mode === 'visible') {
        await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.dragDocumentLabel());
        await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.browseFilesButton());
    }

    testVars.outstandingItemTitle = await pages.erp.uploadDocumentPage.formName().getText();
});

Then(/^the "Upload Document" popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.pageTitle());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.formName());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.validFileExtensionsLabel());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.maxFileSizeLabel());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.cancelUploadDocumentButton());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.closeUploadPageButton());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.dragDocumentLabel());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.browseFilesButton());
});

When(/^I upload "([^"]*)" document$/, async fileName => {
    await testDataHelpers.uploadDocument(fileName);
    const uploadedFileLabel = await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.uploadedFileLabel(
        fileName));

    expect(fileName).is.equal(await uploadedFileLabel.getText());
});

When(/^I "(accept|cancel)" "Upload Document" pop up$/, async action => {
    if (action === constants.popUpActions.ACCEPT) {
        await pages.erp.uploadDocumentPage.uploadDocumentButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.uploadDocumentPage.cancelUploadDocumentButton().Click({waitForVisibility: true});
    }
});

Then(/^I can see "invalid extension" error$/, async () => {
    const errorLabel = await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.invalidExtensionError());
    await expectHelpers.checkTranslation(errorLabel, 'FINEOS_COMMON.VALIDATION.FILE_EXTENSION');
});

Then(/^I can see "success" modal$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.uploadedDocumentSuccessPopUpTitle());
    await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.uploadedDocumentSuccessPopUpMessage());
    await waitHelpers.waitForElementToBeVisible(pages.erp.uploadDocumentPage.uploadedDocumentSuccessPopUpImage());
});

Then(/^the "success" modal closes$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.uploadedDocumentSuccessPopUpTitle());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.uploadedDocumentSuccessPopUpMessage());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.uploadDocumentPage.uploadedDocumentSuccessPopUpImage());
});
