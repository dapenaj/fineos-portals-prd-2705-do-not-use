Then(/^I can see submit error alert is "(visible|not visible)"$/, async visibility => {
    if (visibility === constants.stepWording.VISIBLE) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.sharedModal.submitErrorMessage());
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.sharedModal.submitErrorMessage());
    }
});

Then(
    /^I see a validation message "Please select at least one option" is "(visible|not visible)"$/,
    async visibility => {
        if (visibility === constants.stepWording.VISIBLE) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.sharedModal.requiredSelectedOptionErrorMessage());
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.sharedModal.requiredSelectedOptionErrorMessage());
        }
    }
);

Then(
    /^I see a validation message "Required" is "(visible|not visible)" for field "([^"]*)"$/,
    async (visibility, fieldLabel) => {
        fieldLabel = fieldLabel.replace(constants.regExps.spaces, '-');

        if (visibility === constants.stepWording.VISIBLE) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.sharedModal.requiredErrorMessage(fieldLabel));
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.sharedModal.requiredErrorMessage(fieldLabel));
        }
    }
);

Then(
    /^I see a validation message "Please type your email address in the format myname@domain.com" is "(visible|not visible)"$/,
    async visibility => {
        if (visibility === constants.stepWording.VISIBLE) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.addNewEmailModal.invalidEmailFormatErrorMessage());
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.addNewEmailModal.invalidEmailFormatErrorMessage());
        }
    }
);

When(/^I "(accept|cancel|close X)" "(?:[^"]*)" time modal$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.sharedModal.okButton().first().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.sharedModal.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.sharedModal.closeXButton().Click({waitForVisibility: true});
    }
});

When(/^I click "(yes|no)" button$/, async button => {
    await pages.erp.sharedModal.commonButton(button).Click({waitForVisibility: true});
});

When(/^I tab to the next field$/, async () => {
    await browser.actions().sendKeys(protractor.Key.TAB).perform();
});

When(/^I "(accept|dismiss)" alert$/, async action => {
    if (action === 'accept') {
        await browser.switchTo().alert().accept();
    } else {
        await browser.switchTo().alert().dismiss();
    }
});

When(/^I click the "They want to use a different (email|phone number)" link$/, async linkType => {
    let linkButton;
    let translationPath;

    if (linkType === 'email') {
        linkButton = await pages.erp.sharedModal.useDifferentEmailButton();
        translationPath = 'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON';
    } else {
        linkButton = await pages.erp.sharedModal.useDifferentPhoneNumberButton();
        translationPath = 'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON';
    }

    await expectHelpers.checkTranslation(linkButton, translationPath);
    await linkButton.Click({waitForVisibility: true});
});
