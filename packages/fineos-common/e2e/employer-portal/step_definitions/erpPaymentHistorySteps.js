const moment = require('moment');
const {getRequestDataArray} = require('../../common/helpers/snifferHelper');

Then(
    /^'View Payment History' for "([^"]*)" benefit lists (\d+) of (\d+) payments$/,
    async (paymentName, visiblePayments, allPayments) => {
        const title = await waitHelpers.waitForElementToBeVisible(pages.erp.paymentHistoryPage.historyPageHeader());
        await expectHelpers.checkTranslation(title, 'PAYMENTS.PAYMENTS_VIEW_HISTORY');

        expect(await pages.erp.paymentHistoryPage.benefitName().getText()).is.equal(paymentName);

        let translationMessage = await translationsHelper.getTranslation('PAYMENTS.PAYMENTS_INFO');
        translationMessage = translationMessage.replace('{visiblePayments}', visiblePayments);
        translationMessage = translationMessage.replace('{allPayments}', allPayments);
        expect(await pages.erp.paymentHistoryPage.paymentsInfo().getText()).is.equal(translationMessage);

        await waitHelpers.waitForElementToBeVisible(pages.erp.paymentHistoryPage.closeHistoryButton());
        await waitHelpers.waitForElementToBeVisible(pages.erp.paymentHistoryPage.paymentsFilterButton());
        await waitHelpers.waitForElementToBeVisible(pages.erp.paymentHistoryPage.cancelHistoryButton());

        expect(await pages.erp.paymentHistoryPage.collapsiblePayments().count()).is.equal(parseInt(
            visiblePayments,
            10
        ));
    }
);

When(/^I filter payments by dates$/, async () => {
    await pages.erp.paymentHistoryPage.paymentsFilterButton().Click({waitForVisibility: true});

    const paymentDates = (await getRequestDataArray(constants.endPoints.getPayments, 'paymentDate')).sort();
    // eslint-disable-next-line prefer-destructuring
    testVars.payments.dateFrom = moment(paymentDates[0]).format(constants.dateFormats.shortDate).toString();
    testVars.payments.dateUntil = moment(paymentDates.slice(-1)[0]).add(
        1,
        'day'
    ).format(constants.dateFormats.shortDate).toString();

    await pages.erp.paymentHistoryPage.filterFromDateInput('from').Click({waitForVisibility: true});
    await pages.erp.paymentHistoryPage.filterFromDateInput('from').SendText(testVars.payments.dateFrom + protractor.Key.ENTER);

    await pages.erp.paymentHistoryPage.filterFromDateInput('until').Click({waitForVisibility: true});
    await pages.erp.paymentHistoryPage.filterFromDateInput('until').SendText(testVars.payments.dateUntil + protractor.Key.ENTER);
});

When(/^I (apply|close) filter$/, async filterOption => {
    if (filterOption === constants.stepWording.APPLY) {
        await pages.erp.paymentHistoryPage.applyFilterButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.paymentHistoryPage.cancelFilterButton().Click({waitForVisibility: true});
    }
});

When(/^I reset filter$/, async () => {
    await pages.erp.paymentHistoryPage.resetFilterButton().Click({waitForVisibility: true});
});

When(/^I can see payments list is filtered correctly$/, async () => {
    const uiPaymentsDates = [];
    await waitHelpers.waitForElementToBeVisible(pages.erp.paymentHistoryPage.collapsiblePayments().first());

    const expectedFromDate = moment(testVars.payments.dateFrom, constants.dateFormats.shortDate, true).toDate();
    const expectedUntilDate = moment(testVars.payments.dateUntil, constants.dateFormats.shortDate, true).toDate();

    await pages.erp.paymentHistoryPage.collapsiblePayments().each(async singlePayment => {
        const date = await pages.erp.paymentHistoryPage.paymentDate(singlePayment).getText();
        const actualDate = moment(date, constants.dateFormats.shortDate, true).toDate();
        expect(actualDate).to.be.within(expectedFromDate, expectedUntilDate);
        uiPaymentsDates.push(moment(date, constants.dateFormats.shortDate, true).format(constants.dateFormats.isoDate));
    });

    let apiPaymentDates = (await getRequestDataArray(constants.endPoints.getPayments, 'paymentDate'));
    apiPaymentDates = apiPaymentDates.filter(d => (new Date(d) >= expectedFromDate) && new Date(d) <= expectedUntilDate);
    expect(apiPaymentDates.toString()).is.equal(uiPaymentsDates.toString());
});

When(/^I can see history payments list filtered in descending order$/, async () => {
    const paymentsDates = [];
    await pages.erp.paymentHistoryPage.collapsiblePayments().each(async singlePayment => {
        const actualDateString = await pages.erp.paymentHistoryPage.paymentDate(singlePayment).getText();
        const actualDate = moment(actualDateString, constants.dateFormats.shortDate, true).toDate();
        paymentsDates.push(moment(actualDate, constants.dateFormats.shortDate, true));
    });

    expect(paymentsDates.length).is.greaterThan(0);
    expect(paymentsDates).to.be.descending;
});

Then(/^each payment on the list is (expanded|collapsed)$/, async paymentState => {
    await pages.erp.paymentHistoryPage.paymentHeaders().each(async singlePayment => {
        if (paymentState === 'expanded') {
            expect(await singlePayment.getAttribute('aria-expanded')).to.be.equal('true');
        } else {
            expect(await singlePayment.getAttribute('aria-expanded')).to.be.equal('false');
        }
    });
});

Then(/^each payment has 'Payment Amount and Payment Date' fields$/, async () => {
    await pages.erp.paymentHistoryPage.collapsiblePayments().each(async singlePayment => {
        const paymentFields = (await singlePayment.getText()).split('\n');

        expect(paymentFields.length).is.equal(2);
        expect(paymentFields[0].trim()).to.match(/^(-)?\$\d+\.\d{2}$/g);
        expect(moment(paymentFields[1], constants.dateFormats.shortDate, true).isValid()).is.true;
    });
});

Then(/^I can see (\d+) payments$/, async numberOfPayments => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.paymentHistoryPage.collapsiblePayments().first());
    testVars.payments.paymentsHistory = [];
    await pages.erp.paymentHistoryPage.collapsiblePayments().each(async payment => {
        testVars.payments.paymentsHistory.push(await payment.getText());
    });

    expect(await pages.erp.paymentHistoryPage.collapsiblePayments().count()).is.equal(parseInt(numberOfPayments, 10));
});

When(/^I expand last history payment$/, async () => {
    await pages.erp.paymentHistoryPage.collapsiblePayments().first().Click({waitForVisibility: true});
});

When(/^I expand "(Recurring|Adhoc)" history payment$/, async paymentType => {
    const paymentsTypes = await getRequestDataArray(constants.endPoints.getPayments, 'paymentType');
    const paymentTypeIndex = paymentsTypes.indexOf(paymentType);
    await pages.erp.paymentHistoryPage.collapsiblePayments().get(paymentTypeIndex).Click({waitForVisibility: true});
});

Then(/^latest history payment has "([^"]*)" fields$/, async paymentFields => {
    const lastPayment = await waitHelpers.waitForElementToBeVisible(pages.erp.paymentHistoryPage.collapsiblePayments().first());
    const paymentFieldsArray = paymentFields.split(',').map(f => f.trim());

    for (const field of paymentFieldsArray) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.caseDetailsField(
                lastPayment,
                field
            )
            , timeouts.T30
            , `Page element with label '${field}' was not found.`);
    }
});

When(/^I close 'Latest Payments' history$/, async () => {
    await pages.erp.paymentHistoryPage.cancelHistoryButton().Click({waitForVisibility: true});
});

When(/^I show 'Payments Breakdown' for history payment$/, async () => {
    const lastPayment = await pages.erp.paymentHistoryPage.collapsiblePayments().first();
    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.notificationPage.paymentBreakdownButton(lastPayment).Click({waitForVisibility: true});
});

When(/^I see a list of all adjustments used to calculate the 'Net Payment Amount'$/, async () => {
    const lastPayment = await pages.erp.paymentHistoryPage.collapsiblePayments().first();
    const sum = await actions.erp.notificationActions.sumAdjustmentsAmounts(lastPayment);

    expect(await pages.erp.notificationPage.adjustmentSummaryPayment(lastPayment).first().getText()).contains(sum.toString());
});

Then(/^each history Adjustment has Adjustment Type Name and Adjustment Type Amount$/, async () => {
    const lastPayment = await pages.erp.paymentHistoryPage.collapsiblePayments().first();
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationPage.adjustmentHeaders(lastPayment).first());
    await pages.erp.notificationPage.adjustmentHeaders(lastPayment).each(async adjustment => {
        const adjustmentHeaderFields = (await adjustment.getText()).split('\n');

        expect(adjustmentHeaderFields.length).is.equal(2);
        expect(adjustmentHeaderFields[0]).is.not.empty;
        expect(adjustmentHeaderFields[1].trim()).to.match(/^(-)?\$\d+\.\d{2}$/g);
    });
});

Then(/^the 'Net Payment Amount' is the history payment amount used for the payment line$/, async () => {
    const lastPayment = await pages.erp.paymentHistoryPage.collapsiblePayments().first();
    const netPayment = await pages.erp.notificationPage.adjustmentSummaryPayment(lastPayment).first().getText();
    const paymentHeader = await pages.erp.paymentHistoryPage.paymentHeaders().first().getText();

    expect(netPayment.split('\n')[1]).is.equal(paymentHeader.split('\n')[0]);
});

When(/^I show last history Adjustment Details$/, async () => {
    const lastPayment = await pages.erp.paymentHistoryPage.collapsiblePayments().first();
    await pages.erp.notificationPage.adjustmentDetailsButton(lastPayment).first().Click({waitForVisibility: true});
});

When(/^adjustment details contains following information "([^"]*)"$/, async adjustmentFields => {
    const adjustmentFieldsArray = adjustmentFields.split(',').map(f => f.trim());
    const lastPayment = await pages.erp.paymentHistoryPage.collapsiblePayments().first();
    const adjustmentDetails = await pages.erp.notificationPage.adjustmentDetails(lastPayment).first();

    for (const field of adjustmentFieldsArray) {
        await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.caseDetailsField(
                adjustmentDetails,
                field
            )
            , timeouts.T30
            , `Page element with label '${field}' was not found.`);
    }
});
