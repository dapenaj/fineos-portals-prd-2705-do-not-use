Then(/^the '(Add|Edit) Work Pattern' popup opens$/, async mode => {
    const title = await waitHelpers.waitForElementToBeVisible(
        pages.erp.editWorkPatternPage.workPatternModalTitle(mode),
        timeouts.T30,
        'Expect to see form title'
    );

    await expectHelpers.checkTranslation(title, `WORK_PATTERN.${mode.toString().toUpperCase()}_WORK_PATTERN`);

    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.mandatoryFieldsLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatternTypeSelectLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatternTypeSelect());
    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.okButton());
    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.cancelButton());
    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.closeButton());
});

Then(/^I select "(Fixed|Rotating|Variable)" option$/, async workPatternType => {
    await pages.erp.editWorkPatternPage.workPatternTypeSelect().Click({waitForVisibility: true});
    await pages.erp.editWorkPatternPage.workPatternType(workPatternType).Click({waitForVisibility: true});
});

Then(
    /^I can see the Work pattern drop down with "(Fixed|Rotating|Variable)" option selected$/,
    async workPatternType => {
        const actualOption = await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatternTypeSelect());
        await expectHelpers.checkTranslation(
            actualOption,
            `ENUM_DOMAINS.WORK_PATTERN_TYPE.${workPatternType.toString().toUpperCase()}`
        );
    }
);

Then(/^I see that "Work pattern" drop down contains options: "([^"]*)"$/, async options => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatternTypeSelect());
    await pages.erp.editWorkPatternPage.workPatternTypeSelect().Click({waitForVisibility: true});

    for (let o of options.toString().split(constants.regExps.spaces)) {
        o = o.toString().toUpperCase();
        const actualOption = await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatternType(o));
        await expectHelpers.checkTranslation(actualOption, `ENUM_DOMAINS.WORK_PATTERN_TYPE.${o}`);
    }
});

Then(/^the 'Edit Work Pattern' popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.editWorkPatternPage.workPatternModalTitle());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.editWorkPatternPage.workPatternTypeSelectLabel());
});

Then(/^I can see the "Work pattern submitted" confirmation popup opens$/, async () => {
    const header = await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.confirmationHeader());
    await expectHelpers.checkTranslation(header, 'WORK_PATTERN.SUCCESS_MODAL_HEADER');

    const message = await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.confirmationMessage());
    await expectHelpers.checkTranslation(message, 'WORK_PATTERN.SUBMIT_SUCCESS_MESSAGE');

    await waitHelpers.waitForElementToBeVisible(pages.erp.sharedModal.confirmationSuccessImage());
    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.sharedModal.closeButton(),
        pages.erp.sharedModal.closeXButton()
    ]);
});

When(/^I "(close|close X)" "Work pattern submitted" confirmation popup$/, async button => {
    let actionButton;

    if (button === constants.popUpActions.CLOSE) {
        actionButton = await waitHelpers.waitForElementToBeStatic(pages.erp.sharedModal.closeButton());
    } else {
        actionButton = await waitHelpers.waitForElementToBeStatic(pages.erp.sharedModal.closeXButton());
    }

    await actionButton.Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
});

Then(/^I can see the "Work pattern submitted" confirmation popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(
        pages.erp.editWorkPatternPage.confirmationHeader(),
        timeouts.T30,
        'Expect to not see form header'
    );
});

When(/^I "(accept|cancel|close X)" '(?:Add|Edit) work pattern' popup$/, async button => {
    let actionButton;

    if (button === constants.popUpActions.ACCEPT) {
        actionButton = await waitHelpers.waitForElementToBeStatic(pages.erp.editWorkPatternPage.okButton());
    } else if (button === constants.popUpActions.CANCEL) {
        actionButton = await waitHelpers.waitForElementToBeStatic(pages.erp.editWorkPatternPage.cancelButton());
    } else {
        actionButton = await waitHelpers.waitForElementToBeStatic(pages.erp.editWorkPatternPage.closeButton());
    }

    await actions.erp.editWorkPatternActions.recordEditedWorkPatternData();

    await actionButton.Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
});

When(/^I tab to next field$/, async () => {
    await pages.erp.editWorkPatternPage.workPatternModalTitle().Click({waitForVisibility: true});
});

Then(
    /^I verify the "(at least one filled|not provided information|required|invalid time format)" error with text "(?:[^"]*)" displays for "(?:[^"]*)"$/,
    async errorType => {
        const expectedErrorName = actions.erp.editWorkPatternActions.getValidationErrorName(errorType);
        const expectedError = await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.validationError(
            expectedErrorName));

        await expectHelpers.checkTranslation(expectedError, expectedErrorName);
    }
);

When(
    /^I set "(pattern repeats|day length same|non standard working week)" switch to "(on|off)"$/,
    async (switchName, switchAction) => {
        const switchElement = await actions.erp.editWorkPatternActions.getWorkPatternSwitch(switchName);
        const actualSwitchState = await switchElement.getAttribute('aria-checked');
        if (constants.stepWording.ON === switchAction && (actualSwitchState === 'false' || actualSwitchState === null)) {
            await switchElement.Click({waitForVisibility: true});
        } else if (constants.stepWording.ON !== switchAction && actualSwitchState === 'true') {
            await switchElement.Click({waitForVisibility: true});
        }
    }
);
When(
    /^I can see the "(pattern repeats|day length same|non standard working week|include weekend)" switch is "(on|off|not visible)"$/,
    async (switchName, expectedSwitchState) => {
        if (expectedSwitchState === 'not visible') {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.includeWeekendSwitchOff());
        } else {
            const switchElement = await actions.erp.editWorkPatternActions.getWorkPatternSwitch(switchName);
            let actualSwitchState = await switchElement.getAttribute('aria-checked');
            actualSwitchState = actualSwitchState === 'true' ? 'on' : 'off';

            expect(expectedSwitchState).equals(actualSwitchState);
        }
    }
);

When(
    /^I can see "(1|2|3|4)" week(?:s)? row(?:s)? show(?:s)? with columns "(Mon, Tue, Wed, Thu, Fri|Mon, Tue, Wed, Thu, Fri, Sat, Sun)"$/,
    async (expectedWeeksNumber, week) => {
        const days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY'];

        if (week === 'Mon, Tue, Wed, Thu, Fri, Sat, Sun') {
            days.push(['SATURDAY']);
            days.push(['SUNDAY']);
        }

        const expectedTitle = days.map(d => translationsHelper.getTranslation(`WORK_PATTERN.WEEK_DAYS_SHORTCUT.${d}`)).join(
            ',');
        await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.dayLabels().first());
        const actualTitle = await pages.erp.editWorkPatternPage.dayLabels().getText();
        const weeksArray = testDataHelpers.chunkArray(actualTitle.toString().split(','), days.length);

        expect(weeksArray).is.not.empty;
        weeksArray.forEach(actualWeekTitle => {
            expect(expectedTitle.toString()).equals(actualWeekTitle.toString());
        });
    }
);

Then(
    /^I can see "(1|2|3|4)" row(?:s)? of a week(?: each)? with following times "([^"]*)"$/,
    async (expectedWeeksNumber, expectedTimes) => {
        let actualTimes;
        await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workPatternTypeSelect());
        const actualWorkPattern = await pages.erp.editWorkPatternPage.workPatternTypeSelect().getAttribute(
            'data-test-el');

        if (actualWorkPattern.toString().toLowerCase().includes('fixed')) {
            const switchElement = await actions.erp.editWorkPatternActions.getWorkPatternSwitch(
                'non standard working week');

            if ((await switchElement.getAttribute('aria-checked')).toString() === 'true') {
                await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.allDaysInputs().first());
                actualTimes = await pages.erp.editWorkPatternPage.allDaysInputs().map(el => el.getAttribute('value'));
            } else {
                actualTimes = await pages.erp.editWorkPatternPage.allDaysValues().map(el => el.getText());
            }
        } else if (actualWorkPattern.toString().toLowerCase().includes('rotating')) {
            actualTimes = await pages.erp.editWorkPatternPage.allDaysInputs().map(el => el.getAttribute('value'));
        }

        const weeksArray = testDataHelpers.chunkArray(
            actualTimes.toString().split(','),
            expectedTimes.split(',').length
        );
        expect(weeksArray.length.toString()).equals(expectedWeeksNumber);

        weeksArray.forEach(actualWeekTitle => {
            expect(expectedTimes.toString()).equals(actualWeekTitle.toString());
        });
    }
);

When(/^I set "(2|3|4) Weeks" option$/, async option => {
    const index = Number.parseInt(option, 10) - 2;
    const radioButton = await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.weeksNumberSelects().get(
        index));
    await radioButton.Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeStatic(pages.erp.editWorkPatternPage.weeksNumberSelects().get(index));
});

When(/^I type "([^"]*)" in the "(pattern description|number of weeks|day length)" field$/, async (value, fieldName) => {
    const inputElement = await actions.erp.editWorkPatternActions.getWorkPatternInputField(fieldName);
    await inputElement.SetText(value);
    await expectHelpers.expectElementAttributeValueEquals(inputElement, 'value', value, '', true);
});

When(/^I set input field number valid random hours per day for "same random" day$/, async () => {
    testVars.workPattern.generatedHours = actions.erp.editWorkPatternActions.generateWorkPatternData(
        'hours');
    await pages.erp.editWorkPatternPage.workingDayInput(testVars.workPattern.generatedDay).SetText(testVars.workPattern.generatedHours);
});

When(
    /^I set input fields number "(\d{2}:\d{2})" hours per day for "(all|even|odd|random|same random)" day(?:s)?$/,
    async (value, mode) => {
        if (mode === 'all') {
            const actualWorkPatternType = (await pages.erp.editWorkPatternPage.workPatternTypeSelect().getText()).toString().toLowerCase();
            await waitHelpers.waitForElementToBeVisible(pages.erp.editWorkPatternPage.workingDaysInputs(
                actualWorkPatternType).first());
            await pages.erp.editWorkPatternPage.workingDaysInputs(actualWorkPatternType).each(async day => {
                await day.SetText(value);
            });
        } else if (mode === 'even') {
            const actualWorkPatternType = (await pages.erp.editWorkPatternPage.workPatternTypeSelect().getText()).toString().toLowerCase();
            await pages.erp.editWorkPatternPage.workingDaysInputs(actualWorkPatternType).each(async (day, i) => {
                if (i % 7 % 2) {
                    await day.SetText(value);
                }
            });
        } else if (mode === 'odd') {
            const actualWorkPatternType = (await pages.erp.editWorkPatternPage.workPatternTypeSelect().getText()).toString().toLowerCase();
            await pages.erp.editWorkPatternPage.workingDaysInputs(actualWorkPatternType).each(async (day, i) => {
                if (!(i % 7 % 2)) {
                    await day.SetText(value);
                }
            });
        } else if (mode === 'random') {
            const randomDay = actions.erp.editWorkPatternActions.generateWorkPatternData('workingdays');
            await pages.erp.editWorkPatternPage.workingDayInput(randomDay).SetText(value);
            testVars.workPattern.generatedDay = randomDay;
        } else {
            await pages.erp.editWorkPatternPage.workingDayInput(testVars.workPattern.generatedDay).SetText(value);
        }
    }
);

When(
    /^I set "(random|the same random)" hours per day for "(random|same random)" day$/,
    async (mode, input) => {
        if (mode === 'random') {
            testVars.workPattern.generatedHours = actions.erp.editWorkPatternActions.generateWorkPatternData(
                'hours');
        }

        if (input === 'random') {
            const randomDay = actions.erp.editWorkPatternActions.generateWorkPatternData('workingdays');
            await pages.erp.editWorkPatternPage.workingDayInput(randomDay).SetText(testVars.workPattern.generatedHours);
            testVars.workPattern.generatedDay = randomDay;
        } else {
            await pages.erp.editWorkPatternPage.workingDayInput(testVars.workPattern.generatedDay).SetText(testVars.workPattern.generatedHours);
        }
    }
);

When(/^I clear input fields number of hours per day for "all" days$/, async () => {
    await pages.erp.editWorkPatternPage.allDaysInputs().each(async day => {
        await day.Clear();
    });
});

When(/^I type "([^"]*)" in the "(number of weeks|day length)" field if visible$/, async (value, fieldName) => {
    let switchElement;

    if (fieldName === 'number of weeks') {
        switchElement = await actions.erp.editWorkPatternActions.getWorkPatternSwitch('pattern repeats');
    } else if (fieldName === 'day length') {
        switchElement = await actions.erp.editWorkPatternActions.getWorkPatternSwitch('day length same');
    }

    const actualSwitchState = await switchElement.getAttribute('aria-checked');

    if (actualSwitchState === 'true') {
        const inputElement = await actions.erp.editWorkPatternActions.getWorkPatternInputField(fieldName);
        await inputElement.SetText(value);
        await expectHelpers.expectElementAttributeValueEquals(inputElement, 'value', value, '', true);
    }
});

Then(/^I see "([^"]*)" in the "(number of weeks|day length)" field$/, async (expectedValue, fieldName) => {
    const inputElement = await actions.erp.editWorkPatternActions.getWorkPatternInputField(fieldName);
    await expectHelpers.expectElementAttributeValueEquals(inputElement, 'value', expectedValue, '', true);
});


