Then(/^the 'Edit Eligibility criteria' popup opens$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.editCurrentOccupationPage.unauthorizedAlert());
    const title = await waitHelpers.waitForElementToBeVisible(
        pages.erp.editEligibilityCriteriaPage.editEligibilityCriteriaTitle(),
        timeouts.T30,
        'Expect to see form title'
    );
    await expectHelpers.checkTranslation(title, 'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_EDIT');

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.editEligibilityCriteriaPage.actualWorkedHoursLabel(),
        pages.erp.editEligibilityCriteriaPage.actualWorkedHoursInput(),
        pages.erp.editEligibilityCriteriaPage.workStateLabel(),
        pages.erp.editEligibilityCriteriaPage.workStateSelect(),
        pages.erp.editEligibilityCriteriaPage.cbaLabel(),
        pages.erp.editEligibilityCriteriaPage.cbaInput(),
        pages.erp.editEligibilityCriteriaPage.qualifiersRemark()
    ]);

    expect(await pages.erp.editEligibilityCriteriaPage.checkboxes().count()).is.equal(3);
});

Then(/^the 'Edit Eligibility criteria' popup closes$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeNotVisible(
        pages.erp.editEligibilityCriteriaPage.editEligibilityCriteriaTitle(),
        timeouts.T30,
        'Expect to not see form title'
    );
    await waitHelpers.waitForSpinnersToDisappear();
});

When(/^I "(accept|cancel|close X)" 'Edit Eligibility criteria' popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.editCurrentOccupationPage.okButton().Click({waitForVisibility: true});
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.editCurrentOccupationPage.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.editCurrentOccupationPage.closeButton().Click({waitForVisibility: true});
    }
});

When(
    /^I enter (random|the same random) test data in the 'Eligibility' popup "(Actual hours worked|CBA)" field$/,
    async (mode, formFieldLabel) => {
        let formFieldInput;
        const dataType = formFieldLabel.trim().replace(constants.regExps.spaces, '').toLowerCase();

        if (mode === 'random') {
            testVars.employmentDetails[dataType] = await actions.erp.eligibilityCriteriaActions.generateEligibilityCriteriaData(
                dataType);
        }

        switch (formFieldLabel.toString().toLowerCase()) {
            case 'actual hours worked':
                formFieldInput = await pages.erp.editEligibilityCriteriaPage.actualWorkedHoursInput();
                break;
            case 'cba':
                formFieldInput = await pages.erp.editEligibilityCriteriaPage.cbaInput();
                break;
            default:
                throw new Error(`given field name [${formFieldLabel}] is not supported`);
        }

        await formFieldInput.SetText(testVars.employmentDetails[dataType]);
        expect(await formFieldInput.getAttribute('value')).is.equal(testVars.employmentDetails[dataType].toString());
    }
);

When(
    /^I enter "([^"]*)" test data in the 'Eligibility' popup "(Actual hours worked|CBA)" field$/,
    async (text, formFieldLabel) => {
        let formFieldInput;

        switch (formFieldLabel.toString().toLowerCase()) {
            case 'actual hours worked':
                formFieldInput = await pages.erp.editEligibilityCriteriaPage.actualWorkedHoursInput();
                break;
            case 'cba':
                formFieldInput = await pages.erp.editEligibilityCriteriaPage.cbaInput();
                break;
            default:
                throw new Error(`given field name [${formFieldLabel}] is not supported`);
        }

        await formFieldInput.SetText(text);
        expect(await formFieldInput.getAttribute('value')).is.equal(text);
    }
);

When(/^I choose (random|the same random) "Work state"$/, async mode => {
    const selectName = 'workstate';
    await waitHelpers.waitForElementToBeVisible(await pages.erp.editEligibilityCriteriaPage.workStateSelectArrow());
    await pages.erp.editEligibilityCriteriaPage.workStateSelect().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(pages.erp.editEligibilityCriteriaPage.workStateSelectOptions().last());

    const availableOptions = await pages.erp.editEligibilityCriteriaPage.workStateSelectOptions().getText();

    if (mode === 'random') {
        testVars.employmentDetails[selectName] = await actions.erp.eligibilityCriteriaActions.generateEligibilityCriteriaData(
            selectName,
            availableOptions.toString().split(',').splice(1)
        );
    }
    await pages.erp.editCurrentOccupationPage.dropdownListOption(testVars.employmentDetails[selectName]).Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.editCurrentOccupationPage.dropdownListOption(testVars.employmentDetails[selectName]));
    expect(await pages.erp.editEligibilityCriteriaPage.workStateSelect().getText()).is.equal(testVars.employmentDetails[selectName]);
});

When(
    /^I "(check|uncheck)" "(Within FMLA radius|Works at home|Key employee)" checkbox$/,
    async (actionType, checkboxName) => {
        const actualCheckboxState = await pages.erp.editEligibilityCriteriaPage.checkbox(checkboxName).isSelected();
        if (constants.stepWording.CHECK === actionType && actualCheckboxState === false) {
            await pages.erp.editEligibilityCriteriaPage.checkboxLabel(checkboxName).Click();
        } else if (constants.stepWording.CHECK !== actionType && actualCheckboxState === true) {
            await pages.erp.editEligibilityCriteriaPage.checkboxLabel(checkboxName).Click();
        }
    }
);

When(
    /^I (randomly|the same randomly) "check\/uncheck" "(Within FMLA radius|Works at home|Key employee)" checkbox$/,
    async (mode, checkboxName) => {
        const dataType = checkboxName.trim().replace(constants.regExps.spaces, '').toLowerCase();

        if (mode === 'randomly') {
            testVars.employmentDetails[dataType] = await pages.erp.editEligibilityCriteriaPage.checkbox(checkboxName).isSelected();
        }

        const finalCheckboxState = testVars.employmentDetails[dataType];
        const actualCheckboxState = await pages.erp.editEligibilityCriteriaPage.checkbox(checkboxName).isSelected();

        if (finalCheckboxState === true && actualCheckboxState === false) {
            await pages.erp.editEligibilityCriteriaPage.checkboxLabel(checkboxName).Click();
        } else if (finalCheckboxState === false && actualCheckboxState === true) {
            await pages.erp.editEligibilityCriteriaPage.checkboxLabel(checkboxName).Click();
        }
    }
);

Then(/^"Eligibility Criteria" "([^"]*)" fields were refreshed correctly$/, async fieldLabels => {
    fieldLabels = fieldLabels.split(',').map(item => item.trim());

    for (const label of fieldLabels) {
        const dataType = label.trim().replace(constants.regExps.spaces, '').toLowerCase();
        let actualContent;

        switch (label.toString().toLowerCase()) {
            case 'actual hours worked':
                actualContent = await pages.erp.editEligibilityCriteriaPage.actualWorkedHoursInput().getAttribute(
                    'value');
                break;
            case 'cba':
                actualContent = await pages.erp.editEligibilityCriteriaPage.cbaInput().getAttribute(
                    'value');
                break;
            case 'work state':
                actualContent = await pages.erp.editEligibilityCriteriaPage.workStateSelect().getText();
                break;
            default:
                throw new Error(`given field name [${label}] is not supported`);
        }

        expect(actualContent).is.equal(testVars.employmentDetails[dataType].toString());
    }
});

Then(/^"Eligibility Criteria" "([^"]*)" checkboxes were refreshed correctly$/, async checkboxLabels => {
    checkboxLabels = checkboxLabels.split(',').map(item => item.trim());

    for (const label of checkboxLabels) {
        const dataType = label.trim().replace(constants.regExps.spaces, '').toLowerCase();
        const actualCheckboxState = await pages.erp.editEligibilityCriteriaPage.checkbox(label).isSelected();
        const expectedCheckboxState = testVars.employmentDetails[dataType];

        expect(actualCheckboxState).is.equal(expectedCheckboxState);
    }
});
