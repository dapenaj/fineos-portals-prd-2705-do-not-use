const moment = require('moment');

When(/^I select "(All|Incoming|Outgoing|Unread)" in the "Showing" dropdown$/, async optionName => {
    await pages.erp.webMessagesPage.messagesFilterSelect().Click({waitForVisibility: true});
    await pages.erp.webMessagesPage.filterOption(optionName.toString().toUpperCase()).Click({waitForVisibility: true});

    await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().last());
});

When(/^I select "(Newest first|Oldest first)" option in the "Sorting by" dropdown$/, async optionName => {
    optionName = (optionName === 'Newest first') ? 'DESC' : 'ASC';
    await pages.erp.webMessagesPage.messagesSortSelect().Click({waitForVisibility: true});
    await pages.erp.webMessagesPage.sortOption(optionName.toString().toUpperCase()).Click({waitForVisibility: true});

    await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().last());
});

Then(/^I verify the "Showing" dropdown shows option "(All|Incoming|Outgoing|Unread)" messages$/, async optionName => {
    const expectedOptionText = translationsHelper.getTranslation(`MESSAGES.FILTER.OPTIONS.${optionName.toString().toUpperCase()}`);
    const actualOptionText = await pages.erp.webMessagesPage.filterSelectedOption().getText();

    expect(expectedOptionText.trim()).is.equal(actualOptionText);
});

Then(/^I verify the "Sorting by" dropdown shows "(Newest first|Oldest first)" option$/, async optionName => {
    optionName = (optionName === 'Newest first') ? 'DESC' : 'ASC';
    const expectedOptionText = translationsHelper.getTranslation(`COMMON.SORT_BY_DATE_SELECT.${optionName.toString().toUpperCase()}`);
    const actualOptionText = await pages.erp.webMessagesPage.sortSelectedOption().getText();

    expect(expectedOptionText.trim()).is.equal(actualOptionText);
});

When(/^I click the "(first|random)" web message$/, async message => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first());

    if (message === 'random') {
        await testDataHelpers.getRandomArrayElement((await pages.erp.webMessagesPage.messagesList())).Click({waitForVisibility: true});
    } else {
        await (await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first())).Click({waitForVisibility: true});
    }
});

When(/^I verify the Display\/Reply web message popup (closes|opens)$/, async expectedBehaviour => {
    if (expectedBehaviour === 'opens') {
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesDrawer.messageIcon());
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesDrawer.messageSubject());
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesDrawer.messageDate());
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesDrawer.messageText());
        await waitHelpers.waitForElementToBeClickable(pages.erp.sharedModal.closeXButton());
    } else {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.webMessagesDrawer.messageIcon());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.webMessagesDrawer.messageSubject());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.webMessagesDrawer.messageDate());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.webMessagesDrawer.messageText());
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.sharedModal.closeXButton());
    }
});

When(/^I mark (first|last|random) message as "(read|unread)"$/, async (messageType, action) => {
    let message = await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().last());

    if (messageType === 'random') {
        message = testDataHelpers.getRandomArrayElement((await pages.erp.webMessagesPage.messagesList()).splice(
            1));
    } else if (messageType === 'first') {
        message = await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first());
    }

    await message.Scroll();
    const firstMessageColor = await message.getCssValue('border-left-color');
    const isRead = (firstMessageColor === constants.colors.TRANSPARENT);

    if ((!isRead && action === 'read') || (isRead && action === 'unread')) {
        await pages.erp.webMessagesPage.messageActionsButton(message).Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messageReadUnreadButton(message));
        await waitHelpers.waitForElementToBeStatic(pages.erp.webMessagesPage.messageReadUnreadButton(message));
        await pages.erp.webMessagesPage.messageReadUnreadButton(message).Click({waitForVisibility: true});

        await waitHelpers.waitForElementToBeNotVisible(pages.erp.webMessagesPage.messageReadUnreadButton(message));
    }
});

Then(/^I verify messages are sorted by "Contact Date Time" in "(ascending|descending)"order$/, async sortingOrder => {
    const sortQuery = (sortingOrder === 'ascending') ? 'contactDateTime' : '-contactDateTime';
    const actualFilterOption = await actions.erp.webMessagesActions.getActualFilterOption();

    await waitHelpers.waitForElementToBeVisible(await pages.erp.webMessagesPage.messagesList().last());
    await actions.erp.webMessagesActions.showAllUiMessages();
    await actions.erp.webMessagesActions.getAndCompareApiAndUiMessages(actualFilterOption, sortQuery);
});

Then(/^I verify (?:only )?"(All|Incoming|Outgoing|Unread)" messages show in the list$/, async optionName => {
    const actualSortingOption = await actions.erp.webMessagesActions.getActualSortingOption();
    const sortQuery = (actualSortingOption.toString().toLowerCase().includes('asc')) ? 'contactDateTime' : '-contactDateTime';

    await waitHelpers.waitForElementToBeVisible(await pages.erp.webMessagesPage.messagesList().last());
    await actions.erp.webMessagesActions.showAllUiMessages();
    await actions.erp.webMessagesActions.getAndCompareApiAndUiMessages(optionName, sortQuery);
});

Then(/^I verify the first message is highlighted, opened and contents are displayed$/, async () => {
    let firstMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first());
    await firstMessage.Scroll();

    await expectHelpers.expectElementCssValueEquals(
        firstMessage,
        'background-color',
        constants.stateColors.HIGHLIGHTED,
        ''
    );

    firstMessage = await firstMessage.getText();

    expect(firstMessage).contains(await pages.erp.webMessagesPage.openedMessageSubject().getText());
    expect(firstMessage).contains(await pages.erp.webMessagesPage.openedMessageText().getText());
    expect(firstMessage).contains(await pages.erp.webMessagesPage.openedMessageRelatesTo().getText());
    expect(firstMessage).contains((await pages.erp.webMessagesPage.openedMessageDateTime().getText()).toString().split(
        '-')[0].trim());
});

When(/^I click the "(Employee|Relates to)" hyperlink on the random web message in the list$/, async linkName => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().last());
    const randomMessage = testDataHelpers.getRandomArrayElement((await pages.erp.webMessagesPage.messagesList()));

    linkName = linkName.trim().toLowerCase().replace(constants.regExps.spaces, '-');

    const link = await pages.erp.webMessagesPage.messagesListLink(randomMessage, linkName);
    testVars.actualLinkData = await link.getText();
    await link.Click({waitForVisibility: true});

    await waitHelpers.waitForSpinnersToDisappear();
});

Then(/^I verify each message on the list shows the "([^"]*)" fields$/, async listOfFields => {
    const expectedFields = listOfFields.split(',').map(item => item.trim().toLowerCase().replace(
        constants.regExps.spaces,
        '-'
    ));

    await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().last());
    await pages.erp.webMessagesPage.messagesList().each(async message => {
        for (const field of expectedFields) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesListElement(
                message,
                field
            ));
        }
    });
});

Then(
    /^I verify each message has a "Contact Date" field Displayed in the format "(MMM DD, YYYY|YYYY-MM-DD)" without the message sent time$/,
    async dateFormat => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first());
        await pages.erp.webMessagesPage.messagesList().each(async message => {
            const messageDate = await (await waitHelpers.waitForElementToBeStatic(pages.erp.notificationPage.messageDate(
                message))).getText();
            const actualDate = moment(messageDate, dateFormat, true).toDate();
            moment(moment(actualDate).format(dateFormat), dateFormat, true).isValid();
        });
    }
);

Then(
    /^I verify only unread web messages have highlighted left side bar line and the Sender badge is not greyed out$/,
    async () => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first());

        await pages.erp.webMessagesPage.messagesList().each(async message => {
            const msgState = await message.getAttribute('class');
            const messageBoarderLeftColor = await message.getCssValue('border-left-color');
            const messageIconColor = await (await
            waitHelpers.waitForElementToBeStatic(pages.erp.notificationPage.messageIcon(message))).getCssValue(
                'color');

            if (msgState.toLowerCase().includes('unread')) {
                expect(messageBoarderLeftColor).contains(constants.colors.PURPLE);
                expect(messageIconColor).contains(constants.colors.PURPLE);
            } else {
                expect(messageBoarderLeftColor).contains(constants.colors.TRANSPARENT);
                expect(messageIconColor).contains(constants.colors.GREY);
            }
        });
    }
);

Then(/^I verify first web message is marked as (read|unread)$/, async expectedState => {
    const firstMessage = await waitHelpers.waitForElementToBeVisible(pages.erp.webMessagesPage.messagesList().first());
    const messageBoarderLeftColor = await firstMessage.getCssValue('border-left-color');
    const messageIconColor = await (await
    waitHelpers.waitForElementToBeStatic(pages.erp.notificationPage.messageIcon(firstMessage))).getCssValue(
        'color');

    if (expectedState === 'read') {
        expect(messageBoarderLeftColor).contains(constants.colors.TRANSPARENT);
        expect(messageIconColor).contains(constants.colors.GREY);
    } else {
        expect(messageBoarderLeftColor).contains(constants.colors.PURPLE);
        expect(messageIconColor).contains(constants.colors.PURPLE);
    }
});

