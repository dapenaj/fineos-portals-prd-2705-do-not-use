Given(/^I log in to the application$/, async () => {
    await actions.erp.loginActions.logIn();
});

Given(/^I re-login the application with "([^"]*)"$/, async descriptor => {
    await pages.erp.searchPage.userMenu().Click({waitForVisibility: true});
    await actions.erp.loginActions.logOut();

    await pages.erp.logOutPopUpPage.takeMeToLoginButton().Click({waitForVisibility: true});

    await actions.erp.loginActions.signOut();
    await actions.erp.loginActions.logIn(descriptor);
});

Given(/^I log in to the application with "([^"]*)"$/, async descriptor => {
    await actions.erp.loginActions.logIn(descriptor);
});

Given(/^I log in to the application with "([^"]*)" using new browser window$/, async descriptor => {
    await actions.erp.loginActions.createNewWindowAndSwitchTo();

    if (browser.params.env.name !== 'localhost') {
        await actions.erp.loginActions.signOut();
    }

    await actions.erp.loginActions.logIn(descriptor);
});

When(/^I switch to "(first|second)" window$/, async windowIndex => {
    windowIndex = (windowIndex === 'first') ? 0 : 1;
    await actions.erp.loginActions.switchToWindow(windowIndex);
});

When(
    /^I log in to the application as a user "(with|without)" permissions to "([^"]*)"$/,
    async (withOrWithout, descriptor) => {
        await actions.erp.loginActions.logIn();
    }
);

When(/^I log out the application$/, async () => {
    await pages.erp.searchPage.userMenu().Click({waitForVisibility: true});
    await actions.erp.loginActions.logOut();
});

Then(/^I see popup informing that user was successfully log out$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.logOutPopUpPage.popUpTitle());
    await expectHelpers.checkTranslation(await pages.erp.logOutPopUpPage.popUpTitle(), 'LOGOUT.USER_LOGGED_OUT');
    await expectHelpers.checkTranslation(await pages.erp.logOutPopUpPage.popUpBody(), 'LOGOUT.LOGGED_BODY');
    await expectHelpers.checkTranslation(await pages.erp.logOutPopUpPage.takeMeToLoginButton(), 'LOGOUT.TAKE_ME_LOGIN');
});

Then(/^I see a loading spinner with message "We're logging you out\.\.\." for a moment$/, async () => {
    await expectHelpers.checkTranslation(await waitHelpers.waitForElementToBeVisible(pages.erp.logOutPopUpPage.logOutSpinner()), 'LOGOUT.DURING_LOGOUT');
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.logOutPopUpPage.logOutSpinner());
});

When(/^I refresh the page$/, async () => {
    await browser.navigate().refresh();
});

When(/^I confirm popup by clicking "Take me to login" button$/, async () => {
    await pages.erp.logOutPopUpPage.takeMeToLoginButton().Click({waitForVisibility: true});
});

When(/^I sign out current user on Fineoscloud IDP$/, async () => {
    await actions.erp.loginActions.signOut();
});

Then(/^I am redirected to login page$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.loginPage.signInStatusLabel());
    await waitHelpers.waitForElementToBeVisible(pages.erp.loginPage.signOutCurrentSiteRadioButton());
    await waitHelpers.waitForElementToBeVisible(pages.erp.loginPage.buttonSignOut());
});


