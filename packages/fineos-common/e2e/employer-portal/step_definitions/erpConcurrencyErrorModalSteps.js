When(/^I can see concurrency error pop up$/, async () => {
    const title = await waitHelpers.waitForElementToBeVisible(pages.erp.concurrencyErrorModal.title());
    const description = await waitHelpers.waitForElementToBeVisible(pages.erp.concurrencyErrorModal.description());
    await waitHelpers.waitForElementToBeVisible(pages.erp.concurrencyErrorModal.image());
    await waitHelpers.waitForElementToBeVisible(pages.erp.concurrencyErrorModal.refreshButton());

    await expectHelpers.checkTranslation(title, 'DATA_CONCURRENCY_MODAL.TITLE');
    await expectHelpers.checkTranslation(description, 'DATA_CONCURRENCY_MODAL.DESCRIPTION');
});

Then(/^I see the concurrency error pop up closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.concurrencyErrorModal.title());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.concurrencyErrorModal.description());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.concurrencyErrorModal.image());
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.concurrencyErrorModal.refreshButton());
});

When(/^I click Refresh on the concurrency error pop up$/, async () => {
    await pages.erp.concurrencyErrorModal.refreshButton().Click({waitForVisibility: true});
});
