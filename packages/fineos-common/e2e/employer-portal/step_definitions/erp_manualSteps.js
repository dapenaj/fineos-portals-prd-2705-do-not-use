/* eslint-disable no-empty-function */

const {Then} = require('cucumber');
Then(/^I see field "([^"]*)" in page header has default value: "([^"]*)"$/, function () {
});
Given(/^notifications are available for user with undefined "([^"]*)"$/, function () {
});
When(/^I open any notification of user with correct 'Job Title', 'Organisation Unit', 'Work Site'$/, function () {
});
Then(/^All fields in notification header are populated with correct data$/, function () {
});
When(/^I skip "([^"]*)" and proceed to submit cancellation request$/, async sectionElement => {
});
When(/^I verify that same leave periods in different leave plans were merged to one absence \(list item\)$/,
    async () => {
    }
);
When(/^I verify that only one period is listed$/, async () => {
});
Then(/^I should see job protection card in empty state with assessment details$/, async () => {
});
Then(/^I can customize assessment ongoing messages$/, async () => {
});
Then(/^I should see job protection card in alternative empty state$/, async () => {
});
Then(/^I can customize alternative empty state messages$/, async () => {
});
When(/^I expand Leave Plan$/, async () => {
});
Then(/^Status Indicator displays "([^"]*)" status$/, async () => {
});
Then(/^the period in the middle is "([^"]*)"$/, async () => {
});
When(/^I open notification with consecutive child leaves and "([^"]*)"$/, async () => {
});
Then(/^I verify that the periods are "([^"]*)"$/, async () => {
});
When(/^I open notification with approved monthly child leave plan$/, async () => {
});
Then(/^I verify that the atomic periods are merged into one leave$/, async () => {
});
Then(/^a change of the Employee’s details should be stored in system$/, async () => {
});
Then(/^No intake will be created$/, async () => {
});
Then(/^the input values are sent to the carrier$/, async () => {
});
