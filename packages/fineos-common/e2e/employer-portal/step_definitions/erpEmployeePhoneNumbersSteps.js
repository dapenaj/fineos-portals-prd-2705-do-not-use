const faker = require('faker');
const {getRandomArrayIndex} = require('../../common/helpers/mathHelper');

Then(
    /^the "(Add|Edit)" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title$/,
    async action => {
        await actions.erp.employeeProfileActions.checkAddEditPhoneNumbersPopupElements(action);
    }
);

Then(/^the "(Add|Edit)" "Phone numbers" popup is still visible$/, async action => {
    await actions.erp.employeeProfileActions.checkAddEditPhoneNumbersPopupElements(action);
});

Then(
    /^the Edit "Phone numbers" popup for each phone number item should contain the editable "([^"]*)" fields$/,
    async fieldLabels => {
        const phoneFieldsArray = fieldLabels.split(',').map(f => f.trim());

        for (const field of phoneFieldsArray) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.editNumbersInputs(field).first());
            await pages.erp.employeePhoneNumbersPage.editNumbersInputs(field).each(async input => {
                expect(await input.isEnabled()).equal(true);
            });
        }
    }
);

Then(
    /^the Edit "Phone numbers" popup for each phone number item should contain the editable "Phone Contact Type" select$/,
    async () => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.phoneTypeSelects().first());
        await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().each(async input => {
            expect(await input.isEnabled()).equal(true);
            expect(await input.getAttribute('value').toString() !== '');
        });
    }
);

Then(
    /^the Edit "Phone numbers" popup labels "(have|do not have)" "(\*)" on the fields "([^"]*)"$/,
    async (isRequired, mandatoryCharacter, fieldLabels) => {
        const phoneFieldsArray = fieldLabels.split(',').map(f => f.trim());

        for (const field of phoneFieldsArray) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.phoneDetailsCards().first());
            await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().each(async phoneCard => {
                if (isRequired === constants.stepWording.HAVE) {
                    expect(await phoneCard.getText()).contains(field + mandatoryCharacter);
                } else {
                    expect(await phoneCard.getText()).contains(`${field}\n`);
                }
            });
        }
    }
);

Then(
    /^I enter (random|the same random) number in the "(last|random)" "([^"]*)" fields and select "(Cell Phone|Landline)" phone type$/,
    async (mode, phoneNumberCard, fieldLabels, phoneContactType) => {
        let phoneNumberIndex = await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().count() - 1;
        if (phoneNumberCard === 'random') {
            phoneNumberIndex = faker.random.number(await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().count() - 1);
        }
        const phoneFieldsArray = fieldLabels.split(',').map(f => f.trim());
        const phoneNumbersCards = await pages.erp.employeePhoneNumbersPage.phoneDetailsCards();
        await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().get(phoneNumberIndex).Click({waitForVisibility: true});

        if (phoneContactType === 'Landline') {
            await pages.erp.employeePhoneNumbersPage.landLineSelectOption(phoneNumbersCards[phoneNumberIndex]).Click({waitForVisibility: true});
        } else {
            await pages.erp.employeePhoneNumbersPage.cellPhoneSelectOption(phoneNumbersCards[phoneNumberIndex]).Click({waitForVisibility: true});
        }

        expect(await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().get(phoneNumberIndex).getText()).is.equal(
            phoneContactType);

        for (const field of phoneFieldsArray) {
            const dataType = field.trim().replace(constants.regExps.spaces, '').toLocaleLowerCase();

            if (mode === 'random') {
                testVars.employeePersonalDetails[dataType] = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
                    dataType);
            }

            const formField = await pages.erp.employeePhoneNumbersPage.editNumbersInputs(field).get(
                phoneNumberIndex);
            await formField.Clear();
            await formField.SendText(testVars.employeePersonalDetails[dataType]);

            expect(await formField.getAttribute('value')).is.equal(testVars.employeePersonalDetails[dataType].toString());
        }
    }
);

When(/^I enter "([^"]*)" test data in last "([^"]*)" text box$/, async (testData, fieldLabel) => {
    const formField = await pages.erp.employeePhoneNumbersPage.editNumbersInputs(fieldLabel).last();
    await formField.Clear();
    await formField.SendText(testData);

    expect(testData).is.equal(await pages.erp.employeePhoneNumbersPage.editNumbersInputs(fieldLabel).last().getAttribute(
        'value'));
});

Then(
    /^the Employee has at least (\d) "(Landline|Cell Phone|Any)" "Phone number(?:s)?"$/,
    async (expectedNumberOfPhoneNumbers, phoneType) => {
        let initialNumberOfPhoneNumbers;

        await waitHelpers.waitForElementToBeVisible(await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().first());
        if (phoneType === 'Any') {
            initialNumberOfPhoneNumbers = await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().count();
        } else {
            initialNumberOfPhoneNumbers = await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().filter(async select => (await select.getText()).toString().includes(
                phoneType)).count();
        }

        if (initialNumberOfPhoneNumbers < expectedNumberOfPhoneNumbers) {
            for (let i = 0; i < (expectedNumberOfPhoneNumbers - initialNumberOfPhoneNumbers); i++) {
                const firstPhoneInputValue = (await pages.erp.employeePhoneNumbersPage.editNumbersInputs('Phone number').first().getAttribute(
                    'value')).toString();

                if (firstPhoneInputValue) {
                    await waitHelpers.waitForElementToBeVisible(await pages.erp.employeePhoneNumbersPage.addPhoneNumberButton());
                    await pages.erp.employeePhoneNumbersPage.addPhoneNumberButton().Click({waitForVisibility: true});
                }

                await actions.erp.employeeProfileActions.choosePhoneType(phoneType);
                await actions.erp.employeeProfileActions.addRandomPhoneNumber();
            }
        }
        testVars.employeePersonalDetails.phoneNumberInputs = (await actions.erp.employeeProfileActions.getAllPhoneNumbers()).toString();
    }
);

When(/^I "(accept|cancel|close X)" "Edit Phone numbers" popup$/, async button => {
    if (button === constants.popUpActions.ACCEPT) {
        await pages.erp.employeePhoneNumbersPage.okButton().Click({waitForVisibility: true});
        await waitHelpers.waitForSpinnersToDisappear();
    } else if (button === constants.popUpActions.CANCEL) {
        await pages.erp.employeePhoneNumbersPage.cancelButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.employeePhoneNumbersPage.closeButton().Click({waitForVisibility: true});
    }
});

Then(/^the "Edit Phone numbers" popup closes$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(
        pages.erp.employeePhoneNumbersPage.editPhoneNumbersTitle(),
        timeouts.T30,
        'Expect to not see form title'
    );
});

Then(
    /^the "Phone numbers" should contain list of phone numbers from the recorded values in editable "Phone numbers" fields$/,
    async () => {
        const recordedPhoneNumbers = testVars.searchTarget.personalDetailsTabPhones.toString().replace(
            constants.regExps.newLine,
            ','
        );

        await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.phoneDetailsCards().first());
        const actualPhoneNumbers = await actions.erp.employeeProfileActions.getAllPhoneNumbers();
        expect(actualPhoneNumbers.toString()).is.equal(recordedPhoneNumbers);
    }
);

When(/^I click "Phone Contact Type" select$/, async () => {
    await pages.erp.employeePhoneNumbersPage.phoneTypeSelects().first().Click({waitForVisibility: true});
});

Then(/^the dropdown should contain "Landline" and "Cell Phone"$/, async () => {
    const phoneCard = await pages.erp.employeePhoneNumbersPage.phoneDetailsCards().first();
    await pages.erp.employeePhoneNumbersPage.landLineSelectOption(phoneCard);
    await pages.erp.employeePhoneNumbersPage.cellPhoneSelectOption(phoneCard);
});

Then(/^I click the delete icon beside random phone number$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.phoneDetailsCards().first());
    const originalPhoneNumbers = await actions.erp.employeeProfileActions.getAllPhoneNumbers();
    const randomPhoneNumberIndex = getRandomArrayIndex(await pages.erp.employeePhoneNumbersPage.phoneDetailsCards());
    await pages.erp.employeePhoneNumbersPage.deletePhoneNumberButtons().get(randomPhoneNumberIndex).Click({waitForVisibility: true});

    testVars.employeePersonalDetails.finalPhoneNumbers = originalPhoneNumbers.toString().split(',').filter(
        (e, i) => i !== randomPhoneNumberIndex);
});

Then(
    /^I see the deleted phone number is removed from the list of "Phone number\(s\)" on the "Personal Details" tab$/,
    async () => {
        const expectedPhoneNumbers = testVars.employeePersonalDetails.finalPhoneNumbers;
        let actualPhoneNumbers = await (await waitHelpers.waitForElementToBeVisible(pages.erp.employeeProfilePage.personalDetailsPhones())).getText();
        actualPhoneNumbers = actualPhoneNumbers.replace(constants.regExps.newLine, ',');

        expect(expectedPhoneNumbers.toString()).equals(actualPhoneNumbers);
    }
);

Then(
    /^I see the "\+Add another" link is "(enabled|disabled)" under the phone number text box field$/,
    async isEnabled => {
        const addAnotherPhoneNumberButton = await waitHelpers.waitForElementToBeVisible(pages.erp.employeePhoneNumbersPage.addPhoneNumberButton());
        await expectHelpers.checkTranslation(
            addAnotherPhoneNumberButton,
            'PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_PHONE'
        );
        await browser.sleep(1000);
        if (isEnabled === constants.stepWording.ENABLED) {
            expect(constants.stateColors.ACTIVE_2).is.equal(await pages.erp.employeePhoneNumbersPage.addPhoneNumberButton().getCssValue(
                'color'));
        } else {
            expect(constants.stateColors.INACTIVE_2).is.equal(await pages.erp.employeePhoneNumbersPage.addPhoneNumberButton().getCssValue(
                'color'));
        }
    }
);

When(/^I click the "\+Add another" phone number link$/, async () => {
    await pages.erp.employeePhoneNumbersPage.addPhoneNumberButton().Click({waitForVisibility: true});
});

When(/^I see another the phone number empty text boxes fields added to the list of phone numbers$/, async () => {
    const originalPhoneNumbers = testVars.employeePersonalDetails.phoneNumberInputs.toString().split(',');
    const actualPhoneNumbers = await actions.erp.employeeProfileActions.getAllPhoneNumbers();
    expect(actualPhoneNumbers.length - 1).is.equal(originalPhoneNumbers.length);

    const lastPhoneNumber = actualPhoneNumbers[actualPhoneNumbers.length - 1];
    expect(await lastPhoneNumber).is.empty;
});

Then(/^I verify that "Phone number\(s\)" fields on Edit Personal Details page are not updated$/, async () => {
    const actualData = await pages.erp.employeeProfilePage.personalDetailsPhones().getText();
    expect(actualData.toString()).is.equal(testVars.searchTarget.personalDetailsTabPhones.toString());
});

Then(
    /^"(required|not numeric)" phone number error messages is "(visible|not visible)"$/,
    async (errorType, visibility) => {
        const errorName = errorType.trim().replace(constants.regExps.spaces, '_').toUpperCase();

        if (visibility === constants.stepWording.VISIBLE) {
            await waitHelpers.waitForElementToBeVisible(
                pages.erp.employeePhoneNumbersPage.phoneNumberErrorMessage(errorName),
                timeouts.T30,
                'Expect to see form phone number format error'
            );

            await expectHelpers.checkTranslation(
                await pages.erp.employeePhoneNumbersPage.phoneNumberErrorMessage(errorName),
                `FINEOS_COMMON.VALIDATION.${errorName}`
            );
        } else {
            await waitHelpers.waitForElementToBeNotVisible(
                pages.erp.employeePhoneNumbersPage.phoneNumberErrorMessage(errorName),
                timeouts.T30,
                'Expect to not see form phone number format error'
            );
        }
    }
);

Then(/^I verify last "(Area code|Country code|Phone number)" input is refreshed$/, async fieldName => {
    const actualPhoneNumber = await pages.erp.employeePhoneNumbersPage.editNumbersInputs(fieldName).last().getAttribute(
        'value');

    fieldName = fieldName.trim().replace(constants.regExps.spaces, '').toLocaleLowerCase();
    const expectedPhoneNumber = testVars.employeePersonalDetails[fieldName];

    expect(expectedPhoneNumber.toString()).is.equal(actualPhoneNumber.toString());
});
