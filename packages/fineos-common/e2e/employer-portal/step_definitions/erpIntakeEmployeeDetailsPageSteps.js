const faker = require('faker');

When(/^I should see "([^"]*)" section$/, async pageName => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.employeeDetailsSection());
});

Then(/^the page contains following mandatory fields "([^"]*)"$/, async mandatoryFields => {
    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeEmployeeDetailsPage.intakeFirstName(),
        pages.erp.intakeEmployeeDetailsPage.intakeLastName(),
        pages.erp.intakeEmployeeDetailsPage.areaCodeInput(),
        pages.erp.intakeEmployeeDetailsPage.phoneNumberInput(),
        pages.erp.intakeEmployeeDetailsPage.addressLine1(),
        pages.erp.intakeEmployeeDetailsPage.cityInput(),
        pages.erp.intakeEmployeeDetailsPage.stateDropDown(),
        pages.erp.intakeEmployeeDetailsPage.postalCodeInput()
    ]);
});

Then(/^the page contains following optional fields for not existing employee "([^"]*)"$/, async fields => {
    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeEmployeeDetailsPage.emailField(),
        pages.erp.intakeEmployeeDetailsPage.countryCodeInput(),
        pages.erp.intakeEmployeeDetailsPage.addressLine2(),
        pages.erp.intakeEmployeeDetailsPage.addressLine3()
    ]);
});

Then(/^the page contains following optional fields for existing employee"([^"]*)"$/, async fields => {
    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeEmployeeDetailsPage.emailField(),
        pages.erp.intakeEmployeeDetailsPage.countryCodeInput(),
        pages.erp.intakeEmployeeDetailsPage.addressLine2(),
        pages.erp.intakeEmployeeDetailsPage.addressLine3(),
        pages.erp.intakeOccupationAndEarnings.jobTitleInput()
    ]);
    await verifyFields();
});

Then(
    /^I verify that "(Save and proceed|Proceed|Finish and submit|Confirm and proceed)" button is enabled$/,
    async buttonName => {
        if (buttonName === 'Save and proceed') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.saveAndProceed());
            expect(await pages.erp.intakeEmployeeDetailsPage.saveAndProceed().isEnabled()).is.equal(true);
        } else if (buttonName === 'Proceed') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.proceedButton());
            expect(await pages.erp.intakeEmployeeDetailsPage.proceedButton().isEnabled()).is.equal(true);
        } else if (buttonName === 'Confirm and proceed') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton());
            expect(await pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton().isEnabled()).is.equal(true);
        } else if (buttonName === 'Finish and submit') {
            await waitHelpers.waitForElementToBeVisible(await pages.erp.initialRequestPage.finishAndSubmitBtn());
        }
    }
);

async function fillAllIntakeData() {
    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.intakeEmployeeDetailsPage.intakeFirstName().SetText(testVars.searchTarget.firstName);
    await pages.erp.intakeEmployeeDetailsPage.intakeLastName().SetText(testVars.searchTarget.lastName);
    await pages.erp.intakeEmployeeDetailsPage.areaCodeInput().Click({waitForVisibility: true});
    await pages.erp.intakeEmployeeDetailsPage.areaCodeInput().SetText(testVars.searchTarget.areaCode);
    const [phoneNumber] = testVars.searchTarget.phoneNumbers;
    const phoneDetails = phoneNumber.split('-');
    await pages.erp.intakeEmployeeDetailsPage.phoneNumberInput().SetText(phoneDetails[2]);
    await pages.erp.intakeEmployeeDetailsPage.addressLine1().SetText(testVars.searchTarget.addressLines[0]);
    await pages.erp.intakeEmployeeDetailsPage.cityInput().SetText(testVars.searchTarget.addressLines[3]);
    await pages.erp.intakeEmployeeDetailsPage.postalCodeInput().SetText(testVars.searchTarget.addressLines[4]);
    await actions.erp.intakeActions.selectRandomStateOrProvince('State');
}

When(/^I proceed to "(\(2\) Occupation & Earnings|\(3\) Initial Request)" section$/, async pageName => {
    await fillAllIntakeData();
    await pages.erp.intakeEmployeeDetailsPage.proceedButton().Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
});

Then(
    /^I verify that "(Frequency|Work pattern)" drop down contains options: "([^"]*)"$/,
    async (dropdown, fields) => {
        await waitHelpers.waitForSpinnersToDisappear(15);
        const dropdownOptions = fields.split(',');
        await pages.erp.intakeOccupationAndEarnings.jobTitleInput().SetText(testVars.searchTarget.jobTitle);
        if (dropdown === 'Frequency') {
            await waitHelpers.waitForElementToBeClickable(pages.erp.intakeOccupationAndEarnings.frequencyDropDown());
            await waitHelpers.waitForElementToBeStatic(pages.erp.intakeOccupationAndEarnings.frequencyDropDown());
            await pages.erp.intakeOccupationAndEarnings.frequencyDropDown().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.frequencyOptions().first());
            await waitHelpers.waitForElementToBeClickable(pages.erp.intakeOccupationAndEarnings.frequencyOptions().first());
            expect(dropdownOptions[0]).to.include(await pages.erp.intakeOccupationAndEarnings.pleaseSelect().first().getText());
            expect(dropdownOptions[1]).to.include(await pages.erp.intakeOccupationAndEarnings.weekly().getText());
            expect(dropdownOptions[2]).to.include(await pages.erp.intakeOccupationAndEarnings.monthly().getText());
            expect(dropdownOptions[3]).to.include(await pages.erp.intakeOccupationAndEarnings.yearly().getText());
        } else if (dropdown === 'Work pattern') {
            await waitHelpers.waitForElementToBeClickable(pages.erp.intakeOccupationAndEarnings.workPatternField());
            await pages.erp.intakeOccupationAndEarnings.workPatternField().Click({waitForVisibility: true});
            expect(dropdownOptions[0]).to.include(await pages.erp.intakeOccupationAndEarnings.pleaseSelect().first().getText());
            expect(dropdownOptions[1]).to.include(await pages.erp.intakeOccupationAndEarnings.workPattern(
                dropdownOptions[1]).getText());
            expect(dropdownOptions[2]).to.include(await pages.erp.intakeOccupationAndEarnings.workPattern(
                dropdownOptions[2]).getText());
            expect(dropdownOptions[3]).to.include(await pages.erp.intakeOccupationAndEarnings.workPattern(
                dropdownOptions[3]).getText());
        }
    }
);

Then(/^I should be able to proceed to "(Initial Request)" section$/, async pageName => {
    await pages.erp.intakeEmployeeDetailsPage.proceedButton().Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.initialRequestHeader().first());
});

When(
    /^I proceed to "(\(2\) Occupation & Earnings|\(3\) Initial Request)" section for existing employee$/,
    async pageName => {
        await waitHelpers.waitForSpinnersToDisappear();
        if (pageName === '(2) Occupation & Earnings') {
            await waitHelpers.waitForElementToBeVisible(
                pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton(),
                timeouts.T1
            ).then(async () => {
                await pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton().Click({waitForVisibility: true});
            }, async () => {
                await pages.erp.intakeEmployeeDetailsPage.saveAndProceed().Click({waitForVisibility: true});
            });
        } else if (pageName === '(3) Initial Request') {
            await waitHelpers.waitForSpinnersToDisappear();
            try {
                await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.existingEmployeeDescription());
                await waitHelpers.waitForSpinnersToDisappear();
                await pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton().Click({waitForVisibility: true});
                await pages.erp.intakeOccupationAndEarnings.proceedButton().Click({waitForVisibility: true});
            } catch (NoSuchElementError) {
                await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeOccupationAndEarnings.existingEmployeeDescription());
                await pages.erp.intakeEmployeeDetailsPage.proceedButton().Click({waitForVisibility: true});
            }
        }
    }
);

Then(/^the earnings details are hidden by default$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.earningDetailsSection());
});

Then(/^I can click on the Show Earnings link to see the value$/, async () => {
    await pages.erp.intakeOccupationAndEarnings.showEarningsLink().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.earningDetailsSection());
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.hideEarningsLink());
});

Then(/^the section contains following fields "([^"]*)"$/, async fields => {
    fields = fields.split(',').map(item => item.trim());
    await waitHelpers.waitForSpinnersToDisappear();

    for (let field of fields) {
        field = field.toUpperCase().replace(constants.regExps.spaces, '_');
        const expectedFieldTxt = await translationsHelper.getTranslation(`INTAKE.OCCUPATION_AND_EARNINGS.${field}`);
        await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeOccupationAndEarnings.occupationAndEarningsField(
            field));
        const actualFieldTxt = await pages.erp.intakeOccupationAndEarnings.occupationAndEarningsField(field).getText();

        expect(expectedFieldTxt).to.include(actualFieldTxt);
    }
});

Then(/^I should see list of notification reasons radio buttons$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.notificationReasonRadioButtons().first());
    expect(await pages.erp.initialRequestPage.notificationReasonRadioButtons().count()).is.equal(8);

    await pages.erp.initialRequestPage.notificationReasonRadioButtons().each(async radioButton => {
        const translationPath = await radioButton.getAttribute('data-test-el');
        await expectHelpers.checkTranslation(radioButton, translationPath);
    });
});

Then(
    /^the initial request page labels "(have|do not have)" "(\*)" on the fields "([^"]*)"$/,
    async (isRequired, mandatoryCharacter, labels) => {
        const labelNames = labels.split(',').map(f => f.trim().replace(/\s+/g, '_').toUpperCase());

        for (const labelName of labelNames) {
            let expectedLabel = await translationsHelper.getTranslation(`INTAKE.INITIAL_REQUEST.${labelName}`);

            if (isRequired === constants.stepWording.HAVE) {
                expectedLabel += mandatoryCharacter;
            }

            let actualLabel = await pages.erp.initialRequestPage.formLabel(labelName).getText();
            actualLabel = actualLabel.toString().replace(constants.regExps.newLine, '');

            expect(expectedLabel).is.equal(actualLabel);
        }
    }
);

When(/^I submit intake$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.initialRequestPage.finishAndSubmitBtn().Click({waitForVisibility: true});
});

Then(/^I should see acknowledgement screen$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeAcknowledgmentScreen.confirmationScreen());
});

Then(/^acknowledgement screen should contain reference to notification ID "([^"]*)"$/, async ntnString => {
    await waitHelpers.waitForSpinnersToDisappear();
    expect(await pages.erp.intakeAcknowledgmentScreen.notificationNumber().getText()).to.include(ntnString);
});

When(/^I close acknowledgement screen$/, async () => {
    await actions.erp.intakeActions.closeAcknowledgmentScreen();
});

When(/^I select "([^"]*)" notification reason$/, async reason => {
    const modifiedReason = reason.toString().toUpperCase().replace('/', 'OR').replace(',', '').replace(/\s+/g, '_');
    switch (reason) {
        case 'Sickness / time required for a medical treatment or procedure':
            await pages.erp.initialRequestPage.reasonRadioBtn(
                'SICKNESS_TREATMENT_REQUIRED_FOR_A_MEDICAL_CONDITION_OR_ANY_OTHER_MEDICAL_PROCEDURE').Click({waitForVisibility: true});
            break;
        case 'Child bonding':
            await pages.erp.initialRequestPage.reasonRadioBtn('BONDING_WITH_A_NEW_CHILD').Click({waitForVisibility: true});
            break;
        case 'Time required for another reason':
            await pages.erp.initialRequestPage.reasonRadioBtn('OUT_OF_WORK_FOR_ANOTHER_REASON').Click({waitForVisibility: true});
            break;
        default:
            await pages.erp.initialRequestPage.reasonRadioBtn(modifiedReason).Click({waitForVisibility: true});
    }
});

When(/^I select random date$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    const lastDay = await actions.erp.intakeActions.generateLastDay();
    await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.calendarField());
    await waitHelpers.waitForElementToBeClickable(pages.erp.initialRequestPage.calendarField());
    await pages.erp.initialRequestPage.calendarField().SetText(lastDay + protractor.Key.ENTER);
});

Then(
    /^the "(Occupation & Earnings|Employee's Details)" page contains following optional fields "([^"]*)"$/,
    async (pageName, fields) => {
        if (pageName === 'Occupation & Earnings') {
            await verifyFields();
        } else if (pageName === 'Employee\'s Details') {
            await expectHelpers.expectAllElementsToBeVisible([
                pages.erp.intakeEmployeeDetailsPage.emailField(),
                pages.erp.intakeEmployeeDetailsPage.countryCodeInput(),
                pages.erp.intakeEmployeeDetailsPage.addressLine2(),
                pages.erp.intakeEmployeeDetailsPage.addressLine3()
            ]);
        }
    }
);

Then(/^I verify the number of hours is 8:00 per day and the employee works a standard week \(5d\)$/, async () => {
    expect(await pages.erp.intakeOccupationAndEarnings.hoursPerDayFields().count()).to.be.equal(5);
    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeOccupationAndEarnings.weekDay('monday'),
        pages.erp.intakeOccupationAndEarnings.weekDay('tuesday'),
        pages.erp.intakeOccupationAndEarnings.weekDay('wednesday'),
        pages.erp.intakeOccupationAndEarnings.weekDay('thursday'),
        pages.erp.intakeOccupationAndEarnings.weekDay('friday')
    ]);
});

When(/^I select Work pattern as "([^"]*)"$/, async elementName => {
    await pages.erp.intakeOccupationAndEarnings.jobTitleInput().SetText(testVars.searchTarget.jobTitle);
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeOccupationAndEarnings.workPatternDropDownArrow());
    await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeOccupationAndEarnings.workPatternDropDownArrow());
    await waitHelpers.waitForElementToBeClickable(pages.erp.intakeOccupationAndEarnings.workPatternField());
    await waitHelpers.waitForElementToBeStatic(pages.erp.intakeOccupationAndEarnings.workPatternField());
    await pages.erp.intakeOccupationAndEarnings.workPatternField().Click({waitForVisibility: true});
    await pages.erp.intakeOccupationAndEarnings.workPattern(elementName).Click({waitForVisibility: true});
});

Then(/^I should see employee's details: "([^"]*)"$/, async fields => {
    await waitHelpers.waitForSpinnersToDisappear();
    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeEmployeeDetailsPage.firstName(),
        pages.erp.intakeEmployeeDetailsPage.lastName(),
        pages.erp.intakeEmployeeDetailsPage.phoneCallsPreferences(),
        pages.erp.intakeEmployeeDetailsPage.addressHeader()
    ]);
    await waitHelpers.waitForSpinnersToDisappear();

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeEmployeeDetailsPage.addressLines1(),
        pages.erp.intakeEmployeeDetailsPage.addressLines2(),
        pages.erp.intakeEmployeeDetailsPage.addressLines3(),
        pages.erp.intakeEmployeeDetailsPage.addressCity()
    ]);
});

When(/^I choose to edit employee details with Request Change button$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.editLink());
    await pages.erp.intakeEmployeeDetailsPage.editLink().Click({waitForVisibility: true});
});

When(/^I edit the address fields with (the same random|random) data and confirm$/, async mode => {
    if (mode === 'random') {
        testVars.intake.addressline1 = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
            'addressline1');
        testVars.intake.addressline2 = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
            'addressline2');
        testVars.intake.addressline3 = await actions.erp.employeeProfileActions.generatePersonalDetailsData(
            'addressline3');
    }

    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.intakeEmployeeDetailsPage.editAddressLine1().SetText(testVars.intake.addressline1);
    await pages.erp.intakeEmployeeDetailsPage.editAddressLine2().SetText(testVars.intake.addressline2);
    await pages.erp.intakeEmployeeDetailsPage.editAddressLine3().SetText(testVars.intake.addressline3);

    try {
        await waitHelpers.waitForElementToBeVisible(
            await pages.erp.intakeEmployeeDetailsPage.useDifferentPhoneNumber(),
            timeouts.T1
        );
        await pages.erp.intakeEmployeeDetailsPage.phoneRadioBtn().first().Click({waitForVisibility: true});
    } catch (error) {
        await pages.erp.intakeEmployeeDetailsPage.editPhoneNumberLink().Click({waitForVisibility: true});
        await actions.erp.intakeActions.selectRandomPhoneNumber();
    }
});

When(/^I edit the "([^"]*)" field with "([^"]*)" data and confirm$/, async (fieldLabel, text) => {
    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.intakeEmployeeDetailsPage.intakeField(fieldLabel).SetText(text);
});

Then(/^I see "Intake flow" "([^"]*)" fields were refreshed correctly$/, async fieldLabels => {
    fieldLabels = fieldLabels.split(',').map(item => item.trim());
    const actualAddress = await pages.erp.intakeEditEmployeeDetailsPage.address().getText();

    for (const label of fieldLabels) {
        const dataType = label.trim().replace(constants.regExps.spaces, '').toLowerCase();
        const expectedAddress = testVars.intake[dataType];
        expect(await actualAddress.toString()).contain(expectedAddress);
    }
});

Then(/^I see "Intake flow" "([^"]*)" fields were refreshed with data set in personal details$/, async fieldLabels => {
    fieldLabels = fieldLabels.split(',').map(item => item.trim());
    const actualAddress = await pages.erp.intakeEditEmployeeDetailsPage.address().getText();

    for (const label of fieldLabels) {
        const dataType = label.trim().replace(constants.regExps.spaces, '').toLowerCase();
        expect(await actualAddress.toString()).contain(testVars.employeePersonalDetails[dataType]);
    }
});

When(/^I click on "(OK|Save and proceed|Confirm and proceed)" button$/, async btnName => {
    if (btnName === 'OK') {
        await pages.erp.intakeEditEmployeeDetailsPage.okButton().Click({waitForVisibility: true});
        await waitHelpers.waitForSpinnersToDisappear();
    } else if (btnName === 'Save and proceed') {
        await pages.erp.intakeEmployeeDetailsPage.saveAndProceed().Click({waitForVisibility: true});
        await waitHelpers.waitForSpinnersToDisappear();
        await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeEmployeeDetailsPage.saveAndProceed());
    } else if (btnName === 'Confirm and proceed') {
        await pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton().Click({waitForVisibility: true});
        await waitHelpers.waitForSpinnersToDisappear();
        await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton());
        await waitHelpers.waitForIntakePageEmploymentButtonLoadingToDisappear();
    }
});

Then(/^I can see changes I made in intake flow$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    expect(testVars.intake.addressline1).to.contain(await pages.erp.intakeEmployeeDetailsPage.addressLines1().getText());
    expect(testVars.intake.addressline2).to.contain(await pages.erp.intakeEmployeeDetailsPage.addressLines2().getText());
    expect(testVars.intake.addressline3).to.contain(await pages.erp.intakeEmployeeDetailsPage.addressLines3().getText());
});
When(/^I select random date of hire$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    const lastDay = await actions.erp.intakeActions.generateDateOfHire();
    await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.calendarField());
    await waitHelpers.waitForElementToBeClickable(pages.erp.initialRequestPage.calendarField());
    await pages.erp.initialRequestPage.calendarField().SetText(lastDay + protractor.Key.ENTER);
});

When(/^I select random last day of hire$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    const lastDay = await actions.erp.intakeActions.generateLastDay();
    await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.calendarField());
    await waitHelpers.waitForElementToBeClickable(pages.erp.initialRequestPage.calendarField());
    await pages.erp.initialRequestPage.calendarField().SetText(lastDay + protractor.Key.ENTER);
});

async function verifyFields() {
    expect([
        pages.erp.intakeOccupationAndEarnings.jobTitleHeader().getText(),
        pages.erp.intakeOccupationAndEarnings.dateOfHireHeader().getText(),
        pages.erp.intakeOccupationAndEarnings.earningsHeader().getText(),
        pages.erp.intakeOccupationAndEarnings.frequencyHeader().getText(),
        pages.erp.intakeOccupationAndEarnings.workPatternHeader().getText()
    ]).to.not.contain('*');

    await expectHelpers.checkTranslation(
        pages.erp.intakeOccupationAndEarnings.jobTitleHeader(),
        'INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE'
    );
    await expectHelpers.checkTranslation(
        pages.erp.intakeOccupationAndEarnings.dateOfHireHeader(),
        'INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE'
    );
    await expectHelpers.checkTranslation(
        pages.erp.intakeOccupationAndEarnings.earningsHeader(),
        'INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS'
    );
    await expectHelpers.checkTranslation(
        pages.erp.intakeOccupationAndEarnings.frequencyHeader(),
        'INTAKE.OCCUPATION_AND_EARNINGS.FREQUENCY'
    );
    await expectHelpers.checkTranslation(
        pages.erp.intakeOccupationAndEarnings.workPatternHeader(),
        'INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN'
    );
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.workPatternField());
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.frequencyDropDown());

    await expectHelpers.expectAllElementsToBeVisible([
        pages.erp.intakeOccupationAndEarnings.dateOfHireField(),
        pages.erp.intakeOccupationAndEarnings.earningsHeader(),
        pages.erp.intakeOccupationAndEarnings.earningsInput()
    ]);
}

Given(/^I select random phone number$/, async () => {
    await pages.erp.intakeEmployeeDetailsPage.editPhoneNumberLink().Click({waitForVisibility: true});
    await actions.erp.intakeActions.selectRandomPhoneNumber();
});

Then(
    /^I should see input fields to provide own working hours for each of "([^"]*)" days in work pattern$/,
    async numberOfDays => {
        expect(await pages.erp.intakeOccupationAndEarnings.ownWorkingHoursFields().count()).to.be.equal(parseInt(
            numberOfDays,
            10
        ));
    }
);

Then(/^I shouldn't be able to proceed to "(Occupation & Earnings|Initial Request)" section$/, async pageName => {
    if (pageName === 'Occupation & Earnings') {
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.jobTitleHeader());
    } else {
        await pages.erp.intakeEmployeeDetailsPage.proceedButton().Click({waitForVisibility: true});
        await waitHelpers.waitForElementToBeNotVisible(pages.erp.initialRequestPage.finishAndSubmitBtn());
    }
});

When(/^I try to submit intake without filling "([^"]*)" field$/, async fieldName => {
    let fields = [];
    if (testVars.searchTarget.selectedCountry.toString().toLowerCase() === 'ireland') {
        fields = [
            'firstName',
            'lastName',
            'preferredPhoneNumber.areaCode',
            'preferredPhoneNumber.telephoneNo',
            'address.addressLine1',
            'address.addressLine4',
            'address.postCode'
        ];
    } else {
        fields = [
            'firstName',
            'lastName',
            'preferredPhoneNumber.areaCode',
            'preferredPhoneNumber.telephoneNo',
            'address.addressLine1',
            'address.addressLine4',
            'State',
            'address.postCode'
        ];
    }

    const indexElement = fields.findIndex(el => el.includes(fieldName.toString().replace(' ', '.')));
    if (indexElement !== -1) {
        fields.splice(indexElement, 1);
    }

    await waitHelpers.waitForSpinnersToDisappear();

    for (const f of fields) {
        if (f.toLowerCase() === 'state') {
            await actions.erp.intakeActions.selectRandomStateOrProvince('State');
        } else {
            const formField = await pages.erp.intakeEmployeeDetailsPage.intakeElement(f);
            await formField.Clear();
            await formField.SetText(testVars.searchTarget[f]);
        }
    }
});

When(/^I switch "(on|off)" 'Non standard working week'$/, async switchAction => {
    const nonStandardWeekSwitch = await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.nonStandardWorkingWeekSwitchOff());
    const actualSwitchState = await nonStandardWeekSwitch.getAttribute('aria-checked');

    if (switchAction === 'on' && (actualSwitchState === 'false' || actualSwitchState === null)) {
        await nonStandardWeekSwitch.Click({waitForVisibility: true});
    } else if (switchAction === 'off' && actualSwitchState === 'true') {
        await nonStandardWeekSwitch.Click({waitForVisibility: true});
    }
});

When(/^I switch "(on|off)" Include weekend$/, async switchAction => {
    const includeWeekendSwitch = await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.includeWeekendSwitchOff());
    const actualSwitchState = await includeWeekendSwitch.getAttribute('aria-checked');

    if (switchAction === 'on' && (actualSwitchState === 'false' || actualSwitchState === null)) {
        await includeWeekendSwitch.Click({waitForVisibility: true});
    } else if (switchAction === 'off' && actualSwitchState === 'true') {
        await includeWeekendSwitch.Click({waitForVisibility: true});
    }
});

When(/^I switch "(on|off)" pattern repeats$/, async switchAction => {
    const patternRepeatsSwitch = await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.patternRepeatsSwitch());
    const actualSwitchState = await patternRepeatsSwitch.getAttribute('aria-checked');

    if (switchAction === 'on' && (actualSwitchState === 'false' || actualSwitchState === null)) {
        await patternRepeatsSwitch.Click({waitForVisibility: true});
    } else if (switchAction === 'off' && actualSwitchState === 'true') {
        await patternRepeatsSwitch.Click({waitForVisibility: true});
    }
});

When(/^I switch "(on|off)" day length same$/, async switchAction => {
    const dayLengthSameSwitch = await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.workDaySameLengthSwitch());
    const actualSwitchState = await dayLengthSameSwitch.getAttribute('aria-checked');

    if (switchAction === 'on' && (actualSwitchState === 'false' || actualSwitchState === null)) {
        await dayLengthSameSwitch.Click({waitForVisibility: true});
    } else if (switchAction === 'off' && actualSwitchState === 'true') {
        await dayLengthSameSwitch.Click({waitForVisibility: true});
    }
});

When(/^I fill custom work hours fields$/, async () => {
    const customFields = await pages.erp.intakeOccupationAndEarnings.ownWorkingHoursFields();
    customFields.forEach(field => field.SetText(testVars.searchTarget.hoursPerDay));
});

When(/^I select radiobutton "([^"]*)"$/, async radioBtn => {
    await pages.erp.intakeOccupationAndEarnings.radioButtonList().filter(async el => (await el.getText()).includes(
        radioBtn)).click();
});

Then(
    /^I verify that I can provide own working hours for each of "([^"]*)" days in work pattern$/,
    async numberOfWorkingDays => {
        await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.workingDaysInput().first());
        expect(await pages.erp.intakeOccupationAndEarnings.workingDaysInput().count()).equal(parseInt(
            numberOfWorkingDays,
            10
        ));
        await pages.erp.intakeOccupationAndEarnings.workingDaysInput().each(async input => {
            await input.SetText(testVars.searchTarget.hoursPerDay);
            expect(await input.getAttribute('value')).to.contain(testVars.searchTarget.hoursPerDay);
        });
    }
);

Then(/^I see "([^"]*)" switch item$/, async numberOfSwitches => {
    expect(await pages.erp.intakeOccupationAndEarnings.workPatternSwitch().count()).equal(parseInt(
        numberOfSwitches,
        10
    ));
});

Then(/^I should see pattern description input field$/, async () => {
    const headerTitle = translationsHelper.getTranslation('INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION');
    expect(await pages.erp.intakeOccupationAndEarnings.patternDescriptionHeader().getText()).to.equal(headerTitle);
});

When(/^I try to submit intake without selecting reason$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.initialRequestPage.finishAndSubmitBtn().Click({waitForVisibility: true});
});

Then(/^I shouldn't be able to submit intake$/, async () => {
    await pages.erp.initialRequestPage.finishAndSubmitBtn().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeAcknowledgmentScreen.confirmationScreen());
});

When(/^I click (My Dashboard|Abandon) button to cancel intake$/, async button => {
    await waitHelpers.waitForSpinnersToDisappear();
    try {
        await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeEmployeeDetailsPage.stateProvinceDropDownArrow());
    } catch (error) {
        // eslint-disable-next-line no-console
        console.log('Element is not visible for existing user');
    }

    if (button === 'My Dashboard') {
        await pages.erp.searchPage.myDashboardButton().Click({waitForVisibility: true});
    }

    if (button === 'Abandon') {
        await pages.erp.intakeEmployeeDetailsPage.abandonIntakeButton().Click({waitForVisibility: true});
    }
});

Then(/^I should see confirmation popup asking me if I really want to abandon intake$/, async () => {
    expect(await browser.switchTo().alert().getText()).to.contain(translationsHelper.getTranslation(
        'INTAKE.ABANDON_INTAKE_MODAL'));
});

When(/^I choose "(OK|Cancel)" on confirmation popup$/, async btnName => {
    if (btnName === 'OK') {
        await browser.switchTo().alert().accept();
    } else if (btnName === 'Cancel') {
        await browser.switchTo().alert().dismiss();
    }
});

Then(/^The abandon intake popup disappears$/, async () => {
    try {
        await browser.switchTo().alert();
    } catch (NoSuchAlertError) {
        // eslint-disable-next-line no-console
        console.log('Error modal is not visible');
    }
});

Then(/^I should be able to continue intake creation$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.employeeDetailsFormHeader().first());
});

When(/^I select "([^"]*)" in the country dropdown field$/, async countryName => {
    testVars.searchTarget.selectedCountry = countryName;
    if (countryName.toString().toLowerCase() === 'united states') {
        countryName = 'usa';
    }

    await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeEmployeeDetailsPage.countryDropDown());
    await waitHelpers.waitForElementToBeStatic(await pages.erp.intakeEmployeeDetailsPage.countryDropDownArrow());
    await waitHelpers.waitForElementToBeClickable(await pages.erp.intakeEmployeeDetailsPage.countryDropDownArrow());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.country('USA'));
    await pages.erp.intakeEmployeeDetailsPage.countryDropDown().Click({waitForVisibility: true});
    const element = await pages.erp.intakeEmployeeDetailsPage.country(countryName).Scroll();
    await element.Click({waitForVisibility: true});
});

Then(
    /^I cannot jump to "(\(3\) Initial Request|\(2\) Occupation & Earnings)" using steps navigation panel$/,
    async stepName => {
        if (stepName.toString().includes('2')) {
            await pages.erp.intakeEmployeeDetailsPage.occupationAndEarningsStep().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.workPatternHeader());
        } else if (stepName.toString().includes('3')) {
            await pages.erp.intakeEmployeeDetailsPage.initialRequestStep().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.initialRequestPage.accommodationRequiredRadioBtn());
        }
    }
);

Then(
    /^I can jump to step "(\(1\) Employee's Details|\(2\) Occupation & Earnings|\(3\) Initial Request)" using steps navigation panel$/,
    async stepName => {
        if (testVars.searchTarget.descriptor.toString() === 'existing' && stepName.toString().includes(
            'Employee\'s Details')) {
            await pages.erp.initialRequestPage.intakeDetailsHeader().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.existingEmployeeDescription());
        } else if (stepName.toString().includes('Occupation & Earnings')) {
            await pages.erp.initialRequestPage.occupationAndEarningsStep().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.jobTitleHeader());
        } else if (stepName.toString().includes('Initial Request')) {
            await pages.erp.initialRequestPage.initialRequestHeader().first().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.finishAndSubmitBtn());
        } else if (testVars.searchTarget.descriptor.toString() === 'not existing' && stepName.toString().includes(
            'Employee\'s Details')) {
            await pages.erp.initialRequestPage.intakeDetailsHeader().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.newEmployeeDescription());
        }
    }
);

Then(/^I shouldn't see back button$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeEmployeeDetailsPage.backButton());
});

Then(/^I should see "([^"]*)" button$/, async btnName => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.abandonIntakeButton());
    expect(await pages.erp.intakeEmployeeDetailsPage.abandonIntakeButton().getText()).to.equal(btnName);
});

When(/^I edit employee name$/, async () => {
    await pages.erp.intakeEmployeeDetailsPage.intakeFirstName().SetText(faker.name.firstName(1));
});

When(/^I fill all mandatory fields$/, async () => {
    await fillAllIntakeData();
});

Given(/^I open intake creator for "([^"]*)"$/, async descriptor => {
    await actions.erp.searchActions.openIntakeCreatorForExistingOrNotEmployee(descriptor);
});

When(/^I try to submit intake without selecting contact preferences$/, async () => {
    await pages.erp.intakeEmployeeDetailsPage.confirmAndProceedButton().Click({waitForVisibility: true});
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.areaCodeValidation());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.phoneNoValidation());
});

When(
    /^I click "([^"]*)" button on "(\(2\) Occupation & Earnings|\(3\) Initial Request)"$/,
    async (btnName, stepName) => {
        if (stepName === '(2) Occupation & Earnings') {
            await pages.erp.intakeOccupationAndEarnings.backButton().Click({waitForVisibility: true});
        } else {
            await pages.erp.initialRequestPage.backButton().Click({waitForVisibility: true});
        }
    }
);

Then(
    /^I should land on "(\(2\) Occupation & Earnings|\(1\) Employee's Details|\(3\) Initial Request)" page$/,
    async stepName => {
        if (stepName.toString().includes('Occupation & Earnings')) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.occupationAndEarningsHeader());
        } else if (stepName.toString().includes('Initial Request')) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.initialRequestPage.finishAndSubmitBtn());
        } else {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeEmployeeDetailsPage.employeeDetailsFormHeader().get(
                1));
        }
    }
);

Then(/^I should see "([^"]*)" modal$/, async modalHeader => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeHowDoYouWantToProceedModal.modalHeader());
    expect(await pages.erp.intakeHowDoYouWantToProceedModal.modalHeader().getText()).to.contain(modalHeader);
});

When(/^I click "(NO, discard my changes|YES, save my changes|X \(closing popup\))" button$/, async btnName => {
    if (btnName.toString().includes('YES')) {
        btnName = 'Yes';
        await pages.erp.intakeHowDoYouWantToProceedModal.modalButton(btnName).Click({waitForVisibility: true});
    } else if (btnName.toString().includes('NO')) {
        btnName = 'NO';
        await pages.erp.intakeHowDoYouWantToProceedModal.modalButton(btnName).Click({waitForVisibility: true});
    } else {
        await pages.erp.intakeHowDoYouWantToProceedModal.closeButton().Click({waitForVisibility: true});
    }
});

When(
    /^I try to jump to step "(\(3\) Initial Request|\(2\) Occupation & Earnings)" using steps navigation panel$/,
    async stepName => {
        if (testVars.searchTarget.descriptor.toString() === 'existing' && stepName.toString().includes(
            'Employee\'s Details')) {
            await pages.erp.initialRequestPage.intakeDetailsHeader().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeEmployeeDetailsPage.existingEmployeeDescription());
        } else if (stepName.toString().includes('Occupation & Earnings')) {
            await pages.erp.initialRequestPage.occupationAndEarningsStep().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.jobTitleHeader());
        } else if (stepName.toString().includes('Initial Request')) {
            await pages.erp.initialRequestPage.initialRequestHeader().first().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.initialRequestPage.finishAndSubmitBtn());
        } else if (testVars.searchTarget.descriptor.toString() === 'existing' && stepName.toString().includes(
            'Employee\'s Details')) {
            await pages.erp.initialRequestPage.intakeDetailsHeader().Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeEmployeeDetailsPage.existingEmployeeDescription());
        }
    }
);

Then(/^I should see empty address fields: "([^"]*)"$/, async fieldsList => {
    fieldsList = fieldsList.split(',');
    for (let i = 0; i < fieldsList.length; i++) {
        expect(await pages.erp.intakeEmployeeDetailsPage.getInputByLabel(fieldsList[i]).getAttribute('value')).that.is.empty;
    }
});

Then(/^all changes I made should be discarded$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    expect(testVars.intake.addressline1).to.not.contain(await pages.erp.intakeEmployeeDetailsPage.addressLines().get(
        0).getText());
    expect(testVars.intake.addressline2).to.not.contain(await pages.erp.intakeEmployeeDetailsPage.addressLines().get(
        1).getText());
    expect(testVars.intake.addressline3).to.not.contain(await pages.erp.intakeEmployeeDetailsPage.addressLines().get(
        2).getText());
});

Then(/^I "(can|can not)" see the changes I made in intake flow$/, async visibility => {
    await waitHelpers.waitForSpinnersToDisappear();
    if (visibility === 'can not') {
        expect(await pages.erp.intakeEmployeeDetailsPage.addressLines1().getText()).to.not.contain(testVars.intake.addressline1);
        expect(await pages.erp.intakeEmployeeDetailsPage.addressLines2().getText()).to.not.contain(testVars.intake.addressline2);
        expect(await pages.erp.intakeEmployeeDetailsPage.addressLines3().getText()).to.not.contain(testVars.intake.addressline3);
    } else {
        expect(await pages.erp.intakeEmployeeDetailsPage.addressLines1().getText()).to.contain(testVars.intake.addressline1);
        expect(await pages.erp.intakeEmployeeDetailsPage.addressLines2().getText()).to.contain(testVars.intake.addressline2);
        expect(await pages.erp.intakeEmployeeDetailsPage.addressLines3().getText()).to.contain(testVars.intake.addressline3);
    }
});

When(/^I edit the address fields for not existing employee and confirm$/, async () => {
    const addressLine1 = await actions.erp.employeeProfileActions.generatePersonalDetailsData('addressline1');
    const addressLine2 = await actions.erp.employeeProfileActions.generatePersonalDetailsData('addressline2');
    const addressLine3 = await actions.erp.employeeProfileActions.generatePersonalDetailsData('addressline3');
    testVars.intake.addressline1 = addressLine1;
    testVars.intake.addressline2 = addressLine2;
    testVars.intake.addressline3 = addressLine3;
    await waitHelpers.waitForSpinnersToDisappear();
    await pages.erp.intakeEmployeeDetailsPage.addressLine1().SetText(addressLine1);
    await pages.erp.intakeEmployeeDetailsPage.addressLine2().SetText(addressLine2);
    await pages.erp.intakeEmployeeDetailsPage.addressLine3().SetText(addressLine3);
});

Then(/^I "(can|can not)" see changes I made in intake flow for not existing employee$/, async visibility => {
    await waitHelpers.waitForSpinnersToDisappear();
    if (visibility === 'can') {
        expect(testVars.intake.addressline1).to.contain(await pages.erp.intakeEmployeeDetailsPage.addressLine1().getText());
        expect(testVars.intake.addressline2).to.contain(await pages.erp.intakeEmployeeDetailsPage.addressLine2().getText());
        expect(testVars.intake.addressline3).to.contain(await pages.erp.intakeEmployeeDetailsPage.addressLine3().getText());
    } else {
        expect(testVars.intake.addressline1).to.not.contain(await pages.erp.intakeEmployeeDetailsPage.addressLine1().getText());
        expect(testVars.intake.addressline2).to.not.contain(await pages.erp.intakeEmployeeDetailsPage.addressLine2().getText());
        expect(testVars.intake.addressline3).to.not.contain(await pages.erp.intakeEmployeeDetailsPage.addressLine3().getText());
    }
});

When(/^I set pattern repeats switch to "([^"]*)" providing value "([^"]*)"$/, async (state, repeat) => {
    if (state === 'on' && expect(await pages.erp.intakeOccupationAndEarnings.patternRepeatsSwitch().getAttribute(
        'aria-checked')).to.eq('false')) {
        await pages.erp.intakeOccupationAndEarnings.patternRepeatsSwitch().Click({waitForVisibility: true});
        expect(await pages.erp.intakeOccupationAndEarnings.patternRepeatsSwitch().getAttribute('aria-checked')).to.include(
            true);
        await pages.erp.intakeOccupationAndEarnings.numberOfWeeks().SetText(repeat);
    }
});

When(/^I set day length same switch to "([^"]*)" providing value "([^"]*)"$/, async (state, length) => {
    expect(await pages.erp.intakeOccupationAndEarnings.workDaySameLengthSwitch().getAttribute('aria-checked')).to.include(
        false);
    if (state === 'on' && expect(await pages.erp.intakeOccupationAndEarnings.workDaySameLengthSwitch().getAttribute(
        'aria-checked')).to.include(false)) {
        await pages.erp.intakeOccupationAndEarnings.workDaySameLengthSwitch().Click({waitForVisibility: true});
        expect(await pages.erp.intakeOccupationAndEarnings.workDaySameLengthSwitch().getAttribute('aria-checked')).to.include(
            true);
        await pages.erp.intakeOccupationAndEarnings.dayLength().SetText(length);
    }
});

Then(/^"(Day length|Number of weeks)" input is "(visible|not visible)"$/, async (inputName, state) => {
    if (inputName === 'Day length') {
        if (state === 'visible') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.dayLength());
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.dayLength());
        }
    } else if (inputName === 'Number of weeks') {
        if (state === 'visible') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.intakeOccupationAndEarnings.numberOfWeeks());
        } else {
            await waitHelpers.waitForElementToBeNotVisible(pages.erp.intakeOccupationAndEarnings.numberOfWeeks());
        }
    }
});

When(/^I fill additional information field$/, async () => {
    await pages.erp.intakeOccupationAndEarnings.workingPatternAdditionalInformation().SetText(faker.lorem.words());
});

When(/^I empty additional information field$/, async action => {
    await expect(pages.erp.intakeOccupationAndEarnings.workingPatternAdditionalInformation()).to.be.empty;
});

Then(/^I can see the error message "([^"]*)"$/, async errorMessage => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeOccupationAndEarnings.errorMessage().first());
    expect(await pages.erp.intakeOccupationAndEarnings.errorMessage().last().getText()).to.contain(errorMessage);
});

Then(/^I verify the required error displays under the mandatory address fields "([^"]*)"$/, async fieldsList => {
    fieldsList = fieldsList.split(',');
    for (let i = 0; i < fieldsList.length; i++) {
        expect(await pages.erp.intakeEmployeeDetailsPage.getInputByLabel(fieldsList[i]).getAttribute('value')).that.is.empty;
    }
});

When(/^I select "([^"]*)" in the country dropdown field in edit details page$/, async countryName => {
    testVars.searchTarget.selectedCountry = countryName;
    if (countryName.toString().toLowerCase() === 'united states') {
        countryName = 'usa';
    }

    await actions.erp.intakeActions.selectLastCountryFromDropDown();
    await actions.erp.intakeActions.selectCountryFromDropDown(countryName);
});

Then(/^I verify that the address is updated on the page for "([^"]*)"$/, async fieldsList => {
    fieldsList = fieldsList.split(',');

    for (let i = 0; i < fieldsList.length; i++) {
        expect(testVars.searchTarget.addressLines).to.contain(await pages.erp.intakeEmployeeDetailsPage.getInputByLabel(
            fieldsList[i]).getAttribute('value'));
    }
});

Then(
    /^I fill mandatory "([^"]*)" including the "([^"]*)" postal\/zip code$/,
    async (fieldsList, zipCode) => {
        await actions.erp.intakeActions.fillMandatoryAddressFieldsOnEditPage(fieldsList, zipCode);
    }
);

Given(
    /^I choose to edit employee contact preference by "(Add a new phone as the preferred contact method|Choose an existing phone number as the preferred contact method)"$/,
    async editMethod => {
        await pages.erp.intakeEditEmployeeDetailsPage.editPhoneNumberLink().Click({waitForVisibility: true})
            .then(null, function (err) {
                // eslint-disable-next-line no-console
                console.log(`The error occured is: ${err.name}`);
            });

        if (editMethod.toString().toLowerCase().includes('add')) {
            await actions.erp.intakeActions.fillNewPhoneNumber();
            const newPhoneNumber = `${await pages.erp.intakeEmployeeDetailsPage.getInputByLabel('Country code').getAttribute(
                'value')
            }-${await pages.erp.intakeEmployeeDetailsPage.getInputByLabel('Area code').getAttribute('value')
            }-${await pages.erp.intakeEmployeeDetailsPage.getInputByLabel('Phone number').getAttribute('value')}`;
            testVars.searchTarget.phones = newPhoneNumber;
        } else {
            await actions.erp.intakeActions.selectRandomPhoneNumber();
            testVars.searchTarget.phones = await pages.erp.intakeEmployeeDetailsPage.selectedPhoneNumber().first().getText();
        }
    }
);

When(/^I open Communication Preferences "([^"]*)"$/, async employeeDescriptor => {
    await actions.erp.searchActions.openEmployeeDetailsPage(employeeDescriptor, 'lastName');
});

Then(/^I can see changes in the phone numbers I made in intake flow$/, async () => {
    expect(await pages.erp.communicationPreferencesPage.communicationPreferencesPhones().getText()).to.contain(testVars.searchTarget.phones);
});

Then(/^I verify that the address is updated on the page$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    await waitHelpers.waitForElementToBeVisible(await pages.erp.intakeEmployeeDetailsPage.addressContainer());
    for (let i = 0; i < constants.addressLines.length; i++) {
        expect(await pages.erp.intakeEmployeeDetailsPage.addressContainer().getText()).to.contain(constants.addressLines[i]);
    }
});

When(/^I "(fill|empty)" additional information field$/, async action => {
    if (action === 'fill') {
        await pages.erp.intakeOccupationAndEarnings.workingPatternAdditionalInformation().SetText(faker.lorem.words());
    } else {
        await expect(await pages.erp.intakeOccupationAndEarnings.workingPatternAdditionalInformation().getAttribute(
            'value')).to.be.empty;
    }
});

Then(/^I verify that ok button is not displayed on the page$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(await pages.erp.intakeEmployeeDetailsPage.okButton());
});
