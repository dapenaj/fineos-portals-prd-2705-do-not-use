const notifications = require('./../constants/notifications');
const moment = require('moment');
const {getRandomArrayItem} = require('../../common/helpers/mathHelper');
const {getRequestData} = require('../../common/helpers/snifferHelper');

Then(
    /^I see "(?:Employees expected to return to work|Notifications created|Notifications with outstanding requirements|Leaves decided|Employees approved for leave)" drawer is (closed|opened)$/,
    async state => {
        if (state === 'opened') {
            await actions.erp.notificationsWithOutstandingReqActions.checkIfDrawerElementsAreVisible();
        } else {
            await actions.erp.notificationsWithOutstandingReqActions.checkIfDrawerElementsAreNotVisible();
        }
    }
);

Then(
    /^I see a list of "(Employees expected to return to work|Notifications created|Leaves decided|Employees approved for leave)" in a "(today|this week|next week|week after next|last week|week before last)" date range$/,
    async (widget, selectedDateRange) => {
        const dateRange = (selectedDateRange === 'today') ? moment().toDate() : actions.erp.notificationsWithOutstandingReqActions.getFilteringDateRange();
        if (widget !== 'Employees approved for leave') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedCells().first());
            await pages.erp.notificationsWithOutstandingReqPage.sortedCells().each(async cell => {
                const actualDate = moment(
                    (await cell.getText()).toString(),
                    constants.dateFormats.shortDate,
                    true
                ).toDate();
                if (selectedDateRange === 'today') {
                    expect(actualDate === moment().toDate()).is.true;
                } else {
                    expect((actualDate >= dateRange.startDate) && (actualDate <= dateRange.endDate)).is.true;
                }
            });
        } else if (pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateCell().length !== 0 && selectedDateRange !== 'today') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateCell().first());
            await pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateCell().each(async dateRangeAFL => {
                let dateRangeApprovedForLeave = await dateRangeAFL.getText();
                dateRangeApprovedForLeave = dateRangeApprovedForLeave.slice(-28).split('-');
                const startDateRangeApprovedForLeave = dateRangeApprovedForLeave[0].trim();
                const endDateRangeApprovedForLeave = dateRangeApprovedForLeave[1].trim();
                const actualStartDate = moment(
                    startDateRangeApprovedForLeave.toString(),
                    constants.dateFormats.shortDate,
                    true).toDate();
                const actualEndDate = moment(
                    endDateRangeApprovedForLeave.toString(),
                    constants.dateFormats.shortDate,
                    true).toDate();
                expect((actualStartDate >= dateRange.startDate) && (actualEndDate <= dateRange.endDate)).is.true;
            });
        } else if (pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateMinCell().length !== 0 && selectedDateRange !== 'today') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateMinCell().first());
            await pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateMinCell().each(async dateMinRangeAFL => {
                let dateMinRangeApprovedForLeave = await dateMinRangeAFL.getText();
                dateMinRangeApprovedForLeave = dateMinRangeApprovedForLeave.slice(-28).split('-');
                const startDateMinRangeApprovedForLeave = dateMinRangeApprovedForLeave[0].trim();
                const endDateMinRangeApprovedForLeave = dateMinRangeApprovedForLeave[1].trim();
                const actualStartDateMin = moment(
                    startDateMinRangeApprovedForLeave.toString(),
                    constants.dateFormats.shortDate,
                    true).toDate();
                const actualEndDateMin = moment(
                    endDateMinRangeApprovedForLeave.toString(),
                    constants.dateFormats.shortDate,
                    true).toDate();
                expect((actualStartDateMin >= dateRange.startDate) && (actualEndDateMin <= dateRange.endDate)).is.true;
            });
        } else if (await pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateTodayCell().length !== 0) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateTodayCell().first());
            await pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateTodayCell().each(async dateRangeTodayAFL => {
                let dateTodayApprovedForLeave = await dateRangeTodayAFL.getText();
                dateTodayApprovedForLeave = dateTodayApprovedForLeave.slice(-12);
                const onDate = moment(
                    dateTodayApprovedForLeave.toString(),
                    constants.dateFormats.shortDate,
                    true).toDate();
                if (selectedDateRange === 'today') {
                    expect(onDate === moment().toDate()).is.true;
                } else {
                    expect((onDate >= dateRange.startDate) && (onDate <= dateRange.endDate)).is.true;
                }
            });
        } else if (await pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateTodayMinCell().length !== 0) {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateTodayMinCell().first());
            await pages.erp.notificationsWithOutstandingReqPage.employeesApprovedForLeaveDateTodayMinCell().each(async dateRangeTodayMinAFL => {
                let dateTodayMinApprovedForLeave = await dateRangeTodayMinAFL.getText();
                dateTodayMinApprovedForLeave = dateTodayMinApprovedForLeave.slice(-12);
                const onDateMin = moment(
                    dateTodayMinApprovedForLeave.toString(),
                    constants.dateFormats.shortDate,
                    true).toDate();
                if (selectedDateRange === 'today') {
                    expect(onDateMin === moment().toDate()).is.true;
                } else {
                    expect((onDateMin >= dateRange.startDate) && (onDateMin <= dateRange.endDate)).is.true;
                }
            });
        }
    });

Then(/^The leaves list is sorted by "decision date" with "descending" order by default$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedCells().first());
    const decidedOnDates = await pages.erp.notificationsWithOutstandingReqPage.sortedCells().map(cell => cell.getText());
    for (let i = 0; i < decidedOnDates.length; i++) {
        decidedOnDates[i] = moment(
            decidedOnDates[i].toString(),
            constants.dateFormats.shortDate,
            true
        ).toDate();
    }
    testVars.leavesDecidedOnDate = decidedOnDates;
    for (let j = 0; j < decidedOnDates.length - 1; j++) {
        expect(decidedOnDates[j + 1] <= decidedOnDates[j]).is.true;
    }
});

Then(/^I verify the leaves with the same date are ordered alphabetically by the employee's first name$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.employeeCells().first());
    const employeeNames = await pages.erp.notificationsWithOutstandingReqPage.employeeCells().map(name => name.getText());
    for (let i = 0; i < testVars.leavesDecidedOnDate.length - 1; i++) {
        if (testVars.leavesDecidedOnDate[i] === testVars.leavesDecidedOnDate[i + 1]) {
            expect(employeeNames[i].localeCompare(employeeNames[i + 1]) >= 0).is.true;
        }
    }
});

Then(/^I verify Decision column shows a green tick for approved leaves$/, async () => {
    if (await pages.erp.notificationsWithOutstandingReqPage.decisionCheckIcons().length > 0) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.decisionCheckIcons().first());
        const checkAccepted = await pages.erp.notificationsWithOutstandingReqPage.decisionCheckIcons().map(name => name.getAttribute('aria-label'));
        for (let i = 0; i < checkAccepted.length; i++) {
            expect(checkAccepted[i].contains('Approved')).is.true;
        }
    }
});

Then(/^I verify Decision column shows a red cross for denied leaves$/, async () => {
    if (await pages.erp.notificationsWithOutstandingReqPage.decisionNotCheckIcons().length > 0) {
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.decisionNotCheckIcons().first());
        const checkDenied = await pages.erp.notificationsWithOutstandingReqPage.decisionNotCheckIcons().map(name => name.getAttribute('aria-label'));
        for (let i = 0; i < checkDenied.length; i++) {
            expect(checkDenied[i].contains('Denied')).is.true;
        }
    }
});

Then(
    /^I see the drawer title has correct text and number of filtered "(employees|notifications|leaves|employees AFL)"$/,
    async dataType => {
        let numberOfApiSortedItems = await getRequestData(
            constants.endPoints.filteredNotifications,
            'totalSize',
        );

        if (dataType === 'leaves') {
            numberOfApiSortedItems = await actions.erp.notificationsWithOutstandingReqActions.getNumberOfAllApiLeavesDecided();
        }

        if (dataType !== 'employees AFL') {
            expect(testVars.filtering.totalResults).is.equal(numberOfApiSortedItems.toString());
        }
        let translationField;

        if (dataType === 'employees') {
            translationField = 'EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK';
        } else if (dataType === 'notifications') {
            translationField = 'NOTIFICATIONS_CREATED_FILTER_DATE';
        } else if (dataType === 'leaves') {
            translationField = 'DECISIONS_MADE_FILTER_DATE';
        } else if (dataType === 'employees AFL') {
            translationField = 'EMPLOYEES_APPROVED_FOR_LEAVE_FILTER_DATE';
        }
        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.filteredDrawerHeader());
        const actualHeader = await pages.erp.notificationsWithOutstandingReqPage.filteredDrawerHeader().getText();
        let expectedHeader = translationsHelper.mapTranslationVariables(
            `WIDGET.${translationField}_DRAWER_HEADER`,
            [testVars.filtering.totalResults]
        );
        expectedHeader = `${expectedHeader} ${testVars.filtering.dateRange}`;
        expect(expectedHeader.replace(
            constants.regExps.newLine,
            ' '
        )).is.equal(actualHeader.replace(constants.regExps.newLine, ' '));
    }
);

Then(
    /^I verify the drawer title is a "number of notifications" followed by "notifications with outstanding requirements"$/,
    async () => {
        const expectedNumberOfNotifications = await actions.erp.notificationsWithOutstandingReqActions.countNumberOfAllNotifications();
        let expectedDrawerTitle = translationsHelper.getTranslation('OUTSTANDING_NOTIFICATIONS.TITLE');
        expectedDrawerTitle = `${expectedNumberOfNotifications} ${expectedDrawerTitle}`;

        const actualNumberOfNotifications = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.numberOfNotifications().getText());
        let actualDrawerTitle = await pages.erp.notificationsWithOutstandingReqPage.drawerTitle().getText();
        actualDrawerTitle = `${actualNumberOfNotifications} ${actualDrawerTitle}`;

        expect(expectedDrawerTitle).is.equal(actualDrawerTitle);
    }
);

When(
    /^I click on the first "(Employee Name - Employee Id|Notification Id)" link on the "Notifications with outstanding requirements" list entry$/,
    async linkName => {
        if (linkName === 'Notification Id') {
            const firstNotification = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.notificationsLinks().first());
            testVars.actualLinkData = (await firstNotification.getText()).toString().split('|')[0].toString().trim();
            await firstNotification.Click();
        } else {
            const firstEmployee = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.employeesLinks().first());
            testVars.actualLinkData = await firstEmployee.getText();
            await firstEmployee.Click();
        }
    }
);

Then(/^I should land on "(Employee Profile|Notification Details)" page$/, async searchTarget => {
    let header;

    if (searchTarget === constants.searchTargets.EMPLOYEE_PROFILE) {
        header = await waitHelpers.waitForElementToBeVisible(
            pages.erp.employeeProfilePage.userNameField(),
            timeouts.T60,
            'Expected to see employee profile page'
        );
    } else {
        header = await waitHelpers.waitForElementToBeVisible(
            pages.erp.notificationPage.sectionNotificationDetailHeader(),
            timeouts.T60,
            'Expected to see notification page'
        );
    }
    const actualNotificationId = (await header.getText()).split(constants.regExps.newLine)[0].toString();
    const expectedNotificationId = testVars.actualLinkData.toString().split(constants.regExps.newLine)[0].toString();

    expect(expectedNotificationId).contains(actualNotificationId);
});

When(
    /^I filter the list of notifications by "([^"]+)" with test data "([^"]+)"$/,
    async (searchTarget, testDescriptor) => {
        switch (searchTarget) {
            case constants.searchTargets.EMPLOYEE_FIRST_NAME:
            case constants.searchTargets.EMPLOYEE_LAST_NAME:
                await actions.erp.notificationsWithOutstandingReqActions.filterNotificationsByEmployee(
                    searchTarget,
                    testDescriptor
                );
                break;
            case constants.searchTargets.NOTIFICATION_ID:
                await actions.erp.notificationsWithOutstandingReqActions.filterNotificationsById(testDescriptor);
                break;
            default:
                throw new Error(`search target [${searchTarget}] is not supported`);
        }
    }
);

When(/^I click random "View Calendar" button$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.viewCalendarButtons().first());

    const viewCalendarButton = await pages.erp.notificationsWithOutstandingReqPage.viewCalendarButtons();
    const viewCalendarRandomIndex = await testDataHelpers.getRandomArrayElementIndex(viewCalendarButton);
    const randomViewCalendarButton = viewCalendarButton[viewCalendarRandomIndex];

    testVars.filtering.randomIndex = viewCalendarRandomIndex;
    await randomViewCalendarButton.Click({waitForVisibility: true});
});

When(
    /^I filter the (employees|notifications|leaves) by "(Case|Employee First Name|Decision|Employee Last Name|Notification Date|Job title|Group|Reason|DI)"$/,
    async (dataType, filteringCriteria) => {
        let filteringData;
        const filteringCriteriaWords = filteringCriteria.split(constants.regExps.spaces).map(item => item.trim());
        const filteringHeaderName = filteringCriteriaWords[0].toUpperCase();

        await pages.erp.notificationsWithOutstandingReqPage.filterIcon(filteringHeaderName).Click({waitForVisibility: true});

        if (filteringCriteria === 'Reason') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.reasonsList().first());
            filteringData = testDataHelpers.getRandomArrayElement(await pages.erp.notificationsWithOutstandingReqPage.reasonsList());
            await filteringData.Click({waitForVisibility: true});
            testVars.searchQuery = await filteringData.getText();
        } else if (filteringCriteria === 'DI') {
            filteringData = testDataHelpers.getRandomArrayElement([await pages.erp.sharedModal.yesButton(), await pages.erp.sharedModal.noButton()]);
            await filteringData.Click({waitForVisibility: true});
            testVars.searchQuery = await filteringData.getText();
        } else if (filteringCriteria === 'Decision') {
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.decisionsList().first());
            filteringData = testDataHelpers.getRandomArrayElement(await pages.erp.notificationsWithOutstandingReqPage.decisionsList());
            await filteringData.Click({waitForVisibility: true});
            testVars.searchQuery = await filteringData.getText();
        } else {
            const filteringInputName = (filteringCriteriaWords.length === 1) ? filteringCriteriaWords.toString() : filteringCriteriaWords.slice(
                1).join('');
            const filteringInput = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.filterInput(
                filteringInputName));
            filteringData = await actions.erp.notificationsWithOutstandingReqActions.getColumnFilteringData(
                dataType, filteringCriteria);

            testVars.searchQuery = filteringData;
            await filteringInput.SetText(filteringData);
        }
    }
);

When(
    /^I (apply|cancel) "(Case|Employee First Name|Decision|Employee Last Name|Notification Date|Notification ID|Job title|Group|Reason|DI)" filter$/,
    async (actionType, filterName) => {
        if (actionType === constants.stepWording.APPLY) {
            if (filterName === 'Reason' || filterName === 'DI' || filterName === 'Decision') {
                await pages.erp.sharedModal.okButton().first().Click({waitForVisibility: true});
            } else {
                await pages.erp.notificationsWithOutstandingReqPage.filterSearchButton().Click({waitForVisibility: true});
            }
            await waitHelpers.waitForSpinnersToDisappear();
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.notificationRows().last());
            const listCount = await actions.erp.notificationsWithOutstandingReqActions.countNumberOfAllNotifications();
            const actualListCount = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.filteredItemsNumber());

            await expectHelpers.expectElementTextEqual(
                actualListCount,
                `${listCount}`,
                'Number of filtered items should match with list length'
            );
        } else {
            await pages.erp.notificationsWithOutstandingReqPage.filterResetButton().Click({waitForVisibility: true});
            await waitHelpers.waitForSpinnersToDisappear();
            await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.filteredItemsNumber());
        }
    }
);

When(
    /^I sort the list by "(Case|Creation date|Decided on|Employee|Notified on|Job title|Group|Reason|Expected return to work)" with "(ascending|descending)" order$/,
    async (sortingColumnName, sortingOrder) => {
        let actuallySortedColumn = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedColumnHeader());
        actuallySortedColumn = await actuallySortedColumn.getText();

        if (actuallySortedColumn !== sortingColumnName) {
            sortingColumnName = sortingColumnName.toString().replace(constants.regExps.spaces, '_').toUpperCase();
            await pages.erp.notificationsWithOutstandingReqPage.columnHeader(sortingColumnName).Click({waitForVisibility: true});
        }

        const actualState = await actions.erp.notificationsWithOutstandingReqActions.getActualSorting();

        await actions.erp.notificationsWithOutstandingReqActions.setNotificationsSorting(
            sortingColumnName.split(' ')[0].toString().toUpperCase(),
            actualState,
            sortingOrder
        );
    }
);

Then(
    /^The (?:employees|leaves|notifications) list is sorted by "(Case|Creation date|Decided on|Employee||Notified on|Job title|Group|Reason|Expected return to work)" with "(ascending|descending)" order(?: by default)?$/,
    async (expectedSortedColumn, sortingOrder) => {
        const columnCellValues = [];
        let actualSortedColumn = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedColumnHeader());
        actualSortedColumn = await actualSortedColumn.getText();
        expect(expectedSortedColumn).is.equal(actualSortedColumn);

        await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedCells().first());
        await pages.erp.notificationsWithOutstandingReqPage.sortedCells().each(async cell => {
            cell = (await cell.getText()).toString().trim();

            if (moment(cell, constants.dateFormats.shortDate, true).isValid()) {
                expect(moment(cell, constants.dateFormats.shortDate, true).isValid()).to.be.equal(true);
                columnCellValues.push(moment(cell, constants.dateFormats.shortDate, true));
            } else {
                if (expectedSortedColumn === 'Case') {
                    cell = parseInt(cell.toString().match(/\d+/), 10);
                } else {
                    cell = cell.toString().toLowerCase();
                }
                columnCellValues.push(cell);
            }
        });

        if (sortingOrder === constants.sortingTypes.ASCENDING) {
            expect(columnCellValues).to.be.ascending;
        } else {
            expect(columnCellValues).to.be.descending;
        }
    }
);

Then(/^I can see badge displaying the correct number of filtered items vs total number of items$/, async () => {
    const numberOfFilteredNotifications = await pages.erp.notificationsWithOutstandingReqPage.notificationRows().count();
    const numberOfAlNotifications = (await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.numberOfNotifications().getText())).toString();
    const translation = translationsHelper.mapTranslationVariables(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT',
        [numberOfFilteredNotifications, numberOfAlNotifications]
    );

    expect(await pages.erp.notificationsWithOutstandingReqPage.filteredResultsBadge().getText()).is.equal(
        translation);
});

Then(/^I verify each element of the list contains "([^"]+)"$/, async fields => {
    const ntnRows = pages.erp.notificationsWithOutstandingReqPage.notificationRows();
    const rowsCount = await ntnRows.count();
    await expect(rowsCount).to.be.gt(0, 'Notifications list shouldn\'t be empty');
    const expectedFields = fields.split(',').map(item => item.trim());
    await ntnRows.map(async row => {
        const notificationId = await row.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);
        await waitHelpers.waitForElementToBeVisible(actions.erp.notificationsWithOutstandingReqActions.getFieldByName(
            expectedFields[0],
            notificationId
        ));
        await expectHelpers.expectAllElementsToBeVisible(
            expectedFields.map(item => actions.erp.notificationsWithOutstandingReqActions.getFieldByName(
                item,
                notificationId
            )),
            `Not all fields visible for notification ${notificationId}`
        );
    });
});

Then(/^I verify "Notification Date" is in format "(MMM DD, YYYY|YYYY-MM-DD)"$/, async expectedDateFormat => {
    const ntnRows = pages.erp.notificationsWithOutstandingReqPage.notificationRows();
    const rowsCount = await ntnRows.count();
    await expect(rowsCount).to.be.gt(0, 'Notifications list shouldn\'t be empty');

    await ntnRows.map(async row => {
        const notificationId = await row.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);
        const date = await pages.erp.notificationsWithOutstandingReqPage.tableCellNotificationsDate(
            notificationId).getText();

        expect(moment(date, expectedDateFormat, true).isValid()).is.true;
    });
});

Then(/^I can reset filter by closing the badge$/, async () => {
    await pages.erp.notificationsWithOutstandingReqPage.closeFilteredResultsBadge().Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();
    await expectHelpers.expectElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.closeFilteredResultsBadge());
});

When(/^I click on Show\/Hide actions on notification "([^"]+)"$/, async testDescriptor => {
    const notificationId = notifications.getNotification(testDescriptor).id;
    await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(notificationId).Click();
});

When(/^I click on (Show|Hide) actions on random notification$/, async actionType => {
    const randomNotification = getRandomArrayItem(await pages.erp.notificationsWithOutstandingReqPage.notificationRows());
    const randomNotificationId = await randomNotification.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);
    const actualState = (await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(
        randomNotificationId).getText()).toString().toLowerCase();

    if ((actualState.includes('view') && actionType === 'Show') || (actualState.includes('hide') && actionType === 'Hide')) {
        await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(randomNotificationId).Click();
    }
});

When(/^I find and open notification with document to upload$/, async () => {
    const notification = await actions.erp.notificationsWithOutstandingReqActions.findNotificationWithHyperlink();
    await pages.erp.showHidePanelsModal.closeXButton().Click({waitForVisibility: true});
    await actions.erp.searchActions.openNotification(notification);
});

When(/^I click on "Show Actions" on random notification with hyperlink$/, async () => {
    await actions.erp.notificationsWithOutstandingReqActions.findNotificationWithHyperlink();
});

Then(/^I see that outstanding item is marked as provided$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationsWithOutstandingReqPage.uploadedOutstandingItemIcon(
        testVars.randomNotificationId,
        testVars.outstandingItemTitle
    ));
});

When(/^I click on missing file hyperlink$/, async () => {
    await pages.erp.notificationsWithOutstandingReqPage.missingFilesLinks().first().Click({waitForVisibility: true});
});

Then(
    /^The notification list field "([^"]+)" should only contains elements matching following search criteria$/,
    async filterType => {
        await pages.erp.notificationsWithOutstandingReqPage.notificationRows().each(async notification => {
            const notificationId = await notification.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);

            switch (filterType) {
                case constants.searchTargets.EMPLOYEE_FIRST_NAME:
                case constants.searchTargets.EMPLOYEE_LAST_NAME:
                    await pages.erp.notificationsWithOutstandingReqPage.tableCellEmployee(notificationId);
                    expect(await pages.erp.notificationsWithOutstandingReqPage.tableCellEmployeeName(
                        notificationId).getText()).include(testVars.searchQuery);
                    break;
                case constants.searchTargets.NOTIFICATION_ID:
                    await pages.erp.notificationsWithOutstandingReqPage.tableCellNotification(notificationId);
                    expect(await pages.erp.notificationsWithOutstandingReqPage.tableCellNotification(
                        notificationId).getText()).include(testVars.searchQuery);
                    break;
                default:
                    throw new Error(`filter type [${filterType}] is not supported`);
            }
        });
    }
);

When(/^I click on "Show Actions" on random notification$/, async () => {
    const randomNotification = getRandomArrayItem(await pages.erp.notificationsWithOutstandingReqPage.notificationRows());
    const randomNotificationId = await randomNotification.getAttribute(constants.htmlAttributes.DATA_ROW_KEY);
    await pages.erp.notificationsWithOutstandingReqPage.tableCellActions(randomNotificationId).Click({waitForVisibility: true});
});

Then(/^I should see a list of outstanding requirements and the number of outstanding items$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    const outstandingItems = pages.erp.notificationsWithOutstandingReqPage.tableCellOutstandingItemsListRandom();
    const listCount = await outstandingItems.count();
    const [counter] = (await pages.erp.notificationsWithOutstandingReqPage.outstandingItemsCounterRandom().getText()).match(
        constants.regExps.range);
    await expect(listCount).to.be.gt(0, 'Outstanding items list shouldn\'t be empty');
    expect(counter).to.eq(`${listCount}`, 'Outstanding items counter should match list length');
});

Then(
    /^The (?:notifications|employees|leaves) list is filtered correctly by "(Case|Decision|Employee First Name|Employee Last Name|Notification Date|Job title|Group|Reason|DI)" field$/,
    async fieldName => {
        if (fieldName === 'DI') {
            await pages.erp.notificationsWithOutstandingReqPage.dICells().each(async cell => {
                if (testVars.searchQuery === translationsHelper.getTranslation('FINEOS_COMMON.GENERAL.NO')) {
                    expect(await cell.getText()).equal('--');
                } else {
                    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.checkIcon(
                        cell));
                }
            });
        } else if (fieldName === 'Decision') {
            await pages.erp.notificationsWithOutstandingReqPage.dICells().each(async cell => {
                if (testVars.searchQuery === translationsHelper.getTranslation('WIDGET.LEAVE_REQUEST_DENIED')) {
                    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.xIcon(
                        cell));
                } else {
                    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.checkIcon(
                        cell));
                }
            });
        } else {
            fieldName = fieldName.split(constants.regExps.spaces)[0].trim().toUpperCase();
            await pages.erp.notificationsWithOutstandingReqPage.columnHeader(fieldName).Click({waitForVisibility: true});
            await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.sortedCells().first());
            await pages.erp.notificationsWithOutstandingReqPage.sortedCells().each(async cell => {
                const actualCellTxt = (await cell.getText()).toString().toLowerCase();
                expect(actualCellTxt).include(testVars.searchQuery.toLowerCase());
            });
        }
    }
);

Then(
    /^I should see a list of outstanding requirements and the number of outstanding items for notification "([^"]+)"$/,
    async testDescriptor => {
        const notificationId = notifications.getNotification(testDescriptor).id;
        await waitHelpers.waitForSpinnersToDisappear();
        const outstandingItems = pages.erp.notificationsWithOutstandingReqPage.tableCellOutstandingItemsList(
            notificationId);
        const listCount = await outstandingItems.count();
        const [counter] = (await pages.erp.notificationsWithOutstandingReqPage.outstandingItemsCounter(
            notificationId).getText()).match('[0-9]+');
        await expect(listCount).to.be.gt(0, 'Outstanding items list shouldn\'t be empty');
        expect(counter).to.eq(`${listCount}`, 'Outstanding items counter should match list length');
    }
);

Then(/^notifications counter shows correct number of Notifications$/, async () => {
    await waitHelpers.waitForSpinnersToDisappear();
    const displayedNumberOfNotifications = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.numberOfNotifications().getText());
    const calculatedNumberOfNotifications = (await actions.erp.notificationsWithOutstandingReqActions.countNumberOfAllNotifications()).toString();

    expect(displayedNumberOfNotifications).equal(calculatedNumberOfNotifications);
});

Then(/^navigation panel containing links to other pages is shown$/, async () => {
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.paginationPreviousButton());
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.paginationNextButton());
    await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.paginationOptionsButton());

    const availableNumberOfPages = await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().count();
    const expectedNumberOfPages = await actions.erp.notificationsWithOutstandingReqActions.countNumberOfPages();

    expect(availableNumberOfPages).is.equal(expectedNumberOfPages);
});

Then(/^I should be able to navigate through a list of notification pages$/, async () => {
    await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons();

    for (let i = 0; i < await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().count(); i++) {
        const button = await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().get(i);

        await button.Click({waitForVisibility: true});
        await waitHelpers.waitForSpinnersToDisappear();

        await waitHelpers.waitForElementToBeVisible(await pages.erp.notificationsWithOutstandingReqPage.notificationRows().first());
    }
});

When(/^I choose a (10|20|50|100) page size$/, async pageSize => {
    await pages.erp.notificationsWithOutstandingReqPage.paginationOptionsButton().Click({waitForVisibility: true});
    await pages.erp.notificationsWithOutstandingReqPage.paginationOptionsListItem(pageSize).Click({waitForVisibility: true});
    await waitHelpers.waitForSpinnersToDisappear();

    const availableNotificationsNumber = await pages.erp.notificationsWithOutstandingReqPage.notificationRows().count();
    expect(availableNotificationsNumber).to.be.greaterThan(0);
    expect(availableNotificationsNumber).to.be.lessThan(Number.parseInt(pageSize, 10) + 1);
});

When(/^I navigate to page (previous|next|\d+) on notifications list$/, async pageNumber => {
    if (pageNumber === 'previous') {
        await pages.erp.notificationsWithOutstandingReqPage.paginationPreviousButton().Click({waitForVisibility: true});
    } else if (pageNumber === 'next') {
        await pages.erp.notificationsWithOutstandingReqPage.paginationNextButton().Click({waitForVisibility: true});
    } else {
        await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().get(pageNumber - 1).Click(
            {waitForVisibility: true});
    }
    await waitHelpers.waitForSpinnersToDisappear();
});

Then(/^I see page number (\d+) is (active|inactive)$/, async (pageNumber, pageState) => {
    if (pageState === 'active') {
        expect(await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().get(pageNumber - 1).getAttribute(
            'class')).contains('-active');
    } else {
        expect(await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().get(pageNumber - 1).getAttribute(
            'class')).not.contains('-active');
    }
});

Then(/^the number of notifications on current page is between 1 and (\d+)$/, async expectedNumberOfNotifications => {
    const availableNotificationsNumber = await pages.erp.notificationsWithOutstandingReqPage.notificationRows().count();
    expect(availableNotificationsNumber).to.be.greaterThan(0);
    expect(availableNotificationsNumber).to.be.lessThan(Number.parseInt(expectedNumberOfNotifications, 10) + 1);
});

Then(/^I should see correct number of pages in paging navigation panel$/, async () => {
    const availableNumberOfPages = await pages.erp.notificationsWithOutstandingReqPage.paginationPagesButtons().count();
    const expectedNumberOfPages = await actions.erp.notificationsWithOutstandingReqActions.countNumberOfPages();
    expect(availableNumberOfPages).is.equal(expectedNumberOfPages);
});

Then(/^Small number of notifications with outstanding requirements is available in the system$/, async () => {
    const displayedNumberOfNotifications = await waitHelpers.waitForElementToBeVisible(pages.erp.notificationsWithOutstandingReqPage.numberOfNotifications().getText());
    expect(Number(displayedNumberOfNotifications) <= 10).to.be.true;
});

Then(/^I can't see navigation panel containing links to other pages$/, async () => {
    await waitHelpers.waitForElementToBeNotVisible(pages.erp.notificationsWithOutstandingReqPage.paginationOptions());
});

Then(/^I see an absence calendar is displayed$/, async () => {
    await waitHelpers.waitForElementToBeVisible(await pages.erp.datePicker.prevMonth());
    await waitHelpers.waitForElementToBeVisible(await pages.erp.datePicker.nextMonth());
    await waitHelpers.waitForElementToBeVisible(pages.erp.datePicker.daysList().first());
});

Then(/^I see the calendar by default shows a month with the expected return to work date$/, async () => {
    const returnDatesList = await pages.erp.notificationsWithOutstandingReqPage.sortedCells();
    const returnDate = await returnDatesList[testVars.filtering.randomIndex].getText();
    const actualMonth = (await pages.erp.datePicker.actualMonthYearCards().first().getText()).toString();

    expect(returnDate).contain(actualMonth);
});

