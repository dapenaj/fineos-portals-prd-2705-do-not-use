const {Notifications} = require('./utils');

module.exports = new Notifications(browser.params.env.notifications);
