const {getRandomArrayItem} = require('../../common/helpers/mathHelper');

class Employees {

    /**
     *
     * @param {Array<{descriptor: string, id: string}>} list - list of employees
     */
    constructor(list) {
        this._list = list;
    }

    /**
     * This method gets Employee basing on test descriptor or throws error if Employee not found
     * @returns {{descriptor: string, id: string}}
     */
    getRandomEmployee() {
        const employees = this._list.filter(e => e.hasOwnProperty('isValidData') !== true);
        if (employees.length > 0) {
            const employeeObj = getRandomArrayItem(employees);
            const details = employeesDetails.filter(x => x.id === employeeObj.id);
            const data = details.length > 0 ? details[0].data : {};
            return Object.assign(employeeObj, data);
        }
        throw new Error(`Couldn't find any Employee for environment [${browser.params.env.name}]`);
    }

    /**
     * This method gets Employee basing on test descriptor or throws error if Employee not found
     * @param {string} descriptor - description of config item
     * @param {string} [envName=browser.params.env.name] - optional environment name for which we search notification
     * @returns {{descriptor: string, id: string}}
     */
    getEmployee(descriptor, envName = browser.params.env.name) {
        const results = this._list.filter(item => item.descriptor === descriptor.trim());
        if (results.length === 0) {
            throw new Error(`no results found for Employee descriptor ${descriptor}`);
        }
        const employeeObj = getRandomArrayItem(results);
        const details = employeesDetails.filter(x => x.id === employeeObj.id);
        const data = details.length > 0 ? details[0].data : {};
        return Object.assign(employeeObj, data);
    }
}

class Notifications {

    /**
     * @param {Array<{description: string, descriptor: string, id: string}>} list - list of notifications
     */
    constructor(list) {
        this._list = list;
    }

    /**
     * This method gets notification ID basing on test descriptor or throws error if notification not found
     * @param {string} descriptor - description of notification
     * @returns {{descriptor: string, id: string}}
     */
    getNotification(descriptor) {
        const results = this._list
            .filter(item => (item.descriptor === descriptor.trim() || item.description === descriptor.trim()));

        if (results.length > 0) {
            const ntnObj = getRandomArrayItem(results);
            const details = notificationsDetails.filter(x => x.id === ntnObj.id);
            const data = details.length > 0 ? details[0].data : {};
            return Object.assign(ntnObj, data);
        }
        throw new Error(`no results found for Notifications descriptor ${descriptor}`);
    }

    /**
     * This method gets notification ID basing on test descriptor or throws error if notification not found
     * @param {regex} notificationFormat - notification format
     * @returns {{descriptor: string, id: string}}
     */
    getRandomNotification(notificationFormat = /NTN-\d{1,4}/g) {
        let notifications = this._list.filter(e => e.hasOwnProperty('isValidData') !== true);
        notifications = notifications.filter(e => e.id.match(notificationFormat));

        if (notifications.length > 0) {
            const ntnObj = getRandomArrayItem(notifications);
            const details = notificationsDetails.filter(x => x.id === ntnObj.id);
            const data = details.length > 0 ? details[0].data : {};
            return Object.assign(ntnObj, data);
        }
        throw new Error(`Couldn't find any Notification for envirnoment [${browser.params.env.name}]`);
    }

    /**
     * This method maps notification card name to name existing in DOM
     * @param {string} cardName
     * @returns {string}
     */
    mapNotificationCardsNames(cardName) {
        let mappedCardName;
        switch (cardName.trim().toLowerCase()) {
            case 'workplace accommodation':
            case 'temporary accommodations':
                mappedCardName = constants.notificationCards.WORKPLACE_ACCOMMODATION;
                break;
            case 'job protected leave':
                mappedCardName = constants.notificationCards.JOB_PROTECTED_LEAVE;
                break;
            case 'notification summary':
                mappedCardName = constants.notificationCards.NOTIFICATION_SUMMARY;
                break;
            case 'timeline':
                mappedCardName = constants.notificationCards.TIMELINE;
                break;
            case 'wage replacement':
                mappedCardName = constants.notificationCards.WAGE_REPLACEMENT;
                break;
            default:
                throw new Error(`notification card name [${cardName}] is not supported`);
        }
        return mappedCardName;
    }

    /**
     * This method maps job protected leave type name to name existing in DOM
     * @param {string} leaveType
     * @returns {string}
     */
    mapLeaveTypeNames(leaveType) {
        let mappedLeaveType;

        switch (leaveType.trim().toLowerCase()) {
            case 'continuous time':
            case 'fixed':
                mappedLeaveType = constants.jobAbsenceLegend.CONTINUOUS_TIME;
                break;
            case 'intermittent time':
                mappedLeaveType = constants.jobAbsenceLegend.INTERMITTENT_TIME;
                break;
            case 'reduced schedule':
                mappedLeaveType = constants.jobAbsenceLegend.REDUCED_SCHEDULE;
                break;
            default:
                throw new Error(`job protected leave type [${leaveType}] is not supported`);
        }
        return mappedLeaveType;
    }
}

class Users {

    /**
     * @param {Array<{descriptor: string, username: string, password: string, displayName: string}>} list - list of
     *     users
     */
    constructor(list) {
        this._list = list;
    }

    /**
     * This method gets user basing on test descriptor or throws error if user not found
     * @param {string} descriptor - test data descriptor
     * @returns {{descriptor: string, username: string, password: string, displayName: string}}
     */
    getUser(descriptor) {
        const results = this._list.filter(item => item.descriptor === descriptor.trim());
        if (results.length > 0) {
            return getRandomArrayItem(results);
        }
        throw new Error(`Couldn't find User for descriptor [${descriptor}]`);
    }
}

module.exports.Users = Users;
module.exports.Notifications = Notifications;
module.exports.Employees = Employees;

