class ErrorPage {
    /**
     * @returns {ElementFinder}
     */
    errorPageContainer() {
        return $('[data-test-el="error-page-container"]');
    }

    /**
     * @returns {ElementFinder}
     */
    errorImage() {
        return $('[src="/assets/img/something_went_wrong_color.svg"]');
    }

    /**
     * @returns {ElementFinder}
     */
    errorPageHeader() {
        return $('[data-test-el$="ERROR_PAGE.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    errorPageLine1() {
        return $('[data-test-el$="ERROR_PAGE.LINE_1"]');
    }

    /**
     * @returns {ElementFinder}
     */
    errorPageLine2() {
        return $('[data-test-el$="ERROR_PAGE.LINE_2"]');
    }
}

module.exports = ErrorPage;
