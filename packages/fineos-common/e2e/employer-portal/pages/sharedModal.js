class SharedModal {
    /**
     * @returns {ElementArrayFinder}
     */
    okButton() {
        return $$('[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeXButton() {
        return $('[class*="CloseIcon"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeButton() {
        return $('[role="dialog"] [data-test-el="FINEOS_COMMON.GENERAL.CLOSE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeDrawerButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CLOSE"]');
    }

    /**
     * @param {sting} buttonName
     * @returns {ElementFinder}
     */
    commonButton(buttonName) {
        return $(`[data-test-el="FINEOS_COMMON.GENERAL.${buttonName.toString().toUpperCase()}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    submitErrorMessage() {
        return $('[data-test-el*="NOTIFICATIONS.ALERTS.MESSAGES.SUBMIT_WITH_ERROR_MESSAGE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    requiredSelectedOptionErrorMessage() {
        return $('[data-test-el*="FINEOS_COMMON.VALIDATION.REQUIRED_SELECTED_OPTION"]');
    }

    /**
     * @param {string} - fieldName
     * @returns {ElementFinder}
     */
    requiredErrorMessage(fieldName) {
        return $(`[data-test-el*="${fieldName}"] [data-test-el*="FINEOS_COMMON.VALIDATION.REQUIRED"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationSuccessImage() {
        return $('[role="dialog"] [data-test-el="success-response-image"]');
    }

    /**
     * @returns {ElementFinder}
     */
    mandatoryFieldsLabel() {
        return $('[data-test-el="FINEOS_COMMON.VALIDATION.ASTERISK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    useDifferentEmailButton() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    useDifferentPhoneNumberButton() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    uploadDocumentInput() {
        return $('input[type="file"]');
    }

    /**
     * @returns {ElementFinder}
     */
    yesButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.YES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    noButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.NO"]');
    }

    /**
     * @returns {ElementFinder}
     */
    saveChangesButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.SAVE_CHANGES"]');
    }

}

module.exports = SharedModal;
