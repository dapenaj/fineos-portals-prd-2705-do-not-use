class EditWorkPatternPage {
    /**
     * @param {string} mode (ADD, EDIT)
     * @returns {ElementFinder}
     */
    workPatternModalTitle(mode = 'EDIT') {
        return $(`[role="dialog"] [data-test-el="WORK_PATTERN.${mode.toUpperCase()}_WORK_PATTERN"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    mandatoryFieldsLabel() {
        return $('[data-test-el="FINEOS_COMMON.VALIDATION.ASTERISK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatternTypeSelectLabel() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatternTypeSelect() {
        return $$('[data-test-input-name="workPatternType"] [data-test-el*="ENUM_DOMAINS.WORK_PATTERN_TYPE."]').first();
    }

    /**
     * @param {string} type
     * @returns {ElementFinder}
     */
    workPatternType(type) {
        return $$(`[data-test-el="ENUM_DOMAINS.WORK_PATTERN_TYPE.${type.toUpperCase()}"]`).first();
    }

    /**
     * @returns {ElementFinder}
     */
    nonStandardWorkingWeekLabel() {
        return $('[data-test-el="WORK_PATTERN.NON_STANDARD_WORKING_WEEK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatterRepeatsSwitch() {
        return $('[data-test-el="work-pattern-repeats"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatterRepeatsSwitchLabel() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN_REPEATS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    additionalInformationTextArea() {
        return $('[name*="additionalInformation"]');
    }

    /**
     * @returns {ElementFinder}
     */
    numberOfWeeksInput() {
        return $('[name*="numberOfWeeks"]');
    }

    /**
     * @returns {ElementFinder}
     */
    theSameLengthDaySwitch() {
        return $('[data-test-el="work-day-is-same-length"]');
    }

    /**
     * @returns {ElementFinder}
     */
    theSameLengthDayLabel() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.WORK_DAY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    dayLengthInput() {
        return $('[name*="dayLength"]');
    }

    /**
     * @returns {ElementFinder}
     */
    nonStandardWorkingWeekSwitch() {
        return $('[data-test-el="non-standard-working-week-switch"]');
    }

    /**
     * @param {string} day
     * @returns {ElementFinder}
     */
    workingDayLabel(day) {
        const mappedDay = translationsHelper.getTranslation(`WORK_PATTERN.WEEK_DAYS_SHORTCUT.${day.toUpperCase()}`);
        return element.all(by.labelText(mappedDay)).last();
    }

    /**
     * @returns {ElementArrayFinder}
     */
    dayLabels() {
        return $$('[data-test-el="panel"] [data-test-el*="WORK_PATTERN.WEEK_DAYS_SHORTCUT."]');
    }

    /**
     * @param {string} day
     * @returns {ElementFinder}
     */
    workingDayInput(day) {
        const mappedDay = translationsHelper.getTranslation(`WORK_PATTERN.WEEK_DAYS_SHORTCUT.${day.toUpperCase()}`);
        return element.all(by.labelTextInput(mappedDay)).last();
    }

    /**
     * @returns {ElementArrayFinder}
     */
    allDaysInputs() {
        return $$('[data-test-el="text-input"][name*="attern"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    allDaysValues() {
        return $$('[data-test-el="panel"] [data-test-el="WORK_PATTERN.VIEW_DAY_INPUT"]');
    }

    /**
     * @param {string} workPatternType - rotating, fixed
     * @returns {ElementArrayFinder}
     */
    workingDaysInputs(workPatternType) {
        if (workPatternType.toString().toLowerCase().trim() === 'fixed') {
            return $$('[data-test-el="text-input"][name*="attern"]');
        }
        return $$('[data-test-el*="work-pattern"]:not([hidden]) [name*="workPattern"][data-test-el="text-input"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    weeksNumberSelects() {
        return $$('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.WEEKS_NUMBER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    validationError(errorName) {
        return $$(`[data-test-el="${errorName}"]`).first();
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('button[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $('button[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeButton() {
        return $('[class*="CloseIcon"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationHeader() {
        return $('[role="dialog"] [data-test-el$="WORK_PATTERN.SUCCESS_MODAL_HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationMessage() {
        return $('[role="dialog"] [data-test-el$="WORK_PATTERN.SUBMIT_SUCCESS_MESSAGE"]');
    }
}

module.exports = EditWorkPatternPage;
