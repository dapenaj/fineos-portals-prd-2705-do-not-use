class AddNewPhoneNumberModal {
    /**
     * @returns {ElementFinder}
     */
    modalHeader() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.ADD_PHONE_NUMBER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    modalSubtitle() {
        return $$('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    countryCodeInput() {
        return $('[name="newPhoneNumber.intCode"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneNumberInput() {
        return $('[name="newPhoneNumber.telephoneNo"]');
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('[data-test-el="add-phone-number-form"] [data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $('[data-test-el="add-phone-number-form"] [data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeXButton() {
        return $$('[class*="CloseIcon"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    mandatoryFieldsLabel() {
        return $$('[data-test-el="FINEOS_COMMON.VALIDATION.ASTERISK"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    areaCodeInput() {
        return $('[name="newPhoneNumber.areaCode"]');
    }

    /**
     * @returns {ElementFinder}
     */
    countryCodeLabel() {
        return $('[data-test-el="COMMON.PHONE.COUNTRY_CODE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    areaCodeLabel() {
        return $('[data-test-el="COMMON.PHONE.AREA_CODE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneNumberLabel() {
        return $('[data-test-el="COMMON.PHONE.PHONE_NO"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneTypeDropdown() {
        return $('[data-test-el="phone-type-dropdown"]');
    }

    /**
     * @returns {ElementFinder}
     */
    dropDownArrow() {
        return this.phoneTypeDropdown().$('[aria-label="down"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    phoneTypeOptions() {
        return $$('[role="listbox"] [role="option"]');
    }

    /**
     * @returns {ElementFinder}
     */
    landLineOption() {
        return $('[data-test-el*="ENUM_DOMAINS.PHONE_TYPE.PHONE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cellPhoneOption() {
        return $('[data-test-el*="ENUM_DOMAINS.PHONE_TYPE.CELL"]');
    }

    /**
     * @param {string} labelName - name of label
     * @returns {ElementArrayFinder}
     */
    formLabel(labelName) {
        return element(by.xpath(`//*[contains(@data-test-el,'.${labelName}')]/..`));
    }
}

module.exports = AddNewPhoneNumberModal;
