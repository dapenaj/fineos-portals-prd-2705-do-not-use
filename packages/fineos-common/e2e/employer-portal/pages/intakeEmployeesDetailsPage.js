class IntakeEmployeesDetailsPage {

    /**
     * @returns {ElementArrayFinder}
     */
    employeeDetailsFormHeader() {
        return $$('[data-test-el="INTAKE.EMPLOYEE_DETAILS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeeDetailsSection() {
        return $('[data-test-el="intake-employee-details"]');
    }

    /**
     * @returns {ElementFinder}
     */
    intakeFirstName() {
        return $('[name*="firstName"]');
    }

    /**
     * @param {string} elementName
     * @returns {ElementFinder}
     */
    intakeElement(elementName) {
        return $(`input[name="${elementName}"]`);
    }

    /**
     * @param {string} elementName
     * @returns {ElementFinder}
     */
    country(countryName) {
        const country = countryName.replace(' ', '_').toUpperCase();
        return $$(`[data-test-el="ENUM_DOMAINS.COUNTRIES.${country}"]`).first();
    }

    /**
     * @returns {ElementFinder}
     */
    intakeLastName() {
        return $('[name="lastName"]');
    }

    /**
     * @returns {ElementFinder}
     */
    occupationAndEarningsStep() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    initialRequestStep() {
        return $('[data-test-el="INTAKE.INITIAL_REQUEST.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    emailField() {
        return $('[name="email"]');
    }

    /**
     * @returns {ElementFinder}
     */
    countryCodeInput() {
        return $('[name="preferredPhoneNumber.intCode"]');
    }

    /**
     * @returns {ElementFinder}
     */
    areaCodeInput() {
        return $('[name="preferredPhoneNumber.areaCode"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneNoValidation() {
        return $('[data-test-el="phone-phone-no-field"][class*="ant-form-item-has-error"]');
    }

    /**
     * @returns {ElementFinder}
     */
    areaCodeValidation() {
        return $('[data-test-el="phone-area-code-field"][class*="ant-form-item-has-error"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneNumberInput() {
        return $('[name="preferredPhoneNumber.telephoneNo"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine1() {
        return $('[name="address.addressLine1"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressContainer() {
        return $('[data-test-el*="address"] [data-test-el="text-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine2() {
        return $('[name="address.addressLine2"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine3() {
        return $('[name="address.addressLine3"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine4() {
        return $('[name="address.addressLine4"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cityInput() {
        return $('[name="address.addressLine4"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO  FEP-1341
    stateProvinceDropDownArrow() {
        return $('[data-test-input-name*="address.addressLine6"] .ant-select-arrow');
    }

    /**
     * @returns {ElementFinder}
     */
    stateDropDown() {
        return $$('[data-test-el="dropdown-input"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    provinceDropDown() {
        return $$('[data-test-el="dropdown-input"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    countryDropDown() {
        return $('[data-test-input-name="address.country"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO  FEP-1341
    countryDropDownArrow() {
        return $('[data-test-input-name="address.country"] [class*="arrow"]');
    }

    /**
     * @returns {ElementFinder}
     */
    postalCodeInput() {
        return $('[name="address.postCode"]');
    }

    /**
     * @returns {ElementFinder}
     */
    proceedButton() {
        return $('[data-test-el="INTAKE.PROCEED"]');
    }

    /**
     * @returns {ElementFinder}
     */
    clickButton(buttonName) {
        const btn = buttonName.toUpperCase().replace(constants.regExps.spaces, '_');
        return $(`[data-test-el="INTAKE.${btn}"]`);
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO  FEP-1341
    buttonLoadingIcon() {
        return $$('[class*="btn-loading-icon"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmAndProceedButton() {
        return $('[data-test-el="INTAKE.CONFIRM_AND_PROCEED"]');
    }

    /**
     * @returns {ElementFinder}
     */
    saveAndProceed() {
        return $('[data-test-el="INTAKE.SAVE_AND_PROCEED"]');
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    statesList() {
        return $$('[data-test-el*="-option"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    selectedItem() {
        return $('[data-test-el="undefined-option"][aria-selected="true"]');
    }

    /**
     * @returns {ElementFinder}
     */
    abandonIntakeButton() {
        return $('[data-test-el="INTAKE.ABANDON_INTAKE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    fullName() {
        return $('[data-test-el="intake-username"]');
    }

    /**
     * @returns {ElementFinder}
     */
    firstName() {
        return $('[data-test-el="COMMON.FIELDS.FIRST_NAME_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    lastName() {
        return $('[data-test-el="COMMON.FIELDS.LAST_NAME_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    fullNameHeader() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.FULL_NAME"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneCallsPreferences() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressHeader() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.PRIMARY_ADDRESS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressCity() {
        return $('[data-test-el="COMMON.ADDRESS.CITY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLines1() {
        return $('[data-test-el="address-line1-field"] [data-test-el="text-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
     addressLines2() {
        return $('[data-test-el="address-line2-field"] [data-test-el="text-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
     addressLines3() {
        return $('[data-test-el="address-line3-field"] [data-test-el="text-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editPhoneNumberLink() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.EDIT_PREFERENCES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    useDifferentPhoneNumber() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.USE_DIFFERENT_PHONE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO  FEP-1341
    phoneRadioBtn() {
        return element.all(by.xpath(
            '//span[@data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER"]/../..//div[2]//div//div[@data-test-el="panel"]'));
    }

    /**
     * @returns {ElementArrayFinder}
     */
    phoneNumbers() {
        return $$('[data-test-el="phone-number-item"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO  FEP-1341
    selectedPhoneNumber() {
        return $$('[data-test-el="phone-number-item"] [class*="checked"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editLink() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.EDIT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editAddressLine1() {
        return $('[data-test-el="address-line1-field"] [name="customer.address.addressLine1"]');
    }

    /**
     * @param {string} label - field label
     * @returns {ElementFinder}
     */
    intakeField(label) {
        label = label.trim().replace(constants.regExps.spaces, '_').toUpperCase();
        const mappedFieldName = translationsHelper.getTranslation(`COMMON.ADDRESS.${label}`);
        return element.all(by.labelTextInput(mappedFieldName)).last();
    }

    /**
     * @returns {ElementFinder}
     */
    editAddressLine2() {
        return $('[data-test-el="address-line2-field"] [name="customer.address.addressLine2"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editAddressLine3() {
        return $('[data-test-el="address-line3-field"] [name="customer.address.addressLine3"]');
    }

    /**
     * @returns {ElementFinder}
     */
    existingEmployeeDescription() {
        return $('[data-test-el*="INTAKE.EMPLOYEE_DETAILS.EXISTING_DESCRIPTION"]');
    }

    /**
     * @returns {ElementFinder}
     */
    newEmployeeDescription() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.NEW_DESCRIPTION"]');
    }

    /**
     * @returns {ElementFinder}
     */
    backButton() {
        return $('[data-test-el="NAVIGATION.BACK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    getInputByLabel(label) {
        return element(by.labelTextInput(label));
    }
}

module.exports = IntakeEmployeesDetailsPage;
