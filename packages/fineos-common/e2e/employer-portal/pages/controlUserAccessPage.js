class ControlUserAccessPage {

    /**
     * @returns {ElementFinder}
     */
    pageHeader() {
        return $('[data-test-el="control-access-user-title"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    usersList() {
        return $$('[data-test-el="control-access-user-list-item"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    userNamesList() {
        return $$('[data-test-el="control-access-user-username"]');
    }

    /**
     * @param {ElementFinder} user - list element representing user
     * @returns {ElementFinder}
     */
    userName(user) {
        return user.$('[data-test-el="control-access-user-username"]');
    }

    /**
     * @param {ElementFinder} user - list element representing user
     * @returns {ElementFinder}
     */
    userRole(user) {
        return user.$('[data-test-el="control-access-user-user-role"]');
    }

    /**
     * @param {ElementFinder} user - list element representing user
     * @returns {ElementFinder}
     */
    iconEnabled(user) {
        return user.$('[data-icon="check"]');
    }

    /**
     * @param {ElementFinder} user - list element representing user
     * @returns {ElementFinder}
     */
    iconDisabled(user) {
        return user.$('[data-icon="times"]');
    }

    /**
     * @param {ElementFinder} user - list element representing user
     * @returns {ElementFinder}
     */
    actionButton(user) {
        return user.$('[data-test-el^="CONTROL_USER_ACCESS."][data-test-el$="_USER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationPopover() {
        return $('[role="dialog"][data-enter]');
    }

    /**
     * @returns {ElementFinder}
     */
    buttonYesOnConfirmationPopover() {
        return this.confirmationPopover().$('[data-test-el="FINEOS_COMMON.GENERAL.YES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    buttonNoOnConfirmationPopover() {
        return this.confirmationPopover().$('[data-test-el="FINEOS_COMMON.GENERAL.NO"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationPopoverBody() {
        return this.confirmationPopover().$('span[data-test-el="CONTROL_USER_ACCESS.CHANGE_PERMISSIONS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    alertUserDisabled(userName) {
        return element(by.cssContainingText(
            '[role="status"]',
            `${userName} can no longer access Employer Portal`
        ));
    }

    /**
     * @returns {ElementFinder}
     */
    alertUserEnabled(userName) {
        return element(by.cssContainingText(
            '[role="status"]',
            `${userName} now has access to Employer Portal`
        ));
    }

    /**
     * @returns {ElementFinder}
     */
    searchUserInput() {
        return $('input[data-test-el="CONTROL_USER_ACCESS.SEARCH_PLACEHOLDER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    resetFilterButton() {
        return $('[data-test-el="control-user-access-form"] [aria-label="close-circle"]');
    }

}

module.exports = ControlUserAccessPage;
