class WebMessagesDrawer {
    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementFinder}
     */
    messageSubject() {
        return $('[class*="drawerMessageSubject"]');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementFinder}
     */
    messageIcon() {
        return $('[class*="drawerTitleIcon"]');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementFinder}
     */
    messageDate() {
        return $('[class*="drawerMessageContactDateTime"]');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementFinder}
     */
    messageText() {
        return $('[class*="messageContainer"] [data-test-el="message-narrative"]');
    }
}

module.exports = WebMessagesDrawer;
