class TimeTakenPage {

    /**
     * @returns {ElementArrayFinder}
     */
    leavePlans() {
        return $$('[class*="card"][class*="time-taken"]');
    }

    /**
     * @param {ElementFinder} leavePlanCard - leave plan card element
     * @param {string} elementName
     *
     * @returns {ElementFinder}
     */
    leavePlanElement(leavePlanCard, elementName) {
        elementName = elementName.replace(constants.regExps.spaces, '_').toUpperCase();

        if (elementName.includes('ENTITLEMENT')) {
            return leavePlanCard.$(`[data-test-el="PROFILE.TIME_TAKEN_TAB.${elementName}.LABEL"]`);
        }
        return leavePlanCard.$(`[data-test-el="PROFILE.TIME_TAKEN_TAB.${elementName}"]`);
    }

    /**
     * @param {ElementFinder} leavePlanCard - leave plan card element
     * @returns {ElementFinder}
     */
    leavePlanName(leavePlanCard) {
        return leavePlanCard.$('[class*="cardTitle"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leavePlanTitle() {
        return $$('div[class*="card-module_card"] div[class*="cardTitle"]');
    }

    /**
     * @param {ElementFinder} leavePlanCard - leave plan card element
     * @returns {ElementFinder}
     */
    leavePlanDescription(leavePlanCard) {
        return leavePlanCard.$('[class*="notificationMessage"]');
    }

    /**
     * @param {ElementFinder} leavePlanCard - leave plan card element
     * @returns {ElementFinder}
     */
    calculationMethodHelpIcon(leavePlanCard) {
        return leavePlanCard.$('[data-test-el="time-taken-icon"]');
    }

    /**
     * @param {string} calculationMethod
     *
     * @returns {ElementFinder}
     */
    calculationMethodTooltip(calculationMethod) {
        calculationMethod = calculationMethod.replace(constants.regExps.spaces, '_').toUpperCase();

        return $(`[role='tooltip'] [data-test-el*='PROFILE.TIME_TAKEN_TAB.CALCULATION_METHOD_TYPE.${calculationMethod}']`);
    }

    /**
     * @returns {ElementFinder}
     */
    emptyLeavePlanPlaceholder() {
        return $('[data-test-el="PROFILE.TIME_TAKEN_TAB.EMPTY_STATE"]');
    }
}

module.exports = TimeTakenPage;
