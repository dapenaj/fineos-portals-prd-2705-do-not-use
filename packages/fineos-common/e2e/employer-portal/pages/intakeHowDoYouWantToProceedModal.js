class IntakeHowDoYouWantToProceedModal {
    /**
     * @returns {ElementFinder}
     */
    modalHeader() {
        return $('[data-test-el*="INTAKE.PROCEED_MODAL.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    modalMessage() {
        return $('[data-test-el="INTAKE.PROCEED_MODAL.BODY"]');
    }

    /**
     * @param {btnName}
     * @returns {ElementFinder}
     */
    modalButton(btnName) {
        btnName = btnName.toUpperCase();
        return $(`[data-test-el="INTAKE.PROCEED_MODAL.${btnName}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    closeButton() {
        return $('[class*="dialogCloseIcon"]');
    }
}

module.exports = IntakeHowDoYouWantToProceedModal;
