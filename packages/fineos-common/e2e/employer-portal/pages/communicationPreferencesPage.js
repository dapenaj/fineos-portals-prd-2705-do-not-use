class CommunicationPreferencesPage {
    /**
     * @returns {ElementFinder}
     */
    writtenCorrespondenceSectionTitle() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    alertsAndUpdatesTxt() {
        return $$(
            '[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.MESSAGE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    alertsAndUpdatesEditLink() {
        return $(
            '[data-test-el="notification-of-updates-preference"] [data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    writtenCorrespondenceEditLink() {
        return $(
            '[data-test-el="written-correspondence-preference"] [data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneCallsLink() {
        return $(
            '[data-test-el="direct-correspondence-preference"] button[data-test-el*="PROFILE.COMMUNICATION_PREFERENCES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneCallsEditLink() {
        return $(
            '[data-test-el="direct-correspondence-preference"] [data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneCallsAddLink() {
        return $(
            '[data-test-el="direct-correspondence-preference"] [data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.ADD"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    availablePhoneNumbers() {
        return $$('[data-test-el*="phone-number-card"]');
    }

    /**
     * @returns {ElementFinder}
     */
    theyWantToReceiveAlertsLink() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.RECEIVE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    communicationPreferencesPhones() {
        return $(
            '[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.PHONE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    communicationPreferencesPhonesEmpty() {
        return $(
            '[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.EMPTY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    writtenCorrespondenceEmpty() {
        return $(
            '[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.EMPTY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    writtenCorrespondenceEmail() {
        return $(
            '[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.NOTIFIED_BY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    writtenCorrespondencePhone() {
        return $(
            '[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.NOTIFIED_BY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    writtenCorrespondenceChannel() {
        return $(
            '[data-test-el*="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE"] [data-test-el*="PROFILE.COMMUNICATION_PREFERENCES_TAB."]');
    }
}

module.exports = CommunicationPreferencesPage;
