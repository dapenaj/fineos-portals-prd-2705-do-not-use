class InitialRequestPage {

    /**
     * @returns {ElementArrayFinder}
     */
    initialRequestHeader() {
        return $$('[data-test-el="INTAKE.INITIAL_REQUEST.HEADER"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationReasonRadioButtons() {
        return $$('[data-test-el^="ENUM_DOMAINS.NOTIFICATION_REASON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    reasonRadioBtn(reason) {
        return $(`[data-test-el="ENUM_DOMAINS.NOTIFICATION_REASON.${reason}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    accommodationRequiredRadioBtn() {
        return $('[data-test-el="ENUM_DOMAINS.NOTIFICATION_REASON.ACCOMMODATION_REQUIRED_TO_REMAIN_AT_WORK"]');
    }

    /**
     * @param {string} labelName - name of label
     * @returns {ElementArrayFinder}
     */
    formLabel(labelName) {
        const translation = translationsHelper.getTranslation(`INTAKE.INITIAL_REQUEST.${labelName}`);
        return element(by.labelText(translation));
    }

    /**
     * @returns {ElementFinder}
     */
    lastWorkingDay() {
        return $('[name="notificationLastWorkingDay"]');
    }

    /**
     * @returns {ElementFinder}
     */
    additionalInformation() {
        return $('[name="notificationDescription"]');
    }

    /**
     * @returns {ElementFinder}
     */
    finishAndSubmitBtn() {
        return $('[data-test-el="INTAKE.FINISH"]');
    }

    /**
     * @returns {ElementFinder}
     */
    calendarField() {
        return $('[data-test-el="date-picker"]');
    }

    /**
     * @returns {ElementFinder}
     */
    occupationAndEarningsStep() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    intakeDetailsHeader() {
        return $$('[data-test-el="INTAKE.EMPLOYEE_DETAILS.HEADER"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    backButton() {
        return $('[data-test-el="NAVIGATION.BACK"]');
    }
}

module.exports = InitialRequestPage;
