class EmployeePersonalDetailsPage {
    /**
     * @returns {ElementFinder}
     */
    editPersonalDetailsTitle() {
        return $('[role="dialog"] [data-test-el="PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    primaryAddressLabel() {
        return $('[data-test-el="PROFILE.PERSONAL_DETAILS_TAB.PRIMARY_ADDRESS"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    editPersonalDetailsFields() {
        return element.all(by.xpath(
            '//*[@role="dialog"]//div[@class="ant-form-item-control-input"]//*[text() | @value]'));
    }

    /**
     * @param {string} inputName
     * @returns {ElementFinder}
     */
    editPersonalDetailsInput(inputName) {
        return element(by.labelTextInput(inputName));
    }

    /**
     * @param {string} inputName
     * @returns {ElementFinder}
     */
    editCurrentOccupationInput(inputName) {
        inputName = inputName.replace(constants.regExps.spaces, '_').toUpperCase();
        const fieldLabel = translationsHelper.getTranslation(`PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.${inputName}`);

        return element(by.labelTextInput(fieldLabel));
    }

    /**
     * @param {string} inputName
     * @returns {ElementFinder}
     */
    inputRequiredErrorMessage(inputName) {
        return element(by.xpath(`//span[text()="${inputName}"]/../../../..//span[@data-test-el="FINEOS_COMMON.VALIDATION.REQUIRED"]`));
    }

    /**
     * @param {string} selectName
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    select(selectName) {
        return element(by.xpath(`//span[@data-test-el="COMMON.ADDRESS.${selectName.toUpperCase()}"]/../../../..//div[@class='ant-select-selector']`));
    }

    /**
     * @param {string} option - option name
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    selectOption(option) {
        return element(by.cssContainingText('div[class="ant-select-item-option-content"]', option));
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    selectOptions() {
        return element.all(by.xpath('//div[@class="ant-select-item-option-content"][text()]'));
    }

    /**
     * @param {string} selectName
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    selectArrow(selectName) {
        return $(`div[data-test-el*="${selectName.toLowerCase()}"] span[class="ant-select-arrow"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    canadianPostalCodeErrorMessage() {
        return $('[data-test-el="COMMON.VALIDATION.CANADA_POSTAL_CODE_FORMAT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    clearDateOfBirthButton() {
        return $('[data-test-el="date-of-birth-field"] [data-icon="close-circle"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    addressFields() {
        return $$('[data-test-el="edit-personal-details-form"] [name*="address"]');
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('[data-test-el="edit-personal-details-form"] [data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $('[data-test-el="edit-personal-details-form"] [data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeButton() {
        return $('[class*="CloseIcon"]');
    }
}

module.exports = EmployeePersonalDetailsPage;
