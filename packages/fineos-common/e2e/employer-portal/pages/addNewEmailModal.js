class AddNewEmailModal {
    /**
     * @returns {ElementFinder}
     */
    modalHeader() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    emailAddressInput() {
        return $('[name="emailAddress"]');
    }

    /**
     * @returns {ElementFinder}
     */
    emailAddressLabel() {
        const label = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.ADD_EMAIL_LABEL');
        return element(by.labelText(label));
    }

    /**
     * @returns {ElementFinder}
     */
    mandatoryFieldsLabel() {
        return $$('[data-test-el="FINEOS_COMMON.VALIDATION.ASTERISK"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $$('[data-test-el="FINEOS_COMMON.GENERAL.OK"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $$('[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    closeXButton() {
        return $$('[data-test-el="modal-close-icon-button"]').last();
    }

    /**
     * @returns {ElementArrayFinder}
     */
    radioButtons() {
        return $$('[data-test-el="radio-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    invalidEmailFormatErrorMessage() {
        return $('[data-test-el*="FINEOS_COMMON.VALIDATION.NOT_EMAIL"]');
    }
}

module.exports = AddNewEmailModal;
