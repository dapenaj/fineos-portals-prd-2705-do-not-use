class PaymentHistoryPage {

    /**
     * @returns {ElementFinder}
     */
    historyPageHeader() {
        return $('[data-test-el="PAYMENTS.PAYMENTS_VIEW_HISTORY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelHistoryButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CLOSE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeHistoryButton() {
        return $$('[class*="CloseIcon"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    paymentsFilterButton() {
        return $('[data-test-el="PAYMENTS.FILTER_DATE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    paymentsListContainer() {
        return $('[data-test-el="payments-list"]');
    }

    /**
     * @returns {ElementFinder}
     */
    benefitName() {
        return $('[data-test-el="benefit-case-type"]');
    }

    /**
     * @returns {ElementFinder}
     */
    paymentsInfo() {
        return $('span[data-test-el="PAYMENTS.PAYMENTS_INFO"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    collapsiblePayments() {
        return this.paymentsListContainer().$$('[data-test-el="payment-element"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    // We can't put data-test-el higher in structure.
    paymentHeaders() {
        return this.paymentsListContainer().$$('div[class*="ant-collapse-header"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    paymentDate(parentElement) {
        return parentElement.$('div[data-test-el="payment-header"] > div > span');
    }

    /**
     *
     * @param {string} inputName - name of date input [from, until]
     * @returns {ElementFinder}
     */
    filterFromDateInput(inputName) {
        return $(`[data-test-el="${inputName}"]`);
    }

    /**
     *
     * @returns {ElementFinder}
     */
    applyFilterButton() {
        return $('[data-test-el="PAYMENTS.APPLY_FILTER"]');
    }

    /**
     *
     * @returns {ElementFinder}
     */
    cancelFilterButton() {
        return $('[data-test-el="drawer-close"]');
    }

    /**
     *
     * @returns {ElementFinder}
     */
    resetFilterButton() {
        return $('[data-test-el="PAYMENTS.RESET_FILTER"]');
    }
}

module.exports = PaymentHistoryPage;
