class NotificationPage {

    /**
     * @param {string} translationId
     * @returns {ElementFinder}
     */
    translationElement(translationId) {
        return $(`[data-test-el*="${translationId}"]`);
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    genericLabelsList(parentElement) {
        return parentElement.$$('[data-test-el="property-item-label"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    statusIndicatorField(parentElement) {
        return parentElement.$('[data-test-el="status-label"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sectionNotificationDetails() {
        return $('[data-test-el="notification-detail"],[data-test-el="notification"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sectionNotificationDetailHeader() {
        return $('[data-test-el="notification-detail-header"], [data-test-el="notification-header"]');
    }

    /**
     * @param {string} infoDetails
     * @returns {ElementFinder}
     */
    employeeNotificationDetailHeader(infoDetails) {
        return $(`[data-test-el="notification-header-${infoDetails}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    textNotificationId() {
        return this.sectionNotificationDetailHeader().$('[data-test-el="notification-header-notification-id"]');
    }

    /**
     * @returns {ElementFinder}
     */
    textFullName() {
        return this.sectionNotificationDetailHeader().$('[data-test-el="notification-header-customer-link"]');
    }

    /**
     * @returns {ElementFinder}
     */
    textJobTitle() {
        return this.sectionNotificationDetailHeader().$('[data-test-el="notification-header-job-title"]');
    }

    /**
     * @returns {ElementFinder}
     */
    textOrganisation() {
        return this.sectionNotificationDetailHeader().$('[data-test-el="notification-header-organisation-unit"]');
    }

    /**
     * @returns {ElementFinder}
     */
    textWorkSite() {
        return this.sectionNotificationDetailHeader().$('[data-test-el="notification-header-work-site"]');
    }

    /**
     * @returns {ElementFinder}
     */
    textNotificationReason() {
        return $('[data-test-el*="ENUM_DOMAINS.NOTIFICATION_REASON"]');
    }

    /**
     * @param {string} fieldName - name of searched field
     * @returns {ElementFinder}
     */
    textValueByFieldName(fieldName) {
        return element(by.xpath(`//div[@data-test-el="property-item"][.//*[text() = "${fieldName}"]]/*[@data-test-el="property-item-value"]`));
    }

    /**
     * @param {string} fieldName - name of searched field
     * @returns {ElementArrayFinder}
     */
    textValueJobProtected(fieldName) {
        return element.all(by.xpath(`//div[@data-test-el="property-item"][.//*[text() = "${fieldName}"]]/*[@data-test-el="property-item-value"]`));
    }

    /**
     * @param {string} fieldName - name of searched field
     * @returns {ElementFinder}
     */
    textFieldByName(fieldName) {
        return element(by.xpath(`//div[@data-test-el="property-item"][.//*[text() = "${fieldName}"]]`));
    }

    /**
     * @param {string} fieldName - name of searched field
     * @returns {ElementFinder}
     */
    markedTextFieldByName(fieldName) {
        return this.textFieldByName(fieldName).$('mark');
    }

    /**
     * @returns {ElementFinder}
     */
    textNotificationDate() {
        const txt = translationsHelper.getTranslation('NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON');
        return this.textValueByFieldName(txt);
    }

    /**
     * @returns {ElementFinder}
     */
    textCaseProgress() {
        const txt = translationsHelper.getTranslation('NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS');
        return this.textValueByFieldName(txt);
    }

    /**
     * @returns {ElementFinder}
     */
    textFirstDayMissingWork() {
        return this.textValueByFieldName('First day missing work');
    }

    /**
     * @returns {ElementFinder}
     */
    textAccidentDate() {
        return this.textValueByFieldName('Accident date');
    }

    /**
     * @param cardName name of notification card
     * @returns {ElementArrayFinder}
     */
    cards(cardName) {
        let locator;

        if (cardName === 'job-protected-leave') {
            locator = `div[data-test-el=${cardName}] > div`;
        } else {
            locator = `div[data-test-el=${cardName}]`;
        }
        return $$(locator);
    }

    /**
     * @param cardName name of notification card
     * @returns {ElementFinder}
     */
    collapsibleHeader(cardName) {
        return this.cards(cardName).first().$('[role="button"]');
    }

    /**
     * @param cardName name of notification card
     * @returns {ElementFinder}
     */
    nonCollapsibleHeader(cardName) {
        return this.cards(cardName).first().$('[data-test-el="panel-header"]');
    }

    /**
     * @param cardName name of notification card
     * @returns {ElementArrayFinder}
     */
    collapsibleCards(cardName) {
        return this.cards(cardName).first().$$('[data-test-el="collapsible-card"]');
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    collapsibleCardsOfParent(parentElement) {
        return parentElement.$$('[data-test-el="collapsible-card"]');
    }

    /**
     * @param {string }cardName name of notification card
     * @returns {ElementFinder}
     */
    collapsiblePaymentsCard(cardName) {
        return this.cards(cardName).first().$('[data-test-el="latest-payments"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    collapsiblePayments(parentElement) {
        return parentElement.$$('[data-test-el="payment-element"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    collapsibleContainer(parentElement) {
        return parentElement.$('div[role="button"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    entitlementBeginsTooltip(parentElement) {
        return parentElement.$('[data-test-el="paid-leave-tooltip"]');
    }

    /**
     *
     * @returns {ElementFinder}
     */
    entitlementBeginsTooltipInfo() {
        return $('[data-test-el="NOTIFICATIONS.WAGE_REPLACEMENT.ENTITLEMENTS_TOOLTIP"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    paymentHeaderDetails(parentElement) {
        return parentElement.$('div[data-test-el="payment-header"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    paymentMethodTooltipIcon(parentElement) {
        return parentElement.$('[data-test-el="payment-info-icon"]');
    }

    /**
     * @param {string} tooltipTxt - expected field label
     *
     * @returns {ElementFinder}
     */
    paymentMethodTooltip(tooltipTxt) {
        return $(`span[data-test-el="PAYMENTS.${tooltipTxt}"]`);
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    paymentBreakdownButton(parentElement) {
        return parentElement.$('[data-test-el="PAYMENTS.SHOW_BREAKDOWN"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    adjustmentDetailsButton(parentElement) {
        return parentElement.$$('button[data-test-el="show-payment-breakdown-details"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    adjustmentDetails(parentElement) {
        return parentElement.$$('div[data-test-el="payment-line-properties"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    adjustmentHeaders(parentElement) {
        return parentElement.$$('[data-test-el="payment-item"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    adjustmentSummaryPayment(parentElement) {
        return parentElement.$$('[data-test-el="payment-summary"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    caseStatus(parentElement) {
        return parentElement.$('.ant-badge-status-dot');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    caseStatusText(parentElement) {
        return parentElement.$('[data-test-el="status-label"] [data-test-el*="STATUS"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    caseArrowIcon(parentElement) {
        return parentElement.$('[role="button"] [role="img"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    caseDollarIcon(parentElement) {
        return parentElement.$('[data-test-el="panel-header"] [aria-label*="dollar"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @param {string} text - text that element contains
     * @returns {ElementFinder}
     */
    caseDetailsField(parentElement, text) {
        return parentElement
            .all(by.xpath(`//span[contains(text(),"${text}")]/../../span[@data-test-el="property-item-value"]`)).first();
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @param {string} text - text that element contains
     * @returns {ElementArrayFinder}
     */
    caseDetailsFields(parentElement, text) {
        return parentElement.all(by.xpath(`//span[contains(text(),"${text}")]/../../span[@data-test-el="property-item-value"]/span`));
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    wageReplacementCaseHeader(parentElement) {
        return parentElement.$('[data-test-el*="benefit-header-name"]');
    }

    /**
     * @returns {ElementFinder}
     */
    wageReplacementEmptyPlaceholder() {
        return $('span[data-test-el="NOTIFICATIONS.WAGE_REPLACEMENT.EMPTY"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    latestPayments(parentElement) {
        return parentElement.$('[data-test-el="latest-payments"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    latestPaymentsHeader(parentElement) {
        return parentElement.$('[data-test-el="latest-payments-header"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    latestPaymentsHistoryButton(parentElement) {
        return parentElement.$('[data-test-el="PAYMENTS.PAYMENTS_VIEW_HISTORY_LINK"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    accommodationCaseHeader(parentElement) {
        return parentElement.$('[data-test-el="accomodation-header-title"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    accommodationCaseDate(parentElement) {
        return parentElement.$('[data-test-el="accomodation-header-title"]');
    }

    /**
     * @returns {ElementFinder}
     */
    proposedAccommodationsPlaceholder() {
        return $('span[data-test-el="NOTIFICATIONS.ACCOMMODATIONS.EMPTY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    showReasonButton() {
        return $('span[data-test-el="NOTIFICATIONS.ACCOMMODATIONS.SHOW_REASON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    hideReasonButton() {
        return $('span[data-test-el="NOTIFICATIONS.ACCOMMODATIONS.HIDE_REASON"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    reasonsList() {
        return $$('p[data-test-el*="accommodation-reason"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    jobCaseHeader(parentElement) {
        return parentElement.$('div[data-test-el*="-header"][class*="leave"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    jobLeavePlansList(parentElement) {
        return parentElement.$$('[data-test-el="job-protected-leave-plan-header"]');
    }

    /**
     * @returns {ElementFinder}
     */
    jobNoLeavePlansSection() {
        return $('[data-test-el="job-protected-leave"] [data-test-el="warning-state"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    jobNoLeavePlansList() {
        return $$('[data-test-el$="JOB_PROTECTED_LEAVE.ASSESSMENT_CONCLUDED_TITLE"]~ul li');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    jobNoLeavePlansDecisionsList(parentElement) {
        return parentElement.$('[data-test-el*="STARTING_ON"]>b:nth-of-type(1)');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    jobLeavePlanHeader(parentElement) {
        return this.jobLeavePlansList(parentElement).first();
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    jobLeavePlanManagedBy(parentElement) {
        return parentElement.$('data-test-el="FINEOS_COMMON.HANDLER_INFO.MANAGED_BY"');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    jobLeavePlanPhoneNumber(parentElement) {
        return parentElement.$('data-test-el="FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER"');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    // TODO-FEP-1341
    jobPaidLeaveIndicator(parentElement) {
        return parentElement.$('[data-icon="dollar"]');
    }

    /**
     * @returns {ElementFinder}
     */
    jobPaidLeaveIndicatorTooltip() {
        return $('[data-test-el*="NOTIFICATIONS.JOB_PROTECTED_LEAVE.PAID_LEAVE_TOOLTIP"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    jobPeriodsList(parentElement) {
        return parentElement.$$('[data-test-el="card-body-with-status"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    jobLegend(parentElement) {
        return parentElement.$('[data-test-el="job-protected-leave-legend"]');
    }

    /**
     * @returns {ElementFinder}
     */
    timelineLegendButton() {
        return $('[name="legend-toggle-control"] [data-test-el*="CAPTION"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    timelineLegendColors() {
        return element.all(by.xpath('//p/span[contains(@data-test-el,".LEGEND_COLOR")]/../../ul/li'));
    }

    /**
     * @returns {ElementFinder}
     */
    timelineTodayDate() {
        return $('[data-test-el="timeline-today-tooltip"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    timelineLegendGraphicPatterns() {
        return element.all(by.xpath('//p/span[contains(@data-test-el,".LEGEND_GRAPHIC_PATTERN")]/../../ul/li'));
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @param {string} text - text that element contains
     * @returns {ElementArrayFinder}
     */
    timelineSections(parentElement, text) {
        return parentElement.all(by.cssContainingText('button', text));
    }

    /**
     * @param {ElementFinder} timelineSectionElement - parent element
     * @param {string} text - text that element contains
     * @returns {ElementArrayFinder}
     */
    timelineSectionCases(timelineSectionElement, text) {
        return timelineSectionElement.all(by.xpath('../following-sibling::div'));
    }

    /**
     * @returns {ElementFinder}
     */
    timelineCalendar() {
        return $('div[class*="canvasContainer"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    accommodationCardIcon(parentElement) {
        return parentElement.$('svg[data-icon="universal-access"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    jobCardIcon(parentElement) {
        return parentElement.$('svg[data-icon="safety"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementArrayFinder}
     */
    proposedAccommodations(parentElement) {
        return parentElement.$$('div[data-test-el="proposed-accommodations"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    proposedAccommodationsTitle(parentElement) {
        return parentElement.$('[data-test-el="accommodation-title"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @param {string} leaveType - type of job protected leave
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    absenceCard(parentElement, leaveType) {
        return parentElement.$$(`div[class*="collapse-content-box"] div[class*="${leaveType}"] div[class*="ant-row-top"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    outstandingEmptyMessage() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.OUTSTANDING.EMPTY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    documentsEmptyMessage() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.DOCUMENTS.EMPTY"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    allOutstandingItems() {
        return $$('li[class*="outstanding-information-list"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    providedOutstandingItem(parentElement) {
        return parentElement.$('[data-test-el="NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    providedOutstandingItemIcon(parentElement) {
        return parentElement.$('[data-test-el="uploaded-outstanding-information-icon"]');
    }

    /**
     * @param {string} itemTxt - expected item text
     * @returns {ElementFinder}
     */
    uploadedOutstandingItemIcon(itemTxt) {
        return element(by.cssContainingText(
            '[data-test-el="upload-outstanding-information-item"]',
            `${itemTxt}`
        )).element(by.css('[data-test-el="uploaded-outstanding-information-icon"]'));
    }

    /**
     * @param {string} itemTxt - expected item text
     * @returns {ElementFinder}
     */
    notUploadedOutstandingItemIcon(itemTxt) {
        return element(by.cssContainingText(
            '[data-test-el="upload-outstanding-information-item"]',
            `${itemTxt}`
        )).element(by.css('[data-test-el="upload-outstanding-information-icon"]'));
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    nameOfProvidedDocument(parentElement) {
        return parentElement.$('div[class*="meta-desc"] > div[class]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    uploadDocumentLink(parentElement) {
        return parentElement.$('button[data-test-el*="upload-document-link"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    notProvidedOutstandingItemIcon(parentElement) {
        return parentElement.$('[data-test-el="upload-outstanding-information-icon"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    notProvidedOutstandingItem(parentElement) {
        return parentElement.$('[data-test-el="NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notProvidedItemsNumber() {
        return $('[data-test-el=outstanding-tab-with-label] [data-test-el="text-badge-label"]');
    }

    /**
     * @returns {ElementFinder}
     */
    outstandingTab() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.OUTSTANDING.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messagesTab() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    unreadMessagesCounter() {
        return $('[data-test-el="messages-tab-with-label"] [data-test-el="text-badge-label"]');
    }

    /**
     * @returns {ElementFinder}
     */
    newMessageButton() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    newMessagesList() {
        return $$('[data-test-el="message-list-item"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    messagesSubjectsList() {
        return $$('[data-test-el="message-subject"]');
    }

    // TODO missing locator FEP-1341
    /**
     * @returns {ElementArrayFinder}
     */
    newMessagesReadIcons() {
        return $$('[class*="messageReadIcon"]');
    }

    // TODO missing locator FEP-1341
    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    messageIcon(parentElement) {
        return parentElement.$$('[class*="Icon"]').first();
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    messageDate(parentElement) {
        return parentElement.$('[data-test-el="message-date"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    outstandingListItems() {
        return $$('[data-test-el="upload-outstanding-information-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    documentsTab() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.DOCUMENTS.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    rightArrows() {
        return $$('[data-icon="right"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    documentsList() {
        return $$('[data-test-el="alerts-document-item"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    documentsButtonsList() {
        return $$('[data-test-el="document-download-btn"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    documentItemIcon(parentElement) {
        return parentElement.$('[data-test-el="read-icon"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    documentItemUnreadIcon(parentElement) {
        return parentElement.$('[class*="unreadIcon"]');
    }

    /**
     * @param {ElementFinder} parentElement - parent element
     * @returns {ElementFinder}
     */
    documentItemDate(parentElement) {
        return parentElement.$('[data-test-el="alert-received-date"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationLinkEmployee() {
        return $$('a[class*="notificationLink"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationSummaryActionsButton() {
        return this.cards('notification-summary').first().$('[data-test-el="action-button"]');
    }

    /**
     *
     * @param {string} optionName - name of drop down list option
     * @returns {ElementFinder}
     */
    notificationSummaryActionsDropDown(optionName) {
        return $(`[data-test-el$="${optionName}.DROPDOWN_OPTION"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    requestMoreTimePopupHeader() {
        return $('[data-test-el="REQUEST_FOR_MORE_TIME.POPOVER_HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    requestMoreTimePopupBody() {
        return $('[data-test-el="REQUEST_FOR_MORE_TIME.POPOVER_BODY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    requestMoreTimePopupAcceptButton() {
        return $('[data-test-el="REQUEST_FOR_MORE_TIME.POPOVER_PROCEED"]');
    }

    /**
     * @returns {ElementFinder}
     */
    requestMoreTimePopupCancelButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notOccupiedLeaveDays() {
        return $$(':not(.ant-picker-cell-disabled)[title*="-"]');
    }

    /**
     * @returns {ElementFinder}
     */
    absenceRequestPopUpHeader() {
        return $('span[data-test-el$="REQUEST_FOR_MORE_TIME.SUCCESS_HEAD"]');
    }

    /**
     * @returns {ElementFinder}
     */
    absenceRequestPopUpMessage() {
        return $('span[data-test-el$="REQUEST_FOR_MORE_TIME.SUCCESS_MESSAGE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    absenceRequestPopUpCancelButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    actionsLink() {
        return $('[data-test-el="NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    requestLeaveToRemove() {
        return $('[data-test-el="LEAVE_PERIOD_CHANGE.DROPDOWN_OPTION"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    periodToBeRemoved() {
        return $$('[data-test-el="period-card-title"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    jobProtectedLeaveCards() {
        return $$('[data-test-el="collapsible-card"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    expandedCards() {
        return $$('[data-test-el="card-body-with-status"]');
    }

    /**
     * @returns {ElementFinder}
     */
    showLatestEpisodesButton() {
        return $('[data-test-el="NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    hideLatestEpisodesButton() {
        return $('[data-test-el="NOTIFICATIONS.JOB_PROTECTED_LEAVE.HIDE_ACTUAL_ABSENCE_PERIODS_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341
    actualTimeTakenList() {
        return $('[class*="actual-absence-periods_wrapper"] table');
    }

    /**
     * @returns {ElementFinder}
     */
    viewMoreEpisodes() {
        return $('[data-test-el="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leaveCardStatus(element) {
        return element.$$('[data-test-el*="DECISION_STATUS_CATEGORY"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leaveCardStatusJobProtection() {
        return $$('[data-test-el*="DECISION_STATUS_CATEGORY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    leaveCardStatusCancelled() {
        return $('[data-test-el*="DECISION_STATUS_CATEGORY.CANCELLED"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leaveCardValues(element) {
        return element.$$('[data-test-el="card-body-with-status"] [data-test-el="property-item-value"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    absenceReason() {
        return $$('[data-test-el="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    intermittentTimeDates() {
        return $$('[data-test-el="actual-absence-period-date"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    intermittentTimeDuration() {
        return $$('[data-test-el="actual-absence-period-duration"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    intermittentTimeStatus() {
        return $$('[data-test-el="status-label"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    intermittentTimeIndicators(color) {
        const colour = color.toString().toLowerCase();
        if (color === 'green') {
            return $$('[data-test-el="status-label"][class*="success"]');
        } else if (colour === 'yellow') {
            return $$('[data-test-el="status-label"][class*="warning"]');
        }
        return $$('[data-test-el="status-label"][class*="danger"]');
    }
}

module.exports = NotificationPage;


