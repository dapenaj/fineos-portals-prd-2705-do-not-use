const SharedModal = require('./sharedModal');

class IntermittentTimeModal extends SharedModal {
    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341
    intermittentTimeModal() {
        return $('[role="dialog"]');
    }

    /**
     * @returns {ElementFinder}
     */
    modalTitle() {
        return $('[data-test-el="actual-absence-periods-modal-title"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341
    episodeDatesColumnName() {
        return $('[role="dialog"] [data-test-el="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DATE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341
    episodeDates() {
        return $$('[data-test-el="modal"] [data-test-el="actual-absence-period-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341
    filterIcon() {
        return $('[id*="FILTER_ICON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341
    filterDropDown() {
        return $('[class*="filterDropdown"]');
    }

    /**
     * @returns {ElementFinder}
     */
    filterOptions(option) {
        if (option.toString().toLowerCase() === 'approved') {
            return $('[data-test-el="ENUM_DOMAINS.YES_NO_UNKNOWN.YES"]');
        } else if (option.toString().toLowerCase() === 'pending') {
            return $('[data-test-el="ENUM_DOMAINS.YES_NO_UNKNOWN.UNKNOWN"]');
        }
        return $('[data-test-el="ENUM_DOMAINS.YES_NO_UNKNOWN.NO"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341
    filterDropDownOkBtn() {
        return this.filterDropDown().$('[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341

    decisionStatuses() {
        return $$('[role="dialog"] [data-test-el="status-label"] [data-test-el="actual-absence-period-decision"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341
    intermittentTimeIndicators(color) {
        const colour = color.toString().toLowerCase();
        if (color === 'green') {
            return $$('[role="dialog"] [data-test-el="status-label"][class*="success"]');
        } else if (colour === 'yellow') {
            return $$('[role="dialog"] [data-test-el="status-label"][class*="warning"]');
        }
        return $$('[role="dialog"] [data-test-el="status-label"][class*="danger"]');
    }
}

module.exports = IntermittentTimeModal;
