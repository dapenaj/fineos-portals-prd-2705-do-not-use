class EmployeeEmailsPage {
    /**
     * @returns {ElementFinder}
     */
    editEmailsPopupTitle() {
        return $('[role="dialog"] [data-test-el="PROFILE.PERSONAL_DETAILS_TAB.EDIT_EMAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addEmailPopupTitle() {
        return $('[role="dialog"] [data-test-el="PROFILE.PERSONAL_DETAILS_TAB.ADD_EMAIL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    emailsListTitle() {
        return $$('[data-test-el="emails-field"] [data-test-el="PROFILE.PERSONAL_DETAILS_TAB.EMAILS"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('button[data-test-el="submit-edited-emails"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    emailInputs() {
        return $$('form input[name*="emailAddress"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addEmailButton() {
        return $('[data-test-el="PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_EMAIL"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    deleteEmailButtons() {
        return $$('[data-test-el="delete-email"]');
    }

    /**
     * @returns {ElementFinder}
     */
    emailFormatErrorMessage() {
        return $('span[data-test-el="FINEOS_COMMON.VALIDATION.NOT_EMAIL"]');
    }
}

module.exports = EmployeeEmailsPage;
