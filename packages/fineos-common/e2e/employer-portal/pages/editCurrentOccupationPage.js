class EditCurrentOccupationPage {

    /**
     * @returns {ElementFinder}
     */
    editCurrentOccupationTitle() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.OCCUPATION_TITLE_EDIT"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    editCurrentOccupationData() {
        return element.all(by.xpath(
            '//form[contains(@class,"employment-details")]//div[@class="ant-form-item-control-input"]//*[text() | @value]'));
    }

    /**
     * @returns {ElementFinder}
     */
    adjustedDateOfHireSwitch() {
        return $('[data-test-el="switch-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employmentTypeSelectArrow() {
        return $('[data-test-input-name="employmentType"] [role="img"] [data-icon="down"]');
    }

    /**
     * {string} fieldLabel - label of web element
     * @returns {ElementFinder}
     */
    formInput(fieldLabel) {
        return element.all(by.xpath(`//label//span[contains(@data-test-el,"${fieldLabel}")]/../../../..//input`)).first();
    }

    /**
     * {string} fieldLabel - label of web element
     * @returns {ElementFinder}
     */
    formFieldLabel(fieldLabel) {
        return element(by.xpath(`//label//span[contains(@data-test-el,"${fieldLabel}")]/..`));
    }

    /**
     * @returns {ElementFinder}
     */
    hoursPerWeekInvalidNumber() {
        const translation = translationsHelper.getTranslation('FINEOS_COMMON.VALIDATION.INVALID_NUMBER_VALUE');
        return element(by.spanText(translation));
    }

    /**
     * @returns {ElementFinder}
     */
    hoursPerWeekNonNumeric() {
        const translation = translationsHelper.getTranslation('FINEOS_COMMON.VALIDATION.NOT_NUMERIC');
        return element(by.spanText(translation));
    }

    /**
     * @returns {ElementFinder}
     */
    valueGreaterThanZero() {
        const translation = translationsHelper.getTranslation('FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO');
        return element(by.spanText(translation));
    }

    /**
     * @returns {ElementFinder}
     */
    earningsNotNumericValidation() {
        const translation = translationsHelper.getTranslation('FINEOS_COMMON.VALIDATION.NOT_NUMERIC');
        return element(by.spanText(translation));
    }

    /**
     * @returns {ElementFinder}
     */
    jobTitleInput() {
        return $('input[name="jobTitle"]');
    }

    /**
     * @param {string} name - dropdown(select name) name
     *
     * @returns {ElementFinder}
     */
    dropdownInput(name) {
        if (name.toLowerCase() === 'full/parttime') {
            return $('[data-test-input-name="occupationCategory"]');
        } else if (name.toLowerCase() === 'employmenttype') {
            return $('[data-test-input-name="employmentType"]');
        }
        return $('div[data-test-input-name="earnings.frequency"]');
    }

    /**
     * @param {string} name - dropdown(select name) name
     * @returns {ElementFinder}
     */
    selectedOption(name) {
        return this.dropdownInput(name).$('[data-test-el*="ENUM_DOMAINS"]');
    }

    /**
     * @param {string} name - dropdown(select name) name
     *
     * @returns {ElementArrayFinder}
     */
    dropdownListOptions(name) {
        if (name.toLowerCase() === 'occupationcategory') {
            // TODO FEP-1341 - missing locator
            return $$('div[class*="ant-select-dropdown"]').first().$$('div[class=ant-select-item-option-content]');
        }

        // TODO FEP-1341 - missing locator
        return $$('div[class*="ant-select-dropdown"]').last().$$('div[class=ant-select-item-option-content]');
    }

    /**
     * @param {string} optionText - text visible in dropdown list option
     *
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    dropdownListOption(optionText) {
        return element(by.cssContainingText('div[class="ant-select-item-option-content"]', optionText));
    }

    /**
     * @returns {ElementFinder}
     */
    jobStartDateDatePicker() {
        return $('input[name="jobStartDate"]');
    }

    /**
     * @returns {ElementFinder}
     */
    jobEndDatePicker() {
        return $('input[name="jobEndDate"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workedHoursInput() {
        return $('input[name="hrsWorkedPerWeek"]');
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('button[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $('button[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeButton() {
        return $('button[data-test-el="drawer-close-icon-button"]');
    }

    /**
     * @returns {ElementFinder}
     */
    unauthorizedAlert() {
        return $('[class*="ant-notification-topRight"]');
    }
}

module.exports = EditCurrentOccupationPage;
