class PhoneCallsModal {
    /**
     * @returns {ElementArrayFinder}
     */
    registeredPhoneNumbers() {
        return $$('[data-test-el="selected-phone-number-card"] label, [data-test-el="phone-number-card"] label');
    }

    /**
     * @returns {ElementFinder}
     */
    selectPhoneNumberModalHeader() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneCallsModal() {
        return $('[data-test-el="ant-modal-content"]');
    }

    /**
     * @returns {ElementFinder}
     */
    modalHeader() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    radioButtonPhoneList() {
        return $$('[data-test-el="selected-phone-number-card"], [data-test-el="phone-number-card"]');
    }
}

module.exports = PhoneCallsModal;
