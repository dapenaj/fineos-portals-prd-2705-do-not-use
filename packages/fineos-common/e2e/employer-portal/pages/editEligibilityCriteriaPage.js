class EditEligibilityCriteriaPage {

    /**
     * @returns {ElementFinder}
     */
    editEligibilityCriteriaTitle() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_EDIT"]');
    }

    /**
     * @param {string} checkboxName
     * @returns {ElementFinder}
     */
    checkbox(checkboxName) {
        const checkbox = checkboxName.trim().replace(/\s+/g, '_').toUpperCase();
        return element(by.xpath(
            `//*[contains(@data-test-el,"PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.${checkbox}")]/../../..//*[@data-test-el="checkbox"]`));
    }

    /**
     * @param {string} checkboxName
     * @returns {ElementFinder}
     */
    checkboxLabel(checkboxName) {
        const checkbox = checkboxName.trim().replace(constants.regExps.spaces, '_').toUpperCase();
        return $(`[data-test-el*="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.${checkbox}"]`);
    }

    /**
     * @returns {ElementArrayFinder}
     */
    checkboxes() {
        return $$('[data-test-el="checkbox"]');
    }

    /**
     * @returns {ElementFinder}
     */
    actualWorkedHoursLabel() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR"]');
    }

    /**
     * @returns {ElementFinder}
     */
    actualWorkedHoursInput() {
        return $('[data-test-el="number-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workStateLabel() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORK_STATE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workStateSelect() {
        return $('[data-test-input-name="employmentWorkState"]');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementArrayFinder}
     */
    workStateSelectOptions() {
        return $('div[class*="ant-select-dropdown"]').$$('div[class=ant-select-item-option-content]');
    }

    /**
     * @returns {ElementFinder}
     */
    workStateSelectArrow() {
        return $('[data-test-input-name="employmentWorkState"] [data-icon="down"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cbaLabel() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cbaInput() {
        return $('[name="cbaValue"]');
    }

    /**
     * @returns {ElementFinder}
     */
    qualifiersRemark() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.NOT_EDITED_QUALIFIERS_REMARK"]');
    }
}

module.exports = EditEligibilityCriteriaPage;
