class PhoneCallsModal {
    /**
     * @returns {ElementFinder}
     */
    modalHeader() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    modalCheckBoxSelected() {
        return $$('[data-test-el="checkbox-group"] input:checked');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    modalCheckBoxNotSelected() {
        return $$('input[data-test-el="checkbox"]:not(:checked)');
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemSmsLabel() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_SMS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemEmailLabel() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_EMAIL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemSmsCheckBox() {
        const checkboxName = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_SMS');
        return element(by.labelText(checkboxName));
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemEmailCheckBox() {
        const checkboxName = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_EMAIL');
        return element(by.labelText(checkboxName));
    }

    /**
     * @returns {ElementFinder}
     */
    selectEmailLabel() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    selectPhoneNumberLabel() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    selectedEmail() {
        return $('[data-test-el="selected-email-address-card"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    emailsList() {
        return $$('[data-test-el*="email-address-card"]');
    }

    /**
     * @returns {ElementFinder}
     */
    preferredEmail() {
        return $('[data-test-el="selected-email-address-card"]');
    }

    /**
     * @returns {ElementFinder}
     */
    preferredPhoneNumber() {
        return $('[data-test-el="selected-phone-number-card"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    phoneNumbersList() {
        return $$('[data-test-el*="phone-number-card"]');
    }

}

module.exports = PhoneCallsModal;
