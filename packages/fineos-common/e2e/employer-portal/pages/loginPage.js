class LoginPage {

    /**
     * @returns {ElementFinder}
     */
    panelAppSelector() {
        return element(by.id('idp_OtherRpPanel'));
    }

    /**
     * @returns {ElementFinder}
     */
    radioAppSelect() {
        return element(by.id('idp_OtherRpRadioButton'));
    }

    /**
     * @returns {ElementFinder}
     */
    dropDownAppSelect() {
        return element(by.id('idp_RelyingPartyDropDownList'));
    }

    /**
     * @returns {ElementFinder}
     */
    signInStatusLabel() {
        return element(by.id('idp_SignInThisSiteStatusLabel'));
    }

    /**
     * @returns {ElementFinder}
     */
    signOutCurrentSiteRadioButton() {
        return element(by.id('idp_LocalSignOutRadioButton'));
    }

    /**
     * @returns {ElementFinder}
     */
    buttonSignOut() {
        return element(by.id('idp_SignOutButton'));
    }

    /**
     * @returns {ElementFinder}
     */
    buttonSignIn() {
        return element(by.id('idp_SignInButton'));
    }

    /**
     * @returns {ElementFinder}
     */
    panelLoginForm() {
        return element(by.id('formsAuthenticationArea'));
    }

    /**
     * @returns {ElementFinder}
     */
    inputUserName() {
        return element(by.id('userNameInput'));
    }

    /**
     * @returns {ElementFinder}
     */
    inputPassword() {
        return element(by.id('passwordInput'));
    }

    /**
     * @returns {ElementFinder}
     */
    buttonSubmit() {
        return element(by.id('submitButton'));
    }
}

module.exports = LoginPage;
