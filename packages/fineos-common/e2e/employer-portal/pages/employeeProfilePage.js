class EmployeeProfilePage {

    /**
     * @returns {ElementFinder}
     */
    actionsLink() {
        return $('[data-test-el="PROFILE.ACTIONS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    userNameField() {
        return $('[data-test-el="employee-profile-username"]');
    }

    /**
     * @returns {ElementFinder}
     */
    personalDataField() {
        return $('[data-test-el="employee-profile-tokens"]');
    }

    /**
     * @returns {ElementFinder}
     */
    activeNotificationsTab() {
        return $('[aria-selected="true"] [data-test-el="PROFILE.NOTIFICATIONS_TAB.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    activeEmployeeDetailsTab() {
        return $('[aria-selected="true"] [data-test-el="PROFILE.EMPLOYEE_DETAILS_TAB.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    activeTimeTakenTab() {
        return $('[aria-selected="true"] [data-test-el="PROFILE.TIME_TAKEN_TAB.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationsList() {
        return $$('[data-test-el="notification-list-item"]');
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementFinder}
     */
    notificationLink(parentElement) {
        return parentElement.element(by.css('[data-test-el="notification-list-item"] a'));
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementArrayFinder}
     */
    notificationType(parentElement) {
        return parentElement.all(by.css('span[data-test-el*="NOTIFICATIONS.TYPE"]'));
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementFinder}
     */
    notificationDate(parentElement) {
        return parentElement.element(by.css('[data-test-el="notified-on-date"]'));
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationDates() {
        return $$('[data-test-el="notified-on-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsSortSelect() {
        return $('[data-test-el="select"]');
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementFinder}
     */
    notificationReason(parentElement) {
        return parentElement.element(by.css('[data-test-el="notification-reason"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    lackOfNotificationsMessage() {
        return $('span[data-test-el="PROFILE.NOTIFICATIONS_TAB.EMPTY_STATE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employmentDetailsTab() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    communicationDetailsTab() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE"]');
    }

    /**
     * {string} tab name
     * @returns {ElementFinder}
     */
    employeeTab(tabName) {
        return $(`[data-test-el="PROFILE.${tabName}_TAB.TITLE"]`);
    }

    /**
     * @param {string} selectedTab
     * @returns {ElementFinder}
     */
    employeeProfileTab(selectedTab) {
        selectedTab = selectedTab.replace(constants.regExps.spaces, '_').toUpperCase();
        return $(`[data-test-el$="PROFILE.${selectedTab}_TAB.TITLE"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    employeeNotificationsTab() {
        return $('[data-test-el="PROFILE.NOTIFICATIONS_TAB.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeeDataLabels() {
        return $$('[data-test-el="property-item-label"]');
    }

    /**
     * @param {string} cardName - name of section of Employment Details, e.g. Current occupation, Eligibility
     *     criteria, Work pattern
     * @returns {ElementArrayFinder} - array of all fields of given employment details section
     */
    employmentDetailsCardFields(cardName) {
        cardName = cardName.trim().replace(constants.regExps.spaces, '_').toUpperCase();
        return $$(`[data-test-el*="${cardName}"]`);
    }

    /**
     * @param {string} dataTestElTag - value of dataTestEl tag of field to be located
     * @returns {ElementFinder}
     */
    employmentDetailsField(dataTestElTag) {
        return $(`[data-test-el*="${dataTestElTag}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    employeeIdLabel() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EMPLOYEE_ID"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeeDetailsDataValues() {
        return $$('[data-test-el="property-item-value"]');
    }

    /**
     * @returns {ElementFinder}
     */
    communicationPreferencesTabHeader() {
        return $(
            '[role="tabpanel"] [role="tabpanel"] [data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    preferencesTitles() {
        return $$('[data-test-el="preference-card-title"]');
    }

    /**
     * @returns {ElementFinder}
     */
    activePreferencesTab() {
        return $('[class*="ant-tabs-tab-active"] [id*="communicationPreferences"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editPersonalDetailsButton() {
        return $('[data-test-el="edit-personal-details-button"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editEmailsButton() {
        return $('[data-test-el="email-actions-button"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addEmailsButton() {
        return $('[data-test-el="PROFILE.PERSONAL_DETAILS_TAB.ADD_EMAIL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editPhoneNumbersButton() {
        return $('[data-test-el="phone-number-actions-button"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addPhoneNumbersButton() {
        return $('[data-test-el="PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editCurrentOccupationButton() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editCurrentOccupationTitle() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.OCCUPATION_TITLE_EDIT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    toggleAddDateHire() {
        return $('button[data-test-el="switch-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    adjustedDateOfHireField() {
        return $('[data-test-el="adjusted-hire-date-picker"]');
    }

    /**
     * @returns {ElementFinder}
     */
    jobEndDate() {
        return $('[data-test-el="job-end-date-picker"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    tooltipJobDates() {
        return $$('[data-test-el="form-group-label-question-circle"]');
    }

    /**
     * @returns {ElementFinder}
     */
    infoTooltipJobStartDate() {
        return $('[data-test-el="EDIT_EMPLOYMENT_DETAILS.JOB_START_DATE.POPOVER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    infoTooltipJobEndDate() {
        return $('[data-test-el="EDIT_EMPLOYMENT_DETAILS.JOB_END_DATE.POPOVER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    infoTooltipAdjustedDateHire() {
        return $('[data-test-el="EDIT_EMPLOYMENT_DETAILS.ADJUSTED_HIRE_DATE.POPOVER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    datePickerAdjustedDate() {
        return $('[data-test-el="adjusted-hire-date-picker"]');
    }

    /**
     * @returns {ElementFinder}
     */
    adjustedDateAfterTodayWarning() {
        return $('[data-test-el="ADJUSTED_DATE_OF_HIRE_AFTER_TODAY_WARNING"]');
    }

    /**
     * @returns {ElementFinder}
     */
    adjustedDateAfterJobEndDateWarning() {
        return $('[data-test-el="JOB_END_DATE_AFTER_ADJUSTED_DATE_WARNING"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editEligibilityCriteriaButton() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EDIT_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editWorkPatternButton() {
        return $('[data-test-el="edit-work-pattern-button"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addWorkPatternButton() {
        return $('[data-test-el="add-work-pattern-button"]');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementFinder}
     */
    workPatternCard() {
        return $$('[class*="employment-data-card_card"]').last();
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementArrayFinder}
     */
    workPatternWeeks() {
        return $$('table[class*="work-pattern"]');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementArrayFinder}
     */
    workPatternWeeksHeaders() {
        return this.workPatternWeeks().$$('thead');
    }

    // TODO FEP-1341 - missing locator
    /**
     * @returns {ElementArrayFinder}
     */
    workPatternWeeksTimes() {
        return this.workPatternWeeks().$$('tbody');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    workPatternWeeksLabels() {
        return $$('[class*="wrapper"] [data-test-el="WORK_PATTERN.ROTATING_WEEK_HEADER"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    currentOccupationData() {
        return $$(
            '[data-test-el*="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD."]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    paymentFrequency() {
        return $('[data-test-el*="PROFILE.EMPLOYMENT_DETAILS_TAB.EARNINGS_FREQUENCIES."]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    personalDetailsData() {
        return $$('div[data-test-el*="personal-details"] span[data-test-el="property-item-value"]');
    }

    /**
     * @param {string} fieldLabel
     * @returns {ElementFinder}
     */
    personalDetailsField(fieldLabel) {
        return element(by.xpath(`//span[text()="${fieldLabel}"]/../../span[contains(@data-test-el,"value")]`));
    }

    /**
     * @returns {ElementFinder}
     */
    hoursWorkedPerWeek() {
        return $('[data-test-el="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.HOURS_PER_WEEK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    personalDetailsPhones() {
        return $('[class*="contact-details_phoneNumbers"]>[data-test-el="property-item-value"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    personalDetailsEmail() {
        return element(by.xpath('//span[@data-test-el="PROFILE.PERSONAL_DETAILS_TAB.EMAILS"]/../../span[2]'));
    }

    /**
     * @param {string} state - actual state of checkbox true/false
     * @returns {ElementFinder}
     */
    fmlaCheckboxCheckbox(state) {
        return $(`[data-test-el$="ELIGIBILITY_CRITERIA_CARD.WORKS_WITH_FMLA_${state.toString().toUpperCase()}"]`);
    }

    /**
     * @param {string} state - actual state of checkbox true/false
     * @returns {ElementFinder}
     */
    worksAtHomeCheckbox(state) {
        return $(`[data-test-el$="ELIGIBILITY_CRITERIA_CARD.WORKS_AT_HOME_${state.toString().toUpperCase()}"]`);
    }

    /**
     * @param {string} state - actual state of checkbox true/false
     * @returns {ElementFinder}
     */
    keyEmployeeCheckbox(state) {
        return $(`[data-test-el$="ELIGIBILITY_CRITERIA_CARD.IS_KEY_EMPLOYEE_${state.toString().toUpperCase()}"]`);
    }
}

module.exports = EmployeeProfilePage;
