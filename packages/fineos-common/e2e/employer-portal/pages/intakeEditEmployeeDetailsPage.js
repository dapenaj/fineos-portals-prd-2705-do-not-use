class IntakeEditEmployeeDetailsPage {

    /**
     * @param {string} elementName
     * @returns {ElementFinder}
     */
    country(countryName) {
        const country = countryName.replace(constants.regExps.spaces, '_').toUpperCase();
        return $$(`[role="option"] [data-test-el="ENUM_DOMAINS.COUNTRIES.${country}"]`).first();
    }

    /**
     * @returns {ElementArrayFinder}
     */
    countriesList() {
        return $$('[role="option"] [data-test-el*="ENUM_DOMAINS.COUNTRIES."]');
    }

    /**
     * @returns {ElementFinder}
     */
    occupationAndEarningsStep() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine1() {
        return $('[name="customer.address.addressLine1"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine2() {
        return $('[name="address.addressLine2"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine3() {
        return $('[name="address.addressLine3"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addressLine4() {
        return $('[name="address.addressLine4"]');
    }

    /**
     * @returns {ElementFinder}
     */
    stateDropDown() {
        return $('[data-test-el="address-state-field"] [data-test-el="dropdown-input"]');
    }

    /**
     * @returns {ElementFinder}
     */
    countryDropDown() {
        return $('[data-test-input-name="customer.address.country"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO  FEP-1341
    countryDropDownArrow() {
        return $('[data-test-input-name="customer.address.country"] [class*="arrow"]');
    }

    /**
     * @returns {ElementFinder}
     */
    postalCodeInput() {
        return $('[name="customer.address.postCode"]');
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    statesList() {
        return $$('[data-test-el*="-option"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    selectedItem() {
        return $('[data-test-el="undefined-option"][aria-selected="true"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO  FEP-1341
    addressLines() {
        return $$('[data-test-el="address"] > div');
    }

    /**
     * @returns {ElementFinder}
     */
    address() {
        return $('[data-test-el="address"]');
    }

    /**
     * @returns {ElementFinder}
     */
    editPhoneNumberLink() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.EDIT_PREFERENCES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    useDifferentPhoneNumber() {
        return $('[data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.USE_DIFFERENT_PHONE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO  FEP-1341
    phoneRadioBtn() {
        return element.all(by.xpath(
            '//span[@data-test-el="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER"]/../..//div[2]//div//div[@data-test-el="panel"]'));
    }

    /**
     * @returns {ElementArrayFinder}
     */
    phoneNumbers() {
        return $$('[data-test-el="phone-number-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    getInputByLabel(label) {
        if (label === 'Province' || label === 'State') {
            return element(by.labelTextInput(label));
        }
        return element(by.labelTextInput(label));
    }

    /**
     * @returns {ElementFinder}
     */
    selectedStateOrProvince() {
        return $('[data-test-input-name="customer.address.addressLine6"] [class*="selection-item"]');
    }
}

module.exports = IntakeEditEmployeeDetailsPage;
