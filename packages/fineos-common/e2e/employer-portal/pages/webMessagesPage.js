class WebMessagesPage {
    /**
     * @returns {ElementFinder}
     */
    pageHeader() {
        return $('[data-test-el="messages-page-header"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messagesFilterLabel() {
        return $('[data-test-el="MESSAGES.FILTER.LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messagesFilterSelect() {
        return $('[data-test-el="message-filter-select"]');
    }

    /**
     * @returns {ElementFinder}
     */
    filterSelectedOption() {
        return $$('[data-test-el*="MESSAGES.FILTER.OPTIONS."]').first();
    }

    /**
     * @param {string} - optionName
     * @returns {ElementFinder}
     */
    filterOption(optionName) {
        return $(`[data-test-el*="MESSAGES.FILTER.OPTIONS.${optionName}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    messagesSortLabel() {
        return $('[data-test-el="MESSAGES.SORT.LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messagesSortSelect() {
        return $('[data-test-el="message-sort-select"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sortSelectedOption() {
        return $$('[data-test-el*="COMMON.SORT_BY_DATE_SELECT"]').first();
    }

    /**
     * @param {string} - optionName
     * @returns {ElementFinder}
     */
    sortOption(optionName) {
        return $(`[data-test-el*="COMMON.SORT_BY_DATE_SELECT.${optionName}"]`);
    }

    /**
     * @returns {ElementArrayFinder}
     */
    messagesList() {
        return $$('[data-test-el="message-list-item"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    messagesSubjectsList() {
        return $$('[data-test-el="messages"] [data-test-el="message-subject"]');
    }

    /**
     * @returns {ElementFinder}
     */
    openedMessageSubject() {
        return this.openedMessagePanel().$('[data-test-el="message-subject"]');
    }

    /**
     * @returns {ElementFinder}
     */
    openedMessageDateTime() {
        return this.openedMessagePanel().$('[data-test-el="message-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    openedMessageRelatesTo() {
        return this.openedMessagePanel().$('[data-test-el="message-relates-to-link"]');
    }

    /**
     * @returns {ElementFinder}
     */
    openedMessageText() {
        return this.openedMessagePanel().$('[data-test-el="message-narrative"]');
    }

    /**
     * @returns {ElementFinder}
     */
    openedMessageSenderIcon() {
        return this.openedMessagePanel().$('[data-test-el="message-sender-icon"]');
    }

    /**
     * @returns {ElementFinder}
     */
    openedMessagePanel() {
        return $('[data-test-el="message-details"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messagesListElement(parent, elName) {
        return parent.$(`[data-test-el="messages"] [data-test-el*="message-${elName}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    messagesListLink(parent, linkName) {
        return parent.$(`[data-test-el="messages"] [data-test-el*="message-${linkName}"] a`);
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementFinder}
     */
    messageActionsButton(parentElement) {
        return parentElement.$('[data-test-el="mark-message-icon"]');
    }

    /**
     * @param {ElementFinder} parentElement
     * @returns {ElementFinder}
     */
    messageReadUnreadButton(parentElement) {
        return parentElement.$('[data-test-el="mark-message-popover"]');
    }
}

module.exports = WebMessagesPage;
