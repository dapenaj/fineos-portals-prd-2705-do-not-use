class UploadDocumentPage {
    /**
     * @returns {ElementFinder}
     */
    pageTitle() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD_DOCUMENT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    formName() {
        return $('[data-test-el="upload-module-name"]');
    }

    /**
     * @returns {ElementFinder}
     */
    dragDocumentLabel() {
        return $('[data-test-el="FINEOS_COMMON.UPLOAD.DRAG_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    browseFilesButton() {
        return $('[data-test-el="FINEOS_COMMON.UPLOAD.BROWSE_FILES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    validFileExtensionsLabel() {
        return $('[data-test-el="FINEOS_COMMON.UPLOAD.VALID_EXTENSIONS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    maxFileSizeLabel() {
        return $('[data-test-el="FINEOS_COMMON.UPLOAD.MAX_FILE_SIZE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelUploadDocumentButton() {
        return $('button[data-test-el*="cancel-document-upload"]');
    }

    /**
     * @returns {ElementFinder}
     */
    uploadDocumentButton() {
        return $('button[data-test-el="FINEOS_COMMON.UPLOAD.UPLOAD_BUTTON"]');
    }

    /**
     * @param {string} fileName with extension
     * @returns {ElementFinder}
     */
    uploadedFileLabel(fileName) {
        return $(`[title='${fileName}']`);
    }

    /**
     * @returns {ElementFinder}
     */
    closeUploadPageButton() {
        return $$('[class*="CloseIcon"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    uploadedDocumentSuccessPopUpTitle() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.OUTSTANDING.DOCUMENT_UPLOADED"]');
    }

    /**
     * @returns {ElementFinder}
     */
    uploadedDocumentSuccessPopUpMessage() {
        return $('[data-test-el$="DOCUMENT.RECEIVED_SUCCESS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    uploadedDocumentSuccessPopUpImage() {
        return $('[data-test-el="upload-success-response-image"]');
    }

    /**
     * @returns {ElementFinder}
     */
    invalidExtensionError() {
        return $('[data-test-el="FINEOS_COMMON.VALIDATION.FILE_EXTENSION"]');
    }
}

module.exports = UploadDocumentPage;
