class CancelWithDrawRequestPage {
    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    leaveReasonCards() {
        return $$('[class*="ant-checkbox-wrapper"] > span > div');
    }

    /**
     * @returns {ElementFinder}
     */
    successModalHeader() {
        return $('[data-test-el="LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_HEAD"]');
    }

    /**
     * @returns {ElementFinder}
     */
    successModalCloseIcon() {
        return $('[class*="CloseIcon"]');
    }

    /**
     * @returns {ElementFinder}
     */
    successModalMessage() {
        return $('[data-test-el$="LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_MESSAGE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    selectAllPeriodsCheckboxes() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.SELECT_ALL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationImage() {
        return $('[data-test-el="success-response-image"]');
    }

    /**
     * @returns {ElementFinder}
     */
    successModal() {
        return $('[data-test-el="response-modal-content"]');
    }

    /**
     * @returns {ElementFinder}
     */
    enabledOkButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    disabledOkButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.OK"][disabled]');
    }

    /**
     * @returns {ElementFinder}
     */
    successModalCloseBtn() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CLOSE"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    periodsCheckboxes() {
        return $$('[data-test-el="checkbox-group"] > label');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelRequestToLeaveButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    additionalDetailsInputField() {
        return $('[name="additionalNotes"]');
    }

    /**
     * @returns {ElementFinder}
     */
    additionalDetailsLabel() {
        return $('[data-test-el$="LEAVE_PERIOD_CHANGE.DETAILS_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    reasonForRemovalLabel() {
        return $('[data-test-el$="LEAVE_PERIOD_CHANGE.REASON_FOR_REMOVAL_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    leaveReasonSelect() {
        return $('[data-test-el="ENUM_DOMAINS.PLEASE_SELECT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    selectCancelReasonInput() {
        return $('[data-test-el="dropdown-input"]');
    }

    /**
     * Returns list of cancel reasons in cancel withdraw request form
     * @returns {ArrayElementFinder}
     */
    cancelReasonsList() {
        return $$('[data-test-el*="LEAVE_PERIOD_CHANGE.SELECT_OPTION_"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leaveReasonDateFrom() {
        return $$('[data-test-el="period-start-date"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leaveReasonDateTo() {
        return $$('[data-test-el="period-end-date"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leaveReason() {
        return $$('[data-test-el="period-reason"]');
    }

    /**
     * @returns {ElementFinder}
     */
    requestLeaveForm() {
        return $('[data-test-el="leave-period-change-form"]');
    }
}

module.exports = CancelWithDrawRequestPage;
