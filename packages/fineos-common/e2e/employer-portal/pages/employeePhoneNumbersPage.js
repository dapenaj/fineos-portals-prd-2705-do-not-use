class EmployeePhoneNumbersPage {
    /**
     * @returns {ElementFinder}
     */
    editPhoneNumbersTitle() {
        return $('[role="dialog"] [data-test-el="PROFILE.PERSONAL_DETAILS_TAB.EDIT_PHONE_NUMBERS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addPhoneNumbersTitle() {
        return $('[role="dialog"] [data-test-el="PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    phoneNumbersListTitle() {
        return $('form span[data-test-el="PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    addPhoneNumberButton() {
        return $('button[data-test-el="PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_PHONE"]');
    }

    /**
     * @param {string} txt
     * @returns {ElementArrayFinder}
     */
    editNumbersInputs(txt) {
        return element.all(by.xpath(`//span[text()="${txt}"]/../../../../div//input`));
    }

    /**
     * @param {ElementFinder} phoneNumberCard
     * @returns {ElementArrayFinder}
     */
    numberInputs(phoneNumberCard) {
        return phoneNumberCard.$$('input[name*="phoneNumber"]');
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('button[data-test-el="submit-edited-phone-numbers"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelButton() {
        return $('div button[data-test-el="FINEOS_COMMON.GENERAL.CANCEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeButton() {
        return $('[class*="CloseIcon"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    phoneTypeSelects() {
        return $$('[class*="selection"] [data-test-el*="ENUM_DOMAINS.PHONE_TYPE."]');
    }

    /**
     * @params {ElementFinder} parentSelect
     * @returns {ElementFinder}
     */
    cellPhoneSelectOption(parentSelect) {
        return parentSelect.$('[data-test-el="Cell Phone-option"]');
    }

    /**
     * @params {ElementFinder} parentSelect
     * @returns {ElementFinder}
     */
    landLineSelectOption(parentSelect) {
        return parentSelect.$('[data-test-el="Landline-option"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    phoneDetailsCards() {
        return $$('[class*="phoneDetails"]');
    }

    /**
     * @param {ElementFinder} phoneCard
     * @returns {ElementArrayFinder}
     */
    phoneNumberAllInputs(phoneCard) {
        return phoneCard.$$('[name*="phoneNumbers"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    deletePhoneNumberButtons() {
        return $$('[data-test-el="delete-phone-number"]');
    }

    /**
     * @param {string} errorType
     * @returns {ElementFinder}
     */
    phoneNumberErrorMessage(errorType) {
        return $(`[data-test-el*="FINEOS_COMMON.VALIDATION.${errorType}"][id]`);
    }
}

module.exports = EmployeePhoneNumbersPage;
