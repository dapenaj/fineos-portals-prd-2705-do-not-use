class ShowHidePanelsModal {
    /**
     * @returns {ElementFinder}
     */
    modalTitle() {
        return $('[data-test-el="WIDGET.SHOW_HIDE_PANELS"]');
    }

    /**
     * @param {string} panelName
     * @returns {ElementFinder}
     */
    panelLabel(panelName) {
        panelName = panelName.toString().replace(constants.regExps.spaces, '_').toUpperCase();
        return $(`[data-test-el="panel-customization-list"] [data-test-el="WIDGET.${panelName}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    employeesExpectedToRTWSwitch() {
        return $('[data-test-el="panel-customization-list"] [data-test-el*="employeesExpectedToRTW"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsCreatedSwitch() {
        return $('[data-test-el="panel-customization-list"] [data-test-el*="notificationsCreated"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeXButton() {
        return $('[data-test-el="drawer-close-icon-button"]');
    }

}

module.exports = ShowHidePanelsModal;
