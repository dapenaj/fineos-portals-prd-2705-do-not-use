class LogOutPopUpPage {

    /**
     * @returns {ElementFinder}
     */
    logOutSpinner() {
        return $('[data-test-el="LOGOUT.DURING_LOGOUT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    popUpTitle() {
        return $('span[data-test-el="LOGOUT.USER_LOGGED_OUT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    popUpBody() {
        return $('span[data-test-el="LOGOUT.LOGGED_BODY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    takeMeToLoginButton() {
        return $('button[data-test-el="COMMON.SESSION.TAKE_ME_LOGIN"]');
    }
}

module.exports = LogOutPopUpPage;
