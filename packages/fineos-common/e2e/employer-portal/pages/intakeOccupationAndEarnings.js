class IntakeOccupationAndEarnings {

    /**
     * @param {string} fieldName
     * @returns {ElementFinder}
     */
    occupationAndEarningsField(fieldName) {
        return $(`[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.${fieldName}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    jobTitleHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    occupationAndEarningsHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.HEADER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    frequencyHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.FREQUENCY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    existingEmployeeDescription() {
        return $('[data-test-el$="INTAKE.OCCUPATION_AND_EARNINGS.EXISTING_DESCRIPTION"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatternHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workingPatternAdditionalInformation() {
        return $('[name="variablePattern.additionalInformation"]');
    }

    /**
     * @param {string} elementName
     * @returns {ElementFinder}
     */
    pageElement(elementName) {
        return $(`[name="${elementName}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    dateOfHireHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    earningsHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    dateOfHireField() {
        return $('[name="dateOfHire"]');
    }

    /**
     * @returns {ElementFinder}
     */
    jobTitleInput() {
        return $('[name="jobTitle"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    frequencyDropDown() {
        return $('[data-test-input-name="earnings.frequency"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    frequencyOptions() {
        return $$('[data-test-el*="-option"] [data-test-el*="ENUM_DOMAIN"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    pleaseSelect() {
        return $$('[data-test-el="ENUM_DOMAINS.PLEASE_SELECT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    weekly() {
        return $('[data-test-el="ENUM_DOMAINS.EARNING_FREQUENCY.WEEKLY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    monthly() {
        return $('[data-test-el="ENUM_DOMAINS.EARNING_FREQUENCY.MONTHLY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    yearly() {
        return $('[data-test-el="ENUM_DOMAINS.EARNING_FREQUENCY.YEARLY"]');
    }

    /**
     * @params {elementName}
     * @returns {ElementFinder}
     */
    workPattern(elementName) {
        const element = (`${elementName}`).toUpperCase().trim();
        return $(`[data-test-el="ENUM_DOMAINS.WORK_PATTERN_TYPE.${element}"]`);
    }

    /**
     * @returns {ElementArrayFinder}
     */
    workingDaysInput() {
        return $$('[data-test-el="work-pattern-rotating-row"]:not([hidden]) [name*="rotatingPattern.workPatternWeek"]');
    }

    /**
     * @returns {ElementFinder}
     */
    patternDescriptionHeader() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    workPatternSwitch() {
        return $$('button[role="switch"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    radioButtonList() {
        return $$('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.WEEKS_NUMBER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    earningsInput() {
        return $('[name="earnings.amount"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatternField() {
        return $('[data-test-el="work-pattern-field"] [class*="ant-select-selection-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    earningDetailsSection() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.SALARY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    showEarningsLink() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.SHOW_EARNINGS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    hideEarningsLink() {
        return $('[data-test-el="INTAKE.OCCUPATION_AND_EARNINGS.HIDE_EARNINGS"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    hoursPerDayFields() {
        return $$('[data-test-el="property-item-value"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    ownWorkingHoursFields() {
        return $$('[name*="fixedPattern.workWeek["]');
    }

    /**
     * @param {day}
     * @returns {ElementArrayFinder}
     */
    weekDay(day) {
        const weekDay = day.toString().toUpperCase();
        return $(`[data-test-el="WORK_PATTERN.WEEK_DAYS_SHORTCUT.${weekDay}"]`);
    }

    /**
     * @returns {ElementArrayFinder}
     */
    checkedSteps() {
        return $$('[data-icon="check-circle"]');
    }

    /**
     * @returns {ElementFinder}
     */
    nonStandardWorkingWeekSwitchOff() {
        return $('[data-test-el="non-standard-working-week-switch"]');
    }

    /**
     * @returns {ElementFinder}
     */
    includeWeekendSwitchOff() {
        return $('[data-test-el="include-weekend-switch"]');
    }

    /**
     * @returns {ElementFinder}
     */
    includeWeekendSwitchOn() {
        return $('[data-test-el="include-weekend-switch"][aria-checked="true"]');
    }

    /**
     * @returns {ElementFinder}
     */
    nonStandardWorkingWeekSwitchOn() {
        return $('[data-test-el="non-standard-working-week-switch"][aria-checked="true"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workPatternDropDownArrow() {
        return $('[data-test-input-name="workPatternType"] [data-icon="down"]');
    }

    /**
     * @returns {ElementFinder}
     */
    backButton() {
        return $('[data-test-el="NAVIGATION.BACK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    proceedButton() {
        return $('[data-test-el="INTAKE.PROCEED"]');
    }

    /**
     * @returns {ElementFinder}
     */
    patternRepeatsSwitch() {
        return $('[data-test-el="work-pattern-repeats"]');
    }

    /**
     * @returns {ElementFinder}
     */
    workDaySameLengthSwitch() {
        return $('[data-test-el="work-day-is-same-length"]');
    }

    /**
     * @returns {ElementFinder}
     */
    numberOfWeeks() {
        return $('[name="variablePattern.numberOfWeeks"]');
    }

    /**
     * @returns {ElementFinder}
     */
    dayLength() {
        return $('[name="variablePattern.dayLength"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    errorMessage() {
        return $$('[data-test-el*="FINEOS_COMMON.VALIDATION."]');
    }
}

module.exports = IntakeOccupationAndEarnings;
