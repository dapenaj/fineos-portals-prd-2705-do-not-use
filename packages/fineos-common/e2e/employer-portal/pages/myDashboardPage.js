class MyDashboardPage {

    /**
     * @returns {ElementFinder}
     */
    welcomeMessageTitle() {
        return $('[data-test-el$="WELCOME_MESSAGE.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    showHidePanels() {
        return $('[data-test-el="panel-customization"]');
    }

    /**
     * @returns {ElementFinder}
     */
    welcomeMessageSubtitle() {
        return $('[data-test-el$="WELCOME_MESSAGE.SUBTITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    errorPage() {
        return $('[data-test-el="error-page-container"]');
    }

    /**
     * @returns {ElementFinder}
     */
    panelMainLayout() {
        return element(by.css('[data-test-el="main-layout"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    pageTitle() {
        return $('[data-test-el="my-dashboard-header"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO - FEP-1341
    notificationsWithOutstandingRequirementsCount() {
        return $('[class*="outstanding-notifications_count"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsWithOutstandingRequirementsLabel() {
        return $('[data-test-el="OUTSTANDING_NOTIFICATIONS.WIDGET_TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    leavesDecidedLabel() {
        return $('[data-test-el="WIDGET.DECISIONS_MADE_LEAVES_DECIDED_FILTER"]');
    }

    /**
     * @returns {ElementFinder}
     */
    viewNotificationsDetailsButton() {
        return $('[data-test-el="outstanding-notifications-widget"] [data-test-el="WIDGET.VIEW_DETAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    viewLeavesDecidedDetailsButton() {
        return $('[data-test-el="decisions-made-widget"] [data-test-el="WIDGET.VIEW_DETAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    viewEmployeesApprovedForLeaveDetailsButton() {
        return $('[data-test-el="employees-approved-for-leave"] [data-test-el="WIDGET.VIEW_DETAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    expectedToReturnToWorkWidget() {
        return $('[data-test-el="expected-to-rtw-widget"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsCreatedWidget() {
        return $('[data-test-el="notifications-created-widget"]');
    }

    /**
     * @returns {ElementFinder}
     */
    leavesDecidedWidget() {
        return $('[data-test-el="decisions-made-widget"]');
    }

    /**
     * @returns {ElementFinder}
     */
    leavesDecidedFilterCriteria() {
        return $('[data-test-el="decisions-made-filter-category"]');
    }

    /**
     * @returns {ElementFinder}
     */
    viewEmployeesExpectedToReturnToWorkButton() {
        return this.expectedToReturnToWorkWidget().$('[data-test-el="WIDGET.VIEW_DETAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    viewNotificationsCreatedButton() {
        return this.notificationsCreatedWidget().$('[data-test-el="WIDGET.VIEW_DETAILS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    emptyStateLabel() {
        return $('[data-test-el="OUTSTANDING_NOTIFICATIONS.EMPTY_STATE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeesExpectedToReturnToWorkWidgetEmptyState() {
        return $('[data-test-el="WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_EMPTY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsCreatedWidgetEmptyState() {
        return $('[data-test-el="WIDGET.NOTIFICATIONS_CREATED_EMPTY"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeesExpectedToReturnToWorkWidgetDayFilter() {
        return $('[data-test-el="expected-to-rtw-filter"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    datePeriodFilterOptions() {
        return $$('[data-test-el$="-option"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsCreatedWidgetDayFilter() {
        return $('[data-test-el="notifications-created-filter-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    leavesDecidedWidgetDayFilter() {
        return $('[data-test-el="decisions-made-filter-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeesApprovedForLeaveWidgetDayFilter() {
        return $('[data-test-el="employees-approved-for-leave-filter-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeesExpectedToReturnToWorkWidgetFilterOption(option) {
        return this.employeesExpectedToReturnToWorkWidgetDayFilter().$(`[data-test-el*="${option}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    notificationsCreatedWidgetDayFilterOption(option) {
        return this.notificationsCreatedWidgetDayFilter().$(`[data-test-el*="${option}"]`);
    }

    /**
     * @param {string} option
     * @returns {ElementFinder}
     */
    leavesDecidedWidgetDayFilterOption(option) {
        return this.leavesDecidedWidgetDayFilter().$(`[data-test-el*="${option}"]`);
    }

    /**
     * @param {string} option
     * @returns {ElementFinder}
     */
    employeesApprovedForLeaveWidgetDayFilterOption(option) {
        return this.employeesApprovedForLeaveWidgetDayFilter().$(`[data-test-el*="${option}"]`);
    }

    /**
     * @param {string} option
     * @returns {ElementFinder}
     */
    leavesDecidedWidgetCriteriaFilterOption(option) {
        return $(`[data-test-el*="WIDGET.FILTER_OPTIONS.${option}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    viewEmployeesExpectedToReturnToWorkPieChartTotalResults() {
        return this.expectedToReturnToWorkWidget().$('[class*="totalValue"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    notificationsCreatedWidgetPieChartTotalResults() {
        return this.notificationsCreatedWidget().$('[class*="totalValue"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    leavesDecidedWidgetPieChartTotalResults() {
        return $('[data-test-el="decisions-made-widget"] [class*="totalValue"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    leavesDecidedWidgetPieChartPartialResults() {
        return $$('[data-test-el="decisions-made-widget"] [class*="value"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeesApprovedForLeaveWidgetPieChartTotalResults() {
        // return this.leavesDecidedWidget().$('[class*="totalValue"]');
        return $('[data-test-el="employees-approved-for-leave"] [class*="totalValue"]');
    }

    /**
     * @returns {ElementFinder}
     */
    highContrastModeIcon() {
        return $('[data-icon="universal-access"]');
    }

    /**
     * @returns {ElementFinder}
     */
    highContrastModeSwitch() {
        return $('[data-test-el="switch"]');
    }

    /**
     * @returns {ElementFinder}
     */
    highContrastModeLabel() {
        return $('[data-test-el="FINEOS_COMMON.HIGH_CONTRAST_MODE.SWITCH_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    highContrastModeDescription() {
        return $('[data-test-el="FINEOS_COMMON.HIGH_CONTRAST_MODE.DESCRIPTION"]');
    }
}

module.exports = MyDashboardPage;
