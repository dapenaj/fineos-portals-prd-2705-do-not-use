class IntakeAcknowledgmentScreen {

    /**
     * @returns {ElementFinder}
     */
    confirmationScreen() {
        return $('[data-test-el="intake-success-tick"]');
    }

    /**
     * @returns {ElementFinder}
     */
    notificationNumber() {
        return $('[data-test-el="success-assessment-notification-id"]');
    }

    /**
     * @returns {ElementFinder}
     */
    closeBtn() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.CLOSE"]');
    }
}

module.exports = IntakeAcknowledgmentScreen;
