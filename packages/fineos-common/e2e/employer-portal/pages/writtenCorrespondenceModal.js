class WrittenCorrespondenceModal {
    /**
     * @returns {ElementFinder}
     */
    modalHeader() {
        return $('[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemCorrespondenceViaPostCheckbox() {
        const radioButtonName = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_PAPER');
        return element(by.labelText(radioButtonName));
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemAnSmsCheckbox() {
        const radioButtonName = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_SMS');
        return element(by.labelText(radioButtonName));
    }

    /**
     * @returns {ElementFinder}
     */
    selectedPreferredNumber() {
        return $('[data-test-el="selected-phone-number-card"] label');
    }

    /**
     * @returns {ElementFinder}
     */
    selectedPreferredEmail() {
        return $('[data-test-el="selected-email-address-card"] label');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    registeredMobilePhones() {
        return $$('[data-test-el="selected-phone-number-card"], [data-test-el="phone-number-card"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    registeredEmails() {
        return $$('[data-test-el="selected-email-address-card"], [data-test-el="email-address-card"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemCorrespondenceViaPortalCheckbox() {
        const radioButtonName = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_PORTAL');
        return element(by.labelText(radioButtonName));
    }

    /**
     * @returns {ElementFinder}
     */
    sendThemAnEmailCheckbox() {
        const radioButtonName = translationsHelper.getTranslation('PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_EMAIL');
        return element(by.labelText(radioButtonName));
    }

    /**
     * @returns {ElementFinder}
     */
    okButton() {
        return $('[data-test-el="FINEOS_COMMON.GENERAL.OK"]');
    }

    /**
     * @param {string} errorType
     * @returns {ElementFinder}
     */
    errorMessage(errorType) {
        return $(`[data-test-el="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.VALIDATION.${errorType}"]`);
    }

}

module.exports = WrittenCorrespondenceModal;
