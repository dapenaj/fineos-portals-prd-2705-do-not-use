class ConcurrencyErrorModal {

    /**
     * @returns {ElementFinder}
     */
    title() {
        return $('[data-test-el="DATA_CONCURRENCY_MODAL.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    refreshButton() {
        return $('[data-test-el="DATA_CONCURRENCY_MODAL.REFRESH_BUTTON"]');
    }

    /**
     * @returns {ElementFinder}
     */
    description() {
        return $('[data-test-el="DATA_CONCURRENCY_MODAL.DESCRIPTION"]');
    }

    /**
     * @returns {ElementFinder}
     */
    image() {
        return $('[data-test-el="data-concurrency-response-image"]');
    }
}

module.exports = ConcurrencyErrorModal;
