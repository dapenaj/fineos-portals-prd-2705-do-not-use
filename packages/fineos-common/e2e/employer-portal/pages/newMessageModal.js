class NewMessageModal {

    /**
     * @returns {ElementFinder}
     */
    newMessageModal() {
        return $('[data-test-el="new-message-drawer"]');
    }

    /**
     * @returns {ElementFinder}
     */
    newMessageModalTitle() {
        return this.newMessageModal().$('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    subjectLabel() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    subjectInput() {
        return $('[data-test-el="subject-field"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messageLabel() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messageInput() {
        return $('[data-test-el="message-field"]');
    }

    /**
     * @returns {ElementFinder}
     */
    remainingCharsCounter() {
        return $('[data-test-el="remaining-characters-counter"]');
    }

    /**
     * @returns {ElementFinder}
     */
    remainingCharactersInfo() {
        return $('[data-test-el="remaining-characters-counter"]');
    }

    /**
     * @returns {ElementFinder}
     */
    subjectMinLengthAlert() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MIN_SUBJECT_LENGTH"]');
    }

    /**
     * @returns {ElementFinder}
     */
    subjectMaxLengthAlert() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MAX_SUBJECT_LENGTH"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messageMinLengthAlert() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MIN_LENGTH"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messageMaxLengthAlert() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MAX_LENGTH"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messageSentAlert() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_SENT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sendButton() {
        return $('[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.SEND"]');
    }

    /**
     * @returns {ElementFinder}
     */
    cancelSendingPopUpTxt() {
        return $('[data-test-el$="NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING"]');
    }
}

module.exports = NewMessageModal;
