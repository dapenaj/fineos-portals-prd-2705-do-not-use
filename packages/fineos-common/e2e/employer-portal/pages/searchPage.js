class SearchPage {

    /**
     * @returns {ElementFinder}
     */
    userMenu() {
        return $('[data-test-el="user-options-dropdown-label"]');
    }

    /**
     * @returns {ElementFinder}
     */
    optionControlUserAccess() {
        return $('[data-test-el="USER_ACTIONS.CONTROL_USER_ACCESS"]');
    }

    /**
     * @returns {ElementFinder}
     */
    logOutButton() {
        return $('button[data-test-el="USER_ACTIONS.LOGOUT"]');
    }

    /**
     * @returns {ElementFinder}
     */
    sectionSearch() {
        return element(by.css('[data-test-el="search-form"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    buttonDropdownOptionByText(option) {
        return element(by.cssContainingText('span[data-test-el*="HEADER.SEARCH."]', option));
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    textSelectedSearchMode() {
        return this.sectionSearch().element(by.css('.ant-select-selection-item'));
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    dropdownSearchMode() {
        return this.sectionSearch().element(by.css('[data-test-el="search-type"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    sectionDropdownList() {
        return $('div[class*="ant-select-dropdown"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    sectionDropdownOptions() {
        return this.sectionDropdownList().$$('[data-test-el="search-type-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    inputSearch() {
        return this.sectionSearch().element(by.css('[data-test-el="input"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    sectionSearchResults() {
        return element(by.css('[data-test-el="search-result"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    sectionSearchResult(index) {
        return this.sectionSearchResults().all(by.css('[data-test-el="search-item"]')).get(index);
    }

    /**
     * @returns {ElementFinder}
     */
    sectionSearchResultLink(index) {
        return this.sectionSearchResult(index).element(by.css('[data-test-el="result-title"]'));
    }

    /**
     * @returns {ElementArrayFinder}
     */
    listSearchResults() {
        return this.sectionSearchResults().all(by.css('[data-test-el="search-item"]'));
    }

    /**
     * @returns {ElementFinder}
     */
    tooManyResultsMessage() {
        return $('span[data-test-el$="HEADER.SEARCH.TOO_MANY_RESULTS"]');
    }

    /**
     * @param {ElementFinder} resultItem
     * @returns {ElementFinder}
     */
    searchResultItemTitle(resultItem) {
        return resultItem.$('[data-test-el="result-title"]');
    }

    /**
     * @param {ElementFinder} resultItem
     * @returns {ElementFinder}
     */
    searchResultItemData(resultItem) {
        return resultItem.$('[data-test-el="result-description"]');
    }

    /**
     * @returns {ElementFinder}
     */
    noNotificationFoundMessage() {
        return $('span[data-test-el$="HEADER.SEARCH.NO_NOTIFICATION_FOUND"]');
    }

    /**
     * @returns {ElementFinder}
     */
    noEmployeeFoundMessage() {
        return $('span[data-test-el$="HEADER.SEARCH.NO_EMPLOYEE_FOUND"]');
    }

    /**
     * @returns {ElementFinder}
     */
    searchEmptyItem() {
        return $('[data-test-el="search-empty-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    startNotificationOnBehalfLink() {
        return $('[data-test-el="HEADER.SEARCH.NON_ESTABLISHED_INTAKE_LINK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    makeNotificationLink() {
        return $('[data-test-el="HEADER.SEARCH.ESTABLISHED_INTAKE_LINK"]');
    }

    /**
     * @returns {ElementFinder}
     */
    employeeSearchResult() {
        return $('[data-test-el="search-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    myDashboardButton() {
        return $('[data-test-el="NAVIGATION.MY_DASHBOARD"]');
    }

    /**
     * @returns {ElementFinder}
     */
    messagesButton() {
        return $('[data-test-el="NAVIGATION.MESSAGES"]');
    }

    /**
     * @returns {ElementFinder}
     */
    unreadMessagesCounter() {
        return $('[data-test-el="messages-page-with-label"] [data-test-el="text-badge-label"]');
    }
}

module.exports = SearchPage;
