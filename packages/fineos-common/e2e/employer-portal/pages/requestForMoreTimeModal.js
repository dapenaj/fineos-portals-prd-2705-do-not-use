class RequestForMoreTimeModal {
    /**
     * @returns {ElementFinder}
     */
    absenceFormTitle() {
        return $('[data-test-el="REQUEST_FOR_MORE_TIME.TITLE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    absenceFormStartDate() {
        return $('[data-test-el="range-picker"] [placeholder*="art date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    absenceFormEndDate() {
        return $('[data-test-el="range-picker"] [placeholder*="nd date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    absenceFormTextArea() {
        return $('textArea[name="additionalNotes"]');
    }

    /**
     * @param {string} isoDate
     * @returns {ElementFinder}
     */
    dayMonthDatePicker(isoDate) {
        return $(`table.ant-picker-content td[title="${isoDate}"] div.ant-picker-cell-inner`);
    }

    /**
     * @param {string} text
     * @returns {ElementFinder}
     */
    absenceFormLabels(text) {
        return element(by.cssContainingText('[data-test-el="form-group-label"]', text));
    }

    /**
     * @returns {ElementFinder}
     */
    datePeriodsValidation() {
        return $('[data-test-el="REQUEST_FOR_MORE_TIME.REQUIRED_PERIOD_VALIDATION"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    alertMessages() {
        return $$('[role="alert"] [data-test-el]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationHeader() {
        return $('[data-test-el$="REQUEST_FOR_MORE_TIME.SUCCESS_HEAD"]');
    }

    /**
     * @returns {ElementFinder}
     */
    confirmationMessage() {
        return $('[data-test-el$="REQUEST_FOR_MORE_TIME.SUCCESS_MESSAGE"]');
    }

    /**
     * @returns {ElementFinder}
     */
    existingAbsencePeriodStartDate() {
        return element.all(by.xpath('//*[contains(@class,"periodStart")]/..')).first();
    }

    /**
     * @returns {ElementFinder}
     */
    existingAbsencePeriodEndDate() {
        return element.all(by.xpath('//*[contains(@class,"periodEnd")]/..')).first();
    }
}

module.exports = RequestForMoreTimeModal;
