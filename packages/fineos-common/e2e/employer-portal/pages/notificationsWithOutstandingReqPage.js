class NotificationsWithOutstandingReqPage {
    /**
     * @returns {ElementFinder}
     */
    drawerTitle() {
        return $$('[data-test-el="OUTSTANDING_NOTIFICATIONS.TITLE"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    filteredDrawerHeader() {
        return $('[class*="drawer-module_drawerHeader"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    searchNotificationButton() {
        return $$('[role="button"] [role="img"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    searchEmployeeNameButton() {
        return $$('[role="button"] [role="img"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    inputSearchNotification() {
        return $('#caseNumber, [data-test-el="input-caseNumber"]');
    }

    /**
     * @returns {ElementFinder}
     */
    inputSearchFirstName() {
        return $('input[data-test-el*="FirstName"]');
    }

    /**
     * @returns {ElementFinder}
     */
    inputSearchLastName() {
        return $('input[data-test-el*="LastName"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeeCells() {
        return $$('a[class*="groupable-row-cell-link"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    decisionCheckIcons() {
        return $$('svg[class*="checkIcon"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    decisionNotCheckIcons() {
        return $$('svg[class*="timesIcon"]');
    }

    /**
     * @returns {ElementFinder}
     */
    filterSearchButton() {
        return $('[data-test-el="search-button"]');
    }

    /**
     * @returns {ElementFinder}
     */
    filterResetButton() {
        return $('[data-test-el="reset-button"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    pageSpinners() {
        return $$('[data-test-el="spinner"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    allPaginationButtons() {
        return $$('ul[data-test-el="pagination"]');
    }

    // TODO - FEP-1341
    /**
     * @returns {ElementArrayFinder}
     */
    paginationPagesButtons() {
        return $$('[class*="pagination-item"]');
    }

    /**
     * @returns {ElementFinder}
     */
    paginationPreviousButton() {
        return $('[data-test-el="pagination-prev-link"]');
    }

    /**
     * @returns {ElementFinder}
     */
    paginationNextButton() {
        return $('[data-test-el="pagination-next-link"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    paginationOptionsButton() {
        return $$('ul[class*="pagination"] li').last();
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    paginationOptionsListItem(text) {
        return $('div[class*="ant-select-dropdown"]').element(by.cssContainingText(
            'div[class*="ant-select-item-option-content"]',
            `${text} /`
        ));
    }

    /**
     * @returns {ElementFinder}
     */
    paginationOptions() {
        return $('[data-test-el="pagination"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationRows() {
        return $$('table tr[data-row-key]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationsLinks() {
        return this.notificationRows().$$('[href*="notification"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeesLinks() {
        return this.notificationRows().$$('[href*="profile"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    notificationDatesRows() {
        return $$('[data-test-el="outstanding-notification-table-date"]');
    }

    /**
     * @returns {ElementFinder}
     */
    tooltip() {
        return $('[role="tooltip"]');
    }

    /**
     * @param {string} option
     * @returns {ElementFinder}
     */
    sortButton(option) {
        return this.sortedColumnHeader().$(`[aria-label="caret-${option}"]`);
    }

    /**
     * @param {string} option - up, down
     * @param {ElementFinder} sortColumn
     * @returns {ElementFinder}
     */
    sortButton2(sortColumn, option) {
        return sortColumn.$(`[aria-label="caret-${option}"]`);
    }

    /**
     * @returns {ElementFinder}
     */
    sortColumn() {
        return $('th[class*="column-sort"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    numberOfNotifications() {
        return $('[class*="notifications-header_count_"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableRowById(notificationId) {
        return $(`[data-row-key="${notificationId}"]`);
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellNotification(notificationId) {
        return this.tableRowById(notificationId).$$('td > a').first();
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellEmployeeName(notificationId) {
        return this.tableRowById(notificationId).$$('td > a').last();
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellEmployee(notificationId) {
        return this.tableRowById(notificationId).$$('td > div').first();
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellEmployeeJobTitle(notificationId) {
        return this.tableRowById(notificationId).$('[data-test-el="outstanding-notification-table-job-title"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellEmployeeOrganizationUnit(notificationId) {
        return this.tableRowById(notificationId).$('[data-test-el="outstanding-notification-table-organisation-unit"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellEmployeeWorkSite(notificationId) {
        return this.tableRowById(notificationId).$('[data-test-el="outstanding-notification-table-work-site"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellNotificationsDate(notificationId) {
        return this.tableRowById(notificationId).$('[data-test-el="outstanding-notification-table-date"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    missingFilesLinks() {
        return $$('[data-test-el="upload-document-link"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementFinder}
     */
    tableCellActions(notificationId) {
        return this.tableRowById(notificationId).$('[data-test-el="outstanding-notification-actions"]');
    }

    /**
     * @returns {ElementFinder}
     */
    filteredItemsNumber() {
        return $('[data-test-el="FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT"] strong:nth-child(1)');
    }

    /**
     * @returns {ElementFinder}
     */
    closeFilteredResultsBadge() {
        return $('[data-test-el="filter-items-count-tag"] svg');
    }

    /**
     * @returns {ElementFinder}
     */
    filteredResultsBadge() {
        return $('[data-test-el="FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementArrayFinder}
     */
    tableCellOutstandingItemsList(notificationId) {
        return $(`[data-row-key="${notificationId}"]~tr`).$$('[data-test-el="upload-outstanding-information-item"]');
    }

    /**
     *
     * @returns {ElementArrayFinder}
     */
    tableCellOutstandingItemsListRandom() {
        return $$('[data-test-el="upload-outstanding-information-item"]');
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @param {string} itemTxt - expected item text
     * @returns {ElementArrayFinder}
     */
    uploadedOutstandingItemIcon(notificationId, itemTxt) {
        return element(by.cssContainingText(
            `[data-row-key="${notificationId}"]~tr [data-test-el="upload-outstanding-information-item"]`,
            `${itemTxt}`
        )).element(by.css('[data-test-el="uploaded-outstanding-information-icon"]'));
    }

    /**
     * @param {string} notificationId - notification ID - for example NTN-123
     * @returns {ElementArrayFinder}
     */
    outstandingItemsCounter(notificationId) {
        return $(`[data-row-key="${notificationId}"]~tr [data-test-el="OUTSTANDING_NOTIFICATIONS.NOTIFICATIONS_INFO"]`);
    }

    /**
     *
     * @returns {ElementArrayFinder}
     */
    outstandingItemsCounterRandom() {
        return $('[data-test-el="OUTSTANDING_NOTIFICATIONS.NOTIFICATIONS_INFO"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    sortedCells() {
        return $$('td[class*="sort"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeesApprovedForLeaveDateCell() {
        return $$('[data-test-el="WIDGET.TIME_APPROVED_HOURS"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeesApprovedForLeaveDateTodayCell() {
        return $$('[data-test-el="WIDGET.TIME_APPROVED_HOURS_TODAY"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeesApprovedForLeaveDateMinCell() {
        return $$('[data-test-el="WIDGET.TIME_APPROVED"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    employeesApprovedForLeaveDateTodayMinCell() {
        return $$('[data-test-el="WIDGET.TIME_APPROVED_TODAY"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    dICells() {
        return $$('tr > td:last-of-type');
    }

    /**
     * {ElementFinder} - parentElement
     * @returns {ElementFinder}
     */
    checkIcon(parentElement) {
        return parentElement.$('[data-icon="check"]');
    }

    /**
     * {ElementFinder} - parentElement
     * @returns {ElementFinder}
     */
    xIcon(parentElement) {
        return parentElement.$('[data-icon="times"]');
    }

    /**
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    sortedColumnHeader() {
        return $('th[class*="column-sort"]');
    }

    /**
     * @param {string} headerName - column header name
     * @returns {ElementFinder}
     */
    columnHeader(headerName) {
        return $$(`th [data-test-el*="${headerName}"]`).first();
    }

    /**
     * @returns {ElementFinder}
     */
    downloadListButton() {
        return $('[data-test-el="EXPORT_OUTSTANDING_LIST.BUTTON_TRIGGER"]');
    }

    /**
     * {string} headerName
     * @returns {ElementFinder}
     */
    // TODO FEP-1341 - missing locator
    filterIcon(headerName) {
        if (headerName === 'DI' || headerName === 'DECISION') {
            return this.columnHeader(headerName).element(by.xpath('./../../..//button'));
        }
        return this.columnHeader(headerName).element(by.xpath('./../../../../..//span[@role="button"]'));
    }

    /**
     * {string} fieldName
     * @returns {ElementFinder}
     */
    filterInput(fieldName) {
        return $(`input[name*=${fieldName.slice(1)}]`);
    }

    /**
     * @returns {ElementArrayFinder}
     */
    // TODO FEP-1341 - missing locator
    reasonsList() {
        return $$('ul[class*="table-filter"] li');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    decisionsList() {
        return $$('[data-test-el*="filter"]  [data-test-el*="WIDGET.DECISIONS_MADE_"]');
    }

    /**
     * @returns {ElementArrayFinder}
     */
    viewCalendarButtons() {
        return $$('[data-test-el="WIDGET.ABSENCE_CALENDAR.VIEW_CALENDAR"]');
    }
}

module.exports = NotificationsWithOutstandingReqPage;
