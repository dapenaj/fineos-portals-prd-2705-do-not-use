class DatePicker {
    /**
     * @returns {ElementFinder}
     */
    prevMonth() {
        return $$('button[class*="header-prev-btn"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    prevYear() {
        return $$('button[class*="header-super-prev-btn"]').first();
    }

    /**
     * @returns {ElementFinder}
     */
    nextMonth() {
        return $$('button[class*="header-next-btn"]').last();
    }

    /**
     * @returns {ElementFinder}
     */
    nextYear() {
        return $$('button[class*="header-super-next-btn"]').last();
    }

    /**
     * @returns {ElementArrayFinder}
     */
    daysList() {
        return $$('td[class*="ant-picker-cell"]');
    }

    /**
     * @returns {ElementFinder}
     */
    dayCell(parentElement) {
        return parentElement.element(by.xpath('./*'));
    }

    /**
     * @returns {ElementArrayFinder}
     */
    actualMonthYearCards() {
        return $$('[class*="ant-picker-header-view"]').first().$$('button');
    }
}

module.exports = DatePicker;
