const fs = require('fs');
const configDir = './employer-portal/testData/generatedTestData';

/**
 * Objects that works as proxy to remote Environment, and has the same interface,
 * the only 2 differences is that, it created from remote Environment and has different type
 */
class LocalEnvironment {
    /**
     *
     * @param remoteEnvironment {Environment}
     * @param localUrl {string}
     */
    constructor(remoteEnvironment, localUrl) {
        this._remoteEnvironment = remoteEnvironment;
        this._localUrl = localUrl;
    }

    /**
     * @returns {string}
     */
    get type() {
        return 'local';
    }

    /**
     *
     * @returns {URL}
     */
    get localUrl() {
        return new URL(this._localUrl);
    }

    /**
     * @returns {string}
     */
    get name() {
        return this._remoteEnvironment._name;
    }

    /**
     * @returns {string}
     */
    get url() {
        return this._remoteEnvironment._url;
    }

    /**
     * @returns {string}
     */
    get appName() {
        return this._remoteEnvironment._appName;
    }

    /**
     * @returns {Array<{descriptor: string, id: string}>}
     */
    get employees() {
        return this._remoteEnvironment._employees;
    }

    /**
     * @returns {Array<{description: string, descriptor: string, id: string}>}
     */
    get notifications() {
        return this._remoteEnvironment._notifications;
    }

    /**
     * @returns {Array<{descriptor: string, username: string, password: string, displayName: string}>}
     */
    get users() {
        return this._remoteEnvironment._users;
    }
}

/**
 * Object to store current env settings, can be extended by API urls or other env-specific stuff
 */
class Environment {
    /**
     * @param {string} name
     * @param {string} url
     * @param {string|null} [appName=null]
     * @param {Array<{descriptor: string, id: string}>} employees
     * @param {Array<{description: string, descriptor: string, id: string}>} notifications
     * @param {Array<{descriptor: string, username: string, password: string, displayName: string}>} users
     */
    constructor(
        name,
        url,
        appName = null,
        employees = [],
        notifications = [],
        users = []
    ) {
        this._name = name;
        this._url = url;
        this._appName = appName;
        this._employees = employees;
        this._notifications = notifications;
        this._users = users;
    }

    /**
     *
     * @returns {string}
     */
    get type() {
        return 'remote';
    }

    /**
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     * @returns {string}
     */
    get url() {
        return this._url;
    }

    /**
     * @returns {string}
     */
    get appName() {
        return this._appName;
    }

    /**
     * @returns {Array<{descriptor: string, id: string}>}
     */
    get employees() {
        return this._employees;
    }

    /**
     * @returns {Array<{description: string, descriptor: string, id: string}>}
     */
    get notifications() {
        return this._notifications;
    }

    /**
     * @returns {Array<{descriptor: string, username: string, password: string, displayName: string}>}
     */
    get users() {
        return this._users;
    }
}

/**
 * Definition of env details, can be extended by API urls or other env-specific stuff
 */
class Environments {

    /**
     * Gets test data basing on env name
     * @param {string} path - path to configuration file
     * @param {string} envName - envirnoment name
     * @returns {Object}
     */
    loadDataFromFile(path, envName) {
        if (fs.existsSync(path)) {
            const fileContents = fs.readFileSync(path, 'utf8');
            try {
                const data = JSON.parse(fileContents);
                if (data.portal === envName) {
                    return data;
                }
            } catch (err) {
                throw new Error(`CRITICAL ERROR: Failed to parse configuration JSON ${path}\n${err}`);
            }
        }
        throw new Error(`CRITICAL ERROR: no test data found for envirnoment [${envName}]. Make sure ${path} exists.`);
    }

    /**
     * Gets test data basing on env name
     * @param envName
     * @returns {Environment|LocalEnvironment}
     */
    getEnvironmentData(envName) {
        if (envName === 'demo') {
            return new Environment(envName, 'https://google.com');
        } else if (envName.startsWith('local:')) {
            const [, remoteEnvName] = envName.split(':');
            return new LocalEnvironment(
                this.getEnvironmentDataByEnvName(remoteEnvName),
                process.env.LOCAL_URL || 'http://localhost:3000'
            );
        }

        return this.getEnvironmentDataByEnvName(envName);
    }

    getEnvironmentDataByEnvName(envName) {
        const config = this.loadDataFromFile(`${configDir}/${envName}/env.json`, envName);
        const {notifications} = this.loadDataFromFile(`${configDir}/${envName}/notifications.json`, envName);
        const {employees} = this.loadDataFromFile(`${configDir}/${envName}/employees.json`, envName);
        return new Environment(
            envName,
            config.url,
            config.appName,
            employees,
            notifications,
            config.users
        );
    }

    /**
     * Try to read notification details for mapped cases
     * @param {string} directory - directory in which we need to search
     * @param {Array<{descriptor: string, id: string}>} mappingArray
     * @param {string} envName - envirnoment name
     * @returns {Array<{id:string, data: Object}>}
     */
    readTestDetails(directory, mappingArray, envName) {
        const foundItems = [];
        const missingItems = [];
        mappingArray.forEach(item => {
            const path = `${configDir}/${envName}/${directory}/${item.id}.json`;
            if (fs.existsSync(path)) {
                if (!foundItems.map(ids => ids.id).includes(item.id)) {
                    const fileContents = fs.readFileSync(path, 'utf8');
                    try {
                        foundItems.push({
                            data: JSON.parse(fileContents),
                            id: item.id
                        });
                    } catch (err) {
                        throw new Error(`CRITICAL ERROR: Failed to parse configuration JSON ${path}\n${err}`);
                    }
                }
            } else if (!missingItems.includes(item.id)) {
                missingItems.push(item.id);
            }
        });
        if (missingItems.length > 0) {
            // eslint-disable-next-line no-console
            console.log(`(!) Missing test details for ${directory}: ${missingItems.join(', ')} (!)`);
        }
        return foundItems;
    }
}

module.exports.environments = new Environments();
module.exports.Environment = Environment;
module.exports.LocalEnvironment = LocalEnvironment;
