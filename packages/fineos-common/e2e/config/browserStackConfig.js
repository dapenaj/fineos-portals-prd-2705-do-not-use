const sharedConfig = require('./sharedConfig.js').config;
const browserstack = require('browserstack-local');
const config = {
    afterLaunch() {
        return new Promise(function (resolve, reject) {
            exports.bs_local.stop(resolve);
        });
    },
    // Code to start browserstack local before start of test
    beforeLaunch() {
        /* eslint-disable no-console */
        console.log('Connecting local');
        return new Promise(function (resolve, reject) {
            exports.bs_local = new browserstack.Local();
            exports.bs_local.start({key: exports.config.capabilities['browserstack.key']}, function (error) {
                if (error) {
                    return reject(error);
                }
                console.log('Connected. Now testing...');

                resolve();
            });
        });
    },
    capabilities: {
        browserName: 'chrome',
        'browserstack.key': 'key',
        'browserstack.local': true,
        'browserstack.user': 'user',
        name: 'Fineos Demo - [Protractor] Local Test'
    },
    seleniumAddress: 'http://hub-cloud.browserstack.com/wd/hub'
    // Code to stop browserstack local after end of test
};
const cucumberOpts = {};

exports.config = {...sharedConfig, ...config};
exports.config.cucumberOpts = {...exports.config.cucumberOpts, ...cucumberOpts};
