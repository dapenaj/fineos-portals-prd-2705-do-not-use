const sharedConfig = require('./sharedConfig.js').config;

/**
 * Generates HTML report basing on created json
 */
function makeHtmlReport() {
    const reportFileName = browser.params.reportPath ? browser.params.reportPath : sharedConfig.cucumberOpts.jsonName;
    const reportOptions = {
        ignoreBadJsonFile: true,
        jsonFile: reportFileName,
        launchReport: false,
        name: 'Cucumber Report',
        output: reportFileName.replace('.json', '.html'),
        reportSuiteAsScenarios: true,
        theme: 'hierarchy'
    };
    const reporter = require('cucumber-html-reporter');
    reporter.generate(reportOptions);
}

const config = {
    directConnect: true
};
const cucumberOpts = {};

exports.config = {...sharedConfig, ...config};
exports.config.cucumberOpts = {...exports.config.cucumberOpts, ...cucumberOpts};
exports.config.onComplete = makeHtmlReport;
