const sharedConfig = require('./sharedConfig.js').config;
const cucumberReportJson = 'reports/grid.json';
const config = {
    capabilities: {},
    directConnect: false,
    multiCapabilities: [
        {
            browserName: 'chrome',
            chromeOptions: {
                args: ['--headless', '--disable-gpu', '--window-size=1366,768']
            },
            maxInstances: 5,
            shardTestFiles: true
        }
    ],
    seleniumAddress: 'http://localhost:4444/wd/hub'
};
const cucumberOpts = {
    format: [`json:./${cucumberReportJson}`]
};

exports.config = {...sharedConfig, ...config};
exports.config.cucumberOpts = {...exports.config.cucumberOpts, ...cucumberOpts};
