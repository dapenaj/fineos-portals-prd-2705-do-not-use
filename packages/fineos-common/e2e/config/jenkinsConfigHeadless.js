const sharedConfig = require('./sharedConfig.js').config;
const program = require('commander');
const config = {
    directConnect: true
};
const cucumberOpts = {
    // custom formatter in general produce daily-like report
    // `json:./reports/dailyTestReport.json`
    format: './common/test-utils/customFormatter.js'
};
const chromeOptions = {
    args: [
        '--disable-extensions',
        `--window-size=${program.resolution}`,
        '--incognito',
        '--headless',
        '--disable-dev-shm-usage',
        '--disable-gpu',
        '--no-sandbox',
        '--disable-browser-side-navigation',
        '--disable-infobars',
        '--disableFeature=VizDisplayCompositor'
    ]
};
const loggingPrefs = {
    browser: 'ALL',
    driver: 'ALL',
    server: 'ALL'
};

function overrideCapabilities(capability) {
    return {
        ...capability,
        chromeOptions: {
            ...capability.chromeOptions,
            ...chromeOptions
        },
        loggingPrefs: {
            ...capability.loggingPrefs,
            ...loggingPrefs
        }
    };
}

exports.config = {...sharedConfig, ...config};
exports.config.cucumberOpts = {...exports.config.cucumberOpts, ...cucumberOpts};
if (exports.config.multiCapabilities) {
    exports.config.multiCapabilities = exports.config.multiCapabilities.map(overrideCapabilities);
} else {
    exports.config.capabilities = overrideCapabilities(exports.config.capabilities);
}
