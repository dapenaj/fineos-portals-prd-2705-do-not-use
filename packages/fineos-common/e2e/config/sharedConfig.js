/* eslint-disable no-console */
const date = new Date();
const line = '----------------------------------------------';
const cucumberReportDirectory = 'reports';
const cucumberReportJson = `${cucumberReportDirectory}/${date.valueOf().toString()}.json`;
const { environments } = require('./environments');
const program = require('commander');
const path = require('path');
const { default: parseTags } = require('cucumber-tag-expressions');
const { readdirSync, readFileSync } = require('fs');

program
    .option('--browser <browser>', 'browser to use', /^chrome|firefox$/, 'chrome')
    .option('--build <build>', 'build number', x => parseInt(x, 10), 1)
    .option('--env <environment>', 'environment to run tests on', 'demo')
    .option('--job <job>', 'job name', 'JENKINS JOB')
    .option('--tags <tags>', 'tags to run', null)
    .option('--threads <threads>', 'browser parallel instances', x => parseInt(x, 10), 3)
    .option('--language <language>', 'translation language', 'en')
    .option('--resolution <resolution>', 'browser resolution', '1366,768')
    .option('--headless', 'run browser in headless mode');

program.parse(process.argv);
const processTags = process.argv.find(x => x.indexOf('tags') > -1);
const envName = program.env.toLowerCase();
const environment = environments.getEnvironmentData(envName);
const isLocal = envName.startsWith('local:');
const remoteEnvName = isLocal ? envName.substr('local:'.length) : envName;
const FEATURES_BASE_PATH = '../../../employer-portal/e2e/features';

const buckets = [];

const tagExpressionValidator = parseTags(program.tags);

const availableFeatures =
  readdirSync(path.resolve(__dirname, FEATURES_BASE_PATH))
    .filter(availableFeature => {
      if (!availableFeature.endsWith('.feature')) {
          console.log(`No files with feature extension found in directory ${FEATURES_BASE_PATH}`)
        return false;
      }
      const fileContent = readFileSync(path.resolve(__dirname, FEATURES_BASE_PATH) + '/' + availableFeature, 'utf8');
      return tagExpressionValidator.evaluate(fileContent);
    });
const actualThreads = Math.min(program.threads, availableFeatures.length);
const bucketSize = availableFeatures.length / Math.min(program.threads, availableFeatures.length);

console.log(`${actualThreads} threads created`);

for (let i = 0; i < actualThreads; i++) {
  buckets.push(
    availableFeatures.slice(i * bucketSize, (i + 1) * bucketSize)
  );
}

console.log(`${availableFeatures.length} Features split into ${buckets.length} threads`);

// validate our split logic
if (buckets.reduce((acc, curr) => acc + curr.length, 0) !== availableFeatures.length) {
  console.log('Next buckets identified', buckets);
  console.log('Actual features', availableFeatures);
  throw new Error('Bucket split failed');
}

if (!buckets.length) {
  throw new Error('No buckets identified');
}
console.log('program.browser: ' + program.resolution);
const chromeOptions = {
  args: [
    '--disable-extensions',
      `--window-size=${program.resolution}`,
    '--incognito',
    ...(isLocal ? [
      '--user-data-dir=/tmp/unsecure-chrome-user-data',
      '--disable-web-security',
      '--disable-site-isolation-trials'
    ] : []),

    ...(program.headless ? [
      '--headless'
    ] : [])
  ],
  binary: require('puppeteer').executablePath()
};

const multiCapabilities = buckets.map((bucket, i) => ({
  browserName: program.browser,
  shardTestFiles: true,
  name: `Parallel job ${i + 1}`,
  logName: 'Chromium',
  maxInstances: 1,
  specs: bucket.map(featureFileName => FEATURES_BASE_PATH + '/' + featureFileName),
  chromeOptions
}))

exports.config = {
    allScriptsTimeout: 300000,
    baseUrl: environment.url,
    directConnect: true,
    ...(multiCapabilities.length > 1
      ? { multiCapabilities }
      : { capabilities: multiCapabilities[0] }
    ),
    cucumberOpts: {
        compiler: [],
        defaultTimeoutInterval: 300000,
        format: [/*`json:./${cucumberReportJson}`,*/ './common/test-utils/customFormatter.js'],
        jsonDir: cucumberReportDirectory,
        jsonName: cucumberReportJson,
        require: ['../../../**/step_definitions/*Steps.js', '../**/common/hooks.js'],
        strict: true,
        tags: program.tags
    },
    disableChecks: true,
    framework: 'custom',
    frameworkPath: '../node_modules/protractor-cucumber-framework',
    maxInstances: 15,
    name: `${program.job}-${program.env}-${program.browser}`,
    restartBrowserBetweenTests: true,
    shardTestFiles: true,
    timeout: 300000
};

exports.config.onPrepare = () => {
    process.setMaxListeners(0);
    browser.params.tcPattern = /^@C\d{2,3}-F\d{2,3}[A-Z]?[0-9]?-[\d-]+|TC\d{2,3}/;
    browser.params.env = environment;
    const chai = require('chai');
    chai.use(require('chai-as-promised'));
    chai.use(require('chai-sorted'));
    require('../common/helpers/overrides.js');

    global.expect = chai.expect;
    global.Faker = require('faker');
    global.EC = require('protractor/built/index').ExpectedConditions;
    global.protractor = require('protractor/built/index');
    global.language = program.language;

    global.browser = browser;
    global.element = element;
    global.$ = $;
    global.$$ = $$;
    global.by = by;

    const {Given, Then, When} = require('cucumber');
    global.Given = Given;
    global.When = When;
    global.Then = Then;

    require('../common/helpers/customLocators.js');

    const WaitHelpers = require('../common/helpers/waitHelpers.js');
    global.waitHelpers = new WaitHelpers();

    const ExpectHelpers = require('../common/helpers/expectHelpers.js');
    global.expectHelpers = new ExpectHelpers();

    const TranslationsHelper = require('../common/helpers/translationsHelper.js');
    global.translationsHelper = new TranslationsHelper();

    const DatePickerHelper = require('../common/helpers/datePickerHelper.js');
    global.datePickerHelper = new DatePickerHelper();

    const TestDataHelpers = require('../common/helpers/testDataHelpers.js');
    global.testDataHelpers = new TestDataHelpers();

    global.pages = require('../common/pages.js');
    global.actions = require('../common/actions.js');

    const constants = require('../common/constants.js');
    global.timeouts = constants.timeouts;
    global.constants = constants;
    global.problematicSteps = [];
    /**
     * @type {{searchQuery: null | string, searchTarget: null | {descriptor: string, id: string}}}
     */
    global.testVars = Object.create(constants.testVarsInitObject);
    console.log(line);
    console.log(`Reading test data...`);
    global.employeesDetails = environments.readTestDetails('employees', environment.employees, remoteEnvName);
    global.notificationsDetails = environments.readTestDetails('notifications', environment.notifications, remoteEnvName);
    console.log(line);
};

console.log(line);
console.log(`Running tests on: ${environment.name}`);
console.log(`baseUrl is: ${environment.url}`);
if (processTags) {
    console.log(`tags: ${processTags.slice(processTags.lastIndexOf('=') + 1)}`);
} else if (program.tags) {
    console.log(`tags: ${program.tags}`);
}
console.log(line);
