__________________________________________________________________________________________

#FRAMEWORK DESCRIPTION
__________________________________________________________________________________________
Structure of framework consists of:

+ e2e - main framework folder. Most of sub-folders contains files 
divided by application areas (page objects convention) to keep framework maintainability and readability
    
    + config - this is the place where Protractor configuration is stored, using configuration file you can decide 
    which browser should be used or which test tags should be launched 
    (in this example only tests with **@exampleTag** would be performed).
    For details please refer to [Protractor API](https://www.protractortest.org/#/api-overview)
    
    + common - useful code used across the framework (helpers) + mapping pages and actions to global object
    
    + **(projectName/)** actions - set of atomic test actions which are commonly repeated and can be extracted to reusable action
    
    + **(projectName/)** features - Gherkin scenarios saved in *.feature files
        
    + **(projectName/)** step_definitions - the place where business language is mapped to the code. 
        Basically set of functions which will be used when regular expression will match with Gherkin step.
    
    + **(projectName/)** pages - set of web elements used in tests, only web elements locators are stored here, actions are not allowed.

+ reports - place where test reports will be saved.

__________________________________________________________________________________________

#USEFUL LINKS
__________________________________________________________________________________________

[jQuery selectors reference](https://www.w3schools.com/jquery/jquery_ref_selectors.asp)

[Protractor homepage](https://www.protractortest.org)

[GIT tutorial](https://learngitbranching.js.org)

[Online RegExp parser / debugger](https://regex101.com)

[Gherkin reference](https://docs.cucumber.io/gherkin/reference/)
