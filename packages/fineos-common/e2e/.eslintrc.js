module.exports = {
    'env': {
        'browser': true,
        'es6': true,
        'node': true,
        'protractor': true
    },
    'extends': 'eslint:recommended',
    'globals': {
        'actions': 'readonly',
        'pages': 'readonly',
        'constants': 'readonly',
        'EC': 'readonly',
        'environments': 'readonly',
        'employeesDetails': 'readonly',
        'notificationsDetails': 'readonly',
        'language':  'readonly',
        'expect': 'readonly',
        'Faker': 'readonly',
        'Given': 'readonly',
        'Then': 'readonly',
        'timeouts': 'readonly',
        'expectHelpers': 'readonly',
        'waitHelpers': 'readonly',
        'datePickerHelper': 'readonly',
        'translationsHelper': 'readonly',
        'testDataHelpers': 'readonly',
        'When': 'readonly',
        'world': 'writable',
        'testVars': 'writable',
        'problematicSteps': 'writable'
    },
    'parserOptions': {
        'ecmaVersion': 2018
    },
    'rules': {
        'accessor-pairs': 'error',
        'array-bracket-newline': [
            'error',
            {
                'multiline': true
            }
        ],
        'array-bracket-spacing': [
            'error',
            'never'
        ],
        'array-callback-return': 'off',
        'array-element-newline': [
            'error',
            'consistent'
        ],
        'arrow-body-style': [
            'error',
            'as-needed'
        ],
        'arrow-parens': [
            'error',
            'as-needed'
        ],
        'arrow-spacing': [
            'error',
            {
                'after': true,
                'before': true
            }
        ],
        'block-scoped-var': 'error',
        'block-spacing': [
            'error',
            'never'
        ],
        'brace-style': 'error',
        'callback-return': 'error',
        'capitalized-comments': 'off',
        'class-methods-use-this': 'off',
        'comma-dangle': [
            'error',
            'never'
        ],
        'comma-spacing': [
            'error',
            {
                'after': true,
                'before': false
            }
        ],
        'comma-style': [
            'error',
            'last'
        ],
        'complexity': 'error',
        'computed-property-spacing': [
            'error',
            'never'
        ],
        'consistent-return': 'off',
        'consistent-this': 'error',
        'curly': 'error',
        'default-case': 'error',
        'dot-location': [
            'error',
            'property'
        ],
        'dot-notation': [
            'error',
            {
                'allowKeywords': true
            }
        ],
        'eol-last': 'error',
        'eqeqeq': 'error',
        'func-call-spacing': [
            'error',
            'never'
        ],
        'func-name-matching': 'error',
        'func-names': 'off',
        'func-style': [
            'error',
            'declaration',
            {
                'allowArrowFunctions': true
            }
        ],
        'function-paren-newline': 'off',
        'generator-star-spacing': 'error',
        'global-require': 'off',
        'guard-for-in': 'off',
        'handle-callback-err': 'error',
        'id-blacklist': 'error',
        'id-length': 'off',
        'id-match': 'error',
        'implicit-arrow-linebreak': [
            'error',
            'beside'
        ],
        'indent': [
            'error',
            4,
            {'SwitchCase': 1}
        ],
        'indent-legacy': 'off',
        'init-declarations': 'off',
        'jsx-quotes': 'error',
        'key-spacing': 'error',
        'keyword-spacing': [
            'error',
            {
                'after': true,
                'before': true
            }
        ],
        'line-comment-position': 'off',
        'linebreak-style': 'off',
        'lines-around-comment': 'off',
        'lines-around-directive': 'error',
        'lines-between-class-members': [
            'error',
            'always'
        ],
        'max-classes-per-file': 'off',
        'max-depth': 'error',
        'max-len': 'off',
        'max-lines': 'off',
        'max-lines-per-function': 'off',
        'max-nested-callbacks': 'error',
        'max-params': 'off',
        'max-statements': 'off',
        'max-statements-per-line': 'off',
        'multiline-comment-style': [
            'error',
            'separate-lines'
        ],
        'new-parens': 'error',
        'newline-after-var': 'off',
        'newline-before-return': 'off',
        'newline-per-chained-call': 'off',
        'no-alert': 'error',
        'no-array-constructor': 'error',
        'no-async-promise-executor': 'error',
        'no-await-in-loop': 'off',
        'no-bitwise': 'error',
        'no-buffer-constructor': 'error',
        'no-caller': 'error',
        'no-catch-shadow': 'error',
        'no-confusing-arrow': 'off',
        'no-continue': 'error',
        'no-div-regex': 'error',
        'no-duplicate-imports': 'error',
        'no-else-return': 'error',
        'no-empty-function': 'error',
        'no-eq-null': 'error',
        'no-eval': 'error',
        'no-extend-native': 'error',
        'no-extra-bind': 'error',
        'no-extra-label': 'error',
        'no-extra-parens': 'off',
        'no-floating-decimal': 'error',
        'no-implicit-coercion': 'error',
        'no-implicit-globals': 'off',
        'no-implied-eval': 'error',
        'no-inline-comments': 'off',
        'no-invalid-this': 'error',
        'no-iterator': 'error',
        'no-label-var': 'error',
        'no-labels': 'error',
        'no-lone-blocks': 'error',
        'no-lonely-if': 'error',
        'no-loop-func': 'off',
        'no-magic-numbers': 'off',
        'no-misleading-character-class': 'error',
        'no-mixed-operators': 'error',
        'no-mixed-requires': 'error',
        'no-multi-assign': 'error',
        'no-multi-spaces': [
            'error',
            {
                'ignoreEOLComments': true,
                'exceptions': {'VariableDeclarator': true}
            }
        ],
        'no-multi-str': 'error',
        'function-call-argument-newline': 'always',
        'no-multiple-empty-lines': [
            'error',
            {
                'max': 1,
                'maxEOF': 2
            }
        ],
        'no-native-reassign': 'error',
        'no-negated-condition': 'error',
        'no-negated-in-lhs': 'error',
        'no-nested-ternary': 'error',
        'no-new': 'error',
        'no-new-func': 'error',
        'no-new-object': 'error',
        'no-new-require': 'error',
        'no-new-wrappers': 'error',
        'no-octal-escape': 'error',
        'no-param-reassign': 'off',
        'no-path-concat': 'error',
        'no-plusplus': 'off',
        'no-process-env': 'off',
        'no-process-exit': 'error',
        'no-proto': 'error',
        'no-prototype-builtins': 'off',
        'no-restricted-globals': 'error',
        'no-restricted-imports': 'error',
        'no-restricted-modules': 'error',
        'no-restricted-properties': 'error',
        'no-restricted-syntax': 'error',
        'no-return-assign': 'off',
        'no-return-await': 'off',
        'no-script-url': 'error',
        'no-self-compare': 'error',
        'no-sequences': 'error',
        'no-shadow': 'off',
        'no-shadow-restricted-names': 'error',
        'no-spaced-func': 'off',
        'no-sync': 'off',
        'no-tabs': 'error',
        'no-template-curly-in-string': 'error',
        'no-ternary': 'off',
        'no-throw-literal': 'error',
        'no-trailing-spaces': 'error',
        'no-undef-init': 'error',
        'no-undefined': 'off',
        'no-underscore-dangle': 'off',
        'no-unmodified-loop-condition': 'error',
        'no-unneeded-ternary': 'error',
        'no-use-before-define': 'error',
        'no-useless-call': 'error',
        'no-useless-catch': 'error',
        'no-useless-computed-key': 'error',
        'no-useless-concat': 'error',
        'no-useless-constructor': 'error',
        'no-useless-rename': 'error',
        'no-useless-return': 'error',
        'no-var': 'error',
        'no-void': 'error',
        'no-warning-comments': 'off',
        'no-whitespace-before-property': 'error',
        'no-with': 'error',
        'nonblock-statement-body-position': 'error',
        'object-curly-newline': 'error',
        'object-curly-spacing': 'error',
        'object-shorthand': 'error',
        'one-var': [
            'error',
            'never'
        ],
        'one-var-declaration-per-line': 'error',
        'operator-assignment': 'error',
        'operator-linebreak': 'error',
        'padded-blocks': ['error',
            {
                'blocks': 'never',
                'switches': 'never'
            },
            {
                'allowSingleLineBlocks': false
            }
        ],
        'padding-line-between-statements': [
            'error',
            {'blankLine': 'always', 'prev': '*', 'next': 'class'},
            {'blankLine': 'never', 'prev': ['const', 'let'], 'next': ['const', 'let']},

        ],
        'prefer-arrow-callback': 'off',
        'prefer-const': 'error',
        'prefer-destructuring': 'error',
        'prefer-named-capture-group': 'off',
        'prefer-numeric-literals': 'error',
        'prefer-object-spread': 'error',
        'prefer-promise-reject-errors': 'error',
        'prefer-reflect': 'off',
        'prefer-rest-params': 'error',
        'prefer-spread': 'error',
        'prefer-template': 'error',
        'quote-props': [
            'error',
            'as-needed'
        ],
        'quotes': [
            'error',
            'single',
            {
                'avoidEscape': true
            }
        ],
        'radix': 'error',
        'require-atomic-updates': 'error',
        'require-await': 'error',
        'require-jsdoc': 'off',
        'require-unicode-regexp': 'off',
        'rest-spread-spacing': [
            'error',
            'never'
        ],
        'semi': [
            'error',
            'always'
        ],
        'semi-spacing': [
            'error',
            {
                'after': true,
                'before': false
            }
        ],
        'semi-style': [
            'error',
            'last'
        ],
        'sort-imports': 'error',
        'sort-keys': [
            'error',
            'asc',
            {
                'natural': true,
                'caseSensitive': false
            }
        ],
        'sort-vars': 'error',
        'space-before-blocks': 'error',
        'space-before-function-paren': [
            'error',
            {
                'anonymous': 'always',
                'named': 'never',
                'asyncArrow': 'always'
            }
        ],
        'space-in-parens': [
            'error',
            'never'
        ],
        'space-infix-ops': 'error',
        'space-unary-ops': [
            'error',
            {
                'nonwords': false,
                'words': true
            }
        ],
        'spaced-comment': [
            'error',
            'always'
        ],
        'strict': 'off',
        'switch-colon-spacing': 'error',
        'symbol-description': 'error',
        'template-curly-spacing': [
            'error',
            'never'
        ],
        'template-tag-spacing': 'error',
        'unicode-bom': [
            'error',
            'never'
        ],
        'valid-jsdoc': 'off',
        'vars-on-top': 'error',
        'wrap-iife': 'error',
        'wrap-regex': 'error',
        'yield-star-spacing': 'error',
        'yoda': [
            'error',
            'never'
        ]
    },
    'overrides': [
        {
            'files': '.js',
            'rules': {
                'quotes': [
                    'error',
                    'single'
                ]
            }
        }
    ]
};
