#!/usr/bin/env node

/**
 * This script take index.html from `build` directory,
 * replace <script type="text/javascript" data-anchor="common-variables">
 * with proper content:
 * window.__FINEOS_CONFIG_URL = --config-url;
 * window.__FINEOS_DEFAULT_CONFIG = FileContent(--default-config);
 *
 * and then put it to --target-location as a error.html
 */

const path = require('path');
const fs = require('fs-extra');

function readParam(paramName) {
  const args = process.argv.slice(1);
  const passedParamName = `--${paramName}=`;

  for (const arg of args) {
    if (arg.startsWith(passedParamName)) {
      return arg.substr(passedParamName.length);
    }
  }

  throw new Error(`${passedParamName} not provided`);
}

const BUILD_LOCATION = path.join(__filename, '..', '..', 'build');

const COMMON_VARIABLES_START = `<script type="text/javascript" data-anchor="common-variables">`;
const COMMON_VARIABLES_END = `</script>`;

async function deploy(targetLocation, configUrl, defaultConfigLocation) {
  const defaultConfig = await fs.readJson(defaultConfigLocation);
  let indexFileContent = await fs.readFile(path.join(BUILD_LOCATION, 'index.html'), 'utf-8');

  const configUrlStartPos = indexFileContent.indexOf(COMMON_VARIABLES_START);
  const configUrlEndPos = indexFileContent.indexOf(COMMON_VARIABLES_END, configUrlStartPos);

  indexFileContent = indexFileContent.slice(0, configUrlStartPos)
    + COMMON_VARIABLES_START
    + `window.__FINEOS_CONFIG_URL=${JSON.stringify(configUrl)};`
    + `window.__FINEOS_DEFAULT_CONFIG=${JSON.stringify(defaultConfig)}`
    + COMMON_VARIABLES_END
    + indexFileContent.slice(configUrlEndPos);

  await fs.writeFile(path.join(targetLocation, 'error.html'), indexFileContent, 'utf-8');

  await fs.copy(path.join(BUILD_LOCATION, 'static'), path.join(targetLocation, 'static'));
}

const targetLocation = readParam('target-location');
const configUrl = readParam('config-url');
const defaultConfigLocation = readParam('default-config');

deploy(targetLocation, configUrl, defaultConfigLocation);
