#!/usr/bin/env node

/**
 * This script take en.json from `build` directory
 * and en.json from employer-portal 
 * join both files into one 
 * and then put it to --target-location as an en.json
 */

const path = require('path');
const fs = require('fs-extra');

function readParam(paramName) {
  const args = process.argv.slice(1);
  const passedParamName = `--${paramName}=`;

  for (const arg of args) {
    if (arg.startsWith(passedParamName)) {
      return arg.substr(passedParamName.length);
    }
  }

  throw new Error(`${passedParamName} not provided`);
}

const BUILD_LOCATION = path.join(__filename, '..', '..', 'build');

async function deploy(targetLocation, employerLocation, portalConfigLocation) {
  fs.existsSync(path.join(targetLocation)) && fs.rmdirSync(path.join(targetLocation), { recursive: true });
  !fs.existsSync(path.join(targetLocation)) && fs.mkdirSync(path.join(targetLocation), { recursive: true });
  const portalConfig = await fs.readJson(`${portalConfigLocation}`);

  for (const supportedLanguage of portalConfig.supportedLanguages) {
    const fineosConfig = await fs.readJson(`${BUILD_LOCATION}/../src/lib/i18n/locale-data/${supportedLanguage}.json`);
    const employerConfig = await fs.readJson(`${employerLocation}/${supportedLanguage}.json`);
    const result = Object.assign({}, employerConfig, fineosConfig); 

    await fs.writeJSON(path.join(targetLocation, `${supportedLanguage}.json`), result, 'utf-8');
  }
}

const targetLocation = readParam('target-location');
const employerLocation = readParam('employer-location');
const portalConfigLocation = readParam('portal-config-location');

deploy(targetLocation, employerLocation, portalConfigLocation);
