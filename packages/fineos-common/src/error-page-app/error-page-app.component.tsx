/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import { IntlProvider } from 'react-intl';
import { HashRouter } from 'react-router-dom';
import axios, { AxiosError } from 'axios';

import { ErrorPage, loadThemeFonts, Theme, ThemeModel } from '../lib/ui';
import { flattenMessages } from '../lib/i18n';

const fetchConfig = () => {
  const global = window as any;
  const CONFIG_URL = global.__FINEOS_CONFIG_URL;
  const DEFAULT_CONFIG = global.__FINEOS_DEFAULT_CONFIG;

  return fetch(CONFIG_URL)
    .then(response => response.json())
    .catch(e => DEFAULT_CONFIG)
    .then(config => {
      loadThemeFonts(config.theme, document.head, {
        display: 'swap'
      });

      if (config.theme.header.title) {
        document.title = config.theme.header.title;
      }

      return config;
    });
};

type Config = {
  theme: ThemeModel;
};

const fetchPortalEnvConfig = async (): Promise<any> => {
  try {
    const { data } = await axios.get<any>('/config/portal.env.json');

    return data;
  } catch (error) {
    const { response } = error as AxiosError;
    return Promise.reject(response && response.data);
  }
};

const fetchLocaleData = async (
  locale: string
): Promise<Record<string, string>> => {
  try {
    const { data } = await axios.get<Record<string, string>>(
      `/assets/local/${locale}.json`
    );

    return data;
  } catch (error) {
    const { response } = error as AxiosError;
    return Promise.reject(response && response.data);
  }
};

export const ErrorPageApp = () => {
  const [config, setConfig] = useState<Config | null>(null);
  const [messages, setMessages] = useState<any>();
  const [language, setLanguage] = useState<string>('');

  useEffect(() => {
    fetchConfig().then(setConfig);
  }, []);

  useEffect(() => {
    const getLanguage = async () => {
      const tempLanguage = await fetchPortalEnvConfig();
      setLanguage(tempLanguage.supportedLanguages[0]);
      const tempMessages = await fetchLocaleData(
        tempLanguage.supportedLanguages[0]
      );
      setMessages(tempMessages);
    };
    getLanguage();
  }, [language]);

  return (
    config && (
      <Theme.Provider value={config.theme}>
        <IntlProvider locale={language} messages={flattenMessages(messages)}>
          <HashRouter>
            <ErrorPage />
          </HashRouter>
        </IntlProvider>
      </Theme.Provider>
    )
  );
};
