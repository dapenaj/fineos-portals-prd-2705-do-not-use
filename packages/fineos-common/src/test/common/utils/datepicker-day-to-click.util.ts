export const getDatepickerDayToClick = () => {
  const dayOfMonth = new Date().getDate();
  return (dayOfMonth === 1 ? dayOfMonth + 1 : dayOfMonth - 1).toString();
};
