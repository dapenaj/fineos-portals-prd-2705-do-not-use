/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import userEvent from '@testing-library/user-event';
import { render, screen } from '@testing-library/react';
import { wait } from '@testing-library/dom';

import { HighContrastModeMenu } from '../../../lib/ui/header/high-contrast-mode-menu';
import { ContextWrapper } from '../../common/utils';

describe('HighContrastModeMenu', () => {
  const mockTranslations = {
    'FINEOS_COMMON.HIGH_CONTRAST_MODE.SWITCH_LABEL': 'turn on contrast mode',
    'FINEOS_COMMON.HIGH_CONTRAST_MODE.DESCRIPTION': 'test description'
  };

  test('should render', () => {
    render(
      <ContextWrapper translations={mockTranslations}>
        <HighContrastModeMenu
          userId="testId"
          isContrastModeEnabled={false}
          onContrastModeChange={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByLabelText('FINEOS_COMMON.HIGH_CONTRAST_MODE.MENU_TOGGLE_ICON')
    );

    expect(screen.getByTestId('switch')).toBeInTheDocument();
    expect(
      screen.getByText(
        mockTranslations['FINEOS_COMMON.HIGH_CONTRAST_MODE.SWITCH_LABEL']
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        mockTranslations['FINEOS_COMMON.HIGH_CONTRAST_MODE.DESCRIPTION']
      )
    ).toBeInTheDocument();
  });

  test('should render switch input with proper value checked', () => {
    const { rerender } = render(
      <ContextWrapper>
        <HighContrastModeMenu
          userId="testId"
          isContrastModeEnabled={false}
          onContrastModeChange={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByLabelText('FINEOS_COMMON.HIGH_CONTRAST_MODE.MENU_TOGGLE_ICON')
    );

    expect(screen.getByTestId('switch')).toHaveAttribute(
      'aria-checked',
      'false'
    );

    rerender(
      <ContextWrapper>
        <HighContrastModeMenu
          userId="testId"
          isContrastModeEnabled={true}
          onContrastModeChange={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByLabelText('FINEOS_COMMON.HIGH_CONTRAST_MODE.MENU_TOGGLE_ICON')
    );

    expect(screen.getByTestId('switch')).toHaveAttribute(
      'aria-checked',
      'true'
    );
  });

  test('should call onContrastModeChange', () => {
    const mockOnContrastModeChange = jest.fn();
    render(
      <ContextWrapper>
        <HighContrastModeMenu
          userId="testId"
          isContrastModeEnabled={false}
          onContrastModeChange={mockOnContrastModeChange}
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByLabelText('FINEOS_COMMON.HIGH_CONTRAST_MODE.MENU_TOGGLE_ICON')
    );
    userEvent.click(screen.getByTestId('switch'));

    expect(mockOnContrastModeChange).toHaveBeenCalledTimes(2);
  });

  test('should correctly handle localStorage', async () => {
    const mockUserId = 'testId';
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(() => null),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null)
      },
      writable: true
    });

    const MockWrapperComponent = () => {
      const [isEnabled, setIsEnabled] = useState(false);
      return (
        <ContextWrapper>
          <HighContrastModeMenu
            userId={mockUserId}
            isContrastModeEnabled={isEnabled}
            onContrastModeChange={() => setIsEnabled(enabled => !enabled)}
          />
        </ContextWrapper>
      );
    };

    render(<MockWrapperComponent />);

    userEvent.click(
      screen.getByLabelText('FINEOS_COMMON.HIGH_CONTRAST_MODE.MENU_TOGGLE_ICON')
    );

    await wait();

    expect(window.localStorage.getItem).toHaveBeenCalled();

    userEvent.click(screen.getByTestId('switch'));

    await wait();

    expect(window.localStorage.setItem).toHaveBeenCalled();

    userEvent.click(screen.getByTestId('switch'));

    await wait();

    expect(window.localStorage.removeItem).toHaveBeenCalled();
  });
});
