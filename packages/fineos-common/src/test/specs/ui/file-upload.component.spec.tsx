/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { fireEvent, act, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { IntlProvider } from 'react-intl';

import { render } from '../../config/custom-render';
import { FileUpload, UploadedFile } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme } from '../../../lib/ui/theme';

const uploadFile = async (
  input: HTMLElement,
  file: File = new File(['(⌐□_□)'], 'star-wars.png', { type: 'image/png' })
) =>
  await act(async () => {
    Object.defineProperty(input, 'files', {
      value: [file]
    });

    fireEvent.change(input);
    // loading the file
    await wait();
    // reading the file
    return wait();
  });

describe('FileUpload', () => {
  const translations = {
    'FINEOS_COMMON.UPLOAD.DRAG_LABEL': 'Drag and drop',
    'FINEOS_COMMON.UPLOAD.OR': 'or',
    'FINEOS_COMMON.UPLOAD.BROWSE_FILES': 'Browse Files',
    'FINEOS_COMMON.UPLOAD.VALID_EXTENSIONS': 'Valid Extensions',
    'FINEOS_COMMON.UPLOAD.MAX_FILE_SIZE': 'Max file size',
    'FINEOS_COMMON.UPLOAD.UPLOAD_BUTTON': 'Upload Document',
    'FINEOS_COMMON.UPLOAD.CANCEL': 'Cancel',
    'FINEOS_COMMON.VALIDATION.FILE_EXTENSION': 'File extension invalid',
    'FINEOS_COMMON.VALIDATION.MAX_FILE_SIZE':
      'Too big {fileSize} file, should be less than {maxFileSize}',
    'FINEOS_COMMON.VALIDATION.ADDITIONAL_INFORMATION_VALIDATION': ''
  } as Record<string, string>;

  const mock = jest.fn();

  const props = {
    accept: ['.txt', '.png'],
    isLoading: false,
    maxFileSize: '1mb',
    onCancel: () => mock,
    onSubmit: (data: UploadedFile) => mock
  };

  const getFileUpload = (extraProps?: any) => (
    <Theme.Provider value={mockedTheme}>
      <IntlProvider locale="en" messages={translations}>
        <FileUpload {...props} {...extraProps}>
          star-wars
        </FileUpload>
      </IntlProvider>
    </Theme.Provider>
  );

  test('render', () => {
    const { getByTestId, getByText } = render(getFileUpload());

    expect(getByTestId('file-upload')).toBeInTheDocument();
    expect(getByText('Valid Extensions')).toBeInTheDocument();
    expect(getByText('Upload Document')).toBeInTheDocument();
    expect(getByText('Cancel')).toBeInTheDocument();

    expect(getByText('Upload Document')).toBeDisabled();
  });

  xtest('should upload a file', async () => {
    const { getByType, getByText } = render(getFileUpload());

    const input = getByType('file');
    const uploadBtn = getByText('Upload Document');

    await uploadFile(input);

    expect(uploadBtn).not.toBeDisabled();
    expect(getByText('star-wars')).toBeInTheDocument();

    await act(() => {
      userEvent.click(uploadBtn);
      return wait();
    });

    expect(uploadBtn).toBeDisabled();
  });

  test('should not allow an incorrect file type to be uploaded', async () => {
    const { getByType, getByText } = render(getFileUpload());

    const file = new File(['(⌐□_□)'], 'star-wars.xx', { type: 'unknown' });
    const input = getByType('file');
    const uploadBtn = getByText('Upload Document');

    await uploadFile(input, file);

    await act(wait);

    expect(uploadBtn).toBeDisabled();
    expect(getByText('File extension invalid')).toBeInTheDocument();
  });

  test('should not allow file with size bigger than 5MB to be uploaded even if limit higher', async () => {
    const { getByType, getByText } = render(
      getFileUpload({
        maxFileSize: '100mb'
      })
    );

    const file = new File(
      [Array.prototype.fill.call(new Array(1024 ** 2 * 6), '1').join('')],
      'star-wars.png',
      { type: 'image/png' }
    );
    const input = getByType('file');
    const uploadBtn = getByText('Upload Document');

    await uploadFile(input, file);

    await act(wait);

    expect(uploadBtn).toBeDisabled();
    expect(
      getByText('Too big 6 MB file, should be less than 5 MB')
    ).toBeInTheDocument();
  });

  test('should not allow file with the size bigger than custom limit to be uploaded', async () => {
    const { getByType, getByText } = render(
      getFileUpload({
        maxFileSize: '1mb'
      })
    );

    const file = new File(
      [Array.prototype.fill.call(new Array(1024 ** 2 + 1), '1').join('')],
      'star-wars.png',
      { type: 'image/png' }
    );
    const input = getByType('file');
    const uploadBtn = getByText('Upload Document');

    await uploadFile(input, file);

    await act(wait);

    expect(uploadBtn).toBeDisabled();
    expect(
      getByText('Too big 1 MB file, should be less than 1 MB')
    ).toBeInTheDocument();
  });

  // @TODO: this test randomly fails
  xtest('removing a file from the upload list', async () => {
    const { getByType, getByText, container } = render(getFileUpload());

    const input = getByType('file');
    const uploadBtn = getByText('Upload Document');

    await uploadFile(input);

    expect(getByText('star-wars')).toBeInTheDocument();

    await act(() => {
      userEvent.click(container.querySelector('.anticon-delete')!);
      return wait();
    });

    expect(uploadBtn).toBeDisabled();
  });
});
