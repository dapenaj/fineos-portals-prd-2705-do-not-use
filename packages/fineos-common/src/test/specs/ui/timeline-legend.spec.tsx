/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { TimelineLegend } from '../../../lib/ui/timeline-legend';
import { Theme } from '../../../lib/ui/theme';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('TimelineLegend', () => {
  test('render', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider
          locale="en"
          messages={
            {
              'FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR':
                'LEGEND_COLOR',
              'FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_APPROVED':
                'LEGEND_COLOR_APPROVED',
              'FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_PENDING':
                'LEGEND_COLOR_PENDING',
              'FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_DECLINED':
                'LEGEND_COLOR_DECLINED',
              'FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_CANCELLED':
                'LEGEND_COLOR_CANCELLED',
              'FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_GRAPHIC_PATTERN':
                'LEGEND_GRAPHIC_PATTERN',
              'FINEOS_COMMON.NOTIFICATION.CONTINUOUS_TIME': 'CONTINUOUS_TIME',
              'FINEOS_COMMON.NOTIFICATION.INTERMITTENT_TIME':
                'INTERMITTENT_TIME',
              'FINEOS_COMMON.NOTIFICATION.REDUCED_SCHEDULE': 'REDUCED_SCHEDULE'
            } as Record<string, string>
          }
        >
          <TimelineLegend />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByText('LEGEND_COLOR')).toBeInTheDocument();
    expect(screen.getByText('LEGEND_GRAPHIC_PATTERN')).toBeInTheDocument();
  });
});
