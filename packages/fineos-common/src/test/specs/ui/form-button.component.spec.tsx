/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen } from '@testing-library/react';
import { Formik, Form, Field, FormikHelpers, FormikValues } from 'formik';
import * as Yup from 'yup';
import userEvent from '@testing-library/user-event';
import { wait } from '@testing-library/dom';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme, FormButton, FormGroup, TextInput } from '../../../lib/ui';
import { ButtonType } from '../../../lib/types/button.type';

describe('FormButton', () => {
  const locale = {} as Record<string, string>;
  const validationSchema = Yup.object().shape({
    test: Yup.string().min(3, 'Input too short')
  });
  test('renders', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <Form>
              <FormGroup label={<p>label</p>}>
                <Field name="test" component={TextInput} />
              </FormGroup>
              <FormButton htmlType="submit">test</FormButton>
            </Form>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('action-button')).toBeInTheDocument();
    expect(screen.getByTestId('action-button').textContent).toBe('test');
  });

  test('assign correct class for various button types', () => {
    const buttonTypes = [
      ButtonType.PRIMARY,
      ButtonType.LINK,
      ButtonType.GHOST,
      ButtonType.DANGER
    ];
    const { rerender } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <Form>
              <FormGroup label={<p>label</p>}>
                <Field name="test" component={TextInput} />
              </FormGroup>
              <FormButton htmlType="submit" />
            </Form>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('action-button').className).toContain(
      ButtonType.PRIMARY.toLowerCase()
    );

    buttonTypes.forEach(buttonType => {
      rerender(
        <Theme.Provider value={mockedTheme}>
          <IntlProvider locale="en" messages={locale}>
            <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
              <Form>
                <FormGroup label={<p>label</p>}>
                  <Field name="test" component={TextInput} />
                </FormGroup>
                <FormButton buttonType={buttonType} htmlType="submit" />
              </Form>
            </Formik>
          </IntlProvider>
        </Theme.Provider>
      );
      expect(screen.getByTestId('action-button').className).toContain(
        buttonType.toLowerCase()
      );
    });
  });

  test('integrate with formik validation', async () => {
    const submitForm = jest.fn();
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik
            initialValues={{ test: 'a' }}
            onSubmit={submitForm}
            validationSchema={validationSchema}
          >
            <Form>
              <FormGroup label={<p>label</p>}>
                <Field name="test" component={TextInput} />
              </FormGroup>
              <FormButton htmlType="submit" />
            </Form>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );
    userEvent.click(screen.getByTestId('action-button'));

    await act(wait);

    expect(screen.getByText('Input too short')).toBeInTheDocument();
    expect(screen.getByTestId('action-button')).not.toHaveAttribute('disabled');
  });

  test('reset form values', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik
            initialValues={{ test: '' }}
            onSubmit={jest.fn()}
            validationSchema={validationSchema}
          >
            <Form>
              <FormGroup label={<p>label</p>}>
                <Field name="test" component={TextInput} />
              </FormGroup>
              <FormButton htmlType="reset" />
            </Form>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    userEvent.type(screen.getByTestId('text-input'), 'test');
    expect(screen.getByTestId('text-input')).toHaveValue('test');

    await act(wait);

    userEvent.click(screen.getByTestId('action-button'));
    expect(screen.getByTestId('text-input')).toHaveValue('');
  });

  test('should show spinner', async () => {
    const mockHandleSubmit = (
      values: FormikValues,
      helpers: FormikHelpers<any>
    ) => {
      helpers.setSubmitting(true);
    };

    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={mockHandleSubmit}>
            <Form>
              <FormGroup label={<p>label</p>}>
                <Field name="test" component={TextInput} />
              </FormGroup>
              <FormButton htmlType="submit" />
            </Form>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );
    userEvent.type(screen.getByTestId('text-input'), 'test');
    userEvent.click(screen.getByTestId('action-button'));

    await act(wait);

    expect(document.querySelector('.ant-btn-loading-icon')).toBeInTheDocument();
  });
});
