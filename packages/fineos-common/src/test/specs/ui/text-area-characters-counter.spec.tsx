import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { Formik } from 'formik';

import { Theme, TextAreaCharactersCounter } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('TextAreaCharactersCounter', () => {
  const maxLength = 1000;
  const mockLocales = {
    'FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS': `${maxLength} left`,
    'FINEOS_COMMON.TEXT_AREA.CUSTOM_REMAINING_CHARACTERS': `custom ${maxLength} left`
  } as Record<string, string>;

  test('should render', () => {
    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Theme.Provider value={mockedTheme}>
          <IntlProvider locale="en" messages={mockLocales}>
            <TextAreaCharactersCounter fieldName="test" maxLength={maxLength} />
          </IntlProvider>
        </Theme.Provider>
      </Formik>
    );

    expect(screen.getByText(`${maxLength} left`)).toBeInTheDocument();
  });

  test('should render custom message', () => {
    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Theme.Provider value={mockedTheme}>
          <IntlProvider locale="en" messages={mockLocales}>
            <TextAreaCharactersCounter
              fieldName="test"
              maxLength={maxLength}
              messageTranslationId="FINEOS_COMMON.TEXT_AREA.CUSTOM_REMAINING_CHARACTERS"
            />
          </IntlProvider>
        </Theme.Provider>
      </Formik>
    );

    expect(screen.getByText(`custom ${maxLength} left`)).toBeInTheDocument();
  });
});
