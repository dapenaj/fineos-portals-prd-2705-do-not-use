/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { SomethingWentWrong } from '../../../../lib/ui/something-went-wrong';
import { Theme } from '../../../../lib/ui/theme';
import { mockedTheme } from '../../../fixture/mocked-theme';

describe('SomethingWentWrong', () => {
  const locales = {
    'FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE': 'Something went wrong',
    'FINEOS_COMMON.SOMETHING_WENT_WRONG.MESSAGE': 'MESSAGE'
  } as Record<string, string>;
  test('render', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locales}>
          <SomethingWentWrong />
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('something-went-wrong')).toBeInTheDocument();
    expect(screen.getByTestId('something-went-wrong').textContent).toContain(
      'Something went wrong'
    );
    expect(
      screen.getByTestId('something-went-wrong-image')
    ).toBeInTheDocument();
  });

  test('should render with overwritten text', () => {
    const locale = {
      'FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE': 'Different title',
      'FINEOS_COMMON.SOMETHING_WENT_WRONG.MESSAGE': 'Different message'
    } as Record<string, string>;

    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <SomethingWentWrong />
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('something-went-wrong')).toBeInTheDocument();
    expect(screen.getByTestId('something-went-wrong').textContent).toContain(
      'Different title'
    );
    expect(screen.getByTestId('something-went-wrong').textContent).toContain(
      'Different message'
    );
    expect(
      screen.getByTestId('something-went-wrong-image')
    ).toBeInTheDocument();
  });

  test('should render without an image', () => {
    const { rerender } = render(
      <Theme.Provider value={{ ...mockedTheme, images: {} }}>
        <IntlProvider locale="en" messages={locales}>
          <SomethingWentWrong />
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('something-went-wrong')).toBeInTheDocument();
    expect(
      screen.queryByTestId('something-went-wrong-image')
    ).not.toBeInTheDocument();

    rerender(
      <Theme.Provider value={{ ...mockedTheme, images: undefined }}>
        <IntlProvider locale="en" messages={locales}>
          <SomethingWentWrong />
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('something-went-wrong')).toBeInTheDocument();
    expect(
      screen.queryByTestId('something-went-wrong-image')
    ).not.toBeInTheDocument();
  });
});
