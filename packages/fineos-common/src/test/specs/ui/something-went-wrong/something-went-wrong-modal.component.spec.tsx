/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  render,
  within,
  fireEvent,
  screen,
  act,
  wait
} from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../../fixture/mocked-theme';
import { SomethingWentWrongModal } from '../../../../lib/ui/something-went-wrong';
import { Theme } from '../../../../lib/ui/theme';
import { waitModalClosed } from '../../../common/utils';

describe('SomethingWentWrongModal', () => {
  const locale = {
    'FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE': 'Modal title',
    'FINEOS_COMMON.SOMETHING_WENT_WRONG.MESSAGE': 'Modal message',
    'FINEOS_COMMON.GENERAL.CLOSE': 'Close',
    'FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL': 'MODAL_CLOSE_ICON_LABEL'
  } as Record<string, string>;

  test('should not render content in modal if no error', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <SomethingWentWrongModal response={null}>
            Test content
          </SomethingWentWrongModal>
        </IntlProvider>
      </Theme.Provider>
    );

    await act(wait);

    expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
    expect(screen.queryByText('Test content')).not.toBeInTheDocument();
  });

  test('should render content in modal if error provided', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <SomethingWentWrongModal response={new Error('error')}>
            Test content
          </SomethingWentWrongModal>
        </IntlProvider>
      </Theme.Provider>
    );

    await act(wait);

    expect(
      within(screen.getByRole('dialog')).getByText('Test content')
    ).toBeInTheDocument();
  });

  test('should render default title and message', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <SomethingWentWrongModal response={new Error('error')} />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(getByText('Modal title')).toBeInTheDocument();
    expect(getByText('Modal message')).toBeInTheDocument();
  });

  test('should not render apiFail image if not provided', () => {
    const { container } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <SomethingWentWrongModal response={new Error('error')}>
            Test content
          </SomethingWentWrongModal>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(
      container.querySelector(
        `img[src="${mockedTheme.images?.somethingWentWrongColor}"]`
      )
    ).not.toBeInTheDocument();
  });

  test('should close modal after clicking close', async () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <SomethingWentWrongModal response={new Error('error')}>
            Test content
          </SomethingWentWrongModal>
        </IntlProvider>
      </Theme.Provider>
    );

    fireEvent.click(getByText('Close'));

    await waitModalClosed();
  });
});
