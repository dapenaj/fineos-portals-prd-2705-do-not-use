/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';
import { IntlProvider } from 'react-intl';

import { RichFormattedMessage } from '../../../lib/ui/rich-formatted-message';
import { Theme } from '../../../lib/ui/theme';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('RichFormattedMessage', () => {
  test('render', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider
          locale="en"
          messages={
            {
              TEST: 'Normal <bold>bold</bold> {start} {end}'
            } as Record<string, string>
          }
        >
          <RichFormattedMessage
            id="TEST"
            values={{
              start: moment('2020-01-01'),
              end: moment('2020-01-02')
            }}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    const normal = getByText(/Normal/i);
    const bold = getByText(/bold/i);
    const start = getByText(/Jan 1, 2020/i);
    const end = getByText(/Jan 2, 2020/i);

    expect(normal).toBeInTheDocument();
    expect(bold).toBeInTheDocument();
    expect(start).toBeInTheDocument();
    expect(end).toBeInTheDocument();
  });

  test('render telLink', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider
          locale="en"
          messages={
            {
              TEL: '<tel>01-123-45678</tel>'
            } as Record<string, string>
          }
        >
          <RichFormattedMessage id="TEL" />
        </IntlProvider>
      </Theme.Provider>
    );

    const tel = getByText(/01-123-45678/i);
    expect(tel).toBeInTheDocument();
  });

  test('render emailLink', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider
          locale="en"
          messages={
            {
              EMAIL: '<email>test@test.com</email>'
            } as Record<string, string>
          }
        >
          <RichFormattedMessage id="EMAIL" />
        </IntlProvider>
      </Theme.Provider>
    );

    const email = getByText(/test@test.com/i);
    expect(email).toBeInTheDocument();
  });
});
