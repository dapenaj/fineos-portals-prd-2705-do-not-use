/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { FormattedMessage, Button, Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('FormattedMessage', () => {
  const locale = {
    FORMATTED_MESSAGE_ID: 'FORMATTED_MESSAGE_ID'
  } as Record<string, string>;
  test('should render default formatted message in span', () => {
    const { getByText, getByTestId, container } = render(
      <IntlProvider locale="en" messages={locale}>
        <Theme.Provider value={mockedTheme}>
          <FormattedMessage id="FORMATTED_MESSAGE_ID" />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(container.querySelector('span')).toBeInTheDocument();
    expect(getByText('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
    expect(getByTestId('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
  });

  test('should render formatted message as a div', () => {
    const { getByText, getByTestId, container } = render(
      <IntlProvider locale="en" messages={locale}>
        <Theme.Provider value={mockedTheme}>
          <FormattedMessage id="FORMATTED_MESSAGE_ID" as="div" />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(container.querySelector('span')).not.toBeInTheDocument();
    expect(container.querySelector('div')).toBeInTheDocument();
    expect(getByText('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
    expect(getByTestId('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
  });

  test('should render formatted message as a component', () => {
    const { getByText, getByTestId, container } = render(
      <IntlProvider locale="en" messages={locale}>
        <Theme.Provider value={mockedTheme}>
          <FormattedMessage id="FORMATTED_MESSAGE_ID" as={Button} />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(container.querySelector('span')).toBeInTheDocument();
    expect(getByText('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
    expect(getByTestId('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
  });

  test('should render formatted message with children', () => {
    const { getByText, getByTestId, container } = render(
      <IntlProvider locale="en" messages={locale}>
        <Theme.Provider value={mockedTheme}>
          <FormattedMessage id="FORMATTED_MESSAGE_ID">
            {(extraParams, placeholder) => (
              <div {...extraParams}>{placeholder}</div>
            )}
          </FormattedMessage>
        </Theme.Provider>
      </IntlProvider>
    );

    expect(container.querySelector('div')).toBeInTheDocument();
    expect(getByText('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
    expect(getByTestId('FORMATTED_MESSAGE_ID')).toBeInTheDocument();
  });
});
