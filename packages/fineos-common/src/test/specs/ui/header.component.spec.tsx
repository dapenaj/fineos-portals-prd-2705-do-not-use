/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { NavLink } from 'react-router-dom';

import { Header } from '../../../lib/ui/header';
import { Badge } from '../../../lib/ui/nav-bar';
import { mockedTheme } from '../../fixture/mocked-theme';
import { ContextWrapper } from '../../common/utils';

describe('Header', () => {
  test('should render', () => {
    render(
      <ContextWrapper>
        <Header
          userName="firstName"
          userOptions={[
            <NavLink to="/" exact={true} key={0}>
              HOME
            </NavLink>,
            <NavLink to="/my-messages" exact={true} key={1}>
              MY_MESSAGES
              <Badge>{10}</Badge>
            </NavLink>
          ]}
        />
      </ContextWrapper>
    );
    const element = screen.getByTestId('header');
    expect(element).toBeInTheDocument();
    expect(element.className).toContain('header');

    const elementDropdown = screen.getByTestId('right-bar');
    expect(elementDropdown).toBeInTheDocument();
    expect(elementDropdown.className).toContain('rightBar');

    const elementBranding = screen.getByTestId('left-bar');
    expect(elementBranding).toBeInTheDocument();
    expect(elementBranding.className).toContain('leftBar');

    expect(
      screen.getByAltText(mockedTheme.header.logoAltText!)
    ).toBeInTheDocument();
  });
});
