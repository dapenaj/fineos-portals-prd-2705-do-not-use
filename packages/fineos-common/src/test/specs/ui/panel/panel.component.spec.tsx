/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { Panel, PanelHeader } from '../../../../lib/ui';
import { ContextWrapper } from '../../../common/utils';

describe('Panel', () => {
  const locale = {
    en: {
      TEST_ARIA_LABEL: 'test aria-label'
    }
  };
  test('render', () => {
    render(
      <ContextWrapper>
        <Panel header={<PanelHeader>panel header</PanelHeader>}>
          <div>Short Term Disability</div>
          <div>FinCorp Plan</div>
        </Panel>
      </ContextWrapper>
    );
    const panel = screen.getByTestId('panel');
    expect(panel).toBeInTheDocument();
    expect(panel.className).toContain('wrapper');

    const panelHeader = screen.getByTestId('panel-header');
    expect(panelHeader).toBeInTheDocument();
    expect(panelHeader.className).toContain('wrapper');
  });

  test('should render with accessibility attributes', () => {
    const mockRole = 'region';
    render(
      <ContextWrapper>
        <Panel
          header={<PanelHeader>panel header</PanelHeader>}
          role={mockRole}
          aria-label={locale.en.TEST_ARIA_LABEL}
        >
          <div>Short Term Disability</div>
          <div>FinCorp Plan</div>
        </Panel>
      </ContextWrapper>
    );

    expect(document.querySelector(`[role="${mockRole}"]`)).toBeInTheDocument();
    expect(
      document.querySelector(`[aria-label="${locale.en.TEST_ARIA_LABEL}"]`)
    ).toBeInTheDocument();
  });
});
