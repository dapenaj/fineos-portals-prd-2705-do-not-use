import React from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { IntlProvider } from 'react-intl';

import { DeleteButton } from '../../../lib/ui';
import { Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('DeleteButton', () => {
  test('should render', () => {
    const { getByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <DeleteButton onClick={jest.fn()} />
        </IntlProvider>
      </Theme.Provider>
    );
    expect(getByTestId('delete-button')).toBeInTheDocument();
    expect(document.querySelector('.fa-trash-alt')).toBeInTheDocument();
  });

  test('should call onClick', () => {
    const mockOnClick = jest.fn();
    const { getByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <DeleteButton onClick={mockOnClick} />
        </IntlProvider>
      </Theme.Provider>
    );
    userEvent.click(getByTestId('delete-button'));

    expect(mockOnClick).toHaveBeenCalled();
  });
});
