/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  contrastTextColor,
  primaryDarkColor,
  primaryLight2Color,
  primaryLightColor,
  textStyleToCss
} from '../../../../lib/ui/theme';
import { mockedTheme } from '../../../fixture/mocked-theme';

describe('theme utils', () => {
  test('primaryDarkColor should return primary color with mixed 25% dark', () => {
    expect(primaryDarkColor(mockedTheme.colorSchema)).toBe('#6e2050');
  });

  test('primaryLightColor should return primary color with mixed 50% light', () => {
    expect(primaryLightColor(mockedTheme.colorSchema)).toBe('#c995b5');
  });

  test('primaryLight2Color should return primary color with mixed 90% light', () => {
    expect(primaryLight2Color(mockedTheme.colorSchema)).toBe('#f4eaf0');
  });

  test('contrastTextColor should return black or white depending on contrast to provided color', () => {
    expect(contrastTextColor(mockedTheme, '#f4eaf0')).toBe(
      mockedTheme.colorSchema.neutral9
    );
    expect(contrastTextColor(mockedTheme, '#6e2050')).toBe(
      mockedTheme.colorSchema.neutral1
    );
  });

  test('textStyleToCss should map TextStyle to proper style object', () => {
    expect(
      textStyleToCss({
        fontFamily: '"Roboto"',
        fontStyle: 'italic',
        fontWeight: 'normal',
        fontSize: 11,
        lineHeight: 15,
        textTransform: 'uppercase'
      })
    ).toEqual({
      fontFamily: '"Roboto"',
      fontStyle: 'italic',
      fontWeight: 'normal',
      fontSize: '0.688rem',
      lineHeight: '0.938rem',
      textTransform: 'uppercase'
    });

    expect(
      textStyleToCss({
        fontFamily: '"Roboto"',
        fontStyle: 'italic',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 16
      })
    ).toEqual({
      fontFamily: '"Roboto"',
      fontStyle: 'italic',
      fontWeight: 'normal',
      fontSize: '1rem',
      lineHeight: '1rem'
    });

    expect(
      textStyleToCss(
        {
          fontFamily: '"Roboto"',
          fontStyle: 'italic',
          fontWeight: 'normal',
          fontSize: 16,
          lineHeight: 16
        },
        { suppressLineHeight: true }
      )
    ).toEqual({
      fontFamily: '"Roboto"',
      fontStyle: 'italic',
      fontWeight: 'normal',
      fontSize: '1rem'
    });
  });
});
