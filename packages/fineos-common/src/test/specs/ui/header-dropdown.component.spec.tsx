/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { wait } from '@testing-library/dom';
import { NavLink } from 'react-router-dom';

import { HeaderDropdown } from '../../../lib/ui/header/header-dropdown';
import { ContextWrapper } from '../../common/utils';

describe('HeaderDropdown', () => {
  test('render', () => {
    const { container } = render(
      <ContextWrapper>
        <HeaderDropdown label={<div>label</div>}>
          <NavLink to="/" exact={true}>
            HOME
          </NavLink>
          <NavLink to="/my-messages" exact={true}>
            MY_MESSAGES
          </NavLink>
        </HeaderDropdown>
      </ContextWrapper>
    );

    expect(container.querySelector('.trigger')?.className).toContain('trigger');
  });

  test('should open on enter key', async () => {
    const testContent = 'test content';
    const triggerContent = 'trigger';
    render(
      <ContextWrapper>
        <HeaderDropdown label={<p>{triggerContent}</p>}>
          <p>{testContent}</p>
        </HeaderDropdown>
      </ContextWrapper>
    );

    fireEvent.keyDown(screen.getByText(triggerContent), { key: 'Enter' });

    await wait();

    expect(screen.getByText(testContent)).toBeInTheDocument();
  });

  test('should close on escape key', async () => {
    const testContent = 'test content';
    const triggerContent = 'trigger';
    render(
      <ContextWrapper>
        <HeaderDropdown label={<p>{triggerContent}</p>}>
          <p>{testContent}</p>
        </HeaderDropdown>
      </ContextWrapper>
    );

    fireEvent.keyDown(screen.getByText(triggerContent), { key: 'Enter' });

    await wait();

    expect(screen.getByText(testContent)).toBeInTheDocument();

    fireEvent.keyDown(screen.getByText(triggerContent), { key: 'Escape' });

    await wait();

    // After closing dropdown ant-design is not removing it from tree,
    // instead it adds 'ant-dropdown-hidden' class.
    expect(document.querySelector('.ant-dropdown')?.classList).toContain(
      'ant-dropdown-hidden'
    );
  });
});
