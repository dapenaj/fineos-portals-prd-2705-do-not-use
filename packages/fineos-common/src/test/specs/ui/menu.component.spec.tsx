/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { Button } from 'antd';

import { Menu } from '../../../lib/ui/menu';
import { Badge } from '../../../lib/ui/nav-bar';
import { ButtonType } from '../../../lib/types';

describe('Header', () => {
  test('render', () => {
    const { getByRole, getAllByRole, getByTestId } = render(
      <Menu>
        <Button
          type={ButtonType.PRIMARY}
          block={true}
          data-test-el="action-button"
        >
          HOME
        </Button>
        <Button
          type={ButtonType.PRIMARY}
          block={true}
          data-test-el="action-button"
        >
          MESSAGES
          <Badge data-test-el="badge">{10}</Badge>
        </Button>
      </Menu>
    );
    const element = getByRole('menu');
    expect(element.className).toContain('ant-menu');

    const elements = getAllByRole('menuitem');
    expect(elements.length).toBe(2);

    const badge = getByTestId('badge');
    expect(badge.className).toContain('badge');
  });
});
