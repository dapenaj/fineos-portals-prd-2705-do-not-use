/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Pagination, Theme } from '../../../lib/ui';

describe('Pagination', () => {
  const translations = {
    'FINEOS_COMMON.PAGINATION.PER_PAGE': 'PER_PAGE',
    'FINEOS_COMMON.PAGINATION.PREV_PAGE': 'PREV_PAGE',
    'FINEOS_COMMON.PAGINATION.NEXT_PAGE': 'NEXT_PAGE',
    'FINEOS_COMMON.PAGINATION.PREV_5': 'PREV_5',
    'FINEOS_COMMON.PAGINATION.NEXT_5': 'NEXT_5',
    'FINEOS_COMMON.PAGINATION.PREV_3': 'PREV_3',
    'FINEOS_COMMON.PAGINATION.NEXT_3': 'NEXT_3'
  } as Record<string, string>;

  test('render', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <Pagination />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(queryByTestId('pagination')).toBeInTheDocument();
  });

  test('should render pagination if total more than 10 and hideOnSinglePage set to true', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <Pagination
            pageSize={10}
            total={11}
            hideOnSinglePage={true}
            itemRender={() => 'foo'}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(queryByTestId('pagination')).toBeInTheDocument();
  });

  test('should not render pagination if total equals to 10 and hideOnSinglePage set to true', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <Pagination
            pageSize={10}
            total={10}
            hideOnSinglePage={true}
            itemRender={() => 'foo'}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(queryByTestId('pagination')).not.toBeInTheDocument();
  });

  test('should render pagination if total equals to 10 and hideOnSinglePage set to false', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <Pagination pageSize={10} total={10} itemRender={() => 'foo'} />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(queryByTestId('pagination')).toBeInTheDocument();
  });
});
