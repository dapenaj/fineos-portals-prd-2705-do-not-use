/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { ContentRenderer } from '../../../lib/ui/content-renderer';
import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme } from '../../../lib/ui/theme';

describe('ContentRenderer', () => {
  test('render', () => {
    render(
      <ContentRenderer
        isLoading={false}
        isEmpty={false}
        data-test-el="content-renderer"
      >
        I am content
      </ContentRenderer>
    );

    expect(screen.getByTestId('content-renderer')).toBeInTheDocument();
    expect(screen.getByTestId('content-renderer').textContent).toBe(
      'I am content'
    );
  });

  test('render - isLoading', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <ContentRenderer
          isLoading={true}
          isEmpty={false}
          data-test-el="content-renderer"
        >
          I am content
        </ContentRenderer>
      </Theme.Provider>
    );

    expect(screen.queryByTestId('content-renderer')).not.toBeInTheDocument();
    expect(screen.getByTestId('spinner')).toBeInTheDocument();
  });

  test('render - isEmpty', () => {
    render(
      <ContentRenderer
        isLoading={false}
        isEmpty={true}
        data-test-el="content-renderer"
      >
        I am content
      </ContentRenderer>
    );

    expect(screen.queryByTestId('content-renderer')).not.toBeInTheDocument();
  });

  test('render - isEmpty with emptyContent', () => {
    render(
      <ContentRenderer
        isLoading={false}
        isEmpty={true}
        emptyContent={<div>Empty Content</div>}
      >
        I am content
      </ContentRenderer>
    );

    expect(screen.queryByTestId('content-renderer')).not.toBeInTheDocument();
    expect(screen.getByText(/Empty Content/i)).toBeInTheDocument();
  });

  test('should render SomethingWentWrong by default in case of error', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={{} as Record<string, string>}>
          <ContentRenderer
            isLoading={false}
            isEmpty={true}
            error={new Error('')}
          >
            I am content
          </ContentRenderer>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByTestId('something-went-wrong')).toBeInTheDocument();
  });

  test('should render custom error if provided in case of error', () => {
    render(
      <ContentRenderer
        isLoading={false}
        isEmpty={true}
        error={new Error('')}
        errorContent={<>Some error</>}
      >
        I am content
      </ContentRenderer>
    );

    expect(screen.getByText('Some error')).toBeInTheDocument();
  });
});
