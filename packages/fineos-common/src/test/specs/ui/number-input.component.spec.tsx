/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme, NumberInput, FormattedMessage } from '../../../lib/ui';

describe('NumberInput', () => {
  const mockInputProps = {
    field: { name: 'test' },
    form: {}
  } as any;
  const mockCustomProps = {
    className: 'mock-number-input',
    minValue: 0,
    maxValue: 100,
    step: 5,
    'data-test-el': 'mock-test-el',
    isDisabled: true
  };
  const translations = {
    TEST: 'test {count}'
  } as Record<string, string>;

  test('render', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <NumberInput {...mockInputProps} />
        </IntlProvider>
      </Theme.Provider>
    );

    const element = queryByTestId('number-input');
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute('name', 'test');
    expect(element).toHaveAttribute('id', 'test');
    expect(element).toHaveAttribute('step', '1');
    expect(element).not.toHaveAttribute('autofocus');
    expect(element).not.toHaveAttribute('disabled');
  });

  test('render correctly with custom props', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <NumberInput name="test" {...mockInputProps} {...mockCustomProps} />
        </IntlProvider>
      </Theme.Provider>
    );
    const element = queryByTestId('mock-test-el');
    expect(element).toHaveAttribute('min', mockCustomProps.minValue.toString());
    expect(element).toHaveAttribute('max', mockCustomProps.maxValue.toString());
    expect(element).toHaveAttribute('step', mockCustomProps.step.toString());
    expect(element).toHaveAttribute('disabled');
  });

  test('render children correctly', () => {
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <NumberInput {...mockInputProps}>
            <p>test content</p>
          </NumberInput>
        </IntlProvider>
      </Theme.Provider>
    );
    expect(queryByText('test content')).toBeInTheDocument();
  });

  test('render with formatted placeholder', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <NumberInput
            {...mockInputProps}
            placeholder={<FormattedMessage id={'TEST'} values={{ count: 1 }} />}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    const element = queryByTestId('number-input');
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute('placeholder', 'test 1');
  });
});
