/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { Input, Radio } from 'antd';
import { Formik, Form, Field } from 'formik';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import {
  Theme,
  FormGroup,
  DatePickerInput,
  FormRadioInput
} from '../../../lib/ui';

const mockProps = {
  label: 'test label',
  labelTooltipContent: 'test tooltip content',
  name: 'test'
};
const mockGroupChild = <Input placeholder="test" data-test-el="mock-input" />;

describe('FormGroup', () => {
  const locale = {
    'FINEOS_COMMON.GENERAL.OPTIONAL_LABEL': '(optional)',
    'FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS': 'remaining characters'
  } as Record<string, string>;

  test('renders', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <Form>
              <FormGroup {...mockProps}>
                <p>test</p>
              </FormGroup>
            </Form>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );
    const element = screen.getByTestId('form-group');
    expect(element).toBeInTheDocument();
    expect(screen.getByTestId('test-label-form-group')).toHaveTextContent(
      mockProps.label
    );
  });

  test('renders children correctly', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup {...mockProps}>{mockGroupChild}</FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );
    expect(screen.getByTestId('mock-input')).toBeInTheDocument();
  });

  test('should render optional label if isOptional provided', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup {...mockProps} isOptional={true}>
              {mockGroupChild}
            </FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(getByText('test label', { exact: false })).toHaveTextContent(
      '(optional)'
    );
  });

  test('should add class to the wrapper for narrow components', () => {
    const { rerender } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup {...mockProps}>{mockGroupChild}</FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByTestId('form-group')).not.toHaveClass('fitWrapper');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup {...mockProps}>
              <Field name="test" component={DatePickerInput} />
            </FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByTestId('form-group')).toHaveClass('fitWrapper');
  });

  test('should render text length counter', () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup
              {...mockProps}
              counterMaxLength={10}
              immediateErrorMessages={['test']}
            >
              {mockGroupChild}
            </FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(
      screen.getByTestId('remaining-characters-counter')
    ).toBeInTheDocument();
  });

  test('should render fieldset and legend tags if has fieldset component as child', () => {
    const { rerender } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup
              {...mockProps}
              counterMaxLength={10}
              immediateErrorMessages={['test']}
            >
              <Field name="test" component={FormRadioInput}>
                <Radio>1</Radio>
                <Radio>2</Radio>
              </Field>
            </FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(document.querySelector('fieldset')).toBeInTheDocument();
    expect(document.querySelector('legend')).toBeInTheDocument();

    rerender(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <FormGroup
              {...mockProps}
              counterMaxLength={10}
              immediateErrorMessages={['test']}
            >
              {mockGroupChild}
            </FormGroup>
          </Formik>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(document.querySelector('fieldset')).not.toBeInTheDocument();
    expect(document.querySelector('legend')).not.toBeInTheDocument();
  });
});
