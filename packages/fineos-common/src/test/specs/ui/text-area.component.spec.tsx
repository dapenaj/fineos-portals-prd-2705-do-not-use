/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { Theme, TextArea } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

const mockInputProps = {
  field: { name: 'test' },
  form: {}
} as any;

describe('TextArea', () => {
  test('render', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={mockInputProps}>
          <TextArea {...mockInputProps} />
        </IntlProvider>
      </Theme.Provider>
    );
    const element = queryByTestId('text-area');
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute('id', 'test');
    expect(element).toHaveAttribute('name', 'test');
    expect(element).toHaveAttribute('data-test-el', 'text-area');
    expect(element).not.toHaveAttribute('disabled');
  });

  test('render correctly with custom props', () => {
    const { queryByTestId, queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={mockInputProps}>
          <TextArea
            {...mockInputProps}
            isDisabled={true}
            className="mock-text-area"
          >
            <p>test</p>
          </TextArea>
        </IntlProvider>
      </Theme.Provider>
    );
    const element = queryByTestId('text-area');
    const child = queryByText('test');
    expect(element).toHaveAttribute('disabled');
    expect(element).toHaveClass('mock-text-area');
    expect(child).toBeInTheDocument();
  });
});
