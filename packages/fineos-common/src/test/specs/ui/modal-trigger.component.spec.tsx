/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, render, screen, wait } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import userEvent from '@testing-library/user-event';

import { ModalTrigger } from '../../../lib/ui';
import { Theme } from '../../../lib/ui/theme';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('ModalTrigger', () => {
  test('render', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={{} as Record<string, string>}>
          <ModalTrigger
            ariaLabelId="test"
            modalType="modal"
            modalBody={<div>modalBody</div>}
            header={<div>header</div>}
          >
            <button data-test-el="trigger" id="trigger">
              trigger
            </button>
          </ModalTrigger>
        </IntlProvider>
      </Theme.Provider>
    );

    await act(wait);

    userEvent.click(screen.getByTestId('trigger'));
    await act(wait);

    expect(screen.getByTestId('modal')).toBeInTheDocument();
  });
});
