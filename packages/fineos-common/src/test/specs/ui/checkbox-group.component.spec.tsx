/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Checkbox } from 'antd';
import { Formik, Form, Field } from 'formik';

import { ContextWrapper, releaseEventLoop } from '../../common/utils';
import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme, CheckboxGroup } from '../../../lib/ui';
import { FormGroupContext } from '../../../lib/utils';

const mockFieldProps = {
  field: { name: 'test', value: ['test'] },
  form: {}
} as any;

describe('Checkbox input', () => {
  const translations = {
    'FINEOS_COMMON.GENERAL.SELECT_ALL': 'FINEOS_COMMON.GENERAL.SELECT_ALL'
  } as Record<string, string>;

  test('should render', () => {
    const { queryByTestId, queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <FormGroupContext.Provider
          value={{
            disableFeedback: jest.fn()
          }}
        >
          <CheckboxGroup {...mockFieldProps}>
            <Checkbox value="test1">test1</Checkbox>
          </CheckboxGroup>
        </FormGroupContext.Provider>
      </Theme.Provider>
    );
    expect(queryByTestId('checkbox-group')).toBeInTheDocument();
    expect(queryByTestId('checkbox')).toBeInTheDocument();
    expect(queryByText('test1')).toBeInTheDocument();
    expect(
      queryByText('FINEOS_COMMON.GENERAL.SELECT_ALL')
    ).not.toBeInTheDocument();
  });

  test('should render group with custom props', () => {
    const { queryByTestId, queryAllByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <FormGroupContext.Provider
          value={{
            disableFeedback: jest.fn()
          }}
        >
          <CheckboxGroup {...mockFieldProps} isDisabled={true}>
            <Checkbox value="test1" disabled={true}>
              test1
            </Checkbox>
            <Checkbox value="test2">test2</Checkbox>
          </CheckboxGroup>
        </FormGroupContext.Provider>
      </Theme.Provider>
    );
    expect(queryByTestId('checkbox-group')).not.toHaveAttribute('disabled');
    expect(queryAllByTestId('checkbox')[0]).toHaveAttribute('disabled');
    expect(queryAllByTestId('checkbox')[0]).toHaveAttribute('value', 'test1');
    expect(queryAllByTestId('checkbox')[1]).toHaveAttribute('value', 'test2');
    expect(queryAllByTestId('checkbox').length).toBe(2);
  });

  test('should select and deselect all', async () => {
    const initialValues = { test: [] };
    const { getByText } = render(
      <ContextWrapper translations={translations}>
        <Theme.Provider value={mockedTheme}>
          <FormGroupContext.Provider
            value={{
              disableFeedback: jest.fn()
            }}
          >
            <Formik initialValues={initialValues} onSubmit={jest.fn()}>
              <Form>
                <Field
                  name="test"
                  component={CheckboxGroup}
                  hasSelectAll={true}
                >
                  <Checkbox value="test1">test1</Checkbox>
                  <Checkbox value="test2">test2</Checkbox>
                </Field>
              </Form>
            </Formik>
          </FormGroupContext.Provider>
        </Theme.Provider>
      </ContextWrapper>
    );
    expect(getByText('FINEOS_COMMON.GENERAL.SELECT_ALL')).toBeInTheDocument();

    await act(() => {
      userEvent.click(getByText('FINEOS_COMMON.GENERAL.SELECT_ALL'));
      return releaseEventLoop();
    });

    expect(
      document.querySelectorAll('.ant-checkbox-wrapper-checked').length
    ).toBe(3);

    await act(() => {
      userEvent.click(getByText('FINEOS_COMMON.GENERAL.SELECT_ALL'));
      return releaseEventLoop();
    });

    expect(
      document.querySelectorAll('.ant-checkbox-wrapper-checked').length
    ).toBe(0);
  });
});
