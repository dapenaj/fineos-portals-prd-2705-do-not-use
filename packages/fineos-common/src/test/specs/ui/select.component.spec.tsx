/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme, Select, FormattedMessage } from '../../../lib/ui';

const mockInputProps = {
  field: { name: 'test-dropdown-input' },
  form: {}
} as any;
const mockOptions = [{ value: 'test1', title: 'test1' }];

describe('Dropdown input', () => {
  test('render', () => {
    render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <Select {...mockInputProps} optionElements={mockOptions} />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByTestId('select')).not.toBeDisabled();
  });

  test('render children properly', () => {
    render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <Select {...mockInputProps} optionElements={mockOptions}>
            |<p>test content</p>
          </Select>
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByText('test content')).toBeInTheDocument();
  });

  test('should unwrap placeholder to string', () => {
    render(
      <IntlProvider locale="en" messages={{ test: 'TEST1' }}>
        <Theme.Provider value={mockedTheme}>
          <Select
            {...mockInputProps}
            optionElements={mockOptions}
            isSearchable={true}
            placeholder={<FormattedMessage id="test" />}
          />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByText('TEST1')).toBeInTheDocument();
  });
});
