/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme } from '../../../lib/ui/theme';
import { StatusLabel } from '../../../lib/ui/status-label';
import { StatusType } from '../../../lib/types';

describe('StatusLabel', () => {
  test('render', () => {
    const { getByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel type={StatusType.SUCCESS}>My Status</StatusLabel>
      </Theme.Provider>
    );
    const element = getByTestId('status-label');
    expect(element).toBeInTheDocument();
    expect(element.textContent).toBe('My Status');
  });

  test('render with different type options', () => {
    const { getByTestId, rerender } = render(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel />
      </Theme.Provider>
    );
    const element = getByTestId('status-label');

    expect(element.className).toContain('blank');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel type={StatusType.DANGER} />
      </Theme.Provider>
    );
    expect(element.className).toContain('danger');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel type={StatusType.SUCCESS} />
      </Theme.Provider>
    );
    expect(element.className).toContain('success');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel type={StatusType.WARNING} />
      </Theme.Provider>
    );
    expect(element.className).toContain('warning');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel type={StatusType.NEUTRAL} />
      </Theme.Provider>
    );
    expect(element.className).toContain('neutral');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <StatusLabel type={StatusType.BLANK} />
      </Theme.Provider>
    );
    expect(element.className).toContain('blank');
  });
});
