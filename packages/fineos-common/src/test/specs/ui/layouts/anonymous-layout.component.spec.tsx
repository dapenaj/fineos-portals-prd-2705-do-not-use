/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { AnonymousLayout } from '../../../../lib/ui/layouts/page-layout';
import { ContextWrapper } from '../../../common/utils';

describe('AnonymousLayout', () => {
  test('should render default layout', () => {
    render(
      <ContextWrapper>
        <AnonymousLayout />
      </ContextWrapper>
    );

    expect(screen.getByTestId('header')).toBeInTheDocument();
    expect(screen.getByTestId('left-bar')).toBeInTheDocument();
    expect(screen.getAllByTestId('content-layout').length).toBe(2);
  });

  test('should render with contentHeader', () => {
    render(
      <ContextWrapper>
        <AnonymousLayout contentHeader={<div>Header</div>} />
      </ContextWrapper>
    );

    expect(screen.getByText('Header')).toBeInTheDocument();
  });

  test('should render with sideBar', () => {
    render(
      <ContextWrapper>
        <AnonymousLayout sideBar={<div>Side Bar</div>} />
      </ContextWrapper>
    );

    expect(screen.getByText('Side Bar')).toBeInTheDocument();
  });
});
