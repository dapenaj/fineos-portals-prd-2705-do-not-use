/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';

import { Theme, FormRadioInput, Radio } from '../../../lib/ui';
import { FormGroupContext } from '../../../lib/utils';
import { mockedTheme } from '../../fixture/mocked-theme';

const mockInputProps = {
  field: { name: 'test' },
  form: {}
} as any;

describe('RadioInput', () => {
  test('render', () => {
    const { queryAllByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <FormGroupContext.Provider
          value={{
            disableFeedback: jest.fn()
          }}
        >
          <FormRadioInput {...mockInputProps}>
            <Radio key="0" value="test1" disabled={true}>
              test1
            </Radio>
            <Radio key="1" value="test2">
              test2
            </Radio>
          </FormRadioInput>
        </FormGroupContext.Provider>
      </Theme.Provider>
    );
    const elements = queryAllByTestId('radio-input');
    expect(elements[0]).toHaveAttribute('name', 'test');
    expect(elements[0]).toHaveAttribute('disabled');
    expect(elements[1]).not.toHaveAttribute('disabled');
    expect(elements.length).toBe(2);
  });

  test('render children correcly', () => {
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <FormGroupContext.Provider
          value={{
            disableFeedback: jest.fn()
          }}
        >
          <FormRadioInput {...mockInputProps}>
            <Radio key="0" value="test1">
              test1
            </Radio>
            <Radio key="1" value="test2">
              test2
            </Radio>
          </FormRadioInput>
        </FormGroupContext.Provider>
      </Theme.Provider>
    );
    expect(queryByText('test1')).toBeInTheDocument();
    expect(queryByText('test2')).toBeInTheDocument();
  });
});
