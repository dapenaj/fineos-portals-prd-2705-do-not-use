/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Formik, Field } from 'formik';
import moment from 'moment';
import { IntlProvider } from 'react-intl';

import { DatePickerInput } from '../../../lib/ui/date-picker-input';
import { getDatepickerDayToClick } from '../../common/utils';
import { Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';
import { releaseEventLoop } from '../../common/utils';

const defaultProps = {
  name: 'From',
  isDisabled: true,
  label: 'From',
  validate: (value: any) => {
    if (!Boolean(value)) {
      return 'FINEOS_COMMON.VALIDATION.REQUIRED';
    }
    return undefined;
  }
};
describe('DatePickerInput', () => {
  test('render', () => {
    const { queryByTestId } = render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <Formik initialValues={{}} onSubmit={jest.fn()}>
            <form>
              <Field component={DatePickerInput} {...defaultProps} />
            </form>
          </Formik>
        </Theme.Provider>
      </IntlProvider>
    );
    const element = queryByTestId('date-picker');
    expect(element).toBeInTheDocument();
  });

  test('should call onChange prop', async () => {
    const onChangeSpy = jest.fn();
    const { getAllByText, container } = render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <Formik initialValues={{ test: moment() }} onSubmit={jest.fn()}>
            <form>
              <Field
                component={DatePickerInput}
                name="test"
                onChange={onChangeSpy}
                open={true}
              />
            </form>
          </Formik>
        </Theme.Provider>
      </IntlProvider>
    );

    await act(() => {
      userEvent.click(container.querySelector('input')!);
      return releaseEventLoop();
    });

    await act(() => {
      userEvent.click(getAllByText(getDatepickerDayToClick())[0]);
      return releaseEventLoop();
    });

    expect(onChangeSpy).toHaveBeenCalledTimes(1);
  });
});
