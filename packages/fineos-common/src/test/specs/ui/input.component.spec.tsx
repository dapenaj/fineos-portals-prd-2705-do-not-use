/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { FormattedMessage, Input, Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

const mockInputProps = {
  value: 'mock',
  placeholder: 'mock'
};

describe('input.module', () => {
  describe('Input', () => {
    test('should render', () => {
      render(
        <IntlProvider locale="en">
          <Theme.Provider value={mockedTheme}>
            <Input {...mockInputProps} />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByPlaceholderText('mock')).toHaveValue('mock');
    });

    test('should unwrap placeholder to string', () => {
      render(
        <IntlProvider locale="en" messages={{ test: 'TEST1' }}>
          <Theme.Provider value={mockedTheme}>
            <Input
              {...mockInputProps}
              placeholder={<FormattedMessage id="test" />}
            />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByPlaceholderText('TEST1')).toHaveValue('mock');
    });
  });

  describe('Input.Search', () => {
    test('should render', () => {
      render(
        <IntlProvider locale="en">
          <Theme.Provider value={mockedTheme}>
            <Input.Search {...mockInputProps} />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByPlaceholderText('mock')).toHaveValue('mock');
    });

    test('should unwrap placeholder to string', () => {
      render(
        <IntlProvider locale="en" messages={{ test: 'TEST1' }}>
          <Theme.Provider value={mockedTheme}>
            <Input.Search
              {...mockInputProps}
              placeholder={<FormattedMessage id="test" />}
            />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByPlaceholderText('TEST1')).toHaveValue('mock');
    });
  });
});
