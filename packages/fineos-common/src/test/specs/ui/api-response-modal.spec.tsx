/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { IntlProvider } from 'react-intl';

import { Theme } from '../../../lib/ui/theme';
import { mockedTheme } from '../../fixture/mocked-theme';
import { ApiResponseModal } from '../../../lib/ui';
import { ApiResponseType } from '../../../lib';
import { waitModalClosed } from '../../common/utils';

const locale = {
  'FINEOS_COMMON.GENERAL.CLOSE': 'CLOSE',
  'FINEOS_COMMON.GENERAL.OK': 'OK',
  'FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL': 'MODAL_CLOSE_ICON_LABEL'
} as Record<string, string>;

const renderComponent = (props = {}) => {
  return (
    <Theme.Provider value={mockedTheme}>
      <IntlProvider locale="en" messages={locale}>
        <ApiResponseModal
          response={true}
          title="API_RESPONSE_MODAL_TITLE"
          ariaLabelId="API_RESPONSE_MODAL_TITLE"
          {...props}
        >
          API_RESPONSE_MODAL_BODY
        </ApiResponseModal>
      </IntlProvider>
    </Theme.Provider>
  );
};

describe('ApiResponseModal', () => {
  test('should render', () => {
    render(renderComponent());
    expect(screen.getByTestId('api-response-modal')).toBeInTheDocument();
    expect(screen.getByText('API_RESPONSE_MODAL_TITLE')).toBeInTheDocument();
    expect(screen.getByText('API_RESPONSE_MODAL_BODY')).toBeInTheDocument();
    expect(screen.getByText('CLOSE')).toBeInTheDocument();
    expect(document.querySelector('img')).toBeInTheDocument();
  });

  test('should call onConfirm', async () => {
    const onConfirmSpy = jest.fn();
    render(
      renderComponent({
        onConfirm: onConfirmSpy,
        buttonText: 'OK'
      })
    );

    userEvent.click(screen.getByText('OK'));

    expect(onConfirmSpy).toBeCalled();

    await waitModalClosed();
  });

  test('should close', async () => {
    const onCancelSpy = jest.fn();

    render(
      renderComponent({
        onCancel: onCancelSpy
      })
    );

    userEvent.click(screen.getByText('CLOSE'));

    expect(onCancelSpy).toBeCalled();

    await waitModalClosed();
  });

  test.each([
    [ApiResponseType.FAIL, 'somethingWentWrongColor'],
    [ApiResponseType.SUCCESS, 'greenCheckIntake'],
    [ApiResponseType.UPLOAD_SUCCESS, 'uploadSuccess'],
    [ApiResponseType.UPLOAD_FAIL, 'uploadFail'],
    [ApiResponseType.DATA_CONCURRENCY, 'dataConcurrency']
  ])('should render proper image', (type, expected) => {
    render(renderComponent({ type }));

    expect(document.querySelector('img')?.getAttribute('src')).toBe(expected);
  });
});
