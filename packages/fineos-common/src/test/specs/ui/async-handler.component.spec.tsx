/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { AsyncHandler } from '../../../lib/ui/async-handler';
import { AsyncError, AsyncErrorContext } from '../../../lib/utils';
import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme } from '../../../lib/ui/theme';

describe('AsyncHandler', () => {
  const locale = {
    'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_404': 'MESSAGE_404',
    'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_404': 'DESCRIPTION_404',
    'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_403': 'MESSAGE_403',
    'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_403': 'DESCRIPTION_403',
    'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_401': 'MESSAGE_401',
    'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_401': 'DESCRIPTION_401',
    'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_GENERAL': 'MESSAGE_GENERAL',
    'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_GENERAL': 'DESCRIPTION_GENERAL'
  } as any;

  test('should show notification on regular error', () => {
    let capturedThrowError: AsyncErrorContext['throwError'];

    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <AsyncHandler>
            <AsyncError.Consumer>
              {asyncErrorContext => {
                if (asyncErrorContext) {
                  capturedThrowError = asyncErrorContext.throwError;
                }
                return null;
              }}
            </AsyncError.Consumer>
          </AsyncHandler>
        </IntlProvider>
      </Theme.Provider>
    );

    capturedThrowError!(new Error('test'));

    expect(getByText('MESSAGE_GENERAL')).toBeInTheDocument();
    expect(getByText('DESCRIPTION_GENERAL')).toBeInTheDocument();
  });

  test('should show notification on 404 error', () => {
    let capturedThrowError: AsyncErrorContext['throwError'];

    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <AsyncHandler>
            <AsyncError.Consumer>
              {asyncErrorContext => {
                if (asyncErrorContext) {
                  capturedThrowError = asyncErrorContext.throwError;
                }
                return null;
              }}
            </AsyncError.Consumer>
          </AsyncHandler>
        </IntlProvider>
      </Theme.Provider>
    );

    capturedThrowError!({ status: 404 } as any);

    expect(getByText('MESSAGE_404')).toBeInTheDocument();
    expect(getByText('DESCRIPTION_404')).toBeInTheDocument();
  });

  test('should show notification on 403 error', () => {
    let capturedThrowError: AsyncErrorContext['throwError'];

    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <AsyncHandler>
            <AsyncError.Consumer>
              {asyncErrorContext => {
                if (asyncErrorContext) {
                  capturedThrowError = asyncErrorContext.throwError;
                }
                return null;
              }}
            </AsyncError.Consumer>
          </AsyncHandler>
        </IntlProvider>
      </Theme.Provider>
    );

    capturedThrowError!({ status: 403 } as any);

    expect(queryByText('MESSAGE_403')).toBeInTheDocument();
    expect(queryByText('DESCRIPTION_403')).toBeInTheDocument();
  });

  test('should show notification on 401 error', () => {
    let capturedThrowError: AsyncErrorContext['throwError'];

    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={locale}>
          <AsyncHandler>
            <AsyncError.Consumer>
              {asyncErrorContext => {
                if (asyncErrorContext) {
                  capturedThrowError = asyncErrorContext.throwError;
                }
                return null;
              }}
            </AsyncError.Consumer>
          </AsyncHandler>
        </IntlProvider>
      </Theme.Provider>
    );

    capturedThrowError!({ status: 401 } as any);

    expect(queryByText('MESSAGE_401')).toBeInTheDocument();
    expect(queryByText('DESCRIPTION_401')).toBeInTheDocument();
  });
});
