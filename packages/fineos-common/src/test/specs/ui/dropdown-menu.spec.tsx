import React from 'react';
import userEvent from '@testing-library/user-event';
import { render } from '@testing-library/react';

import { DropdownMenu, Button, Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('DropdownMenu', () => {
  const overlay = (
    <>
      <p>test1</p>
      <p>test2</p>
    </>
  );
  test('should render', () => {
    const { queryByText, getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <DropdownMenu overlay={overlay} trigger={['click']}>
          <Button>Open</Button>
        </DropdownMenu>
      </Theme.Provider>
    );

    expect(getByText('Open')).toBeInTheDocument();
    expect(document.querySelector('.ant-dropdown')).not.toBeInTheDocument();
    expect(queryByText('test1')).not.toBeInTheDocument();
    expect(queryByText('test2')).not.toBeInTheDocument();
  });

  test('should render overlay', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <DropdownMenu overlay={overlay} trigger={['click']}>
          <Button>Open</Button>
        </DropdownMenu>
      </Theme.Provider>
    );

    userEvent.click(getByText('Open'));

    expect(document.querySelector('.ant-dropdown')).toBeInTheDocument();
    expect(getByText('test1')).toBeInTheDocument();
    expect(getByText('test2')).toBeInTheDocument();
  });
});
