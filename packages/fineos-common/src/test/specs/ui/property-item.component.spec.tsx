/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { PropertyItem, Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('PropertyItem', () => {
  test('render', () => {
    const { getByTestId } = render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <PropertyItem label={'label1'} isBlock={false}>
            date
          </PropertyItem>
        </Theme.Provider>
      </IntlProvider>
    );
    const elementLabel = getByTestId('property-item-label');
    expect(elementLabel).toBeInTheDocument();
    expect(elementLabel.textContent).toBe('label1');
    expect(elementLabel.className).toContain('label');

    const elementValue = getByTestId('property-item-value');
    expect(elementValue).toBeInTheDocument();
    expect(elementValue.textContent).toBe('date');
  });

  test('render block', () => {
    const { getByTestId } = render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <PropertyItem label={'label2'}>date1</PropertyItem>
        </Theme.Provider>
      </IntlProvider>
    );
    const elementLabel = getByTestId('property-item-label');
    expect(elementLabel).toBeInTheDocument();
    expect(elementLabel.textContent).toBe('label2');
    expect(elementLabel.className).toContain('block');

    const elementValue = getByTestId('property-item-value');
    expect(elementValue).toBeInTheDocument();
    expect(elementValue.textContent).toBe('date1');
    expect(elementValue.className).toContain('block');
  });

  test('render no children', () => {
    const { getByTestId } = render(
      <IntlProvider locale="en">
        <Theme.Provider value={mockedTheme}>
          <PropertyItem label={'label3'} />
        </Theme.Provider>
      </IntlProvider>
    );
    const elementLabel = getByTestId('property-item-label');
    expect(elementLabel).toBeInTheDocument();
    expect(elementLabel.textContent).toBe('label3');
    expect(elementLabel.className).toContain('block');

    const elementValue = getByTestId('property-item-value');
    expect(elementValue).toBeInTheDocument();
    expect(elementValue.textContent).toBe('-');
  });
});
