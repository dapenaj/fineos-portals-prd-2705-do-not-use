/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { CollapsibleCard } from '../../../lib/ui/collapsible-card';
import { ContextWrapper } from '../../common/utils';

describe('ContentLayout', () => {
  const locale = {
    en: {
      TEST_ARIA_LABEL: 'test aria-label'
    }
  };
  test('render collapsed card', () => {
    render(
      <ContextWrapper>
        <CollapsibleCard title={'Notification Summary'}>
          content
        </CollapsibleCard>
      </ContextWrapper>
    );
    const element = screen.getByTestId('collapsible-card');
    expect(element).toBeInTheDocument();
    expect(screen.queryByText('content')).not.toBeInTheDocument();
  });

  test('render the card as open', () => {
    render(
      <ContextWrapper>
        <CollapsibleCard title={'Notification Summary'} isOpen={true}>
          content
        </CollapsibleCard>
      </ContextWrapper>
    );
    expect(screen.getByText('content')).toBeInTheDocument();
  });

  test('render title using function', () => {
    render(
      <ContextWrapper>
        <CollapsibleCard
          title={({ isCollapsed }) => `isCollapsed: ${isCollapsed}`}
        >
          content
        </CollapsibleCard>
      </ContextWrapper>
    );
    const element = screen.getByTestId('collapsible-card');
    expect(element).toBeInTheDocument();
    expect(screen.getByText('isCollapsed: true')).toBeInTheDocument();
  });

  test('should render with accessibility attributes', () => {
    const mockRole = 'region';
    render(
      <ContextWrapper>
        <CollapsibleCard
          title={({ isCollapsed }) => `isCollapsed: ${isCollapsed}`}
          role={mockRole}
          aria-label={locale.en.TEST_ARIA_LABEL}
        >
          content
        </CollapsibleCard>
      </ContextWrapper>
    );

    expect(document.querySelector(`[role="${mockRole}"]`)).toBeInTheDocument();
    expect(
      document.querySelector(`[aria-label="${locale.en.TEST_ARIA_LABEL}"]`)
    ).toBeInTheDocument();
  });
});
