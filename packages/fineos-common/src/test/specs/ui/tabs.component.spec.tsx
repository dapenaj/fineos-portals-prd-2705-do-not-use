/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme, Tabs } from '../../../lib/ui';

describe('Tabs', () => {
  test('should render with one child', () => {
    const { getAllByText } = render(
      <Theme.Provider value={mockedTheme}>
        <Tabs>
          <Tabs.TabPane tab="Tab 1" key="1">
            Tab 1
          </Tabs.TabPane>
        </Tabs>
      </Theme.Provider>
    );

    expect(getAllByText('Tab 1').length).toBe(2);
  });

  test('should render with couple childer', () => {
    const { getByText } = render(
      <Theme.Provider value={mockedTheme}>
        <Tabs>
          <Tabs.TabPane tab="Tab 1" key="1">
            Tab 1
          </Tabs.TabPane>
          <Tabs.TabPane tab="Tab 2" key="2">
            Tab 2
          </Tabs.TabPane>
        </Tabs>
      </Theme.Provider>
    );

    expect(getByText('Tab 2')).toBeInTheDocument();
  });
  test('should render without childer', () => {
    const { container } = render(
      <Theme.Provider value={mockedTheme}>
        <Tabs className="tabs" />
      </Theme.Provider>
    );

    expect(container.querySelector('div')).not.toBeInTheDocument();
  });
});
