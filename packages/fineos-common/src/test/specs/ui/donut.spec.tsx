/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { Donut } from '../../../lib/ui';
import { ContextWrapper } from '../../common/utils';

describe('Donut', () => {
  test('render', () => {
    render(
      <ContextWrapper>
        <Donut
          data={[
            { label: 'One', value: 60, filterValues: ['One'] },
            { label: 'Two', value: 10, filterValues: ['Two'] },
            { label: 'Three', value: 50, filterValues: ['Three'] },
            { label: 'Four', value: 20, filterValues: ['Four'] },
            { label: 'Five', value: 40, filterValues: ['Five'] },
            { label: 'Six', value: 30, filterValues: ['Six'] }
          ]}
          showTotal={true}
          onSliceClick={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(screen.queryByTestId('donut-graph')).toBeInTheDocument();
  });
  test('should create an "Other" group', () => {
    render(
      <ContextWrapper>
        <Donut
          data={[
            { label: 'One', value: 80, filterValues: ['One'] },
            { label: 'Two', value: 10, filterValues: ['Two'] },
            { label: 'Three', value: 50, filterValues: ['Three'] },
            { label: 'Four', value: 70, filterValues: ['Four'] },
            { label: 'Five', value: 40, filterValues: ['Five'] },
            { label: 'Six', value: 30, filterValues: ['Six'] },
            { label: 'Seven', value: 60, filterValues: ['Seven'] },
            { label: 'Eight', value: 20, filterValues: ['Eight'] }
          ]}
          showTotal={true}
          onSliceClick={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(screen.queryByTestId('donut-graph')).toBeInTheDocument();
    const slices = screen.getAllByTestId('donut-slice');
    expect(slices.length).toBe(6);
    expect(slices[0]).toHaveTextContent('One');
    expect(slices[0]).toHaveTextContent('80');

    expect(slices[1]).toHaveTextContent('Four');
    expect(slices[1]).toHaveTextContent('70');

    expect(slices[2]).toHaveTextContent('Seven');
    expect(slices[2]).toHaveTextContent('60');

    expect(slices[3]).toHaveTextContent('FINEOS_COMMON.DONUT.OTHER');
    expect(slices[3]).toHaveTextContent('60');

    expect(slices[4]).toHaveTextContent('Three');
    expect(slices[4]).toHaveTextContent('50');

    expect(slices[5]).toHaveTextContent('Five');
    expect(slices[5]).toHaveTextContent('40');
  });
});
