import React from 'react';
import { render } from '@testing-library/react';
import { Formik, Field } from 'formik';
import { IntlProvider } from 'react-intl';

import { Form, TextInput, Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('FormComponent', () => {
  const mockLocale = {} as Record<string, string>;

  test('should render', () => {
    const { getByTestId } = render(
      <IntlProvider locale="en" messages={mockLocale}>
        <Theme.Provider value={mockedTheme}>
          <Formik initialValues={{ test: '' }} onSubmit={jest.fn()}>
            <Form>
              <Field name="test" component={TextInput} />
            </Form>
          </Formik>
        </Theme.Provider>
      </IntlProvider>
    );

    expect(document.querySelector('form')).toBeInTheDocument();
    expect(document.querySelector('form')).toHaveAttribute(
      'autoComplete',
      'off'
    );
    expect(getByTestId('text-input')).toBeInTheDocument();
  });
});
