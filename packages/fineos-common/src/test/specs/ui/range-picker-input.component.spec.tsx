/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, waitForElement, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Formik, Field } from 'formik';

import { RangePickerInput } from '../../../lib/ui/range-picker-input';
import {
  ContextWrapper,
  releaseEventLoop,
  getDatepickerDayToClick
} from '../../common/utils';

const defaultProps = {
  name: 'From',
  isDisabled: false,
  label: 'From',
  validate: (value: any) => {
    if (!Boolean(value)) {
      return 'FINEOS_COMMON.VALIDATION.REQUIRED';
    }

    return undefined;
  }
};

const locale = {
  'FINEOS_COMMON.RANGE_PICKER.START_DATE': 'START_DATE',
  'FINEOS_COMMON.RANGE_PICKER.END_DATE': 'END_DATE'
} as Record<string, string>;

describe('RangePickerInput', () => {
  test('render', () => {
    const { getByPlaceholderText } = render(
      <ContextWrapper translations={locale}>
        <Formik initialValues={{}} onSubmit={jest.fn()}>
          <form>
            <Field component={RangePickerInput} {...defaultProps} />
          </form>
        </Formik>
      </ContextWrapper>
    );

    expect(getByPlaceholderText('START_DATE')).toBeInTheDocument();
    expect(getByPlaceholderText('END_DATE')).toBeInTheDocument();
  });

  test('should call onChange prop', async () => {
    const onChangeSpy = jest.fn();

    const { container } = render(
      <ContextWrapper translations={locale}>
        <Formik initialValues={{ test: [] }} onSubmit={jest.fn()}>
          <form>
            <Field
              component={RangePickerInput}
              {...defaultProps}
              onChange={onChangeSpy}
              name="test"
            />
          </form>
        </Formik>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('input')!);
      return releaseEventLoop();
    });

    const picker = (await waitForElement(() =>
      document.body.querySelector('.ant-picker-dropdown')
    )) as HTMLElement;

    await act(() => {
      userEvent.click(
        within(picker).getAllByText(getDatepickerDayToClick())[0]
      );
      return releaseEventLoop();
    });

    await act(() => {
      userEvent.click(
        within(picker).getAllByText(getDatepickerDayToClick())[1]
      );
      return releaseEventLoop();
    });

    expect(onChangeSpy).toHaveBeenCalledTimes(1);
  });
});
