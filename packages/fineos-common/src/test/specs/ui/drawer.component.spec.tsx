/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { IntlProvider } from 'react-intl';

import { Drawer, Theme } from '../../../lib/ui';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('Drawer', () => {
  test('should render', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Drawer
            ariaLabelId="FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL"
            isVisible={true}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByTestId('drawer')).toBeInTheDocument();
  });

  test('should not render if isVisible is false', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Drawer
            ariaLabelId="FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL"
            isVisible={false}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.queryByTestId('drawer')).not.toBeInTheDocument();
  });

  test('should render title', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Drawer
            ariaLabelId="FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL"
            isVisible={true}
            title="test title"
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByText('test title')).toBeInTheDocument();
  });

  test('should render children', async () => {
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Drawer
            ariaLabelId="FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL"
            isVisible={true}
          >
            <p className="testContent">test content</p>
          </Drawer>
        </IntlProvider>
      </Theme.Provider>
    );

    expect(screen.getByText('test content')).toBeInTheDocument();
  });

  test('should close', async () => {
    const spy = jest.fn();
    render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en">
          <Drawer
            ariaLabelId="FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL"
            isVisible={true}
            isClosable={true}
            onClose={spy}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    userEvent.click(
      screen.getByText('FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL')
    );

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
