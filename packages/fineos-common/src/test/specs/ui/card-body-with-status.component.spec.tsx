/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme } from '../../../lib/ui/theme';
import { StatusType, StatusPattern } from '../../../lib/types';
import { CardBodyWithStatus } from '../../../lib/ui/collapsible-card/card-body-with-status';

describe('CardBodyWithStatus', () => {
  test('renders', () => {
    const { getByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <CardBodyWithStatus
          type={StatusType.SUCCESS}
          pattern={StatusPattern.HALVED}
        >
          Card Body
        </CardBodyWithStatus>
      </Theme.Provider>
    );
    const element = getByTestId('card-body-with-status');
    expect(element).toBeInTheDocument();
    expect(element.textContent).toBe('Card Body');
  });

  test('render with different border options', () => {
    const { getByTestId, rerender } = render(
      <Theme.Provider value={mockedTheme}>
        <CardBodyWithStatus>Card Body</CardBodyWithStatus>
      </Theme.Provider>
    );

    const element = getByTestId('card-body-with-status');

    expect(element.className).toContain('blank');
    expect(element.className).toContain('full');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <CardBodyWithStatus
          type={StatusType.SUCCESS}
          pattern={StatusPattern.ANGLED}
        >
          Card Body
        </CardBodyWithStatus>
      </Theme.Provider>
    );
    expect(element.className).toContain('success');
    expect(element.className).toContain('angled');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <CardBodyWithStatus
          type={StatusType.WARNING}
          pattern={StatusPattern.HALVED}
        >
          Card Body
        </CardBodyWithStatus>
      </Theme.Provider>
    );
    expect(element.className).toContain('warning');
    expect(element.className).toContain('halved');

    rerender(
      <Theme.Provider value={mockedTheme}>
        <CardBodyWithStatus
          type={StatusType.DANGER}
          pattern={StatusPattern.FULL}
        >
          Card Body
        </CardBodyWithStatus>
      </Theme.Provider>
    );
    expect(element.className).toContain('danger');
    expect(element.className).toContain('full');
  });
});
