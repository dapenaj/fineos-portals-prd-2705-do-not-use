/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';
import { scaleTime } from 'd3-scale';

import { mockedTheme } from '../../../fixture/mocked-theme';
import { Theme } from '../../../../lib/ui';
import { GanttMonths } from '../../../../lib/ui/gantt/gantt-month-borders.component';

describe('GanttMonths', () => {
  test('renders', () => {
    const months = [
      moment('2020-02-21')
        .clone()
        .startOf('month')
    ];
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <GanttMonths
          months={months}
          scale={scaleTime()
            .domain([moment('1999-12-31'), moment('1999-12-31')])
            .range([0, 419])}
          monthClassName={'monthClassName'}
          lastMonthClassName={''}
          render={month => month.format('MMMM YYYY')}
        />
      </Theme.Provider>
    );
    const element = queryByText('February 2020');
    expect(element).toBeInTheDocument();
  });
});
