/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';

import { mockedTheme } from '../../../fixture/mocked-theme';
import { Theme } from '../../../../lib/ui';
import {
  Gantt,
  GanttEntryColor,
  GanttEntryShape
} from '../../../../lib/ui/gantt';

describe('Gantt', () => {
  test('render', () => {
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <Gantt title="Title GANTT" groups={[]} />
      </Theme.Provider>
    );
    const element = queryByText('Title GANTT');
    expect(element).toBeInTheDocument();
  });

  test('render with wage replacement', () => {
    const wageReplacementRows = [
      {
        id: 'wageReplacement-Short Term Disability-0',
        title: 'Short Term Disability',
        entries: [
          {
            id: 'wageReplacement-Short Term Disability-0-entry',
            color: GanttEntryColor.GREEN,
            shape: GanttEntryShape.FILLED,
            startDate: moment('2019-04-20'),
            endDate: moment('2019-04-20'),
            tooltip: <div>'WAGE_REPLACEMENT_RECURRING'</div>
          }
        ]
      }
    ];
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <Gantt
          title="Title GANTT"
          groups={[
            {
              id: 'wageReplacement',
              title: 'WAGE_REPLACMENT',
              rows: wageReplacementRows
            }
          ]}
        />
      </Theme.Provider>
    );
    const element = queryByText('Title GANTT');
    expect(element).toBeInTheDocument();
    const wageReplacement = queryByText('WAGE_REPLACMENT');
    expect(wageReplacement).toBeInTheDocument();
  });
});
