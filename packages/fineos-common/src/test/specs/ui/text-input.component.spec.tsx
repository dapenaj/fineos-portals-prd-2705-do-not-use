/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme, TextInput, FormattedMessage } from '../../../lib/ui';
import { AutocompleteContext } from '../../../lib/utils';

describe('TextInput', () => {
  const mockInputProps = {
    field: { name: 'test' },
    form: {}
  } as any;
  const translations = {
    TEST: 'test {count}'
  } as Record<string, string>;

  test('render', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <TextInput {...mockInputProps} />
        </IntlProvider>
      </Theme.Provider>
    );

    const element = queryByTestId('text-input');
    expect(element).toBeInTheDocument();
    expect(element).not.toHaveAttribute('disabled');
  });

  test('render correctly with custom props', () => {
    const { queryByTestId, queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <TextInput
            {...mockInputProps}
            isDisabled={true}
            data-test-el="mock-text-input"
            className="mock-input"
          >
            <p>test</p>
          </TextInput>
        </IntlProvider>
      </Theme.Provider>
    );

    const element = queryByTestId('mock-text-input');
    const child = queryByText('test');
    expect(element!.classList).toContain('mock-input');
    expect(element).toHaveAttribute('disabled');
    expect(child).toBeInTheDocument();
  });

  test('render with formatted placeholder', () => {
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <TextInput
            {...mockInputProps}
            placeholder={<FormattedMessage id={'TEST'} values={{ count: 2 }} />}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    const element = queryByTestId('text-input');
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute('placeholder', 'test 2');
  });

  test('render with autocomplete off', () => {
    const mockedAutocomplete = {
      autocomplete: 'off'
    };
    const mockInputPropsWithEmailAddress = {
      field: { name: 'emails[1].emailAddress' },
      form: {}
    } as any;
    const { queryByTestId } = render(
      <Theme.Provider value={mockedTheme}>
        <AutocompleteContext.Provider value={mockedAutocomplete}>
          <IntlProvider locale="en" messages={translations}>
            <TextInput {...mockInputPropsWithEmailAddress} />
          </IntlProvider>
        </AutocompleteContext.Provider>
      </Theme.Provider>
    );

    const element = queryByTestId('text-input');
    expect(element).toBeInTheDocument();
    expect(element).toHaveAttribute('autoComplete', 'off');
  });
});
