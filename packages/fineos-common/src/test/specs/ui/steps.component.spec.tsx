/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { mockedTheme } from '../../fixture/mocked-theme';
import { Theme } from '../../../lib/ui';
import { Steps, StepType } from '../../../lib/ui/steps';

describe('Steps', () => {
  const translations = {
    'INTAKE.EMPLOYEE_DETAILS.HEADER': 'Employee details header',
    'INTAKE.OCCUPATION_AND_EARNINGS.HEADER': 'Occupation and earnings header',
    'INTAKE.INITIAL_REQUEST.HEADER': 'Initial request header',
    'FINEOS_COMMON.ICON_LABEL.AFFIRMATIVE_ICON_LABEL': 'AFFIRMATIVE_ICON_LABEL'
  } as Record<string, string>;

  test('should render steps with one in process status and two in wait status', () => {
    const { getByText, container } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <Steps
            steps={
              [
                {
                  title: 'INTAKE.EMPLOYEE_DETAILS.HEADER'
                },
                {
                  title: 'INTAKE.OCCUPATION_AND_EARNINGS.HEADER'
                },
                {
                  title: 'INTAKE.INITIAL_REQUEST.HEADER'
                }
              ] as StepType[]
            }
            current={0}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(getByText('Employee details header')).toBeInTheDocument();
    expect(getByText('Occupation and earnings header')).toBeInTheDocument();
    expect(getByText('Initial request header')).toBeInTheDocument();
    expect(
      container.querySelector('.ant-steps-item-process')
    ).toBeInTheDocument();
    expect(container.querySelectorAll('.ant-steps-item-wait').length).toEqual(
      2
    );
  });

  test('should render steps with one is finished status, one in process status and one in wait status', () => {
    const { container } = render(
      <Theme.Provider value={mockedTheme}>
        <IntlProvider locale="en" messages={translations}>
          <Steps
            steps={
              [
                {
                  title: 'INTAKE.EMPLOYEE_DETAILS.HEADER'
                },
                {
                  title: 'INTAKE.OCCUPATION_AND_EARNINGS.HEADER'
                },
                {
                  title: 'INTAKE.INITIAL_REQUEST.HEADER'
                }
              ] as StepType[]
            }
            current={1}
          />
        </IntlProvider>
      </Theme.Provider>
    );

    expect(container.querySelectorAll('.ant-steps-item-finish').length).toEqual(
      1
    );
    expect(
      container.querySelectorAll('.ant-steps-item-process').length
    ).toEqual(1);
    expect(container.querySelectorAll('.ant-steps-item-wait').length).toEqual(
      1
    );
  });
});
