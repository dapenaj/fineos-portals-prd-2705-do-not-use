import React from 'react';
import { render } from '@testing-library/react';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { IntlProvider } from 'react-intl';

import { Icon } from '../../../../lib/ui/icon';

describe('Icon', () => {
  test('should render', () => {
    render(
      <IntlProvider locale="">
        <Icon icon={faPhoneAlt} />
      </IntlProvider>
    );

    expect(document.querySelector('svg')).toBeInTheDocument();
    expect(document.querySelector('svg')).toHaveAttribute(
      'aria-hidden',
      'false'
    );
    expect(document.querySelector('.fa-phone-alt')).toBeInTheDocument();
  });
});
