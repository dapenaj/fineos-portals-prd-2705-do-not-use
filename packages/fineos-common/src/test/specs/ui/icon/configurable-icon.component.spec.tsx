import React from 'react';
import { render, screen } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import { ConfigurableIcon } from '../../../../lib/ui/icon';
import { mockedTheme } from '../../../fixture/mocked-theme';
import { Theme } from '../../../../lib/ui/theme';
import { FormattedMessage } from '../../../../lib/ui/formatted-message';

describe('ConfigurableIcon', () => {
  const translations = {
    TEST: 'test msg',
    UNSAFE: `test <script>throw new Error('Ops')</script>unsafe`
  } as Record<string, string>;

  const safeSvg = `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" aria-label="test">
      <path fill="currentColor" d="M248 8C111 8 0" />
    </svg>
  `.trim();

  const unsafeSvg = `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" aria-label="test">
      <path fill="currentColor" d="M248 8C111 8 0" />
      <script>
      // <![CDATA[
      throw new Error('Attack');
      // ]]>
      </script>
    </svg>
  `.trim();

  test('should render configurable icon by name', () => {
    render(
      <IntlProvider locale="en" messages={translations}>
        <Theme.Provider
          value={{ ...mockedTheme, icons: { TEST_ICON: safeSvg } }}
        >
          <ConfigurableIcon name="TEST_ICON" />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByLabelText('test').tagName).toEqual('svg');
    expect(screen.getByLabelText('test').childElementCount).toEqual(1);
  });

  test('should render nothing if icon not set in theme', () => {
    render(
      <IntlProvider locale="en" messages={translations}>
        <Theme.Provider value={{ ...mockedTheme }}>
          <ConfigurableIcon name="TEST_ICON" />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(document.querySelector('svg')).not.toBeInTheDocument();
  });

  test('should remove script tag when rendering SVG', () => {
    render(
      <IntlProvider locale="en" messages={translations}>
        <Theme.Provider
          value={{ ...mockedTheme, icons: { TEST_ICON: unsafeSvg } }}
        >
          <ConfigurableIcon name="TEST_ICON" />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByLabelText('test').querySelector('script')).toBeNull();
    expect(screen.getByLabelText('test').childElementCount).toEqual(1);
  });

  test('should allow to pass className as icon wrapper element class', () => {
    const testClassName = 'test-class-' + Date.now();
    render(
      <IntlProvider locale="en" messages={translations}>
        <Theme.Provider
          value={{ ...mockedTheme, icons: { TEST_ICON: safeSvg } }}
        >
          <ConfigurableIcon name="TEST_ICON" className={testClassName} />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByLabelText('test').parentNode).toHaveClass(testClassName);
  });

  test('should allow to pass iconClassName as class for icon', () => {
    const testClassName = 'icon-test-class-' + Date.now();
    render(
      <IntlProvider locale="en" messages={translations}>
        <Theme.Provider
          value={{ ...mockedTheme, icons: { TEST_ICON: safeSvg } }}
        >
          <ConfigurableIcon name="TEST_ICON" iconClassName={testClassName} />
        </Theme.Provider>
      </IntlProvider>
    );

    expect(screen.getByLabelText('test')).toHaveClass(testClassName);
  });

  describe('a11y', () => {
    test('should set aria-hidden on parent span if no iconLabel provided', () => {
      render(
        <IntlProvider locale="en" messages={translations}>
          <Theme.Provider
            value={{ ...mockedTheme, icons: { TEST_ICON: safeSvg } }}
          >
            <ConfigurableIcon name="TEST_ICON" />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByLabelText('test').parentNode).toHaveAttribute(
        'aria-hidden',
        'true'
      );
    });

    test('should set svg title if iconLabel provided and link it properly', () => {
      render(
        <IntlProvider locale="en" messages={translations}>
          <Theme.Provider
            value={{ ...mockedTheme, icons: { TEST_ICON: safeSvg } }}
          >
            <ConfigurableIcon
              name="TEST_ICON"
              iconLabel={<FormattedMessage id="TEST" />}
            />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByLabelText('test').parentNode).not.toHaveAttribute(
        'aria-hidden',
        'true'
      );
      expect(
        document.getElementById(
          screen.getByLabelText('test').getAttribute('aria-labelledby')!
        )
      ).toContainHTML('test msg');
    });

    test('should strip out unsafe SVG from title', () => {
      render(
        <IntlProvider locale="en" messages={translations}>
          <Theme.Provider
            value={{ ...mockedTheme, icons: { TEST_ICON: safeSvg } }}
          >
            <ConfigurableIcon
              name="TEST_ICON"
              iconLabel={<FormattedMessage id="UNSAFE" />}
            />
          </Theme.Provider>
        </IntlProvider>
      );

      expect(screen.getByLabelText('test').parentNode).not.toHaveAttribute(
        'aria-hidden',
        'true'
      );
      expect(
        document.getElementById(
          screen.getByLabelText('test').getAttribute('aria-labelledby')!
        )
      ).toContainHTML('test unsafe');
    });
  });
});
