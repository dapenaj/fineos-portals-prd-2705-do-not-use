/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import fetchMock, {
  enableFetchMocks,
  disableFetchMocks
} from 'jest-fetch-mock';

import { loadThemeIcons } from '../../../../lib/ui/icon';
import { mockedTheme } from '../../../fixture/mocked-theme';

describe('loadThemeIcons', () => {
  beforeEach(enableFetchMocks);
  afterEach(disableFetchMocks);

  it('should return theme as it is if no icons', async () => {
    const { icons, ...noIconsTheme } = mockedTheme;

    expect(await loadThemeIcons(noIconsTheme)).toBe(noIconsTheme);
  });

  it('should load icons by provided URLs in theme', async () => {
    fetchMock.doMockOnceIf('/test.svg', '<svg></svg>');

    const themeWithIcons = await loadThemeIcons({
      ...mockedTheme,
      icons: {
        TEST_ICON: '/test.svg'
      }
    });
    expect(themeWithIcons.icons?.TEST_ICON).toBe('<svg></svg>');
  });

  it('should load all icons by provided URLs in theme', async () => {
    fetchMock.doMockOnceIf('/test1.svg', '<svg>1</svg>');
    fetchMock.doMockOnceIf('/test2.svg', '<svg>2</svg>');
    fetchMock.doMockOnceIf('/test3.svg', '<svg>3</svg>');

    const themeWithIcons = await loadThemeIcons({
      ...mockedTheme,
      icons: {
        TEST_ICON1: '/test1.svg',
        TEST_ICON2: '/test2.svg',
        TEST_ICON3: '/test3.svg'
      }
    });
    expect(themeWithIcons.icons?.TEST_ICON1).toBe('<svg>1</svg>');
    expect(themeWithIcons.icons?.TEST_ICON2).toBe('<svg>2</svg>');
    expect(themeWithIcons.icons?.TEST_ICON3).toBe('<svg>3</svg>');
  });

  it('should remove icon if error occur during loading', async () => {
    fetchMock.mockRejectOnce(new Error('fail'));

    const themeWithIcons = await loadThemeIcons({
      ...mockedTheme,
      icons: {
        TEST_ICON: '/test.svg'
      }
    });
    expect(themeWithIcons.icons?.TEST_ICON).toBeUndefined();
  });
});
