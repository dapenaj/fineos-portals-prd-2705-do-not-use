/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';

import { FormattedDate } from '../../../lib/ui/formatted-date';
import { Theme } from '../../../lib/ui/theme';
import { mockedTheme } from '../../fixture/mocked-theme';

describe('TimelineLegend', () => {
  test('render', () => {
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <FormattedDate date={moment('2019-12-31')} />
      </Theme.Provider>
    );
    expect(queryByText('Dec 31, 2019')).toBeInTheDocument();
  });

  test('render fallback', () => {
    const { queryByText } = render(
      <Theme.Provider value={mockedTheme}>
        <FormattedDate date={moment('31')} fallback="fallback" />
      </Theme.Provider>
    );
    expect(queryByText('fallback')).toBeInTheDocument();
  });
});
