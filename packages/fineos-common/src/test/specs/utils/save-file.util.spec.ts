/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

/* eslint-disable import/first */
jest.mock('file-saver', () => ({
  saveAs: jest.fn()
}));

import { saveAs } from 'file-saver';

import * as saveFileUtil from '../../../lib/utils/save-file.util';

describe('save-file.util', () => {
  describe('saveBase64File', () => {
    test('should save string to file as blob', () => {
      saveFileUtil.saveBase64File({
        fileName: 'test.json',
        data: 'eyJhIjogMX0=',
        contentType: 'text/json'
      });

      expect(saveAs).toHaveBeenCalledWith(
        new Blob(
          new Uint8Array(
            Array.from('{"a": 1}').map(char => char.charCodeAt(0))
          ) as any
        ),
        'test.json'
      );
    });
  });

  describe('saveCsvFile', () => {
    test('should save a csv file', () => {
      const spy = jest.spyOn(saveFileUtil, 'saveCsvFile');
      const csv: saveFileUtil.CsvDetails = {
        columns: ['test'],
        rows: [['test']],
        filename: 'Test'
      };

      saveFileUtil.saveCsvFile(csv);

      expect(spy).toHaveBeenCalledWith(csv);
    });
  });
});
