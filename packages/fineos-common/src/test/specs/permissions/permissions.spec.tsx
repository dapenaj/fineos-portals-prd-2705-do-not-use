/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';

import {
  AdminPermissions,
  anyFrom,
  ManageCustomerDataPermissions,
  PermissionsScope,
  required,
  usePermissions,
  ViewCustomerDataPermissions,
  withPermissions,
  GroupClientPermissions
} from '../../../lib/permissions';

describe('permissions', () => {
  test('withPermissions should not render target component, if no access', () => {
    const TargetComponent = () => <div>Test</div>;
    const RestrictedComponent = withPermissions(
      AdminPermissions.VIEW_USERS_LIST
    )(TargetComponent);
    const { rerender, queryByText } = render(
      <PermissionsScope.Provider
        value={{ permissions: new Set(), logout: jest.fn() }}
      >
        <RestrictedComponent />
      </PermissionsScope.Provider>
    );

    expect(queryByText('Test')).not.toBeInTheDocument();

    rerender(
      <PermissionsScope.Provider
        value={{
          permissions: new Set([AdminPermissions.VIEW_USERS_LIST]),
          logout: jest.fn()
        }}
      >
        <RestrictedComponent />
      </PermissionsScope.Provider>
    );

    expect(queryByText('Test')).toBeInTheDocument();
  });

  test('usePermissions should return if access is available', () => {
    const permissionsSet = new Set<GroupClientPermissions>();
    const wrapper: React.FC = ({ children }) => (
      <PermissionsScope.Provider
        value={{ permissions: permissionsSet, logout: jest.fn() }}
      >
        {children}
      </PermissionsScope.Provider>
    );
    const { result, rerender } = renderHook(
      () => usePermissions(required(AdminPermissions.VIEW_USERS_LIST)),
      { wrapper }
    );

    expect(result.current).toBe(false);

    permissionsSet.add(AdminPermissions.VIEW_USERS_LIST);
    rerender();

    expect(result.current).toBe(true);
  });

  test('usePermissions should allow to limit access by nested checks', () => {
    const permissionsSet = new Set<GroupClientPermissions>();
    const wrapper: React.FC = ({ children }) => (
      <PermissionsScope.Provider
        value={{ permissions: permissionsSet, logout: jest.fn() }}
      >
        {children}
      </PermissionsScope.Provider>
    );
    const { result, rerender } = renderHook(
      () =>
        usePermissions(
          anyFrom(
            AdminPermissions.VIEW_USER_DETAILS,
            required(
              AdminPermissions.VIEW_USERS_LIST,
              ViewCustomerDataPermissions.VIEW_CUSTOMER_DETAILS,
              anyFrom(
                ManageCustomerDataPermissions.UPDATE_CUSTOMER_CONTACT_DETAILS,
                ManageCustomerDataPermissions.UPDATE_CUSTOMER_DETAILS
              )
            )
          )
        ),
      { wrapper }
    );

    expect(result.current).toBe(false);

    permissionsSet.add(AdminPermissions.VIEW_USER_DETAILS);
    rerender();

    expect(result.current).toBe(true);

    permissionsSet.clear();
    permissionsSet.add(AdminPermissions.VIEW_USERS_LIST);
    permissionsSet.add(ViewCustomerDataPermissions.VIEW_CUSTOMER_DETAILS);
    rerender();

    expect(result.current).toBe(false);

    permissionsSet.add(
      ManageCustomerDataPermissions.UPDATE_CUSTOMER_CONTACT_DETAILS
    );
    rerender();

    expect(result.current).toBe(true);
  });
});
