/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import { act, renderHook } from '@testing-library/react-hooks';
import { noop } from 'lodash';
import { wait } from '@testing-library/react';

import { useAsync, UseAsyncCacheStrategy } from '../../../lib/hooks';
import { AsyncError, SharedState } from '../../../lib/utils';
import { render } from '../../config/custom-render';

describe('useAsync', () => {
  type AsyncState = ReturnType<typeof useAsync>['state'];

  const renderAsyncHook = <Value, Params extends any[]>(
    asyncOp: (...params: Params) => Promise<Value>,
    {
      params,
      options
    }: {
      params: Params;
      options?: any;
    },
    hookOptions?: {
      wrapper: React.ComponentType;
    }
  ) =>
    renderHook(() => useAsync<Value, Params>(asyncOp, params, options), {
      wrapper: ({ children }) => {
        if (hookOptions?.wrapper) {
          const Wrapper = hookOptions?.wrapper;
          return (
            <SharedState>
              <Wrapper>{children}</Wrapper>
            </SharedState>
          );
        } else {
          return <SharedState>{children}</SharedState>;
        }
      }
    });

  test('should perform async op by default if no condition provided', () => {
    const neverResolvedAsync = jest.fn().mockResolvedValue(new Promise(noop));
    renderAsyncHook(neverResolvedAsync, { params: [] });
    expect(neverResolvedAsync).toHaveBeenCalled();
  });

  test('should perform not async op by default if condition provided', () => {
    const neverResolvedAsync = jest.fn().mockResolvedValue(new Promise(noop));
    renderAsyncHook(neverResolvedAsync, {
      params: [],
      options: { shouldExecute: () => false }
    });
    expect(neverResolvedAsync).not.toHaveBeenCalled();
  });

  test('should handle success flow', async () => {
    let resolve: (val: number) => void;

    const neverResolvedAsync = jest.fn().mockResolvedValue(
      new Promise<number>(f => {
        resolve = f;
      })
    );

    const { result, waitForNextUpdate } = renderAsyncHook(neverResolvedAsync, {
      params: [1]
    });

    expect(result.current.state).toEqual({
      isPending: true,
      error: null,
      value: null,
      params: null,
      status: 'PENDING',
      isStale: true
    });

    resolve!(1);
    await waitForNextUpdate();

    expect(result.current.state).toEqual({
      isPending: false,
      error: null,
      value: 1,
      params: [1],
      status: 'SUCCESS',
      isStale: false
    });
  });

  test('should handle error flow', async () => {
    let reject: (val: Error) => void;

    const neverResolvedAsync = jest.fn().mockResolvedValue(
      new Promise((f, r) => {
        reject = r;
      })
    );

    const throwErrorMock = jest.fn();
    const { result, waitForNextUpdate } = renderAsyncHook(
      neverResolvedAsync,
      { params: [1] },
      {
        wrapper: ({ children }) => (
          <AsyncError.Provider value={{ throwError: throwErrorMock }}>
            {children}
          </AsyncError.Provider>
        )
      }
    );

    expect(result.current.state).toEqual({
      isPending: true,
      error: null,
      value: null,
      params: null,
      status: 'PENDING',
      isStale: true
    });

    reject!(new Error('test'));
    await waitForNextUpdate();

    expect(result.current.state).toEqual({
      isPending: false,
      error: new Error('test'),
      value: null,
      params: [1],
      status: 'FAIL',
      isStale: true
    });
    expect(throwErrorMock).toHaveBeenCalledWith(new Error('test'));
  });

  test('should suppress global error handling when provided exact status', async () => {
    let reject: (val: Error) => void;

    const neverResolvedAsync = jest.fn().mockResolvedValue(
      new Promise((f, r) => {
        reject = r;
      })
    );

    const throwErrorMock = jest.fn();
    const { waitForNextUpdate } = renderAsyncHook(
      neverResolvedAsync,
      {
        params: [1],
        options: {
          noGlobalErrorHandlingFor: 400
        }
      },
      {
        wrapper: ({ children }) => (
          <AsyncError.Provider value={{ throwError: throwErrorMock }}>
            {children}
          </AsyncError.Provider>
        )
      }
    );

    reject!({ status: 400 } as any);

    await waitForNextUpdate();

    expect(throwErrorMock).not.toHaveBeenCalled();
  });

  test('should suppress global error handling when provided several statuses', async () => {
    let reject: (val: Error) => void;

    const neverResolvedAsync = jest.fn().mockResolvedValue(
      new Promise((f, r) => {
        reject = r;
      })
    );

    const throwErrorMock = jest.fn();
    const { waitForNextUpdate } = renderAsyncHook(
      neverResolvedAsync,
      {
        params: [1],
        options: {
          noGlobalErrorHandlingFor: [400, 403]
        }
      },
      {
        wrapper: ({ children }) => (
          <AsyncError.Provider value={{ throwError: throwErrorMock }}>
            {children}
          </AsyncError.Provider>
        )
      }
    );

    reject!({ status: 403 } as any);

    await waitForNextUpdate();

    expect(throwErrorMock).not.toHaveBeenCalled();
  });

  test('should suppress global error handling when provided custom function', async () => {
    let reject: (val: Error) => void;

    const neverResolvedAsync = jest.fn().mockResolvedValue(
      new Promise((f, r) => {
        reject = r;
      })
    );

    const throwErrorMock = jest.fn();
    const { waitForNextUpdate } = renderAsyncHook(
      neverResolvedAsync,
      {
        params: [1],
        options: {
          noGlobalErrorHandlingFor: (error: Error) => {
            return error.message === 'test';
          }
        }
      },
      {
        wrapper: ({ children }) => (
          <AsyncError.Provider value={{ throwError: throwErrorMock }}>
            {children}
          </AsyncError.Provider>
        )
      }
    );

    reject!(new Error('test'));

    await waitForNextUpdate();

    expect(throwErrorMock).not.toHaveBeenCalled();
  });

  test('should handle a change flow', async () => {
    const neverResolvedAsync = jest.fn().mockResolvedValue(new Promise(noop));

    const { result } = renderAsyncHook(neverResolvedAsync, { params: [1] });

    const initialState = {
      isPending: true,
      error: null,
      value: null,
      params: null,
      status: 'PENDING',
      isStale: true
    };

    expect(result.current.state).toEqual(initialState);

    act(() => {
      result.current.changeAsyncValue(1);
    });

    expect(result.current.state).toEqual({
      ...initialState,
      status: 'SUCCESS',
      value: 1,
      isStale: false,
      isPending: false
    });
  });

  test('should handle execute again', () => {
    const neverResolvedAsync = jest.fn().mockResolvedValue(new Promise(noop));

    const { result } = renderAsyncHook(neverResolvedAsync, { params: [1] });

    const initialState = {
      isPending: true,
      error: null,
      value: null,
      params: null,
      status: 'PENDING',
      isStale: true
    };

    expect(result.current.state).toEqual(initialState);

    result.current.executeAgain();

    expect(result.current.state).toEqual(initialState);
  });

  test('should identify state as stale on first load', () => {
    const neverResolvedAsync = jest.fn().mockResolvedValue(new Promise(noop));
    const { result } = renderAsyncHook(neverResolvedAsync, {
      params: []
    });
    expect(result.current.state.isStale).toEqual(true);
  });

  describe('shared state', () => {
    const Cmp1 = ({
      onChange,
      param = 1,
      op
    }: {
      onChange: (v: any) => void;
      param?: number;
      op: any;
    }) => {
      const { state } = useAsync(op, [param]);

      useEffect(() => {
        onChange(state.value);
      }, [state.value]);

      return (
        <>
          <div data-test-el="value">{JSON.stringify(state.value)}</div>
          <div data-test-el="isStale">{JSON.stringify(state.isStale)}</div>
        </>
      );
    };

    test('should return correct isStale value', async () => {
      let state1!: AsyncState;
      let state2!: AsyncState;
      let state3!: AsyncState;
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Date.now()]));

      const { getByTestId, rerender } = render(
        <SharedState>
          <Cmp1 onChange={val => (state1 = val)} op={asyncOp} param={1} />
        </SharedState>
      );

      await act(wait);

      rerender(
        <SharedState>
          <Cmp1 onChange={val => (state2 = val)} op={asyncOp} param={2} />
        </SharedState>
      );

      await act(wait);

      rerender(
        <SharedState>
          <Cmp1 onChange={val => (state3 = val)} op={asyncOp} param={1} />
        </SharedState>
      );

      expect(getByTestId('isStale')).toContainHTML('true');
      expect(getByTestId('value')).toContainHTML(JSON.stringify(state1));

      await act(wait);

      expect(getByTestId('isStale')).toContainHTML('false');
      expect(getByTestId('value')).not.toContainHTML(JSON.stringify(state1));
    });

    test('should share state between calls', async () => {
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Math.random()]));

      let state1: [number] = [-1];
      let state2: [number] = [-2];
      let state3: [number] = [-3];

      render(
        <SharedState>
          <Cmp1 onChange={val => (state1 = val)} op={asyncOp} />
          <Cmp1 onChange={val => (state2 = val)} op={asyncOp} />
          <Cmp1
            onChange={val => (state3 = val)}
            op={jest
              .fn()
              .mockImplementation(() => Promise.resolve([Math.random()]))}
          />
        </SharedState>
      );

      await act(wait);

      expect(state1).toBe(state2);
      expect(state1).not.toBe(state3);
      expect(state2).not.toBe(state3);
    });

    test('should execute underlaying function only once for 2 parallel rendering', async () => {
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Math.random()]));

      let state1: [number] = [-1];
      let state2: [number] = [-2];

      render(
        <SharedState>
          <Cmp1 onChange={val => (state1 = val)} op={asyncOp} />
          <Cmp1 onChange={val => (state2 = val)} op={asyncOp} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toBe(state2);
      expect(asyncOp).toHaveBeenCalledTimes(1);
    });

    test('should execute underlaying function only once for 2 step in time rendering', async () => {
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Math.random()]));

      let state1: [number] = [-1];
      let state2: [number] = [-2];

      const TwoStepRender = () => {
        const [state, setState] = useState(false);

        useEffect(() => {
          setTimeout(() => {
            setState(() => true);
          });
        }, [setState]);

        return (
          <SharedState>
            <Cmp1 onChange={val => (state1 = val)} op={asyncOp} />
            {state && <Cmp1 onChange={val => (state2 = val)} op={asyncOp} />}
          </SharedState>
        );
      };

      render(<TwoStepRender />);

      await act(wait);

      expect(state1).toBe(state2);
      expect(asyncOp).toHaveBeenCalledTimes(1);
    });

    test('should allow re-execute if new context created', async () => {
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Math.random()]));

      let state1: [number] = [-1];
      let state2: [number] = [-2];

      const { unmount } = render(
        <SharedState>
          <Cmp1 onChange={val => (state1 = val)} op={asyncOp} />
          <Cmp1 onChange={val => (state2 = val)} op={asyncOp} />
        </SharedState>
      );

      await act(wait);

      unmount();

      await act(wait);

      let state3: [number] = [-3];
      let state4: [number] = [-4];

      render(
        <SharedState>
          <Cmp1 onChange={val => (state3 = val)} op={asyncOp} />
          <Cmp1 onChange={val => (state4 = val)} op={asyncOp} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toBe(state2);
      expect(state3).toBe(state4);
      expect(state1).not.toBe(state3);
      expect(asyncOp).toHaveBeenCalledTimes(2);
    });

    test('should handle a change flow within shared state', async () => {
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Math.random()]));
      let changeAsyncShared: (value: unknown) => void;

      const Cmp2 = ({
        onChange,
        op
      }: {
        onChange: (v: any) => void;
        op: any;
      }) => {
        const { state, changeAsyncValue } = useAsync(op, [1]);

        changeAsyncShared = changeAsyncValue;

        useEffect(() => {
          onChange(state.value);
        }, [state.value]);

        return null;
      };

      let state1: [number] = [-1];
      let state2: [number] = [-2];

      render(
        <SharedState>
          <Cmp2 onChange={val => (state1 = val)} op={asyncOp} />
          <Cmp2 onChange={val => (state2 = val)} op={asyncOp} />
        </SharedState>
      );

      const newState = [-5];

      await act(wait);
      await act(() => {
        changeAsyncShared(newState);
        return wait();
      });

      expect(state1).toBe(newState);
      expect(state1).toBe(state2);
    });

    test('should not reuse same data for the same function but different params', async () => {
      let state1: [number] = [0];
      let state2: [number] = [0];
      let counter = 0;

      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([++counter]));

      render(
        <SharedState>
          <Cmp1 onChange={val => (state1 = val)} op={asyncOp} param={1} />
          <Cmp1 onChange={val => (state2 = val)} op={asyncOp} param={2} />
        </SharedState>
      );

      await act(wait);

      expect(state1).not.toEqual([0]);
      expect(state1).not.toEqual(state2);
    });

    test('should handle execute again within shared state', async () => {
      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([Math.random()]));
      let executeAgainShared: () => void;

      const Cmp2 = ({
        onChange,
        op
      }: {
        onChange: (v: any) => void;
        op: any;
      }) => {
        const { state, executeAgain } = useAsync(op, [1]);

        executeAgainShared = executeAgain;

        useEffect(() => {
          onChange(state.value);
        }, [state.value]);

        return null;
      };

      let state1: [number] = [-1];
      let state2: [number] = [-2];

      render(
        <SharedState>
          <Cmp2 onChange={val => (state1 = val)} op={asyncOp} />
          <Cmp2 onChange={val => (state2 = val)} op={asyncOp} />
        </SharedState>
      );

      await act(wait);
      await act(() => {
        executeAgainShared();
        return wait();
      });

      expect(state1).toBe(state2);
    });

    it('should use shared state for serving data when local cache strategy specified', async () => {
      let state1: [number] = [0];
      let counter = 0;

      const Cmp3 = ({
        onChange,
        op,
        param
      }: {
        onChange: (v: any) => void;
        op: any;
        param: number;
      }) => {
        const { state } = useAsync(op, [param], {
          cacheStrategy: UseAsyncCacheStrategy.LOCAL
        });

        useEffect(() => {
          onChange(state.value);
        }, [state.value]);

        return null;
      };

      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([++counter]));

      const { rerender } = render(
        <SharedState>
          <Cmp3 onChange={val => (state1 = val)} op={asyncOp} param={1} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toEqual([1]);

      rerender(
        <SharedState>
          <Cmp3 onChange={val => (state1 = val)} op={asyncOp} param={2} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toEqual([2]);

      rerender(
        <SharedState>
          <Cmp3 onChange={val => (state1 = val)} op={asyncOp} param={1} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toEqual([1]);
      expect(asyncOp).toHaveBeenCalledTimes(2);
    });

    it('should refetch data when local cache strategy specified, but it was removed', async () => {
      let state1: [number] = [0];
      let counter = 0;
      let removeValue!: () => void;

      const asyncOp = jest
        .fn()
        .mockImplementation(() => Promise.resolve([++counter]));

      const Cmp3 = ({ onChange }: { onChange: (v: any) => void }) => {
        const { state, removeAsyncValue } = useAsync(asyncOp, [], {
          cacheStrategy: UseAsyncCacheStrategy.LOCAL
        });
        removeValue = removeAsyncValue;
        useEffect(() => {
          onChange(state.value);
        }, [state.value]);

        return null;
      };

      const { rerender } = render(
        <SharedState>
          <Cmp3 onChange={val => (state1 = val)} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toEqual([1]);

      removeValue();
      await act(wait);

      rerender(
        <SharedState>
          <Cmp3 onChange={val => (state1 = val)} />
        </SharedState>
      );

      await act(wait);

      expect(state1).toEqual([2]);
    });
  });
});
