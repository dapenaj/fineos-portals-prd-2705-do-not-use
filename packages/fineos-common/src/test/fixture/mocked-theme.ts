/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { ThemeModel } from '../../lib/ui/theme';

export const mockedTheme: ThemeModel = {
  colorSchema: {
    primaryColor: '#932b6a',
    callToActionColor: '#e27816',
    primaryAction: '#007bc2',
    successColor: '#35b558',
    pendingColor: '#ffc20e',
    attentionColor: '#f04c3f',
    infoColor: '#23b7d6',

    neutral9: '#333333',
    neutral8: '#666666',
    neutral7: '#999999',
    neutral6: '#b3b3b3',
    neutral5: '#d2d0d5',
    neutral4: '#e9e7ec',
    neutral3: '#e6e6e6',
    neutral2: '#f4f5f6',
    neutral1: '#ffffff',

    graphColorOne: '#932b6a',
    graphColorTwo: '#00aabc',
    graphColorThree: '#ffc20e',
    graphColorFour: '#35b558',
    graphColorFive: '#f6861f',
    graphColorSix: '#708dd8'
  },
  typography: {
    displayTitle: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: 36,
      lineHeight: 44
    },
    displaySmall: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: 24,
      lineHeight: 30
    },
    pageTitle: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 24,
      lineHeight: 30
    },
    panelTitle: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 18,
      lineHeight: 25
    },
    baseText: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 14,
      lineHeight: 19
    },
    baseTextSemiBold: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 14,
      lineHeight: 19
    },
    smallText: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 12,
      lineHeight: 16
    },
    smallTextSemiBold: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 12,
      lineHeight: 16
    },
    baseTextUpperCase: {
      fontFamily: '"Open Sans"',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: 14,
      lineHeight: 19,
      textTransform: 'uppercase'
    },
    smallTextUpperCase: {
      fontFamily: '"Roboto"',
      fontStyle: 'italic',
      fontWeight: 'normal',
      fontSize: 11,
      lineHeight: 15,
      textTransform: 'uppercase'
    }
  },
  header: {
    logo: '',
    logoAltText: 'test alt text',
    hideTitle: true
  },
  images: {
    somethingWentWrong: 'somethingWentWrong',
    somethingWentWrongColor: 'somethingWentWrongColor',
    uploadFail: 'uploadFail',
    uploadSuccess: 'uploadSuccess',
    welcomeBackground: 'welcomeBackground',
    greenCheckIntake: 'greenCheckIntake',
    fnolRequestAcknowledged: 'fnolRequestAcknowledged',
    dataConcurrency: 'dataConcurrency'
  },
  icons: {},
  highContrastMode: {
    colorSchema: {
      primaryColor: '#932b6a',
      callToActionColor: '#e27816',
      primaryAction: '#007bc2',
      successColor: '#35b558',
      pendingColor: '#ffc20e',
      attentionColor: '#f04c3f',
      infoColor: '#23b7d6',

      neutral9: '#333333',
      neutral8: '#666666',
      neutral7: '#999999',
      neutral6: '#b3b3b3',
      neutral5: '#d2d0d5',
      neutral4: '#e9e7ec',
      neutral3: '#e6e6e6',
      neutral2: '#f4f5f6',
      neutral1: '#ffffff',

      graphColorOne: '#932b6a',
      graphColorTwo: '#00aabc',
      graphColorThree: '#ffc20e',
      graphColorFour: '#35b558',
      graphColorFive: '#f6861f',
      graphColorSix: '#708dd8'
    },
    typography: {
      displayTitle: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 36,
        lineHeight: 44
      },
      displaySmall: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 24,
        lineHeight: 30
      },
      pageTitle: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 24,
        lineHeight: 30
      },
      panelTitle: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 25
      },
      baseText: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 19
      },
      baseTextSemiBold: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 14,
        lineHeight: 19
      },
      smallText: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 16
      },
      smallTextSemiBold: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 12,
        lineHeight: 16
      },
      baseTextUpperCase: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 19,
        textTransform: 'uppercase'
      },
      smallTextUpperCase: {
        fontFamily: '"Roboto"',
        fontStyle: 'italic',
        fontWeight: 'normal',
        fontSize: 11,
        lineHeight: 15,
        textTransform: 'uppercase'
      }
    },
    header: {
      logo: '',
      hideTitle: true
    },
    icons: {
      'MESSAGES.EMPLOYER': `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" aria-label="MESSAGE.EMPLOYER icon">
        <path fill="currentColor" d="M248 8C111 8 0" />
      </svg>`,
      'MESSAGES.CARRIER': `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" aria-label="MESSAGE.CARRIER icon">
        <path fill="currentColor" d="M248 8C111 8 0" />
      </svg>`
    }
  }
};
