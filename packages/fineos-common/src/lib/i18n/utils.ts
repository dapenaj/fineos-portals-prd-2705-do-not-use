/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

/**
 * flattenMessages traverse nested messages object and return it
 * as a flat object
 *
 * @example
 * flattenMessages({
 *   A: {
 *     B: {
 *       C: 'TEST'
 *     },
 *     D: 'Hello!'
 *   }
 * }) == {
 *   'A.B.C': 'TEST',
 *   'A.D': 'Hello!',
 * }
 *
 * @remarks
 *
 * flattenMessages use recursion underneath for unwrapping nested objects
 *
 * @param object - nested messages object
 * @param [prefix=''] - prefix that should be prepended to each key in flat object, shouldn't be used by developer
 *
 * @returns flat messages object
 *
 * @private
 */
export const flattenMessages = (object: any = {}, prefix: string = '') =>
  Object.keys(object).reduce((transaltions: any, key) => {
    const value = object[key];
    const prefixedKey: any = prefix ? `${prefix}.${key}` : key;
    if (typeof value === 'string') {
      transaltions[prefixedKey] = value;
    } else {
      Object.assign(transaltions, flattenMessages(value, prefixedKey));
    }
    return transaltions;
  }, {});
