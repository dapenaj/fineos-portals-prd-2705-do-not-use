/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { ColProps } from 'antd/lib/col';

import { PossiblyFormattedMessageElement } from '../ui/formatted-message';

export type DefaultInputProps = {
  id?: string;
  name?: string;
  isDisabled?: boolean;
  'data-test-el'?: string;
  className?: string;
  wrapperClassName?: string;
  labelCol?: ColProps;
  wrapperCol?: ColProps;
  /**
   * Virtual group is allow to treat validation for few fields as one
   * virtualGroup should point to an object stored in formik
   */
  virtualGroup?: string;
  placeholder?: PossiblyFormattedMessageElement;
};
