/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Checkbox as AntCheckbox } from 'antd';
import { CheckboxChangeEvent as AntCheckboxChangeEvent } from 'antd/lib/checkbox';

import { getFocusedElementShadow } from '../../utils';
import { createThemedStyles, ThemeModel } from '../theme';

type CheckboxProps = React.ComponentProps<typeof AntCheckbox>;
export type CheckboxChangeEvent = AntCheckboxChangeEvent;

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  checkbox: {
    '& .ant-checkbox-inner': {
      borderColor: theme.colorSchema.neutral5
    },
    '& .ant-checkbox-input:focus + .ant-checkbox-inner': {
      boxShadow: getFocusedElementShadow({
        isWide: true,
        color: theme.colorSchema.primaryAction
      })
    },
    '& .ant-checkbox:hover .ant-checkbox-inner, .ant-checkbox-checked::after, .ant-checkbox-checked .ant-checkbox-inner': {
      borderColor: theme.colorSchema.primaryAction
    },
    '& .ant-checkbox-checked .ant-checkbox-inner::after': {
      backgroundColor: theme.colorSchema.primaryAction
    }
  }
}));

export const Checkbox: React.FC<CheckboxProps> = ({
  children,
  className,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <AntCheckbox
      className={classNames(themedStyles.checkbox, className)}
      {...props}
    >
      {children}
    </AntCheckbox>
  );
};
