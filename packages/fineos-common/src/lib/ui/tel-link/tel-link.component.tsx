/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isNil as _isNil, isString as _isString } from 'lodash';
import classNames from 'classnames';

import { mapPhoneToStr, Phone } from '../phone';
import { BasePhone } from '../phone/phone.type';
import { createThemedStyles, textStyleToCss } from '../theme';

type TelLinkProps = {
  phone: string | BasePhone;
  className?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  link: {
    ...textStyleToCss(theme.typography.baseTextSemiBold),
    color: theme.colorSchema.primaryColor
  }
}));

export const TelLink: React.FC<TelLinkProps> = ({
  phone,
  className,
  children
}) => {
  const phoneLink = mapPhoneToStr(phone);
  const themedStyles = useThemedStyles();

  return (
    <a
      href={`tel:${phoneLink}`}
      className={classNames(className, themedStyles.link)}
      target="_blank"
      rel="noopener noreferrer"
      data-test-el="tel-link"
    >
      {_isString(children) || _isNil(children) ? (
        <Phone>{children || phone}</Phone>
      ) : (
        children
      )}
    </a>
  );
};
