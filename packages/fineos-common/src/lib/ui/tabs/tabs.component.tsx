/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Tabs as AntTabs } from 'antd';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';
import { getFocusedElementShadow } from '../../utils';

import styles from './tabs.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  tabs: {
    '& .ant-tabs-tab': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8,
      '&.ant-tabs-tab-active .ant-tabs-tab-btn': {
        ...textStyleToCss(theme.typography.baseText),
        color: theme.colorSchema.primaryColor
      },
      '& .ant-tabs-tab-btn:focus': {
        borderColor: theme.colorSchema.primaryAction,
        boxShadow: getFocusedElementShadow({
          color: theme.colorSchema.primaryAction
        })
      }
    },
    '& .ant-tabs-ink-bar': {
      backgroundColor: theme.colorSchema.primaryColor
    }
  }
}));

type TabPaneProps = {
  hasNoMargin?: boolean;
} & React.ComponentProps<typeof AntTabs.TabPane>;

type TabsType = React.FC<React.ComponentProps<typeof AntTabs>> & {
  TabPane: React.FC<TabPaneProps>;
};

const Tabs: TabsType = ({ className, children, ...props }) => {
  const themedStyles = useThemedStyles();
  const existingChildren = Array.isArray(children)
    ? children.filter(Boolean)
    : children && typeof children === 'object'
    ? [children]
    : [];

  return existingChildren.length ? (
    <AntTabs
      className={classNames(className, styles.tabs, themedStyles.tabs)}
      {...props}
    >
      {existingChildren}
    </AntTabs>
  ) : null;
};

Tabs.TabPane = ({ hasNoMargin, className, ...props }: TabPaneProps) => (
  <AntTabs.TabPane
    className={classNames(className, {
      [styles.noTabPaneMargin]: hasNoMargin
    })}
    {...props}
  />
);

export { Tabs };
