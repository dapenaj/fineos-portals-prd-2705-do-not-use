/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import { InputNumber as AntNumberInput } from 'antd';
import classNames from 'classnames';
import { FieldProps, getIn } from 'formik';
import { InputProps as AntInputProps } from 'antd/lib/input';
import classnames from 'classnames';

import { DefaultInputProps } from '../../types';
import { FormGroupContext, getFocusedElementShadow } from '../../utils';
import { useRawFormattedMessage } from '../formatted-message';
import { createThemedStyles, textStyleToCss } from '../theme';
import { handleVirtualGroupBlurEvent } from '../text-input';

import styles from './number-input.module.scss';

type NumberInputProps = {
  /**
   * Optional attribute to determine if field should be focused after component mounts.
   * Defaults to false
   */
  isAutoFocusEnabled?: boolean;
  /**
   * Optional attribute to determine min value of input
   */
  minValue?: number;
  /**
   * Optional attribute to determine max value of input
   */
  maxValue?: number;
  /**
   * Optional attribute to determine precision of input
   */
  precision?: number;
  /**
   * Optional attribute to determine separator digit of decimal numbers
   */
  decimalSeparator?: string;
  /**
   * Optional attribute to determine step between following input values. Defaults to 1
   */
  step?: number;
  /**
   * Optional attribute to include an element after the number input. Works similar to the prop on TextInput
   */
  addonAfter?: React.ReactNode;
};

const useThemedStyles = createThemedStyles(theme => ({
  numberInputWrapper: {
    '& .ant-input-number-focused': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    },
    '& .ant-input-group .ant-input, .ant-input-group-addon': {
      borderColor: theme.colorSchema.neutral5,
      background: theme.colorSchema.neutral1,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  numberInput: {
    borderColor: theme.colorSchema.neutral5,
    '& .ant-input-number-handler-up-inner, .ant-input-number-handler-down-inner': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-input-number-input': {
      color: theme.colorSchema.neutral8,
      background: theme.colorSchema.neutral1,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-input-number-input::placeholder': {
      color: theme.colorSchema.neutral6,
      ...textStyleToCss(theme.typography.baseText)
    }
  }
}));

export const NumberInput: React.FC<NumberInputProps &
  DefaultInputProps &
  FieldProps &
  Omit<AntInputProps, 'min' | 'max' | 'onChange' | 'value' | 'prefix'>> = ({
  isAutoFocusEnabled,
  isDisabled,
  minValue,
  maxValue,
  field,
  form,
  meta,
  precision,
  decimalSeparator = '.',
  step,
  children,
  virtualGroup,
  addonAfter,
  placeholder,
  ...props
}) => {
  const isInvalid = getIn(form.errors, field.name);
  const { isRequired } = useContext(FormGroupContext);
  const inputPlaceholder = useRawFormattedMessage(placeholder);
  const themedStyles = useThemedStyles();
  return (
    <div
      className={classnames(themedStyles.numberInputWrapper, {
        'ant-input-wrapper ant-input-group': addonAfter // these classes come from antd
      })}
    >
      <AntNumberInput
        className={classNames(themedStyles.numberInput, styles.numberInput)}
        data-test-el={props['data-test-el'] || 'number-input'}
        name={field.name}
        id={field.name}
        autoFocus={isAutoFocusEnabled}
        defaultValue={field.value}
        disabled={isDisabled}
        min={minValue}
        max={maxValue}
        precision={precision}
        decimalSeparator={decimalSeparator}
        step={step}
        placeholder={inputPlaceholder}
        onChange={value => {
          // field.onchange expects a React event.
          // As we only have a value here we set the value explicitly.
          form.setFieldValue(field.name, value);
        }}
        onBlur={e => {
          field.onBlur(e);

          handleVirtualGroupBlurEvent(e, {
            virtualGroup,
            form,
            field,
            meta
          });
        }}
        aria-invalid={isInvalid ? 'true' : undefined}
        aria-required={isRequired}
        aria-describedby={props['aria-describedby']}
        {...props}
      />
      {addonAfter && (
        <span className="ant-input-group-addon">{addonAfter}</span> // the span class comes from antd
      )}
      {children}
    </div>
  );
};
