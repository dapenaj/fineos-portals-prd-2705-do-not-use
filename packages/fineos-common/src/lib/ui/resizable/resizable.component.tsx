/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useRef, useMemo } from 'react';
import classNames from 'classnames';
import {
  Resizable as ReResizable,
  ResizableProps as ReResizableProps
} from 're-resizable';

import { createThemedStyles } from '../theme';

import styles from './resizable.module.scss';

export type ResizableProps = Omit<
  ReResizableProps,
  'minHeight' | 'maxHeight' | 'defaultSize'
> & {
  step?: number;
  minHeight: number;
  maxHeight: number;
  defaultSize: {
    height: number;
    width: number | string;
  };
};

const useThemedStyles = createThemedStyles(theme => ({
  verticalHandler: {
    backgroundColor: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral7,

    '&:focus': {
      borderColor: theme.colorSchema.primaryAction
    }
  }
}));

// normally resizable renders resizer at the end, but we want to have it before content, so we need reverse
// the order of two last rendered elements
const ResizableRenderer = React.forwardRef(
  (
    { children, ...props }: React.HTMLAttributes<HTMLDivElement>,
    ref: Parameters<React.ForwardRefRenderFunction<HTMLDivElement>>[1]
  ) => {
    const childrenAsArray = children as React.ReactNodeArray;
    const childrenWithoutTail = childrenAsArray.slice(
      0,
      childrenAsArray.length - 2
    );
    const childrenTail = childrenAsArray.slice(childrenAsArray.length - 2);
    const newChildren = [...childrenWithoutTail, ...childrenTail.reverse()];

    return (
      <div ref={ref} {...props}>
        {newChildren}
      </div>
    );
  }
);

export const Resizable: React.FC<ResizableProps> = ({
  enable,
  maxHeight,
  minHeight,
  children,
  step = 20,
  defaultSize,
  handleClasses,
  ...props
}: any) => {
  const themedStyles = useThemedStyles();
  const resizableRef = useRef<ReResizable>(null);
  const verticalHandleRef = useRef<HTMLDivElement>(null);

  const handleComponent = useMemo(() => {
    if (enable?.top) {
      const verticalHandler = (
        <div
          ref={verticalHandleRef}
          className={classNames(
            themedStyles.verticalHandler,
            styles.verticalHandler
          )}
          tabIndex={0}
          onKeyDown={(e: React.KeyboardEvent) => {
            const height = resizableRef.current!.resizable?.offsetHeight!;

            switch (e.key) {
              case 'ArrowUp':
                resizableRef.current!.updateSize({
                  height: Math.min(height + step, maxHeight),
                  width: defaultSize.width
                });
                break;
              case 'ArrowDown':
                resizableRef.current!.updateSize({
                  height: Math.max(height - step, minHeight),
                  width: defaultSize.width
                });
                break;
            }
          }}
        />
      );
      return {
        top: verticalHandler
      };
    }
  }, [
    enable,
    maxHeight,
    minHeight,
    resizableRef,
    defaultSize,
    verticalHandleRef
  ]);

  return (
    <ReResizable
      as={ResizableRenderer}
      ref={resizableRef}
      {...props}
      defaultSize={defaultSize}
      enable={enable}
      maxHeight={maxHeight}
      minHeight={minHeight}
      handleClasses={classNames(handleClasses, styles.handle)}
      handleComponent={handleComponent}
      onResizeStop={() => {
        if (
          verticalHandleRef.current &&
          document.activeElement !== verticalHandleRef.current
        ) {
          verticalHandleRef.current.focus();
        }
      }}
    >
      {children}
    </ReResizable>
  );
};
