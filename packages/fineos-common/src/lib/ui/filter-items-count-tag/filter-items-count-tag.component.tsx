/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { Tag } from '../tag';
import { Icon } from '../icon';
import { RichFormattedMessage } from '../rich-formatted-message';
import { FormattedMessage } from '../formatted-message';
import { createThemedStyles } from '../theme';

import styles from './filter-items-count-tag.module.scss';

type FilterItemsCountTagProps = {
  onClearFilters?: () => void;
  filteredItemsCount: number;
  totalItemsCount: number;
  isClosable?: boolean;
  isVisible: boolean;
  className?: string;
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  icon: {
    color: theme.colorSchema.neutral1
  }
}));

export const FilterItemsCountTag: React.FC<FilterItemsCountTagProps> = ({
  onClearFilters,
  filteredItemsCount,
  totalItemsCount,
  isClosable = true,
  isVisible,
  className,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return isVisible ? (
    <Tag
      closable={isClosable}
      onClose={onClearFilters}
      className={classNames(styles.tag, className)}
      tabIndex={0}
      data-test-el={props['data-test-el'] || 'filter-items-count-tag'}
      closeIcon={
        <Icon
          onKeyDown={({ key }: React.KeyboardEvent<SVGElement>) => {
            if (key === 'Enter' && onClearFilters) {
              onClearFilters();
            }
          }}
          tabIndex={0}
          icon={faTimes}
          className={classNames(themedStyles.icon, styles.icon)}
          iconLabel={
            <FormattedMessage id="WIDGET.FILTER_COUNT_TAG_CLOSE_ICON_LABEL" />
          }
        />
      }
    >
      <RichFormattedMessage
        id="FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT"
        role="status"
        values={{
          filterCount: <strong>{filteredItemsCount}</strong>,
          count: <strong>{totalItemsCount}</strong>
        }}
      />
    </Tag>
  ) : null;
};
