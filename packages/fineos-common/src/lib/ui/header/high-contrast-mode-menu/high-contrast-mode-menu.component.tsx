/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect } from 'react';
import classNames from 'classnames';
import { faUniversalAccess } from '@fortawesome/free-solid-svg-icons';

import { SwitchLabel, UiSwitch } from '../../switch';
import { HeaderDropdown } from '../header-dropdown';
import { Icon } from '../../icon';
import { FormattedMessage } from '../../formatted-message';
import { createThemedStyles, textStyleToCss } from '../../theme';
import { encryptedUserId } from '../../../utils';

import styles from './high-contrast-mode-menu.module.scss';

type HighContrastModeMenuProps = {
  isContrastModeEnabled: boolean;
  onContrastModeChange: () => void;
  userId?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  menuWrapper: {
    backgroundColor: `${theme.colorSchema.neutral1} !important`,
    borderRight: `${theme.colorSchema.neutral1} !important`,
    '& .ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected': {
      backgroundColor: theme.colorSchema.neutral1
    },
    '& .ant-menu, .ant-menu-item, .ant-menu-item a': {
      borderRight: `${theme.colorSchema.neutral1} !important`,
      backgroundColor: `${theme.colorSchema.neutral1} !important`,
      color: `${theme.colorSchema.neutral8} !important`,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  description: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss(theme.typography.smallText)
  }
}));

export const HighContrastModeMenu: React.FC<HighContrastModeMenuProps> = ({
  isContrastModeEnabled,
  onContrastModeChange,
  userId
}) => {
  useEffect(() => {
    if (!userId) {
      return;
    }

    const localStorageValue = localStorage.getItem(
      encryptedUserId(userId, 'highContrastMode')
    );
    if (
      (localStorageValue && !isContrastModeEnabled) ||
      (!localStorageValue && isContrastModeEnabled)
    ) {
      onContrastModeChange();
    }
  }, []);

  useEffect(() => {
    if (!userId) {
      return;
    }

    isContrastModeEnabled
      ? localStorage.setItem(
          encryptedUserId(userId, 'highContrastMode'),
          'enabled'
        )
      : localStorage.removeItem(encryptedUserId(userId, 'highContrastMode'));
  }, [isContrastModeEnabled]);

  const themedStyles = useThemedStyles();
  return (
    <HeaderDropdown
      label={
        <Icon
          icon={faUniversalAccess}
          iconLabel={
            <FormattedMessage id="FINEOS_COMMON.HIGH_CONTRAST_MODE.MENU_TOGGLE_ICON" />
          }
          className={styles.accessibilityIcon}
        />
      }
      placement="bottomRight"
      shouldCloseOnOverlayClick={false}
      overlayClassName={classNames(
        themedStyles.menuWrapper,
        styles.menuWrapper
      )}
    >
      <div className={styles.menu}>
        <UiSwitch
          isChecked={isContrastModeEnabled}
          onChange={onContrastModeChange}
          className={styles.highContrastSwitch}
          size="small"
        />
        <SwitchLabel>
          <FormattedMessage id="FINEOS_COMMON.HIGH_CONTRAST_MODE.SWITCH_LABEL" />
        </SwitchLabel>
        <FormattedMessage
          id="FINEOS_COMMON.HIGH_CONTRAST_MODE.DESCRIPTION"
          className={classNames(themedStyles.description, styles.description)}
        />
      </div>
    </HeaderDropdown>
  );
};
