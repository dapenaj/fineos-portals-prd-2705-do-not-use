/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Layout } from 'antd';
import classNames from 'classnames';

import { ContentLayout } from '../layouts/content-layout';
import { Initials } from '../initials';
import { SkipToContentButton } from '../skip-to-content-button';
import {
  contrastTextColor,
  createThemedStyles,
  primaryDarkColor,
  textStyleToCss,
  useTheme
} from '../theme';
import { MAIN_CONTENT_ELEMENT_ID } from '../layouts';
import { FormattedMessage } from '../formatted-message';

import { HeaderDropdown } from './header-dropdown';
import { HighContrastModeMenu } from './high-contrast-mode-menu';
import styles from './header.module.scss';

const { Header: AntHeader } = Layout;

type HeaderViewProps = {
  userName?: string;
  userId?: string;
  userOptions?: React.ReactElement[];
  rightContent?: React.ReactChild;
  isContrastModeEnabled?: boolean;
  onContrastModeToggle?: (isEnabled: boolean) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    background: `linear-gradient(
        90deg,
        ${theme.colorSchema.primaryColor} 0%,
        ${primaryDarkColor(theme.colorSchema)} 100%);
      `,
    color:
      theme.header.titleColor ||
      contrastTextColor(theme, theme.colorSchema.primaryColor)
  },
  userLabel: textStyleToCss(theme.typography.baseText),
  title: {
    ...(theme.header.titleStyles
      ? textStyleToCss(theme.header.titleStyles)
      : {}),
    fontSize:
      theme.header.titleStyles?.fontSize || theme.typography.baseText.fontSize
  }
}));

export const Header: React.FC<HeaderViewProps> = ({
  userName,
  userId,
  userOptions,
  rightContent,
  isContrastModeEnabled,
  onContrastModeToggle
}) => {
  const theme = useTheme();
  const themedStyles = useThemedStyles();

  return (
    <div
      className={classNames(styles.header, themedStyles.header)}
      data-test-el="header"
      role="banner"
    >
      <ContentLayout className={styles.wrapper}>
        <AntHeader>
          <div className={styles.leftBar} data-test-el="left-bar">
            <img
              alt={theme.header.logoAltText}
              src={theme.header.logo}
              className={styles.logo}
            />
            {theme.header.hideTitle !== true && (
              <>
                {theme.header.titleSeparator && (
                  <span className={themedStyles.title}>
                    &nbsp;
                    {theme.header.titleSeparator}
                    &nbsp;
                  </span>
                )}
                <div
                  className={classNames(themedStyles.title, {
                    [styles.spacedTitle]: !theme.header.titleSeparator
                  })}
                >
                  {theme.header.title}
                </div>
              </>
            )}
            <SkipToContentButton to={`#${MAIN_CONTENT_ELEMENT_ID}`}>
              <FormattedMessage id="FINEOS_COMMON.HEADER.SKIP_TO_MAIN_CONTENT" />
            </SkipToContentButton>
          </div>
          {(rightContent || (userOptions && userName)) && (
            <div className={styles.rightBar} data-test-el="right-bar">
              {rightContent && (
                <div className={styles.rightBarSection}>{rightContent}</div>
              )}
              {userOptions && userName && (
                <div className={styles.rightBarSection}>
                  <Initials username={userName} />

                  <HeaderDropdown
                    label={
                      <span
                        className={classNames(
                          themedStyles.userLabel,
                          styles.userLabel
                        )}
                        data-test-el="user-options-dropdown-label"
                      >
                        {userName}
                      </span>
                    }
                  >
                    {userOptions}
                  </HeaderDropdown>
                  {onContrastModeToggle && (
                    <HighContrastModeMenu
                      onContrastModeChange={() =>
                        onContrastModeToggle(!isContrastModeEnabled)
                      }
                      isContrastModeEnabled={!!isContrastModeEnabled}
                      userId={userId}
                    />
                  )}
                </div>
              )}
            </div>
          )}
        </AntHeader>
      </ContentLayout>
    </div>
  );
};
