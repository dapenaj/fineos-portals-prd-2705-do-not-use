/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Dropdown as AntDropdown } from 'antd';
import { DropDownProps as AntDropdownProps } from 'antd/lib/dropdown';
import { CaretDownOutlined } from '@ant-design/icons';

import { Menu } from '../../menu';
import { createThemedStyles, textStyleToCss } from '../../theme';

import styles from './header-dropdown.module.scss';

type DropdownProps = {
  label: React.ReactElement;
  shouldCloseOnOverlayClick?: boolean;
  children: React.ReactElement | React.ReactElement[];
};

const useThemedStyles = createThemedStyles(theme => ({
  dropdownOverlay: {
    ...textStyleToCss(theme.typography.baseText),
    '& .ant-menu': {
      backgroundColor: theme.colorSchema.neutral1,
      borderRightColor: theme.colorSchema.neutral1,
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-menu-item a, .ant-menu-item .ant-btn': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText),
      '&:hover': {
        color: theme.colorSchema.primaryAction
      }
    },
    '& .ant-menu-item:hover': {
      color: theme.colorSchema.primaryAction
    },
    '& .ant-menu-item > *:focus': {
      color: theme.colorSchema.primaryAction
    }
  }
}));

export const HeaderDropdown: React.FC<DropdownProps &
  Omit<AntDropdownProps, 'overlay'>> = ({
  label,
  shouldCloseOnOverlayClick = true,
  placement,
  children,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const [isVisible, setIsVisible] = useState(false);

  return (
    <div
      onKeyDown={({ key }: React.KeyboardEvent<HTMLSpanElement>) => {
        key === 'Escape' && setIsVisible(false);
      }}
    >
      <AntDropdown
        overlay={
          <Menu
            onClick={() => {
              if (shouldCloseOnOverlayClick) {
                setIsVisible(false);
              }
            }}
            className={styles.menu}
          >
            {children}
          </Menu>
        }
        placement={placement}
        visible={isVisible}
        onVisibleChange={isDropdownVisible => setIsVisible(isDropdownVisible)}
        overlayClassName={themedStyles.dropdownOverlay}
        trigger={['click']}
        getPopupContainer={(node: HTMLElement | undefined) => {
          if (node) {
            return node.parentNode as HTMLElement;
          }
          return document.body as HTMLElement;
        }}
        {...props}
      >
        <span
          className={styles.trigger}
          tabIndex={0}
          onKeyDown={({ key }: React.KeyboardEvent<HTMLSpanElement>) => {
            key === 'Enter' && setIsVisible(true);
          }}
        >
          {label}
          {!!(children as React.ReactElement[])?.length && (
            <CaretDownOutlined className={styles.icon} />
          )}
        </span>
      </AntDropdown>
    </div>
  );
};
