/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Upload } from 'antd';
import {
  UploadFileStatus,
  UploadChangeParam,
  UploadFile,
  RcFile
} from 'antd/lib/upload/interface';
import classNames from 'classnames';

import { Spinner } from '../spinner';
import { Button } from '../button';
import { UploadInput } from '../upload-input';
import { createThemedStyles, textStyleToCss } from '../theme';
import { getFileExtension } from '../../utils';
import { ButtonType } from '../../types';
import { FormattedMessage } from '../formatted-message';

import { UploadedFile } from './file-upload.type';
import styles from './file-upload.module.scss';

// this is hard limit, it's shouldn't be even possible to configure
// file size bigger than 5MB
const MAX_FILE_SIZE = 5 * 1024 ** 2; // 5MB

const fileSizeToBytes = (fileSize: string) => {
  const parseResult = /^([\d,]+)([KM]?B)?$/.exec(fileSize.toUpperCase());

  if (!parseResult) {
    return MAX_FILE_SIZE;
  }

  const [, rawSize, rawUnits] = parseResult;
  const units = rawUnits ?? 'B';
  const size = parseInt(rawSize.replace(/,/g, ''), 10);
  let calculatedSize = MAX_FILE_SIZE;

  switch (units) {
    case 'B':
      calculatedSize = size;
      break;
    case 'KB':
      calculatedSize = size * 1024;
      break;
    case 'MB':
      calculatedSize = size * 1024 ** 2;
      break;
  }

  return Math.min(MAX_FILE_SIZE, calculatedSize);
};

const formatBytes = (bytes: number) =>
  bytes.toLocaleString(undefined, {
    minimumFractionDigits: 0,
    maximumFractionDigits: 2
  });

const fileSizeInBytesToFileSize = (bytes: number) => {
  if (bytes < 1024) {
    return `${formatBytes(bytes)} B`;
  }

  if (bytes < 1024 ** 2) {
    return `${formatBytes(bytes / 1024)} KB`;
  }

  if (bytes < 1024 ** 3) {
    return `${formatBytes(bytes / 1024 ** 2)} MB`;
  }

  return `${formatBytes(bytes / 1024 ** 3)} GB`;
};

const useThemedStyles = createThemedStyles(theme => ({
  error: {
    color: theme.colorSchema.attentionColor,
    ...textStyleToCss(theme.typography.baseText)
  },
  textBase: {
    ...textStyleToCss(theme.typography.baseText)
  },
  upload: {
    '& .ant-upload-list-item-name': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  }
}));

type FileUploadContentType = 'BLOB' | 'TEXT';

type FileUploadProps = {
  accept: string[];
  uploadedMediaTypeLabel?: string;
  isLoading: boolean;
  maxFileSize: string;
  onCancel: () => void;
  onSubmit: (data: UploadedFile) => void;
  contentType?: FileUploadContentType;
};

// This table of known type aliases which should be presented together
const KNOWN_FORMAT_ALIASES: [string[], string][] = [
  [['DOC', 'DOCX'], 'DOC(X)'],
  [['JPG', 'JPEG'], 'JP(E)G'],
  [['XLS', 'XLSX'], 'XLS(X)'],
  [['XLS', 'XLSM'], 'XLS(M)'],
  [['XLS', 'XLSX', 'XLSM'], 'XLS(S/M)'],
  [['PPT', 'PPTX'], 'PPT(X)']
];

const getKnownFormatAliasGroups = (format: string): [string[], string][] => {
  const sortedGroups = KNOWN_FORMAT_ALIASES.filter(([formats]) =>
    formats.includes(format)
  )
    // we want to ensure that original format is not presented in known aliases
    .map(
      ([formats, alias]) =>
        [formats.filter(aliasFormat => aliasFormat !== format), alias] as [
          string[],
          string
        ]
    );

  // we need to put on top groups with greater amount of possible aliases
  sortedGroups.sort(([first], [second]) => second.length - first.length);

  return sortedGroups;
};

const compressFormats = (formats: string[]) => {
  const cleanFormats = formats.map(format =>
    format.toUpperCase().replace('.', '')
  );
  const joinedFormats: string[] = [];

  // tslint:disable-next-line
  for (let i = 0; i < cleanFormats.length; i++) {
    const currentCleanFormat = cleanFormats[i];
    const knownFormatAliasGroups = getKnownFormatAliasGroups(
      currentCleanFormat
    );

    if (knownFormatAliasGroups.length) {
      let foundAlias = false;

      // tslint:disable-next-line
      for (let j = 0; j < knownFormatAliasGroups.length && !foundAlias; j++) {
        const [aliases, aliasName] = knownFormatAliasGroups[j];
        const totalAliasesMet = aliases.reduce(
          (aliasesCount, alias) =>
            cleanFormats.includes(alias) ? aliasesCount + 1 : aliasesCount,
          0
        );

        if (totalAliasesMet === aliases.length) {
          for (const metAlias of aliases) {
            cleanFormats.splice(cleanFormats.indexOf(metAlias), 1);
          }
          joinedFormats.push(aliasName);
          foundAlias = true;
        }
      }

      if (!foundAlias) {
        joinedFormats.push(currentCleanFormat);
      }
    } else {
      joinedFormats.push(currentCleanFormat);
    }
  }

  return joinedFormats.map(format => '.' + format);
};

export const FileUpload: React.FC<FileUploadProps> = ({
  accept,
  uploadedMediaTypeLabel = 'Document',
  contentType = 'BLOB',
  children,
  isLoading,
  maxFileSize,
  onCancel,
  onSubmit
}) => {
  const acceptFormatsLabel = compressFormats(accept);
  const lastAcceptedFormatLabel = acceptFormatsLabel.pop();
  const themedStyles = useThemedStyles();
  const maxFileSizeInBytes = fileSizeToBytes(maxFileSize);

  const [fileStatus, setStatus] = useState<UploadFileStatus | undefined | null>(
    null
  );
  const [fileType, setFileType] = useState<string | null>(null);
  const [uploadedFile, setUploadedFile] = useState<string | null>(null);
  const [fileName, setFileName] = useState<string | null>(null);
  const [fileValidExtension, setFileValidExtension] = useState<boolean>(true);
  const [uploadedFileList, setUploadedFileList] = useState<UploadFile[]>([]);
  const [currentFileSize, setCurrentFileSize] = useState(0);

  const handleOnUploadChange = ({
    file: { status, name, originFileObj, size },
    fileList
  }: UploadChangeParam<UploadFile>) => {
    setStatus(status);
    if (status === 'removed') {
      refreshUploadData();
    } else {
      const reader = new FileReader();
      const extension = getFileExtension(name);
      // remove '.'
      const type = extension.substring(1);
      // get filename without extension
      const filename = name.slice(0, name.lastIndexOf('.'));

      setUploadedFileList(fileList);
      setFileType(type);
      setFileName(filename);
      setFileValidExtension(
        extension === '' ? true : accept.includes(extension.toLocaleLowerCase())
      );
      setCurrentFileSize(size);

      if (status !== 'uploading' && originFileObj) {
        reader.onload = () => {
          const result = reader.result as string;

          if (contentType === 'TEXT') {
            setUploadedFile(result);
          } else {
            // only want the data after the type
            const [, data] = result.split(',');
            setUploadedFile(data);
          }
        };

        if (contentType === 'TEXT') {
          reader.readAsText(originFileObj, 'utf8');
        } else {
          reader.readAsDataURL(originFileObj);
        }
      }
    }
  };

  const handleSubmit = () => {
    onSubmit({
      type: fileType!,
      file: uploadedFile!,
      name: fileName!
    });
    refreshUploadData();
  };

  const refreshUploadData = () => {
    // reset the upload
    setCurrentFileSize(0);
    setUploadedFileList([]);
    setStatus(null);
    setFileType(null);
    setFileValidExtension(true);
    setUploadedFile(null);
    setFileName(null);
  };

  const handleBeforeUpload = (rcFile: RcFile, rcFileList: RcFile[]) => {
    setStatus('done');
    return true;
  };

  /*
   * https://stackoverflow.com/questions/51514757/action-function-is-required-with-antd-upload-control-but-i-dont-need-it
   */
  const overrideAction = (response: any) =>
    setTimeout(() => {
      response.onSuccess('ok');
    }, 0);

  const isPending = isLoading || fileStatus === 'uploading';
  const uploadDisabled =
    isLoading ||
    !uploadedFile ||
    !fileValidExtension ||
    currentFileSize > maxFileSizeInBytes;

  return (
    <div className={styles.wrapper} data-test-el="file-upload">
      <div
        data-test-el="upload-module-name"
        className={classNames(styles.name, themedStyles.textBase)}
      >
        {children}
      </div>

      <Upload
        accept={accept.join()}
        beforeUpload={handleBeforeUpload}
        customRequest={overrideAction}
        onChange={info => handleOnUploadChange(info)}
        fileList={uploadedFileList}
        aria-label={uploadedMediaTypeLabel}
        className={classNames(styles.upload, themedStyles.upload)}
      >
        {isPending && <Spinner />}

        {fileValidExtension && fileStatus !== 'done' && !isPending && (
          <UploadInput uploadedMediaTypeLabel={uploadedMediaTypeLabel} />
        )}
      </Upload>

      <div className={classNames(styles.extensions, themedStyles.textBase)}>
        {acceptFormatsLabel.length ? (
          <FormattedMessage
            id="FINEOS_COMMON.UPLOAD.VALID_EXTENSIONS"
            role="alert"
            aria-invalid={true}
            values={{
              acceptedFormats: acceptFormatsLabel.join(', '),
              lastAcceptedFormat: lastAcceptedFormatLabel
            }}
          />
        ) : (
          <FormattedMessage
            id="FINEOS_COMMON.UPLOAD.SINGLE_VALID_EXTENSION"
            role="alert"
            aria-invalid={true}
            values={{
              acceptedFormat: lastAcceptedFormatLabel
            }}
          />
        )}
      </div>

      <div
        className={classNames(styles.extensions, themedStyles.textBase, {
          [themedStyles.error]: currentFileSize > maxFileSizeInBytes
        })}
      >
        {currentFileSize > maxFileSizeInBytes ? (
          <FormattedMessage
            role="alert"
            aria-invalid={true}
            id="FINEOS_COMMON.VALIDATION.MAX_FILE_SIZE"
            values={{
              maxFileSize: fileSizeInBytesToFileSize(maxFileSizeInBytes),
              fileSize: fileSizeInBytesToFileSize(currentFileSize)
            }}
          />
        ) : (
          <FormattedMessage
            id="FINEOS_COMMON.UPLOAD.MAX_FILE_SIZE"
            values={{
              maxFileSize: fileSizeInBytesToFileSize(maxFileSizeInBytes)
            }}
          />
        )}
      </div>

      {!fileValidExtension && (
        <p
          role="alert"
          aria-invalid={true}
          className={classNames(styles.error, themedStyles.error)}
        >
          <FormattedMessage id="FINEOS_COMMON.VALIDATION.FILE_EXTENSION" />
        </p>
      )}

      <div className={styles.controls}>
        <FormattedMessage
          as={Button}
          buttonType={ButtonType.PRIMARY}
          disabled={uploadDisabled}
          onClick={handleSubmit}
          id="FINEOS_COMMON.UPLOAD.UPLOAD_BUTTON"
          values={{ uploadedMediaType: uploadedMediaTypeLabel }}
        />

        <FormattedMessage
          as={Button}
          buttonType={ButtonType.LINK}
          onClick={onCancel}
          disabled={isPending}
          data-test-el="cancel-document-upload-btn"
          id="FINEOS_COMMON.UPLOAD.CANCEL"
        />
      </div>
    </div>
  );
};
