/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Moment } from 'moment';
import { ScaleTime } from 'd3-scale';

type GanttMonthBordersProps = {
  months: Moment[];
  scale: ScaleTime<number, number>;
  monthClassName: string;
  render?: (month: Moment) => React.ReactNode;
  lastMonthClassName: string;
};

const GanttMonths: React.FC<GanttMonthBordersProps> = ({
  months,
  scale,
  monthClassName,
  lastMonthClassName,
  render
}) => {
  const lastMonthIndex = months.length - 1;
  const isOneMonth = lastMonthIndex === 0;
  const monthBorders = months.map((month, i) => {
    const previousOffset = i && scale(months[i - 1].clone().endOf('month'));

    return (
      <div
        className={classNames(monthClassName, {
          [lastMonthClassName]: lastMonthIndex === i
        })}
        key={month.valueOf()}
        style={{
          left: previousOffset,
          width: isOneMonth
            ? scale.range()[1]
            : scale(month.clone().endOf('month')) - previousOffset
        }}
      >
        {render && render(month)}
      </div>
    );
  });

  return <>{monthBorders}</>;
};

const GanttMonthsMemoized = React.memo(GanttMonths);

export { GanttMonthsMemoized as GanttMonths };
