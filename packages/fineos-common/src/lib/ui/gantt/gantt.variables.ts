/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

// this file should be in sync with ./gantt.variables.scss
// unfortunately :export syntax works for webpack only, not for rollup

export const DEFAULT_SPACING = 8; // $default-spacing
export const GROUP_MARGIN_PX = DEFAULT_SPACING; // $gantt-row-margin
export const ROW_HEIGHT_PX = 32; // $gantt-row-height
export const TODAY_TOOLTIP_WIDTH_PX = 70; // $today-tooltip-width
export const TIME_ENTRY_SIZE_PX = 16; // $gantt-time-entry-size
export const BORDER_SIZE_PX = 1; // $gantt-border-size
// value was identified using empirical method
export const DAY_RESERVED_SPACE_PX = 22;
