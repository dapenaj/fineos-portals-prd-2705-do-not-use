/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Moment } from 'moment';

export enum GanttEntryShape {
  FILLED = 'filled',
  STRIPES = 'stripes',
  SEMI_FILLED = 'semi-filled'
}

export enum GanttEntryColor {
  GREEN = 'green',
  YELLOW = 'yellow',
  RED = 'red',
  GREY = 'grey',
  WHITE = 'white'
}

type GanttEntryBase = {
  id: any;
  color: GanttEntryColor;
  shape: GanttEntryShape;
  tooltip?: React.ReactNode;
};

export type GanttEntryTimeless = {
  timeless: true;
  label?: React.ReactNode;
} & GanttEntryBase;

export type GanttEntryNormal = {
  timeless?: false;
  startDate: Moment;
  endDate: Moment;
} & GanttEntryBase;

export type GanttEntry = GanttEntryTimeless | GanttEntryNormal;

export type GanttGroupRow = {
  id: any;
  title: React.ReactNode;
  entries: GanttEntry[];
  disabled?: boolean;
};

export type GanttGroup = {
  id: any;
  title: React.ReactNode;
  rows: GanttGroupRow[];
};

export type RowBox = {
  height: number;
  top: number;
};

export type HeightBoxesRegistry = {
  mainRowBox: RowBox;
  subRowBoxes: RowBox[];
}[];
