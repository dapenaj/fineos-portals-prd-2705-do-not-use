/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GanttEntryColor, GanttEntryShape } from '../gantt.type';
import {
  Color,
  createThemedStyles,
  lightenColor,
  ThemedColorSchema
} from '../../theme';

interface GanttTimeBarsColors {
  color: keyof ThemedColorSchema;
  borderColor?: keyof ThemedColorSchema;
  lighterColor?: Color;
}

const STRIPE_DISTANCE_PX = 12;
const produceShapeStyles = (
  colorScheme: ThemedColorSchema,
  {
    color,
    lighterColor = lightenColor(colorScheme, color),
    borderColor
  }: GanttTimeBarsColors
) => ({
  [`&.${GanttEntryShape.FILLED}`]: {
    backgroundColor: colorScheme[color],
    border: borderColor ? `1px solid ${colorScheme[borderColor]}` : 'none'
  },
  [`&.${GanttEntryShape.STRIPES}`]: {
    background: `repeating-linear-gradient(
      -45deg,
      ${lighterColor},
      ${lighterColor} ${STRIPE_DISTANCE_PX}px,
      ${colorScheme[color]} ${STRIPE_DISTANCE_PX}px,
      ${colorScheme[color]} ${STRIPE_DISTANCE_PX * 2 + 1}px
    )`
  },
  [`&.${GanttEntryShape.SEMI_FILLED}`]: {
    background: `linear-gradient(
      to bottom,
      ${lighterColor},
      ${lighterColor} 50%,
      ${colorScheme[color]} 50%,
      ${colorScheme[color]}
    )`
  }
});

export const useGanttTimeBarsThemedStyles = createThemedStyles(theme => ({
  [GanttEntryColor.GREEN]: produceShapeStyles(theme.colorSchema, {
    color: 'successColor'
  }),
  [GanttEntryColor.YELLOW]: produceShapeStyles(theme.colorSchema, {
    color: 'pendingColor'
  }),
  [GanttEntryColor.RED]: produceShapeStyles(theme.colorSchema, {
    color: 'attentionColor'
  }),
  [GanttEntryColor.GREY]: produceShapeStyles(theme.colorSchema, {
    color: 'neutral6'
  }),
  [GanttEntryColor.WHITE]: produceShapeStyles(theme.colorSchema, {
    color: 'neutral1',
    borderColor: 'neutral5'
  })
}));
