/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { Popover } from 'antd';

import { TIME_ENTRY_SIZE_PX } from '../gantt.variables';
import { GanttEntryColor, GanttEntryShape } from '../gantt.type';
import { createThemedStyles, textStyleToCss } from '../../theme';

import { useGanttTimeBarsThemedStyles } from './gantt-time-bars-styles';
import styles from './gantt-time-bars.module.scss';

type GanttEntryTimelessTimeBarProps = {
  top: number;
  height: number;
  color: GanttEntryColor;
  shape: GanttEntryShape;
  tooltip?: React.ReactNode;
  label?: React.ReactNode;
  fixedCanvasRef: React.RefObject<HTMLElement>;
};

const useThemedStyles = createThemedStyles(theme => ({
  timeBarLabel: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

// internal template component
const GanttEntryTimelessTimeBar: React.FC<GanttEntryTimelessTimeBarProps> = ({
  height,
  top,
  tooltip,
  color,
  shape,
  label,
  fixedCanvasRef
}) => {
  const topOffset = top + (height - TIME_ENTRY_SIZE_PX) / 2;
  const ganttTimeBarsThemedStyles = useGanttTimeBarsThemedStyles();
  const themedStyles = useThemedStyles();

  const timeBar = (
    <div
      className={classNames(
        styles.singleEntryTimeBar,
        ganttTimeBarsThemedStyles[color],
        shape
      )}
      style={{
        top: topOffset
      }}
    />
  );

  // Timeless time bar should be "fixed" per canvas, which means that scrolling shouldn't affect them.
  // It means that it should be rendered outside of normal canvas - the simplest way to achieve this
  // is to use React Portal
  return ReactDOM.createPortal(
    <>
      {tooltip ? (
        <Popover content={tooltip} overlayClassName={styles.tooltipContainer}>
          {timeBar}
        </Popover>
      ) : (
        timeBar
      )}

      {label && (
        <div
          className={classNames(styles.timeBarLabel, themedStyles.timeBarLabel)}
          style={{
            top: topOffset,
            left: TIME_ENTRY_SIZE_PX * 1.5
          }}
        >
          {label}
        </div>
      )}
    </>,
    fixedCanvasRef.current!
  );
};

const GanttEntryTimelessTimeBarMemoized = React.memo(GanttEntryTimelessTimeBar);

export { GanttEntryTimelessTimeBarMemoized as GanttEntryTimelessTimeBar };
