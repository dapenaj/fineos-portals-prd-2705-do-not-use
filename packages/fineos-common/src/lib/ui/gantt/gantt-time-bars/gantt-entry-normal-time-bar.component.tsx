/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Popover } from 'antd';

import { TIME_ENTRY_SIZE_PX } from '../gantt.variables';
import { GanttEntryColor, GanttEntryShape } from '../gantt.type';
import { createThemedStyles } from '../../theme';

import { useGanttTimeBarsThemedStyles } from './gantt-time-bars-styles';
import styles from './gantt-time-bars.module.scss';

type GanttEntryNormalTimeBarProps = {
  top: number;
  height: number;
  left: number;
  width: number;
  color: GanttEntryColor;
  shape: GanttEntryShape;
  tooltip?: React.ReactNode;
  disableStartProjectionLine: boolean;
  disableEndProjectionLine: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  durationTimeBar: {
    '&:before': {
      borderLeftColor: theme.colorSchema.neutral1
    },
    '&:after': {
      borderRightColor: theme.colorSchema.neutral1
    }
  },
  projectionLine: {
    borderLeftColor: theme.colorSchema.neutral4
  }
}));

// internal template component
const GanttEntryNormalTimeBar: React.FC<GanttEntryNormalTimeBarProps> = ({
  top,
  height,
  left,
  width,
  tooltip,
  color,
  shape,
  disableStartProjectionLine,
  disableEndProjectionLine
}) => {
  const topOffset = top + (height - TIME_ENTRY_SIZE_PX) / 2;
  const projectLineHeight = topOffset + (height - TIME_ENTRY_SIZE_PX) / 2;
  const ganttTimeBarsThemedStyles = useGanttTimeBarsThemedStyles();
  const themedStyles = useThemedStyles();

  const timeBar = (
    <div
      className={classNames(
        styles.durationTimeBar,
        themedStyles.durationTimeBar,
        ganttTimeBarsThemedStyles[color],
        shape
      )}
      style={{
        top: topOffset,
        left,
        width
      }}
    />
  );

  // as duration bar may not fit in gantt view, and regular ant tooltip placement can only have fixed options
  // like left, center, right - which doesn't solve this use case.
  // ant tooltip based, on rc-tooltip, which is based on rc-trigger and has option `alignPoint: boolean` (default false)
  // setting this option to true will force showing tooltip based on mouse position, which always gonna be in
  // gantt view. unfortunately ant doesn't have proper type definition for all props which it's actually supports
  // that's why `any` type is required here
  const popoverConfig: any = {
    alignPoint: true
  };

  return (
    <>
      {!disableStartProjectionLine && (
        <div
          className={classNames(
            styles.projectionLine,
            themedStyles.projectionLine
          )}
          style={{
            height: projectLineHeight,
            left
          }}
        />
      )}
      {tooltip ? (
        <Popover
          content={tooltip}
          overlayClassName={styles.tooltipContainer}
          {...popoverConfig}
        >
          {timeBar}
        </Popover>
      ) : (
        timeBar
      )}

      {!disableEndProjectionLine && (
        <div
          className={classNames(
            styles.projectionLine,
            themedStyles.projectionLine
          )}
          style={{
            height: projectLineHeight,
            left: left + width
          }}
        />
      )}
    </>
  );
};

const GanttEntryNormalTimeBarMemoized = React.memo(GanttEntryNormalTimeBar);

export { GanttEntryNormalTimeBarMemoized as GanttEntryNormalTimeBar };
