/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { ScaleTime } from 'd3-scale';
import { flatten as _flatten, sortBy as _sortBy } from 'lodash';

import {
  GanttEntry,
  GanttEntryColor,
  GanttGroup,
  HeightBoxesRegistry,
  GanttEntryTimeless
} from '../gantt.type';

import { GanttEntryNormalTimeBar } from './gantt-entry-normal-time-bar.component';
import { GanttEntryTimelessTimeBar } from './gantt-entry-timeless-time-bar.component';

type GanttGroupBordersProps = {
  groups: GanttGroup[];
  heightBoxesRegistry: HeightBoxesRegistry;
  scale: ScaleTime<number, number>;
  fixedCanvasRef: React.RefObject<HTMLElement>;
};

const sortedColors = [
  GanttEntryColor.GREEN,
  GanttEntryColor.YELLOW,
  GanttEntryColor.RED,
  GanttEntryColor.WHITE,
  GanttEntryColor.GREY
];

const sortEntries = (entries: GanttEntry[]): GanttEntry[] =>
  _sortBy(entries, entry => -sortedColors.indexOf(entry.color));

const GanttTimeBars: React.FC<GanttGroupBordersProps> = ({
  groups,
  heightBoxesRegistry,
  scale,
  fixedCanvasRef
}) => {
  const timeBars: React.ReactNode[] = [];

  for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
    const group = groups[groupIndex];
    const groupHeightBox = heightBoxesRegistry[groupIndex];
    const isExpanded = groupHeightBox.subRowBoxes.length > 0;

    if (isExpanded) {
      for (let rowIndex = 0; rowIndex < group.rows.length; rowIndex++) {
        const row = group.rows[rowIndex];
        const rowHeightBox = groupHeightBox.subRowBoxes[rowIndex];
        const orderedEntries = sortEntries(row.entries);

        for (const entry of orderedEntries) {
          if (!entry.timeless) {
            timeBars.push(
              <GanttEntryNormalTimeBar
                key={entry.id}
                top={rowHeightBox.top}
                height={rowHeightBox.height}
                left={scale(entry.startDate)}
                width={scale(entry.endDate) - scale(entry.startDate)}
                color={entry.color}
                shape={entry.shape}
                tooltip={entry.tooltip}
                disableStartProjectionLine={entry.startDate.isSame(
                  entry.startDate.clone().startOf('month')
                )}
                disableEndProjectionLine={entry.endDate.isSame(
                  entry.endDate.clone().startOf('month')
                )}
              />
            );
          } else {
            timeBars.push(
              <GanttEntryTimelessTimeBar
                key={entry.id}
                label={entry.label}
                height={rowHeightBox.height}
                top={rowHeightBox.top}
                color={entry.color}
                shape={entry.shape}
                tooltip={entry.tooltip}
                fixedCanvasRef={fixedCanvasRef}
              />
            );
          }
        }
      }
    } else {
      // as we assume that higher important is on top, we do reversing
      const entries = sortEntries(_flatten(group.rows.map(row => row.entries)));

      for (const entry of entries) {
        if (!entry.timeless) {
          timeBars.push(
            <GanttEntryNormalTimeBar
              key={entry.id}
              top={groupHeightBox.mainRowBox.top}
              height={groupHeightBox.mainRowBox.height}
              left={scale(entry.startDate)}
              width={scale(entry.endDate) - scale(entry.startDate)}
              color={entry.color}
              shape={entry.shape}
              disableStartProjectionLine={entry.startDate.isSame(
                entry.startDate.clone().startOf('month')
              )}
              disableEndProjectionLine={entry.endDate.isSame(
                entry.endDate.clone().startOf('month')
              )}
            />
          );
        }
      }

      // if timeless circles are the only elements, display first from
      // the top (but last from sorted array) on collapsed timeline
      const timelessOnly = entries.filter(entry => entry.timeless);

      if (timelessOnly.length === entries.length) {
        const entry = timelessOnly[
          timelessOnly.length - 1
        ] as GanttEntryTimeless;

        timeBars.push(
          <GanttEntryTimelessTimeBar
            key={entry.id}
            label={entry.label}
            height={groupHeightBox.mainRowBox.height}
            top={groupHeightBox.mainRowBox.top}
            color={entry.color}
            shape={entry.shape}
            tooltip={entry.tooltip}
            fixedCanvasRef={fixedCanvasRef}
          />
        );
      }
    }
  }

  return <>{timeBars}</>;
};

const GanttTimeBarsMemoized = React.memo(GanttTimeBars);

export { GanttTimeBarsMemoized as GanttTimeBars };
