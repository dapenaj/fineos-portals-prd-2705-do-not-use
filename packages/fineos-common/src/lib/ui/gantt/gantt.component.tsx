/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment, { Moment } from 'moment';
import classNames from 'classnames';
import { debounce as _debounce, flatten as _flatten } from 'lodash';
import { ScaleTime, scaleTime } from 'd3-scale';
import {
  UpOutlined,
  DownOutlined,
  ArrowRightOutlined,
  ArrowLeftOutlined
} from '@ant-design/icons';

import { Button } from '../button';
import { ButtonType } from '../../types';
import { ThemeModel, toRgba, withTheme, textStyleToCss } from '../theme';
import { RichFormattedMessage } from '../rich-formatted-message';
import { TimeTooltipPosition } from '../time-tooltip';

import {
  GanttEntry,
  GanttEntryNormal,
  GanttGroup,
  HeightBoxesRegistry,
  RowBox
} from './gantt.type';
import { GanttGroupBorders } from './gantt-group-borders.component';
import { GanttToday } from './gantt-today';
import { GanttMonths } from './gantt-month-borders.component';
import { GanttTimeBars } from './gantt-time-bars/gantt-time-bars.component';
import { GanttDatesMarks } from './gantt-dates-mark/gantt-dates-marks.component';
import styles from './gantt.module.scss';
import {
  BORDER_SIZE_PX,
  DEFAULT_SPACING,
  GROUP_MARGIN_PX,
  ROW_HEIGHT_PX
} from './gantt.variables';

type GroupsExpanded = {
  [index: string]: boolean;
};

type GroupsRefs = {
  mainRow: React.RefObject<HTMLDivElement>;
  subRows: React.RefObject<HTMLDivElement>[];
}[];

const SIZE_NOT_SET = -1;

const RESERVED_DAYS_OFFSET = 5;

const MAX_VISIBLE_COLUMNS_COUNT = 3;

function generateMonths(startDate: Moment, endDate: Moment): Moment[] {
  const months: Moment[] = [];
  const reservedStartDate = startDate
    .clone()
    .subtract(RESERVED_DAYS_OFFSET, 'day');
  const reservedEndDate = endDate.clone().add(RESERVED_DAYS_OFFSET, 'day');
  let currMonth = reservedStartDate.startOf('month');

  while (currMonth.isBefore(reservedEndDate)) {
    months.push(currMonth);
    currMonth = currMonth.clone().add(1, 'month');
  }

  return months;
}

const produceTheme = (theme: ThemeModel) => ({
  groupsColumn: {
    borderLeftColor: theme.colorSchema.neutral4,
    borderTopColor: theme.colorSchema.neutral4
  },
  groupsColumnCell: {
    borderBottomColor: theme.colorSchema.neutral4,
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  canvasContainer: {
    borderLeftColor: theme.colorSchema.neutral4,
    borderRightColor: theme.colorSchema.neutral4,
    '&:before': {
      borderLeftColor: theme.colorSchema.neutral1
    },
    '&:after': {
      borderRightColor: theme.colorSchema.neutral1
    }
  },
  canvasLayer: {
    borderTopColor: theme.colorSchema.neutral4,
    borderBottomColor: theme.colorSchema.neutral4
  },
  groupsColumnCellRow: {
    [`&.${styles.disabled}`]: {
      color: theme.colorSchema.neutral7
    }
  },
  timeScaleRowCell: {
    borderBottomColor: theme.colorSchema.neutral4,
    borderRightColor: theme.colorSchema.neutral4,
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  timeScaleRow: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  quickLink: {
    background: theme.colorSchema.neutral1,
    color: theme.colorSchema.neutral8,
    '&:hover': {
      background: theme.colorSchema.neutral1,
      color: theme.colorSchema.primaryAction
    },
    [`&.${styles.left}`]: {
      boxShadow: `${DEFAULT_SPACING}px 0 ${DEFAULT_SPACING}px ${DEFAULT_SPACING /
        2}px ${toRgba(theme.colorSchema, 'neutral1', 0.75)}`
    },
    [`&.${styles.right}`]: {
      boxShadow: `-${DEFAULT_SPACING}px 0 ${DEFAULT_SPACING}px ${DEFAULT_SPACING /
        2}px ${toRgba(theme.colorSchema, 'neutral1', 0.75)}`
    }
  },
  gridMonthBorder: {
    borderRightColor: theme.colorSchema.neutral4
  }
});

type GanttComponentProps = {
  title: React.ReactNode;
  groups: GanttGroup[];
  themedStyles: Record<keyof ReturnType<typeof produceTheme>, any>;
};

type GanttComponentState = {
  groupsExpanded: GroupsExpanded;
  heightBoxesRegistry: HeightBoxesRegistry;
  canvasWidth: number;
  canvasHeight: number;
  leftQuickLink: Moment | null;
  rightQuickLink: Moment | null;
};

export const Gantt = withTheme(produceTheme)(
  class extends React.PureComponent<GanttComponentProps, GanttComponentState> {
    static displayName = 'Gantt';

    // for performance reason avoid immediate resizing, it's should be debounced with maxWait
    handleScreenResize = _debounce(() => this.resize(), 100, {
      // at least 1 time per 0.5 second resize should be called
      maxWait: 500
    });

    // assume that we show fixed today
    // it means that it's not covering the case when someone left computer
    // working for 24h and see yesterday as today
    // if it's really would be needed today should be moved to component state
    readonly today: Moment;
    readonly months: Moment[];

    readonly canvasContainerRef = React.createRef<HTMLDivElement>();
    readonly fixedCanvasContainerRef = React.createRef<HTMLDivElement>();
    readonly firstDate: Moment | null;
    readonly lastDate: Moment | null;

    readonly groupsRefs: GroupsRefs;

    private timeScale: ScaleTime<number, number> | null = null;

    constructor(props: GanttComponentProps) {
      super(props);

      const today = moment();
      const entries: GanttEntry[] = _flatten(
        props.groups.map(group => _flatten(group.rows.map(row => row.entries)))
      );
      const anyNormalEntry = entries.find(entry => !entry.timeless) as
        | GanttEntryNormal
        | undefined;
      const anyDate = anyNormalEntry ? anyNormalEntry.startDate : null;

      const [firstDay, lastDay] = anyDate
        ? entries.reduce(
            ([earliestDay, latestDay]: [Moment, Moment], entry) => {
              if (entry.timeless) {
                return [earliestDay, latestDay];
              } else {
                return [
                  entry.startDate.isBefore(earliestDay)
                    ? entry.startDate
                    : earliestDay,
                  entry.endDate.isAfter(latestDay) ? entry.endDate : latestDay
                ];
              }
            },
            [anyDate, anyDate]
          )
        : [null, null];

      this.today = today;
      this.groupsRefs = props.groups.map(group => ({
        mainRow: React.createRef(),
        subRows: group.rows.map(() => React.createRef())
      }));
      this.state = {
        groupsExpanded: props.groups.reduce(
          (groupsExpanded: GroupsExpanded, group) => {
            groupsExpanded[group.id] = false;
            return groupsExpanded;
          },
          {}
        ),
        heightBoxesRegistry: props.groups.map((group, i) => ({
          mainRowBox: {
            top: i * ROW_HEIGHT_PX,
            height: ROW_HEIGHT_PX
          },
          subRowBoxes: group.rows.map(row => ({
            top: 0,
            height: 0
          }))
        })),
        canvasWidth: SIZE_NOT_SET,
        canvasHeight: SIZE_NOT_SET,
        leftQuickLink: null,
        rightQuickLink: null
      };

      this.firstDate = firstDay;
      this.lastDate = lastDay;

      this.months = firstDay
        ? // lastDay always would be set if firstDay exists
          generateMonths(
            moment.min(firstDay, today),
            moment.max(lastDay!, today)
          )
        : [this.today.clone().startOf('month')];
    }

    componentDidMount() {
      // now we can run actual chart rendering as we can reading information about layout from DOM
      this.resize(true);

      window.addEventListener('resize', this.handleScreenResize);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.handleScreenResize);
    }

    handleScroll = () => {
      this.resetQuickLinks();
    };

    resetQuickLinks = () => {
      if (
        this.firstDate &&
        this.lastDate &&
        this.canvasContainerRef.current &&
        this.timeScale
      ) {
        const { current: el } = this.canvasContainerRef;
        const scrollLeft = el.scrollLeft;
        const { canvasWidth } = this.state;
        const firstDayOffset = this.timeScale(this.firstDate);
        const lastDayOffset = this.timeScale(this.lastDate);

        // even taking in account that this is PureComponent, better to skip changing state here
        // if possible, cause this method mostly called as onScroll
        if (scrollLeft > firstDayOffset) {
          if (!this.state.leftQuickLink) {
            this.setState({
              leftQuickLink: this.firstDate.clone()
            });
          }
        } else if (this.state.leftQuickLink) {
          this.setState({
            leftQuickLink: null
          });
        }

        if (scrollLeft + canvasWidth < lastDayOffset) {
          if (!this.state.rightQuickLink) {
            this.setState({
              rightQuickLink: this.lastDate.clone()
            });
          }
        } else if (this.state.rightQuickLink) {
          this.setState({
            rightQuickLink: null
          });
        }
      }
    };

    resize = (isInitial = false) => {
      const canvasContainerEl = this.canvasContainerRef.current!;
      const canvasWidth = canvasContainerEl.offsetWidth;
      const canvasHeight = canvasContainerEl.offsetHeight;

      if (isInitial) {
        this.calculateRowHeights();
      }

      this.setState({ canvasWidth, canvasHeight }, () => {
        const todayOffset = this.timeScale!(this.today);

        if (isInitial && todayOffset > canvasWidth) {
          // recenter scroll so today, always would be shown
          canvasContainerEl.scrollLeft = this.timeScale!(
            this.today.clone().startOf('month')
          );
        }

        if (this.months.length > MAX_VISIBLE_COLUMNS_COUNT) {
          this.resetQuickLinks();
        }
      });
    };

    toggleGroupExpand = (groupId: any) => {
      const { groupsExpanded } = this.state;
      this.setState(
        {
          groupsExpanded: {
            ...this.state.groupsExpanded,
            [groupId]: !groupsExpanded[groupId]
          }
        },
        this.calculateRowHeights
      );
    };

    calculateRowHeights = () => {
      const { groups } = this.props;
      const { groupsExpanded, heightBoxesRegistry } = this.state;
      const recalculatedHeightBoxesRegistry = [...heightBoxesRegistry];
      let topOffset = 0;

      for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
        const group = groups[groupIndex];
        const isExpanded = groupsExpanded[group.id];
        const groupRefs = this.groupsRefs[groupIndex];
        const mainRowHeight = groupRefs.mainRow.current!.offsetHeight;
        const mainRowTop = topOffset;
        const subRowBoxes: RowBox[] = [];

        topOffset += GROUP_MARGIN_PX + mainRowHeight;

        if (isExpanded) {
          for (let rowIndex = 0; rowIndex < group.rows.length; rowIndex++) {
            const rowRef = groupRefs.subRows[rowIndex];
            const rowHeight = rowRef.current!.offsetHeight;

            subRowBoxes.push({
              top: topOffset,
              height: rowHeight
            });

            topOffset += rowHeight;
          }
        }

        recalculatedHeightBoxesRegistry[groupIndex] = {
          mainRowBox: {
            top: mainRowTop,
            height: mainRowHeight + GROUP_MARGIN_PX * 2 + BORDER_SIZE_PX
          },
          subRowBoxes
        };

        topOffset += GROUP_MARGIN_PX + BORDER_SIZE_PX;
      }

      this.setState({
        heightBoxesRegistry: recalculatedHeightBoxesRegistry
      });
    };

    goToLeftLink = () => {
      const firstDayOffset = this.timeScale!(this.firstDate!);
      this.scrollTo(firstDayOffset - this.timeScale!(this.months[1]));
    };

    goToRightLink = () => {
      const lastDayOffset = this.timeScale!(this.lastDate!);
      this.scrollTo(lastDayOffset - this.timeScale!(this.months[1]));
    };

    scrollTo = (leftOffset: number) => {
      const el = this.canvasContainerRef.current!;
      if (typeof el.scrollTo === 'function') {
        el.scrollTo({
          left: leftOffset,
          behavior: 'smooth'
        });
      } else {
        el.scrollLeft = leftOffset;
      }
    };

    buildScaledDomainRange = (): Moment[] => {
      const requiredFirstDate = this.firstDate
        ? moment.min(this.firstDate, this.today).clone()
        : this.today;
      const requiredLastDate = this.lastDate
        ? moment.max(this.lastDate, this.today).clone()
        : this.today;
      const reservedFirstDate = requiredFirstDate.clone().subtract(1, 'day');
      const reservedLastDate = requiredLastDate.clone().add(1, 'day');

      return reservedFirstDate.isSame(reservedLastDate, 'month')
        ? [reservedFirstDate, reservedLastDate]
        : [reservedFirstDate.startOf('month'), reservedLastDate.endOf('month')];
    };

    buildTimeScale = (months: Moment[], canvasWidth: number) => {
      const lastVisibleColumnIndex =
        Math.min(MAX_VISIBLE_COLUMNS_COUNT, months.length) - 1;
      const domainRange: Moment[] =
        lastVisibleColumnIndex === 0
          ? // if all dates within 1 month, scaled domain required
            this.buildScaledDomainRange()
          : [months[0], months[lastVisibleColumnIndex].clone().endOf('month')];

      return scaleTime()
        .domain(domainRange)
        .range([0, canvasWidth]);
    };

    render() {
      const { title, groups, themedStyles } = this.props;
      const {
        groupsExpanded,
        heightBoxesRegistry,
        canvasWidth,
        leftQuickLink,
        rightQuickLink
      } = this.state;
      const months = this.months;
      const isCanvasAvailable = canvasWidth !== SIZE_NOT_SET;
      const timeScale = this.buildTimeScale(months, canvasWidth);
      const wholeCanvasWidth = timeScale(
        months[months.length - 1].clone().endOf('month')
      );
      const isCanvasExtended = months.length > MAX_VISIBLE_COLUMNS_COUNT;
      const today = this.today;

      // allow to use it in events handlers and lifecycle hooks
      this.timeScale = timeScale;

      return (
        <div className={styles.container}>
          <div
            className={classNames(
              styles.groupsColumn,
              themedStyles.groupsColumn
            )}
          >
            <div
              className={classNames(
                styles.groupsColumnCell,
                styles.groupsColumnCellRow,
                themedStyles.groupsColumnCell,
                themedStyles.groupsColumnCellRow
              )}
            >
              {title}
            </div>

            {groups.map((group, groupIndex) => (
              <div
                className={classNames(
                  styles.groupsColumnCell,
                  themedStyles.groupsColumnCell
                )}
                key={group.id}
              >
                <div
                  className={classNames(
                    styles.groupsColumnCellRow,
                    themedStyles.groupsColumnCellRow
                  )}
                  ref={this.groupsRefs[groupIndex].mainRow}
                >
                  <Button
                    buttonType={ButtonType.LINK}
                    tabIndex={-1}
                    onClick={() => this.toggleGroupExpand(group.id)}
                  >
                    {group.title}{' '}
                    {groupsExpanded[group.id] ? (
                      <UpOutlined />
                    ) : (
                      <DownOutlined />
                    )}
                  </Button>
                </div>
                {groupsExpanded[group.id] &&
                  group.rows.map((row, rowIndex) => (
                    <div
                      key={`${row.id}-${rowIndex}`}
                      ref={this.groupsRefs[groupIndex].subRows[rowIndex]}
                      className={classNames(
                        styles.groupsColumnCellRow,
                        styles.groupsColumnCellSubRow,
                        themedStyles.groupsColumnCellRow,
                        row.disabled && styles.disabled
                      )}
                    >
                      {row.title}
                    </div>
                  ))}
              </div>
            ))}
          </div>

          {isCanvasAvailable && leftQuickLink && (
            <Button
              buttonType={ButtonType.LINK}
              className={classNames(
                styles.quickLink,
                themedStyles.quickLink,
                styles.left
              )}
              tabIndex={-1}
              onClick={this.goToLeftLink}
              icon={<ArrowLeftOutlined />}
            >
              <RichFormattedMessage
                id="FINEOS_COMMON.GANTT.QUICK_LINK"
                values={{
                  quickLinkDate: leftQuickLink
                }}
              />
            </Button>
          )}

          {isCanvasAvailable && rightQuickLink && (
            <Button
              buttonType={ButtonType.LINK}
              className={classNames(
                styles.quickLink,
                themedStyles.quickLink,
                styles.right
              )}
              tabIndex={-1}
              onClick={this.goToRightLink}
            >
              <RichFormattedMessage
                id="FINEOS_COMMON.GANTT.QUICK_LINK"
                values={{
                  quickLinkDate: rightQuickLink
                }}
              />
              <ArrowRightOutlined />
            </Button>
          )}

          <div
            className={classNames(
              styles.canvasContainer,
              themedStyles.canvasContainer,
              {
                [styles.extended]: isCanvasExtended
              }
            )}
            ref={this.canvasContainerRef}
            onScroll={isCanvasExtended ? this.handleScroll : undefined}
          >
            <div
              className={classNames(
                styles.canvasLayer,
                themedStyles.canvasLayer
              )}
              style={{
                width: wholeCanvasWidth
              }}
            >
              <div
                className={classNames(
                  styles.timeScaleRow,
                  themedStyles.timeScaleRow
                )}
              >
                {isCanvasAvailable && (
                  <GanttMonths
                    months={months}
                    scale={timeScale}
                    monthClassName={classNames(
                      styles.timeScaleRowCell,
                      themedStyles.timeScaleRowCell
                    )}
                    lastMonthClassName={styles.lastTimeScaleRowCell}
                    render={month => month.format('MMMM YYYY')}
                  />
                )}
              </div>

              <div
                className={classNames(
                  styles.timeScaleRow,
                  themedStyles.timeScaleRow
                )}
              >
                {/* this is only for grid */}
                {isCanvasAvailable && (
                  <GanttMonths
                    months={months}
                    scale={timeScale}
                    monthClassName={classNames(
                      styles.timeScaleRowCell,
                      themedStyles.timeScaleRowCell
                    )}
                    lastMonthClassName={styles.lastTimeScaleRowCell}
                  />
                )}

                {isCanvasAvailable && (
                  <GanttDatesMarks
                    groups={groups}
                    today={isCanvasExtended ? today : null}
                    scale={timeScale}
                  />
                )}
              </div>

              <div className={styles.timeLineCanvas}>
                {isCanvasAvailable && (
                  <GanttToday
                    today={today}
                    scale={timeScale}
                    tooltipPosition={
                      isCanvasExtended
                        ? TimeTooltipPosition.TOP
                        : TimeTooltipPosition.BOTTOM
                    }
                  />
                )}

                {isCanvasAvailable && (
                  <GanttGroupBorders
                    heightBoxesRegistry={heightBoxesRegistry}
                    width={wholeCanvasWidth}
                  />
                )}

                {isCanvasAvailable && (
                  <GanttMonths
                    months={months}
                    scale={timeScale}
                    monthClassName={classNames(
                      styles.gridMonthBorder,
                      themedStyles.gridMonthBorder
                    )}
                    lastMonthClassName={styles.lastGridMonthBorder}
                  />
                )}

                {isCanvasAvailable && (
                  <GanttTimeBars
                    groups={groups}
                    heightBoxesRegistry={heightBoxesRegistry}
                    scale={timeScale}
                    fixedCanvasRef={this.fixedCanvasContainerRef}
                  />
                )}
              </div>
              <div
                className={styles.fixedCanvas}
                ref={this.fixedCanvasContainerRef}
              />
            </div>
          </div>
        </div>
      );
    }
  }
);
