/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';

import styles from './empty-state.module.scss';

type EmptyStateProps = {
  /**
   * Displays a border around the empty state content
   */
  withBorder?: boolean;
  /**
   * Empty State version for small panels (sidebar)
   */
  isSmall?: boolean;
} & JSX.IntrinsicElements['div'];

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  wrapperWithBorder: {
    borderColor: theme.colorSchema.neutral3
  },
  wrapperSmall: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

/**
 * Wraps it's children in a generic empty state styling.
 */
export const EmptyState: React.FC<EmptyStateProps> = ({
  withBorder,
  className,
  isSmall,
  children,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <div
      className={classNames(
        styles.wrapper,
        themedStyles.wrapper,
        {
          [themedStyles.wrapperWithBorder]: withBorder,
          [styles.border]: withBorder,
          [themedStyles.wrapperSmall]: isSmall,
          [styles.small]: isSmall
        },
        className
      )}
      data-test-el="empty-state"
      {...props}
    >
      {children}
    </div>
  );
};
