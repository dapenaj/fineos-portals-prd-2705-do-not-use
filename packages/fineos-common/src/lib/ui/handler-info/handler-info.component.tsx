/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';

import { PropertyItem } from '../property-item';
import { FormattedMessage } from '../formatted-message';
import { createThemedStyles, textStyleToCss } from '../theme';

const useThemedStyles = createThemedStyles(theme => ({
  value: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type HandlerInfoProps = {
  /**
   * Name of case handler
   */
  name?: string;
  /**
   * Phone number of case handler
   */
  phoneNumber?: string;
  /**
   * Optional test element name. Defaults to "handler-info"
   */
  'data-test-el'?: string;
};

export const HandlerInfo: React.FC<HandlerInfoProps> = ({
  name,
  phoneNumber,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <Row align="middle" data-test-el={props['data-test-el'] || 'handler-info'}>
      <Col xs={24} sm={12}>
        <PropertyItem
          label={
            <FormattedMessage id="FINEOS_COMMON.HANDLER_INFO.MANAGED_BY" />
          }
        >
          {name && <span className={themedStyles.value}>{name}</span>}
        </PropertyItem>
      </Col>
      <Col xs={24} sm={12}>
        <PropertyItem
          label={
            <FormattedMessage id="FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER" />
          }
        >
          {phoneNumber && (
            <span className={themedStyles.value}>{phoneNumber}</span>
          )}
        </PropertyItem>
      </Col>
    </Row>
  );
};
