/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Button as AntButton } from 'antd';
import { ButtonProps as AntButtonProps } from 'antd/lib/button';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import { createThemedStyles, textStyleToCss } from '../theme';
import { ButtonType } from '../../types';

import { useButtonTypeStyling } from './button.util';
import styles from './button.module.scss';

export type UiButtonProps = {
  /**
   * Type of button. Defaults to "primary"
   */
  buttonType?: ButtonType;
  /**
   * Optional classname applied to button
   */
  className?: string;
  /**
   * Optional to value from the Link which would enforce rendering
   * as a Link from react-router-dom
   */
  to?: string;
  /**
   * Optional test element name. Defaults to "action-button"
   */
  'data-test-el'?: string;
} & Omit<AntButtonProps, 'htmlType'>;

enum AntButtonType {
  LINK = 'link',
  GHOST = 'ghost',
  DEFAULT = 'default',
  PRIMARY = 'primary',
  DASHED = 'dashed'
}

const useThemedStyles = createThemedStyles(theme => ({
  button: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

const convertToAntButtonType = (typeButton: ButtonType) => {
  return AntButtonType[ButtonType[typeButton] as keyof typeof AntButtonType];
};

const convertDangerToPrimary = (typeButton: ButtonType) => {
  switch (typeButton) {
    case ButtonType.DANGER:
      return ButtonType.PRIMARY;
    default:
      return typeButton;
  }
};

export const UiButton: React.FC<UiButtonProps & AntButtonProps> = ({
  buttonType = ButtonType.PRIMARY,
  className,
  children,
  to,
  ...props
}) => {
  const buttonTypeClassName = useButtonTypeStyling(buttonType);
  const mappedButtonType = convertToAntButtonType(
    convertDangerToPrimary(buttonType)
  );
  const themedStyles = useThemedStyles();

  if (to) {
    return (
      <Link
        to={to}
        component={AntButton}
        className={classNames(
          className,
          styles.linkButton,
          themedStyles.button,
          buttonTypeClassName
        )}
        type={mappedButtonType}
        data-test-el={props['data-test-el'] || 'action-button'}
        onKeyPress={props['onKeyPress']}
      >
        {children}
      </Link>
    );
  }

  return (
    <AntButton
      type={mappedButtonType}
      key="button"
      className={classNames(
        className,
        themedStyles.button,
        buttonTypeClassName,
        {
          [styles.linkButton]: buttonType === ButtonType.LINK,
          [styles.primaryButton]: buttonType === ButtonType.PRIMARY
        }
      )}
      data-test-el={props['data-test-el'] || 'action-button'}
      {...props}
    >
      {children}
    </AntButton>
  );
};
