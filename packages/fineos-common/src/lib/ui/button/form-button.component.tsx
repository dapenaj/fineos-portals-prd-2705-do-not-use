/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { ButtonProps as AntButtonProps } from 'antd/lib/button';
import classNames from 'classnames';
import { useFormikContext } from 'formik';

import { ButtonType } from '../../types';

import { UiButton, UiButtonProps } from './ui-button.component';
import styles from './button.module.scss';

export type FormButtonProps = {
  /**
   * Type of button. Defaults to "primary"
   */
  buttonType?: ButtonType;
  /**
   * Optional classname applied to button
   */
  className?: string;
  /**
   * Optional test element name. Defaults to "action-button"
   */
  'data-test-el'?: string;
  /**
   * Button type
   */
  htmlType: NonNullable<JSX.IntrinsicElements['button']['type']>;
  /**
   * Indicates if the button should call submitForm on Formik. Only used when htmlType = "submit". Defaults to true.
   */
  shouldSubmitForm?: boolean;
} & Omit<AntButtonProps, 'htmlType'>;

export const FormButton: React.FC<FormButtonProps> = ({
  buttonType = ButtonType.PRIMARY,
  className,
  shouldSubmitForm = true,
  children,
  ...props
}) => {
  const formikBag = useFormikContext();
  const formikContext =
    props.htmlType === 'submit' || props.htmlType === 'reset'
      ? formikBag
      : null;
  const isSubmitting = formikContext ? formikContext.isSubmitting : undefined;

  return (
    <UiButton
      buttonType={buttonType}
      key="button"
      onClick={() => {
        if (props.htmlType === 'reset') {
          formikContext!.resetForm({});
        } else if (props.htmlType === 'submit' && shouldSubmitForm) {
          formikContext!.submitForm();
        }
      }}
      className={classNames(className, {
        [styles.linkButton]: buttonType === ButtonType.LINK
      })}
      data-test-el={props['data-test-el'] || 'action-button'}
      loading={props.htmlType === 'submit' && isSubmitting}
      {...(props as UiButtonProps)}
    >
      {children}
    </UiButton>
  );
};
