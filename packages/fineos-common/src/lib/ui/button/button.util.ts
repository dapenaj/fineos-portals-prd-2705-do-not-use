/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  createThemedStyles,
  primaryActionDarkColor,
  primaryActionDark2Color,
  callToActionDarkColor,
  callToActionDark2Color
} from '../theme';
import { ButtonType } from '../../types';
import { getFocusedElementShadow, getPrimaryButtonShadow } from '../../utils';

const useButtonTypeThemedStyles = createThemedStyles(theme => ({
  primary: {
    color: theme.colorSchema.neutral1,
    background: theme.colorSchema.primaryAction,
    borderColor: theme.colorSchema.primaryAction,
    '&:hover': {
      background: primaryActionDarkColor(theme.colorSchema),
      borderColor: primaryActionDarkColor(theme.colorSchema)
    },
    '&:focus': {
      boxShadow: getPrimaryButtonShadow(theme),
      backgroundColor: theme.colorSchema.primaryAction
    }
  },
  link: {
    color: theme.colorSchema.primaryAction,
    '&:hover': {
      color: primaryActionDarkColor(theme.colorSchema)
    },
    '&:focus': {
      color: primaryActionDark2Color(theme.colorSchema),
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  ghost: {
    color: theme.colorSchema.neutral9,
    backgroundColor: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral5,
    '&:hover': {
      backgroundColor: theme.colorSchema.neutral1,
      color: primaryActionDarkColor(theme.colorSchema),
      borderColor: theme.colorSchema.primaryAction
    },
    '&:focus': {
      backgroundColor: theme.colorSchema.neutral1,
      color: primaryActionDark2Color(theme.colorSchema),
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  danger: {
    background: theme.colorSchema.callToActionColor,
    color: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.callToActionColor,
    '&:hover': {
      background: callToActionDarkColor(theme.colorSchema),
      borderColor: callToActionDarkColor(theme.colorSchema)
    },
    '&:focus': {
      background: callToActionDark2Color(theme.colorSchema),
      borderColor: callToActionDark2Color(theme.colorSchema)
    }
  }
}));

export const useButtonTypeStyling = (type: ButtonType) => {
  const typeThemedStyles = useButtonTypeThemedStyles();
  switch (type) {
    case ButtonType.PRIMARY:
      return typeThemedStyles.primary;
    case ButtonType.LINK:
      return typeThemedStyles.link;
    case ButtonType.GHOST:
      return typeThemedStyles.ghost;
    case ButtonType.DANGER:
      return typeThemedStyles.danger;
    default:
      return `button-${type}`;
  }
};
