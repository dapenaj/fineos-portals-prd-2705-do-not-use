/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import { FormattedMessage } from '../formatted-message';
import { textStyleToCss, createThemedStyles } from '../theme';

import styles from './asterisk-info.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  asteriskText: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  }
}));

type AsteriskInfoProps = {
  /**
   * Optional classname applied to message
   */
  className?: string;
};

export const AsteriskInfo: React.FC<AsteriskInfoProps> = ({ className }) => {
  const themedStyles = useThemedStyles();
  return (
    <FormattedMessage
      className={classNames(
        className,
        styles.asterisk,
        themedStyles.asteriskText
      )}
      id="FINEOS_COMMON.VALIDATION.ASTERISK"
    />
  );
};
