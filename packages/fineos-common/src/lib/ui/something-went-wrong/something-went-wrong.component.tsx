/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';

import { useTheme, createThemedStyles, textStyleToCss } from '../theme';
import { FormattedMessage } from '../formatted-message';

import styles from './something-went-wrong.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  message: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type SomethingWentWrongProps = {
  /**
   *  should be set as true to set role 'status' and enable read by screen reader
   */
  hasRoleStatus?: boolean;
};

export const SomethingWentWrong: React.FC<SomethingWentWrongProps> = ({
  hasRoleStatus
}) => {
  const theme = useTheme();
  const themeStyles = useThemedStyles();
  const paramRoleStatus = hasRoleStatus ? { role: 'status' } : {};
  return (
    <div className={styles.wrapper} data-test-el="something-went-wrong">
      <div className={styles.imageWrapper}>
        {theme.images && theme.images.somethingWentWrong && (
          <img
            alt="Something went wrong"
            data-test-el="something-went-wrong-image"
            src={theme.images.somethingWentWrong}
            className={styles.image}
          />
        )}
      </div>

      <div className={classnames(styles.title, themeStyles.title)}>
        <FormattedMessage
          id="FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE"
          {...paramRoleStatus}
        />
      </div>
      <div className={classnames(styles.message, themeStyles.message)}>
        <FormattedMessage
          id="FINEOS_COMMON.SOMETHING_WENT_WRONG.MESSAGE"
          {...paramRoleStatus}
        />
      </div>
    </div>
  );
};
