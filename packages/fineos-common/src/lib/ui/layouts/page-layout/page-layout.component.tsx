/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useRef } from 'react';
import { Col, Row } from 'antd';
import classNames from 'classnames';

import { ThemeModel, createThemedStyles } from '../../theme';
import { ContentLayout } from '../content-layout';
import { Header } from '../../header';
import { NavBar } from '../../nav-bar';
import { Sticky } from '../../sticky';

import styles from './page-layout.module.scss';
import { PageContentHeader } from './page-content-header.component';

type ContentHeader =
  | React.ReactElement<
      React.ComponentProps<typeof PageContentHeader>,
      typeof PageContentHeader
    >
  | React.ReactNode;

type PageLayoutProps = {
  sideBar?: React.ReactNode;
  children?: React.ReactNode | React.ReactChild[];
  contentHeader?: ContentHeader;
  navigation: React.ReactElement[];
  userName: string;
  userId: string;
  userOptions: React.ReactElement[];
  headerRightContent?: React.ReactElement;
  showBackgroundImage?: boolean;
  className?: string;
  isContrastModeEnabled?: boolean;
  onContrastModeToggle?: (isEnabled: boolean) => void;
};

export const MAIN_CONTENT_ELEMENT_ID = 'main-content-section';

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  container: {
    backgroundColor: theme.colorSchema.neutral2
  },
  withBackgroundImage: {
    backgroundImage: `url(${theme?.images?.welcomeBackground})`
  }
}));

const isPageContentHeader = (contentHeader: ContentHeader | undefined) =>
  Boolean(contentHeader) &&
  contentHeader!.hasOwnProperty('type') &&
  (contentHeader as React.ReactElement).type === PageContentHeader;

const isMainContentHeader = (contentHeader: ContentHeader | undefined) =>
  Boolean(contentHeader) && !isPageContentHeader(contentHeader);

const SIDEBAR_OFFSET_TOP = 90;

export const PageLayout = ({
  children,
  contentHeader,
  sideBar,
  userName,
  userId,
  userOptions,
  navigation,
  headerRightContent,
  showBackgroundImage,
  className,
  isContrastModeEnabled,
  onContrastModeToggle
}: PageLayoutProps) => {
  const themedStyles = useThemedStyles();
  const isFullWidth = !sideBar;
  const targetRef = useRef() as React.RefObject<HTMLDivElement>;

  return (
    <div
      data-test-el="page-layout"
      className={classNames(
        styles.container,
        themedStyles.container,
        {
          [themedStyles.withBackgroundImage]: showBackgroundImage
        },
        className
      )}
    >
      <Header
        userName={userName}
        userId={userId}
        userOptions={userOptions}
        rightContent={headerRightContent}
        isContrastModeEnabled={isContrastModeEnabled}
        onContrastModeToggle={onContrastModeToggle}
      />
      <NavBar>{navigation}</NavBar>
      {isPageContentHeader(contentHeader) && contentHeader}

      <div className={styles.mainWrapper} ref={targetRef}>
        <ContentLayout
          direction="column"
          className={styles.mainLayout}
          data-test-el="main-layout"
          role="main"
          id={MAIN_CONTENT_ELEMENT_ID}
        >
          {isMainContentHeader(contentHeader) && (
            <Row gutter={32}>
              <Col span={24}>{contentHeader}</Col>
            </Row>
          )}
          <Row gutter={32}>
            <Col lg={isFullWidth ? 24 : 16}>{children}</Col>

            {!isFullWidth && (
              <Col lg={8}>
                <Sticky offsetTop={SIDEBAR_OFFSET_TOP} target={targetRef}>
                  {sideBar}
                </Sticky>
              </Col>
            )}
          </Row>
        </ContentLayout>
      </div>
    </div>
  );
};
