/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Col, Row } from 'antd';
import classNames from 'classnames';

import { ThemeModel, createThemedStyles } from '../../theme';
import { ContentLayout } from '../content-layout';
import { Header } from '../../header';

import styles from './page-layout.module.scss';

type AnonymousLayoutProps = {
  sideBar?: React.ReactChild[] | React.ReactChild;
  children?: React.ReactNode | React.ReactChild[];
  contentHeader?: React.ReactNode | React.ReactChild[];
};

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  container: {
    backgroundColor: theme.colorSchema.neutral2
  }
}));

export const AnonymousLayout = ({
  children,
  contentHeader,
  sideBar
}: AnonymousLayoutProps) => {
  const themedStyles = useThemedStyles();
  const isFullWidth = !sideBar;

  return (
    <div className={classNames(styles.container, themedStyles.container)}>
      <Header />

      <div className={styles.mainWrapper}>
        <ContentLayout direction="column" className={styles.mainLayout}>
          {contentHeader && (
            <Row gutter={32}>
              <Col>{contentHeader}</Col>
            </Row>
          )}
          <Row gutter={32}>
            <Col md={isFullWidth ? 24 : 16}>{children}</Col>
            {!isFullWidth && <Col md={8}>{sideBar}</Col>}
          </Row>
        </ContentLayout>
      </div>
    </div>
  );
};
