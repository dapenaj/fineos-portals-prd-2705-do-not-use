import React, { FC } from 'react';

export const PageContentHeader: FC<React.ReactNode> = ({ children }) => (
  <>{children}</>
);
