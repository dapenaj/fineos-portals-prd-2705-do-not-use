/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect, useRef } from 'react';
import { DatePicker } from 'antd';
import { PickerProps } from 'antd/lib/date-picker/generatePicker';
import { Moment } from 'moment';
import { FieldProps, getIn } from 'formik';
import classNames from 'classnames';
import { InputProps as AntInputProps } from 'antd/lib/input';
import { DatePickerProps } from 'antd/lib/date-picker';

import { createThemedStyles, textStyleToCss } from '../theme';
import { DefaultInputProps } from '../../types';
import {
  Formatting,
  FormGroupContext,
  getFocusedElementShadow
} from '../../utils';
import { useRawFormattedMessage } from '../formatted-message';

import styles from './date-picker-input.module.scss';

type DatePickerInputProps = DefaultInputProps & {
  /**
   * Callback function passed to disable given dates.
   */
  disabledDate?: (current: Moment | null) => boolean;
  /**
   * Callback function executed when the selected time is changing.
   */
  onChange?: (date: Moment | null, dateString: string) => void;
  placeholder?: React.ReactElement;
  shouldFocusOnMount?: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  datePickerWrapper: {
    '& .ant-picker-focused': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    },
    '& .ant-input': {
      // dedicated styling for Firefox and IE11 is required here
      '&::placeholder': {
        color: theme.colorSchema.neutral6,
        ...textStyleToCss(theme.typography.baseText)
      },
      '&::-moz-placeholder': {
        color: theme.colorSchema.neutral6,
        ...textStyleToCss(theme.typography.baseText)
      },
      '&:-ms-input-placeholder': {
        color: theme.colorSchema.neutral6,
        ...textStyleToCss(theme.typography.baseText)
      }
    }
  },
  datePickerDropdown: {
    '& .ant-picker-panel-container': {
      background: theme.colorSchema.neutral1,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-header-view': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-dropdown, & .ant-picker-header-view button, & .ant-picker-cell-inner': {
      ...textStyleToCss(theme.typography.baseText, { suppressLineHeight: true })
    },
    '& .ant-picker-content th, & .ant-picker-today-btn': {
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-cell-in-view, .ant-picker-content th': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-cell:not(.ant-picker-cell-in-view)': {
      color: theme.colorSchema.neutral5,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-header-super-prev-btn, .ant-picker-header-prev-btn, .ant-picker-header-next-btn, .ant-picker-header-super-next-btn': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-today-btn': {
      color: `${theme.colorSchema.primaryAction} !important`
    },
    '& .ant-picker-cell-selected > .ant-picker-cell-inner': {
      color: theme.colorSchema.neutral1,
      backgroundColor: theme.colorSchema.primaryAction
    },
    '& .ant-picker-cell-in-view.ant-picker-cell-today .ant-picker-cell-inner': {
      '&::before': {
        borderColor: theme.colorSchema.primaryAction
      }
    }
  },
  datePicker: {
    borderColor: theme.colorSchema.neutral5,
    backgroundColor: `${theme.colorSchema.neutral1} !important`,
    '& .ant-picker-input > .ant-picker-suffix, .ant-picker-clear': {
      backgroundColor: theme.colorSchema.neutral1,
      color: theme.colorSchema.neutral8
    },
    '& .ant-picker-input input': {
      backgroundColor: theme.colorSchema.neutral1,
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-input input::placeholder': {
      backgroundColor: theme.colorSchema.neutral1,
      color: theme.colorSchema.neutral6,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker': {
      backgroundColor: `${theme.colorSchema.neutral1} !important`
    },
    '& .ant-form-item-has-error .ant-input-number, .ant-form-item-has-error .ant-picker': {
      backgroundColor: theme.colorSchema.neutral1
    }
  }
}));

/**
 * Date picker input
 */
export const DatePickerInput: React.FC<DatePickerInputProps &
  FieldProps &
  AntInputProps> = ({
  field,
  form: { errors, setFieldValue, setFieldTouched },
  className,
  onChange,
  isDisabled,
  disabledDate,
  labelCol,
  wrapperCol,
  id,
  placeholder,
  shouldFocusOnMount = false,
  ...props
}) => {
  const formatting = useContext(Formatting);
  const isInvalid = getIn(errors, field.name);
  const { isRequired } = useContext(FormGroupContext);
  const themedStyles = useThemedStyles();
  const inputPlaceholder = useRawFormattedMessage(placeholder);

  const pickerRef = useRef(null) as React.RefObject<
    React.Component<PickerProps<Moment>, any, any> & HTMLDivElement
  >;
  useEffect(() => {
    if (shouldFocusOnMount) {
      pickerRef.current && pickerRef.current.focus();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shouldFocusOnMount]);

  return (
    <div
      className={classNames(
        styles.datePickerWrapper,
        themedStyles.datePickerWrapper
      )}
    >
      <DatePicker
        ref={pickerRef}
        className={classNames(themedStyles.datePicker, className)}
        dropdownClassName={themedStyles.datePickerDropdown}
        data-test-el={props['data-test-el'] || 'date-picker'}
        id={id}
        name={field.name}
        value={field.value}
        format={formatting.date}
        disabled={isDisabled}
        disabledDate={disabledDate}
        getPopupContainer={trigger => trigger.parentElement ?? document.body}
        onChange={(date, dateString) => {
          // field.onchange expects a React event.
          // As we only have a value here we set the value explicitly.
          setFieldValue(field.name, date);

          setTimeout(() => {
            // wait to remove "blinking" error
            setFieldTouched(field.name);
          }, 10);

          if (onChange) {
            onChange(date, dateString);
          }
        }}
        aria-invalid={isInvalid ? 'true' : undefined}
        aria-required={isRequired}
        aria-describedby={props['aria-describedby']}
        placeholder={inputPlaceholder}
      />
    </div>
  );
};

export const UiDatePicker: React.FC<DatePickerProps> = ({
  className,
  dropdownClassName,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <DatePicker
      {...props}
      className={classNames(themedStyles.datePicker, className)}
      dropdownClassName={classNames(
        themedStyles.datePickerDropdown,
        dropdownClassName
      )}
      getPopupContainer={trigger => trigger.parentElement ?? document.body}
    />
  );
};
