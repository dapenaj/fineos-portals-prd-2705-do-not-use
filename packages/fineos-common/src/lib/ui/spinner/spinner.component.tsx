/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Spin } from 'antd';
import classnames from 'classnames';

import { createThemedStyles } from '../theme';

import styles from './spinner.module.scss';

type SpinnerSize = 'small' | 'medium' | 'large';

type SpinnerProps = {
  /**
   * Specify Spinner size, possible value: "small", "medium" and "large".
   * Default value: "large"
   */
  size?: SpinnerSize;

  className?: string;
  classNameSpin?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  spinner: {
    '& .ant-spin': {
      color: theme.colorSchema.primaryColor
    },
    '& .ant-spin-dot-item': {
      backgroundColor: theme.colorSchema.primaryColor
    }
  }
}));

/**
 * Generic spinner component typically used when content is loading.
 */
export const Spinner: React.FC<SpinnerProps> = ({
  className,
  size = 'large',
  classNameSpin
}) => {
  const themedStyles = useThemedStyles();

  return (
    <div
      className={classnames(className, styles.spinner, {
        [styles.small]: size === 'small'
      })}
      data-test-el="spinner"
    >
      <Spin
        // themedStyles.spinner is used by e2e test locator "pageSpinners()",
        // in case of change, please also refactor locator method
        className={classnames(themedStyles.spinner, classNameSpin)}
        size={size === 'medium' ? 'default' : size}
      />
    </div>
  );
};
