/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import { useIntl } from 'react-intl';
import { DatePicker } from 'antd';
import { FieldProps, getIn } from 'formik';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';
import { DefaultInputProps } from '../../types';
import { Formatting, getFocusedElementShadow } from '../../utils';

import styles from './range-picker-input.module.scss';

const { RangePicker } = DatePicker;

type RangePickerInputProps = Omit<DefaultInputProps, 'placeholder'> & {
  onChange?: React.ComponentProps<typeof DatePicker.RangePicker>['onChange'];
  dropdownClassName?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  rangePickerWrapper: {
    '& .ant-picker-focused': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  rangePickerDropdown: {
    '& .ant-picker-dropdown, & .ant-picker-header-view button, & .ant-picker-cell-inner': {
      ...textStyleToCss(theme.typography.baseText, { suppressLineHeight: true })
    },
    '& .ant-picker-content th, & .ant-picker-today-btn': {
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  rangePicker: {
    borderColor: theme.colorSchema.neutral5,
    backgroundColor: theme.colorSchema.neutral1,
    '& .ant-picker-suffix, .ant-picker-clear': {
      color: theme.colorSchema.neutral8
    },
    '& .ant-picker-input > input::-webkit-input-placeholder': {
      color: theme.colorSchema.neutral6,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-picker-active-bar': {
      background: theme.colorSchema.primaryAction
    },
    '& .ant-picker-input > input': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8
    }
  }
}));

export const RangePickerInput: React.FC<RangePickerInputProps & FieldProps> = ({
  field,
  form: { errors, setFieldValue, setFieldTouched },
  className,
  dropdownClassName,
  onChange,
  isDisabled,
  ...props
}) => {
  const formatting = useContext(Formatting);
  const intl = useIntl();
  const isInvalid = getIn(errors, field.name);
  const themedStyles = useThemedStyles();

  return (
    <div
      data-test-el={props['data-test-el'] || 'range-picker'}
      className={themedStyles.rangePickerWrapper}
    >
      <RangePicker
        className={classNames(
          themedStyles.rangePicker,
          styles.rangePickerInput,
          className
        )}
        dropdownClassName={classNames(
          dropdownClassName,
          themedStyles.rangePickerDropdown
        )}
        placeholder={[
          intl.formatMessage({ id: 'FINEOS_COMMON.RANGE_PICKER.START_DATE' }),
          intl.formatMessage({ id: 'FINEOS_COMMON.RANGE_PICKER.END_DATE' })
        ]}
        name={field.name}
        value={field.value}
        format={formatting.date}
        disabled={isDisabled}
        onChange={(date, dateString) => {
          // field.onchange expects a React event.
          // As we only have a value here we set the value explicitly.
          setFieldValue(field.name, date);

          setTimeout(() => {
            // wait to remove "blinking" error
            setFieldTouched(field.name);
          }, 10);

          if (onChange) {
            onChange(date, dateString);
          }
        }}
        aria-invalid={isInvalid ? 'true' : undefined}
        {...props}
      />
    </div>
  );
};
