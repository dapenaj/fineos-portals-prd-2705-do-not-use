/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { ReactNode } from 'react';
import { isEmpty as _isEmpty } from 'lodash';

import { DEFAULT_FALLBACK } from '../../constants';

type FallbackValueProps = {
  /**
   * Prioritized message to display.
   */
  value: ReactNode;
  /**
   * Optional fallback message to display, defaults to "-".
   */
  fallback?: ReactNode;
  /**
   * Optional className
   */
  className?: string;
  /**
   * Optional test element name. Defaults to "fallback-value"
   */
  'data-test-el'?: string;
};

export const FallbackValue: React.FC<FallbackValueProps> = ({
  value,
  fallback = DEFAULT_FALLBACK,
  className,
  ...props
}) => (
  <>
    <span
      data-test-el={props['data-test-el'] || 'fallback-value'}
      className={className}
    >
      {!_isEmpty(value) ? value : fallback}
    </span>
  </>
);
