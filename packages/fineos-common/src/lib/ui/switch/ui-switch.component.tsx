/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { ReactNode } from 'react';
import classNames from 'classnames';
import { Switch as AntSwitch } from 'antd';

import { DefaultInputProps } from '../../types';
import { getFocusedElementShadow } from '../../utils';
import { createThemedStyles, ThemeModel } from '../theme';

import styles from './switch.module.scss';

type UiSwitchProps = {
  /**
   * Optional attribute to determine if field should be focused after component mounts
   * Defaults to false
   */
  isAutoFocusEnabled?: boolean;
  /**
   * Optional attribute to determine if switch should be cheked or not by default. Defaults to false
   */
  isDefaultChecked?: boolean;
  /**
   * Optional attribute to determine size of switch input, defaults to "default"
   */
  size?: 'small' | 'default';
  /**
   * Optional attribute to determine content inside checked switch
   */
  checkedChildren?: ReactNode | string;
  /**
   * Optional attribute to determine content inside unchecked switch
   */
  unCheckedChildren?: ReactNode | string;
  /**
   * Determines state of switch - is it checked or not
   */
  isChecked: boolean;
  onChange: (isChecked: boolean) => void;
};

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  switchInput: {
    backgroundColor: theme.colorSchema.neutral5,
    '&:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        isWide: true,
        color: theme.colorSchema.primaryAction
      })
    },
    '& .ant-switch-handle::before': {
      backgroundColor: theme.colorSchema.neutral1
    }
  },
  'ant-switch-checked': {
    backgroundColor: theme.colorSchema.primaryAction
  }
}));

// Plain switch input, which does not have to be inside formik.
export const UiSwitch: React.FC<UiSwitchProps & DefaultInputProps> = ({
  className,
  isAutoFocusEnabled,
  isDisabled,
  size,
  isChecked,
  onChange,
  checkedChildren,
  unCheckedChildren,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <AntSwitch
      {...props}
      className={classNames(
        styles.switchInput,
        themedStyles.switchInput,
        isChecked && themedStyles['ant-switch-checked'],
        className
      )}
      autoFocus={isAutoFocusEnabled}
      disabled={isDisabled}
      size={size}
      checked={isChecked}
      checkedChildren={checkedChildren}
      unCheckedChildren={unCheckedChildren}
      data-test-el={props['data-test-el'] || 'switch'}
      onChange={(isSwitchChecked: boolean) => onChange(isSwitchChecked)}
    />
  );
};
