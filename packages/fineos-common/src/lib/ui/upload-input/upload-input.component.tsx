/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';

import { Button } from '../button';
import { createThemedStyles, textStyleToCss } from '../theme';
import { FormattedMessage } from '../formatted-message';

import styles from './upload-input.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  upload: {
    borderColor: theme.colorSchema.neutral5
  },
  textBase: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type UploadInputProps = {
  /**
   * Optional media type, will be displayed in instruction text.
   * Defaults to 'document'
   */
  uploadedMediaTypeLabel?: string;
};

export const UploadInput: React.FC<UploadInputProps> = ({
  uploadedMediaTypeLabel = 'document'
}) => {
  const themedStyles = useThemedStyles();

  return (
    <div
      className={classnames(styles.upload, themedStyles.upload)}
      data-test-el="upload-input"
    >
      <div className={themedStyles.textBase}>
        <FormattedMessage
          id="FINEOS_COMMON.UPLOAD.DRAG_LABEL"
          values={{ uploadedMediaType: uploadedMediaTypeLabel }}
        />
      </div>

      <div className={styles.spacing}>
        <div className={themedStyles.textBase}>
          <FormattedMessage id="FINEOS_COMMON.UPLOAD.OR" />
        </div>
      </div>

      <Button type="primary" data-test-el="browse-files">
        <FormattedMessage id="FINEOS_COMMON.UPLOAD.BROWSE_FILES" />
      </Button>
    </div>
  );
};
