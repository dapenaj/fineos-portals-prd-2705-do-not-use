/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState, useRef } from 'react';

import { Modal } from '../modal';
import { Drawer } from '../drawer';

type ModalProps = Omit<React.ComponentProps<typeof Modal>, 'visible'> & {
  visible?: boolean;
};
type DrawerProps = Omit<React.ComponentProps<typeof Drawer>, 'visible'>;

type ModalTriggerProps = (
  | (ModalProps & {
      modalType?: 'modal';
    })
  | (DrawerProps & {
      modalType: 'drawer';
    })
) & {
  children:
    | React.ReactElement<{
        onClick: JSX.IntrinsicElements['button']['onClick'];
      }>
    | false;
  modalBody: React.ReactElement<{
    onFinish?: (...args: any[]) => void;
  }>;
};

export const ModalTrigger = ({
  children,
  modalBody,
  modalType = 'modal',
  ...props
}: ModalTriggerProps) => {
  const [isVisible, setIsVisible] = useState(false);
  const triggerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if ('visible' in props) {
      setIsVisible(!!props.visible);
    }
  }, [props]);

  const handleFocusTriggerAfterClose = () =>
    triggerRef?.current?.querySelector('button')?.focus();

  const handleModalClose = (e: React.MouseEvent<HTMLElement>) => {
    setIsVisible(false);
    if ((props as ModalProps).onCancel) {
      (props as ModalProps).onCancel!(e);
    }
    handleFocusTriggerAfterClose();
  };

  const originalOnClick =
    !!children && (children as React.ReactElement).props?.onClick;
  const wrappedChildren =
    children &&
    React.cloneElement(children as React.ReactElement, {
      onClick: (e: React.MouseEventHandler) => {
        setIsVisible(true);
        if (originalOnClick) {
          originalOnClick(e);
        }
      }
    });
  const originalOnFinish = modalBody.props.onFinish;
  const wrappedModalBody = React.cloneElement(modalBody, {
    onFinish: (...args: any[]) => {
      setIsVisible(false);
      if (originalOnFinish) {
        originalOnFinish(...args);
      }
      handleFocusTriggerAfterClose();
    }
  });
  const managedModal =
    modalType === 'drawer' ? (
      <Drawer
        {...(props as DrawerProps)}
        isVisible={isVisible}
        onClose={() => {
          setIsVisible(false);
          if ((props as DrawerProps).onClose) {
            (props as DrawerProps).onClose!();
          }
          handleFocusTriggerAfterClose();
        }}
      >
        {wrappedModalBody}
      </Drawer>
    ) : (
      <Modal
        {...(props as ModalProps)}
        visible={isVisible}
        onCancel={e => handleModalClose(e!)}
      >
        {wrappedModalBody}
      </Modal>
    );

  return (
    <>
      <div ref={triggerRef}>{wrappedChildren}</div>
      {managedModal}
    </>
  );
};
