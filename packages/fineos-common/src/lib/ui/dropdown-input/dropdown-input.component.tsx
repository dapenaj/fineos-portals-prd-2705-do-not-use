/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FieldProps, getIn } from 'formik';
import { InputProps as AntInputProps } from 'antd/lib/input';
import classNames from 'classnames';

import { DefaultInputProps } from '../../types';
import { Select } from '../select';
import { handleVirtualGroupBlurEvent } from '../text-input/text-input.util';
import { createThemedStyles, textStyleToCss } from '../theme';

type SimpleOnChange = (val: string | number) => void;

type DropdownProps<Value> = {
  /**
   * Optional attribute to determine if field should be focused after component mounts
   * Defaults to false
   */
  isAutoFocusEnabled?: boolean;
  /**
   * Optional attribute to determine size of select input, defaults to "default"
   */
  size?: 'small' | 'middle' | 'large';
  /**
   * Indicate if options for dropdown is loading
   */
  isLoading?: boolean;
  /**
   * Children which would be rendered next to select in dropdown wrapper
   */
  children?: React.ReactElement;
  /**
   * Optional props, determines if search options are enabled
   */
  isSearchable?: boolean;
  /**
   * Enables virtual options rendering in Select, disabled by default
   */
  isVirtual?: boolean;
  /**
   * Optional prop, determines className of dropdown
   */
  dropdownClassName?: string;
} & FieldProps &
  DefaultInputProps &
  Omit<AntInputProps, 'placeholder'> &
  (
    | {
        /**
         * List of complex option elements displayed inside dropdown
         */
        optionElements: ComplexOptionElement<Value>[];
        /**
         * For complex options required idResolver which would return id of the complex value
         */
        idResolver: (value: Value) => number | string;

        onChange?: (val: Value) => void;
      }
    | {
        /**
         * List of simple option elements displayed inside dropdown
         */
        optionElements: SimpleOptionElement[];

        onChange?: SimpleOnChange;
      }
  );

/**
 * Typing for "Option" element of dropdown input
 */
type BaseOptionElement = {
  title: string | number | React.ReactElement;
  isDisabled?: boolean;
};

type ComplexOptionElement<Value> = {
  value: Value;
} & BaseOptionElement;

type SimpleOptionElement = {
  value: string | number;
} & BaseOptionElement;

const useThemedStyles = createThemedStyles(theme => ({
  dropdownInput: {
    '& .ant-empty-description': {
      color: theme.colorSchema.neutral7,
      ...textStyleToCss(theme.typography.baseText)
    }
  }
}));

export const DropdownInput = <Value extends any>({
  optionElements,
  id,
  defaultValue,
  dropdownClassName,
  className,
  isAutoFocusEnabled,
  isDisabled,
  isLoading,
  isSearchable = false,
  isVirtual = false,
  placeholder,
  field,
  form,
  meta,
  virtualGroup,
  onChange,
  children,
  ...props
}: DropdownProps<Value>) => {
  const themedStyles = useThemedStyles();
  const { errors, setFieldValue } = form;
  const isInvalid = getIn(errors, field.name);
  const options = optionElements as any[];
  let idResolver: ((value: Value) => number | string) | null = null;
  let targetProps = props;

  if ('idResolver' in targetProps) {
    const { idResolver: skipped, ...propsWithoutIdPropName } = targetProps;
    idResolver = targetProps.idResolver;
    targetProps = propsWithoutIdPropName;
  }
  return (
    <Select
      isDisabled={isDisabled}
      isInsideForm={true}
      value={idResolver ? field.value && idResolver(field.value) : field.value}
      className={classNames(themedStyles.dropdownInput, className)}
      id={id}
      name={field.name}
      placeholder={placeholder}
      isSearchable={isSearchable}
      data-test-el={props['data-test-el'] || 'dropdown-input'}
      dropdownClassName={dropdownClassName}
      onChange={(value: unknown) => {
        if (typeof idResolver === 'function') {
          const selectedOption = options.find(
            option => idResolver!(option.value) === value
          );

          setFieldValue(field.name, selectedOption && selectedOption.value);

          if (onChange) {
            (onChange as SimpleOnChange)(
              selectedOption && selectedOption.value
            );
          }
        } else {
          setFieldValue(field.name, value);

          if (onChange) {
            (onChange as SimpleOnChange)(value as string | number);
          }
        }
      }}
      onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
        form.setFieldTouched(field.name);

        handleVirtualGroupBlurEvent(e, {
          virtualGroup,
          form,
          field,
          meta
        });
      }}
      aria-describedby={(props as any)['aria-describedby']}
      isLoading={isLoading}
      isVirtual={isVirtual}
      optionElements={
        optionElements as
          | SimpleOptionElement[]
          | ComplexOptionElement<string | number>[]
      }
      idResolver={idResolver!}
      isInvalid={isInvalid}
    >
      {children}
    </Select>
  );
};
