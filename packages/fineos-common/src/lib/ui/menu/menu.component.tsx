/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Menu as AntMenu } from 'antd';
import { MenuProps as AntMenuProps } from 'antd/lib/menu';

import { FormattedMessage } from '../formatted-message';
import { Link } from '../link';

const { Item } = AntMenu;

type MenuProps = {
  className?: string;
  mode?: React.ComponentProps<typeof AntMenu>['mode'];
  children: React.ReactElement[] | React.ReactElement;
};

export const Menu: React.FC<MenuProps & AntMenuProps> = ({
  className,
  mode,
  children,
  onClick
}) => (
  <AntMenu onClick={onClick} className={className} mode={mode}>
    {React.Children.map(children, (node: React.ReactElement) => (
      <Item key={node.key}>
        {node.type === Link ||
        (node.type === FormattedMessage && node.props.as === Link)
          ? React.cloneElement(node, {
              disableUnderline: true
            })
          : node}
      </Item>
    ))}
  </AntMenu>
);
