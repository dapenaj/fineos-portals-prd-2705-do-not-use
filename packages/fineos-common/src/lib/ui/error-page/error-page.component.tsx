/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';

import { createThemedStyles, textStyleToCss, useTheme } from '../theme';
import { AnonymousLayout } from '../layouts/page-layout';
import { ConfigurableText } from '../configurable-text';

import styles from './error-page.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  content: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  header: {
    ...textStyleToCss(theme.typography.displayTitle)
  }
}));

export const ErrorPage = () => {
  const themedStyles = useThemedStyles();
  const theme = useTheme();

  return (
    <AnonymousLayout>
      <Row className={styles.container} data-test-el="error-page-container">
        <Col offset={1} span={11} className={themedStyles.content}>
          <p className={themedStyles.header}>
            <ConfigurableText id="ERROR_PAGE.HEADER" />
          </p>
          <p>
            <ConfigurableText id="ERROR_PAGE.LINE_1" />
          </p>
          <p>
            <ConfigurableText id="ERROR_PAGE.LINE_2" />
          </p>
        </Col>
        {theme.images?.somethingWentWrongColor && (
          <Col span={11} className={styles.errorImageContainer}>
            <img
              alt=""
              src={theme.images.somethingWentWrongColor}
              className={styles.errorImage}
            />
          </Col>
        )}
      </Row>
    </AnonymousLayout>
  );
};
