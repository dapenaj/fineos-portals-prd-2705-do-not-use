/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import moment from 'moment';

import { Formatting } from '../../utils';
import { createThemedStyles } from '../theme';
import { TelLink } from '../tel-link';
import { EmailLink } from '../email-link';
import {
  FormattedMessage,
  FormattedMessageAs,
  FormattedMessageProps
} from '../formatted-message';

export type RichFormattedMessageProps<
  As extends FormattedMessageAs = 'span'
> = {
  /**
   * Optional date fallback formatting if passed Moment date is invalid. Default value "" (empty string)
   */
  dateFallback?: React.ReactNode;
} & FormattedMessageProps<As>;

const useThemedStyles = createThemedStyles(theme => ({
  strong: {
    fontWeight: theme.typography.baseTextSemiBold.fontWeight
  },
  highlight: {
    color: theme.colorSchema.primaryColor
  }
}));

/**
 * RichFormattedMessage is extended FormattedMessage from react-intl which contains
 * some predefined features like auto formatting Moment dates when passed as values
 */
export const RichFormattedMessage = <As extends FormattedMessageAs = 'span'>({
  values,
  dateFallback = '',
  ...props
}: RichFormattedMessageProps<As>) => {
  const themedStyles = useThemedStyles();
  const defaultValues = {
    strong: (content: string) => (
      <strong className={themedStyles.strong}>{content}</strong>
    ),
    tel: (content: string) => <TelLink phone={content}>{content}</TelLink>,
    email: (content: string) => (
      <EmailLink email={content}>{content}</EmailLink>
    ),
    highlight: (content: string) => (
      <span className={themedStyles.highlight}>{content}</span>
    )
  };

  const formatting = useContext(Formatting);
  const mappedValues =
    values &&
    Object.keys(values).reduce((acc: any, key) => {
      const value = values[key];

      acc[key] = moment.isMoment(value)
        ? value.isValid()
          ? value.format(formatting.date)
          : dateFallback
        : value;

      return acc;
    }, {});

  return (
    <FormattedMessage<As>
      {...(props as any)}
      values={{ ...defaultValues, ...mappedValues }}
    />
  );
};
