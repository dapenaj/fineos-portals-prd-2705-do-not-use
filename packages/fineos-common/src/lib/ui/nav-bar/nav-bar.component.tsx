/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import { ContentLayout } from '../layouts/content-layout';
import { Menu } from '../menu';
import { createThemedStyles, textStyleToCss } from '../theme';

import styles from './nav-bar.module.scss';

type NavBarProps = {
  children: React.ReactElement[];
};

const useThemedStyles = createThemedStyles(theme => {
  return {
    navBar: {
      backgroundColor: theme.colorSchema.neutral1,
      '& .ant-menu-horizontal > .ant-menu-item > a': {
        color: theme.colorSchema.neutral8,
        ...textStyleToCss(theme.typography.baseText, {
          suppressLineHeight: true
        })
      },
      '& .ant-menu-horizontal > .ant-menu-item > a.active': {
        color: theme.colorSchema.primaryColor,
        ...textStyleToCss(theme.typography.baseTextSemiBold, {
          suppressLineHeight: true
        })
      },
      '& .ant-menu-horizontal > .ant-menu-item > a:hover': {
        color: theme.colorSchema.primaryAction
      },
      '& .ant-menu-horizontal > .ant-menu-item:hover': {
        borderBottomColor: theme.colorSchema.primaryAction
      }
    },
    navigation: {
      background: theme.colorSchema.neutral1
    }
  };
});

export const NavBar: React.FC<NavBarProps> = ({ children }) => {
  const themedStyles = useThemedStyles();

  return (
    <div
      className={classNames(styles.navBar, themedStyles.navBar)}
      data-test-el="nav-bar"
      role="navigation"
    >
      <ContentLayout className={styles.wrapper}>
        <Menu
          mode="horizontal"
          className={classNames(styles.menu, themedStyles.navigation)}
        >
          {children}
        </Menu>
      </ContentLayout>
    </div>
  );
};
