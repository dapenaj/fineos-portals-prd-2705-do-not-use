/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { TextStyle, ThemeModel } from '../theme';

const textStyleToOptions = (textStyle: TextStyle) => {
  return (
    (textStyle.fontWeight === 'normal' ? 400 : textStyle.fontWeight) +
    (textStyle.fontStyle === 'italic' ? 'i' : '')
  );
};

type DiscoveredFont = { [index: string]: string[] };

const discoverFonts = (textStyles: TextStyle[]): DiscoveredFont =>
  textStyles.reduce((fonts: DiscoveredFont, textStyle) => {
    if (textStyle.fontFamily) {
      if (!fonts[textStyle.fontFamily]) {
        fonts[textStyle.fontFamily] = [textStyleToOptions(textStyle)];
      } else {
        const textStyleOptions = textStyleToOptions(textStyle);

        if (!fonts[textStyle.fontFamily].includes(textStyleOptions)) {
          fonts[textStyle.fontFamily].push(textStyleOptions);
        }
      }
    }

    return fonts;
  }, {});

type FontLoadingOptions = {
  display: 'auto' | 'block' | 'swap' | 'fallback' | 'optional';
};

export const loadThemeFonts = (
  theme: ThemeModel,
  targetEl: Node,
  loadingOptions: FontLoadingOptions
) => {
  const formatFontFamily = (fontFamily: string) =>
    fontFamily.replace(/"/g, '').replace(' ', '+');
  const fonts = discoverFonts([
    ...Object.values(theme.typography),
    ...(theme.header.titleStyles ? [theme.header.titleStyles] : [])
  ]);

  // tslint:disable-next-line: forin
  for (const fontFamily in fonts) {
    const linkEl = document.createElement('link');
    const options = fonts[fontFamily].sort().join(',');
    linkEl.rel = 'stylesheet';
    linkEl.href = `https://fonts.googleapis.com/css?family=${formatFontFamily(
      fontFamily
    )}:${options}&display=${loadingOptions.display}`;

    targetEl.insertBefore(linkEl, targetEl.firstChild);
  }
};
