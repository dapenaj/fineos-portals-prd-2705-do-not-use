/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { TransitionEvent, useEffect, useRef, useState } from 'react';
import { useDialogState, Dialog, DialogBackdrop } from 'reakit';
import { useIntl } from 'react-intl';
import classNames from 'classnames';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { ButtonType } from '../../types';
import { getFocusedElementShadow } from '../../utils';
import { createThemedStyles, textStyleToCss } from '../theme';
import { Icon } from '../icon';
import { FormattedMessage } from '../formatted-message';
import { Button } from '../button';

import styles from './drawer.module.scss';

export enum DrawerWidth {
  NARROW = '480px',
  MEDIUM = '640px',
  WIDE = '820px',
  EXTRA_WIDE = '1000px',
  FULL_PAGE = '80%'
}

export type DrawerProps = {
  /**
   * Optional, determines if close icon is visible and closing is enabled, defaults to true
   */
  isClosable?: boolean;
  /**
   * Determines if drawer is initially visible, defaults to false
   */
  isVisible?: boolean;
  /**
   * Optional title of drawer
   */
  title?: string | React.ReactNode;
  /**
   * Optional, determines width if drawer has vertical orientation. Defaults to DrawerWidth.NARROW = 256
   */
  width?: DrawerWidth;
  /**
   * Optional, determines callback called when close button is pressed
   */
  onClose?: (event?: React.SyntheticEvent) => void;
  /**
   * Optional className appended to drawer
   */
  className?: string;
  /**
   * Optional header className appended to drawer
   */
  headerClassName?: string;
  /**
   * Optional body className appended to drawer
   */
  bodyClassName?: string;
  /**
   * Modal aria-label, required by a11y.
   */
  ariaLabelId: string;
  /**
   * Optional values for aria-label translations.
   */
  ariaLabelValues?: any;
  /**
   * Optional flag to determine hiding on esc, default to true
   */
  hideOnEsc?: boolean;
  /**
   * Optional test id selector, defaults to "drawer"
   */
  'data-test-el'?: string;
};

export const useThemedStyles = createThemedStyles(theme => ({
  drawer: {
    ...textStyleToCss(theme.typography.baseText),
    background: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral3,
    '&:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  drawerHeader: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8,
    background: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral3
  },
  drawerBody: {
    background: theme.colorSchema.neutral1
  },
  drawerCloseIcon: {
    color: theme.colorSchema.neutral7,
    fontSize: theme.typography.baseText.fontSize,
    '&:focus': {
      color: theme.colorSchema.neutral7,
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  drawerFooter: {
    backgroundColor: theme.colorSchema.neutral1,
    borderTopColor: theme.colorSchema.neutral3
  }
}));

export const Drawer: React.FC<DrawerProps> = ({
  isVisible = false,
  title,
  width = DrawerWidth.NARROW,
  isClosable = true,
  hideOnEsc = true,
  onClose,
  className,
  headerClassName,
  bodyClassName,
  ariaLabelId,
  ariaLabelValues,
  children,
  'data-test-el': dataTestEl = 'drawer',
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const modalState = useDialogState({ animated: true });
  const intl = useIntl();
  const dialogRef = useRef<HTMLDivElement>(null);
  const backdropRef = useRef<HTMLDivElement>(null);
  const [localVisible, setLocalVisible] = useState(false);
  const [mouseTarget, setMouseTarget] = useState<EventTarget>();

  useEffect(() => {
    if (isVisible) {
      setLocalVisible(true);
      modalState.show();
    } else {
      modalState.hide();
    }
  }, [isVisible]);

  const onTransitionEnd = ({ target }: TransitionEvent) => {
    const isTargetDialog =
      target === dialogRef.current || target === backdropRef.current;

    if (isTargetDialog && localVisible !== isVisible) {
      setLocalVisible(isVisible);
    }
  };

  return (
    <DialogBackdrop
      data-test-el="drawer-container"
      ref={backdropRef}
      onTransitionEnd={onTransitionEnd}
      tabIndex={isVisible ? 0 : -1}
      className={styles.drawerBackdrop}
      // this is to avoid closing modal on releasing mouse click outside of the modal
      onMouseDown={(event: React.MouseEvent) => {
        setMouseTarget(event.target);
      }}
      onMouseUp={() => {
        if (mouseTarget === backdropRef.current && onClose) {
          onClose();
        }
      }}
      {...modalState}
    >
      <Dialog
        ref={dialogRef}
        hideOnClickOutside={false}
        hideOnEsc={hideOnEsc}
        onClick={(event: React.MouseEvent) => event.stopPropagation()}
        aria-label={intl.formatMessage(
          {
            id: ariaLabelId
          },
          ariaLabelValues
        )}
        onKeyDown={(event: React.KeyboardEvent) => {
          if (event.key === 'Escape' && onClose) {
            onClose();
          }
        }}
        role="drawer"
        tabIndex={0}
        className={classNames(styles.drawer, themedStyles.drawer, className)}
        style={{ width }}
        {...modalState}
        {...props}
      >
        {localVisible ? (
          <div className={styles.drawerContent} data-test-el={dataTestEl}>
            <div
              className={classNames(
                styles.drawerHeader,
                themedStyles.drawerHeader,
                headerClassName
              )}
            >
              {title}
              {isClosable && (
                <Button
                  aria-label={intl.formatMessage({
                    id: 'FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL'
                  })}
                  data-test-el="drawer-close-icon-button"
                  buttonType={ButtonType.LINK}
                  className={classNames(
                    styles.drawerCloseIcon,
                    themedStyles.drawerCloseIcon
                  )}
                  onClick={onClose}
                >
                  <Icon
                    icon={faTimes}
                    iconLabel={
                      <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL" />
                    }
                  />
                </Button>
              )}
            </div>
            <div className={classNames(styles.drawerBody, bodyClassName)}>
              {children}
            </div>
          </div>
        ) : null}
      </Dialog>
    </DialogBackdrop>
  );
};

export const DrawerFooter: React.FC<{}> = ({ children }) => {
  const themedStyles = useThemedStyles();
  return (
    <div className={classNames(styles.drawerFooter, themedStyles.drawerFooter)}>
      {children}
    </div>
  );
};
