/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { ReactNode } from 'react';
import { Input as AntInput } from 'antd';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss, toRgba } from '../theme';
import { DefaultInputProps } from '../../types';
import { useRawFormattedMessage } from '../formatted-message';

type InputProps = {
  /**
   * The input content value.
   */
  value: string;
  /**
   * Optional label for Input.
   */
  label?: string;
  /**
   * Determines if input is disabled. Defaults to false.
   */
  isDisabled?: boolean;
  /**
   * Optional classname applied to the panel.
   */
  className?: string;
  /**
   * Allows input to remove content with clean icon. Defaults to true.
   */
  allowClear?: boolean;
  /**
   * Optional label text on the left side of the input field.
   */
  addonBefore?: string | ReactNode;
  /**
   * Optional label text on the right side of the input field.
   */
  addonAfter?: string | ReactNode;
  /**
   * Callback function executed when the input is changing.
   */
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  /**
   * Callback function executed when key is pressed.
   */
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  /**
   * The placeholder text to populate the input.
   */
  placeholder?: string | undefined;
  /**
   * Optional prop, determines if inputs aria-invalid should
   * externally set to true
   */
  isInvalid?: boolean;
  /**
   * The suffix icon for the Input
   */
  suffix?: ReactNode;
  /**
   * Optional prop to indicate describing element.
   */
  'aria-describedby'?: string;
  /**
   * Optional test element name. Defaults to "input".
   */
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  input: {
    '& .ant-input': {
      backgroundColor: theme.colorSchema.neutral1,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-input::placeholder': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral6
    },
    '& .ant-input-clear-icon, .ant-input-suffix': {
      color: theme.colorSchema.neutral6
    }
  },
  search: {
    '& .ant-input-search': {
      backgroundColor: theme.colorSchema.neutral1,
      boxShadow: `inset 0px 2px 4px ${toRgba(
        theme.colorSchema,
        'neutral7',
        0.15
      )}`
    },
    '& .ant-input::placeholder': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral6
    },
    '& .ant-input-affix-wrapper': {
      border: `1px solid ${theme.colorSchema.neutral6}`
    },
    '& .ant-input-search-button': {
      border: `1px solid ${theme.colorSchema.neutral6}`,
      backgroundColor: theme.colorSchema.neutral1,
      color: `${theme.colorSchema.neutral6} !important`
    },
    '& .ant-input-clear-icon': {
      color: theme.colorSchema.neutral6
    }
  }
}));

type InputType = React.FC<InputProps & DefaultInputProps> & {
  Search: React.FC<React.ComponentProps<typeof AntInput.Search>>;
};

const SearchInput: React.FC<React.ComponentProps<typeof AntInput.Search>> = ({
  className,
  placeholder,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const inputPlaceholder = useRawFormattedMessage(placeholder);

  return (
    <AntInput.Search
      placeholder={inputPlaceholder}
      className={classNames(themedStyles.input, themedStyles.search, className)}
      {...props}
    />
  );
};

const Input: InputType = ({
  name,
  value,
  isDisabled = false,
  className,
  addonBefore,
  addonAfter,
  allowClear,
  onChange,
  onKeyDown,
  label,
  placeholder,
  isInvalid,
  suffix,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const inputPlaceholder = useRawFormattedMessage(placeholder);

  return (
    <AntInput
      data-test-el={props['data-test-el'] || 'input'}
      className={classNames(themedStyles.input, className)}
      name={name}
      value={value}
      disabled={isDisabled}
      allowClear={allowClear}
      onChange={onChange}
      onKeyDown={onKeyDown}
      aria-label={label}
      aria-describedby={isInvalid ? props['aria-describedby'] : undefined}
      aria-invalid={isInvalid}
      placeholder={inputPlaceholder}
      addonBefore={addonBefore}
      addonAfter={addonAfter}
      suffix={suffix}
    />
  );
};

Input.Search = SearchInput;

export { Input };
