/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import { createThemedStyles } from '../theme';

import styles from './text-badge.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    color: theme.colorSchema.neutral1,
    backgroundColor: theme.colorSchema.callToActionColor
  }
}));

type TextBadgeProps = {
  label: React.ReactNode;
  isLabelVisible?: boolean;
} & React.HTMLAttributes<HTMLElement>;

export const TextBadge: React.FC<TextBadgeProps> = ({
  children,
  label,
  isLabelVisible = true,
  className,
  ...props
}) => {
  const themedStyles = useThemedStyles();

  return (
    <span
      data-test-el="text-badge-container"
      className={classNames(styles.container, className)}
      {...props}
    >
      {children}
      {isLabelVisible && (
        <span
          className={classNames(styles.label, themedStyles.label)}
          data-test-el="text-badge-label"
        >
          {label}
        </span>
      )}
    </span>
  );
};
