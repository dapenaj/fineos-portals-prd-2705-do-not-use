/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Spin } from 'antd';
import classNames from 'classnames';

import { createThemedStyles } from '../theme';
import { SpinnerType } from '../../types';

import styles from './overlay-spinner.module.scss';

export type OverlaySpinnerProps = {
  size?: SpinnerType;
  className?: string;
  spinnerClassName?: string;
  isLoading: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  overlaySpinner: {
    backgroundColor: theme.colorSchema.neutral1
  },
  spinner: {
    '& .ant-spin': {
      color: theme.colorSchema.primaryColor
    },
    '& .ant-spin-dot-item': {
      backgroundColor: theme.colorSchema.primaryColor
    }
  }
}));

export const OverlaySpinner: React.FC<OverlaySpinnerProps> = ({
  className,
  spinnerClassName,
  size = SpinnerType.LARGE,
  isLoading,
  children
}) => {
  const themedStyles = useThemedStyles();

  return (
    <div className={styles.overlayWrapper}>
      {isLoading && (
        <div
          className={classNames(
            themedStyles.overlaySpinner,
            styles.overlaySpinner,
            className,
            {
              [styles.small]: size === SpinnerType.SMALL
            }
          )}
          data-test-el="spinner"
        >
          <Spin
            className={classNames(themedStyles.spinner, spinnerClassName)}
            size={size === SpinnerType.MEDIUM ? 'default' : size}
          />
        </div>
      )}
      {children}
    </div>
  );
};
