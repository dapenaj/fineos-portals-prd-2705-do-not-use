/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';
import { WarningFilled } from '@ant-design/icons';

import { createThemedStyles, lightenColor } from '../theme';

import styles from './warning-state.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    backgroundColor: lightenColor(theme.colorSchema, 'pendingColor')
  },
  icon: {
    color: theme.colorSchema.pendingColor
  }
}));

type WarningStateProps = {
  /**
   * Optional title to display
   */
  title?: React.ReactNode;
  /**
   * Optional footer to display
   */
  footer?: React.ReactNode;
  /**
   * Optional classname applied to the tooltip trigger
   */
  className?: string;
  /**
   * Optional test element name. Defaults to "tooltip".
   */
  'data-test-el'?: string;
};

/**
 * Wraps it's children in a generic warning state styling.
 */
export const WarningState: React.FC<WarningStateProps> = ({
  title,
  footer,
  className,
  children,
  ...props
}) => {
  const themedStyles = useThemedStyles();

  return (
    <div
      className={classnames(className, styles.wrapper, themedStyles.wrapper)}
      data-test-el={props['data-test-el'] || 'warning-state'}
    >
      <div className={classnames(styles.icon, themedStyles.icon)}>
        <WarningFilled />
      </div>
      <div className={styles.content}>
        {title && <>{title}</>}
        {children}
        {footer && <>{footer}</>}
      </div>
    </div>
  );
};
