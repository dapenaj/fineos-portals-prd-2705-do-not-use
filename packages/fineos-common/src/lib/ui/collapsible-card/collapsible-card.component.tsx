/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Collapse } from 'antd';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';
import { getFocusedElementShadow } from '../../utils';
import {
  PossiblyFormattedMessageElement,
  useRawFormattedMessage
} from '../formatted-message';

import styles from './collapsible-card.module.scss';

const { Panel } = Collapse;

type HeaderRenderer = (config: { isCollapsed: boolean }) => React.ReactNode;

type PanelProxyProps = Omit<React.ComponentProps<typeof Panel>, 'header'> & {
  header: React.ReactNode | HeaderRenderer;
  /**
   * Technically isActive is not a prop for Panel, but it is essential.
   */
  isActive?: boolean;
};

type CollapsibleCardProps = {
  /**
   * Content to display for the Header. This can be a ReactNode or a function.
   * If using a function, the additional property isCollapsed will be available.
   */
  title: React.ReactNode | HeaderRenderer;
  /**
   * Optional boolean to determine if card is rendered open or not. Defaults to closed.
   */
  isOpen?: boolean;
  /**
   * Optional boolean to determine if card has drop shadow. Defaults to true.
   */
  dropShadow?: boolean;
  /**
   * Optional classname applied to the panel.
   */
  className?: string;
  /**
   * Optional test element name. Defaults to "panel".
   */
  'data-test-el'?: string;

  isPanelUI?: boolean;
  /**
   * Optional boolean to determine if card has border between header and body. Defaults to yes.
   */
  bordered?: boolean;
};

const useThemedStyles = createThemedStyles(theme => {
  return {
    panelWrapper: {
      '& .ant-collapse > .ant-collapse-item > .ant-collapse-header': {
        ...textStyleToCss(theme.typography.panelTitle),
        backgroundColor: theme.colorSchema.neutral1,
        color: theme.colorSchema.neutral8,
        '&:focus': {
          borderColor: theme.colorSchema.primaryAction,
          boxShadow: getFocusedElementShadow({
            color: theme.colorSchema.primaryAction
          })
        }
      }
    },
    body: {
      '& .ant-collapse > .ant-collapse-item': {
        '& > .ant-collapse-header:focus': {
          borderColor: theme.colorSchema.primaryAction,
          boxShadow: getFocusedElementShadow({
            color: theme.colorSchema.primaryAction
          })
        },
        '& .ant-collapse-header .ant-collapse-arrow': {
          color: theme.colorSchema.neutral8
        },
        '& .ant-collapse-content': {
          backgroundColor: theme.colorSchema.neutral1
        }
      }
    }
  };
});

/**
 * Wrapper component for the Panel component from antd.
 * If the header prop is a function, then it will receive additional props (isCollapsed).
 */
const PanelProxy: React.FC<PanelProxyProps> = ({ header, ...props }) => (
  <Panel
    header={
      typeof header === 'function'
        ? header({ isCollapsed: !props.isActive })
        : header
    }
    {...props}
  />
);

export const CollapsibleCard: React.FC<CollapsibleCardProps & {
  role?: JSX.IntrinsicElements['div']['role'];
  'aria-label'?: PossiblyFormattedMessageElement;
}> = ({
  title,
  isOpen,
  dropShadow = true,
  isPanelUI,
  className,
  children,
  bordered = true,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const translatedAriaLabel = useRawFormattedMessage(props['aria-label']);
  const collapsibleCardClasses = !dropShadow
    ? classNames(className, styles.noShadow)
    : classNames(styles.collapsibleCard, themedStyles.body, className, {
        [classNames(themedStyles.panelWrapper, styles.panelWrapper)]: isPanelUI
      });

  return (
    <div
      className={collapsibleCardClasses}
      data-test-el={props['data-test-el'] || 'collapsible-card'}
      role={props.role}
      aria-label={translatedAriaLabel}
    >
      <Collapse bordered={bordered} defaultActiveKey={isOpen ? ['1'] : ['0']}>
        <PanelProxy header={title} key="1">
          {children}
        </PanelProxy>
      </Collapse>
    </div>
  );
};
