/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';

import {
  lightenColor,
  createThemedStyles,
  ThemedColorSchema
} from '../../theme';
import { StatusType, StatusPattern } from '../../../types';

import styles from './card-body-with-status.module.scss';

type CardBodyWithStatusProps = {
  /**
   * Indicates the status type using the StatusType enum. Defaults to a blank styling.
   */
  type?: StatusType;
  /**
   * Indicates the status pattern using the StatusPattern enum. Defaults to FULL.
   */
  pattern?: StatusPattern;
  /**
   * Optional test element name. Defaults to "card-body-with-status".
   */
  'data-test-el'?: string;
};

const STRIPE_DISTANCE_PX = 10;

const createTypeStyles = (
  colorScheme: ThemedColorSchema,
  {
    color,
    lighterColor
  }: {
    color: keyof ThemedColorSchema;
    lighterColor?: keyof ThemedColorSchema;
  }
) => {
  const lightenedColor = lighterColor
    ? colorScheme[lighterColor]
    : lightenColor(colorScheme, color);
  return {
    '&.full': {
      '&::before': {
        background: colorScheme[color]
      }
    },
    '&.angled': {
      '&::before': {
        background: `repeating-linear-gradient(
        45deg,
        ${lightenedColor},
        ${lightenedColor} ${STRIPE_DISTANCE_PX}px,
        ${colorScheme[color]} ${STRIPE_DISTANCE_PX}px,
        ${colorScheme[color]} ${STRIPE_DISTANCE_PX * 2}px
      )`
      }
    },
    '&.halved': {
      '&::before': {
        background: `linear-gradient(
        to right,
        ${lightenedColor},
        ${lightenedColor} 50%,
        ${colorScheme[color]} 50%,
        ${colorScheme[color]}
      )`
      }
    }
  };
};

const useThemedStyles = createThemedStyles(theme => ({
  success: createTypeStyles(theme.colorSchema, { color: 'successColor' }),
  warning: createTypeStyles(theme.colorSchema, { color: 'pendingColor' }),
  danger: createTypeStyles(theme.colorSchema, { color: 'attentionColor' }),
  neutral: createTypeStyles(theme.colorSchema, {
    color: 'neutral6',
    lighterColor: 'neutral5'
  }),
  blank: createTypeStyles(theme.colorSchema, { color: 'neutral1' })
}));

/**
 * Used to display a card with a status indication along the left bar.
 * The colour and pattern of the bar can be configured using the prop values.
 */
export const CardBodyWithStatus: React.FC<CardBodyWithStatusProps> = ({
  type = StatusType.BLANK,
  pattern = StatusPattern.FULL,
  children,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <div
      data-test-el={props['data-test-el'] || 'card-body-with-status'}
      className={classnames(styles.wrapper, themedStyles[type], pattern)}
      {...props}
    >
      <div className={styles.content}>{children}</div>
    </div>
  );
};
