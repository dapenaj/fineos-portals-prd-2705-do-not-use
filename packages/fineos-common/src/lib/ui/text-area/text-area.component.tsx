/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect } from 'react';
import { Input } from 'antd';
import { FieldProps, getIn } from 'formik';
import { useIntl } from 'react-intl';
import classNames from 'classnames';

import { DefaultInputProps } from '../../types';
import { FormGroupContext, getFocusedElementShadow } from '../../utils';
import { createThemedStyles, pxToRem, textStyleToCss } from '../theme';

import styles from './text-area.module.scss';

type TextAreaProps = {
  /**
   * Optional text-area size. In case of passing boolean autoSize is enabled/disabled,
   * in case of passing object, max and min text-area sizes are specified. Defaults to false
   */
  autoSize?: boolean | { maxRows: number; minRows: number };
} & DefaultInputProps &
  FieldProps;

const useThemedStyles = createThemedStyles(theme => ({
  textAreaWrapper: {
    '& .ant-input::placeholder': {
      color: theme.colorSchema.neutral6
    },
    '& .ant-input:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  textArea: {
    borderColor: theme.colorSchema.neutral5,
    '&.ant-input, &.ant-input:hover, &.ant-input-affix-wrapper:hover': {
      minHeight: pxToRem(theme.typography.baseText.lineHeight!),
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8,
      backgroundColor: theme.colorSchema.neutral1
    }
  }
}));

export const TextArea: React.FC<TextAreaProps> = ({
  autoSize = false,
  isDisabled = false,
  field,
  form,
  meta,
  children,
  placeholder,
  className,
  wrapperClassName,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const isInvalid = getIn(form.errors, field.name);
  const { isRequired, disableFieldErrorIcon } = useContext(FormGroupContext);
  const intl = useIntl();
  const usablePlaceholder =
    typeof placeholder !== 'string' && !!placeholder
      ? intl.formatMessage(placeholder.props, placeholder.props.values)
      : placeholder;

  useEffect(() => {
    disableFieldErrorIcon();
  }, []);

  return (
    <div
      className={classNames(
        themedStyles.textAreaWrapper,
        styles.textAreaWrapper,
        wrapperClassName
      )}
    >
      <Input.TextArea
        className={classNames(
          themedStyles.textArea,
          styles.textArea,
          className
        )}
        name={field.name}
        id={field.name}
        autoSize={autoSize}
        disabled={isDisabled}
        defaultValue={field.value}
        data-test-el={props['data-test-el'] || 'text-area'}
        onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
          field.onChange(e)
        }
        aria-invalid={isInvalid ? 'true' : undefined}
        aria-required={isRequired}
        placeholder={usablePlaceholder}
        onBlur={e => field.onBlur(e)}
        {...props}
      />
      {children}
    </div>
  );
};
