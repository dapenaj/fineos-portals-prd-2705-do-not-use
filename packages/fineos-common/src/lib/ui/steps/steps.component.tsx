/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Steps as AntSteps } from 'antd';
import { StepsProps as AntStepsProps } from 'antd/lib/steps';
import { faCheckCircle as farCheckCircle } from '@fortawesome/free-regular-svg-icons';
import classNames from 'classnames';
import { isNil as _isNil } from 'lodash';

import { createThemedStyles, textStyleToCss } from '../theme';
import { FormattedMessage } from '../formatted-message';
import { Icon } from '../icon';
import { ContentLayout } from '../layouts';

import { StepType } from './steps.type';
import styles from './steps.module.scss';

const { Step } = AntSteps;

type StepsProps = {
  steps: StepType[];
  current: number;
  /**
   * Make step button not accessible
   */
  isNotAccessible?: boolean;
  maxAvailableStep?: number;
  /**
   * Disables steps navigation
   */
  isStepsDisabled?: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    backgroundColor: theme.colorSchema.neutral2,
    borderTopColor: theme.colorSchema.neutral5,
    borderBottomColor: theme.colorSchema.neutral5,
    '& .ant-steps-item-wait .ant-steps-item-container': {
      '& .ant-steps-item-icon': {
        backgroundColor: theme.colorSchema.neutral2,
        borderColor: theme.colorSchema.neutral5
      },
      '& .ant-steps-item-content .ant-steps-item-title': {
        color: theme.colorSchema.neutral7
      }
    },
    '& .ant-steps-item-process .ant-steps-item-container .ant-steps-item-icon': {
      background: theme.colorSchema.primaryColor,
      borderColor: theme.colorSchema.primaryColor
    },
    '& .ant-steps-item-container > .ant-steps-item-content > .ant-steps-item-title::after': {
      backgroundColor: theme.colorSchema.neutral5
    },
    '& .ant-steps-item-process > .ant-steps-item-container > .ant-steps-item-icon .ant-steps-icon': {
      color: theme.colorSchema.neutral1,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-steps-item-wait .ant-steps-item-icon > .ant-steps-icon': {
      color: theme.colorSchema.neutral6,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  finishStep: {
    color: theme.colorSchema.primaryColor
  },
  text: {
    '& .ant-steps-item-container .ant-steps-item-content .ant-steps-item-title': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.panelTitle)
    }
  }
}));

/**
 * This component wrap step and exclude onStepClick which would make step inaccessible
 *
 * This rely on the internal rc-steps implementation, which not present in types,
 * which force to use `any` type
 */
const InAccessibleStep = ({ onStepClick, stepIndex, ...props }: any) => {
  return (
    <Step
      {...props}
      className={classNames(props.className, {
        [styles.disabledStep]: props.disabled
      })}
      onClick={() => {
        if (!props.disabled) {
          onStepClick(stepIndex);
        }
      }}
    />
  );
};

export const Steps: React.FC<StepsProps & AntStepsProps> = ({
  steps,
  current,
  className,
  maxAvailableStep,
  isNotAccessible = false,
  isStepsDisabled = false,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <div
      className={classNames(themedStyles.wrapper, className, styles.wrapper)}
    >
      <ContentLayout className={styles.stepsContentLayout}>
        <AntSteps current={current} {...props}>
          {steps.map(({ title }, index) => {
            const ActualStep = isNotAccessible ? InAccessibleStep : Step;
            const isDisabled =
              isStepsDisabled ||
              (!_isNil(maxAvailableStep) && maxAvailableStep < index);
            return (
              <ActualStep
                className={classNames(themedStyles.text, styles.step)}
                key={title}
                icon={
                  index < current && (
                    <Icon
                      size="lg"
                      className={themedStyles.finishStep}
                      icon={farCheckCircle}
                      iconLabel={
                        <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.AFFIRMATIVE_ICON_LABEL" />
                      }
                    />
                  )
                }
                title={
                  <FormattedMessage
                    id={title}
                    tabIndex={0}
                    onKeyDown={(e: React.KeyboardEvent) =>
                      e.key === 'Enter' && !isDisabled && props.onChange!(index)
                    }
                  />
                }
                disabled={isDisabled}
              />
            );
          })}
        </AntSteps>
      </ContentLayout>
    </div>
  );
};
