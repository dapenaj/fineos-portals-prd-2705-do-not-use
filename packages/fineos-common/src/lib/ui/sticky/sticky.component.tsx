/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Affix } from 'antd';

import styles from './sticky.module.scss';

type StickyProps = {
  target: React.RefObject<HTMLDivElement>;
  offsetTop?: number;
};

/**
 * Make content fixed without affecting regular elements flow
 */
export const Sticky: React.FC<StickyProps> = ({
  children,
  target,
  offsetTop
}) => (
  <Affix
    data-test-el="sticky"
    offsetTop={offsetTop}
    className={styles.affix}
    target={() => target.current}
  >
    {children}
  </Affix>
);
