/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  useTooltipState,
  Tooltip as ReakitTooltip,
  TooltipReference,
  TooltipArrow
} from 'reakit';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';

import styles from './tooltip.module.scss';

// TODO It should be replaced with internal reakit type for popover placement
// if reakit will provide this type
export type TooltipPlacementType =
  | 'auto'
  | 'top'
  | 'right'
  | 'bottom'
  | 'left'
  | 'top-start'
  | 'right-start'
  | 'bottom-start'
  | 'left-start'
  | 'top-end'
  | 'right-end'
  | 'bottom-end'
  | 'left-end';

type TooltipProps = {
  /**
   * Element which is the tooltip reference
   */
  children: JSX.Element;
  /**
   * Content of the tooltip
   */
  tooltipContent: React.ReactElement | string;
  /**
   * Tooltip placement, defaults to auto.
   */
  placement?: TooltipPlacementType;
};

const useThemedStyles = createThemedStyles(theme => ({
  tooltipDefaults: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8,
    backgroundColor: theme.colorSchema.neutral1
  },
  arrow: {
    fill: theme.colorSchema.neutral1
  }
}));

export const Tooltip: React.FC<TooltipProps> = ({
  children,
  tooltipContent,
  placement = 'auto'
}) => {
  const tooltip = useTooltipState({ placement });
  const themedStyles = useThemedStyles();

  return (
    <>
      <TooltipReference
        role="tooltip"
        {...tooltip}
        className={styles.tooltipReference}
      >
        {children}
      </TooltipReference>
      <ReakitTooltip
        {...tooltip}
        className={classNames(
          styles.tooltipContent,
          themedStyles.tooltipDefaults
        )}
        data-test-el="tooltip"
      >
        <div className={styles.arrowShadow}>
          <TooltipArrow {...tooltip} className={themedStyles.arrow} />
          {tooltipContent}
        </div>
      </ReakitTooltip>
    </>
  );
};
