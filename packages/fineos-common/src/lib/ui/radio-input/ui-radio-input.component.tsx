/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Radio } from 'antd';
import classNames from 'classnames';
import { RadioChangeEvent as AntRadioChangeEvent } from 'antd/lib/radio';
import { InputProps as AntInputProps } from 'antd/lib/input';

import { DefaultInputProps } from '../../types/form.type';
import { createThemedStyles, ThemeModel } from '../theme';
import { getFocusedElementShadow } from '../../utils';

import styles from './radio-input.module.scss';

export type RadioChangeEvent = AntRadioChangeEvent;
type RadioProps<Value extends any = string | number | boolean | undefined> = {
  /**
   * Optional attribute to determine if radio group should have vertical or horizontal direction.
   * Defaults to false (horizontal)
   */
  isVertical?: boolean;
  /**
   * Optional attribute to determine if field is invalid and add aria-invalid attribute if needed.
   */
  isInvalid?: boolean;
  /**
   * Optional attribute to determine if field is required, defaults to false.
   */
  isRequired?: boolean;
  /**
   * Optional onChange handler
   */
  onChange?: (e: RadioChangeEvent) => void;
  onBlur?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  /**
   * List of radio elements displayed inside radio group
   */
  children: React.ReactElement<RadioChild<Value>>[];
  /**
   * Component to wrap children
   */
  wrapperComponent?: (index: number) => React.ReactElement;
};

/**
 * Typing for "Radio" element of radio group
 */
type RadioChild<Value> = {
  className?: string;
  'data-test-el': string;
  value: Value;
  disabled: boolean;
  onBlur: (e: React.ChangeEvent<HTMLInputElement>) => void;
  key: number | string;
};

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  radio: {
    '& .ant-radio-input:focus + .ant-radio-inner': {
      boxShadow: getFocusedElementShadow({
        isWide: true,
        color: theme.colorSchema.primaryAction
      })
    },
    '& .ant-radio:hover .ant-radio-inner, .ant-radio-checked::after, .ant-radio-checked .ant-radio-inner': {
      borderColor: theme.colorSchema.primaryAction
    },
    '& .ant-radio-checked .ant-radio-inner::after': {
      backgroundColor: theme.colorSchema.primaryAction
    },
    '& .ant-radio-inner': {
      borderColor: theme.colorSchema.neutral5
    }
  }
}));

export const UiRadioInput: React.FC<RadioProps &
  DefaultInputProps &
  Omit<AntInputProps, 'form' | 'onChange'>> = ({
  className,
  isVertical = false,
  onChange,
  isDisabled,
  children,
  wrapperComponent,
  name,
  value,
  onBlur,
  isInvalid,
  isRequired = false,
  ...props
}) => {
  const themedStyles = useThemedStyles();

  const clonedChildren = children.map(option =>
    React.cloneElement(option, {
      className: classNames(themedStyles.radio, option.props.className, {
        [styles.isVertical]: isVertical
      }),
      'data-test-el': props['data-test-el'] || 'radio-input',
      onBlur: (e: React.ChangeEvent<HTMLInputElement>) => {
        if (onBlur) {
          onBlur(e);
        }
      },
      key: option.key as string | number
    })
  );

  const childrenWithWrappedComponent = clonedChildren.map(
    (clonedChild, index) => {
      if (wrapperComponent) {
        return React.cloneElement(wrapperComponent(index), {}, clonedChild);
      }
    }
  );

  return (
    <Radio.Group
      name={name}
      value={value}
      disabled={isDisabled}
      className={className}
      onChange={(e: RadioChangeEvent) => {
        if (onChange) {
          onChange(e);
        }
      }}
      aria-invalid={isInvalid}
      aria-required={isRequired}
      aria-describedby={props['aria-describedby']}
      {...props}
    >
      {wrapperComponent ? childrenWithWrappedComponent : clonedChildren}
    </Radio.Group>
  );
};
