/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { ComponentProps, useContext, useEffect } from 'react';
import { FieldProps, getIn } from 'formik';
import { RadioChangeEvent } from 'antd/lib/radio';
import { InputProps as AntInputProps } from 'antd/lib/input';

import { FormGroupContext } from '../../utils';
import { DefaultInputProps } from '../../types';

import { UiRadioInput } from './ui-radio-input.component';

export const FormRadioInput: React.FC<ComponentProps<typeof UiRadioInput> &
  DefaultInputProps &
  FieldProps &
  Omit<AntInputProps, 'form' | 'onChange' | 'onBlur'>> = ({
  className,
  isVertical = false,
  onChange,
  isDisabled,
  field,
  form,
  children,
  wrapperComponent,
  ...props
}) => {
  const isInvalid = getIn(form.errors, field.name);
  const formGroupContext = useContext(FormGroupContext);

  useEffect(() => {
    formGroupContext.disableFeedback(false);
  }, []);

  return (
    <UiRadioInput
      name={field.name}
      value={field.value}
      disabled={isDisabled}
      className={className}
      wrapperComponent={wrapperComponent}
      onChange={(e: RadioChangeEvent) => {
        field.onChange(e);
        if (onChange) {
          onChange(e.target.value);
        }
      }}
      aria-invalid={isInvalid}
      aria-required={formGroupContext.isRequired}
      aria-describedby={props['aria-describedby']}
      isVertical={isVertical}
      {...props}
    >
      {children}
    </UiRadioInput>
  );
};
