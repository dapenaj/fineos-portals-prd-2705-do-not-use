/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import { createThemedStyles, toRgba, textStyleToCss } from '../theme';

import styles from './time-tooltip.module.scss';
import { TimeTooltipPosition } from './time-tooltip-position';

type TimeTooltipProps = JSX.IntrinsicElements['div'] & {
  tooltipClassName?: JSX.IntrinsicElements['div']['className'];
  timeTooltipPosition?: TimeTooltipPosition;
};

const useThemedStyles = createThemedStyles(theme => ({
  today: {
    backgroundColor: toRgba(theme.colorSchema, 'primaryColor', 0.15),
    borderRightColor: theme.colorSchema.primaryColor
  },
  todayTooltip: {
    backgroundColor: theme.colorSchema.primaryColor,
    color: theme.colorSchema.neutral1,
    [`&.${styles.bottom}:after`]: {
      borderBottomColor: theme.colorSchema.primaryColor
    },
    [`&.${styles.top}:after`]: {
      borderTopColor: theme.colorSchema.primaryColor
    },
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const TimeTooltip: React.FC<TimeTooltipProps> = ({
  tooltipClassName,
  children,
  className,
  timeTooltipPosition = TimeTooltipPosition.TOP,
  ...props
}) => {
  const themedStyles = useThemedStyles();

  return (
    <>
      <div className={classNames(className, themedStyles.today)} {...props}>
        <div
          data-test-el="timeline-today-tooltip"
          className={classNames(
            tooltipClassName,
            styles.todayTooltip,
            themedStyles.todayTooltip,
            {
              [styles.bottom]:
                timeTooltipPosition === TimeTooltipPosition.BOTTOM,
              [styles.top]: timeTooltipPosition === TimeTooltipPosition.TOP
            }
          )}
        >
          {children}
        </div>
      </div>
    </>
  );
};
