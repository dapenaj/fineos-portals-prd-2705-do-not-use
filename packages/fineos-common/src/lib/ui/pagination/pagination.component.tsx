/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Pagination as AntPagination } from 'antd';
import { PaginationProps as AntPaginationProps } from 'antd/lib/pagination';
import { useIntl } from 'react-intl';
import {
  faChevronLeft,
  faChevronRight
} from '@fortawesome/free-solid-svg-icons';

import { Icon } from '../icon';
import { FormattedMessage } from '../formatted-message';
import { createThemedStyles, textStyleToCss } from '../theme';
import { getFocusedElementShadow } from '../../utils';

import styles from './pagination.module.scss';

export const usePaginationThemedStyles = createThemedStyles(theme => ({
  pagination: {
    '& .ant-pagination-prev, & .ant-pagination-next': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.smallText, {
        suppressLineHeight: true
      }),
      border: `1px solid ${theme.colorSchema.neutral5}`
    },
    '& .ant-pagination-disabled': {
      color: theme.colorSchema.neutral7
    },
    '& .ant-pagination-item, .ant-pagination-next, .ant-pagination-prev': {
      '&:focus, &:active': {
        borderColor: theme.colorSchema.primaryAction,
        boxShadow: getFocusedElementShadow({
          color: theme.colorSchema.primaryAction
        })
      }
    },
    '& .ant-pagination': {
      fontSize: theme.typography.baseText.fontSize,
      '& .ant-pagination-item': {
        ...textStyleToCss(theme.typography.baseText, {
          suppressLineHeight: true
        }),
        backgroundColor: theme.colorSchema.neutral1,
        borderColor: theme.colorSchema.neutral5,
        '&:focus, &:hover': {
          borderColor: theme.colorSchema.primaryAction,
          '& span': {
            color: theme.colorSchema.primaryAction
          }
        },
        '& span': {
          color: theme.colorSchema.neutral8
        },
        '&.ant-pagination-item-active': {
          backgroundColor: theme.colorSchema.neutral1,
          borderColor: theme.colorSchema.primaryAction,
          '& span': {
            color: theme.colorSchema.primaryAction
          }
        }
      },
      '& .ant-pagination-prev, & .ant-pagination-next': {
        color: theme.colorSchema.primaryAction,
        ...textStyleToCss(theme.typography.baseText, {
          suppressLineHeight: true
        })
      },
      '& .ant-pagination-disabled': {
        color: theme.colorSchema.neutral6
      }
    },
    '& .ant-pagination-options': {
      '& .ant-select-selection': {
        fontSize: theme.typography.baseText.fontSize,
        background: theme.colorSchema.neutral1,
        color: theme.colorSchema.neutral8,
        borderColor: theme.colorSchema.neutral5,
        '&:hover': {
          borderColor: theme.colorSchema.primaryAction
        }
      },
      '& .ant-select-dropdown, .ant-select:not(.ant-select-customize-input) .ant-select-selector': {
        backgroundColor: theme.colorSchema.neutral1,
        color: theme.colorSchema.neutral8,
        ...textStyleToCss(theme.typography.baseText),
        '&:hover': {
          color: theme.colorSchema.neutral7
        }
      },
      '& .ant-select-dropdown-menu-item': {
        color: theme.colorSchema.neutral8,
        ...textStyleToCss(theme.typography.baseText),
        '&:hover': {
          background: theme.colorSchema.neutral2
        },
        '&. ant-select-dropdown-menu-item-selected': {
          ...textStyleToCss(theme.typography.baseTextSemiBold),
          backgroundColor: theme.colorSchema.neutral2,
          color: theme.colorSchema.neutral8,
          '&:hover': {
            background: theme.colorSchema.neutral3
          }
        }
      }
    },
    '& .ant-select-item-option': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText),
      '&:hover': {
        background: theme.colorSchema.neutral3
      }
    },
    '& .ant-select-item-option-selected:not(.ant-select-item-option-disabled), .ant-select-item-option-active:not(.ant-select-item-option-disabled)': {
      ...textStyleToCss(theme.typography.baseTextSemiBold),
      color: theme.colorSchema.neutral8,
      backgroundColor: theme.colorSchema.neutral2
    },
    '& .ant-select-single.ant-select-open .ant-select-selection-item': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText, {
        suppressLineHeight: true
      }),
      '&:hover': {
        color: theme.colorSchema.neutral7
      }
    }
  }
}));

export const PAGINATION_THRESHOLD = 10;
export type PaginationProps = AntPaginationProps;

export const paginationDefaultItemRender = (
  current: number,
  type: 'page' | 'prev' | 'next' | 'jump-prev' | 'jump-next'
) => {
  if (type === 'prev') {
    return (
      <Icon
        icon={faChevronLeft}
        iconLabel={<FormattedMessage id="FINEOS_COMMON.PAGINATION.PREV" />}
        data-test-el="pagination-prev-link"
      />
    );
  }
  if (type === 'next') {
    return (
      <Icon
        icon={faChevronRight}
        iconLabel={<FormattedMessage id="FINEOS_COMMON.PAGINATION.NEXT" />}
        data-test-el="pagination-next-link"
      />
    );
  }
  return (
    <span
      className={styles.paginationLink}
      data-test-el="pagination-page-count"
    >
      {current}
    </span>
  );
};

export const Pagination: React.FC<PaginationProps> = ({
  className,
  itemRender,
  hideOnSinglePage,
  ...props
}) => {
  const themedStyles = usePaginationThemedStyles();
  const intl = useIntl();

  const total = props?.total!;
  if (hideOnSinglePage && total <= 10) {
    return null;
  }

  return (
    <AntPagination
      data-test-el="pagination"
      itemRender={itemRender || paginationDefaultItemRender}
      className={classNames(
        themedStyles.pagination,
        styles.pagination,
        className
      )}
      locale={{
        items_per_page: intl.formatMessage({
          id: 'FINEOS_COMMON.PAGINATION.PER_PAGE'
        }),
        prev_page: intl.formatMessage({
          id: 'FINEOS_COMMON.PAGINATION.PREV_PAGE'
        }),
        next_page: intl.formatMessage({
          id: 'FINEOS_COMMON.PAGINATION.NEXT_PAGE'
        }),
        prev_5: intl.formatMessage(
          {
            id: 'FINEOS_COMMON.PAGINATION.PREV_N'
          },
          { count: 5 }
        ),
        next_5: intl.formatMessage(
          {
            id: 'FINEOS_COMMON.PAGINATION.NEXT_N'
          },
          { count: 5 }
        ),
        prev_3: intl.formatMessage(
          {
            id: 'FINEOS_COMMON.PAGINATION.PREV_N'
          },
          { count: 3 }
        ),
        next_3: intl.formatMessage(
          {
            id: 'FINEOS_COMMON.PAGINATION.NEXT_N'
          },
          { count: 3 }
        )
      }}
      {...props}
    />
  );
};
