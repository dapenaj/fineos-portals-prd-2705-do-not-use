/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';

import { Spinner } from '../spinner';
import { SomethingWentWrong } from '../something-went-wrong';
import { ApiError } from '../../utils';

import styles from './content-renderer.module.scss';

type ContentRendererProps = {
  /**
   * Boolean indicating if the content is loading or not. A Spinner will be shown if true.
   */
  isLoading: boolean;
  /**
   * Boolean indicating if there is no content to display.
   */
  isEmpty: boolean;
  /**
   * Optional component that is displayed if isEmpty is true.
   */
  emptyContent?: React.ReactNode;
  /**
   * Optional error used for showing errorContent.
   */
  error?: Error | ApiError | null;
  /**
   * Optional content which would displayed in case of error. Defaults to <SomethingWentWrong />
   */
  errorContent?: React.ReactNode;
  /**
   * Optional boolean indicating if children component should be destroyed
   */
  shouldNotDestroyOnLoading?: boolean;
  /**
   * Optional spinner class name.
   */
  spinnerClassName?: string;
  /**
   * Optional class name.
   */
  className?: string;
  /**
   * Optional test element name. Defaults to "content-renderer".
   */
  'data-test-el'?: string;
  /**
   * Optional role status for error message displays inside this component
   */
  readErrorMessage?: boolean;
};

/**
 * Used to display content with default loading and empty states.
 */
export const ContentRenderer: React.FC<ContentRendererProps> = ({
  isLoading,
  isEmpty,
  emptyContent,
  error,
  readErrorMessage = false,
  errorContent = <SomethingWentWrong hasRoleStatus={readErrorMessage} />,
  shouldNotDestroyOnLoading,
  spinnerClassName,
  children,
  ...props
}) => {
  if (shouldNotDestroyOnLoading) {
    return (
      <>
        {isLoading && <Spinner className={spinnerClassName} />}

        {error ? (
          errorContent
        ) : (
          <>
            <div
              className={isLoading || isEmpty ? styles.hidden : ''}
              {...props}
            >
              {children}
            </div>
            {!isLoading && isEmpty && emptyContent && <>{emptyContent}</>}
          </>
        )}
      </>
    );
  }

  if (isLoading) {
    return <Spinner className={spinnerClassName} />;
  }

  if (!isLoading && error) {
    return <>{errorContent}</>;
  }

  return (
    <>
      {!isEmpty && <div {...props}>{children}</div>}
      {isEmpty && emptyContent && <>{emptyContent}</>}
    </>
  );
};
