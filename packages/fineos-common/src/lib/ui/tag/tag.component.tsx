/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';
import { Tag as AntTag } from 'antd';
import { TagProps as AntTagProps } from 'antd/lib/tag';

import { createThemedStyles, textStyleToCss } from '../theme';

import styles from './tag.module.scss';

const useThemedStyles = createThemedStyles(themed => ({
  tag: {
    color: themed.colorSchema.primaryColorContrast,
    backgroundColor: themed.colorSchema.primaryColor,
    borderColor: themed.colorSchema.primaryColor,
    ...textStyleToCss(themed.typography.baseText),

    '& .anticon-close': {
      color: themed.colorSchema.primaryColorContrast,
      '&:hover': {
        color: themed.colorSchema.primaryColorContrast
      }
    }
  }
}));

type TagProps = AntTagProps & {
  'data-test-el'?: string;
};

export const Tag: React.FC<TagProps> = ({ children, className, ...props }) => {
  const themedStyles = useThemedStyles();

  return (
    <div data-test-el={props['data-test-el'] || 'tag'} className={className}>
      <AntTag className={classnames(styles.tag, themedStyles.tag)} {...props}>
        {children}
      </AntTag>
    </div>
  );
};
