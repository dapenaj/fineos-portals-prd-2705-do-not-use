/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import { useIntl } from 'react-intl';
import { InputProps as AntInputProps } from 'antd/lib/input';
import { Input } from 'antd';
import { FieldProps, getIn } from 'formik';
import classNames from 'classnames';

import { DefaultInputProps } from '../../types';
import {
  AutocompleteContext,
  FormGroupContext,
  getFocusedElementShadow
} from '../../utils';
import { createThemedStyles, textStyleToCss } from '../theme';

import { handleVirtualGroupBlurEvent } from './text-input.util';
import styles from './text-input.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  inputWrapper: {
    '& .ant-input:focus, .ant-input-affiix-wrapper:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      }),
      '& .ant-input-group-addon': {
        borderColor: theme.colorSchema.primaryAction
      }
    },
    '& .ant-input': {
      color: theme.colorSchema.neutral8,
      backgroundColor: theme.colorSchema.neutral1,
      ...textStyleToCss(theme.typography.baseText),
      // dedicated styling for Firefox and IE11 is required here
      '&::placeholder': {
        color: theme.colorSchema.neutral6
      },
      '&::-moz-placeholder': {
        color: theme.colorSchema.neutral6
      },
      '&:-ms-input-placeholder': {
        color: theme.colorSchema.neutral6
      },
      '&:hover': {
        backgroundColor: `${theme.colorSchema.neutral1} !important`
      }
    },
    '& .ant-input-affix-wrapper': {
      background: theme.colorSchema.neutral1
    },
    '& .ant-input-affix-wrapper-focused': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  input: {
    borderColor: theme.colorSchema.neutral5,
    '& .ant-input, & .ant-input:hover, & .ant-input-affix-wrapper:hover': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8,
      background: theme.colorSchema.neutral1,
      borderColor: theme.colorSchema.neutral5
    },
    '& .ant-input-group .ant-input, .ant-input-group-addon': {
      borderColor: theme.colorSchema.neutral5,
      background: theme.colorSchema.neutral1
    },
    '& .ant-input::placeholder': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral6
    },
    '& .ant-input-clear-icon, .ant-input-suffix': {
      color: theme.colorSchema.neutral6
    }
  }
}));

export const TextInput: React.FC<FieldProps &
  DefaultInputProps &
  Omit<AntInputProps, 'placeholder' | 'autoComplete'>> = ({
  className,
  isDisabled = false,
  id,
  field,
  form,
  meta,
  addonAfter,
  allowClear = false,
  children,
  virtualGroup,
  onClick,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const context = useContext(AutocompleteContext);
  const { isRequired } = useContext(FormGroupContext);
  const isInvalid = getIn(form.errors, field.name);
  const intl = useIntl();
  const placeholder =
    typeof props.placeholder !== 'string' && !!props.placeholder
      ? intl.formatMessage(
          props.placeholder.props,
          props.placeholder.props.values
        )
      : props.placeholder;

  return (
    <div className={classNames(styles.inputWrapper, themedStyles.inputWrapper)}>
      <Input
        name={field.name}
        autoComplete={context.autocomplete}
        id={id}
        className={classNames(themedStyles.input, className)}
        disabled={isDisabled}
        value={field.value}
        data-test-el={props['data-test-el'] || 'text-input'}
        placeholder={placeholder}
        addonAfter={addonAfter}
        onClick={onClick}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.onChange(e)}
        onBlur={e => {
          field.onBlur(e);

          handleVirtualGroupBlurEvent(e, {
            virtualGroup,
            form,
            field,
            meta
          });
        }}
        allowClear={allowClear}
        aria-invalid={isInvalid ? 'true' : undefined}
        aria-required={isRequired}
        aria-describedby={props['aria-describedby']}
      />
      {children}
    </div>
  );
};
