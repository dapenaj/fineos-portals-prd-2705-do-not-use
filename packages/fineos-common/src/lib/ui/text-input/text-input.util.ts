/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FieldProps, getIn } from 'formik';

type HandleVirtualGroupBlurEventConfig = FieldProps & { virtualGroup?: string };

const isInputNameStartWith = (input: HTMLInputElement, str: string) => {
  if (input.name) {
    return input.name.startsWith(str);
  }

  const ancestorWithName = input.closest(
    '[data-test-input-name]'
  ) as HTMLElement | null;
  if (ancestorWithName) {
    const inputName = ancestorWithName.dataset.testInputName || '';

    return inputName.startsWith(str);
  }

  return false;
};

/**
 * This utility function mark fields from the virtual group as touched
 * on blur event if current field is invalid and focus not moving to any
 * other field from the same virtual group
 *
 * @param e
 * @param virtualGroup
 * @param form
 * @param field
 */
export const handleVirtualGroupBlurEvent = (
  e: React.FocusEvent,
  { virtualGroup, form, field }: HandleVirtualGroupBlurEventConfig
) => {
  const relatedTarget = e.relatedTarget;

  if (
    virtualGroup &&
    field.name.startsWith(virtualGroup) &&
    (!relatedTarget ||
      (relatedTarget as HTMLElement).tagName !== 'INPUT' ||
      !isInputNameStartWith(relatedTarget as HTMLInputElement, virtualGroup))
  ) {
    const virtualGroupObject = getIn(form.values, virtualGroup);

    if (typeof virtualGroupObject === 'object' && virtualGroupObject !== null) {
      for (const [fieldName, value] of Object.entries(virtualGroupObject)) {
        if (typeof value !== 'object' || value == null) {
          const virtualGroupFieldName = `${virtualGroup}.${fieldName}`;
          const isFieldFromVirtualGroupInvalid = Boolean(
            getIn(form.errors, virtualGroupFieldName)
          );

          if (
            field.name !== virtualGroupFieldName &&
            isFieldFromVirtualGroupInvalid
          ) {
            form.setFieldTouched(virtualGroupFieldName);
          }
        }
      }
    }
  }
};
