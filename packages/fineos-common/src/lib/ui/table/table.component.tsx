/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import classnames from 'classnames';
import { Table as AntTable } from 'antd';
import {
  TableProps as AntTableProps,
  ColumnProps as AntColumnProps,
  TablePaginationConfig as AntTablePaginationConfig
} from 'antd/lib/table';
import {
  SorterResult as AntSorterResult,
  TableCurrentDataSource as AntTableCurrentDataSource,
  FilterDropdownProps as AntFilterDropdownProps,
  ColumnFilterItem as AntColumnFilterItem
} from 'antd/lib/table/interface';

import { createThemedStyles, textStyleToCss } from '../theme';
import { Spinner } from '../spinner';

import styles from './table.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  table: {
    '& .ant-table-thead > tr > th': {
      color: theme.colorSchema.neutral8,
      backgroundColor: theme.colorSchema.neutral1,
      borderBottomColor: theme.colorSchema.neutral5,
      ...textStyleToCss(theme.typography.smallText),
      '& > .ant-table-filter-icon': {
        color: theme.colorSchema.neutral6,
        ...textStyleToCss(theme.typography.baseText),

        '&.ant-table-filter-selected': {
          color: theme.colorSchema.callToActionColor
        }
      },
      // overwriting ant design colors when hovering
      '&.ant-table-column-has-actions.ant-table-column-has-filters:hover .ant-table-filter-icon:hover': {
        color: theme.colorSchema.neutral6,

        '&.ant-table-filter-selected': {
          color: theme.colorSchema.callToActionColor
        }
      }
    },
    '& .ant-table-filter-dropdown': {
      '& .ant-dropdown-menu-item': {
        color: theme.colorSchema.neutral8
      },
      '& .ant-checkbox-inner': {
        borderColor: theme.colorSchema.primaryAction
      },
      '& .ant-checkbox-checked .ant-checkbox-inner': {
        backgroundColor: theme.colorSchema.primaryAction,
        borderColor: theme.colorSchema.primaryAction
      },
      '& .ant-checkbox-checked::after': {
        borderColor: theme.colorSchema.primaryAction
      },
      '& .ant-checkbox-wrapper:hover .ant-checkbox-inner, .ant-checkbox-input:focus + .ant-checkbox-inner': {
        borderColor: theme.colorSchema.primaryAction
      },
      '& .ant-btn-primary': {
        backgroundColor: theme.colorSchema.primaryAction,
        color: theme.colorSchema.neutral1
      },
      '& .ant-btn-link': {
        backgroundColor: theme.colorSchema.neutral1,
        color: theme.colorSchema.primaryAction,

        '&:disabled': {
          color: theme.colorSchema.neutral5
        }
      }
    },
    '& .ant-table-tbody > tr > td, .ant-table-tbody > tr:hover': {
      backgroundColor: theme.colorSchema.neutral1,
      borderBottomColor: theme.colorSchema.neutral5
    },
    '& .ant-table-tbody > tr, .ant-table-tbody > tr:hover, .ant-table-thead > tr, .ant-table-thead > tr:hover, .ant-table-thead th.ant-table-column-has-sorters:hover': {
      backgroundColor: theme.colorSchema.neutral1
    },
    '& .ant-table-thead > tr > th .ant-table-column-sorter .ant-table-column-sorter-inner .ant-table-column-sorter-up.on': {
      color: theme.colorSchema.callToActionColor
    },
    '& .ant-table-thead > tr > th .ant-table-column-sorter .ant-table-column-sorter-inner .ant-table-column-sorter-down.on': {
      color: theme.colorSchema.callToActionColor
    },
    '& tr.ant-table-expanded-row, tr.ant-table-expanded-row:hover, .ant-table-filter-trigger-container, .ant-table-filter-trigger-container:hover': {
      background: theme.colorSchema.neutral1
    },
    '& .ant-table-thead th.ant-table-column-has-sorters:hover .ant-table-filter-trigger-container': {
      background: theme.colorSchema.neutral1
    },
    '& .ant-table-filter-trigger, & .ant-table-column-sorter-inner': {
      color: theme.colorSchema.neutral5
    },
    '& .ant-empty-description': {
      color: theme.colorSchema.neutral6
    }
  }
}));

export type ColumnFilterItem = AntColumnFilterItem;
export type FilterDropdownProps = AntFilterDropdownProps;
export type ColumnProps<T> = AntColumnProps<T>;
export type TablePaginationConfig = AntTablePaginationConfig;
export type SorterResult<T> = AntSorterResult<T>;
export type TableCurrentDataSource<T> = AntTableCurrentDataSource<T>;
export type TableProps<T> = Omit<AntTableProps<T>, 'loading'> & {
  /**
   * Optional loading identify if table in loading state
   */
  loading?: boolean;
  /**
   * Optional test element name. Defaults to "table".
   */
  'data-test-el'?: string;
};

// You can't use just `<T>` as it will confuse the TSX parser whether it's a JSX tag or a Generic Declaration.
// So you need to specify <T extends unknown> before the function definition.
// Alternatively, you can use <T,> (as per https://github.com/microsoft/TypeScript/issues/15713#issuecomment-499474386)
export const Table = <T extends {}>({
  children,
  loading,
  ...props
}: TableProps<T>) => (
  <div
    className={classnames(styles.table, useThemedStyles().table)}
    data-test-el={props['data-test-el'] || 'table'}
  >
    <AntTable<T>
      loading={useMemo(
        () => ({
          spinning: loading,
          indicator: <Spinner />
        }),
        [loading]
      )}
      getPopupContainer={(node: HTMLElement | undefined) => {
        if (node) {
          return node.parentNode as HTMLElement;
        }
        return document.body as HTMLElement;
      }}
      {...props}
    />
  </div>
);

Table.Column = AntTable.Column;
