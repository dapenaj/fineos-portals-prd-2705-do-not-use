import { SortOrder as AntSortOrder } from 'antd/lib/table/interface';

export enum SortOrder {
  ASCEND = 'ascend',
  DESCEND = 'descend'
}

export const TABLE_SORT_ORDERS = [
  SortOrder.ASCEND,
  SortOrder.DESCEND
] as AntSortOrder[];
