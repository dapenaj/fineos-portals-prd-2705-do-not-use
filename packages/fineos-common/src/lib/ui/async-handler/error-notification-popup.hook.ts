/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useContext, useCallback } from 'react';
import { useIntl } from 'react-intl';
import { notification } from 'antd';

import { ApiError, ErrorContext } from '../../utils';
import { createThemedStyles, textStyleToCss } from '../theme';

const mapErrorToNotification = (error: Error | ApiError): NotificationCall => {
  if ('status' in error) {
    switch (error.status) {
      case 404:
        return {
          type: 'error',
          message: 'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_404',
          description: 'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_404'
        };
      case 403:
        return {
          type: 'warning',
          message: 'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_403',
          description: 'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_403'
        };
      case 401:
        return {
          type: 'error',
          message: 'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_401',
          description: 'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_401'
        };
    }
  }

  return {
    type: 'error',
    message: 'FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_GENERAL',
    description: 'FINEOS_COMMON.ASYNC_ERRORS.DESCRIPTION_GENERAL'
  };
};

const useThemedStyles = createThemedStyles(theming => ({
  errorNotification: {
    backgroundColor: theming.colorSchema.neutral1,
    '& .ant-notification-notice': {
      backgroundColor: theming.colorSchema.neutral1
    },
    '& .anticon.ant-notification-notice-icon-error': {
      color: theming.colorSchema.attentionColor
    },
    '& .anticon.ant-notification-notice-icon-warning': {
      color: theming.colorSchema.pendingColor
    },
    '& .anticon.ant-notification-close-icon': {
      color: theming.colorSchema.neutral8
    },
    '& .ant-notification-notice-message': {
      color: theming.colorSchema.neutral9,
      ...textStyleToCss(theming.typography.panelTitle)
    },
    '& .ant-notification-notice-description': {
      color: theming.colorSchema.neutral8,
      ...textStyleToCss(theming.typography.baseText)
    }
  }
}));

type NotificationCall = {
  type: 'error' | 'warning';
  message: string;
  description: string;
};

export const useErrorNotificationPopup = () => {
  const themedStyles = useThemedStyles();
  const intl = useIntl();
  const errorContext = useContext(ErrorContext);
  const handleCommonErrors = (error: Error | ApiError) => {
    if (
      'status' in error &&
      error.status === 403 &&
      error.json?.message ===
        'The security token included in the request is expired' &&
      errorContext
    ) {
      errorContext.setIsForbidden(true);
      return true;
    } else {
      return false;
    }
  };

  const showErrorNotification = useCallback(
    (error: Error | ApiError) => {
      const callDetails = mapErrorToNotification(error);
      if (!handleCommonErrors(error)) {
        notification[callDetails.type]({
          message: intl.formatMessage({ id: callDetails.message }),
          description: intl.formatMessage({ id: callDetails.description }),
          placement: 'topRight',
          className: themedStyles.errorNotification
        });
      }
    },
    [themedStyles]
  );

  return {
    showErrorNotification,
    cleanUp: notification.destroy,
    handleCommonErrors
  };
};
