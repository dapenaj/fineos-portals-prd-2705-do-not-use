/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { PieArcDatum, Arc } from 'd3';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss, useTheme } from '../theme';

import { DONUT_LABEL_OFFSET } from './donut.constant';
import { DonutData, DonutLabelPosition, DonutLineData } from './donut.type';
import { getSvgTextForWidth } from './donut.utils';
import styles from './donut.module.scss';

type DonutSliceComponentProps = {
  sliceData: PieArcDatum<DonutData>;
  innerArc: Arc<any, PieArcDatum<DonutData>>;
  labelPosition: DonutLabelPosition;
  lineData: DonutLineData;
  svgWidth: number;
  color: string;
  onSliceClick: () => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  polyline: {
    stroke: theme.colorSchema.neutral6
  },
  arc: {
    stroke: theme.colorSchema.neutral1
  },
  value: {
    fill: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseTextSemiBold)
  },
  label: {
    fill: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  }
}));

export const DonutSlice = ({
  sliceData,
  innerArc,
  labelPosition,
  lineData,
  svgWidth,
  color,
  onSliceClick
}: DonutSliceComponentProps) => {
  const themedStyles = useThemedStyles();
  const { typography } = useTheme();

  // label text needs to be parsed so that we can show it with an ellipsis
  const labelText = getSvgTextForWidth(
    sliceData.data.label,
    textStyleToCss(typography.smallText),
    svgWidth / 2 - Math.floor(Math.abs(labelPosition.x))
  );

  const getLabelPosition = () => {
    // put a little distance between the line and the label
    return `translate(${labelPosition.x +
      DONUT_LABEL_OFFSET * (labelPosition.textAnchor === 'start' ? 1 : -1)} ${
      labelPosition.y
    })`;
  };

  /*
   * Each donut slice is made up of 3 parts:
   * 1. <polyline> - the actual donut slice shape
   * 2. <path> - the line connecting the slice to the label
   * 3. <g><text></g> - the value and label for the data
   */
  return (
    <g
      onClick={onSliceClick}
      onKeyDown={(e: React.KeyboardEvent) =>
        e.key === 'Enter' && onSliceClick()
      }
      tabIndex={0}
      data-test-el="donut-slice"
      role="graphics-object"
    >
      <polyline
        className={classNames(styles.polyline, themedStyles.polyline)}
        fill="none"
        points={`${lineData.pointA} ${lineData.pointB} ${lineData.pointC}`}
        role="presentation"
      />
      <path
        className={classNames(styles.arc, themedStyles.arc)}
        d={innerArc(sliceData) || undefined}
        fill={color}
        role="presentation"
      />
      <g
        transform={getLabelPosition()}
        textAnchor={labelPosition.textAnchor}
        aria-label={`${sliceData.data.value} ${sliceData.data.label}`}
      >
        <title>{`${sliceData.data.label}: ${sliceData.data.value}`}</title>
        <text
          className={classNames(styles.text, themedStyles.value)}
          y={(typography.smallText.lineHeight || 0) * 0.25}
        >
          {sliceData.data.value}
        </text>
        <text
          className={classNames(styles.text, themedStyles.label)}
          y={(typography.smallText.lineHeight || 16) * 1.25}
        >
          {labelText}
        </text>
      </g>
    </g>
  );
};
