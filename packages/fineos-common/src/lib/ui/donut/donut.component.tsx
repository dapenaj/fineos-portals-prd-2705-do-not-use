/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useRef, useState } from 'react';
import { arc, pie, PieArcDatum } from 'd3';
import { debounce as _debounce } from 'lodash';
import { useIntl } from 'react-intl';

import { FormattedMessage, useRawFormattedMessage } from '../formatted-message';
import { createThemedStyles, textStyleToCss, useTheme } from '../theme';
import { PossiblyFormattedMessageElement } from '../formatted-message';

import { DonutSlice } from './donut-slice.component';
import {
  GRAPH_HEIGHT,
  GRAPH_INITIAL_WIDTH,
  DONUT_SVG_PADDING,
  DONUT_INNER_RADIUS_MULTIPLIER,
  DONUT_OUTER_RADIUS_MULTIPLIER,
  DONUT_LABEL_INNER_RADIUS_MULTIPLIER,
  DONUT_LABEL_OFFSET
} from './donut.constant';
import { DonutData } from './donut.type';
import {
  calculateDonutSliceLabelAndLineDataPoints,
  groupDataForDisplay
} from './donut.utils';
import styles from './donut.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  totalValue: {
    fill: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  },
  totalLabel: {
    fill: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type DonutComponentProps = {
  /**
   * The data to be shown in the graph
   */
  data: DonutData[];
  /**
   * Optional, determines if the total value should be shown in the center of the Donut Graph. Defaults to true
   */
  showTotal?: boolean;
  /**
   * Optional test element name. Defaults to "donut-graph".
   */
  'data-test-el'?: string;
  /**
   * Optional. The function to be called when a Donut slice is clicked. The DonutData object for the slice is passed as a param.
   */
  onSliceClick?: (data: DonutData) => void;
  /**
   * Optional. This value is necessary if the total number is not the sum of the data.
   */
  totalNumber?: number;
  /**
   * Optional. This value is necessary if the total label is not the default.
   */
  totalNumberLabel?: PossiblyFormattedMessageElement;
  /**
   * Optional. This value is necessary if the total label is not the default.
   */
  totalNumberLabelAriaLabel?: PossiblyFormattedMessageElement;
};

export const Donut = ({
  data,
  showTotal = true,
  onSliceClick,
  totalNumber,
  totalNumberLabel,
  totalNumberLabelAriaLabel,
  ...props
}: DonutComponentProps) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [width, setWidth] = useState<number>(GRAPH_INITIAL_WIDTH);

  // The radius of the donut is half the width or half the height (smallest one) - then subtract some margin
  const radius = Math.min(width, GRAPH_HEIGHT) / 2 - DONUT_SVG_PADDING;

  // arc used for drawing the donut
  const innerArc = arc<PieArcDatum<DonutData>>()
    .innerRadius(radius * DONUT_INNER_RADIUS_MULTIPLIER)
    .outerRadius(radius * DONUT_OUTER_RADIUS_MULTIPLIER);

  // arc used for positioning labels
  const outerArc = arc<PieArcDatum<DonutData>>()
    .innerRadius(radius * DONUT_LABEL_INNER_RADIUS_MULTIPLIER)
    .outerRadius(radius);

  const { colorSchema, typography } = useTheme();
  const intl = useIntl();
  const themedStyles = useThemedStyles();

  // graph colours
  const colorScale = [
    colorSchema.graphColorOne,
    colorSchema.graphColorTwo,
    colorSchema.graphColorThree,
    colorSchema.graphColorFour,
    colorSchema.graphColorFive,
    colorSchema.graphColorSix
  ];

  // generate the donut graph
  const pieGenerator = pie<DonutData>()
    .value(d => d.value)
    .sort(null);
  const donutData = pieGenerator(
    groupDataForDisplay(
      data,
      useRawFormattedMessage(
        <FormattedMessage id="FINEOS_COMMON.DONUT.OTHER" />
      )
    )
  );

  const total = totalNumber || data.reduce((sum, { value }) => sum + value, 0);
  const totalLabel = useRawFormattedMessage(
    totalNumberLabel || (
      <FormattedMessage id="FINEOS_COMMON.DONUT.TOTAL" values={{ total }} />
    )
  );
  const ariaLabel = useRawFormattedMessage(
    totalNumberLabelAriaLabel || (
      <FormattedMessage
        id="FINEOS_COMMON.DONUT.TOTAL_ARIA"
        values={{ total }}
      />
    )
  );

  const labelHeight =
    (typography.baseTextSemiBold.lineHeight || 0) +
    (typography.smallText.lineHeight || 0) +
    DONUT_LABEL_OFFSET;
  const {
    labelPositions,
    lineDataPoints,
    heightOffset
  } = calculateDonutSliceLabelAndLineDataPoints(
    donutData,
    innerArc,
    outerArc,
    radius,
    labelHeight
  );

  const heightToUse =
    GRAPH_HEIGHT + (heightOffset > 0 ? heightOffset + labelHeight / 2 : 0);

  // handle resizing the graph when the window resizes
  useEffect(() => {
    // get and set the width based on the container
    function resize() {
      if (containerRef.current) {
        const { offsetWidth } = containerRef.current;
        setWidth(offsetWidth);
      }
    }
    // debounce the use of the above function so that we don't kill the browser
    const handleResize = _debounce(() => resize(), 100, {
      maxWait: 500
    });

    // listen for a resize
    window.addEventListener('resize', handleResize);
    // call the function now
    resize();
    // provide function for cleanup
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return (
    <div className={styles.container} ref={containerRef}>
      <svg
        className={styles.svg}
        preserveAspectRatio={'xMinYMin slice'}
        viewBox={`0 0 ${width} ${heightToUse}`}
        data-test-el={props['data-test-el'] || 'donut-graph'}
        role="graphics-document"
      >
        <g
          transform={`translate(${width / 2} ${heightToUse / 2})`}
          aria-label={intl.formatMessage({
            id: 'FINEOS_COMMON.DONUT.ARIA_LABEL'
          })}
        >
          {donutData.map((sliceData, index) => (
            <DonutSlice
              key={index}
              sliceData={sliceData}
              innerArc={innerArc}
              labelPosition={labelPositions[index]}
              lineData={lineDataPoints[index]}
              svgWidth={width}
              color={
                sliceData.data.sliceColor ||
                `${colorScale[index % colorScale.length]}`
              }
              onSliceClick={() => onSliceClick && onSliceClick(sliceData.data)}
            />
          ))}
          {showTotal && (
            <g textAnchor="middle" aria-label={ariaLabel}>
              <text className={themedStyles.totalValue} role="status">
                {total}
              </text>
              <text
                className={themedStyles.totalLabel}
                dy={typography.baseText.lineHeight}
                role="status"
              >
                {totalLabel}
              </text>
            </g>
          )}
        </g>
      </svg>
    </div>
  );
};
