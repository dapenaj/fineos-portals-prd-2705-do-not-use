/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { Arc, PieArcDatum } from 'd3';
import { orderBy as _orderBy } from 'lodash';

import {
  DONUT_LABEL_PADDING,
  DONUT_LABEL_RADIUS_MULTIPLIER,
  DONUT_MAX_GROUPS,
  GRAPH_HEIGHT
} from './donut.constant';
import { DonutData, DonutLabelPosition, DonutLineData } from './donut.type';

const isOnRightSideOfGraph = (sliceData: any) =>
  sliceData.startAngle + (sliceData.endAngle - sliceData.startAngle) / 2 <
  Math.PI;

export const calculateDonutSliceLabelAndLineDataPoints = (
  donutData: PieArcDatum<DonutData>[],
  innerArc: Arc<any, PieArcDatum<DonutData>>,
  outerArc: Arc<any, PieArcDatum<DonutData>>,
  radius: number,
  labelHeight: number
) => {
  // calc data points for the lines (from slice to label)
  const lineDataPoints: DonutLineData[] = donutData.map(val => {
    const pointC = outerArc.centroid(val);
    pointC[0] =
      pointC[0] +
      radius *
        DONUT_LABEL_RADIUS_MULTIPLIER *
        (isOnRightSideOfGraph(val) ? 1 : -1);

    return {
      pointA: innerArc.centroid(val),
      pointB: outerArc.centroid(val),
      pointC
    };
  });

  // calc the position of the labels
  const labelPositions: DonutLabelPosition[] = donutData.map(val => {
    const point = outerArc.centroid(val);
    point[0] =
      point[0] +
      radius *
        DONUT_LABEL_RADIUS_MULTIPLIER *
        (isOnRightSideOfGraph(val) ? 1 : -1);

    return {
      label: val.data.label,
      x: point[0],
      y: point[1],
      textAnchor: isOnRightSideOfGraph(val) ? 'start' : 'end'
    };
  });

  // a variable to store the biggest offset from the label positions
  let labelOffset = 0;

  // this function will massage the label positions so they don't overlap.
  const massageLabelPositions = () => {
    let recursive = false;
    // Check labels and mdjust accordingly
    labelPositions.forEach(labelA => {
      const y1 = labelA.y;
      labelPositions.forEach(labelB => {
        // a & b are the same element and don't collide.
        if (labelA.label === labelB.label) {
          return;
        }

        // a & b are on opposite sides of the chart and don't collide
        if (labelA.textAnchor !== labelB.textAnchor) {
          return;
        }

        // deltaY is the distance between the two labels
        const y2 = labelB.y;
        const deltaY = y1 - y2;

        // The spacing is greater than our specified labelHeight, so they don't collide.
        if (Math.abs(deltaY) > labelHeight) {
          return;
        }

        // The labels are colliding and need to be moved slightly.
        // We move the top label up more than the bottom.
        recursive = true;
        labelA.y = +y1 + (deltaY > 0 ? 1 : -1) * 0.75;
        labelB.y = +y2 - (deltaY > 0 ? 1 : -1) * 0.25;
      });
    });
    // We've moved at least one label, so now let's check them all again
    if (recursive) {
      massageLabelPositions();
    }
  };

  // call the above function
  massageLabelPositions();

  // now adjust the last point of each line to be the same as the label
  lineDataPoints.forEach((line, index) => {
    // last point of the line should be equal to the label poisiton
    line.pointC = [labelPositions[index].x, labelPositions[index].y];
    // if pointB is going to cause the line to go up and then back down, then let's ignore it
    if (line.pointC[1] > line.pointB[1]) {
      line.pointB = line.pointC;
    }

    // also use this opportunity to update the labelOffset value
    // has our label moved outside of the graph area?
    labelOffset =
      GRAPH_HEIGHT / 2 < Math.abs(labelPositions[index].y)
        ? Math.abs(labelPositions[index].y) - GRAPH_HEIGHT / 2
        : 0;
  });

  return {
    labelPositions,
    lineDataPoints,
    heightOffset: 0 < labelOffset ? labelOffset : 0
  };
};

const getStringWidth = (str: string, style?: object) => {
  const textMeasureId = '_svg_text_measure_element_';
  const svgW3URL = 'http://www.w3.org/2000/svg';
  try {
    // Calculate length of each word to be used to determine number of words per line
    let textEl = document.getElementById(
      textMeasureId
    ) as SVGTextElement | null;
    if (!textEl) {
      const svg = document.createElementNS(svgW3URL, 'svg');
      svg.style.width = '0';
      svg.style.height = '0';
      svg.style.position = 'absolute';
      svg.style.top = '-100%';
      svg.style.left = '-100%';
      textEl = document.createElementNS(svgW3URL, 'text');
      textEl.setAttribute('id', textMeasureId);
      svg.appendChild(textEl);
      document.body.appendChild(svg);
    }

    Object.assign(textEl.style, style);
    textEl.textContent = str;
    return textEl.getComputedTextLength();
  } catch (e) {
    return null;
  }
};

export const getSvgTextForWidth = (
  text: string,
  style: object,
  allowedWidth: number
): string => {
  // split the text into individual characters
  const characters: string[] = text.split('');
  const ellipsisWidth = getStringWidth('...', style) || 0;
  const maxWidth = allowedWidth - ellipsisWidth;
  let currentWidth = 0;

  // go through each character, calc its width, and add it to the overall text if there is space for it
  let finalText = '';
  for (let index = 0; index < characters.length; index++) {
    const elem = characters[index];
    const elemWidth = getStringWidth(elem, style) || 0;

    if (index + 1 === characters.length && elemWidth <= ellipsisWidth) {
      // add the final character
      currentWidth += elemWidth;
      finalText = `${finalText}${elem}`;
      break;
    }

    if (currentWidth + elemWidth >= maxWidth - DONUT_LABEL_PADDING) {
      break;
    }

    currentWidth += elemWidth;
    finalText = `${finalText}${elem}`;
  }

  return `${finalText}${finalText.length < text.length ? '...' : ''}`;
};

/**
 * Sorts Donut graph data by values descending.
 * If there are more than DONUT_MAX_GROUPS groups of data, all the additional data will combined into an "Other" group.
 * @param data
 */
export const groupDataForDisplay = (data: DonutData[], otherLabel?: string) => {
  const sorted = _orderBy(data, 'value', 'desc');
  let returnArray = sorted.slice(0, DONUT_MAX_GROUPS);

  if (sorted.length >= 6) {
    const otherGroup = sorted.slice(DONUT_MAX_GROUPS).reduce(
      (newGroup, current) => {
        newGroup.value = newGroup.value + current.value;
        newGroup.filterValues!.push(...current.filterValues);
        return newGroup;
      },
      {
        label: otherLabel,
        value: 0,
        filterValues: []
      } as DonutData
    );
    returnArray.push(otherGroup);
    returnArray = _orderBy(returnArray, 'value', 'desc');
  }

  return returnArray;
};
