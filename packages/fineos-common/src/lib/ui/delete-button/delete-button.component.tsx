import React from 'react';
import { ButtonProps } from 'antd/lib/button';
import classNames from 'classnames';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';

import { Button } from '../button';
import { ButtonType } from '../../types';
import { Icon } from '../icon';
import { createThemedStyles } from '../theme';

import styles from './delete-button.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  deleteButton: {
    '& .fa-trash-alt': {
      color: theme.colorSchema.neutral8
    }
  }
}));

type DeleteButtonProps = {
  /**
   * Icon label for accessibility
   */
  iconLabel: React.ReactElement;
  /**
   * Optional test element name. Defaults to "delete-button".
   */
  'data-test-el'?: string;
};

export const DeleteButton: React.FC<DeleteButtonProps & ButtonProps> = ({
  onClick,
  iconLabel,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  return (
    <Button
      className={classNames(themedStyles.deleteButton, styles.deleteButton)}
      data-test-el={props['data-test-el'] || 'delete-button'}
      buttonType={ButtonType.GHOST}
      onClick={onClick}
      {...props}
    >
      <Icon icon={faTrashAlt} iconLabel={iconLabel} />
    </Button>
  );
};
