/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Badge } from 'antd';
import classnames from 'classnames';

import { StatusType } from '../../types';
import { createThemedStyles, textStyleToCss } from '../theme';

import styles from './status-label.module.scss';

type StatusLabelProps = {
  /**
   * Indicates the status type for the status icon using the StatusType enum. Defaults to a neutral styling.
   */
  type?: StatusType;
  /**
   * Optional classname applied to the status label.
   */
  className?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    '& > .ant-badge-status-text': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.smallText)
    }
  },
  warning: {
    '& > .ant-badge-status-dot': {
      background: theme.colorSchema.pendingColor
    }
  },
  success: {
    '& > .ant-badge-status-dot': {
      background: theme.colorSchema.successColor
    }
  },
  danger: {
    '& > .ant-badge-status-dot': {
      background: theme.colorSchema.attentionColor
    }
  },
  neutral: {
    '& > .ant-badge-status-dot': {
      background: theme.colorSchema.neutral6
    }
  },
  blank: {
    '& > .ant-badge-status-dot': {
      background: theme.colorSchema.neutral1,
      border: `1px solid ${theme.colorSchema.neutral5}`
    }
  }
}));

const useStatusTypeStyling = (type?: StatusType) => {
  const themedStyles = useThemedStyles();
  switch (type) {
    case StatusType.DANGER:
      return themedStyles.danger;
    case StatusType.SUCCESS:
      return themedStyles.success;
    case StatusType.WARNING:
      return themedStyles.warning;
    case StatusType.NEUTRAL:
      return themedStyles.neutral;
    case StatusType.BLANK:
    default:
      return themedStyles.blank;
  }
};

/**
 * Displays a coloured status dot, with an optional label rendered
 */
export const StatusLabel: React.FC<StatusLabelProps> = ({
  type,
  className,
  children
}) => {
  const themedStyles = useThemedStyles();
  const statusStyle = useStatusTypeStyling(type);
  return (
    <Badge
      data-test-el="status-label"
      color={statusStyle}
      className={classnames(
        styles.statusLabel,
        statusStyle,
        themedStyles.label,
        className
      )}
      text={children}
      dot={true}
    />
  );
};
