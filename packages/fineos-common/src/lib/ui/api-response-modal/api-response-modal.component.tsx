/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';

import { useElementId } from '../../hooks';
import { ButtonType, ApiResponseType, ApiResponse } from '../../types';
import { Modal } from '../modal';
import { useTheme, ThemeModel } from '../theme';
import { Button } from '../button';
import { FormattedMessage } from '../formatted-message';

import styles from './api-response-modal.module.scss';

export type ApiResponseModalProps = {
  response: ApiResponse | boolean;
  type?: ApiResponseType;
  title: React.ReactNode;
  ariaLabelId?: string;
  isClosable?: boolean;
  buttonText?: React.ReactNode;
  onCancel?: () => void;
  onConfirm?: () => void;
};

const getImg = (type: ApiResponseType, { images }: ThemeModel) => {
  switch (type) {
    default:
    case ApiResponseType.FAIL:
      return images?.somethingWentWrongColor;

    case ApiResponseType.SUCCESS:
      return images?.greenCheckIntake;

    case ApiResponseType.UPLOAD_SUCCESS:
      return images?.uploadSuccess;

    case ApiResponseType.UPLOAD_FAIL:
      return images?.uploadFail;

    case ApiResponseType.DATA_CONCURRENCY:
      return images?.dataConcurrency;
  }
};

export const ApiResponseModal: React.FC<ApiResponseModalProps> = ({
  children,
  response,
  type = ApiResponseType.FAIL,
  title,
  ariaLabelId = 'API_RESPONSE_MODAL.ARIA_LABEL',
  isClosable = true,
  buttonText,
  onCancel,
  onConfirm
}) => {
  const theme = useTheme();
  const img = getImg(type, theme);
  const showImg = Boolean(img);
  const [visible, setVisible] = useState(false);
  const buttonId = useElementId();

  useEffect(() => {
    if (response && !visible) {
      setVisible(true);
    }
  }, [response, visible]);

  const handleClick = () => {
    if (onConfirm) {
      onConfirm();
      setVisible(false);
    } else {
      handleCancel();
    }
  };

  const handleCancel = () => {
    if (onCancel) {
      onCancel();
    }
    setVisible(false);
  };

  return (
    <Modal
      ariaLabelId={ariaLabelId}
      header={title}
      visible={visible}
      closable={isClosable}
      onCancel={handleCancel}
      data-test-el="api-response-modal"
    >
      <Row>
        <Col
          span={showImg ? 15 : 24}
          className={styles.content}
          data-test-el="response-modal-content"
        >
          <div>{children}</div>

          <div>
            <Button
              id={buttonId}
              buttonType={ButtonType.PRIMARY}
              onClick={handleClick}
            >
              {buttonText ? (
                buttonText
              ) : (
                <FormattedMessage id="FINEOS_COMMON.GENERAL.CLOSE" />
              )}
            </Button>
          </div>
        </Col>
        {showImg && (
          <Col offset={1} span={8} className={styles.imageWrapper}>
            <img
              src={img}
              alt=""
              className={styles.apiResponseImage}
              data-test-el={`${type
                .toLowerCase()
                .replace('_', '-')}-response-image`}
            />
          </Col>
        )}
      </Row>
    </Modal>
  );
};
