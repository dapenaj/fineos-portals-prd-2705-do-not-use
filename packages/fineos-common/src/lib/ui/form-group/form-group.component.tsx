/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo, useState } from 'react';
import classNames from 'classnames';
import { Form } from 'antd';
import { FormItemProps } from 'antd/lib/form';
import { getIn, useFormikContext } from 'formik';

import { FormLabel } from '../form-label';
import { DatePickerInput } from '../date-picker-input';
import { RangePickerInput } from '../range-picker-input';
import { UiRadioInput, FormRadioInput } from '../radio-input';
import { DefaultInputProps } from '../../types/form.type';
import { useElementId } from '../../hooks';
import { ConfigurableText } from '../configurable-text';
import { SwitchLabel } from '../switch';
import { FormGroupContext, getFocusedElementShadow } from '../../utils';
import { createThemedStyles, textStyleToCss } from '../theme';
import { TextAreaCharactersCounter } from '../text-area-characters-counter';
import { TooltipPlacementType } from '../tooltip';

import styles from './form-group.module.scss';

const { Item } = Form;

// List of components that need a special class to fit the wrapper to the content.
// This helps avoid issues with feedback (input error) icon.
// The rest of the components have 100% width.
const narrowComponents = [DatePickerInput, RangePickerInput];
const fieldsetComponents = [FormRadioInput, UiRadioInput];

type FormGroupProps = {
  /**
   * Determines text of input value
   */
  label?: React.ReactElement | string | undefined | null;
  /**
   * Determines text of label tooltip
   */
  labelTooltipContent?: React.ReactElement | string;
  /**
   * Optional attribute to determine labels className
   */
  labelClassName?: string;
  /**
   * Optional attribute to determine wrappers className
   */
  wrapperClassName?: string;
  /**
   * Determines if there is "required icon" displayed
   */
  isRequired?: boolean;
  /**
   * Determines if there is "optional label" displayed
   */
  isOptional?: boolean;
  /**
   * Optional render form group without bottom margin
   */
  noMargin?: boolean;
  /**
   * Optional flag to be passed to FormLabel (see details there)
   */
  isSmallLabel?: boolean;
  /**
   * Optional flag to show error message, default is true
   */
  showErrorMessage?: boolean;
  /**
   * Optional flag to enable input invalid styles from outside, defaults to false
   */
  isGroupInvalid?: boolean;
  /**
   * Optional additional descriptive message for the field which would appear under the input.
   */
  additionalMessage?: React.ReactElement<{
    id?: string;
    className?: string;
  }>;
  /**
   * Optional immediate validation for specific error messages
   */
  immediateErrorMessages?: string[];
  /**
   * Optional max length. When used, text length counter is visible
   */
  counterMaxLength?: number;
  /**
   * Optional counter translationId. Overrides default counter message
   */
  counterMessageId?: string;
  /**
   * Optional property for popover placement
   */
  popoverPlacement?: TooltipPlacementType;
} & (
  | {
      /**
       * Determines input name property to link label with it
       */
      name: string;
      children: React.ReactElement<any>;
    }
  | {
      children: React.ReactElement<{ name: string }>;
    }
);

const useThemedStyles = createThemedStyles(theme => ({
  formGroup: {
    '& .ant-form-item-control .ant-form-item-explain-error': {
      color: theme.colorSchema.attentionColor,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-form-item-has-error': {
      '& .ant-input, .ant-input-number': {
        borderColor: theme.colorSchema.attentionColor
      },
      '& .ant-input:focus': {
        borderColor: theme.colorSchema.attentionColor,
        boxShadow: getFocusedElementShadow({
          color: theme.colorSchema.attentionColor
        })
      },
      '& .ant-select:not(.ant-select-disabled):not(.ant-select-customize-input) .ant-select-selector': {
        // Important must be used here because antd uses its own 'important'
        // to style this element
        borderColor: `${theme.colorSchema.attentionColor} !important`
      }
    }
  },
  extra: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  label: {
    color: theme.colorSchema.neutral8
  },
  legendLabel: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  '::placeholder': {
    color: theme.colorSchema.neutral6
  }
}));

export const FormGroup: React.FC<FormGroupProps & DefaultInputProps> = ({
  label,
  labelTooltipContent,
  labelClassName,
  isRequired = false,
  isOptional,
  className,
  wrapperClassName,
  children,
  isSmallLabel,
  noMargin,
  showErrorMessage = true,
  isGroupInvalid = false,
  additionalMessage,
  immediateErrorMessages = [],
  counterMaxLength,
  counterMessageId,
  popoverPlacement,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const [hasFeedback, setHasFeedback] = useState(true);
  const [isFieldErrorIconDisabled, setIsFieldErrorIconDisabled] = useState(
    true
  );
  const name = 'name' in children.props ? children.props.name : props.name;
  const isGroupExternallyInvalid =
    typeof isGroupInvalid === 'boolean' && isGroupInvalid;
  const isSwitchLabel =
    label && typeof label !== 'string' && label.type === SwitchLabel;
  const inputId = useElementId();
  const errorId = useElementId();
  const uniqueId = useElementId();
  const additionalMessageId = useElementId();
  const formikBag = useFormikContext();
  const validation = !!formikBag && getIn(formikBag.errors, name);
  const wasTouched =
    !!formikBag &&
    (getIn(formikBag.touched, name) ||
      immediateErrorMessages.includes(validation));
  const hasError = Boolean(wasTouched && validation) || undefined;
  const showError = Boolean(showErrorMessage) && hasError;
  const hasAdditionalMessage = Boolean(additionalMessage);
  const input = React.cloneElement(children, {
    id: inputId,
    ...((showError ||
      hasAdditionalMessage ||
      (!additionalMessage && counterMaxLength)) && {
      'aria-describedby': showError ? errorId : additionalMessageId
    })
  });
  const isFieldsetComponent = fieldsetComponents.includes(
    input.props?.component
  );
  const formLabelDataTestEl =
    label && typeof label === 'string'
      ? `${label
          .toString()
          .trim()
          .replace(' ', '-')}-form-group`
      : 'form-group-label';
  const Label = isSwitchLabel
    ? label
    : label && (
        <FormLabel
          tooltipContent={labelTooltipContent}
          isRequired={isRequired}
          isOptional={isOptional}
          className={classNames(themedStyles.label, labelClassName, {
            [styles.legendLabel]: isFieldsetComponent
          })}
          isSmallLabel={isSmallLabel}
          data-test-el={formLabelDataTestEl}
          popoverPlacement={popoverPlacement}
        >
          {isFieldsetComponent ? (
            <legend
              className={classNames(themedStyles.legendLabel)}
              id={uniqueId}
            >
              {label}
            </legend>
          ) : (
            <>{label}</>
          )}
        </FormLabel>
      );

  const getEnhancedAdditionalMessage = () => {
    if (!additionalMessage && counterMaxLength) {
      additionalMessage = (
        <TextAreaCharactersCounter
          fieldName={name}
          maxLength={counterMaxLength}
          messageTranslationId={counterMessageId}
        />
      );
      return (
        additionalMessage &&
        !hasError &&
        React.cloneElement(additionalMessage, {
          id: additionalMessageId,
          className: themedStyles.extra
        })
      );
    }
  };

  const fromGroupContextValue = useMemo(
    () => ({
      isRequired,
      disableFeedback: (hasFeedbackValue: boolean) =>
        setHasFeedback(hasFeedbackValue),
      disableFieldErrorIcon: (isDisabled = true) =>
        setIsFieldErrorIconDisabled(isDisabled)
    }),
    [isRequired, setHasFeedback, setIsFieldErrorIconDisabled]
  );

  const formGroupProps = {
    'data-test-el': props['data-test-el'] || 'form-group',
    className: classNames(className, styles.wrapper, {
      [styles.fitWrapper]: narrowComponents.includes(input.props?.component),
      [styles.switchWrapper]: isSwitchLabel,
      [styles.noMargin]: noMargin,
      [styles.disableFieldErrorIcon]: isFieldErrorIconDisabled
    }),
    hasFeedback:
      isGroupExternallyInvalid || (hasFeedback && Boolean(validation)),
    validateStatus: isGroupExternallyInvalid
      ? 'error'
      : showError
      ? hasError && 'error'
      : '',
    htmlFor: inputId,
    extra: getEnhancedAdditionalMessage(),
    help: showError && <ConfigurableText id={validation} elementId={errorId} />,
    label: Label
  } as FormItemProps<any>;

  return (
    <FormGroupContext.Provider value={fromGroupContextValue}>
      <div className={classNames(themedStyles.formGroup, wrapperClassName)}>
        {isFieldsetComponent ? (
          <fieldset
            className={styles.formGroupFieldset}
            aria-labelledby={uniqueId}
          >
            <Item {...formGroupProps}>{input}</Item>
          </fieldset>
        ) : (
          <Item {...formGroupProps}>{input}</Item>
        )}
      </div>
    </FormGroupContext.Provider>
  );
};
