/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import {
  contrastTextColor,
  createThemedStyles,
  textStyleToCss
} from '../theme';

import styles from './card.module.scss';

const useThemedStyles = createThemedStyles(theme => {
  const textColor = contrastTextColor(theme, theme.colorSchema.neutral1);

  return {
    card: {
      background: theme.colorSchema.neutral1,
      color: textColor,
      ...textStyleToCss(theme.typography.baseText)
    }
  };
});

export const Card = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement>
>(({ className, ...props }, ref) => {
  const themedStyles = useThemedStyles();
  return (
    <div
      className={classNames(styles.card, themedStyles.card, className)}
      {...props}
      ref={ref}
    />
  );
});
