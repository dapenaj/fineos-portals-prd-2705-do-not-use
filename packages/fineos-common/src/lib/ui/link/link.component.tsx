/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

import { getFocusedElementShadow } from '../../utils';
import {
  createThemedStyles,
  ThemeModel,
  textStyleToCss,
  darkenColor
} from '../theme';

import styles from './link.module.scss';

/**
 * This function need to be exported cause in many cases 3rd-parties may override link styles
 * which is undesirable, so in context we would need to have reference to original styles
 *
 * @param theme
 */

type LinkProps = {
  disableUnderline?: boolean;
};

export const linkThemedStylesProducer = (theme: ThemeModel) => ({
  link: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.primaryAction,
    '&:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    },
    '&:hover': {
      color: darkenColor(theme.colorSchema, 'primaryAction')
    }
  }
});

const useThemedStyles = createThemedStyles(linkThemedStylesProducer);

export const Link = React.forwardRef<
  NavLink<unknown>,
  React.ComponentProps<typeof NavLink> & LinkProps
>(
  (
    { className, disableUnderline = false, ...props },
    ref: React.Ref<NavLink<unknown>>
  ) => {
    const themedStyles = useThemedStyles();
    return (
      <NavLink
        className={classNames(styles.link, themedStyles.link, className, {
          [styles.underlineLink]: !disableUnderline
        })}
        {...props}
        ref={ref as any}
      />
    );
  }
);
