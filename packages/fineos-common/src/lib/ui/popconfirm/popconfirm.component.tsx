/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useRef, useEffect } from 'react';
import { useIntl } from 'react-intl';
import classNames from 'classnames';
import {
  useDialogState,
  Dialog,
  DialogDisclosure,
  DialogBackdrop
} from 'reakit';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

import { ButtonType } from '../../types';
import { createThemedStyles, textStyleToCss } from '../theme';
import { FormattedMessage } from '../formatted-message';
import { Button } from '../button';
import { Icon } from '../icon';
import { getFocusedElementShadow } from '../../utils';

import styles from './popconfirm.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  popconfirmContent: {
    backgroundColor: theme.colorSchema.neutral1,
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  popconfirmArrow: {
    borderColor: theme.colorSchema.neutral1
  },
  popconfirmIcon: {
    color: theme.colorSchema.pendingColor
  },
  popconfirmTrigger: {
    '&:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  }
}));

const POPCONFIRM_WIDTH = 250;

type PopconfirmProps = {
  /**
   * Trigger element to toggle popconfirm.
   */
  trigger: React.ReactElement;
  /**
   * Modal aria-label, required by a11y.
   */
  ariaLabelId: string;
  /**
   * Optional classname applied to the popconfirm.
   */
  dialogClassName?: string;
  /**
   * Popconfirm message.
   */
  popconfirmMessage: React.ReactElement;
  /**
   * On confirm callback.
   */
  onConfirm: () => void;
  /**
   * Optional on cancel callback. Defaults to hide.
   */
  onCancel?: () => void;
  /**
   * Optional custom icon element.
   */
  icon?: React.ReactElement;
  /**
   * Optional test element name. Defaults to "popconfirm".
   */
  'data-test-el'?: string;
};

export const Popconfirm: React.FC<PopconfirmProps> = ({
  ariaLabelId,
  popconfirmMessage,
  onConfirm,
  onCancel = () => popconfirmState.hide(),
  icon,
  trigger,
  'data-test-el': dataTestEl = 'popconfirm',
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const popconfirmState = useDialogState({ animated: true });
  const intl = useIntl();
  const triggerRef = useRef<HTMLButtonElement>(null);
  const [popconfirmPosition, setPopconfirmPosition] = useState({
    top: 0,
    left: 0
  });
  const [localVisible, setLocalVisible] = useState(false);

  useEffect(() => {
    if (popconfirmState.visible) {
      setLocalVisible(true);
    }
  }, [popconfirmState.visible]);

  const onTransitionEnd = () => {
    if (localVisible !== popconfirmState.visible) {
      setLocalVisible(popconfirmState.visible);
    }
  };

  const handlePopconfirmPosition = () => {
    const triggerEl = triggerRef.current;
    if (triggerEl) {
      // get trigger position and width
      const { top, left, width } = triggerEl.getBoundingClientRect();
      // position popconfirm
      setPopconfirmPosition({
        top: top - 20,
        left: left - (POPCONFIRM_WIDTH - width) / 2
      });
    }
  };

  const iconElement = icon ? (
    icon
  ) : (
    <Icon
      icon={faExclamationTriangle}
      className={classNames(styles.popconfirmIcon, themedStyles.popconfirmIcon)}
      size="2x"
      iconLabel={
        <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.EXCLAMATION_TRIANGLE_ICON_LABEL" />
      }
    />
  );

  return (
    <>
      <DialogDisclosure
        className={classNames(
          styles.popconfirmTrigger,
          themedStyles.popconfirmTrigger
        )}
        ref={triggerRef}
        onClick={handlePopconfirmPosition}
        {...popconfirmState}
      >
        {trigger}
      </DialogDisclosure>
      <DialogBackdrop
        onTransitionEnd={onTransitionEnd}
        tabIndex={popconfirmState.visible ? 0 : -1}
        className={styles.popconfirmBackdrop}
        onClick={onCancel}
        {...popconfirmState}
      >
        <Dialog
          hideOnClickOutside={false}
          hideOnEsc={false}
          onClick={(event: React.MouseEvent) => event.stopPropagation()}
          style={{
            top: popconfirmPosition.top,
            left: popconfirmPosition.left,
            width: POPCONFIRM_WIDTH
          }}
          aria-label={intl.formatMessage({
            id: ariaLabelId
          })}
          onKeyDown={(event: React.KeyboardEvent) => {
            if (event.key === 'Escape' && onCancel) {
              onCancel();
            }
          }}
          className={styles.popconfirm}
          {...popconfirmState}
          {...props}
        >
          {localVisible && (
            <div
              data-test-el={dataTestEl}
              className={classNames(
                themedStyles.popconfirmContent,
                styles.popconfirmContent
              )}
            >
              <div
                className={classNames(
                  themedStyles.popconfirmArrow,
                  styles.popconfirmArrow
                )}
              />
              <div className={styles.popconfirmMessage}>
                {iconElement}
                {popconfirmMessage}
              </div>
              <div className={styles.popconfirmButtons}>
                <FormattedMessage
                  aria-label={intl.formatMessage({
                    id: 'FINEOS_COMMON.GENERAL.NO'
                  })}
                  id="FINEOS_COMMON.GENERAL.NO"
                  as={Button}
                  buttonType={ButtonType.GHOST}
                  onClick={onCancel}
                />
                <FormattedMessage
                  aria-label={intl.formatMessage({
                    id: 'FINEOS_COMMON.GENERAL.YES'
                  })}
                  id="FINEOS_COMMON.GENERAL.YES"
                  as={Button}
                  buttonType={ButtonType.PRIMARY}
                  onClick={onConfirm}
                />
              </div>
            </div>
          )}
        </Dialog>
      </DialogBackdrop>
    </>
  );
};
