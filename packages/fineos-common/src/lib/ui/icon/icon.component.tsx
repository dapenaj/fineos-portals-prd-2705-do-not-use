/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FontAwesomeIcon,
  FontAwesomeIconProps
} from '@fortawesome/react-fontawesome';

import {
  PossiblyFormattedMessageElement,
  useRawFormattedMessage
} from '../formatted-message';

type IconProps = FontAwesomeIconProps & {
  /**
   * iconLabel- accessibility label which should give a sens to assistive technology
   * users, what icon means
   */
  iconLabel: PossiblyFormattedMessageElement;
};

/**
 * FontAwesome icon wrapper, which allows us to add our own props to rendered icon.
 */
export const Icon: React.FC<IconProps> = ({
  icon,
  iconLabel,
  className,
  ...props
}) => {
  const title = useRawFormattedMessage(iconLabel);
  return (
    <FontAwesomeIcon
      icon={icon}
      title={title} // this prop will be used as svg backup <title> inside svg element
      aria-label={title} // svg prop for accessibility purposes
      className={className}
      aria-hidden="false"
      {...props}
    />
  );
};
