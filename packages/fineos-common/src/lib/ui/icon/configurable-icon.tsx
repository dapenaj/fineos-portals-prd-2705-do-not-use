/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import classNames from 'classnames';

import { sanitizeSvg } from '../../utils';
import { useTheme } from '../theme';
import {
  PossiblyFormattedMessageElement,
  useRawFormattedMessage
} from '../formatted-message';
import { useElementId } from '../../hooks';

import styles from './configurable-icon.module.scss';

type ConfigurableIcon = {
  /**
   * name - defines name in theme.icons structure under which icon can be found
   */
  name: string;
  /**
   * className - optional CSS class which can be provided to icon wrapper, useful
   * for setting font-size as icon size based on this value (icon size = 1em)
   * or providing icon color (use CSS color property for this)
   */
  className?: string;
  /**
   * iconClassName - optional CSS class which can be provided to actual SVG
   */
  iconClassName?: string;
  /**
   * iconLabel- accessibility label which should give a sens to assistive technology
   * users, what icon means
   */
  iconLabel?: PossiblyFormattedMessageElement;
};

const createSVGEl = (tagName: string) =>
  document.createElementNS('http://www.w3.org/2000/svg', tagName);

/**
 * ConfigurableIcon allows to inject configurable by carrier icons based on theme
 */
export const ConfigurableIcon: React.FC<ConfigurableIcon> = ({
  name,
  className,
  iconLabel,
  iconClassName,
  ...props
}) => {
  const theme = useTheme();
  const title = useRawFormattedMessage(iconLabel);
  const titleElId = useElementId();

  const styledSvgIcon = useMemo(() => {
    if (theme.icons && name in theme.icons) {
      const el = document.createElement('div');
      el.innerHTML = sanitizeSvg(theme.icons[name]);

      if (el.firstElementChild) {
        const svg = el.firstElementChild;

        if (title) {
          const titleEl = createSVGEl('title');
          titleEl.setAttribute('id', titleElId);
          titleEl.innerHTML = sanitizeSvg(title);

          svg.insertBefore(titleEl, svg.firstChild);
          svg.setAttribute('aria-labelledby', titleElId);
        }

        svg.setAttribute('role', 'img');
        svg.setAttribute(
          'class',
          classNames(
            svg.getAttribute('class'),
            styles.customIcon,
            iconClassName
          )
        );

        return el.innerHTML;
      }

      return null;
    }

    return null;
  }, [theme, name]);

  if (styledSvgIcon) {
    const ariaAttrs = !title
      ? {
          'aria-hidden': true
        }
      : {};
    return (
      // styledSvgIcon sanitized above
      <span
        {...props}
        dangerouslySetInnerHTML={{ __html: styledSvgIcon }}
        className={className}
        {...ariaAttrs}
      />
    );
  }

  return null;
};
