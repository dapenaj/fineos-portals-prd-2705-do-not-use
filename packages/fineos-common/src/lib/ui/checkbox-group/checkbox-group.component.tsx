/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect } from 'react';
import { FieldProps, getIn } from 'formik';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { Checkbox as AntCheckbox } from 'antd';
import classNames from 'classnames';

import { ThemeModel, createThemedStyles } from '../theme';
import { DefaultInputProps } from '../../types/form.type';
import { FormattedMessage } from '../formatted-message';
import { FormGroupContext, getFocusedElementShadow } from '../../utils';
import { Checkbox } from '../checkbox';

import styles from './checkbox-group.module.scss';

const AntCheckboxGroup = AntCheckbox.Group;

type CheckboxProps = {
  /**
   * Optional attribute to determine if radio group should have vertical or horizontal direction.
   * Defaults to false (horizontal)
   */
  isVertical?: boolean;
  /**
   * Optional attribute to determine if "Select all" checkbox and functionality is available.
   * Defaults to false
   */
  hasSelectAll?: boolean;
  /**
   * List of elements displayed as component children
   */
  children: React.ReactElement<CheckboxChildElement>[];
};

export interface CheckboxChildElement {
  value: string | number | boolean;
  'data-test-el'?: string;
  className?: string;
  onBlur?: () => void;
  'aria-required'?: boolean;
  'aria-invalid'?: string;
}
const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  checkbox: {
    '& .ant-checkbox-checked .ant-checkbox-inner': {
      backgroundColor: theme.colorSchema.primaryAction,
      borderColor: theme.colorSchema.primaryAction
    },
    '& .ant-checkbox-checked::after': {
      border: `1px solid ${theme.colorSchema.primaryAction}`
    },
    '& .ant-checkbox-checked .ant-checkbox-inner::after': {
      borderColor: theme.colorSchema.neutral1
    },
    '& .ant-checkbox-wrapper:hover .ant-checkbox-inner, .ant-checkbox-input:focus + .ant-checkbox-inner': {
      borderColor: theme.colorSchema.primaryAction
    },
    '& .ant-checkbox-input:focus + .ant-checkbox-inner': {
      boxShadow: getFocusedElementShadow({
        isWide: true,
        color: theme.colorSchema.primaryAction
      })
    },
    '& .ant-checkbox-inner': {
      borderColor: theme.colorSchema.neutral5,
      background: theme.colorSchema.neutral1
    }
  }
}));

export const CheckboxGroup: React.FC<CheckboxProps &
  DefaultInputProps &
  FieldProps> = ({
  className,
  field,
  form: { setFieldValue, setFieldTouched, errors },
  isVertical = false,
  isDisabled = false,
  hasSelectAll = false,
  children,
  ...props
}) => {
  const { disableFeedback, isRequired } = useContext(FormGroupContext);
  const themedStyles = useThemedStyles();
  const isInvalid = getIn(errors, field.name);
  const optionsList = React.Children.map(
    children,
    (child: React.ReactElement<CheckboxChildElement>) => child.props.value
  );

  useEffect(() => {
    disableFeedback(false);
  }, []);

  const handleChangeAll = (e: CheckboxChangeEvent) => {
    const listToSet = e.target.checked ? optionsList : [];
    setFieldValue(field.name, listToSet);
  };

  const clonedChildren = React.Children.map(
    children,
    (child: React.ReactElement<CheckboxChildElement>) =>
      React.cloneElement(child, {
        'data-test-el': props['data-test-el'] || 'checkbox',
        className: classNames({ [styles.isVertical]: isVertical }),
        onBlur: () => setFieldTouched(field.name),
        'aria-required': isRequired,
        ...(isInvalid && {
          'aria-invalid': 'true'
        })
      })
  );
  return (
    <div
      className={classNames(themedStyles.checkbox, styles.checkbox, className)}
      data-test-el={props['data-test-el' || 'checkbox-wrapper']}
    >
      {hasSelectAll && (
        <Checkbox
          className={classNames({ [styles.isVertical]: isVertical })}
          onChange={handleChangeAll}
          checked={field.value.length === optionsList.length}
          data-test-el={
            props['data-test-el']
              ? `${props['data-test-el']}-select-all`
              : 'checkbox-select-all'
          }
        >
          <FormattedMessage id="FINEOS_COMMON.GENERAL.SELECT_ALL" />
        </Checkbox>
      )}
      <AntCheckboxGroup
        data-test-el={
          props['data-test-el']
            ? `${props['data-test-el']}-group`
            : 'checkbox-group'
        }
        value={field.value}
        onChange={list => setFieldValue(field.name, list)}
        disabled={isDisabled}
      >
        {clonedChildren}
      </AntCheckboxGroup>
    </div>
  );
};
