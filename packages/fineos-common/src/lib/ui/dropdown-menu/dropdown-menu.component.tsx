import React, { useRef } from 'react';
import { Dropdown } from 'antd';
import { DropDownProps } from 'antd/lib/dropdown';
import classNames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';

import styles from './dropdown-menu.module.scss';

type DropdownMenuProps = {
  /**
   * Optional method to track when animation after visible change ends
   */
  onAfterVisibleChange?: (visible: boolean) => void;
  /**
   * Optional flag to determine if popup is destroyed on hide
   */
  destroyPopupOnHide?: boolean;
  /**
   * Optional test element name. Defaults to "dropdown-menu"
   */
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  dropdownMenuOverlay: {
    '& .ant-dropdown-menu': {
      background: theme.colorSchema.neutral1
    },
    '& .ant-dropdown-menu-item > a, .ant-dropdown-menu-item, .ant-dropdown-menu-link': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-dropdown-menu-item:hover, .ant-dropdown-menu-link:hover': {
      background: theme.colorSchema.neutral2
    },
    '& .ant-dropdown-menu-item-group-title': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseTextSemiBold)
    },
    '& .ant-dropdown-menu-item, .ant-dropdown-menu-item span': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  }
}));

export const DropdownMenu: React.FC<DropDownProps & DropdownMenuProps> = ({
  children,
  className,
  overlayClassName,
  onAfterVisibleChange,
  onVisibleChange,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const isVisibleRef = useRef(false);
  const dropdown = (
    <Dropdown
      data-test-el={props['data-test-el'] || 'dropdown-menu'}
      className={className}
      onVisibleChange={val => {
        isVisibleRef.current = val;
        if (onVisibleChange) {
          onVisibleChange(val);
        }
      }}
      overlayClassName={classNames(
        overlayClassName,
        styles.dropdownMenuOverlay,
        themedStyles.dropdownMenuOverlay
      )}
      getPopupContainer={(node: HTMLElement | undefined) => {
        if (node) {
          return node.parentNode as HTMLElement;
        }
        return document.body as HTMLElement;
      }}
      {...props}
    >
      {children}
    </Dropdown>
  );
  return onAfterVisibleChange ? (
    <div
      onAnimationEnd={() => {
        onAfterVisibleChange(isVisibleRef.current);
      }}
    >
      {dropdown}
    </div>
  ) : (
    dropdown
  );
};
