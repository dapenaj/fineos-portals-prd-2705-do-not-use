/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';
import classNames from 'classnames';
import { useDialogState, Dialog, DialogBackdrop } from 'reakit';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

import { ButtonType } from '../../types';
import { createThemedStyles, textStyleToCss } from '../theme';
import { FormattedMessage } from '../formatted-message';
import { Button } from '../button';
import { Icon } from '../icon';

import styles from './overlay-popconfirm.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  popconfirmContent: {
    backgroundColor: theme.colorSchema.neutral1,
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  popconfirmIcon: {
    color: theme.colorSchema.pendingColor
  }
}));

const POPCONFIRM_WIDTH = 250;

type PopconfirmProps = {
  /**
   * State to control modal visibility.
   */
  visible: boolean;
  /**
   * Modal aria-label, required by a11y.
   */
  ariaLabelId: string;
  /**
   * Optional classname applied to the popconfirm.
   */
  dialogClassName?: string;
  /**
   * Popconfirm message.
   */
  popconfirmMessage: React.ReactElement;
  /**
   * Optional horizontal offset to set position of popconfirm, defaults to '50%'.
   */
  horizontalOffset?: number | string;
  /**
   * On confirm callback.
   */
  onConfirm: () => void;
  /**
   * Optional on cancel callback. Defaults to hide.
   */
  onCancel?: () => void;
  /**
   * Optional custom icon element.
   */
  icon?: React.ReactElement;
  /**
   * Optional test element name. Defaults to "overlay-popconfirm".
   */
  'data-test-el'?: string;
};

export const OverlayPopconfirm: React.FC<PopconfirmProps> = ({
  ariaLabelId,
  popconfirmMessage,
  visible,
  icon,
  horizontalOffset = '50%',
  onConfirm,
  onCancel,
  'data-test-el': dataTestEl = 'overlay-popconfirm',
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const popconfirmState = useDialogState({ animated: true });
  const intl = useIntl();
  const [localVisible, setLocalVisible] = useState(false);

  useEffect(() => {
    if (visible) {
      popconfirmState.show();
      setLocalVisible(true);
    } else {
      popconfirmState.hide();
    }
  }, [visible]);

  const onTransitionEnd = () => {
    if (localVisible !== visible) {
      setLocalVisible(visible);
    }
  };

  const iconElement = icon ? (
    icon
  ) : (
    <Icon
      icon={faExclamationTriangle}
      className={classNames(styles.popconfirmIcon, themedStyles.popconfirmIcon)}
      size="2x"
      iconLabel={
        <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.EXCLAMATION_TRIANGLE_ICON_LABEL" />
      }
    />
  );

  return (
    <DialogBackdrop
      onTransitionEnd={onTransitionEnd}
      tabIndex={visible ? 0 : -1}
      className={styles.popconfirmBackdrop}
      onClick={onCancel}
      {...popconfirmState}
    >
      <Dialog
        hideOnClickOutside={false}
        hideOnEsc={false}
        onClick={(event: React.MouseEvent) => event.stopPropagation()}
        style={{
          right: horizontalOffset,
          width: POPCONFIRM_WIDTH
        }}
        aria-label={intl.formatMessage({
          id: ariaLabelId
        })}
        onKeyDown={(event: React.KeyboardEvent) => {
          if (event.key === 'Escape' && onCancel) {
            onCancel();
          }
        }}
        role="dialog"
        className={styles.popconfirm}
        {...popconfirmState}
        {...props}
      >
        {localVisible && (
          <div
            data-test-el={dataTestEl}
            className={classNames(
              themedStyles.popconfirmContent,
              styles.popconfirmContent
            )}
          >
            <div className={styles.popconfirmMessage}>
              {iconElement}
              {popconfirmMessage}
            </div>
            <div className={styles.popconfirmButtons}>
              <FormattedMessage
                aria-label={intl.formatMessage({
                  id: 'FINEOS_COMMON.GENERAL.NO'
                })}
                id="FINEOS_COMMON.GENERAL.NO"
                as={Button}
                buttonType={ButtonType.GHOST}
                onClick={onCancel}
              />
              <FormattedMessage
                aria-label={intl.formatMessage({
                  id: 'FINEOS_COMMON.GENERAL.YES'
                })}
                id="FINEOS_COMMON.GENERAL.YES"
                as={Button}
                buttonType={ButtonType.PRIMARY}
                onClick={onConfirm}
              />
            </div>
          </div>
        )}
      </Dialog>
    </DialogBackdrop>
  );
};
