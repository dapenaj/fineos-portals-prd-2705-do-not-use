/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';
import { DEFAULT_FALLBACK, NDASH_FALLBACK } from '../../constants';
import { useElementId } from '../../hooks';
import {
  PossiblyFormattedMessageElement,
  useRawFormattedMessage
} from '../formatted-message';

import styles from './property-item.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  value: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  label: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallTextSemiBold)
  },
  block: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type PropertyItemProps = {
  /**
   * Optional FormattedMessage for property item label.
   */
  label?: React.ReactNode;
  /**
   * Optional classname applied to the property item.
   */
  className?: string;
  /**
   * Optional boolean to determine if property item is displayed as a block element, or inline. Defaults to block.
   */
  isBlock?: boolean;
  /**
   * Optional test element name. Defaults to "property-item".
   */
  'data-test-el'?: string;
  /**
   * Optional boolean to determine if property item is displayed a dash or en-dash. Default dash.
   */
  isDefaultFallback?: boolean;
  ariaLabelItem?: PossiblyFormattedMessageElement;
};

export const PropertyItem: React.FC<PropertyItemProps> = ({
  label,
  children,
  className,
  isBlock = true,
  isDefaultFallback = true,
  ariaLabelItem,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const uniqueId = useElementId();
  const fallbackValue = isDefaultFallback ? DEFAULT_FALLBACK : NDASH_FALLBACK;
  const ariaLabel = useRawFormattedMessage(ariaLabelItem);

  return (
    <div
      data-test-el={props['data-test-el'] || 'property-item'}
      className={classnames(styles.wrapper, className)}
    >
      {label && (
        <span
          className={classnames(styles.label, themedStyles.label, {
            [styles.block]: isBlock
          })}
          data-test-el="property-item-label"
          id={uniqueId}
        >
          {label}
        </span>
      )}
      <span
        className={classnames(themedStyles.value, {
          [styles.block]: isBlock,
          [themedStyles.block]: isBlock
        })}
        data-test-el="property-item-value"
        aria-describedby={label ? uniqueId : undefined}
        aria-label={ariaLabel}
      >
        {children ? children : fallbackValue}
      </span>
    </div>
  );
};
