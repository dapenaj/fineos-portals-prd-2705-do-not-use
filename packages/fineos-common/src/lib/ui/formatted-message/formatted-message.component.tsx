/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  useIntl,
  MessageDescriptor,
  FormattedMessage as FormattedMessageIntl
} from 'react-intl';
import { PrimitiveType } from 'intl-messageformat';
import { isEmpty as _isEmpty } from 'lodash';

import { Link } from '../link';
import { Button, FormButton, UiButton } from '../button';

type AllowedTags =
  | 'strong'
  | 'span'
  | 'div'
  | 'p'
  | 'em'
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'h6';
type AllowedComponents =
  | typeof UiButton
  | typeof FormButton
  | typeof Button
  | typeof Link;

export type FormattedMessageAs = AllowedTags | AllowedComponents;
export type FormattedMessageProps<As extends FormattedMessageAs = 'span'> = (
  | Omit<
      As extends AllowedTags
        ? JSX.IntrinsicElements[As]
        : React.ComponentProps<As>,
      'id' | 'children'
    >
  | {
      children: (
        extraParams: { 'data-test-el': string; id?: string },
        ...orignalParams: React.ReactNode[]
      ) => React.ReactNode;
    }
) & {
  ariaLabelName?: {
    descriptor: MessageDescriptor;
    values?: Record<string, PrimitiveType>;
  };
} & Omit<
    React.ComponentProps<typeof FormattedMessageIntl>,
    'children' | 'tagName' | 'id'
  > & {
    as?: As;
    elementId?: string;
    id: string;
  };

export const FormattedMessage = <As extends FormattedMessageAs = 'span'>({
  as: Element = 'span' as As,
  elementId,
  id,
  values,
  description,
  defaultMessage,
  ariaLabelName,
  ...restProps
}: FormattedMessageProps<As>) => {
  const intl = useIntl();
  const ariaLabel =
    !_isEmpty(ariaLabelName) && !!ariaLabelName
      ? intl.formatMessage(ariaLabelName.descriptor, ariaLabelName.values)
      : undefined;
  return (
    <FormattedMessageIntl
      id={id}
      values={values}
      description={description}
      defaultMessage={defaultMessage}
    >
      {'children' in restProps
        ? (...translated: string[]) => {
            return restProps.children(
              {
                'data-test-el': id,
                id: elementId
              },
              ...translated
            );
          }
        : (translated: string) => {
            return (
              <Element
                id={elementId}
                data-test-el={id}
                aria-label={ariaLabel}
                {...(restProps as any)}
              >
                {translated}
              </Element>
            );
          }}
    </FormattedMessageIntl>
  );
};
