/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';

import { Color, createThemedStyles } from '../theme';
import { FormattedMessage } from '../formatted-message';

import styles from './timeline-legend.module.scss';

const useThemedStyles = createThemedStyles(theme => {
  const boxBorder = (color: Color) => `1px solid ${color}`;
  const box = (color: Color) => ({
    '&:before': {
      backgroundColor: color,
      border: boxBorder(color)
    }
  });
  const stripeDistance = 0.5; // rem
  return {
    approved: box(theme.colorSchema.successColor),
    pending: box(theme.colorSchema.pendingColor),
    declined: box(theme.colorSchema.attentionColor),
    cancelled: box(theme.colorSchema.neutral6),
    legendGroupCaption: {
      color: theme.colorSchema.neutral8
    },
    legendList: {
      color: theme.colorSchema.neutral8
    },
    continuousTime: {
      '&:before': {
        border: boxBorder(theme.colorSchema.neutral6)
      }
    },
    intermittentTime: {
      '&:before': {
        background: `repeating-linear-gradient(
          -45deg,
          ${theme.colorSchema.neutral5},
          ${theme.colorSchema.neutral5} ${stripeDistance}em,
          ${theme.colorSchema.neutral6} ${stripeDistance}em,
          ${theme.colorSchema.neutral6} ${stripeDistance * 2}em
        )`,
        border: boxBorder(theme.colorSchema.neutral6)
      }
    },
    reducedSchedule: {
      '&:before': {
        background: `linear-gradient(
          to bottom,
          ${theme.colorSchema.neutral5},
          ${theme.colorSchema.neutral5} 50%,
          ${theme.colorSchema.neutral6} 50%,
          ${theme.colorSchema.neutral6}
        )`
      }
    }
  };
});

export const TimelineLegend: React.FC = () => {
  const themedStyles = useThemedStyles();
  return (
    <>
      <div className={styles.legendGroup}>
        <p
          className={classNames(
            styles.legendGroupCaption,
            themedStyles.legendGroupCaption
          )}
        >
          <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR" />
        </p>
        <ul className={classNames(styles.legendList, themedStyles.legendList)}>
          <li className={classNames(styles.legendItem, themedStyles.approved)}>
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_APPROVED" />
          </li>
          <li className={classNames(styles.legendItem, themedStyles.pending)}>
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_PENDING" />
          </li>
          <li className={classNames(styles.legendItem, themedStyles.declined)}>
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_DECLINED" />
          </li>
          <li className={classNames(styles.legendItem, themedStyles.cancelled)}>
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_COLOR_CANCELLED" />
          </li>
        </ul>
      </div>
      <div className={styles.legendGroup}>
        <p
          className={classNames(
            styles.legendGroupCaption,
            themedStyles.legendGroupCaption
          )}
        >
          <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.TIMELINE.LEGEND_GRAPHIC_PATTERN" />
        </p>
        <ul className={classNames(styles.legendList, themedStyles.legendList)}>
          <li
            className={classNames(
              styles.legendItem,
              themedStyles.continuousTime
            )}
          >
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.CONTINUOUS_TIME" />
          </li>
          <li
            className={classNames(
              styles.legendItem,
              themedStyles.intermittentTime
            )}
          >
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.INTERMITTENT_TIME" />
          </li>
          <li
            className={classNames(
              styles.legendItem,
              themedStyles.reducedSchedule
            )}
          >
            <FormattedMessage id="FINEOS_COMMON.NOTIFICATION.REDUCED_SCHEDULE" />
          </li>
        </ul>
      </div>
    </>
  );
};
