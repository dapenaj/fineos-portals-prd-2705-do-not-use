/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { useFormikContext } from 'formik';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

import { FormButton } from '../button';
import { Icon } from '../icon';
import { createThemedStyles } from '../theme';

import styles from './icon-submit-button.module.scss';

type IconSubmitButtonProps = {
  /**
   * Font awesome icon which will be displayed inside a button
   */
  submitIcon: IconDefinition;
  /**
   * Icon label for accessibility
   */
  submitIconLabel: React.ReactElement;
  /**
   * Optional className added to the button
   */
  className?: string;
  /**
   * Optional data-test-el added to the button
   */
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  submitButton: {
    backgroundColor: theme.colorSchema.primaryAction
  },
  submitIcon: {
    color: theme.colorSchema.neutral1,
    fontSize: theme.typography.panelTitle.fontSize
  }
}));

export const IconSubmitButton: React.FC<IconSubmitButtonProps> = ({
  submitIcon,
  submitIconLabel,
  className,
  ...props
}) => {
  const { isSubmitting } = useFormikContext();
  const themedStyles = useThemedStyles();
  return (
    <FormButton
      shouldSubmitForm={false}
      className={classNames(
        styles.button,
        themedStyles.submitButton,
        className
      )}
      data-test-el={props['data-test-el'] || 'icon-submit-button'}
      htmlType="submit"
    >
      {!isSubmitting && (
        <Icon
          icon={submitIcon}
          iconLabel={submitIconLabel}
          className={classNames(styles.submitIcon, themedStyles.submitIcon)}
        />
      )}
    </FormButton>
  );
};
