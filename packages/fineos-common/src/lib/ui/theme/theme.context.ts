/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { createTheming, createUseStyles } from 'react-jss';

import { ThemeModel } from './theme.model';

export const Theme = React.createContext<ThemeModel | null>(null);

export const theming = createTheming(Theme);

export type CreateThemedStyles<ClassNames> = (
  themeFn: ThemeModel
) => Record<keyof ClassNames, any>;

export let IS_RUNTIME_THEME_CONFIGURATION_ENABLED = false;

export const enableRuntimeThemeConfiguration = () => {
  IS_RUNTIME_THEME_CONFIGURATION_ENABLED = true;
};

const typographyClassNameOnChanges = new Set<
  (map: Map<string, Set<string>>) => void
>();

const typographyClassNamesMap = new Map<string, Set<string>>();

export const registerTypographyClassNameChange = (
  callback: (map: Map<string, Set<string>>) => void
): (() => void) => {
  typographyClassNameOnChanges.add(callback);

  setTimeout(() => callback(typographyClassNamesMap));
  return () => {
    typographyClassNameOnChanges.delete(callback);
  };
};

let nextTypographyUpdateTid: number;

const addTypographyClassName = (typographyName: string, selector: string) => {
  if (!typographyClassNamesMap.has(typographyName)) {
    typographyClassNamesMap.set(typographyName, new Set());
  }
  const selectors = typographyClassNamesMap.get(typographyName)!;

  if (!selectors.has(selector)) {
    typographyClassNamesMap.get(typographyName)!.add(selector);

    clearTimeout(nextTypographyUpdateTid);

    nextTypographyUpdateTid = setTimeout(() => {
      for (const typographyClassNameOnChange of Array.from(
        typographyClassNameOnChanges
      )) {
        typographyClassNameOnChange(new Map(typographyClassNamesMap));
      }
    });
  }
};

/**
 * This utility function fixes typing and also allow to read theme from our Theme context
 *
 * @param themeFn
 */
export const createThemedStyles = <ClassNames extends any>(
  themeFn: CreateThemedStyles<ClassNames>
): (() => Record<keyof ClassNames, string>) => {
  let initialStylesObj: Record<keyof ClassNames, any>;

  const useThemeStyles = createUseStyles<ThemeModel>(
    theme => {
      initialStylesObj = themeFn(theme);
      return initialStylesObj;
    },
    { theming } as any
  ) as any;

  return () => {
    const themedStyles = useThemeStyles();

    if (!IS_RUNTIME_THEME_CONFIGURATION_ENABLED) {
      return themedStyles;
    }

    // we don't want this to be changed all the time
    useMemo(() => {
      const themedClassNames = Object.keys(themedStyles);

      for (const themedClassName of themedClassNames) {
        let queue = Object.entries(
          initialStylesObj[themedClassName]
        ).map(item => [`.${themedStyles[themedClassName]}`, ...item]) as [
          string,
          string,
          unknown
        ][];

        while (queue.length) {
          const [currentSelector, prop, value] = queue.pop()!;
          if (prop === '$CUSTOM_FONT_STYLE') {
            addTypographyClassName(value as string, currentSelector);
          } else if (typeof value === 'object' && value) {
            queue = queue.concat(
              Object.entries(value).map(item => [
                prop.replace(/&/g, currentSelector),
                ...item
              ]) as [string, string, unknown][]
            );
          }
        }
      }
    }, [themedStyles]);

    return themedStyles;
  };
};
