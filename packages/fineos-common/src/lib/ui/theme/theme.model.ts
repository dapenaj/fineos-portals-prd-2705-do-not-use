/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export type Color = string;
export type Pixels = number;
export type FontStyle = 'italic' | 'normal';

export type TextStyle = Partial<{
  name: string;
  fontFamily: string;
  fontWeight: string | number;
  fontSize: Pixels;
  fontStyle: FontStyle;
  lineHeight: number;
  textTransform?: 'uppercase' | 'lowercase';
}>;

export type ThemeModel = {
  colorSchema: ThemedColorSchema;
  typography: ThemedTypography;
  header: ThemedHeader;
  images?: ThemedImages;
  icons?: Record<string, string>;
  highContrastMode?: ThemeModel;
};

export type ThemedColorSchema = {
  primaryColor: Color;
  primaryColorContrast?: Color;
  callToActionColor: Color;
  primaryAction: Color;
  successColor: Color;
  pendingColor: Color;
  attentionColor: Color;
  infoColor: Color;

  neutral9: Color;
  neutral8: Color;
  neutral7: Color;
  neutral6: Color; // disabledTextColor
  neutral5: Color;
  neutral4: Color;
  neutral3: Color; // disabledBackgroundColor
  neutral2: Color; // backgroundColor
  neutral1: Color;

  graphColorOne: Color;
  graphColorTwo: Color;
  graphColorThree: Color;
  graphColorFour: Color;
  graphColorFive: Color;
  graphColorSix: Color;
};

export type ThemedTypography = {
  displayTitle: TextStyle;
  displaySmall: TextStyle;
  pageTitle: TextStyle;
  panelTitle: TextStyle;
  baseText: TextStyle;
  baseTextSemiBold: TextStyle;
  smallText: TextStyle;
  smallTextSemiBold: TextStyle;
  baseTextUpperCase: TextStyle;
  smallTextUpperCase: TextStyle;
};

export type ThemedHeader = {
  logo: string;
  logoAltText?: string;
  titleStyles?: TextStyle;
  title?: string;
  titleColor?: Color;
  hideTitle?: boolean;
  titleSeparator?: string;
};

export type ThemedImages = {
  somethingWentWrong?: string;
  somethingWentWrongColor?: string;
  uploadFail?: string;
  uploadSuccess?: string;
  welcomeBackground?: string;
  greenCheckIntake?: string;
  fnolRequestAcknowledged?: string;
  timeout?: string;
  dataConcurrency?: string;
};
