/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { Color, TextStyle, ThemedColorSchema, ThemeModel } from './theme.model';
import { IS_RUNTIME_THEME_CONFIGURATION_ENABLED } from './theme.context';

const BLACK_COLOR = '#000000';
const WHITE_COLOR = '#ffffff';

export const colorVariation = new Map<
  keyof ThemedColorSchema,
  Map<string, (color: string) => string>
>();

type Rgb = [number, number, number];

export function hexToRgb(hexColor: string): Rgb {
  return [
    parseInt(hexColor.substr(1, 2), 16),
    parseInt(hexColor.substr(3, 2), 16),
    parseInt(hexColor.substr(5, 2), 16)
  ];
}

function rgbToHex(rgbColor: Rgb): string {
  return `#${Math.round(rgbColor[0])
    .toString(16)
    .padStart(2, '0')}${Math.round(rgbColor[1])
    .toString(16)
    .padStart(2, '0')}${Math.round(rgbColor[2])
    .toString(16)
    .padStart(2, '0')}`;
}

// convert hex color to hsl
// taken from less.js
export function hexToHSL(hexColor: string): [number, number, number] {
  const rgbColor = hexToRgb(hexColor);
  const r = rgbColor[0] / 255;
  const g = rgbColor[1] / 255;
  const b = rgbColor[2] / 255;
  const max = Math.max(r, g, b);
  const min = Math.min(r, g, b);
  let h: number;
  let s: number;
  const l = (max + min) / 2;
  const d = max - min;

  if (max === min) {
    h = s = 0;
  } else {
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
    }
    h! /= 6;
  }
  return [h! * 360, s, l];
}

// emulate scss mix function
// algorithm taken from https://github.com/sass/libsass/blob/master/src/fn_colors.cpp
// the difference that current implementation doesn't support alpha channel
const mix = (hexColor1: string, hexColor2: string, weight: number) => {
  const rgbColor1 = hexToRgb(hexColor1);
  const rgbColor2 = hexToRgb(hexColor2);

  const w = 2 * weight - 1;
  const w1 = (w + 1) / 2;
  const w2 = 1 - w1;

  const rgb: Rgb = [
    w1 * rgbColor1[0] + w2 * rgbColor2[0],
    w1 * rgbColor1[1] + w2 * rgbColor2[1],
    w1 * rgbColor1[2] + w2 * rgbColor2[2]
  ];

  return rgbToHex(rgb);
};

export const toPureColor = (colorOrVar: string): string => {
  const [, color] = /(#[0-9a-fA-F]{6})/.exec(colorOrVar)!;
  return color;
};

type InitialColor = typeof BLACK_COLOR | typeof WHITE_COLOR;

type BuildTransformer = {
  (
    initialColor: InitialColor,
    factor: number,
    defaultColor: keyof ThemedColorSchema
  ): (colorScheme: ThemedColorSchema) => string;

  (initialColor: InitialColor, factor: number): (
    colorScheme: ThemedColorSchema,
    color: keyof ThemedColorSchema
  ) => string;
};

const buildTransformer: BuildTransformer = (((
  initialColor: InitialColor,
  factor: number,
  defaultColor?: keyof ThemedColorSchema
) => {
  const unscopedTransformer = (
    colorScheme: ThemedColorSchema,
    color: keyof ThemedColorSchema
  ) => {
    const currentColor = colorScheme[color]!;
    const actualColor = mix(initialColor, toPureColor(currentColor), factor);
    const isUsingInternetExplorer =
      window.MSInputMethodContext && (document as any).documentMode;

    if (!IS_RUNTIME_THEME_CONFIGURATION_ENABLED || isUsingInternetExplorer) {
      return actualColor;
    }

    const newColor = `--theme-color-${color}-${defaultColor ?? ''}-${
      initialColor === BLACK_COLOR ? 'black' : 'white'
    }-${(factor * 100).toFixed()}`;
    if (!colorVariation.has(color)) {
      colorVariation.set(color, new Map());
    }
    colorVariation
      .get(color)!
      .set(newColor, (configuredColor: string) =>
        mix(initialColor, toPureColor(configuredColor), factor)
      );

    return `var(${newColor}, ${actualColor})`;
  };

  return defaultColor
    ? (colorScheme: ThemedColorSchema) =>
        unscopedTransformer(colorScheme, defaultColor)
    : unscopedTransformer;
}) as unknown) as BuildTransformer;

export const primaryDarkColor = buildTransformer(
  BLACK_COLOR,
  0.25,
  'primaryColor'
);

export const primaryDark2Color = buildTransformer(
  BLACK_COLOR,
  0.45,
  'primaryColor'
);

export const primaryActionDarkColor = buildTransformer(
  BLACK_COLOR,
  0.25,
  'primaryAction'
);

export const primaryActionDark2Color = buildTransformer(
  BLACK_COLOR,
  0.45,
  'primaryAction'
);

export const primaryLightColor = buildTransformer(
  WHITE_COLOR,
  0.5,
  'primaryColor'
);

export const primaryLight2Color = buildTransformer(
  WHITE_COLOR,
  0.9,
  'primaryColor'
);

export const pendingLightColor = buildTransformer(
  WHITE_COLOR,
  0.7,
  'pendingColor'
);

export const callToActionDarkColor = buildTransformer(
  BLACK_COLOR,
  0.25,
  'callToActionColor'
);

export const callToActionDark2Color = buildTransformer(
  BLACK_COLOR,
  0.45,
  'callToActionColor'
);

export const lightenColor = buildTransformer(WHITE_COLOR, 0.75);
export const darkenColor = buildTransformer(BLACK_COLOR, 0.25);

export const contrastTextColor = (theme: ThemeModel, color: Color): Color => {
  const [, , lightness] = hexToHSL(color);
  return lightness > 0.5
    ? theme.colorSchema.neutral9
    : theme.colorSchema.neutral1;
};

const BROWSER_ROOT_FONT_SIZE = 16;

// remove leading zeros which return from regular fixed
const toOptionalFixed = (num: number, fractionDigits: number) => {
  const numAsStr = num.toFixed(fractionDigits);
  return numAsStr.replace(/(?:\.0{3}|0{1,2})$/, '');
};

export const pxToRem = (pixels: number) =>
  `${toOptionalFixed(pixels / BROWSER_ROOT_FONT_SIZE, 3)}rem`;

type TextStyleToCssOptions = {
  suppressLineHeight: boolean;
};

/**
 * Convert TextStyle to object which can be directly passed to React style
 *
 * @param {TextStyle} textStyles
 * @param {TextStyleToCssOptions} options
 *
 *
 * @return object
 */
export const textStyleToCss = (
  textStyles: TextStyle,
  options: TextStyleToCssOptions = {
    suppressLineHeight: false
  }
): object => {
  const styles: any = {};

  if (textStyles.fontFamily) {
    styles.fontFamily = textStyles.fontFamily;
  }

  if (textStyles.fontWeight) {
    styles.fontWeight = textStyles.fontWeight;
  }

  if (textStyles.fontStyle) {
    styles.fontStyle = textStyles.fontStyle;
  }

  if (textStyles.fontSize) {
    styles.fontSize = pxToRem(textStyles.fontSize);
  }

  if (textStyles.lineHeight && !options.suppressLineHeight) {
    styles.lineHeight = pxToRem(textStyles.lineHeight);
  }

  if (textStyles.textTransform) {
    styles.textTransform = textStyles.textTransform;
  }

  if (IS_RUNTIME_THEME_CONFIGURATION_ENABLED) {
    styles.$CUSTOM_FONT_STYLE = textStyles.name;
  }

  return styles;
};

const applyAlphaToColor = (color: Color, opacity: number) => {
  const [red, green, blue] = hexToRgb(color);
  return `rgba(${red}, ${green}, ${blue}, ${opacity})`;
};

export const toRgba = (
  colorSchema: ThemedColorSchema,
  colorName: keyof ThemedColorSchema,
  opacity: number
) => {
  const actualColor = applyAlphaToColor(
    toPureColor(colorSchema[colorName]!),
    opacity
  );
  const isUsingInternetExplorer =
    window.MSInputMethodContext && (document as any).documentMode;

  if (!IS_RUNTIME_THEME_CONFIGURATION_ENABLED || isUsingInternetExplorer) {
    return actualColor;
  }

  const newColor = `--theme-color-${colorName}-opacity-${(
    opacity * 100
  ).toFixed()}`;

  if (!colorVariation.has(colorName)) {
    colorVariation.set(colorName, new Map());
  }
  colorVariation
    .get(colorName)!
    .set(newColor, (configuredColor: string) =>
      applyAlphaToColor(configuredColor, opacity)
    );

  return `var(${newColor}, ${actualColor})`;
};
