/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import classNames from 'classnames';
import { Select as AntSelect } from 'antd';
import { InputProps as AntInputProps } from 'antd/lib/input';
import { useIntl } from 'react-intl';

import { createThemedStyles, textStyleToCss, toRgba } from '../theme';
import { FormGroupContext, getFocusedElementShadow } from '../../utils';
import { useRawFormattedMessage } from '../formatted-message';
import { DefaultInputProps } from '../../types';

import styles from './select.module.scss';

type SimpleOnChange = (val: string | number) => void;

type SelectProps<Value> = {
  /**
   * Optional attribute to determine if field should be focused after component mounts
   * Defaults to false
   */
  isAutoFocusEnabled?: boolean;
  /**
   * Optional attribute to determine size of select input, defaults to "default"
   */
  size?: 'small' | 'middle' | 'large';
  /**
   * Indicate if options for dropdown is loading
   */
  isLoading?: boolean;
  /**
   * Children which would be rendered next to select in dropdown wrapper
   */
  children?: React.ReactElement;
  /**
   * Optional props, determines if search options are enabled
   */
  isSearchable?: boolean;
  /**
   * Optional prop, determines if field is invalid
   */
  isInvalid?: boolean;
  /**
   * Enables virtual options rendering in Select, disabled by default
   */
  isVirtual?: boolean;
  /**
   * Optional prop, determines className of dropdown
   */
  dropdownClassName?: string;
  /**
   * Optional prop, determines behaviour during onChange event
   * defaults to false
   */
  isInsideForm?: boolean;
  /**
   * Optional prop, determine whether the dropdown menu and the select input are the same width.
   * defaults to true
   */
  dropdownMatchSelectWidth?: boolean;
} & (
  | {
      /**
       * List of complex option elements displayed inside dropdown
       */
      optionElements: ComplexOptionElement<Value>[];
      /**
       * For complex options required idResolver which would return id of the complex value
       */
      idResolver: (value: Value) => number | string;

      onChange?: (val: Value) => void;
    }
  | {
      /**
       * List of simple option elements displayed inside dropdown
       */
      optionElements: SimpleOptionElement[];

      onChange?: SimpleOnChange;
    }
);

/**
 * Typing for "Option" element of dropdown input
 */
type BaseOptionElement = {
  title: string | number | React.ReactElement;
  isDisabled?: boolean;
  'data-test-el'?: string;
};

type ComplexOptionElement<Value> = {
  value: Value;
} & BaseOptionElement;

type SimpleOptionElement = {
  value: string | number;
} & BaseOptionElement;

const useThemedStyles = createThemedStyles(theme => ({
  dropdownInput: {
    '& .ant-select-single': {
      ...textStyleToCss(theme.typography.baseText),
      '& .ant-select-selector': {
        background: theme.colorSchema.neutral1,
        borderColor: theme.colorSchema.neutral5,
        '& .ant-select-selection-placeholder': {
          color: theme.colorSchema.neutral6
        },
        '&:hover': {
          borderColor: theme.colorSchema.primaryAction
        },
        '& .ant-select-selection-item': {
          color: theme.colorSchema.neutral8
        }
      }
    },
    '& .ant-select-arrow': {
      color: theme.colorSchema.neutral8
    },
    '& .ant-select-focused.ant-select-single:not(.ant-select-customize-input) .ant-select-selector': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  dropdown: {
    background: theme.colorSchema.neutral1,
    '& .ant-select-item-option-selected:not(.ant-select-item-option-disabled)': {
      ...textStyleToCss(theme.typography.baseTextSemiBold),
      backgroundColor: toRgba(theme.colorSchema, 'primaryAction', 0.1)
    },
    '& .ant-select-item-option': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText),
      '&:hover': {
        background: theme.colorSchema.neutral2
      }
    },
    '& .ant-select-item-option-active': {
      borderColor: theme.colorSchema.primaryAction
    }
  }
}));

export const Select = <Value extends any>({
  optionElements,
  id,
  className,
  dropdownClassName,
  placeholder,
  isDisabled,
  value,
  name,
  isSearchable = false,
  isInvalid,
  isLoading,
  isVirtual,
  isAutoFocusEnabled = false,
  isInsideForm = false,
  onChange,
  onBlur,
  dropdownMatchSelectWidth = true,
  children,
  ...props
}: SelectProps<Value> &
  DefaultInputProps &
  Omit<AntInputProps, 'placeholder'>) => {
  const { isRequired } = useContext(FormGroupContext);
  const options = optionElements as any[];
  const inputPlaceholder = useRawFormattedMessage(placeholder);
  let idResolver: ((value: any) => number | string) | null = null;
  let targetProps = props;

  if ('idResolver' in targetProps) {
    const {
      idResolver: skippedIdResolver,
      ...propsWithoutIdPropName
    } = targetProps;
    idResolver = targetProps.idResolver;
    targetProps = propsWithoutIdPropName;
  }

  const intl = useIntl();
  const themedStyles = useThemedStyles();

  const getAriaLabel = (label: string | number | React.ReactElement) => {
    if (typeof label === 'string' || typeof label === 'number') {
      return label;
    }
    return (
      label.props.id && intl.formatMessage(label.props, label.props.values)
    );
  };

  return (
    <div
      className={classNames(styles.selectWrapper, themedStyles.dropdownInput)}
    >
      <AntSelect
        className={className}
        getPopupContainer={(node: HTMLElement | undefined) => {
          if (node) {
            return node.parentNode as HTMLElement;
          }
          return document.body as HTMLElement;
        }}
        id={id}
        disabled={isDisabled}
        autoFocus={isAutoFocusEnabled}
        value={value as any}
        data-test-input-name={name}
        placeholder={inputPlaceholder}
        showSearch={isSearchable}
        data-test-el={props['data-test-el'] || 'select'}
        dropdownClassName={classNames(themedStyles.dropdown, dropdownClassName)}
        onChange={(changedValue: unknown) => {
          if (isInsideForm && onChange) {
            (onChange as SimpleOnChange)(changedValue as number | string);
            return;
          }
          if (!isInsideForm && onChange) {
            if (typeof idResolver === 'function') {
              const selectedOption = options.find(
                option => idResolver!(option.value) === changedValue
              );
              onChange(selectedOption && selectedOption.value);
            } else {
              (onChange as SimpleOnChange)(changedValue as string | number);
            }
          }
        }}
        onBlur={(e: any) => {
          if (onBlur) {
            onBlur(e);
          }
        }}
        aria-invalid={isInvalid ? 'true' : undefined}
        aria-describedby={props['aria-describedby']}
        aria-required={isRequired}
        loading={isLoading}
        virtual={isVirtual}
        autoComplete={`${name}-off`}
        dropdownMatchSelectWidth={dropdownMatchSelectWidth}
        {...targetProps}
      >
        {options.map(option => {
          const actualId = idResolver ? idResolver(option.value) : option.value;
          const optionProps = {
            value: actualId,
            disabled: option.isDisabled || false,
            'data-test-el':
              option['data-test-el'] ||
              `${actualId}-option` ||
              'dropdown-input-option',
            role: 'option',
            'aria-label': getAriaLabel(option.title)
          };

          return (
            <AntSelect.Option {...optionProps} key={actualId}>
              {option.title}
            </AntSelect.Option>
          );
        })}
      </AntSelect>
      {children}
    </div>
  );
};
