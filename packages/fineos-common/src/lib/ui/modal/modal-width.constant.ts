export enum ModalWidth {
  MEDIUM = 520,
  WIDE = 750,
  EXTRA_WIDE = 950
}
