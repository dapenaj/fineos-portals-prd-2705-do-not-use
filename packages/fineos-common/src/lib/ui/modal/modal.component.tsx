/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React, { TransitionEvent, useEffect, useRef, useState } from 'react';
import { useDialogState, Dialog, DialogBackdrop } from 'reakit';
import { useIntl } from 'react-intl';
import classNames from 'classnames';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { ButtonType } from '../../types';
import { getFocusedElementShadow } from '../../utils';
import { FormattedMessage } from '../formatted-message';
import { Icon } from '../icon';
import { createThemedStyles, textStyleToCss } from '../theme';
import { Button } from '../button';

import { ModalWidth } from './modal-width.constant';
import styles from './modal.module.scss';

type ModalProps = {
  /**
   * React element for modal header.
   */
  header: React.ReactNode;
  /**
   * State to control modal visibility.
   */
  visible: boolean;
  /**
   * Modal aria-label, required by a11y.
   */
  ariaLabelId: string;
  /**
   * Optional modal width. Defaults to "520px".
   */
  width?: number;
  /**
   * Optional classname applied to the modal.
   */
  dialogClassName?: string;
  /**
   * Optional on cancel callback.
   */
  onCancel?: (param?: React.MouseEvent<HTMLElement>) => void;
  /**
   * Optional flag to allow modal to be closed.
   */
  closable?: boolean;
  /**
   * Optional flag to add mask to modal background.
   */
  mask?: boolean;
  /**
   * Optional test element name. Defaults to "modal".
   */
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  dialog: {
    ...textStyleToCss(theme.typography.baseText),
    background: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral3,
    '&:focus': {
      color: theme.colorSchema.neutral7,
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  dialogHeader: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8,
    background: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral3
  },
  dialogBody: {
    background: theme.colorSchema.neutral1
  },
  dialogCloseIcon: {
    color: theme.colorSchema.neutral7,
    fontSize: theme.typography.baseText.fontSize,
    '&:focus': {
      color: theme.colorSchema.neutral7,
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  }
}));

export const Modal: React.FC<ModalProps> = ({
  header,
  width = ModalWidth.MEDIUM,
  visible,
  children,
  onCancel,
  ariaLabelId,
  dialogClassName,
  closable = true,
  mask = true,
  'data-test-el': dataTestEl = 'modal',
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const modalState = useDialogState({ animated: true });
  const intl = useIntl();
  const dialogRef = useRef<HTMLDivElement>(null);
  const backdropRef = useRef<HTMLDivElement>(null);
  const [localVisible, setLocalVisible] = useState(false);
  const [mouseTarget, setMouseTarget] = useState<EventTarget>();

  useEffect(() => {
    if (visible) {
      modalState.show();
      setLocalVisible(true);
    } else {
      modalState.hide();
    }
  }, [visible]);

  const onTransitionEnd = (event: TransitionEvent) => {
    // escape if not dialog transition
    if (event.target !== dialogRef.current) {
      return;
    }

    if (localVisible !== visible) {
      setLocalVisible(visible);
    }
  };

  return (
    <DialogBackdrop
      ref={backdropRef}
      onTransitionEnd={onTransitionEnd}
      tabIndex={visible ? 0 : -1}
      className={classNames(styles.dialogBackdrop, {
        [styles.dialogBackdropTransparent]: !mask
      })}
      // this is to avoid closing modal on releasing mouse click outside of the modal
      onMouseDown={(event: React.MouseEvent) => {
        setMouseTarget(event.target);
      }}
      onMouseUp={() => {
        if (mouseTarget === backdropRef.current && onCancel) {
          onCancel();
        }
      }}
      {...modalState}
    >
      <Dialog
        hideOnClickOutside={false}
        hideOnEsc={closable}
        ref={dialogRef}
        onClick={(event: React.MouseEvent) => event.stopPropagation()}
        aria-label={intl.formatMessage({
          id: ariaLabelId
        })}
        onKeyDown={(event: React.KeyboardEvent) => {
          if (event.key === 'Escape' && onCancel) {
            onCancel();
          }
        }}
        role="dialog"
        tabIndex={0}
        className={classNames(
          styles.dialog,
          themedStyles.dialog,
          dialogClassName,
          styles[`dialog${width}`]
        )}
        {...modalState}
        {...props}
      >
        {localVisible ? (
          <div data-test-el={dataTestEl}>
            <div
              className={classNames(
                styles.dialogHeader,
                themedStyles.dialogHeader
              )}
            >
              {header}
              {closable && (
                <Button
                  aria-label={intl.formatMessage({
                    id: 'FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL'
                  })}
                  data-test-el="modal-close-icon-button"
                  buttonType={ButtonType.LINK}
                  className={classNames(
                    styles.dialogCloseIcon,
                    themedStyles.dialogCloseIcon
                  )}
                  onClick={onCancel}
                >
                  <Icon
                    icon={faTimes}
                    iconLabel={
                      <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL" />
                    }
                  />
                </Button>
              )}
            </div>
            <div
              className={classNames(styles.dialogBody, themedStyles.dialogBody)}
            >
              {children}
            </div>
          </div>
        ) : null}
      </Dialog>
    </DialogBackdrop>
  );
};
