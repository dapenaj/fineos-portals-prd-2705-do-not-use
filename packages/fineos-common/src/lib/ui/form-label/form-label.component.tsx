/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { createThemedStyles, textStyleToCss } from '../theme';
import { FormattedMessage, useRawFormattedMessage } from '../formatted-message';
import { Tooltip, TooltipPlacementType } from '../tooltip';

import styles from './form-label.module.scss';

/**
 * TODO The title is removed from icon, because in this case screen reader can't read tooltip content in FF and Safari.
 * It is recommended to check in the future if this issue still occurs and move back the title is possible.
 */

export type FormLabelProps = {
  /**
   * Determines if "required icon" is shown next to label text. Defaults to false
   */
  isRequired?: boolean;
  /**
   * Determines if "optional label" is shown next to label text. Defaults to false
   */
  isOptional?: boolean;
  /**
   * Determines content of tooltip attached to form label
   */
  tooltipContent?: React.ReactElement | string;
  /**
   * Optional classname applied to label
   */
  className?: string;
  /**
   * Optional flag which make label looks smaller, useful for fields which are not
   * treated as main
   */
  isSmallLabel?: boolean;
  /**
   * Optional test element name. Defaults to "form-label"
   */
  'data-test-el'?: string;
  /**
   * Optional property for popover placement. Defaults to "bottom
   */
  popoverPlacement?: TooltipPlacementType;
};

const useThemedStyles = createThemedStyles(theme => ({
  labelWrapper: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  smallText: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  requiredMarker: {
    color: theme.colorSchema.primaryColor
  },
  optionalLabel: {
    color: theme.colorSchema.neutral6
  },
  questionIcon: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  }
}));

export const FormLabel: React.FC<FormLabelProps> = ({
  children,
  isRequired = false,
  isOptional = false,
  tooltipContent,
  className,
  isSmallLabel,
  popoverPlacement = 'bottom',
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const iconTitle = useRawFormattedMessage(
    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.QUESTION_MARK_ICON_LABEL" />
  );
  return (
    <span
      data-test-el={props['data-test-el'] || 'form-label'}
      className={classNames(
        {
          [styles.labelWrapper]: !isSmallLabel,
          [themedStyles.labelWrapper]: !isSmallLabel,
          [themedStyles.smallText]: isSmallLabel
        },
        className
      )}
    >
      {children}
      {isOptional && (
        <>
          {' '}
          <span
            className={classNames(
              styles.optionalLabel,
              themedStyles.optionalLabel
            )}
          >
            <FormattedMessage id="FINEOS_COMMON.GENERAL.OPTIONAL_LABEL" />
          </span>
        </>
      )}
      {isRequired && (
        <span
          className={classNames(styles.isRequired, themedStyles.requiredMarker)}
        >
          *
        </span>
      )}
      {tooltipContent && (
        <Tooltip tooltipContent={tooltipContent} placement={popoverPlacement}>
          <FontAwesomeIcon
            onClick={(e: React.MouseEvent) => {
              e.preventDefault();
            }}
            icon={faQuestionCircle}
            data-test-el={
              `${props['data-test-el']}-question-circle` || 'question-circle'
            }
            className={classNames(
              styles.questionIcon,
              themedStyles.questionIcon
            )}
            aria-label={iconTitle}
          />
        </Tooltip>
      )}
    </span>
  );
};
