/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classnames from 'classnames';

import { createThemedStyles, textStyleToCss } from '../theme';
import {
  PossiblyFormattedMessageElement,
  useRawFormattedMessage
} from '../formatted-message';

import styles from './panel.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    color: theme.colorSchema.neutral9,
    ...textStyleToCss(theme.typography.baseText),
    backgroundColor: theme.colorSchema.neutral1
  }
}));

type PanelProps = {
  /**
   * Optional react element for panel header.
   */
  header?: React.ReactNode;
  /**
   * Optional classname applied to the panel.
   */
  className?: string;
  /**
   * Optional test element name. Defaults to "panel".
   */
  'data-test-el'?: string;
  /**
   * Optional, sets tab index for panel
   */
  tabIndex?: number;
};

export const Panel: React.FC<PanelProps & {
  role?: JSX.IntrinsicElements['div']['role'];
  'aria-label'?: PossiblyFormattedMessageElement;
}> = ({ children, className, header, tabIndex, ...props }) => {
  const translatedAriaLabel = useRawFormattedMessage(props['aria-label']);
  const themedStyles = useThemedStyles();
  return (
    <div
      className={classnames(styles.wrapper, themedStyles.wrapper, className)}
      {...props}
      aria-label={translatedAriaLabel}
      data-test-el={props['data-test-el'] || 'panel'}
      {...(tabIndex && { tabIndex: tabIndex })}
    >
      {header}
      {children}
    </div>
  );
};
