import React, { useLayoutEffect } from 'react';
import { Form as FormikForm, FormikFormProps, useFormikContext } from 'formik';

import { AutocompleteContext } from '../../utils';

/**
 * Wrapper component for 'Form' from 'formik' library.
 * Allows to add additional preselected props to 'Form' i.e. 'autoComplete'.
 */
export const Form: React.FC<FormikFormProps> = ({ children, ...props }) => {
  const formikBag = useFormikContext();
  useLayoutEffect(() => {
    const inputToFocus: HTMLElement | null = document.querySelector(
      '[aria-invalid=true]'
    ) as HTMLElement;
    if (inputToFocus) {
      window.scroll(0, inputToFocus.offsetTop);
      if (inputToFocus.nodeName !== 'INPUT') {
        // Check if we still need it after upgrading to React 17.
        const combobox: HTMLElement | null = inputToFocus.querySelector(
          '[role="combobox"]'
        );
        if (combobox) {
          const event = document.createEvent('Event');
          event.initEvent('focus', true, false);
          combobox.dispatchEvent(event);
          combobox.focus();
          return;
        }
      }
      inputToFocus.focus();
    }
  }, [formikBag.submitCount]);

  const shouldStopSubmitForm = (e: React.KeyboardEvent) =>
    e.key === 'Enter' && !(e.target instanceof HTMLButtonElement);

  return (
    <AutocompleteContext.Provider value={{ autocomplete: 'off' }}>
      <FormikForm
        onKeyPress={(e: React.KeyboardEvent) => {
          // prevent to submit the form if the focus is not on the submit type button
          if (shouldStopSubmitForm(e)) {
            e.preventDefault();
          }
        }}
        autoComplete="off"
        {...props}
      >
        {children}
      </FormikForm>
    </AutocompleteContext.Provider>
  );
};
