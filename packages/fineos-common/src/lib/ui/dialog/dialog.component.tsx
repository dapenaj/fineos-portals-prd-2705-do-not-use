/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React, { TransitionEvent, useEffect, useRef, useState } from 'react';
import { DialogBackdrop, Dialog as ReakitDialog, useDialogState } from 'reakit';
import { useIntl } from 'react-intl';

import styles from './dialog.module.scss';

type DialogProps = {
  /**
   * State to control modal visibility.
   */
  visible: boolean;
  /**
   * Modal aria-label, required by a11y.
   */
  ariaLabelId: string;
  /**
   * Optional classname applied to the modal.
   */
  dialogClassName?: string;
  /**
   * Optional on cancel callback.
   */
  onCancel?: () => void;
  /**
   * Optional test element name. Defaults to "dialog".
   */
  'data-test-el'?: string;
} & JSX.IntrinsicElements['div'];

export const Dialog: React.FC<DialogProps> = ({
  visible,
  children,
  ariaLabelId,
  dialogClassName,
  onCancel,
  'data-test-el': dataTestEl = 'dialog',
  ...props
}) => {
  const dialogState = useDialogState({ animated: true });
  const backdropRef = useRef<HTMLDivElement>(null);
  const [localVisible, setLocalVisible] = useState(false);
  const [mouseTarget, setMouseTarget] = useState<EventTarget>();
  const intl = useIntl();

  useEffect(() => {
    if (visible) {
      dialogState.show();
      setLocalVisible(true);
    } else {
      dialogState.hide();
    }
  }, [visible]);

  const onTransitionEnd = (event: TransitionEvent) => {
    // escape if not dialog transition
    if (event.target !== backdropRef.current) {
      return;
    }

    if (localVisible !== visible) {
      setLocalVisible(visible);
    }
  };

  return (
    <DialogBackdrop
      ref={backdropRef}
      onTransitionEnd={onTransitionEnd}
      tabIndex={visible ? 0 : -1}
      className={styles.dialogBackdrop}
      // this is to avoid closing dialog on releasing mouse click outside of the modal
      onMouseDown={(event: React.MouseEvent) => {
        setMouseTarget(event.target);
      }}
      onMouseUp={() => {
        if (mouseTarget === backdropRef.current) {
          onCancel && onCancel();
        }
      }}
      {...dialogState}
    >
      <ReakitDialog
        className={dialogClassName}
        {...dialogState}
        {...props}
        aria-label={intl.formatMessage({
          id: ariaLabelId
        })}
      >
        {children}
      </ReakitDialog>
    </DialogBackdrop>
  );
};
