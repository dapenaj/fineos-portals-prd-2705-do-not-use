import { isEqual, zip } from 'lodash';

export class MapWithCompoundKeys<K, V> implements Map<K, V> {
  readonly [Symbol.toStringTag] = 'MapWithCompoundKeys';

  get size(): number {
    return this.compoundKeys.length;
  }

  private readonly compoundKeys: K[] = [];
  private readonly actualValues: V[] = [];

  [Symbol.iterator](): IterableIterator<[K, V]> {
    return this.entries();
  }

  clear(): void {
    this.compoundKeys.length = 0;
    this.actualValues.length = 0;
  }

  delete(key: K): boolean {
    const compoundKeyIndex = this.getCompoundKeyIndex(key);

    if (compoundKeyIndex >= 0) {
      this.compoundKeys.splice(compoundKeyIndex, 1);
      this.actualValues.splice(compoundKeyIndex, 1);
      return true;
    }

    return false;
  }

  entries(): IterableIterator<[K, V]> {
    const pairs = zip(this.compoundKeys, this.actualValues) as [K, V][];
    return pairs.values();
  }

  forEach(
    callbackfn: (value: V, key: K, map: Map<K, V>) => void,
    thisArg?: any
  ): void {
    this.compoundKeys.forEach((key, index) => {
      callbackfn.call(thisArg, this.actualValues[index], key, this);
    });
  }

  get(key: K): V | undefined {
    const compoundKeyIndex = this.getCompoundKeyIndex(key);
    return this.actualValues[compoundKeyIndex];
  }

  has(key: K): boolean {
    return this.getCompoundKeyIndex(key) >= 0;
  }

  keys(): IterableIterator<K> {
    return this.compoundKeys.values();
  }

  set(key: K, value: V): this {
    const compoundKeyIndex = this.getCompoundKeyIndex(key);

    if (compoundKeyIndex < 0) {
      this.compoundKeys.push(key);
      this.actualValues.push(value);
    } else {
      this.actualValues[compoundKeyIndex] = value;
    }

    return this;
  }

  values(): IterableIterator<V> {
    return this.actualValues.values();
  }

  private getCompoundKeyIndex(key: K): number {
    return this.compoundKeys.findIndex(compoundKey =>
      isEqual(compoundKey, key)
    );
  }
}
