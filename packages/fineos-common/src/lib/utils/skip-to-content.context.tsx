/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';

type SkipToContentType = {
  isSkipping: boolean;
  setIsSkipping: (value: boolean) => void;
};

const SkipToContentContext = React.createContext<SkipToContentType>({
  isSkipping: false,
  setIsSkipping: () => {}
});

/**
 * Context provider that implements SkipToContentContext in order to expose the provider in a clearer way
 *
 * @returns React.Node
 */
export function SkipToContentProvider({
  children
}: {
  children: React.ReactNode;
}) {
  const [isSkipping, setIsSkipping] = React.useState<boolean>(false);

  return (
    <SkipToContentContext.Provider value={{ isSkipping, setIsSkipping }}>
      {children}
    </SkipToContentContext.Provider>
  );
}

/**
 * Hook to expose:
 * - isSkipping:    Flag that controls if the context is in a skipping state,
 *                  so the user has selected to go to the main content through
 *                  a button or link that implements this context.
 *
 * - setIsSkipping: Method that changes the isSkipping flag. Normally called by
 *                  the button or link that tries to change the context state.
 *
 * @returns SkipToContentType
 */
export function useSkipToContent(): SkipToContentType {
  const context = React.useContext(SkipToContentContext);

  if (!context) {
    console.error(
      'useSkipToContent must be used within a SkipToContentProvider'
    );

    return { isSkipping: false, setIsSkipping: () => {} } as SkipToContentType;
  }

  return context;
}
