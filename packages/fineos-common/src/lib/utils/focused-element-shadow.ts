/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { hexToRgb, ThemeModel } from '../ui/theme';

const HEX_LENGTH = 7;
const HEX_START_SYMBOL = '#';

export const getFocusedElementShadow = ({
  isWide,
  color
}: {
  isWide?: boolean;
  color: string;
}) => {
  let parsedColor = color;
  if (color.length !== HEX_LENGTH && !color.startsWith(HEX_START_SYMBOL)) {
    const hexSymbolIndex = color.split('').indexOf(HEX_START_SYMBOL);
    parsedColor = color
      .split('')
      .slice(hexSymbolIndex, hexSymbolIndex + HEX_LENGTH)
      .join('');
  }
  return `0 0 0 ${isWide ? 4 : 2}px rgba(${[...hexToRgb(parsedColor)]}, 0.4)`;
};

export const getPrimaryButtonShadow = (theme: ThemeModel) =>
  `inset 0 0 0 2px rgb(${[...hexToRgb(theme.colorSchema.neutral1)]}),
  ${getFocusedElementShadow({ color: theme.colorSchema.primaryAction })}`;
