/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import sanitizeHtml from 'sanitize-html';

const FILTER_ATTRIBUTES = [
  'height',
  'result',
  'width',
  'x',
  'y',
  'type',
  'tableValues',
  'slope',
  'intercept',
  'amplitude',
  'exponent',
  'offset'
];
const TRANSFER_FUNC_ATTRIBUTES = [
  'type',
  'tableValues',
  'slope',
  'intercept',
  'amplitude',
  'exponent',
  'offset'
];
const PRESENTATION_ATTRIBUTES = [
  'alignment-baseline',
  'baseline-shift',
  'clip',
  'clip-path',
  'clip-rule',
  'color',
  'color-interpolation',
  'color-interpolation-filters',
  'color-profile',
  'color-rendering',
  'cursor',
  'direction',
  'display',
  'dominant-baseline',
  'enable-background',
  'fill',
  'fill-opacity',
  'fill-rule',
  'filter',
  'flood-color',
  'flood-opacity',
  'font-family',
  'font-size',
  'font-size-adjust',
  'font-stretch',
  'font-style',
  'font-variant',
  'font-weight',
  'glyph-orientation-horizontal',
  'glyph-orientation-vertical',
  'image-rendering',
  'kerning',
  'letter-spacing',
  'lighting-color',
  'marker-end',
  'marker-mid',
  'marker-start',
  'mask',
  'opacity',
  'overflow',
  'pointer-events',
  'shape-rendering',
  'solid-color',
  'solid-opacity',
  'stop-color',
  'stop-opacity',
  'stroke',
  'stroke-dasharray',
  'stroke-dashoffset',
  'stroke-linecap',
  'stroke-linejoin',
  'stroke-miterlimit',
  'stroke-opacity',
  'stroke-width',
  'text-anchor',
  'text-decoration',
  'text-rendering',
  'transform',
  'unicode-bidi',
  'vector-effect',
  'visibility',
  'word-spacing',
  'writing-mode'
];
const ARIA_ATTRIBUTES = [
  'aria-activedescendant',
  'aria-atomic',
  'aria-autocomplete',
  'aria-busy',
  'aria-checked',
  'aria-colcount',
  'aria-colindex',
  'aria-colspan',
  'aria-controls',
  'aria-current',
  'aria-describedby',
  'aria-details',
  'aria-disabled',
  'aria-dropeffect',
  'aria-errormessage',
  'aria-expanded',
  'aria-flowto',
  'aria-grabbed',
  'aria-haspopup',
  'aria-hidden',
  'aria-invalid',
  'aria-keyshortcuts',
  'aria-label',
  'aria-labelledby',
  'aria-level',
  'aria-live',
  'aria-modal',
  'aria-multiline',
  'aria-multiselectable',
  'aria-orientation',
  'aria-owns',
  'aria-placeholder',
  'aria-posinset',
  'aria-pressed',
  'aria-readonly',
  'aria-relevant',
  'aria-required',
  'aria-roledescription',
  'aria-rowcount',
  'aria-rowindex',
  'aria-rowspan',
  'aria-selected',
  'aria-setsize',
  'aria-sort',
  'aria-valuemax',
  'aria-valuemin',
  'aria-valuenow',
  'aria-valuetext',
  'role'
];
const CONDITIONAL_ATTRIBUTES = [
  'externalResourcesRequired',
  'requiredExtensions',
  'requiredFeatures',
  'systemLanguage'
];
const ANIMATION_ATTRIBUTES = [
  'attributeName',
  'additive',
  'accumulate',
  'calcMode',
  'values',
  'keyTimes',
  'keySplines',
  'from',
  'to',
  'by',
  'autoReverse',
  'accelerate',
  'decelerate',
  'begin',
  'dur',
  'end',
  'min',
  'max',
  'restart',
  'repeatCount',
  'repeatDur',
  'fill'
];

const svgSanitizeOptions = {
  allowedTags: [
    'animate',
    'animateMotion',
    'animateTransform',
    'circle',
    'clipPath',
    'defs',
    'desc,',
    'ellipse,',
    'feBlend',
    'feColorMatrix',
    'feComponentTransfer',
    'feComposite',
    'feConvolveMatrix',
    'feDiffuseLighting',
    'feDisplacementMap',
    'feDistantLight',
    'feFlood',
    'feFuncA',
    'feFuncB',
    'feFuncG',
    'feFuncR',
    'feGaussianBlur',
    'feMerge',
    'feMergeNode',
    'feMorphology',
    'feOffset',
    'fePointLight',
    'feSpecularLighting',
    'feSpotLight',
    'feTile',
    'feTurbulence',
    'filter',
    'g',
    'line',
    'linearGradient',
    'marker',
    'mask',
    'mpath',
    'path',
    'pattern',
    'polygon',
    'polyline',
    'radialGradient',
    'rect',
    'set',
    'stop',
    'svg',
    'switch',
    'symbol',
    'text',
    'textPath',
    'title',
    'tspan',
    'use',
    'view'
  ],

  allowedAttributes: {
    '*': ['id', 'lang', 'xml:base', 'xml:lang'],
    animate: [...ANIMATION_ATTRIBUTES],
    animateMotion: ['keyPoints', 'path', 'rotate', ...ANIMATION_ATTRIBUTES],
    animateTransform: [
      'type',
      ...ANIMATION_ATTRIBUTES,
      ...CONDITIONAL_ATTRIBUTES
    ],
    circle: [
      'cx',
      'cy',
      'r',
      'pathLength',
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES,
      ...CONDITIONAL_ATTRIBUTES
    ],
    clipPath: [
      'clipPathUnits',
      ...PRESENTATION_ATTRIBUTES,
      ...CONDITIONAL_ATTRIBUTES
    ],
    defs: [...PRESENTATION_ATTRIBUTES],
    desc: [],
    ellipse: [
      'cx',
      'cy',
      'rx',
      'ry',
      'pathLength',
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES,
      ...CONDITIONAL_ATTRIBUTES
    ],
    feBlend: [
      'in',
      'in2',
      'mode',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feColorMatrix: [
      'in',
      'type',
      'values',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feComponentTransfer: [
      'in',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feComposite: [
      'in',
      'in2',
      'operator',
      'k1',
      'k2',
      'k3',
      'k4',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feConvolveMatrix: [
      'in',
      'order',
      'kernelMatrix',
      'divisor',
      'bias',
      'targetX',
      'targetY',
      'edgeMode',
      'kernelUnitLength',
      'preserveAlpha',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feDiffuseLighting: [
      'in',
      'surfaceScale',
      'diffuseConstant',
      'kernelUnitLength',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feDisplacementMap: [
      'in',
      'in2',
      'scale',
      'xChannelSelector',
      'yChannelSelector',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feDistantLight: ['azimuth', 'elevation'],
    feFlood: [
      'flood-color',
      'flood-opacity',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feFuncA: [...TRANSFER_FUNC_ATTRIBUTES],
    feFuncB: [...TRANSFER_FUNC_ATTRIBUTES],
    feFuncG: [...TRANSFER_FUNC_ATTRIBUTES],
    feFuncR: [...TRANSFER_FUNC_ATTRIBUTES],
    feGaussianBlur: [
      'in',
      'stdDeviation',
      'edgeMode',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feMerge: [...PRESENTATION_ATTRIBUTES, ...FILTER_ATTRIBUTES],
    feMergeNode: ['in'],
    feMorphology: [
      'in',
      'operator',
      'radius',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feOffset: [
      'in',
      'dx',
      'dy',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    fePointLight: ['x', 'y', 'z'],
    feSpecularLighting: [
      'in',
      'surfaceScale',
      'specularConstant',
      'specularExponent',
      'kernelUnitLength',
      ...PRESENTATION_ATTRIBUTES,
      ...FILTER_ATTRIBUTES
    ],
    feSpotLight: [
      'x',
      'y',
      'z',
      'pointsAtX',
      'pointsAtY',
      'pointsAtZ',
      'specularExponent',
      'limitingConeAngle'
    ],
    feTile: ['in', ...PRESENTATION_ATTRIBUTES, ...FILTER_ATTRIBUTES],
    feTurbulence: [
      'baseFrequency',
      'numOctaves',
      'seed',
      'stitchTiles',
      'type'
    ],
    filter: [
      'x',
      'y',
      'width',
      'height',
      'filterRes',
      'filterUnits',
      'primitiveUnits'
    ],
    g: [
      ...CONDITIONAL_ATTRIBUTES,
      ...ARIA_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES
    ],
    line: [
      'x1',
      'x2',
      'y1',
      'y2',
      'pathLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...ARIA_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES
    ],
    linearGradient: [
      'gradientUnits',
      'gradientTransform',
      'href',
      'spreadMethod',
      'x1',
      'x2',
      'y1',
      'y2',
      ...PRESENTATION_ATTRIBUTES
    ],
    marker: [
      'markerHeight',
      'markerUnits',
      'markerWidth',
      'orient',
      'preserveAspectRatio',
      'refX',
      'refY',
      'viewBox',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    mask: [
      'height',
      'maskContentUnits',
      'maskUnits',
      'x',
      'y',
      'width',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES
    ],
    mpath: [...CONDITIONAL_ATTRIBUTES, 'href', 'xlink:href'],
    path: [
      'd',
      'pathLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    pattern: [
      'height',
      'href',
      'patternContentUnits',
      'patternTransform',
      'patternUnits',
      'preserveAspectRatio',
      'viewBox',
      'width',
      'x',
      'y',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES
    ],
    polygon: [
      'points',
      'pathLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    polyline: [
      'points',
      'pathLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    radialGradient: [
      'cx',
      'cy',
      'fr',
      'fx',
      'fy',
      'gradientUnits',
      'gradientTransform',
      'href',
      'r',
      'spreadMethod',
      'xlink:href',
      ...PRESENTATION_ATTRIBUTES
    ],
    rect: [
      'x',
      'y',
      'rx',
      'ry',
      'width',
      'height',
      'pathLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    set: ['to', ...ANIMATION_ATTRIBUTES],
    stop: ['offset', 'stop-color', 'stop-opacity', ...PRESENTATION_ATTRIBUTES],
    svg: [
      'height',
      'preserveAspectRatio',
      'viewBox',
      'width',
      'x',
      'y',
      ...CONDITIONAL_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    switch: [],
    symbol: [...CONDITIONAL_ATTRIBUTES, ...PRESENTATION_ATTRIBUTES],
    text: [
      'x',
      'y',
      'dx',
      'dy',
      'rotate',
      'lengthAdjust',
      'textLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    textPath: [
      'href',
      'lengthAdjust',
      'method',
      'path',
      'side',
      'spacing',
      'startOffset',
      'textLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    title: [],
    tspan: [
      'x',
      'y',
      'dx',
      'dy',
      'rotate',
      'lengthAdjust',
      'textLength',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    use: [
      'href',
      'x',
      'y',
      'width',
      'height',
      ...CONDITIONAL_ATTRIBUTES,
      ...PRESENTATION_ATTRIBUTES,
      ...ARIA_ATTRIBUTES
    ],
    view: [
      'viewBox',
      'preserveAspectRatio',
      'zoomAndPan',
      'viewTarget',
      'externalResourcesRequired',
      ...ARIA_ATTRIBUTES
    ]
  },
  parser: {
    xmlMode: true
  }
};

export const sanitizeSvg = (unsafeSvg: string) =>
  sanitizeHtml(unsafeSvg, svgSanitizeOptions);
