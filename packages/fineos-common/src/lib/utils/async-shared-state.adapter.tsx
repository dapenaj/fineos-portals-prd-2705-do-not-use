import React from 'react';
import {
  RecoilRoot,
  atomFamily as RecoilAtomFamily,
  SerializableParam as RecoilSerializableParam,
  atom as RecoilAtom,
  selector as RecoilSelector,
  useRecoilState,
  useRecoilValue
} from 'recoil';

export const atom = RecoilAtom;
export const atomFamily = RecoilAtomFamily;
export const selector = RecoilSelector;
export const useSharedState = useRecoilState;
export const useSharedValue = useRecoilValue;

export type SerializableParam = RecoilSerializableParam;
type SharedStateProps = {
  children: React.ReactNode;
};

export const SharedState: React.FC<SharedStateProps> = ({ children }) => {
  return <RecoilRoot>{children}</RecoilRoot>;
};
