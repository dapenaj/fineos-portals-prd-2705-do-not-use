/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { saveAs } from 'file-saver';

const b64toBlob = (b64Data: string, contentType: string): Blob => {
  // for performance reason it's better to create blob in chunks
  const CHUNK_SIZE = 512;

  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += CHUNK_SIZE) {
    const slice = byteCharacters.slice(offset, offset + CHUNK_SIZE);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  return new Blob(byteArrays, { type: contentType });
};

export type Base64File = {
  fileName: string;
  data: string;
  contentType: string;
};

export const saveBase64File = ({ fileName, data, contentType }: Base64File) =>
  saveAs(b64toBlob(data, contentType), fileName);

export type CsvDetails = {
  columns: string[];
  rows: any[][];
  filename: string;
};

export const saveCsvFile = ({ columns, rows, filename }: CsvDetails) => {
  const rowDelimiter = '"\r\n"';
  const colDelimiter = '","';
  const csvType = 'data:application/csv;charset=utf-8,';
  const csvTypeIE = 'text/csv;charset=utf-8;';

  // create csv table based on CsvDetails
  let csv = '"';
  // create table headers
  csv += columns.join(colDelimiter);
  // create table rows
  rows.forEach(row => {
    csv += rowDelimiter;
    csv += row.join(colDelimiter);
  });
  csv += '"';
  // add csv extension to filename
  filename = `${filename}.csv`;

  // IE 10+
  if (window.navigator.msSaveBlob) {
    const blob = new Blob([decodeURIComponent(encodeURI(csv))], {
      type: csvTypeIE
    });

    window.navigator.msSaveBlob(blob, filename);
  } else {
    const csvData = `${csvType}${encodeURIComponent(csv)}`;
    const link = document.createElement('a');
    link.href = csvData;
    link.setAttribute('visibility', 'hidden');
    link.download = filename;

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
};
