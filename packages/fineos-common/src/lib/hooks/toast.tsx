/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';
import { message } from 'antd';
import { RawIntlProvider, useIntl } from 'react-intl';

import { ToastType } from '../types';
import { createThemedStyles, textStyleToCss } from '../ui/theme';

const useThemedStyles = createThemedStyles(theme => ({
  toast: {
    '& .ant-message-notice-content': {
      background: theme.colorSchema.neutral1,
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  }
}));

/**
 * Create customizable toast notification
 */
export const useToast = () => {
  const intl = useIntl();
  const getCloneWithRole = (content: React.ReactElement) =>
    React.cloneElement(content, { role: 'status' });
  const themedStyles = useThemedStyles();
  return {
    show: (type: ToastType, content: React.ReactElement, duration?: number) => {
      message[type]({
        content: (
          <RawIntlProvider value={intl}>
            {getCloneWithRole(content)}
          </RawIntlProvider>
        ),
        className: themedStyles.toast,
        duration
      });
    }
  };
};
