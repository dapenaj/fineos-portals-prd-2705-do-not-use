/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useEffect, useContext, useMemo, useCallback } from 'react';

import {
  ApiError,
  AsyncError,
  atomFamily,
  MapWithCompoundKeys,
  SerializableParam,
  useSharedState as useAsyncSharedState
} from '../../utils';

import { PartialApiCompleteError } from './partial-api-complete-error';

type AsyncFn<Value, Params extends any[]> = (
  ...params: Params
) => Promise<Value>;

type ErrorChecker = (error: Error | ApiError) => boolean;

type NoGlobalErrorHandlingFor = number | number[] | ErrorChecker;

export enum UseAsyncCacheStrategy {
  // use as a cache when in runtime several requests exists
  RUNTIME = 'runtime',
  // try to use data from the local cache (RecoilRoot) if possible
  LOCAL = 'local-root'
}

type AsyncOptions<Value, Params extends any[]> = {
  /**
   * Allow conditionally to skip invoking async flow depending on passed params
   * @param params
   */
  shouldExecute?: (...params: Params) => boolean;
  /**
   * Default value is the one which would be returned when no value has been received
   * in result of async operation
   */
  defaultValue?: Value;
  /**
   * Allow suppressing global error handling
   */
  noGlobalErrorHandlingFor?: NoGlobalErrorHandlingFor;

  /**
   * Allow customizing cache strategy, by default it's UseAsyncCacheStrategy.RUNTIME
   */
  cacheStrategy?: UseAsyncCacheStrategy;
};

type AsyncState<Value, Params extends any[]> = Readonly<{
  params: Params | null;
  value: Value;
  error: Readonly<Error | ApiError> | null;
  status: 'IDLE' | 'PENDING' | 'SUCCESS' | 'FAIL';
}>;

type ResultAsyncState<Value, Params extends any[]> = AsyncState<
  Value,
  Params
> & {
  isStale: boolean;
  isPending: boolean;
};

type UseAsync<Value, Params extends any[]> = {
  state: ResultAsyncState<Value, Params>;
  changeAsyncValue: (value: Value) => void;
  removeAsyncValue: () => void;
  executeAgain: () => void;
};

type AsyncActions<Value, Params extends any[]> =
  | { type: 'START' }
  | { type: 'SUCCESS'; payload: { value: Value; params: Params } }
  | { type: 'CHANGE'; payload: { value: Value; params: Params } }
  | { type: 'REMOVE' }
  | {
      type: 'PARTIAL_SUCCESS';
      payload: { value: PartialApiCompleteError<Value>; params: Params };
    }
  | { type: 'FAIL'; payload: { error: Error | null; params: Params } };

const defaultShouldExecute = (() => true) as any;

export type UseAsyncVariants = {
  <Value, Params extends any[]>(
    asyncFunction: AsyncFn<Value, Params>,
    params: Params,
    options?: Omit<AsyncOptions<Value, Params>, 'defaultValue'>
  ): UseAsync<Value | null, Params>;

  <Value, Params extends any[]>(
    asyncFunction: AsyncFn<Value, Params>,
    params: Params,
    options: Required<Pick<AsyncOptions<Value, Params>, 'defaultValue'>> &
      Omit<AsyncOptions<Value, Params>, 'defaultValue'>
  ): UseAsync<Value, Params>;
};

type RecoilAtomFamilyParam = Readonly<{
  asyncFunction: number;
  defaultState: Omit<AsyncState<unknown, any[]>, 'value' | 'params'> & {
    readonly value: Readonly<AsyncState<unknown, any[]>['value']>;
    readonly params: Readonly<AsyncState<unknown, any[]>['params']>;
  };
}>;

const asyncAtomFamily = atomFamily<
  AsyncState<any, any[]>,
  RecoilAtomFamilyParam
>({
  key: 'useAsyncHookSharedState',
  default: ({ defaultState }: any) => defaultState,
  dangerouslyAllowMutability: true // while we are using moment.js we cannot allow immutable structure to be used
});

const shouldHandleErrorGlobalMapper = (
  notGlobalErrorHandlingFor?: NoGlobalErrorHandlingFor
): ErrorChecker => {
  if (typeof notGlobalErrorHandlingFor === 'number') {
    return (error: Error | ApiError) => {
      if ('status' in error) {
        return notGlobalErrorHandlingFor !== error.status;
      }
      return true;
    };
  } else if (Array.isArray(notGlobalErrorHandlingFor)) {
    return (error: Error | ApiError) => {
      if ('status' in error) {
        return !notGlobalErrorHandlingFor.includes(error.status);
      }
      return true;
    };
  } else if (typeof notGlobalErrorHandlingFor === 'function') {
    return (error: Error | ApiError) => !notGlobalErrorHandlingFor(error);
  } else {
    return () => true;
  }
};

const uniqValueMap = new MapWithCompoundKeys<
  unknown[],
  RecoilAtomFamilyParam
>();

let monotonicId = 1;

const toUniqSerializedValue = (
  fun: (...params: unknown[]) => Promise<unknown>,
  params: unknown[],
  defaultValue: unknown
): RecoilAtomFamilyParam => {
  const uniqValueKey = [fun, ...params];
  if (!uniqValueMap.has(uniqValueKey)) {
    const uniqValue = {
      asyncFunction: monotonicId++,
      defaultState: {
        params: null,
        value: (defaultValue as unknown) as SerializableParam,
        error: null,
        status: 'IDLE'
      } as Readonly<AsyncState<unknown, any[]>>
    };
    uniqValueMap.set(uniqValueKey, uniqValue as RecoilAtomFamilyParam);

    return uniqValue as RecoilAtomFamilyParam;
  }

  return uniqValueMap.get(uniqValueKey)!;
};

type AsyncOpKey = [(...params: unknown[]) => Promise<unknown>, unknown[]];

type RuntimeTracking = {
  counter: number;
  version: number;
};

let versionCounter = 1;
const runtimeAsyncOp = new MapWithCompoundKeys<AsyncOpKey, RuntimeTracking>();

const getAsyncOpTrackingCounter = (...key: AsyncOpKey) => {
  const runtimeTracking = runtimeAsyncOp.get(key);
  return runtimeTracking ? runtimeTracking.counter : 0;
};

const incAsyncOpTrackingCounter = (...key: AsyncOpKey) => {
  let runtimeTracking = runtimeAsyncOp.get(key);

  if (!runtimeTracking) {
    runtimeTracking = {
      counter: 0,
      version: versionCounter++
    };
    runtimeAsyncOp.set(key, runtimeTracking);
  }

  runtimeTracking.counter += 1;

  return runtimeTracking;
};

// we need to pass version, so we wouldn't decrease this value for new fork of counter
const decAsyncOpTrackingCounter = (
  version: RuntimeTracking['version'],
  ...key: AsyncOpKey
) => {
  const runtimeTracking = runtimeAsyncOp.get(key);

  if (runtimeTracking && runtimeTracking.version === version) {
    runtimeTracking.counter -= 1;
  }
};

const removeAsyncOpTrackingCounter = (...key: AsyncOpKey) => {
  runtimeAsyncOp.delete(key);
};

/**
 * Perform async operation
 *
 * It would store each state per unique combination of asyncFunction and params in shared state of Recoil.js
 */
export const useAsync: UseAsyncVariants = <Value, Params extends any[]>(
  asyncFunction: AsyncFn<Value, Params>,
  params: Params,
  options: AsyncOptions<Value, Params> = {}
): UseAsync<Value, Params> => {
  const asyncError = useContext(AsyncError);
  const cacheStrategy = options.cacheStrategy ?? UseAsyncCacheStrategy.RUNTIME;
  const defaultValue = (options.defaultValue ?? null) as Value;
  const recoilId = toUniqSerializedValue(
    asyncFunction as (...params: unknown[]) => Promise<unknown>,
    params,
    defaultValue
  );

  const [state, setState] = (useAsyncSharedState(
    asyncAtomFamily(recoilId)
  ) as unknown) as [
    AsyncState<Value, Params>,
    (
      update: (s: AsyncState<Value, Params>) => AsyncState<Value, Params>
    ) => void
  ];
  const dispatch = useCallback(
    (action: AsyncActions<Value, Params>) =>
      setState((prevState: AsyncState<Value, Params>) => {
        switch (action.type) {
          case 'REMOVE':
            return {
              params: null,
              value: defaultValue,
              error: null,
              status: 'IDLE'
            };
          case 'START':
            return {
              ...prevState,
              error: null,
              status: 'PENDING'
            };
          case 'SUCCESS':
            return {
              ...prevState,
              value: action.payload.value,
              params: action.payload.params,
              status: 'SUCCESS'
            };
          case 'PARTIAL_SUCCESS':
            return {
              value: action.payload.value.partialResult,
              error: action.payload.value.apiError,
              params: action.payload.params,
              status: 'SUCCESS'
            };
          case 'CHANGE':
            return {
              ...prevState,
              value: action.payload.value,
              status: 'SUCCESS'
            };
          case 'FAIL':
            return {
              ...prevState,
              error: action.payload.error,
              params: action.payload.params,
              status: 'FAIL'
            };
        }
      }),
    [setState, defaultValue]
  );

  const shouldExecute = (options.shouldExecute ?? defaultShouldExecute) as (
    ...params: Params
  ) => boolean;

  const shouldHandleErrorGlobal = useMemo(
    () => shouldHandleErrorGlobalMapper(options.noGlobalErrorHandlingFor),
    [options.noGlobalErrorHandlingFor]
  );

  const changeAsyncValue = (value: Value) =>
    dispatch({ type: 'CHANGE', payload: { value, params } });

  const removeAsyncValue = () => {
    // as we are removing value we shouldn't wait for it anymore
    removeAsyncOpTrackingCounter(
      asyncFunction as (...params: unknown[]) => Promise<unknown>,
      params
    );
    dispatch({ type: 'REMOVE' });
  };

  const sendAsyncRequest = () => {
    dispatch({ type: 'START' });

    asyncFunction(...params)
      .then(response =>
        dispatch({ type: 'SUCCESS', payload: { value: response, params } })
      )
      .catch(maybePartialApiCompleteError => {
        if (maybePartialApiCompleteError instanceof PartialApiCompleteError) {
          if (
            asyncError &&
            shouldHandleErrorGlobal(maybePartialApiCompleteError.apiError)
          ) {
            asyncError.throwError(maybePartialApiCompleteError.apiError);
          }

          dispatch({
            type: 'PARTIAL_SUCCESS',
            payload: {
              value: maybePartialApiCompleteError,
              params
            }
          });
          return;
        }

        return Promise.reject(maybePartialApiCompleteError);
      })
      .catch(error => {
        if (asyncError && shouldHandleErrorGlobal(error)) {
          asyncError.throwError(error);
        }

        dispatch({ type: 'FAIL', payload: { error, params } });
      });
  };

  const executeAgain = useCallback(sendAsyncRequest, params);

  const currentAsyncOpExecAmount = getAsyncOpTrackingCounter(
    asyncFunction as (...params: unknown[]) => Promise<unknown>,
    params
  );

  useEffect(() => {
    const asyncOpExecAmount = getAsyncOpTrackingCounter(
      asyncFunction as (...params: unknown[]) => Promise<unknown>,
      params
    );
    const noRuntimeRequests = asyncOpExecAmount === 0;
    if (shouldExecute(...params)) {
      if (cacheStrategy === UseAsyncCacheStrategy.RUNTIME) {
        if (noRuntimeRequests) {
          sendAsyncRequest();
        }
      } else if (cacheStrategy === UseAsyncCacheStrategy.LOCAL) {
        if (
          (state.status === 'IDLE' || state.status === 'FAIL') &&
          noRuntimeRequests
        ) {
          sendAsyncRequest();
        }
      }
    }
    // eslint-disable-next-line
  }, [params]);

  useEffect(() => {
    const { version } = incAsyncOpTrackingCounter(
      asyncFunction as (...params: unknown[]) => Promise<unknown>,
      params
    );
    return () => {
      decAsyncOpTrackingCounter(
        version,
        asyncFunction as (...params: unknown[]) => Promise<unknown>,
        params
      );
    };
    // eslint-disable-next-line
  }, [asyncFunction, ...params]);

  return {
    state: {
      ...state,
      isPending: state.status === 'PENDING',
      isStale:
        state.status !== 'SUCCESS' ||
        (currentAsyncOpExecAmount === 0 && shouldExecute(...params))
    },
    changeAsyncValue,
    removeAsyncValue,
    executeAgain
  };
};
