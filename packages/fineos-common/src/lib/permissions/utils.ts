/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { ApiPermissions } from './constants';

export type PermissionCheck = ((
  permissionsSet: Set<ApiPermissions>
) => boolean) & { debugName: string };

const unwrapPermission = (permissionsSet: Set<ApiPermissions>) => {
  return (permission: ApiPermissions | PermissionCheck) => {
    if (typeof permission === 'function') {
      return permission(permissionsSet);
    } else {
      return permissionsSet.has(permission as ApiPermissions);
    }
  };
};

const formatDebugName = (
  permissions: (ApiPermissions | PermissionCheck)[]
): string => {
  return permissions
    .map(permission => {
      if (typeof permission === 'function') {
        return permission.debugName;
      } else {
        return permission;
      }
    })
    .join('; ');
};

export const formatAnyFromDebugName = (
  permissions: (ApiPermissions | PermissionCheck)[]
): string => `anyFrom(${formatDebugName(permissions)})`;

export const anyFrom = (
  ...permissions: (ApiPermissions | PermissionCheck)[]
): PermissionCheck => {
  const permissionCheck: PermissionCheck = (
    permissionsSet: Set<ApiPermissions>
  ) => {
    return permissions.some(unwrapPermission(permissionsSet));
  };

  if (process.env.NODE_ENV !== 'production') {
    permissionCheck.debugName = formatAnyFromDebugName(permissions);
  }

  return permissionCheck;
};

export const formatRequiredDebugName = (
  permissions: (ApiPermissions | PermissionCheck)[]
): string => `required(${formatDebugName(permissions)})`;

export const required = (
  ...permissions: (ApiPermissions | PermissionCheck)[]
): PermissionCheck => {
  const permissionCheck: PermissionCheck = (
    permissionsSet: Set<ApiPermissions>
  ) => {
    return permissions.every(unwrapPermission(permissionsSet));
  };

  if (process.env.NODE_ENV !== 'production') {
    permissionCheck.debugName = formatRequiredDebugName(permissions);
  }

  return permissionCheck;
};
