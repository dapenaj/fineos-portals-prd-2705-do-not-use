/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum ViewCustomerDataPermissions {
  // /groupClient/getCustomerForGroupClient
  VIEW_GROUP_CUSTOMERS_LIST = 'URL_GET_GROUPCLIENT_CUSTOMERFORGROUPCLIENT',

  // /groupClient/customers/{customerId}/readCustomerContactDetails
  VIEW_CUSTOMER_CONTACT_DETAILS = 'URL_GET_GROUPCLIENT_CUSTOMERS_READCONTACTDETAILS',

  // /groupClient/customers/{customerId}/readCustomerDetails
  VIEW_CUSTOMER_DETAILS = 'URL_GET_GROUPCLIENT_CUSTOMERS_READCUSTOMERDETAILS',

  // /groupClient/customers
  VIEW_CUSTOMERS_LIST = 'URL_GET_GROUPCLIENT_CUSTOMERS',
  // /groupClient/customers/{customerId}
  VIEW_CUSTOMER = 'URL_GET_GROUPCLIENT_CUSTOMERS_GETSINGLE',

  // /groupClient/customers/{customerId}/email-addresses
  VIEW_CUSTOMER_EMAIL_ADDRESSES = 'URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES',

  // /groupClient/customers/{customerId}/email-addresses/{emailId}
  // URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_GETSINGLE

  // /groupClient/customers/{customerId}/phone-numbers
  VIEW_CUSTOMER_PHONE_NUMBERS = 'URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS',

  // /groupClient/customers/{customerId}/phone-numbers/{phoneNumberId}
  // URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_GETSINGLE

  // /groupClient/customers/{customerId}/communication-preferences
  VIEW_CUSTOMER_COMMUNICATION_PREFERENCES = 'URL_GET_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES',

  // /groupClient/customers/{customerId}/communication-preferences/{communicationPreferenceId}
  VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE = 'URL_GET_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE',

  // /groupClient/customers/{customerId}/customer-info
  VIEW_CUSTOMER_INFO = 'URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMERINFO'
}
