/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum ManageCustomerDataPermissions {
  // /groupClient/customers/{customerId}/updateCustomerContactDetails
  UPDATE_CUSTOMER_CONTACT_DETAILS = 'URL_POST_GROUPCLIENT_CUSTOMERS_CONTACTDETAILS_UPDATE',

  // /groupClient/customers/{customerId}/updateCustomerDetails
  UPDATE_CUSTOMER_DETAILS = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMERDETAILS_UPDATE',

  // /groupClient/customers/{customerId}/email-addresses/{emailId}/edit
  UPDATE_CUSTOMER_EMAILS = 'URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_EDIT',

  // /groupClient/customers/{customerId}/email-addresses
  ADD_CUSTOMER_EMAILS = 'URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD',

  // /groupClient/customers/{customerId}/email-addresses/{emailId}/remove
  REMOVE_CUSTOMER_EMAILS = 'URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_REMOVE',

  // /groupClient/customers/{customerId}/phone-numbers/{phoneNumberId}/edit
  UPDATE_CUSTOMER_PHONE_NUMBERS = 'URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_EDIT',

  // /groupClient/customers/{customerId}/phone-numbers
  ADD_CUSTOMER_PHONE_NUMBERS = 'URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD',

  // /groupClient/customers/{customerId}/phone-numbers/{phoneNumberId}/remove
  REMOVE_CUSTOMER_PHONE_NUMBERS = 'URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_REMOVE',

  // /groupClient/customers/{customerId}/communication-preferences/{communicationPreferenceId}/link
  UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES = 'URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK',

  // /groupClient/customers/{customerId}/customer-info/edit
  UPDATE_CUSTOMER_INFORMATION = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMERINFO_EDIT'
}
