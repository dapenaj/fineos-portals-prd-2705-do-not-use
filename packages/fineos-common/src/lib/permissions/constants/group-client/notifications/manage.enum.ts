/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum ManageNotificationPermissions {
  // /groupClient/cases/{caseId}/eforms/updateEForm/{eformId}
  UPDATE_CASES_EFORM = 'URL_POST_GROUPCLIENT_CASES_EFORM_UPDATE',

  // /groupClient/cases/{caseId}/documents/{documentId}/markread
  MARK_READ_CASE_DOCUMENT = 'URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD',

  // /groupClient/cases/{caseId}/documents/upload/{documentType}
  UPLOAD_CASE_DOCUMENT = 'URL_POST_GROUPCLIENT_CASES_DOCUMENT_UPLOAD',

  // /groupClient/cases/{caseId}/addEForm/{eformType}
  ADD_CASE_EFORM = 'URL_POST_GROUPCLIENT_CASES_EFORM_ADD',

  // /groupClient/cases/{caseId}/documents/base64Upload/{documentType}
  UPLOAD_CASE_BASE64_DOCUMENT = 'URL_POST_GROUPCLIENT_CASES_DOCUMENT_BASE64UPLOAD',

  // /groupClient/eforms/{eFormType}
  ADD_EFORM = 'URL_POST_GROUPCLIENT_EFORMS_ADD',

  // /groupClient/absences/{employeeId}/leave-plans/{leavePlanId}/calculate-availability
  LEAVEPLANS_CALCULATE_AVAILABILITY = 'URL_POST_GROUPCLIENT_LEAVEPLANS_CALCULATEAVAILABILITY',

  // /groupClient/cases/{caseId}/outstanding-information-received
  ADD_CASES_OUTSTANDING_INFORMATION_RECEIVED = 'URL_POST_GROUPCLIENT_CASES_OUTSTANDINGINFORMATIONRECEIVED_ADD',

  // /groupClient/absence/absence-cases/{absenceCaseId}/leave-periods-change-request
  ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST = 'URL_POST_GROUPCLIENT_ABSENCE_LEAVEPERIODSCHANGEREQUEST_ADD'

  // Not covered permissions (but not required for EP ver.1)
  //
  // /groupClient/cases/{caseId}/managedRequirements/{managedReqId}/linkDocument/{docId}
  // URL_POST_GROUPCLIENT_CASES_MANAGEDREQ_LINKDOCUMENT
}
