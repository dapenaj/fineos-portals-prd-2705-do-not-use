/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum ViewNotificationPermissions {
  // /groupClient/claims
  VIEW_CLAIMS_OVERVIEW = 'URL_GET_GROUPCLIENT_CLAIMS',

  // /groupClient/claims/{claimId}/benefits/{benefitId}/readDisabilityBenefit
  VIEW_CLAIMS_READ_DISABILITY_BENEFIT = 'URL_GET_GROUPCLIENT_CLAIMS_DISABILITYBENEFIT_GETSINGLE',

  // /groupClient/claims/{claimId}/readDisabilityDetails
  VIEW_CLAIMS_READ_DISABILITY_DETAILS = 'URL_GET_GROUPCLIENT_CLAIMS_DISABILITYDETAILS',

  // /groupClient/claims/{claimId}/benefits
  VIEW_CLAIMS_BENEFITS_LIST = 'URL_GET_GROUPCLIENT_CLAIMS_BENEFITS',

  // /groupClient/cases/{caseId}/documents
  VIEW_CASES_DOCUMENTS_LIST = 'URL_GET_GROUPCLIENT_CASES_DOCUMENTS',

  // /groupClient/cases/{caseId}/documents/{documentId}/base64Download
  VIEW_CASES_DOWNLOAD_DOCUMENT = 'URL_GET_GROUPCLIENT_CASES_DOCUMENTS_BASE64DOWNLOAD',

  // /groupClient/cases/{caseId}/eforms
  VIEW_CASES_EFORM_LIST = 'URL_GET_GROUPCLIENT_CASES_EFORMS',

  // /groupClient/cases/{caseId}/eforms/{eformId}/readEform
  VIEW_CASES_EFORM_DETAILS = 'URL_GET_GROUPCLIENT_CASES_READEFORM',

  // /groupClient/absence/absence-period-decisions
  VIEW_ABSENCE_PERIOD_DECISIONS = 'URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS',

  // /groupClient/absence/accommodation-cases/{accommodationCaseId}
  VIEW_ACCOMMODATION_DETAILS = 'URL_GET_GROUPCLIENT_ABSENCE_ACCOMMODATIONCASES_GETSINGLE',

  // /groupClient/cases/{caseId}/outstanding-information
  VIEW_CASES_OUTSTANDING_ITEMS_LIST = 'URL_GET_GROUPCLIENT_CASES_OUTSTANDINGINFORMATION',

  // /groupClient/notifications
  VIEW_NOTIFICATIONS = 'URL_GET_GROUPCLIENT_NOTIFICATIONS',

  // ​/groupClient​/notifications​/{notificationId}
  VIEW_NOTIFICATION_DETAILS = 'URL_GET_GROUPCLIENT_NOTIFICATIONS_GETSINGLE',

  // /groupClient/claims/{claimId}/benefits/{benefitId}/readLumpSumBenefit
  VIEW_CLAIMS_LUMP_SUM_BENEFIT = 'URL_GET_GROUPCLIENT_CLAIMS_READLUMPSUMBENEFIT',

  // /groupClient/cases/{caseId}/participants
  VIEW_CASES_PARTICIPANTS_LIST = 'URL_GET_GROUPCLIENT_CASES_PARTICIPANTS',

  // /groupClient/cases/{caseId}/participants/{participantId}/readParticipantDetails
  VIEW_CASES_PARTICIPANT_DETAILS = 'URL_GET_GROUPCLIENT_CASES_READPARTICIPANTDETAILS',

  // /groupClient/cases/{caseId}/participants/{participantId}/readParticipantContactDetails
  VIEW_CASES_PARTICIPANT_CONTACT_DETAILS = 'URL_GET_GROUPCLIENT_CASES_READPARTICIPANTCONTACTDETAILS',

  // /groupClient/absences/{customerId}/leave-plans/{leavePlanId}/leave-availability`
  VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY = 'URL_GET_GROUPCLIENT_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY',

  // /groupClient/absence/absences/{absenceId}/actual-absence-periods
  VIEW_ACTUAL_ABSENCE_PERIODS = 'URL_GET_GROUPCLIENT_ACTUAL_TIME_RECORDED',

  // /groupClient/absence/events
  VIEW_ABSENCE_EVENTS = 'URL_GET_GROUPCLIENT_ABSENCE_CASE_EVENT',

  // /groupClient/absence/employees-leave-hours
  EMPLOYEES_LEAVE_HOURS = 'URL_GET_GROUPCLIENT_ABSENCE_EMPLOYEESAPPROVEDLEAVES'

  // Not covered permissions (but not required for EP ver.1)
  //
  // /groupClient/claims/notifications
  // URL_GET_GROUPCLIENT_CLAIMS_NOTIFICATIONS
  //
  // /groupClient/cases/{caseId}/caseStatusHistory
  // URL_GET_GROUPCLIENT_CASES_CASESTATUSHISTORY
  //
  // /groupClient/cases/{caseId}/managedRequirements
  // URL_GET_GROUPCLIENT_CASES_MANAGEDREQUIREMENTS
  //
  // /groupClient/cases/{caseId}/contacts
  // URL_GET_GROUPCLIENT_CASES_CONTACTS
  //
  // /groupClient/cases/{caseId}/contacts/{contactId}/documents
  // URL_GET_GROUPCLIENT_CASES_CONTACTDOCUMENTS
  //
  // /groupClient/cases/{caseId}/managedRequirements/{managedReqId}/documents
  // URL_GET_GROUPCLIENT_CASES_MANAGEDREQDOCUMENTS
  //
  // /groupClient/cases/{caseId}/managedRequirements/{managedReqId}/eforms
  // URL_GET_GROUPCLIENT_CASES_MANAGEDREQEFORMS
  //
  // /groupClient/claims/{claimId}/socialSecurityBenefit/decisions
  // URL_GET_GROUPCLIENT_CLAIMS_SOCIALSECURITYDECISIONS
  //
  // /groupClient/claims/{claimId}/socialSecurityBenefit
  // URL_GET_GROUPCLIENT_CLAIMS_SOCIALSECURITYBENEFIT
}
