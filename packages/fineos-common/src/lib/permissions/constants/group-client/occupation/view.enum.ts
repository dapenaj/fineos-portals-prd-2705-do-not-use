/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum ViewOccupationPermissions {
  // /groupClient/customers/{customerId}/customer-occupations
  VIEW_CUSTOMER_OCCUPATIONS = 'URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS',

  // /groupClient/customers/{customerId}/customer-occupations/{customerOccupationId}
  // URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_GETSINGLE

  // /groupClient/customers/{customerId}/customer-occupations/{customerOccupationId}/absence-employment
  VIEW_ABSENCE_EMPLOYMENT = 'URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT',

  // /groupClient/customers/{customerId}/customer-occupations/{CustomerOccupationId}/contractual-earnings
  VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS = 'URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS',

  // /groupClient/customers/{customerId}/customer-occupations/{CustomerOccupationId}/contractual-earnings/{contractualEarningsId}
  // URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_GETSINGLE

  // Not covered permissions (but not required for EP ver.1)
  //
  // /groupClient/claims/{claimId}/occupations
  // URL_GET_GROUPCLIENT_CLAIMS_OCCUPATIONS
  //
  // /groupClient/claims/{claimId}/otherIncomeSources
  // URL_GET_GROUPCLIENT_CLAIMS_OTHERINCOME
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/earnings
  // URL_GET_GROUPCLIENT_CLAIMS_OCCUPATIONEARNINGS

  // /groupClient/customers/{customerId}/regular-weekly-work-pattern
  VIEW_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN = 'URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN'
}
