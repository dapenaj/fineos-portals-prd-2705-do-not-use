/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum ManageOccupationPermissions {
  // /groupClient/customers/{customerId}/customer-occupations/{customerOccupationId}/edit
  VIEW_EDIT_EMPLOYMENT_DETAILS = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_EDIT',

  // /groupClient/customers/{customerId}/customer-occupations/{customerOccupationId}/absence-employment/store
  // URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_STORE

  // /groupClient/customers/{customerId}/customer-occupation/{CustomerOccupationId}/contractual-earnings
  ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_ADD',

  // /groupClient/customers/{customerId}/customer-occupations/{CustomerOccupationId}/contractual-earnings/{contractualEarningsId}/edit
  EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_EDIT',

  // /groupClient/customers/{customerId}/customer-occupations/{customerOccupationId}/absence-employment/edit/
  EDIT_ABSENCE_EMPLOYMENT = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_EDIT',

  // /groupClient/customers/{customerId}/customer-occupations/{customerOccupationId}/absence-employment/
  ADD_ABSENCE_EMPLOYMENT = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_ADD',

  // Not covered permissions (but not required for EP ver.1)
  //
  // /groupClient/claims/{claimId}/otherIncomeSources/{incomeSourceId}/removeOtherIncomeSource
  // URL_POST_GROUPCLIENT_CLAIMS_OTHERINCOME_REMOVE
  //
  // /groupClient/claims/{claimId}/otherIncomeSources/{incomeSourceId}/updateOtherIncomeSource
  // URL_POST_GROUPCLIENT_CLAIMS_OTHERINCOME_UPDATE
  //
  // /groupClient/claims/{claimId}/otherIncomeSources/addOtherIncomeSource
  // URL_POST_GROUPCLIENT_CLAIMS_OTHERINCOME_ADD
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/addWeeklyBasisPreDisabilityEarnings
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATIONWKLYPREDISEARNINGS_ADD
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/addMonthlyBasisPreDisabilityEarnings
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATIONMTHLYPREDISEARNINGS_ADD
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/updateWeeklyBasisPreDisabilityEarnings/{earningsId}
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATIONWKLYPREDISEARNINGS_UPDATE
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/removePreDisabilityEarningsRecord/{earningsId}
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATIONPREDISEARNINGS_REMOVE
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/updateMonthlyBasisPreDisabilityEarnings/{earningsId}
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATIONMTHLYPREDISEARNINGS_UPDATE
  //
  // /groupClient/claims/{claimId}/occupations/addOccupation
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATION_ADD
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/removeOccupation
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATION_REMOVE
  //
  // /groupClient/claims/{claimId}/occupations/{occupationId}/updateOccupation
  // URL_POST_GROUPCLIENT_CLAIMS_OCCUPATION_UPDATE
  //
  // /groupClient/customers/{customerId}/regular-weekly-work-pattern/edit
  EDIT_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_EDIT',
  ADD_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN = 'URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_ADD'
}
