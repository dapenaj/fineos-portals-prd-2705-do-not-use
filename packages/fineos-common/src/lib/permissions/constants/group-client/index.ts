/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { AdminPermissions } from './admin.enum';
import { MessagesPermissions } from './messages.enum';
import { ConfiguratorPermissions } from './configurator.enum';
import {
  ManageCustomerDataPermissions,
  ViewCustomerDataPermissions
} from './customer';
import {
  CreateNotificationPermissions,
  ManageNotificationPermissions,
  ViewNotificationPermissions
} from './notifications';
import {
  ManageOccupationPermissions,
  ViewOccupationPermissions
} from './occupation';
import { ManagePaymentPermissions, ViewPaymentsPermissions } from './payments';
import { EVPPermissions } from './evp/evp.enum';

export {
  AdminPermissions,
  MessagesPermissions,
  ConfiguratorPermissions,
  CreateNotificationPermissions,
  ManageCustomerDataPermissions,
  ManageNotificationPermissions,
  ManageOccupationPermissions,
  ManagePaymentPermissions,
  ViewCustomerDataPermissions,
  ViewNotificationPermissions,
  ViewOccupationPermissions,
  ViewPaymentsPermissions,
  EVPPermissions
};

export type GroupClientPermissions =
  | AdminPermissions
  | MessagesPermissions
  | ConfiguratorPermissions
  | CreateNotificationPermissions
  | ManageCustomerDataPermissions
  | ManageNotificationPermissions
  | ManageOccupationPermissions
  | ManagePaymentPermissions
  | ViewCustomerDataPermissions
  | ViewNotificationPermissions
  | ViewOccupationPermissions
  | ViewPaymentsPermissions
  | EVPPermissions;
