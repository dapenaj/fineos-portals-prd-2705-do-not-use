/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useContext } from 'react';

import { PermissionsScope } from './permissions-scope.context';
import { formatRequiredDebugName, PermissionCheck, required } from './utils';
import { ApiPermissions } from './constants';

export type PermissionRule =
  | PermissionCheck
  | ApiPermissions
  | ApiPermissions[];

export const compilePermissionRule = (rule: PermissionRule): PermissionCheck =>
  typeof rule === 'function'
    ? rule
    : Array.isArray(rule)
    ? required(...rule)
    : required(rule);

export const debugPermissionRule = (rule: PermissionRule): string =>
  typeof rule === 'function'
    ? rule.debugName
    : Array.isArray(rule)
    ? formatRequiredDebugName(rule)
    : formatRequiredDebugName([rule]);

export const usePermissions = (rule: PermissionRule) => {
  const { permissions } = useContext(PermissionsScope);
  const check = compilePermissionRule(rule);

  if (permissions) {
    return check(permissions);
  } else if (process.env.NODE_ENV !== 'production') {
    console.warn(
      'Permission hook used without PermissionsScope, permissionCheck: ' +
        check.debugName
    );
  }

  return false;
};
