import * as Yup from 'yup';

export const newEmailValidationSchema = Yup.string()
  .trim()
  .matches(
    /^[\w!#$%&'*+\/=?`{|}~^-]+(?:\.[\w!#$%&'*+\/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$/,
    'FINEOS_COMMON.VALIDATION.NOT_EMAIL'
  )
  .max(119, 'FINEOS_COMMON.VALIDATION.MAX_EMAIL_LENGTH');
