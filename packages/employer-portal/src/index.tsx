/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

// Polyfill Imports
// tslint:disable no-import-side-effect
import 'react-app-polyfill/ie9';
import 'react-app-polyfill/stable';
// tslint:disable-next-line
import './custom-polyfills/promise-allsettled.polyfill';
import './custom-polyfills/document-contains.polyfill';
import './custom-polyfills/prepend.polyfill';

// React Imports
import React from 'react';
import ReactDOM from 'react-dom';

// tslint:disable-next-line
import 'fineos-common/dist/index.css';

import './custom-polyfills/promise-allsettled.polyfill';
import App from './app/app';

// React Imports

// tslint:disable-next-line

ReactDOM.render(<App />, document.getElementById('root'));
