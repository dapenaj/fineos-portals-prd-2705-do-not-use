if (!document.contains) {
  (document as any).contains = function(el: Element) {
    while (el === el.parentNode) {
      // tslint:disable-next-line
      if (el === this) {
        return true;
      }
    }
    return false;
  };
}
