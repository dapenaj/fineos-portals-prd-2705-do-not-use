/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
  loadThemeFonts,
  Theme,
  PermissionsScope,
  AsyncHandler,
  ApiPermissions,
  enableRuntimeThemeConfiguration,
  ConfiguratorPermissions,
  ErrorPage,
  ErrorContext,
  loadThemeIcons,
  encryptedUserId,
  SkipToContentProvider
} from 'fineos-common';
import { ErrorBoundary } from 'react-error-boundary';
import { ApiConfigProps } from 'fineos-js-api-client';
import { HashRouter } from 'react-router-dom';

import { Internationalise } from './i18n/internationalise';
import { Routes } from './routes';
import {
  ClientConfigService,
  ClientConfig,
  AuthenticationService,
  PortalEnvConfigService
} from './shared';
import { overrideSdkAndApiConfigs, Config } from './config';
import { HighContrastModeContext } from './modules/high-contrast-mode/high-contrast-mode.context';
import { ThemeConfigurator } from './modules/theme-configurator/theme-configurator.component';
import { fetchPermissions } from './app.api';

const App: React.FC = () => {
  const [config, setConfig] = useState<ClientConfig | null>(null);
  const [isHighContrastOn, setIsHighContrastOn] = useState(false);
  const [isForbidden, setIsForbidden] = useState(false);
  const [isLoggedOut, setIsLoggedOut] = useState(false);
  const [permissions, setPermissions] = useState<Set<ApiPermissions> | null>(
    null
  );
  const logout = useCallback(() => {
    setPermissions(currentPermissions =>
      currentPermissions?.size ? new Set() : currentPermissions
    );
    setIsLoggedOut(true);
  }, [setPermissions]);
  const [supportedLanguage, setSupportedLanguage] = useState<string>('');
  const currentUserId = AuthenticationService.getInstance().displayUserId;

  const permissionsContextValue = useMemo(
    () => ({
      permissions,
      logout
    }),
    [permissions, logout]
  );

  useEffect(() => {
    const authenticationService = AuthenticationService.getInstance();
    const clientConfigService = ClientConfigService.getInstance();

    if (process.env.NODE_ENV !== 'production') {
      overrideSdkAndApiConfigs();
    }

    authenticationService.saveCredentials();

    async function fetchConfig() {
      try {
        const clientConfig = await clientConfigService.fetchClientConfig();
        const whenThemeUpdated = loadThemeIcons(clientConfig.theme);
        const currentConfig = {
          ...clientConfig,
          theme: await whenThemeUpdated
        };

        loadThemeFonts(clientConfig.theme, document.head, {
          display: 'swap'
        });
        const highContrastMode = currentConfig.theme.highContrastMode!;
        highContrastMode.icons = { ...currentConfig.theme.icons };
        enableRuntimeThemeConfiguration();
        setConfig(currentConfig);

        if (clientConfig.theme.header.title) {
          document.title = clientConfig.theme.header.title;
        }
      } catch (error) {
        setConfig(clientConfigService.DEFAULT_CONFIG);
      }
    }

    fetchConfig();

    fetchPermissions()
      .then(setPermissions)
      .catch(() => setPermissions(new Set()));
  }, []);

  useEffect(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    async function getLanguage() {
      const clientConfig = await portalEnvConfigService.fetchPortalEnvConfig();

      setSupportedLanguage(clientConfig.supportedLanguages[0]);
    }
    getLanguage();
  }, [supportedLanguage]);

  const errorContextValue = useMemo(
    () => ({
      isForbidden,
      setIsForbidden
    }),
    [isForbidden, setIsForbidden]
  );

  const hasViewConfiguratorPermissions = permissions?.has(
    ConfiguratorPermissions.VIEW_CONFIGURATOR
  );

  return (
    permissions &&
    config && (
      <PermissionsScope.Provider value={permissionsContextValue}>
        <Config.Provider value={config}>
          <Theme.Provider
            value={
              isHighContrastOn ? config.theme.highContrastMode! : config.theme
            }
          >
            <ErrorContext.Provider value={errorContextValue}>
              <Internationalise
                supportedLanguage={
                  localStorage.getItem(
                    encryptedUserId(currentUserId, 'locale')
                  ) || supportedLanguage
                }
                decimalSeparator={ApiConfigProps.decimalSeparator}
              >
                <ErrorBoundary FallbackComponent={ErrorPage}>
                  <SkipToContentProvider>
                    <HighContrastModeContext.Provider
                      value={{
                        isEnabled: isHighContrastOn,
                        onIsEnabledChange: (isEnabled: boolean) => {
                          setIsHighContrastOn(isEnabled);
                        }
                      }}
                    >
                      <AsyncHandler>
                        <HashRouter>
                          <Routes
                            features={config.features}
                            isLoggedOut={isLoggedOut}
                          />
                        </HashRouter>
                        {hasViewConfiguratorPermissions && (
                          <ThemeConfigurator
                            isHighContrastOn={isHighContrastOn}
                            config={config}
                            onConfigChange={(currentConfig: ClientConfig) =>
                              setConfig(currentConfig)
                            }
                          />
                        )}
                      </AsyncHandler>
                    </HighContrastModeContext.Provider>
                  </SkipToContentProvider>
                </ErrorBoundary>
              </Internationalise>
            </ErrorContext.Provider>
          </Theme.Provider>
        </Config.Provider>
      </PermissionsScope.Provider>
    )
  );
};

export default App;
