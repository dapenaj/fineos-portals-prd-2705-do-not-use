/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  AuthenticationStrategy,
  SdkConfigProps,
  ServiceFactory,
  InsecureStrategy,
  CognitoStrategy
} from 'fineos-js-api-client';
import { isNil as _isNil } from 'lodash';
import moment from 'moment';
import cookie from 'cookie';

import { AuthenticationCookieParam } from './authentication.type';

export class AuthenticationService {
  get authStrategy(): AuthenticationStrategy {
    return SdkConfigProps.authenticationStrategy;
  }

  get hasRequiredCredentials(): boolean {
    return (
      !_isNil(this.accessKeyId) &&
      !_isNil(this.expiration) &&
      !_isNil(this.secretKey) &&
      !_isNil(this.sessionToken) &&
      !_isNil(this.userId) &&
      !_isNil(this.role)
    );
  }

  get areCredentialsValid(): boolean {
    return moment(this.expiration).isAfter(moment());
  }

  get isAuthenticated(): boolean {
    return this.authStrategy === AuthenticationStrategy.NONE
      ? true
      : this.hasRequiredCredentials && this.areCredentialsValid;
  }

  private static _instance: AuthenticationService;

  displayUserId!: string;
  displayRoles!: string[];
  accessKeyId!: string;
  expiration!: string;
  secretKey!: string;
  sessionToken!: string;
  userId!: string;
  role!: string;
  serviceFactory: ServiceFactory = ServiceFactory.getInstance();

  static getInstance(): AuthenticationService {
    return (
      AuthenticationService._instance ||
      (AuthenticationService._instance = new AuthenticationService())
    );
  }

  saveCredentials() {
    this.authStrategy === AuthenticationStrategy.NONE
      ? this.saveInsecureCredentials()
      : this.saveCognitoCredentials();
  }

  saveInsecureCredentials() {
    this.serviceFactory.strategy = new InsecureStrategy(
      SdkConfigProps.debugUserId
    );

    this.displayUserId = SdkConfigProps.debugDisplayUserId;
    this.displayRoles = SdkConfigProps.debugDisplayRoles;
  }

  saveCognitoCredentials() {
    const cookieValue = cookie.parse(document.cookie);
    if (!cookieValue['portal-cookie']) {
      // in case when we have mocked portal.env.json we don't want to make login redirect, as it doesn't help
      if (!process.env.REACT_APP_PORTAL_ENV_JSON) {
        document.location.href = SdkConfigProps.loginUrl;
      }
    } else {
      const portalCookie = JSON.parse(
        cookieValue['portal-cookie'].replace(/\\"/g, '"')
      );

      this.accessKeyId = portalCookie[
        AuthenticationCookieParam.ACCESS_KEY_ID
      ] as string;
      this.expiration = portalCookie[
        AuthenticationCookieParam.EXPIRATION
      ] as string;
      this.secretKey = portalCookie[
        AuthenticationCookieParam.SECRET_KEY
      ] as string;
      this.sessionToken = portalCookie[
        AuthenticationCookieParam.SESSION_TOKEN
      ] as string;
      this.userId = portalCookie[AuthenticationCookieParam.USER_ID] as string;
      this.role = portalCookie[AuthenticationCookieParam.ROLE] as string;
      this.displayUserId = portalCookie[
        AuthenticationCookieParam.DISPLAY_USER_ID
      ] as string;

      this.serviceFactory.strategy = new CognitoStrategy(
        this.accessKeyId,
        this.expiration,
        this.secretKey,
        this.sessionToken,
        this.userId,
        this.role
      );
    }
  }
}
