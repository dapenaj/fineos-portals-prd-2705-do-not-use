/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import axios, { AxiosError } from 'axios';
import { merge } from 'lodash';

import { ClientConfig } from './client-config.type';
import DEFAULT_CONFIG from './default-config.json';

export class ClientConfigService {
  private static _instance: ClientConfigService;

  readonly DEFAULT_CONFIG = this.decorateConfig(DEFAULT_CONFIG as ClientConfig);

  static getInstance(): ClientConfigService {
    return (
      ClientConfigService._instance ||
      (ClientConfigService._instance = new ClientConfigService())
    );
  }

  async fetchClientConfig(): Promise<ClientConfig> {
    try {
      const { data } = await axios.get<ClientConfig>(
        '/config/client-config.json'
      );

      return this.decorateConfig({
        ...data,
        theme: merge(DEFAULT_CONFIG.theme, data.theme)
      });
    } catch (error) {
      const { response } = error as AxiosError;
      return Promise.reject(response && response.data);
    }
  }

  private decorateConfig(config: ClientConfig): ClientConfig {
    return {
      ...config,
      theme: {
        ...config.theme,
        typography: Object.keys(config.theme.typography).reduce(
          (acc: any, curr) => {
            acc[curr] = {
              name: curr,
              ...(config.theme.typography as any)[curr]
            };
            return acc;
          },
          {}
        )
      }
    };
  }
}
