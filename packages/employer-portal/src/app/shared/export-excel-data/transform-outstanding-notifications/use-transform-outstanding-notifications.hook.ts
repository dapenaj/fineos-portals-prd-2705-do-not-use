/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useContext } from 'react';
import { useIntl } from 'react-intl';
import moment from 'moment';
import { GroupClientNotification } from 'fineos-js-api-client';
import { Formatting } from 'fineos-common';

import { EnumDomain, getEnumDomainTranslation } from '../../enum-domain';

import { getTranslationIdForNotificationKey } from './transform-outstanding-notifications.utils';
import {
  ReceivedNotificationReason,
  MappedNotificationKey
} from './transform-outstanding-notifications.enum';
import { hasClaimTypeSubcase } from '../../utils';

export const useTransformOutstandingNotifications = (
  notifications: GroupClientNotification[],
  notificationKeys: string[]
) => {
  const intl = useIntl();
  const formatting = useContext(Formatting);
  const getTranslatedKey = (key: string) => {
    const id = getTranslationIdForNotificationKey(key);
    return intl.formatMessage({ id });
  };
  const getNotificationReason = (reason?: ReceivedNotificationReason) =>
    intl.formatMessage({
      id: getEnumDomainTranslation(
        EnumDomain.NOTIFICATION_REASON,
        reason || ReceivedNotificationReason.UNKNOWN
      )
    });

  const mappedNotifications = notifications.map(notification => ({
    [MappedNotificationKey.NAME]: `${notification.customer?.firstName} ${notification.customer?.lastName} - ${notification.customer.id}`,
    [MappedNotificationKey.JOB_TITLE]: notification.customer?.jobTitle,
    [MappedNotificationKey.GROUP]: notification.adminGroup,
    [MappedNotificationKey.RETURN_DATE]: moment(
      notification.expectedRTWDate
    ).format(formatting.date),
    [MappedNotificationKey.CASE]: notification.caseNumber,
    [MappedNotificationKey.REASON]: getNotificationReason(
      notification.notificationReason?.name as ReceivedNotificationReason
    ),
    [MappedNotificationKey.CREATED_DATE]: moment(
      notification.createdDate
    ).format(formatting.date),
    [MappedNotificationKey.NOTIFICATION_DATE]: moment(
      notification.notificationDate
    ).format(formatting.date),
    [MappedNotificationKey.DISABILITY]: intl.formatMessage({
      id: hasClaimTypeSubcase(notification)
        ? 'FINEOS_COMMON.GENERAL.YES'
        : 'FINEOS_COMMON.GENERAL.NO'
    }),
    [MappedNotificationKey.ORGANISATION_UNIT]:
      notification.customer.organisationUnit,
    [MappedNotificationKey.WORK_SITE]: notification.customer.workSite
  }));

  const notificationsWithRelevantKeys = mappedNotifications.map(notification =>
    notificationKeys.reduce(
      (total: Record<string, string>, currentKey: string) => {
        total[getTranslatedKey(currentKey)] = (notification as any)[currentKey];
        return total;
      },
      {}
    )
  );

  return notificationsWithRelevantKeys;
};
