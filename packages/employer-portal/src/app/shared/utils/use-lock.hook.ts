import { useMemo, useState } from 'react';

type ReleaseLockFn = () => Promise<void>;

export interface Lock {
  readonly isLocked: boolean;
  readonly releaseLockFn: ReleaseLockFn | null;

  lock(releaseLockFn?: ReleaseLockFn): void;
  unlock(): void;
}

const NO_RELEASE_LOCK_FN = () => Promise.resolve();

export const useLock = (): Lock => {
  const [releaseLockFn, setReleaseLockFn] = useState<ReleaseLockFn | null>(
    null
  );

  return useMemo(
    () => ({
      releaseLockFn,
      isLocked: typeof releaseLockFn === 'function',
      lock(newReleaseLockFn: ReleaseLockFn = NO_RELEASE_LOCK_FN) {
        setReleaseLockFn(() => newReleaseLockFn);
      },
      unlock() {
        setReleaseLockFn(null);
      }
    }),
    [releaseLockFn, setReleaseLockFn]
  );
};
