/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  MessagesPermissions,
  usePermissions,
  useSharedState,
  useAsync,
  atom
} from 'fineos-common';
import { CaseMessage } from 'fineos-js-api-client';

import { fetchUnreadMessagesCount } from '../../shared';

const messagesState = atom({
  key: 'messages',
  default: new Map()
});

let lastKnownValue = 0;

export const useMessageCounter = () => {
  const hasMessagesPermission = usePermissions([
    MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
    MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
    MessagesPermissions.ADD_CASE_WEB_MESSAGE,
    MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
  ]);

  const {
    state: { value: messagesCount, status },
    changeAsyncValue
  } = useAsync(fetchUnreadMessagesCount, [], {
    defaultValue: 0,
    shouldExecute: () => hasMessagesPermission
  });
  if (status === 'SUCCESS') {
    lastKnownValue = messagesCount;
  }
  const [localMessages] = useSharedState(messagesState);
  const syncMessages = (messages: CaseMessage[]) => {
    let tempCounter = 0;
    for (const message of messages) {
      const { id, readByGroupClient } = message;
      if (localMessages.has(id)) {
        const temp = localMessages.get(id);
        if (temp !== readByGroupClient) {
          if (!temp) {
            tempCounter--;
          } else {
            tempCounter++;
          }
          localMessages.set(id, readByGroupClient);
        }
      } else {
        localMessages.set(id, readByGroupClient);
      }
    }
    changeAsyncValue(messagesCount + tempCounter);
  };
  return {
    unreadMessagesCount:
      status === 'IDLE' || status === 'PENDING'
        ? lastKnownValue
        : messagesCount,
    syncMessages: syncMessages
  };
};
