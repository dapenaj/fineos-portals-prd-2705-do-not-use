/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { GroupClientPeriodDecisions } from 'fineos-js-api-client';

// It's not nice to have to check based on this hard-coded string, but there's not much choice
const LEAVE_PLAN_TYPE_NAME_INFORMATIONAL = 'Informational';

export const getFilteredAbsencePeriodDecisions = (
  absencePeriodDecisions: GroupClientPeriodDecisions
): GroupClientPeriodDecisions => ({
  startDate: absencePeriodDecisions.startDate,
  endDate: absencePeriodDecisions.endDate,
  decisions: absencePeriodDecisions.decisions.filter(
    ({ period: { leavePlan } }) => {
      // filter out all leave plans with "Informational"
      if (leavePlan && leavePlan.leavePlanType) {
        return (
          leavePlan.leavePlanType?.leavePlanName !==
          LEAVE_PLAN_TYPE_NAME_INFORMATIONAL
        );
      }
      // if LeavePlan or LeavePlanType are missing, then we can't determine if it should be removed so keep it
      return true;
    }
  )
});
