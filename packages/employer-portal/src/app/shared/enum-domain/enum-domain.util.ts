import { BaseDomain } from 'fineos-js-api-client';

import { EnumDomain } from './enum-domain.enum';

const labelToId = (str: string) =>
  str
    .toUpperCase()
    .replace(/\([^)]+\)/, '')
    .replace(/\[[^\]]+]/, '')
    .replace(/'/g, '')
    .trim()
    .replace(/[-\W]+/g, '_');

/**
 * This function convert enum or enum.name from enumDomain to correct translation id automatically
 */
export const getEnumDomainTranslation = (
  enumDomain: EnumDomain,
  enumInstanceNameOrInstance: string | BaseDomain
) => {
  const name =
    typeof enumInstanceNameOrInstance === 'string'
      ? enumInstanceNameOrInstance
      : enumInstanceNameOrInstance.name;

  const returnedTranslation =
    name === 'Unknown'
      ? 'ENUM_DOMAINS.PLEASE_SELECT'
      : `ENUM_DOMAINS.${labelToId(EnumDomain[enumDomain])}.${labelToId(name)}`;

  return returnedTranslation;
};
