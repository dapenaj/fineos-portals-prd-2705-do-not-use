/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { BaseDomain } from 'fineos-js-api-client';
import { useCallback } from 'react';
import { useAsync } from 'fineos-common';

import { fetchEnumInstances } from './enum-instance.api';
import { EnumDomain } from './enum-domain.enum';

const cachedEnumDomains = new Map<number, Promise<BaseDomain[]>>();

type UseAsyncType = typeof useAsync;

type UseEnumDomainOptions = Pick<
  Parameters<UseAsyncType>[2],
  'shouldExecute'
> & {
  shouldSkipUnknownFiltering?: boolean;
};

const UNKNOWN_NAME = 'Unknown';

export const useEnumDomain = (
  domainName: EnumDomain,
  { shouldExecute, shouldSkipUnknownFiltering }: UseEnumDomainOptions = {
    shouldExecute: () => true
  }
) => {
  const call = useCallback(() => {
    if (!cachedEnumDomains.has(domainName)) {
      cachedEnumDomains.set(domainName, fetchEnumInstances(domainName));
    }
    return cachedEnumDomains.get(domainName);
  }, [domainName]);

  const { state } = useAsync<BaseDomain[], any>(call as any, [domainName], {
    defaultValue: [],
    shouldExecute
  });

  return {
    domains: shouldSkipUnknownFiltering
      ? state.value.filter(
          (enumDomain: BaseDomain) => enumDomain.name !== UNKNOWN_NAME
        )
      : state.value,
    isLoadingDomains: state.isPending,
    loadingDomainsError: state.error
  };
};

export const resetEnumDomainCache = () => {
  cachedEnumDomains.clear();
};
