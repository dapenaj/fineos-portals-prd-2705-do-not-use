/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { FormikProps, Formik, Field } from 'formik';
import { useIntl } from 'react-intl';
import {
  FormButton,
  UiButton,
  ButtonType,
  Form,
  FormGroup,
  TextInput,
  FormattedMessage,
  Icon
} from 'fineos-common';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { TableFilters, NotificationFiltersProps } from '../../../types';
import {
  useFiltersThemedStyles,
  filterLabelMapper
} from '../table-filter.utils';
import styles from '../table-filter.module.scss';

import { getInputFilterValidationSchema } from './table-input-filter.form';

export const TableInputFilter: React.FC<NotificationFiltersProps> = ({
  filterValues,
  onSearch,
  onReset,
  onClose,
  fieldName,
  minLength = 2
}) => {
  const intl = useIntl();
  const themedStyles = useFiltersThemedStyles();
  const [isFiltered, setIsFiltered] = useState<boolean>(false);

  useEffect(() => {
    setIsFiltered(!!filterValues[fieldName as keyof TableFilters]);
  }, [filterValues, fieldName]);

  const doReset = ({ resetForm }: FormikProps<any>) => {
    setIsFiltered(false);
    resetForm({});
    onReset();
  };

  return (
    <div
      className={classNames(styles.wrapper, themedStyles.wrapper)}
      data-test-el="table-input-filter"
    >
      <Formik
        enableReinitialize={true}
        initialValues={filterValues}
        onSubmit={(values, actions) => {
          onSearch({ [fieldName]: values[fieldName] });
          actions.setSubmitting(false);
        }}
        validationSchema={getInputFilterValidationSchema(
          fieldName,
          minLength,
          intl.formatMessage(
            {
              id: 'FINEOS_COMMON.VALIDATION.MIN_LENGTH'
            },
            { minLength }
          )
        )}
      >
        {(props: FormikProps<any>) => (
          <Form>
            <UiButton
              buttonType={ButtonType.LINK}
              onClick={onClose}
              className={styles.iconWrapper}
            >
              <Icon
                icon={faTimes}
                className={themedStyles.icon}
                iconLabel={
                  <FormattedMessage id="FINEOS_COMMON.GENERAL.CLOSE" />
                }
              />
            </UiButton>
            <FormGroup
              isRequired={true}
              label={
                <FormattedMessage
                  id={`NOTIFICATIONS.FILTERS.${filterLabelMapper[fieldName]}`}
                />
              }
              name={fieldName}
              className={themedStyles.item}
            >
              <Field
                name={fieldName}
                placeholder={
                  <FormattedMessage
                    id={`NOTIFICATIONS.FILTERS.${filterLabelMapper[fieldName]}_PLACEHOLDER`}
                  />
                }
                data-test-el={`input-${fieldName}`}
                component={TextInput}
                className={themedStyles.placeholder}
              />
            </FormGroup>
            <FormattedMessage
              id="COMMON.FILTERS.SEARCH"
              as={FormButton}
              htmlType="submit"
              data-test-el="search-button"
            />
            <FormattedMessage
              disabled={!isFiltered}
              id="FINEOS_COMMON.GENERAL.RESET"
              as={FormButton}
              htmlType="reset"
              onClick={() => doReset(props)}
              buttonType={ButtonType.LINK}
              data-test-el="reset-button"
            />
          </Form>
        )}
      </Formik>
    </div>
  );
};
