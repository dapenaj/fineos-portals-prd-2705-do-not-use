/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import classNames from 'classnames';
import { FormikProps, Formik, Field, ErrorMessage } from 'formik';
import {
  FormButton,
  UiButton,
  ButtonType,
  Form,
  FormGroup,
  TextInput,
  FormattedMessage,
  Icon
} from 'fineos-common';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { NotificationFiltersProps, WidgetTableColumn } from '../../../types';
import { useFiltersThemedStyles } from '../table-filter.utils';
import styles from '../table-filter.module.scss';

import { customerFilterValidationSchema } from './customer-filter.form';

export const CustomerFilter: React.FC<NotificationFiltersProps> = ({
  filterValues,
  onSearch,
  onReset,
  onClose
}) => {
  const themedStyles = useFiltersThemedStyles();
  const [isFiltered, setIsFiltered] = useState<boolean>(false);

  useEffect(() => {
    const customer = filterValues[WidgetTableColumn.CUSTOMER];
    setIsFiltered(
      !!customer?.customerFirstName || !!customer?.customerLastName
    );
  }, [filterValues]);

  const doReset = ({ resetForm }: FormikProps<any>) => {
    resetForm({});
    onReset();
  };

  const getErrors = (props: FormikProps<any>) => (
    <ErrorMessage
      name="filter"
      render={() => (
        <div className={classNames(themedStyles.errorMessage, styles.error)}>
          <FormattedMessage id={props.errors.filter as string} />
        </div>
      )}
    />
  );

  return (
    <div
      className={classNames(styles.wrapper, themedStyles.wrapper)}
      data-test-el="filter-customer"
    >
      <Formik
        enableReinitialize={true}
        initialValues={{ filter: { ...filterValues } }}
        onSubmit={(values, actions) => {
          onSearch({
            customer: {
              customerFirstName: values.filter?.customer?.customerFirstName,
              customerLastName: values.filter?.customer?.customerLastName
            }
          });
          actions.setSubmitting(false);
        }}
        validationSchema={customerFilterValidationSchema}
      >
        {(props: FormikProps<any>) => (
          <Form>
            <UiButton
              buttonType={ButtonType.LINK}
              onClick={onClose}
              className={styles.iconWrapper}
            >
              <Icon
                icon={faTimes}
                className={themedStyles.icon}
                iconLabel={
                  <FormattedMessage id="FINEOS_COMMON.GENERAL.CLOSE" />
                }
              />
            </UiButton>
            <Row gutter={32}>
              <Col>
                <FormattedMessage
                  className={classNames(styles.info, themedStyles.infoText)}
                  id="NOTIFICATIONS.FILTERS.INFO"
                />
              </Col>
              <Col span={12}>
                <FormGroup
                  label={
                    <FormattedMessage id="NOTIFICATIONS.FILTERS.CUSTOMER_FIRSTNAME" />
                  }
                  name="filter.customer.customerFirstName"
                  className={themedStyles.item}
                  showErrorMessage={false}
                >
                  <Field
                    name="filter.customer.customerFirstName"
                    placeholder={
                      <FormattedMessage id="NOTIFICATIONS.FILTERS.CUSTOMER_FIRSTNAME_PLACEHOLDER" />
                    }
                    data-test-el="input-customerFirstName"
                    component={TextInput}
                    className={themedStyles.placeholder}
                  />
                </FormGroup>
              </Col>
              <Col span={12}>
                <FormGroup
                  label={
                    <FormattedMessage id="NOTIFICATIONS.FILTERS.CUSTOMER_LASTNAME" />
                  }
                  name="filter.customer.customerLastName"
                  className={themedStyles.item}
                  showErrorMessage={false}
                >
                  <Field
                    name="filter.customer.customerLastName"
                    placeholder={
                      <FormattedMessage id="NOTIFICATIONS.FILTERS.CUSTOMER_LASTNAME_PLACEHOLDER" />
                    }
                    data-test-el="input-customerLastName"
                    component={TextInput}
                    className={themedStyles.placeholder}
                  />
                </FormGroup>
              </Col>
              <Col>{props.errors && getErrors(props)}</Col>
            </Row>
            <FormattedMessage
              id="COMMON.FILTERS.SEARCH"
              as={FormButton}
              htmlType="submit"
              data-test-el="search-button"
            />
            <FormattedMessage
              disabled={!isFiltered}
              id="FINEOS_COMMON.GENERAL.RESET"
              as={FormButton}
              htmlType="reset"
              onClick={() => doReset(props)}
              buttonType={ButtonType.LINK}
              data-test-el="reset-button"
            />
          </Form>
        )}
      </Formik>
    </div>
  );
};
