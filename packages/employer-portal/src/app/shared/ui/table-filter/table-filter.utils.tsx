/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { FC } from 'react';
import classNames from 'classnames';
import { IntlShape } from 'react-intl';
import { AbsenceEvent, GroupClientNotification } from 'fineos-js-api-client';
import {
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  UiButton,
  ButtonType,
  Icon
} from 'fineos-common';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

import { hasClaimTypeSubcase } from '../../utils';
import { TableFilters, CaseType } from '../../types';
import { EnumDomain, getEnumDomainTranslation } from '../../enum-domain';
import { EmployeesApprovedForLeaveType } from '../../../modules/my-dashboard/employees-approved-for-leave/employees-approved-for-leave-widget.enums';

import styles from './table-filter.module.scss';

export const initialTableFilters: TableFilters = {
  jobTitle: '',
  adminGroup: '',
  customer: {},
  caseNumber: '',
  notificationReason: [],
  disabilityRelated: '',
  decision: '',
  employee: {},
  caseReference: '',
  eventName: ''
};

type FilterIconElementProps = {
  id: string;
  icon: IconProp;
  isActive: boolean;
  onClick?: () => void;
};

export const filterByJobTitle = (
  filters: TableFilters,
  jobTitle: string = ''
) =>
  jobTitle.toLowerCase().includes(filters.jobTitle?.toLowerCase() || '') ||
  !filters.jobTitle;

export const filterByAdminGroup = (
  filters: TableFilters,
  adminGroup: string = ''
) =>
  adminGroup.toLowerCase().includes(filters.adminGroup?.toLowerCase() || '') ||
  !filters.adminGroup;

export const filterByFirstName = (
  { customer }: TableFilters,
  firstName: string = ''
) =>
  firstName
    .toLowerCase()
    .includes(customer?.customerFirstName?.toLowerCase() || '') ||
  !customer?.customerFirstName;

export const filterByLastName = (
  { customer }: TableFilters,
  lastName: string = ''
) =>
  lastName
    .toLowerCase()
    .includes(customer?.customerLastName?.toLowerCase() || '') ||
  !customer?.customerLastName;

export const filterByNotification = (
  filters: TableFilters,
  caseNumber: string = ''
) =>
  caseNumber.toLowerCase().includes(filters.caseNumber?.toLowerCase() || '') ||
  !filters.caseNumber;

export const filterByReason = (
  { notificationReason }: TableFilters,
  reason: string = ''
) => notificationReason?.includes(reason) || !notificationReason?.length;

export const filterByDI = (
  { disabilityRelated }: TableFilters,
  notification: GroupClientNotification
) =>
  !disabilityRelated
    ? true
    : disabilityRelated === CaseType.DISABILITY_RELATED
    ? hasClaimTypeSubcase(notification)
    : !hasClaimTypeSubcase(notification);

export const getReasonNameFilters = <
  T extends
    | GroupClientNotification
    | AbsenceEvent
    | EmployeesApprovedForLeaveType
>(
  dataWithReason: T[],
  mapFn: (notification: T) => string,
  intl: IntlShape
) =>
  Array.from(new Set(dataWithReason.map(mapFn))).map(item => ({
    text: intl.formatMessage({
      id: getEnumDomainTranslation(EnumDomain.NOTIFICATION_REASON, item)
    }),
    value: item
  }));

export const filterByLeaveType = (
  { leaveType: leaveTypeFilter }: TableFilters,
  leaveType: string = ''
) => leaveTypeFilter?.includes(leaveType) || !leaveTypeFilter?.length;

export const filterByDecision = (
  { decision: decisionFilter }: TableFilters,
  decision: string = ''
) => decision === decisionFilter || !decisionFilter;

export const useFiltersThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    backgroundColor: theme.colorSchema.neutral1
  },
  item: {
    '& .ant-form-item-label': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  placeholder: {
    ...textStyleToCss(theme.typography.baseText, { suppressLineHeight: true })
  },
  icon: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  errorMessage: {
    color: theme.colorSchema.attentionColor,
    ...textStyleToCss(theme.typography.baseText)
  },
  infoText: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  footer: {
    backgroundColor: theme.colorSchema.neutral1,
    borderTop: `1px solid ${theme.colorSchema.neutral5}`,
    ...textStyleToCss(theme.typography.baseText)
  },
  filterButton: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss(theme.typography.smallText),
    '&:hover, &:focus': {
      color: theme.colorSchema.neutral8
    }
  },
  filterActive: {
    color: theme.colorSchema.primaryAction,
    '&:hover, &:focus': {
      color: theme.colorSchema.primaryAction
    }
  },
  checkbox: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  radio: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  label: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const FilterIconElement: FC<FilterIconElementProps> = ({
  id,
  icon,
  isActive,
  onClick
}) => {
  const themedStyles = useFiltersThemedStyles();
  return (
    <UiButton
      onClick={onClick}
      id={id}
      className={classNames(themedStyles.filterButton, styles.filterButton, {
        [themedStyles.filterActive]: isActive
      })}
      buttonType={ButtonType.LINK}
    >
      <Icon icon={icon} iconLabel={<FormattedMessage id={id} />} />
    </UiButton>
  );
};

export const filterLabelMapper: TableFilters = {
  jobTitle: 'JOB_TITLE',
  caseNumber: 'NOTIFICATION_ID',
  adminGroup: 'ADMIN_GROUP'
};
