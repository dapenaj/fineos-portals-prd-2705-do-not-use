/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import {
  FormattedMessage,
  Checkbox,
  CheckboxChangeEvent,
  FilterDropdownProps,
  ColumnFilterItem,
  Button,
  ButtonType,
  Icon,
  UiButton
} from 'fineos-common';
import { isEqual as _isEqual } from 'lodash';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { TableFilters, WidgetTableColumn } from '../../../types';
import {
  initialTableFilters,
  useFiltersThemedStyles
} from '../table-filter.utils';
import styles from '../table-filter.module.scss';
import { NotificationReason } from '../../../../modules/notification/notification-reason/notification-reason.component';

type TableCheckboxFilterProps = FilterDropdownProps & {
  onFilter: (columnFilters: TableFilters) => void;
  onClose: () => void;
  column: WidgetTableColumn;
  filterValues: TableFilters;
  className?: string;
  label: React.ReactNode;
};

export const TableCheckboxFilter: React.FC<TableCheckboxFilterProps> = ({
  filters,
  filterValues,
  column,
  className,
  label,
  onFilter,
  onClose,
  confirm,
  clearFilters
}) => {
  const themedStyles = useFiltersThemedStyles();
  const [localFilters, setLocalFilters] = useState<string[]>([]);
  const [isFiltered, setIsFiltered] = useState<boolean>(false);

  useEffect(() => {
    if (_isEqual(filterValues, initialTableFilters)) {
      setLocalFilters([]);
    }
    const filter = filterValues[column];
    if (!_isEqual(filterValues, initialTableFilters) && Array.isArray(filter)) {
      setLocalFilters(filter);
    }

    if (!filterValues[column]) {
      setLocalFilters([]);
    }

    setIsFiltered(Array.isArray(filter) && !!filter.length);
  }, [filterValues, column]);

  return (
    <div
      className={classNames(
        styles.filterDropdown,
        themedStyles.wrapper,
        className
      )}
      data-test-el="table-checkbox-filter"
    >
      <UiButton
        buttonType={ButtonType.LINK}
        onClick={() => {
          confirm();
          onClose();
        }}
        className={styles.iconWrapper}
      >
        <Icon
          icon={faTimes}
          className={themedStyles.icon}
          iconLabel={<FormattedMessage id="FINEOS_COMMON.GENERAL.CLOSE" />}
        />
      </UiButton>
      <div className={styles.container}>
        {label}
        <ul className={styles.filters}>
          {filters?.map(({ text, value }: ColumnFilterItem, index: number) => (
            <li key={index}>
              <Checkbox
                className={themedStyles.checkbox}
                onChange={({ target }: CheckboxChangeEvent) => {
                  const currentKey = target.value;
                  const isKeySelected = localFilters.find(
                    (key: string) => key === currentKey
                  );
                  setLocalFilters(
                    isKeySelected
                      ? localFilters.filter((key: string) => key !== currentKey)
                      : [...localFilters, currentKey]
                  );
                }}
                value={value}
                checked={localFilters.some((key: string) => key === value)}
              >
                {column === WidgetTableColumn.REASON ? (
                  <NotificationReason reason={value as string} />
                ) : (
                  text
                )}
              </Checkbox>
            </li>
          ))}
        </ul>
      </div>

      <div className={classNames(styles.footer, themedStyles.footer)}>
        <FormattedMessage
          as={Button}
          onClick={() => {
            onFilter({ [column]: localFilters });
            confirm();
          }}
          id="FINEOS_COMMON.GENERAL.OK"
        />
        <FormattedMessage
          disabled={!isFiltered}
          as={Button}
          buttonType={ButtonType.LINK}
          onClick={() => {
            clearFilters && clearFilters();
            setLocalFilters([]);
            onFilter(initialTableFilters);
            setIsFiltered(false);
            confirm();
          }}
          id="FINEOS_COMMON.GENERAL.RESET"
        />
      </div>
    </div>
  );
};
