/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import { Field, getIn, useFormikContext } from 'formik';
import { FormGroup, TextInput, FormattedMessage } from 'fineos-common';
import { BaseDomain } from 'fineos-js-api-client';

import { EnumNameSelect } from '../enum-select';
import { EnumDomain } from '../../enum-domain';

import { isCanadaCountry, isUSACountry } from './address.util';
import { initialAddressInputValue } from './address-input.form';
import { CountrySelect } from './country-select.component';
import styles from './address.module.scss';

type AddressInputProps = {
  name: string;
  className?: string;
};

const generateAddressLines = (name: string, from: number, to: number) => {
  const lines = [];

  for (let i = from; i <= to; i++) {
    lines.push(
      <Col span={12} key={i}>
        <FormGroup
          label={<FormattedMessage id={`COMMON.ADDRESS.ADDRESS_LINE${i}`} />}
          data-test-el={`address-line${i}-field`}
        >
          <Field name={`${name}.addressLine${i}`} component={TextInput} />
        </FormGroup>
      </Col>
    );
  }

  return lines;
};

export const AddressInput: React.FC<AddressInputProps> = ({
  name,
  className
}) => {
  const formik = useFormikContext();
  const address = getIn(formik.values, name);

  let countryCustomisedSection;

  if (isUSACountry(address.country)) {
    countryCustomisedSection = (
      <>
        {generateAddressLines(name, 2, 3)}

        <Col xs={16}>
          <FormGroup
            label={<FormattedMessage id="COMMON.ADDRESS.CITY" />}
            data-test-el="address-city-field"
          >
            <Field name={`${name}.addressLine4`} component={TextInput} />
          </FormGroup>
        </Col>

        <Col xs={8}>
          <FormGroup
            isRequired={true}
            className={styles.stateFormGroup}
            label={<FormattedMessage id="COMMON.ADDRESS.STATE" />}
            data-test-el="address-state-field"
          >
            <Field
              name={`${name}.addressLine6`}
              component={EnumNameSelect}
              enumDomain={EnumDomain.US_STATES}
            />
          </FormGroup>
        </Col>

        <Col xs={8}>
          <FormGroup
            isRequired={true}
            name={`${name}.postCode`}
            label={<FormattedMessage id="COMMON.ADDRESS.ZIP_CODE" />}
            data-test-el="address-post-code-field"
          >
            <Field name={`${name}.postCode`} component={TextInput} />
          </FormGroup>
        </Col>
      </>
    );
  } else if (isCanadaCountry(address.country)) {
    countryCustomisedSection = (
      <>
        {generateAddressLines(name, 2, 3)}

        <Col xs={16}>
          <FormGroup
            isRequired={true}
            label={<FormattedMessage id="COMMON.ADDRESS.CITY" />}
            data-test-el="address-city-field"
          >
            <Field name={`${name}.addressLine4`} component={TextInput} />
          </FormGroup>
        </Col>

        <Col xs={8}>
          <FormGroup
            isRequired={true}
            className={styles.stateFormGroup}
            label={<FormattedMessage id="COMMON.ADDRESS.PROVINCE" />}
            data-test-el="address-province-field"
          >
            <Field
              name={`${name}.addressLine6`}
              component={EnumNameSelect}
              enumDomain={EnumDomain.CANADA_PROVINCES}
            />
          </FormGroup>
        </Col>

        <Col xs={8}>
          <FormGroup
            isRequired={true}
            name={`${name}.postCode`}
            label={<FormattedMessage id="COMMON.ADDRESS.POSTAL_CODE" />}
            data-test-el="address-post-code-field"
          >
            <Field name={`${name}.postCode`} component={TextInput} />
          </FormGroup>
        </Col>
      </>
    );
  } else {
    countryCustomisedSection = (
      <>
        {generateAddressLines(name, 2, 7)}

        <Col xs={8}>
          <FormGroup
            isRequired={true}
            name={`${name}.postCode`}
            label={<FormattedMessage id="COMMON.ADDRESS.POSTAL_CODE" />}
            data-test-el="address-post-code-field"
          >
            <Field name={`${name}.postCode`} component={TextInput} />
          </FormGroup>
        </Col>
      </>
    );
  }

  return (
    <Row gutter={32} className={className}>
      <Col span={24}>
        <FormGroup
          isRequired={true}
          label={<FormattedMessage id="COMMON.ADDRESS.COUNTRY" />}
          data-test-el="country-field"
        >
          <Field
            name={`${name}.country`}
            component={CountrySelect}
            onChange={(country: BaseDomain) => {
              // reset all fields when country selection get changed
              formik.setFieldValue(name, {
                ...initialAddressInputValue,
                country
              });

              for (const fieldName of Object.keys(initialAddressInputValue)) {
                formik.setFieldTouched(`${name}.${fieldName}`, false);
              }
            }}
          />
        </FormGroup>
      </Col>

      <Col span={24}>
        <FormGroup
          isRequired={true}
          label={<FormattedMessage id="COMMON.ADDRESS.ADDRESS_LINE1" />}
          data-test-el="address-line1-field"
        >
          <Field name={`${name}.addressLine1`} component={TextInput} />
        </FormGroup>
      </Col>

      {countryCustomisedSection}
    </Row>
  );
};
