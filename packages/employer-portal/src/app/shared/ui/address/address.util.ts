import { BaseDomain } from 'fineos-js-api-client';

export const USA = 'USA';
export const CANADA = 'Canada';

export const isUSACountry = (country: BaseDomain | null | undefined) =>
  !country || country?.name === USA;
export const isCanadaCountry = (country: BaseDomain | null | undefined) =>
  country?.name === CANADA;
