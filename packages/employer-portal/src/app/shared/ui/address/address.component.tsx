/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { GroupClientAddress } from 'fineos-js-api-client';
import { FormattedMessage } from 'fineos-common';

import { EnumDomain, getEnumDomainTranslation } from '../../enum-domain';

import { isCanadaCountry, isUSACountry } from './address.util';
import styles from './address.module.scss';

type AddressProps = {
  address: GroupClientAddress;
};

export const Address: React.FC<AddressProps> = ({ address }) => (
  <>
    {isUSACountry(address.country) || isCanadaCountry(address.country) ? (
      <div className={styles.wrapper} data-test-el="address">
        {address.addressLine1 && <div>{address.addressLine1}</div>}
        {address.addressLine2 && <div>{address.addressLine2}</div>}
        {address.addressLine3 && <div>{address.addressLine3}</div>}
        <div>
          {address.addressLine4}
          {address.addressLine6 && `, ${address.addressLine6}`}
        </div>
        {address.postCode && <div>{address.postCode}</div>}
        {address.country && (
          <FormattedMessage
            id={getEnumDomainTranslation(EnumDomain.COUNTRIES, address.country)}
            defaultMessage={address.country.name}
            as="div"
          />
        )}
      </div>
    ) : (
      <div className={styles.wrapper} data-test-el="address">
        {address.addressLine1 && <div>{address.addressLine1}</div>}
        {address.addressLine2 && <div>{address.addressLine2}</div>}
        {address.addressLine3 && <div>{address.addressLine3}</div>}
        {address.addressLine4 && <div>{address.addressLine4}</div>}
        {address.addressLine5 && <div>{address.addressLine5}</div>}
        {address.addressLine6 && <div>{address.addressLine6}</div>}
        {address.addressLine7 && <div>{address.addressLine7}</div>}
        {address.postCode && <div>{address.postCode}</div>}
        {address.country && (
          <FormattedMessage
            id={getEnumDomainTranslation(EnumDomain.COUNTRIES, address.country)}
            defaultMessage={address.country.name}
            as="div"
          />
        )}
      </div>
    )}
  </>
);
