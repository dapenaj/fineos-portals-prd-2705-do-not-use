/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { BaseDomain } from 'fineos-js-api-client';

import { isCanadaCountry, isUSACountry } from './address.util';

const MAX_ADDRESS_LENGTH = 40;

export type AddressInputFormValue = {
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  addressLine4: string; // city for USA and Canada
  addressLine5: string;
  addressLine6: string | undefined; // state for USA / province for Canada
  addressLine7: string;
  postCode: string;
  country: BaseDomain | undefined;
  premiseNo: string;
};

export const initialAddressInputValue: AddressInputFormValue = {
  addressLine1: '',
  addressLine2: '',
  addressLine3: '',
  addressLine4: '',
  addressLine5: '',
  addressLine6: undefined,
  addressLine7: '',
  postCode: '',
  country: undefined,
  premiseNo: ''
};

export const addressInputValidation = Yup.lazy((value: AddressInputFormValue) =>
  (isUSACountry(value?.country)
    ? Yup.object({
        addressLine4: Yup.string()
          .trim()
          .max(
            MAX_ADDRESS_LENGTH,
            'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'
          ),
        addressLine6: Yup.string()
          .trim()
          .notOneOf(['Unknown'], 'FINEOS_COMMON.VALIDATION.REQUIRED')
          .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      })
    : isCanadaCountry(value?.country)
    ? Yup.object({
        addressLine4: Yup.string()
          .trim()
          .required('FINEOS_COMMON.VALIDATION.REQUIRED')
          .max(
            MAX_ADDRESS_LENGTH,
            'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'
          ),
        addressLine6: Yup.string()
          .trim()
          .notOneOf(['Unknown'], 'FINEOS_COMMON.VALIDATION.REQUIRED')
          .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      })
    : (Yup.object() as any)
  ).shape({
    addressLine1: Yup.string()
      .trim()
      .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      .max(MAX_ADDRESS_LENGTH, 'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'),
    addressLine2: Yup.string()
      .test(
        'addressLine3',
        'FINEOS_COMMON.VALIDATION.PREVIOUS_LINE',
        (addressLine2: string) =>
          (isUSACountry(value?.country) || isCanadaCountry(value?.country)) &&
          !!value.addressLine3
            ? !!addressLine2 && !!addressLine2.trim()
            : true
      )
      .max(MAX_ADDRESS_LENGTH, 'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'),
    addressLine3: Yup.string()
      .notRequired()
      .max(MAX_ADDRESS_LENGTH, 'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'),
    addressLine5: Yup.string()
      .notRequired()
      .max(MAX_ADDRESS_LENGTH, 'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'),
    addressLine7: Yup.string()
      .notRequired()
      .max(MAX_ADDRESS_LENGTH, 'FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH'),
    postCode: Yup.string()
      .matches(/\S/, {
        excludeEmptyString: true,
        message: 'FINEOS_COMMON.VALIDATION.REQUIRED'
      })
      .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      .when(
        'country',
        (country: AddressInputFormValue['country'], schema: Yup.StringSchema) =>
          isUSACountry(country)
            ? schema.clone().max(25, 'COMMON.VALIDATION.MAX_ZIP_CODE')
            : isCanadaCountry(country)
            ? schema
                .clone()
                .matches(
                  /^(?:[A-Za-z]\d){3}$/,
                  'COMMON.VALIDATION.CANADA_POSTAL_CODE_FORMAT'
                )
            : schema.clone().max(25, 'COMMON.VALIDATION.MAX_POSTAL_CODE')
      )
  })
);
