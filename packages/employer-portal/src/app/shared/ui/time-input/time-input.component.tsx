/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import { TextInput, FormattedMessage } from 'fineos-common';
import { getIn, useFormikContext } from 'formik';

type TimeInputProps = Omit<
  React.ComponentProps<typeof TextInput>,
  'placeholder'
>;

const parseTimeStr = (inputValue: string) => {
  if (!inputValue) {
    return {
      hours: null,
      minutes: null
    };
  }
  const result = /^(\d{1,2}):(\d{2})$/.exec(inputValue);
  if (!result) {
    return {
      hours: Number.NaN,
      minutes: Number.NaN
    };
  }
  const [, hours, minutes] = result;
  return {
    hours: parseInt(hours, 10),
    minutes: parseInt(minutes, 10)
  };
};

const mapTimeToStr = (
  value: { hours: number; minutes: number } | { hours: null; minutes: null }
) => {
  if (value.hours === null) {
    return '';
  }
  if (isNaN(value.hours)) {
    return null;
  }
  return `${String(value.hours).padStart(2, '0')}:${String(
    value.minutes
  ).padStart(2, '0')}`;
};

/**
 * TimeInput component for providing time in the format hh:mm
 *
 * @constructor
 */
export const TimeInput = (props: TimeInputProps) => {
  const formik = useFormikContext();
  const initialValue = getIn(formik.initialValues, props?.field.name);
  const [rawValue, setRawValue] = useState(mapTimeToStr(props.field.value));

  useEffect(() => {
    setRawValue(mapTimeToStr(props.field.value));
    // eslint-disable-next-line
  }, [initialValue]);

  return (
    <TextInput
      placeholder={<FormattedMessage id="COMMON.TIME.INPUT_PLACEHOLDER" />}
      {...props}
      field={{
        ...props.field,
        value: rawValue,
        onChange: (e: React.ChangeEvent<any>) => {
          setRawValue(e.target.value);
          props.form.setFieldValue(props.field.name, {
            ...props.field.value,
            ...parseTimeStr(e.target.value)
          });
        }
      }}
    />
  );
};
