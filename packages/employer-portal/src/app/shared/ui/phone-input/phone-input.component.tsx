/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import { Field } from 'formik';
import {
  FormGroup,
  TextInput,
  createThemedStyles,
  FormattedMessage
} from 'fineos-common';

import styles from './phone-input.module.scss';

type PhoneInputProps = {
  name: string;
  isDisabled?: boolean;
  onInputClick?: () => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    color: theme.colorSchema.neutral8
  }
}));

export const PhoneInput: React.FC<PhoneInputProps> = ({
  name,
  isDisabled = false,
  onInputClick
}) => {
  const themedStyles = useThemedStyles();
  return (
    <Row gutter={24} className={styles.container}>
      <Col span={7}>
        <FormGroup
          label={<FormattedMessage id="COMMON.PHONE.COUNTRY_CODE" />}
          labelClassName={themedStyles.label}
          data-test-el="phone-int-code-field"
        >
          <Field
            component={TextInput}
            name={`${name}.intCode`}
            isDisabled={isDisabled}
            onClick={onInputClick}
            placeholder={
              <FormattedMessage id="COMMON.PHONE.COUNTRY_CODE_PLACEHOLDER" />
            }
          />
        </FormGroup>
      </Col>

      <Col span={7}>
        <FormGroup
          label={<FormattedMessage id="COMMON.PHONE.AREA_CODE" />}
          labelClassName={themedStyles.label}
          isRequired={true}
          data-test-el="phone-area-code-field"
        >
          <Field
            component={TextInput}
            name={`${name}.areaCode`}
            isDisabled={isDisabled}
            onClick={onInputClick}
            placeholder={
              <FormattedMessage id="COMMON.PHONE.AREA_CODE_PLACEHOLDER" />
            }
          />
        </FormGroup>
      </Col>

      <Col span={10}>
        <FormGroup
          label={<FormattedMessage id="COMMON.PHONE.PHONE_NO" />}
          labelClassName={themedStyles.label}
          isRequired={true}
          data-test-el="phone-phone-no-field"
        >
          <Field
            component={TextInput}
            name={`${name}.telephoneNo`}
            isDisabled={isDisabled}
            onClick={onInputClick}
            placeholder={
              <FormattedMessage id="COMMON.PHONE.PHONE_NO_PLACEHOLDER" />
            }
          />
        </FormGroup>
      </Col>
    </Row>
  );
};
