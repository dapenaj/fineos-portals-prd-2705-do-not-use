/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect, useMemo } from 'react';
import {
  FormattedMessage,
  FormGroupContext,
  DropdownInput
} from 'fineos-common';
import { BaseDomain } from 'fineos-js-api-client';

import { EnumDomain, useEnumDomain } from '../../enum-domain';

import { resetFormWithInitialValues } from './enum-validation.util';

type EnumNameSelectProps = {
  enumDomain: EnumDomain;
  defaultName?: string;
} & Omit<React.ComponentProps<typeof DropdownInput>, 'optionElements'>;

const NULLABLE_DEFAULT_NAME = 'Unknown';

/**
 * This component is responsible for showing select box for enum domains
 */
export const EnumNameSelect: React.FC<EnumNameSelectProps> = ({
  enumDomain,
  defaultName,
  isDisabled,
  form,
  field,
  ...props
}) => {
  const { isRequired } = useContext(FormGroupContext);
  const { domains, isLoadingDomains } = useEnumDomain(enumDomain);
  useEffect(() => {
    if (!field.value && domains.length && defaultName) {
      const defaultValue = domains.find(
        (enumInstance: BaseDomain) => enumInstance.name === defaultName
      );
      if (!!defaultValue) {
        resetFormWithInitialValues(form, field, defaultValue);
      }
    }
  }, [domains, defaultName, field, form]);

  const options = useMemo(
    () =>
      (isRequired
        ? domains.filter(
            enumDomainRecord => enumDomainRecord.name !== NULLABLE_DEFAULT_NAME
          )
        : domains
      ).map(enumDomainRecord => ({
        value: enumDomainRecord.name,
        title:
          enumDomainRecord.name === NULLABLE_DEFAULT_NAME ? (
            <FormattedMessage id={'ENUM_DOMAINS.PLEASE_SELECT'} />
          ) : (
            enumDomainRecord.name
          )
      })),
    [domains, isRequired]
  );

  return (
    <DropdownInput
      isDisabled={isDisabled || !options.length}
      optionElements={options}
      isLoading={isLoadingDomains}
      isSearchable={true}
      placeholder={
        isRequired ? (
          <FormattedMessage id={'ENUM_DOMAINS.PLEASE_SELECT'} />
        ) : (
          undefined
        )
      }
      form={form}
      field={field}
      {...props}
    />
  );
};
