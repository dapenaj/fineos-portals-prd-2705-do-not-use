/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect, useMemo } from 'react';
import { BaseDomain } from 'fineos-js-api-client';
import {
  FormattedMessage,
  FormGroupContext,
  DropdownInput
} from 'fineos-common';
import { sortBy as _sortBy, isEmpty as _isEmpty } from 'lodash';
import { useIntl, IntlShape } from 'react-intl';

import {
  EnumDomain,
  getEnumDomainTranslation,
  useEnumDomain
} from '../../enum-domain';

import { resetFormWithInitialValues } from './enum-validation.util';

type EnumSelectProps = {
  enumDomain: EnumDomain;
  defaultName?: string;
  topNames?: string[];
  allowedDisplayNames?: string[];
  shouldSkipUnknown?: boolean;
} & Omit<React.ComponentProps<typeof DropdownInput>, 'optionElements'>;

type EnumSubOption = {
  value: number;
  title: string;
};

const enumDomainIdResolver = (
  enumInstance: BaseDomain,
  enumDomain: EnumDomain,
  intl: IntlShape
) => {
  return `${intl.formatMessage({
    id: getEnumDomainTranslation(enumDomain, enumInstance.name),
    defaultMessage: enumInstance.name
  })}`;
};

const NULLABLE_DEFAULT_NAME = 'Unknown';

/**
 * This component is responsible for showing select box for enum domains
 */
export const EnumSelect = ({
  enumDomain,
  isDisabled,
  defaultName = NULLABLE_DEFAULT_NAME,
  topNames,
  allowedDisplayNames,
  field,
  form,
  shouldSkipUnknown,
  ...props
}: EnumSelectProps) => {
  const { isRequired } = useContext(FormGroupContext);
  const { domains, isLoadingDomains } = useEnumDomain(enumDomain, {
    shouldSkipUnknownFiltering: shouldSkipUnknown ?? isRequired
  });
  const intl = useIntl();

  useEffect(() => {
    if (!field.value && domains.length && defaultName) {
      const defaultValue = domains.find(
        (enumInstance: BaseDomain) => enumInstance.name === defaultName
      );
      if (!!defaultValue) {
        resetFormWithInitialValues(form, field, defaultValue);
      }
    }
  }, [domains, defaultName, field, form]);

  const options = useMemo(() => {
    const filteredDomains = !_isEmpty(allowedDisplayNames)
      ? domains.filter(domain => allowedDisplayNames!.includes(domain.name))
      : domains;
    const sortedEnums = topNames
      ? _sortBy(filteredDomains, enumDomainRecord => {
          const topPosition = topNames.indexOf(enumDomainRecord.name);
          return topPosition > -1 ? topPosition : topNames.length;
        })
      : filteredDomains;

    return sortedEnums.map(enumDomainRecord => ({
      value: enumDomainRecord,
      title: (
        <FormattedMessage
          id={getEnumDomainTranslation(enumDomain, enumDomainRecord)}
          defaultMessage={enumDomainRecord.name}
        />
      )
    }));
  }, [enumDomain, domains, topNames, allowedDisplayNames]);

  return (
    <DropdownInput
      isDisabled={isDisabled || !options.length}
      optionElements={options}
      idResolver={(enumInstance: BaseDomain) =>
        enumDomainIdResolver(enumInstance, enumDomain, intl)
      }
      isLoading={isLoadingDomains}
      field={field}
      form={form}
      isSearchable={true}
      filterOption={(query: string, option: EnumSubOption) => {
        const selectedDomainName = domains.find(
          domain => domain.fullId === option.value
        )!.name;
        return selectedDomainName.toLowerCase().includes(query.toLowerCase());
      }}
      {...(props as any)}
    />
  );
};
