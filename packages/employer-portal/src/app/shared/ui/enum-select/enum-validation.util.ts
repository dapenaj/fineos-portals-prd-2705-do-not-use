/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { BaseDomain } from 'fineos-js-api-client';
import {
  assign as _assign,
  set as _set,
  cloneDeep as _cloneDeep,
  isEqual as _isEqual,
  get as _get
} from 'lodash';
import { FormikValues, FieldProps } from 'formik';

export const isEnumValidationNotEmpty = (
  value: BaseDomain | null | undefined
): value is BaseDomain => Boolean(value) && value!.name !== 'Unknown';

export const requiredEnumValidation = (field: string) =>
  Yup.mixed().test(
    field,
    'FINEOS_COMMON.VALIDATION.REQUIRED',
    isEnumValidationNotEmpty
  );

export const resetFormWithInitialValues = (
  form: FormikValues,
  field: FieldProps['field'],
  defaultValue: BaseDomain
) => {
  if (
    !form.dirty &&
    !_isEqual(_get(form.initialValues, field.name), defaultValue)
  ) {
    const defaultObject = _set(
      _cloneDeep(form.initialValues),
      field.name,
      defaultValue
    );
    form.resetForm({
      values: _assign({}, form.initialValues, defaultObject)
    });
  }
  if (form.dirty) {
    form.setFieldValue(field.name, defaultValue);
  }
};
