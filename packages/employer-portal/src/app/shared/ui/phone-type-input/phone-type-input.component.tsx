/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import { Field } from 'formik';
import { DropdownInput, FormGroup, FormattedMessage } from 'fineos-common';

import { EnumSelect } from '../enum-select';
import { EnumDomain } from '../../enum-domain';

import { PhoneType } from './phone-type.enum';
import styles from './phone-type-input.module.scss';

type PhoneTypeInputProps = {
  allowedPhoneTypeNames?: PhoneType[];
  label?: React.ReactElement;
  labelClassName?: string;
} & Omit<
  React.ComponentProps<typeof DropdownInput>,
  'optionElements' | 'field' | 'form' | 'meta'
>;

export const PhoneTypeInput: React.FC<PhoneTypeInputProps> = ({
  allowedPhoneTypeNames = [],
  name,
  label = <FormattedMessage id="COMMON.PHONE.PHONE_TYPE" />,
  labelClassName,
  isDisabled,
  ...props
}) => {
  const targetPhoneTypes = _isEmpty(allowedPhoneTypeNames)
    ? [PhoneType.CELL, PhoneType.PHONE]
    : allowedPhoneTypeNames;
  return (
    <FormGroup
      label={label}
      isRequired={true}
      className={styles.phoneTypeFormGroup}
      labelClassName={labelClassName}
    >
      <Field
        name={name}
        enumDomain={EnumDomain.PHONE_TYPE}
        allowedDisplayNames={targetPhoneTypes}
        isDisabled={isDisabled || targetPhoneTypes.length === 1}
        component={EnumSelect}
        defaultName={PhoneType.CELL}
        {...props}
      />
    </FormGroup>
  );
};
