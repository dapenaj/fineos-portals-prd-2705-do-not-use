/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CaseMessage, NewMessage } from 'fineos-js-api-client';
import { FormattedMessage, ToastType, useToast } from 'fineos-common';

import { Filter, Mode } from '../../../modules/messages/messages.utils';

import {
  initialFormValue,
  formValidation,
  NewMessageFormValues,
  SUBJECT_MAX_LENGTH
} from './message-details.form';

type MessageDetailsFormikProps = {
  currentMessage: CaseMessage | undefined;
  onMessageAdd: (message: NewMessage) => Promise<void>;
  onClose: () => void;
  mode?: React.MutableRefObject<Mode>;
  onFilterSet?: (filter: Filter) => void;
  filter?: Filter;
  resetFormRef?: React.MutableRefObject<() => void>;
};

export const useMessageDetailsFormik = ({
  currentMessage,
  onMessageAdd,
  onClose,
  mode,
  onFilterSet,
  filter,
  resetFormRef
}: MessageDetailsFormikProps) => {
  const { show } = useToast();

  const parseSubject = (subject: string) => {
    let tempSubject = subject;
    if (subject.length > SUBJECT_MAX_LENGTH) {
      tempSubject = subject.substr(0, SUBJECT_MAX_LENGTH);
    }
    return tempSubject.startsWith('Re:') ? tempSubject : `Re: ${tempSubject}`;
  };

  const handleSubmit = async ({ message }: NewMessageFormValues) => {
    const caseId = currentMessage!.case.id;
    const subject = parseSubject(currentMessage!.subject);
    await onMessageAdd({
      caseId,
      subject,
      narrative: message
    });
    if (resetFormRef) {
      resetFormRef.current();
    }
    if (onFilterSet && filter !== Filter.ALL) {
      onFilterSet(Filter.ALL);
    }
    show(
      ToastType.SUCCESS,
      <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_SENT" />
    );

    if (mode && !mode.current.isAdding) {
      mode.current.isAdding = true;
    }
    onClose();
  };
  return {
    initialValues: initialFormValue,
    validationSchema: formValidation,
    validateOnMount: true,
    onSubmit: handleSubmit
  };
};
