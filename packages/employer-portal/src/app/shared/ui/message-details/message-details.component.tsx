/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Field, useFormikContext } from 'formik';
import { CaseMessage } from 'fineos-js-api-client';
import {
  FormGroup,
  Form,
  TextArea,
  FormattedMessage,
  Resizable,
  ResizableProps,
  IconSubmitButton,
  ConfigurableText,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';

import { BlockNavigation } from '../../../shared';
import { Filter } from '../../../modules/messages/messages.utils';
import { SortDirection } from '../sort-by-date-select';

import {
  MESSAGE_MAX_LENGTH,
  MESSAGE_MIN_LENTGH,
  NewMessageFormValues
} from './message-details.form';
import styles from './message-details.module.scss';

export type MessageDetailsProps = {
  currentMessage: CaseMessage | undefined;
  resizableOptions?: ResizableProps;
  filter?: Filter;
  sort?: SortDirection;
};

const useThemedStyles = createThemedStyles(theme => ({
  formContainer: {
    '&:before': {
      borderColor: theme.colorSchema.neutral8,
      background: theme.colorSchema.neutral1
    }
  },
  message: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  }
}));

export const MessageDetails: React.FC<MessageDetailsProps> = ({
  currentMessage,
  resizableOptions = {
    maxHeight: 520,
    minHeight: 240,
    defaultSize: { height: 240, width: '100%' }
  },
  filter,
  sort
}) => {
  const { values, resetForm, isSubmitting } = useFormikContext();
  const { message } = values as NewMessageFormValues;
  const key = React.useRef(0);
  React.useEffect(() => {
    // because resetForm is not sync with antd input I need to force rerender
    key.current++;
    resetForm();
    // eslint-disable-next-line
  }, [currentMessage, filter, sort]);
  const themedStyles = useThemedStyles();
  return (
    <div className={styles.mainContainer}>
      <div className={styles.messageContainer}>
        <div
          className={classNames(themedStyles.message, styles.message)}
          data-test-el="message-narrative"
          tabIndex={0}
        >
          {currentMessage?.narrative}
        </div>
      </div>
      {currentMessage && !currentMessage?.msgOriginatesFromPortal && (
        <Resizable {...resizableOptions} enable={{ top: true }}>
          <div
            className={classNames(
              styles.formContainer,
              themedStyles.formContainer
            )}
          >
            <Form
              className={styles.container}
              data-test-el="reply-message-form"
              key={key.current}
            >
              <FormGroup
                className={styles.textAreaFormGroup}
                wrapperClassName={styles.container}
                isRequired={true}
                name="message"
                counterMaxLength={MESSAGE_MAX_LENGTH}
                immediateErrorMessages={[
                  'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.REPLY_MESSAGE_MAX_LENGTH'
                ]}
                counterMessageId="NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_COUNTER"
              >
                <Field
                  onKeyPress={(e: React.KeyboardEvent) => e.stopPropagation()}
                  name="message"
                  data-test-el="message-field"
                  component={TextArea}
                  className={styles.textArea}
                  wrapperClassName={styles.container}
                  placeholder={
                    <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER" />
                  }
                />
              </FormGroup>
              <IconSubmitButton
                submitIcon={faPaperPlane}
                submitIconLabel={
                  <FormattedMessage id="MESSAGES.SEND_MESSAGE_ALT_TEXT" />
                }
                className={styles.submitButton}
                data-test-el="reply-icon-submit-button"
              />
            </Form>
          </div>
        </Resizable>
      )}
      <BlockNavigation
        when={message?.length >= MESSAGE_MIN_LENTGH && !isSubmitting}
        message={<ConfigurableText id="USER_ACTIONS.LEAVE_PAGE" />}
      />
    </div>
  );
};
