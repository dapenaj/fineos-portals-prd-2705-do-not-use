/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientDocumentService,
  Base64Document,
  OutstandingInformation,
  CaseDocument
} from 'fineos-js-api-client';
import { UploadedFile } from 'fineos-common';

const groupClientDocumentService = GroupClientDocumentService.getInstance();

export const uploadDocument = (
  caseId: string,
  { informationType }: OutstandingInformation,
  { name, type, file }: UploadedFile
) =>
  groupClientDocumentService.uploadDocument(caseId, informationType, {
    fileName: name,
    fileExtension: type,
    base64EncodedFileContents: file,
    contentType: type,
    description: name
  } as Base64Document);

export const setDocumentAsReceived = (
  caseId: string,
  { informationType }: OutstandingInformation,
  { documentId }: CaseDocument
) =>
  groupClientDocumentService.setOutstandingInformationAsReceived(caseId, {
    informationType,
    documentId: String(documentId)
  });
