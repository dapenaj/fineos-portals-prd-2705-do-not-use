/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  OutstandingInformation,
  ApiError,
  CaseDocument,
  ApiConfigProps
} from 'fineos-js-api-client';
import {
  Modal,
  FileUpload,
  UploadedFile,
  Button,
  ButtonType,
  SomethingWentWrongModal,
  ConfigurableText,
  usePermissions,
  ManageNotificationPermissions,
  SuccessModal,
  ApiResponseType,
  FormattedMessage,
  useErrorNotificationPopup
} from 'fineos-common';

import { markDocumentAsRead } from '../../api';

import {
  uploadDocument,
  setDocumentAsReceived
} from './upload-document-link.api';
import styles from './upload-document-link.module.scss';

type UploadDocumentLinkProps = {
  outstandingInfo: OutstandingInformation;
  caseId: string;
  onDocumentMarkRead: () => void;
};

type ErrorState = null | Error | ApiError;

export const UploadDocumentLink: React.FC<UploadDocumentLinkProps> = ({
  outstandingInfo,
  caseId,
  onDocumentMarkRead
}) => {
  const { handleCommonErrors } = useErrorNotificationPopup();
  const [isVisible, setVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [hasUploadError, setUploadError] = useState<ErrorState>(null);
  const [hasReceivedError, setReceivedError] = useState<ErrorState>(null);
  const [hasReceivedSuccess, setReceivedSuccess] = useState<boolean>(false);
  const [documentId, setDocumentId] = useState(Number.NaN);

  const hasSetDocumentReceivedPermission = usePermissions(
    ManageNotificationPermissions.ADD_CASES_OUTSTANDING_INFORMATION_RECEIVED
  );

  const hasMarkReadPermission = usePermissions(
    ManageNotificationPermissions.MARK_READ_CASE_DOCUMENT
  );

  const handleSubmit = async (data: UploadedFile) => {
    try {
      setIsLoading(true);

      const document = await uploadDocument(caseId, outstandingInfo, data);

      setDocumentId(document.documentId);

      hasSetDocumentReceivedPermission
        ? await handleDocumentUploaded(document)
        : handleSetReceivedError(
            new Error(
              'User does not have permission to set the document as received.'
            )
          );
    } catch (error) {
      setVisible(false);
      if (!handleCommonErrors(error)) {
        setUploadError(error);
      }
    } finally {
      setIsLoading(false);
    }
  };

  const handleDocumentUploaded = async (document: CaseDocument) => {
    try {
      setIsLoading(true);

      await setDocumentAsReceived(caseId, outstandingInfo, document);

      handleSetReceivedSuccess();
    } catch (error) {
      handleSetReceivedError(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSetReceivedError = (error: ErrorState) => {
    setVisible(false);
    setReceivedError(error);
  };

  const handleSetReceivedSuccess = () => {
    setVisible(false);
    setReceivedSuccess(true);
  };

  const handleOnSuccessCancel = async () => {
    if (hasMarkReadPermission) {
      try {
        await markDocumentAsRead(caseId, documentId);
      } catch (error) {
        setReceivedError(null);
      }
    }

    onDocumentMarkRead();
    setReceivedError(null);
  };

  return (
    <>
      <Button
        buttonType={ButtonType.LINK}
        className={styles.link}
        onClick={() => setVisible(!isVisible)}
        data-test-el="upload-document-link"
      >
        {outstandingInfo.informationType}
      </Button>

      <Modal
        ariaLabelId="NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD_DOCUMENT"
        header={
          <FormattedMessage id="NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD_DOCUMENT" />
        }
        visible={isVisible}
        onCancel={() => setVisible(false)}
        data-test-el="upload-document-modal"
      >
        <FileUpload
          isLoading={isLoading}
          onCancel={() => setVisible(false)}
          onSubmit={handleSubmit}
          maxFileSize={ApiConfigProps.uploadFileMaxSize}
          accept={ApiConfigProps.uploadFileAllowedExtensions}
        >
          {outstandingInfo.informationType}
        </FileUpload>
      </Modal>

      {hasUploadError && (
        <SomethingWentWrongModal
          response={hasUploadError}
          type={ApiResponseType.UPLOAD_FAIL}
          onCancel={() => setUploadError(null)}
        >
          <ConfigurableText id="DOCUMENT.UPLOAD_ERROR" />
        </SomethingWentWrongModal>
      )}

      {hasReceivedError && (
        <SuccessModal
          title={
            <FormattedMessage id="NOTIFICATIONS.ALERTS.OUTSTANDING.RECEIVED_ERROR" />
          }
          ariaLabelId="NOTIFICATIONS.ALERTS.OUTSTANDING.RECEIVED_ERROR"
          type={ApiResponseType.UPLOAD_SUCCESS}
          onCancel={handleOnSuccessCancel}
        >
          <ConfigurableText id="DOCUMENT.RECEIVED_ERROR" />
        </SuccessModal>
      )}

      {hasReceivedSuccess && (
        <SuccessModal
          title={
            <FormattedMessage id="NOTIFICATIONS.ALERTS.OUTSTANDING.DOCUMENT_UPLOADED" />
          }
          ariaLabelId="NOTIFICATIONS.ALERTS.OUTSTANDING.DOCUMENT_UPLOADED"
          type={ApiResponseType.UPLOAD_SUCCESS}
          onCancel={handleOnSuccessCancel}
        >
          <ConfigurableText id="DOCUMENT.RECEIVED_SUCCESS" />
        </SuccessModal>
      )}
    </>
  );
};
