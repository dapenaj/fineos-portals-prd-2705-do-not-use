/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import { CSVLink } from 'react-csv';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import {
  Button,
  ButtonType,
  FormattedMessage,
  Icon,
  ModalTrigger,
  ModalFooter,
  UiButton,
  UiRadioInput,
  FormGroup,
  Radio
} from 'fineos-common';

import { exportExcelData, ExcelFileFormat } from '../../';

import { FileFormat } from './export-data-modal.enum';
import styles from './export-data-modal.module.scss';

type ExportDataModalProps<T> = {
  data: { [K in keyof T]: string }[];
  fileNameTranslationId: string;
  className?: string;
};

const radioOptions = [
  {
    title: <FormattedMessage id="EXPORT_OUTSTANDING_LIST.FILE_FORMAT.XLSX" />,
    value: FileFormat.XLSX,
    key: '1'
  },
  {
    title: <FormattedMessage id="EXPORT_OUTSTANDING_LIST.FILE_FORMAT.XLS" />,
    value: FileFormat.XLS,
    key: '2'
  },
  {
    title: <FormattedMessage id="EXPORT_OUTSTANDING_LIST.FILE_FORMAT.CSV" />,
    value: FileFormat.CSV,
    key: '3'
  }
];

export const ExportDataModal = <T extends {}>({
  data,
  fileNameTranslationId,
  className
}: ExportDataModalProps<T>) => {
  const [selectedOption, setSelectedOption] = useState<FileFormat>(
    radioOptions[0].value
  );
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const fileName = useIntl().formatMessage({ id: fileNameTranslationId });

  const downloadButton = (
    <FormattedMessage
      as={UiButton}
      onClick={() => {
        if (selectedOption !== FileFormat.CSV && data) {
          exportExcelData<T>({
            extension: (selectedOption as unknown) as ExcelFileFormat,
            data,
            fileName
          });
          setIsModalVisible(false);
        }
      }}
      id="EXPORT_OUTSTANDING_LIST.DOWNLOAD_BUTTON"
    />
  );

  return (
    <ModalTrigger
      ariaLabelId="EXPORT_OUTSTANDING_LIST.MODAL_HEADER"
      modalType="modal"
      visible={isModalVisible}
      header={<FormattedMessage id="EXPORT_OUTSTANDING_LIST.MODAL_HEADER" />}
      modalBody={
        <>
          <FormGroup
            label={
              <FormattedMessage id="EXPORT_OUTSTANDING_LIST.FILE_FORMAT_LABEL" />
            }
            isRequired={true}
          >
            <UiRadioInput
              value={selectedOption}
              isVertical={true}
              className={styles.radioGroup}
              onChange={e => {
                setSelectedOption(e.target.value);
              }}
            >
              {radioOptions.map(option => (
                <Radio value={option.value} key={option.key}>
                  {option.title}
                </Radio>
              ))}
            </UiRadioInput>
          </FormGroup>

          <ModalFooter>
            {selectedOption === FileFormat.CSV ? (
              <CSVLink
                filename={fileName}
                onClick={() => setIsModalVisible(false)}
                data={data}
              >
                {downloadButton}
              </CSVLink>
            ) : (
              <>{downloadButton}</>
            )}
            <FormattedMessage
              as={UiButton}
              buttonType={ButtonType.LINK}
              onClick={() => setIsModalVisible(false)}
              id="FINEOS_COMMON.GENERAL.CANCEL"
            />
          </ModalFooter>
        </>
      }
      onCancel={() => setIsModalVisible(false)}
    >
      <Button
        data-test-el="export-modal-trigger"
        buttonType={ButtonType.LINK}
        className={classNames(styles.downloadListButton, className)}
        disabled={_isEmpty(data)}
        onClick={() => setIsModalVisible(true)}
      >
        <Icon
          icon={faDownload}
          iconLabel={
            <FormattedMessage id="EXPORT_OUTSTANDING_LIST.TRIGGER_ICON_LABEL" />
          }
        />
        <FormattedMessage
          id="EXPORT_OUTSTANDING_LIST.BUTTON_TRIGGER"
          className={styles.buttonText}
        />
      </Button>
    </ModalTrigger>
  );
};
