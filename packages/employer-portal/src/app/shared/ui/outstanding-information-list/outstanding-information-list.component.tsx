/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  List,
  DotIcon,
  Icon,
  createThemedStyles,
  textStyleToCss,
  usePermissions,
  ManageNotificationPermissions,
  FormattedMessage,
  darkenColor
} from 'fineos-common';
import classNames from 'classnames';
import { faList, faCheck } from '@fortawesome/free-solid-svg-icons';
import { CustomerSummary, OutstandingInformation } from 'fineos-js-api-client';

import { UploadDocumentLink } from '../upload-document-link';

import styles from './outstanding-information-list.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  uploadOutstandingInformationIcon: {
    color: theme.colorSchema.neutral5
  },
  uploadedOutstandingInformationIcon: {
    color: darkenColor(theme.colorSchema, 'successColor')
  },
  uploadOutstandingInformationItem: {
    borderLeftColor: theme.colorSchema.primaryColor
  },
  title: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  description: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type OutstandingInformationListProps = {
  outstandingInfos: OutstandingInformation[];
  customer: CustomerSummary;
  caseId: string;
  isItemBorderVisible?: boolean;
  refreshOutstandingInfo: () => void;
};

export const OutstandingInformationList: React.FC<OutstandingInformationListProps> = ({
  outstandingInfos,
  customer,
  caseId,
  isItemBorderVisible = true,
  refreshOutstandingInfo
}) => {
  const themedStyles = useThemedStyles();
  const hasUploadPermission = usePermissions(
    ManageNotificationPermissions.UPLOAD_CASE_BASE64_DOCUMENT
  );

  const markAsActionForMe = ({
    sourcePartyId,
    infoReceived
  }: OutstandingInformation) =>
    sourcePartyId !== customer.customerNo && !infoReceived;

  const getInfoReceivedMessageId = (info: OutstandingInformation) =>
    info.sourcePartyName !== ''
      ? info.infoReceived
        ? 'NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED'
        : 'NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD'
      : 'NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD_UNKNOWN';

  return (
    <List
      header={
        <FormattedMessage
          className={themedStyles.description}
          id="NOTIFICATIONS.ALERTS.DOWNLOAD_INFO"
        />
      }
      dataSource={outstandingInfos}
      renderItem={(outstandingInfo: OutstandingInformation) => (
        <List.Item
          key={outstandingInfo.informationType}
          data-test-el="upload-outstanding-information-item"
          className={classNames(styles.documentItem, {
            [themedStyles.uploadOutstandingInformationItem]:
              !outstandingInfo.infoReceived && isItemBorderVisible
          })}
        >
          <List.Item.Meta
            avatar={
              !outstandingInfo.infoReceived ? (
                <DotIcon
                  className={styles.documentIcon}
                  isVisible={markAsActionForMe(outstandingInfo)}
                >
                  <Icon
                    icon={faList}
                    data-test-el="upload-outstanding-information-icon"
                    iconLabel={
                      markAsActionForMe(outstandingInfo) ? (
                        <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.LIST_ICON_LABEL_WITH_ACTIONS" />
                      ) : (
                        <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.LIST_ICON_LABEL" />
                      )
                    }
                    className={classNames(
                      themedStyles.uploadOutstandingInformationIcon
                    )}
                  />
                </DotIcon>
              ) : (
                <Icon
                  icon={faCheck}
                  data-test-el="uploaded-outstanding-information-icon"
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.AFFIRMATIVE_ICON_LABEL" />
                  }
                  className={classNames(
                    styles.documentIcon,
                    themedStyles.uploadedOutstandingInformationIcon
                  )}
                />
              )
            }
            description={
              <>
                {!outstandingInfo.infoReceived && hasUploadPermission ? (
                  <UploadDocumentLink
                    outstandingInfo={outstandingInfo}
                    caseId={caseId}
                    onDocumentMarkRead={refreshOutstandingInfo}
                  />
                ) : (
                  <div className={themedStyles.title}>
                    {outstandingInfo.informationType}
                  </div>
                )}
                <FormattedMessage
                  className={classNames(
                    styles.description,
                    themedStyles.description
                  )}
                  id={getInfoReceivedMessageId(outstandingInfo)}
                  values={{
                    sourcePartyName: markAsActionForMe(outstandingInfo) ? (
                      <strong>{outstandingInfo.sourcePartyName}</strong>
                    ) : (
                      outstandingInfo.sourcePartyName
                    )
                  }}
                />
              </>
            }
          />
        </List.Item>
      )}
    />
  );
};
