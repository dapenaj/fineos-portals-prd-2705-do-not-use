/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { MultipleTokens } from 'fineos-common';
import { isEmpty as _isEmpty } from 'lodash';

import styles from './token-title.module.scss';

type TokenTitleProps = {
  tokens: React.ReactNode[];
  rightContent?: React.ReactNode;
};

export const TokenTitle: React.FC<TokenTitleProps> = ({
  children,
  tokens,
  rightContent
}) => (
  <div className={styles.wrapper}>
    <div className={styles.content}>
      {children}
      {!_isEmpty(tokens) && (
        <div className={styles.tokens} data-test-el="employee-profile-tokens">
          <MultipleTokens tokens={tokens} />
        </div>
      )}
    </div>

    {rightContent && <div className={styles.rightContent}>{rightContent}</div>}
  </div>
);
