/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Panel,
  ButtonType,
  UiButton,
  Icon,
  Donut,
  ContentRenderer,
  EmptyState,
  DonutData,
  ApiError,
  PossiblyFormattedMessageElement
} from 'fineos-common';
import classNames from 'classnames';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

import styles from './graph-widget.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  },
  icon: {
    color: theme.colorSchema.primaryAction,
    ...textStyleToCss(theme.typography.baseText)
  },
  footer: {
    borderTopColor: theme.colorSchema.neutral3
  }
}));

type GraphWidgetProps = {
  data: DonutData[];
  filters: React.ReactElement[];
  isLoading: boolean;
  error?: Error | ApiError | null;
  emptyMessage: React.ReactNode;
  onViewDetails: (filterValues?: string[]) => void;
  /**
   * Optional. This value is necessary if the total number is not the sum of the data.
   */
  totalNumber?: number;
  /**
   * Optional. This value is necessary if the total label is not the default.
   */
  totalNumberLabel?: PossiblyFormattedMessageElement;
  /**
   * Optional. This value is necessary if the total label is not the default.
   */
  totalNumberLabelAriaLabel?: PossiblyFormattedMessageElement;
};

export const GraphWidget: React.FC<GraphWidgetProps> = ({
  data,
  filters,
  isLoading,
  error,
  emptyMessage,
  onViewDetails,
  totalNumber,
  totalNumberLabel,
  totalNumberLabelAriaLabel
}) => {
  const themedStyles = useThemedStyles();
  const isGraphDataEmpty = !data.length;
  return (
    <Panel
      className={styles.panel}
      role="search"
      header={
        <div className={styles.header}>
          {filters.map((filter, id) => (
            <React.Fragment key={id}>{filter}</React.Fragment>
          ))}
        </div>
      }
    >
      <ContentRenderer
        className={styles.content}
        spinnerClassName={styles.noContent}
        isLoading={isLoading}
        isEmpty={isGraphDataEmpty}
        emptyContent={
          <EmptyState className={styles.noContent}>{emptyMessage}</EmptyState>
        }
        error={error}
        readErrorMessage={true}
      >
        <Donut
          data-test-el="donut"
          data={data}
          onSliceClick={(donutData: DonutData) =>
            onViewDetails(donutData.filterValues)
          }
          totalNumber={totalNumber}
          totalNumberLabel={totalNumberLabel}
          totalNumberLabelAriaLabel={totalNumberLabelAriaLabel}
        />
      </ContentRenderer>
      {!error && !isGraphDataEmpty && !isLoading && (
        <div
          className={classNames(themedStyles.footer, styles.footer)}
          data-test-el="graph-widget-footer"
        >
          <FormattedMessage
            onClick={() => onViewDetails()}
            className={styles.widgetButton}
            as={UiButton}
            buttonType={ButtonType.LINK}
            id="WIDGET.VIEW_DETAILS"
            icon={
              <Icon
                className={classNames(themedStyles.icon, styles.icon)}
                icon={faArrowRight}
                iconLabel={<FormattedMessage id="WIDGET.VIEW_DETAILS_ICON" />}
              />
            }
          />
        </div>
      )}
    </Panel>
  );
};
