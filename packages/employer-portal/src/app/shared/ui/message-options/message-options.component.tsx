/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { MouseEvent, useEffect, useState } from 'react';
import classNames from 'classnames';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import {
  Icon,
  FormattedMessage,
  createThemedStyles,
  DropdownMenu,
  useErrorNotificationPopup,
  SomethingWentWrongModal,
  Button,
  ButtonType
} from 'fineos-common';
import { CaseMessage } from 'fineos-js-api-client';
import { Menu } from 'antd';

import { editMessage } from '../../api';

import styles from './message-options.module.scss';

type MessageOptionsProps = {
  tabIndex?: number;
  message: CaseMessage;
  onMessageEdit: (message: CaseMessage) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  icon: {
    color: theme.colorSchema.neutral8
  }
}));

export const MessageOptions: React.FC<MessageOptionsProps> = ({
  message,
  onMessageEdit,
  tabIndex
}) => {
  const themedStyles = useThemedStyles();
  const { handleCommonErrors } = useErrorNotificationPopup();
  const [error, setError] = useState(null);
  const [localMessage, setLocalMessage] = useState<CaseMessage | null>(null);
  const [isVisible, setIsVisible] = useState(false);
  const optionsMessage = localMessage || message;
  useEffect(() => {
    if (isVisible && localMessage) {
      setLocalMessage(null);
    }
  }, [isVisible, localMessage]);

  const toggleReadByGroupClient = async (e: MouseEvent<HTMLButtonElement>) => {
    setIsVisible(false);
    e.stopPropagation();
    setLocalMessage(message);
    const newMessage = {
      ...message,
      readByGroupClient: !message.readByGroupClient
    };

    onMessageEdit(newMessage);
    handleFocus(true);
    try {
      await editMessage(newMessage);
    } catch (error) {
      onMessageEdit(message); // rollback
      handleCommonErrors(error);
      if (!handleCommonErrors(error)) {
        setError(error);
      }
    }
  };

  const handleBlur = () => {
    setLocalMessage(null);
    setIsVisible(false);
    handleFocus(true);
  };

  const handleFocus = (focusToggle = false) => {
    const markAsReadButton = document.querySelector(
      '[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.MARK_AS_READ"]'
    ) as HTMLButtonElement;

    const markAsUnreadButton = document.querySelector(
      '[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.MARK_AS_UNREAD"]'
    ) as HTMLButtonElement;

    const toggleButton = document.querySelector(
      '[data-test-el="action-button"]'
    ) as HTMLButtonElement;

    if (!focusToggle) {
      markAsReadButton?.focus();
      markAsUnreadButton?.focus();
    } else {
      toggleButton.focus();
    }
  };

  return (
    <div className={styles.container} onAnimationEnd={() => handleFocus()}>
      <DropdownMenu
        data-test-el="mark-message-dropdown"
        overlay={
          <Menu data-test-el="mark-message-popover" className={styles.menu}>
            <Menu.Item className={styles.menuItem}>
              <FormattedMessage
                onBlur={handleBlur}
                onKeyDown={({ key }: React.KeyboardEvent) =>
                  key === 'Escape' && handleBlur()
                }
                as={Button}
                className={styles.button}
                onClick={toggleReadByGroupClient}
                buttonType={ButtonType.LINK}
                id={`NOTIFICATIONS.ALERTS.MESSAGES.${
                  optionsMessage.readByGroupClient
                    ? 'MARK_AS_UNREAD'
                    : 'MARK_AS_READ'
                }`}
              />
            </Menu.Item>
          </Menu>
        }
        trigger={['click']}
        placement="bottomRight"
        onAfterVisibleChange={setIsVisible}
        destroyPopupOnHide={true}
        visible={isVisible}
      >
        <Button
          buttonType={ButtonType.LINK}
          className={styles.trigger}
          onClick={(e: MouseEvent<HTMLButtonElement>) => {
            e.stopPropagation();
            setIsVisible(true);
          }}
          tabIndex={tabIndex}
        >
          <Icon
            iconLabel={
              <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_ACTIONS" />
            }
            icon={faEllipsisV}
            className={classNames(styles.icon, themedStyles.icon)}
            data-test-el="mark-message-icon"
          />
        </Button>
      </DropdownMenu>
      {error && (
        <SomethingWentWrongModal
          response={true}
          onCancel={() => setError(null)}
        >
          <FormattedMessage id="FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE" />
        </SomethingWentWrongModal>
      )}
    </div>
  );
};
