/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  CaseMessage,
  GroupClientMessageService,
  GroupClientMessageSearch,
  GroupClientCaseMessageSearch,
  NewMessage
} from 'fineos-js-api-client';
import moment from 'moment';

import { Filter } from '../../modules/messages/messages.utils';
import { SortDirection } from '../ui';

export const fetchMessages = (
  filter: Filter,
  sort: SortDirection,
  index: number = 0
) => {
  const search = new GroupClientMessageSearch()
    .pageIndex(index)
    .pageSize(20)
    .sort('contactDateTime', sort);
  switch (filter) {
    case Filter.UNREAD:
      search.readByGroupClient(false);
      break;
    case Filter.INCOMING:
      search.msgOriginatesFromPortal(false);
      break;
    case Filter.OUTGOING:
      search.msgOriginatesFromPortal(true);
      break;
  }
  return GroupClientMessageService.getInstance().fetchMessages(search);
};

export const fetchMessagesByCaseId = (
  caseId: string
): Promise<CaseMessage[]> => {
  const search = new GroupClientCaseMessageSearch();
  return GroupClientMessageService.getInstance()
    .fetchMessagesByCaseId(caseId, search.includeWebMessagesFromSubCases())
    .then(({ data }) =>
      data.sort((a: CaseMessage, b: CaseMessage) =>
        moment(b.contactDateTime).diff(moment(a.contactDateTime))
      )
    );
};

export const fetchUnreadMessagesCount = () =>
  GroupClientMessageService.getInstance()
    .fetchMessages(
      new GroupClientMessageSearch().countOnly(true).readByGroupClient(false)
    )
    .then(({ total }) => total);

export const editMessage = (message: CaseMessage) =>
  GroupClientMessageService.getInstance().editMessage(message);

export const addMessage = (message: NewMessage) =>
  GroupClientMessageService.getInstance().addMessage(message);
