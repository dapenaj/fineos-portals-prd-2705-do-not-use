/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientCustomerService,
  GroupClientCustomerInfo,
  BaseEmailAddress,
  GroupClientEmailAddress,
  GroupClientPhoneNumber,
  GroupClientPhoneNumberResource
} from 'fineos-js-api-client';

const groupClientCustomerService = GroupClientCustomerService.getInstance();

export const fetchCustomerInfo = async (customerId: string) =>
  groupClientCustomerService.fetchCustomerInfo(customerId);

export const editCustomerInfo = async (body: GroupClientCustomerInfo) => {
  return await groupClientCustomerService.editCustomerInfo(body);
};

export const fetchCustomerOccupation = async (customerId: string) => {
  return await groupClientCustomerService.fetchCustomerOccupations(customerId);
};

export const fetchPhoneNumbers = async (customerId: string) => {
  const {
    elements
  } = await groupClientCustomerService.fetchCustomerPhoneNumbers(customerId);

  return elements;
};

export const fetchEmailAddresses = async (customerId: string) => {
  const {
    elements
  } = await groupClientCustomerService.fetchCustomerEmailAddresses(customerId);

  return elements;
};

export const fetchCustomerCommunicationPreferences = async (
  customerId: string
) => {
  const {
    elements
  } = await groupClientCustomerService.fetchCustomerCommunicationPreferences(
    customerId
  );

  return elements;
};

export const addCustomerPhoneNumber = async (
  customerId: string,
  phoneNumber: GroupClientPhoneNumberResource
): Promise<GroupClientPhoneNumber> =>
  GroupClientCustomerService.getInstance().addCustomerPhoneNumber(
    customerId,
    phoneNumber
  );

export const addPhoneArray = async (
  customerId: string,
  phoneNumbers: GroupClientPhoneNumberResource[]
): Promise<GroupClientPhoneNumber[]> => {
  const createdPhoneNumbers: GroupClientPhoneNumber[] = [];

  for (const addedPhoneNumber of phoneNumbers) {
    createdPhoneNumbers.push(
      await addCustomerPhoneNumber(customerId, addedPhoneNumber)
    );
  }

  return createdPhoneNumbers;
};

export const addCustomerEmail = async (
  customerId: string,
  emailAddress: BaseEmailAddress
): Promise<GroupClientEmailAddress> =>
  GroupClientCustomerService.getInstance().addCustomerEmail(
    customerId,
    emailAddress
  );

export const addEmailArray = async (
  customerId: string,
  emailAddresses: BaseEmailAddress[]
) => {
  const createdEmailAddresses: BaseEmailAddress[] = [];
  for (const addedEmailAddress of emailAddresses) {
    createdEmailAddresses.push(
      await addCustomerEmail(customerId, addedEmailAddress)
    );
  }

  return createdEmailAddresses;
};
