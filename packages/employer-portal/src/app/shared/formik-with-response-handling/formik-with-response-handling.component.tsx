/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Formik, FormikConfig, FormikHelpers } from 'formik';
import { ConcurrencyUpdateError } from 'fineos-js-api-client';
import {
  ApiResponseModal,
  ApiResponseType,
  FormattedMessage,
  SomethingWentWrongModal,
  useErrorNotificationPopup
} from 'fineos-common';

type FormikWithResponseHandlingProps = {
  genericErrorModalMessage?: React.ReactElement<{ id: string }>;
  dataParseFunction?: (...args: any[]) => unknown;
};

const isConcurrencyError = (
  error: Error | null
): error is ConcurrencyUpdateError => error instanceof ConcurrencyUpdateError;

const isGenericError = (error: Error | null): error is Error =>
  !!error && !isConcurrencyError(error);

export const FormikWithResponseHandling: React.FC<
  FormikConfig<any> & FormikWithResponseHandlingProps
> = ({
  initialValues,
  genericErrorModalMessage = (
    <FormattedMessage id="FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE" />
  ),
  dataParseFunction,
  onSubmit,
  children,
  ...props
}) => {
  const { handleCommonErrors } = useErrorNotificationPopup();
  const [refreshCount, setRefreshCount] = useState(0);
  const [refreshedData, setRefreshedData] = useState<any>(null);
  const [error, setError] = useState<null | Error>(null);

  const handleSubmit = async (
    data: unknown,
    formikHelpers: FormikHelpers<unknown>
  ) => {
    try {
      await onSubmit(data, formikHelpers);
    } catch (error) {
      setError(error);
    }
  };

  const handleRefresh = (concurrencyError: ConcurrencyUpdateError) => {
    setError(null);
    setRefreshCount(count => count + 1);
    if (dataParseFunction) {
      setRefreshedData(dataParseFunction(concurrencyError.remoteData));
    }
  };

  return (
    <>
      <Formik
        key={refreshCount}
        initialValues={refreshedData ?? initialValues}
        enableReinitialize={true}
        onSubmit={handleSubmit}
        {...props}
      >
        {children}
      </Formik>
      {isConcurrencyError(error) && (
        <ApiResponseModal
          type={ApiResponseType.DATA_CONCURRENCY}
          response={true}
          isClosable={false}
          onCancel={() => handleRefresh(error)}
          onConfirm={() => handleRefresh(error)}
          ariaLabelId="DATA_CONCURRENCY_MODAL.TITLE"
          title={<FormattedMessage id="DATA_CONCURRENCY_MODAL.TITLE" />}
          buttonText={
            <FormattedMessage id="DATA_CONCURRENCY_MODAL.REFRESH_BUTTON" />
          }
        >
          <FormattedMessage id="DATA_CONCURRENCY_MODAL.DESCRIPTION" />
        </ApiResponseModal>
      )}

      {isGenericError(error) && !handleCommonErrors(error) && (
        <SomethingWentWrongModal
          response={true}
          onCancel={() => setError(null)}
        >
          {genericErrorModalMessage}
        </SomethingWentWrongModal>
      )}
    </>
  );
};
