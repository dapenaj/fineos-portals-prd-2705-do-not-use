/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';

export const TODAY = () => moment();

export const TOMORROW = () =>
  moment()
    .startOf('day')
    .add(1, 'days');

export const validateIfAfter = (
  func: () => moment.Moment,
  message: string
) => ({
  name: 'afterThreshold',
  message,
  test: (date: Date | null) =>
    date === null || (date instanceof Date && moment(date).isAfter(func()))
});

export const validateIfBefore = (
  func: () => moment.Moment,
  message: string
) => ({
  name: 'beforeThreshold',
  message,
  test: (date: Date | null) =>
    date === null || (date instanceof Date && moment(date).isBefore(func()))
});
