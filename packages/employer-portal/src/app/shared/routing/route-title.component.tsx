/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import {
  PossiblyFormattedMessageElement,
  Theme,
  useRawFormattedMessage
} from 'fineos-common';

type RouteTitleProps = {
  title?: Exclude<PossiblyFormattedMessageElement, string>;
  match: RouteComponentProps['match'] | undefined;
};

const initialPageTitle = document.title;

/**
 * This is internal routing component, it's just needed for mounting proper page title
 *
 */
export const RouteTitle: React.FC<RouteTitleProps> = ({
  title,
  match,
  children
}) => {
  const theme = useContext(Theme)!;
  const localisedTitle = useRawFormattedMessage(
    title &&
      React.cloneElement(title, {
        values: {
          ...title.props.values,
          ...match?.params
        }
      })
  );

  const generalTitle = theme?.header?.title || initialPageTitle;

  useEffect(() => {
    document.title = [localisedTitle, generalTitle].filter(Boolean).join(' | ');
  }, [localisedTitle, generalTitle]);

  return <>{children}</>;
};
