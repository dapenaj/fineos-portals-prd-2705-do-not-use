/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Route as ReactRouterRoute,
  RouteProps as ReactRouterRouteProps,
  RouteComponentProps,
  Redirect
} from 'react-router-dom';
import {
  PermissionRule,
  PossiblyFormattedMessageElement,
  SharedState,
  usePermissions
} from 'fineos-common';

import { RouteTitle } from './route-title.component';

type RouteProps<T extends ReactRouterRouteProps = ReactRouterRouteProps> = Omit<
  T,
  'render'
> & {
  render: NonNullable<T['render']>;
  permissions?: PermissionRule;
  title?: Exclude<PossiblyFormattedMessageElement, string>;
};

/**
 * This is wrapper component over react-router Route, but with some extra functionality.
 *
 * It allow to define title on route level (however, maybe we need to introduce page component at some point)
 * Also route can be protected by permissions, if permissions are not satisfied user would be redericted to the home.
 * So, please don't define any permissions for the home page.
 */
export const Route = <T extends ReactRouterRouteProps = ReactRouterRouteProps>({
  render,
  title,
  permissions = [],
  ...props
}: RouteProps<T>) => {
  const hasPermission = usePermissions(permissions);

  if (!hasPermission) {
    return <Redirect to="/" />;
  }

  return (
    <ReactRouterRoute
      {...(props as ReactRouterRouteProps)}
      render={(params: RouteComponentProps<any>) => {
        const actualRouteComponent = React.cloneElement(
          render(params) as React.ReactElement,
          {
            key: params.location.pathname
          }
        );
        return (
          <SharedState key={params.location.pathname}>
            <RouteTitle match={params.match} title={title}>
              {actualRouteComponent}
            </RouteTitle>
          </SharedState>
        );
      }}
    />
  );
};
