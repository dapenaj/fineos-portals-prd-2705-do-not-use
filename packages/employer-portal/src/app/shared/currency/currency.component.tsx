/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import { FormattedNumber } from 'react-intl';

import { Formatting } from 'fineos-common';

type CurrencyProps = {
  /**
   * Number to be displayed in currency format
   */
  amount: number | string;
  /**
   * Optional minimum number of fraction digits. Defaults to 2.
   */
  minimumFractionDigits?: number;
  /**
   * Optional maximum number of fraction digits. Defaults to 2.
   */
  maximumFractionDigits?: number;
  /**
   * Optional test element name. Defaults to "currency".
   */
  'data-test-el'?: string;
};

export const Currency: React.FC<CurrencyProps> = ({ amount, ...props }) => {
  const formatting = useContext(Formatting);

  return (
    <FormattedNumber
      data-test-el={props['data-test-el'] || 'currency'}
      value={typeof amount === 'string' ? Number(amount) : amount}
      // eslint-disable-next-line
      style="currency"
      currency={formatting.currency}
      {...props}
    />
  );
};
