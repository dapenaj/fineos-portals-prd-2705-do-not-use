/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect } from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import { Prompt } from 'react-router-dom';
import {
  FormattedMessage,
  PermissionsScope,
  useSkipToContent
} from 'fineos-common';

const promptBeforeUnload = (event: BeforeUnloadEvent) => {
  // Cancel the event.
  event.preventDefault();
  // Chrome (and legacy IE) requires returnValue to be set.
  event.returnValue = '';
};

type BlockNavigationProps = Omit<
  React.ComponentProps<typeof Prompt>,
  'message'
> & {
  message: React.ReactElement<React.ComponentProps<typeof FormattedMessage>>;
};

/**
 * This component block navigating out from the current screen
 */
export const BlockNavigation: React.FC<BlockNavigationProps> = ({
  message: Message,
  when,
  ...props
}) => {
  const { permissions } = useContext(PermissionsScope);
  const { isSkipping } = useSkipToContent();
  const translatedBlockNavigation = React.cloneElement(Message, {
    children: (extraParams: unknown, text: React.ReactNode) => (
      <Prompt
        when={!_isEmpty(permissions) && !isSkipping && when}
        message={text as string}
        {...props}
      />
    )
  });

  // as we use Prompt in HashStrategy router, React-Router will take care only
  // about internal rerouting, so we need to prevent outside routing maunally
  useEffect(() => {
    if (!isSkipping && when) {
      window.addEventListener('beforeunload', promptBeforeUnload);

      return () => {
        window.removeEventListener('beforeunload', promptBeforeUnload);
      };
    }
    // eslint-disable-next-line
  }, [when]);

  return translatedBlockNavigation;
};
