/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FormGroup,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage
} from 'fineos-common';
import { Field, FormikProps, useFormikContext, getIn } from 'formik';

import { TimeInput } from '../ui/time-input';

import { WorkPatternValue } from './work-pattern.type';

type FieldDayInputProps = {
  day: string;
  name: string;
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    ...textStyleToCss(theme.typography.smallTextSemiBold),
    color: theme.colorSchema.neutral8
  }
}));

export const FieldDayInput: React.FC<FieldDayInputProps> = ({
  day,
  name,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const formikBag: FormikProps<{
    [key: string]: WorkPatternValue;
  }> = useFormikContext();
  const fieldDayInputErrors = getIn(formikBag.errors, name);

  return (
    <FormGroup
      label={
        <div className={themedStyles.label}>
          <FormattedMessage
            ariaLabelName={{
              descriptor: { id: `WORK_PATTERN.WEEK_DAYS.${day}` }
            }}
            role="text"
            id={`WORK_PATTERN.WEEK_DAYS_SHORTCUT.${day}`}
          />
        </div>
      }
      data-test-el={props['data-test-el'] || 'editable-day-input'}
      showErrorMessage={false}
      isGroupInvalid={!!fieldDayInputErrors}
    >
      <Field name={name} component={TimeInput} />
    </FormGroup>
  );
};
