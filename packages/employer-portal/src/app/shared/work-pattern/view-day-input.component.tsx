/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  PropertyItem,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';

import { WeekBasedDayPatternNullable } from './work-pattern.type';

type ViewDayInputProps = {
  day?: string;
  value: WeekBasedDayPatternNullable | null | undefined;
  'data-test-el'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  text: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const ViewDayInput: React.FC<ViewDayInputProps> = ({
  day,
  value,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const shouldShowValue =
    typeof value?.hours === 'number' &&
    !(value?.hours === 0 && value?.minutes === 0);

  return (
    <PropertyItem
      data-test-el={props['data-test-el'] || 'not-editable-day-input'}
      isDefaultFallback={false}
      ariaLabelItem={
        !shouldShowValue ? (
          <FormattedMessage id="WORK_PATTERN.NO_VALUE" />
        ) : (
          undefined
        )
      }
    >
      {shouldShowValue && (
        <FormattedMessage
          className={themedStyles.text}
          id={'WORK_PATTERN.VIEW_DAY_INPUT'}
          values={{ hours: value?.hours, minutes: value?.minutes }}
        />
      )}
    </PropertyItem>
  );
};
