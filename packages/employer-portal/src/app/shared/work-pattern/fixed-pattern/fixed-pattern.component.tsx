/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import {
  Field,
  getIn,
  useFormikContext,
  ErrorMessage,
  FormikProps
} from 'formik';
import { Row, Col } from 'antd';
import classNames from 'classnames';
import {
  FormGroup,
  Panel,
  FormSwitch,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage
} from 'fineos-common';
import { BaseDomain } from 'fineos-js-api-client';
import { isObject as _isObject } from 'lodash';

import { Config } from '../../../config';
import {
  WeekBasedDayPatternNullable,
  WorkPatternValue
} from '../work-pattern.type';
import { WORK_DAYS, WEEKEND_DAYS } from '../work-patterns.constant';
import { FieldDayInput } from '../field-day-input.component';
import { DEFAULT_FIXED_WORK_WEEK } from '../work-pattern.form';
import { ViewAllWeekPattern } from '../view-all-week-pattern.component';
import styles from '../work-pattern.module.scss';

import { FixedPatternFormValues } from './fixed-pattern.form';

type FixedPatternProps = {
  nameScope: string;
  weekDayNamesTypeDomains: BaseDomain[];
};

const useThemedStyles = createThemedStyles(theme => ({
  switchLabel: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  errorMessage: {
    color: theme.colorSchema.attentionColor,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const FixedPattern: React.FC<FixedPatternProps> = ({
  nameScope,
  weekDayNamesTypeDomains
}) => {
  const config = useContext(Config);
  const dayLength = config.configurableWorkPattern.FIXED_DAY_LENGTH;
  const themedStyles = useThemedStyles();
  const { values, setFieldValue } = useFormikContext();
  const {
    isNonStandardWeek,
    includeWeekend,
    workWeek
  }: FixedPatternFormValues = getIn(values, nameScope);
  const formikBag: FormikProps<{
    [key: string]: WorkPatternValue;
  }> = useFormikContext();
  const fieldDayInputErrors = getIn(formikBag.errors, nameScope);

  const getWeekBasedDayPattern = (
    day: string
  ): WeekBasedDayPatternNullable | null =>
    workWeek?.find(workWeekDay => {
      return workWeekDay.dayOfWeek.name.toLocaleUpperCase() === day;
    }) || null;

  const getErrors = () => {
    if (fieldDayInputErrors && fieldDayInputErrors?.workWeek) {
      return {
        fieldName: `${nameScope}.workWeek`,
        errorMessageId: 'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT'
      };
    }
    if (fieldDayInputErrors && !_isObject(fieldDayInputErrors)) {
      return {
        fieldName: nameScope,
        errorMessageId: 'FINEOS_COMMON.VALIDATION.AT_LEAST_ONE_FILLED'
      };
    }
  };
  const errorValue = getErrors();
  const errorComponent = errorValue && (
    <ErrorMessage
      name={errorValue.fieldName}
      render={() => (
        <div
          className={classNames(themedStyles.errorMessage, styles.error)}
          role="alert"
        >
          <FormattedMessage id={errorValue.errorMessageId} />
        </div>
      )}
    />
  );
  return (
    <>
      <FormGroup
        className={styles.switchWrapper}
        label={
          <FormattedMessage
            id="WORK_PATTERN.NON_STANDARD_WORKING_WEEK"
            className={classNames(styles.col, themedStyles.switchLabel)}
          />
        }
        labelTooltipContent={
          <FormattedMessage id="WORK_PATTERN.NON_STANDARD_WORKING_WEEK_DESCRIPTION" />
        }
        popoverPlacement="top"
      >
        <Field
          name={`${nameScope}.isNonStandardWeek`}
          component={FormSwitch}
          data-test-el="non-standard-working-week-switch"
          onSwitchChange={() => {
            const workWeekValue = [
              ...DEFAULT_FIXED_WORK_WEEK(
                weekDayNamesTypeDomains,
                dayLength,
                !isNonStandardWeek
              )
            ];
            setFieldValue(`${nameScope}.workWeek`, workWeekValue);
            setFieldValue(`${nameScope}.includeWeekend`, false);
          }}
        />
      </FormGroup>
      <Panel className={styles.panelWrapper}>
        <Row>
          {isNonStandardWeek ? (
            WORK_DAYS.map((day, index) => {
              return (
                <Col className={styles.col} xs={8} sm={4} key={index}>
                  <FieldDayInput
                    name={`${nameScope}.workWeek[${index}]`}
                    day={day}
                    data-test-el={`${day}-input`}
                  />
                </Col>
              );
            })
          ) : (
            <Col span={24}>
              <ViewAllWeekPattern
                getDay={getWeekBasedDayPattern}
                days={WORK_DAYS}
              />
            </Col>
          )}

          {isNonStandardWeek &&
            ((!includeWeekend && fieldDayInputErrors?.workWeek) ||
              fieldDayInputErrors?.workWeek?.length < 6 ||
              (fieldDayInputErrors && !_isObject(fieldDayInputErrors))) &&
            errorComponent}
        </Row>
        {isNonStandardWeek && (
          <FormGroup
            className={styles.switchWrapper}
            label={
              <FormattedMessage
                id="INTAKE.OCCUPATION_AND_EARNINGS.INCLUDE_WEEKEND"
                className={classNames(styles.col, themedStyles.switchLabel)}
              />
            }
          >
            <Field
              name={`${nameScope}.includeWeekend`}
              data-test-el="include-weekend-switch"
              component={FormSwitch}
              onSwitchChange={() => {
                if (includeWeekend) {
                  const result = workWeek.map(day => {
                    if (
                      WEEKEND_DAYS.includes(
                        day.dayOfWeek.name.toLocaleUpperCase()
                      )
                    ) {
                      return {
                        dayOfWeek: { ...day.dayOfWeek },
                        hours: null,
                        minutes: null,
                        weekNumber: 1
                      } as WeekBasedDayPatternNullable;
                    }
                    return day;
                  });
                  setFieldValue(`${nameScope}.workWeek`, result);
                }
              }}
            />
          </FormGroup>
        )}
        <Row>
          {includeWeekend &&
            WEEKEND_DAYS.map((day, index) => {
              return (
                <Col className={styles.col} xs={8} sm={4} key={index}>
                  <FieldDayInput
                    day={day}
                    name={`${nameScope}.workWeek[${index + 5}]`}
                    data-test-el={`${day}-input`}
                  />
                </Col>
              );
            })}
          {includeWeekend && fieldDayInputErrors?.workWeek?.length > 5 && (
            <Col xs={24}>{errorComponent}</Col>
          )}
        </Row>
      </Panel>
    </>
  );
};
