/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { BaseDomain } from 'fineos-js-api-client';
import { isNull as _isNull } from 'lodash';
import * as Yup from 'yup';

import {
  WeekBasedDayPatternNullable,
  EnumOptionValue,
  RotatingPatternType,
  FixedPatternType
} from './work-pattern.type';
import {
  isFixedWorkPatternType,
  isRotatingWorkPatternType,
  isVariableWorkPatternType
} from './work-pattern.util';

export const DEFAULT_FIXED_WORK_WEEK = (
  domains: BaseDomain[],
  dayLength: {
    hours: number;
    minutes: number;
  },
  isNull?: boolean
) => {
  return domains.map(
    domain =>
      ({
        dayOfWeek: { ...domain },
        hours: isNull ? null : dayLength.hours,
        minutes: isNull ? null : dayLength.minutes,
        weekNumber: 1
      } as WeekBasedDayPatternNullable)
  );
};

export const EMPTY_WORK_WEEK = (weekNumber: number, domains: BaseDomain[]) =>
  domains.map(
    domain =>
      ({
        dayOfWeek: { ...domain },
        hours: null,
        minutes: null,
        weekNumber
      } as WeekBasedDayPatternNullable)
  );

export const WORK_PATTER_DEFAULT = (
  domains: BaseDomain[],
  dayLength: {
    hours: number;
    minutes: number;
  }
) => ({
  workPatternType: null,
  fixedPattern: {
    isNonStandardWeek: false,
    includeWeekend: false,
    workWeek: DEFAULT_FIXED_WORK_WEEK(domains, dayLength)
  },
  rotatingPattern: {
    workPatternWeek1: [...EMPTY_WORK_WEEK(1, domains)],
    workPatternWeek2: [...EMPTY_WORK_WEEK(2, domains)],
    workPatternWeek3: [...EMPTY_WORK_WEEK(3, domains)],
    workPatternWeek4: [...EMPTY_WORK_WEEK(4, domains)]
  },
  variablePattern: null
});

const isNanValue = (hours: number | null, minutes: number | null) =>
  !_isNull(hours) && !_isNull(minutes) && isNaN(hours) && isNaN(minutes);

const isProperlyFilled = (hours: number | null, minutes: number | null) =>
  ((hours && minutes) ||
    (hours && minutes === 0) ||
    (hours === 0 && minutes)) &&
  (((hours === 0 || hours === 24) && minutes === 0) ||
    hours >= 24 ||
    minutes > 59);

const getSelectedOption = (
  workPatternType: (BaseDomain & EnumOptionValue) | null
) =>
  workPatternType?.subOptions.find(
    option => option.value.fullId === workPatternType?.selectedSubOption
  );

const getValidationStrategy = (
  workPatternType: (BaseDomain & EnumOptionValue) | null,
  title: string
) => {
  const value = getSelectedOption(workPatternType);

  return value && value.title === title;
};

const isValidTime = (value: WeekBasedDayPatternNullable) => {
  const { hours, minutes } = value;
  if (!!isProperlyFilled(hours, minutes)) {
    return false;
  }
  return !isNanValue(hours, minutes);
};

const isEmptyValue = Yup.object().test(
  'valid-time-format',
  'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT',
  (value: WeekBasedDayPatternNullable) => !!value && isValidTime(value)
);

export const weekSchema = Yup.array().of(isEmptyValue);

const hasAtLeastOneFilled = (workPatternWeek: WeekBasedDayPatternNullable[]) =>
  !!workPatternWeek &&
  workPatternWeek.every(
    (day: WeekBasedDayPatternNullable) =>
      (day.hours === 0 && day.minutes === 0) ||
      (day.hours === null && day.minutes === null)
  );

export const variablePatternValidation = Yup.object({
  additionalInformation: Yup.string()
    .max(2000, 'WORK_PATTERN.VALIDATION.MAX_DIGITS_VALIDATION')
    .required('WORK_PATTERN.VALIDATION.ADDITIONAL_INFORMATION_VALIDATION'),
  dayLength: Yup.object().when('workDayIsSameLength', {
    is: true,
    then: Yup.object().test(
      'valid-time-format',
      'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT',
      (value: WeekBasedDayPatternNullable) =>
        !!value &&
        isValidTime(value) &&
        !(value.hours === 0 && value.minutes === 0)
    )
  }),
  numberOfWeeks: Yup.mixed().when('workPatternRepeatsWeekly', {
    is: true,
    then: Yup.mixed().required('FINEOS_COMMON.VALIDATION.REQUIRED')
  })
});

export const fixedPatternSchema = Yup.object().when('workPatternType', {
  is: (workPatternType: BaseDomain | null) =>
    workPatternType && isFixedWorkPatternType(workPatternType),
  then: Yup.object()
    .test(
      'at-least-one-without-zero-or-null',
      'FINEOS_COMMON.VALIDATION.AT_LEAST_ONE_FILLED',
      (fixedPatternType: FixedPatternType) => {
        if (!fixedPatternType.includeWeekend) {
          fixedPatternType.workWeek.splice(
            fixedPatternType.workWeek.length - 2,
            2
          );
        }

        return !fixedPatternType.workWeek.every(
          (day: WeekBasedDayPatternNullable) =>
            (day.hours === 0 && day.minutes === 0) ||
            (day.hours === null && day.minutes === null)
        );
      }
    )
    .shape({
      workWeek: weekSchema
    })
});

export const rotatingPatternSchema = Yup.object().when('workPatternType', {
  is: (workPatternType: (BaseDomain & EnumOptionValue) | null) =>
    workPatternType && isRotatingWorkPatternType(workPatternType),
  then: Yup.object()
    .test(
      'at-least-one-filled',
      'FINEOS_COMMON.VALIDATION.AT_LEAST_ONE_FILLED',
      (weekBasedDayPatternNullable: RotatingPatternType) =>
        Object.keys(weekBasedDayPatternNullable).some(
          (key: string) =>
            !hasAtLeastOneFilled(
              weekBasedDayPatternNullable[key as keyof RotatingPatternType]
            )
        )
    )
    .when('workPatternType', {
      is: (workPatternType: (BaseDomain & EnumOptionValue) | null) =>
        getValidationStrategy(workPatternType, '2'),
      then: Yup.object({
        workPatternWeek1: weekSchema,
        workPatternWeek2: weekSchema
      }),
      otherwise: Yup.object().when('workPatternType', {
        is: (workPatternType: (BaseDomain & EnumOptionValue) | null) =>
          getValidationStrategy(workPatternType, '3'),
        then: Yup.object({
          workPatternWeek1: weekSchema,
          workPatternWeek2: weekSchema,
          workPatternWeek3: weekSchema
        }),
        otherwise: Yup.object({
          workPatternWeek1: weekSchema,
          workPatternWeek2: weekSchema,
          workPatternWeek3: weekSchema,
          workPatternWeek4: weekSchema
        })
      })
    })
});

export const variablePatternSchema = Yup.mixed().when('workPatternType', {
  is: (workPatternType: BaseDomain | null) =>
    workPatternType && isVariableWorkPatternType(workPatternType),
  then: variablePatternValidation.clone()
});
