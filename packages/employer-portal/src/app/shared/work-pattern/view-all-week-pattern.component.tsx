/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { BaseDomain } from 'fineos-js-api-client';
import { getIn, useFormikContext } from 'formik';
import {
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import classNames from 'classnames';

import { EnumDomain, useEnumDomain } from '../enum-domain';

import { ViewDayInput } from './view-day-input.component';
import { ALL_WEEK } from './work-patterns.constant';
import { getValueForWeek } from './work-pattern.util';
import { WeekBasedDayPatternNullable } from './work-pattern.type';
import styles from './work-pattern.module.scss';

type TableType = {
  title: React.ReactNode;
  render: React.ReactNode;
  key: string;
};

type ViewAllWeekPatternProps = {
  nameScope?: string;
  index?: number;
  weekBasedDayPatternNullable?: WeekBasedDayPatternNullable[];
  days?: string[];
  getDay?: (
    day: string,
    weekBasedDayPatterns?: WeekBasedDayPatternNullable[],
    weekNumber?: number,
    domains?: BaseDomain[]
  ) => WeekBasedDayPatternNullable | null;
};

const useThemedStyles = createThemedStyles(theme => ({
  value: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallTextSemiBold)
  }
}));

export const ViewAllWeekPattern: React.FC<ViewAllWeekPatternProps> = ({
  nameScope,
  index = 1,
  weekBasedDayPatternNullable,
  days = ALL_WEEK,
  getDay = getValueForWeek
}) => {
  const themedStyles = useThemedStyles();
  const { domains } = useEnumDomain(EnumDomain.WEEK_DAYS, {
    shouldSkipUnknownFiltering: true
  });
  const formik = useFormikContext();
  const state = weekBasedDayPatternNullable
    ? weekBasedDayPatternNullable
    : !!nameScope
    ? (getIn(formik.values, nameScope) as WeekBasedDayPatternNullable[])
    : ([] as WeekBasedDayPatternNullable[]);

  const columns = days.map(day => {
    const value = getDay(
      day,
      state,
      index,
      domains
    ) as WeekBasedDayPatternNullable;

    return {
      key: day,
      title: (
        <FormattedMessage
          ariaLabelName={{
            descriptor: { id: `WORK_PATTERN.WEEK_DAYS.${day}` }
          }}
          role="text"
          id={`WORK_PATTERN.WEEK_DAYS_SHORTCUT.${day}`}
        />
      ),
      render: (
        <ViewDayInput
          value={value}
          data-test-el={`week-${index}-${day}-input`}
        />
      )
    } as TableType;
  });

  const weeksToRender = useMemo(
    () => (
      <table className={styles.tableWrapper}>
        <thead>
          <tr>
            {columns.map((column: TableType) => {
              return (
                <th
                  className={classNames(themedStyles.header, styles.tableCol)}
                  key={column.key}
                >
                  {column.title}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          <tr>
            {columns.map((column: TableType) => {
              return (
                <td
                  className={classNames(themedStyles.value, styles.tableCol)}
                  key={column.key}
                >
                  {column.render}
                </td>
              );
            })}
          </tr>
        </tbody>
      </table>
    ),
    [columns, themedStyles]
  );

  return <>{weeksToRender}</>;
};
