/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import {
  AlignToSwitchLabel,
  FormattedMessage,
  FormGroup,
  SwitchLabel,
  FormSwitch,
  NumberInput,
  TextArea
} from 'fineos-common';
import { Row, Col } from 'antd';
import { Field, getIn, useFormikContext } from 'formik';

import { TimeInput } from '../../ui/time-input';
import { VariablePatternFormValues } from '../work-pattern.type';
import { LanguageContext } from '../../../shared/layouts/language/language.context';

import styles from './variable-pattern.module.scss';

type VariablePatternProps = {
  nameScope: string;
};

export const VariablePattern: React.FC<VariablePatternProps> = ({
  nameScope
}) => {
  const { decimalSeparator } = useContext(LanguageContext);
  const { values, setFieldTouched, setFieldValue } = useFormikContext();
  const variablePatternFormValues: VariablePatternFormValues = getIn(
    values,
    nameScope
  );

  return (
    <>
      <Row>
        <Col span={24}>
          <FormGroup
            isRequired={true}
            label={
              <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION" />
            }
            data-test-el="additional-info-field"
          >
            <Field
              placeholder={
                <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION_PLACEHOLDER" />
              }
              name={`${nameScope}.additionalInformation`}
              component={TextArea}
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col span={24}>
          <FormGroup
            label={
              <SwitchLabel>
                <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN_REPEATS" />
              </SwitchLabel>
            }
            noMargin={Boolean(
              variablePatternFormValues?.workPatternRepeatsWeekly
            )}
          >
            <Field
              name={`${nameScope}.workPatternRepeatsWeekly`}
              component={FormSwitch}
              data-test-el="work-pattern-repeats"
              onSwitchChange={() => {
                setFieldTouched(`${nameScope}.numberOfWeeks`, false);
                setFieldValue(`${nameScope}.numberOfWeeks`, null);
              }}
            />
          </FormGroup>
        </Col>

        {variablePatternFormValues?.workPatternRepeatsWeekly && (
          <Col span={24}>
            <AlignToSwitchLabel>
              <FormGroup
                label={
                  <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.NUMBER_OF_WEEKS" />
                }
                isRequired={true}
                isSmallLabel={true}
                data-test-el="number-of-weeks-field"
                className={styles.extraField}
              >
                <Field
                  name={`${nameScope}.numberOfWeeks`}
                  min={2}
                  max={8}
                  component={NumberInput}
                  decimalSeparator={decimalSeparator}
                />
              </FormGroup>
            </AlignToSwitchLabel>
          </Col>
        )}
      </Row>
      <Row>
        <Col span={24}>
          <FormGroup
            label={
              <SwitchLabel>
                <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.WORK_DAY" />
              </SwitchLabel>
            }
            noMargin={Boolean(variablePatternFormValues?.workDayIsSameLength)}
          >
            <Field
              name={`${nameScope}.workDayIsSameLength`}
              data-test-el="work-day-is-same-length"
              component={FormSwitch}
              onSwitchChange={() => {
                setFieldTouched(`${nameScope}.dayLength`, false);
                setFieldValue(`${nameScope}.dayLength`, '');
              }}
            />
          </FormGroup>
        </Col>

        {variablePatternFormValues?.workDayIsSameLength && (
          <Col span={24}>
            <AlignToSwitchLabel>
              <FormGroup
                label={
                  <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH" />
                }
                isRequired={true}
                isSmallLabel={true}
                data-test-el="day-length-field"
                className={styles.extraField}
              >
                <Field name={`${nameScope}.dayLength`} component={TimeInput} />
              </FormGroup>
            </AlignToSwitchLabel>
          </Col>
        )}
      </Row>
    </>
  );
};
