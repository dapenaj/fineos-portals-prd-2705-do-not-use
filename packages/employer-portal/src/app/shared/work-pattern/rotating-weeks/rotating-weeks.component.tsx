/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import {
  Panel,
  createThemedStyles,
  FormattedMessage,
  textStyleToCss
} from 'fineos-common';
import { getIn, useFormikContext, ErrorMessage, FormikProps } from 'formik';
import classNames from 'classnames';
import { isObject as _isObject } from 'lodash';

import { WorkPatternValue } from '../work-pattern.type';
import { rotatingWeeks } from '../work-pattern.util';
import { ALL_WEEK } from '../work-patterns.constant';
import { FieldDayInput } from '../field-day-input.component';

import styles from './rotating-weeks.module.scss';

type RotatingWeeksProps = {
  nameScope: string;
  weeksIndex: number;
};

const useThemedStyles = createThemedStyles(theme => ({
  errorMessage: {
    color: theme.colorSchema.attentionColor,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const RotatingWeeks: React.FC<RotatingWeeksProps> = ({
  nameScope,
  weeksIndex
}) => {
  const themedStyles = useThemedStyles();
  const formikBag: FormikProps<{
    [key: string]: WorkPatternValue;
  }> = useFormikContext();
  const fieldDayInputErrors = getIn(formikBag.errors, nameScope);
  const getErrors = () => {
    if (fieldDayInputErrors && fieldDayInputErrors[rotatingWeeks[weeksIndex]]) {
      return {
        fieldName: `${nameScope}.${rotatingWeeks[weeksIndex]}`,
        errorMessageId: 'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT'
      };
    }
    if (fieldDayInputErrors && !_isObject(fieldDayInputErrors)) {
      return {
        fieldName: nameScope,
        errorMessageId: 'FINEOS_COMMON.VALIDATION.AT_LEAST_ONE_FILLED'
      };
    }
  };
  const errorValue = getErrors();

  return (
    <Panel className={styles.panelWrapper}>
      <Row>
        {ALL_WEEK.map((day, index) => {
          return (
            <Col className={styles.col} xs={6} sm={4} md={3} key={day}>
              <FieldDayInput
                name={`${nameScope}.${rotatingWeeks[weeksIndex]}[${index}]`}
                day={day}
                data-test-el={`week-${weeksIndex}-${day}-input`}
              />
            </Col>
          );
        })}
        {errorValue && (
          <Col xs={24}>
            <ErrorMessage
              name={errorValue.fieldName}
              render={() => (
                <div
                  className={classNames(
                    themedStyles.errorMessage,
                    styles.error
                  )}
                  role="alert"
                >
                  <FormattedMessage id={errorValue.errorMessageId} />
                </div>
              )}
            />
          </Col>
        )}
      </Row>
    </Panel>
  );
};
