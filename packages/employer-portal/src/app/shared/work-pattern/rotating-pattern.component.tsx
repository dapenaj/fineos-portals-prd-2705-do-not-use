/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useMemo, useState } from 'react';
import {
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import classNames from 'classnames';

import { RotatingWeeks } from './rotating-weeks';
import styles from './work-pattern.module.scss';

type RotatingPatternProps = {
  nameScope: string;
  numberOfWeeks: number;
};
const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

const ROTATING_WEEKS_VARIANTS = [2, 3, 4];

export const RotatingPattern: React.FC<RotatingPatternProps> = ({
  nameScope,
  numberOfWeeks
}) => {
  const [visibleWeeksNumber, setVisibleWeeksNumber] = useState<number>(
    numberOfWeeks
  );
  const [hiddenWeeks, setHiddenWeeks] = useState<number[]>([]);
  const themedStyles = useThemedStyles();

  useEffect(() => {
    if (numberOfWeeks > visibleWeeksNumber) {
      setVisibleWeeksNumber(numberOfWeeks);
    }
  }, [numberOfWeeks, visibleWeeksNumber]);

  useEffect(() => {
    setHiddenWeeks([
      ...ROTATING_WEEKS_VARIANTS.filter(item => item > numberOfWeeks)
    ]);
  }, [numberOfWeeks]);

  const rotatingWeeks = useMemo(() => {
    const weeks: React.ReactElement[] = [];

    for (let index = 0; index < visibleWeeksNumber; index++) {
      weeks.push(
        <div
          key={index}
          className={classNames(styles.panelTitle, {
            [styles.expandingPanelTitle]:
              index > 1 && !hiddenWeeks.includes(index + 1),
            [styles.collapsingPanelTitle]: hiddenWeeks.includes(index + 1)
          })}
          data-test-el="work-pattern-rotating-row"
          onAnimationEnd={() => {
            if (numberOfWeeks < visibleWeeksNumber) {
              setVisibleWeeksNumber(numberOfWeeks);
            }
          }}
        >
          <FormattedMessage
            id="WORK_PATTERN.ROTATING_WEEK_HEADER"
            values={{ count: index + 1 }}
            className={themedStyles.header}
          />
          <RotatingWeeks weeksIndex={index + 1} nameScope={nameScope} />
        </div>
      );
    }
    return weeks;
  }, [
    visibleWeeksNumber,
    hiddenWeeks,
    nameScope,
    themedStyles.header,
    numberOfWeeks
  ]);
  return <>{rotatingWeeks}</>;
};
