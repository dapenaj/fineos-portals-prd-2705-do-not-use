/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { BaseDomain } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { WeekBasedDayPatternNullable } from './work-pattern.type';

export enum WorkPatternBasis {
  WEEK_BASED = 'Week Based',
  OTHER = 'Other',
  UNKNOWN = 'UNKNOWN'
}

export const getWeekNumbers = (str: string): string => {
  const parseResult = str.match(/\d+/)!;
  if (parseResult) {
    return parseResult[0];
  }

  return '';
};

export const getWeekNumbersAsInt = (str: string): number =>
  parseInt(getWeekNumbers(str), 10);

export const isWeeklyWorkPattern = (
  enumInstance: BaseDomain | undefined
): boolean => Boolean(enumInstance?.name === 'Weekly Work Pattern');

export const isFixedWorkPatternType = (
  enumInstance: BaseDomain | undefined
): boolean => Boolean(enumInstance?.name === 'Fixed');

export const isVariableWorkPatternType = (
  enumInstance: BaseDomain | undefined
): boolean => Boolean(enumInstance?.name === 'Variable');

export const isRotatingWorkPatternType = (
  enumInstance: BaseDomain | undefined
): boolean => Boolean(enumInstance?.name?.includes('Rotating'));

export const getValueForWeek = (
  day: string,
  weekBasedDayPatterns?: WeekBasedDayPatternNullable[],
  weekNumber?: number,
  domains?: BaseDomain[]
): WeekBasedDayPatternNullable => {
  const weekBasedDayPattern = weekBasedDayPatterns?.find(value => {
    return (
      !_isEmpty(value) &&
      value.weekNumber === weekNumber &&
      value.dayOfWeek?.name.toLocaleUpperCase() === day
    );
  });

  const domainValue = domains?.find(domain => {
    return domain?.name.toLocaleUpperCase() === day;
  });

  return (
    weekBasedDayPattern ||
    ({
      dayOfWeek: domainValue,
      hours: null,
      minutes: null,
      weekNumber
    } as WeekBasedDayPatternNullable)
  );
};

export const rotatingWeeks: { [key: number]: string } = {
  1: 'workPatternWeek1',
  2: 'workPatternWeek2',
  3: 'workPatternWeek3',
  4: 'workPatternWeek4'
};
