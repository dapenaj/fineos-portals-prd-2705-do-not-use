/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useCallback, useEffect, useMemo, useContext } from 'react';
import { Field, useFormikContext, getIn } from 'formik';
import classNames from 'classnames';
import { Col, Radio } from 'antd';
import {
  createThemedStyles,
  FormattedMessage,
  FormGroup,
  FormRadioInput,
  textStyleToCss,
  DropdownInput
} from 'fineos-common';
import { BaseDomain } from 'fineos-js-api-client';
import { useIntl } from 'react-intl';
import { assign as _assign } from 'lodash';

import { Config } from '../../../../app/config';
import {
  EnumDomain,
  getEnumDomainTranslation,
  useEnumDomain
} from '../../enum-domain';
import {
  getWeekNumbers,
  getWeekNumbersAsInt,
  isFixedWorkPatternType,
  isRotatingWorkPatternType,
  isVariableWorkPatternType,
  isWeeklyWorkPattern
} from '../work-pattern.util';
import { EMPTY_WORK_WEEK, DEFAULT_FIXED_WORK_WEEK } from '../work-pattern.form';
import { EnumOptionValue, EnumSubOption } from '../work-pattern.type';
import { variablePatternFormValue } from '../variable-pattern/variable-pattern.form';
import { isDomainNameUnknown } from '../../utils';

import {
  getWorkPattern,
  enumDomainNameResolver,
  getResolver,
  ROTATING_EMULATED
} from './work-pattern-type-select.util';
import styles from './work-pattern-type-select.module.scss';

type EnumOption = {
  value: EnumOptionValue;
  title: React.ReactNode;
};

const FIXED_EMULATED = 'Fixed';
const UNKNOWN_EMULATED = 'Unknown';

type WorkPatternTypeSectionProps = {
  shouldSkipUnknownWorkPatternFiltering?: boolean;
  shouldSkipUnknownWeekDaysFiltering?: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  patternTypeDescription: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  text: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  }
}));

/**
 * This is workPatternType section, as it's not a single field but a complex object
 *
 * @remark it can be refactored to a single field for complex object, but it would
 * require sufficient effort
 */
export const WorkPatternTypeSection: React.FC<WorkPatternTypeSectionProps> = ({
  shouldSkipUnknownWorkPatternFiltering = true,
  shouldSkipUnknownWeekDaysFiltering = true
}) => {
  const config = useContext(Config);
  const dayLength = config.configurableWorkPattern.FIXED_DAY_LENGTH;
  const formik = useFormikContext<any>();
  const workPatternType = getIn(formik.values, 'workPatternType');
  const intl = useIntl();

  const { domains, isLoadingDomains } = useEnumDomain(
    EnumDomain.WORK_PATTERN_TYPE,
    {
      shouldSkipUnknownFiltering: shouldSkipUnknownWorkPatternFiltering
    }
  );

  const { domains: weekDayNamesTypeDomains } = useEnumDomain(
    EnumDomain.WEEK_DAYS,
    {
      shouldSkipUnknownFiltering: shouldSkipUnknownWeekDaysFiltering
    }
  );

  const getDefaultValue = useCallback(
    (defaultName: string) => {
      const defaultValue = domains.find(
        (enumInstance: BaseDomain) => enumInstance.name === defaultName
      );
      if (defaultValue) {
        formik.setFieldValue('workPatternType', defaultValue);
        if (!formik.dirty) {
          formik.resetForm({
            values: _assign({}, formik.initialValues, {
              workPatternType: defaultValue
            })
          });
        }
      }
    },
    [domains, formik]
  );

  const setWorkPatternFromUnknown = useCallback(() => {
    if (
      shouldSkipUnknownWorkPatternFiltering &&
      workPatternType &&
      !workPatternType.selectedSubOption &&
      workPatternType.name === UNKNOWN_EMULATED &&
      domains.length
    ) {
      getDefaultValue(FIXED_EMULATED);
    }
  }, [
    getDefaultValue,
    shouldSkipUnknownWorkPatternFiltering,
    workPatternType,
    domains
  ]);

  const setWorkPatternFromState = useCallback(() => {
    if (
      workPatternType &&
      !workPatternType.selectedSubOption &&
      isRotatingWorkPatternType(workPatternType) &&
      domains.length
    ) {
      const subOptions = domains
        .filter(enumDomain => isRotatingWorkPatternType(enumDomain))
        .map(enumDomain => ({
          value: enumDomain,
          title: getWeekNumbers(enumDomain.name)
        }));
      formik.setFieldValue('workPatternType', subOptions[0].value);
      formik.setFieldValue(
        'workPatternType.selectedSubOption',
        workPatternType.fullId
      );
      formik.setFieldValue('workPatternType.subOptions', [...subOptions]);
    }
  }, [workPatternType, formik, domains]);

  useEffect(() => {
    if (!workPatternType && domains.length) {
      getDefaultValue(UNKNOWN_EMULATED);
    }
    setWorkPatternFromState();
    setWorkPatternFromUnknown();
  }, [
    workPatternType,
    domains,
    getDefaultValue,
    setWorkPatternFromState,
    setWorkPatternFromUnknown
  ]);

  const themedStyles = useThemedStyles();
  const options: EnumOption[] = useMemo(
    () =>
      domains
        .filter(enumDomain => !isWeeklyWorkPattern(enumDomain))
        .map(enumDomain => ({
          value: {
            ...enumDomain,
            subOptions: [] as EnumSubOption[],
            selectedSubOption: enumDomain.fullId
          },
          title: (
            <>
              <FormattedMessage
                id={getEnumDomainTranslation(
                  EnumDomain.WORK_PATTERN_TYPE,
                  enumDomain
                )}
                defaultMessage={enumDomain.name}
              />
              {!isDomainNameUnknown(enumDomain) && (
                <FormattedMessage
                  id={`WORK_PATTERN.TYPE_DESCRIPTION.${enumDomain.name.toUpperCase()}`}
                  className={classNames(
                    themedStyles.patternTypeDescription,
                    styles.patternTypeDescription
                  )}
                />
              )}
            </>
          )
        }))
        .reduce((acc: EnumOption[], curr: EnumOption) => {
          if (isRotatingWorkPatternType(curr.value)) {
            const rotatingOptionIndex = acc.findIndex(item =>
              isRotatingWorkPatternType(item.value)
            );

            if (rotatingOptionIndex < 0) {
              const { subOptions, ...subOptionValue } = curr.value;
              const option: EnumOption = {
                value: curr.value,
                title: (
                  <>
                    <FormattedMessage
                      id={getEnumDomainTranslation(
                        EnumDomain.WORK_PATTERN_TYPE,
                        ROTATING_EMULATED
                      )}
                    />
                    <FormattedMessage
                      id={`WORK_PATTERN.TYPE_DESCRIPTION.${ROTATING_EMULATED.toUpperCase()}`}
                      className={classNames(
                        themedStyles.patternTypeDescription,
                        styles.patternTypeDescription
                      )}
                    />
                  </>
                )
              };

              option.value.subOptions.push({
                value: subOptionValue,
                title: getWeekNumbers(curr.value.name)
              });

              acc.push(option);
            } else {
              const { subOptions, ...subOptionValue } = curr.value;
              acc[rotatingOptionIndex].value.subOptions.push({
                value: subOptionValue,
                title: getWeekNumbers(curr.value.name)
              });
            }
          } else {
            acc.push({ ...curr, value: curr.value });
          }

          return acc;
        }, []),
    [domains, themedStyles.patternTypeDescription]
  );

  const currentOption = options.find(
    option =>
      enumDomainNameResolver(option.value) ===
      (formik.values.workPatternType &&
        enumDomainNameResolver(formik.values.workPatternType))
  );

  return (
    <>
      <Col span={12}>
        <FormGroup
          label={
            <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN" />
          }
          data-test-el="work-pattern-field"
        >
          <Field
            name="workPatternType"
            component={DropdownInput}
            onChange={(value: BaseDomain) => {
              if (isRotatingWorkPatternType(value)) {
                formik.setFieldValue('rotatingPattern', {
                  workPatternWeek1: [
                    ...EMPTY_WORK_WEEK(1, weekDayNamesTypeDomains)
                  ],
                  workPatternWeek2: [
                    ...EMPTY_WORK_WEEK(2, weekDayNamesTypeDomains)
                  ],
                  workPatternWeek3: [
                    ...EMPTY_WORK_WEEK(3, weekDayNamesTypeDomains)
                  ],
                  workPatternWeek4: [
                    ...EMPTY_WORK_WEEK(4, weekDayNamesTypeDomains)
                  ]
                });
              } else {
                formik.setFieldValue('rotatingPattern', undefined);
              }

              if (isVariableWorkPatternType(value)) {
                formik.setFieldValue('variablePattern', {
                  ...variablePatternFormValue
                });
              } else {
                formik.setFieldValue('variablePattern', null);
              }

              formik.setFieldValue('fixedPattern.isNonStandardWeek', false);
              formik.setFieldValue('fixedPattern.includeWeekend', false);
              if (isFixedWorkPatternType(value)) {
                formik.setFieldValue(
                  'fixedPattern.workWeek',
                  DEFAULT_FIXED_WORK_WEEK(weekDayNamesTypeDomains, dayLength)
                );
              } else {
                formik.setFieldValue('fixedPattern.workWeek', undefined);
              }
            }}
            isDisabled={!options.length}
            optionElements={options}
            idResolver={(enumInstance: EnumOptionValue) =>
              getResolver(enumInstance, intl)
            }
            isLoading={isLoadingDomains}
          />
        </FormGroup>

        {currentOption?.value.subOptions.length ? (
          <div className={styles.rotatingRadioInput}>
            <FormGroup
              label={
                <FormattedMessage id="WORK_PATTERN.ROTATING_RADIO_LABEL" />
              }
              isRequired={true}
            >
              <Field
                name="workPatternType"
                component={FormRadioInput}
                value={formik.values.workPatternType.selectedSubOption}
                onChange={(value: string) => {
                  const selectedWeek = domains.find(
                    domain => domain.fullId === parseInt(value, 10)
                  );
                  const weeksNumber = getWeekNumbersAsInt(selectedWeek!.name);
                  const rotatingPatternToSet = formik.values.rotatingPattern;

                  if (weeksNumber === 3 && rotatingPatternToSet) {
                    formik.setFieldValue(
                      'rotatingPattern.workPatternWeek4',
                      EMPTY_WORK_WEEK(4, weekDayNamesTypeDomains)
                    );
                  }

                  if (weeksNumber === 2 && rotatingPatternToSet) {
                    formik.setFieldValue(
                      'rotatingPattern.workPatternWeek3',
                      EMPTY_WORK_WEEK(3, weekDayNamesTypeDomains)
                    );
                    formik.setFieldValue(
                      'rotatingPattern.workPatternWeek4',
                      EMPTY_WORK_WEEK(4, weekDayNamesTypeDomains)
                    );
                  }

                  const newValue = {
                    ...formik.values.workPatternType,
                    selectedSubOption: value
                  };

                  formik.setFieldValue('workPatternType', newValue);
                }}
                onBlur={() => {
                  formik.setFieldTouched('workPatternType');
                }}
                isVertical={true}
                form={formik}
                meta={formik.getFieldMeta('workPatternType')}
              >
                {currentOption?.value.subOptions.map(subOption => (
                  <Radio
                    value={subOption.value.fullId}
                    key={subOption.value.fullId}
                  >
                    <FormattedMessage
                      className={themedStyles.text}
                      id="INTAKE.OCCUPATION_AND_EARNINGS.WEEKS_NUMBER"
                      values={{
                        numberOfWeeks: parseInt(subOption.title, 10)
                      }}
                    />
                  </Radio>
                ))}
              </Field>
            </FormGroup>
          </div>
        ) : null}
      </Col>

      <Col span={24}>
        {formik.values &&
          formik.values.workPatternType &&
          getWorkPattern(
            formik.values.workPatternType,
            weekDayNamesTypeDomains
          )}
      </Col>
    </>
  );
};
