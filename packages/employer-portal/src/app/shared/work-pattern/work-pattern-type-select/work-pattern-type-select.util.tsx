/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { BaseDomain } from 'fineos-js-api-client';
import { IntlShape } from 'react-intl';

import {
  getWeekNumbersAsInt,
  isFixedWorkPatternType,
  isRotatingWorkPatternType,
  isVariableWorkPatternType
} from '../work-pattern.util';
import { EnumOptionValue } from '../work-pattern.type';
import { FixedPattern } from '../fixed-pattern/fixed-pattern.component';
import { VariablePattern } from '../variable-pattern';
import { RotatingPattern } from '../rotating-pattern.component';
import { isDomainNameUnknown } from '../../utils';

export const ROTATING_EMULATED = 'Rotating';

export const enumDomainNameResolver = (enumInstance: EnumOptionValue) =>
  enumInstance && enumInstance.name;

export const getResolver = (enumInstance: EnumOptionValue, intl: IntlShape) => {
  const workPatternType = isRotatingWorkPatternType(enumInstance)
    ? ROTATING_EMULATED.toLocaleUpperCase()
    : enumInstance.name.toUpperCase();
  return !isDomainNameUnknown(enumInstance)
    ? `${intl.formatMessage({
        id: `ENUM_DOMAINS.WORK_PATTERN_TYPE.${workPatternType}`
      })}, ${intl.formatMessage({
        id: `WORK_PATTERN.TYPE_DESCRIPTION.${workPatternType}`
      })}`
    : '';
};

export const getWorkPattern = (
  enumInstance: EnumOptionValue,
  weekDayNamesTypeDomains: BaseDomain[]
) => {
  if (isFixedWorkPatternType(enumInstance)) {
    return (
      <FixedPattern
        nameScope="fixedPattern"
        weekDayNamesTypeDomains={weekDayNamesTypeDomains}
      />
    );
  } else if (isVariableWorkPatternType(enumInstance)) {
    return <VariablePattern nameScope="variablePattern" />;
  } else if (isRotatingWorkPatternType(enumInstance)) {
    const selectedSubOption =
      enumInstance.subOptions &&
      enumInstance.subOptions.find(
        subOption => subOption.value.fullId === enumInstance.selectedSubOption
      );
    const numberOfWeeks = selectedSubOption
      ? getWeekNumbersAsInt(selectedSubOption.value.name)
      : 0;
    if (numberOfWeeks) {
      return (
        <RotatingPattern
          nameScope="rotatingPattern"
          numberOfWeeks={numberOfWeeks}
        />
      );
    }
  }

  return null;
};
