/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { WeekBasedDayPattern, BaseDomain } from 'fineos-js-api-client';
import { Moment } from 'moment';

export type WeekBasedDayPatternNullable = Pick<
  WeekBasedDayPattern,
  'dayOfWeek' | 'weekNumber'
> & {
  hours: number | null;
  minutes: number | null;
};

export type VariablePatternFormValues = {
  workPatternRepeatsWeekly: boolean;
  numberOfWeeks: number | null;
  workDayIsSameLength: boolean;
  dayLength: WeekBasedDayPatternNullable;
  additionalInformation: string;
  workPatternType?: BaseDomain;
};

export type EnumSubOption = {
  value: BaseDomain;
  title: string;
};

export type EnumOptionValue = BaseDomain & {
  subOptions: EnumSubOption[];
  selectedSubOption: number;
};

export type FixedPatternType = {
  isNonStandardWeek: boolean;
  includeWeekend: boolean;
  workWeek: WeekBasedDayPatternNullable[];
};

export type RotatingPatternType = {
  workPatternWeek1: WeekBasedDayPatternNullable[];
  workPatternWeek2: WeekBasedDayPatternNullable[];
  workPatternWeek3: WeekBasedDayPatternNullable[];
  workPatternWeek4: WeekBasedDayPatternNullable[];
};

export type WorkPatternValue = {
  workPatternType: BaseDomain & EnumOptionValue;
  fixedPattern: FixedPatternType;
  rotatingPattern: RotatingPatternType;
  variablePattern: VariablePatternFormValues;
  workWeekStarts: BaseDomain;
  patternStartDate: Moment;
  patternStatus: BaseDomain;
};
