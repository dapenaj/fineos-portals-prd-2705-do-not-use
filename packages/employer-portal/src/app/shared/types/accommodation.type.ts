/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

export enum WorkplaceAccommodationStatusLabel {
  PENDING_IN_ASSESSMENT = 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_ASSESSMENT',
  PENDING_OPTIONS = 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_OPTIONS',
  PENDING_IMPLEMENTING = 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_IMPLEMENTING',
  PENDING_RECOGNIZED = 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_RECOGNIZED',
  NOT_ACCOMMODATED = 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.NOT_ACCOMMODATED',
  ACCOMMODATED = 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.ACCOMMODATED'
}

export enum PendingAssessmentStage {
  GATHER_ADDITIONAL_INFO = 'Gather Additional Information',
  GENERATE_DETERMINATION = 'Generate Determination Notice Accommodation Accepted Correspondence',
  EVALUATE_ACCOMMODATION = 'Evaluate Accommodation Options',
  MOVE_TO_IMPLEMENT = 'Move to Implement Accommodation'
}

export enum PendingOptionsStage {
  EXPLORE_OPTIONS = 'Explore Additional Options'
}

export enum PendingImplementingStage {
  IMPLEMENT_ACCOMMODATION = 'Implement Accommodation'
}

export enum PendingRecognizedStage {
  REQUEST_VALIDATED = 'Request Validated',
  REQUEST_SUBMITTED = 'Request Submitted',
  START = 'Start',
  INTAKE_IN_PROGRESS = 'Intake in Progress',
  GENERATE_ACCOMMODATION_ACKNOWLEDGMENT = 'Generate Accommodation Acknowledgment',
  INTAKE_COMPLETE = 'Intake Complete',
  REQUEST_RECOGNIZED = 'Request Recognized'
}

export enum NotAccommodatedStage {
  CLOSED_INEFFECTIVE = 'Closed Ineffective',
  CLOSED_AS_EFFECTIVE = 'Close as Effective',
  MOVE_TO_CLOSED_EFFECTIVE = 'Move to Closed Effective',
  CLOSE_AS_INEFFECTIVE = 'Close as Ineffective',
  CLOSED = 'Closed',
  GENERATE_ACCOMMODATION_CLOSED_INEFFECTIVE = 'Generate Accommodation Closed Ineffective Correspondence',
  MOVE_TO_CLOSED_INEFFECTIVE = 'Move to Closed Ineffective',
  GENERATE_ACCOMMODATION_CLOSED_EFFECTIVE = 'Generate Accommodation Closed Effective',
  CLOSED_EFFECTIVE = 'Closed Effective'
}

export enum AccommodatedStage {
  MOVE_TO_MONITORING = 'Move to Monitoring',
  ACCOMMODATED = 'Accommodated',
  MOVE_TO_CLOSED = 'Move to Closed as Accommodated',
  GENERATE_ACCOMMODATION = 'Generate Accommodation Monitoring',
  MONITORING = 'Monitoring'
}

export type WorkplaceAccommodationStage =
  | PendingAssessmentStage
  | PendingOptionsStage
  | PendingImplementingStage
  | PendingRecognizedStage
  | NotAccommodatedStage
  | AccommodatedStage;

export enum WorkplaceAccommodationPhase {
  REGISTRATION = 'Registration',
  ASSESSMENT = 'Assessment',
  MONITORING = 'Monitoring',
  COMPLETION = 'Completion',
  CLOSED = 'Closed'
}
