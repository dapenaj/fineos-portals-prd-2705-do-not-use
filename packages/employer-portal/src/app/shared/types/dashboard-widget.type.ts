/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GroupClientNotification } from 'fineos-js-api-client';

export enum DateFilter {
  TODAY = 'TODAY',
  THIS_WEEK = 'THIS_WEEK',
  NEXT_WEEK = 'NEXT_WEEK',
  PREV_WEEK = 'PREV_WEEK',
  WEEK_AFTER_NEXT = 'WEEK_AFTER_NEXT',
  WEEK_BEFORE_LAST = 'WEEK_BEFORE_LAST'
}

export enum ForMeFilter {
  'ALL',
  'FOR_ME',
  'FOR_OTHERS'
}

export type DateRange = {
  startDate: string;
  endDate: string;
  displayDateRange: string | React.ReactNode;
  value: string;
};

export type NotificationAPIFilters = {
  expectedRTW?: DateRange;
  notificationsCreated?: DateRange;
};

export type TableFilters = {
  caseNumber?: string;
  customer?: {
    customerFirstName?: string;
    customerLastName?: string;
  };
  jobTitle?: string;
  adminGroup?: string;
  notificationReason?: string[];
  disabilityRelated?: string;
  decision?: string;
  leaveType?: string[];
  employee?: {
    customerFirstName?: string;
    customerLastName?: string;
  };
  caseReference?: string;
  eventName?: string;
};

export type NotificationFiltersProps = {
  filterValues: TableFilters;
  fieldName: keyof TableFilters;
  onSearch: (filters: TableFilters) => void;
  onReset: () => void;
  onClose: () => void;
  minLength?: number;
};

export enum WidgetTableColumn {
  CUSTOMER = 'customer',
  JOB_TITLE = 'jobTitle',
  ADMIN_GROUP = 'adminGroup',
  CASE_NUMBER = 'caseNumber',
  REASON = 'notificationReason',
  DI = 'disabilityRelated',
  DECISION = 'decision',
  LEAVE_TYPE = 'leaveType'
}

export type ExportedGroupClientNotification = Omit<
  GroupClientNotification,
  | 'notificationReason'
  | 'notifiedBy'
  | 'notificationDate'
  | 'createdDate'
  | 'subCases'
  | 'customer'
  | 'caseHanler'
  | 'expectedRTWDate'
> & {
  notificationReason: string;
  notifiedBy: string;
  notificationDate: string;
  createdDate: string;
  expectedRTWDate: string;
  subCases: string;
  customer: string;
  caseHandler: string;
};

export enum CaseType {
  DISABILITY_RELATED = 'DISABILITY_RELATED',
  NON_DISABILITY_RELATED = 'NON_DISABILITY_RELATED'
}
