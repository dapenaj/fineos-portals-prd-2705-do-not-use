/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { Benefit, DisabilityBenefit } from 'fineos-js-api-client';
import { get as _get } from 'lodash';

export type BenefitGroup<
  T extends ClaimDisabilityBenefit | PaidLeaveBenefit
> = Pick<
  Benefit,
  | 'benefitHandler'
  | 'benefitHandlerPhoneNo'
  | 'stageName'
  | 'status'
  | 'benefitCaseType'
  | 'benefitId'
> & {
  name: string;
  benefits: T[];
};

export type ClaimDisabilityBenefit = Benefit & {
  disabilityBenefit?: DisabilityBenefit;
  claimId: string;
};

export type PaidLeaveBenefit = Benefit & {
  disabilityBenefit?: DisabilityBenefit;
  paidLeaveCaseId: string;
  leavePlanName: string;
  reasonName: string;
  type: string;
};

export const isClaimDisabilityBenefit = (
  benefit: ClaimDisabilityBenefit | PaidLeaveBenefit
): benefit is ClaimDisabilityBenefit => {
  return (benefit as ClaimDisabilityBenefit).claimId !== undefined;
};

export const isPaidLeaveBenefit = (
  benefit: ClaimDisabilityBenefit | PaidLeaveBenefit
): benefit is PaidLeaveBenefit => {
  return (benefit as PaidLeaveBenefit).paidLeaveCaseId !== undefined;
};

export const isClaimDisabilityBenefitGroup = (
  benefitGroup: BenefitGroup<ClaimDisabilityBenefit | PaidLeaveBenefit>
): benefitGroup is BenefitGroup<ClaimDisabilityBenefit> => {
  return _get(benefitGroup, ['benefits', 0, 'claimId']) !== undefined;
};

export const RECURRING_BENEFIT = 'Recurring Benefit';
