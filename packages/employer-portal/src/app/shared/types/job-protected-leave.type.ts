/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { Decision } from 'fineos-js-api-client';

export enum DecisionStatusCategory {
  PENDING = 'Pending', // Yellow
  CANCELLED = 'Cancelled', // Grey
  DENIED = 'Denied', // Red
  APPROVED = 'Approved', // Green
  NONE = 'None', // Not Rendered - this should never happen
  SKIPPED = 'Skipped' // Unknown
}

export type EnhancedDecision = Decision & {
  statusCategory: DecisionStatusCategory;
};

export type JobProtectedLeavePlan = {
  leavePlanName: string;
  decisions: EnhancedDecision[];
  calculationMethod?: string;
};

export type JobProtectedLeaveChange = {
  reasonName: string;
  decisions: EnhancedDecision[];
};

export enum JobProtectedAssessmentStatus {
  PENDING = 'Pending',
  DENIED = 'Denied',
  OTHER = 'Other'
}

export type JobProtectedAssessment = {
  status: JobProtectedAssessmentStatus;
  decisions: Decision[];
};

export enum DecisionPeriodStatus {
  CANCELLED = 'Cancelled'
}

export enum LeavePlanCategory {
  PAID = 'Paid'
}

export enum JobProtectedLeaveStatus {
  TIME_OFF_PERIOD = 'Time off period', // Continuous (Full)
  EPISODIC = 'Episodic', // Intermittent (Angle)
  REDUCED_SCHEDULE = 'Reduced Schedule' // Reduced (Half)
}

export type DecisionStatusCategoryCount = {
  status: DecisionStatusCategory;
  count: number;
};

export enum DecisionReasonName {
  PREGNANCY = 'Pregnancy/Maternity'
}

export enum DecisionQualifierOne {
  POSTNATAL = 'Postnatal Disability',
  OTHER = 'Other'
}
