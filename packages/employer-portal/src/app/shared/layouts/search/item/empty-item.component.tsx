/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  ConfigurableText,
  usePermissions,
  ManageNotificationPermissions,
  Link,
  FormattedMessage
} from 'fineos-common';
import { useHistory } from 'react-router-dom';

import { SearchType, SearchResult } from '../search.type';
import { isLocked } from '../../../../modules/intake/intake.service';

import { useThemedStyles } from './item-themed-styles';
import styles from './item.module.scss';

type EmptyItemProps = {
  searchResult: SearchResult;
  performedQuery: string;
  elementId?: string;
};

export const EmptyItem: React.FC<EmptyItemProps> = ({
  searchResult: { type },
  performedQuery,
  elementId
}) => {
  const themedStyles = useThemedStyles();

  const hasNonEstablishedPermission = usePermissions(
    ManageNotificationPermissions.ADD_EFORM
  );

  const history = useHistory();
  const intakeUrl = `/employee/intake?intakeId=${Math.random()}`;

  return (
    <div
      id={elementId}
      className={classNames(
        themedStyles.searchMessage,
        type === SearchType.NOTIFICATION
          ? styles.emptyMessage
          : styles.employeeEmptyMessage
      )}
      data-test-el="search-empty-item"
    >
      <ConfigurableText
        id={
          'HEADER.SEARCH.' +
          (type === SearchType.EMPLOYEE
            ? 'NO_EMPLOYEE_FOUND'
            : 'NO_NOTIFICATION_FOUND')
        }
        values={{
          query: performedQuery
        }}
        role="status"
      />

      {type === SearchType.EMPLOYEE &&
        hasNonEstablishedPermission &&
        !isLocked() && (
          <div
            className={styles.nonEstablishedIntake}
            data-test-el="non-established-intake-link"
          >
            <FormattedMessage
              id="HEADER.SEARCH.NON_ESTABLISHED_INTAKE_LINK"
              as={Link}
              to={intakeUrl}
              onKeyPress={() => history.push(intakeUrl)} // this is workaround for FF
            />
          </div>
        )}
    </div>
  );
};
