/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CustomerSummary } from 'fineos-js-api-client';
import {
  Link,
  List,
  MultipleTokens,
  FallbackValue,
  usePermissions,
  CreateNotificationPermissions,
  ViewCustomerDataPermissions,
  Button,
  FormattedMessage,
  Icon
} from 'fineos-common';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';

import { isLocked } from '../../../../modules/intake/intake.service';

import { useThemedStyles } from './item-themed-styles';
import styles from './item.module.scss';

type EmployeeItemProps = {
  item: CustomerSummary;
  closePopup: () => void;
};

const Item = List.Item;
const Meta = Item.Meta;

export const EmployeeItem: React.FC<EmployeeItemProps> = ({
  item: {
    customerNo,
    firstName,
    lastName,
    jobTitle,
    organisationUnit,
    workSite
  },
  closePopup
}) => {
  const themedStyles = useThemedStyles();

  const hasEstablishedIntakePermission = usePermissions(
    CreateNotificationPermissions.ADD_NOTIFICATION
  );
  const hasCustomerInfoPermissions = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
  );
  const history = useHistory();
  const customerNoProfilePath = `/profile/${customerNo}`;

  const handleKeyPress = (url: string) => {
    closePopup();
    history.push(url); // this is workaround for FF
  };

  return (
    <>
      <Item
        tabIndex={0}
        className={classNames(styles.searchItem, themedStyles.searchItem)}
        data-test-el="search-item"
      >
        <Meta
          avatar={
            <Icon
              icon={faUser}
              className={classNames(styles.searchIcon, themedStyles.searchIcon)}
              iconLabel={
                <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.USER_ICON_LABEL" />
              }
            />
          }
          title={
            <span
              className={themedStyles.searchItemLink}
              data-test-el="result-title"
            >
              <Link
                to={customerNoProfilePath}
                onClick={closePopup}
                tabIndex={0}
                onKeyPress={() => handleKeyPress(customerNoProfilePath)}
              >{`${firstName} ${lastName}`}</Link>
            </span>
          }
          description={
            <span
              data-test-el="result-description"
              className={classNames(themedStyles.subText, styles.subText)}
            >
              <MultipleTokens
                tokens={[
                  <FallbackValue
                    key={0}
                    value={jobTitle}
                    fallback={
                      <FormattedMessage
                        id={'EMPLOYEE.FALLBACK.UNKNOWN_JOB_TITLE'}
                      />
                    }
                  />,
                  <FallbackValue
                    key={1}
                    value={organisationUnit}
                    fallback={
                      <FormattedMessage id={'EMPLOYEE.FALLBACK.UNKNOWN_UNIT'} />
                    }
                  />,
                  <FallbackValue
                    key={2}
                    value={workSite}
                    fallback={
                      <FormattedMessage
                        id={'EMPLOYEE.FALLBACK.UNKNOWN_LOCATION'}
                      />
                    }
                  />
                ]}
              />
            </span>
          }
        />

        {hasEstablishedIntakePermission &&
          hasCustomerInfoPermissions &&
          !isLocked() && (
            <Button
              to={`/employee/${customerNo}/intake?intakeId=${Math.random()}`}
              className={styles.establishedIntakeLink}
              data-test-el="established-intake-link"
              tabIndex={0}
              onKeyPress={() =>
                handleKeyPress(
                  `/employee/${customerNo}/intake?intakeId=${Math.random()}`
                )
              }
            >
              <FormattedMessage id="HEADER.SEARCH.ESTABLISHED_INTAKE_LINK" />
            </Button>
          )}
      </Item>
    </>
  );
};
