/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { Fragment } from 'react';
import { GroupClientNotification } from 'fineos-js-api-client';
import {
  Link,
  List,
  MultipleTokens,
  Icon,
  FormattedMessage
} from 'fineos-common';
import { faFolderOpen } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';

import { NotificationReason } from '../../../../modules/notification/notification-reason';

import { useThemedStyles } from './item-themed-styles';
import styles from './item.module.scss';

type NotificationItemProps = {
  item: GroupClientNotification;
  closePopup: () => void;
};

const Item = List.Item;
const Meta = Item.Meta;

export const NotificationItem: React.FC<NotificationItemProps> = ({
  item: {
    caseNumber,
    customer: { firstName, lastName },
    notificationReason
  },
  closePopup
}) => {
  const themedStyles = useThemedStyles();
  const history = useHistory();
  const caseNumberNotificationPath = `/notification/${caseNumber}`;

  const handleKeyPress = () => {
    closePopup();
    history.push(caseNumberNotificationPath); // this is workaround for FF
  };

  return (
    <Link
      to={caseNumberNotificationPath}
      onClick={closePopup}
      data-test-el="search-item"
      className={styles.searchResultLink}
      tabIndex={0}
      onKeyPress={handleKeyPress}
    >
      <Item className={classNames(styles.searchItem, themedStyles.searchItem)}>
        <Meta
          avatar={
            <Icon
              icon={faFolderOpen}
              className={classNames(styles.searchIcon, themedStyles.searchIcon)}
              iconLabel={
                <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.FOLDER_OPEN_ICON_LABEL" />
              }
            />
          }
          title={
            <span
              className={themedStyles.searchItemLink}
              data-test-el="result-title"
            >
              {caseNumber}
            </span>
          }
          description={
            <span
              className={classNames(themedStyles.subText, styles.subText)}
              data-test-el="result-description"
            >
              <MultipleTokens
                tokens={[
                  <span className={themedStyles.notificationOwner} key={0}>
                    {`${firstName} ${lastName}`}
                  </span>,
                  <Fragment key={1}>
                    <NotificationReason reason={notificationReason} />
                  </Fragment>
                ]}
              />
            </span>
          }
        />
      </Item>
    </Link>
  );
};
