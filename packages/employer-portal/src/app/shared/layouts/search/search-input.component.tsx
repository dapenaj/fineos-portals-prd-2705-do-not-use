/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import {
  Input,
  InputSpinner,
  useElementId,
  Select,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Icon,
  Button,
  ButtonType
} from 'fineos-common';
import { faSearch, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

import { SearchType } from './search.type';
import styles from './search.module.scss';

type SearchInputProps = {
  query: string;
  searchType: SearchType;
  isPending: boolean;
  isSearchTypeSwitchAvailable: boolean;
  isInvalid?: boolean;
  onSearchQueryChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onSearchKeyDown: (e: React.KeyboardEvent) => void;
  onSearchTypeChange: (newSearchType: SearchType) => void;
  onClearQuery: () => void;
  'aria-describedby'?: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  searchBar: {
    borderColor: theme.colorSchema.neutral4,
    '& .ant-input-affix-wrapper, .ant-input-group-addon, .ant-input-group-addon > svg': {
      background: theme.colorSchema.neutral1
    },
    '& .ant-input': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  searchIcon: {
    background: theme.colorSchema.neutral2,
    borderColor: theme.colorSchema.neutral4,
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  searchTypeSelect: {
    background: theme.colorSchema.neutral2,
    borderColor: theme.colorSchema.neutral4,
    '& .ant-select-selection-item': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  searchTypeSelectDropdown: {
    background: theme.colorSchema.neutral1,
    '& .ant-select-item-option': {
      background: theme.colorSchema.neutral1,
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    '& .ant-select-item-option-active': {
      background: theme.colorSchema.neutral2,
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseTextSemiBold)
    }
  },
  clearIcon: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const SearchInput: React.FC<SearchInputProps> = ({
  query,
  searchType,
  isPending,
  isInvalid,
  isSearchTypeSwitchAvailable,
  onSearchQueryChange,
  onSearchKeyDown,
  onSearchTypeChange,
  onClearQuery,
  ...props
}) => {
  const searchLabelId = useElementId();
  const intl = useIntl();
  const searchTypesLabel = useMemo(
    () => ({
      [SearchType.EMPLOYEE]: (labelId?: string) => (
        <FormattedMessage
          id="HEADER.SEARCH.EMPLOYEE_SEARCH"
          elementId={labelId}
        />
      ),
      [SearchType.NOTIFICATION]: (labelId?: string) => (
        <FormattedMessage
          id="HEADER.SEARCH.NOTIFICATION_SEARCH"
          elementId={labelId}
        />
      )
    }),
    []
  );
  const themedStyles = useThemedStyles();

  return (
    <Input
      className={classNames(themedStyles.searchBar, styles.searchBar)}
      value={query}
      onChange={onSearchQueryChange}
      onKeyDown={onSearchKeyDown}
      aria-describedby={props['aria-describedby']}
      label={intl.formatMessage({
        id:
          searchType === SearchType.NOTIFICATION
            ? 'HEADER.SEARCH.NOTIFICATION_SEARCH'
            : 'HEADER.SEARCH.EMPLOYEE_SEARCH'
      })}
      isInvalid={isInvalid}
      suffix={
        query.length ? (
          <Button
            buttonType={ButtonType.LINK}
            className={styles.suffix}
            onClick={onClearQuery}
          >
            <Icon
              icon={faTimesCircle}
              iconLabel={
                <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.CLEAR_ICON_LABEL" />
              }
              className={themedStyles.clearIcon}
            />
          </Button>
        ) : null
      }
      addonBefore={
        isSearchTypeSwitchAvailable ? (
          <>
            <label htmlFor={searchLabelId} className={styles.hidden}>
              {searchLabelId}
            </label>
            <Select
              id={searchLabelId}
              defaultValue={intl.formatMessage({
                id: `HEADER.SEARCH.EMPLOYEE_SEARCH`
              })}
              onChange={(value: unknown) =>
                onSearchTypeChange(value as SearchType)
              }
              className={classNames(
                themedStyles.searchTypeSelect,
                styles.searchTypeSelect
              )}
              idResolver={(value: string) =>
                intl.formatMessage({ id: `HEADER.SEARCH.${value}_SEARCH` })
              }
              data-test-el="search-type"
              dropdownClassName={themedStyles.searchTypeSelectDropdown}
              optionElements={[
                {
                  title: searchTypesLabel[SearchType.EMPLOYEE](),
                  value: SearchType.EMPLOYEE,
                  'data-test-el': 'search-type-item'
                },
                {
                  title: searchTypesLabel[SearchType.NOTIFICATION](),
                  value: SearchType.NOTIFICATION,
                  'data-test-el': 'search-type-item'
                }
              ]}
            />
          </>
        ) : (
          searchTypesLabel[searchType](searchLabelId)
        )
      }
      allowClear={true}
      addonAfter={
        isPending ? (
          <InputSpinner />
        ) : (
          <Icon
            icon={faSearch}
            iconLabel={<FormattedMessage id="COMMON.FILTERS.SEARCH" />}
            className={classNames(themedStyles.searchIcon)}
          />
        )
      }
    />
  );
};
