/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useCallback, useState, useMemo } from 'react';
import {
  anyFrom,
  required,
  CANCEL_KEY,
  Card,
  Form,
  List,
  useAsync,
  usePermissions,
  ViewCustomerDataPermissions,
  ViewNotificationPermissions,
  ThemeModel,
  withPermissions,
  createThemedStyles,
  useElementId
} from 'fineos-common';
import { GroupClientNotification, CustomerSummary } from 'fineos-js-api-client';
import { Formik } from 'formik';
import { debounce as _debounce } from 'lodash';
import classNames from 'classnames';

import {
  NotificationItem,
  EmployeeItem,
  EmptyItem,
  FooterItem,
  RefineSearchItem
} from './item';
import { search } from './search.api';
import { SearchType } from './search.type';
import { SearchInput } from './search-input.component';
import styles from './search.module.scss';

const MIN_EMPLOYEE_SEARCH = 3;
const MIN_NOTIFICATION_SEARCH = 1;
const NOTIFICATION_PREFIX = 'NTN-';

export const MAX_DISPLAY_SEARCH_AMOUNT = 5;

const normalizeNotificationQuery = (query: string) => {
  if (!query.startsWith(NOTIFICATION_PREFIX)) {
    const notificationIdRegex = /^\d+$/;

    if (notificationIdRegex.test(query)) {
      return NOTIFICATION_PREFIX + query;
    } else {
      // even though that in requirement defined only prepend notification prefix, it would be nice to
      // catch some simple mistakes like double letters, or missed letter
      const cleanUpRegex = new RegExp(
        `^(?:${NOTIFICATION_PREFIX.split('').join('|')})+`,
        'i'
      );
      return query.replace(cleanUpRegex, NOTIFICATION_PREFIX);
    }
  }

  return query;
};

const isValidEmployeeSearch = (query: string) =>
  query.length >= MIN_EMPLOYEE_SEARCH;

const isValidNotificationSearch = (query: string) => {
  const normalQuery = normalizeNotificationQuery(query).replace(
    NOTIFICATION_PREFIX,
    ''
  );
  return normalQuery.length >= MIN_NOTIFICATION_SEARCH;
};

const isSearchAvailable = (searchType: SearchType, query: string) =>
  searchType === SearchType.EMPLOYEE
    ? isValidEmployeeSearch(query)
    : isValidNotificationSearch(query);

type SearchParams = Parameters<typeof search>;

const SEARCH_DEBOUNCE_DELAY = 500; // ms

const emptyQuery = (searchType: SearchType) =>
  searchType === SearchType.NOTIFICATION ? NOTIFICATION_PREFIX : '';

const isSameSearchParams = (
  first: SearchParams | null,
  second: SearchParams | null
) => first && second && first[0] === second[0] && first[1] === second[1];

const notificationPermissionRule =
  ViewNotificationPermissions.VIEW_NOTIFICATIONS;

const customerPermissionRule = required(
  ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
  ViewCustomerDataPermissions.VIEW_CUSTOMER
);

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  searchResult: {
    '& .ant-list-split .ant-list-item': {
      borderBottom: `1px solid ${theme.colorSchema.neutral3}`
    }
  }
}));

export const Search = withPermissions(
  anyFrom(notificationPermissionRule, customerPermissionRule)
)(() => {
  const themedStyles = useThemedStyles();
  const hasNotificationsPermission = usePermissions(notificationPermissionRule);
  const hasCustomersPermission = usePermissions(customerPermissionRule);

  const [searchType, setSearchType] = useState(
    hasCustomersPermission ? SearchType.EMPLOYEE : SearchType.NOTIFICATION
  );
  const [query, setQuery] = useState(emptyQuery(searchType));
  const [performedQuery, setPerformedQuery] = useState('');
  const shouldPerformSearch = useCallback(
    () => isSearchAvailable(searchType, performedQuery),
    [searchType, performedQuery]
  );
  const {
    state: { value: searchResult, isPending, params: requestParams, error }
  } = useAsync(search, [searchType, performedQuery], {
    defaultValue: null,
    shouldExecute: shouldPerformSearch,
    noGlobalErrorHandlingFor: 400
  });
  const errorDescribeElementId = useElementId();

  const shouldRefineTheSearch =
    error && 'status' in error && error.status === 400;

  const runSearch = (currentSearchType: SearchType, currentQuery: string) => {
    if (isSearchAvailable(currentSearchType, currentQuery) && !isPending) {
      const targetQuery =
        currentSearchType === SearchType.NOTIFICATION
          ? normalizeNotificationQuery(currentQuery)
          : currentQuery;
      setQuery(targetQuery);
      setPerformedQuery(targetQuery);
    }
  };

  const debouncedSearch = useMemo(
    () => _debounce(runSearch, SEARCH_DEBOUNCE_DELAY),
    // eslint-disable-next-line
    []
  );

  const handleSearchTypeChange = (newSearchType: SearchType) => {
    setSearchType(newSearchType);
    setQuery(emptyQuery(newSearchType));
    setPerformedQuery('');
    debouncedSearch.cancel();
  };

  const handleSearchKeyDown = (e: React.KeyboardEvent) => {
    if (e.key === CANCEL_KEY) {
      setQuery(emptyQuery(searchType));
      setPerformedQuery('');
      debouncedSearch.cancel();
    }
  };

  const handleSearchQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
    setPerformedQuery('');
    debouncedSearch(searchType, e.target.value);
  };

  const handleClearQuery = () => {
    setQuery('');
    setPerformedQuery('');
    debouncedSearch.cancel();
  };

  const closePopup = () => {
    setPerformedQuery('');
    debouncedSearch.cancel();
  };

  const searchResultsLength = searchResult?.items.length || 0;

  const isSearchInputInvalid =
    !!query &&
    (searchResultsLength > MAX_DISPLAY_SEARCH_AMOUNT || !searchResultsLength);

  return (
    <Formik
      initialValues={{ query: '' }}
      onSubmit={() => debouncedSearch.flush()}
    >
      <Form
        className={styles.container}
        data-test-el="search-form"
        role="search"
      >
        <SearchInput
          query={query}
          searchType={searchType}
          isPending={isPending}
          isSearchTypeSwitchAvailable={
            hasNotificationsPermission && hasCustomersPermission
          }
          aria-describedby={errorDescribeElementId}
          isInvalid={isSearchInputInvalid}
          onSearchQueryChange={handleSearchQueryChange}
          onSearchKeyDown={handleSearchKeyDown}
          onSearchTypeChange={handleSearchTypeChange}
          onClearQuery={handleClearQuery}
        />

        {shouldRefineTheSearch &&
          isSameSearchParams([searchType, performedQuery], requestParams) && (
            <RefineSearchItem />
          )}

        {searchResult &&
          !shouldRefineTheSearch &&
          isSameSearchParams([searchType, performedQuery], requestParams) &&
          !isPending && (
            <Card
              data-test-el="search-result"
              className={classNames(
                styles.searchResult,
                themedStyles.searchResult
              )}
            >
              {searchResultsLength ? (
                <List<CustomerSummary | GroupClientNotification>
                  itemLayout="horizontal"
                  dataSource={searchResult.items.slice(
                    0,
                    MAX_DISPLAY_SEARCH_AMOUNT
                  )}
                  footer={
                    <FooterItem
                      resultsLength={searchResultsLength}
                      performedQuery={performedQuery}
                      elementId={errorDescribeElementId}
                    />
                  }
                  renderItem={(
                    item: CustomerSummary | GroupClientNotification
                  ) => (
                    <>
                      {searchResult.type === SearchType.NOTIFICATION ? (
                        <NotificationItem
                          item={item as GroupClientNotification}
                          closePopup={closePopup}
                        />
                      ) : (
                        <EmployeeItem
                          item={item as CustomerSummary}
                          closePopup={closePopup}
                        />
                      )}
                    </>
                  )}
                />
              ) : (
                <EmptyItem
                  elementId={errorDescribeElementId}
                  searchResult={searchResult}
                  performedQuery={performedQuery}
                />
              )}
            </Card>
          )}
      </Form>
    </Formik>
  );
});
