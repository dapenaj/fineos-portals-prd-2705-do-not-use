/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext } from 'react';
import {
  PageLayout as BasePageLayout,
  Link,
  AdminPermissions,
  usePermissions,
  MessagesPermissions,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  PermissionsScope,
  TextBadge,
  EVPPermissions,
  linkThemedStylesProducer
} from 'fineos-common';
import classNames from 'classnames';
import { SdkConfig } from 'fineos-js-api-client';

import { HighContrastModeContext } from '../../modules/high-contrast-mode/high-contrast-mode.context';
import { LogoutAction } from '../../modules/logout';
import { AuthenticationService } from '../authentication';
import { useMessageCounter } from '../utils';

import { Search } from './search/search.component';
import { LanguageMenu } from './language/language-menu.component';
import styles from './page-layout.module.scss';

type PageLayoutProps = Pick<
  React.ComponentProps<typeof BasePageLayout>,
  'children' | 'sideBar' | 'contentHeader' | 'showBackgroundImage'
>;

const getUserOptions = (hasAdminPermissions: boolean): React.ReactElement[] => {
  const userOptions: React.ReactElement[] = [];
  if (hasAdminPermissions) {
    userOptions.push(
      <FormattedMessage
        key={0}
        id="USER_ACTIONS.CONTROL_USER_ACCESS"
        as={Link}
        to="/control-user-access"
      />
    );
  }
  userOptions.push(<LogoutAction key={1} />);
  return userOptions;
};

const userIdToName = (userId: string) => {
  return userId
    .replace(/@.+$/, '')
    .split(/[^\w-]+/g)
    .map(token =>
      token
        .split('-')
        .filter(Boolean)
        .map(
          nameToken =>
            nameToken[0].toLocaleUpperCase() +
            nameToken.substr(1).toLocaleLowerCase()
        )
        .join('-')
    )
    .join(' ');
};

const useThemedStyles = createThemedStyles(theme => ({
  appLayout: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));
const useLinkThemedStyles = createThemedStyles(linkThemedStylesProducer);

export const PageLayout = React.memo(
  ({
    children,
    contentHeader,
    sideBar,
    showBackgroundImage
  }: PageLayoutProps) => {
    const { permissions } = useContext(PermissionsScope);
    const { isEnabled, onIsEnabledChange } = useContext(
      HighContrastModeContext
    );
    const isAuthorized = permissions !== null && permissions.size > 0;

    const hasAdminPermissions = usePermissions([
      AdminPermissions.VIEW_USERS_LIST,
      AdminPermissions.EDIT_USER_DETAILS
    ]);

    const hasWebMessagesPermissions = usePermissions([
      MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
      MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
      MessagesPermissions.ADD_CASE_WEB_MESSAGE,
      MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
    ]);

    const hasEVPAccessPermission = usePermissions(
      EVPPermissions.ACCESS_BILLING_PORTAL
    );

    const userId = AuthenticationService.getInstance().displayUserId || '';

    const themedStyles = useThemedStyles();
    const linkThemedStyles = useLinkThemedStyles();
    const { unreadMessagesCount } = useMessageCounter();

    const getNavigationLinks = (): React.ReactElement[] => {
      const navigationLinks: React.ReactElement[] = [
        <FormattedMessage
          to="/"
          key="/"
          exact={true}
          as={Link}
          className={styles.link}
          id="NAVIGATION.MY_DASHBOARD"
        />
      ];
      if (hasWebMessagesPermissions) {
        navigationLinks.push(
          <Link to="/messages" key="/messages" exact={true}>
            <TextBadge
              key="/messages"
              label={unreadMessagesCount > 99 ? '+99' : unreadMessagesCount}
              isLabelVisible={!!unreadMessagesCount}
              data-test-el="messages-page-with-label"
            >
              <FormattedMessage
                className={styles.link}
                id="NAVIGATION.MESSAGES"
              />
            </TextBadge>
          </Link>
        );
      }
      if (hasEVPAccessPermission) {
        navigationLinks.push(
          <a
            href={SdkConfig.getPath(['employerViewpointUrl'], '')}
            className={classNames(styles.link, linkThemedStyles.link)}
            key={'/billing-portal'}
            data-test-el="billing-link"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FormattedMessage className={styles.link} id="NAVIGATION.BILLING" />
          </a>
        );
      }
      return navigationLinks;
    };
    return (
      <BasePageLayout
        sideBar={sideBar}
        contentHeader={contentHeader}
        showBackgroundImage={showBackgroundImage}
        userName={
          isAuthorized
            ? userIdToName(
                AuthenticationService.getInstance().displayUserId || ''
              )
            : ''
        }
        userId={userId}
        navigation={getNavigationLinks()}
        isContrastModeEnabled={!!isEnabled}
        onContrastModeToggle={(isModeEnabled: boolean) =>
          onIsEnabledChange(isModeEnabled)
        }
        userOptions={isAuthorized ? getUserOptions(hasAdminPermissions) : []}
        headerRightContent={
          <>
            <LanguageMenu userId={userId} />
            <Search />
          </>
        }
        className={classNames('app-layout', themedStyles.appLayout)}
      >
        {children}
      </BasePageLayout>
    );
  }
);
