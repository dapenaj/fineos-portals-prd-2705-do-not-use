/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect, useContext } from 'react';
import { useIntl } from 'react-intl';
import { Menu } from 'antd';
import {
  DropdownMenu,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Button,
  ButtonType,
  encryptedUserId,
  Icon
} from 'fineos-common';
import { CaretDownOutlined } from '@ant-design/icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import { PortalEnvConfigService } from '../../../shared';
import { IntakeContext } from '../../../modules/intake/common/intake-context';
import { LanguageContext } from './language.context';

import styles from './language-menu.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  languageLabel: textStyleToCss(theme.typography.baseText)
}));

type LanguageMenuProps = {
  userId: string;
};

export const LanguageMenu: React.FC<LanguageMenuProps> = ({ userId }) => {
  const intl = useIntl();
  const themedStyles = useThemedStyles();
  const [languages, setLanguages] = useState<string[]>([]);
  const { language: languageContext, setLanguage } = useContext(
    LanguageContext
  );
  const [isVisible, setIsVisible] = useState(false);
  const intakeContext = useContext(IntakeContext);
  const message = intl.formatMessage({
    id: 'INTAKE.ABANDON_INTAKE_MODAL'
  });

  useEffect(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    async function getLanguage() {
      const clientConfig = await portalEnvConfigService.fetchPortalEnvConfig();
      setLanguages(clientConfig.supportedLanguages);
    }
    getLanguage();
  }, [languageContext]);

  const shouldChangeLanguage = () => {
    if (intakeContext) {
      const { isFirstStep, navigationLock, isIntakeStarted } = intakeContext;
      const isIntakeLocked = navigationLock.isLocked;
      const getConfirmFromFirstStep = () =>
        !(isIntakeLocked || isIntakeStarted) || window.confirm(message);
      return isFirstStep ? getConfirmFromFirstStep() : window.confirm(message);
    }
    return true;
  };

  const overlay: JSX.Element = (
    <Menu data-test-el="language-dropdown">
      <Menu.ItemGroup
        title={intl.formatMessage({
          id: 'LANGUAGE.TITLE'
        })}
      >
        {languages.map((language: string, index: number) => (
          <Menu.Item
            className={styles.menuItem}
            key={`language-dropdown-${index}`}
          >
            <FormattedMessage
              key={index}
              as={Button}
              buttonType={ButtonType.LINK}
              onClick={() => {
                if (shouldChangeLanguage()) {
                  setLanguage(language as string);
                  localStorage.setItem(
                    encryptedUserId(userId, 'locale'),
                    language
                  );
                }
                setIsVisible(false);
              }}
              className={styles.button}
              id={`LANGUAGE.${language.toLocaleUpperCase()}`}
            />
          </Menu.Item>
        ))}
      </Menu.ItemGroup>
    </Menu>
  );

  return languageContext && languages.length > 1 ? (
    <div>
      <DropdownMenu
        data-test-el="language-dropdown"
        overlay={overlay}
        visible={isVisible}
        className={styles.wrapper}
        onVisibleChange={(visible: boolean) => setIsVisible(visible)}
        trigger={['click']}
        placement="bottomRight"
      >
        <span
          className={styles.trigger}
          tabIndex={0}
          onKeyDown={({ key }: React.KeyboardEvent<HTMLSpanElement>) =>
            key === 'Enter' && setIsVisible(true)
          }
        >
          <span
            className={classNames(
              themedStyles.languageLabel,
              styles.languageLabel
            )}
            data-test-el="language-options-dropdown-label"
          >
            <Icon
              className={styles.icon}
              icon={faGlobe}
              iconLabel={<FormattedMessage id="LANGUAGE.ARIA_LABEL" />}
            />
            {intl.formatMessage({
              id: `LANGUAGE.${languageContext?.toLocaleUpperCase()}`
            })}
          </span>
          {languages.length && <CaretDownOutlined />}
        </span>
      </DropdownMenu>
    </div>
  ) : null;
};
