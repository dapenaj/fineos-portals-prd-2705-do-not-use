/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useEffect } from 'react';
import { Redirect, Switch, useHistory } from 'react-router-dom';
import {
  ViewCustomerDataPermissions,
  AdminPermissions,
  ErrorPage,
  PermissionsScope,
  ErrorContext,
  MessagesPermissions,
  FormattedMessage
} from 'fineos-common';

import { Home } from './modules/home/home.component';
import { MyDashboard } from './modules/my-dashboard/my-dashboard.component';
import { Messages } from './modules/messages/messages.component';
import { EmployeeProfile } from './modules/employee-profile';
import { Notification } from './modules/notification';
import { EstablishedIntake, NonEstablishedIntake } from './modules/intake';
import { ControlUserAccess } from './modules/control-access-user';
import { LogoutPage } from './modules/logout';
import { SessionTimeoutPage } from './modules/session';
import { Route, Features } from './shared';

export const Routes = ({
  features,
  isLoggedOut
}: {
  features: Features;
  isLoggedOut?: boolean;
}) => {
  const { permissions, logout } = useContext(PermissionsScope);
  const { isForbidden } = useContext(ErrorContext);
  const isAuthorized = permissions !== null && permissions.size > 0;
  const history = useHistory();
  const { showDashboard, helpInfoEnabled } = features;

  useEffect(() => {
    if (isForbidden) {
      history.push('/session-timeout');
      logout();
    }
  }, [isForbidden, logout, history]);

  return (
    <Switch>
      <Route
        path="/logout"
        exact={true}
        title={<FormattedMessage id="PAGE_TITLES.LOGOUT" />}
        render={() => <LogoutPage />}
      />

      <Route
        path="/session-timeout"
        exact={true}
        title={<FormattedMessage id="PAGE_TITLES.SESSION_TIMEOUT" />}
        render={() => <SessionTimeoutPage />}
      />

      {isAuthorized && (
        <Route
          path="/messages"
          title={<FormattedMessage id="PAGE_TITLES.MESSAGES" />}
          exact={true}
          permissions={[
            MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
            MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
            MessagesPermissions.ADD_CASE_WEB_MESSAGE,
            MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
          ]}
          render={() => <Messages />}
        />
      )}

      {isAuthorized && (
        <Route
          path="/profile/:employeeId"
          permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER]}
          title={<FormattedMessage id="PAGE_TITLES.EMPLOYEE" />}
          render={({ match: { params } }) => (
            <EmployeeProfile employeeId={params.employeeId} />
          )}
        />
      )}

      {isAuthorized && (
        <Route
          path="/notification/:notificationId"
          title={<FormattedMessage id="PAGE_TITLES.NOTIFICATION" />}
          render={({ match: { params } }) => (
            <Notification notificationId={params.notificationId} />
          )}
        />
      )}

      {isAuthorized && (
        <Route
          path="/control-user-access"
          title={<FormattedMessage id="PAGE_TITLES.CONTROL_USER_ACCESS" />}
          permissions={[
            AdminPermissions.VIEW_USERS_LIST,
            AdminPermissions.EDIT_USER_DETAILS
          ]}
          render={() => <ControlUserAccess />}
        />
      )}

      {isAuthorized && (
        <Route
          path="/employee/intake"
          title={<FormattedMessage id="PAGE_TITLES.NOT_ESTABLISHED_INTAKE" />}
          permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
          render={() => <NonEstablishedIntake />}
        />
      )}

      {isAuthorized && (
        <Route
          path="/employee/:employeeId/intake"
          title={<FormattedMessage id="PAGE_TITLES.ESTABLISHED_INTAKE" />}
          render={({ match: { params } }) => (
            <EstablishedIntake employeeId={params.employeeId} />
          )}
        />
      )}

      {isLoggedOut && <Redirect to="/logout" />}

      <Route
        path="/"
        exact={true}
        render={({ location }) =>
          location.pathname === '/' ? (
            showDashboard ? (
              <MyDashboard helpInfoEnabled={helpInfoEnabled} />
            ) : (
              <Home />
            )
          ) : (
            <ErrorPage />
          )
        }
      />

      <Route
        title={<FormattedMessage id="PAGE_TITLES.ERROR" />}
        render={() => <ErrorPage />}
      />
    </Switch>
  );
};
