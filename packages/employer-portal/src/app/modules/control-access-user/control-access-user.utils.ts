import { BaseDomain, GroupClientUser } from 'fineos-js-api-client';

import { AuthenticationService } from '../../shared';

import { UserStatusName } from './control-user-access.enum';

export const usersWithoutCurrentUser = (users: GroupClientUser[]) => {
  const currentUserId = AuthenticationService.getInstance().displayUserId?.toLowerCase();
  return users.filter(
    user => user.userReferenceIdentifier?.toLowerCase() !== currentUserId
  );
};

export const getStatusToSet = (
  statusDomains: BaseDomain[],
  name: UserStatusName
) => {
  const searchedStatus = statusDomains.find(domain => domain.name === name);
  return {
    domainId: searchedStatus!.domainId,
    domainName: searchedStatus!.domainName,
    fullId: searchedStatus!.fullId,
    name: searchedStatus!.name
  };
};
