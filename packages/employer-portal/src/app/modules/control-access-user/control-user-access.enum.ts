export enum UserStatusName {
  ACTIVE = 'Active',
  DISABLED_MANUALLY = 'Disabled - Manually'
}
