import { GroupClientUser } from 'fineos-js-api-client';

export type ControlUserAccessForm = {
  query: string;
  changedUserId: string | null;
  users: GroupClientUser[];
};

export const controlAccessUserInitialValues = (
  users: GroupClientUser[]
): ControlUserAccessForm => ({
  query: '',
  changedUserId: null,
  users
});
