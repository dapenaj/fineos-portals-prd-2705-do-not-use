/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientUserService,
  GroupClientUser,
  ConcurrencyUpdateError
} from 'fineos-js-api-client';

import { usersWithoutCurrentUser } from './control-access-user.utils';

export const fetchUsers = async (): Promise<GroupClientUser[]> => {
  const { elements } = await GroupClientUserService.getInstance().fetchUsers();
  return usersWithoutCurrentUser(elements);
};

export const updateUser = async (
  userToUpdate: GroupClientUser
): Promise<GroupClientUser> => {
  let groupClientUser: GroupClientUser;

  try {
    groupClientUser = await GroupClientUserService.getInstance().updateUser(
      userToUpdate
    );
    // we need to get new etags
    await fetchUsers();
  } catch (error) {
    if (error instanceof ConcurrencyUpdateError) {
      throw new ConcurrencyUpdateError(error.remoteData.elements);
    }
  }

  return groupClientUser! as GroupClientUser;
};
