/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Field } from 'formik';
import {
  createThemedStyles,
  textStyleToCss,
  ContentRenderer,
  List,
  Button,
  ButtonType,
  Popconfirm,
  EmptyState,
  Input,
  toRgba,
  useAsync,
  ConfigurableText,
  getConfigurableText,
  Spinner,
  FormattedMessage,
  Icon,
  useToast,
  ToastType,
  FormGroup,
  darkenColor,
  Form
} from 'fineos-common';
import { BaseDomain, GroupClientUser } from 'fineos-js-api-client';
import { faCheck, faTimes, faSearch } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import { Row, Col } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';
import { useIntl } from 'react-intl';

import {
  PageLayout,
  FormikWithResponseHandling,
  useEnumDomain,
  EnumDomain
} from '../../shared';

import {
  controlAccessUserInitialValues,
  ControlUserAccessForm
} from './control-access-user.form';
import { fetchUsers, updateUser } from './control-access-user.api';
import {
  usersWithoutCurrentUser,
  getStatusToSet
} from './control-access-user.utils';
import { UserStatusName } from './control-user-access.enum';
import styles from './control-access-user.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  },
  statusIcon: {
    color: theme.colorSchema.successColor
  },
  disabledIcon: {
    color: theme.colorSchema.neutral6
  },
  searchIcon: {
    color: theme.colorSchema.neutral6
  },
  disabled: {
    color: theme.colorSchema.neutral7
  },
  button: {
    color: darkenColor(theme.colorSchema, 'attentionColor')
  },
  wrapper: {
    backgroundColor: theme.colorSchema.neutral1
  },
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  },
  description: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  search: {
    '& .ant-input-search': {
      backgroundColor: theme.colorSchema.neutral1,
      boxShadow: `inset 0px 2px 4px ${toRgba(
        theme.colorSchema,
        'neutral7',
        0.15
      )}`
    },
    '& .ant-input-affix-wrapper': {
      backgroundColor: theme.colorSchema.neutral1,
      border: `1px solid ${theme.colorSchema.neutral6}`
    },
    '& .ant-input-group-addon': {
      backgroundColor: theme.colorSchema.neutral1,
      border: `1px solid ${theme.colorSchema.neutral6}`,
      color: theme.colorSchema.neutral6,
      '&:last-child': {
        borderLeft: 0
      }
    }
  },
  searchQuery: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const ControlUserAccess = () => {
  const [mutations, setMutations] = useState({} as { [key: string]: boolean });
  const [processingUserId, setProcessingUserId] = useState<string | null>(null);
  const { show } = useToast();
  const themedStyles = useThemedStyles();

  const {
    state: { isPending, value: users, error }
  } = useAsync(fetchUsers, [], {
    defaultValue: []
  });
  const { domains: accountStatusDomains } = useEnumDomain(
    EnumDomain.ACCOUNT_STATUS
  );

  const intl = useIntl();

  const getFilteredUsers = (query: string, actualUsers: GroupClientUser[]) =>
    actualUsers
      .filter(user =>
        user?.nameOfUser?.toLowerCase().includes(query?.toLowerCase())
      )
      .map(user => ({
        ...user,
        enabled: user.id in mutations ? mutations[user.id] : user.enabled
      }));

  const handleChange = (
    initialUsers: GroupClientUser[],
    currentUser: GroupClientUser,
    setFieldValue: (fieldName: string, value: unknown) => void,
    submitForm: () => void
  ) => {
    const index = initialUsers.findIndex(
      initialUser => initialUser.id === currentUser.id
    );
    const newUsers = [...initialUsers];
    const statusToSet = !_isEmpty(accountStatusDomains)
      ? getStatusToSet(
          accountStatusDomains!,
          currentUser.enabled
            ? UserStatusName.DISABLED_MANUALLY
            : UserStatusName.ACTIVE
        )
      : {};

    newUsers[index] = {
      ...newUsers[index],
      enabled: !currentUser.enabled,
      status: statusToSet as BaseDomain
    };
    setFieldValue('changedUserId', currentUser.id);
    setFieldValue('users', newUsers);
    submitForm();
  };

  const handleSubmit = async (formData: ControlUserAccessForm) => {
    const processingUserIndex = formData.users.findIndex(
      user => user.id === formData.changedUserId
    );
    try {
      setProcessingUserId(formData.users[processingUserIndex].id);
      const updatedUser = await updateUser(formData.users[processingUserIndex]);

      setMutations({ ...mutations, [updatedUser.id]: updatedUser.enabled });
      const id = updatedUser.enabled
        ? 'CONTROL_USER_ACCESS.ENABLED_USER_INFO'
        : 'CONTROL_USER_ACCESS.DISABLED_USER_INFO';
      show(
        ToastType.SUCCESS,
        <span>
          {getConfigurableText(intl, id, {
            user: updatedUser.nameOfUser
          })}
        </span>
      );
    } finally {
      setProcessingUserId(null);
    }
  };

  const getActions = (
    item: GroupClientUser,
    submitForm: () => void,
    setFieldValue: (fieldName: string, value: unknown) => void
  ) => {
    if (processingUserId === item.id) {
      return [<Spinner key={0} className={styles.spinner} size="small" />];
    }

    if (item.enabled) {
      return [
        <Popconfirm
          key={0}
          ariaLabelId="CONTROL_USER_ACCESS.DISABLE_USER_ARIA_LABEL"
          trigger={
            <FormattedMessage
              id="CONTROL_USER_ACCESS.DISABLE_USER"
              ariaLabelName={{
                descriptor: {
                  id: 'CONTROL_USER_ACCESS.DISABLE_USER_ARIA_LABEL'
                },
                values: { userName: item.nameOfUser }
              }}
              className={classNames({
                [themedStyles.button]: item.enabled
              })}
            />
          }
          popconfirmMessage={
            <ConfigurableText id="CONTROL_USER_ACCESS.CHANGE_PERMISSIONS" />
          }
          onConfirm={() => handleChange(users, item, setFieldValue, submitForm)}
        />
      ];
    }

    return [
      <FormattedMessage
        id="CONTROL_USER_ACCESS.ENABLE_USER"
        ariaLabelName={{
          descriptor: { id: 'CONTROL_USER_ACCESS.ENABLE_USER_ARIA_LABEL' },
          values: { userName: item.nameOfUser }
        }}
        key={0}
        className={classNames({
          [themedStyles.button]: item.enabled
        })}
        onClick={() => handleChange(users, item, setFieldValue, submitForm)}
        as={Button}
        buttonType={ButtonType.LINK}
      />
    ];
  };
  return (
    <PageLayout
      contentHeader={
        <FormattedMessage
          data-test-el="control-access-user-title"
          id="USER_ACTIONS.CONTROL_USER_ACCESS"
          as="h1"
          className={classNames(styles.header, themedStyles.header)}
        />
      }
    >
      <ContentRenderer
        error={error}
        className={styles.container}
        isLoading={isPending}
        isEmpty={_isEmpty(users)}
        emptyContent={
          <EmptyState withBorder={true}>
            <FormattedMessage id="CONTROL_USER_ACCESS.EMPTY" />
          </EmptyState>
        }
      >
        <FormikWithResponseHandling
          initialValues={controlAccessUserInitialValues(users)}
          onSubmit={handleSubmit}
          dataParseFunction={data => ({
            query: '',
            changedUserId: null,
            users: usersWithoutCurrentUser(data)
          })}
        >
          {({ submitForm, setFieldValue, values }) => (
            <Form data-test-el="control-user-access-form">
              <Row>
                <Col
                  xs={12}
                  sm={6}
                  className={classNames(styles.search, themedStyles.search)}
                >
                  <FormattedMessage id="CONTROL_USER_ACCESS.SEARCH_PLACEHOLDER">
                    {(extraParams, placeholder) => (
                      <FormGroup
                        label={
                          <FormattedMessage id="CONTROL_USER_ACCESS.SEARCH" />
                        }
                      >
                        <Field
                          addonAfter={
                            <Icon
                              className={themedStyles.searchIcon}
                              icon={faSearch}
                              iconLabel="CONTROL_USER_ACCESS.SEARCH_ICON_LABEL"
                            />
                          }
                          component={Input}
                          name="query"
                          allowClear={true}
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ) => setFieldValue('query', event.target.value)}
                          onSearch={(query: string) =>
                            setFieldValue('query', query)
                          }
                          placeholder={placeholder}
                          {...extraParams}
                        />
                      </FormGroup>
                    )}
                  </FormattedMessage>
                </Col>
              </Row>
              {values.query && (
                <Row
                  className={classNames(
                    styles.searchQuery,
                    themedStyles.searchQuery
                  )}
                >
                  <Col xs={24} sm={12}>
                    <FormattedMessage
                      id="CONTROL_USER_ACCESS.RESULTS_FOR"
                      values={{
                        searchQuery: values.query,
                        results: getFilteredUsers(values.query, values.users)
                          .length
                      }}
                    />
                  </Col>
                </Row>
              )}
              {!_isEmpty(getFilteredUsers(values.query, values.users)) && (
                <List<GroupClientUser>
                  data-test-el="control-access-user-list"
                  dataSource={getFilteredUsers(values.query, values.users)}
                  renderItem={item => (
                    <List.Item
                      data-test-el="control-access-user-list-item"
                      className={classNames(
                        themedStyles.wrapper,
                        styles.wrapper
                      )}
                      actions={getActions(item, submitForm, setFieldValue)}
                    >
                      <List.Item.Meta
                        className={styles.body}
                        avatar={
                          <Icon
                            icon={item.enabled ? faCheck : faTimes}
                            className={classNames(
                              styles.statusIcon,
                              themedStyles.statusIcon,
                              { [themedStyles.disabledIcon]: !item.enabled }
                            )}
                            iconLabel={
                              <FormattedMessage
                                id={`CONTROL_USER_ACCESS.${
                                  item.enabled
                                    ? 'ENABLED_USER_ARIA_LABEL'
                                    : 'DISABLED_USER_ARIA_LABEL'
                                }`}
                                values={{ userName: item.nameOfUser }}
                              />
                            }
                          />
                        }
                        description={
                          <>
                            <div
                              data-test-el="control-access-user-username"
                              className={classNames(
                                styles.title,
                                themedStyles.title,
                                {
                                  [themedStyles.disabled]: !item.enabled
                                }
                              )}
                            >
                              <span
                                title={item.nameOfUser}
                                className={styles.userName}
                              >
                                {item.nameOfUser}
                              </span>
                              {!item.enabled && (
                                <span
                                  data-test-el="control-access-user-user-role"
                                  className={classNames(
                                    themedStyles.description,
                                    styles.description,
                                    themedStyles.disabled
                                  )}
                                >
                                  (
                                  <FormattedMessage id="CONTROL_USER_ACCESS.DISABLED" />
                                  )
                                </span>
                              )}
                            </div>
                            <div
                              data-test-el="control-access-user-user-role"
                              className={classNames(
                                themedStyles.description,
                                themedStyles.disabled
                              )}
                            >
                              {item.role?.name}
                            </div>
                          </>
                        }
                      />
                    </List.Item>
                  )}
                />
              )}
            </Form>
          )}
        </FormikWithResponseHandling>
      </ContentRenderer>
    </PageLayout>
  );
};
