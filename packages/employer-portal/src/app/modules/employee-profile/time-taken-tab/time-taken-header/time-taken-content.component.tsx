/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Col, Row } from 'antd';
import {
  Tabs,
  FormattedMessage,
  createThemedStyles,
  PropertyItem,
  TimeAmount,
  textStyleToCss
} from 'fineos-common';
import { EmployeeLeaveBalance } from 'fineos-js-api-client';

import { CalculationMethod } from '../time-taken.api';
import { getNumberDecimalUnitsReference } from '../time-taken.util';
import { TimeTakenTooltip } from '../time-taken-tooltip';

import styles from './time-taken-header.module.scss';

type TimeTakenContentProps = React.ComponentProps<typeof TabPane> & {
  employeeLeaveBalance: EmployeeLeaveBalance;
  leavePlanName: string;
  calculationMethod: string;
};

const TabPane = Tabs.TabPane;

const useThemedStyles = createThemedStyles(theme => ({
  cardTitle: {
    ...textStyleToCss(theme.typography.panelTitle),
    color: theme.colorSchema.neutral8
  },
  availableBalance: {
    ...textStyleToCss(theme.typography.displaySmall)
  },
  notificationMessage: {
    ...textStyleToCss(theme.typography.smallText)
  }
}));

const getMethodTypeName = (calculationMethod: string) => {
  return [
    CalculationMethod.CALENDAR_YEAR,
    CalculationMethod.FIXED_YEAR
  ].includes(calculationMethod as CalculationMethod)
    ? 'YEAR'
    : 'ROLLING';
};

/**
 * TimeTakenContent component provide value for time taken tab
 */
export const TimeTakenContent: React.FC<TimeTakenContentProps> = ({
  employeeLeaveBalance,
  leavePlanName,
  calculationMethod
}) => {
  const themedStyles = useThemedStyles();

  return (
    <>
      <Row className={themedStyles.cardTitle}>{leavePlanName}</Row>
      <Row>
        <Col span={14}>
          <Row>
            <Col span={16} className={styles.colPadding}>
              <PropertyItem
                label={
                  <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.TIME_AVAILABLE" />
                }
              >
                <div className={themedStyles.availableBalance}>
                  <TimeAmount
                    numberDecimalUnitsReference={getNumberDecimalUnitsReference(
                      employeeLeaveBalance
                    )}
                    amount={employeeLeaveBalance.availableBalance}
                    timeBasis={employeeLeaveBalance.timeBasis}
                  />
                </div>
              </PropertyItem>
            </Col>
            <Col span={8} className={styles.col}>
              <PropertyItem
                label={
                  <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.APPROVED_TIME" />
                }
              >
                <TimeAmount
                  numberDecimalUnitsReference={getNumberDecimalUnitsReference(
                    employeeLeaveBalance
                  )}
                  amount={employeeLeaveBalance.approvedTime}
                  timeBasis={employeeLeaveBalance.timeBasis}
                />
              </PropertyItem>
            </Col>
          </Row>
          <Row>
            <Col span={16} className={styles.colPadding}>
              <PropertyItem>
                <div className={themedStyles.notificationMessage}>
                  {employeeLeaveBalance.notificationMessage}
                </div>
              </PropertyItem>
            </Col>
            <Col span={8} className={styles.col}>
              <PropertyItem
                label={
                  <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.PENDING_TIME" />
                }
              >
                <TimeAmount
                  numberDecimalUnitsReference={getNumberDecimalUnitsReference(
                    employeeLeaveBalance
                  )}
                  amount={employeeLeaveBalance.pendingTime}
                  timeBasis={employeeLeaveBalance.timeBasis}
                />
              </PropertyItem>
            </Col>
          </Row>
        </Col>
        <Col span={10}>
          <Row>
            <Col span={24} className={styles.col}>
              <PropertyItem
                label={
                  <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.TOTAL_PLAN_ENTITLEMENT.LABEL" />
                }
              >
                <FormattedMessage
                  id={`PROFILE.TIME_TAKEN_TAB.TOTAL_PLAN_ENTITLEMENT.VALUE.${getMethodTypeName(
                    calculationMethod
                  )}`}
                  values={{
                    timeEntitlement: employeeLeaveBalance.timeEntitlement,
                    timeBasis: employeeLeaveBalance.timeBasis.toLocaleLowerCase(),
                    timeWithinPeriod: employeeLeaveBalance.timeWithinPeriod,
                    timeWithinPeriodBasis: employeeLeaveBalance.timeWithinPeriodBasis.toLocaleLowerCase()
                  }}
                />
              </PropertyItem>
            </Col>
            <Col span={24} className={styles.col}>
              <PropertyItem
                label={
                  <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.CALCULATION_METHOD" />
                }
              >
                {calculationMethod}
                <TimeTakenTooltip calculationMethod={calculationMethod} />
              </PropertyItem>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};
