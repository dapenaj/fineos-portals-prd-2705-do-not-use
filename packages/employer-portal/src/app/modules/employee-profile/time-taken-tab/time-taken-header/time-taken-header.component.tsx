/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Tabs, CollapsibleCard, Card } from 'fineos-common';
import { EmployeeLeaveBalance } from 'fineos-js-api-client';

import { EntitlementWindow } from '../entitlement-window';
import { AbsencePeriodDecisionsForTimeOff } from '../time-taken.util';

import styles from './time-taken-header.module.scss';
import { TimeTakenContent } from './time-taken-content.component';

const TabPane = Tabs.TabPane;

type TimeTakenHeaderProps = React.ComponentProps<typeof TabPane> & {
  employeeLeaveBalance: EmployeeLeaveBalance;
  leavePlanName: string;
  calculationMethod: string;
  absencePeriodDecisionsWithTimeOff:
    | AbsencePeriodDecisionsForTimeOff[]
    | undefined;
  active?: boolean;
};

/**
 * TimeTakenHeader component provide value for time taken tab
 */
export const TimeTakenHeader: React.FC<TimeTakenHeaderProps> = ({
  employeeLeaveBalance,
  leavePlanName,
  calculationMethod,
  absencePeriodDecisionsWithTimeOff,
  ...props
}) => {
  const hideTimeline = false;

  return hideTimeline ? (
    <CollapsibleCard
      key={leavePlanName}
      className={styles.wrapper}
      isOpen={true}
      isPanelUI={true}
      title={
        <TimeTakenContent
          employeeLeaveBalance={employeeLeaveBalance}
          leavePlanName={leavePlanName}
          calculationMethod={calculationMethod}
        />
      }
    >
      {absencePeriodDecisionsWithTimeOff && (
        <EntitlementWindow
          active={props.active}
          employeeLeaveBalance={employeeLeaveBalance}
          decisions={
            absencePeriodDecisionsWithTimeOff.find(
              absencePeriodDecision =>
                absencePeriodDecision.leavePlanName === leavePlanName
            )?.decisions || []
          }
        />
      )}
    </CollapsibleCard>
  ) : (
    <Card key={leavePlanName} className={styles.wrapper}>
      <TimeTakenContent
        employeeLeaveBalance={employeeLeaveBalance}
        leavePlanName={leavePlanName}
        calculationMethod={calculationMethod}
      />
    </Card>
  );
};
