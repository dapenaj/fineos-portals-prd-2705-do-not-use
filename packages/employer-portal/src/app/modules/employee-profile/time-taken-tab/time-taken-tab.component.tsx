/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo, useState } from 'react';
import {
  ContentRenderer,
  EmptyState,
  FormattedMessage,
  Tabs,
  useAsync,
  usePermissions,
  ViewNotificationPermissions
} from 'fineos-common';
import { isEmpty as _isEmpty } from 'lodash';
import moment from 'moment';
import { GroupClientNotification, SubCaseType } from 'fineos-js-api-client';

import { getFilteredAbsencePeriodDecisions } from '../../../shared';
import {
  EMPTY_NOTIFICATIONS_LIST,
  fetchNotifications
} from '../notifications-tab/notifications-tab.api';

import {
  EmployeeLeaveBalanceLeavePlan,
  fetchAbsencePeriodDecisions,
  fetchLeaveAvailability
} from './time-taken.api';
import { getAbsencePeriodDecisionsForTimeOff } from './time-taken.util';
import { TimeTakenHeader } from './time-taken-header';

type TimeTakenTabProps = React.ComponentProps<typeof TabPane> & {
  /**
   * Current user id
   */
  employeeId: string;
  active?: boolean;
};

const TabPane = Tabs.TabPane;

/**
 * TimeTakenTab component provide value for time taken tab
 */
export const TimeTakenTab: React.FC<TimeTakenTabProps> = ({
  employeeId,
  ...props
}) => {
  const hasAbsencePeriodDecisionsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS
  );
  const hasAbsenceLeavePlansPermission = usePermissions(
    ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
  );

  const [startDate] = useState(moment().subtract(18, 'months'));
  const [endDate] = useState(moment().add(6, 'months'));
  const {
    state: {
      value: { data: notifications }
    }
  } = useAsync(fetchNotifications, [employeeId], {
    defaultValue: EMPTY_NOTIFICATIONS_LIST
  });

  const {
    state: {
      value: absencePeriodDecisions,
      isPending: isPendingAbsencePeriodDecisions
    }
  } = useAsync(
    fetchAbsencePeriodDecisions,
    // we pass though notifications, however it's not existing in call implementation
    // just used in condition if call should be executed or not
    [employeeId, startDate, endDate, notifications] as any,
    {
      // _1, _2, _3 - it's original call parameters, however as they not needed they have
      // never type
      shouldExecute: ((
        _1: never,
        _2: never,
        _3: never,
        employeeNotifications: GroupClientNotification[]
      ) =>
        hasAbsencePeriodDecisionsPermission &&
        employeeNotifications.some(notification =>
          notification.subCases.some(
            subCase => subCase.caseType === SubCaseType.ABSENCE_TYPE
          )
        )) as any
    }
  );

  const absencePeriodDecisionsWithTimeOff = useMemo(() => {
    if (absencePeriodDecisions && absencePeriodDecisions.decisions) {
      return getAbsencePeriodDecisionsForTimeOff(
        getFilteredAbsencePeriodDecisions(absencePeriodDecisions)
      );
    }
  }, [absencePeriodDecisions]);

  const {
    state: {
      value: employeeLeaveBalances,
      isPending: isPendingEmployeeLeaveBalances
    }
  } = useAsync(
    fetchLeaveAvailability,
    [employeeId, absencePeriodDecisionsWithTimeOff!],
    {
      shouldExecute: () =>
        hasAbsenceLeavePlansPermission &&
        !!absencePeriodDecisionsWithTimeOff &&
        !isPendingAbsencePeriodDecisions,
      defaultValue: []
    }
  );

  return (
    <TabPane {...props} data-test-el="time-taken-tab">
      <ContentRenderer
        isLoading={
          isPendingAbsencePeriodDecisions ||
          (isPendingEmployeeLeaveBalances && _isEmpty(employeeLeaveBalances))
        }
        isEmpty={
          !isPendingAbsencePeriodDecisions &&
          !isPendingEmployeeLeaveBalances &&
          _isEmpty(employeeLeaveBalances)
        }
        emptyContent={
          <EmptyState withBorder={true}>
            <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.EMPTY_STATE" />
          </EmptyState>
        }
      >
        {employeeLeaveBalances &&
          employeeLeaveBalances.map(
            ({
              employeeLeaveBalance,
              leavePlanName,
              calculationMethod
            }: EmployeeLeaveBalanceLeavePlan) =>
              employeeLeaveBalance && (
                <TimeTakenHeader
                  key={leavePlanName}
                  employeeLeaveBalance={employeeLeaveBalance}
                  leavePlanName={leavePlanName}
                  calculationMethod={calculationMethod}
                  absencePeriodDecisionsWithTimeOff={
                    absencePeriodDecisionsWithTimeOff
                  }
                  {...props}
                />
              )
          )}
      </ContentRenderer>
    </TabPane>
  );
};
