/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  ConfigurableText,
  Icon,
  FormattedMessage,
  Tooltip
} from 'fineos-common';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

import { CalculationMethod } from '../time-taken.api';

import styles from './time-taken-tooltip.module.scss';

type TooltipProps = {
  /**
   * Calculated method in employeeLeaveBalance object
   */
  calculationMethod: string;
};

/**
 *
 * @param calculationMethod - calculated method name
 *
 * @returns value by type
 */
const getCalculatedMethod = (calculationMethod: string) => {
  switch (calculationMethod) {
    case CalculationMethod.CALENDAR_YEAR:
      return 'CALENDAR_YEAR';
    case CalculationMethod.FIXED_YEAR:
      return 'FIXED_YEAR';
    case CalculationMethod.ROLLING_BACK:
      return 'ROLLING_BACK';
    case CalculationMethod.ROLLING_FORWARD:
      return 'ROLLING_FORWARD';
  }

  return calculationMethod;
};

/**
 * TimeTakenTooltip component provide the display of the appropriate tooltip by the calculated method
 */
export const TimeTakenTooltip: React.FC<TooltipProps> = ({
  calculationMethod
}) => (
  <div className={styles.tooltipWrapper}>
    <Tooltip
      tooltipContent={
        <ConfigurableText
          id={`PROFILE.TIME_TAKEN_TAB.CALCULATION_METHOD_TYPE.${getCalculatedMethod(
            calculationMethod
          )}`}
        />
      }
      placement="bottom"
    >
      <Icon
        data-test-el="time-taken-icon"
        icon={faQuestionCircle}
        iconLabel={
          <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.QUESTION_MARK_ICON_LABEL" />
        }
      />
    </Tooltip>
  </div>
);
