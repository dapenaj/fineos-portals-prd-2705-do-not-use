/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientPeriodDecisions,
  Decision,
  EmployeeLeaveBalance
} from 'fineos-js-api-client';

import { DecisionStatusCategory, JobProtectedLeavePlan } from '../../../shared';
import { groupByLeavePlanName } from '../../notification/job-protected-leave/job-protected-leave.util';

/**
 *
 * @param decisions - list of decisions
 *
 * @returns The filtered list by status category
 */
const getDecisionsForTimeOff = (decisions: Decision[]) => {
  const timeTakenStatuses = [
    DecisionStatusCategory.PENDING,
    DecisionStatusCategory.APPROVED,
    DecisionStatusCategory.DENIED
  ];

  return groupByLeavePlanName(decisions).reduce(
    (acc: JobProtectedLeavePlan[], curr) => {
      const timeTakenDecisions = curr.decisions.filter(
        ({ statusCategory, period }) =>
          timeTakenStatuses.includes(statusCategory) &&
          period.leavePlan &&
          period.leavePlan.calculationPeriodMethod
      );

      if (timeTakenDecisions.length) {
        acc.push({
          ...curr,
          decisions: timeTakenDecisions
        });
      }

      return acc;
    },
    []
  );
};

export type AbsencePeriodDecisionsForTimeOff = Omit<
  JobProtectedLeavePlan,
  'calculationMethod'
> & {
  id: string;
  calculationMethod: string;
};

/**
 *
 * @param absencePeriodDecisions - object of GroupClientPeriodDecisions type
 *
 * @returns The list of JobProtectedLeavePlan type which is group by leave plan name
 */
const getAbsencePeriodDecisionsForTimeOff = (
  absencePeriodDecisions: GroupClientPeriodDecisions | null
): AbsencePeriodDecisionsForTimeOff[] | undefined => {
  if (absencePeriodDecisions && absencePeriodDecisions.decisions) {
    const decisionsWithStatus = getDecisionsForTimeOff(
      absencePeriodDecisions.decisions
    );

    return decisionsWithStatus.map(jobProtectedLeavePlan => {
      const decisionWithCalculationMethod = jobProtectedLeavePlan.decisions.find(
        decision => decision.period.leavePlan!.calculationPeriodMethod
      );

      return {
        id: decisionWithCalculationMethod!.period.leavePlan!.id,
        leavePlanName: jobProtectedLeavePlan.leavePlanName,
        decisions: jobProtectedLeavePlan.decisions,
        calculationMethod: decisionWithCalculationMethod!.period.leavePlan!
          .calculationPeriodMethod
      };
    });
  }
};

const getNumberDecimalUnitsReference = (balance: EmployeeLeaveBalance) => {
  const numbers = [
    balance.approvedTime,
    balance.availableBalance,
    balance.pendingTime,
    balance.timeEntitlement
  ];

  return numbers.reduce((acc, curr) => {
    const [, decimalPart] = curr.toString().split('.');
    const decimalPartLength = decimalPart ? decimalPart.length : 0;
    return acc < decimalPartLength ? decimalPartLength : acc;
  }, 0);
};

export { getAbsencePeriodDecisionsForTimeOff, getNumberDecimalUnitsReference };
