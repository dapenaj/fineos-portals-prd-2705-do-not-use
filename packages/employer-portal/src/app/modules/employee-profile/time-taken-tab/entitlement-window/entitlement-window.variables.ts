export const ENTITLEMENT_TIMELINE_WEEK_WIDTH = 15;

export const entitlementWeekOffset = (weekIndex: number) =>
  weekIndex * ENTITLEMENT_TIMELINE_WEEK_WIDTH;
