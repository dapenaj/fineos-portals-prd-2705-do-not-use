/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useLayoutEffect, useState, useMemo, ReactElement } from 'react';
import { EmployeeLeaveBalance } from 'fineos-js-api-client';
import {
  createThemedStyles,
  textStyleToCss,
  Carousel,
  Tooltip
} from 'fineos-common';
import moment from 'moment';
import classNames from 'classnames';
import { groupBy as _groupBy } from 'lodash';

import { EnhancedDecision } from '../../../../shared';

import styles from './entitlement-window.module.scss';
import {
  ENTITLEMENT_TIMELINE_WEEK_WIDTH,
  entitlementWeekOffset
} from './entitlement-window.variables';
import { EntitlementToday } from './entitlement-today';
import { EntitlementDecision } from './entitlement-decision';

type EntitlementWindowProps = {
  employeeLeaveBalance: EmployeeLeaveBalance;
  decisions: EnhancedDecision[];
  active?: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  entitlementWindow: {
    borderColor: theme.colorSchema.primaryColor
  },
  window: {
    color: theme.colorSchema.neutral1,
    borderColor: theme.colorSchema.neutral1,
    background: theme.colorSchema.neutral4,
    '&$windowFilledFuture': {
      borderColor: theme.colorSchema.neutral1,
      background: theme.colorSchema.primaryColor
    },
    '&$windowFilledPast': {
      borderColor: theme.colorSchema.neutral1,
      background: theme.colorSchema.neutral6
    }
  },
  windowFilledFuture: {},
  windowFilledPast: {},
  train: {
    '&:hover:after': {
      backgroundColor: theme.colorSchema.infoColor
    },
    '&:focus:after': {
      backgroundColor: theme.colorSchema.infoColor,
      borderColor: theme.colorSchema.primaryColor
    }
  },
  tooltip: {
    ...textStyleToCss(theme.typography.baseText),
    '& .ant-popover-inner-content': {
      background: theme.colorSchema.primaryColor,
      color: theme.colorSchema.neutral1
    },
    '& .ant-popover-content >.ant-popover-arrow': {
      borderColor: `${theme.colorSchema.primaryColor}`
    }
  }
}));

const isContainsDecision = (
  originalDecisions: EnhancedDecision[] | undefined,
  targetDecisions: EnhancedDecision[]
) =>
  originalDecisions
    ? targetDecisions.some(targetDecision =>
        originalDecisions.some(
          originalDecision =>
            originalDecision.period.leaveRequest.reasonName ===
            targetDecision.period.leaveRequest.reasonName
        )
      )
    : false;

/**
 * EntitlementWindow component provide value for entitlement window
 * Not use on UI now
 */
export const EntitlementWindow = React.memo(
  ({ employeeLeaveBalance, decisions }: EntitlementWindowProps) => {
    const themedStyles = useThemedStyles();
    const actualElRef = React.createRef<HTMLDivElement>();
    const [panelWidth, setPanelWidth] = useState(0);
    const entitlementStartDate = useMemo(
      () => employeeLeaveBalance.availabilityPeriodStartDate.clone().day(0),
      [employeeLeaveBalance]
    );
    const entitlementEndDate = useMemo(
      () => employeeLeaveBalance.availabilityPeriodEndDate.clone().day(0),
      [employeeLeaveBalance]
    );

    const weeksInEntitlement = Math.ceil(
      entitlementEndDate.diff(entitlementStartDate, 'week', true)
    );
    const entitlementWidth =
      weeksInEntitlement * ENTITLEMENT_TIMELINE_WEEK_WIDTH;

    useLayoutEffect(() => {
      setPanelWidth(actualElRef?.current?.clientWidth || 0);

      const handlePanelResize = () => {
        setPanelWidth(actualElRef?.current?.clientWidth || 0);
      };

      window.addEventListener('resize', handlePanelResize);

      return () => {
        window.removeEventListener('resize', handlePanelResize);
      };
    }, [setPanelWidth, actualElRef]);

    const drawWindowsOutsideEntitlement = () => {
      const results = [];
      const widthOutsideEntitlement = panelWidth - entitlementWidth;
      const numberOutsideWindow = Math.ceil(
        widthOutsideEntitlement / ENTITLEMENT_TIMELINE_WEEK_WIDTH / 2
      );

      for (let i = 0; i < numberOutsideWindow; i++) {
        results.push(
          <div
            key={i}
            className={classNames(themedStyles.window, styles.window)}
          />
        );
      }
      return results;
    };

    const firstDayOfCurrentWeek = moment().day(0);
    let currentWeekIndex = 0;

    const entitlementWindowBoxes = [];

    let lastTrain: ReactElement[] = [];
    let lastTrainDecisions = [];
    for (let i = 0; i < weeksInEntitlement; i++) {
      const weekStartDay = entitlementStartDate.clone().add(i, 'week');
      const weekEndDay = weekStartDay.clone().day(6);
      const isPast = weekStartDay.isBefore(firstDayOfCurrentWeek, 'day');
      const boxDecisions = decisions.filter(
        ({ period: { startDate, endDate } }) =>
          weekStartDay.isSameOrBefore(endDate, 'day') &&
          weekEndDay.isSameOrAfter(startDate, 'day')
      );

      if (firstDayOfCurrentWeek.isSame(weekStartDay, 'day')) {
        currentWeekIndex = i;
      }

      if (
        !boxDecisions.length ||
        !isContainsDecision(
          lastTrainDecisions[lastTrainDecisions.length - 1],
          boxDecisions
        )
      ) {
        if (lastTrain.length) {
          const targetDecisions = Object.entries(
            _groupBy(
              lastTrainDecisions.flat(),
              decision => decision.period.leaveRequest.reasonName
            )
          );
          const popoverContent =
            targetDecisions.length === 1 ? (
              <EntitlementDecision
                decisions={targetDecisions[0][1]}
                balance={employeeLeaveBalance}
              />
            ) : (
              <Carousel>
                {targetDecisions.map(([reason, reasonDecisions]) => (
                  <EntitlementDecision
                    decisions={reasonDecisions}
                    balance={employeeLeaveBalance}
                    key={reason}
                  />
                ))}
              </Carousel>
            );

          entitlementWindowBoxes.push(
            <Tooltip
              tooltipContent={popoverContent}
              key={weekEndDay.toISOString()}
            >
              <div className={classNames(styles.train, themedStyles.train)}>
                {lastTrain}
              </div>
            </Tooltip>
          );
          lastTrain = [];
          lastTrainDecisions = [];
        }
      }

      if (boxDecisions.length) {
        lastTrain.push(
          <div
            key={weekStartDay.toISOString()}
            className={classNames(themedStyles.window, styles.window, {
              [themedStyles.windowFilledPast]: boxDecisions.length && isPast,
              [themedStyles.windowFilledFuture]: boxDecisions.length && !isPast,
              [styles.multipleDecisions]: boxDecisions.length > 1
            })}
          />
        );
        lastTrainDecisions.push(boxDecisions);
      } else {
        entitlementWindowBoxes.push(
          <div
            key={weekStartDay.toISOString()}
            className={classNames(themedStyles.window, styles.window, {
              [themedStyles.windowFilledPast]: boxDecisions.length && isPast,
              [themedStyles.windowFilledFuture]: boxDecisions.length && !isPast,
              [styles.multipleDecisions]: boxDecisions.length > 1
            })}
          />
        );
      }
    }

    const outsideElements = drawWindowsOutsideEntitlement();

    return (
      <div
        data-test-el="entitlement-window"
        ref={actualElRef}
        className={styles.frame}
      >
        {outsideElements}

        <EntitlementToday
          currentWeekOffset={outsideElements.length + currentWeekIndex}
        />

        <div
          className={classNames(
            styles.entitlementWindow,
            themedStyles.entitlementWindow
          )}
          style={{
            left: entitlementWeekOffset(outsideElements.length),
            width: entitlementWidth + 2
          }}
        />

        {entitlementWindowBoxes}

        {outsideElements}
      </div>
    );
  }
);
