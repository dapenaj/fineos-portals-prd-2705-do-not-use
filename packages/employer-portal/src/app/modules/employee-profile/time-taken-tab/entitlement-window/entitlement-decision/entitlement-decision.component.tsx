/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import {
  createThemedStyles,
  FormattedMessage,
  PropertyItem,
  TimeAmount
} from 'fineos-common';
import moment from 'moment';
import classNames from 'classnames';
import { uniqBy as _uniqBy } from 'lodash';
import { EmployeeLeaveBalance } from 'fineos-js-api-client';

import {
  DecisionStatusCategory,
  EnhancedDecision
} from '../../../../../shared/types';
import { NotificationReason } from '../../../../notification/notification-reason';
import { getNumberDecimalUnitsReference } from '../../time-taken.util';

import styles from './entitlement-decision.module.scss';

type EntitlementDecisionProps = {
  decisions: EnhancedDecision[];
  balance: EmployeeLeaveBalance;
};

const useThemedStyles = createThemedStyles(theme => ({
  pending: {
    '&:before': {
      backgroundColor: theme.colorSchema.pendingColor
    }
  },
  approved: {
    '&:before': {
      backgroundColor: theme.colorSchema.successColor
    }
  }
}));

// this is O^2 complexity
const getDurationByType = (
  decisions: EnhancedDecision[],
  status: DecisionStatusCategory
) => {
  const actualRanges: [moment.Moment, moment.Moment][] = [];

  // potentially requests may overlap, so we need to ensure that we actually counting time and not just diffs
  for (const decision of decisions) {
    if (decision.statusCategory === status) {
      const potentialRange: [moment.Moment, moment.Moment] = [
        decision.period.startDate.clone(),
        decision.period.endDate.clone()
      ];

      for (const actualRange of actualRanges) {
        if (actualRange[1].isAfter(potentialRange[0])) {
          potentialRange[0] = actualRange[1];
        }
        if (actualRange[0].isBefore(potentialRange[1])) {
          potentialRange[1] = actualRange[0];
        }
      }

      if (potentialRange[0].isBefore(potentialRange[1], 'hour')) {
        actualRanges.push(potentialRange);
      }
    }
  }

  return actualRanges.reduce(
    (totalDuration, [startDate, endDate]) =>
      totalDuration.add(
        endDate.valueOf() - startDate.valueOf(),
        'milliseconds'
      ),
    moment.duration()
  );
};

export const EntitlementDecision = React.memo(
  ({ decisions, balance }: EntitlementDecisionProps) => {
    const themedStyles = useThemedStyles();
    const reason = decisions[0]?.period?.leaveRequest?.reasonName;
    const { approvedDuration, pendingDuration } = useMemo(() => {
      const normalizedDecisions = _uniqBy(
        decisions,
        ({ period: { startDate, endDate } }) =>
          startDate.toString() + endDate.toString()
      );
      return {
        approvedDuration: getDurationByType(
          normalizedDecisions,
          DecisionStatusCategory.APPROVED
        ),
        pendingDuration: getDurationByType(
          normalizedDecisions,
          DecisionStatusCategory.PENDING
        )
      };
    }, [decisions]);

    return (
      <div className={styles.entitlementDecision}>
        <div className={styles.row}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.ENTITLEMENT_WINDOW.TIME_APPROVED" />
            }
          >
            <div className={classNames(styles.mark, themedStyles.approved)}>
              <TimeAmount
                numberDecimalUnitsReference={getNumberDecimalUnitsReference(
                  balance
                )}
                duration={approvedDuration}
                timeBasis={balance.timeBasis}
              />
            </div>
          </PropertyItem>
        </div>

        <div className={styles.row}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.ENTITLEMENT_WINDOW.TIME_PENDING" />
            }
          >
            <div className={classNames(styles.mark, themedStyles.pending)}>
              <TimeAmount
                numberDecimalUnitsReference={getNumberDecimalUnitsReference(
                  balance
                )}
                duration={pendingDuration}
                timeBasis={balance.timeBasis}
              />
            </div>
          </PropertyItem>
        </div>

        <div className={styles.row}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.TIME_TAKEN_TAB.ENTITLEMENT_WINDOW.REASON" />
            }
          >
            <NotificationReason reason={reason} />
          </PropertyItem>
        </div>
      </div>
    );
  }
);
