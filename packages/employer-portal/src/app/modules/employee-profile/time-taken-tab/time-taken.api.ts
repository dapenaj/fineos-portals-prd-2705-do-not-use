/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  AbsencePeriodDecisionsSearch,
  GroupClientAbsenceService,
  GroupClientPeriodDecisions,
  AbscenceLeaveAvailabilitySearch,
  EmployeeLeaveBalance
} from 'fineos-js-api-client';
import { PartialApiCompleteError } from 'fineos-common';
import moment, { Moment } from 'moment';
import { chain as _chain } from 'lodash';

import { EnhancedDecision } from '../../../shared';

import { AbsencePeriodDecisionsForTimeOff } from './time-taken.util';

const groupClientAbsenceService = GroupClientAbsenceService.getInstance();

type LeavePlanNameWithDecisions = {
  id: string;
  leavePlanName: string;
  decisions: EnhancedDecision[];
  calculationMethod: string;
  calculationDate: Moment;
};

export type EmployeeLeaveBalanceLeavePlan = {
  leavePlanName: string;
  employeeLeaveBalance: EmployeeLeaveBalance;
  calculationMethod: CalculationMethod;
  endDate: Moment;
};

export enum CalculationMethod {
  ROLLING_BACK = 'Rolling Back',
  ROLLING_FORWARD = 'Rolling Forward',
  CALENDAR_YEAR = 'Calendar Year',
  FIXED_YEAR = 'Fixed Year'
}

/**
 *
 * @param customerId - current user id
 * @param startDate - actual date minus 18 months
 * @param endDate - actual date plus 6 months
 *
 * @returns Promise with GroupClientPeriodDecisions object
 */
const fetchAbsencePeriodDecisions = (
  customerId: string,
  startDate: Moment,
  endDate: Moment
): Promise<GroupClientPeriodDecisions> => {
  const search = new AbsencePeriodDecisionsSearch();
  search.customerId(customerId);
  search.startDate(startDate);
  search.endDate(endDate);

  return groupClientAbsenceService.fetchAbsencePeriodDecisions(search);
};

/**
 *
 * @param groupedByLeavePlanIds - list of decisions with leave plan name
 * @param customerId - current user id
 * @param search - object AbscenceLeaveAvailabilitySearch
 *
 * @returns object of type EmployeeLeaveBalance
 */
const getLeaveAvailability = async (
  groupedByLeavePlanIds: LeavePlanNameWithDecisions[],
  customerId: string
): Promise<EmployeeLeaveBalanceLeavePlan[]> => {
  const results = await Promise.allSettled(
    groupedByLeavePlanIds.map(
      (groupedByLeavePlanId: LeavePlanNameWithDecisions) => {
        const search = new AbscenceLeaveAvailabilitySearch();
        if (
          groupedByLeavePlanId.calculationMethod &&
          CalculationMethod.ROLLING_BACK.includes(
            groupedByLeavePlanId.calculationMethod
          )
        ) {
          search.calculationDate(groupedByLeavePlanId.calculationDate);
        }

        return groupClientAbsenceService.fetchLeaveAvailability(
          customerId,
          groupedByLeavePlanId.id,
          search
        );
      }
    )
  );

  const leaveAvailability: EmployeeLeaveBalanceLeavePlan[] = [];
  let apiError = null;

  for (let i = 0; i < results.length; i++) {
    const result = results[i];
    const groupedByLeavePlanId = groupedByLeavePlanIds[i];

    if (result.status === 'fulfilled') {
      leaveAvailability.push({
        leavePlanName: groupedByLeavePlanId.leavePlanName,
        employeeLeaveBalance: result.value,
        calculationMethod: groupedByLeavePlanId.calculationMethod as CalculationMethod,
        endDate: groupedByLeavePlanId.calculationDate
      });
    } else {
      apiError = result.reason;
    }
  }

  if (apiError) {
    throw new PartialApiCompleteError(apiError, leaveAvailability);
  }

  return leaveAvailability;
};

/**
 *
 * @param decisions - list of decisions with status category
 *
 * @returns The last period end date.
 */
const getCalculationDate = (decisions: EnhancedDecision[]) => {
  return decisions.reduce(
    (date: moment.Moment, curr) =>
      curr.period.endDate.isAfter(date) ? curr.period.endDate : date,
    moment(0)
  );
};

/**
 *
 * @param customerId - current user id
 * @param absencePeriodDecisions - list of JobProtectedLeavePlan type
 */
const fetchLeaveAvailability = async (
  customerId: string,
  absencePeriodDecisions: AbsencePeriodDecisionsForTimeOff[]
): Promise<EmployeeLeaveBalanceLeavePlan[]> => {
  const employeeLeaveBalanceLeavePlan = await getLeaveAvailability(
    absencePeriodDecisions.reduce(
      (
        acc: LeavePlanNameWithDecisions[],
        {
          id,
          leavePlanName,
          decisions,
          calculationMethod
        }: AbsencePeriodDecisionsForTimeOff
      ) => {
        const groupedByLeavePlanIds = _chain(decisions)
          .filter(decision => Boolean(decision.period.leavePlan))
          .groupBy(decision => decision.period.leavePlan!.id)
          .value();

        for (const leavePlanId of Object.keys(groupedByLeavePlanIds)) {
          const leavePlanDecisions = groupedByLeavePlanIds[leavePlanId].filter(
            Boolean
          );

          if (leavePlanDecisions.length) {
            const existingItem = acc.find(item => item.id === leavePlanId);

            if (existingItem) {
              existingItem.decisions = [
                ...existingItem.decisions,
                ...leavePlanDecisions
              ];
              existingItem.calculationDate = getCalculationDate(
                leavePlanDecisions
              );
            } else {
              acc.push({
                id,
                leavePlanName,
                decisions: leavePlanDecisions,
                calculationDate: getCalculationDate(leavePlanDecisions),
                calculationMethod
              });
            }
          }
        }

        return acc;
      },
      [] as LeavePlanNameWithDecisions[]
    ),
    customerId
  );

  return _chain(employeeLeaveBalanceLeavePlan)
    .orderBy(['leavePlanName'], ['asc'])
    .orderBy(['endDate'], ['desc'])
    .value();
};

export { fetchAbsencePeriodDecisions, fetchLeaveAvailability };
