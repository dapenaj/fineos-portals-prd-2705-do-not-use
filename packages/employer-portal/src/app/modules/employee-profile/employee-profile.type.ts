export type UpdateCustomer = {
  firstName: string;
  lastName: string;
};
