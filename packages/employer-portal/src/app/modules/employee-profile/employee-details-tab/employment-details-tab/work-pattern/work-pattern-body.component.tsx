/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useMemo } from 'react';
import {
  FormattedMessage,
  ModalFooter,
  FormButton,
  UiButton,
  ButtonType,
  Form,
  AsteriskInfo,
  useAsync
} from 'fineos-common';
import {
  WeekBasedWorkPattern,
  BaseDomain,
  WorkPatternEFormRequest,
  WeekBasedDayPattern
} from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { WorkPatternTypeSection } from '../../../../../shared/work-pattern/work-pattern-type-select';
import {
  FormikWithResponseHandling,
  ModalFormViewMode,
  isFixedWorkPatternType,
  isRotatingWorkPatternType,
  WorkPatternBasis,
  isVariableWorkPatternType,
  WorkPatternValue,
  WeekBasedDayPatternNullable,
  WEEKEND_DAYS,
  isDomainNameUnknown
} from '../../../../../shared';
import { EnumSubOption } from '../../../../intake/non-established-intake/occupation-and-earnings';
import { EnumDomain, useEnumDomain } from '../../../../../shared/enum-domain';
import { Config } from '../../../../../config';
import { fetchMostRecentCustomerOccupation } from '../../../employee-profile.api';

import {
  editWeekBasedWorkPattern,
  editVariableWorkPattern,
  addWeekBasedWorkPattern
} from './work-pattern.api';
import {
  workPatternFormValidation,
  buildWorkPattern
} from './work-pattern.form';
import { fetchWorkPattern } from './work-pattern.api';

type WorkPatternModalBodyProps = {
  onFinish?: () => void;
  setRefreshedData: (refreshedData: WeekBasedWorkPattern | null) => void;
  refreshedData: WeekBasedWorkPattern | null;
  customerId: string;
  onSetMode: (mode: ModalFormViewMode) => void;
};

export const WorkPatternModalBody: React.FC<WorkPatternModalBodyProps> = ({
  onFinish = () => undefined,
  setRefreshedData,
  refreshedData,
  customerId,
  onSetMode
}) => {
  const { domains: weekDays } = useEnumDomain(EnumDomain.WEEK_DAYS, {
    shouldSkipUnknownFiltering: true
  });
  const { domains: workPatternStatus } = useEnumDomain(
    EnumDomain.WORK_PATTERN_STATUS
  );

  const { domains: workPatternTypes } = useEnumDomain(
    EnumDomain.WORK_PATTERN_TYPE,
    {
      shouldSkipUnknownFiltering: true
    }
  );
  const config = useContext(Config);
  const dayLengthConfig = config.configurableWorkPattern.FIXED_DAY_LENGTH;

  const {
    state: { value: occupation },
    changeAsyncValue: refreshCustomerOccupationImMemory
  } = useAsync(fetchMostRecentCustomerOccupation, [customerId!]);
  const isWeeklyBasedPattern =
    occupation?.workPatternBasis === WorkPatternBasis.WEEK_BASED;
  const {
    state: { value: workPattern },
    changeAsyncValue: refreshWorkPattern
  } = useAsync(fetchWorkPattern, [customerId, occupation?.id!], {
    shouldExecute: () => isWeeklyBasedPattern
  });

  const initialValues = useMemo(
    () =>
      buildWorkPattern(
        workPattern,
        isWeeklyBasedPattern,
        weekDays,
        workPatternTypes,
        dayLengthConfig
      ),
    [
      workPattern,
      isWeeklyBasedPattern,
      weekDays,
      workPatternTypes,
      dayLengthConfig
    ]
  );
  const getWorkPatternType = (
    subOptions: EnumSubOption[],
    selectedSubOption: number
  ): BaseDomain => {
    const result = subOptions.find(
      subOption => subOption.value.fullId === selectedSubOption
    );
    return result ? (result.value as BaseDomain) : ({} as BaseDomain);
  };

  const hasDayValue = (day: WeekBasedDayPatternNullable) =>
    day.hours === null && day.minutes === null;

  const getDayValue = (values: WeekBasedDayPatternNullable[]) =>
    values.map(day => {
      if (hasDayValue(day)) {
        return {
          ...day,
          hours: 0,
          minutes: 0
        } as WeekBasedDayPattern;
      }
      return day as WeekBasedDayPattern;
    });

  const getDefaultFixedWorkWeek = (
    domains: BaseDomain[],
    dayLength: {
      hours: number;
      minutes: number;
    }
  ) => {
    return domains.map(domain => {
      const isWeekend = WEEKEND_DAYS.includes(domain.name.toLocaleUpperCase());
      return {
        dayOfWeek: { ...domain },
        hours: isWeekend ? 0 : dayLength.hours,
        minutes: isWeekend ? 0 : dayLength.minutes,
        weekNumber: 1
      } as WeekBasedDayPattern;
    });
  };

  const handleSubmit = async (data: WorkPatternValue) => {
    const weekBasedWorkPattern: WeekBasedWorkPattern = {} as WeekBasedWorkPattern;

    weekBasedWorkPattern.workPatternType = !_isEmpty(
      data.workPatternType.subOptions
    )
      ? getWorkPatternType(
          data.workPatternType.subOptions,
          data.workPatternType.selectedSubOption
        )
      : data.workPatternType;
    if (isFixedWorkPatternType(weekBasedWorkPattern.workPatternType)) {
      weekBasedWorkPattern.workPatternDays = data.fixedPattern.isNonStandardWeek
        ? [...getDayValue(data.fixedPattern.workWeek)]
        : [...getDefaultFixedWorkWeek(weekDays, dayLengthConfig)];
    }
    if (isRotatingWorkPatternType(weekBasedWorkPattern.workPatternType)) {
      weekBasedWorkPattern.workPatternDays = [
        ...getDayValue([
          ...data.rotatingPattern.workPatternWeek1,
          ...data.rotatingPattern.workPatternWeek2
        ])
      ];
      if (
        weekBasedWorkPattern.workPatternType.name.includes('3') ||
        weekBasedWorkPattern.workPatternType.name.includes('4')
      ) {
        weekBasedWorkPattern.workPatternDays = [
          ...weekBasedWorkPattern.workPatternDays,
          ...getDayValue(data.rotatingPattern.workPatternWeek3)
        ] as WeekBasedDayPattern[];
      }
      if (weekBasedWorkPattern.workPatternType.name.includes('4')) {
        weekBasedWorkPattern.workPatternDays = [
          ...weekBasedWorkPattern.workPatternDays,
          ...getDayValue(data.rotatingPattern.workPatternWeek4)
        ] as WeekBasedDayPattern[];
      }
    }
    if (isVariableWorkPatternType(weekBasedWorkPattern.workPatternType)) {
      const workPatternEFormRequest = new WorkPatternEFormRequest();

      const {
        additionalInformation,
        workPatternRepeatsWeekly,
        numberOfWeeks,
        workDayIsSameLength,
        dayLength
      } = data.variablePattern;

      workPatternEFormRequest.patternDescription = additionalInformation;
      workPatternEFormRequest.workPatternType =
        weekBasedWorkPattern.workPatternType;
      workPatternEFormRequest.doesPatternRepeat = workPatternRepeatsWeekly;
      workPatternEFormRequest.numberOfRepeatedWeeks = numberOfWeeks || 0;
      workPatternEFormRequest.isWorkDaySameLength = workDayIsSameLength;
      workPatternEFormRequest.occupationSummary = `${
        occupation?.jobTitle
      } start date ${occupation?.jobStartDate.format('MM/DD/YYYY')}`;
      workPatternEFormRequest.fixedWorkDayLength =
        dayLength.hours !== null && dayLength.minutes !== null
          ? `${dayLength.hours}:${dayLength.minutes}`
          : '';
      workPatternEFormRequest.customerId = customerId;

      await editVariableWorkPattern(workPatternEFormRequest);
      onSetMode(ModalFormViewMode.UPLOAD_SUCCESS);
    }

    if (
      occupation &&
      (isFixedWorkPatternType(weekBasedWorkPattern.workPatternType) ||
        isRotatingWorkPatternType(weekBasedWorkPattern.workPatternType))
    ) {
      const { patternStartDate, workWeekStarts, patternStatus } = data;
      weekBasedWorkPattern.patternStartDate = patternStartDate;
      weekBasedWorkPattern.workWeekStarts = workWeekStarts;
      weekBasedWorkPattern.patternStatus = patternStatus;
      let newWeekBasedWorkPattern: WeekBasedWorkPattern;
      if (workPattern) {
        newWeekBasedWorkPattern = await editWeekBasedWorkPattern(
          customerId,
          occupation.id,
          weekBasedWorkPattern
        );
      } else {
        weekBasedWorkPattern.patternStartDate = occupation.jobStartDate;
        const workWeekStartsTemp = weekDays.find(
          (day: BaseDomain) => day.name === 'Monday'
        );
        if (workWeekStartsTemp) {
          weekBasedWorkPattern.workWeekStarts = workWeekStartsTemp;
        }
        const workPatternStatusTemp = workPatternStatus.find(
          isDomainNameUnknown
        );

        if (workPatternStatusTemp) {
          weekBasedWorkPattern.patternStatus = workPatternStatusTemp;
        }
        newWeekBasedWorkPattern = await addWeekBasedWorkPattern(
          customerId,
          occupation.id,
          weekBasedWorkPattern
        );
      }

      const valueToRefresh = {
        ...newWeekBasedWorkPattern,
        workPatternType: weekBasedWorkPattern.workPatternType
      } as WeekBasedWorkPattern;

      refreshWorkPattern(valueToRefresh);
      refreshCustomerOccupationImMemory({
        ...occupation,
        workPatternBasis: WorkPatternBasis.WEEK_BASED
      });
    }
    onFinish();
  };

  return (
    <FormikWithResponseHandling
      validationSchema={workPatternFormValidation}
      initialValues={initialValues}
      onSubmit={handleSubmit}
      dataParseFunction={(data: WeekBasedWorkPattern | null) => {
        setRefreshedData(data);
        return buildWorkPattern(
          data,
          isWeeklyBasedPattern,
          weekDays,
          workPatternTypes,
          dayLengthConfig
        );
      }}
      validateOnChange={false}
    >
      <Form>
        <AsteriskInfo />
        <WorkPatternTypeSection />
        <ModalFooter>
          <FormattedMessage
            shouldSubmitForm={false}
            as={FormButton}
            htmlType="submit"
            id="FINEOS_COMMON.GENERAL.OK"
          />

          <FormattedMessage
            as={UiButton}
            onClick={() => {
              if (!_isEmpty(refreshedData)) {
                refreshWorkPattern(refreshedData);
              }
              onFinish();
            }}
            buttonType={ButtonType.LINK}
            id="FINEOS_COMMON.GENERAL.CANCEL"
          />
        </ModalFooter>
      </Form>
    </FormikWithResponseHandling>
  );
};
