import React from 'react';
import classNames from 'classnames';
import { faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';
import {
  createThemedStyles,
  FormattedMessage,
  Icon,
  textStyleToCss
} from 'fineos-common';

type InfoWithIconProps = {
  isConfirmed: boolean;
  children: React.ReactElement<{ className?: string }>;
};

const useThemedStyles = createThemedStyles(theme => ({
  affirmativeIcon: {
    color: theme.colorSchema.successColor,
    fontSize: theme.typography.baseText.fontSize
  },
  negativeIcon: {
    color: theme.colorSchema.neutral7,
    fontSize: theme.typography.baseText.fontSize
  },
  affirmativeText: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.baseTextSemiBold)
  },
  negativeText: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const InfoWithIcon: React.FC<InfoWithIconProps> = ({
  isConfirmed,
  children
}) => {
  const themedStyles = useThemedStyles();
  const styledChildren = React.cloneElement(children, {
    className: isConfirmed
      ? themedStyles.affirmativeText
      : themedStyles.negativeText
  });
  return (
    <div>
      <Icon
        icon={isConfirmed ? faCheck : faTimes}
        className={classNames(
          isConfirmed ? themedStyles.affirmativeIcon : themedStyles.negativeIcon
        )}
        iconLabel={
          <FormattedMessage
            id={
              isConfirmed
                ? 'FINEOS_COMMON.ICON_LABEL.AFFIRMATIVE_ICON_LABEL'
                : 'FINEOS_COMMON.ICON_LABEL.NEGATIVE_ICON_LABEL'
            }
          />
        }
      />{' '}
      {styledChildren}
    </div>
  );
};
