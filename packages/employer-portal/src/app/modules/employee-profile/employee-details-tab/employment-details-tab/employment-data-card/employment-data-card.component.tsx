/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  createThemedStyles,
  textStyleToCss,
  Card,
  ContentRenderer
} from 'fineos-common';

import styles from './employment-data-card.module.scss';

type EmploymentDataCardProps = {
  title: React.ReactNode;
  emptyContent: React.ReactNode;
  actionButton?: React.ReactNode;
  isLoading: boolean;
  children: React.ReactNode;
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    ...textStyleToCss(theme.typography.panelTitle),
    color: theme.colorSchema.neutral8
  },
  content: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  }
}));
export const EmploymentDataCard: React.FC<EmploymentDataCardProps> = ({
  title,
  emptyContent,
  isLoading,
  children
}) => {
  const themedStyles = useThemedStyles();
  return (
    <Card className={styles.card}>
      <h3 className={classNames(styles.title, themedStyles.title)}>{title}</h3>
      <ContentRenderer
        isEmpty={!children}
        isLoading={isLoading}
        emptyContent={
          <div className={classNames(styles.emptyState, themedStyles.content)}>
            {emptyContent}
          </div>
        }
      >
        <div className={classNames(styles.children, themedStyles.content)}>
          {children}
        </div>
      </ContentRenderer>
    </Card>
  );
};
