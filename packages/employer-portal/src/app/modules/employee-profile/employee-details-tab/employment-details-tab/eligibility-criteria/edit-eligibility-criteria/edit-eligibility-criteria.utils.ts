/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { CustomerAbsenceEmployment, CustomerOccupation } from 'fineos-js-api-client';

import {
  EditEligibilityCriteriaForm,
  CheckboxValue
} from './edit-eligibility-criteria.type';

export const CHECKBOX_FIELDS = [
  'withinFMLACriteria',
  'workingAtHome',
  'keyEmployee'
];

export const filteredCheckboxValues = (
  absenceEmployment: CustomerAbsenceEmployment
) =>
  CHECKBOX_FIELDS.filter(field => Boolean((absenceEmployment as any)[field]));

export const isAbsenceEmploymentNotChanged = (
  edited: EditEligibilityCriteriaForm,
  fetched: CustomerAbsenceEmployment | null
) => {
  const areCheckboxValuesEqual = CHECKBOX_FIELDS.every(
    field =>
      fetched &&
      (fetched as any)[field] ===
        edited?.checkboxValues?.includes(field as CheckboxValue)
  );
  return (
    areCheckboxValuesEqual &&
    edited.employmentWorkState.name === fetched?.employmentWorkState.name &&
    edited.hoursWorkedPerYear === fetched?.hoursWorkedPerYear &&
    edited.cbaValue === fetched?.cbaValue
  );
};

export const parsedEligibilityCriteria = (
  editedEligibilityCriteria: EditEligibilityCriteriaForm,
  customerOccupation: CustomerOccupation & CustomerAbsenceEmployment
) => {
  const hasCheckboxValue = (value: string) =>
    editedEligibilityCriteria.checkboxValues.includes(value as CheckboxValue);

  const parsedData = new CustomerOccupation();
  Object.assign(parsedData, {
    adjustedHireDate: customerOccupation?.adjustedHireDate,
    cbaValue: editedEligibilityCriteria?.cbaValue,
    employmentClassification: customerOccupation?.employmentClassification,
    employmentType: customerOccupation?.employmentType,
    employmentWorkState: editedEligibilityCriteria.employmentWorkState,
    hoursWorkedPerYear: editedEligibilityCriteria?.hoursWorkedPerYear,
    withinFMLACriteria: hasCheckboxValue('withinFMLACriteria'),
    workingAtHome: hasCheckboxValue('workingAtHome'),
    keyEmployee: hasCheckboxValue('keyEmployee')
  });

  if (customerOccupation?.id) {
    Object.assign(parsedData, {
      id: customerOccupation.id
    });
  }
  return parsedData;
};
