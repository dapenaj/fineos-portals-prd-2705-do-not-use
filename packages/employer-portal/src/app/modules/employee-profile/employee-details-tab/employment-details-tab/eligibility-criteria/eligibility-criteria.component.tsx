/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FormattedMessage,
  ManageOccupationPermissions,
  usePermissions
} from 'fineos-common';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation
} from 'fineos-js-api-client';

import { isDomainNameUnknown } from '../../../../../shared';
import { EmploymentDataCard } from '../employment-data-card/employment-data-card.component';

import { EligibilityCriteriaContent } from './eligibility-criteria-content.component';
import { EditEligibilityCriteria } from './edit-eligibility-criteria/edit-eligibility-criteria.component';
import styles from './eligilibity-criteria.module.scss';

type EligibilityCriteriaProps = {
  employeeId: string;
  occupation: (CustomerOccupation & CustomerAbsenceEmployment) | null;
  absenceEmployment: CustomerAbsenceEmployment | null;
  isAbsenceEmploymentPending: boolean;
};

const DEFAULT_OCCUPATION = {
  hoursWorkedPerYear: null,
  withinFMLACriteria: false,
  workingAtHome: false,
  keyEmployee: false,
  employmentWorkState: null,
  cbaValue: '',
  occupationQualifiers: [] as CustomerAbsenceEmployment['occupationQualifiers']
};

export const EligibilityCriteria: React.FC<EligibilityCriteriaProps> = ({
  employeeId,
  occupation,
  absenceEmployment,
  isAbsenceEmploymentPending
}) => {
  const hasEditPermission = usePermissions(
    ManageOccupationPermissions.VIEW_EDIT_EMPLOYMENT_DETAILS
  );

  const {
    hoursWorkedPerYear,
    withinFMLACriteria,
    workingAtHome,
    keyEmployee,
    employmentWorkState,
    cbaValue,
    occupationQualifiers
  } = occupation || DEFAULT_OCCUPATION;

  const hasTextDataConditions = [
    !!hoursWorkedPerYear ||
      cbaValue ||
      !isDomainNameUnknown(employmentWorkState) ||
      occupationQualifiers.some(qualifier =>
        Boolean(qualifier.qualifierDescription)
      )
  ];
  const hasAnyBooleanData = [
    withinFMLACriteria,
    workingAtHome,
    keyEmployee
  ].some(Boolean);
  const hasAnyTextData = hasTextDataConditions.some(condition =>
    Boolean(condition)
  );
  const hasAnyData = hasAnyBooleanData || hasAnyTextData;
  return (
    <EmploymentDataCard
      title={
        <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.TITLE" />
      }
      isLoading={isAbsenceEmploymentPending}
      emptyContent={
        <>
          <FormattedMessage
            className={styles.emptyState}
            id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EMPTY_STATE"
          />
          {hasEditPermission && (
            <EditEligibilityCriteria
              titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
              buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
              buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
              employeeId={employeeId}
            />
          )}
        </>
      }
    >
      {hasAnyData && (
        <EligibilityCriteriaContent
          absenceEmployment={absenceEmployment!}
          occupation={occupation}
          employeeId={employeeId}
        />
      )}
    </EmploymentDataCard>
  );
};
