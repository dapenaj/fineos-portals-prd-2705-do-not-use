/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { Moment } from 'moment';
import {
  CustomerOccupation,
  CustomerContractualEarnings,
  CustomerAbsenceEmployment,
  BaseDomain
} from 'fineos-js-api-client';

import { unscaledAmount } from '../../../../../../shared';
import {
  validateIfBefore,
  validateIfAfter,
  TOMORROW,
  TODAY
} from '../../../../../../shared/validation';
import {
  isEnumValidationNotEmpty,
  requiredEnumValidation
} from '../../../../../../shared/ui/enum-select/enum-validation.util';

export const buildEditEmploymentDetailsValidation = (
  shouldShowContractualEarnings: boolean,
  isNewContractualEarnings: boolean
) =>
  Yup.object().shape({
    jobTitle: Yup.string()
      .matches(/[a-z]/, 'FINEOS_COMMON.VALIDATION.REQUIRED')
      .max(100, 'PROFILE.EMPLOYMENT_DETAILS_TAB.JOB_TITLE_LENGTH_VALIDATION')
      .required('FINEOS_COMMON.VALIDATION.REQUIRED'),
    jobStartDate: Yup.date()
      .nullable()
      .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      .test(
        validateIfBefore(
          TOMORROW,
          'PROFILE.EMPLOYMENT_DETAILS_TAB.START_DATE_VALIDATION'
        )
      ),
    jobEndDate: Yup.date()
      .nullable()
      .test(
        validateIfAfter(
          TODAY,
          'PROFILE.EMPLOYMENT_DETAILS_TAB.END_DATE_VALIDATION'
        )
      ),
    hrsWorkedPerWeek: Yup.number()
      .nullable()
      .min(0, 'FINEOS_COMMON.VALIDATION.INVALID_NUMBER_VALUE')
      .max(99.99, 'FINEOS_COMMON.VALIDATION.MAX_WORKING_HOURS_PER_WEEK_AMOUNT')
      .typeError('FINEOS_COMMON.VALIDATION.NOT_NUMERIC'),
    earnings: contractualEarningsValidation(
      shouldShowContractualEarnings,
      isNewContractualEarnings
    ),
    employmentType: Yup.object(),
    isAdjustedDateOfHireVisible: Yup.boolean().nullable(),
    adjustedHireDate: Yup.date()
      .nullable()
      .when('isAdjustedDateOfHireVisible', {
        is: true,
        then: Yup.date().required('FINEOS_COMMON.VALIDATION.REQUIRED')
      })
  });

const hasValidDate = (date: Moment | undefined | null) =>
  date && date.isValid();

export const buildEditEmploymentDetailsInitialValues = (
  occupation: CustomerOccupation | null,
  contractualEarnings: CustomerContractualEarnings | null,
  absenceEmployment: CustomerAbsenceEmployment | null
) => ({
  jobTitle: occupation?.jobTitle,
  jobStartDate: hasValidDate(occupation?.jobStartDate)
    ? occupation!.jobStartDate
    : null,
  jobEndDate: hasValidDate(occupation?.jobEndDate)
    ? occupation!.jobEndDate
    : null,
  hrsWorkedPerWeek: occupation?.hrsWorkedPerWeek,
  occupationCategory:
    occupation?.employmentCat?.name !== 'Unknown'
      ? occupation?.employmentCat
      : null,
  earnings: {
    amount: contractualEarnings?.amount
      ? unscaledAmount(
          contractualEarnings?.amount?.amountMinorUnits,
          contractualEarnings?.amount?.scale
        )
      : null,
    frequency: contractualEarnings?.frequency
      ? contractualEarnings.frequency
      : null
  },
  employmentType: absenceEmployment?.employmentType,
  isAdjustedDateOfHireVisible: hasValidDate(
    absenceEmployment?.adjustedHireDate
  ),
  adjustedHireDate: hasValidDate(absenceEmployment?.adjustedHireDate)
    ? absenceEmployment!.adjustedHireDate
    : null
});

const contractualEarningsValidation = (
  shouldShowContractualEarnings: boolean,
  isNewContractualEarnings: boolean
) =>
  Yup.lazy(
    (earnings: { amount: number; frequency: BaseDomain }): Yup.Schema<any> =>
      shouldShowContractualEarnings &&
      (earnings.amount ||
        isEnumValidationNotEmpty(earnings.frequency) ||
        !isNewContractualEarnings)
        ? Yup.object({
            amount: Yup.number()
              .required('FINEOS_COMMON.VALIDATION.REQUIRED')
              .transform((value, originalValue) =>
                originalValue === null ? undefined : value
              )
              .typeError('FINEOS_COMMON.VALIDATION.NOT_NUMERIC')
              .moreThan(0, 'FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO')
              .test(
                'max-length',
                'PROFILE.EMPLOYMENT_DETAILS_TAB.MAX_DIGITS_VALIDATION',
                value => value && value.toString().replace('.', '').length <= 14
              )
              .test(
                'is-decimal',
                'FINEOS_COMMON.VALIDATION.INVALID_PRECISION',
                value => {
                  const precisionValue = 2;
                  const splitValue =
                    value
                      ?.toLocaleString(undefined, {
                        useGrouping: false,
                        maximumSignificantDigits: 21
                      })
                      .split('.') || [];
                  return (
                    splitValue.length === 1 ||
                    (splitValue.length > 1 &&
                      splitValue[1].length <= precisionValue)
                  );
                }
              )
              .transform((currentValue, originalValue) =>
                typeof originalValue === 'string' && originalValue.endsWith('.')
                  ? originalValue
                  : currentValue
              ),
            frequency: requiredEnumValidation('frequency')
          })
        : Yup.object({
            amount: Yup.number()
              .typeError('FINEOS_COMMON.VALIDATION.NOT_NUMERIC')
              .moreThan(0, 'FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO')
              .nullable(),
            frequency: Yup.mixed().nullable()
          })
  );
