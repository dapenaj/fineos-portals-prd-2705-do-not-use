/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useState } from 'react';
import { Field } from 'formik';
import classNames from 'classnames';
import {
  FormattedMessage,
  FormGroup,
  CheckboxGroup,
  Checkbox,
  FormLabel,
  ConfigurableText,
  NumberInput,
  TextInput,
  RichFormattedMessage,
  FormButton,
  UiButton,
  ButtonType,
  createThemedStyles,
  textStyleToCss,
  useAsync,
  Form,
  DrawerWidth,
  Button,
  Drawer,
  DrawerFooter
} from 'fineos-common';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation
} from 'fineos-js-api-client';

import {
  EnumDomain,
  EnumSelect,
  FormikWithResponseHandling
} from '../../../../../../shared';
import { LanguageContext } from '../../../../../../shared/layouts/language/language.context';
import {
  fetchMostRecentCustomerOccupation,
  editCustomerOccupation
} from '../../../../employee-profile.api';

import {
  buildEditEligibilityCriteriaInitialValues,
  buildEditEligibilityCriteriaValidation
} from './edit-eligibility-criteria.form';
import {
  addAbsenceEmployment,
  editAbsenceEmployment
} from './edit-eligibility-criteria.api';
import { EditEligibilityCriteriaForm } from './edit-eligibility-criteria.type';
import {
  parsedEligibilityCriteria,
  isAbsenceEmploymentNotChanged,
  filteredCheckboxValues
} from './edit-eligibility-criteria.utils';
import styles from './edit-eligibility-criteria.module.scss';

type EditEligibilityCriteriaProps = {
  employeeId: string;
  titleId: string;
  buttonId: string;
  buttonAriaLabelId: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    ...textStyleToCss(theme.typography.smallText)
  },
  text: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  }
}));

export const EditEligibilityCriteria: React.FC<EditEligibilityCriteriaProps> = ({
  employeeId,
  titleId,
  buttonId,
  buttonAriaLabelId
}) => {
  const { decimalSeparator } = useContext(LanguageContext);
  const {
    state: { value: occupation },
    executeAgain: refreshOccupation,
    changeAsyncValue: refreshOccupationInMemory
  } = useAsync(fetchMostRecentCustomerOccupation, [employeeId!], {
    shouldExecute: () => !!employeeId
  });

  const [isDrawerVisible, setIsDrawerVisible] = useState(false);

  const handleSubmit = async (formData: EditEligibilityCriteriaForm) => {
    if (isAbsenceEmploymentNotChanged(formData, occupation as any)) {
      setIsDrawerVisible(false);
      return;
    }

    if (occupation) {
      const parsedEligibilityCriteriaData = parsedEligibilityCriteria(
        formData,
        occupation as CustomerOccupation & CustomerAbsenceEmployment
      );

      const parsedOccupation = {
        ...occupation,
        ...parsedEligibilityCriteriaData
      } as CustomerOccupation & CustomerAbsenceEmployment;

      await editCustomerOccupation(employeeId, parsedOccupation);

      refreshOccupation();
      setIsDrawerVisible(false);
    }
  };

  const themedStyles = useThemedStyles();
  return (
    <>
      <FormattedMessage
        onClick={() => setIsDrawerVisible(true)}
        as={Button}
        buttonType={ButtonType.LINK}
        className={styles.actionButton}
        id={buttonId}
        ariaLabelName={{
          descriptor: {
            id: buttonAriaLabelId
          }
        }}
      />

      <Drawer
        ariaLabelId={titleId}
        title={<FormattedMessage id={titleId} />}
        width={DrawerWidth.NARROW}
        isVisible={isDrawerVisible}
        onClose={() => setIsDrawerVisible(false)}
      >
        <FormikWithResponseHandling
          initialValues={buildEditEligibilityCriteriaInitialValues(
            occupation as any
          )}
          validationSchema={buildEditEligibilityCriteriaValidation}
          onSubmit={handleSubmit}
          dataParseFunction={data => {
            refreshOccupationInMemory(data);
            return {
              ...data,
              checkboxValues: filteredCheckboxValues(data)
            };
          }}
        >
          <Form className={styles.eligibilityForm}>
            <div className={styles.heightStretch}>
              <FormGroup className={styles.checkboxWrapper}>
                <Field name="checkboxValues" component={CheckboxGroup}>
                  <Checkbox value="withinFMLACriteria">
                    <FormLabel
                      tooltipContent={
                        <ConfigurableText id="EDIT_ELIGIBILITY_CRITERIA.WITHIN_FMLA.POPOVER" />
                      }
                    >
                      <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WITHIN_FMLA_RADIUS" />
                    </FormLabel>
                  </Checkbox>
                  <Checkbox value="workingAtHome">
                    <FormLabel>
                      <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORKS_AT_HOME" />
                    </FormLabel>
                  </Checkbox>
                  <Checkbox value="keyEmployee">
                    <FormLabel>
                      <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.KEY_EMPLOYEE" />
                    </FormLabel>
                  </Checkbox>
                </Field>
              </FormGroup>
              <FormGroup
                className={styles.inputGroup}
                labelClassName={themedStyles.label}
                label={
                  <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR" />
                }
              >
                <Field
                  name="hoursWorkedPerYear"
                  component={NumberInput}
                  decimalSeparator={decimalSeparator}
                  min={0}
                  precision={0}
                />
              </FormGroup>
              <FormGroup
                className={styles.inputGroup}
                labelClassName={themedStyles.label}
                label={
                  <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORK_STATE" />
                }
              >
                <Field
                  name="employmentWorkState"
                  component={EnumSelect}
                  enumDomain={EnumDomain.US_STATES}
                />
              </FormGroup>
              <FormGroup
                className={styles.inputGroup}
                labelClassName={themedStyles.label}
                label={
                  <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER" />
                }
              >
                <Field
                  name="cbaValue"
                  component={TextInput}
                  allowClear={true}
                  placeholder={
                    <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER_PLACEHOLDER" />
                  }
                />
              </FormGroup>
              <RichFormattedMessage
                className={classNames(
                  styles.qualifiersRemark,
                  themedStyles.text
                )}
                id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.NOT_EDITED_QUALIFIERS_REMARK"
              />
            </div>
            <DrawerFooter>
              <FormattedMessage
                as={FormButton}
                htmlType="submit"
                shouldSubmitForm={false}
                id="FINEOS_COMMON.GENERAL.OK"
              />
              <FormattedMessage
                as={UiButton}
                buttonType={ButtonType.LINK}
                onClick={() => setIsDrawerVisible(false)}
                id="FINEOS_COMMON.GENERAL.CANCEL"
              />
            </DrawerFooter>
          </Form>
        </FormikWithResponseHandling>
      </Drawer>
    </>
  );
};
