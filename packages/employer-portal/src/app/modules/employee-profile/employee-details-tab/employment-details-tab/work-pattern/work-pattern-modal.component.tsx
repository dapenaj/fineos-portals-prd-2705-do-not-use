/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  FormattedMessage,
  Button,
  ButtonType,
  useAsync,
  ModalWidth,
  Modal
} from 'fineos-common';
import { WeekBasedWorkPattern } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { ModalFormViewMode, WorkPatternBasis } from '../../../../../shared';
import { fetchMostRecentCustomerOccupation } from '../../../employee-profile.api';
import { EmploymentCardActions } from '../common/employment-card-actions';

import { WorkPatternModalBody } from './work-pattern-body.component';
import { fetchWorkPattern } from './work-pattern.api';

type WorkPatternModalProps = {
  customerId: string;
  onSetMode: (mode: ModalFormViewMode) => void;
  modalHeader?: React.ReactElement;
  triggerButtonName?: React.ReactElement;
};

export const WorkPatternModal: React.FC<WorkPatternModalProps> = ({
  customerId,
  onSetMode,
  modalHeader = <FormattedMessage id="WORK_PATTERN.EDIT_WORK_PATTERN" />,
  triggerButtonName = (
    <FormattedMessage
      data-test-el="edit-work-pattern-button"
      id="WORK_PATTERN.EDIT_WORK_PATTERN"
    />
  )
}) => {
  const [
    refreshedData,
    setRefreshedData
  ] = useState<WeekBasedWorkPattern | null>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {
    state: { value: occupation }
  } = useAsync(fetchMostRecentCustomerOccupation, [customerId!]);
  const isWeeklyBasedPattern =
    occupation?.workPatternBasis === WorkPatternBasis.WEEK_BASED;
  const { changeAsyncValue: refreshWorkPattern } = useAsync(
    fetchWorkPattern,
    [customerId, occupation?.id!],
    {
      shouldExecute: () => isWeeklyBasedPattern
    }
  );
  const triggerButton = React.cloneElement(triggerButtonName, {
    as: Button,
    buttonType: ButtonType.LINK,
    ariaLabelName: {
      descriptor: {
        id: 'WORK_PATTERN.EDIT_WORK_PATTERN_ARIA_LABEL'
      }
    },
    onClick: () => setIsModalVisible(true)
  });

  return (
    <EmploymentCardActions>
      {triggerButton}
      <Modal
        visible={isModalVisible}
        ariaLabelId="WORK_PATTERN.EDIT_WORK_PATTERN"
        data-test-el="edit-work-pattern-modal"
        header={modalHeader}
        onCancel={() => {
          setIsModalVisible(false);
          if (!_isEmpty(refreshedData)) {
            refreshWorkPattern(refreshedData);
          }
        }}
        width={ModalWidth.WIDE}
      >
        <WorkPatternModalBody
          onFinish={() => setIsModalVisible(false)}
          refreshedData={refreshedData}
          setRefreshedData={setRefreshedData}
          onSetMode={onSetMode}
          customerId={customerId}
        />
      </Modal>
    </EmploymentCardActions>
  );
};
