/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Tabs,
  PropertyItem,
  FormattedMessage,
  usePermissions,
  ViewOccupationPermissions,
  useAsync
} from 'fineos-common';
import { Row, Col } from 'antd';
import { CustomerOccupation, CustomerAbsenceEmployment } from 'fineos-js-api-client';
import classNames from 'classnames';

import { TabHeader } from '../tab-header';
import { TabBody } from '../tab-body';
import { useEmployeeDetailsTabThemedStyles } from '../employee-details-tab.styles';
import styles from '../employee-details-tab.module.scss';

import { CurrentOccupation } from './current-occupation/current-occupation.component';
import { fetchAbsenceEmployment } from './employment-details-tab.api';
import { EligibilityCriteria } from './eligibility-criteria/eligibility-criteria.component';
import { WorkPattern } from './work-pattern';

type EmploymentDetailsTabProps = React.ComponentProps<typeof Tabs.TabPane> & {
  employeeId: string;
  isOccupationPending: boolean;
  occupation: (CustomerOccupation & CustomerAbsenceEmployment) | null;
  onOccupationChange: () => void;
};

export const EmploymentDetailsTab: React.FC<EmploymentDetailsTabProps> = ({
  employeeId,
  isOccupationPending,
  occupation,
  onOccupationChange,
  ...props
}) => {
  const hasRegularWeeklyWorkPatternPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
  );

  const hasViewAbsenceEmploymentPermission = usePermissions(
    ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT
  );

  const {
    state: { value: absenceEmployment, isPending: isAbsenceEmploymentPending }
  } = useAsync(fetchAbsenceEmployment, [employeeId, occupation?.id || ''], {
    shouldExecute: () => !!occupation?.id && hasViewAbsenceEmploymentPermission
  });

  const commonThemedStyles = useEmployeeDetailsTabThemedStyles();

  return (
    <Tabs.TabPane {...props} data-test-el="employment-details-tab">
      <TabHeader>
        <Row>
          <Col md={16}>
            <FormattedMessage
              as="h2"
              id="PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE"
              className={classNames(styles.header, commonThemedStyles.header)}
            />
          </Col>
          <Col>
            <PropertyItem
              label={
                <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EMPLOYEE_ID" />
              }
            >
              <span className={commonThemedStyles.text}>{employeeId}</span>
            </PropertyItem>
          </Col>
        </Row>
      </TabHeader>
      <TabBody>
        <>
          <div className={styles.employeeDetailsCard}>
            <CurrentOccupation
              employeeId={employeeId}
              occupation={occupation}
              isOccupationPending={isOccupationPending}
              absenceEmployment={absenceEmployment}
              isAbsenceEmploymentPending={isAbsenceEmploymentPending}
            />
          </div>
          {
            <div className={styles.employeeDetailsCard}>
              <EligibilityCriteria
                employeeId={employeeId}
                absenceEmployment={absenceEmployment}
                occupation={occupation}
                isAbsenceEmploymentPending={isAbsenceEmploymentPending}
              />
            </div>
          }
          <>
            {hasRegularWeeklyWorkPatternPermission && occupation && (
              <div className={styles.employeeDetailsCard}>
                <WorkPattern occupation={occupation} customerId={employeeId} />
              </div>
            )}
          </>
        </>
      </TabBody>
    </Tabs.TabPane>
  );
};
