/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useContext } from 'react';
import { Row, Col } from 'antd';
import { Field } from 'formik';
import classNames from 'classnames';
import { isEqual as _isEqual, isEmpty as _isEmpty } from 'lodash';
import { Moment } from 'moment';
import {
  FormGroup,
  NumberInput,
  TextInput,
  DatePickerInput,
  FormButton,
  UiButton,
  ButtonType,
  Form,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage,
  usePermissions,
  ViewOccupationPermissions,
  ManageOccupationPermissions,
  FormSwitch,
  SwitchLabel,
  useAsync,
  ConfigurableText,
  PartialApiCompleteError,
  AsteriskInfo,
  DrawerWidth,
  Button,
  Drawer,
  DrawerFooter
} from 'fineos-common';
import {
  CustomerAbsenceEmployment,
  CustomerContractualEarnings
} from 'fineos-js-api-client';

import {
  EnumSelect,
  EnumDomain,
  useEnumDomain,
  FormikWithResponseHandling,
  fetchContractualEarnings,
  getEnumDomainTranslation
} from '../../../../../../shared';
import { EarningFrequencyTypeSelect } from '../../../../../../shared/ui/earning-frequency-select';
import { LanguageContext } from '../../../../../../shared/layouts/language/language.context';
import { fetchMostRecentCustomerOccupation } from '../../../../employee-profile.api';
import { fetchAbsenceEmployment } from '../../employment-details-tab.api';

import { AdjustedDateOfHireFormGroup } from './adjusted-date-of-hire-form-group/adjusted-date-of-hire-form-group.component';
import { EmploymentDetailsForm } from './edit-employment-details-form.type';
import {
  buildEditEmploymentDetailsInitialValues,
  buildEditEmploymentDetailsValidation
} from './edit-employment-details.form';
import {
  addAbsenceEmployment,
  addContractualEarnings,
  editContractualEarnings,
  editEmploymentDetails,
  editAbsenceEmployment,
  updateEmploymentDetails,
  UpdateEmploymentDetailsRequests,
  UpdateEmploymentDetails
} from './edit-employment-details.api';
import {
  shouldUpdateContractualEarnings,
  shouldUpdateAbsenceData,
  parsedOccupation,
  parsedContractualEarnings,
  parsedAbsenceEmployment,
  isNewContractualEarnings
} from './edit-employment-details.utils';
import styles from './edit-employment-details.module.scss';

type EditEmploymentDetailsProps = {
  employeeId: string;
  titleId: string;
  buttonId: string;
  buttonAriaLabelId: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  value: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  dropdown: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const EditEmploymentDetails: React.FC<EditEmploymentDetailsProps> = ({
  employeeId,
  titleId,
  buttonId,
  buttonAriaLabelId
}) => {
  const themedStyles = useThemedStyles();
  const { decimalSeparator } = useContext(LanguageContext);
  const { domains: earningTypeDomains } = useEnumDomain(
    EnumDomain.EARNINGS_TYPE
  );
  const [hasPartialEditError, setHasPartialEditError] = useState<boolean>(
    false
  );
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);

  const hasViewEarningsPermission = usePermissions([
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
  ]);
  const hasEditEarningsPermission = usePermissions([
    ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
  ]);
  const hasAddEarningsPermission = usePermissions([
    ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
  ]);
  const hasViewCustomerOccupationsPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
  );
  const hasViewAbsenceEmploymentPermission = usePermissions(
    ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT
  );
  const hasEditAbsenceEmploymentPermission = usePermissions(
    ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT
  );
  const hasAddAbsenceEmploymentPermission = usePermissions(
    ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT
  );

  const {
    state: { value: occupation },
    changeAsyncValue: refreshOccupationsInMemory
  } = useAsync(fetchMostRecentCustomerOccupation, [employeeId!], {
    shouldExecute: () => hasViewCustomerOccupationsPermission
  });

  const {
    state: { value: contractualEarnings },
    changeAsyncValue: refreshContractualEarningsInMemory
  } = useAsync(fetchContractualEarnings, [employeeId, occupation?.id || ''], {
    shouldExecute: () =>
      !!occupation?.id && hasViewCustomerOccupationsPermission
  });

  const {
    state: { value: absenceEmployment },
    changeAsyncValue: refreshAbsenceEmploymentInMemory
  } = useAsync(fetchAbsenceEmployment, [employeeId, occupation?.id || ''], {
    shouldExecute: () => !!occupation?.id && hasViewAbsenceEmploymentPermission
  });

  const shouldDisableContractualEarnings =
    (contractualEarnings && !hasEditEarningsPermission) ||
    (!contractualEarnings && !hasAddEarningsPermission);

  const shouldDisableAbsenceEmployment =
    !hasAddAbsenceEmploymentPermission || !hasEditAbsenceEmploymentPermission;

  const handleSubmit = async (formData: EmploymentDetailsForm) => {
    const requestsToSend: UpdateEmploymentDetailsRequests = {};
    const parsedOccupationData = parsedOccupation(formData, occupation!);

    const parsedContractualEarningsData = parsedContractualEarnings(
      formData,
      contractualEarnings,
      earningTypeDomains,
      decimalSeparator
    );
    const parsedAbsenceEmploymentData = parsedAbsenceEmployment(
      formData,
      absenceEmployment
    );
    const shouldUpdateEarnings = shouldUpdateContractualEarnings(
      formData,
      contractualEarnings
    );
    const shouldUpdateOccupation = !_isEqual(occupation, parsedOccupationData);
    const shouldUpdateAbsenceEmployment = shouldUpdateAbsenceData(
      formData,
      absenceEmployment
    );

    if (shouldUpdateOccupation) {
      requestsToSend.occupation = editEmploymentDetails(
        employeeId,
        parsedOccupationData
      );
    }

    if (shouldUpdateEarnings) {
      const requestParams = [
        employeeId,
        occupation!.id,
        parsedContractualEarningsData as CustomerContractualEarnings
      ] as const;
      requestsToSend.earnings = isNewContractualEarnings(contractualEarnings)
        ? addContractualEarnings(...requestParams)
        : editContractualEarnings(...requestParams);
    }

    if (shouldUpdateAbsenceEmployment) {
      const requestParams = [
        employeeId,
        occupation!.id,
        parsedAbsenceEmploymentData as CustomerAbsenceEmployment
      ] as const;
      requestsToSend.absence = !!absenceEmployment
        ? editAbsenceEmployment(...requestParams)
        : addAbsenceEmployment(...requestParams);
    }

    if (_isEmpty(requestsToSend)) {
      setIsDrawerVisible(false);
      return;
    }
    const applyResults = (result: UpdateEmploymentDetails) => {
      if (shouldUpdateOccupation && result.occupation) {
        refreshOccupationsInMemory(result.occupation);
      }

      if (shouldUpdateEarnings && result.earnings) {
        refreshContractualEarningsInMemory(result.earnings);
      }

      if (shouldUpdateAbsenceEmployment && result.absence) {
        refreshAbsenceEmploymentInMemory(result.absence);
      }
    };

    try {
      const result = applyResults(
        await updateEmploymentDetails(requestsToSend)
      );
      setIsDrawerVisible(false);
      return result;
    } catch (partialErrorResponseData) {
      if (partialErrorResponseData instanceof PartialApiCompleteError) {
        setHasPartialEditError(true);
        applyResults(partialErrorResponseData.partialResult);
      }

      throw partialErrorResponseData;
    }
  };

  const parseEmploymentDetails = (data: UpdateEmploymentDetails) => {
    data.earnings && refreshContractualEarningsInMemory(data.earnings);
    data.occupation && refreshOccupationsInMemory(data.occupation);
    data.absence && refreshAbsenceEmploymentInMemory(data.absence);
    return buildEditEmploymentDetailsInitialValues(
      data.occupation ?? occupation,
      data.earnings ?? contractualEarnings,
      data.absence ?? absenceEmployment
    );
  };

  return (
    <>
      <FormattedMessage
        onClick={() => setIsDrawerVisible(true)}
        as={Button}
        buttonType={ButtonType.LINK}
        id={buttonId}
        ariaLabelName={{
          descriptor: {
            id: buttonAriaLabelId
          }
        }}
      />
      <Drawer
        isVisible={isDrawerVisible}
        ariaLabelId={titleId}
        title={<FormattedMessage id={titleId} />}
        width={DrawerWidth.NARROW}
        onClose={() => setIsDrawerVisible(false)}
      >
        <FormikWithResponseHandling
          initialValues={buildEditEmploymentDetailsInitialValues(
            occupation,
            contractualEarnings,
            absenceEmployment
          )}
          validationSchema={buildEditEmploymentDetailsValidation(
            hasViewEarningsPermission,
            isNewContractualEarnings(contractualEarnings)
          )}
          onSubmit={handleSubmit}
          genericErrorModalMessage={
            <FormattedMessage
              id={
                hasPartialEditError
                  ? 'FINEOS_COMMON.SOMETHING_WENT_WRONG.PARTIAL_API_ERROR_MESSAGE'
                  : 'FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE'
              }
            />
          }
          dataParseFunction={parseEmploymentDetails}
        >
          {({ setFieldValue, values }) => (
            <>
              <Form
                className={styles.employmentForm}
                data-test-el="edit-employment-details-form"
              >
                <div className={styles.heightStretch}>
                  <AsteriskInfo />
                  <Row justify="space-between">
                    <Col xs={24} sm={11}>
                      <FormGroup
                        isRequired={true}
                        labelClassName={themedStyles.label}
                        label={
                          <FormattedMessage
                            id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE"
                            ariaLabelName={{
                              descriptor: {
                                id:
                                  'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE'
                              }
                            }}
                          />
                        }
                      >
                        <Field name="jobTitle" component={TextInput} />
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={11}>
                      <FormGroup
                        labelClassName={themedStyles.label}
                        label={
                          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EMPLOYMENT_TYPE" />
                        }
                      >
                        <Field
                          name="employmentType"
                          component={EnumSelect}
                          enumDomain={EnumDomain.EMPLOYMENT_TYPE}
                          isDisabled={shouldDisableAbsenceEmployment}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row justify="space-between">
                    <Col xs={24} sm={11}>
                      <FormGroup
                        isRequired={true}
                        className={classNames(
                          styles.datepickerGroup,
                          themedStyles.value
                        )}
                        labelClassName={themedStyles.label}
                        label={
                          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_START_DATE" />
                        }
                        labelTooltipContent={
                          <ConfigurableText id="EDIT_EMPLOYMENT_DETAILS.JOB_START_DATE.POPOVER" />
                        }
                      >
                        <Field
                          disabledDate={(date: Moment) =>
                            date.isAfter(values.jobEndDate)
                          }
                          name="jobStartDate"
                          data-test-el="job-start-date-picker"
                          component={DatePickerInput}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={11}>
                      <FormGroup
                        labelClassName={themedStyles.label}
                        label={
                          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.FULL_OR_PART_TIME" />
                        }
                      >
                        <Field
                          name="occupationCategory"
                          component={EnumSelect}
                          enumDomain={EnumDomain.OCCUPATION_CATEGORY}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row justify="space-between">
                    <Col xs={24} sm={11}>
                      <FormGroup
                        className={classNames(
                          styles.datepickerGroup,
                          themedStyles.value
                        )}
                        labelClassName={themedStyles.label}
                        label={
                          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_END_DATE" />
                        }
                        labelTooltipContent={
                          <ConfigurableText id="EDIT_EMPLOYMENT_DETAILS.JOB_END_DATE.POPOVER" />
                        }
                      >
                        <Field
                          disabledDate={(date: Moment) =>
                            date?.isBefore(values.jobStartDate)
                          }
                          name="jobEndDate"
                          data-test-el="job-end-date-picker"
                          component={DatePickerInput}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs={24} sm={11}>
                      <FormGroup
                        isRequired={true}
                        className={classNames(
                          styles.hrsWorkedPerWeekGroup,
                          themedStyles.value
                        )}
                        labelClassName={themedStyles.label}
                        label={
                          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK" />
                        }
                      >
                        <Field
                          name="hrsWorkedPerWeek"
                          precision={1}
                          component={NumberInput}
                          value={values.hrsWorkedPerWeek}
                          decimalSeparator={decimalSeparator}
                          minValue={0}
                          maxValue={99.9}
                          onChange={(value: number) =>
                            setFieldValue('hrsWorkedPerWeek', value || 0)
                          }
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row className={styles.row}>
                    <Field
                      name="isAdjustedDateOfHireVisible"
                      component={FormSwitch}
                      size="small"
                    />
                    <SwitchLabel>
                      <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ADJUSTED_DATE_OF_HIRE" />
                    </SwitchLabel>
                  </Row>
                  <>
                    {values.isAdjustedDateOfHireVisible && (
                      <Row>
                        <Col xs={24} sm={11}>
                          <AdjustedDateOfHireFormGroup
                            isDisabled={shouldDisableAbsenceEmployment}
                          />
                        </Col>
                      </Row>
                    )}
                  </>

                  {hasViewEarningsPermission && (
                    <Row justify="space-between" className={styles.row}>
                      <Col xs={24} sm={11}>
                        <FormGroup
                          labelClassName={themedStyles.label}
                          label={
                            <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS" />
                          }
                          data-test-el="earnings-field"
                        >
                          <Field
                            name="earnings.amount"
                            virtualGroup="earnings"
                            isDisabled={shouldDisableContractualEarnings}
                            component={NumberInput}
                            decimalSeparator={decimalSeparator}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={24} sm={11}>
                        <FormGroup
                          labelClassName={themedStyles.label}
                          label={
                            <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.FREQUENCY" />
                          }
                          data-test-el="earnings-frequency-field"
                        >
                          <Field
                            name="earnings.frequency"
                            dropdownClassName={themedStyles.dropdown}
                            virtualGroup="frequency"
                            isDisabled={shouldDisableContractualEarnings}
                            data-test-el="earning-frequency"
                            component={EarningFrequencyTypeSelect}
                            value={
                              <FormattedMessage
                                id={getEnumDomainTranslation(
                                  EnumDomain.EARNING_FREQUENCY,
                                  values?.frequency?.name || 'Unknown'
                                )}
                              />
                            }
                            enumDomain={EnumDomain.EARNING_FREQUENCY}
                            shouldSkipUnknown={Boolean(values.frequency)} // TODO - should this be initialValues???
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  )}
                </div>
              </Form>
              <DrawerFooter>
                <FormattedMessage
                  as={FormButton}
                  htmlType="submit"
                  id="FINEOS_COMMON.GENERAL.OK"
                />
                <FormattedMessage
                  as={UiButton}
                  buttonType={ButtonType.LINK}
                  onClick={() => setIsDrawerVisible(false)}
                  id="FINEOS_COMMON.GENERAL.CANCEL"
                />
              </DrawerFooter>
            </>
          )}
        </FormikWithResponseHandling>
      </Drawer>
    </>
  );
};
