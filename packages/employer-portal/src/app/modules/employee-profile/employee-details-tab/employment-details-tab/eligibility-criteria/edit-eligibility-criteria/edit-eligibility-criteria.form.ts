/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { CustomerAbsenceEmployment } from 'fineos-js-api-client';

import { filteredCheckboxValues } from './edit-eligibility-criteria.utils';

const HOURS_IN_YEAR = 24 * 365;

export const buildEditEligibilityCriteriaInitialValues = (
  absenceEmployment: CustomerAbsenceEmployment | null
) => ({
  checkboxValues: absenceEmployment
    ? filteredCheckboxValues(absenceEmployment)
    : [],
  hoursWorkedPerYear: absenceEmployment?.hoursWorkedPerYear,
  employmentWorkState: absenceEmployment?.employmentWorkState,
  cbaValue: absenceEmployment?.cbaValue
});

export const buildEditEligibilityCriteriaValidation = Yup.object({
  checkboxValues: Yup.array()
    .min(0)
    .max(3),
  hoursWorkedPerYear: Yup.number()
    .nullable()
    .min(0, 'FINEOS_COMMON.VALIDATION.MORE_THAN_OR_EQUAL_ZERO')
    .max(
      HOURS_IN_YEAR,
      'FINEOS_COMMON.VALIDATION.MAX_WORKING_HOURS_PER_YEAR_AMOUNT'
    )
    .typeError('FINEOS_COMMON.VALIDATION.NOT_NUMERIC'),
  employmentWorkState: Yup.mixed(),
  cbaValue: Yup.string()
    .nullable()
    .max(20, 'FINEOS_COMMON.VALIDATION.MAX_CBA_LENGTH')
});
