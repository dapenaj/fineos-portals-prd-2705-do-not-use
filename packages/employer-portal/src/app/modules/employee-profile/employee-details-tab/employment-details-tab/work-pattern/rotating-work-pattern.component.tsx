/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { getIn, useFormikContext } from 'formik';
import {
  FormattedMessage,
  Card,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';

import {
  ViewAllWeekPattern,
  getWeekNumbersAsInt,
  rotatingWeeks,
  RotatingPatternType
} from '../../../../../shared';

import styles from './work-pattern.module.scss';

type ViewAllWeekPatternProps = {
  nameScope?: string;
  weekBasedDayPatternNullable?: RotatingPatternType;
  workPatternType: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  }
}));

export const RotatingWorkPattern: React.FC<ViewAllWeekPatternProps> = ({
  nameScope,
  weekBasedDayPatternNullable,
  workPatternType
}) => {
  const themedStyles = useThemedStyles();
  const formik = useFormikContext();
  const state = weekBasedDayPatternNullable
    ? weekBasedDayPatternNullable
    : nameScope && getIn(formik.values, nameScope);

  const weeksToRender = useMemo(() => {
    const weeks = [];
    const weeksNumber = getWeekNumbersAsInt(workPatternType);

    for (let count = 1; count <= weeksNumber; count++) {
      const rotating = rotatingWeeks[count];
      weeks.push(
        <div key={count} className={styles.wrapper}>
          <FormattedMessage
            className={themedStyles.title}
            id="WORK_PATTERN.ROTATING_WEEK_HEADER"
            values={{ count }}
          />
          <Card className={styles.card}>
            {!nameScope ? (
              <ViewAllWeekPattern
                index={count}
                weekBasedDayPatternNullable={state[rotating]}
              />
            ) : (
              <ViewAllWeekPattern
                index={count}
                nameScope={`${nameScope}.${rotating}`}
              />
            )}
          </Card>
        </div>
      );
    }

    return weeks;
  }, [state, nameScope, workPatternType, themedStyles]);

  return <>{weeksToRender}</>;
};
