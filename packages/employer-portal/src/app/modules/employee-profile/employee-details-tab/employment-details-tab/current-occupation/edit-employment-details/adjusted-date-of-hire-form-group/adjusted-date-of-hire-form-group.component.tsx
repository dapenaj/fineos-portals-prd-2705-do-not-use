/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment from 'moment';
import { Field, useFormikContext } from 'formik';
import {
  ConfigurableText,
  createThemedStyles,
  DatePickerInput,
  FormattedMessage,
  FormGroup,
  textStyleToCss
} from 'fineos-common';

import { EmploymentDetailsForm } from '../edit-employment-details-form.type';
import styles from '../edit-employment-details.module.scss';

type AdjustedDateOfHireFormGroupProps = {
  isDisabled?: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  warningMessage: {
    color: theme.colorSchema.attentionColor,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const AdjustedDateOfHireFormGroup: React.FC<AdjustedDateOfHireFormGroupProps> = ({
  isDisabled = false
}) => {
  const { values } = useFormikContext<EmploymentDetailsForm>();
  const isAdjustedDateOfHireAfterJobEnd = values?.adjustedHireDate?.isAfter(
    values.jobEndDate
  );
  const isAdjustedDateOfHireAfterToday = values?.adjustedHireDate?.isAfter(
    moment()
  );
  const isWarningVisible =
    isAdjustedDateOfHireAfterJobEnd || isAdjustedDateOfHireAfterToday;
  const themedStyles = useThemedStyles();

  return (
    <>
      <FormGroup
        data-test-el="adjusted-date-of-hire-form-group"
        className={styles.datepickerGroup}
        labelClassName={themedStyles.label}
        isGroupInvalid={isWarningVisible}
        noMargin={isWarningVisible}
        label={
          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ADJUSTED_DATE_OF_HIRE" />
        }
        labelTooltipContent={
          <ConfigurableText id="EDIT_EMPLOYMENT_DETAILS.ADJUSTED_HIRE_DATE.POPOVER" />
        }
        isRequired={true}
      >
        <Field
          name="adjustedHireDate"
          data-test-el="adjusted-hire-date-picker"
          component={DatePickerInput}
          isDisabled={isDisabled}
        />
      </FormGroup>
      {isAdjustedDateOfHireAfterToday && !isAdjustedDateOfHireAfterJobEnd && (
        <div className={themedStyles.warningMessage}>
          <ConfigurableText id="ADJUSTED_DATE_OF_HIRE_AFTER_TODAY_WARNING" />
        </div>
      )}
      {isAdjustedDateOfHireAfterJobEnd && (
        <div className={themedStyles.warningMessage}>
          <ConfigurableText id="JOB_END_DATE_AFTER_ADJUSTED_DATE_WARNING" />
        </div>
      )}
    </>
  );
};
