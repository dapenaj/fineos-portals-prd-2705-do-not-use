/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  ConfigurableText,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage,
  RichFormattedMessage,
  useAsync,
  usePermissions,
  ManageOccupationPermissions,
  SuccessModal,
  Card
} from 'fineos-common';
import { CustomerOccupation } from 'fineos-js-api-client';

import {
  ViewAllWeekPattern,
  isRotatingWorkPatternType,
  isFixedWorkPatternType,
  WorkPatternBasis,
  ModalFormViewMode,
  ALL_WEEK
} from '../../../../../shared';
import { EmploymentDataCard } from '../employment-data-card/employment-data-card.component';

import { UnsetWorkPattern } from './unset-work-pattern.component';
import { RotatingWorkPattern } from './rotating-work-pattern.component';
import { WorkPatternModal } from './work-pattern-modal.component';
import { fetchWorkPattern } from './work-pattern.api';
import { useWorkPatternInitialValues } from './work-pattern.form';
import styles from './work-pattern.module.scss';

type WorkPatternProps = {
  customerId: string;
  occupation: CustomerOccupation | null;
};

const useThemedStyles = createThemedStyles(theme => ({
  text: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  employeeWorkPattern: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  }
}));

export const WorkPattern: React.FC<WorkPatternProps> = ({
  customerId,
  occupation
}) => {
  const [mode, setMode] = useState(ModalFormViewMode.FORM);
  const hasEditRegularWeeklyWorkPatternPermission = usePermissions(
    ManageOccupationPermissions.EDIT_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
  );
  const themedStyles = useThemedStyles();

  const isWeeklyBasedPattern =
    occupation?.workPatternBasis === WorkPatternBasis.WEEK_BASED;
  const isVariableWorkPattern =
    occupation?.workPatternBasis === WorkPatternBasis.OTHER;
  const isNotProvidedWorkPattern =
    occupation?.workPatternBasis.toLocaleUpperCase() ===
    WorkPatternBasis.UNKNOWN;

  const {
    state: { value: workPattern, isPending: isWorkPatternPending }
  } = useAsync(fetchWorkPattern, [customerId, occupation?.id!], {
    shouldExecute: () => isWeeklyBasedPattern
  });

  const { fixedPattern, rotatingPattern } = useWorkPatternInitialValues(
    workPattern,
    isWeeklyBasedPattern
  );
  const SuccessModalComponent = (
    <SuccessModal
      title={<FormattedMessage id="WORK_PATTERN.SUCCESS_MODAL_HEADER" />}
      ariaLabelId="WORK_PATTERN.SUCCESS_MODAL_HEADER"
      onCancel={() => setMode(ModalFormViewMode.FORM)}
    >
      <ConfigurableText id="WORK_PATTERN.SUBMIT_SUCCESS_MESSAGE" role="alert" />
    </SuccessModal>
  );

  return (
    <EmploymentDataCard
      title={<FormattedMessage id="WORK_PATTERN.HEADER" />}
      emptyContent={
        isNotProvidedWorkPattern && (
          <>
            {ModalFormViewMode.UPLOAD_SUCCESS === mode && SuccessModalComponent}

            <UnsetWorkPattern onSetMode={setMode} customerId={customerId} />
          </>
        )
      }
      isLoading={isWorkPatternPending}
    >
      {!isNotProvidedWorkPattern && (
        <>
          {ModalFormViewMode.UPLOAD_SUCCESS === mode && SuccessModalComponent}
          {isVariableWorkPattern && (
            <>
              <ConfigurableText
                className={themedStyles.text}
                id="WORK_PATTERN.VARIABLE_WORK_PATTERN"
              />
            </>
          )}

          {isWeeklyBasedPattern && (
            <>
              {isFixedWorkPatternType(workPattern?.workPatternType) && (
                <>
                  <RichFormattedMessage
                    as="div"
                    className={themedStyles.employeeWorkPattern}
                    id="WORK_PATTERN.EMPLOYEE_WORK_PATTERN_FIXED"
                  />
                  <Card className={styles.card}>
                    <ViewAllWeekPattern
                      days={ALL_WEEK}
                      weekBasedDayPatternNullable={fixedPattern.workWeek}
                    />
                  </Card>
                </>
              )}

              {isRotatingWorkPatternType(workPattern?.workPatternType) && (
                <>
                  <RichFormattedMessage
                    as="div"
                    className={themedStyles.employeeWorkPattern}
                    id="WORK_PATTERN.EMPLOYEE_WORK_PATTERN_ROTATING"
                  />

                  <RotatingWorkPattern
                    weekBasedDayPatternNullable={rotatingPattern}
                    workPatternType={workPattern!.workPatternType?.name}
                  />
                </>
              )}

              {hasEditRegularWeeklyWorkPatternPermission && (
                <WorkPatternModal onSetMode={setMode} customerId={customerId} />
              )}
            </>
          )}
        </>
      )}
    </EmploymentDataCard>
  );
};
