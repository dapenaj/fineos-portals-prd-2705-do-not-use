/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { isEmpty as _isEmpty } from 'lodash';
import {
  CustomerOccupation,
  GroupClientCustomerService,
  CustomerContractualEarnings,
  ConcurrencyUpdateError,
  CustomerAbsenceEmployment
} from 'fineos-js-api-client';
import { ApiError, PartialApiCompleteError } from 'fineos-common';

export type UpdateEmploymentDetails = {
  occupation?: CustomerOccupation;
  earnings?: CustomerContractualEarnings;
  absence?: CustomerAbsenceEmployment;
};

export type UpdateEmploymentDetailsRequests = {
  [P in keyof UpdateEmploymentDetails]: Promise<
    NonNullable<UpdateEmploymentDetails[P]>
  >;
};

type UpdateEmploymentDetailsRequestsRecords = NonNullable<
  {
    [K in keyof UpdateEmploymentDetails]: [
      K,
      NonNullable<UpdateEmploymentDetails[K]>
    ];
  }[keyof UpdateEmploymentDetails]
>[];

const service = GroupClientCustomerService.getInstance();

export const editEmploymentDetails = (
  customerId: string,
  customerOccupation: CustomerOccupation
) => service.editCustomerOccupation(customerId, customerOccupation);

export const addContractualEarnings = (
  customerId: string,
  customerOccupationId: string,
  contractualEarning: CustomerContractualEarnings
) =>
  service.addCustomerContractualEarnings({
    customerId,
    customerOccupationId,
    contractualEarning
  });

export const editContractualEarnings = (
  employeeId: string,
  customerOccupationId: string,
  contractualEarning: CustomerContractualEarnings
) =>
  service.editCustomerContractualEarnings(
    employeeId,
    customerOccupationId,
    contractualEarning
  );

export const addAbsenceEmployment = (
  customerId: string,
  customerOccupationId: string,
  absenceEmployment: CustomerAbsenceEmployment
) =>
  service.addCustomerAbsenceEmployment(
    customerId,
    customerOccupationId,
    absenceEmployment
  );

export const editAbsenceEmployment = (
  customerId: string,
  customerOccupationId: string,
  absenceEmployment: CustomerAbsenceEmployment
) =>
  service.editCustomerAbsenceEmployment(
    customerId,
    customerOccupationId,
    absenceEmployment
  );

export const updateEmploymentDetails = async (
  requests: UpdateEmploymentDetailsRequests
): Promise<UpdateEmploymentDetails> => {
  const requestList = await Promise.allSettled(
    Object.entries(requests).map(([name, promise]) =>
      (promise as Promise<unknown>)
        .then(
          value => [name, value] as UpdateEmploymentDetailsRequestsRecords[0]
        )
        .catch(error => Promise.reject([name, error]))
    )
  );

  const updateEmploymentDetailsResponse: UpdateEmploymentDetails = {};
  const updateEmploymentDetailsConcurrency: UpdateEmploymentDetails = {};
  const updateEmploymentDetailsErrors: {
    [P in keyof UpdateEmploymentDetails]: ApiError;
  } = {};
  for (const settledResult of requestList) {
    if (settledResult.status !== 'fulfilled') {
      const [name, error]: [
        keyof UpdateEmploymentDetails,
        ApiError | ConcurrencyUpdateError
      ] = settledResult.reason;
      if (error instanceof ConcurrencyUpdateError) {
        updateEmploymentDetailsConcurrency[name] = _isEmpty(
          error.remoteData?.elements
        )
          ? error.remoteData
          : error.remoteData?.elements[0];
      } else {
        updateEmploymentDetailsErrors[name] = error;
      }
    } else {
      const [name, response] = settledResult.value;
      updateEmploymentDetailsResponse[name] = response as any;
    }
  }

  if (!_isEmpty(updateEmploymentDetailsErrors)) {
    const firstError = Object.values(updateEmploymentDetailsErrors)[0]!;
    if (_isEmpty(updateEmploymentDetailsResponse)) {
      throw firstError;
    } else {
      throw new PartialApiCompleteError(firstError, {
        ...updateEmploymentDetailsResponse,
        ...updateEmploymentDetailsConcurrency
      });
    }
  }
  if (!_isEmpty(updateEmploymentDetailsConcurrency)) {
    throw new ConcurrencyUpdateError({
      ...updateEmploymentDetailsResponse,
      ...updateEmploymentDetailsConcurrency
    });
  }

  return updateEmploymentDetailsResponse;
};
