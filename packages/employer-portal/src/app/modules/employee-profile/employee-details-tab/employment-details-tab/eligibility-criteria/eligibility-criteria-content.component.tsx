/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import classNames from 'classnames';
import {
  createThemedStyles,
  FallbackValue,
  FormattedMessage,
  ManageOccupationPermissions,
  RichFormattedMessage,
  textStyleToCss,
  usePermissions
} from 'fineos-common';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation
} from 'fineos-js-api-client';

import { EnumDomain, EnumValue } from '../../../../../shared/enum-domain';
import { EmploymentCardActions } from '../common';
import { isDomainNameUnknown } from '../../../../../shared';

import { EditEligibilityCriteria } from './edit-eligibility-criteria/edit-eligibility-criteria.component';
import { InfoWithIcon } from './info-with-icon.component';
import styles from './eligilibity-criteria.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  sectionTitle: {
    ...textStyleToCss(theme.typography.baseTextSemiBold),
    color: theme.colorSchema.neutral8
  }
}));

type EligibilityCriteriaContentProps = {
  employeeId: string;
  occupation: (CustomerOccupation & CustomerAbsenceEmployment) | null;
  absenceEmployment: CustomerAbsenceEmployment;
};

const DEFAULT_OCCUPATION = {
  hoursWorkedPerYear: null,
  withinFMLACriteria: false,
  workingAtHome: false,
  keyEmployee: false,
  employmentWorkState: null,
  cbaValue: '',
  occupationQualifiers: [] as CustomerAbsenceEmployment['occupationQualifiers']
};

export const EligibilityCriteriaContent: React.FC<EligibilityCriteriaContentProps> = ({
  employeeId,
  occupation,
  absenceEmployment
}) => {
  const hasEditPermission = usePermissions(
    ManageOccupationPermissions.VIEW_EDIT_EMPLOYMENT_DETAILS
  );

  const {
    hoursWorkedPerYear,
    withinFMLACriteria,
    workingAtHome,
    keyEmployee,
    employmentWorkState,
    cbaValue,
    occupationQualifiers
  } = occupation || DEFAULT_OCCUPATION;

  const mappedQualifiers =
    occupationQualifiers?.length &&
    occupationQualifiers
      .map(qualifier => qualifier.qualifierDescription)
      .join(', ');

  const themedStyles = useThemedStyles();
  return (
    <>
      <FormattedMessage
        id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.YOUR_EMPLOYEE"
        className={classNames(themedStyles.sectionTitle, styles.sectionTitle)}
      />
      <div className={styles.booleanDataSection}>
        <InfoWithIcon isConfirmed={withinFMLACriteria}>
          <FormattedMessage
            as={withinFMLACriteria ? 'strong' : 'span'}
            id={
              withinFMLACriteria
                ? 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_WITH_FMLA_TRUE'
                : 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_WITH_FMLA_FALSE'
            }
          />
        </InfoWithIcon>
        <InfoWithIcon isConfirmed={workingAtHome}>
          <FormattedMessage
            as={workingAtHome ? 'strong' : 'span'}
            id={
              workingAtHome
                ? 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_AT_HOME_TRUE'
                : 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_AT_HOME_FALSE'
            }
          />
        </InfoWithIcon>
        <InfoWithIcon isConfirmed={keyEmployee}>
          <FormattedMessage
            as={keyEmployee ? 'strong' : 'span'}
            id={
              keyEmployee
                ? 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.IS_KEY_EMPLOYEE_TRUE'
                : 'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.IS_KEY_EMPLOYEE_FALSE'
            }
          />
        </InfoWithIcon>
      </div>
      {typeof hoursWorkedPerYear === 'number' && (
        <RichFormattedMessage
          className={styles.informationLine}
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.HOURS_PER_YEAR"
          values={{
            hoursPerYear: hoursWorkedPerYear
          }}
        />
      )}
      {employmentWorkState && (
        <RichFormattedMessage
          className={styles.informationLine}
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORK_STATE"
          values={{
            workState: (
              <FallbackValue
                value={
                  !isDomainNameUnknown(employmentWorkState) && (
                    <EnumValue
                      domainId={EnumDomain.US_STATES}
                      enumValue={employmentWorkState}
                    />
                  )
                }
              />
            )
          }}
        />
      )}
      {cbaValue && (
        <RichFormattedMessage
          className={styles.informationLine}
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.CBA_NUMBER"
          values={{
            CBANumber: cbaValue
          }}
        />
      )}
      {!_isEmpty(occupationQualifiers) && (
        <RichFormattedMessage
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.OCCUPATION_QUALIFIERS"
          values={{
            occupationQualifiers: mappedQualifiers
          }}
        />
      )}

      <EmploymentCardActions>
        {hasEditPermission && (
          <EditEligibilityCriteria
            titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_EDIT"
            buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EDIT_BUTTON"
            buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EDIT_BUTTON_ARIA_LABEL"
            employeeId={employeeId}
          />
        )}
      </EmploymentCardActions>
    </>
  );
};
