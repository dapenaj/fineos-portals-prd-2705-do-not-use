/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FormattedMessage,
  FormattedDate,
  RichFormattedMessage,
  createThemedStyles,
  FallbackValue,
  usePermissions,
  ViewOccupationPermissions
} from 'fineos-common';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation,
  CustomerContractualEarnings
} from 'fineos-js-api-client';

import {
  getEarningsFrequency,
  isDomainNameUnknown,
  unscaledAmount,
  Currency
} from '../../../../../shared';
import { EmploymentCardActions } from '../common';

import { EditEmploymentDetails } from './edit-employment-details';
import styles from './current-occupation.module.scss';

type CurrentOccupationProps = {
  employeeId: string;
  occupation: CustomerOccupation | null;
  absenceEmployment: CustomerAbsenceEmployment | null;
  contractualEarnings: CustomerContractualEarnings | null;
};

const useThemedStyles = createThemedStyles(theme => ({
  highlight: {
    color: theme.colorSchema.primaryColor
  }
}));

export const CurrentOccupationContent: React.FC<CurrentOccupationProps> = ({
  employeeId,
  occupation,
  absenceEmployment,
  contractualEarnings
}) => {
  const hasViewAbsenceEmploymentPermission = usePermissions(
    ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT
  );

  const transformEnumDomainNameToTranslationKey = (name: string) =>
    name.replace(' ', '_').toUpperCase();

  const shouldShowAdjustedDateOfHire =
    hasViewAbsenceEmploymentPermission &&
    absenceEmployment?.adjustedHireDate &&
    absenceEmployment?.adjustedHireDate.isValid();

  const themedStyles = useThemedStyles();
  return (
    <>
      {occupation?.jobTitle &&
        occupation?.jobStartDate &&
        occupation.jobStartDate.isValid() && (
          <div className={styles.informationLine}>
            {occupation?.jobEndDate && occupation.jobEndDate.isValid() ? (
              <RichFormattedMessage
                id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.JOB_TITLE_WITH_START_DATE_AND_END_DATE"
                values={{
                  jobTitle: occupation?.jobTitle,
                  jobStartDate: (
                    <FormattedDate date={occupation.jobStartDate} />
                  ),
                  jobEndDate: <FormattedDate date={occupation.jobEndDate} />
                }}
              />
            ) : (
              <RichFormattedMessage
                id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.JOB_TITLE_AND_START_DATE"
                values={{
                  jobTitle: occupation?.jobTitle,
                  jobStartDate: <FormattedDate date={occupation.jobStartDate} />
                }}
              />
            )}
          </div>
        )}
      {shouldShowAdjustedDateOfHire && (
        <RichFormattedMessage
          className={styles.informationLine}
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADJUSTED_DATE_OF_HIRE"
          values={{
            adjustedHireDate: (
              <FormattedDate date={absenceEmployment!.adjustedHireDate} />
            )
          }}
        />
      )}
      {hasViewAbsenceEmploymentPermission && absenceEmployment?.employmentType && (
        <RichFormattedMessage
          className={styles.informationLine}
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPLOYMENT_TYPE"
          values={{
            employmentType: (
              <FallbackValue
                value={
                  !isDomainNameUnknown(absenceEmployment.employmentType) && (
                    <FormattedMessage
                      id={`PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPLOYMENT_TYPE_NAME.${transformEnumDomainNameToTranslationKey(
                        absenceEmployment.employmentType.name
                      )}`}
                    />
                  )
                }
              />
            )
          }}
        />
      )}
      <RichFormattedMessage
        className={styles.informationLine}
        id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.WORK_BASIS"
        values={{
          workBasis: (
            <FallbackValue
              value={
                !isDomainNameUnknown(occupation?.employmentCat) && (
                  <FormattedMessage
                    id={`ENUM_DOMAINS.OCCUPATION_CATEGORY.${transformEnumDomainNameToTranslationKey(
                      occupation!.employmentCat!.name
                    )}`}
                  />
                )
              }
            />
          )
        }}
      />
      {occupation?.hrsWorkedPerWeek && (
        <RichFormattedMessage
          className={styles.informationLine}
          id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.HOURS_PER_WEEK"
          values={{
            hoursPerWeek: occupation.hrsWorkedPerWeek
          }}
        />
      )}
      {!!contractualEarnings?.amount?.amountMinorUnits &&
        contractualEarnings?.frequency?.name && (
          <div className={styles.informationLine}>
            <RichFormattedMessage
              id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.CONTRACTUAL_EARNINGS_AMOUNT"
              values={{
                amount: (
                  <Currency
                    amount={unscaledAmount(
                      contractualEarnings.amount.amountMinorUnits,
                      contractualEarnings.amount.scale || 2
                    )}
                  />
                )
              }}
            />{' '}
            <strong>
              <FormattedMessage
                className={themedStyles.highlight}
                id={`PROFILE.EMPLOYMENT_DETAILS_TAB.EARNINGS_FREQUENCIES.${getEarningsFrequency(
                  contractualEarnings.frequency.name
                )}`}
              />
            </strong>
          </div>
        )}
      <EmploymentCardActions>
        <EditEmploymentDetails
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.OCCUPATION_TITLE_EDIT"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON_ARIA_LABEL"
          employeeId={employeeId}
        />
      </EmploymentCardActions>
    </>
  );
};
