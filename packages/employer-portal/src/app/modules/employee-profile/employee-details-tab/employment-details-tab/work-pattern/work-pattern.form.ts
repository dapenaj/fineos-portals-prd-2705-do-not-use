/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useMemo, useContext } from 'react';
import {
  WeekBasedWorkPattern,
  WeekBasedDayPattern,
  BaseDomain
} from 'fineos-js-api-client';
import * as Yup from 'yup';

import { Config } from '../../../../../../app/config';
import {
  isRotatingWorkPatternType,
  isFixedWorkPatternType,
  DEFAULT_FIXED_WORK_WEEK,
  EMPTY_WORK_WEEK,
  getValueForWeek,
  ALL_WEEK,
  WeekBasedDayPatternNullable,
  WEEKEND_DAYS,
  WorkPatternValue,
  RotatingPatternType,
  fixedPatternSchema,
  rotatingPatternSchema,
  variablePatternSchema,
  VariablePatternFormValues,
  EnumDomain,
  useEnumDomain,
  WORK_DAYS
} from '../../../../../shared';

const hasWeekend = (workPatternDays: WeekBasedDayPattern[]) =>
  workPatternDays &&
  workPatternDays.some(
    workPatternDay =>
      workPatternDay.dayOfWeek &&
      WEEKEND_DAYS.includes(
        workPatternDay.dayOfWeek.name.toLocaleUpperCase()
      ) &&
      (workPatternDay.hours !== null || workPatternDay.minutes !== null) &&
      (workPatternDay.hours !== 0 || workPatternDay.minutes !== 0)
  );

const getValueForAllWeek = (
  workPatternDays: WeekBasedDayPattern[],
  weekNumber: number,
  weekDays: BaseDomain[]
): WeekBasedDayPatternNullable[] =>
  ALL_WEEK.map(
    day =>
      getValueForWeek(
        day,
        workPatternDays,
        weekNumber,
        weekDays
      ) as WeekBasedDayPatternNullable
  );

export const buildWorkPattern = (
  workPattern: WeekBasedWorkPattern | null,
  isWeeklyBasedPattern: boolean,
  weekDays: BaseDomain[],
  workPatternTypes: BaseDomain[],
  dayLength: {
    hours: number;
    minutes: number;
  }
) => {
  const fixedPattern = {
    isNonStandardWeek: false,
    includeWeekend: false,
    workWeek: DEFAULT_FIXED_WORK_WEEK(weekDays, dayLength)
  };
  const rotatingPattern = {
    workPatternWeek1: [...EMPTY_WORK_WEEK(1, weekDays)],
    workPatternWeek2: [...EMPTY_WORK_WEEK(2, weekDays)],
    workPatternWeek3: [...EMPTY_WORK_WEEK(3, weekDays)],
    workPatternWeek4: [...EMPTY_WORK_WEEK(4, weekDays)]
  } as RotatingPatternType;
  const variablePattern: VariablePatternFormValues = {
    dayLength: { hours: null, minutes: null }
  } as VariablePatternFormValues;

  if (workPattern && isWeeklyBasedPattern) {
    if (isFixedWorkPatternType(workPattern.workPatternType)) {
      const hasWeekendIncluded = hasWeekend(workPattern.workPatternDays);
      fixedPattern.isNonStandardWeek =
        hasWeekendIncluded ||
        !workPattern.workPatternDays.every(day => {
          if (WORK_DAYS.includes(day.dayOfWeek.name.toLocaleUpperCase())) {
            return (
              day.hours === dayLength.hours && day.minutes === dayLength.minutes
            );
          }
          return true;
        });
      fixedPattern.includeWeekend = hasWeekendIncluded;
      fixedPattern.workWeek = getValueForAllWeek(
        [...workPattern.workPatternDays],
        1,
        weekDays
      );
    }

    if (isRotatingWorkPatternType(workPattern.workPatternType)) {
      rotatingPattern.workPatternWeek1 = getValueForAllWeek(
        [...workPattern.workPatternDays],
        1,
        weekDays
      );
      rotatingPattern.workPatternWeek2 = getValueForAllWeek(
        [...workPattern.workPatternDays],
        2,
        weekDays
      );
      rotatingPattern.workPatternWeek3 = getValueForAllWeek(
        [...workPattern.workPatternDays],
        3,
        weekDays
      );

      rotatingPattern.workPatternWeek4 = getValueForAllWeek(
        [...workPattern.workPatternDays],
        4,
        weekDays
      );
    }
  }

  const workPatternType =
    workPattern && isWeeklyBasedPattern
      ? {
          domainId: workPattern?.workPatternType.domainId,
          fullId: workPattern?.workPatternType.fullId,
          name: workPattern?.workPatternType.name,
          domainName: workPattern?.workPatternType.domainName
        }
      : workPatternTypes.find(isFixedWorkPatternType);

  return {
    workPatternType,
    fixedPattern,
    rotatingPattern,
    variablePattern,
    workWeekStarts: workPattern?.workWeekStarts,
    patternStartDate: workPattern?.patternStartDate,
    patternStatus: workPattern?.patternStatus
  } as WorkPatternValue;
};

export const useWorkPatternInitialValues = (
  workPattern: WeekBasedWorkPattern | null,
  isWeeklyBasedPattern: boolean
) => {
  const { domains: weekDays } = useEnumDomain(EnumDomain.WEEK_DAYS, {
    shouldSkipUnknownFiltering: true
  });
  const { domains: workPatternTypes } = useEnumDomain(
    EnumDomain.WORK_PATTERN_TYPE,
    {
      shouldSkipUnknownFiltering: true
    }
  );
  const config = useContext(Config);
  const dayLength = config.configurableWorkPattern.FIXED_DAY_LENGTH;
  return useMemo(() => {
    return {
      ...buildWorkPattern(
        workPattern,
        isWeeklyBasedPattern,
        weekDays,
        workPatternTypes,
        dayLength
      )
    };
  }, [
    workPattern,
    weekDays,
    workPatternTypes,
    isWeeklyBasedPattern,
    dayLength
  ]);
};

export const workPatternFormValidation = Yup.object({
  fixedPattern: fixedPatternSchema,
  variablePattern: variablePatternSchema,
  rotatingPattern: rotatingPatternSchema
});
