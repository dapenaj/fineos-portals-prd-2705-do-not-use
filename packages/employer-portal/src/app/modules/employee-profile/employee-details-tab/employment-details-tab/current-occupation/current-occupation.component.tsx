/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FormattedMessage,
  useAsync,
  usePermissions,
  ViewOccupationPermissions,
  ManageOccupationPermissions,
  anyFrom
} from 'fineos-common';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation
} from 'fineos-js-api-client';

import {
  fetchContractualEarnings,
  isDomainNameUnknown
} from '../../../../../shared';
import { EmploymentDataCard } from '../employment-data-card/employment-data-card.component';

import { EditEmploymentDetails } from './edit-employment-details';
import { CurrentOccupationContent } from './current-occupation-content.component';

type CurrentOccupationProps = {
  employeeId: string;
  occupation: CustomerOccupation | null;
  isOccupationPending: boolean;
  absenceEmployment: CustomerAbsenceEmployment | null;
  isAbsenceEmploymentPending: boolean;
};

export const CurrentOccupation: React.FC<CurrentOccupationProps> = ({
  employeeId,
  occupation,
  isOccupationPending,
  absenceEmployment,
  isAbsenceEmploymentPending
}) => {
  const hasViewContractualEarningsPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
  );

  const hasAddEmploymentDetailsPermission = usePermissions(
    anyFrom(
      ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT,
      ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
    )
  );

  const {
    state: {
      value: contractualEarnings,
      isPending: isContractualEarningsPending
    }
  } = useAsync(fetchContractualEarnings, [employeeId, occupation?.id || ''], {
    shouldExecute: () =>
      !!occupation?.id && hasViewContractualEarningsPermission
  });

  const displayConditions = [
    occupation?.jobTitle &&
      occupation.jobStartDate &&
      occupation.jobStartDate.isValid(),
    occupation?.hrsWorkedPerWeek,
    absenceEmployment?.adjustedHireDate &&
      absenceEmployment.adjustedHireDate.isValid(),
    !isDomainNameUnknown(absenceEmployment?.employmentType),
    contractualEarnings?.amount?.amountMinorUnits &&
      contractualEarnings?.frequency?.name,
    !isDomainNameUnknown(occupation?.employmentCat)
  ];
  const hasAnyData = displayConditions.some(Boolean);
  return (
    <>
      <EmploymentDataCard
        title={
          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.TITLE" />
        }
        emptyContent={
          <>
            <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPTY_STATE" />
            {hasAddEmploymentDetailsPermission && (
              <EditEmploymentDetails
                titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.OCCUPATION_TITLE_ADD"
                buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADD_BUTTON"
                buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADD_BUTTON_ARIA_LABEL"
                employeeId={employeeId}
              />
            )}
          </>
        }
        isLoading={
          isOccupationPending ||
          isContractualEarningsPending ||
          isAbsenceEmploymentPending
        }
      >
        {hasAnyData && (
          <CurrentOccupationContent
            employeeId={employeeId}
            absenceEmployment={absenceEmployment}
            occupation={occupation}
            contractualEarnings={contractualEarnings}
          />
        )}
      </EmploymentDataCard>
    </>
  );
};
