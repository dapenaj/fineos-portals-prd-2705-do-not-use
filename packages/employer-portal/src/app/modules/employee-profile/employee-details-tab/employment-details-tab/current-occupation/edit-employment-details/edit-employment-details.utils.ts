/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  CustomerOccupation,
  CustomerContractualEarnings,
  BaseDomain,
  CustomerAbsenceEmployment
} from 'fineos-js-api-client';
import {
  cloneDeep as _cloneDeep,
  isEqual as _isEqual,
  isEmpty as _isEmpty
} from 'lodash';
import moment from 'moment';

import {
  isDomainNameUnknown,
  scaledAmount,
  unscaledAmount
} from '../../../../../../shared';

import { EmploymentDetailsForm } from './edit-employment-details-form.type';

const DEFAULT_SCALE = 2;
const DEFAULT_CURRENCY = '---';
const EARNING_TYPE_DOMAIN_NAME = 'Wages';

export const shouldUpdateContractualEarnings = (
  formData: EmploymentDetailsForm,
  contractualEarnings: CustomerContractualEarnings | null
) =>
  (contractualEarnings === null && formData.earnings.amount) ||
  (contractualEarnings !== null &&
    (!_isEqual(
      formData.earnings.amount,
      unscaledAmount(contractualEarnings.amount?.amountMinorUnits, 2)
    ) ||
      !_isEqual(
        formData.earnings.frequency?.fullId,
        contractualEarnings.frequency?.fullId
      )));

export const shouldUpdateAbsenceData = (
  formData: EmploymentDetailsForm,
  absenceEmployment: CustomerAbsenceEmployment | null
) =>
  (!formData.isAdjustedDateOfHireVisible && formData.adjustedHireDate) ||
  !_isEqual(
    absenceEmployment?.adjustedHireDate || null,
    formData?.adjustedHireDate || null
  ) ||
  !_isEqual(absenceEmployment?.employmentType, formData?.employmentType);

export const parsedOccupation = (
  editedOccupation: EmploymentDetailsForm,
  fetchedOccupation: CustomerOccupation
) => {
  const {
    jobTitle,
    jobStartDate,
    jobEndDate,
    hrsWorkedPerWeek,
    occupationCategory
  } = editedOccupation;
  const occupationClone = _cloneDeep(fetchedOccupation);
  occupationClone.jobTitle = jobTitle;
  occupationClone.jobStartDate = jobStartDate;
  occupationClone.hrsWorkedPerWeek = hrsWorkedPerWeek?.toString();
  if (occupationCategory) {
    occupationClone.employmentCat = occupationCategory;
  }
  occupationClone.jobEndDate = jobEndDate;

  return occupationClone;
};

export const parsedContractualEarnings = (
  editedEarnings: EmploymentDetailsForm,
  fetchedEarnings: CustomerContractualEarnings | null,
  earningTypeDomains: BaseDomain[],
  decimalSeparator: string
) => {
  const { earnings } = editedEarnings;
  if (fetchedEarnings) {
    return {
      ...fetchedEarnings,
      amount: {
        ...fetchedEarnings.amount,
        amountMinorUnits: !!earnings.amount
          ? scaledAmount(
              earnings.amount,
              fetchedEarnings.amount.scale,
              decimalSeparator
            )
          : null
      },
      frequency: { ...earnings.frequency }
    };
  }

  return {
    amount: {
      amountMinorUnits: !!earnings.amount
        ? scaledAmount(earnings.amount!, DEFAULT_SCALE, decimalSeparator)
        : null,
      scale: DEFAULT_SCALE,
      currency: DEFAULT_CURRENCY
    },
    frequency: { ...earnings.frequency },
    earningsType: earningTypeDomains.find(
      domain => domain.name === EARNING_TYPE_DOMAIN_NAME
    ),
    effectiveDate: moment()
  };
};

export const parsedAbsenceEmployment = (
  formData: EmploymentDetailsForm,
  absenceEmployment: CustomerAbsenceEmployment | null
) => {
  const parsedData = new CustomerAbsenceEmployment();

  Object.assign(parsedData, {
    adjustedHireDate: formData.isAdjustedDateOfHireVisible
      ? formData.adjustedHireDate
      : null,
    cbaValue: absenceEmployment?.cbaValue,
    employmentClassification: absenceEmployment?.employmentClassification,
    employmentType: formData.employmentType,
    employmentWorkState: absenceEmployment?.employmentWorkState,
    hoursWorkedPerYear: absenceEmployment?.hoursWorkedPerYear,
    keyEmployee: absenceEmployment?.keyEmployee,
    withinFMLACriteria: absenceEmployment?.withinFMLACriteria,
    workingAtHome: absenceEmployment?.workingAtHome
  });

  if (absenceEmployment?.id) {
    Object.assign(parsedData, {
      id: absenceEmployment.id
    });
  }
  return parsedData;
};

export const isNewContractualEarnings = (
  contractualEarnings: CustomerContractualEarnings | null
) =>
  contractualEarnings === null ||
  (!_isEmpty(contractualEarnings.amount?.amountMinorUnits) &&
    !isDomainNameUnknown(contractualEarnings.frequency));

export const isNewAbsenceEmployment = (
  absenceEmployment: CustomerAbsenceEmployment | null
) =>
  !absenceEmployment?.adjustedHireDate?.isValid() &&
  isDomainNameUnknown(absenceEmployment?.employmentType);
