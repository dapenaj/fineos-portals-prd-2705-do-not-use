/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  AsteriskInfo,
  PropertyItem,
  FormattedDate,
  Button,
  ButtonType,
  Modal,
  ManageCustomerDataPermissions,
  usePermissions,
  FormattedMessage
} from 'fineos-common';
import { Row, Col } from 'antd';
import { GroupClientCustomerInfo } from 'fineos-js-api-client';

import { Address } from '../../../../../shared';
import employeeDetailsTabStyles from '../../employee-details-tab.module.scss';

import styles from './personal-details.module.scss';
import { EditPersonalDetailsForm } from './edit-personal-details-form.component';

type PersonalDetailsProps = {
  customer: GroupClientCustomerInfo;
  onCustomerRefresh: (data: GroupClientCustomerInfo) => void;
};

export const PersonalDetails: React.FC<PersonalDetailsProps> = ({
  customer,
  onCustomerRefresh
}) => {
  const hasPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION
  );

  const [isFormVisible, toggleForm] = useState(false);
  const onFormClose = () => {
    toggleForm(false);
  };

  return (
    <>
      <Row className={employeeDetailsTabStyles.row}>
        <Col xs={24}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.FULL_NAME" />
            }
          >
            {customer.firstName} {customer.lastName}
          </PropertyItem>
        </Col>
      </Row>

      <Row className={employeeDetailsTabStyles.row}>
        <Col xs={24}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.DATE_OF_BIRTH" />
            }
          >
            <FormattedDate date={customer.dateOfBirth} />
          </PropertyItem>
        </Col>
      </Row>

      <Row>
        <Col xs={24}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.ADDRESS" />
            }
          >
            {!!customer.address && <Address address={customer.address} />}
          </PropertyItem>
        </Col>
      </Row>

      {hasPermission && (
        <Row className={employeeDetailsTabStyles.row}>
          <Col xs={24}>
            <div className={styles.link}>
              <Button
                data-test-el="edit-personal-details-button"
                buttonType={ButtonType.LINK}
                onClick={() => toggleForm(!isFormVisible)}
              >
                <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS" />
              </Button>
            </div>
          </Col>
        </Row>
      )}

      <Modal
        ariaLabelId="PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS"
        header={
          <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS" />
        }
        visible={isFormVisible}
        onCancel={() => toggleForm(false)}
        data-test-el="edit-personal-details-modal"
      >
        <AsteriskInfo />
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={onFormClose}
          onCustomerRefresh={onCustomerRefresh}
          onEditFinish={changedDetails => {
            onCustomerRefresh(changedDetails!);
            onFormClose();
          }}
        />
      </Modal>
    </>
  );
};
