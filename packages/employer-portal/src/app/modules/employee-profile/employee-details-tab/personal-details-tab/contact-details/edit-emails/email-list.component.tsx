/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useLayoutEffect, useState } from 'react';
import {
  TextInput,
  Button,
  ButtonType,
  DeleteButton,
  FormattedMessage,
  usePermissions,
  ManageCustomerDataPermissions,
  FormGroup
} from 'fineos-common';
import { Col } from 'antd';
import { Field, FieldArray } from 'formik';
import { GroupClientEmailAddress } from 'fineos-js-api-client';
import classNames from 'classnames';

import { DEFAULT_EMAIL } from './edit-emails.constant';
import styles from './edit-emails.module.scss';

type EmailListProps = {
  values: { emails: GroupClientEmailAddress[] };
  isValid: boolean;
  removedEmailData: GroupClientEmailAddress[];
  onEmailDataRemove: (
    groupClientEmailAddress: GroupClientEmailAddress[]
  ) => void;
};

export const EmailList: React.FC<EmailListProps> = ({
  values,
  isValid,
  removedEmailData,
  onEmailDataRemove
}) => {
  const lastInputElRef = React.createRef() as React.RefObject<HTMLDivElement>;

  const hasAddEmailPermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS
  );
  const hasEditEmailPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS
  );
  const hasRemoveEmailPermission = usePermissions(
    ManageCustomerDataPermissions.REMOVE_CUSTOMER_EMAILS
  );

  const isEmailInputDisabled = (email: GroupClientEmailAddress) =>
    (!hasEditEmailPermission && !!email.id) ||
    (!hasAddEmailPermission && !email.id);

  const [shouldFocusOnLastInput, setShouldFocusOnLastInput] = useState(false);

  useLayoutEffect(() => {
    if (!isValid && shouldFocusOnLastInput) {
      const input = lastInputElRef?.current?.querySelector(
        'input[type="text"]'
      ) as HTMLDivElement;
      input?.focus();

      setShouldFocusOnLastInput(false);
    }
    // eslint-disable-next-line
  }, [isValid]);

  return (
    <FieldArray
      name="emails"
      render={arrayHelpers => (
        <>
          {values.emails.map(
            (email: GroupClientEmailAddress, index: number) => (
              <React.Fragment key={index}>
                <Col
                  ref={lastInputElRef}
                  xs={values.emails.length > 1 ? 22 : 24}
                  className={styles.email}
                >
                  <FormGroup
                    isRequired={index === 0}
                    noMargin={true}
                    className={classNames({
                      [styles.hiddenLabel]: index !== 0
                    })}
                    label={
                      <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.EMAILS" />
                    }
                  >
                    <Field
                      name={`emails[${index}].emailAddress`}
                      component={TextInput}
                      isDisabled={isEmailInputDisabled(email)}
                      aria-required={true}
                    />
                  </FormGroup>
                </Col>
                {hasRemoveEmailPermission && values.emails.length > 1 && (
                  <Col
                    xs={2}
                    className={classNames(
                      styles.email,
                      styles.emailDeleteButton,
                      { [styles.firstEmailDeleteButton]: index === 0 }
                    )}
                  >
                    <DeleteButton
                      iconLabel={
                        <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.DELETE_EMAIL" />
                      }
                      data-test-el="delete-email"
                      onClick={() => {
                        if (email.id) {
                          onEmailDataRemove([...removedEmailData, email]);
                        }
                        arrayHelpers.remove(index);
                      }}
                    />
                  </Col>
                )}
              </React.Fragment>
            )
          )}
          {hasAddEmailPermission && (
            <div className={styles.link}>
              <Button
                buttonType={ButtonType.LINK}
                onClick={() => {
                  arrayHelpers.push(DEFAULT_EMAIL);
                  setShouldFocusOnLastInput(true);
                }}
                disabled={!isValid}
              >
                <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_EMAIL" />
              </Button>
            </div>
          )}
        </>
      )}
    />
  );
};
