/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GroupClientEmailAddress } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { addCustomerEmail } from '../../../../../../shared';

import { EditEmailsFormType } from './edit-emails-form.type';
import { editCustomerEmail } from './';
import { removeCustomerEmail } from './edit-emails.api';

export const getEditEmailsData = (
  formData: EditEmailsFormType[],
  emails: GroupClientEmailAddress[]
) => {
  const editedEmails = formData.filter(
    (formEmail, index) =>
      formEmail.id &&
      formEmail.id === emails[index].id &&
      formEmail.emailAddress !== emails[index].emailAddress
  );
  return editedEmails as GroupClientEmailAddress[];
};

export const getAddedEmailsData = (
  formData: EditEmailsFormType[],
  emails: GroupClientEmailAddress[]
) =>
  formData.filter(
    formEmail =>
      (formEmail.id === '' && formData.indexOf(formEmail)) ||
      (!_isEmpty(formEmail) && _isEmpty(emails))
  );

export const applyEditEmailChange = async (
  customerId: string,
  editedEmails: GroupClientEmailAddress[]
): Promise<GroupClientEmailAddress[]> => {
  const successfullyEditedEmails: GroupClientEmailAddress[] = [];

  for (const editedEmail of editedEmails) {
    successfullyEditedEmails.push(
      await editCustomerEmail(
        customerId,
        editedEmail.id,
        editedEmail.emailAddress
      )
    );
  }

  return successfullyEditedEmails;
};

export const applyAddEmailChange = async (
  customerId: string,
  addedEmails: GroupClientEmailAddress[]
): Promise<GroupClientEmailAddress[]> => {
  const createdEmails: GroupClientEmailAddress[] = [];

  for (const addedEmail of addedEmails) {
    createdEmails.push(
      await addCustomerEmail(customerId, {
        emailAddress: addedEmail.emailAddress
      })
    );
  }

  return createdEmails;
};

export const applyRemoveEmailChange = async (
  customerId: string,
  removedEmails: GroupClientEmailAddress[]
) => {
  for (const removedEmail of removedEmails) {
    await removeCustomerEmail(customerId, removedEmail.id);
  }
};
