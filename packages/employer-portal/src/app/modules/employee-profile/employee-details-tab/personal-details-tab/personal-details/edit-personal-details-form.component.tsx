/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useRef } from 'react';
import {
  FormButton,
  ButtonType,
  FormGroup,
  TextInput,
  DatePickerInput,
  Form,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage,
  ModalFooter
} from 'fineos-common';
import { Row, Col } from 'antd';
import moment from 'moment';
import { GroupClientCustomerInfo } from 'fineos-js-api-client';
import { Field } from 'formik';
import classNames from 'classnames';
import { isEqual as _isEqual } from 'lodash';

import {
  AddressInput,
  FormikWithResponseHandling
} from '../../../../../shared';

import styles from './personal-details.module.scss';
import {
  buildEditPersonalDetailsInitialValues,
  buildEditPersonalDetailsValidation
} from './edit-personal-details.form';
import { PersonalDetailsForm } from './personal-details-form.type';
import { parsedPersonalDetailsFormData } from './edit-personal-details.utils';
import { editPersonalDetails } from './personal-details.api';

type EditPersonalDetailsFormProps = {
  customer: GroupClientCustomerInfo;
  onCustomerRefresh: (data: GroupClientCustomerInfo) => void;
  onFormClose: () => void;
  onEditFinish: (changedPersonalDetails?: GroupClientCustomerInfo) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    color: theme.colorSchema.neutral8
  },
  addressLabel: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const EditPersonalDetailsForm: React.FC<EditPersonalDetailsFormProps> = ({
  customer,
  onCustomerRefresh,
  onFormClose,
  onEditFinish
}) => {
  const shouldUpdateAfterConcurrency = useRef(false);
  const handleSubmit = async (data: PersonalDetailsForm) => {
    const parsedOccupationData = parsedPersonalDetailsFormData(data, customer);
    if (
      _isEqual(customer, parsedOccupationData) &&
      !shouldUpdateAfterConcurrency.current
    ) {
      onFormClose();
      return;
    }
    const personalDetailsResponse = await editPersonalDetails(
      parsedOccupationData
    );
    onEditFinish(personalDetailsResponse);
    shouldUpdateAfterConcurrency.current = false;
  };

  const themedStyles = useThemedStyles();

  return (
    <FormikWithResponseHandling
      initialValues={buildEditPersonalDetailsInitialValues(customer)}
      validationSchema={buildEditPersonalDetailsValidation}
      onSubmit={handleSubmit}
      dataParseFunction={data => {
        shouldUpdateAfterConcurrency.current = true;
        onCustomerRefresh(data);
        return buildEditPersonalDetailsInitialValues(data);
      }}
    >
      <Form
        className={styles.personalDetailsForm}
        data-test-el="edit-personal-details-form"
      >
        <Row justify="space-between">
          <Col xs={24} sm={11}>
            <FormGroup
              labelClassName={themedStyles.label}
              isRequired={true}
              label={<FormattedMessage id="COMMON.FIELDS.FIRST_NAME_LABEL" />}
              data-test-el="first-name-field"
            >
              <Field name="firstName" component={TextInput} />
            </FormGroup>
          </Col>

          <Col xs={24} sm={11}>
            <FormGroup
              labelClassName={themedStyles.label}
              isRequired={true}
              label={<FormattedMessage id="COMMON.FIELDS.LAST_NAME_LABEL" />}
              data-test-el="last-name-field"
            >
              <Field name="lastName" component={TextInput} />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs={24}>
            <FormGroup
              labelClassName={themedStyles.label}
              isRequired={true}
              label={
                <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.DATE_OF_BIRTH" />
              }
              data-test-el="date-of-birth-field"
            >
              <Field
                name="dateOfBirth"
                component={DatePickerInput}
                disabledDate={(date: moment.Moment) => date.isAfter(moment())}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col xs={24}>
            <div
              className={classNames(
                themedStyles.addressLabel,
                styles.addressLabel
              )}
            >
              <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.PRIMARY_ADDRESS" />
            </div>

            <AddressInput name="address" className={styles.address} />
          </Col>
        </Row>

        <ModalFooter>
          <FormattedMessage
            as={FormButton}
            htmlType="submit"
            shouldSubmitForm={false}
            id="FINEOS_COMMON.GENERAL.OK"
          />
          <FormattedMessage
            as={FormButton}
            buttonType={ButtonType.LINK}
            htmlType="reset"
            onClick={onFormClose}
            id="FINEOS_COMMON.GENERAL.CANCEL"
          />
        </ModalFooter>
      </Form>
    </FormikWithResponseHandling>
  );
};
