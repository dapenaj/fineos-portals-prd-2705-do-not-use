/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  ConcurrencyUpdateError,
  GroupClientCustomerService,
  GroupClientPhoneNumber,
  GroupClientPhoneNumberResource
} from 'fineos-js-api-client';

export const editCustomerPhones = async (
  customerId: string,
  phoneNumber: GroupClientPhoneNumber
): Promise<GroupClientPhoneNumber> => {
  try {
    return await GroupClientCustomerService.getInstance().editCustomerPhoneNumber(
      customerId,
      phoneNumber
    );
  } catch (error) {
    if (error instanceof ConcurrencyUpdateError) {
      throw new ConcurrencyUpdateError(error.remoteData.elements);
    } else {
      throw error;
    }
  }
};

export const addCustomerPhones = async (
  customerId: string,
  phoneNumber: GroupClientPhoneNumberResource
): Promise<GroupClientPhoneNumber> => {
  return GroupClientCustomerService.getInstance().addCustomerPhoneNumber(
    customerId,
    phoneNumber
  );
};

export const removeCustomerPhones = async (
  customerId: string,
  phoneNumberId: string
): Promise<GroupClientPhoneNumber> => {
  return GroupClientCustomerService.getInstance().removeCustomerPhoneNumber(
    customerId,
    phoneNumberId
  );
};
