/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GroupClientPhoneNumber } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { EditPhonesFormType } from './edit-phone-numbers.form.type';
import {
  editCustomerPhones,
  removeCustomerPhones
} from './edit-phone-numbers.api';

export const getEditPhonesData = (
  formData: EditPhonesFormType[],
  phoneNumbers: GroupClientPhoneNumber[]
) => {
  const editedPhones = formData.filter(
    (formPhoneNumber, index) =>
      formPhoneNumber.id &&
      formPhoneNumber.id === phoneNumbers[index].id &&
      (formPhoneNumber.contactMethod !== phoneNumbers[index].contactMethod ||
        formPhoneNumber.intCode !== phoneNumbers[index].intCode ||
        formPhoneNumber.areaCode !== phoneNumbers[index].areaCode ||
        formPhoneNumber.telephoneNo !== phoneNumbers[index].telephoneNo ||
        formPhoneNumber.exDirectory !== phoneNumbers[index].exDirectory ||
        formPhoneNumber.extension !== phoneNumbers[index].extension)
  );
  return editedPhones;
};

export const getAddedPhonesData = (
  formData: EditPhonesFormType[],
  phoneNumbers: GroupClientPhoneNumber[]
) =>
  formData.filter(
    formPhoneNumber =>
      (!formPhoneNumber.id && formData.indexOf(formPhoneNumber)) ||
      (!_isEmpty(formPhoneNumber) && _isEmpty(phoneNumbers))
  );

export const getRemovedPhonesData = (
  formData: EditPhonesFormType[],
  phoneNumbers: GroupClientPhoneNumber[]
) =>
  phoneNumbers?.filter(
    phoneNumber =>
      !formData.some(formPhoneNumber => formPhoneNumber.id === phoneNumber.id)
  );

export const applyEditPhoneChange = async (
  customerId: string,
  editedPhones: GroupClientPhoneNumber[]
): Promise<GroupClientPhoneNumber[]> => {
  const editedPhoneNumbers: GroupClientPhoneNumber[] = [];

  for (const editedPhoneNumber of editedPhones) {
    editedPhoneNumbers.push(
      await editCustomerPhones(customerId, editedPhoneNumber)
    );
  }

  return editedPhoneNumbers;
};

export const applyRemovePhoneChange = async (
  customerId: string,
  removedPhones: GroupClientPhoneNumber[]
) => {
  for (const removedPhone of removedPhones) {
    await removeCustomerPhones(customerId, removedPhone.id);
  }
};
