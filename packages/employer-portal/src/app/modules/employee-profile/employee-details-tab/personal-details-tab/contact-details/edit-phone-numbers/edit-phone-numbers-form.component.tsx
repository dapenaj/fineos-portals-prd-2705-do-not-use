/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  createThemedStyles,
  ButtonType,
  FormButton,
  UiButton,
  Form,
  FormattedMessage,
  ModalFooter
} from 'fineos-common';
import { Row } from 'antd';
import { GroupClientPhoneNumber } from 'fineos-js-api-client';
import { FormikHelpers } from 'formik';
import classNames from 'classnames';
import { isEmpty as _isEmpty } from 'lodash';

import {
  addPhoneArray,
  FormikWithResponseHandling
} from '../../../../../../shared';

import { PhoneList } from './phone-list.component';
import { buildEditPhonesValidation } from './edit-phone-numbers.form';
import {
  getEditPhonesData,
  getAddedPhonesData,
  applyEditPhoneChange,
  applyRemovePhoneChange,
  getRemovedPhonesData
} from './edit-phone-numbers.util';
import { DEFAULT_PHONE_NUMBER } from './edit-phone-numbers.constant';
import styles from './edit-phone-numbers.module.scss';

type EditPhoneNumbersFormProps = {
  customerId: string;
  phoneNumbers: GroupClientPhoneNumber[];
  onShouldRefreshChange: (
    shouldRefresh: boolean,
    changedDetails?: GroupClientPhoneNumber[]
  ) => void;
  onFormClose: () => void;
  onEditFinish: (changedDetails: GroupClientPhoneNumber[]) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    color: theme.colorSchema.neutral8
  }
}));

export const EditPhoneNumbersForm: React.FC<EditPhoneNumbersFormProps> = ({
  phoneNumbers,
  customerId,
  onFormClose,
  onShouldRefreshChange,
  onEditFinish
}) => {
  const themedStyles = useThemedStyles();

  const handleSubmit = async (
    data: {
      phoneNumbers: GroupClientPhoneNumber[];
    },
    formikProps: FormikHelpers<{
      phoneNumbers: GroupClientPhoneNumber[];
    }>
  ) => {
    const editedPhoneNumbers = getEditPhonesData(
      data.phoneNumbers,
      phoneNumbers
    );
    await applyEditPhoneChange(customerId, editedPhoneNumbers);

    const addedPhones = getAddedPhonesData(data.phoneNumbers, phoneNumbers);
    const savedPhones = await addPhoneArray(customerId, addedPhones);

    // We update phone ID after is saved
    savedPhones.forEach(phone => {
      const phoneToUpdate = data.phoneNumbers.find(
        item =>
          item.areaCode === phone.areaCode &&
          item.telephoneNo === phone.telephoneNo
      );

      if (phoneToUpdate) {
        phoneToUpdate.id = phone.id;
      }
    });

    const removedPhonesData = getRemovedPhonesData(
      data.phoneNumbers,
      phoneNumbers
    );

    await applyRemovePhoneChange(
      customerId,
      removedPhonesData.filter(phoneData => phoneData.id)
    );

    onEditFinish(data.phoneNumbers);
    formikProps.resetForm();
    onShouldRefreshChange(false);
  };

  return (
    <FormikWithResponseHandling
      initialValues={
        !_isEmpty(phoneNumbers)
          ? { phoneNumbers }
          : { phoneNumbers: [DEFAULT_PHONE_NUMBER] as GroupClientPhoneNumber[] }
      }
      validationSchema={buildEditPhonesValidation}
      validateOnMount={true}
      dataParseFunction={data => {
        onShouldRefreshChange(true, data);
        return { phoneNumbers: data };
      }}
      onSubmit={handleSubmit}
    >
      {({ values, isValid, touched }) => (
        <Form>
          <Row justify="space-between">
            <div
              className={themedStyles.label}
              data-test-el="phone-numbers-field"
            >
              <div className={classNames(themedStyles.label, styles.phones)}>
                <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS" />
              </div>
              <PhoneList
                values={values}
                isValid={isValid}
                touched={touched}
                phoneNumbers={phoneNumbers}
              />
            </div>
          </Row>
          <ModalFooter>
            <FormattedMessage
              as={FormButton}
              htmlType="submit"
              shouldSubmitForm={false}
              data-test-el="submit-edited-phone-numbers"
              id="FINEOS_COMMON.GENERAL.OK"
            />
            <FormattedMessage
              as={UiButton}
              buttonType={ButtonType.LINK}
              // if concurrency occured, we should refresh underlying data even when cancel pressed
              onClick={onFormClose}
              id="FINEOS_COMMON.GENERAL.CANCEL"
            />
          </ModalFooter>
        </Form>
      )}
    </FormikWithResponseHandling>
  );
};
