/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Tabs,
  useAsync,
  usePermissions,
  ViewCustomerDataPermissions,
  ContentRenderer,
  FormattedMessage
} from 'fineos-common';
import { isEmpty as _isEmpty } from 'lodash';
import { Row, Col } from 'antd';
import classNames from 'classnames';

import { TabHeader } from '../tab-header';
import { TabBody } from '../tab-body';
import {
  fetchCustomerInfo,
  fetchEmailAddresses,
  fetchPhoneNumbers
} from '../../../../shared';
import { UpdateCustomer } from '../../employee-profile.type';
import { fetchCommunicationPreferences } from '../communication-preferences-tab/communication-preferences-tab.api';
import { useEmployeeDetailsTabThemedStyles } from '../employee-details-tab.styles';
import commonStyles from '../employee-details-tab.module.scss';

import { PersonalDetails } from './personal-details';
import { ContactDetails } from './contact-details/contact-details.component';
import { GroupClientPhoneNumber } from 'fineos-js-api-client';

type PersonalDetailsTabProps = React.ComponentProps<typeof Tabs.TabPane> & {
  employeeId: string;
  onCustomerUpdate: ({ firstName, lastName }: UpdateCustomer) => void;
};

export const PersonalDetailsTab: React.FC<PersonalDetailsTabProps> = ({
  employeeId,
  onCustomerUpdate,
  ...props
}) => {
  const hasCustomerInfoPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
  );

  const hasEmailPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_EMAIL_ADDRESSES
  );

  const hasPhoneNumberPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
  );

  const {
    executeAgain: refreshCommunicationPreferences
  } = useAsync(fetchCommunicationPreferences, [employeeId]);

  const {
    state: { isPending: infoPending, value: customer },
    changeAsyncValue: refreshCustomerInfoInMemory
  } = useAsync(fetchCustomerInfo, [employeeId], {
    shouldExecute: () => hasCustomerInfoPermission
  });

  const {
    state: { isPending: emailPending, value: emails },
    changeAsyncValue: refreshEmailAddressesInMemory
  } = useAsync(fetchEmailAddresses, [employeeId], {
    shouldExecute: () => hasEmailPermission,
    defaultValue: []
  });

  const {
    state: { isPending: phonePending, value: phones },
    changeAsyncValue: refreshPhoneNumbersInMemory
  } = useAsync(fetchPhoneNumbers, [employeeId], {
    shouldExecute: () => hasPhoneNumberPermission,
    defaultValue: []
  });

  const isPending = infoPending || emailPending || phonePending;

  const commonThemedStyles = useEmployeeDetailsTabThemedStyles();

  const doRefreshForPhones = (phones?: GroupClientPhoneNumber[]) => {
    phones && refreshPhoneNumbersInMemory(phones);
    refreshCommunicationPreferences();
  };

  return (
    <Tabs.TabPane {...props}>
      <TabHeader>
        <FormattedMessage
          className={classNames(commonStyles.header, commonThemedStyles.header)}
          as="h2"
          id="PROFILE.PERSONAL_DETAILS_TAB.TITLE"
        />
      </TabHeader>
      <TabBody>
        <ContentRenderer
          isLoading={isPending}
          isEmpty={_isEmpty(customer)}
          data-test-el="personal-details-tab"
        >
          <>
            {customer && (
              <Row>
                <Col xs={24} md={12}>
                  <PersonalDetails
                    customer={customer}
                    onCustomerRefresh={data => {
                      refreshCustomerInfoInMemory(data);
                      onCustomerUpdate({
                        firstName: data.firstName,
                        lastName: data.lastName
                      });
                    }}
                  />
                </Col>
                <Col xs={24} md={12}>
                  <ContactDetails
                    customerId={customer.id}
                    emails={emails}
                    onChangedEmail={emails => {
                      emails && refreshEmailAddressesInMemory(emails);
                      refreshCommunicationPreferences();
                    }}
                    phones={phones}
                    onChangedPhones={doRefreshForPhones}
                  />
                </Col>
              </Row>
            )}
          </>
        </ContentRenderer>
      </TabBody>
    </Tabs.TabPane>
  );
};
