/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useRef, useState } from 'react';
import {
  AsteriskInfo,
  PropertyItem,
  Button,
  ButtonType,
  Modal,
  usePermissions,
  ManageCustomerDataPermissions,
  FormattedMessage
} from 'fineos-common';
import { isEmpty as _isEmpty } from 'lodash';
import { Row, Col } from 'antd';
import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber
} from 'fineos-js-api-client';

import { formatPhoneForDisplay } from '../../../../../shared';
import employeeDetailsTabStyles from '../../employee-details-tab.module.scss';

import styles from './contact-details.module.scss';
import { EditPhoneNumbersForm } from './edit-phone-numbers/edit-phone-numbers-form.component';
import { EditEmailsForm } from './edit-emails/edit-emails-form.component';

type ContactDetailsProps = {
  customerId: string;
  emails: GroupClientEmailAddress[];
  onChangedEmail: (emails?: GroupClientEmailAddress[]) => void;
  phones: GroupClientPhoneNumber[];
  onChangedPhones: (phones?: GroupClientPhoneNumber[]) => void;
};

const shouldRefresh = (
  shouldRefreshRef: React.MutableRefObject<boolean>,
  updatedLists: React.MutableRefObject<
    GroupClientPhoneNumber[] | GroupClientEmailAddress[] | undefined
  >
) => shouldRefreshRef.current && !_isEmpty(updatedLists.current);

export const ContactDetails: React.FC<ContactDetailsProps> = ({
  customerId,
  emails,
  onChangedEmail,
  phones,
  onChangedPhones
}) => {
  const hasEditEmailPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS
  );
  const hasAddEmailPermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS
  );
  const hasEditPhonePermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
  );
  const hasAddPhonePermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS
  );
  const isPhoneNumbersEditAvailable =
    hasAddPhonePermission || (hasEditPhonePermission && phones.length);
  const isEmailsEditAvailable =
    hasAddEmailPermission || (hasEditEmailPermission && emails.length);

  const [isEditEmailFormVisible, toggleEditEmailForm] = useState(false);
  const [isEditPhonesFormVisible, toggleEditPhonesForm] = useState(false);
  const onCloseEditEmailForm = () => toggleEditEmailForm(false);
  const onCloseEditPhonesForm = () => toggleEditPhonesForm(false);
  const shouldRefreshEmails = useRef(false);
  const shouldRefreshPhones = useRef(false);
  const updatedEmails = useRef<GroupClientEmailAddress[]>();
  const updatedPhones = useRef<GroupClientPhoneNumber[]>();

  return (
    <>
      <Row>
        <Col xs={24}>
          <PropertyItem
            label={
              <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.EMAILS" />
            }
            className={styles.emails}
          >
            {!_isEmpty(emails) &&
              emails.map((email, index) => (
                <div key={`EMAIL_${index}`}>{email.emailAddress}</div>
              ))}
          </PropertyItem>
        </Col>
      </Row>

      {isEmailsEditAvailable && (
        <Row className={employeeDetailsTabStyles.row}>
          <Col xs={24}>
            <div className={styles.link}>
              <Button
                data-test-el="email-actions-button"
                buttonType={ButtonType.LINK}
                onClick={() => {
                  toggleEditEmailForm(!isEditEmailFormVisible);
                  shouldRefreshEmails.current = false;
                }}
              >
                <FormattedMessage
                  id={
                    !_isEmpty(emails)
                      ? 'PROFILE.PERSONAL_DETAILS_TAB.EDIT_EMAILS'
                      : 'PROFILE.PERSONAL_DETAILS_TAB.ADD_EMAIL'
                  }
                />
              </Button>
            </div>
          </Col>
        </Row>
      )}

      <Row>
        <Col xs={24}>
          <PropertyItem
            className={styles.phoneNumbers}
            label={
              <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS" />
            }
          >
            {!_isEmpty(phones) &&
              phones.map((phone, index) => (
                <div key={`PHONE_${index}`}>{formatPhoneForDisplay(phone)}</div>
              ))}
          </PropertyItem>
        </Col>
      </Row>

      {isPhoneNumbersEditAvailable && (
        <Row className={employeeDetailsTabStyles.row}>
          <Col xs={24}>
            <div className={styles.link}>
              <Button
                data-test-el="phone-number-actions-button"
                buttonType={ButtonType.LINK}
                onClick={() => {
                  toggleEditPhonesForm(!isEditPhonesFormVisible);
                  shouldRefreshPhones.current = false;
                }}
              >
                <FormattedMessage
                  id={
                    !_isEmpty(phones)
                      ? 'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PHONE_NUMBERS'
                      : 'PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE'
                  }
                />
              </Button>
            </div>
          </Col>
        </Row>
      )}

      <Modal
        ariaLabelId={`PROFILE.PERSONAL_DETAILS_TAB.${
          !_isEmpty(emails) ? 'EDIT_EMAILS' : 'ADD_EMAIL'
        }`}
        header={
          <FormattedMessage
            id={`PROFILE.PERSONAL_DETAILS_TAB.${
              !_isEmpty(emails) ? 'EDIT_EMAILS' : 'ADD_EMAIL'
            }`}
          />
        }
        visible={isEditEmailFormVisible}
        onCancel={() => {
          if (shouldRefresh(shouldRefreshEmails, updatedEmails)) {
            onChangedEmail(updatedEmails.current);
          }
          toggleEditEmailForm(false);
          shouldRefreshEmails.current = false;
        }}
        data-test-el="edit-personal-details-modal"
      >
        <AsteriskInfo />
        <EditEmailsForm
          customerId={customerId}
          emails={emails}
          onFormClose={() => {
            if (shouldRefresh(shouldRefreshEmails, updatedEmails)) {
              onChangedEmail(updatedEmails.current);
            }
            onCloseEditEmailForm();
          }}
          onShouldRefreshChange={(
            shouldRefreshTemp: boolean,
            emailsTemp: GroupClientEmailAddress[] | undefined
          ) => {
            shouldRefreshEmails.current = shouldRefreshTemp;
            if (shouldRefreshTemp && emailsTemp) {
              updatedEmails.current = emailsTemp;
            }
          }}
          onEditFinish={emailsTemp => {
            onChangedEmail(emailsTemp);
            toggleEditEmailForm(false);
          }}
        />
      </Modal>

      <Modal
        ariaLabelId={`PROFILE.PERSONAL_DETAILS_TAB.${
          !_isEmpty(phones) ? 'EDIT_PHONE_NUMBERS' : 'ADD_PHONE'
        }`}
        header={
          <FormattedMessage
            id={`PROFILE.PERSONAL_DETAILS_TAB.${
              !_isEmpty(phones) ? 'EDIT_PHONE_NUMBERS' : 'ADD_PHONE'
            }`}
          />
        }
        visible={isEditPhonesFormVisible}
        onCancel={() => {
          if (shouldRefresh(shouldRefreshPhones, updatedPhones)) {
            onChangedPhones(updatedPhones.current);
          }
          toggleEditPhonesForm(false);
          shouldRefreshPhones.current = false;
        }}
        data-test-el="edit-personal-details-modal"
      >
        <AsteriskInfo />
        <EditPhoneNumbersForm
          customerId={customerId}
          phoneNumbers={phones}
          onFormClose={() => {
            if (shouldRefresh(shouldRefreshPhones, updatedPhones)) {
              onChangedPhones(updatedPhones.current);
            }
            onCloseEditPhonesForm();
          }}
          onShouldRefreshChange={(
            shouldRefreshTemp: boolean,
            phonesTemp: GroupClientPhoneNumber[] | undefined
          ) => {
            shouldRefreshPhones.current = shouldRefreshTemp;
            if (shouldRefreshTemp && phonesTemp) {
              updatedPhones.current = phonesTemp;
            }
          }}
          onEditFinish={phonesTemp => {
            onChangedPhones(phonesTemp);
            toggleEditPhonesForm(false);
          }}
        />
      </Modal>
    </>
  );
};
