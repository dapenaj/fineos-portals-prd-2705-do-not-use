/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  FormButton,
  UiButton,
  ButtonType,
  Form,
  FormattedMessage,
  ModalFooter,
  FormGroup
} from 'fineos-common';
import { Row } from 'antd';
import { FormikHelpers } from 'formik';
import { GroupClientEmailAddress } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { FormikWithResponseHandling } from '../../../../../../shared';

import { EmailList } from './email-list.component';
import { buildEditEmailValidation } from './edit-emails.form';
import {
  getEditEmailsData,
  getAddedEmailsData,
  applyEditEmailChange,
  applyAddEmailChange,
  applyRemoveEmailChange
} from './edit-emails.util';
import { DEFAULT_EMAIL } from './edit-emails.constant';
import styles from './edit-emails.module.scss';

type EditEmailsFormProps = {
  customerId: string;
  emails: GroupClientEmailAddress[];
  onShouldRefreshChange: (
    shouldRefresh: boolean,
    changedDetails?: GroupClientEmailAddress[]
  ) => void;
  onFormClose: () => void;
  onEditFinish: (changedDetails: GroupClientEmailAddress[]) => void;
};

export const EditEmailsForm: React.FC<EditEmailsFormProps> = ({
  customerId,
  emails,
  onShouldRefreshChange,
  onFormClose,
  onEditFinish
}) => {
  const [removedEmailData, setRemovedEmailData] = useState<
    GroupClientEmailAddress[]
  >([]);

  const handleSubmit = async (
    data: { emails: GroupClientEmailAddress[] },
    formikProps: FormikHelpers<{
      emails: GroupClientEmailAddress[];
    }>
  ) => {
    const editedEmails = getEditEmailsData(data.emails, emails);
    await applyEditEmailChange(customerId, editedEmails);

    const addedEmails = getAddedEmailsData(data.emails, emails);
    await applyAddEmailChange(customerId, addedEmails);
    await applyRemoveEmailChange(customerId, removedEmailData);

    onEditFinish(data.emails);
    formikProps.resetForm();
    onShouldRefreshChange(false);
  };

  const initialValues = _isEmpty(emails)
    ? { emails: [DEFAULT_EMAIL] }
    : { emails };

  return (
    <FormikWithResponseHandling
      initialValues={initialValues}
      validationSchema={buildEditEmailValidation()}
      validateOnMount={true}
      dataParseFunction={data => {
        onShouldRefreshChange(true, data);
        return { emails: data };
      }}
      onSubmit={handleSubmit}
    >
      {({ values, isValid }) => (
        <Form>
          <FormGroup
            data-test-el="emails-field"
            isRequired={true}
            showErrorMessage={false}
          >
            <Row className={styles.row}>
              <EmailList
                values={values}
                isValid={isValid}
                removedEmailData={removedEmailData}
                onEmailDataRemove={setRemovedEmailData}
              />
            </Row>
          </FormGroup>
          <ModalFooter>
            <FormattedMessage
              as={FormButton}
              htmlType="submit"
              shouldSubmitForm={false}
              data-test-el="submit-edited-emails"
              id="FINEOS_COMMON.GENERAL.OK"
            />
            <FormattedMessage
              as={UiButton}
              buttonType={ButtonType.LINK}
              onMouseDown={onFormClose}
              onKeyDown={({ key }: React.KeyboardEvent) => {
                key === 'Enter' && onFormClose();
              }}
              id="FINEOS_COMMON.GENERAL.CANCEL"
            />
          </ModalFooter>
        </Form>
      )}
    </FormikWithResponseHandling>
  );
};
