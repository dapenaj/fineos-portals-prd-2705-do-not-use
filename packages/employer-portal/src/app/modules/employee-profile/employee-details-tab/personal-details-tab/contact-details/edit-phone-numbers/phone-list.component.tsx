/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useLayoutEffect, useState } from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import {
  createThemedStyles,
  ButtonType,
  Button,
  Card,
  DeleteButton,
  FormattedMessage,
  usePermissions,
  ManageCustomerDataPermissions
} from 'fineos-common';
import { Row, Col } from 'antd';
import { GroupClientPhoneNumber } from 'fineos-js-api-client';
import { FieldArray, FormikTouched } from 'formik';
import classNames from 'classnames';

import {
  PhoneInput,
  PhoneTypeInput,
  withAutoScroll
} from '../../../../../../shared';

import { DEFAULT_PHONE_NUMBER } from './edit-phone-numbers.constant';
import styles from './edit-phone-numbers.module.scss';

type PhoneListProps = {
  phoneNumbers: GroupClientPhoneNumber[];
  values: { phoneNumbers: GroupClientPhoneNumber[] };
  isValid: boolean;
  touched: FormikTouched<any>;
};

const useThemedStyles = createThemedStyles(theme => ({
  phoneDetails: {
    border: `1px solid ${theme.colorSchema.neutral3}`
  }
}));

const CardWithAutoScroll = withAutoScroll(Card);

export const PhoneList: React.FC<PhoneListProps> = ({
  phoneNumbers,
  values,
  isValid,
  touched
}) => {
  const lastGroupElRef = React.createRef() as React.RefObject<HTMLDivElement>;

  const themedStyles = useThemedStyles();
  const hasAddPhoneNumberPermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS
  );
  const hasEditPhoneNumberPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
  );
  const hasRemovePhoneNumberPermission = usePermissions(
    ManageCustomerDataPermissions.REMOVE_CUSTOMER_PHONE_NUMBERS
  );
  const isPhoneInputDisabled = (phone: GroupClientPhoneNumber) =>
    (!hasEditPhoneNumberPermission && !!phone.id) ||
    (!hasAddPhoneNumberPermission && !phone.id);

  const [shouldFocusOnLastGroup, setShouldFocusOnLastGroup] = useState(false);

  useLayoutEffect(() => {
    if (!isValid && shouldFocusOnLastGroup) {
      const combobox = lastGroupElRef?.current?.querySelector(
        'input[role="combobox"]'
      ) as HTMLDivElement;

      if (combobox) {
        const event = document.createEvent('Event');
        event.initEvent('focus', true, false);
        combobox.dispatchEvent(event);
        combobox.focus();
      }

      setShouldFocusOnLastGroup(false);
    }
    // eslint-disable-next-line
  }, [isValid]);

  return (
    <FieldArray
      name="phoneNumbers"
      render={arrayHelpers => (
        <>
          <div className={styles.scrollWrapper} data-test-el="phone-list">
            {values.phoneNumbers?.map(
              (phone: GroupClientPhoneNumber, index: number) => {
                const TargetCard =
                  values.phoneNumbers?.length > phoneNumbers?.length &&
                  index === values.phoneNumbers?.length - 1
                    ? CardWithAutoScroll
                    : Card;
                return (
                  <TargetCard
                    className={classNames(
                      styles.phoneDetails,
                      themedStyles.phoneDetails
                    )}
                    key={index}
                  >
                    <Row>
                      <Col ref={lastGroupElRef} xs={24}>
                        <Row>
                          <Col xs={values.phoneNumbers.length > 1 ? 22 : 24}>
                            <PhoneTypeInput
                              name={`phoneNumbers[${index}].contactMethod`}
                              className={styles.dropdown}
                              isDisabled={isPhoneInputDisabled(phone)}
                            />
                          </Col>
                          {hasRemovePhoneNumberPermission &&
                            values.phoneNumbers.length > 1 && (
                              <Col
                                xs={2}
                                className={styles.deleteNumberButtonWrapper}
                              >
                                <DeleteButton
                                  className={styles.deleteNumberButton}
                                  iconLabel={
                                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.DELETE_PHONE_NUMBER_LABEL" />
                                  }
                                  data-test-el="delete-phone-number"
                                  onClick={() => {
                                    arrayHelpers.remove(index);
                                  }}
                                />
                              </Col>
                            )}
                        </Row>
                        <PhoneInput
                          name={`phoneNumbers[${index}]`}
                          isDisabled={isPhoneInputDisabled(phone)}
                        />
                      </Col>
                    </Row>
                  </TargetCard>
                );
              }
            )}
          </div>
          {hasAddPhoneNumberPermission && (
            <Row>
              <FormattedMessage
                className={styles.link}
                as={Button}
                buttonType={ButtonType.LINK}
                onClick={() => {
                  arrayHelpers.push(DEFAULT_PHONE_NUMBER);
                  setShouldFocusOnLastGroup(true);
                }}
                disabled={
                  !isValid || (_isEmpty(phoneNumbers) && _isEmpty(touched))
                }
                id="PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_PHONE"
              />
            </Row>
          )}
        </>
      )}
    />
  );
};
