/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage, createThemedStyles } from 'fineos-common';
import classNames from 'classnames';

import { ReactComponent as PersonalDetailsIcon } from '../../../../../assets/img/personal_details.svg';
import { ReactComponent as EmploymentDetailsIcon } from '../../../../../assets/img/employment_details.svg';
import { ReactComponent as CommunicationPreferencesIcon } from '../../../../../assets/img/communication_preferences.svg';
import { EmployeeDetailsTabKey } from '../employee-details-tab.component';

import styles from './tab-label.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    color: theme.colorSchema.neutral8,
    '& > svg': {
      fill: theme.colorSchema.primaryColor
    }
  },
  activeWrapper: {
    color: theme.colorSchema.primaryColorContrast,
    '& > svg': {
      fill: theme.colorSchema.primaryColorContrast
    }
  }
}));

const getTranslationFromTabKey = (key: EmployeeDetailsTabKey) => {
  switch (key) {
    case EmployeeDetailsTabKey.COMMUNICATION_PREFERENCES:
      return (
        <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE" />
      );
    case EmployeeDetailsTabKey.EMPLOYMENT_DETAILS:
      return <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE" />;
    default:
      return <FormattedMessage id="PROFILE.PERSONAL_DETAILS_TAB.TITLE" />;
  }
};

const getIconFromTabKey = (key: EmployeeDetailsTabKey) => {
  switch (key) {
    case EmployeeDetailsTabKey.COMMUNICATION_PREFERENCES:
      return <CommunicationPreferencesIcon />;
    case EmployeeDetailsTabKey.EMPLOYMENT_DETAILS:
      return <EmploymentDetailsIcon />;
    default:
      return <PersonalDetailsIcon />;
  }
};

type TabLabelProps = {
  activeTab: EmployeeDetailsTabKey;
  tabKey: EmployeeDetailsTabKey;
};

export const TabLabel: React.FC<TabLabelProps> = ({ tabKey, activeTab }) => {
  const themedStyles = useThemedStyles();
  return (
    <div
      className={classNames(themedStyles.wrapper, styles.wrapper, {
        [themedStyles.activeWrapper]: tabKey === activeTab
      })}
    >
      {getIconFromTabKey(tabKey)}
      <div className={styles.label}>{getTranslationFromTabKey(tabKey)}</div>
    </div>
  );
};
