/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  Tabs,
  usePermissions,
  ViewCustomerDataPermissions,
  ViewOccupationPermissions,
  createThemedStyles
} from 'fineos-common';
import classNames from 'classnames';
import { CustomerOccupation, CustomerAbsenceEmployment } from 'fineos-js-api-client';

import { UpdateCustomer } from '../employee-profile.type';

import { PersonalDetailsTab } from './personal-details-tab';
import { EmploymentDetailsTab } from './employment-details-tab';
import { CommunicationPreferencesTab } from './communication-preferences-tab';
import { TabLabel } from './tab-label';
import styles from './employee-details-tab.module.scss';

export enum EmployeeDetailsTabKey {
  PERSONAL_DETAILS = 'personalDetails',
  EMPLOYMENT_DETAILS = 'employmentDetails',
  COMMUNICATION_PREFERENCES = 'communicationPreferences'
}

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    '& .ant-tabs-tab': {
      background: theme.colorSchema.neutral1,
      '&:hover': {
        color: 'inherit'
      },
      '& .ant-tabs-tab-btn:focus': {
        borderColor: theme.colorSchema.neutral1,
        boxShadow: `0 0 0 2px ${theme.colorSchema.primaryColor}`
      }
    },
    '& .ant-tabs-tab.ant-tabs-tab-active': {
      background: theme.colorSchema.primaryColor,
      color: theme.colorSchema.primaryColorContrast,
      '&:hover': {
        color: theme.colorSchema.primaryColorContrast
      },
      '&:after': {
        borderRightColor: theme.colorSchema.neutral1
      }
    },
    '& [role="tabpanel"]': {
      background: theme.colorSchema.neutral1
    }
  }
}));

type EmployeeDetailsTabProps = React.ComponentProps<typeof Tabs.TabPane> & {
  employeeId: string;
  isOccupationPending: boolean;
  occupation: (CustomerOccupation & CustomerAbsenceEmployment) | null;
  onOccupationChange: () => void;
  onCustomerUpdate: ({ firstName, lastName }: UpdateCustomer) => void;
};

export const EmployeeDetailsTab: React.FC<EmployeeDetailsTabProps> = ({
  employeeId,
  isOccupationPending,
  occupation,
  onOccupationChange,
  onCustomerUpdate,
  ...props
}) => {
  const [activeTab, setActiveTab] = useState(
    EmployeeDetailsTabKey.PERSONAL_DETAILS
  );

  const hasPersonalDetailsPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
  );
  const hasEmploymentDetailsPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
  );
  const hasCommunicationPreferencesPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_COMMUNICATION_PREFERENCES
  );

  const themedStyles = useThemedStyles();

  return (
    <Tabs.TabPane {...props}>
      <Tabs
        tabPosition="left"
        className={classNames(styles.wrapper, themedStyles.wrapper)}
        onChange={(activeKey: string) =>
          setActiveTab(activeKey as EmployeeDetailsTabKey)
        }
      >
        {hasPersonalDetailsPermission && (
          <PersonalDetailsTab
            employeeId={employeeId}
            key={EmployeeDetailsTabKey.PERSONAL_DETAILS}
            onCustomerUpdate={onCustomerUpdate}
            tab={
              <TabLabel
                tabKey={EmployeeDetailsTabKey.PERSONAL_DETAILS}
                activeTab={activeTab}
              />
            }
          />
        )}
        {hasEmploymentDetailsPermission && (
          <EmploymentDetailsTab
            employeeId={employeeId}
            isOccupationPending={isOccupationPending}
            occupation={occupation}
            onOccupationChange={onOccupationChange}
            key={EmployeeDetailsTabKey.EMPLOYMENT_DETAILS}
            tab={
              <TabLabel
                tabKey={EmployeeDetailsTabKey.EMPLOYMENT_DETAILS}
                activeTab={activeTab}
              />
            }
          />
        )}
        {hasCommunicationPreferencesPermission && (
          <CommunicationPreferencesTab
            employeeId={employeeId}
            key={EmployeeDetailsTabKey.COMMUNICATION_PREFERENCES}
            tab={
              <TabLabel
                tabKey={EmployeeDetailsTabKey.COMMUNICATION_PREFERENCES}
                activeTab={activeTab}
              />
            }
          />
        )}
      </Tabs>
    </Tabs.TabPane>
  );
};
