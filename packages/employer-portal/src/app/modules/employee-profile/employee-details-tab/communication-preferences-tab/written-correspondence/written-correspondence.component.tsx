/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useMemo, useContext } from 'react';
import {
  Button,
  ButtonType,
  FormattedMessage,
  usePermissions,
  ManageCustomerDataPermissions,
  ViewCustomerDataPermissions
} from 'fineos-common';

import { Preference, usePreferenceThemedStyles } from '../preference';
import { formatPhoneForDisplay } from '../../../../../shared';
import { CommunicationPreferencesContext } from '../communication-preferences-tab.context';
import {
  getValidEmail,
  getValidPhone
} from '../communication-preferences-tab.util';

import { WrittenCorrespondenceModal } from './written-correspondence-modal/written-correspondence-modal.component';

type WrittenCorrespondenceProps = {
  employeeId: string;
};

export const WrittenCorrespondence: React.FC<WrittenCorrespondenceProps> = ({
  employeeId
}) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  const { communicationPreferences } = useContext(
    CommunicationPreferencesContext
  );
  const email = getValidEmail(communicationPreferences?.writtenCorrespondence);
  const phone = getValidPhone(communicationPreferences?.writtenCorrespondence);

  const hasManageCommunicationPreferencesPermissions = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES
  );

  const hasViewPhoneNumbersPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
  );

  const hasViewEmailsPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_EMAIL_ADDRESSES
  );

  const isActionButtonVisible = useMemo(
    () =>
      hasManageCommunicationPreferencesPermissions &&
      hasViewPhoneNumbersPermission &&
      hasViewEmailsPermission,
    [
      hasManageCommunicationPreferencesPermissions,
      hasViewEmailsPermission,
      hasViewPhoneNumbersPermission
    ]
  );

  const themedStyles = usePreferenceThemedStyles();
  return (
    <>
      <Preference
        data-test-el="written-correspondence-preference"
        title={
          <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.TITLE" />
        }
        action={
          isActionButtonVisible && (
            <FormattedMessage
              as={Button}
              buttonType={ButtonType.LINK}
              onClick={() => setIsModalVisible(true)}
              id="PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT"
            />
          )
        }
      >
        {email || phone ? (
          <>
            <div>
              <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.PAPERLESS" />
            </div>
            <div>
              <FormattedMessage
                id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.NOTIFIED_BY"
                values={{
                  label: (
                    <FormattedMessage
                      as="strong"
                      className={themedStyles.highlight}
                      id={`PROFILE.COMMUNICATION_PREFERENCES_TAB.${
                        email ? 'EMAIL' : 'SMS'
                      }`}
                    />
                  ),
                  value: (
                    <strong className={themedStyles.highlight}>
                      {email
                        ? email.emailAddress
                        : formatPhoneForDisplay(phone!)}
                    </strong>
                  )
                }}
              />
            </div>
          </>
        ) : (
          <FormattedMessage
            id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.EMPTY"
            values={{
              paper: (
                <FormattedMessage
                  as="strong"
                  className={themedStyles.highlight}
                  id="PROFILE.COMMUNICATION_PREFERENCES_TAB.PAPER"
                />
              ),
              post: (
                <FormattedMessage
                  as="strong"
                  className={themedStyles.highlight}
                  id="PROFILE.COMMUNICATION_PREFERENCES_TAB.VIA_POST"
                />
              )
            }}
          />
        )}
      </Preference>

      <WrittenCorrespondenceModal
        isVisible={isModalVisible}
        employeeId={employeeId}
        onModalClose={() => setIsModalVisible(false)}
      />
    </>
  );
};
