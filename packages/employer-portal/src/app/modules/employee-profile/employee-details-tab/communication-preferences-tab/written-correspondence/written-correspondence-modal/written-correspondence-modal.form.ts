/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { isEmpty as _isEmpty } from 'lodash';
import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber
} from 'fineos-js-api-client';
import { newEmailValidationSchema } from 'fineos-common';

import {
  validateSelectedContactIndex,
  hasWrittenCorrespondenceAllowedNumber,
  hasNotificationOfUpdatesAllowedNumber,
  hasDirectCorrespondenceAllowedNumber,
  isLandline
} from '../../communication-preferences.form';
import {
  WrittenCorrespondenceType,
  CommunicationOptions,
  FullCommunicationPreference
} from '../../communication-preferences-tab.enum';
import {
  phoneInputValidation,
  PhoneType,
  initialPhoneInputValue
} from '../../../../../../shared';
import { getValidTypePhoneNumbers } from '../../communication-preferences-tab.util';

import { WrittenCorrespondenceFormState } from './written-correspondence-modal.type';

export const getSelectedEmailAddressIndex = (
  emailAddresses: GroupClientEmailAddress[],
  communicationPreferences: FullCommunicationPreference
) => {
  let selectedEmailAddress: string | null;
  const { notificationOfUpdates, writtenCorrespondence } =
    communicationPreferences || {};

  if (!_isEmpty(writtenCorrespondence?.emailAddresses)) {
    selectedEmailAddress = writtenCorrespondence!.emailAddresses[0]
      .emailAddress;
  } else if (!_isEmpty(notificationOfUpdates?.emailAddresses)) {
    selectedEmailAddress = notificationOfUpdates!.emailAddresses[0]
      .emailAddress;
  } else {
    selectedEmailAddress = null;
  }

  if (emailAddresses?.length === 1) {
    return 0;
  }

  return emailAddresses?.findIndex(
    address => address.emailAddress === selectedEmailAddress
  );
};

export const getSelectedPhoneNumberIndex = (
  phoneNumbers: GroupClientPhoneNumber[],
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
): number => {
  const { directCorrespondence, notificationOfUpdates, writtenCorrespondence } =
    communicationPreferences || {};
  const hasWrittenCorrespondenceNumber = hasWrittenCorrespondenceAllowedNumber(
    communicationPreferences,
    allowedPhoneTypes
  );
  const hasNotificationOfUpdatesNumber = hasNotificationOfUpdatesAllowedNumber(
    communicationPreferences,
    allowedPhoneTypes
  );
  const hasDirectCorrespondenceNumber = hasDirectCorrespondenceAllowedNumber(
    communicationPreferences,
    allowedPhoneTypes
  );
  const validTypePhoneNumbers = getValidTypePhoneNumbers(
    allowedPhoneTypes,
    phoneNumbers
  );

  // select phone number if there is only one
  if (phoneNumbers?.length === 1) {
    return 0;
  }

  if (!hasWrittenCorrespondenceNumber) {
    // if both contexts have number selected and both of them are mobile
    if (
      hasDirectCorrespondenceNumber &&
      hasNotificationOfUpdatesNumber &&
      !isLandline(directCorrespondence!.phoneNumbers[0]) &&
      !isLandline(notificationOfUpdates!.phoneNumbers[0])
    ) {
      return -1;
    }

    // if has only direct correspondence mobile number
    if (
      hasDirectCorrespondenceNumber &&
      !hasNotificationOfUpdatesNumber &&
      !isLandline(directCorrespondence!.phoneNumbers[0])
    ) {
      return validTypePhoneNumbers?.findIndex(
        phoneNumber =>
          phoneNumber.id === directCorrespondence?.phoneNumbers[0].id
      );
    }

    // if has only notification of updates number or directCorrespondence is landline
    if (
      hasNotificationOfUpdatesNumber &&
      (!hasDirectCorrespondenceNumber ||
        isLandline(directCorrespondence!.phoneNumbers[0]))
    ) {
      return validTypePhoneNumbers.findIndex(
        phoneNumber =>
          phoneNumber.id === notificationOfUpdates?.phoneNumbers[0].id
      );
    }
    return -1;
  }

  return validTypePhoneNumbers?.findIndex(
    phoneNumber => phoneNumber.id === writtenCorrespondence?.phoneNumbers[0]?.id
  );
};

const getSelectedCommunicationType = (
  phoneNumbers: GroupClientPhoneNumber[],
  emailAddresses: GroupClientEmailAddress[],
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
) => {
  const validTypePhoneNumbers = getValidTypePhoneNumbers(
    allowedPhoneTypes,
    phoneNumbers
  );
  const selectedPhoneNumberIndex = validTypePhoneNumbers?.findIndex(
    phoneNumber =>
      phoneNumber.id ===
      communicationPreferences.writtenCorrespondence?.phoneNumbers[0]?.id
  );
  const selectedEmailAddressIndex = emailAddresses?.findIndex(
    emailAddress =>
      emailAddress.emailAddress ===
      communicationPreferences.writtenCorrespondence?.emailAddresses[0]
        ?.emailAddress
  );

  if (selectedEmailAddressIndex !== -1) {
    return CommunicationOptions.EMAIL;
  }

  if (selectedPhoneNumberIndex !== -1) {
    return CommunicationOptions.SMS;
  }

  return null;
};

export const writtenCorrespondeceInitialValue = (
  emailAddresses: GroupClientEmailAddress[],
  phoneNumbers: GroupClientPhoneNumber[],
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
): WrittenCorrespondenceFormState => {
  const selectedPhoneNumberIndex = getSelectedPhoneNumberIndex(
    phoneNumbers,
    communicationPreferences,
    allowedPhoneTypes
  );
  return {
    mainCommunicationType: getSelectedCommunicationType(
      phoneNumbers,
      emailAddresses,
      communicationPreferences,
      allowedPhoneTypes
    )
      ? WrittenCorrespondenceType.PORTAL
      : WrittenCorrespondenceType.PAPER,
    portalCommunicationType: getSelectedCommunicationType(
      phoneNumbers,
      emailAddresses,
      communicationPreferences,
      allowedPhoneTypes
    ),
    newPhoneType: null,
    newPhoneNumber: initialPhoneInputValue,
    emailAddress: '',
    selectedPhoneNumberIndex,
    selectedEmailAddressIndex: getSelectedEmailAddressIndex(
      emailAddresses,
      communicationPreferences
    )
  };
};

export const writtenCorrespondeceValidation = (
  emailAddresses: GroupClientEmailAddress[],
  phoneNumbers: GroupClientPhoneNumber[]
) =>
  Yup.object({
    mainCommunicationType: Yup.string()
      .oneOf(Object.values(WrittenCorrespondenceType))
      .required(),
    portalCommunicationType: Yup.mixed().when('mainCommunicationType', {
      is: WrittenCorrespondenceType.PORTAL,
      then: Yup.string()
        .nullable()
        .oneOf(
          Object.values(CommunicationOptions),
          'FINEOS_COMMON.VALIDATION.REQUIRED_SELECTED_OPTION'
        )
    }),
    emailAddress: Yup.string().when(
      ['mainCommunicationType', 'portalCommunicationType'],
      {
        is: (
          mainCommunicationType: WrittenCorrespondenceType,
          portalCommunicationType: CommunicationOptions
        ) =>
          mainCommunicationType === WrittenCorrespondenceType.PORTAL &&
          portalCommunicationType === CommunicationOptions.EMAIL &&
          _isEmpty(emailAddresses),
        then: newEmailValidationSchema
          .clone()
          .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      }
    ),
    newPhoneType: Yup.mixed(),
    newPhoneNumber: Yup.object()
      .nullable()
      .when(['mainCommunicationType', 'portalCommunicationType'], {
        is: (
          mainCommunicationType: WrittenCorrespondenceType,
          portalCommunicationType: CommunicationOptions
        ) =>
          mainCommunicationType === WrittenCorrespondenceType.PORTAL &&
          portalCommunicationType === CommunicationOptions.SMS &&
          _isEmpty(phoneNumbers),
        then: phoneInputValidation.clone()
      }),
    selectedPhoneNumberIndex: Yup.number().when(
      ['mainCommunicationType', 'portalCommunicationType'],
      {
        is: (
          mainCommunicationType: WrittenCorrespondenceType,
          portalCommunicationType: CommunicationOptions
        ) =>
          mainCommunicationType === WrittenCorrespondenceType.PORTAL &&
          portalCommunicationType === CommunicationOptions.SMS,
        then: validateSelectedContactIndex(phoneNumbers, 'PHONE')
      }
    ),
    selectedEmailAddressIndex: Yup.number().when(
      ['mainCommunicationType', 'portalCommunicationType'],
      {
        is: (
          mainCommunicationType: WrittenCorrespondenceType,
          portalCommunicationType: CommunicationOptions
        ) =>
          mainCommunicationType === WrittenCorrespondenceType.PORTAL &&
          portalCommunicationType === CommunicationOptions.EMAIL,
        then: validateSelectedContactIndex(emailAddresses, 'EMAIL')
      }
    )
  });
