import React, { useState, useContext, useEffect } from 'react';
import { Field, useFormikContext } from 'formik';
import classNames from 'classnames';
import {
  FormattedMessage,
  Button,
  ButtonType,
  createThemedStyles,
  textStyleToCss,
  primaryLight2Color,
  Card,
  FormRadioInput,
  usePermissions,
  ManageCustomerDataPermissions,
  FormGroup,
  Radio
} from 'fineos-common';
import { BaseEmailAddress } from 'fineos-js-api-client';

import { withAutoScroll } from '../../../../../shared';
import { AddEmailModal } from '../add-email-modal/add-email-modal.component';
import { CommunicationPreferencesContext } from '../communication-preferences-tab.context';
import communicationPreferencesStyles from '../communication-preferences-tab.module.scss';

import styles from './select-email.module.scss';

type SelectEmailProps = {
  onEmailPass: (email: BaseEmailAddress) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  selectEmailLabel: {
    ...textStyleToCss(theme.typography.baseText)
  },
  hasHighlight: {
    background: primaryLight2Color(theme.colorSchema)
  }
}));

const CardWithAutoScroll = withAutoScroll(Card);

export const SelectEmail: React.FC<SelectEmailProps> = ({ onEmailPass }) => {
  const [isAddEmailVisible, setIsAddEmailVisible] = useState<boolean>(false);
  const [temporaryEmailList, setTemporaryEmailList] = useState<
    BaseEmailAddress[]
  >([]);
  const [
    recentlyAddedEmail,
    setRecentlyAddedEmail
  ] = useState<BaseEmailAddress | null>(null);

  const { emailAddresses } = useContext(CommunicationPreferencesContext);

  const hasAddNewEmailPermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS
  );

  const shouldAutoScroll = (
    emailList: BaseEmailAddress[],
    recentEmail: BaseEmailAddress | null,
    index: number
  ) =>
    emailList.findIndex(
      email => email.emailAddress === recentEmail?.emailAddress
    ) === 0 && index === 0;

  const formikContext: any = useFormikContext();

  useEffect(() => {
    setTemporaryEmailList(emailAddresses);
  }, [emailAddresses]);

  const themedStyles = useThemedStyles();
  return (
    <>
      <div className={styles.selectEmail} data-test-el="select-email">
        <FormGroup
          isRequired={true}
          className={communicationPreferencesStyles.selectContactItemForm}
          label={
            <div
              className={classNames(
                communicationPreferencesStyles.contentLabel,
                themedStyles.selectEmailLabel
              )}
            >
              <FormattedMessage
                id="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.LABEL"
                values={{
                  actionType: 'Select'
                }}
              />
            </div>
          }
        >
          <Field
            name="selectedEmailAddressIndex"
            component={FormRadioInput}
            className={communicationPreferencesStyles.overflowWrapper}
            value={formikContext.values.selectedEmailAddressIndex || 0}
            wrapperComponent={(index: number) => {
              const TargetCard = shouldAutoScroll(
                temporaryEmailList,
                recentlyAddedEmail,
                index
              )
                ? CardWithAutoScroll
                : Card;
              return (
                <TargetCard
                  key={temporaryEmailList.length - index}
                  // Prop below was added for e2e testing purposes
                  data-test-el={
                    formikContext.values.selectedEmailAddressIndex === index
                      ? 'selected-email-address-card'
                      : 'email-address-card'
                  }
                  className={classNames(styles.emailCard, {
                    [themedStyles.hasHighlight]:
                      formikContext.values.selectedEmailAddressIndex === index
                  })}
                />
              );
            }}
          >
            {temporaryEmailList.map((email, index) => (
              <div
                key={temporaryEmailList.length - index}
                title={email.emailAddress}
              >
                <Radio className={styles.radioOption} value={index}>
                  {email.emailAddress}
                </Radio>
              </div>
            ))}
          </Field>
        </FormGroup>
        {hasAddNewEmailPermission && (
          <FormattedMessage
            as={Button}
            className={
              communicationPreferencesStyles.anotherCommunicationButton
            }
            buttonType={ButtonType.LINK}
            onClick={() => setIsAddEmailVisible(true)}
            id="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON"
          />
        )}
      </div>

      <AddEmailModal
        isVisible={isAddEmailVisible}
        onEmailPass={(email: BaseEmailAddress) => {
          onEmailPass(email);
          setRecentlyAddedEmail(email);
          setTemporaryEmailList([email, ...temporaryEmailList!]);
        }}
        onModalClose={() => setIsAddEmailVisible(false)}
      />
    </>
  );
};
