import React from 'react';
import { Row } from 'antd';

import { PhoneInput, PhoneType, PhoneTypeInput } from '../../../../../shared';
import communicationPreferencesStyles from '../communication-preferences-tab.module.scss';

type AddPhoneNumberFormProps = {
  allowedPhoneTypeNames?: PhoneType[];
};

export const AddPhoneNumberForm: React.FC<AddPhoneNumberFormProps> = ({
  allowedPhoneTypeNames = []
}) => (
  <div
    className={communicationPreferencesStyles.addElementForm}
    data-test-el="add-phone-number-form"
  >
    <Row className={communicationPreferencesStyles.row}>
      <PhoneTypeInput
        name="newPhoneType"
        data-test-el="phone-type-dropdown"
        allowedPhoneTypeNames={allowedPhoneTypeNames}
      />
    </Row>
    <Row className={communicationPreferencesStyles.lastRow}>
      <PhoneInput name="newPhoneNumber" />
    </Row>
  </div>
);
