/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useMemo, useContext } from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import {
  Button,
  ButtonType,
  FormattedMessage,
  usePermissions,
  ViewCustomerDataPermissions,
  ManageCustomerDataPermissions
} from 'fineos-common';

import { Preference, usePreferenceThemedStyles } from '../preference';
import { formatPhoneForDisplay } from '../../../../../shared';
import { CommunicationPreferencesContext } from '../communication-preferences-tab.context';
import { getValidPhone } from '../communication-preferences-tab.util';
import { PhoneCallsModals } from '../communication-preferences-tab.enum';

import { PhoneCallsModal } from './phone-calls-modal/phone-calls-modal.component';

type DirectCorrespondenceProps = {
  employeeId: string;
};

export const DirectCorrespondence: React.FC<DirectCorrespondenceProps> = ({
  employeeId
}) => {
  const [
    correspondenceType,
    setCorrespondenceType
  ] = useState<PhoneCallsModals | null>(null);

  const { communicationPreferences, phoneNumbers } = useContext(
    CommunicationPreferencesContext
  );

  const hasViewPhoneNumbersPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
  );

  const hasAddPhoneNumbersPermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS
  );

  const hasEditPhoneNumbersPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
  );

  const hasManageCommunicationPreferencesPermissions = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES
  );

  const phone = getValidPhone(communicationPreferences?.directCorrespondence);

  const shouldShowAddPhoneNumberSection = useMemo(
    () =>
      (hasManageCommunicationPreferencesPermissions &&
        hasViewPhoneNumbersPermission &&
        hasEditPhoneNumbersPermission &&
        !_isEmpty(phoneNumbers) &&
        !phone) ||
      (hasManageCommunicationPreferencesPermissions &&
        hasAddPhoneNumbersPermission &&
        _isEmpty(phoneNumbers) &&
        !phone),
    [
      hasManageCommunicationPreferencesPermissions,
      hasViewPhoneNumbersPermission,
      hasEditPhoneNumbersPermission,
      hasAddPhoneNumbersPermission,
      phoneNumbers,
      phone
    ]
  );

  const themedStyles = usePreferenceThemedStyles();
  return (
    <>
      {hasViewPhoneNumbersPermission && (
        <Preference
          data-test-el="direct-correspondence-preference"
          title={
            <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.TITLE" />
          }
          action={
            hasManageCommunicationPreferencesPermissions && (
              <FormattedMessage
                as={Button}
                buttonType={ButtonType.LINK}
                onClick={() =>
                  setCorrespondenceType(PhoneCallsModals.PHONE_CALLS)
                }
                id={
                  shouldShowAddPhoneNumberSection
                    ? 'PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.ADD'
                    : 'PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT'
                }
              />
            )
          }
        >
          {phone ? (
            <FormattedMessage
              id="PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.PHONE"
              values={{
                phone: (
                  <strong className={themedStyles.highlight}>
                    {formatPhoneForDisplay(phone)}
                  </strong>
                )
              }}
            />
          ) : (
            <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.EMPTY" />
          )}
        </Preference>
      )}
      <PhoneCallsModal
        employeeId={employeeId}
        onModalClose={() => setCorrespondenceType(null)}
        isVisible={correspondenceType === PhoneCallsModals.PHONE_CALLS}
      />
    </>
  );
};
