/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { newEmailValidationSchema } from 'fineos-common';
import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber
} from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';
import * as Yup from 'yup';

import {
  initialPhoneInputValue,
  phoneInputValidation,
  PhoneType
} from '../../../../../../shared';
import {
  CommunicationOptions,
  FullCommunicationPreference
} from '../../communication-preferences-tab.enum';
import { getValidTypePhoneNumbers } from '../../communication-preferences-tab.util';
import {
  hasDirectCorrespondenceAllowedNumber,
  hasNotificationOfUpdatesAllowedNumber,
  hasWrittenCorrespondenceAllowedNumber,
  isLandline,
  validateSelectedContactIndex
} from '../../communication-preferences.form';

export const getSelectedEmailAddressIndex = (
  emailAddresses: GroupClientEmailAddress[],
  communicationPreferences: FullCommunicationPreference
) => {
  let selectedEmailAddress: string | null;
  const { notificationOfUpdates, writtenCorrespondence } =
    communicationPreferences || {};

  if (!_isEmpty(notificationOfUpdates?.emailAddresses)) {
    selectedEmailAddress = notificationOfUpdates!.emailAddresses[0]
      .emailAddress;
  } else if (!_isEmpty(writtenCorrespondence?.emailAddresses)) {
    selectedEmailAddress = writtenCorrespondence!.emailAddresses[0]
      .emailAddress;
  } else {
    selectedEmailAddress = null;
  }

  if (emailAddresses?.length === 1) {
    return 0;
  }

  return emailAddresses?.findIndex(
    address => address.emailAddress === selectedEmailAddress
  );
};

export const getSelectedPhoneNumberIndex = (
  phoneNumbers: GroupClientPhoneNumber[],
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
) => {
  const { directCorrespondence, notificationOfUpdates, writtenCorrespondence } =
    communicationPreferences || {};
  const hasWrittenCorrespondenceNumber = hasWrittenCorrespondenceAllowedNumber(
    communicationPreferences,
    allowedPhoneTypes
  );
  const hasNotificationOfUpdatesNumber = hasNotificationOfUpdatesAllowedNumber(
    communicationPreferences,
    allowedPhoneTypes
  );
  const hasDirectCorrespondenceNumber = hasDirectCorrespondenceAllowedNumber(
    communicationPreferences,
    allowedPhoneTypes
  );
  const validTypePhoneNumbers = getValidTypePhoneNumbers(
    allowedPhoneTypes,
    phoneNumbers
  );

  // select phone number if there is only one
  if (phoneNumbers?.length === 1) {
    return 0;
  }

  if (!hasNotificationOfUpdatesNumber) {
    // if both contexts have number selected and both of them are mobile
    if (
      hasDirectCorrespondenceNumber &&
      hasWrittenCorrespondenceNumber &&
      !isLandline(
        communicationPreferences.directCorrespondence!.phoneNumbers[0]
      ) &&
      !isLandline(
        communicationPreferences.writtenCorrespondence!.phoneNumbers[0]
      )
    ) {
      return -1;
    }

    // if has only direct correspondence mobile number
    if (
      hasDirectCorrespondenceNumber &&
      !hasWrittenCorrespondenceNumber &&
      !isLandline(directCorrespondence!.phoneNumbers[0])
    ) {
      return validTypePhoneNumbers?.findIndex(
        phoneNumber =>
          phoneNumber.id === directCorrespondence?.phoneNumbers[0].id
      );
    }

    // if has only writtenCorrespondence number or directCorrespondence is landline
    if (
      hasWrittenCorrespondenceNumber &&
      (!hasDirectCorrespondenceNumber ||
        isLandline(directCorrespondence!.phoneNumbers[0]))
    ) {
      return validTypePhoneNumbers.findIndex(
        phoneNumber =>
          phoneNumber.id === writtenCorrespondence?.phoneNumbers[0].id
      );
    }

    return -1;
  }

  return validTypePhoneNumbers?.findIndex(
    phoneNumber => phoneNumber.id === notificationOfUpdates?.phoneNumbers[0]?.id
  );
};

const getSelectedCommunicationTypes = (
  communicationPreferences: FullCommunicationPreference
) => {
  const selectedCommunicationTypes = [];

  if (
    !_isEmpty(communicationPreferences?.notificationOfUpdates?.emailAddresses)
  ) {
    selectedCommunicationTypes.push(CommunicationOptions.EMAIL);
  }

  if (
    !_isEmpty(communicationPreferences?.notificationOfUpdates?.phoneNumbers)
  ) {
    selectedCommunicationTypes.push(CommunicationOptions.SMS);
  }

  return selectedCommunicationTypes;
};

export const notificationOfUpdatesInitialValue = (
  emailAddresses: GroupClientEmailAddress[],
  phoneNumbers: GroupClientPhoneNumber[],
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
) => {
  return {
    communicationType: getSelectedCommunicationTypes(communicationPreferences),
    emailAddress: '',
    newPhoneType: null,
    newPhoneNumber: initialPhoneInputValue,
    selectedEmailAddressIndex: getSelectedEmailAddressIndex(
      emailAddresses,
      communicationPreferences
    ),
    selectedPhoneNumberIndex: getSelectedPhoneNumberIndex(
      phoneNumbers,
      communicationPreferences,
      allowedPhoneTypes
    )
  };
};

export const notificationOfUpdatesValidation = (
  phoneNumbers: GroupClientPhoneNumber[],
  emailAddresses: GroupClientEmailAddress[]
) =>
  Yup.object({
    communicationType: Yup.array().min(
      1,
      'FINEOS_COMMON.VALIDATION.REQUIRED_AT_LEAST_ONE_OPTION'
    ),
    emailAddress: Yup.string().when('communicationType', {
      is: communicationType =>
        !!communicationType.includes(CommunicationOptions.EMAIL) &&
        _isEmpty(emailAddresses),
      then: newEmailValidationSchema
        .clone()
        .required('FINEOS_COMMON.VALIDATION.REQUIRED')
    }),
    newPhoneType: Yup.mixed(),
    newPhoneNumber: Yup.object().when('communicationType', {
      is: communicationType =>
        !!communicationType.includes(CommunicationOptions.SMS) &&
        _isEmpty(phoneNumbers),
      then: phoneInputValidation.clone()
    }),
    selectedPhoneNumberIndex: Yup.number().when('communicationType', {
      is: communicationType =>
        !!communicationType.includes(CommunicationOptions.SMS),
      then: validateSelectedContactIndex(phoneNumbers, 'PHONE')
    }),
    selectedEmailAddressIndex: Yup.number().when('communicationType', {
      is: communicationType =>
        !!communicationType.includes(CommunicationOptions.EMAIL) &&
        !_isEmpty(emailAddresses),
      then: validateSelectedContactIndex(emailAddresses, 'EMAIL')
    })
  });
