import React from 'react';
import { noop as _noop } from 'lodash';

import {
  FullCommunicationPreference,
  PreferencesContext
} from './communication-preferences-tab.enum';

export const CommunicationPreferencesContext = React.createContext<
  PreferencesContext
>({
  emailAddresses: [],
  phoneNumbers: [],
  communicationPreferences: {} as FullCommunicationPreference,
  onCommunicationPreferencesEdit: _noop,
  onEmailAdd: _noop,
  onPhoneNumberAdd: _noop
});
