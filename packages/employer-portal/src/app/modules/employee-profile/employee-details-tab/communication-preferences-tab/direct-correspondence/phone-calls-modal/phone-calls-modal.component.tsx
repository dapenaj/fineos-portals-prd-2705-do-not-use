import React, { useContext, useState } from 'react';
import { Formik } from 'formik';
import { isEmpty as _isEmpty } from 'lodash';
import {
  AsteriskInfo,
  FormButton,
  UiButton,
  ButtonType,
  Form,
  FormattedMessage,
  ModalFooter,
  useErrorNotificationPopup,
  Modal
} from 'fineos-common';
import {
  GroupClientPhoneNumber,
  GroupClientPhoneNumberResource
} from 'fineos-js-api-client';

import { addPhoneArray } from '../../../../../../shared';
import { changeCommunicationPreferences } from '../../communication-preferences-tab.api';
import { SelectPhoneNumber } from '../../select-phone-number/select-phone-number.component';
import { AddPhoneNumberForm } from '../../add-phone-number-form/add-phone-number-form.component';
import communicationPreferencesStyles from '../../communication-preferences-tab.module.scss';
import { CommunicationPreferencesContext } from '../../communication-preferences-tab.context';
import { mapPhoneNumberToApiFormat } from '../../communication-preferences-tab.util';
import { CommunicationPreferencesType } from '../../communication-preferences.constant';

import { PhoneCallsFormState } from './phone-calls-modal.type';
import {
  newPhoneNumberInitialValues,
  newPhoneNumberValidation
} from './phone-calls-modal.form';

type PhoneCallsModalProps = {
  employeeId: string;
  onModalClose: () => void;
  isVisible: boolean;
};

export const PhoneCallsModal: React.FC<PhoneCallsModalProps> = ({
  employeeId,
  onModalClose,
  isVisible
}) => {
  const { showErrorNotification } = useErrorNotificationPopup();
  const [passedNumbers, setPassedNumbers] = useState<
    GroupClientPhoneNumberResource[]
  >([]);
  const {
    communicationPreferences,
    onCommunicationPreferencesEdit,
    phoneNumbers,
    onPhoneNumberAdd
  } = useContext(CommunicationPreferencesContext);

  const selectedPhoneNumberIndex = communicationPreferences
    ?.directCorrespondence?.phoneNumbers?.length
    ? phoneNumbers?.findIndex(
        phoneNumber =>
          phoneNumber.id ===
          communicationPreferences.directCorrespondence!.phoneNumbers[0]?.id
      )
    : !!phoneNumbers?.length && phoneNumbers?.length === 1
    ? 0
    : -1;

  const sendAddedPhoneNumbers = (
    phoneNumbersToAdd: GroupClientPhoneNumberResource[]
  ) => {
    return addPhoneArray(employeeId, phoneNumbersToAdd)
      .then((phoneNumbersResponse: GroupClientPhoneNumber[]) => {
        onPhoneNumberAdd(phoneNumbersResponse);
        sendChangeCommunicationPreferences(phoneNumbersResponse[0]);
      })
      .catch(showErrorNotification);
  };

  const sendChangeCommunicationPreferences = (
    phoneNumber: GroupClientPhoneNumber
  ) => {
    const changePreferencesData = {
      id: phoneNumber.id,
      resource: 'PhoneNumber',
      relName: ''
    };
    return changeCommunicationPreferences(
      employeeId,
      CommunicationPreferencesType.DIRECT_CORRESPONDENCE_ID,
      changePreferencesData
    )
      .then(data => {
        onCommunicationPreferencesEdit({
          ...communicationPreferences,
          directCorrespondence: {
            ...(communicationPreferences.directCorrespondence! || {}),
            phoneNumbers: data.phoneNumbers
          }
        });
        onModalClose();
      })
      .catch(showErrorNotification);
  };

  const submitAddedNumber = (formData: PhoneCallsFormState) => {
    if (!!formData.newPhoneNumber.telephoneNo && _isEmpty(passedNumbers)) {
      const mappedPhoneNumber = mapPhoneNumberToApiFormat(formData);
      sendAddedPhoneNumbers([mappedPhoneNumber]);
      return;
    }
    if (!_isEmpty(passedNumbers)) {
      sendAddedPhoneNumbers(passedNumbers);
      setPassedNumbers([]);
    } else {
      sendChangeCommunicationPreferences(
        phoneNumbers[formData.selectedPhoneNumberIndex]
      );
    }
  };

  return (
    <Modal
      ariaLabelId="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.TITLE"
      visible={isVisible}
      header={
        <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.TITLE" />
      }
      onCancel={onModalClose}
    >
      <Formik
        onSubmit={submitAddedNumber}
        validateOnMount={true}
        initialValues={newPhoneNumberInitialValues(selectedPhoneNumberIndex)}
        validationSchema={newPhoneNumberValidation(phoneNumbers)}
      >
        {({ setFieldValue, values, setFieldTouched }) => (
          <Form>
            <AsteriskInfo />
            {_isEmpty(phoneNumbers) || values.newPhoneNumber.telephoneNo ? (
              <AddPhoneNumberForm />
            ) : (
              <div className={communicationPreferencesStyles.modalContent}>
                <SelectPhoneNumber
                  setFieldTouched={setFieldTouched}
                  onPhoneNumberPass={(
                    phoneNumber: GroupClientPhoneNumberResource
                  ) => {
                    setPassedNumbers([phoneNumber, ...passedNumbers]);
                    setFieldValue('selectedPhoneNumberIndex', 0);
                  }}
                />
              </div>
            )}
            <ModalFooter>
              <FormattedMessage
                as={FormButton}
                htmlType="submit"
                shouldSubmitForm={false}
                id="FINEOS_COMMON.GENERAL.OK"
              />
              <FormattedMessage
                as={UiButton}
                buttonType={ButtonType.LINK}
                onClick={onModalClose}
                id="FINEOS_COMMON.GENERAL.CANCEL"
              />
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};
