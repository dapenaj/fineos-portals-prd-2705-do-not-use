/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Card, createThemedStyles, textStyleToCss } from 'fineos-common';
import classNames from 'classnames';

import styles from './preference.module.scss';

export const usePreferenceThemedStyles = createThemedStyles(theme => ({
  title: {
    ...textStyleToCss(theme.typography.panelTitle),
    color: theme.colorSchema.neutral8
  },
  body: {
    color: theme.colorSchema.neutral8
  },
  highlight: {
    ...textStyleToCss(theme.typography.baseTextSemiBold),
    color: theme.colorSchema.primaryColor
  }
}));

type PreferenceProps = {
  title: React.ReactNode;
  action: React.ReactNode;
  'data-test-el'?: string;
};

export const Preference: React.FC<PreferenceProps> = ({
  title,
  action,
  children,
  ...props
}) => {
  const themedStyles = usePreferenceThemedStyles();

  return (
    <Card
      className={styles.preference}
      data-test-el={props['data-test-el'] || 'preference-card'}
    >
      <h3
        className={classNames(themedStyles.title, styles.title)}
        data-test-el="preference-card-title"
      >
        {title}
      </h3>

      <div className={classNames(themedStyles.body, styles.body)}>
        {children}
      </div>
      <div className={styles.action}>{action}</div>
    </Card>
  );
};
