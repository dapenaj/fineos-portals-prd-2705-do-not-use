/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useMemo, useContext } from 'react';
import {
  Button,
  ButtonType,
  FormattedMessage,
  usePermissions,
  ManageCustomerDataPermissions,
  ViewCustomerDataPermissions
} from 'fineos-common';

import { formatPhoneForDisplay } from '../../../../../shared';
import { Preference, usePreferenceThemedStyles } from '../preference';
import {
  getValidEmail,
  getValidPhone
} from '../communication-preferences-tab.util';
import { CommunicationPreferencesContext } from '../communication-preferences-tab.context';

import { NotificationOfUpdatesModal } from './notification-of-updates-modal/notification-of-updates-modal.component';
import styles from './notification-of-updates.module.scss';

type NotificationOfUpdatesProps = {
  employeeId: string;
};

export const NotificationOfUpdates: React.FC<NotificationOfUpdatesProps> = ({
  employeeId
}) => {
  const [isUpdatesModalVisible, setIsUpdatesModalVisible] = useState<boolean>(
    false
  );
  const { communicationPreferences } = useContext(
    CommunicationPreferencesContext
  );

  const email = getValidEmail(communicationPreferences?.notificationOfUpdates);
  const phone = getValidPhone(communicationPreferences?.notificationOfUpdates);

  const hasManageCommunicationPreferencesPermissions = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES
  );

  const hasViewPhoneNumbersPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
  );

  const hasViewEmailsPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_EMAIL_ADDRESSES
  );

  const hasBasePermissions = useMemo(
    () =>
      hasManageCommunicationPreferencesPermissions &&
      hasViewPhoneNumbersPermission &&
      hasViewEmailsPermission,
    [
      hasManageCommunicationPreferencesPermissions,
      hasViewEmailsPermission,
      hasViewPhoneNumbersPermission
    ]
  );

  const isActionButtonVisible = useMemo(
    () =>
      hasManageCommunicationPreferencesPermissions &&
      hasViewPhoneNumbersPermission &&
      hasViewEmailsPermission,
    [
      hasManageCommunicationPreferencesPermissions,
      hasViewPhoneNumbersPermission,
      hasViewEmailsPermission
    ]
  );

  const themedStyles = usePreferenceThemedStyles();
  return (
    <>
      {hasBasePermissions && (
        <Preference
          data-test-el="notification-of-updates-preference"
          title={
            <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.TITLE" />
          }
          action={
            isActionButtonVisible && (
              <FormattedMessage
                as={Button}
                buttonType={ButtonType.LINK}
                onClick={() => setIsUpdatesModalVisible(true)}
                id={
                  email || phone
                    ? 'PROFILE.COMMUNICATION_PREFERENCES_TAB.EDIT'
                    : 'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.RECEIVE'
                }
              />
            )
          }
        >
          <>
            {hasBasePermissions && email && (
              <div className={styles.email}>
                <FormattedMessage
                  id="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.MESSAGE"
                  values={{
                    label: (
                      <FormattedMessage
                        className={themedStyles.highlight}
                        as="strong"
                        id="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL"
                      />
                    ),
                    value: (
                      <strong className={themedStyles.highlight}>
                        {email.emailAddress}
                      </strong>
                    )
                  }}
                />
              </div>
            )}

            {hasBasePermissions && phone && (
              <div>
                <FormattedMessage
                  id="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.MESSAGE"
                  values={{
                    label: (
                      <FormattedMessage
                        className={themedStyles.highlight}
                        as="strong"
                        id="PROFILE.COMMUNICATION_PREFERENCES_TAB.SMS"
                      />
                    ),
                    value: (
                      <strong className={themedStyles.highlight}>
                        {formatPhoneForDisplay(phone)}
                      </strong>
                    )
                  }}
                />
              </div>
            )}

            {!phone && !email && (
              <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATION_OF_UPDATES.EMPTY" />
            )}
          </>
        </Preference>
      )}

      <NotificationOfUpdatesModal
        isVisible={isUpdatesModalVisible}
        onModalClose={() => setIsUpdatesModalVisible(false)}
        employeeId={employeeId}
      />
    </>
  );
};
