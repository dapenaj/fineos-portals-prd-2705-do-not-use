import * as Yup from 'yup';
import { isEmpty as _isEmpty } from 'lodash';
import {
  GroupClientPhoneNumber,
  GroupClientEmailAddress
} from 'fineos-js-api-client';

import { PhoneType } from '../../../../shared';

import { FullCommunicationPreference } from './communication-preferences-tab.enum';

export const validateSelectedContactIndex = (
  contactList: GroupClientPhoneNumber[] | GroupClientEmailAddress[],
  communicationType: string
) => {
  if (_isEmpty(contactList)) {
    return Yup.number().nullable();
  }
  return Yup.number().min(
    0,
    `PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.VALIDATION.${communicationType}`
  );
};

const hasContactMethod = (phoneTypes: PhoneType[], contactMethodName: string) =>
  phoneTypes.includes(contactMethodName as PhoneType);

export const hasWrittenCorrespondenceAllowedNumber = (
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
) =>
  !_isEmpty(
    communicationPreferences?.writtenCorrespondence?.phoneNumbers?.filter(
      ({ contactMethod }) =>
        hasContactMethod(allowedPhoneTypes, contactMethod?.name)
    )
  );

export const hasNotificationOfUpdatesAllowedNumber = (
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
) =>
  !_isEmpty(
    communicationPreferences?.notificationOfUpdates?.phoneNumbers?.filter(
      ({ contactMethod }) =>
        hasContactMethod(allowedPhoneTypes, contactMethod?.name)
    )
  );

export const hasDirectCorrespondenceAllowedNumber = (
  communicationPreferences: FullCommunicationPreference,
  allowedPhoneTypes: PhoneType[]
) =>
  !_isEmpty(
    communicationPreferences?.directCorrespondence?.phoneNumbers?.filter(
      ({ contactMethod }) =>
        hasContactMethod(allowedPhoneTypes, contactMethod?.name)
    )
  );

export const isLandline = (phoneNumber: GroupClientPhoneNumber) =>
  phoneNumber.contactMethod?.name === PhoneType.PHONE;
