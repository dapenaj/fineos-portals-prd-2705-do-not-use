import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber,
  CommunicationPreference
} from 'fineos-js-api-client';

import { changeCommunicationPreferences } from '../../communication-preferences-tab.api';
import { CommunicationPreferencesType } from '../../communication-preferences.constant';
import { ResourceType } from '../../communication-preferences-tab.enum';

const getPreferenceChangeBody = (id: string, resource: ResourceType) => ({
  id,
  resource,
  relName: ''
});

export const changeNotificationOfUpdatesPreferences = async (
  customerId: string,
  communicationPreferenceId: CommunicationPreferencesType,
  selectedEmail: GroupClientEmailAddress | null,
  selectedPhoneNumber: GroupClientPhoneNumber | null
) => {
  const requests: CommunicationPreference[] = [];
  if (!selectedEmail) {
    // Unlink email address from communication preferences
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        getPreferenceChangeBody('', ResourceType.EMAIL)
      )
    );
  } else {
    // Link email address to communication preferences
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        getPreferenceChangeBody(selectedEmail!.id, ResourceType.EMAIL)
      )
    );
  }

  if (!selectedPhoneNumber) {
    // Unlink phone number from communication preferences
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        getPreferenceChangeBody('', ResourceType.PHONE_NUMBER)
      )
    );
  } else {
    // Link phone number to communication preferences
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        getPreferenceChangeBody(
          selectedPhoneNumber!.id,
          ResourceType.PHONE_NUMBER
        )
      )
    );
  }
  return requests;
};
