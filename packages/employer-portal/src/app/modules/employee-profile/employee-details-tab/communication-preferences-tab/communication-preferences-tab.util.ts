/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  CommunicationPreference,
  GroupClientPhoneNumber,
  GroupClientEmailAddress,
  GroupClientPhoneNumberResource
} from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import { PhoneType } from '../../../../shared';

import { PhoneCallsFormState } from './direct-correspondence/phone-calls-modal/phone-calls-modal.type';
import { NotificationOfUpdatesFormState } from './notification-of-updates/notification-of-updates-modal/notification-of-updates-modal.type';
import { WrittenCorrespondenceFormState } from './written-correspondence/written-correspondence-modal/written-correspondence-modal.type';

export const getValidPhone = (
  preference?: CommunicationPreference
): GroupClientPhoneNumber | undefined =>
  preference && !_isEmpty(preference.phoneNumbers)
    ? preference.phoneNumbers[0]
    : undefined;

export const getValidMobile = (
  preference?: CommunicationPreference
): GroupClientPhoneNumber | undefined =>
  preference && !_isEmpty(preference.phoneNumbers)
    ? preference.phoneNumbers.find(
        phone =>
          phone.contactMethod &&
          (phone.contactMethod.name === PhoneType.MOBILE ||
            phone.contactMethod.name === PhoneType.CELL)
      )
    : undefined;

export const getValidEmail = (
  preference?: CommunicationPreference
): GroupClientEmailAddress | undefined =>
  preference && !_isEmpty(preference.emailAddresses)
    ? preference.emailAddresses[0]
    : undefined;

export const mapPhoneNumberToApiFormat = (
  formData:
    | PhoneCallsFormState
    | NotificationOfUpdatesFormState
    | WrittenCorrespondenceFormState
): GroupClientPhoneNumberResource => ({
  contactMethod: formData.newPhoneType!,
  areaCode: formData.newPhoneNumber.areaCode,
  extension: '',
  intCode: formData.newPhoneNumber.intCode,
  telephoneNo: formData.newPhoneNumber.telephoneNo,
  exDirectory: true
});

export const getValidTypePhoneNumbers = (
  allowedPhoneTypes: PhoneType[],
  phoneNumbers: GroupClientPhoneNumber[]
) =>
  allowedPhoneTypes?.length
    ? phoneNumbers?.filter(phoneNumber =>
        allowedPhoneTypes.includes(phoneNumber.contactMethod.name as PhoneType)
      )
    : phoneNumbers;
