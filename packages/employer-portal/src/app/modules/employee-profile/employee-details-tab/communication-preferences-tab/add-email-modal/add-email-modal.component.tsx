import React from 'react';
import { Formik } from 'formik';
import {
  AsteriskInfo,
  Modal,
  ModalFooter,
  FormattedMessage,
  FormButton,
  UiButton,
  ButtonType,
  Form
} from 'fineos-common';
import { BaseEmailAddress } from 'fineos-js-api-client';

import { AddEmailForm } from '../add-email-form/add-email-form.component';
import communicationPreferencesStyles from '../communication-preferences-tab.module.scss';

import { newEmailValidationSchema } from './add-email-modal.form';

type AddEmailModalProps = {
  isVisible: boolean;
  onEmailPass: (email: BaseEmailAddress) => void;
  onModalClose: () => void;
};

export const AddEmailModal: React.FC<AddEmailModalProps> = ({
  isVisible,
  onEmailPass,
  onModalClose
}) => {
  const handleSubmit = (formData: BaseEmailAddress) => {
    onEmailPass(formData);
    onModalClose();
  };
  return (
    <Modal
      ariaLabelId="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.TITLE"
      visible={isVisible}
      data-test-el="add-email-modal"
      onCancel={onModalClose}
      header={
        <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.TITLE" />
      }
    >
      <AsteriskInfo />
      <Formik
        onSubmit={handleSubmit}
        validateOnMount={true}
        initialValues={{
          emailAddress: ''
        }}
        validationSchema={newEmailValidationSchema}
      >
        <Form className={communicationPreferencesStyles.addElementForm}>
          <AddEmailForm />
          <ModalFooter>
            <FormattedMessage
              as={FormButton}
              htmlType="submit"
              shouldSubmitForm={false}
              id="FINEOS_COMMON.GENERAL.OK"
            />
            <FormattedMessage
              as={UiButton}
              buttonType={ButtonType.LINK}
              onClick={onModalClose}
              id="FINEOS_COMMON.GENERAL.CANCEL"
            />
          </ModalFooter>
        </Form>
      </Formik>
    </Modal>
  );
};
