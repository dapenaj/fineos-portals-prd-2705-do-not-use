/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useContext, useEffect } from 'react';
import { Field, useFormikContext } from 'formik';
import classNames from 'classnames';
import { GroupClientPhoneNumberResource } from 'fineos-js-api-client';
import {
  FormattedMessage,
  Button,
  ButtonType,
  createThemedStyles,
  primaryLight2Color,
  Card,
  textStyleToCss,
  usePermissions,
  ManageCustomerDataPermissions,
  FormRadioInput,
  FormGroup,
  Radio
} from 'fineos-common';

import {
  formatPhoneForDisplay,
  PhoneType,
  withAutoScroll
} from '../../../../../shared';
import { AddPhoneNumberModal } from '../add-phone-number-modal/add-phone-number-modal.component';
import { CommunicationPreferencesContext } from '../communication-preferences-tab.context';
import { getValidTypePhoneNumbers } from '../communication-preferences-tab.util';
import { PhoneCallsFormState } from '../direct-correspondence/phone-calls-modal/phone-calls-modal.type';
import { NotificationOfUpdatesFormState } from '../notification-of-updates/notification-of-updates-modal';
import { WrittenCorrespondenceFormState } from '../written-correspondence/written-correspondence-modal';
import communicationPreferencesStyles from '../communication-preferences-tab.module.scss';

import styles from './select-phone-number.module.scss';

type SelectPhoneNumberProps = {
  onPhoneNumberPass: (phoneNumber: GroupClientPhoneNumberResource) => void;
  allowedPhoneTypeNames?: PhoneType[];
  setFieldTouched?: (
    field: string,
    isTouched?: boolean | undefined,
    shouldValidate?: boolean | undefined
  ) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  selectPhoneNumberLabel: {
    ...textStyleToCss(theme.typography.baseText)
  },
  hasHighlight: {
    background: primaryLight2Color(theme.colorSchema)
  }
}));

const CardWithAutoScroll = withAutoScroll(Card);

export const SelectPhoneNumber: React.FC<SelectPhoneNumberProps> = ({
  onPhoneNumberPass,
  allowedPhoneTypeNames = [],
  setFieldTouched
}) => {
  const [isAddNewNumberVisible, setIsAddNewNumberVisible] = useState<boolean>(
    false
  );
  const [temporaryNumberList, setTemporaryNumberList] = useState<
    GroupClientPhoneNumberResource[]
  >([]);
  const [
    recentlyAddedPhone,
    setRecentlyAddedPhone
  ] = useState<GroupClientPhoneNumberResource | null>(null);

  const { phoneNumbers } = useContext(CommunicationPreferencesContext);

  const hasAddNewNumberPermission = usePermissions(
    ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS
  );

  const formikContext = useFormikContext<
    | PhoneCallsFormState
    | NotificationOfUpdatesFormState
    | WrittenCorrespondenceFormState
  >();

  useEffect(() => {
    const validTypePhoneNumbers = getValidTypePhoneNumbers(
      allowedPhoneTypeNames,
      phoneNumbers
    );
    setTemporaryNumberList(validTypePhoneNumbers);
    // eslint-disable-next-line
  }, [phoneNumbers]);

  const shouldAutoscroll = (
    phoneList: GroupClientPhoneNumberResource[],
    recentPhone: GroupClientPhoneNumberResource | null,
    index: number
  ) => {
    const stringifiedNumber = (phone: GroupClientPhoneNumberResource) =>
      `${phone.intCode}-${phone.areaCode}-${phone.telephoneNo}`;
    if (!recentPhone) {
      return false;
    }
    return (
      phoneList.findIndex(
        phone => stringifiedNumber(phone) === stringifiedNumber(recentPhone)
      ) === 0 && index === 0
    );
  };

  const themedStyles = useThemedStyles();

  const phoneNumberFieldName = 'selectedPhoneNumberIndex';

  return (
    <>
      <div
        className={styles.selectPhoneNumber}
        data-test-el="select-phone-number"
      >
        <div
          className={classNames(
            communicationPreferencesStyles.contentLabel,
            themedStyles.selectPhoneNumberLabel
          )}
        />
        <FormGroup
          isRequired={true}
          className={communicationPreferencesStyles.selectContactItemForm}
          label={
            <div
              className={classNames(
                communicationPreferencesStyles.contentLabel,
                themedStyles.selectPhoneNumberLabel
              )}
            >
              <FormattedMessage
                id="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL"
                values={{
                  actionType: 'Select'
                }}
              />
            </div>
          }
        >
          <Field
            onBlur={() =>
              setFieldTouched &&
              setFieldTouched(phoneNumberFieldName, true, true)
            }
            name={phoneNumberFieldName}
            component={FormRadioInput}
            className={communicationPreferencesStyles.overflowWrapper}
            value={formikContext.values.selectedPhoneNumberIndex || 0}
            wrapperComponent={(index: number) => {
              const TargetCard = shouldAutoscroll(
                temporaryNumberList,
                recentlyAddedPhone,
                index
              )
                ? CardWithAutoScroll
                : Card;
              return (
                <TargetCard
                  key={temporaryNumberList.length - index}
                  // Prop below was added for e2e testing purposes
                  data-test-el={
                    formikContext.values.selectedPhoneNumberIndex === index
                      ? 'selected-phone-number-card'
                      : 'phone-number-card'
                  }
                  className={classNames(styles.phoneNumberCard, {
                    [themedStyles.hasHighlight]:
                      formikContext.values.selectedPhoneNumberIndex === index
                  })}
                />
              );
            }}
          >
            {temporaryNumberList!.map((phoneNumber, index) => (
              <Radio
                value={index}
                key={temporaryNumberList.length - index}
                className={styles.radioOption}
              >
                {formatPhoneForDisplay(phoneNumber)}
              </Radio>
            ))}
          </Field>
        </FormGroup>
        {hasAddNewNumberPermission && (
          <FormattedMessage
            as={Button}
            className={
              communicationPreferencesStyles.anotherCommunicationButton
            }
            buttonType={ButtonType.LINK}
            onClick={() => setIsAddNewNumberVisible(true)}
            id="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON"
          />
        )}
      </div>

      <AddPhoneNumberModal
        allowedPhoneTypeNames={allowedPhoneTypeNames}
        isVisible={isAddNewNumberVisible}
        onPhoneNumberPass={(phone: GroupClientPhoneNumberResource) => {
          onPhoneNumberPass(phone);
          setRecentlyAddedPhone(phone);
          setTemporaryNumberList([phone, ...temporaryNumberList!]);
        }}
        onModalClose={() => setIsAddNewNumberVisible(false)}
      />
    </>
  );
};
