/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientCustomerService,
  LinkRequest,
  GroupClientPhoneNumberResource,
  BaseEmailAddress
} from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import {
  fetchCustomerCommunicationPreferences,
  addPhoneArray,
  addEmailArray
} from '../../../../shared';

import { CommunicationPreferencesType } from './communication-preferences.constant';

export const fetchCommunicationPreferences = async (customerId: string) => {
  const preferences = await fetchCustomerCommunicationPreferences(customerId);

  const directCorrespondence = preferences.find(
    ({ id }) => id === CommunicationPreferencesType.DIRECT_CORRESPONDENCE_ID
  );

  const notificationOfUpdates = preferences.find(
    ({ id }) => id === CommunicationPreferencesType.NOTIFICATION_OF_UPDATES_ID
  );

  const writtenCorrespondence = preferences.find(
    ({ id }) => id === CommunicationPreferencesType.WRITTEN_CORRESPONDENCE_ID
  );

  return {
    directCorrespondence,
    notificationOfUpdates,
    writtenCorrespondence
  };
};

export const changeCommunicationPreferences = (
  customerId: string,
  communicationPreferenceId: CommunicationPreferencesType,
  body: LinkRequest
) =>
  GroupClientCustomerService.getInstance().linkCustomerCommunicationPreference(
    customerId,
    communicationPreferenceId,
    body
  );

export const sendSelectedCommunicationTypes = async (
  employeeId: string,
  phoneNumbers: GroupClientPhoneNumberResource[] | null,
  emailAddresses: BaseEmailAddress[] | null
) => {
  const requests = {} as any;
  if (!_isEmpty(phoneNumbers)) {
    requests.addedPhoneNumbers = await addPhoneArray(employeeId, phoneNumbers!);
  }
  if (!_isEmpty(emailAddresses)) {
    requests.addedEmailAddresses = await addEmailArray(
      employeeId,
      emailAddresses!
    );
  }
  return requests;
};
