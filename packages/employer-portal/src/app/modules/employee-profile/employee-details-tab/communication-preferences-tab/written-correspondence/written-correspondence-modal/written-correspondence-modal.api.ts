import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber,
  CommunicationPreference
} from 'fineos-js-api-client';

import { changeCommunicationPreferences } from '../../communication-preferences-tab.api';
import { CommunicationPreferencesType } from '../../communication-preferences.constant';
import { ResourceType } from '../../communication-preferences-tab.enum';

export const changeWrittenCorrespondencePreferences = async (
  customerId: string,
  communicationPreferenceId: CommunicationPreferencesType,
  selectedEmail: GroupClientEmailAddress | null,
  selectedPhoneNumber: GroupClientPhoneNumber | null
) => {
  const requests: CommunicationPreference[] = [];
  const preferenceData = (id: string, resource: ResourceType) => ({
    id,
    resource,
    relName: ''
  });

  if (!selectedEmail && !selectedPhoneNumber) {
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        preferenceData('', ResourceType.EMAIL)
      )
    );
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        preferenceData('', ResourceType.PHONE_NUMBER)
      )
    );
  }

  if (selectedEmail) {
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        preferenceData(selectedEmail!.id, ResourceType.EMAIL)
      )
    );
  }

  if (selectedPhoneNumber) {
    requests.push(
      await changeCommunicationPreferences(
        customerId,
        communicationPreferenceId,
        preferenceData(selectedPhoneNumber!.id, ResourceType.PHONE_NUMBER)
      )
    );
  }

  return requests;
};
