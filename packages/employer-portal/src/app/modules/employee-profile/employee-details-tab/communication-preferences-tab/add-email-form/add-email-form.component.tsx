import React from 'react';
import { Row, Col } from 'antd';
import { Field } from 'formik';
import { FormattedMessage, FormGroup, TextInput } from 'fineos-common';

import communicationPreferencesStyles from '../communication-preferences-tab.module.scss';

export const AddEmailForm: React.FC = () => {
  return (
    <div
      className={communicationPreferencesStyles.addElementForm}
      data-test-el="add-email-form"
    >
      <Row
        justify="space-between"
        className={communicationPreferencesStyles.row}
      >
        <Col xs={24} sm={24}>
          <FormGroup
            isRequired={true}
            showErrorMessage={true}
            label={
              <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.ADD_EMAIL_LABEL" />
            }
          >
            <Field name="emailAddress" component={TextInput} />
          </FormGroup>
        </Col>
      </Row>
    </div>
  );
};
