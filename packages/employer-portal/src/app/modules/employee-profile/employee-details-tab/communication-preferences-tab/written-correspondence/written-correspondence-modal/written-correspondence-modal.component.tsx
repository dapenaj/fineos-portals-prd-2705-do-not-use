import React, { useContext, useState } from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import { Field, Formik } from 'formik';
import classNames from 'classnames';
import {
  FormButton,
  UiButton,
  ButtonType,
  createThemedStyles,
  Form,
  FormattedMessage,
  ModalFooter,
  FormRadioInput,
  FormGroup,
  textStyleToCss,
  useErrorNotificationPopup,
  AsteriskInfo,
  Radio,
  Modal
} from 'fineos-common';
import {
  BaseEmailAddress,
  GroupClientEmailAddress,
  GroupClientPhoneNumber,
  GroupClientPhoneNumberResource
} from 'fineos-js-api-client';

import {
  CommunicationOptions,
  WrittenCorrespondenceType
} from '../../communication-preferences-tab.enum';
import { sendSelectedCommunicationTypes } from '../../communication-preferences-tab.api';
import { AddEmailForm } from '../../add-email-form/add-email-form.component';
import { AddPhoneNumberForm } from '../../add-phone-number-form/add-phone-number-form.component';
import { SelectEmail } from '../../select-email/select-email.component';
import { SelectPhoneNumber } from '../../select-phone-number/select-phone-number.component';
import {
  mapPhoneNumberToApiFormat,
  getValidTypePhoneNumbers
} from '../../communication-preferences-tab.util';
import { CommunicationPreferencesContext } from '../../communication-preferences-tab.context';
import { CommunicationPreferencesType } from '../../communication-preferences.constant';
import { PhoneType } from '../../../../../../shared';

import { changeWrittenCorrespondencePreferences } from './written-correspondence-modal.api';
import { WrittenCorrespondenceFormState } from './written-correspondence-modal.type';
import {
  writtenCorrespondeceInitialValue,
  writtenCorrespondeceValidation,
  getSelectedEmailAddressIndex,
  getSelectedPhoneNumberIndex
} from './written-correspondence-modal.form';
import styles from './written-correspondence-modal.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  textLabel: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

const ALLOWED_PHONE_TYPES = [PhoneType.CELL];

type WrittenCorrespondenceModalProps = {
  employeeId: string;
  onModalClose: () => void;
  isVisible: boolean;
};

export const WrittenCorrespondenceModal: React.FC<WrittenCorrespondenceModalProps> = ({
  employeeId,
  onModalClose,
  isVisible
}) => {
  const [passedEmails, setPassedEmails] = useState<BaseEmailAddress[]>([]);
  const [passedPhoneNumbers, setPassedPhoneNumbers] = useState<
    GroupClientPhoneNumberResource[]
  >([]);

  const { showErrorNotification } = useErrorNotificationPopup();
  const {
    communicationPreferences,
    phoneNumbers,
    emailAddresses,
    onPhoneNumberAdd,
    onEmailAdd,
    onCommunicationPreferencesEdit
  } = useContext(CommunicationPreferencesContext);

  const addSelectedCommunicationTypes = (
    phones: GroupClientPhoneNumberResource[],
    emails: BaseEmailAddress[]
  ) =>
    sendSelectedCommunicationTypes(employeeId, phones, emails)
      .then(data => {
        if (!_isEmpty(data.addedPhoneNumbers)) {
          onPhoneNumberAdd(data.addedPhoneNumbers);
          setPassedPhoneNumbers([]);
        }
        if (!_isEmpty(data.addedEmailAddresses)) {
          onEmailAdd(data.addedEmailAddresses);
          setPassedEmails([]);
        }
        changeCommunicationPreferences(
          data.addedEmailAddresses,
          data.addedPhoneNumbers
        );
      })
      .catch(showErrorNotification);

  const changeCommunicationPreferences = (
    addedEmailAddresses: GroupClientEmailAddress[],
    addedPhoneNumbers: GroupClientPhoneNumber[]
  ) =>
    changeWrittenCorrespondencePreferences(
      employeeId,
      CommunicationPreferencesType.WRITTEN_CORRESPONDENCE_ID,
      !_isEmpty(addedEmailAddresses) ? addedEmailAddresses[0] : null,
      !_isEmpty(addedPhoneNumbers) ? addedPhoneNumbers[0] : null
    )
      .then(() => {
        onCommunicationPreferencesEdit({
          ...communicationPreferences,
          writtenCorrespondence: {
            ...(communicationPreferences.writtenCorrespondence! || {}),
            emailAddresses: !_isEmpty(addedEmailAddresses)
              ? [addedEmailAddresses[0]]
              : [],
            phoneNumbers: !_isEmpty(addedPhoneNumbers)
              ? [addedPhoneNumbers[0]]
              : []
          }
        });
        onModalClose();
      })
      .catch(showErrorNotification);

  const handleAddNewCommunication = (
    formData: WrittenCorrespondenceFormState
  ) => {
    const mappedPhone =
      formData.portalCommunicationType === CommunicationOptions.SMS
        ? [mapPhoneNumberToApiFormat(formData)]
        : [];
    const mappedEmail =
      formData.portalCommunicationType === CommunicationOptions.EMAIL
        ? [{ emailAddress: formData.emailAddress }]
        : [];
    addSelectedCommunicationTypes(mappedPhone, mappedEmail);
  };

  const handleSubmit = (formData: WrittenCorrespondenceFormState) => {
    if (formData.mainCommunicationType === WrittenCorrespondenceType.PAPER) {
      changeCommunicationPreferences([], []);
      return;
    }
    if (formData.emailAddress || formData.newPhoneNumber.telephoneNo) {
      handleAddNewCommunication(formData);
    } else {
      if (_isEmpty(passedEmails) && _isEmpty(passedPhoneNumbers)) {
        const validPhoneNumbers = getValidTypePhoneNumbers(
          ALLOWED_PHONE_TYPES,
          phoneNumbers
        );
        changeCommunicationPreferences(
          formData.portalCommunicationType === CommunicationOptions.EMAIL
            ? [emailAddresses[formData.selectedEmailAddressIndex]]
            : [],
          formData.portalCommunicationType === CommunicationOptions.SMS
            ? [validPhoneNumbers[formData.selectedPhoneNumberIndex]]
            : []
        );
      } else {
        addSelectedCommunicationTypes(passedPhoneNumbers, passedEmails);
      }
    }
  };

  const themedStyles = useThemedStyles();
  return (
    <Modal
      ariaLabelId="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.TITLE"
      visible={isVisible}
      header={
        <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.TITLE" />
      }
      onCancel={onModalClose}
    >
      <Formik
        onSubmit={handleSubmit}
        validateOnMount={true}
        initialValues={writtenCorrespondeceInitialValue(
          emailAddresses,
          getValidTypePhoneNumbers(ALLOWED_PHONE_TYPES, phoneNumbers),
          communicationPreferences,
          ALLOWED_PHONE_TYPES
        )}
        validationSchema={writtenCorrespondeceValidation(
          emailAddresses,
          getValidTypePhoneNumbers(ALLOWED_PHONE_TYPES, phoneNumbers)
        )}
      >
        {({ values, setFieldValue, setFieldTouched }) => (
          <Form>
            <AsteriskInfo />
            <FormGroup
              isRequired={true}
              className={styles.writtenCorrespondenceRadio}
              label={
                <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_QUESTION" />
              }
            >
              <Field
                component={FormRadioInput}
                size="large"
                name="mainCommunicationType"
              >
                <Radio
                  key={WrittenCorrespondenceType.PAPER}
                  value={WrittenCorrespondenceType.PAPER}
                >
                  <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_PAPER" />
                </Radio>
                <Radio
                  key={WrittenCorrespondenceType.PORTAL}
                  value={WrittenCorrespondenceType.PORTAL}
                >
                  <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_PORTAL" />
                </Radio>
              </Field>
            </FormGroup>

            {values.mainCommunicationType ===
              WrittenCorrespondenceType.PORTAL && (
              <>
                <div
                  className={classNames(
                    themedStyles.textLabel,
                    styles.description
                  )}
                >
                  <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_DESCRIPTION" />
                </div>
                <FormGroup
                  isRequired={true}
                  className={styles.writtenCorrespondenceRadio}
                  label={
                    <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_QUESTION" />
                  }
                >
                  <Field
                    component={FormRadioInput}
                    name="portalCommunicationType"
                    size="large"
                  >
                    <Radio
                      key={CommunicationOptions.EMAIL}
                      value={CommunicationOptions.EMAIL}
                      onChange={() => {
                        setFieldTouched('selectedEmailAddressIndex', false);
                        setFieldValue(
                          'selectedEmailAddressIndex',
                          getSelectedEmailAddressIndex(
                            emailAddresses,
                            communicationPreferences
                          )
                        );
                      }}
                    >
                      <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_EMAIL" />
                    </Radio>
                    <Radio
                      key={CommunicationOptions.SMS}
                      value={CommunicationOptions.SMS}
                      onChange={() => {
                        setFieldTouched('selectedPhoneNumberIndex', false);
                        setFieldValue(
                          'selectedPhoneNumberIndex',
                          getSelectedPhoneNumberIndex(
                            phoneNumbers,
                            communicationPreferences,
                            ALLOWED_PHONE_TYPES
                          )
                        );
                      }}
                    >
                      <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_SMS" />
                    </Radio>
                  </Field>
                </FormGroup>

                {values.portalCommunicationType ===
                  CommunicationOptions.EMAIL && (
                  <>
                    {_isEmpty(emailAddresses) ? (
                      <AddEmailForm />
                    ) : (
                      <SelectEmail
                        onEmailPass={(emailAddress: BaseEmailAddress) => {
                          setPassedEmails([emailAddress, ...passedEmails]);
                          setFieldValue('selectedEmailAddressIndex', 0);
                        }}
                      />
                    )}
                  </>
                )}

                {values.portalCommunicationType ===
                  CommunicationOptions.SMS && (
                  <>
                    {_isEmpty(
                      getValidTypePhoneNumbers(
                        ALLOWED_PHONE_TYPES,
                        phoneNumbers
                      )
                    ) ? (
                      <AddPhoneNumberForm
                        allowedPhoneTypeNames={ALLOWED_PHONE_TYPES}
                      />
                    ) : (
                      <SelectPhoneNumber
                        allowedPhoneTypeNames={ALLOWED_PHONE_TYPES}
                        onPhoneNumberPass={(
                          phoneNumber: GroupClientPhoneNumberResource
                        ) => {
                          setPassedPhoneNumbers([
                            phoneNumber,
                            ...passedPhoneNumbers
                          ]);
                          setFieldValue('selectedPhoneNumberIndex', 0);
                        }}
                      />
                    )}
                  </>
                )}
                <ModalFooter>
                  <FormattedMessage
                    as={FormButton}
                    htmlType="submit"
                    shouldSubmitForm={false}
                    id="FINEOS_COMMON.GENERAL.OK"
                  />
                  <FormattedMessage
                    as={UiButton}
                    buttonType={ButtonType.LINK}
                    onClick={onModalClose}
                    id="FINEOS_COMMON.GENERAL.CANCEL"
                  />
                </ModalFooter>
              </>
            )}

            {values.mainCommunicationType ===
              WrittenCorrespondenceType.PAPER && (
              <ModalFooter>
                <FormattedMessage
                  as={FormButton}
                  htmlType="submit"
                  shouldSubmitForm={false}
                  id="FINEOS_COMMON.GENERAL.OK"
                />
                <FormattedMessage
                  as={UiButton}
                  buttonType={ButtonType.LINK}
                  onClick={onModalClose}
                  id="FINEOS_COMMON.GENERAL.CANCEL"
                />
              </ModalFooter>
            )}
          </Form>
        )}
      </Formik>
    </Modal>
  );
};
