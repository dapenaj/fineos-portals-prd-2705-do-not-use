/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo, useState, useEffect } from 'react';
import {
  Tabs,
  useAsync,
  ContentRenderer,
  FormattedMessage,
  usePermissions,
  ViewCustomerDataPermissions
} from 'fineos-common';
import { isEmpty as _isEmpty } from 'lodash';
import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber
} from 'fineos-js-api-client';
import classNames from 'classnames';

import { TabHeader } from '../tab-header';
import { TabBody } from '../tab-body';
import { fetchPhoneNumbers, fetchEmailAddresses } from '../../../../shared';
import { useEmployeeDetailsTabThemedStyles } from '../employee-details-tab.styles';
import commonStyles from '../employee-details-tab.module.scss';

import { fetchCommunicationPreferences } from './communication-preferences-tab.api';
import { CommunicationPreferencesContext } from './communication-preferences-tab.context';
import { DirectCorrespondence } from './direct-correspondence';
import { NotificationOfUpdates } from './notification-of-updates';
import { WrittenCorrespondence } from './written-correspondence';
import styles from './communication-preferences-tab.module.scss';
import {
  FullCommunicationPreference,
  PreferencesContext
} from './communication-preferences-tab.enum';

type CommunicationPreferencesTabProps = React.ComponentProps<
  typeof Tabs.TabPane
> & {
  employeeId: string;
};

export const CommunicationPreferencesTab: React.FC<CommunicationPreferencesTabProps> = ({
  employeeId,
  ...props
}) => {
  const [emailAddresses, setEmailAddresses] = useState<
    GroupClientEmailAddress[]
  >([]);
  const [phoneNumbers, setPhoneNumbers] = useState<GroupClientPhoneNumber[]>(
    []
  );
  const [
    editedCommunicationPreferences,
    setEditedCommunicationPreferences
  ] = useState<FullCommunicationPreference | null>(null);

  const hasViewPhoneNumbersPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
  );

  const hasViewEmailsPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_EMAIL_ADDRESSES
  );

  const {
    state: { isPending, value: fetchedCommunicationPreferences },
    changeAsyncValue: refreshCommunicationPreferencesInMemory
  } = useAsync(fetchCommunicationPreferences, [employeeId]);

  const {
    state: { value: employeePhoneNumbers },
    changeAsyncValue: refreshPhoneNumbersInMemory
  } = useAsync(fetchPhoneNumbers, [employeeId], {
    shouldExecute: () => hasViewPhoneNumbersPermission
  });

  const {
    state: { value: employeeEmails },
    changeAsyncValue: refreshEmailsInMemory
  } = useAsync(fetchEmailAddresses, [employeeId], {
    shouldExecute: () => hasViewEmailsPermission
  });

  useEffect(() => {
    setEmailAddresses(employeeEmails!);
    setPhoneNumbers(employeePhoneNumbers!);
  }, [employeeEmails, employeePhoneNumbers]);

  useEffect(() => {
    setEditedCommunicationPreferences(
      fetchedCommunicationPreferences as FullCommunicationPreference
    );
  }, [fetchedCommunicationPreferences]);

  const contextValue = useMemo(
    () => ({
      emailAddresses,
      phoneNumbers,
      communicationPreferences: editedCommunicationPreferences,
      onCommunicationPreferencesEdit: (
        newCommunicationPreference: FullCommunicationPreference
      ) => {
        refreshCommunicationPreferencesInMemory({
          directCorrespondence: newCommunicationPreference.directCorrespondence,
          notificationOfUpdates:
            newCommunicationPreference.notificationOfUpdates,
          writtenCorrespondence:
            newCommunicationPreference.writtenCorrespondence
        });
        setEditedCommunicationPreferences(newCommunicationPreference);
      },
      onEmailAdd: (emails: GroupClientEmailAddress[]) => {
        setEmailAddresses([...emails, ...emailAddresses]);
        refreshEmailsInMemory([...emails, ...emailAddresses]);
      },
      onPhoneNumberAdd: (phones: GroupClientPhoneNumber[]) => {
        setPhoneNumbers([...phones, ...phoneNumbers]);
        refreshPhoneNumbersInMemory([...phones, ...phoneNumbers]);
      }
    }),
    [
      emailAddresses,
      phoneNumbers,
      editedCommunicationPreferences,
      refreshCommunicationPreferencesInMemory,
      refreshEmailsInMemory,
      refreshPhoneNumbersInMemory
    ]
  );

  const commonThemedStyles = useEmployeeDetailsTabThemedStyles();

  return (
    <Tabs.TabPane {...props} data-test-el="communication-preferences-tab">
      <TabHeader>
        <FormattedMessage
          className={classNames(commonStyles.header, commonThemedStyles.header)}
          as="h2"
          id="PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE"
        />
      </TabHeader>
      <TabBody contentClassName={styles.tabContent}>
        <ContentRenderer
          isLoading={isPending}
          isEmpty={_isEmpty(editedCommunicationPreferences)}
        >
          {!!editedCommunicationPreferences && (
            <CommunicationPreferencesContext.Provider
              value={contextValue as PreferencesContext}
            >
              <DirectCorrespondence employeeId={employeeId} />

              <NotificationOfUpdates employeeId={employeeId} />

              <WrittenCorrespondence employeeId={employeeId} />
            </CommunicationPreferencesContext.Provider>
          )}
        </ContentRenderer>
      </TabBody>
    </Tabs.TabPane>
  );
};
