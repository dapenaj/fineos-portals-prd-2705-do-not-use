import React from 'react';
import { Formik } from 'formik';
import classNames from 'classnames';
import {
  AsteriskInfo,
  Modal,
  FormattedMessage,
  ModalFooter,
  FormButton,
  UiButton,
  ButtonType,
  Form,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { GroupClientPhoneNumberResource } from 'fineos-js-api-client';

import { AddPhoneNumberForm } from '../add-phone-number-form/add-phone-number-form.component';
import { PhoneType, initialPhoneInputValue } from '../../../../../shared';
import communicationPreferencesStyles from '../communication-preferences-tab.module.scss';
import { mapPhoneNumberToApiFormat } from '../communication-preferences-tab.util';
import { PhoneCallsFormState } from '../direct-correspondence/phone-calls-modal/phone-calls-modal.type';

import { newPhoneNumberValidationSchema } from './add-phone-number-modal.form';

type AddPhoneNumberModalProps = {
  isVisible: boolean;
  onPhoneNumberPass: (phone: GroupClientPhoneNumberResource) => void;
  onModalClose: () => void;
  allowedPhoneTypeNames?: PhoneType[];
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const AddPhoneNumberModal: React.FC<AddPhoneNumberModalProps> = ({
  isVisible,
  onPhoneNumberPass,
  onModalClose,
  allowedPhoneTypeNames = []
}) => {
  const handleSubmit = (formData: PhoneCallsFormState) => {
    const mappedPhoneNumber = mapPhoneNumberToApiFormat(formData);
    onPhoneNumberPass(mappedPhoneNumber);
    onModalClose();
  };

  const themedStyles = useThemedStyles();
  return (
    <Modal
      ariaLabelId="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.ADD_PHONE_NUMBER"
      visible={isVisible}
      data-test-el="add-phone-number-modal"
      onCancel={onModalClose}
      header={
        <FormattedMessage id="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.ADD_PHONE_NUMBER" />
      }
    >
      <AsteriskInfo />
      <div
        className={classNames(
          communicationPreferencesStyles.modalLabel,
          themedStyles.label
        )}
      >
        <FormattedMessage
          id="PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL"
          values={{
            actionType: 'Add'
          }}
        />
      </div>
      <Formik
        onSubmit={handleSubmit}
        validateOnMount={true}
        initialValues={
          {
            newPhoneNumber: {
              ...initialPhoneInputValue
            }
          } as PhoneCallsFormState
        }
        validationSchema={newPhoneNumberValidationSchema}
      >
        <Form
          className={communicationPreferencesStyles.addElementForm}
          data-test-el="add-phone-number-form"
        >
          <AddPhoneNumberForm allowedPhoneTypeNames={allowedPhoneTypeNames} />
          <ModalFooter>
            <FormattedMessage
              as={FormButton}
              htmlType="submit"
              shouldSubmitForm={false}
              id="FINEOS_COMMON.GENERAL.OK"
            />
            <FormattedMessage
              as={UiButton}
              buttonType={ButtonType.LINK}
              onClick={onModalClose}
              id="FINEOS_COMMON.GENERAL.CANCEL"
            />
          </ModalFooter>
        </Form>
      </Formik>
    </Modal>
  );
};
