import React, { useContext, useState } from 'react';
import { Checkbox } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { Field, Formik } from 'formik';
import classNames from 'classnames';
import { isEmpty as _isEmpty } from 'lodash';
import {
  FormButton,
  UiButton,
  ButtonType,
  CheckboxGroup,
  FormGroup,
  Form,
  createThemedStyles,
  FormattedMessage,
  ModalFooter,
  textStyleToCss,
  useErrorNotificationPopup,
  AsteriskInfo,
  Modal
} from 'fineos-common';
import {
  BaseEmailAddress,
  GroupClientEmailAddress,
  GroupClientPhoneNumber,
  GroupClientPhoneNumberResource
} from 'fineos-js-api-client';

import { SelectEmail } from '../../select-email/select-email.component';
import { SelectPhoneNumber } from '../../select-phone-number/select-phone-number.component';
import { CommunicationOptions } from '../../communication-preferences-tab.enum';
import { AddPhoneNumberForm } from '../../add-phone-number-form/add-phone-number-form.component';
import { AddEmailForm } from '../../add-email-form/add-email-form.component';
import { sendSelectedCommunicationTypes } from '../../communication-preferences-tab.api';
import { CommunicationPreferencesContext } from '../../communication-preferences-tab.context';
import communicationPreferencesStyles from '../../communication-preferences-tab.module.scss';
import {
  mapPhoneNumberToApiFormat,
  getValidTypePhoneNumbers
} from '../../communication-preferences-tab.util';
import { CommunicationPreferencesType } from '../../communication-preferences.constant';
import { PhoneType } from '../../../../../../shared';

import { changeNotificationOfUpdatesPreferences } from './notification-of-updates.api';
import {
  ChangePreferencesResponse,
  NotificationOfUpdatesFormState
} from './notification-of-updates-modal.type';
import {
  notificationOfUpdatesInitialValue,
  notificationOfUpdatesValidation,
  getSelectedPhoneNumberIndex,
  getSelectedEmailAddressIndex
} from './notification-of-updates-modal.form';
import styles from './notification-of-updates-modal.module.scss';

type NotificationOfUpdatesModalProps = {
  employeeId: string;
  onModalClose: () => void;
  isVisible: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  label: {
    ...textStyleToCss(theme.typography.baseText)
  },
  checkboxLabel: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

const ALLOWED_PHONE_TYPES = [PhoneType.CELL];

export const NotificationOfUpdatesModal: React.FC<NotificationOfUpdatesModalProps> = ({
  employeeId,
  onModalClose,
  isVisible
}) => {
  const [passedEmails, setPassedEmails] = useState<BaseEmailAddress[]>([]);
  const [passedPhoneNumbers, setPassedPhoneNumbers] = useState<
    GroupClientPhoneNumberResource[]
  >([]);

  const { showErrorNotification } = useErrorNotificationPopup();
  const {
    communicationPreferences,
    onCommunicationPreferencesEdit,
    emailAddresses,
    phoneNumbers,
    onPhoneNumberAdd,
    onEmailAdd
  } = useContext(CommunicationPreferencesContext);

  const addSelectedCommunicationTypes = (
    formData: NotificationOfUpdatesFormState
  ) => {
    const hasEmail =
      !!formData.emailAddress &&
      formData.communicationType.includes(CommunicationOptions.EMAIL);
    const hasNumber =
      !!formData.newPhoneNumber.telephoneNo &&
      formData.communicationType.includes(CommunicationOptions.SMS);
    return sendSelectedCommunicationTypes(
      employeeId,
      hasNumber ? [mapPhoneNumberToApiFormat(formData)] : passedPhoneNumbers,
      hasEmail ? [{ emailAddress: formData.emailAddress }] : passedEmails
    )
      .then(data => {
        if (data.addedEmailAddresses) {
          onEmailAdd(data.addedEmailAddresses);
          setPassedEmails([]);
        }
        if (data.addedPhoneNumbers) {
          onPhoneNumberAdd(data.addedPhoneNumbers);
          setPassedPhoneNumbers([]);
        }
        changeCommunicationPreferences(data, formData);
      })
      .catch(showErrorNotification);
  };

  const changeCommunicationPreferences = (
    addedData: ChangePreferencesResponse,
    formData: NotificationOfUpdatesFormState
  ) => {
    const validEmails = [
      ...(addedData.addedEmailAddresses ? addedData.addedEmailAddresses : []),
      ...emailAddresses
    ];
    const validPhoneNumbers = getValidTypePhoneNumbers(ALLOWED_PHONE_TYPES, [
      ...(addedData.addedPhoneNumbers ? addedData.addedPhoneNumbers : []),
      ...phoneNumbers
    ]);
    let emailToSet: GroupClientEmailAddress | null = !!formData.emailAddress
      ? addedData.addedEmailAddresses[0]
      : validEmails[formData.selectedEmailAddressIndex];
    let numberToSet: GroupClientPhoneNumber | null = !!formData.newPhoneNumber
      .telephoneNo
      ? addedData.addedPhoneNumbers[0]
      : validPhoneNumbers[formData.selectedPhoneNumberIndex];
    if (!formData.communicationType.includes(CommunicationOptions.EMAIL)) {
      emailToSet = null;
    }
    if (!formData.communicationType.includes(CommunicationOptions.SMS)) {
      numberToSet = null;
    }

    return changeNotificationOfUpdatesPreferences(
      employeeId,
      CommunicationPreferencesType.NOTIFICATION_OF_UPDATES_ID,
      emailToSet,
      numberToSet
    )
      .then(() => {
        onCommunicationPreferencesEdit({
          ...communicationPreferences,
          notificationOfUpdates: {
            ...(communicationPreferences.notificationOfUpdates! || {}),
            emailAddresses: emailToSet ? [emailToSet] : [],
            phoneNumbers: numberToSet ? [numberToSet] : []
          }
        });
        onModalClose();
      })
      .catch(showErrorNotification);
  };

  const handleSubmit = (formData: NotificationOfUpdatesFormState) => {
    if (
      _isEmpty(passedEmails) &&
      _isEmpty(passedPhoneNumbers) &&
      !formData.emailAddress &&
      !formData.newPhoneNumber?.telephoneNo
    ) {
      changeCommunicationPreferences({} as ChangePreferencesResponse, formData);
      return;
    }
    addSelectedCommunicationTypes(formData);
  };
  // to avoid tslint rule 140 max length
  const headerId =
    'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.TITLE';
  const notificationTypeId =
    'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.NOTIFICATION_TYPE_QUESTION';
  const themedStyles = useThemedStyles();
  return (
    <Modal
      ariaLabelId={headerId}
      visible={isVisible}
      onCancel={onModalClose}
      header={<FormattedMessage id={headerId} />}
    >
      <Formik
        onSubmit={handleSubmit}
        validateOnMount={true}
        initialValues={notificationOfUpdatesInitialValue(
          emailAddresses,
          getValidTypePhoneNumbers(ALLOWED_PHONE_TYPES, phoneNumbers),
          communicationPreferences,
          ALLOWED_PHONE_TYPES
        )}
        validationSchema={notificationOfUpdatesValidation(
          getValidTypePhoneNumbers(ALLOWED_PHONE_TYPES, phoneNumbers),
          emailAddresses
        )}
      >
        {({ values, setFieldValue }) => (
          <Form>
            <AsteriskInfo />
            <div
              className={classNames({
                [communicationPreferencesStyles.modalContent]: _isEmpty(
                  values.communicationType
                )
              })}
            >
              <div className={styles.notificationTypeForm}>
                <FormGroup
                  isRequired={true}
                  labelClassName={classNames(themedStyles.label, styles.label)}
                  label={<FormattedMessage id={notificationTypeId} />}
                >
                  <Field component={CheckboxGroup} name="communicationType">
                    <Checkbox
                      value={CommunicationOptions.EMAIL}
                      onChange={(event: CheckboxChangeEvent) => {
                        if (!event.target?.checked) {
                          setFieldValue(
                            'selectedEmailAddressIndex',
                            getSelectedEmailAddressIndex(
                              emailAddresses,
                              communicationPreferences
                            )
                          );
                        }
                      }}
                    >
                      <FormattedMessage
                        className={themedStyles.checkboxLabel}
                        id="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_EMAIL"
                      />
                    </Checkbox>
                    <Checkbox
                      value={CommunicationOptions.SMS}
                      onChange={(event: CheckboxChangeEvent) => {
                        if (!event.target?.checked) {
                          setFieldValue(
                            'selectedPhoneNumberIndex',
                            getSelectedPhoneNumberIndex(
                              phoneNumbers,
                              communicationPreferences,
                              ALLOWED_PHONE_TYPES
                            )
                          );
                        }
                      }}
                    >
                      <FormattedMessage
                        className={themedStyles.checkboxLabel}
                        id="PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_SMS"
                      />
                    </Checkbox>
                  </Field>
                </FormGroup>
                {values.communicationType.includes(
                  CommunicationOptions.EMAIL
                ) && (
                  <>
                    {_isEmpty(emailAddresses) ? (
                      <div
                        className={
                          communicationPreferencesStyles.addElementForm
                        }
                      >
                        <AddEmailForm />
                      </div>
                    ) : (
                      <SelectEmail
                        onEmailPass={(emailAddress: BaseEmailAddress) => {
                          setFieldValue('selectedEmailAddressIndex', 0);
                          setPassedEmails([emailAddress, ...passedEmails]);
                        }}
                      />
                    )}
                  </>
                )}
                {values.communicationType.includes(
                  CommunicationOptions.SMS
                ) && (
                  <>
                    {_isEmpty(
                      getValidTypePhoneNumbers(
                        ALLOWED_PHONE_TYPES,
                        phoneNumbers
                      )
                    ) ? (
                      <div
                        className={
                          communicationPreferencesStyles.addElementForm
                        }
                      >
                        <AddPhoneNumberForm
                          allowedPhoneTypeNames={ALLOWED_PHONE_TYPES}
                        />
                      </div>
                    ) : (
                      <SelectPhoneNumber
                        allowedPhoneTypeNames={ALLOWED_PHONE_TYPES}
                        onPhoneNumberPass={(
                          phoneNumber: GroupClientPhoneNumberResource
                        ) => {
                          setFieldValue('selectedPhoneNumberIndex', 0);
                          setPassedPhoneNumbers([
                            phoneNumber,
                            ...passedPhoneNumbers
                          ]);
                        }}
                      />
                    )}
                  </>
                )}
              </div>
            </div>
            <ModalFooter>
              <FormattedMessage
                shouldSubmitForm={false}
                as={FormButton}
                id="FINEOS_COMMON.GENERAL.OK"
                htmlType="submit"
              />
              <FormattedMessage
                as={UiButton}
                buttonType={ButtonType.LINK}
                onClick={onModalClose}
                id="FINEOS_COMMON.GENERAL.CANCEL"
              />
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};
