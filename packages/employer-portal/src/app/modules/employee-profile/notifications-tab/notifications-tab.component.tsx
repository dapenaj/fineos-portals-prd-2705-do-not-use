/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import {
  useAsync,
  Divider,
  createThemedStyles,
  EmptyState,
  Pill,
  Card,
  FormattedDate,
  Tabs,
  Link,
  textStyleToCss,
  usePermissions,
  CreateNotificationPermissions,
  ContentRenderer,
  FormattedMessage,
  ButtonType,
  Button,
  FormGroup
} from 'fineos-common';
import classNames from 'classnames';
import { Row, Col } from 'antd';
import { isEmpty as _isEmpty, orderBy as _orderBy } from 'lodash';
import { GroupClientNotification, SubCaseType } from 'fineos-js-api-client';

import { NotificationReason } from '../../notification/notification-reason';
import { SortByDateSelect, SortDirection } from '../../../shared';

import {
  fetchNotifications,
  EMPTY_NOTIFICATIONS_LIST
} from './notifications-tab.api';
import styles from './notifications-tab.module.scss';

type NotificationsTabProps = React.ComponentProps<typeof TabPane> & {
  employeeId: string;
};

const TabPane = Tabs.TabPane;

const useThemedStyles = createThemedStyles(theme => ({
  notificationLink: {
    ...textStyleToCss(theme.typography.panelTitle)
  },
  notifiedOn: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  notifiedOnDate: {
    ...textStyleToCss(theme.typography.smallTextSemiBold)
  },
  notificationReason: {
    color: theme.colorSchema.neutral8
  }
}));

const notificationsIsLabeledBy = (
  notification: GroupClientNotification,
  entitlement: SubCaseType
): boolean =>
  notification.subCases.some(subCase => subCase.caseType === entitlement);

export const NotificationsTab: React.FC<NotificationsTabProps> = ({
  employeeId,
  ...props
}) => {
  const [sortDirection, setSortDirection] = useState<SortDirection>(
    SortDirection.DESC
  );
  const [sortedNotifications, setSortedNotifications] = useState<
    GroupClientNotification[] | null
  >(null);
  const themedStyles = useThemedStyles();
  const intl = useIntl();
  const hasEstablishedIntakePermission = usePermissions(
    CreateNotificationPermissions.ADD_NOTIFICATION
  );

  const {
    state: {
      value: { data: notifications },
      isPending
    }
  } = useAsync(fetchNotifications, [employeeId], {
    defaultValue: EMPTY_NOTIFICATIONS_LIST
  });

  const getSortedNotifications = (
    allNotifications: GroupClientNotification[],
    direction: SortDirection
  ) =>
    _orderBy(allNotifications, notification => notification.createdDate, [
      direction.toLowerCase() as 'asc' | 'desc'
    ]);

  useEffect(() => {
    if (!sortedNotifications && notifications.length) {
      setSortedNotifications(
        getSortedNotifications(notifications, SortDirection.DESC)
      );
    }
  }, [notifications, sortedNotifications]);

  useEffect(() => {
    setSortedNotifications(
      getSortedNotifications(notifications, sortDirection)
    );
  }, [sortDirection, notifications]);

  const isNotificationListEmpty = _isEmpty(sortedNotifications);

  return (
    <div className={styles.container}>
      <TabPane {...props}>
        <ContentRenderer
          isLoading={isPending}
          isEmpty={isNotificationListEmpty}
          data-test-el="notification-tab"
          emptyContent={
            <EmptyState withBorder={true}>
              <FormattedMessage id="PROFILE.NOTIFICATIONS_TAB.EMPTY_STATE" />

              {hasEstablishedIntakePermission && (
                <div className={styles.establishedIntake}>
                  <Link to={`/employee/${employeeId}/intake`}>
                    <FormattedMessage
                      as={Button}
                      buttonType={ButtonType.PRIMARY}
                      id="HEADER.SEARCH.ESTABLISHED_INTAKE_LINK"
                    />
                  </Link>
                </div>
              )}
            </EmptyState>
          }
        >
          <FormGroup
            label={
              <FormattedMessage id="PROFILE.NOTIFICATIONS_TAB.NOTIFICATIONS_SORTING_LABEL" />
            }
            className={classNames(styles.sortByDateSelect, {
              [styles.sorterNotDisplayed]: isNotificationListEmpty
            })}
          >
            <SortByDateSelect
              value={sortDirection}
              onChange={(value: unknown) =>
                setSortDirection(value as SortDirection)
              }
            />
          </FormGroup>
          {!isNotificationListEmpty &&
            sortedNotifications!.map(
              (notification: GroupClientNotification) => (
                <Card
                  data-test-el="notification-list-item"
                  key={notification.caseNumber}
                  className={styles.notificationItem}
                >
                  <Row>
                    <Col span={4} className={styles.notificationSection}>
                      <Link
                        to={`/notification/${notification.caseNumber}`}
                        className={themedStyles.notificationLink}
                      >
                        {notification.caseNumber}
                      </Link>
                      <div className={themedStyles.notifiedOn}>
                        <FormattedMessage id="PROFILE.NOTIFICATIONS_TAB.NOTIFIED_ON" />{' '}
                        <FormattedDate
                          data-test-el="notified-on-date"
                          date={notification.createdDate}
                          className={themedStyles.notifiedOnDate}
                        />
                      </div>
                    </Col>

                    <Col span={1} className={styles.notificationColDivider}>
                      <Divider type="vertical" className={styles.divider} />
                    </Col>

                    <Col
                      data-test-el="notification-reason"
                      span={6}
                      className={classNames(
                        styles.notificationSection,
                        styles.notificationReason,
                        themedStyles.notificationReason
                      )}
                    >
                      <NotificationReason
                        reason={notification.notificationReason}
                      />
                    </Col>

                    <Col span={1} className={styles.notificationColDivider}>
                      <Divider type="vertical" className={styles.divider} />
                    </Col>

                    <Col span={5} className={styles.notificationSection}>
                      {notificationsIsLabeledBy(
                        notification,
                        SubCaseType.CLAIM_TYPE
                      ) && (
                        <Pill
                          title={intl.formatMessage({
                            id: 'NOTIFICATIONS.TYPE.WAGE_REPLACEMENT'
                          })}
                          className={styles.entitlementLabel}
                        >
                          <FormattedMessage id="NOTIFICATIONS.TYPE.WAGE_REPLACEMENT" />
                        </Pill>
                      )}
                      {notificationsIsLabeledBy(
                        notification,
                        SubCaseType.ABSENCE_TYPE
                      ) && (
                        <Pill
                          title={intl.formatMessage({
                            id: 'NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE'
                          })}
                          className={styles.entitlementLabel}
                        >
                          <FormattedMessage id="NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE" />
                        </Pill>
                      )}
                      {notificationsIsLabeledBy(
                        notification,
                        SubCaseType.ACCOMMODATION_TYPE
                      ) && (
                        <Pill
                          title={intl.formatMessage({
                            id: 'NOTIFICATIONS.TYPE.WORKPLACE_ACCOMMODATION'
                          })}
                          className={styles.entitlementLabel}
                        >
                          <FormattedMessage id="NOTIFICATIONS.TYPE.WORKPLACE_ACCOMMODATION" />
                        </Pill>
                      )}
                    </Col>
                  </Row>
                </Card>
              )
            )}
        </ContentRenderer>
      </TabPane>
    </div>
  );
};
