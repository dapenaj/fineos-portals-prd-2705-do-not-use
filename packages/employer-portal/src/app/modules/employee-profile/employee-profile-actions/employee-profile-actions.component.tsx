/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Menu,
  createThemedStyles,
  Link,
  FormattedMessage,
  DropdownMenu,
  textStyleToCss,
  Button
} from 'fineos-common';
import classNames from 'classnames';
import { DownOutlined } from '@ant-design/icons';

import styles from './employee-profile-actions.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  trigger: {
    color: theme.colorSchema.neutral1,
    ...textStyleToCss(theme.typography.baseText)
  },
  menu: {
    backgroundColor: `${theme.colorSchema.neutral1} !important`,
    borderRight: `${theme.colorSchema.neutral2} !important`,
    '& .ant-menu, .ant-menu-item, .ant-menu-item a': {
      backgroundColor: `${theme.colorSchema.neutral1} !important`,
      color: `${theme.colorSchema.neutral8} !important`,
      ...textStyleToCss(theme.typography.baseText)
    }
  }
}));

type EmployeeProfileActions = {
  employeeId: string;
};

export const EmployeeProfileActions: React.FC<EmployeeProfileActions> = ({
  employeeId
}) => {
  const themedStyles = useThemedStyles();

  return (
    <DropdownMenu
      overlay={
        <Menu className={classNames(styles.menu, themedStyles.menu)}>
          <FormattedMessage
            key={0}
            id="HEADER.SEARCH.ESTABLISHED_INTAKE_LINK"
            as={Link}
            to={`/employee/${employeeId}/intake`}
          />
        </Menu>
      }
      trigger={['click']}
      placement="bottomRight"
      data-test-el="employee-profile-actions"
    >
      <div className={classNames(styles.trigger, themedStyles.trigger)}>
        <Button>
          <FormattedMessage id="PROFILE.ACTIONS" />
          <DownOutlined className={styles.icon} />
        </Button>
      </div>
    </DropdownMenu>
  );
};
