/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Tabs,
  useAsync,
  Spinner,
  createThemedStyles,
  textStyleToCss,
  usePermissions,
  anyFrom,
  ViewNotificationPermissions,
  ViewOccupationPermissions,
  ViewCustomerDataPermissions,
  FallbackValue,
  CreateNotificationPermissions,
  FormattedMessage,
  ConfigurableText
} from 'fineos-common';
import classNames from 'classnames';

import {
  usePreloadEnumDomain,
  PageLayout,
  TokenTitle,
  EnumDomain
} from '../../shared';

import {
  fetchCustomer,
  fetchMostRecentCustomerOccupation
} from './employee-profile.api';
import { NotificationsTab } from './notifications-tab';
import { EmployeeDetailsTab } from './employee-details-tab';
import { TimeTakenTab } from './time-taken-tab';
import styles from './employee-profile.module.scss';
import { EmployeeProfileActions } from './employee-profile-actions';
import { UpdateCustomer } from './employee-profile.type';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation
} from 'fineos-js-api-client';

const useThemedStyles = createThemedStyles(theme => ({
  userName: {
    ...textStyleToCss(theme.typography.pageTitle),
    color: theme.colorSchema.neutral8
  },
  userOccupationSummary: {
    color: theme.colorSchema.neutral8
  }
}));

type EmployeeProfileProps = {
  employeeId: string;
};

export const EmployeeProfile: React.FC<EmployeeProfileProps> = ({
  employeeId,
  ...props
}) => {
  const themedStyles = useThemedStyles();

  const hasNotificationPermission = usePermissions(
    ViewNotificationPermissions.VIEW_NOTIFICATIONS
  );

  const hasViewCustomerOccupationsPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
  );

  const hasEmployeeDetailsPermissions = usePermissions(
    anyFrom(
      ViewCustomerDataPermissions.VIEW_CUSTOMER,
      ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
      ViewCustomerDataPermissions.VIEW_CUSTOMER_COMMUNICATION_PREFERENCES
    )
  );

  const hasActionsPermissions = usePermissions(
    CreateNotificationPermissions.ADD_NOTIFICATION
  );

  const hasCustomerInfoPermissions = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
  );

  const hasTimeTakenPermissions = usePermissions([
    ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
    ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
  ]);

  const hasEstablishedIntakePermission = usePermissions(
    CreateNotificationPermissions.ADD_NOTIFICATION
  );

  usePreloadEnumDomain(
    EnumDomain.OCCUPATION_CATEGORY,
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
  );

  usePreloadEnumDomain(
    EnumDomain.WEEK_DAYS,
    ViewOccupationPermissions.VIEW_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
  );

  usePreloadEnumDomain(
    EnumDomain.WORK_PATTERN_TYPE,
    ViewOccupationPermissions.VIEW_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
  );

  const {
    state: { isPending, value: employee },
    changeAsyncValue
  } = useAsync(fetchCustomer, [employeeId!]);

  const {
    state: { isPending: isOccupationPending, value: occupation },
    executeAgain: refreshMostCustomerOccupation
  } = useAsync(fetchMostRecentCustomerOccupation, [employeeId!], {
    shouldExecute: () => hasViewCustomerOccupationsPermission
  });

  return (
    <PageLayout
      contentHeader={
        !!employee && (
          <div
            className={classNames(
              themedStyles.userOccupationSummary,
              styles.userOccupationSummary
            )}
          >
            <TokenTitle
              tokens={[
                <FallbackValue
                  key={0}
                  value={employee.jobTitle}
                  fallback={
                    <FormattedMessage
                      id={'EMPLOYEE.FALLBACK.UNKNOWN_JOB_TITLE'}
                    />
                  }
                />,
                <FallbackValue
                  key={1}
                  value={employee.organisationUnit}
                  fallback={
                    <FormattedMessage id={'EMPLOYEE.FALLBACK.UNKNOWN_UNIT'} />
                  }
                />,
                <FallbackValue
                  key={2}
                  value={employee.workSite}
                  fallback={
                    <FormattedMessage
                      id={'EMPLOYEE.FALLBACK.UNKNOWN_LOCATION'}
                    />
                  }
                />
              ]}
            >
              <h1
                className={themedStyles.userName}
                data-test-el="employee-profile-username"
              >
                {employee.firstName} {employee.lastName}
              </h1>
            </TokenTitle>
            {hasActionsPermissions &&
              hasCustomerInfoPermissions &&
              hasEstablishedIntakePermission && (
                <EmployeeProfileActions employeeId={employeeId} />
              )}
          </div>
        )
      }
    >
      {!isPending ? (
        !!employee && (
          <Tabs className={styles.tabs} data-test-el="epmloyee-profile-tabs">
            {hasNotificationPermission && (
              <NotificationsTab
                employeeId={employee.id}
                tab={<FormattedMessage id="PROFILE.NOTIFICATIONS_TAB.TITLE" />}
                key="notifications"
              />
            )}
            {hasEmployeeDetailsPermissions && (
              <EmployeeDetailsTab
                employeeId={employee.id}
                occupation={
                  occupation as
                    | (CustomerAbsenceEmployment & CustomerOccupation)
                    | null
                }
                isOccupationPending={isOccupationPending}
                onOccupationChange={refreshMostCustomerOccupation}
                onCustomerUpdate={({ firstName, lastName }: UpdateCustomer) =>
                  changeAsyncValue({
                    ...employee,
                    firstName,
                    lastName
                  })
                }
                tab={
                  <FormattedMessage id="PROFILE.EMPLOYEE_DETAILS_TAB.TITLE" />
                }
                key="employee-details"
              />
            )}
            {hasTimeTakenPermissions && (
              <TimeTakenTab
                employeeId={employee.id}
                tab={<ConfigurableText id="PROFILE.TIME_TAKEN_TAB.TITLE" />}
                key="time-taken"
                {...props}
              />
            )}
          </Tabs>
        )
      ) : (
        <Spinner />
      )}
    </PageLayout>
  );
};
