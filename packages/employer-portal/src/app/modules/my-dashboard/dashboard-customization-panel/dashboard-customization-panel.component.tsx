/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  FormattedMessage,
  Drawer,
  UiSwitch,
  SwitchLabel,
  UiButton,
  ButtonType,
  DrawerFooter
} from 'fineos-common';

import { DashboardConfig } from '../dashboard.type';

import styles from './dashboard-customization-panel.module.scss';

type DashboardCustomizationPanelProps = {
  config: DashboardConfig;
  isDrawerVisible: boolean;
  onClose: () => void;
  setConfig: (config: DashboardConfig) => void;
};

export const DashboardCustomizationPanel: React.FC<DashboardCustomizationPanelProps> = ({
  config,
  isDrawerVisible,
  onClose,
  setConfig
}) => {
  const [localConfig, setLocalConfig] = useState<DashboardConfig>(config);

  const toggleComponentVisiblity = (
    key: keyof DashboardConfig,
    visible: boolean
  ) => {
    setLocalConfig({
      ...localConfig,
      [key]: {
        ...localConfig[key],
        isVisible: visible
      }
    });
  };

  const handleClose = () => {
    setLocalConfig(config);
    onClose();
  };

  return (
    <Drawer
      ariaLabelId="WIDGET.SHOW_HIDE_PANELS"
      data-test-el="panel-customization-drawer"
      onClose={handleClose}
      isClosable={true}
      className={styles.drawer}
      isVisible={isDrawerVisible}
      title={<FormattedMessage id="WIDGET.SHOW_HIDE_PANELS" />}
    >
      <ul className={styles.list} data-test-el="panel-customization-list">
        {Object.entries(localConfig).map(
          ([key, { id, isVisible, hasPermission }]) =>
            hasPermission && (
              <li key={key}>
                <UiSwitch
                  data-test-el={`panel-customization-item-${key}`}
                  id={key}
                  isChecked={isVisible}
                  onChange={() => {
                    toggleComponentVisiblity(
                      key as keyof DashboardConfig,
                      !isVisible
                    );
                  }}
                  size="small"
                  aria-labelledby={id}
                />
                <SwitchLabel>
                  <FormattedMessage id={id} elementId={id} />
                </SwitchLabel>
              </li>
            )
        )}
      </ul>
      <DrawerFooter>
        <FormattedMessage
          as={UiButton}
          buttonType={ButtonType.PRIMARY}
          id="FINEOS_COMMON.GENERAL.SAVE_CHANGES"
          onClick={() => {
            setConfig(localConfig);
            onClose();
          }}
        />
        <FormattedMessage
          as={UiButton}
          buttonType={ButtonType.LINK}
          id="FINEOS_COMMON.GENERAL.CANCEL"
          onClick={handleClose}
        />
      </DrawerFooter>
    </Drawer>
  );
};
