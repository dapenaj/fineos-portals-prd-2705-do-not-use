/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import classNames from 'classnames';
import { groupBy as _groupBy, isEqual as _isEqual } from 'lodash';
import {
  FormattedMessage,
  Drawer,
  DrawerWidth,
  ContentRenderer,
  DonutData,
  FilterItemsCountTag,
  DrawerFooter,
  PAGINATION_THRESHOLD
} from 'fineos-common';
import { GroupClientNotification } from 'fineos-js-api-client';

import {
  DateFilter,
  GraphWidget,
  getEnumDomainTranslation,
  EnumDomain,
  useTransformOutstandingNotifications,
  initialTableFilters,
  hasClaimTypeSubcase,
  MappedNotificationKey
} from '../../../shared';
import { useWidgetThemedStyles } from '../dashboard-widget.utils';
import { WidgetKey, WidgetProps } from '../dashboard.type';
import { useWidgetState, WidgetDrawerFooter } from '../widget-shared';
import styles from '../my-dashboard.module.scss';

import {
  CategoryFilter,
  DISABILITY_RELATED_KEY,
  NON_DISABILITY_RELATED_KEY
} from './notification-created-widget.enum';
import {
  NotificationsCreatedDrawerDateFilter,
  NotificationsCreatedWidgetCategoryFilter,
  NotificationsCreatedWidgetDateFilter,
  NotificationsCreatedWidgetHelpInfo,
  NotificationsCreatedDrawerHelpInfo
} from './ui';
import { NotificationsCreatedTable } from './notifications-created-table.component';

export const NotificationsCreatedWidget: React.FC<WidgetProps> = ({
  helpInfoEnabled
}) => {
  const themedStyles = useWidgetThemedStyles();
  const intl = useIntl();

  const {
    notifications,
    filteredNotifications,
    totalNotifications,
    isPending,
    error,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    onSliceClick
  } = useWidgetState(WidgetKey.NOTIFICATIONS_CREATED);

  const [currentCategoryFilter, setCurrentCategoryFilter] = useState<
    CategoryFilter
  >(CategoryFilter.DISABILITY_RELATED);

  const [isDrawerVisible, setIsDrawerVisible] = useState(false);

  const downloadReadyNotifications = useTransformOutstandingNotifications(
    filteredNotifications.sort((a, b) => a.createdDate.diff(b.createdDate)),
    [
      MappedNotificationKey.NAME,
      MappedNotificationKey.JOB_TITLE,
      MappedNotificationKey.GROUP,
      MappedNotificationKey.REASON,
      MappedNotificationKey.CREATED_DATE,
      MappedNotificationKey.CASE,
      MappedNotificationKey.DISABILITY
    ]
  );

  const getNotificationReasonName = (notification: GroupClientNotification) =>
    typeof notification.notificationReason === 'string'
      ? notification.notificationReason
      : notification.notificationReason.name;

  const graphData: DonutData[] = Object.entries(
    _groupBy(notifications, notification => {
      const reasonName = getNotificationReasonName(notification);
      // this will group the notifications by reason or if any sub cases are claims
      // group keys are translation ids
      return currentCategoryFilter === CategoryFilter.REASON
        ? reasonName
        : hasClaimTypeSubcase(notification)
        ? DISABILITY_RELATED_KEY
        : NON_DISABILITY_RELATED_KEY;
    })
  ).map(([key, value]) => {
    // Note: if category is REASON -> key = reason name value from API
    const translationKey =
      currentCategoryFilter === CategoryFilter.REASON
        ? `${getEnumDomainTranslation(EnumDomain.NOTIFICATION_REASON, key)}`
        : key;
    return {
      label: intl.formatMessage({ id: translationKey }),
      value: value.length,
      filterValues: [key]
    };
  });

  const onCategoryFilterChange = (value: CategoryFilter) => {
    setCurrentCategoryFilter(value);
  };

  const onDateFilterChange = (value: DateFilter) =>
    onApiFilter(value as DateFilter);

  const handleReasonFilterSliceClick = (filterValues: string[]) => {
    onSliceClick(
      notifications.filter((notification: GroupClientNotification) => {
        return filterValues.includes(notification.notificationReason.name);
      }),
      filterValues,
      currentCategoryFilter
    );
  };

  const getFilterValues = (filterValues: string[]) =>
    filterValues.includes(DISABILITY_RELATED_KEY)
      ? 'DISABILITY_RELATED'
      : 'NON_DISABILITY_RELATED';

  const handleDisabilityRelatedFilterSliceClick = (filterValues: string[]) => {
    onSliceClick(
      notifications.filter((notification: GroupClientNotification) =>
        filterValues.includes(DISABILITY_RELATED_KEY)
          ? hasClaimTypeSubcase(notification)
          : !hasClaimTypeSubcase(notification)
      ),
      [getFilterValues(filterValues)],
      currentCategoryFilter
    );
  };

  const handleOpenDrawer = (filterValues?: string[]) => {
    setIsDrawerVisible(true);
    if (!filterValues || filterValues.length === 0) {
      onSliceClick(notifications);
      return;
    }

    currentCategoryFilter === CategoryFilter.REASON
      ? handleReasonFilterSliceClick(filterValues)
      : handleDisabilityRelatedFilterSliceClick(filterValues);
  };

  const handleClose = () => {
    onClearFilters();
    setIsDrawerVisible(false);
  };

  const isFilterBadgeVisible =
    !isPending &&
    (notifications.length !== filteredNotifications.length ||
      !_isEqual(columnFilters, initialTableFilters));

  return (
    <div
      className={styles.container}
      data-test-el="notifications-created-widget"
    >
      <NotificationsCreatedWidgetHelpInfo isVisible={helpInfoEnabled} />
      <GraphWidget
        onViewDetails={handleOpenDrawer}
        data={graphData}
        isLoading={isPending}
        emptyMessage={
          <FormattedMessage
            id="WIDGET.NOTIFICATIONS_CREATED_EMPTY"
            role="status"
          />
        }
        error={error}
        filters={[
          <NotificationsCreatedWidgetDateFilter
            key={0}
            onChange={(value: unknown) =>
              onDateFilterChange(value as DateFilter)
            }
            apiFilters={apiFilters}
            themedStyles={themedStyles}
          />,
          <NotificationsCreatedWidgetCategoryFilter
            key={1}
            onChange={(value: unknown) =>
              onCategoryFilterChange(value as CategoryFilter)
            }
            currentFilter={currentCategoryFilter}
            themedStyles={themedStyles}
          />
        ]}
      />
      <Drawer
        ariaLabelId="WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER"
        ariaLabelValues={{ count: totalNotifications }}
        className={styles.drawer}
        headerClassName={styles.drawerHeader}
        bodyClassName={classNames({
          [styles.drawerBody]:
            filteredNotifications.length > PAGINATION_THRESHOLD
        })}
        data-test-el="notifications-created-drawer"
        onClose={handleClose}
        isVisible={isDrawerVisible}
        width={DrawerWidth.FULL_PAGE}
        title={
          <div className={styles.drawerHeaderContent}>
            <div className={styles.filterInputWrapper}>
              <NotificationsCreatedDrawerDateFilter
                onChange={onDateFilterChange}
                apiFilters={apiFilters}
                themedStyles={themedStyles}
                total={totalNotifications}
                isPending={isPending}
              />
              <NotificationsCreatedDrawerHelpInfo isVisible={helpInfoEnabled} />
            </div>
            <FilterItemsCountTag
              isVisible={isFilterBadgeVisible}
              filteredItemsCount={filteredNotifications.length}
              totalItemsCount={notifications.length}
              onClearFilters={onClearFilters}
            />
          </div>
        }
      >
        <ContentRenderer
          shouldNotDestroyOnLoading={true}
          spinnerClassName={styles.spinnerContainer}
          isLoading={isPending}
          isEmpty={false}
        >
          <NotificationsCreatedTable
            notifications={notifications}
            filteredNotifications={filteredNotifications}
            isPending={isPending}
            columnFilters={columnFilters}
            onColumnFilter={onColumnFilter}
            onClearFilters={onClearFilters}
          />
        </ContentRenderer>
        <DrawerFooter>
          <WidgetDrawerFooter
            isVisible={!isPending}
            dataLength={filteredNotifications.length}
            downloadData={downloadReadyNotifications}
            fileNameTranslationId="EXPORT_OUTSTANDING_LIST.FILE_NAME.NOTIFICATIONS_CREATED"
            onCloseClick={handleClose}
          />
        </DrawerFooter>
      </Drawer>
    </div>
  );
};
