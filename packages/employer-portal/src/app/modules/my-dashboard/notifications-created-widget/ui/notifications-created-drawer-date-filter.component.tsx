/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import { RichFormattedMessage, Select } from 'fineos-common';

import { DateFilter, NotificationAPIFilters } from '../../../../shared';
import {
  getDateFilters,
  useDateFilters,
  WidgetFilter,
  WidgetFilterCounter
} from '../../widget-shared';
import { getDateFilterValue } from '../../dashboard-widget.utils';
import styles from '../../my-dashboard.module.scss';

type NotificationsCreatedDrawerDateFilterProps = {
  onChange: (category: DateFilter) => void;
  apiFilters: NotificationAPIFilters;
  themedStyles: Record<string, string>;
  isPending: boolean;
  total: number;
};

export const NotificationsCreatedDrawerDateFilter: React.FC<NotificationsCreatedDrawerDateFilterProps> = ({
  onChange,
  apiFilters,
  themedStyles,
  isPending,
  total
}) => {
  const intl = useIntl();
  const { pastFilterOptions } = useDateFilters();
  return (
    <WidgetFilter
      label={
        <RichFormattedMessage
          className={classNames(styles.drawerTitle, themedStyles.title)}
          as="h1"
          id="WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER"
          values={{
            count: <WidgetFilterCounter isPending={isPending} total={total} />
          }}
        />
      }
      selectInput={
        <Select
          bordered={false}
          value={getDateFilterValue(
            intl,
            getDateFilters(intl),
            apiFilters?.notificationsCreated?.value
          )}
          idResolver={(value: unknown) =>
            getDateFilterValue(intl, getDateFilters(intl), value as string)!
          }
          data-test-el="notifications-created-drawer-filter"
          className={classNames(themedStyles.drawerSelect, styles.drawerSelect)}
          dropdownClassName={styles.selectDropdown}
          onChange={(value: unknown) => onChange(value as DateFilter)}
          name="filter"
          optionElements={pastFilterOptions}
          dropdownMatchSelectWidth={false}
        />
      }
    />
  );
};
