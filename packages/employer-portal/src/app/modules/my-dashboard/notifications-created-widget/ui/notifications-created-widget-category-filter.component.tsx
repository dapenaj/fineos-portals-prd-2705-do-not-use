/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage, Select } from 'fineos-common';

import { WidgetFilter } from '../../widget-shared';
import styles from '../../my-dashboard.module.scss';
import { CategoryFilter } from '../notification-created-widget.enum';

const FILTER_OPTIONS = [
  {
    value: CategoryFilter.DISABILITY_RELATED,
    title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.DISABILITY_RELATED" />
  },
  {
    value: CategoryFilter.REASON,
    title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.REASON" />
  }
];

type NotificationsCreatedWidgetCategoryFilterProps = {
  themedStyles: Record<string, string>;
  onChange: (value: unknown) => void;
  currentFilter: CategoryFilter;
};

export const NotificationsCreatedWidgetCategoryFilter: React.FC<NotificationsCreatedWidgetCategoryFilterProps> = ({
  onChange,
  currentFilter,
  themedStyles
}) => (
  <WidgetFilter
    label={
      <FormattedMessage
        className={themedStyles.filterText}
        id="WIDGET.NOTIFICATIONS_CREATED_FILTER_CATEGORY"
      />
    }
    selectInput={
      <Select
        bordered={false}
        value={currentFilter}
        defaultValue={currentFilter}
        data-test-el="notifications-created-filter-category"
        className={themedStyles.select}
        dropdownClassName={styles.selectDropdown}
        onChange={onChange}
        name="filter"
        optionElements={FILTER_OPTIONS}
        dropdownMatchSelectWidth={false}
      />
    }
  />
);
