/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useState, useMemo, useEffect } from 'react';
import { AbsenceEmployeesOnLeaveSearch } from 'fineos-js-api-client';
import { useAsync } from 'fineos-common';
import moment from 'moment';

import {
  AuthenticationService,
  DateFilter,
  TableFilters,
  WidgetTableColumn
} from '../../../shared';
import { DateRangeFilter } from '../dashboard.type';
import { useDateFilters } from '../widget-shared';

import { getFilteredEmployeesApprovedForLeave } from './employees-approved-for-leave-widget-drawer/employees-approved-for-leave-widget-drawer.utils';
import { fetchEmployeesOnLeave } from './employees-approved-for-leave-widget.api';
import { EmployeesApprovedForLeaveType } from './employees-approved-for-leave-widget.enums';
import { getEmployeesApprovedForLeaveForDrawer } from './employees-approved-for-leave-widget.utils';

export const useWidgetState = () => {
  const userId = AuthenticationService.getInstance().displayUserId;
  const { DATE_FILTERS } = useDateFilters();
  const [apiFilters, setApiFilters] = useState<DateRangeFilter>({
    startDate: DATE_FILTERS.TODAY.startDate,
    endDate: DATE_FILTERS.TODAY.endDate
  });
  const [columnFilters, setColumnFilters] = useState<TableFilters>({});
  const employeesApprovedForLeaveTypes: EmployeesApprovedForLeaveType[] = [];
  const [
    filteredEmployeesApprovedForLeaveForDrawer,
    setFilteredEmployeesApprovedForLeaveForDrawer
  ] = useState<EmployeesApprovedForLeaveType[]>([]);

  const memorisedDateRange = useMemo(() => {
    const absenceEmployeesOnLeaveSearch = new AbsenceEmployeesOnLeaveSearch();
    const startDate = moment(apiFilters.startDate);
    const endDate = moment(apiFilters.endDate);
    absenceEmployeesOnLeaveSearch.startDate(startDate);
    absenceEmployeesOnLeaveSearch.endDate(endDate);
    return absenceEmployeesOnLeaveSearch;
  }, [apiFilters]);

  const {
    state: { value: employeesApprovedForLeave, isPending, error }
  } = useAsync(fetchEmployeesOnLeave, [userId, memorisedDateRange], {
    shouldExecute: () => !!apiFilters
  });

  const onColumnFilter = (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => {
    let currentFilters = { ...columnFilters };

    if (column) {
      const currentColumnFilterValue = filterValues[column];
      const isCurrentColumnFilterValueArray = Array.isArray(currentColumnFilterValue);
      const isOtherThanArrayWithValue = !isCurrentColumnFilterValueArray && !!currentColumnFilterValue;
      const isNotEmptyArrayAsValue = isCurrentColumnFilterValueArray && !!((currentColumnFilterValue as []).length);

      if (isOtherThanArrayWithValue || isNotEmptyArrayAsValue) {
        currentFilters = { ...columnFilters, [column]: filterValues[column] };
      } else {
        delete currentFilters[column];
      }
    }

    setFilteredEmployeesApprovedForLeaveForDrawer(
      getFilteredEmployeesApprovedForLeave(
        getEmployeesApprovedForLeaveForDrawer(employeesApprovedForLeaveTypes),
        currentFilters
      )
    );
    setColumnFilters(currentFilters);
  };

  const onClearFilters = (column?: WidgetTableColumn) => {
    if (!column || !Object.values(WidgetTableColumn).includes(column)) {
      setFilteredEmployeesApprovedForLeaveForDrawer(
        getEmployeesApprovedForLeaveForDrawer(employeesApprovedForLeaveTypes)
      );
      setColumnFilters({});
      return;
    }
    const { [column]: columnToReset, ...restOfFilters } = columnFilters;

    setFilteredEmployeesApprovedForLeaveForDrawer(
      getFilteredEmployeesApprovedForLeave(
        getEmployeesApprovedForLeaveForDrawer(employeesApprovedForLeaveTypes),
        restOfFilters
      )
    );
    setColumnFilters({ ...restOfFilters });
  };

  const onApiFilter = (value: DateFilter) => {
    const key = value as keyof typeof DATE_FILTERS;

    setApiFilters({
      startDate: DATE_FILTERS[key].startDate,
      endDate: DATE_FILTERS[key].endDate
    });

    onClearFilters();
  };

  // We need to refresh filtered approved employees after each API call
  // We intentionally avoid the employeesApprovedForLeaveTypes watch, cause it changes every time we use the selector and it
  // creates a loop when we only need to set new values whenever the employeesApprovedForLeave has changed by API call
  useEffect(() => {
    setFilteredEmployeesApprovedForLeaveForDrawer(
      getEmployeesApprovedForLeaveForDrawer(employeesApprovedForLeaveTypes)
    );
  }, [employeesApprovedForLeave])

  return {
    employeesApprovedForLeave,
    setColumnFilters,
    filteredEmployeesApprovedForLeaveForDrawer,
    isPending,
    error,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    setFilteredEmployeesApprovedForLeaveForDrawer,
    employeesApprovedForLeaveTypes
  };
};
