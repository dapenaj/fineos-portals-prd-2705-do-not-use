/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage, RichFormattedMessage } from 'fineos-common';

import { WidgetHelpInfoPopover } from '../../widget-shared';
import styles from '../../my-dashboard.module.scss';

type EmployeesApprovedForLeaveWidgetHelpInfoProps = { isVisible?: boolean };

export const EmployeesApprovedForLeaveWidgetHelpInfo = ({
  isVisible = false
}: EmployeesApprovedForLeaveWidgetHelpInfoProps) =>
  isVisible ? (
    <WidgetHelpInfoPopover
      data-test-el="employees-approved-for-leave-help"
      triggerClassName={styles.helpPopoverTrigger}
      content={
        <>
          <FormattedMessage
            className={styles.helpPopoverInfoBlock}
            as="strong"
            id="HELP_INFO_DASHBOARDS.EMPLOYEES_ELEAVE_TITLE"
          />
          <RichFormattedMessage
            className={styles.helpPopoverInfoBlock}
            as="div"
            id="HELP_INFO_DASHBOARDS.EMPLOYEES_ELEAVE_PANEL"
          />
          <FormattedMessage
            as="strong"
            id="HELP_INFO_DASHBOARDS.FILTER_BY_LEAVE_TYPE"
          />
          <RichFormattedMessage
            className={styles.helpPopoverInfoBlock}
            as="div"
            id="HELP_INFO_DASHBOARDS.EMPLOYEES_ELEAVE_LEAVE_TYPE"
          />
          <FormattedMessage
            as="strong"
            id="HELP_INFO_DASHBOARDS.FILTER_BY_REASON"
          />
          <RichFormattedMessage
            className={styles.helpPopoverInfoBlock}
            as="div"
            id="HELP_INFO_DASHBOARDS.EMPLOYEES_ELEAVE_REASON"
          />
          <FormattedMessage
            as="strong"
            id="HELP_INFO_DASHBOARDS.FILTER_BY_GROUP"
          />
          <FormattedMessage id="HELP_INFO_DASHBOARDS.EMPLOYEES_ELEAVE_GROUP" />
        </>
      }
    />
  ) : null;
