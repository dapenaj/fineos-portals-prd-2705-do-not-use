/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage, Select } from 'fineos-common';

import { WidgetFilter } from '../../widget-shared';
import styles from '../../my-dashboard.module.scss';
import { CategoryFilter } from '../employees-approved-for-leave-widget.enums';

type EmployeesApprovedForLeaveCategoryFilterProps = {
  onChange: (value: unknown) => void;
  themedStyles: Record<string, string>;
  value: CategoryFilter;
};

export const EmployeesApprovedForLeaveCategoryFilter: React.FC<EmployeesApprovedForLeaveCategoryFilterProps> = ({
  onChange,
  themedStyles,
  value
}) => (
  <WidgetFilter
    label={
      <FormattedMessage
        className={themedStyles.filterText}
        id="WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_TOTAL_FILTER_CATEGORY"
      />
    }
    selectInput={
      <Select
        bordered={false}
        value={value}
        data-test-el="employees-approved-for-leave-filter-category"
        className={themedStyles.select}
        dropdownClassName={styles.selectDropdown}
        onChange={onChange}
        name="filter"
        optionElements={[
          {
            value: CategoryFilter.REASON,
            title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.REASON" />
          },
          {
            value: CategoryFilter.GROUP,
            title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.GROUP" />
          },
          {
            value: CategoryFilter.LEAVE_TYPE,
            title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.LEAVE_TYPE" />
          }
        ]}
        dropdownMatchSelectWidth={false}
      />
    }
  />
);
