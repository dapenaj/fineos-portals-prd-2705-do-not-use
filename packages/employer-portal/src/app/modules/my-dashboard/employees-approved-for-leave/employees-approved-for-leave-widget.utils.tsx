/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { groupBy as _groupBy } from 'lodash';

import {
  CategoryFilter,
  EmployeesApprovedForLeaveType,
  FormatMessageTimeApprovedProps
} from './employees-approved-for-leave-widget.enums';

export const getFilteredDataFlatStructure = (
  employeesApprovedForLeaveTypes: EmployeesApprovedForLeaveType[],
  selectedCategoryFilter: CategoryFilter
) => {
  switch (selectedCategoryFilter) {
    case CategoryFilter.REASON:
      return _groupBy(
        employeesApprovedForLeaveTypes,
        ({ notificationReason: { name } }) => name
      );
    case CategoryFilter.GROUP:
      const groupByEmployeeId = _groupBy(
        employeesApprovedForLeaveTypes,
        ({ employee: { id } }) => id
      );

      return _groupBy(groupByEmployeeId, ([value]) => {
        return value.adminGroup;
      });
    case CategoryFilter.LEAVE_TYPE:
      return _groupBy(
        groupByLeaveType(employeesApprovedForLeaveTypes),
        ({ leaveType }) => leaveType
      );
  }
};

const groupByLeaveType = (
  employeesApprovedForLeaveTypes: EmployeesApprovedForLeaveType[]
) => {
  const allLeavesInfo: {
    leaveType: string;
    hoursApproved: number;
  }[] = [];

  employeesApprovedForLeaveTypes.forEach(({ leavesInfo }) => {
    allLeavesInfo.push(...leavesInfo);
  });

  return allLeavesInfo;
};

export const getEmployeesApprovedForLeaveForDrawer = (
  employeesApprovedForLeaveTypes: EmployeesApprovedForLeaveType[]
) => {
  const results: EmployeesApprovedForLeaveType[] = [];
  employeesApprovedForLeaveTypes.forEach(
    (employeesApprovedForLeaveType, index) => {
      employeesApprovedForLeaveType.leavesInfo.forEach((leaveInfo, key) => {
        results.push({
          ...employeesApprovedForLeaveType,
          leaveType: leaveInfo.leaveType,
          leavesInfo: [leaveInfo],
          id: `${index} - ${key}`
        });
      });
    }
  );

  return results;
};

export const getFilteredEmployeesApprovedForLeaveForDrawer = (
  employeesApprovedForLeaveTypes: EmployeesApprovedForLeaveType[],
  selectedCategoryFilter: CategoryFilter,
  filterValues: string[]
) => {
  switch (selectedCategoryFilter) {
    case CategoryFilter.LEAVE_TYPE:
      return employeesApprovedForLeaveTypes.filter(({ leaveType }) =>
        filterValues.includes(leaveType)
      );
    case CategoryFilter.GROUP:
      return employeesApprovedForLeaveTypes.filter(({ adminGroup }) =>
        filterValues.includes(adminGroup)
      );
    case CategoryFilter.REASON:
      return employeesApprovedForLeaveTypes.filter(
        ({ notificationReason: { name } }) => filterValues.includes(name)
      );
  }
};

export const getTimeApprovedColumnText = (
  { minutes }: FormatMessageTimeApprovedProps,
  isSingleDay: boolean
) => {
  if (isSingleDay && minutes === 0) {
    return 'WIDGET.TIME_APPROVED_HOURS_TODAY';
  } else if (isSingleDay) {
    return 'WIDGET.TIME_APPROVED_TODAY';
  } else if (minutes === 0) {
    return 'WIDGET.TIME_APPROVED_HOURS';
  } else {
    return 'WIDGET.TIME_APPROVED';
  }
};
