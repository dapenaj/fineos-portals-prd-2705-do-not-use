/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';
import { BaseDomain } from 'fineos-js-api-client';

export enum CategoryFilter {
  REASON = 'REASON',
  GROUP = 'GROUP',
  LEAVE_TYPE = 'LEAVE_TYPE'
}

export enum LeaveType {
  CONTINUOUS = 'Time off period',
  INTERMITTENT = 'Episodic',
  REDUCED_SCHEDULE = 'Reduced Schedule'
}

export const filterName = {
  [CategoryFilter.REASON]: 'notificationReason',
  [CategoryFilter.GROUP]: 'adminGroup',
  [CategoryFilter.LEAVE_TYPE]: 'leaveType'
};

export const getLeaveTypeTranslation = (leaveType: string) => {
  switch (leaveType) {
    case LeaveType.CONTINUOUS:
      return 'WIDGET.LEAVE_TYPE.CONTINUOUS_TIME';
    case LeaveType.INTERMITTENT:
      return 'WIDGET.LEAVE_TYPE.INTERMITTENT_TIME';
    default:
      return 'WIDGET.LEAVE_TYPE.REDUCED_SCHEDULE';
  }
};

export type EmployeeType = {
  id: string;
  firstName: string;
  lastName: string;
};

export type LeavesInfoType = {
  leaveType: string;
  hoursApproved: number;
  timeApproved: React.ReactNode;
  timeApprovedText: string;
};

export type FormatMessageTimeApprovedProps = {
  hours: number;
  minutes: number;
};

// flat structure for filter and show it into the table
export type EmployeesApprovedForLeaveType = {
  id: string;
  employee: EmployeeType;
  adminGroup: string;
  notificationCase: { id: string };
  notificationReason: BaseDomain;
  leavesInfo: LeavesInfoType[];
  leaveType: string;
  isInGroupWithPrevious?: boolean;
  isInGroupWithNext?: boolean;
};
