/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import moment from 'moment';
import {
  NotificationCase,
  LeaveApproveDetails,
  AbsenceDays
} from 'fineos-js-api-client';
import { useIntl } from 'react-intl';
import { groupBy as _groupBy } from 'lodash';
import { FormattedMessage } from 'fineos-common';

import {
  GraphWidget,
  EnumDomain,
  getEnumDomainTranslation,
  DateFilter
} from '../../../shared';
import { getDateFilters, useDateFilters } from '../widget-shared';
import {
  useWidgetThemedStyles,
  getDateFilterValue,
  getDateSelectedRange
} from '../dashboard-widget.utils';
import { WidgetProps } from '../dashboard.type';
import styles from '../my-dashboard.module.scss';

import {
  CategoryFilter,
  getLeaveTypeTranslation,
  EmployeesApprovedForLeaveType,
  EmployeeType,
  filterName
} from './employees-approved-for-leave-widget.enums';
import {
  getFilteredEmployeesApprovedForLeaveForDrawer,
  getFilteredDataFlatStructure,
  getEmployeesApprovedForLeaveForDrawer,
  getTimeApprovedColumnText
} from './employees-approved-for-leave-widget.utils';
import { EmployeesApprovedForLeaveWidgetDrawer } from './employees-approved-for-leave-widget-drawer/employees-approved-for-leave-widget-drawer.component';
import { EmployeesApprovedForLeaveWidgetHelpInfo } from './ui';
import {
  EmployeesApprovedForLeaveDateFilter,
  EmployeesApprovedForLeaveCategoryFilter
} from './ui';
import { useWidgetState } from './use-widget-state.hook';

export const EmployeesApprovedForLeaveWidget: React.FC<WidgetProps> = ({
  helpInfoEnabled
}) => {
  const { DATE_FILTERS } = useDateFilters();
  const [selectedCategoryFilter, setSelectedCategoryFilter] = useState<
    CategoryFilter
  >(CategoryFilter.REASON);
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);
  const intl = useIntl();
  const themedStyles = useWidgetThemedStyles();

  const {
    employeesApprovedForLeave,
    setColumnFilters,
    filteredEmployeesApprovedForLeaveForDrawer,
    isPending,
    error,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    setFilteredEmployeesApprovedForLeaveForDrawer,
    employeesApprovedForLeaveTypes
  } = useWidgetState();

  const filteredEmployeesApprovedForLeave: {
    name: string;
    value: NotificationCase[] | LeaveApproveDetails[];
  }[] = [];

  const getGroupedDataLabel = (key: string) => {
    switch (selectedCategoryFilter) {
      case CategoryFilter.REASON:
        return intl.formatMessage({
          id: `${getEnumDomainTranslation(EnumDomain.NOTIFICATION_REASON, key)}`
        });
      case CategoryFilter.LEAVE_TYPE:
        return intl.formatMessage({
          id: getLeaveTypeTranslation(key)
        });
      default:
        return key;
    }
  };

  const selectedDateRange = getDateSelectedRange(apiFilters, DATE_FILTERS);

  employeesApprovedForLeave?.forEach(
    ({ id, firstName, lastName, notificationCases }) => {
      const result: EmployeeType = { id, firstName, lastName } as EmployeeType;
      notificationCases.forEach(
        ({ adminGroup, caseNumber, notificationReason, absenceDays }) => {
          const leavesInfo: LeaveApproveDetails[] = [];
          absenceDays.forEach((absenceDay: AbsenceDays) => {
            leavesInfo.push(...absenceDay.leaveApproveDetails);
          });
          const groupedByLeaveType = _groupBy(
            leavesInfo,
            ({ leaveType }: LeaveApproveDetails) => leaveType.name
          );

          const preparedLeavesInfo = Object.entries(groupedByLeaveType).map(
            ([leaveType, value]) => {
              const hoursApproved = value.reduce(
                (count: number, { timeApproved }: LeaveApproveDetails) => {
                  return count + Number(timeApproved) * 60;
                },
                0
              );
              let hours = Math.floor(hoursApproved / 60);
              let minutes = hoursApproved % 60;

              let formatMessageValues = {
                hours,
                minutes,
                from: moment(absenceDays[0].date).format('MMM DD, YYYY'),
                to: moment(absenceDays[absenceDays.length - 1].date).format(
                  'MMM DD, YYYY'
                )
              };

              const timeApprovedLiteral = getTimeApprovedColumnText(
                formatMessageValues,
                selectedDateRange === DateFilter.TODAY ||
                  absenceDays.length === 1
              );

              return {
                leaveType,
                hoursApproved,
                timeApprovedText: intl.formatMessage(
                  {
                    id: timeApprovedLiteral
                  },
                  formatMessageValues
                ),
                timeApproved: (
                  <FormattedMessage
                    id={timeApprovedLiteral}
                    values={formatMessageValues}
                  />
                )
              };
            }
          );

          employeesApprovedForLeaveTypes.push({
            employee: { ...result } as EmployeeType,
            adminGroup,
            notificationCase: { id: caseNumber },
            notificationReason,
            leavesInfo: preparedLeavesInfo
          } as EmployeesApprovedForLeaveType);
        }
      );
    }
  );

  const filteredDataSlice = getFilteredDataFlatStructure(
    employeesApprovedForLeaveTypes,
    selectedCategoryFilter
  );

  filteredEmployeesApprovedForLeave.push(
    ...Object.entries(filteredDataSlice).map(([key, value]) => ({
      name: key,
      value
    }))
  );

  const graphData = filteredEmployeesApprovedForLeave.map(
    ({ name, value }) => ({
      label: getGroupedDataLabel(name),
      value: value.length,
      filterValues: [name]
    })
  );

  const total = employeesApprovedForLeave?.length;

  const sortedEmployeesApprovedForLeaveType = (
    employeesApprovedForLeave: EmployeesApprovedForLeaveType[]
  ) =>
    employeesApprovedForLeave.sort((a, b) =>
      `${a.employee.firstName} ${a.employee.lastName}`.localeCompare(
        `${b.employee.firstName} ${b.employee.lastName}`
      )
    );

  const handleDrawerClose = () => {
    setIsDrawerVisible(false);
    setColumnFilters({});
    setFilteredEmployeesApprovedForLeaveForDrawer(
      sortedEmployeesApprovedForLeaveType(
        filteredEmployeesApprovedForLeaveForDrawer
      )
    );
  };

  const onSlideClick = (filterValues: string[]) => {
    setColumnFilters({ [filterName[selectedCategoryFilter]]: filterValues });
    setFilteredEmployeesApprovedForLeaveForDrawer(
      getFilteredEmployeesApprovedForLeaveForDrawer(
        getEmployeesApprovedForLeaveForDrawer(employeesApprovedForLeaveTypes),
        selectedCategoryFilter,
        filterValues
      )
    );
  };

  const handleOpenDrawer = (filterValues?: string[]) => {
    filterValues
      ? onSlideClick(filterValues)
      : setFilteredEmployeesApprovedForLeaveForDrawer(
          getEmployeesApprovedForLeaveForDrawer(employeesApprovedForLeaveTypes)
        );

    setIsDrawerVisible(true);
  };

  return (
    <div
      data-test-el="employees-approved-for-leave"
      className={styles.container}
    >
      <EmployeesApprovedForLeaveWidgetHelpInfo isVisible={helpInfoEnabled} />
      <GraphWidget
        data={graphData}
        totalNumber={total}
        totalNumberLabel={
          <FormattedMessage
            id="WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_TOTAL"
            values={{ total }}
          />
        }
        totalNumberLabelAriaLabel={
          <FormattedMessage
            id="WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_TOTAL_ARIA_LABEL"
            values={{ total }}
          />
        }
        isLoading={isPending}
        error={error}
        emptyMessage={
          <FormattedMessage
            id="WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_EMPTY"
            role="status"
          />
        }
        onViewDetails={handleOpenDrawer}
        filters={[
          <>
            <EmployeesApprovedForLeaveDateFilter
              value={getDateFilterValue(
                intl,
                getDateFilters(intl),
                selectedDateRange
              )}
              themedStyles={themedStyles}
              onChange={(value: unknown) => onApiFilter(value as DateFilter)}
            />
            <EmployeesApprovedForLeaveCategoryFilter
              onChange={(value: unknown) => {
                setSelectedCategoryFilter(value as CategoryFilter);
              }}
              themedStyles={themedStyles}
              value={selectedCategoryFilter}
            />
          </>
        ]}
      />
      <EmployeesApprovedForLeaveWidgetDrawer
        handleClose={handleDrawerClose}
        onColumnFilter={onColumnFilter}
        onClearFilters={onClearFilters}
        onApiFilter={onApiFilter}
        columnFilters={columnFilters}
        isDrawerVisible={isDrawerVisible}
        isPending={isPending}
        apiFilters={apiFilters}
        totalEmployeesApprovedForLeave={total || 0}
        visibleEmployeesApprovedForLeave={sortedEmployeesApprovedForLeaveType(
          filteredEmployeesApprovedForLeaveForDrawer
        )}
        sortedData={employeesApprovedForLeaveTypes}
        isHelpInfoEnabled={helpInfoEnabled}
      />
    </div>
  );
};
