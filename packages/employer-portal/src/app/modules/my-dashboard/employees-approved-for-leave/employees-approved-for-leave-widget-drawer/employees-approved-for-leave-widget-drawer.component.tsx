/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { useIntl } from 'react-intl';
import { uniqBy as _uniqBy } from 'lodash';
import classNames from 'classnames';
import {
  Drawer,
  DrawerWidth,
  FilterItemsCountTag,
  ContentRenderer,
  DrawerFooter,
  PAGINATION_THRESHOLD
} from 'fineos-common';

import {
  TableFilters,
  WidgetTableColumn,
  DateFilter
} from '../../../../shared';
import {
  getDateFilterValue,
  getDateSelectedRange
} from '../../dashboard-widget.utils';
import { DateRangeFilter } from '../../dashboard.type';
import styles from '../../my-dashboard.module.scss';
import {
  getDateFilters,
  useDateFilters,
  WidgetDrawerFooter
} from '../../widget-shared';
import { EmployeesApprovedForLeaveType } from '../employees-approved-for-leave-widget.enums';
import { EmployeesApprovedForLeaveDrawerDateFilter } from '../ui/employees-approved-for-leave-drawer-date-filter.component';

import { useTransformEmployeesApprovedForLeave } from './use-transform-employees-approved-for-leave.hook';
import { EmployeesApprovedForLeaveTable } from './employees-approved-for-leave-table.component';

type EmployeesApprovedForLeaveWidgetDrawerProps = {
  isDrawerVisible: boolean;
  isPending: boolean;
  apiFilters: DateRangeFilter;
  sortedData: EmployeesApprovedForLeaveType[];
  totalEmployeesApprovedForLeave: number;
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void;
  onClearFilters: (column?: WidgetTableColumn) => void;
  handleClose: () => void;
  onApiFilter: (value: DateFilter) => void;
  columnFilters: TableFilters;
  visibleEmployeesApprovedForLeave: EmployeesApprovedForLeaveType[];
  isHelpInfoEnabled: boolean;
};

export const EmployeesApprovedForLeaveWidgetDrawer: React.FC<EmployeesApprovedForLeaveWidgetDrawerProps> = ({
  isDrawerVisible,
  isPending,
  apiFilters,
  totalEmployeesApprovedForLeave,
  onColumnFilter,
  onClearFilters,
  handleClose,
  onApiFilter,
  columnFilters,
  visibleEmployeesApprovedForLeave,
  sortedData,
  isHelpInfoEnabled
}) => {
  const intl = useIntl();
  const { DATE_FILTERS } = useDateFilters();

  const countByEmployeeId = _uniqBy(
    visibleEmployeesApprovedForLeave,
    ({ employee: { id } }) => id
  ).length;

  const isFilterBadgeVisible =
    !isPending && !!Object.keys(columnFilters).length;

  const drawerTitle = (
    <div className={styles.drawerHeaderContent}>
      <EmployeesApprovedForLeaveDrawerDateFilter
        onChange={(value: unknown) => onApiFilter(value as DateFilter)}
        isPending={isPending}
        total={totalEmployeesApprovedForLeave}
        isHelpInfoEnabled={isHelpInfoEnabled}
        value={getDateFilterValue(
          intl,
          getDateFilters(intl),
          getDateSelectedRange(apiFilters, DATE_FILTERS)
        )}
      />
      <FilterItemsCountTag
        isVisible={isFilterBadgeVisible}
        filteredItemsCount={countByEmployeeId}
        totalItemsCount={totalEmployeesApprovedForLeave}
        onClearFilters={onClearFilters}
      />
    </div>
  );

  const downloadReadyEmployeesApprovedForLeave = useTransformEmployeesApprovedForLeave(
    visibleEmployeesApprovedForLeave
  );

  return (
    <Drawer
      ariaLabelId="WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_FILTER_DATE_DRAWER_HEADER"
      ariaLabelValues={{ count: totalEmployeesApprovedForLeave }}
      className={styles.drawer}
      headerClassName={styles.drawerHeader}
      bodyClassName={classNames({
        [styles.drawerBody]:
          visibleEmployeesApprovedForLeave.length > PAGINATION_THRESHOLD
      })}
      isVisible={isDrawerVisible}
      onClose={handleClose}
      width={DrawerWidth.FULL_PAGE}
      title={drawerTitle}
    >
      <ContentRenderer
        shouldNotDestroyOnLoading={true}
        spinnerClassName={styles.spinnerContainer}
        isLoading={isPending}
        isEmpty={false}
      >
        <EmployeesApprovedForLeaveTable
          isPending={isPending}
          columnFilters={columnFilters}
          onColumnFilter={onColumnFilter}
          onClearFilters={onClearFilters}
          sortedData={sortedData}
          visibleEmployeesApprovedForLeave={visibleEmployeesApprovedForLeave}
          apiFilters={apiFilters}
        />
      </ContentRenderer>
      <DrawerFooter>
        <WidgetDrawerFooter
          isVisible={!isPending}
          dataLength={visibleEmployeesApprovedForLeave.length}
          downloadData={downloadReadyEmployeesApprovedForLeave}
          fileNameTranslationId="EXPORT_OUTSTANDING_LIST.FILE_NAME.EMPLOYEES_APPROVED_FOR_LEAVE"
          onCloseClick={handleClose}
        />
      </DrawerFooter>
    </Drawer>
  );
};
