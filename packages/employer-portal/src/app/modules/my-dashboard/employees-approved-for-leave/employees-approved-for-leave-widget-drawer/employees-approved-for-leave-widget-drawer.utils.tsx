/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';
import { BaseDomain } from 'fineos-js-api-client';
import { IntlShape } from 'react-intl';
import { Link, FormattedMessage, SortOrder } from 'fineos-common';
import classNames from 'classnames';
import { uniqBy as _uniqBy } from 'lodash';

import {
  WidgetTableColumn,
  TableFilters,
  getReasonNameFilters,
  filterByFirstName,
  filterByLastName,
  filterByAdminGroup,
  filterByReason,
  filterByNotification,
  filterByLeaveType
} from '../../../../shared';
import {
  getConfigForCustomerFilter,
  getConfigForCheckboxFilter,
  getConfigForInputFilter,
  getNotificationReasonRender,
  getWithTwoLineEllipsisRender,
  getCustomerRender,
  getAdminGroupRender
} from '../../dashboard-table.utils';
import { FilterIconLabel } from '../../dashboard.type';
import { WidgetDrawerColumnSorter } from '../../widget-shared';
import {
  EmployeeType,
  LeavesInfoType,
  EmployeesApprovedForLeaveType,
  getLeaveTypeTranslation
} from '../employees-approved-for-leave-widget.enums';

const getEmployeeColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.EMPLOYEE" />,
  dataIndex: 'employee',
  key: 'id',
  className: 'groupable-row-cell',
  width: '20%',
  render: ({ id, firstName, lastName }: EmployeeType) =>
    getCustomerRender(id, firstName, lastName),
  defaultSortOrder: SortOrder.ASCEND,
  sorter: (
    a: EmployeesApprovedForLeaveType,
    b: EmployeesApprovedForLeaveType
  ) =>
    `${a.employee.firstName} ${a.employee.lastName}`.localeCompare(
      `${b.employee.firstName} ${b.employee.lastName}`
    ),
  ...getConfigForCustomerFilter(columnFilters, onColumnFilter, onClearFilters)
});

const getAdminGroupColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.GROUP" />,
  dataIndex: 'adminGroup',
  key: 'adminGroup',
  className: 'groupable-row-cell',
  width: '20%',
  render: (adminGroup: string) => getAdminGroupRender(adminGroup, themedStyles),
  sorter: (
    a: EmployeesApprovedForLeaveType,
    b: EmployeesApprovedForLeaveType
  ) => a.adminGroup.localeCompare(b.adminGroup),
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.ADMIN_GROUP,
    WidgetTableColumn.ADMIN_GROUP
  )
});

const getNotificationReasonColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  intl: IntlShape,
  themedStyles: Record<string, string>,
  sortedData: EmployeesApprovedForLeaveType[]
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.REASON" />,
  dataIndex: 'notificationReason',
  key: 'notificationReason',
  render: (notificationReason: BaseDomain) =>
    getNotificationReasonRender(notificationReason, themedStyles),
  filters: getReasonNameFilters<EmployeesApprovedForLeaveType>(
    sortedData,
    ({ notificationReason }) => notificationReason.name,
    intl
  ),
  ...getConfigForCheckboxFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles
  )
});

const getCaseNumberColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.CASE" />,
  dataIndex: 'notificationCase',
  key: 'id',
  width: '12%',
  render: ({ id }: { id: string }) => (
    <Link to={`/notification/${id}`}>{id}</Link>
  ),
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.CASE_NUMBER,
    WidgetTableColumn.CASE_NUMBER,
    1
  )
});

const getLeaveTypeFilters = (
  employeesApprovedForLeaveTypes: EmployeesApprovedForLeaveType[],
  intl: IntlShape
) => {
  const levelTypes: { text: string; value: string }[] = [];
  employeesApprovedForLeaveTypes.forEach(({ leavesInfo }) => {
    levelTypes.push(
      ...Array.from(new Set(leavesInfo.map(({ leaveType }) => leaveType))).map(
        item => ({
          text: intl.formatMessage({
            id: getLeaveTypeTranslation(item)
          }),
          value: item
        })
      )
    );
  });

  return _uniqBy(levelTypes, 'value');
};

const getLeaveTypeColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  intl: IntlShape,
  themedStyles: Record<string, string>,
  sortedData: EmployeesApprovedForLeaveType[]
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.LEAVE_TYPE" />,
  dataIndex: 'leaveType',
  key: 'leaveType',
  render: (leaveType: string) =>
    getWithTwoLineEllipsisRender(
      intl.formatMessage({
        id: getLeaveTypeTranslation(leaveType)
      }),
      themedStyles
    ),
  filters: getLeaveTypeFilters(sortedData, intl),
  ...getConfigForCheckboxFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles,
    FilterIconLabel.LEAVE_TYPE,
    WidgetTableColumn.LEAVE_TYPE,
    'WIDGET.TABLE_LABELS.LEAVE_TYPE'
  )
});

const getTimeApproved = (
  themedStyles: Record<string, string>,
  apiFilters?: string
) => ({
  title: (
    <FormattedMessage
      id={`WIDGET.FILTER_OPTIONS.${apiFilters}_WITHOUT_DATE_RANGE`}
    />
  ),
  dataIndex: 'leavesInfo',
  key: 'leavesInfo',
  width: '12%',
  render: (leavesInfo: LeavesInfoType[]) => (
    <div className={themedStyles.defaultText}>{leavesInfo[0].timeApproved}</div>
  )
});

export const getTableColumns = ({
  columnFilters,
  onColumnFilter,
  onClearFilters,
  intl,
  themedStyles,
  sortedData,
  apiFilters
}: {
  columnFilters: TableFilters;
  onColumnFilter: (filters: TableFilters) => void;
  onClearFilters: (column: WidgetTableColumn) => void;
  intl: IntlShape;
  themedStyles: Record<string, string>;
  sortedData: EmployeesApprovedForLeaveType[];
  apiFilters?: string;
}) => [
  getEmployeeColumn(columnFilters, onColumnFilter, onClearFilters),
  getAdminGroupColumn(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles
  ),
  getCaseNumberColumn(columnFilters, onColumnFilter, onClearFilters),
  getNotificationReasonColumn(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    intl,
    themedStyles,
    sortedData
  ),
  getLeaveTypeColumn(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    intl,
    themedStyles,
    sortedData
  ),
  getTimeApproved(themedStyles, apiFilters)
];

export const getFilteredEmployeesApprovedForLeave = (
  employeesApprovedForLeave: EmployeesApprovedForLeaveType[],
  columnFilters: TableFilters
) =>
  employeesApprovedForLeave.filter(
    ({
      employee: { firstName, lastName },
      adminGroup,
      notificationReason: { name: reasonName },
      notificationCase: { id: notificationCaseId },
      leaveType
    }) =>
      filterByFirstName(columnFilters, firstName) &&
      filterByLastName(columnFilters, lastName) &&
      filterByAdminGroup(columnFilters, adminGroup) &&
      filterByReason(columnFilters, reasonName) &&
      filterByNotification(columnFilters, notificationCaseId) &&
      filterByLeaveType(columnFilters, leaveType)
  );

export const getTableRowClassName = ({
  isInGroupWithNext,
  isInGroupWithPrevious
}: EmployeesApprovedForLeaveType) =>
  classNames({
    'employees-afl-table-row-group-header': isInGroupWithNext,
    'employees-afl-table-row-group-element': isInGroupWithPrevious
  });

function checkIsInGroupWithPrevious(
  employees: EmployeesApprovedForLeaveType[],
  employee: EmployeesApprovedForLeaveType,
  index: number
) {
  const prevIndex = Math.max(index - 1, 0);
  const prevEmployee = employees[prevIndex];

  return (
    index > 0 &&
    prevEmployee.employee.firstName === employee.employee.firstName &&
    prevEmployee.employee.lastName === employee.employee.lastName &&
    prevEmployee.employee.id === employee.employee.id
  );
}

function checkIsInGroupWithNext(
  employees: EmployeesApprovedForLeaveType[],
  employee: EmployeesApprovedForLeaveType,
  index: number
) {
  const nextIndex = Math.min(index + 1, employees.length - 1);
  const nextEmployee = employees[nextIndex];
  return (
    index < employees.length - 1 &&
    nextEmployee.employee.firstName === employee.employee.firstName &&
    nextEmployee.employee.lastName === employee.employee.lastName &&
    nextEmployee.employee.id === employee.employee.id
  );
}

/**
 * This function flags every row to allow simulating grouping by employee.
 *
 * it should only be used when the given array is ordered by employee name
 *
 * @param drawerEmployeesApprovedForLeave
 */
export const flagEmployeesApprovedForLeaveAsGroupRow = (
  drawerEmployeesApprovedForLeave: EmployeesApprovedForLeaveType[]
) =>
  drawerEmployeesApprovedForLeave.map((employee, index) => ({
    ...employee,
    isInGroupWithPrevious: checkIsInGroupWithPrevious(
      drawerEmployeesApprovedForLeave,
      employee,
      index
    ),
    isInGroupWithNext: checkIsInGroupWithNext(
      drawerEmployeesApprovedForLeave,
      employee,
      index
    )
  }));
