/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  usePermissions,
  ViewNotificationPermissions,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Button,
  ButtonType
} from 'fineos-common';
import classNames from 'classnames';

import { PageLayout } from '../../shared';
import { OutstandingNotificationsWidget } from '../outstanding-notifications';
import { WelcomeMessage } from '../welcome-message';

import { EmployeesExpectedToRTWWidget } from './employees-expected-to-rtw-widget';
import { NotificationsCreatedWidget } from './notifications-created-widget';
import { DashboardCustomizationPanel } from './dashboard-customization-panel';
import { useDashboardConfig } from './widget-shared';
import { EmployeesApprovedForLeaveWidget } from './employees-approved-for-leave/employees-approved-for-leave-widget.component';

import { DashboardConfig } from './dashboard.type';
import { DecisionsMadeWidget } from './decisions-made-widget';
import styles from './my-dashboard.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  }
}));

type MyDashboardProps = {
  helpInfoEnabled: boolean;
};

export const MyDashboard: React.FC<MyDashboardProps> = ({
  helpInfoEnabled
}) => {
  const WIDGETS = {
    employeesExpectedToRTW: (
      <EmployeesExpectedToRTWWidget helpInfoEnabled={helpInfoEnabled} key={2} />
    ),
    notificationsCreated: (
      <NotificationsCreatedWidget helpInfoEnabled={helpInfoEnabled} key={3} />
    ),
    decisionsMade: (
      <DecisionsMadeWidget helpInfoEnabled={helpInfoEnabled} key={4} />
    ),
    employeesApprovedForLeave: (
      <EmployeesApprovedForLeaveWidget
        helpInfoEnabled={helpInfoEnabled}
        key={5}
      />
    )
  };

  const hasViewNotificationsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_NOTIFICATIONS
  );

  const hasViewAbsencePermission = usePermissions(
    ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
  );

  const hasViewEmployeesLeaveHoursPermission = usePermissions(
    ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS
  );

  const initialConfig: DashboardConfig = {
    employeesExpectedToRTW: {
      id: 'WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK',
      isVisible: true,
      hasPermission: true
    },
    notificationsCreated: {
      id: 'WIDGET.NOTIFICATIONS_CREATED',
      isVisible: true,
      hasPermission: true
    },
    decisionsMade: {
      id: 'WIDGET.LEAVES_DECIDED',
      isVisible: true,
      hasPermission: hasViewAbsencePermission
    },
    employeesApprovedForLeave: {
      id: 'WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE',
      isVisible: true,
      hasPermission: hasViewEmployeesLeaveHoursPermission
    }
  };

  const [dashboardConfig, setDashboardConfig] = useDashboardConfig(
    initialConfig
  );

  const [
    isCustomizationPanelVisible,
    setIsCustomizationPanelVisible
  ] = useState(false);

  const getWidgets = () =>
    Object.entries(dashboardConfig).map(
      ([key, configItem]) =>
        configItem.isVisible &&
        configItem.hasPermission &&
        WIDGETS[key as keyof DashboardConfig]
    );

  const themedStyles = useThemedStyles();
  return (
    <PageLayout
      sideBar={null}
      contentHeader={
        hasViewNotificationsPermission ? (
          <div className={styles.headerContainer}>
            <FormattedMessage
              data-test-el="my-dashboard-header"
              id="PAGE_TITLES.MY_DASHBOARD"
              as="h1"
              className={classNames(styles.header, themedStyles.header)}
            />
            <FormattedMessage
              data-test-el="panel-customization"
              id="WIDGET.SHOW_HIDE_PANELS"
              as={Button}
              buttonType={ButtonType.LINK}
              onClick={() => setIsCustomizationPanelVisible(true)}
            />
          </div>
        ) : null
      }
      showBackgroundImage={!hasViewNotificationsPermission}
    >
      <DashboardCustomizationPanel
        isDrawerVisible={isCustomizationPanelVisible}
        onClose={() => setIsCustomizationPanelVisible(false)}
        config={dashboardConfig}
        setConfig={(config: DashboardConfig) => {
          setDashboardConfig(config);
        }}
      />
      {hasViewNotificationsPermission ? (
        <div className={styles.widgets}>
          <OutstandingNotificationsWidget
            helpInfoEnabled={helpInfoEnabled}
            key={1}
          />
          {getWidgets()}
        </div>
      ) : (
        <WelcomeMessage />
      )}
    </PageLayout>
  );
};
