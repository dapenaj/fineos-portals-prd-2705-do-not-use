/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { IntlShape } from 'react-intl';
import moment from 'moment';
import {
  createThemedStyles,
  textStyleToCss,
  pxToRem,
  getFocusedElementShadow,
  darkenColor
} from 'fineos-common';

import { DateFilterType, DateRangeFilter } from './dashboard.type';

export const useWidgetThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  },
  filterText: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseTextSemiBold)
  },
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.displaySmall, {
      suppressLineHeight: true
    })
  },
  select: {
    '& .ant-select-arrow': {
      ...textStyleToCss(theme.typography.baseText, {
        suppressLineHeight: true
      })
    },
    '& .ant-select-selector .ant-select-selection-item, .ant-select-arrow': {
      color: `${theme.colorSchema.primaryAction} !important`
    },
    '&.ant-select-borderless.ant-select-focused .ant-select-selector': {
      borderColor: `${theme.colorSchema.primaryAction} !important`,
      boxShadow: `${getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })} !important`
    }
  },
  drawerSelect: {
    '& .ant-select-selector .ant-select-selection-item': {
      ...textStyleToCss(theme.typography.displaySmall, {
        suppressLineHeight: true
      }),
      color: `${theme.colorSchema.primaryAction} !important`
    },
    '& .ant-select-arrow': {
      ...textStyleToCss(theme.typography.panelTitle, {
        suppressLineHeight: true
      }),
      color: `${theme.colorSchema.primaryAction} !important`
    },
    '&.ant-select-borderless.ant-select-focused .ant-select-selector': {
      borderColor: `${theme.colorSchema.primaryAction} !important`,
      boxShadow: `${getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })} !important`
    }
  },
  twoLineEllipsis: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8,
    maxHeight: pxToRem(theme.typography.baseText.lineHeight! * 2)
  },
  checkIcon: {
    ...textStyleToCss(theme.typography.baseText),
    color: darkenColor(theme.colorSchema, 'successColor')
  },
  timesIcon: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.attentionColor
  },
  tableCell: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  label: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  actionTableHeader: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  defaultText: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const getDateFilterValue = (
  intl: IntlShape,
  dateFilters: DateFilterType,
  value?: string
) => {
  if (value) {
    const startDate = moment(dateFilters[value].startDate).format('MMM DD');
    const endDate = moment(dateFilters[value].endDate).format('MMM DD');
    return intl.formatMessage(
      { id: `WIDGET.FILTER_OPTIONS.${value}` },
      {
        dateRange: `${startDate} - ${endDate}`
      }
    );
  }
};

export const getDateSelectedRange = (
  selectedDateRange: DateRangeFilter,
  DATE_FILTERS: DateFilterType
) =>
  Object.keys(DATE_FILTERS).find(
    key =>
      DATE_FILTERS[key as keyof typeof DATE_FILTERS].startDate ===
        selectedDateRange.startDate &&
      DATE_FILTERS[key as keyof typeof DATE_FILTERS].endDate ===
        selectedDateRange.endDate
  );
