/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useState, useEffect } from 'react';
import { GroupClientNotification } from 'fineos-js-api-client';
import { useAsync } from 'fineos-common';

import {
  DateFilter,
  fetchNotifications,
  NotificationAPIFilters,
  TableFilters,
  WidgetTableColumn,
  filterByJobTitle,
  filterByAdminGroup,
  filterByFirstName,
  filterByLastName,
  initialTableFilters,
  filterByNotification,
  filterByReason,
  filterByDI
} from '../../../shared';
import { WidgetKey } from '../dashboard.type';
import { CategoryFilter } from '../notifications-created-widget/notification-created-widget.enum';

import { useDateFilters } from './use-date-filters.hook';

export const useWidgetState = (widgetKey: WidgetKey) => {
  const { DATE_FILTERS } = useDateFilters();
  const [columnFilters, setColumnFilters] = useState<TableFilters>(
    initialTableFilters
  );
  const [apiFilters, setApiFilters] = useState<NotificationAPIFilters>({
    [widgetKey]: DATE_FILTERS[DateFilter.TODAY]
  });
  const [filteredNotifications, setFilteredNotifications] = useState<
    GroupClientNotification[]
  >([]);

  const {
    state: {
      value: { data: notifications, total: totalNotifications },
      isPending,
      error
    }
  } = useAsync(fetchNotifications, [apiFilters], {
    defaultValue: { data: [], total: 0 }
  });

  useEffect(() => {
    setFilteredNotifications(notifications);
  }, [notifications]);

  const onApiFilter = (value: DateFilter) => {
    setApiFilters({ [widgetKey]: DATE_FILTERS[value] });

    onClearFilters();
  };

  const onColumnFilter = (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => {
    let currentFilters = { ...columnFilters };
    if (column) {
      currentFilters = { ...columnFilters, [column]: filterValues[column] };
    }

    setColumnFilters(currentFilters);
    filterColumnNotifications(currentFilters, notifications);
  };

  const filterColumnNotifications = (
    currentFilters: TableFilters,
    currentNotifications: GroupClientNotification[]
  ) => {
    setFilteredNotifications(
      currentNotifications.filter((notification: GroupClientNotification) => {
        const {
          customer,
          adminGroup,
          caseNumber,
          notificationReason
        } = notification;
        return (
          filterByJobTitle(currentFilters, customer.jobTitle) &&
          filterByAdminGroup(currentFilters, adminGroup) &&
          filterByFirstName(currentFilters, customer.firstName) &&
          filterByLastName(currentFilters, customer.lastName) &&
          filterByNotification(currentFilters, caseNumber) &&
          filterByReason(currentFilters, notificationReason.name) &&
          filterByDI(currentFilters, notification)
        );
      })
    );
  };

  const onClearFilters = (column?: WidgetTableColumn) => {
    if (!column || !Object.values(WidgetTableColumn).includes(column)) {
      setFilteredNotifications(notifications);
      setColumnFilters(initialTableFilters);
      return;
    }

    const tempFilters = {
      ...columnFilters,
      [column]: initialTableFilters[column]
    };
    setColumnFilters(tempFilters);
    filterColumnNotifications(tempFilters, notifications);
  };

  const getFilterName = (selectedCategoryFilter: string) => {
    switch (selectedCategoryFilter) {
      case CategoryFilter.REASON:
        return 'notificationReason';
      default:
        return 'disabilityRelated';
    }
  };

  const setColumnFiltersByCategory = (
    filterValues?: string[],
    selectedCategoryFilter?: string
  ) => {
    if (filterValues && selectedCategoryFilter) {
      if (selectedCategoryFilter === 'adminGroup') {
        setColumnFilters({
          adminGroup: filterValues[0]
        });
      } else {
        setColumnFilters({
          [getFilterName(selectedCategoryFilter)]: filterValues
        });
      }
    }
  };

  const onSliceClick = (
    sliceNotifications: GroupClientNotification[],
    filterValues?: string[],
    selectedCategoryFilter?: string
  ) => {
    setColumnFiltersByCategory(filterValues, selectedCategoryFilter);
    setFilteredNotifications(sliceNotifications);
  };

  return {
    notifications,
    filteredNotifications,
    totalNotifications,
    isPending,
    error,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    onSliceClick
  };
};
