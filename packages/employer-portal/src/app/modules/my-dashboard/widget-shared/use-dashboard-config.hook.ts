/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useState } from 'react';
import { encryptedUserId } from 'fineos-common';

import { AuthenticationService } from '../../../shared';

import { DashboardConfig } from '../dashboard.type';

export const useDashboardConfig = (
  initialConfig: DashboardConfig
): [DashboardConfig, (dashboardConfig: DashboardConfig) => void] => {
  const userId = AuthenticationService.getInstance().displayUserId || '';

  const setDashboardConfig = (config: DashboardConfig) => {
    setConfig(config);
    localStorage.setItem(
      encryptedUserId(userId, 'dashboardConfig'),
      JSON.stringify(config)
    );
  };

  const getDashboardConfig = () => {
    const persistedConfig = localStorage.getItem(
      encryptedUserId(userId, 'dashboardConfig')
    );
    return persistedConfig && JSON.parse(persistedConfig);
  };

  const [dashboardConfig, setConfig] = useState<DashboardConfig>(
    () => getDashboardConfig() || initialConfig
  );

  return [dashboardConfig, setDashboardConfig];
};
