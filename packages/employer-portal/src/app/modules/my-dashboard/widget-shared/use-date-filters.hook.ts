/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import moment from 'moment';
import { useIntl, IntlShape } from 'react-intl';

import { DateFilter } from '../../../shared';
import { DateFilterType } from '../dashboard.type';

const getDates = () => ({
  thisWeekStart: moment().startOf('week'),
  thisWeekEnd: moment().endOf('week'),
  prevWeekStart: moment()
    .startOf('week')
    .subtract('1', 'week'),
  prevWeekEnd: moment()
    .endOf('week')
    .subtract('1', 'week'),
  nextWeekStart: moment()
    .startOf('week')
    .add('1', 'week'),
  nextWeekEnd: moment()
    .endOf('week')
    .add('1', 'week'),
  weekBeforeLastStart: moment()
    .startOf('week')
    .subtract('2', 'week'),
  weekBeforeLastEnd: moment()
    .endOf('week')
    .subtract('2', 'week'),
  weekAfterNextStart: moment()
    .startOf('week')
    .add('2', 'week'),
  weekAfterNextEnd: moment()
    .endOf('week')
    .add('2', 'week')
});

export const getDateFilters = (intl: IntlShape) => {
  const dates = getDates();
  return {
    TODAY: {
      startDate: moment()
        .startOf('day')
        .format('YYYY-MM-DD'),
      endDate: moment()
        .startOf('day')
        .format('YYYY-MM-DD'),
      displayDateRange: intl.formatMessage({
        id: 'WIDGET.FILTER_OPTIONS.TODAY'
      }),
      value: DateFilter.TODAY
    },
    THIS_WEEK: {
      startDate: dates.thisWeekStart.format('YYYY-MM-DD'),
      endDate: dates.thisWeekEnd.format('YYYY-MM-DD'),
      displayDateRange: intl.formatMessage(
        { id: 'WIDGET.FILTER_OPTIONS.THIS_WEEK' },
        {
          dateRange: `${dates.thisWeekStart.format(
            'MMM DD'
          )} - ${dates.thisWeekEnd.format('MMM DD')}`
        }
      ),
      value: DateFilter.THIS_WEEK
    },
    PREV_WEEK: {
      startDate: dates.prevWeekStart.format('YYYY-MM-DD'),
      endDate: dates.prevWeekEnd.format('YYYY-MM-DD'),
      displayDateRange: intl.formatMessage(
        { id: 'WIDGET.FILTER_OPTIONS.PREV_WEEK' },
        {
          dateRange: `${dates.prevWeekStart.format(
            'MMM DD'
          )} - ${dates.prevWeekEnd.format('MMM DD')}`
        }
      ),
      value: DateFilter.PREV_WEEK
    },
    NEXT_WEEK: {
      startDate: dates.nextWeekStart.format('YYYY-MM-DD'),
      endDate: dates.nextWeekEnd.format('YYYY-MM-DD'),
      displayDateRange: intl.formatMessage(
        { id: 'WIDGET.FILTER_OPTIONS.NEXT_WEEK' },
        {
          dateRange: `${dates.nextWeekStart.format(
            'MMM DD'
          )} - ${dates.nextWeekEnd.format('MMM DD')}`
        }
      ),
      value: DateFilter.NEXT_WEEK
    },
    WEEK_BEFORE_LAST: {
      startDate: dates.weekBeforeLastStart.format('YYYY-MM-DD'),
      endDate: dates.weekBeforeLastEnd.format('YYYY-MM-DD'),
      displayDateRange: intl.formatMessage(
        { id: 'WIDGET.FILTER_OPTIONS.WEEK_BEFORE_LAST' },
        {
          dateRange: `${dates.weekBeforeLastStart.format(
            'MMM DD'
          )} - ${dates.weekBeforeLastEnd.format('MMM DD')}`
        }
      ),
      value: DateFilter.WEEK_BEFORE_LAST
    },
    WEEK_AFTER_NEXT: {
      startDate: dates.weekAfterNextStart.format('YYYY-MM-DD'),
      endDate: dates.weekAfterNextEnd.format('YYYY-MM-DD'),
      displayDateRange: intl.formatMessage(
        { id: 'WIDGET.FILTER_OPTIONS.WEEK_AFTER_NEXT' },
        {
          dateRange: `${dates.weekAfterNextStart.format(
            'MMM DD'
          )} - ${dates.weekAfterNextEnd.format('MMM DD')}`
        }
      ),
      value: DateFilter.WEEK_AFTER_NEXT
    }
  } as DateFilterType;
};

export const useDateFilters = () => {
  const intl = useIntl();
  const DATE_FILTERS = getDateFilters(intl);

  const futureFilterOptions = [
    {
      value: DateFilter.TODAY,
      title: DATE_FILTERS.TODAY.displayDateRange
    },
    {
      value: DateFilter.THIS_WEEK,
      title: DATE_FILTERS.THIS_WEEK.displayDateRange
    },
    {
      value: DateFilter.NEXT_WEEK,
      title: DATE_FILTERS.NEXT_WEEK.displayDateRange
    },
    {
      value: DateFilter.WEEK_AFTER_NEXT,
      title: DATE_FILTERS.WEEK_AFTER_NEXT.displayDateRange
    }
  ];

  const pastFilterOptions = [
    {
      value: DateFilter.TODAY,
      title: DATE_FILTERS.TODAY.displayDateRange
    },
    {
      value: DateFilter.THIS_WEEK,
      title: DATE_FILTERS.THIS_WEEK.displayDateRange
    },
    {
      value: DateFilter.PREV_WEEK,
      title: DATE_FILTERS.PREV_WEEK.displayDateRange
    },
    {
      value: DateFilter.WEEK_BEFORE_LAST,
      title: DATE_FILTERS.WEEK_BEFORE_LAST.displayDateRange
    }
  ];
  return {
    futureFilterOptions,
    pastFilterOptions,
    DATE_FILTERS
  };
};
