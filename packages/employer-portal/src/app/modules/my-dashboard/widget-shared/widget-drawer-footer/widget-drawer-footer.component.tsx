import React from 'react';
import classNames from 'classnames';
import {
  ButtonType,
  FormattedMessage,
  PAGINATION_THRESHOLD,
  UiButton
} from 'fineos-common';

import { ExportDataModal } from '../../../../shared';

import styles from './widget-drawer-footer.module.scss';

type WidgetDrawerFooterProps<T> = {
  dataLength: number;
  downloadData: T[];
  fileNameTranslationId: string;
  onCloseClick: () => void;
  isVisible: boolean;
};

export const WidgetDrawerFooter = <DataType extends {}>({
  dataLength,
  downloadData,
  fileNameTranslationId,
  onCloseClick,
  isVisible
}: WidgetDrawerFooterProps<DataType>) => {
  return isVisible ? (
    <div
      className={classNames(styles.drawerFooter, {
        [styles.responsiveDrawerFooter]: dataLength > PAGINATION_THRESHOLD
      })}
    >
      <ExportDataModal
        data={downloadData}
        fileNameTranslationId={fileNameTranslationId}
      />
      <FormattedMessage
        as={UiButton}
        className={styles.closeButton}
        buttonType={ButtonType.PRIMARY}
        id="FINEOS_COMMON.GENERAL.CLOSE"
        onClick={onCloseClick}
      />
    </div>
  ) : null;
};
