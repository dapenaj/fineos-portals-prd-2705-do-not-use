/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import {
  Icon,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Button,
  ButtonType,
  useElementId,
  ReakitPopover,
  ReakitPopoverArrow,
  ReakitPopoverDisclosure,
  ReakitUsePopoverState
} from 'fineos-common';
import { faQuestionCircle, faTimes } from '@fortawesome/free-solid-svg-icons';

import styles from './widget-help-info-popover.module.scss';

export const useHelpThemedStyles = createThemedStyles(theme => ({
  helpPopoverTriggerIcon: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss({
      ...theme.typography.displaySmall
    })
  },
  helpPopoverCloseIcon: {
    color: theme.colorSchema.neutral6,
    ...textStyleToCss({
      ...theme.typography.displaySmall
    })
  },
  helpPopoverContent: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText),
    backgroundColor: theme.colorSchema.neutral1
  },
  arrow: {
    fill: theme.colorSchema.neutral1
  }
}));

type WidgetHelpInfoPopoverProps = {
  content: React.ReactElement | React.ReactNode;
  triggerClassName?: string;
};

export const WidgetHelpInfoPopover: React.FC<WidgetHelpInfoPopoverProps> = ({
  content,
  triggerClassName
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const themedStyles = useHelpThemedStyles();
  const buttonId = useElementId();
  const popover = ReakitUsePopoverState({
    placement: 'bottom',
    animated: 250
  });

  useEffect(() => {
    setIsOpen(popover.visible);
  }, [popover.visible]);

  return (
    <div className={styles.popoverTrigger}>
      <ReakitPopoverDisclosure
        {...popover}
        className={classNames(triggerClassName, styles.helpIcon)}
      >
        <Icon
          className={classNames(themedStyles.helpPopoverTriggerIcon)}
          icon={faQuestionCircle}
          data-test-el="help-info-icon"
          iconLabel={
            <FormattedMessage id="HELP_INFO_DASHBOARDS.HELP_INFO_ICON" />
          }
        />
      </ReakitPopoverDisclosure>

      <ReakitPopover
        {...popover}
        className={styles.popover}
        data-test-el="help-info-popover"
      >
        <ReakitPopoverArrow
          {...popover}
          className={classNames(
            styles.arrow,
            styles.animation,
            themedStyles.arrow,
            {
              [styles.arrowHidden]: !popover.visible
            }
          )}
        />
        {isOpen ? (
          <div
            className={classNames(
              styles.popoverContent,
              styles.animation,
              styles.popoverOverlay,
              themedStyles.helpPopoverContent
            )}
          >
            <Button
              id={buttonId}
              buttonType={ButtonType.LINK}
              className={styles.popoverCloseIcon}
              onClick={popover.hide}
            >
              <Icon
                className={themedStyles.helpPopoverCloseIcon}
                icon={faTimes}
                iconLabel={
                  <FormattedMessage id="HELP_INFO_DASHBOARDS.CLOSE_INFO_ICON" />
                }
                data-test-el="help-info-icon"
              />
            </Button>
            {content}
          </div>
        ) : null}
      </ReakitPopover>
    </div>
  );
};
