/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { AbsenceEvent } from 'fineos-js-api-client';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import {
  usePaginationThemedStyles,
  Table,
  PAGINATION_THRESHOLD,
  paginationDefaultItemRender,
  PaginationProps
} from 'fineos-common';

import { TableFilters, WidgetTableColumn } from '../../../../shared';
import { useWidgetThemedStyles } from '../../dashboard-widget.utils';
import styles from '../../my-dashboard.module.scss';

import { getTableColumns } from './decisions-made-widget-drawer.utils';

type DecisionsMadeTableProps = {
  isPending: boolean;
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void;
  onClearFilters: (column: WidgetTableColumn) => void;
  columnFilters: TableFilters;
  sortedData: AbsenceEvent[];
  visibleAbsenceEvents: AbsenceEvent[];
};

export const DecisionsMadeTable: React.FC<DecisionsMadeTableProps> = ({
  isPending,
  columnFilters,
  onColumnFilter,
  onClearFilters,
  sortedData,
  visibleAbsenceEvents
}) => {
  const intl = useIntl();
  const themedStyles = useWidgetThemedStyles();
  const paginationThemedStyles = usePaginationThemedStyles();

  const columns = getTableColumns({
    columnFilters,
    onColumnFilter,
    onClearFilters,
    intl,
    themedStyles,
    sortedData: sortedData
  });

  return (
    <Table
      rowKey="id"
      className={classNames(styles.table, paginationThemedStyles.pagination)}
      dataSource={visibleAbsenceEvents}
      columns={columns}
      loading={isPending}
      pagination={
        visibleAbsenceEvents.length > PAGINATION_THRESHOLD &&
        ({
          showSizeChanger: true,
          itemRender: paginationDefaultItemRender,
          'data-test-el': 'pagination'
        } as PaginationProps)
      }
    />
  );
};
