/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useContext } from 'react';
import { useIntl } from 'react-intl';
import moment from 'moment';
import { AbsenceEvent } from 'fineos-js-api-client';
import { Formatting } from 'fineos-common';

import { EnumDomain, getEnumDomainTranslation } from '../../../../shared';

export const useLeavesDecidedDownloadData = (leavesDecided: AbsenceEvent[]) => {
  const intl = useIntl();
  const formatting = useContext(Formatting);

  const getTranslation = (column: string) =>
    intl.formatMessage({
      id: `WIDGET.TABLE_LABELS.${column}`
    });

  return leavesDecided.map(leave => ({
    [getTranslation(
      'EMPLOYEE'
    )]: `${leave.employee.firstName} ${leave.employee.lastName} - ${leave.employee.id}`,
    [getTranslation('GROUP')]: leave.adminGroup,
    [getTranslation('REASON')]: intl.formatMessage({
      id: getEnumDomainTranslation(
        EnumDomain.NOTIFICATION_REASON,
        leave.notificationReason.name
      )
    }),
    [getTranslation('DECIDED_DATE')]: moment(leave.eventDate).format(
      formatting.date
    ),
    [getTranslation('CASE')]: leave.notificationCase.caseReference,
    [getTranslation('DECISION')]: intl.formatMessage({
      id: `WIDGET.${leave.eventName.toUpperCase().replace(/ /g, '_')}`
    })
  }));
};
