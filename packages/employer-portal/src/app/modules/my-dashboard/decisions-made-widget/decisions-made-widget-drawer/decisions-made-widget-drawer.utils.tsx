/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment, { Moment } from 'moment';
import { groupBy as _groupBy } from 'lodash';
import { IntlShape } from 'react-intl';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  FormattedDate,
  FormattedMessage,
  Icon,
  Link,
  SortOrder
} from 'fineos-common';
import { AbsenceEvent, BaseDomain } from 'fineos-js-api-client';

import {
  getReasonNameFilters,
  TableFilters,
  WidgetTableColumn,
  filterByFirstName,
  filterByLastName,
  filterByAdminGroup,
  filterByReason,
  filterByNotification,
  filterByDecision
} from '../../../../shared';
import {
  sortByTranslatedNotificationReason,
  getConfigForCustomerFilter,
  getConfigForRadioFilter,
  getConfigForCheckboxFilter,
  getConfigForInputFilter,
  getNotificationReasonRender,
  getWithTwoLineEllipsisRender,
  getCustomerRender
} from '../../dashboard-table.utils';
import { FilterIconLabel } from '../../dashboard.type';
import styles from '../../my-dashboard.module.scss';
import { WidgetDrawerColumnSorter } from '../../widget-shared';
import { AbsenceEventDecision } from '../decisions-made-widget.enum';

export const sortedAbsenceEvents = (absenceEvents: AbsenceEvent[]) => {
  const sortedData: AbsenceEvent[] = [];
  const eventsGroupedByDate = _groupBy(
    absenceEvents,
    absenceEvent => absenceEvent.eventDate
  );
  const sortedKeys = Object.keys(eventsGroupedByDate).sort((a, b) =>
    moment(new Date(b)).diff(moment(new Date(a)))
  );

  // if items have the same eventDate, then sort them by employee`s firstName (descending)
  sortedKeys.forEach(key => {
    if (eventsGroupedByDate[key].length === 1) {
      sortedData.push(...eventsGroupedByDate[key]);
    } else {
      sortedData.push(
        ...eventsGroupedByDate[key].sort((a, b) =>
          a.employee.firstName.localeCompare(b.employee.firstName)
        )
      );
    }
  });
  return sortedData;
};

export const filteredAbsenceEvents = (
  absenceEvents: AbsenceEvent[],
  columnFilters: TableFilters
) =>
  absenceEvents.filter(
    ({
      employee: { firstName, lastName },
      adminGroup,
      notificationReason: { name: reasonName },
      notificationCase: { caseReference: notificationCaseId },
      eventName: decision
    }) =>
      filterByFirstName(columnFilters, firstName) &&
      filterByLastName(columnFilters, lastName) &&
      filterByAdminGroup(columnFilters, adminGroup) &&
      filterByNotification(columnFilters, notificationCaseId) &&
      filterByReason(columnFilters, reasonName) &&
      filterByDecision(columnFilters, decision)
  );

const getEmployeeColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.EMPLOYEE" />,
  dataIndex: 'employee',
  key: 'employee',
  width: '20%',
  render: ({ firstName, lastName, id }: AbsenceEvent['employee']) =>
    getCustomerRender(id, firstName, lastName),
  sorter: (a: AbsenceEvent, b: AbsenceEvent) =>
    `${a.employee.firstName} ${a.employee.lastName}`.localeCompare(
      `${b.employee.firstName} ${b.employee.lastName}`
    ),
  ...getConfigForCustomerFilter(columnFilters, onColumnFilter, onClearFilters)
});

const getAdminGroupColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.GROUP" />,
  dataIndex: 'adminGroup',
  key: 'adminGroup',
  width: '20%',
  render: (adminGroup: string) =>
    getWithTwoLineEllipsisRender(adminGroup, themedStyles),
  sorter: (a: AbsenceEvent, b: AbsenceEvent) =>
    a.adminGroup.localeCompare(b.adminGroup),
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.ADMIN_GROUP,
    WidgetTableColumn.ADMIN_GROUP,
    1
  )
});

const getNotificationReasonColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  intl: IntlShape,
  themedStyles: Record<string, string>,
  sortedData: AbsenceEvent[]
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.REASON" />,
  dataIndex: 'notificationReason',
  key: 'notificationReason',
  render: (notificationReason: BaseDomain) =>
    getNotificationReasonRender(notificationReason, themedStyles),
  sorter: (a: AbsenceEvent, b: AbsenceEvent) =>
    sortByTranslatedNotificationReason<AbsenceEvent>(a, b, intl),
  filters: getReasonNameFilters<AbsenceEvent>(
    sortedData,
    ({ notificationReason }) => notificationReason.name,
    intl
  ),
  ...getConfigForCheckboxFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles
  )
});

const getCaseNumberColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.CASE" />,
  dataIndex: 'notificationCase',
  key: 'caseReference',
  width: '12%',
  render: ({ caseReference }: { caseReference: string }) => (
    <Link to={`/notification/${caseReference}`}>{caseReference}</Link>
  ),
  sorter: (a: AbsenceEvent, b: AbsenceEvent) =>
    +a.notificationCase.caseReference.slice(4) -
    +b.notificationCase.caseReference.slice(4),
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.CASE_NUMBER,
    WidgetTableColumn.CASE_NUMBER,
    1
  )
});

const getEventDateColumn = (themedStyles: Record<string, string>) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.DECIDED_DATE" />,
  dataIndex: 'eventDate',
  key: 'eventDate',
  width: '12%',
  render: (eventDate: Moment) => (
    <FormattedDate date={eventDate} className={themedStyles.tableCell} />
  ),
  defaultSortOrder: SortOrder.DESCEND,
  sorter: (a: AbsenceEvent, b: AbsenceEvent) =>
    moment(a.eventDate).diff(moment(b.eventDate))
});

const getEventColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (filters: TableFilters) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>
) => ({
  title: (
    <FormattedMessage
      /*
        BUG - FEP-2171
        setting min width is fragile solution, because of the translation,
        but there is just not enough space on 200% zoom to fit all columns,
        so unless layout for 200% changes, there is no clean solution here
      */
      className={styles.minWidthColumn}
      id="WIDGET.TABLE_LABELS.DECISION"
    />
  ),
  dataIndex: 'eventName',
  key: 'eventName',
  width: '12%',
  filters: [
    {
      text: (
        <FormattedMessage id="WIDGET.DECISIONS_MADE_LEAVE_REQUEST_APPROVED_LABEL" />
      ),
      value: AbsenceEventDecision.APPROVED
    },
    {
      text: (
        <FormattedMessage id="WIDGET.DECISIONS_MADE_LEAVE_REQUEST_DENIED_LABEL" />
      ),
      value: AbsenceEventDecision.DENIED
    }
  ],
  ...getConfigForRadioFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles,
    FilterIconLabel.DECISION,
    WidgetTableColumn.DECISION,
    'WIDGET.TABLE_LABELS.DECISION'
  ),
  render: (decision: AbsenceEventDecision) => (
    <Icon
      className={
        decision === AbsenceEventDecision.APPROVED
          ? themedStyles.checkIcon
          : themedStyles.timesIcon
      }
      icon={decision === AbsenceEventDecision.APPROVED ? faCheck : faTimes}
      iconLabel={
        <FormattedMessage
          id={
            decision === AbsenceEventDecision.APPROVED
              ? 'WIDGET.DECISIONS_MADE_LEAVE_REQUEST_APPROVED_LABEL'
              : 'WIDGET.DECISIONS_MADE_LEAVE_REQUEST_DENIED_LABEL'
          }
        />
      }
    />
  )
});

export const getTableColumns = ({
  columnFilters,
  onColumnFilter,
  onClearFilters,
  intl,
  themedStyles,
  sortedData
}: {
  columnFilters: TableFilters;
  onColumnFilter: (filters: TableFilters) => void;
  onClearFilters: (column: WidgetTableColumn) => void;
  intl: IntlShape;
  themedStyles: Record<string, string>;
  sortedData: AbsenceEvent[];
}) => [
  getEmployeeColumn(columnFilters, onColumnFilter, onClearFilters),
  getAdminGroupColumn(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles
  ),
  getNotificationReasonColumn(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    intl,
    themedStyles,
    sortedData
  ),
  { ...getEventDateColumn(themedStyles) },
  getCaseNumberColumn(columnFilters, onColumnFilter, onClearFilters),
  getEventColumn(columnFilters, onColumnFilter, onClearFilters, themedStyles)
];
