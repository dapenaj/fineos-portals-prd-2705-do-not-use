/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isEqual as _isEqual } from 'lodash';
import classNames from 'classnames';
import { AbsenceEvent } from 'fineos-js-api-client';
import {
  Drawer,
  DrawerFooter,
  DrawerWidth,
  FilterItemsCountTag,
  ContentRenderer,
  PAGINATION_THRESHOLD
} from 'fineos-common';

import {
  TableFilters,
  WidgetTableColumn,
  DateFilter,
  initialTableFilters
} from '../../../../shared';
import { DateRangeFilter } from '../../dashboard.type';
import styles from '../../my-dashboard.module.scss';
import { WidgetDrawerFooter } from '../../widget-shared';
import {
  DecisionsMadeDrawerDateFilter,
  DecisionsMadeDrawerHelpInfo
} from '../ui';

import { sortedAbsenceEvents } from './decisions-made-widget-drawer.utils';
import { DecisionsMadeTable } from './decisions-made-table.component';
import { useLeavesDecidedDownloadData } from './use-leaves-decided-download-data.hook';

type DecisionsMadeWidgetDrawerProps = {
  isDrawerVisible: boolean;
  isPending: boolean;
  apiFilters: DateRangeFilter;
  visibleAbsenceEvents: AbsenceEvent[];
  allAbsenceEvents: AbsenceEvent[];
  onApiFilter: (value: DateFilter) => void;
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void;
  handleClose: () => void;
  onClearFilters: (column?: WidgetTableColumn) => void;
  columnFilters: TableFilters;
  helpInfoEnabled: boolean;
};

export const DecisionsMadeWidgetDrawer: React.FC<DecisionsMadeWidgetDrawerProps> = ({
  isDrawerVisible,
  isPending,
  apiFilters,
  visibleAbsenceEvents,
  allAbsenceEvents,
  onApiFilter,
  onColumnFilter,
  handleClose,
  onClearFilters,
  columnFilters,
  helpInfoEnabled
}) => {
  const downloadReadyLeavesDecided = useLeavesDecidedDownloadData(
    sortedAbsenceEvents(visibleAbsenceEvents)
  );

  const isFilterBadgeVisible =
    !isPending &&
    (visibleAbsenceEvents.length !== allAbsenceEvents.length ||
      !_isEqual(columnFilters, initialTableFilters));

  const drawerTitle = (
    <div className={styles.drawerHeaderContent}>
      <DecisionsMadeDrawerDateFilter
        apiFilters={apiFilters}
        isPending={isPending}
        total={allAbsenceEvents.length}
        onChange={(value: unknown) => onApiFilter(value as DateFilter)}
      />
      <DecisionsMadeDrawerHelpInfo isVisible={helpInfoEnabled} />
      <FilterItemsCountTag
        isVisible={isFilterBadgeVisible}
        filteredItemsCount={visibleAbsenceEvents.length}
        totalItemsCount={allAbsenceEvents.length}
        onClearFilters={onClearFilters}
      />
    </div>
  );

  return (
    <Drawer
      ariaLabelId="WIDGET.DECISIONS_MADE_FILTER_DATE_DRAWER_HEADER"
      ariaLabelValues={{ count: allAbsenceEvents.length }}
      className={styles.drawer}
      headerClassName={styles.drawerHeader}
      bodyClassName={classNames({
        [styles.drawerBody]: visibleAbsenceEvents.length > PAGINATION_THRESHOLD
      })}
      isVisible={isDrawerVisible}
      onClose={handleClose}
      width={DrawerWidth.FULL_PAGE}
      title={drawerTitle}
    >
      <ContentRenderer
        shouldNotDestroyOnLoading={true}
        spinnerClassName={styles.spinnerContainer}
        isLoading={isPending}
        isEmpty={false}
      >
        <DecisionsMadeTable
          isPending={isPending}
          columnFilters={columnFilters}
          onColumnFilter={onColumnFilter}
          onClearFilters={onClearFilters}
          sortedData={sortedAbsenceEvents(allAbsenceEvents)}
          visibleAbsenceEvents={visibleAbsenceEvents}
        />
      </ContentRenderer>
      <DrawerFooter>
        <WidgetDrawerFooter
          isVisible={!isPending}
          dataLength={visibleAbsenceEvents.length}
          downloadData={downloadReadyLeavesDecided}
          fileNameTranslationId="EXPORT_OUTSTANDING_LIST.FILE_NAME.LEAVES_DECIDED"
          onCloseClick={handleClose}
        />
      </DrawerFooter>
    </Drawer>
  );
};
