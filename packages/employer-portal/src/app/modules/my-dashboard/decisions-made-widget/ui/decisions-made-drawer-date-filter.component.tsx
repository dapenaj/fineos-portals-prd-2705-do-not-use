/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import { RichFormattedMessage, Select } from 'fineos-common';

import {
  getDateFilterValue,
  getDateSelectedRange,
  useWidgetThemedStyles
} from '../../dashboard-widget.utils';
import {
  getDateFilters,
  useDateFilters,
  WidgetFilter,
  WidgetFilterCounter
} from '../../widget-shared';
import styles from '../../my-dashboard.module.scss';
import { DateRangeFilter } from '../../dashboard.type';

type DecisionsMadeDrawerDateFilterProps = {
  apiFilters: DateRangeFilter;
  onChange: (value: unknown) => void;
  isPending: boolean;
  total: number;
};

export const DecisionsMadeDrawerDateFilter: React.FC<DecisionsMadeDrawerDateFilterProps> = ({
  apiFilters,
  onChange,
  isPending,
  total
}) => {
  const themedStyles = useWidgetThemedStyles();
  const { pastFilterOptions, DATE_FILTERS } = useDateFilters();
  const intl = useIntl();
  return (
    <WidgetFilter
      label={
        <RichFormattedMessage
          className={classNames(styles.drawerTitle, themedStyles.title)}
          as="h1"
          id="WIDGET.DECISIONS_MADE_FILTER_DATE_DRAWER_HEADER"
          values={{
            count: <WidgetFilterCounter isPending={isPending} total={total} />
          }}
        />
      }
      selectInput={
        <Select
          bordered={false}
          value={getDateFilterValue(
            intl,
            getDateFilters(intl),
            getDateSelectedRange(apiFilters, DATE_FILTERS)
          )}
          idResolver={(value: unknown) =>
            getDateFilterValue(intl, getDateFilters(intl), value as string)!
          }
          data-test-el="decisions-made-drawer-filter"
          className={classNames(themedStyles.drawerSelect, styles.drawerSelect)}
          dropdownClassName={styles.selectDropdown}
          onChange={onChange}
          name="filter"
          optionElements={pastFilterOptions}
          dropdownMatchSelectWidth={false}
        />
      }
    />
  );
};
