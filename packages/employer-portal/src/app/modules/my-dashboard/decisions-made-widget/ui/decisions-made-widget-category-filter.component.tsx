/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage, Select } from 'fineos-common';

import { WidgetFilter } from '../../widget-shared';
import styles from '../../my-dashboard.module.scss';
import { CategoryFilter } from '../decisions-made-widget.enum';

const FILTER_OPTIONS = [
  {
    value: CategoryFilter.REASON,
    title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.REASON" />
  },
  {
    value: CategoryFilter.DECISION,
    title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.DECISION" />
  },
  {
    value: CategoryFilter.GROUP,
    title: <FormattedMessage id="WIDGET.FILTER_OPTIONS.GROUP" />
  }
];

type DecisionsMadeWidgetCategoryFilterProps = {
  selectedCategoryFilter: CategoryFilter;
  onChange: (value: unknown) => void;
  themedStyles: Record<string, string>;
};

export const DecisionsMadeWidgetCategoryFilter: React.FC<DecisionsMadeWidgetCategoryFilterProps> = ({
  selectedCategoryFilter,
  onChange,
  themedStyles
}) => {
  return (
    <WidgetFilter
      label={
        <FormattedMessage
          className={themedStyles.filterText}
          id="WIDGET.DECISIONS_MADE_FILTER_CATEGORY"
        />
      }
      selectInput={
        <Select
          bordered={false}
          value={selectedCategoryFilter}
          data-test-el="decisions-made-filter-category"
          className={themedStyles.select}
          dropdownClassName={styles.selectDropdown}
          onChange={onChange}
          name="filter"
          optionElements={FILTER_OPTIONS}
          dropdownMatchSelectWidth={false}
        />
      }
    />
  );
};
