/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage } from 'fineos-common';

import styles from '../../my-dashboard.module.scss';
import { WidgetHelpInfoPopover } from '../../widget-shared';

import helpInfoStyles from './decisions-made-widget-help-info.module.scss';

type DecisionsMadeWidgetHelpInfoProps = { isVisible: boolean };

export const DecisionsMadeWidgetHelpInfo: React.FC<DecisionsMadeWidgetHelpInfoProps> = ({
  isVisible
}) =>
  isVisible ? (
    <WidgetHelpInfoPopover
      data-test-el="decision-made-widget-help"
      triggerClassName={styles.helpPopoverTrigger}
      content={
        <>
          <FormattedMessage
            className={styles.helpPopoverInfoBlock}
            as="strong"
            id="HELP_INFO_DASHBOARDS.DECISION_MADE_TITLE"
          />
          <FormattedMessage id="HELP_INFO_DASHBOARDS.DECISION_MADE_DESCRIPTION" />
          <FormattedMessage
            className={helpInfoStyles.helpPopoverInfoSubBlock}
            as="strong"
            id="HELP_INFO_DASHBOARDS.DECISIONS_MADE_FILTER_DECISION_TITLE"
          />
          <FormattedMessage id="HELP_INFO_DASHBOARDS.DECISIONS_MADE_FILTER_DECISION_DESCRIPTION" />
          <FormattedMessage
            className={helpInfoStyles.helpPopoverInfoSubBlock}
            as="strong"
            id="HELP_INFO_DASHBOARDS.DECISIONS_MADE_FILTER_REASON_TITLE"
          />
          <FormattedMessage id="HELP_INFO_DASHBOARDS.DECISIONS_MADE_FILTER_REASON_DESCRIPTION" />
          <FormattedMessage
            className={helpInfoStyles.helpPopoverInfoSubBlock}
            as="strong"
            id="HELP_INFO_DASHBOARDS.DECISIONS_MADE_FILTER_GROUP_TITLE"
          />
          <FormattedMessage id="HELP_INFO_DASHBOARDS.DECISIONS_MADE_FILTER_GROUP_DESCRIPTION" />
        </>
      }
    />
  ) : null;
