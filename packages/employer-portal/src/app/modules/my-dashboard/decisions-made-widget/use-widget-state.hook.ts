/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useState, useMemo, useEffect } from 'react';
import { AbsenceEvent } from 'fineos-js-api-client';
import { useAsync } from 'fineos-common';
import moment from 'moment';

import { DateFilter, TableFilters, WidgetTableColumn } from '../../../shared';
import { DateRangeFilter } from '../dashboard.type';
import { useDateFilters } from '../widget-shared';
import { initialTableFilters } from '../../../shared/ui/table-filter/table-filter.utils';

import {
  fetchApprovedAbsenceEvents,
  fetchDeniedAbsenceEvents
} from './decisions-made-widget.api';
import { filteredAbsenceEvents as filteredAbsenceEventsFunc } from './decisions-made-widget-drawer/decisions-made-widget-drawer.utils';

export const useWidgetState = () => {
  const { DATE_FILTERS } = useDateFilters();
  const [filteredAbsenceEvents, setFilteredAbsenceEvents] = useState<
    AbsenceEvent[]
  >([]);
  const [apiFilters, setApiFilters] = useState<DateRangeFilter>({
    startDate: DATE_FILTERS.TODAY.startDate,
    endDate: DATE_FILTERS.TODAY.endDate
  });

  const memorisedDateRange = useMemo(
    () => ({
      startDate: moment(apiFilters.startDate),
      endDate: moment(apiFilters.endDate)
    }),
    [apiFilters]
  );

  const [columnFilters, setColumnFilters] = useState<TableFilters>(
    initialTableFilters
  );

  const {
    state: {
      value: approvedAbsenceEvents,
      isPending: isApprovedPending,
      error: errorApproved
    }
  } = useAsync(
    fetchApprovedAbsenceEvents,
    [memorisedDateRange.endDate, memorisedDateRange.startDate],
    {
      shouldExecute: () => !!apiFilters
    }
  );

  const {
    state: {
      value: deniedAbsenceEvents,
      isPending: isDeniedPending,
      error: errorDenied
    }
  } = useAsync(
    fetchDeniedAbsenceEvents,
    [memorisedDateRange.endDate, memorisedDateRange.startDate],
    {
      shouldExecute: () => !!apiFilters
    }
  );

  useEffect(() => {
    setFilteredAbsenceEvents([
      ...(approvedAbsenceEvents || []),
      ...(deniedAbsenceEvents || [])
    ]);
  }, [approvedAbsenceEvents, deniedAbsenceEvents]);

  const onColumnFilter = (filters: TableFilters) => {
    setFilteredAbsenceEvents(
      filteredAbsenceEventsFunc(
        [...(approvedAbsenceEvents || []), ...(deniedAbsenceEvents || [])],
        {
          ...columnFilters,
          ...filters
        }
      )
    );
    setColumnFilters(enabledFilters => ({
      ...enabledFilters,
      ...filters
    }));
  };

  const onApiFilter = (value: DateFilter) => {
    const key = value as keyof typeof DATE_FILTERS;
    setApiFilters({
      startDate: DATE_FILTERS[key].startDate,
      endDate: DATE_FILTERS[key].endDate
    });

    onClearFilters();
  };

  const onClearFilters = (column?: WidgetTableColumn) => {
    if (!column || !Object.values(WidgetTableColumn).includes(column)) {
      setFilteredAbsenceEvents([
        ...(approvedAbsenceEvents || []),
        ...(deniedAbsenceEvents || [])
      ]);
      setColumnFilters(initialTableFilters);
      return;
    }
    const { [column]: columnToReset, ...restOfFilters } = columnFilters;
    setFilteredAbsenceEvents(
      filteredAbsenceEventsFunc(
        [...(approvedAbsenceEvents || []), ...(deniedAbsenceEvents || [])],
        restOfFilters
      )
    );
    setColumnFilters({
      ...restOfFilters,
      [column]: initialTableFilters[column]
    });
  };

  return {
    absenceEvents: [
      ...(approvedAbsenceEvents || []),
      ...(deniedAbsenceEvents || [])
    ],
    setColumnFilters,
    filteredAbsenceEvents,
    isPending: isApprovedPending && isDeniedPending,
    error: errorApproved || errorDenied,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    setFilteredAbsenceEvents
  };
};
