/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { groupBy as _groupBy } from 'lodash';
import { useIntl } from 'react-intl';
import { AbsenceEvent } from 'fineos-js-api-client';
import { FormattedMessage, useTheme } from 'fineos-common';

import {
  EnumDomain,
  GraphWidget,
  getEnumDomainTranslation,
  DateFilter,
  initialTableFilters
} from '../../../shared';
import { useWidgetThemedStyles } from '../dashboard-widget.utils';
import styles from '../my-dashboard.module.scss';

import {
  DecisionsMadeWidgetDateFilter,
  DecisionsMadeWidgetCategoryFilter,
  DecisionsMadeWidgetHelpInfo
} from './ui';
import {
  CategoryFilter,
  AbsenceEventDecision
} from './decisions-made-widget.enum';
import { DecisionsMadeWidgetDrawer } from './decisions-made-widget-drawer/decisions-made-widget-drawer.component';
import { useWidgetState } from './use-widget-state.hook';

type DecisionsMadeWidgetProps = {
  helpInfoEnabled: boolean;
};

export const DecisionsMadeWidget = ({
  helpInfoEnabled
}: DecisionsMadeWidgetProps) => {
  const {
    absenceEvents,
    setColumnFilters,
    filteredAbsenceEvents,
    isPending,
    error,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    setFilteredAbsenceEvents
  } = useWidgetState();
  const themedStyles = useWidgetThemedStyles();
  const intl = useIntl();
  const theme = useTheme();
  const [isDrawerVisible, setIsDrawerVisible] = useState<boolean>(false);
  const [selectedCategoryFilter, setSelectedCategoryFilter] = useState<
    CategoryFilter
  >(CategoryFilter.DECISION);

  const getFilteredDataSlice = (data: AbsenceEvent) => {
    switch (selectedCategoryFilter) {
      case CategoryFilter.DECISION:
        return data.eventName;
      case CategoryFilter.REASON:
        return data.notificationReason.name;
      default:
        return data.adminGroup;
    }
  };

  const filterName = {
    [CategoryFilter.REASON]: 'notificationReason',
    [CategoryFilter.GROUP]: 'adminGroup',
    [CategoryFilter.DECISION]: 'decision'
  };

  const getGroupedDataLabel = (key: string) => {
    switch (selectedCategoryFilter) {
      case CategoryFilter.DECISION:
        const translationId = key.toUpperCase().replace(/\s+/g, '_');
        return intl.formatMessage({
          id: `WIDGET.DECISIONS_MADE_${translationId}_LABEL_GRAPH`
        });
      case CategoryFilter.REASON:
        return intl.formatMessage({
          id: `${getEnumDomainTranslation(EnumDomain.NOTIFICATION_REASON, key)}`
        });
      default:
        return key;
    }
  };

  const getSliceColor = (key: string) => {
    if (selectedCategoryFilter === CategoryFilter.DECISION) {
      return {
        sliceColor:
          key === AbsenceEventDecision.APPROVED
            ? theme.colorSchema.successColor
            : theme.colorSchema.attentionColor
      };
    } else {
      return {};
    }
  };

  const graphData = Object.entries(
    _groupBy(absenceEvents, absenceEvent => getFilteredDataSlice(absenceEvent))
  ).map(([key, value]) => ({
    label: getGroupedDataLabel(key),
    value: value.length,
    filterValues: [key],
    ...getSliceColor(key)
  }));

  const getData = (filterValues: string[]) =>
    filterValues.reduce((total: AbsenceEvent[], current: string) => {
      const filtered = absenceEvents?.filter(
        (absenceEvent: AbsenceEvent) =>
          getFilteredDataSlice(absenceEvent) === current
      );
      total = [...total, ...(filtered || [])];
      return total;
    }, []);

  const onSlideClick = (filterValues: string[]) => {
    setColumnFilters({ [filterName[selectedCategoryFilter]]: filterValues });
    setFilteredAbsenceEvents(getData(filterValues));
  };

  const handleOpenDrawer = (filterValues?: string[]) => {
    filterValues
      ? onSlideClick(filterValues)
      : setFilteredAbsenceEvents(absenceEvents!);
    setIsDrawerVisible(true);
  };

  const handleClose = () => {
    setIsDrawerVisible(false);
    setColumnFilters(initialTableFilters);
  };

  return (
    <div className={styles.container} data-test-el="decisions-made-widget">
      <DecisionsMadeWidgetHelpInfo isVisible={helpInfoEnabled} />
      <GraphWidget
        data={graphData}
        filters={[
          <DecisionsMadeWidgetDateFilter
            key={0}
            onChange={(value: unknown) => onApiFilter(value as DateFilter)}
            apiFilters={apiFilters}
            themedStyles={themedStyles}
          />,
          <DecisionsMadeWidgetCategoryFilter
            key={1}
            onChange={(value: unknown) => {
              setSelectedCategoryFilter(value as CategoryFilter);
            }}
            themedStyles={themedStyles}
            selectedCategoryFilter={selectedCategoryFilter}
          />
        ]}
        isLoading={isPending}
        emptyMessage={
          <FormattedMessage id="WIDGET.DECISIONS_MADE_EMPTY" role="status" />
        }
        error={error}
        onViewDetails={handleOpenDrawer}
      />
      <DecisionsMadeWidgetDrawer
        isDrawerVisible={isDrawerVisible}
        isPending={isPending}
        onColumnFilter={onColumnFilter}
        onClearFilters={onClearFilters}
        handleClose={handleClose}
        visibleAbsenceEvents={filteredAbsenceEvents}
        allAbsenceEvents={absenceEvents || []}
        apiFilters={apiFilters}
        onApiFilter={onApiFilter}
        columnFilters={columnFilters}
        helpInfoEnabled={helpInfoEnabled}
      />
    </div>
  );
};
