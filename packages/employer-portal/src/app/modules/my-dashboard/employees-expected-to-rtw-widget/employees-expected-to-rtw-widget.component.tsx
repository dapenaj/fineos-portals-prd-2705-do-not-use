/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { groupBy as _groupBy, isEqual as _isEqual } from 'lodash';
import classNames from 'classnames';
import { GroupClientNotification } from 'fineos-js-api-client';
import {
  FormattedMessage,
  Drawer,
  DrawerWidth,
  ContentRenderer,
  DonutData,
  FilterItemsCountTag,
  DrawerFooter,
  PAGINATION_THRESHOLD
} from 'fineos-common';

import {
  DateFilter,
  GraphWidget,
  useTransformOutstandingNotifications,
  initialTableFilters,
  MappedNotificationKey
} from '../../../shared';
import { useWidgetThemedStyles } from '../dashboard-widget.utils';
import { WidgetKey, WidgetProps } from '../dashboard.type';
import { useWidgetState, WidgetDrawerFooter } from '../widget-shared';
import styles from '../my-dashboard.module.scss';

import {
  EmployeesERTWWidgetHelpInfo,
  EmployeesERTWWidgetDateFilter,
  EmployeesERTWDrawerDateFilter,
  EmployeesERTWDrawerHelpInfo
} from './ui';
import { EmployeesExpectedToRTWTable } from './employees-expected-to-rtw-table.component';

export const EmployeesExpectedToRTWWidget: React.FC<WidgetProps> = ({
  helpInfoEnabled
}) => {
  const themedStyles = useWidgetThemedStyles();
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);

  const {
    notifications,
    filteredNotifications,
    totalNotifications,
    isPending,
    error,
    apiFilters,
    columnFilters,
    onApiFilter,
    onColumnFilter,
    onClearFilters,
    onSliceClick
  } = useWidgetState(WidgetKey.EXPECTED_RTW);

  const downloadReadyNotifications = useTransformOutstandingNotifications(
    filteredNotifications.sort((a, b) =>
      a.expectedRTWDate?.diff(b.expectedRTWDate)
    ),
    [
      MappedNotificationKey.NAME,
      MappedNotificationKey.JOB_TITLE,
      MappedNotificationKey.GROUP,
      MappedNotificationKey.RETURN_DATE,
      MappedNotificationKey.CASE
    ]
  );

  const graphData: DonutData[] = Object.entries(
    _groupBy(notifications, notification => notification.adminGroup)
  ).map(([key, value]) => ({
    label: key,
    value: value.length,
    filterValues: [key]
  }));

  const isFilterBadgeVisible =
    !isPending &&
    (notifications.length !== filteredNotifications.length ||
      !_isEqual(columnFilters, initialTableFilters));

  const getNotificationsByAdminGroup = (
    allNotifications: GroupClientNotification[],
    adminGroups: string[]
  ) =>
    allNotifications.filter(({ adminGroup }) =>
      adminGroups.includes(adminGroup)
    );

  const handleClose = () => {
    onClearFilters();
    setIsDrawerVisible(false);
  };

  const handleOpenDrawer = (adminGroups?: string[]) => {
    onSliceClick(
      adminGroups && adminGroups.length
        ? getNotificationsByAdminGroup(notifications, adminGroups)
        : notifications,
      adminGroups,
      'adminGroup'
    );
    setIsDrawerVisible(true);
  };

  return (
    <div className={styles.container} data-test-el="expected-to-rtw-widget">
      <EmployeesERTWWidgetHelpInfo isVisible={helpInfoEnabled} />
      <GraphWidget
        data={graphData}
        isLoading={isPending}
        emptyMessage={
          <FormattedMessage
            id="WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_EMPTY"
            role="status"
          />
        }
        error={error}
        onViewDetails={handleOpenDrawer}
        filters={[
          <EmployeesERTWWidgetDateFilter
            key={0}
            apiFilters={apiFilters}
            onChange={(value: unknown) => onApiFilter(value as DateFilter)}
            themedStyles={themedStyles}
          />
        ]}
      />
      <Drawer
        ariaLabelId="WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER"
        ariaLabelValues={{ count: totalNotifications }}
        className={styles.drawer}
        headerClassName={styles.drawerHeader}
        bodyClassName={classNames({
          [styles.drawerBody]:
            filteredNotifications.length > PAGINATION_THRESHOLD
        })}
        data-test-el="expected-to-rtw-drawer"
        onClose={handleClose}
        isVisible={isDrawerVisible}
        width={DrawerWidth.FULL_PAGE}
        title={
          <div className={styles.drawerHeaderContent}>
            <div className={styles.filterInputWrapper}>
              <EmployeesERTWDrawerDateFilter
                onChange={(value: unknown) => onApiFilter(value as DateFilter)}
                apiFilters={apiFilters}
                themedStyles={themedStyles}
                isPending={isPending}
                total={totalNotifications}
              />
              <EmployeesERTWDrawerHelpInfo isVisible={helpInfoEnabled} />
            </div>
            <FilterItemsCountTag
              isVisible={isFilterBadgeVisible}
              filteredItemsCount={filteredNotifications.length}
              totalItemsCount={notifications.length}
              onClearFilters={onClearFilters}
            />
          </div>
        }
      >
        <ContentRenderer
          shouldNotDestroyOnLoading={true}
          spinnerClassName={styles.spinnerContainer}
          isLoading={isPending}
          isEmpty={false}
        >
          <EmployeesExpectedToRTWTable
            filteredNotifications={filteredNotifications}
            isPending={isPending}
            columnFilters={columnFilters}
            onColumnFilter={onColumnFilter}
            onClearFilters={onClearFilters}
          />
        </ContentRenderer>
        <DrawerFooter>
          <WidgetDrawerFooter
            isVisible={!isPending}
            dataLength={filteredNotifications.length}
            downloadData={downloadReadyNotifications}
            fileNameTranslationId="EXPORT_OUTSTANDING_LIST.FILE_NAME.EMPLOYEE_EXPECTED_RTW"
            onCloseClick={handleClose}
          />
        </DrawerFooter>
      </Drawer>
    </div>
  );
};
