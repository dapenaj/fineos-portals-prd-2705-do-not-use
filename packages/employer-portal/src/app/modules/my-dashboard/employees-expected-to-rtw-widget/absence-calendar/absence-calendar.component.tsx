/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import moment, { Moment } from 'moment';
import { chain as _chain } from 'lodash';
import classNames from 'classnames';
import { GroupClientNotification } from 'fineos-js-api-client';
import {
  FormattedMessage,
  ButtonType,
  createThemedStyles,
  Button,
  useAsync,
  usePermissions,
  ViewNotificationPermissions,
  useGanttTimeBarsThemedStyles,
  primaryLightColor,
  primaryLight2Color,
  UiDatePicker,
  textStyleToCss,
  lightenColor
} from 'fineos-common';

import {
  ABSENCE_PERIOD_TYPE,
  EnhancedGroupClientPeriodDecision
} from '../../../../shared';
import { fetchEnhancedAbsencePeriodDecisions } from '../../../notification/notification.api';
import { decisionStatusToColor } from '../../../notification/timeline/timeline-job-protected-leave.util';
import { wageReplacementTypeToShape } from '../../../notification/timeline/timeline-wage-replacement.util';
import {
  decisionsSortOrder,
  flatNonCancelledDecisions
} from '../../../notification/job-protected-leave/job-protected-leave.util';
import { getAllAbsencePeriodDecisions } from '../../../notification/notification.util';

import styles from './absence-calendar.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  datePickerDropdown: {
    '& .ant-picker-range-wrapper': {
      '& .ant-picker-range-arrow::after': {
        borderColor: theme.colorSchema.neutral1
      },
      '& .ant-picker-panel-container': {
        background: theme.colorSchema.neutral1
      }
    },
    '& .ant-picker-cell-disabled .ant-picker-cell-inner': {
      color: lightenColor(theme.colorSchema, 'neutral9')
    },
    '& .ant-picker-cell-disabled.ant-picker-cell-today .ant-picker-cell-inner::after': {
      border: `1px solid ${theme.colorSchema.primaryAction}`
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-selected .ant-picker-cell-inner,
     .ant-picker-cell-in-view.ant-picker-cell-range-start.ant-picker-cell-inner,
      .ant-picker-cell-in-view.ant-picker-cell-range-end .ant-picker-cell-inner,
      .ant-picker-cell-in-view.ant-picker-cell-range-start .ant-picker-cell-inner`]: {
      backgroundColor: 'transparent',
      color: lightenColor(theme.colorSchema, 'neutral9')
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-in-range::before,
     .ant-picker-cell-in-view.ant-picker-cell-range-start:not(.ant-picker-cell-range-start-single)::before,
     .ant-picker-cell-in-view.ant-picker-cell-range-end:not(.ant-picker-cell-range-end-single)::before`]: {
      background: primaryLightColor(theme.colorSchema)
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-start.ant-picker-cell-range-hover::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-end.ant-picker-cell-range-hover::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-start:not(.ant-picker-cell-range-start-single).ant-picker-cell-range-hover-start::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-end:not(.ant-picker-cell-range-end-single).ant-picker-cell-range-hover-end::before,
    .ant-picker-panel > :not(.ant-picker-date-panel) .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-start::before,
    .ant-picker-panel > :not(.ant-picker-date-panel) .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-end::before,
    .ant-picker-date-panel .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-start .ant-picker-cell-inner::after,
    .ant-picker-date-panel
    .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-end .ant-picker-cell-inner::after`]: {
      background: primaryLight2Color(theme.colorSchema)
    },
    [`& tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover:first-child::after,
    tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover-end:first-child::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-edge-start:not(.ant-picker-cell-range-hover-edge-start-near-range)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-start::after`]: {
      borderLeftColor: primaryLightColor(theme.colorSchema)
    },
    [`& tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover:last-child::after,
    tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover-start:last-child::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-edge-end:not(.ant-picker-cell-range-hover-edge-end-near-range)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-end::after`]: {
      borderRightColor: primaryLightColor(theme.colorSchema)
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-range-hover-start:not(.ant-picker-cell-in-range):not(.ant-picker-cell-range-start):not(.ant-picker-cell-range-end)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-end:not(.ant-picker-cell-in-range):not(.ant-picker-cell-range-start):not(.ant-picker-cell-range-end)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-start.ant-picker-cell-range-start-single::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-end.ant-picker-cell-range-end-single::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover:not(.ant-picker-cell-in-range)::after`]: {
      borderTopColor: primaryLightColor(theme.colorSchema),
      borderBottomColor: primaryLightColor(theme.colorSchema)
    },
    [`& .${styles.periodStart}`]: {
      '&:before': {
        borderLeftColor: theme.colorSchema.neutral1
      }
    },
    [`& .${styles.periodEnd}`]: {
      '&:after': {
        borderRightColor: theme.colorSchema.neutral1
      }
    },
    '& .ant-picker-cell-selected > .ant-picker-cell-inner': {
      color: lightenColor(theme.colorSchema, 'neutral9'),
      backgroundColor: theme.colorSchema.neutral1
    }
  },
  datePickerFooterText: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  today: {
    '&:before': {
      border: `1px solid ${theme.colorSchema.primaryAction}`
    }
  },
  expectedRTWDate: {
    '&:before': {
      border: `1px solid ${theme.colorSchema.attentionColor} !important`
    }
  },
  buttonWrapper: {
    borderTop: `1px solid ${theme.colorSchema.neutral4}`
  }
}));

const MAX_POSSIBLE_PERIOD = 10000;

type AbsenceCalendarProps = {
  notification: GroupClientNotification;
};

export const AbsenceCalendar: React.FC<AbsenceCalendarProps> = ({
  notification
}) => {
  const themedStyles = useThemedStyles();
  const ganttTimeBarsThemedStyles = useGanttTimeBarsThemedStyles();
  const [isOpen, setIsOpen] = useState(false);
  const [selectedDate, setSelectedDate] = useState<Moment>(
    notification.expectedRTWDate
  );
  const hasAbsencePeriodDecisionsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS
  );

  const {
    state: { value: enhancedAbsencePeriodDecisions },
    executeAgain
  } = useAsync(fetchEnhancedAbsencePeriodDecisions, [notification], {
    shouldExecute: () => hasAbsencePeriodDecisionsPermission && isOpen
  });

  useEffect(() => {
    if (!isOpen) {
      setSelectedDate(notification.expectedRTWDate);
    }
  }, [isOpen, notification]);

  // group and sort absence decision periods
  const enhancedDecisions = useMemo(() => {
    const absencePeriodDecisions = getAllAbsencePeriodDecisions(
      enhancedAbsencePeriodDecisions
    );
    const absencePeriod = absencePeriodDecisions
      ? (absencePeriodDecisions[0] as EnhancedGroupClientPeriodDecision)
      : null;

    return _chain(flatNonCancelledDecisions(absencePeriod?.decisions || []))
      .sortBy(decision => {
        // first, we sort by status
        const sortIndexByStatus =
          decisionsSortOrder.indexOf(decision.statusCategory) *
          MAX_POSSIBLE_PERIOD;
        // second, we sort by longest decision period, cause from UI perspective
        // long non-breaking periods looks better
        //
        // NOTE: it shouldn't be possible to have decision longer than ~27 years,
        // but better to add safe constraint with Math.max in order to handle those
        // unexpected cases
        const sortIndexByDays = Math.max(
          MAX_POSSIBLE_PERIOD -
            decision.period.endDate.diff(decision.period.startDate, 'days'),
          0
        );

        return sortIndexByStatus + sortIndexByDays;
      })
      .value();
  }, [enhancedAbsencePeriodDecisions]);

  // color date cell if its part of the absence period
  const dateRender = useCallback(
    (current: Moment) => {
      const dateClasses = [];

      const isExpectedRTWDate = moment(current).isSame(
        notification.expectedRTWDate,
        'day'
      );

      for (const decision of enhancedDecisions) {
        const color = decisionStatusToColor(decision.statusCategory);

        if (
          color &&
          current.isBetween(
            decision.period.startDate,
            decision.period.endDate,
            'day',
            '[]'
          )
        ) {
          if (current.isSame(decision.period.startDate, 'day')) {
            dateClasses.push(styles.periodStart);
          }

          if (current.isSame(decision.period.endDate, 'day')) {
            dateClasses.push(styles.periodEnd);
          }

          dateClasses.push(
            ganttTimeBarsThemedStyles[color],
            wageReplacementTypeToShape(
              decision.period.type as ABSENCE_PERIOD_TYPE
            )
          );

          break;
        }
      }

      const dayElement = (
        <div
          className={classNames('ant-picker-cell-inner', {
            [styles.expectedRTWDateIndicator]: isExpectedRTWDate,
            [themedStyles.expectedRTWDate]: isExpectedRTWDate
          })}
        >
          {current.date()}
        </div>
      );

      if (!dateClasses.length) {
        return dayElement;
      }

      return (
        <div className={classNames(dateClasses)}>
          <div className={classNames(styles.lighterBackground)}>
            {dayElement}
          </div>
        </div>
      );
    },
    [notification, enhancedDecisions, themedStyles, ganttTimeBarsThemedStyles]
  );

  const containerRef = useRef<HTMLDivElement>(null);
  const footerRef = useRef<HTMLDivElement>(null);
  const onAnimationEnd = () => {
    // antd datepicker workaround for handling focus and closing with "Escape"
    const todayButton = footerRef?.current?.querySelector(
      '[data-test-el="absence-calendar-today-button"]'
    ) as HTMLButtonElement;

    const viewCalendarButton = containerRef?.current?.querySelector(
      '[data-test-el="WIDGET.ABSENCE_CALENDAR.VIEW_CALENDAR"]'
    ) as HTMLButtonElement;
    if (isOpen) {
      todayButton?.focus();
    } else {
      viewCalendarButton?.focus();
    }
  };

  return (
    <div
      ref={containerRef}
      className={styles.container}
      onAnimationEnd={onAnimationEnd}
    >
      <FormattedMessage
        as={Button}
        buttonType={ButtonType.LINK}
        onClick={() => {
          setIsOpen(!isOpen);
          executeAgain();
        }}
        id="WIDGET.ABSENCE_CALENDAR.VIEW_CALENDAR"
      />
      <UiDatePicker
        className={styles.datePicker}
        open={isOpen}
        onOpenChange={() => setIsOpen(!isOpen)}
        dateRender={dateRender}
        disabledDate={(date: Moment) => true}
        dropdownClassName={classNames(
          styles.datePickerDropdown,
          themedStyles.datePickerDropdown
        )}
        renderExtraFooter={() => (
          <>
            <div className={styles.datePickerFooter}>
              <FormattedMessage
                className={classNames(
                  themedStyles.datePickerFooterText,
                  styles.legend
                )}
                id="WIDGET.ABSENCE_CALENDAR.LEGEND"
                data-test-el="absence-calendar-legend"
              />
              <FormattedMessage
                className={classNames(
                  themedStyles.datePickerFooterText,
                  themedStyles.today,
                  styles.today
                )}
                id="WIDGET.ABSENCE_CALENDAR.TODAY"
                data-test-el="absence-calendar-legend-today"
              />
              <FormattedMessage
                className={classNames(
                  themedStyles.datePickerFooterText,
                  themedStyles.expectedRTWDate,
                  styles.expectedRTWDate
                )}
                id="WIDGET.ABSENCE_CALENDAR.EXPECTED_RETURN_TO_WORK_DATE"
                data-test-el="absence-calendar-legend-expectedRTW"
              />
            </div>
            <div
              className={classNames(
                styles.buttonWrapper,
                themedStyles.buttonWrapper
              )}
              ref={footerRef}
            >
              <FormattedMessage
                as={Button}
                buttonType={ButtonType.LINK}
                onClick={() => {
                  setSelectedDate(moment());
                }}
                id="WIDGET.ABSENCE_CALENDAR.TODAY"
                data-test-el="absence-calendar-today-button"
                onKeyDown={(event: React.KeyboardEvent) => {
                  event.stopPropagation();
                  if (isOpen && event.key === 'Escape') {
                    setIsOpen(false);
                  }
                }}
              />
            </div>
          </>
        )}
        showToday={false}
        value={selectedDate}
      />
    </div>
  );
};
