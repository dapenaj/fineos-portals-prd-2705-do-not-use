/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import { FormattedMessage, Select } from 'fineos-common';

import { NotificationAPIFilters } from '../../../../shared';
import {
  getDateFilters,
  useDateFilters,
  WidgetFilter
} from '../../widget-shared';
import { getDateFilterValue } from '../../dashboard-widget.utils';
import styles from '../../my-dashboard.module.scss';

type EmployeesERTWWidgetDateFilterProps = {
  onChange: (value: unknown) => void;
  apiFilters: NotificationAPIFilters;
  themedStyles: Record<string, string>;
};

export const EmployeesERTWWidgetDateFilter: React.FC<EmployeesERTWWidgetDateFilterProps> = ({
  onChange,
  apiFilters,
  themedStyles
}) => {
  const intl = useIntl();
  const { futureFilterOptions } = useDateFilters();
  return (
    <WidgetFilter
      label={
        <FormattedMessage
          className={classNames(styles.filterText, themedStyles.filterText)}
          id="WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK"
          role="heading"
        />
      }
      selectInput={
        <Select
          bordered={false}
          value={getDateFilterValue(
            intl,
            getDateFilters(intl),
            apiFilters?.expectedRTW?.value
          )}
          idResolver={(value: unknown) =>
            getDateFilterValue(intl, getDateFilters(intl), value as string)!
          }
          data-test-el="expected-to-rtw-filter"
          className={classNames(styles.select, themedStyles.select)}
          dropdownClassName={styles.selectDropdown}
          onChange={onChange}
          name="filter"
          optionElements={futureFilterOptions}
          dropdownMatchSelectWidth={false}
        />
      }
    />
  );
};
