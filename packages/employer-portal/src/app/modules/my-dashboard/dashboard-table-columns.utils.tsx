/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { IntlShape } from 'react-intl';
import { Moment } from 'moment';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import {
  FormattedMessage,
  Link,
  FormattedDate,
  Icon,
  SortOrder
} from 'fineos-common';
import {
  BaseDomain,
  CustomerSummary,
  GroupClientNotification
} from 'fineos-js-api-client';

import {
  getReasonNameFilters,
  TableFilters,
  WidgetTableColumn,
  hasClaimTypeSubcase,
  CaseType
} from '../../shared';

import {
  getNotificationReasonRender,
  getConfigForRadioFilter,
  getConfigForCustomerFilter,
  getCustomerRender,
  getConfigForInputFilter,
  getWithTwoLineEllipsisRender,
  getConfigForCheckboxFilter,
  sortByTranslatedNotificationReason
} from './dashboard-table.utils';
import { AbsenceCalendar } from './employees-expected-to-rtw-widget/absence-calendar';
import { FilterIconLabel } from './dashboard.type';
import { WidgetDrawerColumnSorter } from './widget-shared';

export const getEmployeeColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.EMPLOYEE" />,
  dataIndex: 'customer',
  key: 'customer',
  width: '20%',
  ...getConfigForCustomerFilter(columnFilters, onColumnFilter, onClearFilters),
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    `${a.customer.firstName} ${a.customer.lastName}`.localeCompare(
      `${b.customer.firstName} ${b.customer.lastName}`
    ),
  render: ({ firstName, lastName, id }: CustomerSummary) =>
    getCustomerRender(id, firstName, lastName)
});

export const getJobTitleColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.JOB_TITLE" />,
  dataIndex: 'customer',
  key: 'jobTitle',
  width: '11%',
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.JOB_TITLE,
    WidgetTableColumn.JOB_TITLE
  ),
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    a.customer.jobTitle?.localeCompare(b.customer.jobTitle),
  render: ({ jobTitle }: CustomerSummary) =>
    getWithTwoLineEllipsisRender(jobTitle, themedStyles)
});

export const getAdminGroupColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.GROUP" />,
  dataIndex: 'adminGroup',
  key: 'adminGroup',
  width: '15%',
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.ADMIN_GROUP,
    WidgetTableColumn.ADMIN_GROUP
  ),
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    a.adminGroup?.localeCompare(b.adminGroup),
  render: (adminGroup: string) =>
    getWithTwoLineEllipsisRender(adminGroup, themedStyles)
});

export const getReasonColumn = (
  notifications: GroupClientNotification[],
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>,
  intl: IntlShape
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.REASON" />,
  dataIndex: 'notificationReason',
  key: 'notificationReason',
  filters: getReasonNameFilters(
    notifications,
    ({ notificationReason }) => notificationReason.name,
    intl
  ),
  ...getConfigForCheckboxFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles
  ),
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    sortByTranslatedNotificationReason<GroupClientNotification>(a, b, intl),
  render: (notificationReason: BaseDomain) =>
    getNotificationReasonRender(notificationReason, themedStyles)
});

export const getReturnDateColumn = (themedStyles: Record<string, string>) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.RETURN_DATE" />,
  dataIndex: 'expectedRTWDate',
  key: 'expectedRTWDate',
  width: '15%',
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    a.expectedRTWDate?.diff(b.expectedRTWDate),
  defaultSortOrder: SortOrder.ASCEND,
  render: (expectedRTWDate: Moment) => (
    <FormattedDate date={expectedRTWDate} className={themedStyles.tableCell} />
  )
});

export const getCreatedDateColumn = (themedStyles: Record<string, string>) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.CREATION_DATE" />,
  dataIndex: 'createdDate',
  key: 'createdDate',
  width: '13%',
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    a.createdDate?.diff(b.createdDate),
  defaultSortOrder: SortOrder.ASCEND,
  render: (createdDate: Moment) => (
    <FormattedDate date={createdDate} className={themedStyles.tableCell} />
  )
});

export const getCaseNumberColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  title: <WidgetDrawerColumnSorter id="WIDGET.TABLE_LABELS.CASE" />,
  dataIndex: 'caseNumber',
  key: 'caseNumber',
  width: '10%',
  ...getConfigForInputFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    FilterIconLabel.CASE_NUMBER,
    WidgetTableColumn.CASE_NUMBER,
    1
  ),
  sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
    +a.caseNumber.slice(4) - +b.caseNumber.slice(4),
  render: (caseNumber: string) => (
    <Link to={`/notification/${caseNumber}`}>{caseNumber}</Link>
  )
});

export const getAbsenceCalendarColumn = () => ({
  title: '',
  dataIndex: '',
  key: 'viewCalendar',
  width: '12%',
  render: (notification: GroupClientNotification) => (
    <AbsenceCalendar notification={notification} />
  )
});

export const getDIColumn = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>
) => ({
  title: <FormattedMessage id="WIDGET.TABLE_LABELS.DI" />,
  dataIndex: '',
  key: 'DI',
  width: '6%',
  filters: [
    {
      text: <FormattedMessage id="FINEOS_COMMON.GENERAL.YES" />,
      value: CaseType.DISABILITY_RELATED
    },
    {
      text: <FormattedMessage id="FINEOS_COMMON.GENERAL.NO" />,
      value: CaseType.NON_DISABILITY_RELATED
    }
  ],
  ...getConfigForRadioFilter(
    columnFilters,
    onColumnFilter,
    onClearFilters,
    themedStyles,
    FilterIconLabel.DI,
    WidgetTableColumn.DI,
    'NOTIFICATIONS.FILTERS.DI'
  ),
  render: (notification: GroupClientNotification) =>
    hasClaimTypeSubcase(notification) ? (
      <Icon
        className={themedStyles.checkIcon}
        icon={faCheck}
        iconLabel={<FormattedMessage id="WIDGET.TABLE_LABELS.DI_ARIA_LABEL" />}
      />
    ) : (
      <div className={themedStyles.tableCell}>--</div>
    )
});
