/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { IntlShape } from 'react-intl';
import { faFilter, faSearch } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import { FormattedMessage, FilterDropdownProps, Link } from 'fineos-common';
import {
  BaseDomain,
  GroupClientNotification,
  AbsenceEvent
} from 'fineos-js-api-client';

import {
  TableCheckboxFilter,
  CustomerFilter,
  FilterIconElement,
  TableFilters,
  WidgetTableColumn,
  TableInputFilter,
  TableRadioFilter,
  getEnumDomainTranslation,
  EnumDomain
} from '../../shared';
import { NotificationReason } from '../notification/notification-reason';

import { EmployeesApprovedForLeaveType } from './employees-approved-for-leave/employees-approved-for-leave-widget.enums';
import { FilterIconLabel } from './dashboard.type';
import styles from './my-dashboard.module.scss';

export const handleFocusTrigger = (id: string) => {
  const icon = document.getElementById(id);
  icon?.focus();
};

export const sortByTranslatedNotificationReason = <
  T extends
    | GroupClientNotification
    | AbsenceEvent
    | EmployeesApprovedForLeaveType
>(
  a: T,
  b: T,
  intl: IntlShape
) => {
  const firstTranslatedReasonName = intl.formatMessage({
    id: getEnumDomainTranslation(
      EnumDomain.NOTIFICATION_REASON,
      a.notificationReason.name
    )
  });
  const secondTranslatedReasonName = intl.formatMessage({
    id: getEnumDomainTranslation(
      EnumDomain.NOTIFICATION_REASON,
      b.notificationReason.name
    )
  });

  return firstTranslatedReasonName.localeCompare(secondTranslatedReasonName);
};

export const getNotificationReasonRender = (
  notificationReason: BaseDomain,
  themedStyles: Record<string, string>
) => (
  <div
    title={notificationReason.name}
    className={classNames(styles.twoLineEllipsis, themedStyles.twoLineEllipsis)}
  >
    <NotificationReason reason={notificationReason} />
  </div>
);

export const getWithTwoLineEllipsisRender = (
  text: string,
  themedStyles: Record<string, string>
) => (
  <div
    title={text}
    className={classNames(styles.twoLineEllipsis, themedStyles.twoLineEllipsis)}
  >
    {text}
  </div>
);

const GROUPABLE_COLUMN_CLASS = 'groupable-row-cell-link';

export const getAdminGroupRender = (
  text: string,
  themedStyles: Record<string, string>
) => (
  <div
    title={text}
    className={classNames(
      GROUPABLE_COLUMN_CLASS,
      styles.twoLineEllipsis,
      themedStyles.twoLineEllipsis
    )}
  >
    {text}
  </div>
);

export const getCustomerRender = (
  id: string,
  firstName: string,
  lastName: string
) => (
  <Link
    className={GROUPABLE_COLUMN_CLASS}
    to={`/profile/${id}`}
  >{`${firstName} ${lastName} - ${id}`}</Link>
);

export const getConfigForCustomerFilter = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void
) => ({
  filterIcon: () => (
    <FilterIconElement
      isActive={
        !!columnFilters[WidgetTableColumn.CUSTOMER]?.customerFirstName ||
        !!columnFilters[WidgetTableColumn.CUSTOMER]?.customerLastName
      }
      icon={faSearch}
      id={FilterIconLabel.CUSTOMER}
    />
  ),
  filterDropdown: ({ confirm }: FilterDropdownProps) => (
    <CustomerFilter
      filterValues={columnFilters}
      fieldName={WidgetTableColumn.CUSTOMER}
      onSearch={(searchFilters: TableFilters) => {
        onColumnFilter(searchFilters, WidgetTableColumn.CUSTOMER);
        confirm();
        handleFocusTrigger(FilterIconLabel.CUSTOMER);
      }}
      onReset={() => {
        onClearFilters(WidgetTableColumn.CUSTOMER);
        confirm();
        handleFocusTrigger(FilterIconLabel.CUSTOMER);
      }}
      onClose={() => {
        confirm();
        handleFocusTrigger(FilterIconLabel.CUSTOMER);
      }}
    />
  )
});

export const getConfigForRadioFilter = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>,
  filterType: FilterIconLabel,
  columnType: WidgetTableColumn,
  filterLabel: string
) => ({
  filterIcon: () => (
    <FilterIconElement
      isActive={!!columnFilters[columnType]}
      icon={faFilter}
      id={filterType}
    />
  ),
  filterDropdown: (props: FilterDropdownProps) => (
    <TableRadioFilter
      {...props}
      className={styles.radioFilterDropdown}
      filterValues={columnFilters}
      column={columnType}
      clearFilters={() => {
        onClearFilters(columnType);
        handleFocusTrigger(filterType);
      }}
      onFilter={(values: TableFilters) => {
        onColumnFilter(values, columnType);
        handleFocusTrigger(filterType);
      }}
      onClose={() => {
        handleFocusTrigger(filterType);
      }}
      label={
        <FormattedMessage className={themedStyles.label} id={filterLabel} />
      }
    />
  )
});

export const getConfigForCheckboxFilter = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  themedStyles: Record<string, string>,
  filterType: FilterIconLabel = FilterIconLabel.REASON,
  columnType: WidgetTableColumn = WidgetTableColumn.REASON,
  widgetLabel: string = 'WIDGET.TABLE_LABELS.REASON'
) => {
  const checkboxFilters = columnFilters[columnType];
  const isFilterApplied =
    Array.isArray(checkboxFilters) && !!checkboxFilters.length;
  return {
    filterIcon: () => (
      <FilterIconElement
        isActive={isFilterApplied}
        icon={faFilter}
        id={filterType}
      />
    ),
    filterDropdown: (props: FilterDropdownProps) => (
      <TableCheckboxFilter
        {...props}
        filterValues={columnFilters}
        column={columnType}
        clearFilters={() => {
          onClearFilters(columnType);
          handleFocusTrigger(filterType);
        }}
        onFilter={values => {
          onColumnFilter(values!, columnType);
          handleFocusTrigger(filterType);
        }}
        onClose={() => {
          handleFocusTrigger(filterType);
        }}
        label={
          <FormattedMessage className={themedStyles.label} id={widgetLabel} />
        }
      />
    )
  };
};

export const getConfigForInputFilter = (
  columnFilters: TableFilters,
  onColumnFilter: (
    filterValues: TableFilters,
    column?: WidgetTableColumn
  ) => void,
  onClearFilters: (column: WidgetTableColumn) => void,
  filterType: FilterIconLabel,
  columnType: WidgetTableColumn,
  minLength?: number
) => ({
  filterIcon: () => (
    <FilterIconElement
      isActive={!!columnFilters[columnType]}
      icon={faSearch}
      id={filterType}
    />
  ),
  filterDropdown: ({ confirm }: FilterDropdownProps) => (
    <TableInputFilter
      minLength={minLength}
      filterValues={columnFilters}
      fieldName={columnType}
      onSearch={(searchFilters: TableFilters) => {
        onColumnFilter(searchFilters, columnType);
        confirm();
        handleFocusTrigger(filterType);
      }}
      onReset={() => {
        onClearFilters(columnType);
        confirm();
        handleFocusTrigger(filterType);
      }}
      onClose={() => {
        confirm();
        handleFocusTrigger(filterType);
      }}
    />
  )
});
