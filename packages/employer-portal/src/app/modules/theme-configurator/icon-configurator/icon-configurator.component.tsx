/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect, useMemo } from 'react';
import {
  ThemedImages,
  Modal,
  FileUpload,
  UploadedFile,
  RichFormattedMessage,
  FormattedMessage,
  sanitizeSvg
} from 'fineos-common';
import classNames from 'classnames';

import { useThemeConfiguratorThemedStyles } from '../theme-configurator-themed-styles.hook';
import configuratorStyles from '../theme-configurator.module.scss';

import { IconConfiguratorLoader } from './icon-configurator-loader.component';
import styles from './icon-configurator.module.scss';

type IconConfiguratorProps = {
  currentImages: Record<string, string>;
  onCustomIconAdd: (image: string, imageGroup: string) => void;
  onConfigChange: (typography: ThemedImages) => void;
};

export const IconConfigurator: React.FC<IconConfiguratorProps> = ({
  currentImages,
  onCustomIconAdd,
  onConfigChange
}) => {
  // eslint-disable-next-line
  const initialImages = useMemo(() => currentImages, []);
  const [imageList, setImageList] = useState<Record<string, string> | null>(
    null
  );
  const [currentAdding, setCurrentAdding] = useState<keyof ThemedImages | null>(
    null
  );

  useEffect(() => {
    setImageList(currentImages as Record<string, string>);
  }, [currentImages]);

  const handleImageUpload = (image: UploadedFile, imageGroup: string) => {
    const sanitizedSvg = sanitizeSvg(image.file);

    onCustomIconAdd(sanitizedSvg, imageGroup);
    setCurrentAdding(null);
    onConfigChange({
      ...currentImages,
      [imageGroup]: sanitizedSvg
    });
  };

  const themedStyles = useThemeConfiguratorThemedStyles();
  return (
    <div className={styles.images}>
      <div
        className={classNames(
          configuratorStyles.sectionHead,
          themedStyles.sectionHead,
          styles.head
        )}
      >
        <FormattedMessage id="THEME_CONFIGURATOR.ICONS.HEAD" />
      </div>
      {imageList &&
        Object.keys(currentImages).map((imageGroup, index) => (
          <IconConfiguratorLoader
            key={imageGroup}
            imageGroup={imageGroup}
            isSeparatorVisible={index < Object.keys(currentImages).length - 1}
            onCurrentAddingChange={() =>
              setCurrentAdding(imageGroup as keyof ThemedImages)
            }
          />
        ))}
      <Modal
        ariaLabelId="THEME_CONFIGURATOR.ICONS.UPLOAD_MODAL_TITLE"
        header={
          <FormattedMessage id="THEME_CONFIGURATOR.ICONS.UPLOAD_MODAL_TITLE" />
        }
        visible={!!currentAdding}
        onCancel={() => setCurrentAdding(null)}
      >
        <FileUpload
          accept={['.svg']}
          uploadedMediaTypeLabel="image"
          contentType="TEXT"
          isLoading={false}
          onCancel={() => setCurrentAdding(null)}
          onSubmit={file => handleImageUpload(file, currentAdding!)}
          maxFileSize="2MB"
        >
          {currentAdding && (
            <RichFormattedMessage
              id="THEME_CONFIGURATOR.ICONS.DESCRIPTION"
              values={{
                external: (href: string) => (
                  <a
                    href={href}
                    target="_blank"
                    rel="noopener noreferrer nofollow"
                  >
                    {href}
                  </a>
                )
              }}
            />
          )}
        </FileUpload>
      </Modal>
    </div>
  );
};
