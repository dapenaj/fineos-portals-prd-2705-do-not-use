/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import { Row, Col, InputNumber, Switch, Input } from 'antd';
import { cloneDeep as _cloneDeep, isEmpty as _isEmpty } from 'lodash';
import classNames from 'classnames';
import {
  Button,
  loadThemeFonts,
  TextStyle,
  ThemedTypography,
  ThemeModel,
  registerTypographyClassNameChange,
  FormattedMessage,
  Select
} from 'fineos-common';

import { useThemeConfiguratorThemedStyles } from '../theme-configurator-themed-styles.hook';
import { TextGroupParent, TextStyleProperty } from '../theme-configurator.enum';
import configuratorStyles from '../theme-configurator.module.scss';

import {
  typographyConfig,
  typographyParents,
  CalculatedTypographyConfig,
  InheritedField
} from './typography-configurator.config';
import styles from './typography-configurator.module.scss';

type TypographyConfiguratorProps = {
  currentTheme: ThemeModel;
  onConfigChange: (typography: ThemedTypography) => void;
};

export const TypographyConfigurator: React.FC<TypographyConfiguratorProps> = ({
  currentTheme,
  onConfigChange
}) => {
  const [focusedTypography, setFocusedTypography] = useState('');
  const [fonts, setFonts] = useState(new Map<string, Set<string>>());
  const [hasAdvancedMode, setHasAdvancedMode] = useState<boolean>(false);
  const [currentAdding, setCurrentAdding] = useState<{
    groupName: string;
    value: string;
  } | null>(null);
  const [fontFamilyList, setFontFamilyList] = useState<Record<
    string,
    string
  > | null>(null);

  useEffect(() => {
    let fontFamilies = {} as Record<string, string>;
    Object.keys(currentTheme.typography).map(
      typographyGroup =>
        (fontFamilies = {
          ...fontFamilies,
          [typographyGroup]: (currentTheme.typography as any)[typographyGroup]
            .fontFamily
        })
    );
    setFontFamilyList(fontFamilies);
  }, [currentTheme]);

  const onTypographyChange = (
    changeGroup: keyof ThemedTypography,
    changeParameter: TextStyleProperty,
    value: string | number
  ) => {
    const editedTheme = _cloneDeep(currentTheme);
    const groupIndex = typographyConfig.findIndex(
      ({ groupName }) => groupName === changeGroup
    );
    const hasGroupInDefaultFields = !!typographyConfig[
      groupIndex
    ].defaultFields.find(field => field === changeParameter);
    const hasGroupInOptionalFields = typographyConfig[
      groupIndex
    ].valueCalculation?.optionalInheritedFields.find(
      ({ name }) => name === changeParameter
    );
    if (hasGroupInDefaultFields) {
      (editedTheme.typography as any)[changeGroup][changeParameter] = value;
      onConfigChange(editedTheme.typography);
      calculateFieldValues(changeGroup, changeParameter, value);
      if (changeParameter === TextStyleProperty.FONT_FAMILY) {
        loadThemeFonts(editedTheme, document.head, { display: 'swap' });
      }
    }
    if (hasAdvancedMode && hasGroupInOptionalFields) {
      (editedTheme.typography as any)[changeGroup][changeParameter] = value;
      onConfigChange(editedTheme.typography);
    }
  };

  const calculateFieldValues = (
    changeGroup: keyof ThemedTypography,
    changeParameter: keyof TextStyle,
    value: string | number
  ) => {
    const editedTheme = _cloneDeep(currentTheme);
    if (typographyParents.includes(changeGroup as TextGroupParent)) {
      (editedTheme.typography as any)[changeGroup][changeParameter] = value;
      typographyConfig.forEach(configItem => {
        configItem.valueCalculation?.defaultInheritedFields.map(field => {
          assignValuesToFields(
            editedTheme.typography,
            configItem,
            changeParameter,
            field
          );
        });
        configItem.valueCalculation?.optionalInheritedFields.map(field => {
          assignValuesToFields(
            editedTheme.typography,
            configItem,
            changeParameter,
            field
          );
        });
      });
    }
  };

  const assignValuesToFields = (
    editedTypography: ThemedTypography,
    config: CalculatedTypographyConfig,
    changeParameter: keyof TextStyle,
    inheritedField: InheritedField
  ) => {
    if (inheritedField.name === changeParameter) {
      editedTypography[config.groupName][changeParameter] = inheritedField.ratio
        ? (editedTypography as any)[config.valueCalculation?.parent!][
            changeParameter
          ] * inheritedField.ratio
        : (editedTypography as any)[config.valueCalculation?.parent!][
            changeParameter
          ];
      onConfigChange(editedTypography);
    }
  };

  const getFontWeightValue = (value: number | string): number => {
    if (value === 'bold') {
      return 700;
    } else if (value === 'normal') {
      return 400;
    } else {
      return Number(value);
    }
  };

  const onFontFamilyEdit = (
    groupName: keyof ThemedTypography,
    value: string
  ) => {
    setCurrentAdding({ groupName, value });
    setFontFamilyList({
      ...fontFamilyList!,
      [groupName]: value
    });
  };

  const buildTypographyInputField = (
    editedTypography: ThemedTypography,
    field: TextStyleProperty,
    groupName: keyof ThemedTypography
  ) => {
    switch (field) {
      case TextStyleProperty.FONT_FAMILY: {
        return (
          <Row>
            <Col>
              <label
                htmlFor={`${groupName}-fontFamily`}
                className={classNames(
                  themedStyles.typographyLabel,
                  configuratorStyles.inputLabel
                )}
              >
                <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_FAMILY_LABEL" />
              </label>
              <Row justify="space-between">
                <Col xs={18}>
                  <Input
                    id={`${groupName}-fontFamily`}
                    name="fontFamily"
                    value={
                      !_isEmpty(fontFamilyList)
                        ? fontFamilyList![groupName].replace(/"/g, '')
                        : ''
                    }
                    onChange={e => onFontFamilyEdit(groupName, e.target.value)}
                  />
                </Col>
                <Col>
                  <Button
                    disabled={groupName !== currentAdding?.groupName}
                    onClick={() =>
                      onTypographyChange(
                        groupName,
                        TextStyleProperty.FONT_FAMILY,
                        fontFamilyList![groupName]
                      )
                    }
                  >
                    <FormattedMessage id="FINEOS_COMMON.GENERAL.LOAD" />
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        );
      }
      case TextStyleProperty.FONT_WEIGHT: {
        return (
          <Col sm={8} className={styles.inputWrapper}>
            <label
              htmlFor={`${groupName}-fontWeight`}
              className={classNames(
                themedStyles.typographyLabel,
                configuratorStyles.inputLabel
              )}
            >
              <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_WEIGHT_LABEL" />
            </label>
            <InputNumber
              id={`${groupName}-fontWeight`}
              step={100}
              min={100}
              max={1000}
              value={getFontWeightValue(
                editedTypography[groupName].fontWeight!
              )}
              onChange={value =>
                onTypographyChange(
                  groupName,
                  TextStyleProperty.FONT_WEIGHT,
                  value!
                )
              }
            />
          </Col>
        );
      }
      case TextStyleProperty.FONT_SIZE: {
        return (
          <Col sm={8} className={styles.inputWrapper}>
            <label
              htmlFor={`${groupName}-fontSize`}
              className={classNames(
                themedStyles.typographyLabel,
                configuratorStyles.inputLabel
              )}
            >
              <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_SIZE_LABEL" />
            </label>
            <InputNumber
              id={`${groupName}-fontSize`}
              min={1}
              precision={0}
              value={editedTypography[groupName].fontSize}
              onChange={value => {
                onTypographyChange(
                  groupName,
                  TextStyleProperty.FONT_SIZE,
                  value!
                );
              }}
            />
          </Col>
        );
      }
      case TextStyleProperty.LINE_HEIGHT: {
        return (
          <Col sm={8} className={styles.inputWrapper}>
            <label
              htmlFor={`${groupName}-lineHeight`}
              className={classNames(
                themedStyles.typographyLabel,
                configuratorStyles.inputLabel
              )}
            >
              <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.LINE_HEIGHT_LABEL" />
            </label>
            <InputNumber
              id={`${groupName}-lineHeight`}
              min={1}
              precision={0}
              value={editedTypography[groupName].lineHeight}
              onChange={value =>
                onTypographyChange(
                  groupName,
                  TextStyleProperty.LINE_HEIGHT,
                  value!
                )
              }
            />
          </Col>
        );
      }
      case TextStyleProperty.FONT_STYLE: {
        return (
          <Row>
            <Col>
              <label
                htmlFor={`${groupName}-fontStyle`}
                className={classNames(
                  themedStyles.typographyLabel,
                  configuratorStyles.inputLabel
                )}
              >
                <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_LABEL" />
              </label>
              <Row>
                <Col xs={18}>
                  <Select
                    id={`${groupName}-fontStyle`}
                    value={editedTypography[groupName].fontStyle}
                    onChange={(value: unknown) =>
                      onTypographyChange(
                        groupName,
                        TextStyleProperty.FONT_STYLE,
                        value as string | number
                      )
                    }
                    optionElements={[
                      {
                        value: 'normal',
                        title: (
                          <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_NORMAL" />
                        )
                      },
                      {
                        value: 'italic',
                        title: (
                          <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_ITALIC" />
                        )
                      },
                      {
                        value: 'oblique',
                        title: (
                          <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_OBLIQUE" />
                        )
                      }
                    ]}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        );
      }
    }
  };
  const themedStyles = useThemeConfiguratorThemedStyles();

  useEffect(() => {
    return registerTypographyClassNameChange(setFonts);
  }, [setFonts]);

  useEffect(() => {
    if (focusedTypography) {
      const selectedFont = fonts.get(focusedTypography);
      if (selectedFont) {
        const head = document.head || document.getElementsByTagName('head')[0];
        const style = document.createElement('style');
        style.id = 'fonts-debug';
        const css = Array.from(selectedFont.values()).reduce((acc, curr) => {
          return (
            acc +
            `${curr} {
              outline: -webkit-focus-ring-color auto 5px !important;
            }`
          );
        }, '');

        head.appendChild(style);

        style.appendChild(document.createTextNode(css));

        return () => {
          head.removeChild(style);
        };
      }
    }
  }, [focusedTypography, fonts]);

  return (
    <div className={styles.typographyConfigurator}>
      <div
        className={classNames(
          configuratorStyles.sectionHead,
          themedStyles.sectionHead
        )}
      >
        <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.HEAD" />
      </div>
      <div className={styles.advancedModeToggle}>
        <Switch
          size="small"
          onChange={isChecked => setHasAdvancedMode(isChecked)}
        />
        <span
          className={classNames(
            themedStyles.typographyLabel,
            styles.advancedModeLabel
          )}
        >
          <FormattedMessage id="THEME_CONFIGURATOR.TYPOGRAPHY.ADVANCED_MODE" />
        </span>
      </div>
      <div className={styles.scrollContent}>
        {typographyConfig.map(
          ({ groupName, defaultFields, advancedModeFields }) =>
            (!_isEmpty(defaultFields) || !_isEmpty(advancedModeFields)) && (
              <React.Fragment key={groupName}>
                <div className={styles.textGroupSection}>
                  <div
                    className={classNames(
                      configuratorStyles.groupHead,
                      themedStyles.groupHead
                    )}
                    onClick={() => {
                      if (focusedTypography === groupName) {
                        setFocusedTypography('');
                      } else {
                        setFocusedTypography(groupName);
                      }
                    }}
                  >
                    {groupName}
                  </div>
                  <Row>
                    {defaultFields.map(field => (
                      <React.Fragment key={field}>
                        {buildTypographyInputField(
                          currentTheme.typography,
                          field,
                          groupName
                        )}
                      </React.Fragment>
                    ))}
                    {hasAdvancedMode &&
                      advancedModeFields.map(field => (
                        <React.Fragment key={field}>
                          {buildTypographyInputField(
                            currentTheme.typography,
                            field,
                            groupName
                          )}
                        </React.Fragment>
                      ))}
                  </Row>
                </div>
                <hr
                  className={classNames(
                    configuratorStyles.divider,
                    themedStyles.divider
                  )}
                />
              </React.Fragment>
            )
        )}
      </div>
    </div>
  );
};
