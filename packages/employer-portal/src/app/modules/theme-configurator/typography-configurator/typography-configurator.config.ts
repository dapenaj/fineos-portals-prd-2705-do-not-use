/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { ThemedTypography } from 'fineos-common';

import { TextGroupParent, TextStyleProperty } from '../theme-configurator.enum';

type TypographyLabel =
  | 'Base Text'
  | 'Page Title'
  | 'Panel Title'
  | 'Small Text'
  | 'Display Title'
  | 'Display Small';

export type InheritedField = {
  name: TextStyleProperty;
  ratio?: number;
};

export type CalculatedTypographyConfig = {
  groupName: keyof ThemedTypography;
  label?: TypographyLabel;
  defaultFields: TextStyleProperty[];
  advancedModeFields: TextStyleProperty[];
  valueCalculation?: {
    defaultInheritedFields: InheritedField[];
    optionalInheritedFields: InheritedField[];
    parent: TextGroupParent;
  };
};

export const typographyParents: TextGroupParent[] = [
  TextGroupParent.BASE_TEXT,
  TextGroupParent.DISPLAY_TITLE
];

export const typographyConfig: CalculatedTypographyConfig[] = [
  {
    groupName: 'baseText',
    label: 'Base Text',
    defaultFields: [
      TextStyleProperty.FONT_FAMILY,
      TextStyleProperty.FONT_WEIGHT,
      TextStyleProperty.FONT_SIZE,
      TextStyleProperty.LINE_HEIGHT
    ],
    advancedModeFields: []
  },
  {
    groupName: 'pageTitle',
    label: 'Page Title',
    defaultFields: [TextStyleProperty.FONT_WEIGHT],
    advancedModeFields: [
      TextStyleProperty.FONT_SIZE,
      TextStyleProperty.LINE_HEIGHT
    ],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE }
      ],
      optionalInheritedFields: [
        { name: TextStyleProperty.FONT_SIZE, ratio: 1.71 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 1.58 }
      ]
    }
  },
  {
    groupName: 'panelTitle',
    label: 'Panel Title',
    defaultFields: [TextStyleProperty.FONT_WEIGHT],
    advancedModeFields: [
      TextStyleProperty.FONT_SIZE,
      TextStyleProperty.LINE_HEIGHT
    ],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE }
      ],
      optionalInheritedFields: [
        { name: TextStyleProperty.FONT_SIZE, ratio: 1.29 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 1.32 }
      ]
    }
  },
  {
    groupName: 'baseTextSemiBold',
    defaultFields: [],
    advancedModeFields: [],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE },
        { name: TextStyleProperty.FONT_SIZE, ratio: 1 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 1 }
      ],
      optionalInheritedFields: []
    }
  },
  {
    groupName: 'smallText',
    label: 'Small Text',
    defaultFields: [TextStyleProperty.FONT_WEIGHT],
    advancedModeFields: [
      TextStyleProperty.FONT_SIZE,
      TextStyleProperty.LINE_HEIGHT
    ],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE }
      ],
      optionalInheritedFields: [
        { name: TextStyleProperty.FONT_SIZE, ratio: 0.86 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 0.84 }
      ]
    }
  },
  {
    groupName: 'smallTextSemiBold',
    defaultFields: [],
    advancedModeFields: [],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE },
        { name: TextStyleProperty.FONT_SIZE, ratio: 0.86 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 0.84 }
      ],
      optionalInheritedFields: []
    }
  },
  {
    groupName: 'baseTextUpperCase',
    defaultFields: [],
    advancedModeFields: [],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE },
        { name: TextStyleProperty.FONT_WEIGHT },
        { name: TextStyleProperty.FONT_SIZE, ratio: 1 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 1 }
      ],
      optionalInheritedFields: []
    }
  },
  {
    groupName: 'smallTextUpperCase',
    defaultFields: [],
    advancedModeFields: [],
    valueCalculation: {
      parent: TextGroupParent.BASE_TEXT,
      defaultInheritedFields: [
        { name: TextStyleProperty.FONT_FAMILY },
        { name: TextStyleProperty.FONT_STYLE },
        { name: TextStyleProperty.FONT_WEIGHT },
        { name: TextStyleProperty.FONT_SIZE, ratio: 0.79 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 0.79 }
      ],
      optionalInheritedFields: []
    }
  },
  {
    groupName: 'displayTitle',
    label: 'Display Title',
    defaultFields: [
      TextStyleProperty.FONT_FAMILY,
      TextStyleProperty.FONT_STYLE,
      TextStyleProperty.FONT_WEIGHT,
      TextStyleProperty.FONT_SIZE,
      TextStyleProperty.LINE_HEIGHT
    ],
    advancedModeFields: []
  },
  {
    groupName: 'displaySmall',
    label: 'Display Small',
    defaultFields: [
      TextStyleProperty.FONT_STYLE,
      TextStyleProperty.FONT_WEIGHT
    ],
    advancedModeFields: [
      TextStyleProperty.FONT_SIZE,
      TextStyleProperty.LINE_HEIGHT
    ],
    valueCalculation: {
      parent: TextGroupParent.DISPLAY_TITLE,
      defaultInheritedFields: [{ name: TextStyleProperty.FONT_FAMILY }],
      optionalInheritedFields: [
        { name: TextStyleProperty.FONT_SIZE, ratio: 0.67 },
        { name: TextStyleProperty.LINE_HEIGHT, ratio: 0.68 }
      ]
    }
  }
];
