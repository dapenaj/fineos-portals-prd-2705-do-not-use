/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useMemo, useEffect } from 'react';
import {
  Button,
  ButtonType,
  PopoverThemeConfigurator,
  ThemedColorSchema,
  ThemedHeader,
  ThemedImages,
  ThemedTypography,
  ThemeModel,
  UploadedFile,
  FormattedMessage,
  Icon
} from 'fineos-common';
import {
  cloneDeep as _cloneDeep,
  isEmpty as _isEmpty,
  merge as _merge
} from 'lodash';
import classNames from 'classnames';
import { saveAs } from 'file-saver';
import JSzip from 'jszip';
import {
  faPalette,
  faTextHeight,
  faUndo,
  faDownload,
  faIcons,
  faEllipsisH
} from '@fortawesome/free-solid-svg-icons';
import { faImages } from '@fortawesome/free-regular-svg-icons';

import { ConfiguratorSections } from './theme-configurator.enum';
import { ColorPaletteConfigurator } from './color-palette-configurator/color-palette-configurator.component';
import { TypographyConfigurator } from './typography-configurator/typography-configurator.component';
import { ImageConfigurator } from './image-configurator/image-configurator.component';
import { HeaderConfigurator } from './header-configurator/header-configurator.component';
import {
  CUSTOM_ICON_PATH,
  CUSTOM_IMG_PATH,
  DEFAULT_TOOLTIP_PROPS,
  ICON_SIZE,
  ZIP_FOLDER_NAME
} from './theme-configurator.const';
import {
  encodeContentBasedName,
  base64ToBlob,
  findUniqueImageGroups
} from './theme-configurator.utils';
import { ClientConfig } from './../../shared';
import { IconConfigurator } from './icon-configurator/icon-configurator.component';
import { useThemeConfiguratorThemedStyles } from './theme-configurator-themed-styles.hook';
import styles from './theme-configurator.module.scss';

type ThemeConfigiratorProps = {
  isFixed?: boolean;
  isHighContrastOn: boolean;
  config: ClientConfig;
  onConfigChange: (config: ClientConfig) => void;
};

export const ThemeConfigurator: React.FC<ThemeConfigiratorProps> = ({
  isFixed = true,
  isHighContrastOn,
  config,
  onConfigChange
}) => {
  // eslint-disable-next-line
  const initialConfig = useMemo(() => _cloneDeep(config), []);
  const [isVisible, setIsVisible] = useState(true);
  const [
    activeSection,
    setActiveSection
  ] = useState<ConfiguratorSections | null>(null);
  const [customLogo, setCustomLogo] = useState<UploadedFile | null>(null);
  const [customImages, setCustomImages] = useState<Record<
    string,
    UploadedFile
  > | null>();
  const [customIcons, setCustomIcons] = useState<Record<string, string>>({});

  useEffect(() => {
    const toggleConfigurator = (e: KeyboardEvent) => {
      if (e.ctrlKey && e.key === 'u') {
        setIsVisible(visible => !visible);
      }
    };
    document.body.addEventListener('keyup', toggleConfigurator);
    return () => {
      document.body.removeEventListener('keyup', toggleConfigurator);
    };
  }, []);

  const onConfigPartChange = (
    changedConfig:
      | ThemedColorSchema
      | ThemedHeader
      | ThemedImages
      | ThemedTypography
      | Record<string, string>,
    parameter: keyof ThemeModel
  ) => {
    const newConfig = _merge({}, config, {
      theme: isHighContrastOn
        ? {
            ...config.theme,
            highContrastMode: {
              ...config.theme.highContrastMode,
              [parameter]: changedConfig
            }
          }
        : {
            ...config.theme,
            [parameter]: changedConfig
          }
    });
    onConfigChange(newConfig);
  };

  const handleCustomImageAdd = async (
    image: UploadedFile,
    imageGroup: keyof ThemedImages
  ) => {
    const encodedName = await encodeContentBasedName(image.file);
    setCustomImages({
      ...customImages,
      [imageGroup]: { file: image.file, name: encodedName, type: image.type }
    });
  };

  const handleCustomIconAdd = (image: string, imageGroup: string) => {
    setCustomIcons({
      ...customIcons,
      [imageGroup]: image
    });
  };

  const handleCustomLogoAdd = async (logo: UploadedFile) => {
    const encodedName = await encodeContentBasedName(logo.file);
    setCustomLogo({
      file: logo.file,
      name: encodedName,
      type: logo.type
    });
  };

  const handleConfigExport = async () => {
    const exportedConfig = _cloneDeep(config);
    const zip = new JSzip();
    const mainFolder = zip.folder('theme');
    const customImagesFolder = mainFolder?.folder('assets')
      ?.folder('img')
      ?.folder('custom-images');
    const customIconsFolder = mainFolder?.folder('assets')?.folder('icon')?.folder('custom-icons');

    if (!_isEmpty(customIcons)) {
      for (const [iconName, icon] of Object.entries(customIcons)) {
        const persistedSvgName = (await encodeContentBasedName(icon)) + '.svg';
        customIconsFolder?.file(persistedSvgName, icon);
        (exportedConfig as any).theme.icons[
          iconName
        ] = `${CUSTOM_ICON_PATH}/${persistedSvgName}`;
      }
    }

    if (!_isEmpty(customImages)) {
      Object.keys(customImages!).forEach(image => {
        let hasInitialImage = false;
        const currentImage = (customImages as Record<string, UploadedFile>)[
          image
        ];
        const imageFullName = `${currentImage.name}.${currentImage.type}`;
        const blobImage = base64ToBlob(currentImage);
        const uniqueImageGroups = findUniqueImageGroups(customImages!, image);
        (exportedConfig as any).theme.images[
          image
        ] = `${CUSTOM_IMG_PATH}/${imageFullName}`;
        if (!uniqueImageGroups.length || !hasInitialImage) {
          customImagesFolder?.file(imageFullName, blobImage);
          hasInitialImage = true;
        }
      });
    }

    if (customLogo) {
      const blobImage = base64ToBlob(customLogo);
      const imageFullName = `${customLogo.name}.${customLogo.type}`;
      exportedConfig.theme.header.logo = `${CUSTOM_IMG_PATH}/${imageFullName}`;
      customImagesFolder?.file(imageFullName, blobImage);
    }

    mainFolder?.folder('config')?.file('client-config.json', JSON.stringify(exportedConfig));
    zip.generateAsync({ type: 'blob' }).then(content => {
      saveAs(content, ZIP_FOLDER_NAME);
    });
  };

  const handleConfigReset = () => {
    setCustomImages({});
    setCustomIcons(initialConfig.theme.icons || {});
    setCustomLogo(null);
    onConfigChange(initialConfig);
  };

  const getButtonClassNames = (buttonSection: ConfiguratorSections) => [
    styles.sidebarButton,
    themedStyles.sidebarButton,
    buttonSection === activeSection ? themedStyles.activeButton : null
  ];

  const themedStyles = useThemeConfiguratorThemedStyles();
  return (
    <>
      {initialConfig && isVisible && (
        <div
          data-test-el="theme-configurator"
          className={classNames(
            styles.sidebarWrapper,
            themedStyles.sidebarWrapper,
            {
              [styles.fixed]: isFixed
            }
          )}
        >
          <div className={styles.configuratorSections}>
            <PopoverThemeConfigurator
              {...DEFAULT_TOOLTIP_PROPS}
              overlayClassName={styles.colorsPopover}
              content={
                <ColorPaletteConfigurator
                  currentColorSchema={
                    isHighContrastOn
                      ? config.theme.highContrastMode?.colorSchema!
                      : config.theme.colorSchema
                  }
                  onConfigChange={palette =>
                    onConfigPartChange(palette, 'colorSchema')
                  }
                />
              }
            >
              <Button
                buttonType={ButtonType.GHOST}
                onClick={() => setActiveSection(ConfiguratorSections.COLORS)}
                className={classNames(
                  getButtonClassNames(ConfiguratorSections.COLORS)
                )}
              >
                <Icon
                  icon={faPalette}
                  size={ICON_SIZE}
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.PALETTE_ICON_LABEL" />
                  }
                />
                <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.COLORS" />
              </Button>
            </PopoverThemeConfigurator>
            <PopoverThemeConfigurator
              {...DEFAULT_TOOLTIP_PROPS}
              overlayClassName={styles.typographyPopover}
              content={
                <TypographyConfigurator
                  currentTheme={
                    isHighContrastOn
                      ? config.theme.highContrastMode!
                      : config.theme
                  }
                  onConfigChange={typography =>
                    onConfigPartChange(typography, 'typography')
                  }
                />
              }
            >
              <Button
                buttonType={ButtonType.GHOST}
                onClick={() =>
                  setActiveSection(ConfiguratorSections.TYPOGRAPHY)
                }
                className={classNames(
                  getButtonClassNames(ConfiguratorSections.TYPOGRAPHY)
                )}
              >
                <Icon
                  icon={faTextHeight}
                  size={ICON_SIZE}
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.TEXT_HEIGHT_ICON_LABEL" />
                  }
                />
                <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.TYPOGRAPHY" />
              </Button>
            </PopoverThemeConfigurator>
            <PopoverThemeConfigurator
              {...DEFAULT_TOOLTIP_PROPS}
              overlayClassName={styles.imagesPopover}
              content={
                <ImageConfigurator
                  currentImages={
                    isHighContrastOn
                      ? config.theme.highContrastMode?.images!
                      : config.theme.images!
                  }
                  onCustomImageAdd={(image, imageGroup) =>
                    handleCustomImageAdd(image, imageGroup)
                  }
                  onConfigChange={images =>
                    onConfigPartChange(images, 'images')
                  }
                />
              }
            >
              <Button
                buttonType={ButtonType.GHOST}
                onClick={() => setActiveSection(ConfiguratorSections.IMAGES)}
                className={classNames(
                  getButtonClassNames(ConfiguratorSections.IMAGES)
                )}
              >
                <Icon
                  icon={faImages}
                  size={ICON_SIZE}
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.IMAGES_ICON_LABEL" />
                  }
                />
                <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.IMAGES" />
              </Button>
            </PopoverThemeConfigurator>
            <PopoverThemeConfigurator
              {...DEFAULT_TOOLTIP_PROPS}
              overlayClassName={styles.imagesPopover}
              content={
                <IconConfigurator
                  currentImages={
                    isHighContrastOn
                      ? config.theme.highContrastMode!.icons!
                      : config.theme.icons!
                  }
                  onCustomIconAdd={handleCustomIconAdd}
                  onConfigChange={icons => onConfigPartChange(icons, 'icons')}
                />
              }
            >
              <Button
                buttonType={ButtonType.GHOST}
                onClick={() => setActiveSection(ConfiguratorSections.ICONS)}
                className={classNames(
                  getButtonClassNames(ConfiguratorSections.ICONS)
                )}
              >
                <Icon
                  icon={faIcons}
                  size={ICON_SIZE}
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.IMAGES_ICON_LABEL" />
                  }
                />
                <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.ICONS" />
              </Button>
            </PopoverThemeConfigurator>
            <PopoverThemeConfigurator
              {...DEFAULT_TOOLTIP_PROPS}
              overlayClassName={styles.headerPopover}
              content={
                <HeaderConfigurator
                  currentTheme={
                    isHighContrastOn
                      ? config.theme.highContrastMode!
                      : config.theme
                  }
                  onCustomLogoAdd={logo => handleCustomLogoAdd(logo)}
                  onConfigChange={header =>
                    onConfigPartChange(header, 'header')
                  }
                />
              }
            >
              <Button
                buttonType={ButtonType.GHOST}
                onClick={() => setActiveSection(ConfiguratorSections.HEADER)}
                className={classNames(
                  getButtonClassNames(ConfiguratorSections.HEADER)
                )}
              >
                <Icon
                  icon={faEllipsisH}
                  size={ICON_SIZE}
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.ELLIPSIS_ICON_LABEL" />
                  }
                />
                <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.HEADER" />
              </Button>
            </PopoverThemeConfigurator>
          </div>
          <div className={styles.actionButtons}>
            <Button
              buttonType={ButtonType.GHOST}
              className={classNames(
                styles.sidebarButton,
                themedStyles.sidebarButton
              )}
              onClick={handleConfigReset}
            >
              <Icon
                icon={faUndo}
                size={ICON_SIZE}
                iconLabel={
                  <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.UNDO_ICON_LABEL" />
                }
              />
              <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.REVERT" />
            </Button>
            <Button
              buttonType={ButtonType.GHOST}
              className={classNames(
                styles.sidebarButton,
                themedStyles.sidebarButton
              )}
              onClick={handleConfigExport}
            >
              <Icon
                icon={faDownload}
                size={ICON_SIZE}
                iconLabel={
                  <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.DOWNLOAD_ICON_LABEL" />
                }
              />
              <FormattedMessage id="THEME_CONFIGURATOR.NAVBAR.EXPORT" />
            </Button>
          </div>
        </div>
      )}
    </>
  );
};
