/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { isEmpty as _isEmpty } from 'lodash';
import { ThemeModel, createThemedStyles, textStyleToCss } from 'fineos-common';

// To increase Theme Configurator readability, theme font sizes
// and color-palette visual feedback should be not applied to it.
// Unusual useThemedStyles construction overrides themed font sizes and colours.
let initialTheme = {} as ThemeModel;
export const useThemeConfiguratorThemedStyles = createThemedStyles(theme => {
  if (_isEmpty(initialTheme)) {
    initialTheme = theme;
  }
  return {
    sidebarWrapper: {
      background: initialTheme.colorSchema.neutral4
    },
    sidebarButton: {
      ...textStyleToCss(theme.typography.smallText),
      background: theme.colorSchema.neutral3,
      fontSize: initialTheme.typography.smallText.fontSize,
      '&:hover': {
        color: initialTheme.colorSchema.primaryAction
      },
      '&:focus': {
        color: initialTheme.colorSchema.primaryAction
      }
    },
    activeButton: {
      color: initialTheme.colorSchema.primaryAction
    },
    title: {
      ...textStyleToCss(theme.typography.panelTitle),
      color: theme.colorSchema.neutral8,
      fontSize: initialTheme.typography.panelTitle.fontSize
    },
    groupHead: {
      ...textStyleToCss(theme.typography.baseText),
      color: initialTheme.colorSchema.neutral7,
      fontSize: initialTheme.typography.baseText.fontSize
    },
    typographyLabel: {
      ...textStyleToCss(theme.typography.smallText),
      fontSize: initialTheme.typography.smallText.fontSize
    },
    sectionHead: {
      ...textStyleToCss(theme.typography.baseTextSemiBold),
      fontSize: initialTheme.typography.baseTextSemiBold.fontSize
    },
    infoIcon: {
      fontSize: initialTheme.typography.panelTitle.fontSize
    },
    divider: {
      borderColor: initialTheme.colorSchema.neutral5
    },
    headerLogoPopover: {
      '& .ant-popover-inner-content': {
        background: theme.colorSchema.primaryColor
      }
    }
  };
});
