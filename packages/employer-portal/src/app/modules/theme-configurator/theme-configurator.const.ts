/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { AbstractTooltipProps } from 'antd/lib/tooltip';

export const ICON_SIZE = '2x';
export const ZIP_FOLDER_NAME = 'theme';
export const CUSTOM_IMG_PATH = '/assets/img/custom-images';
export const CUSTOM_ICON_PATH = '/assets/icon/custom-icons';
export const DEFAULT_TOOLTIP_PROPS = {
  trigger: 'click',
  placement: 'leftTop'
} as AbstractTooltipProps & { content: React.ReactElement };
