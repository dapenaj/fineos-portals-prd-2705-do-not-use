/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import classNames from 'classnames';
import {
  Button,
  PopoverThemeConfigurator,
  FormattedMessage
} from 'fineos-common';
import { InfoCircleOutlined } from '@ant-design/icons';

import { useThemeConfiguratorThemedStyles } from '../theme-configurator-themed-styles.hook';
import configuratorStyles from '../theme-configurator.module.scss';

import styles from './image-configurator.module.scss';

type ImageConfiguratorLoaderProps = {
  imageGroup: string;
  imageList: Record<string, string>;
  isSeparatorVisible?: boolean;
  popoverClassName?: string;
  onCurrentAddingChange: (imageGroup: string) => void;
};

export const ImageConfiguratorLoader: React.FC<ImageConfiguratorLoaderProps> = ({
  imageGroup,
  imageList,
  isSeparatorVisible = true,
  popoverClassName,
  onCurrentAddingChange
}) => {
  const themedStyles = useThemeConfiguratorThemedStyles();
  return (
    <>
      <div
        className={classNames(
          configuratorStyles.groupHead,
          themedStyles.groupHead
        )}
      >
        <FormattedMessage
          id={`THEME_CONFIGURATOR.IMAGE_CONFIGURATOR.${imageGroup
            .replace(/([a-z])([A-Z])/g, '$1_$2')
            .toLocaleUpperCase()}`}
        />
      </div>
      <Row align="middle">
        <Col>
          <Button onClick={() => onCurrentAddingChange(imageGroup)}>
            <FormattedMessage id="FINEOS_COMMON.GENERAL.LOAD" />
          </Button>
        </Col>
        <Col>
          <PopoverThemeConfigurator
            placement="bottomLeft"
            overlayClassName={popoverClassName}
            title={
              <FormattedMessage id="THEME_CONFIGURATOR.IMAGES.PREVIEW_HEAD" />
            }
            content={
              <img
                className={styles.previewImage}
                src={imageList[imageGroup]}
                alt={imageList[imageGroup]}
              />
            }
          >
            <InfoCircleOutlined
              className={classNames(
                configuratorStyles.infoIcon,
                themedStyles.infoIcon
              )}
            />
          </PopoverThemeConfigurator>
        </Col>
      </Row>
      {isSeparatorVisible && (
        <hr
          className={classNames(
            themedStyles.divider,
            configuratorStyles.divider
          )}
        />
      )}
    </>
  );
};
