/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import {
  ThemedImages,
  Modal,
  FileUpload,
  UploadedFile,
  RichFormattedMessage,
  FormattedMessage
} from 'fineos-common';
import classNames from 'classnames';

import { useThemeConfiguratorThemedStyles } from '../theme-configurator-themed-styles.hook';
import configuratorStyles from '../theme-configurator.module.scss';

import { ImageConfiguratorLoader } from './image-configurator-loader.component';
import { recomendedImageSizes } from './recommended-image-sizes.config';
import styles from './image-configurator.module.scss';

type ImagesConfiguratorProps = {
  currentImages: ThemedImages;
  onCustomImageAdd: (
    image: UploadedFile,
    imageGroup: keyof ThemedImages
  ) => void;
  onConfigChange: (typography: ThemedImages) => void;
};

export const ImageConfigurator: React.FC<ImagesConfiguratorProps> = ({
  currentImages,
  onCustomImageAdd,
  onConfigChange
}) => {
  const [imageList, setImageList] = useState<Record<string, string> | null>(
    null
  );
  const [currentAdding, setCurrentAdding] = useState<keyof ThemedImages | null>(
    null
  );

  useEffect(() => {
    setImageList(currentImages as Record<string, string>);
  }, [currentImages]);

  const handleImageUpload = (
    image: UploadedFile,
    imageGroup: keyof ThemedImages
  ) => {
    onCustomImageAdd(image, imageGroup);
    setCurrentAdding(null);
    onConfigChange({
      ...currentImages,
      [imageGroup]: `data:image/${image.type};base64, ${image.file}`
    });
  };

  const themedStyles = useThemeConfiguratorThemedStyles();
  return (
    <div className={styles.images}>
      <div
        className={classNames(
          configuratorStyles.sectionHead,
          themedStyles.sectionHead,
          styles.head
        )}
      >
        <FormattedMessage id="THEME_CONFIGURATOR.IMAGES.HEAD" />
      </div>
      {imageList &&
        Object.keys(currentImages).map((imageGroup, index) => (
          <ImageConfiguratorLoader
            key={imageGroup}
            imageGroup={imageGroup}
            imageList={imageList}
            isSeparatorVisible={index < Object.keys(currentImages).length - 1}
            onCurrentAddingChange={() =>
              setCurrentAdding(imageGroup as keyof ThemedImages)
            }
          />
        ))}
      <Modal
        ariaLabelId="THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_MODAL_TITLE"
        header={
          <FormattedMessage id="THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_MODAL_TITLE" />
        }
        visible={!!currentAdding}
        onCancel={() => setCurrentAdding(null)}
      >
        <FileUpload
          accept={['.jpg', '.jpeg', '.png']}
          uploadedMediaTypeLabel="image"
          isLoading={false}
          onCancel={() => setCurrentAdding(null)}
          onSubmit={file => handleImageUpload(file, currentAdding!)}
          maxFileSize="2MB"
        >
          {currentAdding && (
            <RichFormattedMessage
              id="THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_RECOMMENDED_SIZE"
              values={{
                imageGroup: <strong>{currentAdding}</strong>,
                width: (
                  <strong>{recomendedImageSizes[currentAdding].width}</strong>
                ),
                height: (
                  <strong>{recomendedImageSizes[currentAdding].height}</strong>
                )
              }}
            />
          )}
        </FileUpload>
      </Modal>
    </div>
  );
};
