/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Input, InputNumber } from 'antd';
import classNames from 'classnames';
import {
  ThemedHeader,
  Modal,
  FileUpload,
  UploadedFile,
  RichFormattedMessage,
  FormattedMessage,
  ThemeModel
} from 'fineos-common';

import { useThemeConfiguratorThemedStyles } from '../theme-configurator-themed-styles.hook';
import { ImageConfiguratorLoader } from '../image-configurator/image-configurator-loader.component';
import { recomendedImageSizes } from '../image-configurator/recommended-image-sizes.config';
import configuratorStyles from '../theme-configurator.module.scss';

import styles from './header-configurator.module.scss';

type HeaderConfiguratorProps = {
  currentTheme: ThemeModel;
  onCustomLogoAdd: (logo: UploadedFile) => void;
  onConfigChange: (header: ThemedHeader) => void;
};

export const HeaderConfigurator: React.FC<HeaderConfiguratorProps> = ({
  currentTheme,
  onCustomLogoAdd,
  onConfigChange
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const handleLogoChange = (image: UploadedFile) => {
    onCustomLogoAdd(image);
    onConfigChange({
      ...currentTheme.header,
      logo: `data:image/${image.type};base64, ${image.file}`
    });
    setIsModalVisible(false);
  };

  const themedStyles = useThemeConfiguratorThemedStyles();
  return (
    <div className={styles.headerConfigurator}>
      <div
        className={classNames(
          configuratorStyles.sectionHead,
          themedStyles.sectionHead
        )}
      >
        <FormattedMessage id="THEME_CONFIGURATOR.HEADER.HEAD" />
      </div>
      <div className={styles.groupSection}>
        <label
          htmlFor="pageTitle"
          className={classNames(
            configuratorStyles.groupHead,
            themedStyles.groupHead
          )}
        >
          <FormattedMessage id="THEME_CONFIGURATOR.HEADER.PAGE_TITLE" />
        </label>
        <Input
          id="pageTitle"
          value={currentTheme.header.title}
          onChange={e => {
            onConfigChange({
              ...currentTheme.header,
              hideTitle: false,
              titleSeparator: e.target.value
                ? currentTheme.header.titleSeparator
                : '',
              title: e.target.value
            });
          }}
        />
      </div>
      <div className={styles.groupSection}>
        <label
          htmlFor="titleSeparator"
          className={classNames(
            configuratorStyles.groupHead,
            themedStyles.groupHead
          )}
        >
          <FormattedMessage id="THEME_CONFIGURATOR.HEADER.TITLE_SEPARATOR" />
        </label>
        <Input
          id="titleSeparator"
          value={currentTheme.header.titleSeparator}
          onChange={e => {
            onConfigChange({
              ...currentTheme.header,
              titleSeparator: e.target.value
            });
          }}
        />
      </div>

      <div className={styles.groupSection}>
        <label
          htmlFor="titleFontSize"
          className={classNames(
            configuratorStyles.groupHead,
            themedStyles.groupHead
          )}
        >
          <FormattedMessage id="THEME_CONFIGURATOR.HEADER.TITLE_FONT_SIZE" />
        </label>
        <InputNumber
          id="titleFontSize"
          precision={0}
          min={1}
          value={
            currentTheme.header.titleStyles?.fontSize ||
            currentTheme.typography.baseText.fontSize
          }
          onChange={value => {
            onConfigChange({
              ...currentTheme.header,
              titleStyles: {
                ...currentTheme.header.titleStyles,
                fontSize: value as number
              }
            });
          }}
        />
      </div>

      <div className={styles.groupSection}>
        <label
          htmlFor="logoAltText"
          className={classNames(
            configuratorStyles.groupHead,
            themedStyles.groupHead
          )}
        >
          <FormattedMessage id="THEME_CONFIGURATOR.HEADER.LOGO_ALT_TEXT" />
        </label>
        <Input
          id="logoAltText"
          value={currentTheme.header.logoAltText}
          onChange={e => {
            onConfigChange({
              ...currentTheme.header,
              logoAltText: e.target.value
            });
          }}
        />
      </div>

      <div className={styles.groupSection}>
        <ImageConfiguratorLoader
          imageGroup="logo"
          imageList={{ logo: currentTheme.header.logo }}
          isSeparatorVisible={false}
          popoverClassName={themedStyles.headerLogoPopover}
          onCurrentAddingChange={() => setIsModalVisible(true)}
        />
      </div>
      <Modal
        ariaLabelId="THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_MODAL_TITLE"
        header={
          <FormattedMessage id="THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_MODAL_TITLE" />
        }
        visible={isModalVisible}
        onCancel={() => setIsModalVisible(false)}
      >
        <FileUpload
          data-test-el="header-upload"
          accept={['.jpg', '.jpeg', '.png']}
          uploadedMediaTypeLabel="image"
          isLoading={false}
          onCancel={() => setIsModalVisible(false)}
          onSubmit={image => handleLogoChange(image)}
          maxFileSize="2MB"
        >
          <RichFormattedMessage
            id="THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_RECOMMENDED_SIZE"
            values={{
              imageGroup: <strong>logo</strong>,
              width: <strong>{recomendedImageSizes.logo.width}</strong>,
              height: <strong>{recomendedImageSizes.logo.height}</strong>
            }}
          />
        </FileUpload>
      </Modal>
    </div>
  );
};
