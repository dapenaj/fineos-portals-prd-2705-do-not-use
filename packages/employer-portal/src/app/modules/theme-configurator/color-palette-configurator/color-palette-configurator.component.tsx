/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';
import InputColor, { Color } from 'react-input-color';
import classNames from 'classnames';
import {
  ThemedColorSchema,
  colorVariation,
  FormattedMessage
} from 'fineos-common';

import { useThemeConfiguratorThemedStyles } from '../theme-configurator-themed-styles.hook';
import configuratorStyles from '../theme-configurator.module.scss';

import { toColor, makeColorBright } from './color-palette-configurator.utils';

type ColorPaletteConfigiratorProps = {
  currentColorSchema: ThemedColorSchema;
  onConfigChange: (colorSchema: ThemedColorSchema) => void;
};

export const ColorPaletteConfigurator: React.FC<ColorPaletteConfigiratorProps> = ({
  currentColorSchema,
  onConfigChange
}) => {
  const [focusedColor, setFocusedColor] = useState('');

  useEffect(() => {
    onConfigChange(
      (Object.keys(currentColorSchema) as (keyof ThemedColorSchema)[]).reduce(
        (acc: any, curr: keyof ThemedColorSchema) => {
          acc[curr] = `var(--theme-color-${curr}, ${toColor(
            currentColorSchema[curr]!
          )})`;
          return acc;
        },
        {}
      )
    );
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (focusedColor) {
      const mainColorsToHide = Object.entries(currentColorSchema).filter(
        ([colorName]) => focusedColor !== colorName
      ) as [string, string][];
      const dependentColorsToHide = (Array.from<
        [keyof ThemedColorSchema, Map<string, (color: string) => string>]
      >(colorVariation.entries())
        .filter(([colorName]) => focusedColor !== colorName)
        .flatMap(([colorName, allPalette]) =>
          Array.from(allPalette.entries()).map(palette => [
            colorName,
            ...palette
          ])
        ) as unknown) as [
        keyof ThemedColorSchema,
        string,
        (color: string) => string
      ][];

      const newVariables = mainColorsToHide
        .map(
          ([colorName, colorOrVar]) =>
            `--theme-color-${colorName}: ${makeColorBright(colorOrVar)};`
        )
        .concat(
          dependentColorsToHide.map(
            ([colorName, colorVar, mutation]) =>
              `${colorVar}: ${makeColorBright(
                mutation(currentColorSchema[colorName]!)
              )};`
          )
        )
        .join('\n');

      const head = document.head || document.getElementsByTagName('head')[0];
      const style = document.createElement('style');
      style.id = 'color-debug';

      head.insertBefore(style, head.firstChild);

      style.appendChild(
        document.createTextNode(`
        :root {
          ${newVariables}
        }`)
      );
      return () => {
        head.removeChild(style);
      };
    }
    // eslint-disable-next-line
  }, [focusedColor]);

  const onColorSchemaChange = (color: string, value: Color) => {
    if (!/^#[0-9A-F]{6}$/i.test(value.hex)) {
      return;
    }
    onConfigChange({
      ...currentColorSchema,
      [color]: `var(--theme-color-${color}, ${value.hex})`
    });
  };

  const themedStyles = useThemeConfiguratorThemedStyles();
  return (
    <>
      <div
        className={classNames(
          configuratorStyles.sectionHead,
          themedStyles.sectionHead
        )}
      >
        <FormattedMessage id="THEME_CONFIGURATOR.COLORS.HEAD" />
      </div>
      {Object.keys(currentColorSchema).map((color: string) => (
        <div key={color}>
          {color === 'neutral9' && (
            <hr
              className={classNames(
                themedStyles.divider,
                configuratorStyles.divider
              )}
            />
          )}
          <Row className={configuratorStyles.row} gutter={[20, 0]}>
            <Col sm={6}>
              <div className={configuratorStyles.inputColor}>
                <InputColor
                  initialValue={toColor((currentColorSchema as any)[color])}
                  placement="left"
                  onChange={(value: Color) => onColorSchemaChange(color, value)}
                />
              </div>
            </Col>
            <Col sm={6}>
              <span
                className={classNames(
                  configuratorStyles.groupHead,
                  themedStyles.groupHead
                )}
                onClick={() => {
                  if (focusedColor === color) {
                    setFocusedColor('');
                  } else {
                    setFocusedColor(color);
                  }
                }}
              >
                {color}
              </span>
            </Col>
          </Row>
        </div>
      ))}
    </>
  );
};
