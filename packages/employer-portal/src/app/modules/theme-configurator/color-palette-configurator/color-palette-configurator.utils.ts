/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

type RgbaColor = [number, number, number, number];

const HEX_COLOR_RE = /(?:#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2}))/;
const RGBA_COLOR_RE = /rgba\(([\d]+?)\s*?,\s*?([\d]+?)\s*?,\s*([\d]+?)\s*?,\s*?([\d.]+?)\s*\)/;

export const toColor = (colorOrVar: string): string => {
  const [, r, g, b] = HEX_COLOR_RE.exec(colorOrVar)!;
  return '#' + [r, g, b].join('');
};

export const toRgbaColor = (colorOrVar: string): RgbaColor => {
  if (HEX_COLOR_RE.test(colorOrVar)) {
    const [, r, g, b] = HEX_COLOR_RE.exec(colorOrVar)!;
    return [parseInt(r, 16), parseInt(g, 16), parseInt(b, 16), 1];
  } else if (RGBA_COLOR_RE.test(colorOrVar)) {
    const [, r, g, b, a] = RGBA_COLOR_RE.exec(colorOrVar)!;
    return [parseInt(r, 10), parseInt(g, 10), parseInt(b, 10), parseFloat(a)];
  }

  return [0, 0, 0, 1];
};

export const makeColorBright = (colorOrVar: string) => {
  const [r, g, b, a] = toRgbaColor(colorOrVar);
  return `rgba(${r}, ${g}, ${b}, ${(a * 0.15).toFixed(2)})`;
};
