/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { UploadedFile } from 'fineos-common';

export const base64ToBlob = async (image: UploadedFile) => {
  const imgString = atob(image.file);
  const imgArrayBuffer = new ArrayBuffer(imgString.length);
  const imgUintArray = new Uint8Array(imgArrayBuffer);
  for (let index = 0; index < imgString.length; index++) {
    imgUintArray[index] = imgString.charCodeAt(index);
  }
  return new Blob([imgUintArray], { type: `image/${image.type}` });
};

export const encodeContentBasedName = async (file: string): Promise<string> => {
  const msgUint8 = new TextEncoder().encode(file);
  const hashBuffer = await crypto.subtle.digest('SHA-1', msgUint8);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  return hashArray.map(hash => hash.toString(16).padStart(2, '0')).join('');
};

export const findUniqueImageGroups = (
  customImages: Record<string, UploadedFile>,
  currentImage: string
) =>
  Object.keys(customImages).filter(
    image =>
      image !== currentImage &&
      customImages[currentImage].name === customImages[image].name
  );
