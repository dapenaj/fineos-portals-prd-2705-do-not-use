/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  createThemedStyles,
  FormattedMessage,
  Modal,
  Spinner,
  textStyleToCss
} from 'fineos-common';
import React from 'react';

import styles from './logout.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  spinner: {
    color: theme.colorSchema.neutral8
  },
  info: {
    color: theme.colorSchema.neutral1,
    ...textStyleToCss(theme.typography.pageTitle)
  },
  classNameSpin: {
    '& .ant-spin': {
      color: theme.colorSchema.neutral1
    },
    '& .ant-spin-dot-item': {
      backgroundColor: theme.colorSchema.neutral1
    }
  }
}));

export const LogoutSpinner = () => {
  const themedStyles = useThemedStyles();

  return (
    <Modal
      ariaLabelId="LOGOUT.DURING_LOGOUT"
      visible={true}
      key={1}
      header={null}
      closable={false}
      dialogClassName={styles.wrapper}
    >
      <div className={styles.spinner}>
        <FormattedMessage
          className={themedStyles.info}
          id="LOGOUT.DURING_LOGOUT"
        />
        <Spinner
          classNameSpin={themedStyles.classNameSpin}
          className={themedStyles.spinner}
        />
      </div>
    </Modal>
  );
};
