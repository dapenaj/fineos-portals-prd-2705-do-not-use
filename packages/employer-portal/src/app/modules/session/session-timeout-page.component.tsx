/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  createThemedStyles,
  Modal,
  FormattedMessage,
  Button,
  textStyleToCss
} from 'fineos-common';
import { Row, Col } from 'antd';
import { SdkConfigProps } from 'fineos-js-api-client';
import classNames from 'classnames';

import styles from './session.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  sessionPage: {
    backgroundColor: theme.colorSchema.neutral2,
    backgroundImage: `url(${theme?.images?.welcomeBackground})`
  },
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  text: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const SessionTimeoutPage = () => {
  const themedStyles = useThemedStyles();

  return (
    <div className={classNames(styles.sessionPage, themedStyles.sessionPage)}>
      <Modal
        ariaLabelId="COMMON.SESSION.LOGGED_OUT"
        visible={true}
        mask={false}
        closable={false}
        dialogClassName={styles.wrapper}
        header={
          <FormattedMessage
            className={themedStyles.header}
            id="COMMON.SESSION.LOGGED_OUT"
          />
        }
      >
        <Row>
          <Col xs={16} sm={16}>
            <FormattedMessage
              className={themedStyles.text}
              id="COMMON.SESSION.ALREADY_LOG_OUT"
            />
          </Col>
          <Col xs={8} sm={8}>
            <div className={styles.icon} />
          </Col>
        </Row>
        <a href={SdkConfigProps.loginUrl}>
          <FormattedMessage as={Button} id="COMMON.SESSION.TAKE_ME_LOGIN" />
        </a>
      </Modal>
    </div>
  );
};
