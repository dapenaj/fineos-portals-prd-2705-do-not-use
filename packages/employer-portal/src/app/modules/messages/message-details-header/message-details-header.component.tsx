/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  createThemedStyles,
  textStyleToCss,
  pxToRem,
  FormattedDateTime,
  FormattedMessage,
  Link,
  usePermissions,
  ViewNotificationPermissions
} from 'fineos-common';
import classNames from 'classnames';
import { CaseMessage } from 'fineos-js-api-client';

import { MessageIcon } from '../message-icon';

import styles from './message-details-header.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  headerTitleWrapper: {
    ...textStyleToCss(theme.typography.panelTitle),
    color: theme.colorSchema.neutral8
  },
  headerMessageSubject: {
    maxHeight: pxToRem(theme.typography.panelTitle.lineHeight! * 1)
  },
  headerMessageContactDateTime: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  headerTitleIcon: {
    color: theme.colorSchema.neutral5
  },
  headerRelatesTo: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  }
}));

type MessageDetailsHeaderProps = {
  currentMessage: CaseMessage | undefined;
};

export const MessageDetailsHeader: React.FC<MessageDetailsHeaderProps> = ({
  currentMessage
}) => {
  const hasNotificationPermission = usePermissions(
    ViewNotificationPermissions.VIEW_NOTIFICATIONS
  );

  const themedStyles = useThemedStyles();
  return (
    <div className={styles.header}>
      {currentMessage && (
        <>
          <div className={styles.headerDetailsWrapper}>
            <div
              className={classNames(
                styles.headerTitleWrapper,
                themedStyles.headerTitleWrapper
              )}
            >
              <MessageIcon
                message={currentMessage}
                className={classNames(
                  styles.headerTitleIcon,
                  themedStyles.headerTitleIcon
                )}
                data-test-el="message-sender-icon"
              />
              <div
                title={currentMessage.subject}
                className={classNames(
                  styles.headerMessageSubject,
                  themedStyles.headerMessageSubject
                )}
                data-test-el="message-subject"
              >
                {currentMessage.subject}
              </div>
            </div>
            <div
              className={classNames(
                styles.headerRelatesTo,
                themedStyles.headerRelatesTo
              )}
              data-test-el="message-relates-to-link"
            >
              <FormattedMessage id="MESSAGES.RELATES_TO" />
              {hasNotificationPermission ? (
                <Link to={`/notification/${currentMessage.rootCase.id}`}>
                  {currentMessage.rootCase.caseReference}
                </Link>
              ) : (
                currentMessage.rootCase.caseReference
              )}
            </div>
          </div>
          <FormattedDateTime
            className={classNames(
              styles.headerMessageContactDateTime,
              themedStyles.headerMessageContactDateTime
            )}
            date={currentMessage.contactDateTime}
            data-test-el="message-date"
          />
        </>
      )}
    </div>
  );
};
