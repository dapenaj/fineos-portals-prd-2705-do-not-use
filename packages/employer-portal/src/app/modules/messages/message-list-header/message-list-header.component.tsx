/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import {
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Select,
  FormGroup
} from 'fineos-common';

import { SortByDateSelect, SortDirection } from '../../../shared';
import { Filter } from '../messages.utils';

import styles from './message-list-header.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  selectLabel: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  select: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8,
    // @TODO delete styles below after `accessible mode` will be implemented
    '& .ant-select-arrow': {
      color: theme.colorSchema.neutral8
    },
    '& .ant-select-item-option:not(.ant-select-item-option-selected):not(.ant-select-item-option-active)': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8,
      backgroundColor: theme.colorSchema.neutral1
    },
    '& .ant-select-item-option-selected': {
      ...textStyleToCss(theme.typography.baseTextSemiBold),
      color: theme.colorSchema.neutral9
    },
    '&.ant-select:not(.ant-select-customize-input) .ant-select-selector': {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8,
      backgroundColor: theme.colorSchema.neutral1
    }
  }
}));

type MessageListHeaderProps = {
  filter: Filter;
  sort: SortDirection;
  onFilter: (value: Filter) => void;
  onSort: (value: SortDirection) => void;
};

export const MessageListHeader: React.FC<MessageListHeaderProps> = ({
  filter,
  sort,
  onFilter,
  onSort
}) => {
  const intl = useIntl();
  const themedStyles = useThemedStyles();
  return (
    <div className={styles.messageListHeader}>
      <FormGroup
        className={styles.messageListFilter}
        labelClassName={classNames(
          themedStyles.selectLabel,
          styles.selectLabel
        )}
        label={<FormattedMessage id="MESSAGES.FILTER.LABEL" />}
        noMargin={true}
      >
        <Select
          value={intl.formatMessage({
            id: `MESSAGES.FILTER.OPTIONS.${filter}`
          })}
          defaultValue={intl.formatMessage({
            id: `MESSAGES.FILTER.OPTIONS.ALL`
          })}
          data-test-el="message-filter-select"
          className={themedStyles.select}
          dropdownClassName={styles.selectDropdown}
          onChange={(value: unknown) => onFilter(value as Filter)}
          name="filter"
          idResolver={(value: string) =>
            intl.formatMessage({ id: `MESSAGES.FILTER.OPTIONS.${value}` })
          }
          optionElements={[
            {
              value: Filter.ALL,
              title: (
                <FormattedMessage key={0} id="MESSAGES.FILTER.OPTIONS.ALL" />
              )
            },
            {
              value: Filter.UNREAD,
              title: (
                <FormattedMessage key={1} id="MESSAGES.FILTER.OPTIONS.UNREAD" />
              )
            },
            {
              value: Filter.INCOMING,
              title: (
                <FormattedMessage
                  key={2}
                  id="MESSAGES.FILTER.OPTIONS.INCOMING"
                />
              )
            },
            {
              value: Filter.OUTGOING,
              title: (
                <FormattedMessage
                  key={3}
                  id="MESSAGES.FILTER.OPTIONS.OUTGOING"
                />
              )
            }
          ]}
        />
      </FormGroup>
      <FormGroup
        className={styles.messageListSorting}
        labelClassName={classNames(
          themedStyles.selectLabel,
          styles.selectLabel
        )}
        label={<FormattedMessage id="MESSAGES.SORT.LABEL" />}
        noMargin={true}
      >
        <SortByDateSelect
          value={intl.formatMessage({
            id: `COMMON.SORT_BY_DATE_SELECT.${sort}`
          })}
          data-test-el="message-sort-select"
          className={themedStyles.select}
          dropdownClassName={styles.selectDropdown}
          onChange={(value: unknown) => onSort(value as SortDirection)}
          idResolver={value =>
            intl.formatMessage({ id: `COMMON.SORT_BY_DATE_SELECT.${value}` })
          }
        />
      </FormGroup>
    </div>
  );
};
