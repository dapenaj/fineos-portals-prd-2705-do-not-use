/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { SortDirection } from '../../shared';

export enum Filter {
  ALL = 'ALL',
  UNREAD = 'UNREAD',
  INCOMING = 'INCOMING',
  OUTGOING = 'OUTGOING'
}

export type Options = {
  filter: Filter;
  sort: SortDirection;
};

export type Mode = {
  isFiltering: boolean;
  isAdding: boolean;
};

export type MessageListRef = {
  container: HTMLDivElement;
  returnFocus(): void;
  focusNext(): void;
};

export function scrollIntoMessageView(
  container: Element,
  element: HTMLDataListElement,
  isDown: boolean
) {
  // this value should be in sync with $item-full-height message-list.module.scss
  const ITEM_FULL_HEIGHT = 186;
  if (!container || !element) {
    return false;
  }
  // Get container properties
  const containerTop = container.scrollTop;
  const containerBottom = containerTop + container.clientHeight;

  // Get element properties
  const elementTop = element.offsetTop;
  const elementBottom = elementTop + element.clientHeight;

  // Check if in viewport
  const isTotallyVisible =
    elementTop >= containerTop && elementBottom <= containerBottom;

  if (!isTotallyVisible) {
    // this is the part of the message that is not visible in the viewport
    // and we need to scroll by that offset, to make it fully visible
    // either top or bottom
    const offsetBottom = ITEM_FULL_HEIGHT + elementTop - containerBottom;
    const offsetTop = ITEM_FULL_HEIGHT + containerTop - elementBottom;

    container.scrollTop += isDown ? offsetBottom : -offsetTop;
  }
}
