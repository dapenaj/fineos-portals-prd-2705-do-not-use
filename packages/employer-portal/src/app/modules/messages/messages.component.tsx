/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, {
  useState,
  useRef,
  useEffect,
  useCallback,
  useLayoutEffect
} from 'react';
import classNames from 'classnames';
import {
  EmptyState,
  ContentRenderer,
  Panel,
  getAllFocusableElements,
  FocusTrap,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  OverlaySpinner,
  OverlayPopconfirm,
  ConfigurableText
} from 'fineos-common';
import { CaseMessage } from 'fineos-js-api-client';
import { Col, Row } from 'antd';

import {
  PageLayout,
  MessageDetails,
  editMessage,
  useMessageCounter,
  useMessageDetailsFormik,
  FormikWithResponseHandling,
  SortDirection
} from '../../shared';
import { INPUT_MIN_LENGTH } from '../notification/alerts/alerts-messages-tab/new-message-form';

import styles from './messages.module.scss';
import { Mode, Filter, MessageListRef } from './messages.utils';
import { useMessage } from './use-message.hook';
import { MessageListHeader } from './message-list-header';
import { MessageDetailsHeader } from './message-details-header';
import { MessageList } from './message-list';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.pageTitle)
  }
}));

const resizableOptions = {
  maxHeight: 320,
  minHeight: 180,
  defaultSize: { height: 180, width: '100%' }
};

export const Messages = () => {
  const themedStyles = useThemedStyles();
  const [currentMessage, setCurrentMessage] = useState<
    CaseMessage | undefined
  >();
  const focusTrapStartRef = useRef<HTMLDivElement>(null);
  const focusTrapEndRef = useRef<HTMLDivElement>(null);
  const messageDetailsRef = useRef<HTMLDivElement>(null);
  const index = useRef<number>(0);

  const mode = useRef<Mode>({
    isFiltering: false,
    isAdding: false
  });
  const [sort, setSort] = useState<SortDirection>(SortDirection.DESC);
  const [filter, setFilter] = useState<Filter>(Filter.ALL);
  const [initialLoad, setInitialLoad] = useState(true);
  // contains info about current and next focus
  const messageListRef = useRef<MessageListRef | null>(null);
  const {
    messages,
    totalMessages,
    isLoadingMessages,
    fetchMessagesError,
    hasMoreElements,
    updateMessage,
    addMessage,
    loadMore,
    isStale
  } = useMessage(filter, sort);
  const { syncMessages } = useMessageCounter();

  // is set to visible when user is changing current message and message in response input has correct length
  const [popconfirm, setPopconfirm] = useState<{
    isVisible: boolean;
    callback?: () => void;
  }>({ isVisible: false });

  const messageSelectedCbRef: React.MutableRefObject<
    null | (() => void)
  > = useRef(null);

  useLayoutEffect(() => {
    if (messageSelectedCbRef.current) {
      messageSelectedCbRef.current!();
    }
  }, [currentMessage]);

  const onMessageClick = useCallback(
    (message: CaseMessage): Promise<boolean> => {
      // if is current massage doesn't change anything
      if (message.id === currentMessage?.id) {
        return Promise.resolve(false);
      }

      // if input is not empty show modal and stay on current message
      if (isFilled.current) {
        setPopconfirm({
          isVisible: true,
          callback: () => onMessageClick(message)
        });
        return Promise.resolve(false);
      }
      let newCurrentMessage: CaseMessage = message;
      // if message is unread set it as read
      if (!message.readByGroupClient) {
        newCurrentMessage = { ...message, readByGroupClient: true };
        editMessage(newCurrentMessage);
        updateMessage(newCurrentMessage);
      }
      setCurrentMessage(newCurrentMessage);
      return new Promise<boolean>(f => {
        messageSelectedCbRef.current = () => {
          messageSelectedCbRef.current = null;
          f(true);
        };
      });
    },
    [updateMessage, currentMessage]
  );

  useEffect(() => {
    setInitialLoad(false);
    // update counter
    if (!isStale) {
      syncMessages(messages);
    }
    if (mode.current.isFiltering) {
      if (messages.length) {
        onMessageClick(messages[0]);
      } else {
        setCurrentMessage(undefined);
      }
      mode.current.isFiltering = false;
    }
    // eslint-disable-next-line
  }, [messages]);

  useEffect(() => {
    index.current = 0;
    if (messageListRef.current) {
      messageListRef.current.container.scrollTop = 0;
    }
  }, [filter, sort]);

  useEffect(() => {
    mode.current.isFiltering = true;
  }, [filter, sort]);

  // is set to true if message in response input has correct length but is not sent
  const isFilled = useRef(false);
  // tslint:disable-next-line:no-empty
  const resetFormRef = useRef(() => {});
  const formikProps = useMessageDetailsFormik({
    currentMessage,
    mode,
    onMessageAdd: addMessage,
    // tslint:disable-next-line:no-empty
    onClose: () => {},
    onFilterSet: setFilter,
    filter,
    resetFormRef
  });
  const isLastMessageSelected =
    Boolean(currentMessage) && currentMessage === messages[messages.length - 1];

  return (
    <>
      <PageLayout
        contentHeader={
          <FormattedMessage
            data-test-el="messages-page-header"
            id="MESSAGES.HEADER"
            as="h1"
            className={classNames(styles.header, themedStyles.header)}
          />
        }
      >
        <ContentRenderer
          isEmpty={!totalMessages && !isLoadingMessages && initialLoad}
          isLoading={isLoadingMessages && initialLoad}
          error={fetchMessagesError}
          shouldNotDestroyOnLoading={true}
          emptyContent={
            <EmptyState withBorder={true}>
              <FormattedMessage id="MESSAGES.EMPTY" />
            </EmptyState>
          }
        >
          <OverlaySpinner isLoading={isLoadingMessages}>
            <Row gutter={1}>
              <Col sm={10} md={8}>
                <Panel
                  data-test-el="messages"
                  className={styles.panel}
                  header={
                    <MessageListHeader
                      filter={filter}
                      sort={sort}
                      onFilter={(newFilter: Filter) => {
                        if (isFilled.current) {
                          setPopconfirm({
                            isVisible: true,
                            callback: () => setFilter(newFilter)
                          });
                        } else {
                          setFilter(newFilter);
                        }
                      }}
                      onSort={(newSort: SortDirection) => {
                        if (isFilled.current) {
                          setPopconfirm({
                            isVisible: true,
                            callback: () => setSort(newSort)
                          });
                        } else {
                          setSort(newSort);
                        }
                      }}
                    />
                  }
                >
                  {messages.length ? (
                    <MessageList
                      ref={messageListRef}
                      className={styles.messageList}
                      messages={messages}
                      currentMessage={currentMessage}
                      hasMoreElements={hasMoreElements}
                      index={index}
                      onMessageClick={onMessageClick}
                      onMessageUpdate={updateMessage}
                      onMoveFocusPrev={() => {
                        const focusableElements = getAllFocusableElements(
                          messageDetailsRef.current!
                        );
                        focusableElements[focusableElements.length - 1].focus();
                      }}
                      onMoveFocusNext={() => {
                        const [firstFocusableEl] = getAllFocusableElements(
                          messageDetailsRef.current!
                        );
                        firstFocusableEl.focus();
                      }}
                      onLoadMore={() => {
                        index.current += 1;
                        return loadMore(index.current);
                      }}
                      mode={mode}
                      options={{ filter, sort }}
                    />
                  ) : (
                    !isLoadingMessages && (
                      <EmptyState isSmall={true}>
                        <FormattedMessage
                          id="MESSAGES.EMPTY"
                          className={styles.messageListEmpty}
                        />
                      </EmptyState>
                    )
                  )}
                </Panel>
              </Col>

              {messages.length > 0 && (
                <FocusTrap
                  ref={focusTrapStartRef}
                  onFocus={(e: React.FocusEvent) => {
                    messageListRef.current!.returnFocus();
                  }}
                />
              )}

              <Col sm={14} md={16} ref={messageDetailsRef}>
                <Panel
                  data-test-el="message-details"
                  className={styles.panel}
                  header={
                    <MessageDetailsHeader currentMessage={currentMessage} />
                  }
                >
                  <div className={styles.messageDetails}>
                    <FormikWithResponseHandling {...formikProps}>
                      {({ values, resetForm }) => {
                        resetFormRef.current = resetForm;
                        isFilled.current =
                          values.message.length >= INPUT_MIN_LENGTH;
                        return (
                          <MessageDetails
                            currentMessage={currentMessage}
                            resizableOptions={resizableOptions}
                            filter={filter}
                            sort={sort}
                          />
                        );
                      }}
                    </FormikWithResponseHandling>
                  </div>
                </Panel>
              </Col>

              {!isLastMessageSelected && messages.length > 0 && (
                <FocusTrap
                  ref={focusTrapEndRef}
                  onFocus={(e: React.FocusEvent) => {
                    messageListRef.current!.focusNext();
                  }}
                />
              )}
            </Row>
          </OverlaySpinner>
        </ContentRenderer>
      </PageLayout>
      <OverlayPopconfirm
        ariaLabelId="USER_ACTIONS.LEAVE_PAGE"
        onCancel={() => {
          setPopconfirm({ isVisible: false });
        }}
        onConfirm={() => {
          setPopconfirm({ isVisible: false });
          isFilled.current = false;
          popconfirm.callback && popconfirm.callback();
        }}
        visible={popconfirm!.isVisible}
        popconfirmMessage={<ConfigurableText id="USER_ACTIONS.LEAVE_PAGE" />}
      />
    </>
  );
};
