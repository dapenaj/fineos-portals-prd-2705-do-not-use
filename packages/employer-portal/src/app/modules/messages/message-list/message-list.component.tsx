/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect, useImperativeHandle, useRef } from 'react';
import {
  List,
  createThemedStyles,
  textStyleToCss,
  pxToRem,
  getAllFocusableElements,
  FocusTrap,
  FormattedDate,
  FormattedMessage,
  Link,
  usePermissions,
  ViewNotificationPermissions,
  ViewCustomerDataPermissions,
  primaryLight2Color,
  InfiniteScroll,
  Spinner,
  useErrorNotificationPopup,
  ScreenReaderMessage,
  useElementId,
  getFocusedElementShadow
} from 'fineos-common';
import classNames from 'classnames';
import { CaseMessage } from 'fineos-js-api-client';

import { MessageOptions, SortDirection } from '../../../shared';
import {
  Mode,
  Options,
  Filter,
  scrollIntoMessageView,
  MessageListRef
} from '../messages.utils';
import { MessageIcon } from '../message-icon';

import styles from './message-list.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  messageItemMeta: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  messageReadIcon: {
    color: theme.colorSchema.neutral5
  },
  messageUnreadIcon: {
    color: theme.colorSchema.primaryColor
  },
  messageUnreadItem: {
    borderLeftColor: theme.colorSchema.primaryColor
  },
  messageSubject: {
    ...textStyleToCss(theme.typography.baseTextSemiBold),
    color: theme.colorSchema.primaryAction,
    maxHeight: pxToRem(theme.typography.baseTextSemiBold.lineHeight! * 1)
  },
  messageContactDateTime: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  messageNarrative: {
    ...textStyleToCss(theme.typography.smallTextSemiBold),
    color: theme.colorSchema.neutral8,
    maxHeight: pxToRem(theme.typography.smallTextSemiBold.lineHeight! * 2)
  },
  messageLink: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  messageActive: {
    backgroundColor: primaryLight2Color(theme.colorSchema)
  },
  containerFocused: {
    borderColor: theme.colorSchema.primaryAction,
    boxShadow: getFocusedElementShadow({
      color: theme.colorSchema.primaryAction
    })
  }
}));

type MessageListProps = {
  className?: string;
  messages: CaseMessage[];
  currentMessage: CaseMessage | undefined;
  isItemBorderVisible?: boolean;
  hasMoreElements: boolean;
  index: React.MutableRefObject<number>;
  mode: React.MutableRefObject<Mode>;
  onMessageUpdate: (message: CaseMessage) => void;
  onMessageClick: (message: CaseMessage) => Promise<boolean>;
  onMoveFocusPrev: () => void;
  onMoveFocusNext: () => void;
  onLoadMore: () => Promise<void>;
  options: Options;
};

const SCREEN_READER_MESSAGE_LENGTH = 100;

type handledKeyCodes = 'ArrowDown' | 'ArrowUp' | 'Home' | 'End';

export const MessageList = React.forwardRef(
  (
    {
      messages,
      currentMessage,
      className,
      isItemBorderVisible = true,
      hasMoreElements,
      index,
      mode,
      onMessageUpdate,
      onMessageClick,
      onMoveFocusPrev,
      onMoveFocusNext,
      onLoadMore,
      options
    }: MessageListProps,
    ref: Parameters<
      React.ForwardRefRenderFunction<MessageListRef, MessageListProps>
    >[1]
  ) => {
    const themedStyles = useThemedStyles();
    const containerRef = useRef<HTMLDivElement>(null);
    const itemIdPrefix = useElementId();
    const internalContainerRef = useRef<HTMLUListElement>(null);
    const [isListFocused, setIsListFocused] = useState(false);

    const [isMessageAnimated, setIsMessageAnimated] = useState(false);
    const [isSpinnerVisible, setIsSpinnerVisible] = useState(false);
    const addedElementToMessagesList = useRef(mode.current.isAdding);
    const { showErrorNotification } = useErrorNotificationPopup();
    const hasNotificationPermission = usePermissions(
      ViewNotificationPermissions.VIEW_NOTIFICATIONS
    );
    const hasEmployeeDetailsPermissions = usePermissions(
      ViewCustomerDataPermissions.VIEW_CUSTOMER
    );

    useEffect(() => {
      const { filter, sort } = options;
      if (mode.current.isAdding) {
        index.current = 0;
        addedElementToMessagesList.current = true;
        if (containerRef.current) {
          containerRef.current.scrollTop = 0;
        }
        onMessageClick(messages[0]);
        if (filter !== Filter.ALL || sort === SortDirection.ASC) {
          mode.current.isAdding = false;
          return;
        }
        setIsMessageAnimated(true);
      }
    }, [messages, onMessageClick, index, mode, options, containerRef]);

    const currentMessageIndex = currentMessage
      ? messages.indexOf(currentMessage)
      : -1;
    const dataTestElForMessageList = '[data-test-el="message-list-item"]';

    useImperativeHandle(
      ref,
      (): MessageListRef => ({
        container: containerRef.current!,

        returnFocus() {
          if (currentMessageIndex > -1) {
            const currentMessageContainerEl = containerRef.current!.querySelectorAll(
              dataTestElForMessageList
            )[currentMessageIndex];
            const allFocusableElements = getAllFocusableElements(
              currentMessageContainerEl
            );
            allFocusableElements[allFocusableElements.length - 1].focus();
          }
        },

        focusNext() {
          if (
            currentMessageIndex > -1 &&
            currentMessageIndex < messages.length - 1
          ) {
            let nextIndex = currentMessageIndex + 1;
            // if element is added we want to go inside this element
            if (addedElementToMessagesList.current) {
              nextIndex = 0;
              addedElementToMessagesList.current = false;
            }
            internalContainerRef.current!.focus();
            onMessageClick(messages[nextIndex]);
          }
        }
      }),
      [currentMessageIndex, containerRef, onMessageClick, messages]
    );

    const handleOnBlur = (e: React.FocusEvent) => {
      setIsListFocused(false);
      if (
        e.currentTarget === e.target &&
        e.relatedTarget &&
        e.currentTarget.contains(e.relatedTarget as Node)
      ) {
        e.preventDefault();
        const messageEls = containerRef.current!.querySelectorAll(
          dataTestElForMessageList
        );
        const currentMessageContainerEl = messageEls[currentMessageIndex];
        if (currentMessageContainerEl) {
          const [firstEl] = getAllFocusableElements(currentMessageContainerEl);
          firstEl.focus();
        }
      }
    };

    const handleOnKeyDown = (e: React.KeyboardEvent) => {
      if (currentMessage) {
        let nextItemIndex = -1;
        const list = containerRef.current;
        const messageEls = list!.querySelectorAll(dataTestElForMessageList);
        const key: handledKeyCodes = e.key as handledKeyCodes;
        switch (key) {
          case 'ArrowDown':
            e.preventDefault(); // prevent scrolling by browser, we handle that manually
            if (currentMessageIndex < messages.length - 1) {
              nextItemIndex = currentMessageIndex + 1;
              scrollIntoMessageView(
                list!,
                messageEls[nextItemIndex] as HTMLDataListElement,
                true
              );
            }
            break;
          case 'ArrowUp':
            e.preventDefault(); // prevent scrolling by browser, we handle that manually
            if (currentMessageIndex > 0) {
              nextItemIndex = currentMessageIndex - 1;
              scrollIntoMessageView(
                list!,
                messageEls[nextItemIndex] as HTMLDataListElement,
                false
              );
            }
            break;
          case 'Home':
            nextItemIndex = 0;
            break;
          case 'End':
            nextItemIndex = messages.length - 1;
            break;
        }

        if (nextItemIndex > -1) {
          onMessageClick(messages[nextItemIndex]);
          (e.currentTarget as HTMLElement).focus();
        }
      }
    };

    const renderMessageDescription = (message: CaseMessage) => {
      const {
        readByGroupClient,
        id,
        subject,
        contactDateTime,
        narrative,
        rootCase,
        customer
      } = message;
      return (
        <>
          <ScreenReaderMessage
            id="NOTIFICATIONS.ALERTS.MESSAGES.UNREAD_MESSAGE"
            hidden={readByGroupClient}
          />
          <ScreenReaderMessage
            id="NOTIFICATIONS.ALERTS.MESSAGES.OPEN_MESSAGE"
            hidden={id !== currentMessage?.id}
          />
          <div
            title={subject}
            className={classNames(
              styles.messageSubject,
              themedStyles.messageSubject
            )}
          >
            <strong data-test-el="message-subject">{subject}</strong>
          </div>

          <div
            className={classNames(
              styles.messageContactDateTime,
              themedStyles.messageContactDateTime
            )}
          >
            <FormattedDate data-test-el="message-date" date={contactDateTime} />
          </div>
          <div
            className={classNames(
              styles.messageNarrative,
              themedStyles.messageNarrative
            )}
            data-test-el="message-narrative"
          >
            {/* Message narrative below should be displayed only as preview
                              of full message. CSS is used to obain this result.
                              To allow the screen reader to read it correctly, we need
                              to hide original text from it and force it to read
                              programatically determined content. */}
            <span aria-hidden={true}>{narrative}</span>
            <ScreenReaderMessage
              id="NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_PREVIEW"
              values={{
                previewContent: narrative.substr(
                  0,
                  SCREEN_READER_MESSAGE_LENGTH
                )
              }}
            />
          </div>
          <>
            <p
              className={classNames(
                styles.messageRelatesTo,
                themedStyles.messageLink
              )}
              data-test-el="message-relates-to-link"
            >
              <FormattedMessage id="MESSAGES.RELATES_TO" />
              {hasNotificationPermission ? (
                <Link
                  onClick={e => e.stopPropagation()}
                  to={`/notification/${rootCase.id}`}
                >
                  {rootCase.caseReference}
                </Link>
              ) : (
                rootCase.caseReference
              )}
            </p>
            <p
              className={classNames(
                styles.messageEmployee,
                themedStyles.messageLink
              )}
              data-test-el="message-employee-link"
            >
              <FormattedMessage id="MESSAGES.EMPLOYEE" />
              {hasEmployeeDetailsPermissions ? (
                <Link
                  onClick={e => e.stopPropagation()}
                  to={`/profile/${customer.id}`}
                >
                  {`${customer.firstName} ${customer.lastName}`}
                </Link>
              ) : (
                `${customer.firstName} ${customer.lastName}`
              )}
            </p>
          </>
        </>
      );
    };

    const renderMessagesList = () => (
      <List data-test-el="message-list">
        {messages ? (
          <List.InternalContainer
            ref={internalContainerRef}
            aria-live="polite"
            role="listbox"
            tabIndex={0}
            aria-activedescendant={
              currentMessage
                ? `${itemIdPrefix}-${currentMessage.id}`
                : undefined
            }
            onFocus={(e: React.FocusEvent) => {
              if (e.currentTarget === e.target) {
                setIsListFocused(true);
              }
            }}
            onBlur={handleOnBlur}
            onKeyDown={handleOnKeyDown}
          >
            {messages.map((message: CaseMessage, messageIndex: number) => {
              const isMessageUnread =
                !message.readByGroupClient && isItemBorderVisible;
              const isMessageActive = message.id === currentMessage?.id;
              return (
                <React.Fragment key={message.id}>
                  {message === currentMessage && messageIndex > 0 && (
                    <FocusTrap
                      onFocus={() => {
                        onMessageClick(messages[messageIndex - 1]).then(
                          isSelectChanged => {
                            if (isSelectChanged) {
                              onMoveFocusPrev();
                            }
                          }
                        );
                      }}
                    />
                  )}

                  <List.Item
                    className={classNames(styles.messageItem, {
                      [themedStyles.messageUnreadItem]: isMessageUnread,
                      [themedStyles.messageActive]: isMessageActive,
                      [styles.animation]: isMessageAnimated
                    })}
                    role="option"
                    id={`${itemIdPrefix}-${message.id}`}
                    aria-selected={message === currentMessage}
                    data-test-el="message-list-item"
                    onClick={() => {
                      onMessageClick(message);
                      internalContainerRef.current!.focus();
                    }}
                    onAnimationEnd={() => {
                      setIsMessageAnimated(false);
                      mode.current.isAdding = false;
                    }}
                  >
                    <List.Item.Meta
                      className={themedStyles.messageItemMeta}
                      avatar={
                        <MessageIcon
                          message={message}
                          className={classNames(
                            styles.messageIcon,
                            message.readByGroupClient
                              ? themedStyles.messageReadIcon
                              : themedStyles.messageUnreadIcon
                          )}
                          data-test-el="message-sender-icon"
                        />
                      }
                      description={renderMessageDescription(message)}
                    />
                    <MessageOptions
                      message={message}
                      onMessageEdit={onMessageUpdate}
                    />
                  </List.Item>

                  {message === currentMessage && (
                    <FocusTrap onFocus={onMoveFocusNext} />
                  )}
                </React.Fragment>
              );
            })}
          </List.InternalContainer>
        ) : null}
      </List>
    );

    return (
      <div
        className={classNames(className, {
          [themedStyles.containerFocused]: isListFocused
        })}
        ref={containerRef}
      >
        <InfiniteScroll
          key="infinite-scroll"
          initialLoad={false}
          pageStart={0}
          loadMore={async () => {
            if (isSpinnerVisible) {
              return;
            }
            try {
              setIsSpinnerVisible(true);
              if (hasMoreElements) {
                await onLoadMore();
              }
            } catch (e) {
              showErrorNotification(e);
              index.current -= 1;
            } finally {
              setIsSpinnerVisible(false);
            }
          }}
          loader={
            <Spinner
              key={0}
              className={classNames(styles.spinner, {
                [styles.spinnerHidden]: !isSpinnerVisible
              })}
            />
          }
          hasMore={hasMoreElements}
          useWindow={false}
        >
          {renderMessagesList()}
        </InfiniteScroll>
      </div>
    );
  }
);
