/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  CollapsibleCard,
  ContentRenderer,
  ViewPaymentsPermissions,
  withPermissions,
  useAsync,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { isEmpty as _isEmpty, orderBy as _orderBy } from 'lodash';
import moment from 'moment';

import { PaymentsHistory } from './payments-history';
import { Payment } from './payment';
import { fetchPayments } from './latest-payments.api';
import styles from './latest-payments.module.scss';

type LatestPaymentsProps = {
  benefitId: string;
  claimId: string;
  benefitCaseType: string;
};

const PAYMENTS_DISPLAY_COUNT = 4;

export const useThemedStyles = createThemedStyles(theme => ({
  text: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const LatestPaymentsComponent: React.FC<LatestPaymentsProps> = ({
  claimId,
  benefitId,
  benefitCaseType
}) => {
  const {
    state: { value: payments, isPending }
  } = useAsync(fetchPayments, [claimId], { defaultValue: [] });
  const paymentsPerBenefit = payments.filter(
    payment => payment.benefitCaseNumber === benefitId
  );
  const orderedPayments = _orderBy(
    paymentsPerBenefit,
    'paymentDate',
    'desc'
  ).filter(payment =>
    payment.paymentDate.isAfter(moment().subtract(1, 'year'))
  );
  const slicedPayments = orderedPayments.slice(0, PAYMENTS_DISPLAY_COUNT);
  const themedStyles = useThemedStyles();
  return (
    <ContentRenderer isLoading={isPending} isEmpty={_isEmpty(slicedPayments)}>
      <CollapsibleCard
        className={styles.wrapper}
        data-test-el="latest-payments"
        dropShadow={false}
        title={
          <div
            className={styles.headerWrapper}
            data-test-el="latest-payments-header"
          >
            <FormattedMessage
              className={themedStyles.text}
              id="PAYMENTS.LATEST_PAYMENTS"
            />
            {orderedPayments &&
              orderedPayments.length > PAYMENTS_DISPLAY_COUNT && (
                <PaymentsHistory
                  benefitCaseType={benefitCaseType}
                  claimId={claimId}
                  payments={orderedPayments}
                />
              )}
          </div>
        }
      >
        <div className={styles.payments}>
          {slicedPayments!.map((payment, index) => {
            return (
              <Payment
                key={`${payment.paymentId}-${index}`}
                claimId={claimId}
                payment={payment}
              />
            );
          })}
        </div>
      </CollapsibleCard>
    </ContentRenderer>
  );
};

export const LatestPayments = withPermissions(
  ViewPaymentsPermissions.VIEW_CLAIMS_PAYMENTS_LIST
)(LatestPaymentsComponent);
