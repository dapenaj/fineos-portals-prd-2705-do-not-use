/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import classNames from 'classnames';
import {
  createThemedStyles,
  textStyleToCss,
  Modal,
  Button,
  ButtonType,
  FormattedMessage,
  ModalWidth,
  ModalFooter,
  UiButton
} from 'fineos-common';
import { GroupClientPayment } from 'fineos-js-api-client';

import { FilterBody } from './filter-body';
import styles from './payments-history.module.scss';

type PaymentsHistoryProps = {
  claimId: string;
  payments: GroupClientPayment[];
  benefitCaseType: string;
};

const useThemedStyles = createThemedStyles(theme => {
  return {
    header: {
      ...textStyleToCss(theme.typography.panelTitle),
      color: theme.colorSchema.neutral8
    },
    base: {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.primaryAction
    }
  };
});

export const PaymentsHistory: React.FC<PaymentsHistoryProps> = ({
  payments,
  claimId,
  benefitCaseType
}) => {
  const themedStyles = useThemedStyles();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const handleClose = () => setIsModalVisible(false);
  return (
    <>
      <FormattedMessage
        as={Button}
        buttonType={ButtonType.LINK}
        id="PAYMENTS.PAYMENTS_VIEW_HISTORY_LINK"
        className={classNames(themedStyles.base, styles.viewHistoryLink)}
        onClick={(e: React.MouseEvent) => {
          e.stopPropagation();
          setIsModalVisible(true);
        }}
      />
      <Modal
        ariaLabelId="PAYMENTS.PAYMENTS_VIEW_HISTORY"
        visible={isModalVisible}
        onCancel={handleClose}
        width={ModalWidth.WIDE}
        header={
          <div className={themedStyles.header}>
            <FormattedMessage id="PAYMENTS.PAYMENTS_VIEW_HISTORY" />
          </div>
        }
      >
        <>
          <FilterBody
            benefitCaseType={benefitCaseType}
            claimId={claimId}
            payments={payments}
          />
          <ModalFooter>
            <FormattedMessage
              as={UiButton}
              onClick={handleClose}
              id="FINEOS_COMMON.GENERAL.CLOSE"
            />
          </ModalFooter>
        </>
      </Modal>
    </>
  );
};
