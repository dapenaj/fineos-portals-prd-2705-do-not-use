/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useContext } from 'react';
import {
  createThemedStyles,
  textStyleToCss,
  ContentRenderer,
  Button,
  ButtonType,
  EmptyState,
  RichFormattedMessage,
  FormattedMessage,
  UiButton
} from 'fineos-common';
import { GroupClientPayment } from 'fineos-js-api-client';
import moment from 'moment';
import classNames from 'classnames';

import { Payment as PaymentComponent } from '../../payment';
import { DateRangeDrawer } from '../date-range-drawer';
import { Config } from '../../../../../config';

import styles from './filter-body.module.scss';

type FilterBodyProps = {
  payments: GroupClientPayment[];
  claimId: string;
  benefitCaseType: string;
};

const useThemedStyles = createThemedStyles(theme => {
  return {
    header: {
      ...textStyleToCss(theme.typography.panelTitle),
      color: theme.colorSchema.neutral8
    },
    base: {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.primaryAction
    },
    color: {
      color: theme.colorSchema.neutral8
    }
  };
});

export const FilterBody: React.FC<FilterBodyProps> = ({
  payments,
  claimId,
  benefitCaseType
}) => {
  const config = useContext(Config);
  const sizeList = config.configurableSizeList.PAYMENTS_HISTORY;
  const themedStyles = useThemedStyles();
  const [from, setFrom] = useState<moment.Moment | null>();
  const [until, setUntil] = useState<moment.Moment | null>();
  const [filterPayments, setFilterPayments] = useState(
    payments.slice(0, sizeList)
  );
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const onFilter = (fromDate: moment.Moment, untilDate: moment.Moment) => {
    if (fromDate.isValid() && untilDate.isValid()) {
      setFrom(fromDate);
      setUntil(untilDate);
    }
    const filteredPayments = payments
      .filter(({ paymentDate }) =>
        paymentDate.isBetween(fromDate, untilDate, 'day', '[]')
      )
      .slice(0, sizeList);

    setFilterPayments(filteredPayments);
  };

  return (
    <ContentRenderer isLoading={false} isEmpty={false}>
      <div
        className={classNames(themedStyles.header, styles.header)}
        data-test-el="benefit-case-type"
      >
        {benefitCaseType}
      </div>
      <div className={themedStyles.base}>
        {!!from && !!until ? (
          <div className={classNames(themedStyles.color, styles.bodyWrapper)}>
            <RichFormattedMessage
              id="PAYMENTS.FILTER_INFO"
              values={{
                from,
                until
              }}
            />
            &nbsp;
            <FormattedMessage
              as={Button}
              buttonType={ButtonType.LINK}
              id="PAYMENTS.RESET_FILTER"
              onClick={() => {
                setFrom(null);
                setUntil(null);
                setFilterPayments(payments.slice(0, sizeList));
              }}
            />
          </div>
        ) : (
          <div className={classNames(themedStyles.color, styles.bodyWrapper)}>
            <FormattedMessage
              id="PAYMENTS.PAYMENTS_INFO"
              values={{
                visiblePayments:
                  payments.length > sizeList ? sizeList : payments.length,
                allPayments: payments.length
              }}
            />
            <FormattedMessage
              className={styles.filterDateLink}
              as={UiButton}
              buttonType={ButtonType.LINK}
              onClick={() => setIsVisible(true)}
              id="PAYMENTS.FILTER_DATE"
            />
            <DateRangeDrawer
              isVisible={isVisible}
              onClose={() => setIsVisible(false)}
              onFilter={onFilter}
            />
          </div>
        )}
      </div>
      <div className={styles.contentWrapper} data-test-el="payments-list">
        {!!filterPayments && !!filterPayments.length ? (
          filterPayments.map((payment, index) => {
            return (
              <div
                key={`${payment.paymentId}-${index}`}
                className={styles.content}
              >
                <PaymentComponent claimId={claimId} payment={payment} />
              </div>
            );
          })
        ) : (
          <EmptyState withBorder={true}>
            <FormattedMessage id="PAYMENTS.EMPTY_STATE" role="status" />
          </EmptyState>
        )}
      </div>
    </ContentRenderer>
  );
};
