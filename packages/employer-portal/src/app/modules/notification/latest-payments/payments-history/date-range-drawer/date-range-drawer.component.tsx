/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  createThemedStyles,
  textStyleToCss,
  FormButton,
  UiButton,
  ButtonType,
  Drawer,
  DrawerWidth,
  FormattedMessage,
  FormGroup,
  AsteriskInfo,
  DatePickerInput,
  DrawerFooter
} from 'fineos-common';
import * as Yup from 'yup';
import { Field } from 'formik';
import { Moment } from 'moment';
import classNames from 'classnames';

import { FormikWithResponseHandling } from '../../../../../shared';

import styles from './date-range-drawer.module.scss';

const validationSchema = Yup.object({
  from: Yup.string()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED')
    .nullable(),
  until: Yup.string()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED')
    .nullable()
});

const initialValues = { from: '', until: '' };

const useThemedStyles = createThemedStyles(theme => {
  return {
    base: {
      ...textStyleToCss(theme.typography.baseText)
    },
    colorBase: {
      color: theme.colorSchema.neutral8
    },
    datePicker: {
      '& .ant-picker-dropdown, & .ant-picker-header-view button, & .ant-picker-cell-inner': {
        ...textStyleToCss(theme.typography.baseText, {
          suppressLineHeight: true
        })
      },
      '& .ant-picker-content th, & .ant-picker-today-btn': {
        ...textStyleToCss(theme.typography.baseText)
      }
    }
  };
});

type DateRangeDrawerProps = {
  onFilter: (fromDate: Moment, untilDate: Moment) => void;
  isVisible: boolean;
  onClose: () => void;
};

export const DateRangeDrawer: React.FC<DateRangeDrawerProps> = ({
  onFilter,
  onClose,
  isVisible
}) => {
  const themedStyles = useThemedStyles();
  const [from, setFrom] = useState<Moment | null>();
  const [until, setUntil] = useState<Moment | null>();

  const handleFocusTriggerOnCancel = () => {
    const trigger = document.querySelector(
      '[data-test-el="PAYMENTS.FILTER_DATE"]'
    ) as HTMLButtonElement;
    trigger?.focus();
  };

  const handleFocusTriggerOnSubmit = () => {
    const trigger = document.querySelector(
      '[data-test-el="PAYMENTS.RESET_FILTER"]'
    ) as HTMLButtonElement;
    trigger?.focus();
  };

  const handleClose = () => {
    setFrom(null);
    setUntil(null);
    onClose();
    handleFocusTriggerOnCancel();
  };

  return (
    <Drawer
      ariaLabelId="PAYMENTS.DATE_RANGE"
      title={<FormattedMessage id="PAYMENTS.DATE_RANGE" />}
      width={DrawerWidth.NARROW}
      onClose={handleClose}
      isVisible={isVisible}
    >
      <FormikWithResponseHandling
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={({ from: changedFrom, until: changedUntil }) => {
          onFilter(changedFrom, changedUntil);
          onClose();
          handleFocusTriggerOnSubmit();
        }}
      >
        <>
          <div className={styles.wrapper}>
            <div
              className={classNames(
                themedStyles.base,
                themedStyles.colorBase,
                styles.contentWrapper
              )}
            >
              <AsteriskInfo />
              <FormGroup
                isRequired={true}
                labelClassName={styles.label}
                label={<FormattedMessage id="PAYMENTS.FROM" />}
              >
                <Field
                  placeholder={
                    <FormattedMessage id="PAYMENTS.SELECT_START_DATE_PLACEHOLDER" />
                  }
                  data-test-el="from"
                  shouldFocusOnMount={isVisible}
                  component={DatePickerInput}
                  name="from"
                  value={from}
                  onChange={(date: Moment) => setFrom(date)}
                  disabledDate={(currentDate: Moment | null) =>
                    until?.isValid() && currentDate?.isValid()
                      ? currentDate.isBefore(until)
                      : false
                  }
                  dropdownClassName={themedStyles.datePicker}
                />
              </FormGroup>
              <FormGroup
                isRequired={true}
                labelClassName={styles.label}
                label={<FormattedMessage id="PAYMENTS.TO" />}
              >
                <Field
                  placeholder={
                    <FormattedMessage id="PAYMENTS.SELECT_END_DATE_PLACEHOLDER" />
                  }
                  data-test-el="until"
                  component={DatePickerInput}
                  name="until"
                  value={until}
                  onChange={(date: Moment) => setUntil(date)}
                  disabledDate={(currentDate: Moment | null) =>
                    from?.isValid() && currentDate?.isValid()
                      ? currentDate.isBefore(from)
                      : false
                  }
                  dropdownClassName={themedStyles.datePicker}
                />
              </FormGroup>
            </div>
          </div>
          <DrawerFooter>
            <FormattedMessage
              className={themedStyles.base}
              as={FormButton}
              id="PAYMENTS.APPLY_FILTER"
              htmlType="submit"
            />
            <FormattedMessage
              className={classNames(themedStyles.base, styles.closeButton)}
              as={UiButton}
              buttonType={ButtonType.LINK}
              onClick={handleClose}
              data-test-el="drawer-close"
              id="FINEOS_COMMON.GENERAL.CLOSE"
            />
          </DrawerFooter>
        </>
      </FormikWithResponseHandling>
    </Drawer>
  );
};
