/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { GroupClientPayment } from 'fineos-js-api-client';
import {
  PropertyItem,
  FormattedMessage,
  Icon,
  Tooltip,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import styles from './payment-method-tooltip.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  tooltipWrapper: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type TooltipProps = {
  payment: GroupClientPayment;
};

export const PaymentMethodTooltip: React.FC<TooltipProps> = ({ payment }) => {
  const themedStyles = useThemedStyles();

  const getTooltipContent = () => (
    <>
      {!!payment.nominatedPayeeName && (
        <PropertyItem
          className={styles.item}
          label={<FormattedMessage id="PAYMENTS.PAID_TO" />}
        >
          {payment.nominatedPayeeName}
        </PropertyItem>
      )}
      {payment.accountTransferInfo ? (
        <>
          <PropertyItem
            className={styles.item}
            label={<FormattedMessage id="PAYMENTS.BANK_INSTITUTION" />}
          >
            {payment.accountTransferInfo.bankInstituteName}
          </PropertyItem>
          <PropertyItem
            label={<FormattedMessage id="PAYMENTS.ACCOUNT_NUMBER" />}
          >
            {payment.accountTransferInfo.bankAccountNumber}
          </PropertyItem>
        </>
      ) : (
        <PropertyItem label={<FormattedMessage id="PAYMENTS.CHECK_NUMBER" />}>
          {payment.chequePaymentInfo!.chequeNumber}
        </PropertyItem>
      )}
    </>
  );

  return (
    <div
      className={classNames(themedStyles.tooltipWrapper, styles.tooltipWrapper)}
    >
      <Tooltip tooltipContent={getTooltipContent()} placement="bottom">
        <Icon
          className={styles.wrapper}
          icon={faInfoCircle}
          iconLabel={
            <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.INFO_ICON_LABEL" />
          }
          data-test-el="payment-info-icon"
        />
      </Tooltip>
    </div>
  );
};
