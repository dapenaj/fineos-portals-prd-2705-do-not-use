/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { GroupClientPayment } from 'fineos-js-api-client';
import {
  PropertyItem,
  ContentLayout,
  FormattedDate,
  createThemedStyles,
  ThemeModel,
  FormattedMessage
} from 'fineos-common';
import { Row, Col } from 'antd';
import classNames from 'classnames';

import { PaymentType, PAYMENT_TYPES } from '../../../../../shared';

import { PaymentMethodTooltip } from './payment-method-tooltip';
import styles from './payment-details.module.scss';

type PaymentDetailsProps = {
  payment: GroupClientPayment;
};

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  wrapper: {
    borderTopColor: theme.colorSchema.neutral3
  }
}));

export const PaymentDetails: React.FC<PaymentDetailsProps> = ({ payment }) => {
  const themedStyles = useThemedStyles();
  const isPaymentRecurring = useMemo(
    () =>
      payment.paymentType === PaymentType.RECURRING ||
      (!PAYMENT_TYPES.includes(payment.paymentType as PaymentType) &&
        payment.periodStartDate?.isValid() &&
        payment.periodEndDate?.isValid()),
    [payment]
  );

  return (
    <ContentLayout
      className={classNames(styles.wrapper, themedStyles.wrapper)}
      direction="column"
    >
      <Row>
        <Col xs={24} md={6} sm={12}>
          <PropertyItem label={<FormattedMessage id="PAYMENTS.PAID_TO" />}>
            {payment.payeeName}
          </PropertyItem>
        </Col>
        <Col xs={24} md={6} sm={12}>
          <PropertyItem
            label={<FormattedMessage id="PAYMENTS.PAYMENT_METHOD" />}
          >
            {payment.paymentMethod}
            <PaymentMethodTooltip payment={payment} />
          </PropertyItem>
        </Col>
        {isPaymentRecurring ? (
          <>
            <Col xs={24} md={6} sm={12}>
              <PropertyItem
                label={<FormattedMessage id="PAYMENTS.PERIOD_START_DATE" />}
              >
                <FormattedDate date={payment.periodStartDate!} />
              </PropertyItem>
            </Col>
            <Col xs={24} md={6} sm={12}>
              <PropertyItem
                label={<FormattedMessage id="PAYMENTS.PERIOD_END_DATE" />}
              >
                <FormattedDate date={payment.periodEndDate!} />
              </PropertyItem>
            </Col>
          </>
        ) : (
          <Col xs={24} md={6} sm={12}>
            <PropertyItem
              label={<FormattedMessage id="PAYMENTS.EFFECTIVE_DATE" />}
            >
              <FormattedDate date={payment.effectiveDate} />
            </PropertyItem>
          </Col>
        )}
      </Row>
    </ContentLayout>
  );
};
