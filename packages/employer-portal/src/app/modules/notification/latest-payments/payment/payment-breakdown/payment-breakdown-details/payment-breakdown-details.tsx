/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  Button,
  ButtonType,
  PropertyItem,
  createThemedStyles,
  textStyleToCss,
  FormattedDate,
  FormattedMessage
} from 'fineos-common';
import classNames from 'classnames';
import { PaymentLinesDetail } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import styles from './payment-breakdown-details.module.scss';

type PaymentBreakdownDetailsProps = {
  paymentLineDetail: PaymentLinesDetail;
};

const useThemedStyles = createThemedStyles(theme => ({
  details: {
    borderColor: theme.colorSchema.infoColor
  },
  property: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  }
}));

export const PaymentBreakdownDetails: React.FC<PaymentBreakdownDetailsProps> = ({
  paymentLineDetail
}) => {
  const themedStyles = useThemedStyles();
  const [showDetails, toggleDetails] = useState(false);
  return (
    <div data-test-el="payment-breakdown-details">
      <div className={classNames(styles.details, themedStyles.details)}>
        <Button
          buttonType={ButtonType.LINK}
          onClick={() => {
            toggleDetails(!showDetails);
          }}
          data-test-el="show-payment-breakdown-details"
        >
          <FormattedMessage
            id={
              showDetails ? 'PAYMENTS.DETAILS_MINUS' : 'PAYMENTS.DETAILS_PLUS'
            }
          />
        </Button>
        {showDetails && (
          <div
            data-test-el="payment-line-properties"
            className={classNames(themedStyles.property, styles.property)}
          >
            {!_isEmpty(paymentLineDetail.payee) && (
              <>
                <PropertyItem
                  className={styles.property}
                  isBlock={false}
                  label={<FormattedMessage id="PAYMENTS.PAID_TO" />}
                >
                  <span className={themedStyles.property}>
                    {paymentLineDetail.payee}
                  </span>
                </PropertyItem>
              </>
            )}
            <PropertyItem
              className={styles.property}
              isBlock={false}
              label={<FormattedMessage id="PAYMENTS.START_DATE" />}
            >
              {paymentLineDetail.startDate && (
                <FormattedDate
                  className={themedStyles.property}
                  date={paymentLineDetail.startDate}
                  defaultValue={'-'}
                />
              )}
            </PropertyItem>
            <PropertyItem
              className={styles.property}
              isBlock={false}
              label={<FormattedMessage id="PAYMENTS.END_DATE" />}
            >
              {paymentLineDetail.endDate && (
                <FormattedDate
                  className={themedStyles.property}
                  date={paymentLineDetail.endDate}
                  defaultValue={'-'}
                />
              )}
            </PropertyItem>
          </div>
        )}
      </div>
    </div>
  );
};
