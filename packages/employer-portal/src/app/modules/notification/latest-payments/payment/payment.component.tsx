/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { GroupClientPayment } from 'fineos-js-api-client';
import {
  CollapsibleCard,
  useAsync,
  usePermissions,
  ViewPaymentsPermissions,
  ContentRenderer,
  Button,
  ButtonType,
  FormattedMessage
} from 'fineos-common';
import { isEmpty as _isEmpty } from 'lodash';

import { fetchPaymentLines } from '../latest-payments.api';

import { PaymentBreakdown } from './payment-breakdown';
import { PaymentHeader } from './payment-header';
import { PaymentDetails } from './payment-details';
import styles from './payment.module.scss';

type PaymentProps = {
  claimId: string;
  payment: GroupClientPayment;
};

export const Payment: React.FC<PaymentProps> = ({ claimId, payment }) => {
  const hasPaymentLinesPermission = usePermissions(
    ViewPaymentsPermissions.VIEW_CLAIMS_PAYMENT_LINES_LIST
  );

  const {
    state: { value: paymentLines, isPending }
  } = useAsync(fetchPaymentLines, [claimId, payment.paymentId], {
    shouldExecute: () => hasPaymentLinesPermission
  });

  const [showPaymentBreakdown, togglePaymentBreakdown] = useState(false);

  return (
    <CollapsibleCard
      data-test-el="payment-element"
      className={styles.wrapper}
      title={<PaymentHeader payment={payment} />}
    >
      <div className={styles.wrapper} data-test-el="payments-details">
        <PaymentDetails payment={payment} />
        <ContentRenderer isLoading={isPending} isEmpty={_isEmpty(paymentLines)}>
          <Button
            className={styles.link}
            data-test-el="show-payment-breakdown"
            buttonType={ButtonType.LINK}
            onClick={() => togglePaymentBreakdown(!showPaymentBreakdown)}
          >
            <FormattedMessage
              id={
                showPaymentBreakdown
                  ? 'PAYMENTS.HIDE_BREAKDOWN'
                  : 'PAYMENTS.SHOW_BREAKDOWN'
              }
            />
          </Button>
          {showPaymentBreakdown && (
            <PaymentBreakdown payment={payment} paymentLines={paymentLines!} />
          )}
        </ContentRenderer>
      </div>
    </CollapsibleCard>
  );
};
