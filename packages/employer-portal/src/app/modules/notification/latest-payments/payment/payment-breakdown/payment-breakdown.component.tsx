/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  createThemedStyles,
  textStyleToCss,
  ThemeModel,
  FormattedMessage
} from 'fineos-common';
import {
  GroupClientPaymentLine,
  GroupClientPayment
} from 'fineos-js-api-client';
import classNames from 'classnames';

import { Currency } from '../../../../../shared';

import { PaymentBreakdownDetails } from './payment-breakdown-details';
import styles from './payment-breakdown.module.scss';

type PaymentBreakdownProps = {
  payment: GroupClientPayment;
  paymentLines: GroupClientPaymentLine[];
};

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  wrapper: {
    borderColor: theme.colorSchema.neutral3,
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  grossPayment: {
    borderBottomColor: theme.colorSchema.neutral4,
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  netPayment: {
    borderTopColor: theme.colorSchema.neutral6,
    colorSchema: theme.colorSchema.neutral6,
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  paymentSummary: {
    ...textStyleToCss(theme.typography.baseTextSemiBold),
    color: theme.colorSchema.neutral8
  }
}));

export const PaymentBreakdown: React.FC<PaymentBreakdownProps> = ({
  payment,
  paymentLines
}) => {
  const themedStyles = useThemedStyles();

  return (
    <div data-test-el="payment-breakdown">
      {paymentLines?.map((paymentLine, index) => (
        <div key={index}>
          <div
            className={classNames(
              styles.grossPayment,
              themedStyles.grossPayment
            )}
          >
            <div className={styles.payment} data-test-el="payment-item">
              <span>{paymentLine.lineType}</span>
              <Currency amount={paymentLine.amount} />
            </div>
          </div>
          {paymentLine.paymentLinesDetail.map(paymentLineDetail => (
            <div
              key={paymentLineDetail.adjustmentId}
              className={classNames(themedStyles.wrapper, styles.wrapper)}
            >
              <div className={styles.title}>
                <span>{paymentLine.lineType}</span>
                <Currency amount={paymentLineDetail.amount} />
              </div>
              <PaymentBreakdownDetails paymentLineDetail={paymentLineDetail} />
            </div>
          ))}
        </div>
      ))}
      <div className={classNames(styles.netPayment, themedStyles.netPayment)}>
        <div
          data-test-el="payment-summary"
          className={classNames(styles.payment, themedStyles.paymentSummary)}
        >
          <FormattedMessage as="strong" id="PAYMENTS.NET_PAYMENT_AMOUNT" />
          <Currency amount={payment.paymentAmount} />
        </div>
      </div>
    </div>
  );
};
