/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import {
  ContentRenderer,
  useAsync,
  usePermissions,
  ViewNotificationPermissions
} from 'fineos-common';
import { SubCaseType } from 'fineos-js-api-client';

import { PageLayout } from '../../shared';

import {
  fetchAccommodationCases,
  fetchNotification,
  fetchEnhancedAbsencePeriodDecisions,
  fetchDisabilityClaims,
  fetchClaimDisabilityBenefits,
  fetchPaidLeaveBenefits
} from './notification.api';
import {
  getAllAbsencePeriodDecisions,
  getPaidLeaveCasesFromPeriodDecisions
} from './notification.util';
import { NotificationHeader } from './notification-header';
import { NotificationSummary } from './notification-summary';
import { JobProtectedLeave } from './job-protected-leave';
import { WageReplacement } from './wage-replacement';
import { WorkplaceAccommodation } from './workplace-accommodation';
import { Alerts } from './alerts';
import { Timeline } from './timeline';

type NotificationProps = {
  notificationId: string;
};

const EMPTY_ARRAY: [] = [];

export const Notification: React.FC<NotificationProps> = ({
  notificationId
}) => {
  /**
   * PERMISSIONS
   */
  const hasNotificationsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_NOTIFICATIONS
  );
  const hasClaimsReadDisabilityDetailsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_DETAILS
  );
  const hasAbsencePeriodDecisionsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS
  );
  const hasAccommodationsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_ACCOMMODATION_DETAILS
  );
  const hasClaimBenefitsListPermission = usePermissions(
    ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST
  );
  const hasReadDisabilityBenefitsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_BENEFIT
  );

  /**
   * NOTIFICATION
   */
  const {
    state: { value: notification, isPending: isPendingNotification }
  } = useAsync(fetchNotification, [notificationId!], {
    shouldExecute: () => hasNotificationsPermission
  });

  /**
   * JOB-PROTECTED LEAVE
   */
  const absenceIds = useMemo(
    () =>
      hasAbsencePeriodDecisionsPermission && !_isEmpty(notification)
        ? notification!.subCases
            .filter(
              ({ caseType }) =>
                caseType === SubCaseType.ABSENCE_TYPE ||
                caseType === SubCaseType.ABSENCE_TAKEOVER_TYPE ||
                caseType === SubCaseType.ABSENCE_HISTORICAL_CASE_TYPE
            )
            .map(({ id }) => id)
        : [],
    [notification, hasAbsencePeriodDecisionsPermission]
  );

  const {
    state: {
      value: enhancedAbsencePeriodDecisions,
      isPending: isPendingAbsencePeriodDecisions
    }
  } = useAsync(fetchEnhancedAbsencePeriodDecisions, [notification], {
    shouldExecute: () => hasAbsencePeriodDecisionsPermission && !!notification
  });

  // We have enough info to determine if the Job Protected Leave section should be shown
  const showJobProtectedLeave = !_isEmpty(absenceIds);

  /**
   * DISABILITY CLAIMS
   */
  const {
    state: { value: disabilityClaims, isPending: isPendingClaim }
  } = useAsync(fetchDisabilityClaims, [notification], {
    shouldExecute: () =>
      hasClaimsReadDisabilityDetailsPermission && !!notification
  });

  /**
   * WAGE REPLACEMENT
   */
  const claimIds = useMemo(
    () =>
      hasClaimBenefitsListPermission && !_isEmpty(notification)
        ? notification!.subCases
            .filter(({ caseType }) => caseType === SubCaseType.CLAIM_TYPE)
            .map(({ id }) => id)
        : [],
    [notification, hasClaimBenefitsListPermission]
  );

  const paidLeaveCases = useMemo(
    () =>
      hasClaimBenefitsListPermission && hasAbsencePeriodDecisionsPermission
        ? getPaidLeaveCasesFromPeriodDecisions(
            getAllAbsencePeriodDecisions(enhancedAbsencePeriodDecisions)
          )
        : [],
    [
      enhancedAbsencePeriodDecisions,
      hasClaimBenefitsListPermission,
      hasAbsencePeriodDecisionsPermission
    ]
  );

  // We have enough info to determine if the Wage Replacement section should be shown
  const showWageReplacement = !_isEmpty(claimIds) || !_isEmpty(paidLeaveCases);

  const {
    state: { value: claimBenefits, isPending: claimBenefitsLoading }
  } = useAsync(
    fetchClaimDisabilityBenefits,
    [claimIds, hasReadDisabilityBenefitsPermission],
    {
      shouldExecute: () => hasClaimBenefitsListPermission && !!notification
    }
  );

  const {
    state: { value: paidLeaveBenefits, isPending: paidLeaveBenefitsLoading }
  } = useAsync(
    fetchPaidLeaveBenefits,
    [paidLeaveCases, hasReadDisabilityBenefitsPermission],
    {
      shouldExecute: () =>
        hasClaimBenefitsListPermission &&
        hasAbsencePeriodDecisionsPermission &&
        !!notification
    }
  );

  const loadingWageReplacement =
    claimBenefitsLoading || paidLeaveBenefitsLoading;

  /**
   * WORKPLACE ACCOMMODATIONS
   */
  const accommodationCaseIds = useMemo(
    () =>
      hasAccommodationsPermission && !_isEmpty(notification)
        ? notification!.subCases
            .filter(
              ({ caseType }) => caseType === SubCaseType.ACCOMMODATION_TYPE
            )
            .map(subCase => subCase.id)
        : [],
    [notification, hasAccommodationsPermission]
  );

  // We have enough info to determine if the Workplace Accommodation section should be shown
  const showWorkplaceAccommodation = !_isEmpty(accommodationCaseIds);

  const {
    state: { value: accommodations, isPending: isPendingAccommodation }
  } = useAsync(fetchAccommodationCases, [accommodationCaseIds], {
    shouldExecute: () => hasAccommodationsPermission
  });

  const showTimeline =
    Boolean(notification) &&
    (showJobProtectedLeave ||
      showWageReplacement ||
      showWorkplaceAccommodation);

  const isTimelineLoading =
    isPendingAbsencePeriodDecisions ||
    loadingWageReplacement ||
    isPendingAccommodation;

  return (
    <PageLayout
      sideBar={
        notification?.customer && (
          <Alerts caseId={notificationId} customer={notification?.customer} />
        )
      }
      contentHeader={
        <NotificationHeader
          notificationId={notificationId!}
          notification={notification}
        />
      }
    >
      {hasNotificationsPermission && (
        <ContentRenderer
          isLoading={
            isPendingNotification &&
            isPendingAbsencePeriodDecisions &&
            isPendingClaim &&
            isPendingAccommodation
          }
          isEmpty={!notification}
          data-test-el="notification"
        >
          <NotificationSummary
            notification={notification!}
            disabilityClaims={disabilityClaims || EMPTY_ARRAY}
            isJobProtectedLeaveVisible={showJobProtectedLeave}
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
          />

          {showTimeline && (
            <Timeline
              key={notificationId}
              enhancedAbsencePeriodDecisions={
                enhancedAbsencePeriodDecisions || EMPTY_ARRAY
              }
              claimBenefits={claimBenefits || EMPTY_ARRAY}
              paidLeaveBenefits={paidLeaveBenefits || EMPTY_ARRAY}
              accommodations={accommodations || EMPTY_ARRAY}
              isLoading={isTimelineLoading}
            />
          )}

          {showJobProtectedLeave && (
            <JobProtectedLeave
              enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
              isLoading={isPendingAbsencePeriodDecisions}
            />
          )}

          {showWageReplacement && (
            <WageReplacement
              claimBenefits={claimBenefits}
              paidLeaveBenefits={paidLeaveBenefits}
              isLoading={loadingWageReplacement}
            />
          )}

          {showWorkplaceAccommodation && (
            <WorkplaceAccommodation
              accommodations={accommodations}
              notification={notification}
              isLoading={isPendingAccommodation}
            />
          )}
        </ContentRenderer>
      )}
    </PageLayout>
  );
};
