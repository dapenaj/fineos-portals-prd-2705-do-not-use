/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import {
  GanttEntryColor,
  GanttEntryShape,
  GanttGroupRow,
  ConfigurableText
} from 'fineos-common';
import moment from 'moment';
import { IntlShape } from 'react-intl';

import {
  ABSENCE_PERIOD_TYPE,
  ClaimDisabilityBenefit,
  PaidLeaveBenefit,
  RECURRING_BENEFIT,
  WageReplacementDisplayStatus,
  WageReplacementStatus
} from '../../../shared';
import {
  convertStatusToDisplayStatus,
  groupByLeavePlanAndSort
} from '../wage-replacement/wage-replacement.util';
import { parseBenefitName } from '../benefit.util';

const wageReplacementStatusToColor = (status: WageReplacementDisplayStatus) => {
  switch (status) {
    case 'Approved':
      return GanttEntryColor.GREEN;
    case 'Pending':
      return GanttEntryColor.YELLOW;
    case 'Denied':
      return GanttEntryColor.RED;
    case 'Decided':
      return GanttEntryColor.WHITE;
    case 'Closed':
      return GanttEntryColor.GREY;
    case 'Unknown':
      return GanttEntryColor.WHITE
  }
};

export const wageReplacementTypeToShape = (
  type: ABSENCE_PERIOD_TYPE
): GanttEntryShape => {
  switch (type) {
    case ABSENCE_PERIOD_TYPE.INTERMITTENT:
      return GanttEntryShape.STRIPES;
    case ABSENCE_PERIOD_TYPE.REDUCED_SCHEDULE:
      return GanttEntryShape.SEMI_FILLED;
    case ABSENCE_PERIOD_TYPE.FIXED:
    default:
      return GanttEntryShape.FILLED;
  }
};

const wageReplacementStartDate = (
  benefit: ClaimDisabilityBenefit | PaidLeaveBenefit
) =>
  benefit.benefitRightCategory === RECURRING_BENEFIT &&
  benefit.disabilityBenefit
    ? benefit.disabilityBenefit.benefitStartDate
    : benefit.creationDate;

const wageReplacementEndDate = (
  benefit: ClaimDisabilityBenefit | PaidLeaveBenefit
) =>
  benefit.benefitRightCategory === RECURRING_BENEFIT &&
  moment.isMoment(benefit.disabilityBenefit?.approvedThroughDate) &&
  benefit.disabilityBenefit!.approvedThroughDate.isValid()
    ? benefit.disabilityBenefit!.approvedThroughDate
    : benefit.creationDate;

const wageReplacementToTooltip = (
  benefit: ClaimDisabilityBenefit | PaidLeaveBenefit
) => {
  const status = convertStatusToDisplayStatus(
    benefit.status as WageReplacementStatus,
    benefit.stageName
  );

  if (status === 'Approved') {
    if (benefit.benefitRightCategory === RECURRING_BENEFIT) {
      return (
        <ConfigurableText
          id="NOTIFICATIONS.TIMELINE.TOOLTIPS.WAGE_REPLACEMENT_RECURRING"
          values={{
            startDate: wageReplacementStartDate(benefit),
            endDate: wageReplacementEndDate(benefit)
          }}
        />
      );
    } else {
      return (
        <ConfigurableText
          id="NOTIFICATIONS.TIMELINE.TOOLTIPS.WAGE_REPLACEMENT_LUMP"
          values={{
            startDate: wageReplacementStartDate(benefit),
            endDate: wageReplacementEndDate(benefit)
          }}
        />
      );
    }
  } else if (status === 'Closed') {
    return (
      <ConfigurableText id="NOTIFICATIONS.TIMELINE.TOOLTIPS.WAGE_REPLACEMENT_CLOSED" />
    );
  }
};

export const buildTimelineWageReplacementRows = (
  claimBenefits: ClaimDisabilityBenefit[],
  paidLeaveBenefits: PaidLeaveBenefit[],
  intl: IntlShape
): GanttGroupRow[] => {
  const wageReplacementRows: GanttGroupRow[] = [];

  const groupedClaimBenefits = groupByLeavePlanAndSort<ClaimDisabilityBenefit>(
    claimBenefits || []
  );
  const groupedPaidLeaveBenefits = groupByLeavePlanAndSort<PaidLeaveBenefit>(
    paidLeaveBenefits || []
  );

  if (_isEmpty(groupedClaimBenefits) && _isEmpty(groupedPaidLeaveBenefits)) {
    return wageReplacementRows;
  }

  const getTranslationIdForBenefitStatus = (
    label: WageReplacementDisplayStatus
  ) =>
    intl.formatMessage({
      id: `DISABILITY_BENEFIT_STATUS.${label.toLocaleUpperCase()}`
    });

  for (let i = 0; i < groupedClaimBenefits.length; i++) {
    const benefitGroup = groupedClaimBenefits[i];

    const label = convertStatusToDisplayStatus(
      benefitGroup.status as WageReplacementStatus,
      benefitGroup.stageName
    );

    const color = wageReplacementStatusToColor(label);

    wageReplacementRows.push({
      id: `wageReplacement-${benefitGroup.benefitCaseType}-${i}`,
      title: parseBenefitName(benefitGroup.name, intl),
      entries: benefitGroup.benefits.map((benefit, j) => {
        const wageReplacementEntryId = `wageReplacement-${benefit.benefitCaseType}-${i}-${j}-entry`;

        return color === GanttEntryColor.GREEN
          ? {
              id: wageReplacementEntryId,
              color,
              shape: GanttEntryShape.FILLED,
              startDate: wageReplacementStartDate(benefit),
              endDate: wageReplacementEndDate(benefit),
              tooltip: wageReplacementToTooltip(benefit)
            }
          : {
              id: wageReplacementEntryId,
              color,
              shape: GanttEntryShape.FILLED,
              label: getTranslationIdForBenefitStatus(label),
              timeless: true
            };
      })
    });
  }

  for (let i = 0; i < groupedPaidLeaveBenefits.length; i++) {
    const benefitGroup = groupedPaidLeaveBenefits[i];

    const label = convertStatusToDisplayStatus(
      benefitGroup.status as WageReplacementStatus,
      benefitGroup.stageName
    );

    const color = wageReplacementStatusToColor(
      convertStatusToDisplayStatus(
        benefitGroup.status as WageReplacementStatus,
        benefitGroup.stageName
      )
    );

    wageReplacementRows.push({
      id: `wageReplacement-${benefitGroup.benefitCaseType}-${i}`,
      title: parseBenefitName(benefitGroup.name, intl),
      entries: benefitGroup.benefits.map((benefit, j) => {
        const wageReplacementEntryId = `wageReplacement-${benefit.benefitCaseType}-${i}-${j}-entry`;

        const shape = wageReplacementTypeToShape(
          benefit.type as ABSENCE_PERIOD_TYPE
        );

        return color === GanttEntryColor.GREEN
          ? {
              id: wageReplacementEntryId,
              color,
              shape,
              startDate: wageReplacementStartDate(benefit),
              endDate: wageReplacementEndDate(benefit),
              tooltip: wageReplacementToTooltip(benefit)
            }
          : {
              id: wageReplacementEntryId,
              color,
              shape,
              label: getTranslationIdForBenefitStatus(label),
              timeless: true
            };
      })
    });
  }

  return wageReplacementRows;
};
