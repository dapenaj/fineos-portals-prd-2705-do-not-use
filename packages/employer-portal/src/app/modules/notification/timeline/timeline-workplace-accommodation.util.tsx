/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  GanttEntryColor,
  GanttEntryShape,
  GanttGroupRow,
  MultipleTokens,
  ConfigurableText
} from 'fineos-common';
import { GroupClientAccommodationCaseSummary } from 'fineos-js-api-client';

import { getWorkplaceAccommodationStatusLabel } from '../workplace-accommodation';
import {
  WorkplaceAccommodationPhase,
  WorkplaceAccommodationStage,
  WorkplaceAccommodationStatusLabel
} from '../../../shared';

const statusLabelToColor = (status: WorkplaceAccommodationStatusLabel) => {
  switch (status) {
    case WorkplaceAccommodationStatusLabel.ACCOMMODATED:
      return GanttEntryColor.GREEN;
    case WorkplaceAccommodationStatusLabel.NOT_ACCOMMODATED:
      return GanttEntryColor.RED;
    case WorkplaceAccommodationStatusLabel.PENDING_IMPLEMENTING:
    case WorkplaceAccommodationStatusLabel.PENDING_IN_ASSESSMENT:
    case WorkplaceAccommodationStatusLabel.PENDING_OPTIONS:
    case WorkplaceAccommodationStatusLabel.PENDING_RECOGNIZED:
      return GanttEntryColor.YELLOW;
  }
};

export const buildTimelineWorkplaceAccommodationsRows = (
  accommodationSummaries: GroupClientAccommodationCaseSummary[]
): GanttGroupRow[] => {
  const workplaceAccommodationsRows: GanttGroupRow[] = [];

  for (const accommodationSummary of accommodationSummaries) {
    const tempAccommodation: GroupClientAccommodationCaseSummary = {
      ...accommodationSummary,
      accommodations: accommodationSummary.accommodations.filter(
        ({ implementedDate, endDate }) =>
          !!implementedDate &&
          implementedDate.isValid() &&
          !!endDate &&
          endDate.isValid()
      )
    };

    for (let i = 0; i < tempAccommodation.accommodations.length; i++) {
      const accommodation = accommodationSummary.accommodations[i];
      const statusLabel = getWorkplaceAccommodationStatusLabel(
        accommodationSummary.stage as WorkplaceAccommodationStage,
        accommodationSummary.phase as WorkplaceAccommodationPhase,
        accommodationSummary
      );
      const startDate = accommodation.implementedDate;
      const endDate =
        accommodation.endDate && accommodation.endDate.isValid()
          ? accommodation.endDate
          : accommodation.implementedDate;

      const tooltip =
        statusLabel === WorkplaceAccommodationStatusLabel.ACCOMMODATED ? (
          <ConfigurableText
            id="NOTIFICATIONS.TIMELINE.TOOLTIPS.WORKPLACE_ACCOMMODATION"
            values={{
              startDate,
              endDate
            }}
          />
        ) : null;

      workplaceAccommodationsRows.push({
        id: `workplaceAccommodation-${accommodationSummary.id}-${i}`,
        title: (
          <MultipleTokens
            tokens={[
              accommodation.accommodationCategory,
              accommodation.accommodationType
            ]}
          />
        ),
        entries: [
          {
            id: `workplaceAccommodation-${accommodationSummary.id}-${i}-entry`,
            startDate,
            endDate,
            color: statusLabelToColor(statusLabel),
            shape: GanttEntryShape.FILLED,
            tooltip
          }
        ]
      });
    }
  }

  return workplaceAccommodationsRows;
};
