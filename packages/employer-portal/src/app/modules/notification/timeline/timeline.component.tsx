/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import {
  Button,
  ButtonType,
  ContentRenderer,
  Gantt,
  GanttGroup,
  Panel,
  TimelineLegend,
  CollapsibleCard,
  FormattedMessage
} from 'fineos-common';
import { GroupClientAccommodationCaseSummary } from 'fineos-js-api-client';

import {
  ClaimDisabilityBenefit,
  EnhancedAbsencePeriodDecisions,
  PaidLeaveBenefit
} from '../../../shared';

import { buildTimelineWageReplacementRows } from './timeline-wage-replacement.util';
import { buildTimelineJobProtectedLeaveRows } from './timeline-job-protected-leave.util';
import { buildTimelineWorkplaceAccommodationsRows } from './timeline-workplace-accommodation.util';
import styles from './timeline.module.scss';

type TimelineProps = {
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[];
  claimBenefits: ClaimDisabilityBenefit[];
  paidLeaveBenefits: PaidLeaveBenefit[];
  accommodations: GroupClientAccommodationCaseSummary[];
  isLoading: boolean;
};

/**
 * Timeline shows notification details in time
 *
 * TECH NOTE: There no need to have permission checking here, cause basically,
 * if no permission would be provided, then no data would be passed to Timeline
 * and it would render nothing
 *
 * @param accommodations
 * @param isLoading
 * @param enhancedAbsencePeriodDecisions
 * @param claimBenefits
 * @param paidLeaveBenefits
 * @constructor
 */
export const Timeline: React.FC<TimelineProps> = ({
  accommodations,
  isLoading,
  enhancedAbsencePeriodDecisions,
  claimBenefits,
  paidLeaveBenefits
}) => {
  const [isLegendShown, setIsLegendShown] = useState(false);
  const intl = useIntl();

  const ganttGroups: GanttGroup[] = [];

  const jobProtectedLeaveGroups = buildTimelineJobProtectedLeaveRows(
    enhancedAbsencePeriodDecisions
  );
  const wageReplacementRows = buildTimelineWageReplacementRows(
    claimBenefits,
    paidLeaveBenefits,
    intl
  );
  const workplaceAccommodationRows = buildTimelineWorkplaceAccommodationsRows(
    accommodations
  );

  if (jobProtectedLeaveGroups.length) {
    for (const jobProtectedLeaveGroup of jobProtectedLeaveGroups) {
      ganttGroups.push(jobProtectedLeaveGroup);
    }
  }

  if (wageReplacementRows.length) {
    ganttGroups.push({
      id: 'wageReplacement',
      title: <FormattedMessage id="NOTIFICATIONS.TYPE.WAGE_REPLACEMENT" />,
      rows: wageReplacementRows
    });
  }

  if (workplaceAccommodationRows.length) {
    ganttGroups.push({
      id: 'workplaceAccommodation',
      title: (
        <FormattedMessage id="NOTIFICATIONS.TYPE.TEMPORARY_ACCOMMODATIONS" />
      ),
      rows: workplaceAccommodationRows
    });
  }

  return ganttGroups.length && !isLoading ? (
    <ContentRenderer
      isEmpty={false}
      isLoading={isLoading}
      data-test-el="timeline"
    >
      <CollapsibleCard
        title={<FormattedMessage id="NOTIFICATIONS.TIMELINE.TITLE" />}
        role="region"
        aria-label={<FormattedMessage id="NOTIFICATIONS.TIMELINE.TITLE_ARIA" />}
        isOpen={true}
        isPanelUI={true}
      >
        <div className={styles.legendControlRow}>
          <Button
            buttonType={ButtonType.LINK}
            name="legend-toggle-control"
            tabIndex={-1}
            onClick={() => setIsLegendShown(!isLegendShown)}
          >
            <FormattedMessage
              id={
                'NOTIFICATIONS.TIMELINE.' +
                (!isLegendShown ? 'SHOW_CAPTION' : 'CLOSE_CAPTION')
              }
            />
          </Button>
        </div>

        <div className={styles.ganttContainer}>
          <Gantt
            title={<FormattedMessage id="NOTIFICATIONS.TIMELINE.GANTT_TITLE" />}
            groups={ganttGroups}
          />

          <Panel
            className={classNames(styles.legend, isLegendShown && styles.open)}
            data-test-el="timeline-legend"
            tabIndex={-1}
          >
            <TimelineLegend />
          </Panel>
        </div>
      </CollapsibleCard>
    </ContentRenderer>
  ) : null;
};
