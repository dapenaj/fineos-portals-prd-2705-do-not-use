/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  GanttGroupRow,
  GanttEntry,
  GanttEntryShape,
  GanttEntryColor,
  ConfigurableText,
  GanttGroup,
  FormattedMessage
} from 'fineos-common';

import {
  DecisionStatusCategory,
  EnhancedAbsencePeriodDecisions,
  EnhancedDecision,
  JobProtectedLeaveStatus
} from '../../../shared';
import { groupByLeavePlanName } from '../job-protected-leave/job-protected-leave.util';

const jobProtectedLeaveStatusToShape = (
  jobProtectedLeaveStatus: JobProtectedLeaveStatus
): GanttEntryShape => {
  switch (jobProtectedLeaveStatus) {
    case JobProtectedLeaveStatus.EPISODIC:
      return GanttEntryShape.STRIPES;
    case JobProtectedLeaveStatus.REDUCED_SCHEDULE:
      return GanttEntryShape.SEMI_FILLED;
    default:
      return GanttEntryShape.FILLED;
  }
};

export const decisionStatusToColor = (
  statusCategory: DecisionStatusCategory
): GanttEntryColor | null => {
  switch (statusCategory) {
    case DecisionStatusCategory.APPROVED:
      return GanttEntryColor.GREEN;
    case DecisionStatusCategory.DENIED:
      return GanttEntryColor.RED;
    case DecisionStatusCategory.PENDING:
      return GanttEntryColor.YELLOW;
    case DecisionStatusCategory.CANCELLED:
      return GanttEntryColor.GREY;
  }

  return null;
};

const jobProtectedLeaveStatusToTooltipPart = (
  status: DecisionStatusCategory
): string | null => {
  switch (status) {
    case DecisionStatusCategory.APPROVED:
      return 'APPROVED';
    case DecisionStatusCategory.PENDING:
      return 'PENDING';
    case DecisionStatusCategory.DENIED:
      return 'DENIED';
    case DecisionStatusCategory.CANCELLED:
      return 'CANCELLED';
    case DecisionStatusCategory.SKIPPED:
      return 'CLOSED';
  }

  return null;
};

const jobProtectedLeaveTypeToTooltipPart = (type: JobProtectedLeaveStatus) => {
  switch (type) {
    case JobProtectedLeaveStatus.EPISODIC:
      return 'INTERMITTENT';
    case JobProtectedLeaveStatus.REDUCED_SCHEDULE:
      return 'REDUCED';
    case JobProtectedLeaveStatus.TIME_OFF_PERIOD:
      return 'CONTINUOUS';
  }
};

const jobProtectedLeaveDecisionToTooltip = (decision: EnhancedDecision) => {
  const tooltipStatus = jobProtectedLeaveStatusToTooltipPart(
    decision.statusCategory
  );
  const tooltipType = jobProtectedLeaveTypeToTooltipPart(
    decision.period.type as JobProtectedLeaveStatus
  );

  if (tooltipStatus) {
    return (
      <ConfigurableText
        id={`NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_${tooltipStatus}_${tooltipType}`}
        values={{
          startDate: decision.period.startDate,
          endDate: decision.period.endDate
        }}
      />
    );
  }
};

export const buildTimelineJobProtectedLeaveRows = (
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[]
): GanttGroup[] => {
  const jobProtectedLeaveRowGroups: GanttGroup[] = [];
  const jobProtectedLeaveGroups = enhancedAbsencePeriodDecisions.map(
    ({ absenceId, absencePeriodDecisions }) => {
      const { decisions } = absencePeriodDecisions;

      return { absenceId, leavePlans: groupByLeavePlanName(decisions) };
    }
  );

  if (jobProtectedLeaveGroups.length) {
    for (const { absenceId, leavePlans } of jobProtectedLeaveGroups) {
      const jobProtectedLeaveRows: GanttGroupRow[] = [];

      for (const leavePlan of leavePlans) {
        jobProtectedLeaveRows.push({
          id: leavePlan.leavePlanName,
          title: leavePlan.leavePlanName,
          entries: leavePlan.decisions.reduce(
            (acc: GanttEntry[], decision: EnhancedDecision, index: number) => {
              const color = decisionStatusToColor(decision.statusCategory);

              if (color !== null) {
                acc.push({
                  id: `jobProtectedLeave-${absenceId}-${leavePlan.leavePlanName}-${index}`,
                  color,
                  shape: jobProtectedLeaveStatusToShape(
                    decision.period.type as JobProtectedLeaveStatus
                  ),
                  startDate: decision.period.startDate,
                  endDate: decision.period.endDate,
                  tooltip: jobProtectedLeaveDecisionToTooltip(decision)
                });
              }

              return acc;
            },
            []
          )
        });
      }

      if (jobProtectedLeaveRows.length) {
        jobProtectedLeaveRowGroups.push({
          id: `jobProtectedLeaveGroup-${absenceId}`,
          title: (
            <FormattedMessage id="NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE" />
          ),
          rows: jobProtectedLeaveRows
        });
      }
    }
  }

  return jobProtectedLeaveRowGroups;
};
