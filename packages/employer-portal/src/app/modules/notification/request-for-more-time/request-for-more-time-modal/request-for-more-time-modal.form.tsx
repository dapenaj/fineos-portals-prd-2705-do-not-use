import * as Yup from 'yup';

import { RequestForMoreTimeFormValues } from './request-for-more-time-modal.type';

const ADDITIONAL_NOTES_MIN_LENGTH = 5;
export const ADDITIONAL_NOTES_MAX_LENGTH = 250;

export const requestForMoreTimeInitialValues: RequestForMoreTimeFormValues = {
  changeRequestPeriods: [],
  additionalNotes: ''
};

export const requestForMoreTimeValidation = Yup.object({
  changeRequestPeriods: Yup.array()
    .required('REQUEST_FOR_MORE_TIME.REQUIRED_PERIOD_VALIDATION')
    .nullable(),
  additionalNotes: Yup.string()
    .nullable()
    .min(
      ADDITIONAL_NOTES_MIN_LENGTH,
      'FINEOS_COMMON.VALIDATION.MIN_TEXTAREA_LENGTH'
    )
    .max(
      ADDITIONAL_NOTES_MAX_LENGTH,
      'REQUEST_FOR_MORE_TIME.ADDITIONAL_NOTES_MAX_LENGTH_VALIDATION'
    )
});
