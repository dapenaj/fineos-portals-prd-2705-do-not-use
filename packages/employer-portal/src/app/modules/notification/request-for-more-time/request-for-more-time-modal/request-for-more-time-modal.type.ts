import { Moment } from 'moment';

export type RequestForMoreTimeFormValues = {
  changeRequestPeriods: [Moment, Moment] | [];
  additionalNotes: string;
};
