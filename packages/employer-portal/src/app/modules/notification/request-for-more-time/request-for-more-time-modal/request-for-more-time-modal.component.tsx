/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { Field, Formik, FormikHelpers } from 'formik';
import { Row } from 'antd';
import moment, { Moment } from 'moment';
import classNames from 'classnames';
import { chain as _chain } from 'lodash';
import {
  AsteriskInfo,
  Modal,
  FormButton,
  UiButton,
  ButtonType,
  Form,
  FormGroup,
  TextArea,
  RangePickerInput,
  ModalFooter,
  useErrorNotificationPopup,
  useGanttTimeBarsThemedStyles,
  createThemedStyles,
  primaryLightColor,
  FormattedMessage,
  primaryLight2Color
} from 'fineos-common';
import { LeavePeriodChangeReason } from 'fineos-js-api-client';

import {
  EnhancedGroupClientPeriodDecision,
  ABSENCE_PERIOD_TYPE
} from '../../../../shared';
import { wageReplacementTypeToShape } from '../../timeline/timeline-wage-replacement.util';
import { decisionStatusToColor } from '../../timeline/timeline-job-protected-leave.util';
import {
  decisionsSortOrder,
  flatNonCancelledDecisions
} from '../../job-protected-leave/job-protected-leave.util';

import {
  requestForMoreTimeInitialValues,
  requestForMoreTimeValidation,
  ADDITIONAL_NOTES_MAX_LENGTH
} from './request-for-more-time-modal.form';
import { RequestForMoreTimeFormValues } from './request-for-more-time-modal.type';
import { leavePeriodChangeRequest } from './request-for-more-time-modal.api';
import styles from './request-for-more-time-modal.module.scss';

type RequestForMoreTimeModalProps = {
  absenceId: string;
  absencePeriod: EnhancedGroupClientPeriodDecision | null;
  onCancel: () => void;
  onFormSubmit: () => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    '& .ant-picker-range-wrapper': {
      '& .ant-picker-range-arrow::after': {
        borderColor: theme.colorSchema.neutral1
      },
      '& .ant-picker-panel-container': {
        background: theme.colorSchema.neutral1
      }
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-selected .ant-picker-cell-inner,
     .ant-picker-cell-in-view.ant-picker-cell-range-start.ant-picker-cell-inner,
      .ant-picker-cell-in-view.ant-picker-cell-range-end .ant-picker-cell-inner,
      .ant-picker-cell-in-view.ant-picker-cell-range-start .ant-picker-cell-inner`]: {
      background: theme.colorSchema.primaryColor
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-in-range::before,
     .ant-picker-cell-in-view.ant-picker-cell-range-start:not(.ant-picker-cell-range-start-single)::before,
     .ant-picker-cell-in-view.ant-picker-cell-range-end:not(.ant-picker-cell-range-end-single)::before`]: {
      background: primaryLightColor(theme.colorSchema)
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-start.ant-picker-cell-range-hover::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-end.ant-picker-cell-range-hover::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-start:not(.ant-picker-cell-range-start-single).ant-picker-cell-range-hover-start::before,
    .ant-picker-cell-in-view.ant-picker-cell-range-end:not(.ant-picker-cell-range-end-single).ant-picker-cell-range-hover-end::before,
    .ant-picker-panel > :not(.ant-picker-date-panel) .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-start::before,
    .ant-picker-panel > :not(.ant-picker-date-panel) .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-end::before,
    .ant-picker-date-panel .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-start .ant-picker-cell-inner::after,
    .ant-picker-date-panel
    .ant-picker-cell-in-view.ant-picker-cell-in-range.ant-picker-cell-range-hover-end .ant-picker-cell-inner::after`]: {
      background: primaryLight2Color(theme.colorSchema)
    },
    [`& tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover:first-child::after,
    tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover-end:first-child::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-edge-start:not(.ant-picker-cell-range-hover-edge-start-near-range)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-start::after`]: {
      borderLeftColor: primaryLightColor(theme.colorSchema)
    },
    [`& tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover:last-child::after,
    tr > .ant-picker-cell-in-view.ant-picker-cell-range-hover-start:last-child::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-edge-end:not(.ant-picker-cell-range-hover-edge-end-near-range)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-end::after`]: {
      borderRightColor: primaryLightColor(theme.colorSchema)
    },
    [`& .ant-picker-cell-in-view.ant-picker-cell-range-hover-start:not(.ant-picker-cell-in-range):not(.ant-picker-cell-range-start):not(.ant-picker-cell-range-end)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-end:not(.ant-picker-cell-in-range):not(.ant-picker-cell-range-start):not(.ant-picker-cell-range-end)::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-start.ant-picker-cell-range-start-single::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover-end.ant-picker-cell-range-end-single::after,
    .ant-picker-cell-in-view.ant-picker-cell-range-hover:not(.ant-picker-cell-in-range)::after`]: {
      borderTopColor: primaryLightColor(theme.colorSchema),
      borderBottomColor: primaryLightColor(theme.colorSchema)
    },
    [`& .${styles.periodStart}`]: {
      '&:before': {
        borderLeftColor: theme.colorSchema.neutral1
      }
    },
    [`& .${styles.periodEnd}`]: {
      '&:after': {
        borderRightColor: theme.colorSchema.neutral1
      }
    }
  }
}));

// we assume that there would be no period longer than 3 years
// which is 1095 days, but we want to have round values so it's
// rounded up to 10000 (~27 years)
const MAX_POSSIBLE_PERIOD = 10000;

export const RequestForMoreTimeModal: React.FC<RequestForMoreTimeModalProps> = ({
  absenceId,
  absencePeriod,
  onCancel,
  onFormSubmit
}) => {
  const { showErrorNotification } = useErrorNotificationPopup();
  const ganttTimeBarsThemedStyles = useGanttTimeBarsThemedStyles();
  const themedStyles = useThemedStyles();

  const enhancedDecisions = useMemo(
    () =>
      _chain(flatNonCancelledDecisions(absencePeriod?.decisions || []))
        .sortBy(decision => {
          // first, we sort by status
          const sortIndexByStatus =
            decisionsSortOrder.indexOf(decision.statusCategory) *
            MAX_POSSIBLE_PERIOD;
          // second, we sort by longest decision period, cause from UI perspective
          // long non-breaking periods looks better
          //
          // NOTE: it shouldn't be possible to have decision longer than ~27 years,
          // but better to add safe constraint with Math.max in order to handle those
          // unexpected cases
          const sortIndexByDays = Math.max(
            MAX_POSSIBLE_PERIOD -
              decision.period.endDate.diff(decision.period.startDate, 'days'),
            0
          );

          return sortIndexByStatus + sortIndexByDays;
        })
        .value(),
    [absencePeriod]
  );
  const disabledDate = (current: Moment) => {
    let disableDay = false;

    for (const decision of enhancedDecisions) {
      if (
        current.isBetween(
          decision.period.startDate,
          decision.period.endDate,
          'day',
          '[]'
        )
      ) {
        disableDay = true;
      }
    }

    return current < moment().startOf('day') || disableDay;
  };

  const dateRender = (current: Moment) => {
    const dateClasses = [];

    for (const decision of enhancedDecisions) {
      const color = decisionStatusToColor(decision.statusCategory);

      if (
        color &&
        current.isBetween(
          decision.period.startDate,
          decision.period.endDate,
          'day',
          '[]'
        )
      ) {
        if (current.isSame(decision.period.startDate, 'day')) {
          dateClasses.push(styles.periodStart);
        }

        if (current.isSame(decision.period.endDate, 'day')) {
          dateClasses.push(styles.periodEnd);
        }

        dateClasses.push(
          ganttTimeBarsThemedStyles[color],
          wageReplacementTypeToShape(
            decision.period.type as ABSENCE_PERIOD_TYPE
          )
        );

        break;
      }
    }

    const dayElement = (
      <div className="ant-picker-cell-inner">{current.date()}</div>
    );

    if (!dateClasses.length) {
      return dayElement;
    }

    return (
      <div className={classNames(dateClasses)}>
        <div className={styles.lighterBackground}>{dayElement}</div>
      </div>
    );
  };

  const handleSubmit = async (
    formValues: RequestForMoreTimeFormValues,
    formikBag: FormikHelpers<RequestForMoreTimeFormValues>
  ) => {
    const data = {
      reason: LeavePeriodChangeReason.UNKNOWN,
      changeRequestPeriods: [
        {
          startDate: formValues.changeRequestPeriods[0]!,
          endDate: formValues.changeRequestPeriods[1]!
        }
      ],
      additionalNotes: formValues.additionalNotes
    };

    try {
      await leavePeriodChangeRequest(absenceId, data);
      onFormSubmit();
    } catch (e) {
      showErrorNotification(e);
    } finally {
      formikBag.setSubmitting(false);
    }
  };

  const changeRequestPeriodsFieldName = 'changeRequestPeriods';

  return (
    <Modal
      ariaLabelId="REQUEST_FOR_MORE_TIME.TITLE"
      header={<FormattedMessage id="REQUEST_FOR_MORE_TIME.TITLE" />}
      visible={true}
      onCancel={onCancel}
      data-test-el="request-for-more-time-modal"
    >
      <AsteriskInfo />
      <Formik
        initialValues={requestForMoreTimeInitialValues}
        validationSchema={requestForMoreTimeValidation}
        validateOnMount={true}
        onSubmit={handleSubmit}
      >
        {({ setFieldTouched }) => (
          <Form>
            <Row>
              <FormGroup
                isRequired={true}
                label={
                  <FormattedMessage id="REQUEST_FOR_MORE_TIME.EXTRA_TIME_LABEL" />
                }
              >
                <Field
                  onBlur={() => {
                    setFieldTouched(changeRequestPeriodsFieldName, true, true);
                  }}
                  name={changeRequestPeriodsFieldName}
                  component={RangePickerInput}
                  disabledDate={disabledDate}
                  dateRender={dateRender}
                  dropdownClassName={themedStyles.wrapper}
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup
                label={
                  <FormattedMessage id="REQUEST_FOR_MORE_TIME.DESCRIPTION_LABEL" />
                }
                counterMaxLength={ADDITIONAL_NOTES_MAX_LENGTH}
                immediateErrorMessages={[
                  'REQUEST_FOR_MORE_TIME.ADDITIONAL_NOTES_MAX_LENGTH_VALIDATION'
                ]}
              >
                <Field name="additionalNotes" component={TextArea} />
              </FormGroup>
            </Row>
            <ModalFooter>
              <FormattedMessage
                as={FormButton}
                htmlType="submit"
                shouldSubmitForm={false}
                id="FINEOS_COMMON.GENERAL.OK"
              />

              <FormattedMessage
                as={UiButton}
                buttonType={ButtonType.LINK}
                onClick={onCancel}
                id="FINEOS_COMMON.GENERAL.CANCEL"
              />
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};
