/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Row } from 'antd';
import classNames from 'classnames';
import { useIntl } from 'react-intl';
import {
  Button,
  ButtonType,
  createThemedStyles,
  textStyleToCss,
  ConfigurableText,
  FormattedMessage,
  useElementId,
  ReakitPopover,
  ReakitPopoverDisclosure,
  ReakitPopoverArrow,
  ReakitUsePopoverState,
  getFocusedElementShadow
} from 'fineos-common';
import { BaseDomain } from 'fineos-js-api-client';

import { EnumDomain, getEnumDomainTranslation } from '../../../../shared';

import styles from './request-for-more-time-popover.module.scss';

type RequestForMoreTimePopoverProps = {
  onConfirm: () => void;
  className?: string;
  notificationReason: BaseDomain;
};

const useThemedStyles = createThemedStyles(theme => ({
  overlay: {
    borderColor: theme.colorSchema.neutral1,
    backgroundColor: 'transparent'
  },
  content: {
    backgroundColor: theme.colorSchema.neutral1
  },
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  body: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  actionPrimaryButton: {
    '& > span': {
      color: `${theme.colorSchema.neutral1} !important`
    }
  },
  arrow: {
    fill: theme.colorSchema.neutral1
  },
  button: {
    backgroundColor: theme.colorSchema.neutral1,
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText),
    '&:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    },
    '&:hover': {
      backgroundColor: theme.colorSchema.neutral2
    }
  }
}));

export const RequestForMoreTimePopover: React.FC<RequestForMoreTimePopoverProps> = ({
  children,
  notificationReason,
  onConfirm
}) => {
  const themedStyles = useThemedStyles();
  const [isPopoverVisible, setPopoverVisible] = useState(false);
  const intl = useIntl();
  const popover = ReakitUsePopoverState({ placement: 'bottom', animated: 250 });
  const buttonId = useElementId();

  React.useEffect(() => {
    setPopoverVisible(popover.visible);
  }, [popover.visible]);

  // Create focus loop for popover
  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === 'Tab') {
      const button = document.getElementById(buttonId) as HTMLButtonElement;
      button && button.focus();
      button && button.blur();
    }
  };

  const content = (
    <Row
      className={styles.container}
      data-test-el="request-for-more-time-popover-content"
    >
      <FormattedMessage
        id="REQUEST_FOR_MORE_TIME.POPOVER_HEADER"
        as="h3"
        className={classNames(styles.popoverHeader, themedStyles.header)}
      />

      <p className={themedStyles.body}>
        <ConfigurableText
          id="REQUEST_FOR_MORE_TIME.POPOVER_BODY"
          values={{
            notificationReason: intl.formatMessage({
              id: getEnumDomainTranslation(
                EnumDomain.NOTIFICATION_REASON,
                notificationReason
              ),
              defaultMessage: notificationReason.name
            })
          }}
        />
      </p>

      <Row className={styles.popoverFooter}>
        <Button
          id={buttonId}
          onClick={() => {
            setPopoverVisible(false);
            onConfirm();
          }}
          className={themedStyles.actionPrimaryButton}
          buttonType={ButtonType.PRIMARY}
        >
          <FormattedMessage id="REQUEST_FOR_MORE_TIME.POPOVER_PROCEED" />
        </Button>
        <Button
          buttonType={ButtonType.LINK}
          onKeyDown={handleKeyDown}
          onClick={() => {
            popover.hide();
            setPopoverVisible(false);
          }}
        >
          <FormattedMessage id="FINEOS_COMMON.GENERAL.CANCEL" />
        </Button>
      </Row>
    </Row>
  );
  const onAnimationEnd = () => {
    // wait until animation end, find button and focus it
    const button = document.getElementById(buttonId);
    button && button.focus();
  };

  return (
    <div
      className={styles.popoverContainer}
      data-test-el="request-for-more-time-popover"
    >
      <ReakitPopoverDisclosure
        {...popover}
        className={classNames(styles.button, themedStyles.button)}
        onAnimationEnd={onAnimationEnd}
      >
        {children}
      </ReakitPopoverDisclosure>
      <ReakitPopover {...popover} className={styles.popover}>
        {isPopoverVisible ? (
          <div
            className={classNames(styles.animation, themedStyles.overlay)}
            data-test-el="request-for-more-time-popover-open"
          >
            <ReakitPopoverArrow
              {...popover}
              className={classNames(styles.arrowShadow, themedStyles.arrow)}
            />
            <div
              className={classNames(
                styles.popoverContent,
                themedStyles.content
              )}
            >
              {content}
            </div>
          </div>
        ) : null}
      </ReakitPopover>
    </div>
  );
};
