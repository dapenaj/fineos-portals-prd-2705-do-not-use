/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GroupClientPeriodDecisions } from 'fineos-js-api-client';
import { flatten as _flatten, isEmpty as _isEmpty } from 'lodash';

import {
  EnhancedAbsencePeriodDecisions,
  PaidLeaveCaseSummary
} from '../../shared/types';

const getAllAbsencePeriodDecisions = (
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[] | null
): GroupClientPeriodDecisions[] | null =>
  !_isEmpty(enhancedAbsencePeriodDecisions)
    ? _flatten(
        enhancedAbsencePeriodDecisions!.map(period => ({
          absenceId: period.absenceId,
          startDate: period.absencePeriodDecisions.startDate,
          endDate: period.absencePeriodDecisions.endDate,
          decisions: period.absencePeriodDecisions.decisions,
          calculationPeriodMethod: ''
        }))
      )
    : null;

const getPaidLeaveCasesFromPeriodDecisions = (
  absencePeriodDecisions: GroupClientPeriodDecisions[] | null
) =>
  !_isEmpty(absencePeriodDecisions)
    ? _flatten(
        (absencePeriodDecisions as GroupClientPeriodDecisions[]).map(
          ({ decisions }) =>
            !_isEmpty(decisions)
              ? decisions.reduce(
                  (
                    paidLeaveCases: PaidLeaveCaseSummary[],
                    {
                      period: {
                        leavePlan,
                        leaveRequest: { reasonName },
                        type
                      }
                    }
                  ) => {
                    if (
                      leavePlan &&
                      leavePlan.paidLeaveCaseId &&
                      !paidLeaveCases.some(
                        paidLeaveCase =>
                          paidLeaveCase.id === leavePlan.paidLeaveCaseId
                      )
                    ) {
                      paidLeaveCases.push({
                        id: leavePlan.paidLeaveCaseId,
                        name: leavePlan.name || leavePlan.shortName,
                        reasonName,
                        type
                      });
                    }

                    return paidLeaveCases;
                  },
                  []
                )
              : []
        )
      )
    : [];

export { getAllAbsencePeriodDecisions, getPaidLeaveCasesFromPeriodDecisions };
