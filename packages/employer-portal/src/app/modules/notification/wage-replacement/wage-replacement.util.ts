/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { chain as _chain } from 'lodash';
import { StatusType, StatusPattern } from 'fineos-common';

import {
  ClaimDisabilityBenefit,
  PaidLeaveBenefit,
  isPaidLeaveBenefit,
  WageReplacementDisplayStatus,
  WageReplacementStatus,
  ABSENCE_PERIOD_TYPE,
  BenefitGroup
} from '../../../shared';

export const convertStatusToDisplayStatus = (
  status: WageReplacementStatus,
  stageName: string
): WageReplacementDisplayStatus => {
  switch (status) {
    case 'Pending':
      return 'Pending';
    case 'Decided':
      return getStatusFromStageName(stageName);
    case 'Closed':
      return 'Closed';
    case 'Unknown':
      return getStatusFromStageNameWithUnknown(stageName);
    default:
      return 'Unknown';
  }
};

export const getStatusFromStageNameWithUnknown = (
  stageName: string
): WageReplacementDisplayStatus => {
  switch (stageName) {
    case 'Approved':
      return 'Approved';
    case 'Denied':
      return 'Denied';
    case 'Pending':
      return 'Pending';
    case 'Closed':
      return 'Closed';
    default:
      return 'Unknown';
  }
};

// Returns a status based on the stageName (assuming Benefit.status === "Decided")
export const getStatusFromStageName = (
  stageName: string
): WageReplacementDisplayStatus => {
  switch (stageName) {
    case 'Approved':
      return 'Approved';
    case 'Denied':
      return 'Denied';
    case 'Closed':
      return 'Closed';
    default:
      return 'Decided';
  }
};

export const getStatusTypeByStatus = (status: string): StatusType => {
  switch (status) {
    case 'Pending':
      return StatusType.WARNING;
    case 'Approved':
      return StatusType.SUCCESS;
    case 'Denied':
      return StatusType.DANGER;
    case 'Closed':
      return StatusType.NEUTRAL;
    case 'Decided':
      return StatusType.BLANK;
    case 'Unknown':
      return StatusType.BLANK;
    default:
      return StatusType.BLANK;
  }
};

export const groupByLeavePlanAndSort = <
  T extends ClaimDisabilityBenefit | PaidLeaveBenefit = PaidLeaveBenefit
>(
  benefits: T[]
): BenefitGroup<T>[] => {
  return _chain(benefits)
    .orderBy(
      [
        // sort by creationDate (most recent first)
        'creationDate',
        // then sort alphabetically by benefitCaseType or leavePlanName
        val => {
          return isPaidLeaveBenefit(val)
            ? val.leavePlanName
            : val.benefitCaseType;
        }
      ],
      ['desc', 'asc']
    )
    .groupBy(benefit => {
      const benefitName = isPaidLeaveBenefit(benefit)
        ? benefit.leavePlanName
        : benefit.benefitCaseType;

      return [
        benefitName,
        benefit.benefitHandler,
        benefit.benefitHandlerPhoneNo,
        benefit.stageName,
        benefit.status
      ].join('-');
    })
    .map(values => {
      const first = values[0];

      return {
        name: isPaidLeaveBenefit(first)
          ? first.leavePlanName
          : first.benefitCaseType,
        benefitId: first.benefitId,
        status: first.status,
        stageName: first.stageName,
        benefitHandler: first.benefitHandler,
        benefitHandlerPhoneNo: first.benefitHandlerPhoneNo,
        benefitCaseType: first.benefitCaseType,
        benefits: values
      };
    })
    .value();
};

export const getStatusPatternForAbsencePeriodType = (
  type: ABSENCE_PERIOD_TYPE
) => {
  switch (type) {
    case ABSENCE_PERIOD_TYPE.INTERMITTENT:
      return StatusPattern.ANGLED;
    case ABSENCE_PERIOD_TYPE.REDUCED_SCHEDULE:
      return StatusPattern.HALVED;
    case ABSENCE_PERIOD_TYPE.FIXED:
    default:
      return StatusPattern.FULL;
  }
};

export const statusLabels = {
  Pending: 'DISABILITY_BENEFIT_STATUS.PENDING',
  Approved: 'DISABILITY_BENEFIT_STATUS.APPROVED',
  Denied: 'DISABILITY_BENEFIT_STATUS.DENIED',
  Decided: 'DISABILITY_BENEFIT_STATUS.DECIDED',
  Closed: 'DISABILITY_BENEFIT_STATUS.CLOSED',
  Unknown: 'DISABILITY_BENEFIT_STATUS.UNKNOWN',
};
