/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';
import {
  CardBodyWithStatus,
  ContentLayout,
  PropertyItem,
  FormattedDate,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { useIntl } from 'react-intl';

import {
  ClaimDisabilityBenefit,
  WageReplacementStatus
} from '../../../../../shared';
import { WageReplacementStatusLabel } from '../../wage-replacement-status-label';
import {
  getStatusTypeByStatus,
  convertStatusToDisplayStatus,
  statusLabels
} from '../../wage-replacement.util';
import { LatestPayments } from '../../../latest-payments';

import styles from './claim-disability-benefit-card.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  text: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export type ClaimDisabilityBenefitCardBodyProps = {
  benefit: ClaimDisabilityBenefit;
};
export const ClaimDisabilityBenefitCardBody: React.FC<ClaimDisabilityBenefitCardBodyProps> = ({
  benefit: {
    benefitId,
    claimId,
    creationDate,
    status,
    stageName,
    disabilityBenefit,
    benefitCaseType
  }
}) => {
  const intl = useIntl();
  const themedStyles = useThemedStyles();
  const displayStatus = convertStatusToDisplayStatus(
    status as WageReplacementStatus,
    stageName
  );

  return (
    <CardBodyWithStatus
      type={getStatusTypeByStatus(displayStatus)}
      aria-label={intl.formatMessage({
        id: statusLabels[displayStatus]
      })}
    >
      <ContentLayout direction="column" noPadding={true}>
        <Row align="top">
          <Col xs={24} sm={12} md={6}>
            <PropertyItem
              className={styles.property}
              label={
                <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE" />
              }
            >
              {!_isEmpty(disabilityBenefit) ? (
                <FormattedDate
                  className={themedStyles.text}
                  date={disabilityBenefit!.benefitStartDate}
                />
              ) : !!creationDate ? (
                <FormattedDate
                  className={themedStyles.text}
                  date={creationDate}
                />
              ) : (
                ''
              )}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <PropertyItem
              className={styles.property}
              label={
                <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH" />
              }
            >
              {!_isEmpty(disabilityBenefit) && (
                <FormattedDate
                  className={themedStyles.text}
                  date={disabilityBenefit!.approvedThroughDate}
                />
              )}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <PropertyItem
              className={styles.property}
              label={
                <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.EXPECTED_RESOLUTION" />
              }
            >
              {!_isEmpty(disabilityBenefit) && (
                <FormattedDate
                  className={themedStyles.text}
                  date={disabilityBenefit!.expectedResolutionDate}
                />
              )}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <WageReplacementStatusLabel
              status={status as WageReplacementStatus}
              stageName={stageName}
            />
          </Col>
        </Row>

        <LatestPayments
          benefitId={(benefitId as unknown) as string}
          claimId={claimId}
          benefitCaseType={benefitCaseType}
        />
      </ContentLayout>
    </CardBodyWithStatus>
  );
};
