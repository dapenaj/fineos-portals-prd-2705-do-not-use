/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CollapsibleCard } from 'fineos-common';

import {
  ClaimDisabilityBenefit,
  PaidLeaveBenefit,
  isClaimDisabilityBenefit,
  isClaimDisabilityBenefitGroup,
  BenefitGroup
} from '../../../../shared';

import {
  ClaimDisabilityBenefitCardHeader,
  ClaimDisabilityBenefitCardBody
} from './claim-disability-benefit-card';
import {
  PaidLeaveBenefitCardHeader,
  PaidLeaveBenefitCardBody
} from './paid-leave-benefit-card';
import styles from './wage-replacement-cards.module.scss';

enum BENEFIT_TYPE_KEY_PREFIX {
  CLAIM_DISABILITY_BENEFIT = 'cbd',
  PAID_LEAVE_BENEFIT = 'plb'
}

export type WageReplacementCardsProps = {
  benefitGroup: BenefitGroup<ClaimDisabilityBenefit | PaidLeaveBenefit>[];
};

export const WageReplacementCards: React.FC<WageReplacementCardsProps> = ({
  benefitGroup
}) => (
  <div className={styles.wrapper} data-test-el="wage-replacement-cards">
    {benefitGroup.map(group => {
      return (
        <CollapsibleCard
          key={`${
            isClaimDisabilityBenefitGroup(group)
              ? BENEFIT_TYPE_KEY_PREFIX.CLAIM_DISABILITY_BENEFIT
              : BENEFIT_TYPE_KEY_PREFIX.PAID_LEAVE_BENEFIT
          }-${group.benefitId}`}
          title={({ isCollapsed }: { isCollapsed: boolean }) =>
            // Render the correct card header depending on the benefit
            isClaimDisabilityBenefitGroup(group) ? (
              <ClaimDisabilityBenefitCardHeader
                isCollapsed={isCollapsed}
                benefitCaseType={group.name}
                status={group.status}
                stageName={group.stageName}
                benefitHandler={group.benefitHandler}
                benefitHandlerPhoneNo={group.benefitHandlerPhoneNo}
                count={group.benefits.length}
              />
            ) : (
              <PaidLeaveBenefitCardHeader
                isCollapsed={isCollapsed}
                leavePlanName={group.name}
                status={group.status}
                stageName={group.stageName}
                benefitHandler={group.benefitHandler}
                benefitHandlerPhoneNo={group.benefitHandlerPhoneNo}
                count={group.benefits.length}
              />
            )
          }
        >
          {group.benefits.map(benefit =>
            isClaimDisabilityBenefit(benefit) ? (
              <ClaimDisabilityBenefitCardBody
                key={`${BENEFIT_TYPE_KEY_PREFIX.CLAIM_DISABILITY_BENEFIT}-${benefit.benefitId}`}
                benefit={benefit as ClaimDisabilityBenefit}
              />
            ) : (
              <PaidLeaveBenefitCardBody
                key={`${BENEFIT_TYPE_KEY_PREFIX.PAID_LEAVE_BENEFIT}-${benefit.benefitId}`}
                benefit={benefit as PaidLeaveBenefit}
              />
            )
          )}
        </CollapsibleCard>
      );
    })}
  </div>
);
