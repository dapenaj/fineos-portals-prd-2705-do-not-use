/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import {
  CardBodyWithStatus,
  ContentLayout,
  PropertyItem,
  ConfigurableText,
  FormattedDate,
  FormattedMessage,
  Icon,
  Tooltip
} from 'fineos-common';
import { useIntl } from 'react-intl';

import {
  PaidLeaveBenefit,
  WageReplacementStatus,
  ABSENCE_PERIOD_TYPE
} from '../../../../../shared';
import { WageReplacementStatusLabel } from '../../wage-replacement-status-label';
import {
  getStatusTypeByStatus,
  convertStatusToDisplayStatus,
  getStatusPatternForAbsencePeriodType
} from '../../wage-replacement.util';
import { LatestPayments } from '../../../latest-payments';

import styles from './paid-leave-benefit-card.module.scss';

export type PaidLeaveBenefitCardBodyProps = {
  benefit: PaidLeaveBenefit;
};
export const PaidLeaveBenefitCardBody: React.FC<PaidLeaveBenefitCardBodyProps> = ({
  benefit: {
    benefitId,
    benefitCaseType,
    paidLeaveCaseId,
    creationDate,
    status,
    stageName,
    reasonName,
    type,
    disabilityBenefit
  }
}) => {
  const intl = useIntl();
  const pattern = getStatusPatternForAbsencePeriodType(
    type as ABSENCE_PERIOD_TYPE
  );

  return (
    <CardBodyWithStatus
      type={getStatusTypeByStatus(
        convertStatusToDisplayStatus(status as WageReplacementStatus, stageName)
      )}
      pattern={pattern}
      aria-label={intl.formatMessage({
        id: pattern
      })}
    >
      <ContentLayout direction="column" noPadding={true}>
        <Row align="top">
          <Col xs={24} sm={12} md={6}>
            <PropertyItem
              className={styles.property}
              label={
                <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.ABSENCE_REASON" />
              }
            >
              {reasonName}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <PropertyItem
              className={styles.property}
              label={
                type === ABSENCE_PERIOD_TYPE.INTERMITTENT ? (
                  <>
                    <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.ENTITLEMENT_BEGINS" />
                    <Tooltip
                      tooltipContent={
                        <ConfigurableText
                          id={
                            'NOTIFICATIONS.WAGE_REPLACEMENT.ENTITLEMENTS_TOOLTIP'
                          }
                        />
                      }
                    >
                      <Icon
                        icon={faQuestionCircle}
                        iconLabel={
                          <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.QUESTION_MARK_ICON_LABEL" />
                        }
                        data-test-el="paid-leave-tooltip"
                      />
                    </Tooltip>
                  </>
                ) : (
                  <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE" />
                )
              }
            >
              {!_isEmpty(disabilityBenefit) ? (
                <FormattedDate date={disabilityBenefit!.benefitStartDate} />
              ) : !!creationDate ? (
                <FormattedDate date={creationDate} />
              ) : (
                ''
              )}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <PropertyItem
              className={styles.property}
              label={
                <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH" />
              }
            >
              {!_isEmpty(disabilityBenefit) && (
                <FormattedDate date={disabilityBenefit!.approvedThroughDate} />
              )}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <WageReplacementStatusLabel
              status={status as WageReplacementStatus}
              stageName={stageName}
            />
          </Col>
        </Row>

        <LatestPayments
          benefitId={(benefitId as unknown) as string}
          claimId={paidLeaveCaseId}
          benefitCaseType={benefitCaseType}
        />
      </ContentLayout>
    </CardBodyWithStatus>
  );
};
