/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Row, Col } from 'antd';
import {
  ContentLayout,
  createThemedStyles,
  textStyleToCss,
  FallbackValue,
  HandlerInfo
} from 'fineos-common';
import { useIntl } from 'react-intl';

import { WageReplacementStatus } from '../../../../../shared';
import { parseBenefitName } from '../../../benefit.util';
import { WageReplacementStatusLabel } from '../../wage-replacement-status-label';

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export type ClaimDisabilityBenefitCardHeaderProps = {
  benefitCaseType: string;
  status: string;
  stageName: string;
  benefitHandler: string;
  benefitHandlerPhoneNo: string;
  isCollapsed: boolean;
  count: number;
};
export const ClaimDisabilityBenefitCardHeader: React.FC<ClaimDisabilityBenefitCardHeaderProps> = ({
  benefitCaseType,
  status,
  stageName,
  benefitHandler,
  benefitHandlerPhoneNo,
  isCollapsed,
  count
}) => {
  const themedStyles = useThemedStyles();
  const intl = useIntl();

  return (
    <div data-test-el="disability-benefit-header">
      {isCollapsed ? (
        <>
          <div
            className={themedStyles.title}
            data-test-el="disability-benefit-header-name"
          >
            <FallbackValue value={parseBenefitName(benefitCaseType, intl)} />
          </div>
          <WageReplacementStatusLabel
            status={status as WageReplacementStatus}
            stageName={stageName}
            count={count}
          />
        </>
      ) : (
        <ContentLayout direction="column" noPadding={true}>
          <Row align="top">
            <Col xs={24} sm={12}>
              <div
                className={themedStyles.title}
                data-test-el="disability-benefit-header-name"
              >
                <FallbackValue
                  value={parseBenefitName(benefitCaseType, intl)}
                />
              </div>
            </Col>
            <Col xs={24} sm={12}>
              <HandlerInfo
                name={benefitHandler}
                phoneNumber={benefitHandlerPhoneNo}
                data-test-el="disability-benefit-header-properties"
              />
            </Col>
          </Row>
        </ContentLayout>
      )}
    </div>
  );
};
