/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import {
  Panel,
  ContentRenderer,
  ViewNotificationPermissions,
  EmptyState,
  withPermissions,
  FormattedMessage
} from 'fineos-common';

import { ClaimDisabilityBenefit, PaidLeaveBenefit } from '../../../shared';

import { WageReplacementHeader } from './wage-replacement-header';
import { WageReplacementCards } from './wage-replacement-cards';
import { groupByLeavePlanAndSort } from './wage-replacement.util';
import styles from './wage-replacement.module.scss';

export type WageReplacementProps = {
  claimBenefits: ClaimDisabilityBenefit[] | null;
  paidLeaveBenefits: PaidLeaveBenefit[] | null;
  isLoading: boolean;
};

export const WageReplacementComponent: React.FC<WageReplacementProps> = ({
  claimBenefits,
  paidLeaveBenefits,
  isLoading
}) => {
  const groupedClaimBenefits = groupByLeavePlanAndSort(claimBenefits || []);
  const groupedPaidLeaveBenefits = groupByLeavePlanAndSort(
    paidLeaveBenefits || []
  );
  const isEmpty =
    _isEmpty(groupedClaimBenefits) && _isEmpty(groupedPaidLeaveBenefits);

  return (
    <ContentRenderer
      isLoading={isLoading}
      isEmpty={isEmpty}
      emptyContent={
        <EmptyState withBorder={true}>
          <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.EMPTY" />
        </EmptyState>
      }
    >
      {!_isEmpty(groupedClaimBenefits) && (
        <Panel
          data-test-el="wage-replacement"
          role="region"
          aria-label={
            <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.PANEL_TITLE" />
          }
          header={<WageReplacementHeader />}
          className={styles.wrapper}
        >
          <WageReplacementCards benefitGroup={groupedClaimBenefits} />
        </Panel>
      )}

      {!_isEmpty(groupedPaidLeaveBenefits) && (
        <Panel
          data-test-el="wage-replacement"
          role="region"
          aria-label={
            <FormattedMessage id="NOTIFICATIONS.WAGE_REPLACEMENT.PANEL_TITLE" />
          }
          header={<WageReplacementHeader />}
          className={styles.wrapper}
        >
          <WageReplacementCards benefitGroup={groupedPaidLeaveBenefits} />
        </Panel>
      )}
    </ContentRenderer>
  );
};

export const WageReplacement = withPermissions(
  ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST
)(WageReplacementComponent);
