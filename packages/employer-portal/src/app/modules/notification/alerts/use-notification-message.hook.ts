/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { MessagesPermissions, usePermissions, useAsync } from 'fineos-common';
import { CaseMessage, NewMessage } from 'fineos-js-api-client';

import { fetchMessagesByCaseId, addMessage } from '../../../shared';

export const useNotificationMessage = (caseId: string) => {
  const hasMessagesPermission = usePermissions([
    MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
    MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
    MessagesPermissions.ADD_CASE_WEB_MESSAGE,
    MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
  ]);

  const {
    state: {
      value: response,
      isPending: isLoadingMessages,
      error: fetchMessagesError,
      status
    },
    changeAsyncValue,
    executeAgain
  } = useAsync(fetchMessagesByCaseId, [caseId], {
    defaultValue: [],
    shouldExecute: () => hasMessagesPermission
  });

  return {
    messages: response,
    isLoadingMessages,
    fetchMessagesError,
    status,
    updateMessage: (message: CaseMessage) =>
      changeAsyncValue(
        response.map((localMessage: CaseMessage) =>
          localMessage.id === message.id ? message : localMessage
        )
      ),
    addMessage: async (message: NewMessage) => {
      await addMessage({ caseId, ...message });
      executeAgain();
    }
  };
};
