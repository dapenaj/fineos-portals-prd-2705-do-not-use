/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { useFormikContext } from 'formik';
import {
  createThemedStyles,
  textStyleToCss,
  pxToRem,
  FormattedDate,
  Drawer,
  DrawerWidth,
  ConfigurableText,
  OverlayPopconfirm
} from 'fineos-common';
import classNames from 'classnames';
import { CaseMessage } from 'fineos-js-api-client';

import { MessageDetails } from '../../../../../shared';
import { MessageIcon } from '../../../../messages/message-icon';
import { NewMessageFormValues, INPUT_MIN_LENGTH } from '../new-message-form';

import styles from './reply-message.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  drawerMessageSubject: {
    ...textStyleToCss(theme.typography.panelTitle),
    maxHeight: pxToRem(theme.typography.panelTitle.lineHeight! * 2)
  },
  drawerMessageContactDateTime: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  drawerTitleIcon: {
    color: theme.colorSchema.neutral5
  }
}));

type ReplyMessageProps = {
  currentMessage: CaseMessage | undefined;
  onClose: () => void;
};

export const ReplyMessage: React.FC<ReplyMessageProps> = ({
  currentMessage,
  onClose
}) => {
  const themedStyles = useThemedStyles();
  const { values } = useFormikContext();
  const [isOverlayPopconfirmVisible, setIsOverlayPopconfirmVisible] = useState(
    false
  );
  const isFormValid = Object.values(values as NewMessageFormValues).some(
    value => value.length >= INPUT_MIN_LENGTH
  );

  const onConfirmationClose = () => {
    if (!isFormValid) {
      onClose();
      return;
    }
    setIsOverlayPopconfirmVisible(true);
  };

  const getHorizontalOffset = () => +DrawerWidth.MEDIUM.split('px')[0] / 2;

  return (
    <Drawer
      hideOnEsc={!isFormValid}
      ariaLabelId="NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE"
      className={styles.drawer}
      bodyClassName={styles.drawerBody}
      width={DrawerWidth.MEDIUM}
      isVisible={true}
      data-test-el="reply-message-drawer"
      onClose={onConfirmationClose}
      title={
        currentMessage && (
          <div className={styles.drawerHeader}>
            <div className={styles.drawerTitleWrapper}>
              <MessageIcon
                message={currentMessage}
                className={classNames(
                  styles.drawerTitleIcon,
                  themedStyles.drawerTitleIcon
                )}
              />
              <div
                className={classNames(
                  styles.drawerMessageSubject,
                  themedStyles.drawerMessageSubject
                )}
              >
                {currentMessage.subject}
              </div>
            </div>
            <FormattedDate
              className={classNames(
                styles.drawerMessageContactDateTime,
                themedStyles.drawerMessageContactDateTime
              )}
              date={currentMessage.contactDateTime}
            />
          </div>
        )
      }
    >
      <MessageDetails currentMessage={currentMessage} />
      <OverlayPopconfirm
        horizontalOffset={getHorizontalOffset()}
        ariaLabelId="USER_ACTIONS.CLOSE_WINDOW"
        onCancel={() => setIsOverlayPopconfirmVisible(false)}
        onConfirm={onClose}
        visible={isOverlayPopconfirmVisible}
        popconfirmMessage={<ConfigurableText id="USER_ACTIONS.CLOSE_WINDOW" />}
      />
    </Drawer>
  );
};
