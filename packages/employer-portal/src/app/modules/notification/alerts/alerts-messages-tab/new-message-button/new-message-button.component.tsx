/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import classNames from 'classnames';
import {
  Button,
  ButtonType,
  FormattedMessage,
  createThemedStyles
} from 'fineos-common';
import { NewMessage } from 'fineos-js-api-client';

import { NewMessageForm, useNewMessageFormik } from '../new-message-form';
import { FormikWithResponseHandling } from '../../../../../shared';

import styles from './new-message-button.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  buttonWrapper: {
    backgroundColor: theme.colorSchema.neutral1
  }
}));

type NewMessageButtonProps = {
  onMessageAdd: (message: NewMessage) => Promise<void>;
};

export const NewMessageButton: React.FC<NewMessageButtonProps> = ({
  onMessageAdd
}) => {
  const themedStyles = useThemedStyles();
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);
  const onClose = () => {
    setIsDrawerVisible(false);
    const trigger = document.querySelector(
      '[data-test-el="NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE"]'
    ) as HTMLButtonElement;
    trigger?.focus();
  };
  const formikProps = useNewMessageFormik({
    onMessageAdd,
    onClose
  });
  return (
    <div
      className={classNames(themedStyles.buttonWrapper, styles.buttonWrapper)}
    >
      <FormattedMessage
        as={Button}
        onClick={() => setIsDrawerVisible(true)}
        className={styles.button}
        buttonType={ButtonType.PRIMARY}
        id="NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE"
      />
      {isDrawerVisible && (
        <FormikWithResponseHandling {...formikProps}>
          <NewMessageForm onMessageAdd={onMessageAdd} onClose={onClose} />
        </FormikWithResponseHandling>
      )}
    </div>
  );
};
