/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect } from 'react';
import {
  EmptyState,
  Tabs,
  ContentRenderer,
  FormattedMessage
} from 'fineos-common';

import { useMessageCounter } from '../../../../shared';
import { useNotificationMessage } from '../use-notification-message.hook';

import { NewMessageButton } from './new-message-button';
import { CaseMessageList } from './case-message-list';
import styles from './alerts-messages-tab.module.scss';

const { TabPane } = Tabs;

type AlertsMessagesTabProps = React.ComponentProps<typeof TabPane> & {
  caseId: string;
};

export const AlertsMessagesTab: React.FC<AlertsMessagesTabProps> = ({
  caseId,
  ...props
}) => {
  const {
    messages,
    isLoadingMessages,
    fetchMessagesError,
    status,
    updateMessage,
    addMessage
  } = useNotificationMessage(caseId);
  const { syncMessages } = useMessageCounter();

  useEffect(() => {
    syncMessages(messages);
    // eslint-disable-next-line
  }, [messages]);

  return (
    <TabPane {...props} hasNoMargin={true} data-test-el="alerts-messages-tab">
      <div className={styles.mainWrapper}>
        <div className={styles.contentWrapper}>
          <ContentRenderer
            isEmpty={!messages.length}
            isLoading={isLoadingMessages}
            error={fetchMessagesError}
            shouldNotDestroyOnLoading={true}
            emptyContent={
              <EmptyState isSmall={true}>
                <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.EMPTY" />
              </EmptyState>
            }
          >
            <CaseMessageList
              messages={messages}
              onMessageUpdate={updateMessage}
              onMessageAdd={addMessage}
              status={status}
            />
          </ContentRenderer>
        </div>
        <NewMessageButton onMessageAdd={addMessage} />
      </div>
    </TabPane>
  );
};
