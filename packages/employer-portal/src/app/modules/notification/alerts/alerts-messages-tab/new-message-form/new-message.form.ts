/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';

export const INPUT_MIN_LENGTH = 5;
export const MESSAGE_MAX_LENGTH = 4000;
export const SUBJECT_MAX_LENGTH = 196;

export type NewMessageFormValues = {
  subject: string;
  message: string;
};

export const initialFormValues: NewMessageFormValues = {
  subject: '',
  message: ''
};

export const formValidation = Yup.object({
  subject: Yup.string()
    .required('NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MIN_SUBJECT_LENGTH')
    .min(
      INPUT_MIN_LENGTH,
      'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MIN_SUBJECT_LENGTH'
    )
    .max(
      SUBJECT_MAX_LENGTH,
      'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MAX_SUBJECT_LENGTH'
    )
    .trim(),
  message: Yup.string()
    .required('NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MIN_LENGTH')
    .min(
      INPUT_MIN_LENGTH,
      'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MIN_LENGTH'
    )
    .max(
      MESSAGE_MAX_LENGTH,
      'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MAX_LENGTH'
    )
});
