/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useRef, useState } from 'react';
import {
  Button,
  ButtonType,
  createThemedStyles,
  FormattedDate,
  List,
  pxToRem,
  ScreenReaderMessage,
  textStyleToCss
} from 'fineos-common';
import classNames from 'classnames';
import { CaseMessage, NewMessage } from 'fineos-js-api-client';

import {
  editMessage,
  FormikWithResponseHandling,
  MessageOptions,
  useMessageDetailsFormik
} from '../../../../../shared';
import { MessageIcon } from '../../../../messages/message-icon';
import { ReplyMessage } from '../reply-message';

import styles from './case-message-list.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  messageItemMeta: {
    ...textStyleToCss(theme.typography.baseText),
    color: theme.colorSchema.neutral8
  },
  messageReadIcon: {
    color: theme.colorSchema.neutral5
  },
  messageUnreadIcon: {
    color: theme.colorSchema.primaryColor
  },
  messageUnreadItem: {
    borderLeftColor: theme.colorSchema.primaryColor
  },
  messageSubject: {
    ...textStyleToCss(theme.typography.baseTextSemiBold),
    color: theme.colorSchema.primaryAction,
    // fallback for elder browser - just limit max-height
    maxHeight: pxToRem(theme.typography.baseTextSemiBold.lineHeight! * 1)
  },
  messageContactDateTime: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  },
  messageNarrative: {
    ...textStyleToCss(theme.typography.smallTextSemiBold),
    color: theme.colorSchema.neutral8,
    maxHeight: pxToRem(theme.typography.smallTextSemiBold.lineHeight! * 2)
  }
}));

type CaseMessageListProps = {
  messages: CaseMessage[];
  isItemBorderVisible?: boolean;
  onMessageUpdate: (message: CaseMessage) => void;
  onMessageAdd: (message: NewMessage) => Promise<void>;
  status: string;
};

export const CaseMessageList: React.FC<CaseMessageListProps> = ({
  messages,
  isItemBorderVisible = true,
  onMessageUpdate,
  onMessageAdd,
  status
}) => {
  const themedStyles = useThemedStyles();
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);
  const [currentMessage, setCurrentMessage] = useState<
    CaseMessage | undefined
  >();
  const [initialList, setInitialList] = useState<CaseMessage[] | null>(null);
  const [isMessageAnimated, setIsMessageAnimated] = useState(false);
  const wrapperRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (status === 'SUCCESS' && initialList === null) {
      setInitialList(messages);
      return;
    }
    if (initialList && messages.length > initialList.length) {
      const el = wrapperRef.current;
      el?.scrollIntoView({
        behavior: 'auto',
        block: 'nearest',
        inline: 'start'
      });
      setIsMessageAnimated(true);
      setInitialList(messages);
    }
  }, [status, initialList, messages]);

  const onMessageClick = async (message: CaseMessage) => {
    setIsDrawerVisible(true);
    const readMessage = { ...message, readByGroupClient: true };
    setCurrentMessage(readMessage);
    onMessageUpdate(readMessage);
    await editMessage(readMessage);
  };

  const onDrawerClose = () => {
    setIsDrawerVisible(false);
    if (currentMessage) {
      const trigger = document.getElementById(currentMessage.id);
      trigger?.focus();
    }
  };

  const formikProps = useMessageDetailsFormik({
    currentMessage,
    onMessageAdd,
    onClose: onDrawerClose
  });

  return (
    <div ref={wrapperRef}>
      <List
        dataSource={messages}
        renderItem={(message: CaseMessage) => (
          <List.Item
            key={message.id}
            className={classNames(styles.messageItem, {
              [themedStyles.messageUnreadItem]:
                !message.readByGroupClient && isItemBorderVisible,
              [styles.animation]: isMessageAnimated
            })}
            data-test-el="message-list-item"
            onClick={() => onMessageClick(message)}
            onAnimationEnd={() => {
              setIsMessageAnimated(false);
            }}
          >
            <List.Item.Meta
              className={themedStyles.messageItemMeta}
              avatar={
                <MessageIcon
                  message={message}
                  className={classNames(
                    styles.messageIcon,
                    message.readByGroupClient
                      ? themedStyles.messageReadIcon
                      : themedStyles.messageUnreadIcon
                  )}
                  data-test-el="message-sender-icon"
                />
              }
              description={
                <>
                  {!message.readByGroupClient && (
                    <ScreenReaderMessage id="NOTIFICATIONS.ALERTS.MESSAGES.UNREAD_MESSAGE" />
                  )}
                  <Button
                    id={message.id}
                    title={message.subject}
                    className={styles.messageSubjectButton}
                    buttonType={ButtonType.LINK}
                    onClick={() => onMessageClick(message)}
                  >
                    <strong
                      className={classNames(
                        styles.messageSubject,
                        themedStyles.messageSubject
                      )}
                      data-test-el="message-subject"
                    >
                      {message.subject}
                    </strong>
                  </Button>
                  <div
                    className={classNames(
                      styles.messageContactDateTime,
                      themedStyles.messageContactDateTime
                    )}
                  >
                    <FormattedDate
                      data-test-el="message-date"
                      date={message.contactDateTime}
                    />
                  </div>
                  <div
                    className={classNames(
                      styles.messageNarrative,
                      themedStyles.messageNarrative
                    )}
                    data-test-el="message-narrative"
                  >
                    {message.narrative}
                  </div>
                </>
              }
            />
            <MessageOptions message={message} onMessageEdit={onMessageUpdate} />
          </List.Item>
        )}
      />
      {isDrawerVisible && (
        <FormikWithResponseHandling {...formikProps}>
          <ReplyMessage
            currentMessage={currentMessage}
            onClose={onDrawerClose}
          />
        </FormikWithResponseHandling>
      )}
    </div>
  );
};
