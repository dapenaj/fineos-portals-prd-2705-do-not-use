/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Field, useFormikContext } from 'formik';
import {
  AsteriskInfo,
  FormButton,
  ButtonType,
  FormGroup,
  Drawer,
  DrawerWidth,
  Form,
  TextArea,
  TextInput,
  FormattedMessage,
  ConfigurableText,
  OverlayPopconfirm,
  Popconfirm,
  useButtonTypeStyling,
  DrawerFooter
} from 'fineos-common';
import { NewMessage } from 'fineos-js-api-client';

import styles from './new-message-form.module.scss';
import {
  NewMessageFormValues,
  INPUT_MIN_LENGTH,
  MESSAGE_MAX_LENGTH
} from './new-message.form';

export type NewMessageFormProps = {
  onMessageAdd: (message: NewMessage) => Promise<void>;
  onClose: () => void;
};

export const NewMessageForm: React.FC<NewMessageFormProps> = ({ onClose }) => {
  const [isOverlayPopconfirmVisible, setIsOverlayPopconfirmVisible] = useState(
    false
  );
  const { values } = useFormikContext();
  const isFormValid = Object.values(values as NewMessageFormValues).some(
    value => value.length >= INPUT_MIN_LENGTH
  );

  const onConfirmationClose = () => {
    if (!isFormValid) {
      onClose();
      return;
    }
    setIsOverlayPopconfirmVisible(true);
  };

  const cancelButtonStyles = useButtonTypeStyling(ButtonType.LINK);

  const getHorizontalOffset = () => +DrawerWidth.MEDIUM.split('px')[0] / 2;

  return (
    <Drawer
      hideOnEsc={!isFormValid}
      ariaLabelId="NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE"
      width={DrawerWidth.MEDIUM}
      onClose={onConfirmationClose}
      isVisible={true}
      data-test-el="new-message-drawer"
      title={
        <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE" />
      }
    >
      <Form className={styles.form} data-test-el="new-message-form">
        <div>
          <AsteriskInfo />
          <FormGroup
            isRequired={true}
            label={
              <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL" />
            }
            name="subject"
          >
            <Field
              name="subject"
              data-test-el="subject-field"
              component={TextInput}
            />
          </FormGroup>
          <FormGroup
            className={styles.textAreaFormGroup}
            isRequired={true}
            label={
              <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL" />
            }
            name="message"
            counterMaxLength={MESSAGE_MAX_LENGTH}
            immediateErrorMessages={[
              'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MAX_LENGTH'
            ]}
          >
            <Field
              name="message"
              data-test-el="message-field"
              component={TextArea}
              className={styles.textArea}
              onKeyPress={(e: React.KeyboardEvent) => e.stopPropagation()}
            />
          </FormGroup>
        </div>
      </Form>
      <OverlayPopconfirm
        horizontalOffset={getHorizontalOffset()}
        ariaLabelId="NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING"
        onCancel={() => setIsOverlayPopconfirmVisible(false)}
        onConfirm={onClose}
        visible={isOverlayPopconfirmVisible}
        popconfirmMessage={
          <ConfigurableText id="NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING" />
        }
      />
      <DrawerFooter>
        <FormattedMessage
          as={FormButton}
          htmlType="submit"
          id="NOTIFICATIONS.ALERTS.MESSAGES.SEND"
        />
        {isFormValid ? (
          <Popconfirm
            trigger={
              <FormattedMessage
                className={cancelButtonStyles}
                id="FINEOS_COMMON.GENERAL.CANCEL"
              />
            }
            ariaLabelId="NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING"
            popconfirmMessage={
              <ConfigurableText id="NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING" />
            }
            onConfirm={onClose}
          />
        ) : (
          <FormattedMessage
            as={FormButton}
            buttonType={ButtonType.LINK}
            htmlType="reset"
            onClick={onClose}
            id="FINEOS_COMMON.GENERAL.CANCEL"
          />
        )}
      </DrawerFooter>
    </Drawer>
  );
};
