/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CustomerSummary } from 'fineos-js-api-client';
import {
  Panel,
  PanelHeader,
  Tabs,
  ViewNotificationPermissions,
  MessagesPermissions,
  anyFrom,
  usePermissions,
  withPermissions,
  TextBadge,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Icon,
  primaryActionDark2Color,
  getFocusedElementShadow,
  useAsync
} from 'fineos-common';
import { CaseMessage, OutstandingInformation } from 'fineos-js-api-client';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import styles from './alerts.module.scss';
import { AlertsDocumentsTab } from './alerts-documents-tab';
import { AlertsOutstandingTab } from './alerts-outstanding-tab';
import { AlertsMessagesTab } from './alerts-messages-tab';
import { useOutstandingInfo } from './use-outstanding-info.hook';
import { useNotificationMessage } from './use-notification-message.hook';
import { fetchDocuments } from './alerts-documents-tab/alerts-documents-tab.api';

type AlertsProps = {
  caseId: string;
  customer: CustomerSummary;
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  alertsTab: {
    '& .ant-tabs-tab': {
      color: theme.colorSchema.neutral8,
      '&:hover': {
        color: theme.colorSchema.primaryAction
      },
      '& .ant-tabs-tab-btn:focus': {
        color: primaryActionDark2Color(theme.colorSchema)
      }
    },
    '& .ant-tabs-tab-btn, .ant-tabs-tabpane': {
      '&:focus': {
        borderColor: theme.colorSchema.primaryAction,
        boxShadow: getFocusedElementShadow({
          color: theme.colorSchema.primaryAction
        })
      }
    }
  }
}));

export const Alerts = withPermissions(
  anyFrom(
    ViewNotificationPermissions.VIEW_CASES_DOCUMENTS_LIST,
    ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST,
    MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
    MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
    MessagesPermissions.ADD_CASE_WEB_MESSAGE,
    MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
  )
)(({ caseId, customer }: AlertsProps) => {
  const hasDocumentsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_CASES_DOCUMENTS_LIST
  );
  const hasOutstandingAccess = usePermissions(
    ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
  );
  const hasWebMessagesAccess = usePermissions([
    MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
    MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
    MessagesPermissions.ADD_CASE_WEB_MESSAGE,
    MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
  ]);

  const outstandingState = useOutstandingInfo(caseId);
  const activeOutstandingActions = outstandingState.outstandingInfos.filter(
    (outstanding: OutstandingInformation) => !outstanding.infoReceived
  );

  const { messages } = useNotificationMessage(caseId);
  const unreadMessagesLength = messages.filter(
    (message: CaseMessage) => !message.readByGroupClient
  ).length;

  const {
    state: {
      value: documents,
      isPending: isLoadingDocuments,
      error: fetchDocumentsError
    },
    executeAgain: refreshDocuments
  } = useAsync(fetchDocuments, [caseId], {
    defaultValue: [],
    shouldExecute: () => hasDocumentsPermission
  });

  const themedStyles = useThemedStyles();
  return (
    <Panel
      role="complementary"
      header={
        <PanelHeader className={themedStyles.title}>
          <Icon
            icon={faBell}
            iconLabel={
              <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.BELL_ICON_LABEL" />
            }
          />{' '}
          <FormattedMessage id="NOTIFICATIONS.ALERTS.TITLE" />
        </PanelHeader>
      }
    >
      <Tabs className={classNames(themedStyles.alertsTab, styles.alertsTabs)}>
        {hasOutstandingAccess && (
          <AlertsOutstandingTab
            key="outstanding"
            tab={
              activeOutstandingActions.length ? (
                <TextBadge
                  label={activeOutstandingActions.length}
                  data-test-el="outstanding-tab-with-label"
                >
                  <FormattedMessage id="NOTIFICATIONS.ALERTS.OUTSTANDING.TITLE" />
                </TextBadge>
              ) : (
                <FormattedMessage id="NOTIFICATIONS.ALERTS.OUTSTANDING.TITLE" />
              )
            }
            className={styles.tabPane}
            caseId={caseId}
            handleRefreshOutstandingInfo={() => {
              outstandingState.executeAgain();
              refreshDocuments();
            }}
            customer={customer}
            {...outstandingState}
          />
        )}

        {hasWebMessagesAccess && (
          <AlertsMessagesTab
            key="messages"
            tab={
              unreadMessagesLength ? (
                <TextBadge
                  label={
                    unreadMessagesLength > 99 ? '+99' : unreadMessagesLength
                  }
                  data-test-el="messages-tab-with-label"
                >
                  <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.TITLE" />
                </TextBadge>
              ) : (
                <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.TITLE" />
              )
            }
            className={styles.tabPane}
            caseId={caseId}
          />
        )}

        {hasDocumentsPermission && (
          <AlertsDocumentsTab
            key="documents"
            tab={<FormattedMessage id="NOTIFICATIONS.ALERTS.DOCUMENTS.TITLE" />}
            caseId={caseId}
            className={styles.tabPane}
            documents={documents}
            isLoadingDocuments={isLoadingDocuments}
            fetchDocumentsError={fetchDocumentsError}
            refreshDocuments={refreshDocuments}
          />
        )}
      </Tabs>
    </Panel>
  );
});
