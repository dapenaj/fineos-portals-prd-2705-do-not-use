/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import {
  Button,
  SomethingWentWrongModal,
  useAsync,
  saveBase64File,
  ButtonType,
  usePermissions,
  ManageNotificationPermissions,
  ApiResponseType,
  FormattedMessage
} from 'fineos-common';
import { CaseDocument } from 'fineos-js-api-client';

import { markDocumentAsRead } from '../../../../../shared';

import { downloadDocument } from './download-document-link.api';
import styles from './download-document-link.module.scss';

type DownloadDocumentLinkProps = {
  document: CaseDocument;
  className: string;
  documentMarkedAsRead: () => void;
};

export const DownloadDocumentLink: React.FC<DownloadDocumentLinkProps> = ({
  children,
  document: { caseId, documentId, originalFilename, isRead },
  className,
  documentMarkedAsRead
}) => {
  const hasMarkReadPermission = usePermissions(
    ManageNotificationPermissions.MARK_READ_CASE_DOCUMENT
  );

  const [downloadDocumentId, setDownloadDocumentId] = useState(Number.NaN);

  const {
    state: { value: base64Document, error }
  } = useAsync(downloadDocument, [caseId, downloadDocumentId], {
    shouldExecute: (id: string, docId: number) => !isNaN(docId),
    noGlobalErrorHandlingFor: [400, 404, 500]
  });

  useEffect(() => {
    async function handleDownloadedDocument() {
      if (hasMarkReadPermission && !isRead) {
        await markDocumentAsRead(caseId, documentId);

        documentMarkedAsRead();
      }
    }

    if (base64Document) {
      const {
        fileName,
        fileExtension,
        base64EncodedFileContents,
        contentType
      } = base64Document;

      saveBase64File({
        fileName: fileName ? `${fileName}.${fileExtension}` : originalFilename,
        data: base64EncodedFileContents,
        contentType
      });

      handleDownloadedDocument();
    } // eslint-disable-next-line
  }, [base64Document]);

  return (
    <>
      <Button
        buttonType={ButtonType.LINK}
        onClick={() => setDownloadDocumentId(documentId)}
        className={classNames(className, styles.link)}
        data-test-el="document-download-btn"
      >
        {children}
      </Button>

      <SomethingWentWrongModal
        response={error}
        type={ApiResponseType.UPLOAD_FAIL}
      >
        <FormattedMessage id="NOTIFICATIONS.ALERTS.DOCUMENTS.DOWNLOAD_ERROR" />
      </SomethingWentWrongModal>
    </>
  );
};
