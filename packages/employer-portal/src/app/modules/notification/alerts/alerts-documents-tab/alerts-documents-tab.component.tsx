/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CaseDocument } from 'fineos-js-api-client';
import classNames from 'classnames';
import {
  EmptyState,
  FormattedDate,
  List,
  Tabs,
  Icon,
  usePermissions,
  createThemedStyles,
  textStyleToCss,
  ViewNotificationPermissions,
  ContentRenderer,
  FormattedMessage,
  ApiError
} from 'fineos-common';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';

import { DownloadDocumentLink } from './download-document-link';
import styles from './alerts-documents-tab.module.scss';

const { TabPane } = Tabs;

type AlertsDocumentsTabProps = React.ComponentProps<typeof TabPane> & {
  caseId: string;
  documents: CaseDocument[];
  isLoadingDocuments: boolean;
  fetchDocumentsError: Readonly<Error | ApiError> | null;
  refreshDocuments: () => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  unreadDocumentItem: {
    borderLeftColor: theme.colorSchema.primaryColor
  },
  readIcon: {
    color: theme.colorSchema.neutral5
  },
  unreadIcon: {
    color: theme.colorSchema.primaryColor
  },
  title: textStyleToCss(theme.typography.baseText),
  titleColor: {
    color: theme.colorSchema.neutral8
  },
  receivedDate: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  }
}));

export const AlertsDocumentsTab: React.FC<AlertsDocumentsTabProps> = ({
  caseId,
  documents = [],
  isLoadingDocuments,
  fetchDocumentsError,
  refreshDocuments,
  ...props
}) => {
  const hasDownloadDocumentPermission = usePermissions(
    ViewNotificationPermissions.VIEW_CASES_DOWNLOAD_DOCUMENT
  );

  const themedStyles = useThemedStyles();

  return (
    <TabPane {...props} hasNoMargin={true}>
      <ContentRenderer
        isEmpty={!documents.length}
        isLoading={isLoadingDocuments}
        error={fetchDocumentsError}
        data-test-el="alerts-documents-tab"
        emptyContent={
          <EmptyState>
            <FormattedMessage id="NOTIFICATIONS.ALERTS.DOCUMENTS.EMPTY" />
          </EmptyState>
        }
      >
        <List
          dataSource={documents}
          renderItem={(document: CaseDocument) => {
            const isRead = document.isRead || document.readForMyOrganisation;
            const unRead = !document.isRead && !document.readForMyOrganisation;

            return (
              <List.Item
                key={document.documentId}
                data-test-el="alerts-document-item"
                className={classNames(styles.documentItem, {
                  [themedStyles.unreadDocumentItem]: unRead
                })}
              >
                <List.Item.Meta
                  avatar={
                    <Icon
                      icon={faFileAlt}
                      data-test-el="read-icon"
                      iconLabel={
                        <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.READ_ICON_LABEL" />
                      }
                      className={classNames(styles.documentIcon, {
                        [themedStyles.readIcon]: isRead,
                        [themedStyles.unreadIcon]: unRead
                      })}
                    />
                  }
                  description={
                    <>
                      {hasDownloadDocumentPermission ? (
                        <DownloadDocumentLink
                          document={document}
                          className={themedStyles.title}
                          documentMarkedAsRead={() => refreshDocuments()}
                        >
                          {document.name}
                        </DownloadDocumentLink>
                      ) : (
                        <div
                          className={classNames(
                            themedStyles.titleColor,
                            themedStyles.title
                          )}
                        >
                          {document.name}
                        </div>
                      )}
                      <FormattedDate
                        data-test-el="alert-received-date"
                        date={document.receivedDate}
                        className={classNames(
                          styles.receivedDate,
                          themedStyles.receivedDate
                        )}
                      />
                    </>
                  }
                />
              </List.Item>
            );
          }}
        />
      </ContentRenderer>
    </TabPane>
  );
};
