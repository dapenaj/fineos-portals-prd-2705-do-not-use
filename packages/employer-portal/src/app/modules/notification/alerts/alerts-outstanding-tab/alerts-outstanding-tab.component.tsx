/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CustomerSummary } from 'fineos-js-api-client';
import {
  EmptyState,
  Tabs,
  ContentRenderer,
  FormattedMessage
} from 'fineos-common';

import { OutstandingInformationList } from '../../../../shared';
import { useOutstandingInfo } from '../use-outstanding-info.hook';

const { TabPane } = Tabs;

type AlertsOutstandingTabProps = React.ComponentProps<typeof TabPane> &
  ReturnType<typeof useOutstandingInfo> & {
    caseId: string;
    handleRefreshOutstandingInfo: () => void;
  } & { customer: CustomerSummary };

export const AlertsOutstandingTab: React.FC<AlertsOutstandingTabProps> = ({
  outstandingInfos,
  isLoadingOutstanding,
  fetchOutstandingError,
  caseId,
  handleRefreshOutstandingInfo,
  customer,
  ...props
}) => (
  <TabPane {...props} hasNoMargin={true}>
    <ContentRenderer
      data-test-el="alerts-outstanding-tab"
      isEmpty={!outstandingInfos.length}
      isLoading={isLoadingOutstanding}
      error={fetchOutstandingError}
      emptyContent={
        <EmptyState>
          <FormattedMessage id="NOTIFICATIONS.ALERTS.OUTSTANDING.EMPTY" />
        </EmptyState>
      }
    >
      <OutstandingInformationList
        outstandingInfos={outstandingInfos}
        caseId={caseId}
        refreshOutstandingInfo={handleRefreshOutstandingInfo}
        customer={customer}
      />
    </ContentRenderer>
  </TabPane>
);
