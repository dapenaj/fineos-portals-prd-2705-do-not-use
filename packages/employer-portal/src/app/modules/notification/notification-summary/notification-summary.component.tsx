/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useMemo, useCallback } from 'react';
import classNames from 'classnames';
import { Col, Row } from 'antd';
import {
  PropertyItem,
  createThemedStyles,
  CollapsibleCard,
  usePermissions,
  ManageNotificationPermissions,
  FormattedMessage,
  textStyleToCss,
  Button,
  ButtonType
} from 'fineos-common';
import {
  GroupClientNotification,
  DisabilityClaim,
  SubCaseType
} from 'fineos-js-api-client';

import {
  EnhancedGroupClientPeriodDecision,
  EnhancedAbsencePeriodDecisions
} from '../../../shared';
import { LeavePeriodChange } from '../leave-period-change/leave-period-change.component';
import { RequestForMoreTime } from '../request-for-more-time/request-for-more-time.component';
import { RequestForMoreTimePopover } from '../request-for-more-time/request-for-more-time-popover/request-for-more-time-popover.component';
import { getDecisionsToRemove } from '../leave-period-change/edit-leave-period/edit-leave-period.utils';
import { getAllAbsencePeriodDecisions } from '../notification.util';
import { NotificationReason } from '../notification-reason';

import {
  buildNotificationProperties,
  NotificationSummaryColumn,
  getNotificationCaseResult
} from './notification-summary.util';
import { ActionsDropdown } from './actions-dropdown/actions-dropdown.component';
import { Header } from './header';
import styles from './notification-summary.module.scss';

type NotificationSummaryProps = {
  notification: GroupClientNotification;
  disabilityClaims: DisabilityClaim[] | null;
  isJobProtectedLeaveVisible: boolean;
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[] | null;
};

const useThemedStyles = createThemedStyles(theme => {
  return {
    border: {
      borderLeftColor: theme.colorSchema.neutral4
    },
    propertyValue: {
      ...textStyleToCss(theme.typography.baseText)
    }
  };
});

export const NotificationSummary: React.FC<NotificationSummaryProps> = ({
  notification,
  disabilityClaims,
  isJobProtectedLeaveVisible,
  enhancedAbsencePeriodDecisions
}) => {
  const themedStyles = useThemedStyles();
  const absenceSubCases = notification.subCases.filter(
    subCase => subCase.caseType === SubCaseType.ABSENCE_TYPE
  );
  const absencePeriodDecisions = useMemo(
    () => getAllAbsencePeriodDecisions(enhancedAbsencePeriodDecisions) || [],
    [enhancedAbsencePeriodDecisions]
  );

  const [isEditLeavePeriod, setIsEditLeavePeriod] = useState(false);
  const [isEditRequestForMoreTime, setIsEditRequestForMoreTime] = useState(
    false
  );

  const hasLeavePeriodsChangePermission = usePermissions(
    ManageNotificationPermissions.ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST
  );
  const removableDecisions = useMemo(
    () => getDecisionsToRemove(enhancedAbsencePeriodDecisions),
    [enhancedAbsencePeriodDecisions]
  );
  const isLeavePeriodAvailable = useMemo(
    () =>
      Boolean(
        hasLeavePeriodsChangePermission &&
          isJobProtectedLeaveVisible &&
          removableDecisions.length
      ),
    [
      hasLeavePeriodsChangePermission,
      isJobProtectedLeaveVisible,
      removableDecisions
    ]
  );

  const RequestForMoreTimePopoverWrapper = useCallback(
    (
      props: Omit<
        React.ComponentProps<typeof RequestForMoreTimePopover>,
        'notificationReason'
      >
    ) => (
      <RequestForMoreTimePopover
        notificationReason={notification.notificationReason}
        {...props}
      />
    ),
    [notification]
  );

  const actionsMenuItems = [
    {
      title: (
        <FormattedMessage
          id="LEAVE_PERIOD_CHANGE.DROPDOWN_OPTION"
          buttonType={ButtonType.LINK}
          as={Button}
          className={styles.itemButton}
        />
      ),
      visible: isLeavePeriodAvailable,
      action: () => setIsEditLeavePeriod(true)
    },
    {
      title: <FormattedMessage id="REQUEST_FOR_MORE_TIME.DROPDOWN_OPTION" />,
      visible: hasLeavePeriodsChangePermission,
      popover: RequestForMoreTimePopoverWrapper,
      action: () => setIsEditRequestForMoreTime(true)
    }
  ];

  return (
    <>
      <CollapsibleCard
        role="region"
        aria-label={
          <FormattedMessage id="NOTIFICATIONS.NOTIFICATION_SUMMARY.TITLE" />
        }
        title={
          <Row justify="space-between">
            <Col span={20}>
              <Header
                label={
                  <FormattedMessage id="NOTIFICATIONS.NOTIFICATION_SUMMARY.TITLE" />
                }
                reason={
                  <NotificationReason
                    reason={notification.notificationReason}
                  />
                }
              />
            </Col>
            <Col span={4}>
              {absenceSubCases.length === 1 && (
                <ActionsDropdown menuItems={actionsMenuItems} />
              )}
            </Col>
          </Row>
        }
        isOpen={true}
        isPanelUI={true}
        data-test-el="notification-summary"
      >
        <Row>
          <Col xs={24} sm={6} className={styles.progressCol}>
            <PropertyItem
              className={styles.property}
              label={
                <FormattedMessage id="NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS" />
              }
            >
              <div>
                <FormattedMessage
                  className={themedStyles.propertyValue}
                  id={`NOTIFICATIONS.CASE_PROGRESS.${getNotificationCaseResult(
                    notification.subCases
                  )}`}
                />
              </div>
            </PropertyItem>
          </Col>
          <Col
            xs={24}
            sm={18}
            className={classNames(themedStyles.border, styles.datesSeparator)}
          >
            <Row>
              {notification &&
                buildNotificationProperties(
                  notification,
                  absencePeriodDecisions,
                  disabilityClaims
                ).map(({ label, value }: NotificationSummaryColumn) => {
                  return (
                    <Col
                      xs={24}
                      sm={12}
                      md={8}
                      key={label}
                      className={styles.col}
                    >
                      <PropertyItem
                        className={styles.property}
                        label={<FormattedMessage id={label} />}
                      >
                        <span className={themedStyles.propertyValue}>
                          {value}
                        </span>
                      </PropertyItem>
                    </Col>
                  );
                })}
            </Row>
          </Col>
        </Row>
      </CollapsibleCard>

      {isEditLeavePeriod && (
        <LeavePeriodChange
          isEditLeavePeriod={isEditLeavePeriod}
          onEditLeavePeriodChange={() => setIsEditLeavePeriod(false)}
          decisions={removableDecisions}
        />
      )}

      {isEditRequestForMoreTime && (
        <RequestForMoreTime
          absenceId={absenceSubCases[0].id}
          onCancel={() => setIsEditRequestForMoreTime(false)}
          absencePeriod={
            absencePeriodDecisions
              ? (absencePeriodDecisions[0] as EnhancedGroupClientPeriodDecision)
              : null
          }
        />
      )}
    </>
  );
};
