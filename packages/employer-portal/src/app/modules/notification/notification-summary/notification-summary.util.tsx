/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  GroupClientNotification,
  GroupClientPeriodDecisions,
  DisabilityClaim,
  SubCase,
  SubCaseType
} from 'fineos-js-api-client';
import { flatten as _flatten, isEmpty as _isEmpty } from 'lodash';
import { Moment } from 'moment';
import { FormattedDate, FallbackValue, MarkedText } from 'fineos-common';

import {
  PeriodStatus,
  PeriodType,
  NotificationCaseStatus
} from '../../../shared';
import { NotificationReason } from '../notification-reason.type';

import {
  NOTIFICATION_PENDING_STATUSES,
  NOTIFICATION_CLOSED_STATUSES,
  NOTIFICATION_DECIDED_STATUSES
} from './notification-summary.constant';

export type NotificationSummaryColumn = {
  label: string;
  value: React.ReactNode;
};

const CONTINUOUS_LEAVE_PERIOD = 'Time off period';

export const buildNotificationProperties = (
  notification: GroupClientNotification,
  absencePeriodDecisions: GroupClientPeriodDecisions[] | null,
  disabilityClaims: DisabilityClaim[] | null
): NotificationSummaryColumn[] => {
  const results: NotificationSummaryColumn[] = [
    {
      label: 'NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON',
      value: getFormattedDate(notification.notificationDate)
    }
  ];

  if (!_isEmpty(disabilityClaims) || !_isEmpty(absencePeriodDecisions)) {
    const firstDayMissingWorkDays = getFirstDayMissingWorkDays(
      !_isEmpty(disabilityClaims) ? disabilityClaims! : [],
      !_isEmpty(absencePeriodDecisions) ? absencePeriodDecisions! : []
    );
    results.push({
      label: 'NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING',
      value: (
        <FallbackValue value={getFormattedDate(firstDayMissingWorkDays[0])} />
      )
    });
  }

  if (shouldDisplayExpectedRTW(notification, absencePeriodDecisions)) {
    results.push({
      label: 'NOTIFICATIONS.NOTIFICATION_SUMMARY.EXPECTED_RETURN_TO_WORK',
      value: (
        <MarkedText>
          <FallbackValue
            value={getFormattedDate(notification.expectedRTWDate)}
          />
        </MarkedText>
      )
    });
  }

  if (!_isEmpty(disabilityClaims)) {
    const accidentDates: Moment[] = getAccidentDates(disabilityClaims!);
    if (notification.notificationReason.name === NotificationReason.ACCIDENT) {
      results.push({
        label: 'NOTIFICATIONS.NOTIFICATION_SUMMARY.ACCIDENT_DATE',
        value: <FallbackValue value={getFormattedDate(accidentDates[0])} />
      });
    }
  }

  return results;
};

const shouldDisplayExpectedRTW = (
  currentNotification: GroupClientNotification,
  currentAbsencePeriodDecisions: GroupClientPeriodDecisions[] | null
) => {
  const isSubcaseType = (type: SubCaseType) =>
    currentNotification.subCases.some(subCase => subCase.caseType === type);

  const hasDisabilityClaim = isSubcaseType(SubCaseType.CLAIM_TYPE);
  const hasContinuousPeriodType = currentAbsencePeriodDecisions
    ?.map(periodDecision =>
      periodDecision.decisions.some(
        decision => decision.period.type === CONTINUOUS_LEAVE_PERIOD
      )
    )
    .some(Boolean);
  const isCaseAccomodationType =
    isSubcaseType(SubCaseType.ACCOMMODATION_TYPE) &&
    !isSubcaseType(SubCaseType.ABSENCE_TYPE) &&
    !isSubcaseType(SubCaseType.CLAIM_TYPE);

  return (
    (hasDisabilityClaim || hasContinuousPeriodType) && !isCaseAccomodationType
  );
};

const getFormattedDate = (date: Moment | null) =>
  date && date.isValid() ? <FormattedDate date={date} /> : null;

const getFirstDayMissingWorkDays = (
  disabilityClaims: DisabilityClaim[],
  absencePeriodDecisions: GroupClientPeriodDecisions[]
): Moment[] =>
  [
    ...getFirstDayMissedWork(disabilityClaims),
    ...getStartDate(absencePeriodDecisions)
  ]
    .filter(date => date && date.isValid())
    .sort((a, b) => a.diff(b));

const getAccidentDates = (disabilityClaims: DisabilityClaim[]): Moment[] =>
  _flatten(disabilityClaims)
    .map(({ accidentDate }) => accidentDate)
    .filter(date => date && date.isValid())
    .sort((a, b) => a.diff(b));

const getStartDate = (
  absencePeriodDecisions: GroupClientPeriodDecisions[]
): Moment[] => {
  const startDates: Moment[] = [];
  const periodTypes = [
    PeriodType.TIME_OFF,
    PeriodType.INCAPACITY,
    PeriodType.OFFICE_VISIT
  ];

  _flatten(absencePeriodDecisions).forEach(({ decisions }) => {
    startDates.push(
      ...decisions
        .filter(
          ({ period: { type, status, relatedToEpisodic } }) =>
            periodTypes.some(periodType => periodType === type) &&
            status !== PeriodStatus.EPISODIC &&
            status !== PeriodStatus.CANCELLED &&
            !relatedToEpisodic
        )
        .filter(decision => decision.period.startDate)
        .map(decision => decision.period.startDate)
    );
  });
  return startDates;
};

const getFirstDayMissedWork = (disabilityClaims: DisabilityClaim[]): Moment[] =>
  _flatten(disabilityClaims).map(
    ({ firstDayMissedWork }) => firstDayMissedWork
  );

export const getNotificationCaseResult = (
  subCases: SubCase[]
): NotificationCaseStatus => {
  const statuses = subCases.map(subCase => subCase.status);

  if (!!statuses.length) {
    const allPendingStatuses = statuses.every(status =>
      NOTIFICATION_PENDING_STATUSES.includes(status)
    );
    const allClosedStatuses = statuses.every(status =>
      NOTIFICATION_CLOSED_STATUSES.includes(status)
    );
    const allDecidedStatuses = statuses.every(status =>
      NOTIFICATION_DECIDED_STATUSES.includes(status)
    );
    const someDecidedStatuses = statuses.some(status =>
      NOTIFICATION_DECIDED_STATUSES.includes(status)
    );
    const someClosedStatuses = statuses.some(status =>
      NOTIFICATION_CLOSED_STATUSES.includes(status)
    );
    const somePendingStatuses = statuses.some(status =>
      NOTIFICATION_PENDING_STATUSES.includes(status)
    );

    if (allPendingStatuses) {
      return NotificationCaseStatus.PENDING;
    } else if (allClosedStatuses) {
      return NotificationCaseStatus.CLOSED;
    } else if (
      allDecidedStatuses ||
      (someClosedStatuses && someDecidedStatuses)
    ) {
      return NotificationCaseStatus.DECIDED;
    } else if (
      someDecidedStatuses ||
      (someClosedStatuses && somePendingStatuses)
    ) {
      return NotificationCaseStatus.SOME_DECISIONS;
    }

    return NotificationCaseStatus.UNDETERMINED;
  }

  return NotificationCaseStatus.UNDETERMINED;
};
