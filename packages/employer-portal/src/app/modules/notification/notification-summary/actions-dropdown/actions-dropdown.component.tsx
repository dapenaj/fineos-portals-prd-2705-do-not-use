/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Menu } from 'antd';
import { Button, FormattedMessage, DropdownMenu } from 'fineos-common';
import { DownOutlined } from '@ant-design/icons';

import styles from './actions-dropdown.module.scss';

type ActionsDropdownMenuItem = {
  title: React.ReactNode;
  visible: boolean;
  action: () => void;
  popover?: React.ElementType<{
    onConfirm: () => void;
    className?: string;
  }>;
};

type ActionsDropdownProps = {
  menuItems: ActionsDropdownMenuItem[];
};

export const ActionsDropdown: React.FC<ActionsDropdownProps> = ({
  menuItems
}) => {
  const [isMenuVisible, setMenuVisible] = useState(false);

  if (!menuItems.filter(item => item.visible).length) {
    return null;
  }

  const actionsMenu = (
    <Menu className={styles.dropdownWrapper} data-test-el="actions-dropdown">
      {menuItems
        .filter(item => item.visible)
        .map(({ title, action, popover: PopoverElement }, index) => (
          <Menu.Item
            className={styles.menuItem}
            key={`actions-dropdown-${index}`}
          >
            {PopoverElement ? (
              <PopoverElement
                onConfirm={() => {
                  action();
                  setMenuVisible(false);
                }}
              >
                {title}
              </PopoverElement>
            ) : (
              <div
                onClick={() => {
                  action();
                  setMenuVisible(false);
                }}
              >
                {title}
              </div>
            )}
          </Menu.Item>
        ))}
    </Menu>
  );

  return (
    <div onClick={e => e.stopPropagation()}>
      <DropdownMenu
        data-test-el="withdraw-action-trigger"
        overlay={actionsMenu}
        trigger={['click']}
        placement="bottomRight"
        visible={isMenuVisible}
        onVisibleChange={(isVisible: boolean) => setMenuVisible(isVisible)}
      >
        <Button>
          <FormattedMessage id="NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON" />
          <DownOutlined className={styles.downIcon} />
        </Button>
      </DropdownMenu>
    </div>
  );
};
