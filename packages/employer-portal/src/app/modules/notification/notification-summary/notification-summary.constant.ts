/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  AbsenceCaseStatus,
  AccommodationCaseStatus,
  ClaimCaseStatus
} from 'fineos-js-api-client';

const ABSENCE_PENDING_STATUSES = [
  AbsenceCaseStatus.UNKNOWN,
  AbsenceCaseStatus.REGISTRATION,
  AbsenceCaseStatus.ADJUDICATION,
  AbsenceCaseStatus.EXTEND
];

const CLAIM_PENDING_STATUSES = [
  ClaimCaseStatus.UNKNOWN,
  ClaimCaseStatus.PENDING,
  ClaimCaseStatus.NOTIFICATION_INCOMPLETE,
  ClaimCaseStatus.PENDING_CLAIM,
  ClaimCaseStatus.OPEN
];

const ACCOMMODATION_PENDING_STATUSES = [
  AccommodationCaseStatus.UNKNOWN,
  AccommodationCaseStatus.REGISTRATION,
  AccommodationCaseStatus.ASSESSMENT,
  AccommodationCaseStatus.MONITORING
];

export const NOTIFICATION_PENDING_STATUSES = [
  ...ABSENCE_PENDING_STATUSES,
  ...CLAIM_PENDING_STATUSES,
  ...ACCOMMODATION_PENDING_STATUSES
];

const ABSENCE_CLOSED_STATUSES = [AbsenceCaseStatus.CLOSED];

const CLAIM_CLOSED_STATUSES = [ClaimCaseStatus.CLOSED];

const ACCOMMODATION_CLOSED_STATUSES = [AccommodationCaseStatus.CLOSED];

export const NOTIFICATION_CLOSED_STATUSES = [
  ...ABSENCE_CLOSED_STATUSES,
  ...CLAIM_CLOSED_STATUSES,
  ...ACCOMMODATION_CLOSED_STATUSES
];

const ABSENCE_DECIDED_STATUSES = [
  AbsenceCaseStatus.MANAGED_LEAVE,
  AbsenceCaseStatus.COMPLETION
];

const CLAIM_DECIDED_STATUSES = [ClaimCaseStatus.DECIDED];

const ACCOMMODATION_DECIDED_STATUSES = [AccommodationCaseStatus.COMPLETION];

export const NOTIFICATION_DECIDED_STATUSES = [
  ...ABSENCE_DECIDED_STATUSES,
  ...CLAIM_DECIDED_STATUSES,
  ...ACCOMMODATION_DECIDED_STATUSES
];
