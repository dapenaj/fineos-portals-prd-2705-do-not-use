/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientNotificationService,
  GroupClientNotification,
  DisabilityClaim,
  GroupClientAccommodationCaseSummary,
  GroupClientAccommodationService,
  AbsencePeriodDecisionsSearch,
  GroupClientAbsenceService,
  GroupClientClaimService,
  SubCaseType
} from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';
import { PartialApiCompleteError } from 'fineos-common';

import {
  EnhancedAbsencePeriodDecisions,
  ClaimDisabilityBenefit,
  RECURRING_BENEFIT,
  PaidLeaveCaseSummary,
  PaidLeaveBenefit,
  getFilteredAbsencePeriodDecisions
} from '../../shared';

const groupClientAbsenceService = GroupClientAbsenceService.getInstance();
const groupClientClaimService = GroupClientClaimService.getInstance();
const groupClientNotificationService = GroupClientNotificationService.getInstance();
const accommodationService = GroupClientAccommodationService.getInstance();

const fetchNotification = (
  notificationId: string
): Promise<GroupClientNotification> =>
  groupClientNotificationService.fetchNotification(notificationId);

const fetchEnhancedAbsencePeriodDecisions = async (
  notification: GroupClientNotification | null
): Promise<EnhancedAbsencePeriodDecisions[]> => {
  if (notification && !_isEmpty(notification.subCases)) {
    const results = await Promise.allSettled(
      notification.subCases
        .filter(
          ({ caseType }) =>
            caseType === SubCaseType.ABSENCE_TYPE ||
            caseType === SubCaseType.ABSENCE_TAKEOVER_TYPE ||
            caseType === SubCaseType.ABSENCE_HISTORICAL_CASE_TYPE
        )
        .map(async ({ id, caseHandler }) => {
          const search = new AbsencePeriodDecisionsSearch();
          search.absenceId(id);

          const absencePeriodDecisions = await groupClientAbsenceService.fetchAbsencePeriodDecisions(
            search
          );

          return {
            absenceId: id,
            caseHandler,
            absencePeriodDecisions: getFilteredAbsencePeriodDecisions(
              absencePeriodDecisions
            )
          };
        })
    );

    const absences: EnhancedAbsencePeriodDecisions[] = [];
    let apiError = null;

    for (const result of results) {
      if (result.status === 'fulfilled') {
        absences.push(result.value);
      } else {
        apiError = result.reason;
      }
    }

    if (apiError) {
      throw new PartialApiCompleteError(apiError, absences);
    }

    return absences;
  }

  return Promise.resolve([]);
};

const fetchDisabilityClaims = async (
  notification: GroupClientNotification | null
): Promise<DisabilityClaim[]> => {
  if (notification && !_isEmpty(notification.subCases)) {
    const results = await Promise.allSettled(
      notification.subCases
        .filter(({ caseType }) => caseType === SubCaseType.CLAIM_TYPE)
        .map(async ({ id }) => {
          const readDisabilityResults = await groupClientClaimService.fetchDisabilityDetails(
            id
          );

          return readDisabilityResults.disabilityClaim;
        })
    );

    const disabilityClaims: DisabilityClaim[] = [];
    let apiError = null;

    for (const result of results) {
      if (result.status === 'fulfilled') {
        disabilityClaims.push(result.value);
      } else {
        apiError = result.reason;
      }
    }

    if (apiError) {
      throw new PartialApiCompleteError(apiError, disabilityClaims);
    }

    return disabilityClaims;
  }

  return Promise.resolve([]);
};

const fetchClaimDisabilityBenefits = async (
  claimIds: string[],
  includeRecurringBenefits: boolean
): Promise<ClaimDisabilityBenefit[]> => {
  const claimBenefitResults = await Promise.allSettled(
    claimIds.map(async claimId => {
      const benefits = await groupClientClaimService.fetchBenefits(claimId);
      return benefits.map(
        benefit =>
          ({
            ...benefit,
            claimId
          } as ClaimDisabilityBenefit)
      );
    })
  );

  let filteredClaimBenefits: ClaimDisabilityBenefit[] = [];
  let apiError = null;

  for (const claimBenefitResult of claimBenefitResults) {
    if (claimBenefitResult.status === 'fulfilled') {
      filteredClaimBenefits = filteredClaimBenefits.concat(
        claimBenefitResult.value.filter(ben => !_isEmpty(ben))
      );
    } else {
      apiError = claimBenefitResult.reason;
    }
  }

  if (includeRecurringBenefits) {
    // we have permission for the API, so get some results
    const disabilityBenefitSummaryResults = await Promise.allSettled(
      filteredClaimBenefits
        .filter(benefit => benefit.benefitRightCategory === RECURRING_BENEFIT)
        .map(async benefit =>
          groupClientClaimService.fetchDisabilityBenefit(
            benefit.claimId,
            `${benefit.benefitId}`
          )
        )
    );

    for (const disabilityBenefitSummaryResult of disabilityBenefitSummaryResults) {
      if (disabilityBenefitSummaryResult.status === 'fulfilled') {
        const filteredClaimBenefit = filteredClaimBenefits.find(
          ({ benefitId }) =>
            disabilityBenefitSummaryResult.value.benefitSummary.benefitId ===
            benefitId
        )!;

        filteredClaimBenefit.disabilityBenefit =
          disabilityBenefitSummaryResult.value.disabilityBenefit;
      } else {
        apiError = disabilityBenefitSummaryResult.reason;
      }
    }

    if (apiError) {
      throw new PartialApiCompleteError(apiError, filteredClaimBenefits);
    }

    return filteredClaimBenefits;
  } else {
    const resultFilteredClaimBenefits = filteredClaimBenefits.filter(
      ({ benefitRightCategory }) => benefitRightCategory !== RECURRING_BENEFIT
    );
    if (apiError) {
      throw new PartialApiCompleteError(apiError, resultFilteredClaimBenefits);
    }
    // we don't have permission for the API, so filter out those benefits
    return resultFilteredClaimBenefits;
  }
};

const fetchPaidLeaveBenefits = async (
  paidLeaveCases: PaidLeaveCaseSummary[],
  includeRecurringBenefits: boolean
): Promise<PaidLeaveBenefit[]> => {
  const claimBenefitResults = await Promise.allSettled(
    paidLeaveCases.map(async ({ id, name, reasonName, type }) => {
      const benefits = await groupClientClaimService.fetchBenefits(id);
      return benefits.map(
        benefit =>
          ({
            ...benefit,
            paidLeaveCaseId: id,
            leavePlanName: name,
            reasonName,
            type
          } as PaidLeaveBenefit)
      );
    })
  );

  let filteredClaimBenefits: PaidLeaveBenefit[] = [];
  let apiError = null;

  for (const claimBenefitResult of claimBenefitResults) {
    if (claimBenefitResult.status === 'fulfilled') {
      filteredClaimBenefits = filteredClaimBenefits.concat(
        claimBenefitResult.value.filter(ben => !_isEmpty(ben))
      );
    } else {
      apiError = claimBenefitResult.reason;
    }
  }

  if (includeRecurringBenefits) {
    // we have permission for the API, so get some results
    const disabilityBenefitSummaryResults = await Promise.allSettled(
      filteredClaimBenefits
        .filter(benefit => benefit.benefitRightCategory === RECURRING_BENEFIT)
        .map(async benefit =>
          groupClientClaimService.fetchDisabilityBenefit(
            benefit.paidLeaveCaseId,
            `${benefit.benefitId}`
          )
        )
    );

    for (const disabilityBenefitSummaryResult of disabilityBenefitSummaryResults) {
      if (disabilityBenefitSummaryResult.status === 'fulfilled') {
        const filteredClaimBenefit = filteredClaimBenefits.find(
          ({ benefitId }) =>
            disabilityBenefitSummaryResult.value.benefitSummary.benefitId ===
            benefitId
        )!;

        filteredClaimBenefit.disabilityBenefit =
          disabilityBenefitSummaryResult.value.disabilityBenefit;
      } else {
        apiError = disabilityBenefitSummaryResult.reason;
      }
    }

    if (apiError) {
      throw new PartialApiCompleteError(apiError, filteredClaimBenefits);
    }

    return filteredClaimBenefits;
  } else {
    const resultFilteredClaimBenefits = filteredClaimBenefits.filter(
      ({ benefitRightCategory }) => benefitRightCategory !== RECURRING_BENEFIT
    );
    if (apiError) {
      throw new PartialApiCompleteError(apiError, resultFilteredClaimBenefits);
    }
    // we don't have permission for the API, so filter out those benefits
    return resultFilteredClaimBenefits;
  }
};

const fetchAccommodationCases = async (
  accommodationCaseIds: string[]
): Promise<GroupClientAccommodationCaseSummary[]> => {
  if (!_isEmpty(accommodationCaseIds)) {
    const results = await Promise.allSettled(
      accommodationCaseIds.map(accommodationCaseId =>
        accommodationService.fetchAccommodations(accommodationCaseId)
      )
    );

    const accommodationCases: GroupClientAccommodationCaseSummary[] = [];
    let apiError = null;

    for (const result of results) {
      if (result.status === 'fulfilled') {
        if (!_isEmpty(result.value)) {
          accommodationCases.push(result.value);
        }
      } else {
        apiError = result.reason;
      }
    }

    if (apiError) {
      throw new PartialApiCompleteError(apiError, accommodationCases);
    }

    return accommodationCases;
  }

  return [];
};

export {
  fetchNotification,
  fetchEnhancedAbsencePeriodDecisions,
  fetchDisabilityClaims,
  fetchClaimDisabilityBenefits,
  fetchPaidLeaveBenefits,
  fetchAccommodationCases
};
