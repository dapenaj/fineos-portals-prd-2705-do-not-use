/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FallbackValue,
  createThemedStyles,
  textStyleToCss,
  usePermissions,
  ViewCustomerDataPermissions,
  FormattedMessage,
  Link
} from 'fineos-common';
import { GroupClientNotification } from 'fineos-js-api-client';
import { RightOutlined } from '@ant-design/icons';
import classNames from 'classnames';

import { TokenTitle } from '../../../shared';

import styles from './notification-header.module.scss';

const useThemedStyles = createThemedStyles(theme => {
  return {
    wrapper: {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8
    },
    notificationId: {
      ...textStyleToCss(theme.typography.pageTitle),
      color: theme.colorSchema.neutral8
    },
    link: {
      ...textStyleToCss(theme.typography.pageTitle)
    },
    text: {
      ...textStyleToCss(theme.typography.baseText),
      color: theme.colorSchema.neutral8
    }
  };
});

type NotificationHeaderProps = {
  notificationId: string;
  notification: GroupClientNotification | null;
};

export const NotificationHeader: React.FC<NotificationHeaderProps> = ({
  notificationId,
  notification
}) => {
  const hasEmployeeProfilePermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER
  );
  const themedStyles = useThemedStyles();

  return (
    <div
      className={classNames(styles.wrapper, themedStyles.wrapper)}
      data-test-el="notification-header"
    >
      <h1
        className={themedStyles.notificationId}
        data-test-el="notification-header-notification-id"
      >
        {notificationId}
      </h1>
      <span className={themedStyles.notificationId}>
        <RightOutlined className={styles.icon} />
      </span>

      <TokenTitle
        tokens={[
          <FallbackValue
            className={themedStyles.text}
            key={0}
            data-test-el="notification-header-job-title"
            value={notification?.customer?.jobTitle}
            fallback={
              <FormattedMessage id={'EMPLOYEE.FALLBACK.UNKNOWN_JOB_TITLE'} />
            }
          />,
          <FallbackValue
            className={themedStyles.text}
            key={1}
            data-test-el="notification-header-organisation-unit"
            value={notification?.customer?.organisationUnit}
            fallback={
              <FormattedMessage id={'EMPLOYEE.FALLBACK.UNKNOWN_UNIT'} />
            }
          />,
          <FallbackValue
            className={themedStyles.text}
            key={2}
            data-test-el="notification-header-work-site"
            value={notification?.customer.workSite}
            fallback={
              <FormattedMessage
                className={themedStyles.text}
                id={'EMPLOYEE.FALLBACK.UNKNOWN_LOCATION'}
              />
            }
          />
        ]}
      >
        {notification &&
          (hasEmployeeProfilePermission ? (
            <Link
              data-test-el="notification-header-customer-link"
              className={themedStyles.link}
              to={`/profile/${notification.customer.id}`}
            >
              {notification.customer.firstName} {notification.customer.lastName}
            </Link>
          ) : (
            <span className={themedStyles.link}>
              {notification.customer.firstName} {notification.customer.lastName}
            </span>
          ))}
      </TokenTitle>
    </div>
  );
};
