/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { GroupClientAccommodation } from 'fineos-js-api-client';
import { Row, Col } from 'antd';
import {
  ContentLayout,
  PropertyItem,
  FormattedDate,
  FormattedMessage
} from 'fineos-common';

import styles from './proposed-accommodation-properties.module.scss';

type ProposedAccommodationProperties = {
  accommodation: GroupClientAccommodation;
};

export const ProposedAccommodationProperties: React.FC<ProposedAccommodationProperties> = ({
  accommodation: {
    accommodationDescription,
    createDate,
    implementedDate,
    endDate
  }
}) => {
  return (
    <div
      className={styles.wrapper}
      data-test-el="proposed-accommodation-properties"
    >
      <ContentLayout direction="column" noPadding={true}>
        <Row align="top">
          <Col xs={24} sm={12}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.DESCRIPTION" />
              }
            >
              {accommodationDescription}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={6}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.REQUEST_DATE" />
              }
            >
              <FormattedDate date={createDate} />
            </PropertyItem>
          </Col>
          <Col xs={24} sm={6}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATION_DATE" />
              }
            >
              <>
                <FormattedDate date={implementedDate} />
                {!!endDate && endDate.isValid() && (
                  <span>
                    {' '}
                    - <FormattedDate date={endDate} />
                  </span>
                )}
              </>
            </PropertyItem>
          </Col>
        </Row>
      </ContentLayout>
    </div>
  );
};
