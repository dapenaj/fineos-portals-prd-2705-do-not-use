/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { GroupClientAccommodation } from 'fineos-js-api-client';
import {
  CollapsibleCard,
  MultipleTokens,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';

import { ProposedAccommodationProperties } from './proposed-accommodation-properties';
import styles from './proposed-accommodations.module.scss';

type ProposedAccommodationsProps = {
  accommodations: GroupClientAccommodation[];
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const ProposedAccommodations: React.FC<ProposedAccommodationsProps> = ({
  accommodations
}) => {
  const themedStyles = useThemedStyles();
  return (
    <div className={styles.wrapper}>
      {accommodations.map((accommodation, index) => (
        <CollapsibleCard
          data-test-el="proposed-accommodations"
          key={`proposed_accommodation_${index}`}
          title={
            <div className={themedStyles.title}>
              <MultipleTokens
                tokens={[
                  accommodation.accommodationCategory,
                  accommodation.accommodationType
                ]}
              />
            </div>
          }
        >
          <ProposedAccommodationProperties accommodation={accommodation} />
        </CollapsibleCard>
      ))}
    </div>
  );
};
