/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  textStyleToCss,
  createThemedStyles,
  Button,
  ButtonType,
  FormattedMessage
} from 'fineos-common';
import classNames from 'classnames';

import styles from './show-reason.module.scss';

type ShowReasonProps = {
  reasons: string[];
};

const useThemedStyles = createThemedStyles(theme => ({
  reason: {
    ...textStyleToCss(theme.typography.smallText)
  }
}));

export const ShowReason: React.FC<ShowReasonProps> = ({ reasons }) => {
  const [isVisible, showReason] = useState(false);
  const themedStyles = useThemedStyles();
  return (
    <div>
      {!isVisible ? (
        <Button
          data-test-el="show-reason"
          buttonType={ButtonType.LINK}
          onClick={() => showReason(!isVisible)}
          className={styles.reasonBtn}
        >
          <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.SHOW_REASON" />
        </Button>
      ) : (
        <div>
          {reasons.map((reason, index) => (
            <p
              key={index}
              data-test-el="closed-accommodation-reason"
              className={classNames(themedStyles.reason, styles.reason)}
            >
              {reason}
            </p>
          ))}
          <Button
            data-test-el="hide-reason"
            buttonType={ButtonType.LINK}
            onClick={() => showReason(!isVisible)}
            className={styles.reasonBtn}
          >
            <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.HIDE_REASON" />
          </Button>
        </div>
      )}
    </div>
  );
};
