/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  GroupClientAccommodationCaseSummary,
  BaseDomain
} from 'fineos-js-api-client';
import { Col, Row } from 'antd';
import {
  CardBodyWithStatus,
  PropertyItem,
  StatusLabel,
  EmptyState,
  createThemedStyles,
  textStyleToCss,
  FallbackValue,
  FormattedMessage
} from 'fineos-common';
import classNames from 'classnames';
import { isEmpty as _isEmpty } from 'lodash';
import { useIntl } from 'react-intl';

import { WorkplaceAccommodationStatusLabel } from '../../../../shared';
import { getWorkplaceAccommodationStatus } from '../workplace-accommodation.util';

import { ProposedAccommodations } from './proposed-accommodations';
import { ShowReason } from './show-reason/show-reason.component';
import styles from './accommodation.module.scss';
import {
  EnumDomain,
  getEnumDomainTranslation
} from '../../../../shared/enum-domain';

type AccommodationPropertiesProps = {
  accommodation: GroupClientAccommodationCaseSummary;
  notifiedBy?: BaseDomain;
  statusLabel: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    ...textStyleToCss(theme.typography.panelTitle),
    color: theme.colorSchema.neutral8
  }
}));

export const AccommodationProperties: React.FC<AccommodationPropertiesProps> = ({
  accommodation: { limitations, accommodations, closureReasons },
  notifiedBy,
  statusLabel
}) => {
  const intl = useIntl();
  const themedStyles = useThemedStyles();
  const status = getWorkplaceAccommodationStatus(
    statusLabel as WorkplaceAccommodationStatusLabel
  );

  return (
    <CardBodyWithStatus
      data-test-el="accommodation-properties"
      type={status}
      aria-label={intl.formatMessage({
        id: statusLabel
      })}
    >
      <div className={styles.card}>
        <Row align="top">
          <Col xs={24} sm={12}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.LIMITATIONS" />
              }
            >
              {!_isEmpty(limitations) &&
                limitations.map((limitation: string, index) => (
                  <div key={`${index}${limitation}`}> - {limitation}</div>
                ))}
            </PropertyItem>
          </Col>
          <Col xs={24} sm={6}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.NOTIFIED_BY" />
              }
            >
              <FallbackValue
                value={
                  notifiedBy?.name && (
                    <FormattedMessage
                      id={getEnumDomainTranslation(
                        EnumDomain.NOTIFICATION_CASE_NOTIFIED_BY,
                        notifiedBy?.name
                      )}
                    />
                  )
                }
              />
            </PropertyItem>
          </Col>
          <Col xs={24} sm={6}>
            <PropertyItem
              label={
                <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.STATUS" />
              }
            >
              <StatusLabel type={status}>
                <FormattedMessage id={statusLabel} />
              </StatusLabel>
              {statusLabel ===
                WorkplaceAccommodationStatusLabel.NOT_ACCOMMODATED &&
                !_isEmpty(closureReasons) && (
                  <ShowReason reasons={closureReasons} />
                )}
            </PropertyItem>
          </Col>
        </Row>

        <div
          className={classNames(themedStyles.title, styles.title)}
          data-test-el="accommodation-title"
        >
          <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATIONS_PROPOSED" />
        </div>
        {!_isEmpty(accommodations) ? (
          <ProposedAccommodations accommodations={accommodations} />
        ) : (
          <EmptyState withBorder={true}>
            <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.EMPTY" />
          </EmptyState>
        )}
      </div>
    </CardBodyWithStatus>
  );
};
