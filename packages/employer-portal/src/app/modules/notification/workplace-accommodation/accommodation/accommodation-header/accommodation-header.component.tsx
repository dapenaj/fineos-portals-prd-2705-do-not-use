/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  StatusLabel,
  ContentLayout,
  PropertyItem,
  MultipleTokens,
  HandlerInfo,
  FormattedDate,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { GroupClientAccommodationCaseSummary } from 'fineos-js-api-client';
import { Col, Row } from 'antd';

import {
  getWorkplaceAccommodationStatus,
  getWorkplaceAccommodationStatusLabel
} from '../../workplace-accommodation.util';
import {
  WorkplaceAccommodationStatusLabel,
  WorkplaceAccommodationStage,
  WorkplaceAccommodationPhase
} from '../../../../../shared';

import styles from './accommodation-header.module.scss';

type AccommodationRequestHeaderProps = {
  accommodation: GroupClientAccommodationCaseSummary;
  isCollapsed: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const AccommodationRequestHeader: React.FC<AccommodationRequestHeaderProps> = ({
  isCollapsed,
  accommodation
}) => {
  const title = (
    <FormattedMessage
      id="NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATION_REQUEST"
      key={1}
    />
  );
  const date = <FormattedDate date={accommodation.notificationDate} key={2} />;
  const statusLabel = getWorkplaceAccommodationStatusLabel(
    accommodation.stage as WorkplaceAccommodationStage,
    accommodation.phase as WorkplaceAccommodationPhase,
    accommodation
  );
  const themedStyles = useThemedStyles();
  return (
    <>
      {isCollapsed ? (
        <>
          <div
            className={themedStyles.title}
            data-test-el="accomodation-header-title"
          >
            <MultipleTokens tokens={[title, date]} />
          </div>
          <StatusLabel
            type={getWorkplaceAccommodationStatus(
              statusLabel as WorkplaceAccommodationStatusLabel
            )}
          >
            <FormattedMessage id={statusLabel} />
          </StatusLabel>
        </>
      ) : (
        <ContentLayout direction="column" noPadding={true}>
          <Row align="middle" className={styles.openHeader}>
            <Col xs={24} sm={12}>
              <div
                className={themedStyles.title}
                data-test-el="accomodation-header-title"
              >
                {title}
              </div>
              <PropertyItem
                label={<FormattedMessage id="NOTIFICATIONS.CREATED_ON" />}
                isBlock={false}
              >
                {date}
              </PropertyItem>
            </Col>
            <Col xs={24} sm={12}>
              <HandlerInfo
                name={accommodation.caseHandler?.name || ''}
                phoneNumber={accommodation.caseHandler?.phoneNumber || ''}
              />
            </Col>
          </Row>
        </ContentLayout>
      )}
    </>
  );
};
