/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { isEmpty as _isEmpty } from 'lodash';
import {
  Panel,
  ContentRenderer,
  ViewNotificationPermissions,
  withPermissions,
  FormattedMessage
} from 'fineos-common';
import {
  GroupClientAccommodationCaseSummary,
  GroupClientNotification
} from 'fineos-js-api-client';

import styles from './workplace-accommodation.module.scss';
import { Accommodation } from './accommodation';
import { WorkplaceAccommodationHeader } from './workplace-accommodation-header';

type WorkplaceAccommodationProps = {
  isLoading: boolean;
  accommodations: GroupClientAccommodationCaseSummary[] | null;
  notification: GroupClientNotification | null;
};

export const WorkplaceAccommodationComponent: React.FC<WorkplaceAccommodationProps> = ({
  isLoading,
  accommodations,
  notification
}) => {
  const isEmpty = _isEmpty(accommodations);

  return (
    <Panel
      data-test-el="workplace-accommodation-panel"
      header={<WorkplaceAccommodationHeader />}
      role="region"
      aria-label={
        <FormattedMessage id="NOTIFICATIONS.ACCOMMODATIONS.PANEL_TITLE" />
      }
      className={styles.wrapper}
    >
      <ContentRenderer isLoading={isLoading} isEmpty={isEmpty}>
        {notification &&
          !isEmpty &&
          accommodations!.map(accommodation => (
            <Accommodation
              key={accommodation.id}
              accommodation={accommodation}
              notifiedBy={notification.notifiedBy}
            />
          ))}
      </ContentRenderer>
    </Panel>
  );
};

export const WorkplaceAccommodation = withPermissions(
  ViewNotificationPermissions.VIEW_ACCOMMODATION_DETAILS
)(WorkplaceAccommodationComponent);
