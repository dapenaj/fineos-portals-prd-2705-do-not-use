/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { StatusType } from 'fineos-common';
import { GroupClientAccommodationCaseSummary } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import {
  WorkplaceAccommodationStatusLabel,
  WorkplaceAccommodationStage,
  WorkplaceAccommodationPhase,
  PendingOptionsStage,
  AccommodatedStage
} from '../../../shared';

export const getWorkplaceAccommodationStatusLabel = (
  stage: WorkplaceAccommodationStage,
  phase: WorkplaceAccommodationPhase,
  accommodationCase: GroupClientAccommodationCaseSummary
) => {
  const acceptedDates = accommodationCase.accommodations.filter(
    ({ acceptedDate }) => acceptedDate! && acceptedDate.isValid()
  );
  switch (phase) {
    case WorkplaceAccommodationPhase.ASSESSMENT:
      return stage === PendingOptionsStage.EXPLORE_OPTIONS
        ? WorkplaceAccommodationStatusLabel.PENDING_OPTIONS
        : WorkplaceAccommodationStatusLabel.PENDING_IN_ASSESSMENT;
    case WorkplaceAccommodationPhase.CLOSED:
      return !_isEmpty(acceptedDates)
        ? WorkplaceAccommodationStatusLabel.ACCOMMODATED
        : WorkplaceAccommodationStatusLabel.NOT_ACCOMMODATED;
    case WorkplaceAccommodationPhase.COMPLETION:
      return stage === AccommodatedStage.MOVE_TO_MONITORING ||
        stage === AccommodatedStage.ACCOMMODATED ||
        stage === AccommodatedStage.MOVE_TO_CLOSED ||
        stage === AccommodatedStage.GENERATE_ACCOMMODATION
        ? WorkplaceAccommodationStatusLabel.ACCOMMODATED
        : WorkplaceAccommodationStatusLabel.NOT_ACCOMMODATED;
    case WorkplaceAccommodationPhase.MONITORING:
      return stage === AccommodatedStage.MONITORING
        ? WorkplaceAccommodationStatusLabel.ACCOMMODATED
        : WorkplaceAccommodationStatusLabel.PENDING_IMPLEMENTING;
    case WorkplaceAccommodationPhase.REGISTRATION:
      return WorkplaceAccommodationStatusLabel.PENDING_RECOGNIZED;
  }
};

export const getWorkplaceAccommodationStatus = (
  statusLabel: WorkplaceAccommodationStatusLabel
) => {
  switch (statusLabel) {
    case 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_ASSESSMENT':
    case 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_RECOGNIZED':
    case 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_IMPLEMENTING':
    case 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_OPTIONS':
      return StatusType.WARNING;
    case 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.NOT_ACCOMMODATED':
      return StatusType.DANGER;
    case 'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.ACCOMMODATED':
      return StatusType.SUCCESS;
  }
};
