/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Moment } from 'moment';
import { ActualAbsencePeriod } from 'fineos-js-api-client';
import { CardBodyWithStatus, ContentLayout } from 'fineos-common';
import { Row } from 'antd';
import { useIntl } from 'react-intl';

import { JobProtectedLeaveStatus } from '../../../../../shared';
import {
  getDecisionPattern,
  getDecisionStatusLabelType,
  isIntermittentType
} from '../job-protective-leave-cards.util';
import { getStatusLabelTranslation } from '../../job-protected-leave.util';
import {
  DecisionWithGeneratedId,
  LeavePlanWithTemporaryIdDecisions
} from '../job-protected-leave-cards.type';

import { DecisionProperties } from './decision-properties.component';
import { IntermittentDecisionProperties } from './intermittent-decision-properties.component';

type JobProtectedLeaveCardBodyProps = {
  decision: DecisionWithGeneratedId;
  leavePlan: LeavePlanWithTemporaryIdDecisions;
  actualsPerLeavePlan: Record<string, ActualAbsencePeriod[]>;
};

const MS_IN_DAY = 1000 * 60 * 60 * 24;

export const JobProtectedLeaveCardBody: React.FC<JobProtectedLeaveCardBodyProps> = ({
  decision,
  leavePlan,
  actualsPerLeavePlan
}) => {
  const { period, statusCategory } = decision;

  const isActualDuringDecision = (
    periodDate: Moment,
    decisionStartDate: Moment,
    decisionEndDate: Moment
  ) =>
    periodDate.isBetween(decisionStartDate, decisionEndDate, undefined, '[]');

  const getPeriodAndDecisionDaysDiff = (
    decisionStartDate: Moment,
    decisionEndDate: Moment,
    periodDate: Moment
  ) => {
    const diffInMs =
      Math.abs(decisionStartDate.diff(periodDate)) <
      Math.abs(decisionEndDate.diff(periodDate))
        ? decisionStartDate.diff(periodDate)
        : decisionEndDate.diff(periodDate);

    return Math.abs(diffInMs) / MS_IN_DAY;
  };

  const getActualsForDecision = () => {
    const leavePlanId = decision.period.leavePlan
      ? decision.period.leavePlan.id
      : '';
    if (!actualsPerLeavePlan || !actualsPerLeavePlan[leavePlanId]) {
      return [];
    }
    const actualsDuringDecision = actualsPerLeavePlan[
      leavePlanId
    ].filter(actual =>
      isActualDuringDecision(
        actual.actualDate,
        decision.period.startDate,
        decision.period.endDate
      )
    );

    if (
      actualsDuringDecision.length === actualsPerLeavePlan[leavePlanId].length
    ) {
      return actualsDuringDecision;
    } else {
      const actualsNotDuringDecision = actualsPerLeavePlan[leavePlanId].filter(
        actual =>
          !isActualDuringDecision(
            actual.actualDate,
            decision.period.startDate,
            decision.period.endDate
          )
      );

      actualsNotDuringDecision.forEach(actual => {
        const decisionsPerDiff: Record<number, DecisionWithGeneratedId[]> = {};
        leavePlan.decisions.forEach(
          (currentDecision: DecisionWithGeneratedId) => {
            const decisionDiffToActual = getPeriodAndDecisionDaysDiff(
              currentDecision.period.startDate,
              currentDecision.period.endDate,
              actual.actualDate
            );

            decisionsPerDiff[decisionDiffToActual] = [
              ...(decisionsPerDiff[decisionDiffToActual] || []),
              currentDecision
            ];
          }
        );

        const smallestDiff = Math.min(
          ...Object.keys(decisionsPerDiff).map(key => Number(key))
        );
        const isEqualDecisionId = (decisionId: number) =>
          decisionId === decision.generatedId;

        const earliestDecision =
          decisionsPerDiff[smallestDiff].length === 1
            ? decisionsPerDiff[smallestDiff][0]
            : decisionsPerDiff[smallestDiff].sort((a, b) =>
                a.period.startDate.diff(b.period.startDate)
              )[0];

        isEqualDecisionId(earliestDecision.generatedId) &&
          actualsDuringDecision.push(actual);
      });

      return actualsDuringDecision;
    }
  };

  return (
    <CardBodyWithStatus
      type={getDecisionStatusLabelType(statusCategory)}
      pattern={getDecisionPattern(period.type as JobProtectedLeaveStatus)}
    >
      <ContentLayout direction="column" noPadding={true}>
        <Row align="top">
          {isIntermittentType(period.type as JobProtectedLeaveStatus) ? (
            <IntermittentDecisionProperties
              decision={decision}
              leavePlan={leavePlan}
              actuals={getActualsForDecision()}
            />
          ) : (
            <DecisionProperties decision={decision} />
          )}
        </Row>
      </ContentLayout>
    </CardBodyWithStatus>
  );
};
