/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { StatusType, StatusPattern } from 'fineos-common';

import {
  DecisionStatusCategory,
  JobProtectedLeaveStatus
} from '../../../../shared';

export const getDecisionStatusLabelType = (
  statusCategory: DecisionStatusCategory
): StatusType => {
  switch (statusCategory) {
    case DecisionStatusCategory.APPROVED:
      return StatusType.SUCCESS;
    case DecisionStatusCategory.DENIED:
      return StatusType.DANGER;
    case DecisionStatusCategory.PENDING:
      return StatusType.WARNING;
    case DecisionStatusCategory.CANCELLED:
      return StatusType.NEUTRAL;
    default:
      return StatusType.BLANK;
  }
};

export const getDecisionStatusLabel = (
  statusCategory: DecisionStatusCategory
): string => {
  switch (statusCategory) {
    case DecisionStatusCategory.APPROVED:
      return 'DECISION_STATUS_CATEGORY.APPROVED';
    case DecisionStatusCategory.DENIED:
      return 'DECISION_STATUS_CATEGORY.DENIED';
    case DecisionStatusCategory.PENDING:
      return 'DECISION_STATUS_CATEGORY.PENDING';
    case DecisionStatusCategory.CANCELLED:
      return 'DECISION_STATUS_CATEGORY.CANCELLED';
    default:
      return statusCategory;
  }
};

export const getDecisionPattern = (
  type: JobProtectedLeaveStatus
): StatusPattern => {
  switch (type) {
    case JobProtectedLeaveStatus.EPISODIC:
      return StatusPattern.ANGLED;
    case JobProtectedLeaveStatus.REDUCED_SCHEDULE:
      return StatusPattern.HALVED;
    default:
      return StatusPattern.FULL;
  }
};

export const isIntermittentType = (type: JobProtectedLeaveStatus) =>
  type === JobProtectedLeaveStatus.EPISODIC;
