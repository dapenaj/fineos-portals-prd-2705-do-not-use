/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { ActualAbsencePeriod } from 'fineos-js-api-client';
import {
  Button,
  ButtonType,
  FormattedMessage,
  Modal,
  ModalFooter,
  ModalWidth,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';

import { ActualAbsencePeriodsTable } from './actual-absence-periods-table/actual-absence-periods-table.component';
import { PERIODS_NUMBER_THRESHOLD } from './actual-absence-periods.type';
import styles from './actual-absence-periods.module.scss';

type ActualAbsencePeriodsProps = {
  leavePlanName: string;
  actualAbsencePeriods: ActualAbsencePeriod[];
  leaveRequestReasonName: string;
};

const useThemedStyles = createThemedStyles(theme => ({
  modalTitle: {
    color: theme.colorSchema.neutral9,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const ActualAbsencePeriods: React.FC<ActualAbsencePeriodsProps> = ({
  leavePlanName,
  actualAbsencePeriods,
  leaveRequestReasonName
}) => {
  const [areDetailsVisible, setAreDetailsVisible] = useState(false);
  const themedStyles = useThemedStyles();
  return (
    <div className={styles.wrapper}>
      <ActualAbsencePeriodsTable
        data-test-el="limited-absence-periods-table"
        actualAbsencePeriods={actualAbsencePeriods}
      />

      {actualAbsencePeriods.length > PERIODS_NUMBER_THRESHOLD && (
        <>
          <FormattedMessage
            ariaLabelName={{
              descriptor: {
                id:
                  'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON_ARIA_LABEL'
              }
            }}
            as={Button}
            buttonType={ButtonType.LINK}
            className={styles.showMoreButton}
            id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON"
            onClick={() => setAreDetailsVisible(true)}
          />
          <Modal
            ariaLabelId="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON_ARIA_LABEL"
            header={
              <span
                data-test-el="actual-absence-periods-modal-title"
                className={themedStyles.modalTitle}
              >
                {leavePlanName} - {leaveRequestReasonName}
              </span>
            }
            width={ModalWidth.EXTRA_WIDE}
            visible={areDetailsVisible}
            onCancel={() => setAreDetailsVisible(false)}
          >
            <>
              <ActualAbsencePeriodsTable
                data-test-el="modal-absence-periods-table"
                className={styles.periodsTable}
                actualAbsencePeriods={actualAbsencePeriods}
                isSimpleVersion={false}
              />
              <ModalFooter>
                <FormattedMessage
                  className={styles.closeButton}
                  as={Button}
                  id="FINEOS_COMMON.GENERAL.CLOSE"
                  onClick={() => setAreDetailsVisible(false)}
                />
              </ModalFooter>
            </>
          </Modal>
        </>
      )}
    </div>
  );
};
