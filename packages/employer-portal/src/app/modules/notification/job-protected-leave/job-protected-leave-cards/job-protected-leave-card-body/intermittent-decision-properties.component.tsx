/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { Row, Col } from 'antd';
import { isEmpty as _isEmpty } from 'lodash';
import { ActualAbsencePeriod } from 'fineos-js-api-client';
import {
  PropertyItem,
  RichFormattedMessage,
  FormattedMessage,
  Button,
  ButtonType,
  StatusLabel,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';

import {
  EnhancedDecision,
  DecisionStatusCategory,
  DecisionReasonName,
  DecisionQualifierOne,
  JobProtectedLeavePlan
} from '../../../../../shared';
import {
  getDecisionStatusLabel,
  getDecisionStatusLabelType
} from '../job-protective-leave-cards.util';

import { getPregnancyQualifier } from './job-protective-leave-body-utils';
import { ActualAbsencePeriods } from './actual-absence-periods/actual-absence-periods.component';
import styles from './job-protected-leave-card-body.module.scss';

type IntermittentDecisionPropertiesProps = {
  decision: EnhancedDecision;
  leavePlan: JobProtectedLeavePlan;
  actuals: ActualAbsencePeriod[];
};

const useThemedStyles = createThemedStyles(theme => ({
  text: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const IntermittentDecisionProperties: React.FC<IntermittentDecisionPropertiesProps> = ({
  decision: { period, statusCategory },
  leavePlan,
  actuals
}) => {
  const [isAbsenceDetailsVisible, setIsAbsenceDetailsVisible] = useState<
    boolean
  >(false);
  const themedStyles = useThemedStyles();
  return (
    <div className={styles.intermittentWrapper}>
      <Row>
        <Col xs={24} sm={12} md={6}>
          <PropertyItem
            className={styles.property}
            label={
              <FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON" />
            }
          >
            <span className={themedStyles.text}>
              {period.leaveRequest.reasonName}
              {period.leaveRequest.reasonName ===
                DecisionReasonName.PREGNANCY &&
                getPregnancyQualifier(
                  period.leaveRequest.qualifier1 as DecisionQualifierOne
                )}
            </span>
          </PropertyItem>
        </Col>
        <Col xs={24} sm={12} md={6}>
          <PropertyItem
            className={styles.property}
            label={
              <FormattedMessage
                id={
                  statusCategory === DecisionStatusCategory.PENDING
                    ? 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.REQUESTED_BETWEEN'
                    : 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.APPROVED_BETWEEN'
                }
              />
            }
          >
            <RichFormattedMessage
              className={themedStyles.text}
              id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.BETWEEN_DATES"
              values={{
                start: period.startDate,
                end: period.endDate
              }}
            />
          </PropertyItem>
        </Col>
        <Col xs={24} sm={12} md={6} />
        <Col xs={24} sm={12} md={6}>
          <StatusLabel type={getDecisionStatusLabelType(statusCategory)}>
            <FormattedMessage id={getDecisionStatusLabel(statusCategory)} />
          </StatusLabel>
        </Col>
      </Row>

      {!_isEmpty(actuals) && (
        <FormattedMessage
          as={Button}
          buttonType={ButtonType.LINK}
          id={
            isAbsenceDetailsVisible
              ? 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.HIDE_ACTUAL_ABSENCE_PERIODS_BUTTON'
              : 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
          }
          className={styles.intermittentButton}
          onClick={() => setIsAbsenceDetailsVisible(isVisible => !isVisible)}
        />
      )}

      {isAbsenceDetailsVisible && (
        <ActualAbsencePeriods
          leavePlanName={leavePlan.leavePlanName}
          actualAbsencePeriods={actuals}
          leaveRequestReasonName={period.leaveRequest.reasonName}
        />
      )}
    </div>
  );
};
