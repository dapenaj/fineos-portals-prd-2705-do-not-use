import {
  ActualAbsencePeriodsService,
  ActualAbsencePeriod
} from 'fineos-js-api-client';

export const fetchActualAbsencePeriods = (
  absenceId: string
): Promise<ActualAbsencePeriod[]> =>
  ActualAbsencePeriodsService.getInstance().fetchActualAbsencePeriods(
    absenceId
  );
