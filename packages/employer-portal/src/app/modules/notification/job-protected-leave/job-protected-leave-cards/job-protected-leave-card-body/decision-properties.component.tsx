/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Col } from 'antd';
import {
  PropertyItem,
  FormattedDate,
  FormattedMessage,
  StatusLabel,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';

import {
  EnhancedDecision,
  DecisionStatusCategory,
  DecisionReasonName,
  DecisionQualifierOne,
  JobProtectedLeaveStatus
} from '../../../../../shared';
import {
  getDecisionStatusLabel,
  getDecisionStatusLabelType
} from '../job-protective-leave-cards.util';

import { getPregnancyQualifier } from './job-protective-leave-body-utils';
import styles from './job-protected-leave-card-body.module.scss';

type DecisionPropertiesProps = {
  decision: EnhancedDecision;
};

const useThemedStyles = createThemedStyles(theme => ({
  text: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const DecisionProperties: React.FC<DecisionPropertiesProps> = ({
  decision: {
    period: {
      type,
      leaveRequest: { reasonName, qualifier1 },
      startDate,
      endDate
    },
    statusCategory
  }
}) => {
  const themedStyles = useThemedStyles();
  return (
    <>
      <Col xs={24} sm={12} md={6}>
        <PropertyItem
          label={
            <FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON" />
          }
          className={styles.property}
        >
          <span className={themedStyles.text}>
            {reasonName}
            {reasonName === DecisionReasonName.PREGNANCY &&
              getPregnancyQualifier(qualifier1 as DecisionQualifierOne)}
          </span>
        </PropertyItem>
      </Col>
      <Col xs={24} sm={12} md={6}>
        <PropertyItem
          label={
            <FormattedMessage
              id={
                type === JobProtectedLeaveStatus.REDUCED_SCHEDULE
                  ? 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.PERIOD_BEGINS'
                  : 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEAVE_BEGINS'
              }
            />
          }
          className={styles.property}
        >
          <FormattedDate className={themedStyles.text} date={startDate} />
        </PropertyItem>
      </Col>
      <Col xs={24} sm={12} md={6}>
        <PropertyItem
          label={
            <FormattedMessage
              id={
                statusCategory === DecisionStatusCategory.APPROVED
                  ? 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.APPROVED_THROUGH'
                  : 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.REQUESTED_THROUGH'
              }
            />
          }
          className={styles.property}
        >
          <FormattedDate className={themedStyles.text} date={endDate} />
        </PropertyItem>
      </Col>
      <Col xs={24} sm={12} md={6}>
        <StatusLabel type={getDecisionStatusLabelType(statusCategory)}>
          <FormattedMessage id={getDecisionStatusLabel(statusCategory)} />
        </StatusLabel>
      </Col>
    </>
  );
};
