/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo } from 'react';
import { ActualAbsencePeriod, CaseHandler } from 'fineos-js-api-client';
import { CollapsibleCard } from 'fineos-common';

import { JobProtectedLeavePlan, PeriodStatus } from '../../../../shared';
import { JobProtectedLeaveCardHeader } from './job-protected-leave-card-header';
import { JobProtectedLeaveCardBody } from './job-protected-leave-card-body';
import {
  DecisionWithGeneratedId,
  LeavePlanWithTemporaryIdDecisions
} from './job-protected-leave-cards.type';

import styles from './job-protected-leave-cards.module.scss';

type JobProtectedLeaveCardsProps = {
  leavePlans: JobProtectedLeavePlan[];
  caseHandler: CaseHandler;
  actualsPerLeavePlan: Record<string, ActualAbsencePeriod[]>;
};

export const JobProtectedLeaveCards: React.FC<JobProtectedLeaveCardsProps> = ({
  leavePlans,
  caseHandler,
  actualsPerLeavePlan
}) => {
  const decisionsSortedByStartDate = (
    leavePlan: LeavePlanWithTemporaryIdDecisions
  ) =>
    leavePlan.decisions.sort((a, b) =>
      b.period.startDate.diff(a.period.startDate)
    );

  // As API does not provide any unique id for a decision, we need to prepare it by ourselves.
  // It will be used later in logic of assigning a actual absence period to decision
  // (see JobProtectedLeaveCardBody component)
  const leavePlansWithDecisionTemporaryId: LeavePlanWithTemporaryIdDecisions[] = useMemo(
    () =>
      leavePlans.map(leavePlan => ({
        ...leavePlan,
        decisions: leavePlan.decisions.map((decision, index) => ({
          ...decision,
          generatedId: index
        }))
      })),
    [leavePlans]
  );

  return (
    <div className={styles.wrapper} data-test-el="job-protected-leave-cards">
      {leavePlansWithDecisionTemporaryId.map((leavePlan, index) => {
        return (
          <CollapsibleCard
            title={({ isCollapsed }: { isCollapsed: boolean }) => (
              <JobProtectedLeaveCardHeader
                isCollapsed={isCollapsed}
                leavePlan={leavePlan}
                caseHandler={caseHandler}
              />
            )}
            key={`${leavePlan.leavePlanName}_${index}`}
          >
            {decisionsSortedByStartDate(leavePlan).map(
              (decision: DecisionWithGeneratedId) => (
                <JobProtectedLeaveCardBody
                  decision={decision}
                  key={decision.generatedId}
                  leavePlan={leavePlan}
                  actualsPerLeavePlan={actualsPerLeavePlan}
                />
              )
            )}
          </CollapsibleCard>
        );
      })}
    </div>
  );
};
