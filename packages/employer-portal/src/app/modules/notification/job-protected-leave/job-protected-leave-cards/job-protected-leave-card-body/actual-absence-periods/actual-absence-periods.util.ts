/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { StatusType } from 'fineos-common';

import {
  ManagerDecision,
  ActualAbsencePeriodStatus
} from './actual-absence-periods.type';

export const mapManagerDecisionToStatus = (
  managerDecision: ManagerDecision
) => {
  switch (managerDecision) {
    case ManagerDecision.UNKNOWN:
      return StatusType.WARNING;
    case ManagerDecision.YES:
      return StatusType.SUCCESS;
    case ManagerDecision.NO:
      return StatusType.DANGER;
  }
};

export const getStatusFromDecisionStatus = (
  status: ActualAbsencePeriodStatus
) => {
  switch (status) {
    case ActualAbsencePeriodStatus.APPROVED:
      return StatusType.SUCCESS;
    case ActualAbsencePeriodStatus.DENIED:
      return StatusType.DANGER;
    case ActualAbsencePeriodStatus.PENDING:
      return StatusType.WARNING;
    case ActualAbsencePeriodStatus.CANCELLED:
      return StatusType.NEUTRAL;
    default:
      return StatusType.BLANK;
  }
};
