/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { ComponentProps, useRef, useState } from 'react';
import classNames from 'classnames';
import { ActualAbsencePeriod } from 'fineos-js-api-client';
import {
  FormattedMessage,
  FormattedDate,
  Table,
  TABLE_SORT_ORDERS,
  StatusLabel,
  createThemedStyles,
  textStyleToCss,
  SortOrder,
  Dialog,
  FilterDropdownProps,
  ButtonType,
  Button,
  Checkbox,
  CheckboxChangeEvent,
  getFocusedElementShadow
} from 'fineos-common';
import { faFilter } from '@fortawesome/free-solid-svg-icons';

import { FilterIconElement } from '../../../../../../../shared';
import {
  mapManagerDecisionToStatus,
  getStatusFromDecisionStatus
} from '../actual-absence-periods.util';
import {
  EpisodicPeriodBasis,
  ManagerDecision,
  PERIODS_NUMBER_THRESHOLD,
  ActualAbsencePeriodStatus
} from '../actual-absence-periods.type';

import styles from './actual-absence-periods-table.module.scss';

type ActualAbsencePeriodsTableProps = {
  isSimpleVersion?: boolean;
  actualAbsencePeriods: ActualAbsencePeriod[];
};

const useThemedStyles = createThemedStyles(theme => ({
  dataItem: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  },
  filterDropdown: {
    color: theme.colorSchema.neutral8,
    backgroundColor: theme.colorSchema.neutral1,
    ...textStyleToCss(theme.typography.baseText),
    '&:focus': {
      color: theme.colorSchema.neutral7,
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  },
  footer: {
    borderTop: `1px solid ${theme.colorSchema.neutral5}`
  },
  checkbox: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  actionTableHeader: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  }
}));

const POPCONFIRM_WIDTH = 250;
const DECISION_TABLE_HEAD_WIDTH = '25%';
const TABLE_SCROLL_WRAPPER_HEIGHT = 355;

const FILTERS = [
  {
    text: <FormattedMessage id={`ENUM_DOMAINS.ACTUAL_STATUS.APPROVED`} />,
    value: ActualAbsencePeriodStatus.APPROVED
  },
  {
    text: <FormattedMessage id={`ENUM_DOMAINS.ACTUAL_STATUS.CANCELLED`} />,
    value: ActualAbsencePeriodStatus.CANCELLED
  },
  {
    text: <FormattedMessage id={`ENUM_DOMAINS.ACTUAL_STATUS.DENIED`} />,
    value: ActualAbsencePeriodStatus.DENIED
  },
  {
    text: <FormattedMessage id={`ENUM_DOMAINS.ACTUAL_STATUS.PENDING`} />,
    value: ActualAbsencePeriodStatus.PENDING
  }
];

export const ActualAbsencePeriodsTable: React.FC<ActualAbsencePeriodsTableProps &
  ComponentProps<typeof Table>> = ({
  className,
  isSimpleVersion = true,
  actualAbsencePeriods,
  ...props
}) => {
  const themedStyles = useThemedStyles();
  const [isVisible, setIsVisible] = useState(false);
  const triggerRef = useRef<HTMLDivElement>(null);
  const [popconfirmPosition, setPopconfirmPosition] = useState({
    top: 0,
    left: 0
  });
  const [filters, setFilters] = useState<string[]>([]);
  const [isFiltered, setIsFiltered] = useState<boolean>(false);

  const handleFocusTrigger = () => {
    const button = triggerRef.current?.firstElementChild as HTMLButtonElement;
    button.focus();
  };

  const getSortedPeriods = (periods: ActualAbsencePeriod[]) => {
    const sortedPeriods = periods.sort((first, second) =>
      second.actualDate.diff(first.actualDate)
    );
    return periods.length <= PERIODS_NUMBER_THRESHOLD || !isSimpleVersion
      ? sortedPeriods
      : sortedPeriods.slice(0, PERIODS_NUMBER_THRESHOLD);
  };

  const getPeriodDateColumn = () => {
    const baseProperties = {
      title: (
        <FormattedMessage
          className={classNames(
            styles.actionTableHeader,
            themedStyles.actionTableHeader
          )}
          as={Button}
          buttonType={ButtonType.LINK}
          id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DATE"
          ariaLabelName={{
            descriptor: {
              id:
                'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DATE_SORTER'
            }
          }}
        />
      ),
      render: (period: ActualAbsencePeriod) => (
        <FormattedDate
          className={themedStyles.dataItem}
          date={period.actualDate}
          data-test-el="actual-absence-period-date"
        />
      )
    };

    if (!isSimpleVersion) {
      return {
        ...baseProperties,
        // the way of sorter arguments typing is forced by antd
        sorter: (first: {}, second: {}) =>
          (first as ActualAbsencePeriod).actualDate.diff(
            (second as ActualAbsencePeriod).actualDate
          ),
        sortDirections: TABLE_SORT_ORDERS,
        defaultSortOrder: SortOrder.DESCEND
      };
    }

    return baseProperties;
  };

  const getDurationDataElement = (
    periodBasisName: EpisodicPeriodBasis,
    duration: number
  ) => {
    switch (periodBasisName) {
      case EpisodicPeriodBasis.MINUTES: {
        const MINUTES_IN_HOUR = 60;
        const hours = Math.floor(duration / MINUTES_IN_HOUR);
        const minutes = duration % MINUTES_IN_HOUR;
        return (
          <span data-test-el="actual-absence-period-duration">
            <FormattedMessage
              id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_HOURS"
              values={{ hours }}
              className={themedStyles.dataItem}
            />
            {!!minutes && (
              <>
                {' '}
                <FormattedMessage
                  id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_MINUTES"
                  values={{ minutes }}
                  className={themedStyles.dataItem}
                />
              </>
            )}
          </span>
        );
      }
      case EpisodicPeriodBasis.HOURS: {
        return (
          <FormattedMessage
            data-test-el="actual-absence-period-duration"
            id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_HOURS"
            values={{ hours: duration }}
            className={themedStyles.dataItem}
          />
        );
      }
      case EpisodicPeriodBasis.DAYS: {
        return (
          <FormattedMessage
            data-test-el="actual-absence-period-duration"
            id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_DAYS"
            values={{ days: duration }}
            className={themedStyles.dataItem}
          />
        );
      }
      default: {
        return (
          <FormattedMessage
            data-test-el="actual-absence-period-duration"
            id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.NO_TIME_ENTERED"
            className={themedStyles.dataItem}
          />
        );
      }
    }
  };

  const periodDurationColumn = {
    title: (
      <FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DURATION" />
    ),
    render: (data: ActualAbsencePeriod) =>
      getDurationDataElement(
        data.episodePeriodBasis.name as EpisodicPeriodBasis,
        data.episodePeriodDuration
      )
  };

  const filterIcon = () => (
    <div ref={triggerRef}>
      <FilterIconElement
        onClick={() => {
          const triggerEl = triggerRef.current;
          if (triggerEl) {
            // get trigger position and width
            const { bottom, left, width } = triggerEl.getBoundingClientRect();
            // position popconfirm
            setPopconfirmPosition({
              top: bottom + 20,
              left: left - (POPCONFIRM_WIDTH - width) / 2
            });
          }
          setIsVisible(true);
        }}
        isActive={isFiltered}
        icon={faFilter}
        id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DECISION_FILTER_ICON"
      />
    </div>
  );

  const filterDropdown = ({
    setSelectedKeys,
    confirm,
    clearFilters
  }: FilterDropdownProps) => (
    <Dialog
      data-test-el="absence-periods-table-checkbox-filter"
      dialogClassName={classNames(
        styles.filterDropdown,
        themedStyles.filterDropdown
      )}
      onCancel={() => {
        setIsVisible(false);
        handleFocusTrigger();
      }}
      onKeyDown={(event: React.KeyboardEvent) => {
        if (event.key === 'Escape') {
          setIsVisible(false);
          handleFocusTrigger();
        }
      }}
      tabIndex={0}
      style={{
        top: popconfirmPosition.top,
        left: popconfirmPosition.left
      }}
      visible={isVisible}
      ariaLabelId="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DECISION_FILTER"
    >
      <ul className={styles.filters}>
        {FILTERS.map(
          (
            {
              text,
              value
            }: { text: React.ReactElement; value: ActualAbsencePeriodStatus },
            index: number
          ) => (
            <li key={index}>
              <Checkbox
                data-test-el={value}
                className={themedStyles.checkbox}
                onChange={({ target }: CheckboxChangeEvent) => {
                  const currentKey = target.value;
                  const isKeySelected = filters.find(
                    (key: string) => key === currentKey
                  );
                  setFilters(
                    isKeySelected
                      ? filters.filter((key: string) => key !== currentKey)
                      : [...filters, currentKey]
                  );
                }}
                value={value}
                checked={!!filters.find((key: string) => key === value)}
              >
                {text}
              </Checkbox>
            </li>
          )
        )}
      </ul>
      <div className={classNames(styles.footer, themedStyles.footer)}>
        <FormattedMessage
          disabled={!isFiltered}
          as={Button}
          buttonType={ButtonType.LINK}
          onClick={() => {
            setSelectedKeys([]);
            clearFilters && clearFilters();
            setFilters([]);
            setIsFiltered(false);
            setIsVisible(false);
            confirm();
            handleFocusTrigger();
          }}
          id="FINEOS_COMMON.GENERAL.RESET"
        />
        <FormattedMessage
          as={Button}
          onClick={() => {
            setSelectedKeys(filters);
            setIsFiltered(true);
            setIsVisible(false);
            confirm();
            handleFocusTrigger();
          }}
          id="FINEOS_COMMON.GENERAL.OK"
        />
      </div>
    </Dialog>
  );

  const getPeriodDecisionColumn = () => {
    const baseProperties = {
      title: (
        <FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DECISION" />
      ),
      render: (data: ActualAbsencePeriod) => (
        <StatusLabel
          type={getStatusFromDecisionStatus(
            data.status.name as ActualAbsencePeriodStatus
          )}
        >
          <FormattedMessage
            data-test-el="actual-absence-period-decision"
            id={`ENUM_DOMAINS.ACTUAL_STATUS.${data.status.name.toUpperCase()}`}
          />
        </StatusLabel>
      ),
      width: DECISION_TABLE_HEAD_WIDTH
    };

    if (!isSimpleVersion) {
      return {
        ...baseProperties,
        // the way of sorter arguments typing is forced by antd
        onFilter: (value: string | number | boolean, record: {}) =>
          (record as ActualAbsencePeriod).status.name === value,
        filterDropdown,
        filterIcon
      };
    }
    return baseProperties;
  };

  return (
    <Table
      className={classNames(className, styles.periodsTable)}
      scroll={{
        y: isSimpleVersion ? undefined : TABLE_SCROLL_WRAPPER_HEIGHT
      }}
      loading={false}
      pagination={false}
      dataSource={getSortedPeriods(actualAbsencePeriods)}
      rowKey={(record: unknown) => (record as ActualAbsencePeriod).id}
      columns={[
        { ...getPeriodDateColumn() },
        { ...periodDurationColumn },
        { ...getPeriodDecisionColumn() }
      ]}
      {...props}
    />
  );
};
