/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  StatusLabel,
  ContentLayout,
  HandlerInfo,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { Row, Col } from 'antd';
import classNames from 'classnames';
import { CaseHandler } from 'fineos-js-api-client';

import { JobProtectedLeavePlan } from '../../../../../shared';
import {
  getDecisionStatusLabelType,
  getDecisionStatusLabel
} from '../job-protective-leave-cards.util';

import { countPeriodDecisionsByStatusCategory } from './job-protected-leave-card-header.util';
import { PaidLeavePlanIndicator } from './paid-leave-plan-indicator.component';
import styles from './job-protected-leave-card-header.module.scss';

type JobProtectedLeaveCardHeaderProps = {
  isCollapsed: boolean;
  leavePlan: JobProtectedLeavePlan;
  caseHandler: CaseHandler;
};

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const JobProtectedLeaveCardHeader: React.FC<JobProtectedLeaveCardHeaderProps> = ({
  isCollapsed,
  leavePlan: { leavePlanName, decisions },
  caseHandler: { name, telephoneNo }
}) => {
  const themedStyles = useThemedStyles();
  return (
    <div data-test-el="job-protected-leave-card-header">
      {isCollapsed ? (
        <>
          <div
            className={classNames(themedStyles.header, styles.header)}
            data-test-el="job-protected-leave-plan-header"
          >
            <span className={styles.headerTitle}>{leavePlanName}</span>
            <PaidLeavePlanIndicator decisions={decisions} />
          </div>
          <div className={styles.wrapper}>
            {countPeriodDecisionsByStatusCategory(decisions).map(
              ({ status, count }, index) => (
                <StatusLabel
                  key={index}
                  type={getDecisionStatusLabelType(status)}
                >
                  <FormattedMessage id={getDecisionStatusLabel(status)} /> (
                  {count})
                </StatusLabel>
              )
            )}
          </div>
        </>
      ) : (
        <ContentLayout direction="column" noPadding={true}>
          <Row align="top" data-test-el="job-protected-leave-plan-row">
            <Col xs={24} sm={12}>
              <div
                className={classNames(styles.header, themedStyles.header)}
                data-test-el="job-protected-leave-plan-header"
              >
                <span className={styles.headerTitle}>{leavePlanName}</span>
                <PaidLeavePlanIndicator decisions={decisions} />
              </div>
            </Col>
            <Col xs={24} sm={12}>
              <HandlerInfo
                name={name}
                phoneNumber={telephoneNo}
                data-test-el="job-protected-leave-plan-header-properties"
              />
            </Col>
          </Row>
        </ContentLayout>
      )}
    </div>
  );
};
