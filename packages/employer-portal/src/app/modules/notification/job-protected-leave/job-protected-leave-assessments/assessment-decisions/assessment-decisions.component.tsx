/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Decision } from 'fineos-js-api-client';
import moment from 'moment';
import {
  createThemedStyles,
  textStyleToCss,
  FormattedDate,
  FormattedMessage
} from 'fineos-common';

import styles from './assessment-decisions.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  listItem: {
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type AssessmentDecisionsProps = {
  decisions: Decision[];
};

export const AssessmentDecisions: React.FC<AssessmentDecisionsProps> = ({
  decisions
}) => {
  const themedStyles = useThemedStyles();

  return (
    <ul className={styles.list}>
      {decisions.map(
        (
          {
            period: {
              leaveRequest: { reasonName },
              startDate,
              endDate
            }
          },
          index
        ) => (
          <li key={index} className={themedStyles.listItem}>
            <FormattedMessage
              id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.STARTING_ON"
              values={{
                reason: <b>{reasonName}</b>,
                date: (
                  <b>
                    <FormattedDate date={startDate} />
                  </b>
                )
              }}
            />

            {moment(endDate).isValid() && (
              <FormattedMessage
                id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ENDING_ON"
                values={{
                  date: (
                    <b>
                      <FormattedDate date={endDate} />
                    </b>
                  )
                }}
              />
            )}
          </li>
        )
      )}
    </ul>
  );
};
