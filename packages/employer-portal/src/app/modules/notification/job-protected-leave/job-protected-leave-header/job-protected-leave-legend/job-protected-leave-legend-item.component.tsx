/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { createThemedStyles, FormattedMessage } from 'fineos-common';

import { JobProtectedLeaveStatus } from '../../../../../shared';
import { getStatusLabelTranslation } from '../../job-protected-leave.util';

import styles from './job-protected-leave-legend-item.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  icon: {
    background: theme.colorSchema.neutral7
  },
  angle: {
    background: `linear-gradient(to bottom left, ${theme.colorSchema.neutral7} 50%, ${theme.colorSchema.neutral3} 50%)`
  },
  half: {
    background: `linear-gradient(to left, ${theme.colorSchema.neutral7} 50%, ${theme.colorSchema.neutral3} 50%)`
  },
  legendStatus: {
    color: theme.colorSchema.neutral8
  }
}));

type JobProtectedLeaveLegendItemProps = {
  status: JobProtectedLeaveStatus;
};

const getBorderStyle = (
  status: JobProtectedLeaveStatus,
  themedStyles: Record<'icon' | 'angle' | 'half', string>
) => {
  switch (status) {
    case JobProtectedLeaveStatus.EPISODIC:
      return themedStyles.angle;
    case JobProtectedLeaveStatus.REDUCED_SCHEDULE:
      return themedStyles.half;
    default:
      return '';
  }
};

export const JobProtectedLeaveLegendItem: React.FC<JobProtectedLeaveLegendItemProps> = ({
  status
}) => {
  const themedStyles = useThemedStyles();

  return (
    <div className={styles.wrapper}>
      <div
        className={classNames(
          styles.icon,
          themedStyles.icon,
          getBorderStyle(status, themedStyles)
        )}
      />
      <FormattedMessage
        className={themedStyles.legendStatus}
        id={getStatusLabelTranslation(status)}
      />
    </div>
  );
};
