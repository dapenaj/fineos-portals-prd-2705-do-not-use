/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  PanelHeader,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage
} from 'fineos-common';
import { SafetyOutlined } from '@ant-design/icons';

import { JobProtectedLeaveLegend } from './job-protected-leave-legend';
import styles from './job-protected-leave-header.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss({
      ...theme.typography.panelTitle,
      // Mockups showed the panel title size as 22px, but the ui overview listed it as 18px
      // - Fernanda confirmed it should follow page title size (24px)
      fontSize: theme.typography.pageTitle.fontSize
    })
  },
  icon: {
    color: theme.colorSchema.neutral5
  },
  legend: {
    fontSize: theme.typography.smallText.fontSize,
    color: theme.colorSchema.neutral7
  }
}));

export const JobProtectedLeaveHeader = () => {
  const themedStyles = useThemedStyles();

  return (
    <PanelHeader className={classNames(styles.header, themedStyles.header)}>
      <SafetyOutlined className={classNames(styles.icon, themedStyles.icon)} />
      <FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.PANEL_TITLE" />

      <div
        className={classNames(styles.legend, themedStyles.legend)}
        data-test-el="job-protected-leave-legend"
      >
        <JobProtectedLeaveLegend />
      </div>
    </PanelHeader>
  );
};
