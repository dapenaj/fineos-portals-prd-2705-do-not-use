/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useEffect, useState } from 'react';
import {
  Panel,
  withPermissions,
  ViewNotificationPermissions,
  ContentRenderer,
  WarningState,
  ConfigurableText,
  FormattedMessage
} from 'fineos-common';
import { cloneDeep as _cloneDeep, isEmpty as _isEmpty } from 'lodash';

import { EnhancedAbsencePeriodDecisions } from '../../../shared';

import {
  groupByLeavePlanName,
  groupByNoLeavePlanName
} from './job-protected-leave.util';
import { JobProtectedLeaveHeader } from './job-protected-leave-header';
import { JobProtectedLeaveCards } from './job-protected-leave-cards';
import { JobProtectedAssessments } from './job-protected-leave-assessments';
import styles from './job-protected-leave.module.scss';
import { fetchActualAbsencePeriods } from './job-protected-leave-cards/job-protected-leave-card-body/actual-absence-periods/actual-absence-periods.api';
import { ActualAbsencePeriod } from 'fineos-js-api-client';
import { ActualAbsencePeriodStatus } from './job-protected-leave-cards/job-protected-leave-card-body/actual-absence-periods/actual-absence-periods.type';

type JobProtectedLeaveProps = {
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[] | null;
  isLoading: boolean;
};

export const JobProtectedLeaveComponent: React.FC<JobProtectedLeaveProps> = ({
  enhancedAbsencePeriodDecisions,
  isLoading
}) => {
  const [actualsPerLeavePlan, setActualsPerLeavePlan] = useState(
    {} as Record<string, ActualAbsencePeriod[]>
  );

  const [isLoadingActuals, setLoadingActuals] = useState(true);

  useEffect(() => {
    async function fetchActuals() {
      if (
        enhancedAbsencePeriodDecisions &&
        enhancedAbsencePeriodDecisions.length
      ) {
        const result = await fetchActualAbsencePeriods(
          enhancedAbsencePeriodDecisions[0].absenceId
        );
        const {
          decisions
        } = enhancedAbsencePeriodDecisions[0].absencePeriodDecisions;

        const actuals: Record<string, any> = {};
        result.forEach(actual => {
          const decisionMatched = decisions.filter(d => {
            if (
              d.period.leavePlan &&
              d.period.leavePlan.adjudicationStatus === 'Rejected'
            ) {
              return false;
            }

            const a = actual.id.split('-');
            const paddedIdPart = a[1].padStart(10, '0');
            const paddedActualId = `${a[0]}-${paddedIdPart}`;

            return d.period.periodReference.includes(paddedActualId);
          });

          decisionMatched.forEach(decision => {
            const actualCopy = _cloneDeep(actual);

            if (actualCopy && decision && decision.period.leavePlan) {
              if (!actuals[decision.period.leavePlan.id]) {
                actuals[decision.period.leavePlan.id] = [];
              }

              if (
                actualCopy.status.name === ActualAbsencePeriodStatus.APPROVED &&
                decision.period.timeDecisionStatus === 'Denied'
              ) {
                actualCopy.status.name = decision.period.timeDecisionStatus;
              }

              actuals[decision.period.leavePlan.id].push(actualCopy);
            }
          });
        });

        setActualsPerLeavePlan(actuals);
        setLoadingActuals(false);
      }
    }

    fetchActuals();
  }, [enhancedAbsencePeriodDecisions]);

  return (
    <ContentRenderer
      isEmpty={_isEmpty(enhancedAbsencePeriodDecisions)}
      isLoading={isLoading || isLoadingActuals}
      data-test-el="job-protected-leave"
    >
      {enhancedAbsencePeriodDecisions?.map(
        ({ absenceId, absencePeriodDecisions, caseHandler }, index) => {
          const { decisions } = absencePeriodDecisions;

          const leavePlans = groupByLeavePlanName(decisions);
          const leaveAssessments = groupByNoLeavePlanName(decisions);
          const showJobProtectedLeave =
            !_isEmpty(leaveAssessments) || !_isEmpty(leavePlans);

          return (
            <Panel
              data-test-el="job-protected-leave-panel"
              role="region"
              aria-label={
                <FormattedMessage id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.PANEL_TITLE" />
              }
              header={<JobProtectedLeaveHeader />}
              className={styles.wrapper}
              key={`${absenceId}-${index}`}
            >
              <ContentRenderer
                isEmpty={!showJobProtectedLeave}
                isLoading={false}
                emptyContent={
                  <WarningState>
                    <ConfigurableText id="NOTIFICATIONS.JOB_PROTECTED_LEAVE.EMPTY" />
                  </WarningState>
                }
              >
                <JobProtectedLeaveCards
                  actualsPerLeavePlan={actualsPerLeavePlan}
                  leavePlans={leavePlans}
                  caseHandler={caseHandler}
                />

                {!!leaveAssessments.length && (
                  <JobProtectedAssessments
                    leaveAssessments={leaveAssessments}
                  />
                )}
              </ContentRenderer>
            </Panel>
          );
        }
      )}
    </ContentRenderer>
  );
};

export const JobProtectedLeave = withPermissions(
  ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS
)(JobProtectedLeaveComponent);
