/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { Decision, GroupClientPeriod } from 'fineos-js-api-client';
import {
  chain as _chain,
  isEmpty as _isEmpty,
  orderBy as _orderBy,
  sortBy as _sortBy,
  uniqBy as _uniqBy
} from 'lodash';

import {
  DecisionStatusCategory,
  EnhancedDecision,
  JobProtectedAssessment,
  JobProtectedAssessmentStatus,
  JobProtectedLeavePlan,
  JobProtectedLeaveStatus,
  DecisionPeriodStatus
} from '../../../shared';

const getStatusCategory = (
  period: GroupClientPeriod,
  prevPeriod: GroupClientPeriod | null,
  nextPeriod: GroupClientPeriod | null
): DecisionStatusCategory => {
  const statusCategory = getDecisionStatus(period);

  return !statusCategory
    ? getDecisionStatusBasedOnPreviousAndNext(period, prevPeriod, nextPeriod)
    : statusCategory;
};

const getDecisionStatus = (
  period: GroupClientPeriod
): DecisionStatusCategory | null => {
  switch (period.leaveRequest.decisionStatus) {
    case 'Pending':
    case 'Projected':
    case 'In Review':
      return DecisionStatusCategory.PENDING;
    case 'Cancelled':
      return DecisionStatusCategory.CANCELLED;
    case 'Denied':
      return DecisionStatusCategory.DENIED;
    case 'Approved':
      return getAdjudicationStatus(period);
    default:
      return DecisionStatusCategory.NONE;
  }
};

const getAdjudicationStatus = (
  period: GroupClientPeriod
): DecisionStatusCategory | null => {
  switch (period.leavePlan?.adjudicationStatus) {
    case 'Rejected':
      return DecisionStatusCategory.DENIED;
    case 'Accepted':
      return getTimeDecisionStatus(period);
    default:
      return DecisionStatusCategory.NONE;
  }
};

const getTimeDecisionStatus = (
  period: GroupClientPeriod
): DecisionStatusCategory | null => {
  switch (period.timeDecisionStatus) {
    case 'Pending':
    case 'Pending Certification':
      return DecisionStatusCategory.PENDING;
    case 'Approved':
    case 'Time Available':
    case 'Certified':
      return DecisionStatusCategory.APPROVED;
    case 'Denied':
      return DecisionStatusCategory.DENIED;
    default:
      return getPeriodStatus(period);
  }
};

const getPeriodStatus = (
  period: GroupClientPeriod
): DecisionStatusCategory | null => {
  switch (period.status) {
    case 'Cancelled':
      return DecisionStatusCategory.CANCELLED;
    case 'Episodic':
      return DecisionStatusCategory.APPROVED;
    default:
      return null;
  }
};

const doPeriodsMatch = (
  prevPeriod: GroupClientPeriod,
  nextPeriod: GroupClientPeriod
): boolean =>
  prevPeriod.leavePlan?.name === nextPeriod.leavePlan?.name &&
  prevPeriod.leaveRequest.reasonName === nextPeriod.leaveRequest.reasonName &&
  prevPeriod.type === nextPeriod.type;

const getDecisionStatusBasedOnPreviousAndNext = (
  period: GroupClientPeriod,
  prevPeriod: GroupClientPeriod | null,
  nextPeriod: GroupClientPeriod | null
): DecisionStatusCategory => {
  if (prevPeriod && nextPeriod) {
    const prevStatus = getDecisionStatus(prevPeriod);
    const nextStatus = getDecisionStatus(nextPeriod);

    if (
      doPeriodsMatch(prevPeriod, nextPeriod) &&
      doPeriodsMatch(prevPeriod, period) &&
      prevStatus !== null &&
      prevStatus === nextStatus
    ) {
      return prevStatus;
    }
  } else if (prevPeriod) {
    const prevStatus = getDecisionStatus(prevPeriod);
    if (doPeriodsMatch(prevPeriod, period) && prevStatus !== null) {
      return prevStatus;
    }
  } else if (nextPeriod) {
    const nextStatus = getDecisionStatus(nextPeriod);
    if (doPeriodsMatch(nextPeriod, period) && nextStatus !== null) {
      return nextStatus;
    }
  }

  // If we get to this point, then we can't figure out the status and it is set to "skipped"
  // This is technically a possible scenario, but extremely unlikely.
  return DecisionStatusCategory.SKIPPED;
};

export const enhanceDecisions = (decisions: Decision[]) => {
  let orderedDecisions: Decision[] = [];
  const enhancedDecisions: EnhancedDecision[] = [];

  orderedDecisions = _orderBy(
    decisions,
    [
      (decision: Decision) => decision.period.startDate,
      (decision: Decision) => decision.period.leaveRequest.reasonName
    ],
    ['desc', 'asc']
  );

  orderedDecisions.forEach((decision, index, array) => {
    const prevDecision = index > 0 ? array[index - 1] : null;
    const nextDecision = index < array.length - 1 ? array[index + 1] : null;

    const prevPeriod = prevDecision ? prevDecision.period : null;
    const nextPeriod = nextDecision ? nextDecision.period : null;

    enhancedDecisions.push({
      ...decision,
      statusCategory: getStatusCategory(decision.period, prevPeriod, nextPeriod)
    });
  });

  return enhancedDecisions;
};

const getDecisionsWithStatusCategories = (
  decisions: Decision[]
): JobProtectedLeavePlan[] => {
  // returns true for periods that should NOT be skipped
  const filterSkippedPeriods = ({
    leavePlan,
    parentPeriodReference,
    status
  }: GroupClientPeriod) =>
    !!leavePlan &&
    leavePlan.name !== '' &&
    (parentPeriodReference === '' ||
      (parentPeriodReference !== '' && status === 'Cancelled'));

  const filterPeriodsWithParentRelatedToEpisodic = ({
    parentPeriodReference,
    status
  }: GroupClientPeriod) => {
    if (parentPeriodReference === '') {
      return true;
    }

    const parentDecision = decisions.find(
      d =>
        d.period.periodReference.split('-').join(':') === parentPeriodReference
    );

    return (
      DecisionPeriodStatus.CANCELLED !== status ||
      !(!!parentDecision && parentDecision.period.relatedToEpisodic)
    );
  };

  return (
    _chain(decisions)
      // remove decisions that do not have a leave plan name
      .filter(decision => filterSkippedPeriods(decision.period))
      .filter(decision =>
        filterPeriodsWithParentRelatedToEpisodic(decision.period)
      )
      // group by leave plan name
      .groupBy(decision => decision.period.leavePlan!.name)
      .map((value, key) => ({
        leavePlanName: key,
        decisions: enhanceDecisions(value)
      }))
      .value()
  );
};

export const isDecisionWithUnsupportedStatus = ({
  statusCategory
}: EnhancedDecision) =>
  statusCategory !== DecisionStatusCategory.SKIPPED &&
  statusCategory !== DecisionStatusCategory.NONE;

const getMergedConsecutiveLeavePlans = (leavePlans: JobProtectedLeavePlan[]) =>
  leavePlans.map(({ leavePlanName, decisions }) => ({
    leavePlanName,
    // order decisions by startDate and then reason name
    decisions: _chain(decisions)
      // remove decisions if the status category is skipped or none
      .filter(isDecisionWithUnsupportedStatus)
      // group the leave plans that have the same properties (consecutive)
      .groupBy(
        val =>
          `${val.period.leavePlan?.name}-${val.period.leaveRequest.reasonName}-${val.period.type}-${val.statusCategory}`
      )
      .map(values => mergeConsecutiveDecisions(values))
      .flattenDeep()
      .value()
  }));

export const groupByLeavePlanName = (
  decisions: Decision[]
): JobProtectedLeavePlan[] => {
  const groupedAndEnhancedLeavePlans = getDecisionsWithStatusCategories(
    decisions
  );

  return getMergedConsecutiveLeavePlans(groupedAndEnhancedLeavePlans).filter(
    value => !_isEmpty(value.decisions)
  );
};

export const decisionsSortOrder = [
  DecisionStatusCategory.APPROVED,
  DecisionStatusCategory.PENDING,
  DecisionStatusCategory.DENIED,
  DecisionStatusCategory.CANCELLED,
  DecisionStatusCategory.SKIPPED,
  DecisionStatusCategory.NONE
];

export const flatNonCancelledDecisions = (decisions: Decision[]) => {
  const flatDecisions = groupByLeavePlanName(decisions)
    .flatMap(leavePlan => leavePlan.decisions)
    .filter(
      decision => decision.statusCategory !== DecisionStatusCategory.CANCELLED
    );
  // we need to sort based on status in order to ensure that we would remove items with lower status
  const sortedDecision = _sortBy(flatDecisions, decision =>
    decisionsSortOrder.indexOf(decision.statusCategory)
  );
  // remove decision with the same periods
  return _uniqBy(
    sortedDecision,
    decision =>
      // same start / end dates
      decision.period.startDate.toISOString() +
      decision.period.endDate.toISOString() +
      // same absence reasons
      decision.period.leaveRequest.reasonName +
      decision.period.leaveRequest.qualifier1 +
      decision.period.leaveRequest.qualifier2 +
      // same period types (Fixed, Episodic, Reduced Schedule)
      decision.period.type
  );
};

export const groupByNoLeavePlanName = (
  decisions: Decision[]
): JobProtectedAssessment[] => {
  const pendingDecisionStatuses = ['Pending', 'Projected', 'In Review'];
  const deniedDecisionStatuses = ['Denied', 'Cancelled'];

  return _chain(decisions)
    .filter(
      decision =>
        !decision.period.leavePlan ||
        (decision.period.leavePlan.name === '' &&
          decision.period.parentPeriodReference === '')
    )
    .orderBy([decision => decision.period.leaveRequest.decisionStatus])
    .groupBy(({ period: { leaveRequest: { decisionStatus } } }) =>
      pendingDecisionStatuses.includes(decisionStatus)
        ? JobProtectedAssessmentStatus.PENDING
        : deniedDecisionStatuses.includes(decisionStatus)
        ? JobProtectedAssessmentStatus.DENIED
        : JobProtectedAssessmentStatus.OTHER
    )
    .map((values, key) => ({
      status: key as JobProtectedAssessmentStatus,
      decisions: values
    }))
    .value();
};

// merges an array of period decisions into a single decision
const mergeConsecutiveDecisionsFromArray = (
  decisionsToMerge: EnhancedDecision[]
): EnhancedDecision =>
  _orderBy(
    decisionsToMerge,
    decision => decision.period.startDate,
    'asc'
  ).reduce((older, newer) => ({
    ...older,
    ...newer,
    period: {
      ...older.period,
      endDate: newer.period.endDate
    }
  }));

export const mergeConsecutiveDecisions = (
  decisions: EnhancedDecision[]
): EnhancedDecision[] => {
  const decisionsToMerge: EnhancedDecision[][] = [];
  let tempDecisions: EnhancedDecision[] = [];

  // split the decisions into smaller arrays filled with CONSECUTIVE decisions
  decisions.forEach((current, index, array) => {
    const nextItem = index + 1 < array.length ? array[index + 1] : null;
    if (nextItem && doPeriodsMatch(current.period, nextItem.period)) {
      tempDecisions.push(current);
    } else if (nextItem) {
      tempDecisions.push(current);
      decisionsToMerge.push([...tempDecisions]);
      tempDecisions = [];
    } else {
      const prevItem = index - 1 > -1 ? array[index - 1] : null;
      if (prevItem && doPeriodsMatch(prevItem.period, current.period)) {
        tempDecisions.push(current);
        decisionsToMerge.push([...tempDecisions]);
      } else {
        decisionsToMerge.push([...tempDecisions]);
        decisionsToMerge.push([current]);
      }
    }
  });

  // for each group of decisions, merge them into a single decision for rendering
  const finalDecisions: EnhancedDecision[] = [];

  decisionsToMerge.forEach(group =>
    group.length > 1
      ? finalDecisions.push(mergeConsecutiveDecisionsFromArray(group))
      : finalDecisions.push(...group)
  );

  // return the final set of decisions
  return finalDecisions;
};

export const getStatusLabelTranslation = (
  status: JobProtectedLeaveStatus
): string => {
  switch (status) {
    case JobProtectedLeaveStatus.EPISODIC:
      return 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEGEND.INTERMITTENT_TIME';
    case JobProtectedLeaveStatus.REDUCED_SCHEDULE:
      return 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEGEND.REDUCED_SCHEDULE';
    case JobProtectedLeaveStatus.TIME_OFF_PERIOD:
      return 'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEGEND.CONTINUOUS_TIME';
    default:
      return '';
  }
};
