/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useEffect } from 'react';
import { Decision } from 'fineos-js-api-client';
import {
  FormButton,
  UiButton,
  ButtonType,
  FormGroup,
  Drawer,
  Form,
  TextArea,
  CheckboxGroup,
  Checkbox,
  FormattedDate,
  createThemedStyles,
  primaryLight2Color,
  textStyleToCss,
  useAsync,
  FormattedMessage,
  AsteriskInfo,
  DropdownInput,
  DrawerFooter
} from 'fineos-common';
import { Formik, Field } from 'formik';
import classNames from 'classnames';
import { CancelLeavePeriodReason } from 'fineos-js-api-client';

import { LeavePeriodChange } from '../../types';

import { changeLeavePeriods } from './edit-leave-period.component.api';
import { parseDataToApiRequestFormat } from './edit-leave-period.utils';
import {
  editLeavePeriodFormInitialValues,
  editLeavePeriodFormValidation,
  ADDITIONAL_NOTES_MAX_LENGTH
} from './edit-leave-period.form';
import styles from './edit-leave-period.module.scss';

type EditLeavePeriodProps = {
  decisions: Decision[];
  isEditLeavePeriod: boolean;
  onEditLeavePeriodChange: () => void;
  onFormSubmit: () => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  hasHighlight: {
    '& .ant-checkbox-group .ant-checkbox-wrapper-checked': {
      background: primaryLight2Color(theme.colorSchema)
    }
  },
  primaryCardText: {
    ...textStyleToCss(theme.typography.baseTextSemiBold)
  },
  secondaryCardText: {
    ...textStyleToCss(theme.typography.baseText)
  },
  requiredMarker: {
    color: theme.colorSchema.primaryColor
  }
}));

export const EditLeavePeriod: React.FC<EditLeavePeriodProps> = ({
  isEditLeavePeriod,
  decisions,
  onEditLeavePeriodChange,
  onFormSubmit
}) => {
  const themedStyles = useThemedStyles();
  const [leavePeriodToSend, setLeavePeriodToSend] = useState();
  const dropdownOptions = [
    {
      value: null,
      title: <FormattedMessage id="ENUM_DOMAINS.PLEASE_SELECT" />
    },
    {
      value: CancelLeavePeriodReason.EMPLOYEE_REQUEST,
      title: <FormattedMessage id="LEAVE_PERIOD_CHANGE.SELECT_OPTION_REQUEST" />
    },
    {
      value: CancelLeavePeriodReason.REQUEST_ERROR,
      title: <FormattedMessage id="LEAVE_PERIOD_CHANGE.SELECT_OPTION_ERROR" />
    }
  ];
  const {
    state: { value: leavePeriodChangeResponse, error }
  } = useAsync(changeLeavePeriods, [leavePeriodToSend!], {
    defaultValue: null,
    shouldExecute: () => !!leavePeriodToSend
  });

  useEffect(() => {
    if (leavePeriodChangeResponse && !error) {
      onFormSubmit();
    }
  }, [leavePeriodChangeResponse, error, onFormSubmit]);

  const handleSubmit = (formValues: LeavePeriodChange<number[]>) => {
    setLeavePeriodToSend({
      changeRequestPeriods: parseDataToApiRequestFormat(formValues, decisions),
      reason: formValues.reason,
      additionalNotes: formValues.additionalNotes
    } as any);
  };

  return (
    <Drawer
      ariaLabelId="LEAVE_PERIOD_CHANGE.FORM_HEAD"
      isVisible={isEditLeavePeriod}
      data-test-el="leave-period-change-form"
      isClosable={true}
      onClose={onEditLeavePeriodChange}
      title={<FormattedMessage id="LEAVE_PERIOD_CHANGE.FORM_HEAD" />}
    >
      <Formik
        initialValues={editLeavePeriodFormInitialValues(decisions)}
        validationSchema={editLeavePeriodFormValidation}
        validateOnMount={true}
        onSubmit={values => {
          handleSubmit(values);
        }}
      >
        <>
          <div className={styles.mainContent}>
            <Form>
              <div className={styles.mainFormContent}>
                <AsteriskInfo />
                <FormGroup
                  isRequired={decisions.length > 1}
                  name="changeRequestPeriods"
                >
                  <>
                    {decisions.length > 1 && (
                      <>
                        <FormattedMessage id="LEAVE_PERIOD_CHANGE.MORE_THAN_ONE_PERIOD_LABEL" />
                        <span className={themedStyles.requiredMarker}>*</span>
                      </>
                    )}

                    <Field
                      name="changeRequestPeriods"
                      isVertical={true}
                      hasSelectAll={decisions.length > 1}
                      component={CheckboxGroup}
                      className={classNames(
                        styles.checkboxWrapper,
                        themedStyles.hasHighlight
                      )}
                    >
                      {decisions.map((decision, index) => (
                        <Checkbox
                          value={index}
                          key={index}
                          disabled={decisions.length === 1}
                        >
                          <div className={styles.cardTextWrapper}>
                            <div
                              className={themedStyles.primaryCardText}
                              data-test-el="period-card-title"
                            >
                              <FormattedDate
                                date={decision.period.startDate}
                                data-test-el="period-start-date"
                              />
                              <span> - </span>
                              <FormattedDate
                                date={decision.period.endDate}
                                data-test-el="period-end-date"
                              />
                              <span> {`(${decision.period.type})`}</span>
                            </div>
                            <div data-test-el="period-reason">
                              {decision.period.leaveRequest.reasonName}
                            </div>
                          </div>
                        </Checkbox>
                      ))}
                    </Field>
                  </>
                </FormGroup>
                <FormGroup
                  label={
                    <FormattedMessage id="LEAVE_PERIOD_CHANGE.REASON_FOR_REMOVAL_LABEL" />
                  }
                  isRequired={true}
                  name="reason"
                >
                  <Field
                    name="reason"
                    component={DropdownInput}
                    optionElements={dropdownOptions}
                  />
                </FormGroup>
                <FormGroup
                  label={
                    <FormattedMessage id="LEAVE_PERIOD_CHANGE.DETAILS_LABEL" />
                  }
                  counterMaxLength={ADDITIONAL_NOTES_MAX_LENGTH}
                  immediateErrorMessages={[
                    'LEAVE_PERIOD_CHANGE.REASON_MAX_LENGTH_VALIDATION'
                  ]}
                  name="additionalNotes"
                >
                  <Field name="additionalNotes" component={TextArea} />
                </FormGroup>
              </div>
            </Form>
          </div>
          <DrawerFooter>
            <FormattedMessage
              as={FormButton}
              htmlType="submit"
              id="FINEOS_COMMON.GENERAL.OK"
            />

            <FormattedMessage
              as={UiButton}
              buttonType={ButtonType.LINK}
              onClick={onEditLeavePeriodChange}
              id="FINEOS_COMMON.GENERAL.CANCEL"
            />
          </DrawerFooter>
        </>
      </Formik>
    </Drawer>
  );
};
