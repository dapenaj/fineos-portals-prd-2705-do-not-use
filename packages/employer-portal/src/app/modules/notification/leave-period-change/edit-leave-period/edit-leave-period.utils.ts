/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { LeavePeriod, Decision } from 'fineos-js-api-client';
import { chain as _chain } from 'lodash';

import { LeavePeriodChange } from '../../types';
import { EnhancedAbsencePeriodDecisions } from '../../../../shared';
import { flatNonCancelledDecisions } from '../../job-protected-leave/job-protected-leave.util';

export const parseDataToApiRequestFormat = (
  formValues: LeavePeriodChange<number[]>,
  flattenDecisions: Decision[]
) => {
  const periods: { [key: string]: LeavePeriod[] } = {};
  formValues.changeRequestPeriods.forEach(index => {
    const currentDecision = flattenDecisions[index];
    if (!periods.hasOwnProperty(currentDecision.absence.id!)) {
      periods[currentDecision.absence.id!] = [];
    }
    periods[currentDecision.absence.id!].push({
      startDate: currentDecision.period.startDate,
      endDate: currentDecision.period.endDate
    });
  });
  return periods;
};

const getResults = (
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[]
) => {
  const absenceDecisions = enhancedAbsencePeriodDecisions.map(
    enhancedAbsencePeriodDecision =>
      flatNonCancelledDecisions(
        enhancedAbsencePeriodDecision.absencePeriodDecisions.decisions
      )
  );

  return _chain(absenceDecisions)
    .flatMap()
    .orderBy(['period.startDate', 'period.endDate'], ['desc', 'desc'])
    .value();
};

export const getDecisionsToRemove = (
  enhancedAbsencePeriodDecisions: EnhancedAbsencePeriodDecisions[] | null
): Decision[] => {
  return enhancedAbsencePeriodDecisions?.length
    ? getResults(enhancedAbsencePeriodDecisions)
    : [];
};
