/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { Decision } from 'fineos-js-api-client';

const ADDITIONAL_NOTES_MIN_LENGTH = 5;
export const ADDITIONAL_NOTES_MAX_LENGTH = 250;

export const editLeavePeriodFormValidation = Yup.object({
  changeRequestPeriods: Yup.array().required(
    'LEAVE_PERIOD_CHANGE.REQUIRED_PERIOD_VALIDATION'
  ),
  reason: Yup.string()
    .nullable()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED'),
  additionalNotes: Yup.string()
    .nullable()
    .min(
      ADDITIONAL_NOTES_MIN_LENGTH,
      'FINEOS_COMMON.VALIDATION.MIN_TEXTAREA_LENGTH'
    )
    .max(
      ADDITIONAL_NOTES_MAX_LENGTH,
      'LEAVE_PERIOD_CHANGE.REASON_MAX_LENGTH_VALIDATION'
    )
});

export const editLeavePeriodFormInitialValues = (
  decisions: Decision[] | null
) => ({
  reason: null,
  additionalNotes: '',
  changeRequestPeriods: !!decisions && decisions.length === 1 ? [0] : []
});
