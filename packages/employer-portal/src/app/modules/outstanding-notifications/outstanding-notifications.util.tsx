/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { ColumnProps } from 'antd/lib/table';
import { faCircle, faSearch } from '@fortawesome/free-solid-svg-icons';
import { GroupClientNotification } from 'fineos-js-api-client';
import {
  MultipleTokens,
  Link,
  FallbackValue,
  FormattedDate,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  Button,
  ButtonType,
  FilterDropdownProps,
  Icon
} from 'fineos-common';
import classNames from 'classnames';

import { handleFocusTrigger } from '../my-dashboard/dashboard-table.utils';
import { FilterIconLabel } from '../my-dashboard/dashboard.type';
import { NotificationReason } from '../notification/notification-reason';
import {
  CustomerFilter,
  FilterIconElement,
  TableFilters,
  WidgetTableColumn,
  TableInputFilter
} from '../../shared';

import styles from './outstanding-notifications.module.scss';

export const generateTableColumnsUsing = (
  themedStyles: Record<string, string>,
  filters: TableFilters,
  onSearch: (column: string, selectedKeys: any, confirm?: () => void) => void,
  onReset: (column: string, clearFilters?: () => void) => void,
  hasActionsPermission: boolean
) => {
  const defaultTableColumns = [
    {
      title: (
        <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.COLUMNS.NOTIFICATION_ID" />
      ),
      dataIndex: 'caseNumber',
      width: '33%',
      filterIcon: () => (
        <FilterIconElement
          isActive={!!filters[WidgetTableColumn.CASE_NUMBER]}
          icon={faSearch}
          id={FilterIconLabel.CASE_NUMBER}
        />
      ),
      filterDropdown: ({ confirm, clearFilters }: FilterDropdownProps) => (
        <TableInputFilter
          fieldName={WidgetTableColumn.CASE_NUMBER}
          minLength={1}
          filterValues={filters}
          onSearch={(searchFilters: TableFilters) => {
            onSearch(WidgetTableColumn.CASE_NUMBER, searchFilters, confirm);
            handleFocusTrigger(FilterIconLabel.CASE_NUMBER);
          }}
          onReset={() => {
            onReset(WidgetTableColumn.CASE_NUMBER, clearFilters);
            handleFocusTrigger(FilterIconLabel.CASE_NUMBER);
          }}
          onClose={() => {
            confirm();
            handleFocusTrigger(FilterIconLabel.CASE_NUMBER);
          }}
        />
      ),
      filteredValue: filters.caseNumber || null,
      render: (caseNumber, renderedNotification) => (
        <Link to={`/notification/${caseNumber}`}>
          <MultipleTokens
            tokens={[
              caseNumber,
              <NotificationReason
                key={renderedNotification.id}
                reason={renderedNotification.notificationReason}
              />
            ]}
          />
        </Link>
      )
    },
    {
      title: (
        <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.COLUMNS.EMPLOYEE" />
      ),
      dataIndex: 'customer',
      width: '44%',
      filterIcon: () => (
        <FilterIconElement
          isActive={
            !!filters[WidgetTableColumn.CUSTOMER]?.customerFirstName ||
            !!filters[WidgetTableColumn.CUSTOMER]?.customerLastName
          }
          icon={faSearch}
          id={FilterIconLabel.CUSTOMER}
        />
      ),
      filterDropdown: ({ confirm, clearFilters }: FilterDropdownProps) => (
        <CustomerFilter
          fieldName={WidgetTableColumn.CUSTOMER}
          filterValues={filters}
          onSearch={(searchFilters: TableFilters) => {
            onSearch(WidgetTableColumn.CUSTOMER, searchFilters, confirm);
            handleFocusTrigger(FilterIconLabel.CUSTOMER);
          }}
          onReset={() => {
            onReset(WidgetTableColumn.CUSTOMER, clearFilters);
            handleFocusTrigger(FilterIconLabel.CUSTOMER);
          }}
          onClose={() => {
            confirm();
            handleFocusTrigger(FilterIconLabel.CUSTOMER);
          }}
        />
      ),
      filteredValue:
        filters.customer?.customerFirstName ||
        filters.customer?.customerLastName ||
        null,
      render: customer =>
        customer ? (
          <>
            <Link to={`/profile/${customer.id}`}>
              {`${customer.firstName} ${customer.lastName} - ${customer.id}`}
            </Link>
            <div className={themedStyles.customerInfo}>
              <MultipleTokens
                tokens={[
                  <FallbackValue
                    key={0}
                    data-test-el="outstanding-notification-table-job-title"
                    value={customer?.jobTitle}
                    fallback={
                      <FormattedMessage
                        id={'EMPLOYEE.FALLBACK.UNKNOWN_JOB_TITLE'}
                      />
                    }
                  />,
                  <FallbackValue
                    key={1}
                    data-test-el="outstanding-notification-table-organisation-unit"
                    value={customer?.organisationUnit}
                    fallback={
                      <FormattedMessage id={'EMPLOYEE.FALLBACK.UNKNOWN_UNIT'} />
                    }
                  />,
                  <FallbackValue
                    key={2}
                    data-test-el="outstanding-notification-table-work-site"
                    value={customer.workSite}
                    fallback={
                      <FormattedMessage
                        id={'EMPLOYEE.FALLBACK.UNKNOWN_LOCATION'}
                      />
                    }
                  />
                ]}
              />
            </div>
          </>
        ) : (
          ''
        )
    },
    {
      title: (
        <FormattedMessage
          className={classNames(
            styles.actionTableHeader,
            themedStyles.actionTableHeader
          )}
          as={Button}
          buttonType={ButtonType.LINK}
          id="OUTSTANDING_NOTIFICATIONS.COLUMNS.NOTIFICATION_DATE"
          ariaLabelName={{
            descriptor: {
              id:
                'OUTSTANDING_NOTIFICATIONS.COLUMNS.NOTIFICATION_DATE_SORTER_LABEL'
            }
          }}
        />
      ),
      dataIndex: 'notificationDate',
      sorter: (a: GroupClientNotification, b: GroupClientNotification) =>
        a.notificationDate?.diff(b.notificationDate),
      width: hasActionsPermission ? '15%' : undefined,
      defaultSortOrder: 'ascend',
      render: notificationDate => (
        <FormattedDate
          className={themedStyles.formattedDate}
          date={notificationDate}
          data-test-el="outstanding-notification-table-date"
        />
      )
    }
  ] as ColumnProps<GroupClientNotification>[];

  return hasActionsPermission
    ? [
        ...defaultTableColumns,
        {
          title: () => (
            <>
              <Icon
                className={classNames(
                  styles.actionsForMeIcon,
                  themedStyles.actionsForMeIcon
                )}
                icon={faCircle}
                iconLabel={
                  <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME_ICON" />
                }
              />
              <FormattedMessage
                id="OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME"
                className={styles.actionsForMeLabel}
              />
            </>
          ),
          render: () => '',
          className: styles.expandableColumn
        } as ColumnProps<GroupClientNotification>
      ]
    : defaultTableColumns;
};

export const useFiltersThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    backgroundColor: theme.colorSchema.neutral1
  },
  item: {
    '& .ant-form-item-label': {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  },
  placeholder: {
    ...textStyleToCss(theme.typography.baseText, { suppressLineHeight: true })
  },
  icon: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  errorMessage: {
    color: theme.colorSchema.attentionColor,
    ...textStyleToCss(theme.typography.baseText)
  },
  infoText: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.smallText)
  }
}));

export const useOutstandingNotificationsThemedStyles = createThemedStyles(
  theme => ({
    customerInfo: {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    actionTableHeader: {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.smallText)
    },
    title: {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss({
        ...theme.typography.displaySmall
      })
    },
    count: {
      color: theme.colorSchema.neutral1,
      backgroundColor: theme.colorSchema.callToActionColor,
      ...textStyleToCss({
        ...theme.typography.displayTitle
      })
    },
    message: {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    },
    icon: {
      color: theme.colorSchema.primaryAction,
      ...textStyleToCss(theme.typography.baseText)
    },
    actionsForMeIcon: {
      color: theme.colorSchema.callToActionColor
    },
    formattedDate: {
      color: theme.colorSchema.neutral8,
      ...textStyleToCss(theme.typography.baseText)
    }
  })
);
