/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  Panel,
  ContentRenderer,
  FormattedMessage,
  EmptyState,
  PAGINATION_THRESHOLD,
  FilterItemsCountTag
} from 'fineos-common';
import { GroupClientNotification } from 'fineos-js-api-client';

import {
  ExportDataModal,
  ExportedGroupClientNotification,
  useTransformOutstandingNotifications,
  MappedNotificationKey
} from '../../shared';

import { OutstandingNotificationsHeader } from './outstanding-notifications-header';
import { OutstandingNotificationsTable } from './outstanding-notifications-table.component';
import { useOutstandingNotifications } from './outstanding-notifications.hook';
import styles from './outstanding-notifications.module.scss';

export const OutstandingNotifications = () => {
  const {
    unfilteredTotal,
    notifications,
    isPending,
    isEmpty,
    isFiltered,
    error,
    filters,
    clearFilterValues,
    updateFilterValues,
    isOutstandingLoading,
    setForMeCurrentFilter,
    currentForMeFilter
  } = useOutstandingNotifications();

  const downloadReadyNotifications = useTransformOutstandingNotifications(
    notifications.data.sort((a, b) => a.createdDate.diff(b.createdDate)),
    [
      MappedNotificationKey.CASE,
      MappedNotificationKey.REASON,
      MappedNotificationKey.NAME,
      MappedNotificationKey.NOTIFICATION_DATE
    ]
  );

  const shouldShowFiltering =
    !isPending && isFiltered && unfilteredTotal !== notifications.total;

  return (
    <Panel
      data-test-el="outstanding-notifications"
      className={styles.wrapper}
      header={
        <>
          <OutstandingNotificationsHeader
            count={unfilteredTotal}
            showExplanatoryText={true}
          />
          <FilterItemsCountTag
            isVisible={shouldShowFiltering}
            filteredItemsCount={isEmpty ? 0 : notifications.total}
            totalItemsCount={unfilteredTotal}
            onClearFilters={() => clearFilterValues()}
          />
        </>
      }
    >
      <ContentRenderer
        isLoading={isPending}
        isEmpty={false}
        error={error}
        emptyContent={
          <EmptyState className={styles.emptyState}>
            <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.EMPTY_STATE" />
          </EmptyState>
        }
      >
        <>
          <OutstandingNotificationsTable
            isOutstandingLoading={isOutstandingLoading}
            onChangeForMeFilter={setForMeCurrentFilter}
            notifications={notifications}
            isPending={isPending}
            isFiltered={isFiltered}
            filters={filters}
            currentForMeFilter={currentForMeFilter}
            onClearFilterValues={clearFilterValues}
            onUpdateFilterValues={updateFilterValues}
            className={classNames(styles.tablePagination, {
              [styles.bottomExtendedTablePadding]:
                downloadReadyNotifications.length < PAGINATION_THRESHOLD
            })}
          />
          <div className={styles.exportList}>
            <ExportDataModal<GroupClientNotification>
              data={
                downloadReadyNotifications as ExportedGroupClientNotification[]
              }
              fileNameTranslationId="EXPORT_OUTSTANDING_LIST.FILE_NAME.OUTSTANDING_NOTIFICATIONS"
            />
          </div>
        </>
      </ContentRenderer>
    </Panel>
  );
};
