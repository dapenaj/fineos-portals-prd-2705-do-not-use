/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { useState, useEffect, useCallback } from 'react';
import { isEmpty as _isEmpty, isEqual as _isEqual } from 'lodash';
import {
  GroupClientNotification,
  OutstandingInformation,
  Page
} from 'fineos-js-api-client';
import { useAsync } from 'fineos-common';

import {
  fetchOutstandingInformation,
  ForMeFilter,
  initialTableFilters,
  TableFilters
} from '../../shared';

import { fetchOutstandingNotifications } from './outstanding-notifications.api';

const EMPTY_LIST: Page<GroupClientNotification> = { data: [], total: 0 };

export const useOutstandingNotifications = () => {
  const [forMeFilteredNotifications, setForMeFilteredNotifications] = useState<
    Record<string, GroupClientNotification[]>
  >({
    [ForMeFilter.ALL]: [],
    [ForMeFilter.FOR_ME]: [],
    [ForMeFilter.FOR_OTHERS]: []
  });
  const [filters, setFilters] = useState<TableFilters>(initialTableFilters);
  const [filteredNotifications, setFilteredNotifications] = useState(
    EMPTY_LIST.data
  );
  const [isFiltered, setIsFiltered] = useState<boolean>(false);
  const [isOutstandingLoading, setIsOutstandingLoading] = useState<boolean>(
    false
  );
  const [hasGotOutstanding, setHasGotOutstanding] = useState<boolean>(false);
  const [forMeCurrentFilter, setForMeCurrentFilter] = useState<ForMeFilter>(
    ForMeFilter.ALL
  );
  const {
    state: { value: notifications, isPending, error }
  } = useAsync(fetchOutstandingNotifications, [], {
    defaultValue: EMPTY_LIST
  });

  // checks if a notification is for me or not
  const checkNotificationIsForMe = (
    notification: GroupClientNotification,
    { infoReceived, sourcePartyId, uploadCaseNumber }: OutstandingInformation
  ) =>
    uploadCaseNumber.includes(notification.id) &&
    sourcePartyId !== notification.customer.customerNo &&
    !infoReceived;

  const getNotificationsWithOutstandingData = useCallback(async () => {
    // temporarily set the value of ForMeFilter.ALL
    setForMeFilteredNotifications({
      [ForMeFilter.ALL]: [...notifications.data],
      [ForMeFilter.FOR_ME]: [],
      [ForMeFilter.FOR_OTHERS]: []
    });
    // temp arrays to store values
    const forMeArr: GroupClientNotification[] = [];
    const forOthersArr: GroupClientNotification[] = [];

    setIsOutstandingLoading(true);

    // wait for all outstanding data to be fetched before storing/using it
    Promise.allSettled(
      notifications.data.map((notification: GroupClientNotification) =>
        fetchOutstandingInformation(notification.id)
          .then(data => {
            const isForMe = data.find(outstanding =>
              checkNotificationIsForMe(
                notification,
                outstanding as OutstandingInformation
              )
            );

            if (isForMe) {
              forMeArr.push(notification);
            } else {
              forOthersArr.push(notification);
            }
          })
          .catch(() => [])
      )
    ).then(() => {
      // all the fetches are finished... so now store the data
      setForMeFilteredNotifications({
        [ForMeFilter.ALL]: [...notifications.data],
        [ForMeFilter.FOR_ME]: forMeArr,
        [ForMeFilter.FOR_OTHERS]: forOthersArr
      });
      setIsOutstandingLoading(false);
    });
  }, [notifications]);

  useEffect(() => {
    if (!hasGotOutstanding && notifications.data.length) {
      getNotificationsWithOutstandingData();
      setHasGotOutstanding(true);
    }
  }, [notifications, getNotificationsWithOutstandingData, hasGotOutstanding]);

  useEffect(() => {
    let temp = forMeFilteredNotifications[forMeCurrentFilter];

    const caseNumber = filters.caseNumber;
    const firstName = filters.customer?.customerFirstName;
    const lastName = filters.customer?.customerLastName;

    if (caseNumber) {
      temp = temp.filter((notification: GroupClientNotification) =>
        notification.caseNumber.toLowerCase().includes(caseNumber.toLowerCase())
      );
    }
    if (firstName) {
      temp = temp.filter(({ customer }: GroupClientNotification) =>
        customer.firstName.toLowerCase().includes(firstName.toLowerCase())
      );
    }
    if (lastName) {
      temp = temp.filter(({ customer }: GroupClientNotification) =>
        customer.lastName.toLowerCase().includes(lastName.toLowerCase())
      );
    }

    setFilteredNotifications(temp);
  }, [notifications, filters, forMeCurrentFilter, forMeFilteredNotifications]);

  const updateFilterValues = (selectedFilters: TableFilters) => {
    setFilters({ ...filters, ...selectedFilters });
    setIsFiltered(true);
  };

  const clearFilterValues = (
    column: string = '',
    clearFilters?: () => void
  ) => {
    // reset all filters
    if (!column) {
      setForMeCurrentFilter(ForMeFilter.ALL);
      setFilters(initialTableFilters);
      clearFilters && clearFilters();
      setIsFiltered(false);
      return;
    }
    // reset filter for specific column
    const newFilters = {
      ...filters,
      [column]: initialTableFilters[column as keyof TableFilters]
    };
    setFilters(newFilters);
    clearFilters && clearFilters();
    // hide filter indicator when no filter applied
    if (
      _isEqual(newFilters, initialTableFilters) &&
      forMeCurrentFilter === ForMeFilter.ALL
    ) {
      setIsFiltered(false);
    }
  };

  const changeForMeFilter = (filter: ForMeFilter) => {
    if (filter === ForMeFilter.ALL) {
      setFilters(initialTableFilters);
      setForMeCurrentFilter(ForMeFilter.ALL);
      setIsFiltered(false);
      return;
    }

    setForMeCurrentFilter(filter);
    setIsFiltered(true);
  };

  return {
    unfilteredTotal: notifications.data.length,
    notifications: {
      data: filteredNotifications,
      total: filteredNotifications.length
    },
    isPending,
    isOutstandingLoading,
    isEmpty: _isEmpty(notifications.data),
    setForMeCurrentFilter: changeForMeFilter,
    currentForMeFilter: forMeCurrentFilter,
    isFiltered,
    error,
    filters,
    clearFilterValues,
    updateFilterValues,
    forMeFilteredNotifications
  };
};
