/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  FormattedMessage,
  Spinner,
  Select,
  FormGroup,
  createThemedStyles,
  textStyleToCss,
  ThemeModel
} from 'fineos-common';

import { ForMeFilter } from '../../../shared';

import styles from './outstanding-notifications-table-forme-filter.module.scss';
import classNames from 'classnames';

type ForMeFilterProps = {
  isOutstandingLoading: boolean;
  isReseted: boolean;
  onChangeForMeFilter: (filter: ForMeFilter) => void;
};

const actionsFilter = [
  {
    title: (
      <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.ACTIONS_FILTER.ALL" />
    ),
    value: ForMeFilter.ALL
  },
  {
    title: (
      <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.ACTIONS_FILTER.FOR_ME" />
    ),
    value: ForMeFilter.FOR_ME
  },
  {
    title: (
      <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.ACTIONS_FILTER.FOR_OTHERS" />
    ),
    value: ForMeFilter.FOR_OTHERS
  }
];

const useThemedStyles = createThemedStyles((theme: ThemeModel) => ({
  selectLabel: {
    ...textStyleToCss(theme.typography.smallText),
    color: theme.colorSchema.neutral8
  }
}));

const OutstandingNotificationsTableForMeFilterComponent = ({
  onChangeForMeFilter,
  isOutstandingLoading,
  isReseted
}: ForMeFilterProps) => {
  const themedStyles = useThemedStyles();
  const [filter, setFilter] = React.useState<ForMeFilter>(ForMeFilter.ALL);
  // It looks for outstandings for each notification where it could be 'forMe' or not
  const filterByForMeFilter = (filter: unknown) => {
    const value: ForMeFilter = filter as ForMeFilter;

    setFilter(value);
    onChangeForMeFilter(value);
  };

  const valueForSelect = isReseted ? actionsFilter[0].value : filter;

  React.useEffect(() => {
    if (isReseted) {
      setFilter(ForMeFilter.ALL);
    }
  }, [isReseted]);

  return (
    <div className={styles.outstandingForMe}>
      <FormGroup
        className={styles.outstandingForMeGroup}
        labelClassName={classNames(
          themedStyles.selectLabel,
          styles.outstandingForMeLabel
        )}
        label={
          <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.SHOWING_FILTER" />
        }
      >
        <Select
          bordered={false}
          className={styles.outstandingForMeSelector}
          data-test-el="outstanding-notification-filter-selector"
          defaultValue={actionsFilter[0].value}
          disabled={isOutstandingLoading}
          onChange={filterByForMeFilter}
          optionElements={actionsFilter}
          value={valueForSelect}
        />
      </FormGroup>
      {isOutstandingLoading ? (
        <div data-test-el="outstanding-notification-filter-spinner">
          <Spinner size="small" />
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export const OutstandingNotificationsTableForMeFilter = React.memo(
  OutstandingNotificationsTableForMeFilterComponent
);
