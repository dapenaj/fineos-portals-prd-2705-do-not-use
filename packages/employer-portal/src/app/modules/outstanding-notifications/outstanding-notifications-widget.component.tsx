/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import classNames from 'classnames';
import {
  Panel,
  ContentRenderer,
  RichFormattedMessage,
  Icon,
  FormattedMessage,
  ButtonType,
  UiButton,
  Drawer,
  DrawerWidth,
  EmptyState,
  FilterItemsCountTag,
  DrawerFooter,
  PAGINATION_THRESHOLD
} from 'fineos-common';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

import {
  useTransformOutstandingNotifications,
  MappedNotificationKey
} from '../../shared';
import widgetStyles from '../my-dashboard/my-dashboard.module.scss';
import { WidgetProps } from '../my-dashboard/dashboard.type';
import { WidgetDrawerFooter } from '../my-dashboard/widget-shared';

import {
  OutstandingNotificationsDrawerHelpInfo,
  OutstandingNotificationsWidgetHelp
} from './ui';
import { OutstandingNotificationsHeader } from './outstanding-notifications-header';
import { OutstandingNotificationsTable } from './outstanding-notifications-table.component';
import { useOutstandingNotifications } from './outstanding-notifications.hook';
import { useOutstandingNotificationsThemedStyles } from './outstanding-notifications.util';
import styles from './outstanding-notifications.module.scss';

export const OutstandingNotificationsWidget: React.FC<WidgetProps> = ({
  helpInfoEnabled
}) => {
  const themedStyles = useOutstandingNotificationsThemedStyles();
  const {
    unfilteredTotal,
    notifications,
    isPending,
    isEmpty,
    isFiltered,
    error,
    filters,
    clearFilterValues,
    updateFilterValues,
    setForMeCurrentFilter,
    isOutstandingLoading,
    currentForMeFilter
  } = useOutstandingNotifications();
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);

  const downloadReadyNotifications = useTransformOutstandingNotifications(
    notifications.data.sort((a, b) => a.createdDate.diff(b.createdDate)),
    [
      MappedNotificationKey.CASE,
      MappedNotificationKey.REASON,
      MappedNotificationKey.NAME,
      MappedNotificationKey.JOB_TITLE,
      MappedNotificationKey.ORGANISATION_UNIT,
      MappedNotificationKey.WORK_SITE,
      MappedNotificationKey.NOTIFICATION_DATE
    ]
  );

  const handleClose = () => {
    clearFilterValues();
    setIsDrawerVisible(false);
  };

  return (
    <Panel
      data-test-el="outstanding-notifications-widget"
      className={styles.wrapper}
      header={
        <>
          <ContentRenderer
            isLoading={isPending}
            isEmpty={!unfilteredTotal}
            error={error}
            emptyContent={
              <EmptyState className={styles.emptyState}>
                <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.EMPTY_STATE" />
              </EmptyState>
            }
          >
            <div className={styles.widgetWrapper}>
              <div className={styles.widgetTitleWrapper}>
                <div className={classNames(themedStyles.count, styles.count)}>
                  {unfilteredTotal}
                </div>
                <div className={styles.widgetContentWrapper}>
                  <RichFormattedMessage
                    className={classNames(styles.title, themedStyles.title)}
                    as="h2"
                    id="OUTSTANDING_NOTIFICATIONS.WIDGET_TITLE"
                  />
                  <OutstandingNotificationsWidgetHelp
                    isVisible={helpInfoEnabled}
                  />
                </div>
              </div>
              <FormattedMessage
                className={styles.widgetButton}
                as={UiButton}
                buttonType={ButtonType.LINK}
                id="WIDGET.VIEW_DETAILS"
                icon={
                  <Icon
                    className={classNames(themedStyles.icon, styles.icon)}
                    icon={faArrowRight}
                    iconLabel={
                      <FormattedMessage id="WIDGET.VIEW_DETAILS_ICON" />
                    }
                  />
                }
                onClick={() => setIsDrawerVisible(true)}
              />
            </div>
          </ContentRenderer>
          <Drawer
            ariaLabelId="OUTSTANDING_NOTIFICATIONS.TITLE"
            ariaLabelValues={{ count: unfilteredTotal }}
            className={styles.drawer}
            headerClassName={styles.drawerHeader}
            bodyClassName={classNames({
              [styles.drawerBody]:
                notifications.data.length > PAGINATION_THRESHOLD
            })}
            data-test-el="outstanding-notifications"
            onClose={handleClose}
            isVisible={isDrawerVisible}
            width={DrawerWidth.FULL_PAGE}
            title={
              <div className={styles.widgetTitleWrapper}>
                <OutstandingNotificationsHeader
                  count={unfilteredTotal}
                  countWrapperClassName={styles.drawerCountWrapper}
                />
                <OutstandingNotificationsDrawerHelpInfo
                  isVisible={helpInfoEnabled}
                />
                <FilterItemsCountTag
                  isVisible={isFiltered && !isPending}
                  filteredItemsCount={isEmpty ? 0 : notifications.total}
                  totalItemsCount={unfilteredTotal}
                  onClearFilters={() => clearFilterValues()}
                />
              </div>
            }
          >
            <ContentRenderer
              shouldNotDestroyOnLoading={true}
              spinnerClassName={styles.spinnerContainer}
              isLoading={isPending}
              isEmpty={false}
            >
              <OutstandingNotificationsTable
                notifications={notifications}
                isPending={isPending}
                filters={filters}
                isFiltered={isFiltered}
                currentForMeFilter={currentForMeFilter}
                onClearFilterValues={clearFilterValues}
                onUpdateFilterValues={updateFilterValues}
                className={classNames(styles.drawerTable, widgetStyles.table)}
                onChangeForMeFilter={setForMeCurrentFilter}
                isOutstandingLoading={isOutstandingLoading}
              />
            </ContentRenderer>
            <DrawerFooter>
              <WidgetDrawerFooter
                isVisible={!isPending}
                dataLength={notifications.data.length}
                downloadData={downloadReadyNotifications}
                fileNameTranslationId="EXPORT_OUTSTANDING_LIST.FILE_NAME.OUTSTANDING_NOTIFICATIONS"
                onCloseClick={handleClose}
              />
            </DrawerFooter>
          </Drawer>
        </>
      }
    />
  );
};
