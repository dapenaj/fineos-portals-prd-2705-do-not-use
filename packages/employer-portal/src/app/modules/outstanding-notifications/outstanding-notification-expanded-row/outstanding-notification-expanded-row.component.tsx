/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  createThemedStyles,
  FormattedMessage,
  Spinner,
  textStyleToCss
} from 'fineos-common';
import classNames from 'classnames';
import { CustomerSummary } from 'fineos-js-api-client';

import { OutstandingInformationList } from '../../../shared';
import { useOutstandingInformation } from '../outstanding-information.hook';

import styles from './outstanding-notification-expanded-row.module.scss';

type OutstandingNotificationExpandedRowProps = {
  caseId: string;
  customer: CustomerSummary;
};

const useThemedStyles = createThemedStyles(theme => ({
  info: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

export const OutstandingNotificationExpandedRow: React.FC<OutstandingNotificationExpandedRowProps> = ({
  caseId,
  customer
}) => {
  const {
    state: { value: outstandingInfos, isPending },
    executeAgain
  } = useOutstandingInformation(caseId);
  const themedStyles = useThemedStyles();
  return (
    <div
      className={styles.wrapper}
      data-test-el="outstanding-notification-expanded-row"
    >
      {isPending && <Spinner />}
      {!isPending && (
        <div className={styles.content}>
          <div className={classNames(styles.info, themedStyles.info)}>
            <FormattedMessage
              id="OUTSTANDING_NOTIFICATIONS.NOTIFICATIONS_INFO"
              values={{ value: outstandingInfos.length }}
            />
          </div>
          <div
            data-test-el="outstanding-information-list"
            className={styles.list}
          >
            <OutstandingInformationList
              customer={customer}
              outstandingInfos={outstandingInfos}
              caseId={caseId}
              isItemBorderVisible={false}
              refreshOutstandingInfo={() => executeAgain()}
            />
          </div>
        </div>
      )}
    </div>
  );
};
