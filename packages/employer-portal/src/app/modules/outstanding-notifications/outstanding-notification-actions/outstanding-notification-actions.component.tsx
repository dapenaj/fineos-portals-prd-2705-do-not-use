/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Button,
  ButtonType,
  FormattedMessage,
  TableProps,
  Icon,
  createThemedStyles
} from 'fineos-common';
import {
  GroupClientNotification,
  OutstandingInformation
} from 'fineos-js-api-client';
import {
  faChevronRight,
  faChevronUp,
  faCircle
} from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import { useOutstandingInformation } from '../outstanding-information.hook';

import styles from './outstanding-notification-actions.module.scss';

type OutstandingNotificationActionsProps = Parameters<
  NonNullable<TableProps<GroupClientNotification>['expandIcon']>
>[0];

const useThemedStyles = createThemedStyles(theme => ({
  actionsForMeIcon: {
    color: theme.colorSchema.callToActionColor
  }
}));

export const OutstandingNotificationActions: React.FC<OutstandingNotificationActionsProps> = ({
  expanded,
  onExpand,
  record
}) => {
  const { removeAsyncValue } = useOutstandingInformation(record.id, expanded);
  const themedStyles = useThemedStyles();

  const {
    state: { value: outstandingInfos, isPending }
  } = useOutstandingInformation(record.id);

  const actionForMe = (infos: OutstandingInformation[] | null) =>
    infos?.some(
      ({ sourcePartyId, infoReceived }: OutstandingInformation) =>
        sourcePartyId !== record.customer.customerNo && !infoReceived
    );

  const [initial, setInitial] = React.useState<OutstandingInformation[] | null>(
    null
  );

  React.useEffect(() => {
    if (!isPending && outstandingInfos.length && initial === null) {
      setInitial(outstandingInfos);
    }
    if (
      !isPending &&
      outstandingInfos.length &&
      !actionForMe(outstandingInfos)
    ) {
      setInitial(null);
    }
    // eslint-disable-next-line
  }, [outstandingInfos]);

  const isActionsForMeVisible = isPending
    ? actionForMe(initial)
    : actionForMe(outstandingInfos);

  return (
    <Button
      className={styles.button}
      buttonType={ButtonType.LINK}
      onClick={(event: React.MouseEvent<HTMLElement>) => {
        if (expanded) {
          removeAsyncValue();
        }
        if (onExpand) {
          onExpand(record, event);
        }
      }}
      data-test-el="outstanding-notification-actions"
      icon={
        <Icon
          className={styles.icon}
          icon={expanded ? faChevronUp : faChevronRight}
          iconLabel={
            <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.EXPAND_ICON_LABEL" />
          }
        />
      }
    >
      {isActionsForMeVisible && (
        <Icon
          className={classNames(
            styles.actionsForMeIcon,
            themedStyles.actionsForMeIcon
          )}
          icon={faCircle}
          iconLabel={
            <FormattedMessage id="OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME_ICON" />
          }
        />
      )}
      <FormattedMessage
        id={`OUTSTANDING_NOTIFICATIONS.${
          expanded ? 'HIDE_ACTIONS' : 'VIEW_ACTIONS'
        }`}
      />
    </Button>
  );
};
