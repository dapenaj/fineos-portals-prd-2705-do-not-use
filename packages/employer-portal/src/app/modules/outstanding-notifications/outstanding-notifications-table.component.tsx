/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import classNames from 'classnames';
import {
  paginationDefaultItemRender,
  PaginationProps,
  PAGINATION_THRESHOLD,
  Table,
  TableProps,
  usePaginationThemedStyles,
  usePermissions,
  ViewNotificationPermissions
} from 'fineos-common';
import { GroupClientNotification, Page } from 'fineos-js-api-client';

import { ForMeFilter, TableFilters } from '../../shared';

import { OutstandingNotificationExpandedRow } from './outstanding-notification-expanded-row/outstanding-notification-expanded-row.component';
import { OutstandingNotificationActions } from './outstanding-notification-actions/outstanding-notification-actions.component';
import OutstandingNotificationsTableFormeFilter from './outstanding-notification-table-forme-filter';
import {
  generateTableColumnsUsing,
  useOutstandingNotificationsThemedStyles
} from './outstanding-notifications.util';
import styles from './outstanding-notifications.module.scss';

type OutstandingNotificationsTableProps = {
  isPending: boolean;
  isFiltered: boolean;
  isOutstandingLoading: boolean;
  notifications: Page<GroupClientNotification>;
  filters: TableFilters;
  currentForMeFilter: ForMeFilter;
  onClearFilterValues: (column: string, clearFilters?: () => void) => void;
  onUpdateFilterValues: (selectedFilters: TableFilters) => void;
  onChangeForMeFilter: (filter: ForMeFilter) => void;
  className?: string;
};

export const OutstandingNotificationsTable: React.FC<OutstandingNotificationsTableProps> = ({
  isPending,
  isFiltered,
  isOutstandingLoading,
  notifications,
  filters,
  onClearFilterValues,
  onUpdateFilterValues,
  onChangeForMeFilter,
  className,
  currentForMeFilter
}) => {
  const themedStyles = useOutstandingNotificationsThemedStyles();
  const paginationThemedStyles = usePaginationThemedStyles();

  const hasActionsPermission = usePermissions(
    ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
  );

  // manually track current page so we can reset it on clearing filters
  const [currentPage, setCurrentPage] = useState(1);
  // manually track expanded rows so we can reset them on clearing filters
  const [expandedRowKeys, setExpandedRowKeys] = useState<React.Key[]>([]);

  // Table Config
  const columns = generateTableColumnsUsing(
    themedStyles,
    filters,
    (column: string, selectedFilters: TableFilters, confirm?: () => void) => {
      onUpdateFilterValues(selectedFilters);

      // confirm() should close the filter dropdown
      if (confirm) {
        confirm();
      }
    },
    (column: string, clearFilters?: () => void) => {
      onClearFilterValues(column, clearFilters);
    },
    hasActionsPermission
  );

  const defaultTableProps: TableProps<GroupClientNotification> = {
    columns,
    rowKey: (record: GroupClientNotification) => record.caseNumber,
    className: classNames(
      styles.table,
      paginationThemedStyles.pagination,
      styles.outstandingNotificationsTable,
      className
    ),
    loading: isPending
  };

  const tableProps: any = !hasActionsPermission
    ? defaultTableProps
    : {
        ...defaultTableProps,
        expandedRowRender: (record: GroupClientNotification) => (
          <OutstandingNotificationExpandedRow
            caseId={record.id}
            customer={record.customer}
          />
        ),
        expandIcon: (
          props: React.ComponentProps<typeof OutstandingNotificationActions>
        ) => <OutstandingNotificationActions {...props} />,
        expandIconColumnIndex: columns.length - 1,
        expandedRowKeys: expandedRowKeys,
        onExpandedRowsChange: setExpandedRowKeys
      };

  return (
    <>
      <OutstandingNotificationsTableFormeFilter
        isOutstandingLoading={isOutstandingLoading}
        onChangeForMeFilter={(filter: ForMeFilter) => {
          onChangeForMeFilter(filter);
          setCurrentPage(1);
          setExpandedRowKeys([]);
        }}
        isReseted={!isFiltered && currentForMeFilter === ForMeFilter.ALL}
      />
      <Table
        {...tableProps}
        pagination={
          notifications.total > PAGINATION_THRESHOLD &&
          ({
            showSizeChanger: true,
            itemRender: paginationDefaultItemRender,
            current: currentPage,
            onChange: setCurrentPage,
            'data-test-el': 'pagination'
          } as PaginationProps)
        }
        dataSource={notifications.data}
      />
    </>
  );
};
