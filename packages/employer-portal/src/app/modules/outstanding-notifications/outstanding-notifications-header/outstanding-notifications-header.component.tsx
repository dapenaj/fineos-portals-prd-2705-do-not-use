/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  createThemedStyles,
  textStyleToCss,
  FormattedMessage
} from 'fineos-common';

import styles from './outstanding-notifications-header.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(
      {
        ...theme.typography.displaySmall
      },
      { suppressLineHeight: true }
    )
  },
  icon: {
    color: theme.colorSchema.neutral1
  },
  message: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  },
  count: {
    color: theme.colorSchema.neutral8,
    // I am setting only font-weight here, because font-size is handled by em
    fontWeight: theme.typography.baseTextSemiBold.fontWeight
  }
}));

type OutstandingNotificationsHeaderProps = {
  count: number;
  showExplanatoryText?: boolean;
  countWrapperClassName?: string;
};

export const OutstandingNotificationsHeader: React.FC<OutstandingNotificationsHeaderProps> = ({
  count,
  showExplanatoryText = false,
  countWrapperClassName
}) => {
  const themedStyles = useThemedStyles();
  return (
    <>
      <div className={styles.wrapper}>
        <div
          className={classNames(
            themedStyles.title,
            styles.countWrapper,
            countWrapperClassName
          )}
        >
          <span
            data-test-el="header-count"
            className={classNames(styles.count, themedStyles.count)}
          >
            {count}
          </span>
        </div>
        <div>
          <div className={styles.titleWrapper}>
            <FormattedMessage
              className={classNames(styles.title, themedStyles.title)}
              id="OUTSTANDING_NOTIFICATIONS.TITLE"
            />
          </div>
          {showExplanatoryText && (
            <FormattedMessage
              className={classNames(themedStyles.message, styles.message)}
              as="p"
              id="OUTSTANDING_NOTIFICATIONS.WIDGET_MESSAGE"
            />
          )}
        </div>
      </div>
    </>
  );
};
