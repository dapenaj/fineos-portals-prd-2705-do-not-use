/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useMemo, useState } from 'react';
import { Col, Row } from 'antd';
import {
  AsteriskInfo,
  usePermissions,
  ViewCustomerDataPermissions,
  FormButton,
  Form,
  useAsync,
  ContentRenderer,
  SomethingWentWrongModal,
  FormattedMessage,
  ManageCustomerDataPermissions,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import {
  isEmpty as _isEmpty,
  isEqual as _isEqual,
  assign as _assign,
  isNil as _isNil
} from 'lodash';
import { FormikProps } from 'formik';
import * as Yup from 'yup';
import classNames from 'classnames';
import {
  GroupClientCustomerInfo,
  GroupClientPhoneNumber,
  ConcurrencyUpdateError
} from 'fineos-js-api-client';

import { IntakePanel } from '../../common';
import { fetchCustomerInfo, editCustomerInfo } from '../../../../shared';
import { IntakePanelFooter } from '../../common/intake-panel-footer';
import { DEFAULT_PHONE_NUMBER } from '../../../employee-profile/employee-details-tab/personal-details-tab/contact-details/edit-phone-numbers/edit-phone-numbers.constant';
import { CommunicationPreferencesResource } from '../../../employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences.constant';
import { FormikWithResponseHandling } from '../../../../shared';

import { ProfileDetails } from './profile-details';
import styles from './employee-details.module.scss';
import { PhoneCalls } from './phone-calls';
import {
  buildInitialEstablishedProfileDetailsFormValue,
  establishedEmployeeProfileValidation,
  EstablishedProfileDetailsFormValue
} from './profile-details/profile-details.form';
import { establishedEmployeePhoneCallsValidation } from './phone-calls/phone-calls.form';
import {
  fetchCommunicationPreferencePhoneNumbers,
  fetchCustomerPhoneNumbers
} from './phone-calls/phone-calls.api';
import {
  addEmployeePhoneNumber,
  linkCustomerCommunicationPreference
} from './employee-details.api';

const useThemedStyles = createThemedStyles(theme => ({
  emplyeeDetailsHeader: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

type EstablishedEmployeeDetailsProps = {
  employeeId: string;
  onFinish: (employeeDetailsForm: EmployeeDetailsForm) => void;
};

export type EmployeeDetailsForm = {
  customer: EstablishedProfileDetailsFormValue;
  phoneNumbers: GroupClientPhoneNumber[] | null;
  preferredPhoneNumberIndex: number;
};

export const EstablishedEmployeeDetails: React.FC<EstablishedEmployeeDetailsProps> = ({
  employeeId,
  onFinish
}) => {
  const [updateError, setUpdateError] = useState<Error | null>(null);

  const hasCustomerInfoPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
  );

  const hasUpdateCustomerCommunicationPreferencesPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES
  );

  const hasPhoneNumberPermission = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
  );

  const hasCustomerCommunicationPreferencesGetSingle = usePermissions(
    ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
  );

  const hasPermissionToCommunicationPreferences =
    hasCustomerCommunicationPreferencesGetSingle &&
    hasUpdateCustomerCommunicationPreferencesPermission;

  const validationSchema = Yup.object({
    customer: hasCustomerInfoPermission
      ? establishedEmployeeProfileValidation
      : Yup.mixed(),
    phoneNumbers: hasPermissionToCommunicationPreferences
      ? establishedEmployeePhoneCallsValidation
      : Yup.mixed(),
    preferredPhoneNumberIndex: Yup.number().moreThan(
      -1,
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    )
  });

  // Customer info request
  const {
    state: { value: customerInfo, isPending: isPendingCustomerInfo }
  } = useAsync(fetchCustomerInfo, [employeeId], {
    shouldExecute: () => hasCustomerInfoPermission
  });

  // Phone numbers requests
  const {
    state: {
      value: communicationPhoneNumbers,
      isPending: communicationPhoneNumbersPending
    }
  } = useAsync(fetchCommunicationPreferencePhoneNumbers, [employeeId], {
    shouldExecute: () => hasCustomerCommunicationPreferencesGetSingle
  });

  // If first request is empty, do another
  const shouldFetchCustomerPhoneNumbers = useMemo(
    () => !!communicationPhoneNumbers && _isEmpty(communicationPhoneNumbers),
    [communicationPhoneNumbers]
  );

  const {
    state: {
      value: customerPhoneNumbers,
      isPending: customerPhoneNumbersPending,
      error: customerPhoneNumbersError
    },
    executeAgain: executeCustomerPhoneNumbers
  } = useAsync(fetchCustomerPhoneNumbers, [employeeId], {
    shouldExecute: () => false
  });

  // only perform this API call if there are no phone numbers from communication preferences
  // and if we haven't already tried to get customer phone numbers
  if (
    hasPermissionToCommunicationPreferences &&
    hasPhoneNumberPermission &&
    shouldFetchCustomerPhoneNumbers &&
    !customerPhoneNumbers &&
    !customerPhoneNumbersPending &&
    !customerPhoneNumbersError
  ) {
    executeCustomerPhoneNumbers();
  }

  const isLoadingPhoneNumbers =
    communicationPhoneNumbersPending || customerPhoneNumbersPending;

  const fetchedPhoneNumbers = useMemo(
    () =>
      communicationPhoneNumbers && !_isEmpty(communicationPhoneNumbers)
        ? communicationPhoneNumbers
        : customerPhoneNumbers,
    [communicationPhoneNumbers, customerPhoneNumbers]
  );

  const phoneNumbers = useMemo(
    () =>
      !_isEmpty(fetchedPhoneNumbers)
        ? fetchedPhoneNumbers
        : [DEFAULT_PHONE_NUMBER as GroupClientPhoneNumber],
    [fetchedPhoneNumbers]
  );

  const initialValues: EmployeeDetailsForm = useMemo(
    () => ({
      customer: buildInitialEstablishedProfileDetailsFormValue(
        customerInfo as GroupClientCustomerInfo
      ),
      phoneNumbers,
      preferredPhoneNumberIndex: phoneNumbers?.length === 1 ? 0 : -1
    }),
    [customerInfo, phoneNumbers]
  );

  const hasAddress = Boolean(customerInfo?.address);

  const isValuesChanged = (
    data: EmployeeDetailsForm,
    key: keyof EmployeeDetailsForm
  ) => {
    return !_isEqual(data[key], initialValues[key]);
  };

  const handleSubmit = async (formValues: EmployeeDetailsForm) => {
    if (hasCustomerInfoPermission && isValuesChanged(formValues, 'customer')) {
      const requestData = _assign({}, customerInfo, formValues.customer);
      try {
        await editCustomerInfo(requestData);
      } catch (error) {
        if (error instanceof ConcurrencyUpdateError) {
          throw error;
        } else {
          setUpdateError(error);
          return;
        }
      }
    }

    if (
      hasPhoneNumberPermission &&
      hasCustomerCommunicationPreferencesGetSingle &&
      hasUpdateCustomerCommunicationPreferencesPermission
    ) {
      const chosenPhoneNumber = (formValues.phoneNumbers || [])[
        formValues.preferredPhoneNumberIndex
      ];

      let communicationId = chosenPhoneNumber.id;

      if (_isNil(chosenPhoneNumber.id)) {
        const newPhoneNumber = await addEmployeePhoneNumber(
          customerInfo?.id!,
          chosenPhoneNumber
        );

        communicationId = newPhoneNumber.id!;
      }

      await linkCustomerCommunicationPreference(customerInfo?.id!, {
        id: communicationId,
        resource: CommunicationPreferencesResource.PHONE_NUMBER,
        relName: ''
      });
    }

    onFinish(formValues);
  };

  const isContentEmpty =
    !(hasCustomerInfoPermission || hasPhoneNumberPermission) &&
    _isEmpty(customerInfo) &&
    _isEmpty(fetchedPhoneNumbers);

  const themedStyles = useThemedStyles();
  return (
    <IntakePanel
      header={
        <FormattedMessage
          id="INTAKE.EMPLOYEE_DETAILS.HEADER"
          as="h1"
          className={classNames(
            themedStyles.emplyeeDetailsHeader,
            styles.employeeDetailsHeader
          )}
        />
      }
    >
      <ContentRenderer
        isLoading={isPendingCustomerInfo || isLoadingPhoneNumbers}
        isEmpty={isContentEmpty}
      >
        <AsteriskInfo className={styles.asterisk} />
        <FormikWithResponseHandling
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
          dataParseFunction={data => ({
            customer: data,
            phoneNumbers,
            preferredPhoneNumberIndex: phoneNumbers?.length === 1 ? 0 : -1
          })}
        >
          {({ values }: FormikProps<EmployeeDetailsForm>) => {
            return (
              <Form>
                <Row gutter={32}>
                  <Col span={24}>
                    <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.EXISTING_DESCRIPTION" />
                  </Col>

                  {hasCustomerInfoPermission && (
                    <Col className={styles.section} span={24}>
                      <ProfileDetails hasAddress={hasAddress} />
                    </Col>
                  )}

                  {hasPermissionToCommunicationPreferences && (
                    <Col className={styles.section} span={24}>
                      <PhoneCalls
                        hasPhoneNumbers={!_isEmpty(fetchedPhoneNumbers)}
                      />
                    </Col>
                  )}
                </Row>

                <IntakePanelFooter
                  nextButton={
                    <FormattedMessage
                      as={FormButton}
                      htmlType="submit"
                      shouldSubmitForm={false}
                      id={
                        isValuesChanged(values, 'customer') ||
                        isValuesChanged(values, 'preferredPhoneNumberIndex')
                          ? 'INTAKE.SAVE_AND_PROCEED'
                          : 'INTAKE.CONFIRM_AND_PROCEED'
                      }
                    />
                  }
                />
              </Form>
            );
          }}
        </FormikWithResponseHandling>
      </ContentRenderer>

      <SomethingWentWrongModal
        response={updateError}
        onCancel={() => setUpdateError(null)}
      />
    </IntakePanel>
  );
};
