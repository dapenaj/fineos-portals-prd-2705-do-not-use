/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Field, getIn, useFormikContext, FormikErrors } from 'formik';
import {
  FormGroup,
  TextInput,
  Button,
  ButtonType,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage
} from 'fineos-common';
import { Row, Col } from 'antd';

import { AddressInput } from '../../../../../../shared';
import { EstablishedProfileDetailsFormValue } from '../profile-details.form';

import styles from './profile-details.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  subHeader: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

type ProfileDetailsFormProps = {
  onOkClick: () => void;
  onCancelClick: () => void;
  hasAddress?: boolean;
};

export const ProfileDetailsForm: React.FC<ProfileDetailsFormProps> = ({
  onOkClick,
  onCancelClick,
  hasAddress = true
}) => {
  const themedStyles = useThemedStyles();
  const formik = useFormikContext<EstablishedProfileDetailsFormValue>();

  // we want here to simulate formik behavior: when validation finds errors we move focus into first input with error
  const focusFirstElementWithError = (
    customerErrors: Partial<EstablishedProfileDetailsFormValue>
  ) => {
    const errorKeys = Object.keys(customerErrors);
    const firstErrorFromList = errorKeys[0];
    let elementToFocus: HTMLElement;
    if (firstErrorFromList !== 'address') {
      const elementsWithErrors = document.getElementsByName(
        `customer.${firstErrorFromList}`
      );
      elementToFocus = elementsWithErrors[0];
    } else {
      // if this is address we have to get address element
      const addressKeys = customerErrors.address
        ? Object.keys(customerErrors?.address)
        : [];
      const elementsWithErrors = document.getElementsByName(
        `customer.address.${addressKeys[0]}`
      );
      elementToFocus = elementsWithErrors[0];
    }
    elementToFocus.focus();
  };

  const triggerValidation = () => {
    formik.setFieldTouched('customer.firstName');
    formik.setFieldTouched('customer.lastName');

    const addressValues = getIn(formik.values, 'customer.address');
    for (const addressField of Object.keys(addressValues)) {
      formik.setFieldTouched('customer.address.' + addressField);
    }
    formik
      .validateForm()
      .then((errors: FormikErrors<EstablishedProfileDetailsFormValue>) => {
        const customerErrors = getIn(errors || {}, 'customer') || {};
        if (Object.keys(customerErrors).length === 0) {
          onOkClick();
        } else {
          focusFirstElementWithError(customerErrors);
        }
      });
  };

  return (
    <Row gutter={32}>
      <Col span={12}>
        <FormGroup
          isRequired={true}
          label={<FormattedMessage id="COMMON.FIELDS.FIRST_NAME_LABEL" />}
        >
          <Field name="customer.firstName" component={TextInput} />
        </FormGroup>
      </Col>

      <Col span={12}>
        <FormGroup
          isRequired={true}
          label={<FormattedMessage id="COMMON.FIELDS.LAST_NAME_LABEL" />}
        >
          <Field name="customer.lastName" component={TextInput} />
        </FormGroup>
      </Col>

      {!hasAddress && (
        <Col>
          <p>
            <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.NO_ADDRESS_MESSAGE" />
          </p>
        </Col>
      )}

      <Col>
        <FormattedMessage
          id="INTAKE.EMPLOYEE_DETAILS.PRIMARY_ADDRESS.HEADER"
          as="h2"
          className={classNames(themedStyles.subHeader, styles.subHeader)}
        />

        <AddressInput name="customer.address" />
      </Col>
    </Row>
  );
};
