/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { getIn, useFormikContext } from 'formik';
import classNames from 'classnames';
import {
  Button,
  ButtonType,
  TelLink,
  FormattedMessage,
  createThemedStyles,
  getFocusedElementShadow
} from 'fineos-common';

import { EstablishedPhoneCallsFormValue } from '../../phone-calls.form';

import styles from './phone-numbers-renderer.module.scss';

type PhoneNumbersRendererProps = {
  onEditClick: () => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  phoneLink: {
    '&:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  }
}));

export const PhoneNumbersRenderer: React.FC<PhoneNumbersRendererProps> = ({
  onEditClick
}) => {
  const { values } = useFormikContext<EstablishedPhoneCallsFormValue>();
  const phoneNumbers = getIn(values, 'phoneNumbers');
  const preferredPhoneNumberIndex = getIn(values, 'preferredPhoneNumberIndex');
  const themedStyles = useThemedStyles();

  if (!phoneNumbers?.length) {
    return null;
  }
  return (
    <>
      <FormattedMessage
        id="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.CONTACT_PHONE"
        as="p"
        values={{
          phone: (
            <strong>
              <TelLink
                className={classNames(
                  styles.numberLink,
                  themedStyles.phoneLink
                )}
                phone={phoneNumbers[preferredPhoneNumberIndex]}
              />
            </strong>
          )
        }}
      />
      <FormattedMessage
        as={Button}
        id="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.EDIT_PREFERENCES"
        className={styles.linkButton}
        buttonType={ButtonType.LINK}
        onClick={onEditClick}
      />
    </>
  );
};
