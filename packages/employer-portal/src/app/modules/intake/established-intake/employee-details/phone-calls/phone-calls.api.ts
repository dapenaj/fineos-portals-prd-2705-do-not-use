/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { uniqBy as _uniqBy } from 'lodash';
import { GroupClientCustomerService } from 'fineos-js-api-client';

import { CommunicationPreferencesType } from '../../../../employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences.constant';

const groupClientCustomerService = GroupClientCustomerService.getInstance();

export const fetchCommunicationPreferencePhoneNumbers = async (
  customerId: string
) => {
  const {
    phoneNumbers
  } = await groupClientCustomerService.fetchCustomerCommunicationPreference(
    customerId,
    CommunicationPreferencesType.DIRECT_CORRESPONDENCE_ID
  );

  return _uniqBy(
    phoneNumbers,
    phoneNumber =>
      phoneNumber.intCode + phoneNumber.areaCode + phoneNumber.telephoneNo
  );
};

export const fetchCustomerPhoneNumbers = async (customerId: string) => {
  const {
    elements
  } = await groupClientCustomerService.fetchCustomerPhoneNumbers(customerId);
  return _uniqBy(
    elements,
    phoneNumber =>
      phoneNumber.intCode + phoneNumber.areaCode + phoneNumber.telephoneNo
  );
};
