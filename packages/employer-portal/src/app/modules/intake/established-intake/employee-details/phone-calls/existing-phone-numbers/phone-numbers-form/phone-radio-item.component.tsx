/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  createThemedStyles,
  primaryLight2Color,
  Panel,
  Radio
} from 'fineos-common';
import { Row, Col } from 'antd';
import classNames from 'classnames';

import styles from './phone-numbers-form.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  selectedPhoneItem: {
    backgroundColor: primaryLight2Color(theme.colorSchema)
  }
}));

export const PhoneRadioItem = ({
  isSelected,
  children,
  label,
  ...props
}: React.ComponentProps<typeof Radio> & {
  isSelected: boolean;
  label: React.ReactNode;
}) => {
  const themedStyles = useThemedStyles();

  return (
    <Panel
      data-test-el="phone-number-item"
      className={classNames(styles.phoneItem, {
        [themedStyles.selectedPhoneItem]: isSelected
      })}
    >
      <Row>
        <Col className={styles.col} xs={2} sm={1}>
          <Radio {...props}>
            <span className={styles.hideLabel}>{label}</span>
          </Radio>
        </Col>
        <Col xs={44} sm={22}>
          {children}
        </Col>
      </Row>
    </Panel>
  );
};
