/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { GroupClientCustomerInfo } from 'fineos-js-api-client';

import {
  AddressInputFormValue,
  addressInputValidation,
  initialAddressInputValue
} from '../../../../../shared';

export type EstablishedProfileDetailsFormValue = {
  firstName: string;
  lastName: string;
  address: AddressInputFormValue;
};

const emptyProfileDetailsFormValue: EstablishedProfileDetailsFormValue = {
  firstName: '',
  lastName: '',
  address: {
    ...initialAddressInputValue
  }
};

export const establishedEmployeeProfileValidation = Yup.object({
  firstName: Yup.string()
    .trim()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED'),
  lastName: Yup.string()
    .trim()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED'),
  address: addressInputValidation
});

export const buildInitialEstablishedProfileDetailsFormValue = (
  customer: GroupClientCustomerInfo | null
): EstablishedProfileDetailsFormValue => {
  if (customer) {
    return {
      firstName: customer.firstName,
      lastName: customer.lastName,
      address: customer.address || { ...initialAddressInputValue }
    };
  }

  return emptyProfileDetailsFormValue;
};
