/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { FormattedMessage } from 'fineos-common';
import { Row, Col } from 'antd';

import { PhoneInput, PhoneTypeInput } from '../../../../../../shared';

import styles from './new-phone-number.module.scss';

export const NewPhoneNumber: React.FC = () => {
  return (
    <>
      <Row gutter={[16, 16]}>
        <Col span={24}>
          <PhoneTypeInput
            name={`phoneNumbers[0].contactMethod`}
            label={
              <span className={styles.phoneTypeLabel}>
                <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.ADD_PHONE_DESCRIPTION" />
              </span>
            }
            labelClassName={styles.phoneTypeLabel}
            className={styles.dropdown}
          />
        </Col>
      </Row>

      <PhoneInput name="phoneNumbers[0]" />
    </>
  );
};
