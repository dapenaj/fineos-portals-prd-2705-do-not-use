/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Button,
  createThemedStyles,
  textStyleToCss,
  ButtonType,
  FormattedMessage,
  usePermissions,
  ManageCustomerDataPermissions
} from 'fineos-common';
import classNames from 'classnames';
import { useFormikContext, getIn } from 'formik';

import { Address } from '../../../../../../shared';
import { EstablishedProfileDetailsFormValue } from '../profile-details.form';

import styles from './profile-details-renderer.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  userName: {
    ...textStyleToCss(theme.typography.pageTitle),
    color: theme.colorSchema.neutral8
  },
  label: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss(theme.typography.smallText)
  }
}));

type ProfileDetailsRendererProps = {
  onEditClick: () => void;
};

export const ProfileDetailsRenderer: React.FC<ProfileDetailsRendererProps> = ({
  onEditClick
}) => {
  const hasCustomerInfoPermission = usePermissions(
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION
  );
  const themedStyles = useThemedStyles();
  const { values } = useFormikContext<EstablishedProfileDetailsFormValue>();
  const firstName = getIn(values, 'customer.firstName');
  const lastName = getIn(values, 'customer.lastName');
  const address = getIn(values, 'customer.address');

  return (
    <>
      <div className={themedStyles.label}>
        <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.FULL_NAME" />
      </div>

      <strong
        className={classNames(styles.userName, themedStyles.userName)}
        data-test-el="intake-username"
      >
        {firstName} {lastName}
      </strong>

      {address && (
        <>
          <div className={themedStyles.label}>
            <FormattedMessage id="COMMON.ADDRESS.SECTION_HEADER" />
          </div>

          <Address address={address} />
        </>
      )}
      {hasCustomerInfoPermission && (
        <Button
          className={styles.linkButton}
          buttonType={ButtonType.LINK}
          onClick={onEditClick}
        >
          <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.EDIT" />
        </Button>
      )}
    </>
  );
};
