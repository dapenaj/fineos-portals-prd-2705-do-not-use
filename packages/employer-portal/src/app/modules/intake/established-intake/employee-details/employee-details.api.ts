import {
  GroupClientCustomerService,
  GroupClientPhoneNumber,
  GroupClientPhoneNumberResource,
  LinkRequest,
  CommunicationPreference
} from 'fineos-js-api-client';

import { CommunicationPreferencesType } from '../../../employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences.constant';

export const addEmployeePhoneNumber = async (
  customerId: string,
  phoneNumber: GroupClientPhoneNumberResource
): Promise<GroupClientPhoneNumber> => {
  return GroupClientCustomerService.getInstance().addCustomerPhoneNumber(
    customerId,
    phoneNumber
  );
};

export const linkCustomerCommunicationPreference = async (
  customerId: string,
  body: LinkRequest
): Promise<CommunicationPreference> => {
  return GroupClientCustomerService.getInstance().linkCustomerCommunicationPreference(
    customerId,
    CommunicationPreferencesType.DIRECT_CORRESPONDENCE_ID,
    body
  );
};
