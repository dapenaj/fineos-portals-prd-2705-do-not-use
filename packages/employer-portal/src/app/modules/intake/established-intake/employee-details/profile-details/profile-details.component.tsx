/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { useFormikContext, getIn } from 'formik';
import { Panel } from 'fineos-common';

import { ProfileDetailsForm } from './profile-details-form';
import { EstablishedProfileDetailsFormValue } from './profile-details.form';
import styles from './profile-details.module.scss';

type ProfileDetailsProps = {
  hasAddress?: boolean;
};

/**
 * This is part of form with possibility to change client profile details.
 * We have two parts here. User can edit them separately and every part has validation
 * which is provided by Formik from parent form.
 * This is important that the children of ProfileDetails shouldn't trigger submit of parent form,
 * because here it only part of logic the main form.
 * @param hasAddress
 */
export const ProfileDetails: React.FC<ProfileDetailsProps> = ({
  hasAddress
}) => {
  const {
    initialValues,
    setFieldValue
  } = useFormikContext<EstablishedProfileDetailsFormValue>();
  const [isEdit, setIsEdit] = useState(!hasAddress);

  return (
    <Panel className={styles.card}>
      <ProfileDetailsForm
        onOkClick={() => {
          setIsEdit(false);
        }}
        onCancelClick={() => {
          setFieldValue('customer', getIn(initialValues, 'customer'));
          setIsEdit(false);
        }}
        hasAddress={hasAddress}
      />
    </Panel>
  );
};
