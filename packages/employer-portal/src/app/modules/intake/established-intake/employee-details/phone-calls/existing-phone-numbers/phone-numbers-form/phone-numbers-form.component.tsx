/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import { Row, Col } from 'antd';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Field, getIn, useFormikContext } from 'formik';
import { useRawFormattedMessage } from 'fineos-common';
import { GroupClientPhoneNumber } from 'fineos-js-api-client';
import {
  Button,
  ButtonType,
  FormRadioInput,
  mapPhoneToStr,
  Phone,
  FormattedMessage,
  FormGroup,
  Icon
} from 'fineos-common';

import { PhoneInput, PhoneTypeInput } from '../../../../../../../shared';
import { EstablishedPhoneCallsFormValue } from '../../phone-calls.form';

import { PhoneRadioItem } from './phone-radio-item.component';
import styles from './phone-numbers-form.module.scss';

export const PhoneNumbersForm: React.FC = () => {
  const { values, setFieldValue } = useFormikContext<
    EstablishedPhoneCallsFormValue
  >();
  const phoneNumbers = getIn(values, 'phoneNumbers');
  const preferredPhoneNumberIndex = getIn(values, 'preferredPhoneNumberIndex');

  const hasNewPhoneEntry = phoneNumbers.some(
    (phoneNumber: GroupClientPhoneNumber) => !phoneNumber.id
  );

  const phoneTypeInputLabel = (
    <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.LABEL" />
  );
  const rawLabelForPhoneTypeInput = useRawFormattedMessage(phoneTypeInputLabel);

  const deleteNewPhoneEntry = () => {
    const newPhoneNumbers = phoneNumbers.filter(
      (phoneNumber: GroupClientPhoneNumber) => phoneNumber.id
    );

    setFieldValue('phoneNumbers', newPhoneNumbers);
    setFieldValue('preferredPhoneNumberIndex', 0);
  };

  return (
    <>
      <FormGroup
        isRequired={true}
        showErrorMessage={true}
        label={
          <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.SELECT_PHONE_NUMBER" />
        }
      >
        <Field
          component={FormRadioInput}
          name="preferredPhoneNumberIndex"
          className={styles.phoneRadioGroup}
        >
          {phoneNumbers.map(
            (phoneNumber: GroupClientPhoneNumber, i: number) => (
              <div
                onClick={() => {
                  setFieldValue('preferredPhoneNumberIndex', i);
                }}
                className={styles.phoneRadio}
                key={phoneNumber.id ? mapPhoneToStr(phoneNumber) : 'new-phone'}
              >
                <PhoneRadioItem
                  value={i}
                  className={classNames({
                    [styles.newPhoneItem]: !phoneNumber.id
                  })}
                  isSelected={i === preferredPhoneNumberIndex}
                  label={
                    phoneNumber.id ? (
                      <Phone>{phoneNumber}</Phone>
                    ) : (
                      rawLabelForPhoneTypeInput
                    )
                  }
                >
                  {phoneNumber.id ? (
                    <Phone>{phoneNumber}</Phone>
                  ) : (
                    <>
                      <Row gutter={[32, 32]}>
                        <Col xs={22}>
                          <PhoneTypeInput
                            name={`phoneNumbers[${i}].contactMethod`}
                            onClick={(e: React.MouseEvent) => {
                              setFieldValue('preferredPhoneNumberIndex', i);
                              e.preventDefault();
                            }}
                          />
                        </Col>
                        <Col xs={2} className={styles.delete}>
                          <Button
                            className={styles.deleteButton}
                            data-test-el="delete-phone-number"
                            buttonType={ButtonType.GHOST}
                            onClick={(e: React.MouseEvent) => {
                              e.stopPropagation();
                              e.preventDefault();

                              deleteNewPhoneEntry();
                            }}
                          >
                            <Icon
                              icon={faTrashAlt}
                              iconLabel={
                                <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.DELETE_PHONE_NUMBER_LABEL" />
                              }
                            />
                          </Button>
                        </Col>
                      </Row>
                      <Row>
                        <PhoneInput
                          onInputClick={() => {
                            setFieldValue('preferredPhoneNumberIndex', i);
                          }}
                          name={`phoneNumbers[${i}]`}
                        />
                      </Row>
                    </>
                  )}
                </PhoneRadioItem>
              </div>
            )
          )}
        </Field>
      </FormGroup>

      {!hasNewPhoneEntry && (
        <Button
          className={styles.linkButton}
          buttonType={ButtonType.LINK}
          onClick={() => {
            const newPhoneNumbers = [
              ...phoneNumbers,
              {
                intCode: '',
                areaCode: '',
                telephoneNo: ''
              }
            ];

            setFieldValue('phoneNumbers', newPhoneNumbers);
          }}
        >
          <FormattedMessage id="INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.USE_DIFFERENT_PHONE" />
        </Button>
      )}
    </>
  );
};
