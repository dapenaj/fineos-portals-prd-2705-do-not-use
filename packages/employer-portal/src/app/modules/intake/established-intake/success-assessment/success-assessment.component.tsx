/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  ConfigurableText,
  createThemedStyles,
  textStyleToCss,
  Panel,
  FormattedMessage,
  getFocusedElementShadow
} from 'fineos-common';
import classNames from 'classnames';
import { Row, Col } from 'antd';
import { CreatedGroupClientNotification } from 'fineos-js-api-client';

import styles from './success-assessment.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  notificationId: {
    ...textStyleToCss(theme.typography.pageTitle),
    color: theme.colorSchema.neutral9
  },
  policy: {
    ...textStyleToCss(theme.typography.smallText)
  },
  successDetails: {
    '& a:focus': {
      borderColor: theme.colorSchema.primaryAction,
      boxShadow: getFocusedElementShadow({
        color: theme.colorSchema.primaryAction
      })
    }
  }
}));

type EstablishedSuccessAssessmentProps = {
  notification: CreatedGroupClientNotification;
};

export const EstablishedSuccessAssessment: React.FC<EstablishedSuccessAssessmentProps> = ({
  notification: {
    notificationId,
    customer: { name }
  }
}) => {
  const themedStyles = useThemedStyles();

  return (
    <>
      <Row gutter={[64, 64]}>
        <Col>
          <ConfigurableText
            id="INTAKE.ESTABLISHED.SUCCESS.SUB_TITLE"
            values={{
              name
            }}
          />
        </Col>
      </Row>
      <Row gutter={[32, 64]}>
        <Col xs={15}>
          <Panel className={styles.notificationWrapper}>
            <div>
              <FormattedMessage id="INTAKE.ESTABLISHED.SUCCESS.NOTIFICATION_REFERENCE" />
            </div>
            <strong
              data-test-el="success-assessment-notification-id"
              className={classNames(
                styles.notificationId,
                themedStyles.notificationId
              )}
            >
              {notificationId}
            </strong>
            <div>
              <FormattedMessage id="INTAKE.ESTABLISHED.SUCCESS.NOTIFICATION_REFERENCE_INFO" />
            </div>
          </Panel>
        </Col>
        <Col xs={9}>
          <ConfigurableText
            className={classNames(
              styles.successDetails,
              themedStyles.successDetails
            )}
            id="INTAKE.ESTABLISHED.SUCCESS.ADDITIONAL_DETAILS"
          />
        </Col>
      </Row>
      <Row gutter={[64, 64]}>
        <Col xs={24}>
          <div className={themedStyles.policy}>
            <ConfigurableText id="INTAKE.ESTABLISHED.SUCCESS.PRIVACY_NOTICE" />
          </div>
        </Col>
      </Row>
    </>
  );
};
