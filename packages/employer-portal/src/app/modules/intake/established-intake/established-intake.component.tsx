/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import moment from 'moment';
import {
  ContentRenderer,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import {
  StartNotificationRequest,
  CreatedGroupClientNotification
} from 'fineos-js-api-client';
import { assign as _assign } from 'lodash';

import {
  PageLayout,
  usePreloadEnumDomain,
  EnumDomain,
  OccupationEarning,
  formatPhoneForDisplay
} from '../../../shared';
import {
  IntakeErrorAssessment,
  InitialRequest,
  InitialRequestFormValue,
  IntakeSubmission,
  IntakeWrapper
} from '../common';
import { useIntakeLock } from '../intake.service';

import {
  EmployeeDetailsForm,
  EstablishedEmployeeDetails
} from './employee-details';
import { EstablishedOccupationAndEarnings } from './occupation-and-earnings';
import { addEstablishedIntake } from './established-intake.api';
import { EstablishedSuccessAssessment } from './success-assessment';

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.displayTitle)
  }
}));

type EstablishedIntakeProps = {
  employeeId: string;
};

type PartialEstablishedIntakeValue = {
  employeeDetails?: EmployeeDetailsForm;
  occupationEarnings?: OccupationEarning;
  initialRequest?: InitialRequestFormValue;
};

export const EstablishedIntake: React.FC<EstablishedIntakeProps> = ({
  employeeId
}) => {
  const [formValue, setFormValue] = useState<PartialEstablishedIntakeValue>({});
  const [
    notification,
    setNotification
  ] = useState<CreatedGroupClientNotification | null>(null);
  const [responseError, setResponseError] = useState<Error | null>(null);
  const [isRequestPending, setIsRequestPending] = useState<boolean>(false);

  usePreloadEnumDomain(EnumDomain.NOTIFICATION_REASON);

  const handleRequest = async (data: InitialRequestFormValue) => {
    const mapData: StartNotificationRequest = {
      description: data.notificationDescription!,
      lastWorkingDate: moment(data.notificationLastWorkingDay!),
      notificationReason: data.notificationReason!.name,
      customerId: employeeId,

      // This info is used to create a new Person with a role of Notifier
      // The info should belong to the logged in user, but this is not available in the portal.
      // For now, the employee info is supplied instead.
      notifierFirstName: formValue.employeeDetails!.customer.firstName,
      notifierLastName: formValue.employeeDetails!.customer.lastName,
      notifierPhone: formValue.employeeDetails!.phoneNumbers
        ? formatPhoneForDisplay(
            formValue.employeeDetails!.phoneNumbers[
              formValue.employeeDetails!.preferredPhoneNumberIndex
            ]
          )
        : ''
    };

    try {
      setIsRequestPending(true);
      const response = await addEstablishedIntake(mapData);

      setNotification(response);
    } catch (e) {
      setResponseError(e);
    } finally {
      setIsRequestPending(false);
    }
  };

  const isSubmitted = Boolean(notification || responseError);
  useIntakeLock(isSubmitted);

  const themedStyles = useThemedStyles();
  return !isRequestPending && !isSubmitted ? (
    <IntakeWrapper>
      <EstablishedEmployeeDetails
        onFinish={(values: EmployeeDetailsForm) => {
          setFormValue(_assign({}, formValue, { employeeDetails: values }));
        }}
        employeeId={employeeId}
      />

      <EstablishedOccupationAndEarnings
        employeeId={employeeId}
        onFinish={values => {
          setFormValue(_assign({}, formValue, { occupationEarnings: values }));
        }}
      />

      <InitialRequest
        customInitialValues={formValue.initialRequest || null}
        onFinish={(values: InitialRequestFormValue) => {
          setFormValue(_assign({}, formValue, { initialRequest: values }));
        }}
        onSubmit={handleRequest}
        jobStartDate={formValue.occupationEarnings?.jobStartDate || null}
      />
    </IntakeWrapper>
  ) : (
    <PageLayout showBackgroundImage={true}>
      <ContentRenderer isLoading={isRequestPending} isEmpty={false}>
        {notification && (
          <IntakeSubmission
            title={
              <FormattedMessage
                id="INTAKE.ESTABLISHED.SUCCESS.TITLE"
                as="h1"
                className={themedStyles.title}
              />
            }
            showSuccessTick={true}
          >
            <EstablishedSuccessAssessment notification={notification} />
          </IntakeSubmission>
        )}

        {responseError && (
          <IntakeSubmission
            title={<FormattedMessage id="INTAKE.ERROR.TITLE" />}
          >
            <IntakeErrorAssessment />
          </IntakeSubmission>
        )}
      </ContentRenderer>
    </PageLayout>
  );
};
