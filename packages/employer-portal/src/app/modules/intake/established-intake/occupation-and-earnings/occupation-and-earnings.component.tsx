/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useMemo } from 'react';
import {
  Panel,
  Button,
  ButtonType,
  PropertyItem,
  FormattedDate,
  ConfigurableText,
  useAsync,
  usePermissions,
  ViewOccupationPermissions,
  ContentRenderer,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { Col, Row } from 'antd';
import classNames from 'classnames';
import { isEmpty as _isEmpty } from 'lodash';

import {
  getEarningsFrequency,
  fetchContractualEarnings,
  OccupationEarning,
  unscaledAmount,
  Currency
} from '../../../../shared';
import { IntakePanel } from '../../common';
import { fetchCustomerOccupationByDate } from '../established-intake.api';
import { IntakePanelFooter } from '../../common/intake-panel-footer';

import styles from './occupation-and-earnings.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

type EstablishedOccupationAndEarningsProps = {
  employeeId: string;
  onFinish: (value: OccupationEarning | null) => void;
};

export const EstablishedOccupationAndEarnings: React.FC<EstablishedOccupationAndEarningsProps> = ({
  employeeId,
  onFinish
}) => {
  const [areEarningsVisible, setAreEarningsVisible] = useState(false);

  const hasViewCustomerOccupationsPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
  );

  const hasContractualEarningsPermission = usePermissions(
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
  );

  const {
    state: { value: customerOccupation, isPending: isLoadingCustomerOccupation }
  } = useAsync(fetchCustomerOccupationByDate, [employeeId], {
    shouldExecute: () => hasViewCustomerOccupationsPermission
  });

  const customerOccupationId = useMemo(
    () => (!!customerOccupation ? customerOccupation.id : ''),
    [customerOccupation]
  );

  const {
    state: {
      value: contractualEarnings,
      isPending: isLoadingContractualEarnings
    }
  } = useAsync(fetchContractualEarnings, [employeeId, customerOccupationId], {
    shouldExecute: () =>
      !!customerOccupationId &&
      hasContractualEarningsPermission &&
      !isLoadingCustomerOccupation
  });
  const isContentEmpty =
    !(
      hasViewCustomerOccupationsPermission || hasContractualEarningsPermission
    ) &&
    _isEmpty(customerOccupation) &&
    _isEmpty(contractualEarnings);

  const themedStyles = useThemedStyles();
  return (
    <IntakePanel
      header={
        <FormattedMessage
          id="INTAKE.OCCUPATION_AND_EARNINGS.HEADER"
          as="h1"
          className={classNames(themedStyles.header, styles.header)}
        />
      }
    >
      <ContentRenderer
        isLoading={isLoadingCustomerOccupation || isLoadingContractualEarnings}
        isEmpty={isContentEmpty}
      >
        <Row gutter={32}>
          <Col span={24}>
            <ConfigurableText id="INTAKE.OCCUPATION_AND_EARNINGS.EXISTING_DESCRIPTION" />
          </Col>
          <Col span={24} className={styles.section}>
            <Panel className={styles.card}>
              <Row>
                <Col span={12}>
                  <Row gutter={[32, 16]}>
                    <Col span={24}>
                      <PropertyItem
                        label={
                          <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE" />
                        }
                      >
                        {customerOccupation?.jobTitle}
                      </PropertyItem>
                    </Col>
                  </Row>
                  <Row gutter={[32, 16]}>
                    <Col span={24}>
                      <PropertyItem
                        label={
                          <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE" />
                        }
                      >
                        {!!customerOccupation?.jobStartDate && (
                          <FormattedDate
                            date={customerOccupation.jobStartDate}
                          />
                        )}
                      </PropertyItem>
                    </Col>
                  </Row>
                </Col>

                {hasContractualEarningsPermission && (
                  <Col span={12}>
                    <PropertyItem
                      label={
                        <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS" />
                      }
                    >
                      {!!contractualEarnings?.frequency.name && (
                        <>
                          {areEarningsVisible && (
                            <FormattedMessage
                              id="INTAKE.OCCUPATION_AND_EARNINGS.SALARY"
                              values={{
                                frequency: (
                                  <FormattedMessage
                                    id={`PROFILE.EMPLOYMENT_DETAILS_TAB.EARNINGS_FREQUENCIES.${getEarningsFrequency(
                                      contractualEarnings.frequency.name
                                    )}`}
                                  />
                                ),
                                amount: (
                                  <Currency
                                    amount={unscaledAmount(
                                      contractualEarnings.amount
                                        .amountMinorUnits,
                                      contractualEarnings.amount.scale
                                    )}
                                  />
                                )
                              }}
                            />
                          )}
                          <FormattedMessage
                            className={classNames(
                              areEarningsVisible
                                ? styles.hideEarningsButton
                                : styles.showEarningsButton
                            )}
                            as={Button}
                            buttonType={ButtonType.LINK}
                            onClick={() =>
                              setAreEarningsVisible(areVisible => !areVisible)
                            }
                            id={
                              areEarningsVisible
                                ? 'INTAKE.OCCUPATION_AND_EARNINGS.HIDE_EARNINGS'
                                : 'INTAKE.OCCUPATION_AND_EARNINGS.SHOW_EARNINGS'
                            }
                          />
                        </>
                      )}
                    </PropertyItem>
                  </Col>
                )}
              </Row>
            </Panel>
          </Col>
        </Row>
      </ContentRenderer>
      <IntakePanelFooter
        nextButton={
          <FormattedMessage
            as={Button}
            id="INTAKE.PROCEED"
            onClick={() => onFinish(customerOccupation)}
          />
        }
      />
    </IntakePanel>
  );
};
