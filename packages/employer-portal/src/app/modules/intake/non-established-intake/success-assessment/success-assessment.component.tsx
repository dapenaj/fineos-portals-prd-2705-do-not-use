/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { useIntl } from 'react-intl';
import {
  ConfigurableText,
  createThemedStyles,
  textStyleToCss,
  useTheme
} from 'fineos-common';
import { NotificationEformRequest } from 'fineos-js-api-client';
import classNames from 'classnames';

import styles from './success-assessment.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  employer: {
    color: theme.colorSchema.primaryColor
  },
  happensNextTitle: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.displaySmall)
  },
  policy: {
    ...textStyleToCss(theme.typography.smallText)
  }
}));

type NonEstablishedSuccessAssessmentProps = {
  intakeRequest: NotificationEformRequest;
};

export const NonEstablishedSuccessAssessment: React.FC<NonEstablishedSuccessAssessmentProps> = ({
  intakeRequest: { firstName, lastName }
}) => {
  const intl = useIntl();
  const themedStyles = useThemedStyles();
  const theme = useTheme();

  return (
    <div className={styles.wrapper}>
      <div className={styles.subtitle}>
        <ConfigurableText
          id="INTAKE.NON_ESTABLISHED.SUCCESS.SUB_TITLE"
          values={{
            name: `${firstName} ${lastName}`
          }}
        />
      </div>

      <div className={styles.graphic}>
        <div className={styles.labels}>
          <div className={classNames(styles.employer, themedStyles.employer)}>
            <ConfigurableText id="INTAKE.NON_ESTABLISHED.SUCCESS.EMPLOYER" />
          </div>
          <div>
            <ConfigurableText id="INTAKE.NON_ESTABLISHED.SUCCESS.EMPLOYEE" />
          </div>
          <div className={styles.physician}>
            <ConfigurableText id="INTAKE.NON_ESTABLISHED.SUCCESS.PHYSICIAN" />
          </div>
        </div>

        <img
          src={theme?.images?.fnolRequestAcknowledged}
          alt={intl.formatMessage({
            id: 'INTAKE.NON_ESTABLISHED.SUCCESS.IMAGE_ALT_TEXT'
          })}
          className={styles.img}
        />
      </div>

      <div className={styles.happensNext}>
        <ConfigurableText
          id="INTAKE.NON_ESTABLISHED.SUCCESS.WHAT_HAPPENS_NEXT_TITLE"
          as="h2"
          className={classNames(
            styles.happensNextTitle,
            themedStyles.happensNextTitle
          )}
        />
        <ConfigurableText id="INTAKE.NON_ESTABLISHED.SUCCESS.WHAT_HAPPENS_NEXT_MESSAGE" />
      </div>

      <div className={themedStyles.policy}>
        <ConfigurableText id="INTAKE.NON_ESTABLISHED.SUCCESS.PRIVACY_NOTICE" />
      </div>
    </div>
  );
};
