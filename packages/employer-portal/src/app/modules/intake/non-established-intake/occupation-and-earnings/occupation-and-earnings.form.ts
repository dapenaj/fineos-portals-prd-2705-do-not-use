/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { BaseDomain } from 'fineos-js-api-client';

import {
  isEnumValidationNotEmpty,
  requiredEnumValidation
} from '../../../../shared/ui/enum-select/enum-validation.util';
import {
  WeekBasedDayPatternNullable,
  WORK_PATTER_DEFAULT,
  fixedPatternSchema,
  rotatingPatternSchema,
  variablePatternSchema,
  VariablePatternFormValues,
  FixedPatternFormValues
} from '../../../../shared/work-pattern';

export type EnumSubOption = {
  value: BaseDomain;
  title: string;
};

export type EnumOptionValue = BaseDomain & {
  subOptions: EnumSubOption[];
  selectedSubOption: number;
};

export type RotatingPatternFormValues = {
  workPatternWeek1?: WeekBasedDayPatternNullable[];
  workPatternWeek2?: WeekBasedDayPatternNullable[];
  workPatternWeek3?: WeekBasedDayPatternNullable[];
  workPatternWeek4?: WeekBasedDayPatternNullable[];
};

export type Earnings = {
  amount: string;
  frequency: BaseDomain | null;
};

export type NonEstablishedOccupationAndEarningsFormValue = {
  jobTitle: string;
  dateOfHire: string | null;
  earnings: Earnings;
  workPatternType: EnumOptionValue | null;
  fixedPattern: FixedPatternFormValues;
  rotatingPattern: RotatingPatternFormValues;
  variablePattern: VariablePatternFormValues | null;
};

// @TODO find out how to obtain currency scale for non-established intake
// (when there is no occupationId and employeeId needed to contractual earnings GET request)
const DEFAULT_SCALE = 2;

export const initialNonEstablishedOccupationAndEarningsFormValue = (
  weekDayNamesTypeDomains: BaseDomain[],
  dayLength: {
    hours: number;
    minutes: number;
  }
) => ({
  jobTitle: '',
  dateOfHire: null,
  earnings: {
    amount: '',
    frequency: null
  },
  ...WORK_PATTER_DEFAULT(weekDayNamesTypeDomains, dayLength)
});

export const buildNonEstablishedOccupationAndEarningsFormValidation = () => {
  return Yup.object({
    jobTitle: Yup.string(),
    dateOfHire: Yup.mixed(),
    fixedPattern: fixedPatternSchema,
    earnings: Yup.lazy((earnings: any) =>
      earnings.amount || isEnumValidationNotEmpty(earnings.frequency)
        ? Yup.object({
            amount: Yup.number()
              .typeError('FINEOS_COMMON.VALIDATION.NOT_NUMERIC')
              .moreThan(0, 'FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO')
              .required('FINEOS_COMMON.VALIDATION.REQUIRED')
              .test(
                'is-decimal',
                'FINEOS_COMMON.VALIDATION.INVALID_PRECISION',
                value => {
                  // "." can be used as the decimalSeparator here because the value has already been converted to english
                  const splitValue = value?.toString().split('.') || [];
                  return (
                    splitValue[1]?.length <= DEFAULT_SCALE ||
                    splitValue.length === 1
                  );
                }
              ),
            frequency: requiredEnumValidation('frequency')
          })
        : Yup.object({
            amount: Yup.number()
              .typeError('FINEOS_COMMON.VALIDATION.NOT_NUMERIC')
              .moreThan(0, 'FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO')
              .nullable(),
            frequency: Yup.mixed().nullable()
          })
    ),
    variablePattern: variablePatternSchema,
    rotatingPattern: rotatingPatternSchema
  });
};
