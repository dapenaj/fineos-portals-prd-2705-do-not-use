/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useContext, useMemo } from 'react';
import classNames from 'classnames';
import {
  AsteriskInfo,
  FormGroup,
  TextInput,
  DatePickerInput,
  FormButton,
  Form,
  FormattedMessage,
  createThemedStyles,
  textStyleToCss,
  NumberInput
} from 'fineos-common';
import { BaseDomain } from 'fineos-js-api-client';
import { Field, Formik } from 'formik';
import { Row, Col } from 'antd';

import { IntakePanel, IntakePanelFooter } from '../../common';
import { LanguageContext } from '../../../../shared/layouts/language/language.context';
import { EarningFrequencyTypeSelect } from '../../../../shared/ui/earning-frequency-select';
import { WorkPatternTypeSection } from '../../../../shared/work-pattern/work-pattern-type-select';

import {
  NonEstablishedOccupationAndEarningsFormValue,
  initialNonEstablishedOccupationAndEarningsFormValue,
  buildNonEstablishedOccupationAndEarningsFormValidation
} from './occupation-and-earnings.form';
import styles from './occupation-and-earnings.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

type NonEstablishedOccupationAndEarningsProps = {
  customInitialValues: NonEstablishedOccupationAndEarningsFormValue | null;
  onFinish: (values: NonEstablishedOccupationAndEarningsFormValue) => void;
  weekDayNamesTypeDomains: BaseDomain[];
  dayLength: {
    hours: number;
    minutes: number;
  };
};

export const NonEstablishedOccupationAndEarnings: React.FC<NonEstablishedOccupationAndEarningsProps> = ({
  customInitialValues,
  onFinish,
  weekDayNamesTypeDomains,
  dayLength
}) => {
  const initialValues = useMemo(
    () =>
      customInitialValues ||
      initialNonEstablishedOccupationAndEarningsFormValue(
        weekDayNamesTypeDomains,
        dayLength
      ),
    [customInitialValues, weekDayNamesTypeDomains, dayLength]
  );
  const themedStyles = useThemedStyles();
  const { decimalSeparator } = useContext(LanguageContext);

  return (
    <IntakePanel
      header={
        <FormattedMessage
          id="INTAKE.OCCUPATION_AND_EARNINGS.HEADER"
          as="h1"
          className={classNames(themedStyles.header, styles.header)}
        />
      }
    >
      <AsteriskInfo className={styles.asterisk} />
      <Formik
        validationSchema={buildNonEstablishedOccupationAndEarningsFormValidation()}
        initialValues={initialValues}
        validateOnMount={true}
        onSubmit={(values: NonEstablishedOccupationAndEarningsFormValue) => {
          onFinish(values);
        }}
      >
        <Form className={styles.occupationEarningsForm}>
          <Row gutter={32} data-test-el="intake-occupation-and-earnings">
            <Col span={24}>
              <FormGroup
                label={
                  <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE" />
                }
                data-test-el="job-title-field"
              >
                <Field name="jobTitle" component={TextInput} />
              </FormGroup>
            </Col>
            <Col span={24}>
              <FormGroup
                label={
                  <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE" />
                }
                data-test-el="date-of-hire-field"
              >
                <Field name="dateOfHire" component={DatePickerInput} />
              </FormGroup>
            </Col>
            <Col span={12}>
              <FormGroup
                label={
                  <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS" />
                }
                data-test-el="earnings-field"
              >
                <Field
                  name="earnings.amount"
                  virtualGroup="earnings"
                  component={NumberInput}
                  decimalSeparator={decimalSeparator}
                />
              </FormGroup>
            </Col>
            <Col span={12}>
              <FormGroup
                label={
                  <FormattedMessage id="INTAKE.OCCUPATION_AND_EARNINGS.FREQUENCY" />
                }
                data-test-el="earnings-frequency-field"
              >
                <Field
                  name="earnings.frequency"
                  virtualGroup="earnings"
                  component={EarningFrequencyTypeSelect}
                />
              </FormGroup>
            </Col>

            <WorkPatternTypeSection
              shouldSkipUnknownWorkPatternFiltering={false}
            />
          </Row>

          <IntakePanelFooter
            nextButton={
              <FormattedMessage
                as={FormButton}
                htmlType="submit"
                shouldSubmitForm={false}
                id="INTAKE.PROCEED"
              />
            }
          />
        </Form>
      </Formik>
    </IntakePanel>
  );
};
