/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { NotificationEformRequest } from 'fineos-js-api-client';
import { isEmpty as _isEmpty } from 'lodash';

import {
  formatPhoneForDisplay,
  formatAddressForDisplay,
  formatDateForApi,
  DisplayAddress,
  PhoneType
} from '../../../shared';
import { isEnumValidationNotEmpty } from '../../../shared/ui/enum-select/enum-validation.util';
import {
  isFixedWorkPatternType,
  isRotatingWorkPatternType,
  isVariableWorkPatternType,
  WeekBasedDayPatternNullable,
  VariablePatternFormValues
} from '../../../shared/work-pattern';

import { NonEstablishedIntakeValue } from './non-established-intake.form';

const STANDARD_WORK_WEEK = 'mon:8:00, tue:8:00, wed:8:00, thu:8:00, fri:8:00';

const formatPhoneDetails = (
  phoneType: PhoneType,
  phoneNumber: Parameters<typeof formatPhoneForDisplay>[0]
) => {
  let formattedPhoneType: string;

  switch (phoneType) {
    case PhoneType.MOBILE:
      formattedPhoneType = 'Mobile';
      break;
    case PhoneType.CELL:
      formattedPhoneType = 'Cell';
      break;
    case PhoneType.PHONE:
      formattedPhoneType = 'Phone';
      break;
  }

  return `${formattedPhoneType} ${formatPhoneForDisplay(phoneNumber)}`;
};

const getWorkWeekPattern = (
  weekBasedDayPatterns: WeekBasedDayPatternNullable[]
): string => {
  let pattern = '';
  const weekBasedDayPatternsNotNull = weekBasedDayPatterns.filter(
    weekBasedDayPattern =>
      weekBasedDayPattern.hours !== null && weekBasedDayPattern.minutes !== null
  );

  if (weekBasedDayPatternsNotNull) {
    pattern = weekBasedDayPatternsNotNull.reduce(
      (first: string, second: WeekBasedDayPatternNullable) =>
        first +
        `${second.dayOfWeek.name.substring(0, 3).toLowerCase()}:${
          second.hours
        }:${second.minutes}, `,
      ''
    );
  }

  return pattern.slice(0, -2);
};

const getWorkPatternOtherInfo = (
  pattern: VariablePatternFormValues
): string => {
  const hasWeeklyInfo =
    pattern.workPatternRepeatsWeekly &&
    !_isEmpty(String(pattern.numberOfWeeks));
  const hasDayInfo =
    pattern.workDayIsSameLength && !_isEmpty(pattern.dayLength);

  const weeklyInfo = (weeks: string) => `Pattern repeats for ${weeks} weeks.`;
  const dayInfo = (length: string) =>
    `Work day is always the same length: ${length}.`;

  if (hasWeeklyInfo && hasDayInfo) {
    return `${weeklyInfo(String(pattern.numberOfWeeks!))} ${dayInfo(
      `${pattern.dayLength.hours}:${pattern.dayLength.minutes}`
    )} ${pattern.additionalInformation}`;
  } else if (hasWeeklyInfo && !hasDayInfo) {
    return `${weeklyInfo(String(pattern.numberOfWeeks!))} ${
      pattern.additionalInformation
    }`;
  } else if (!hasWeeklyInfo && hasDayInfo) {
    return `${dayInfo(
      `${pattern.dayLength.hours}:${pattern.dayLength.minutes}`
    )} ${pattern.additionalInformation}`;
  }

  return pattern.additionalInformation;
};

export const buildNonEstablishedIntakeEForm = ({
  employeeDetails,
  initialRequest,
  occupationEarnings
}: NonEstablishedIntakeValue) => {
  const request = new NotificationEformRequest();

  request.firstName = employeeDetails.firstName;

  request.lastName = employeeDetails.lastName;

  request.phoneNumber = formatPhoneDetails(
    employeeDetails.phoneType!.name as PhoneType,
    employeeDetails.preferredPhoneNumber
  );

  if (employeeDetails.address.country) {
    request.address = formatAddressForDisplay(
      employeeDetails.address as DisplayAddress
    );
  }

  request.notificationReason = initialRequest.notificationReason!;

  if (!_isEmpty(employeeDetails.email)) {
    request.email = employeeDetails.email;
  }

  if (!_isEmpty(initialRequest.notificationLastWorkingDay)) {
    request.notificationLastWorkingDay = formatDateForApi(
      initialRequest.notificationLastWorkingDay!
    );
  }

  if (!_isEmpty(initialRequest.notificationDescription)) {
    request.notificationDescription = initialRequest.notificationDescription;
  }

  if (!_isEmpty(occupationEarnings)) {
    if (occupationEarnings?.jobTitle) {
      request.jobTitle = occupationEarnings?.jobTitle;
    }

    if (occupationEarnings?.dateOfHire) {
      request.dateOfHire = formatDateForApi(occupationEarnings?.dateOfHire);
    }

    if (occupationEarnings?.earnings.amount) {
      request.earnings = `${occupationEarnings?.earnings.amount}`;
    }

    if (isEnumValidationNotEmpty(occupationEarnings?.earnings.frequency)) {
      request.earningFrequency = occupationEarnings?.earnings.frequency;
    }

    if (isEnumValidationNotEmpty(occupationEarnings?.workPatternType)) {
      if (
        isFixedWorkPatternType(occupationEarnings!.workPatternType) ||
        isVariableWorkPatternType(occupationEarnings!.workPatternType)
      ) {
        request.workPatternType = occupationEarnings?.workPatternType!;

        if (request.workPatternType.name === 'Fixed') {
          // Handle a Fixed Pattern
          if (!occupationEarnings!.fixedPattern.isNonStandardWeek) {
            request.workPatternWeek1 = STANDARD_WORK_WEEK;
          } else if (
            occupationEarnings!.fixedPattern.isNonStandardWeek &&
            !_isEmpty(occupationEarnings!.fixedPattern.workWeek)
          ) {
            request.workPatternWeek1 = getWorkWeekPattern(
              occupationEarnings!.fixedPattern.workWeek
            );
          }
        } else {
          // handle a Variable Pattern
          if (occupationEarnings?.variablePattern) {
            request.workPatternOtherInfo = getWorkPatternOtherInfo(
              occupationEarnings?.variablePattern
            );
          }
        }
      } else {
        // handle a Rotating Pattern
        if (isRotatingWorkPatternType(occupationEarnings!.workPatternType)) {
          const subWorkPatternType = occupationEarnings!.workPatternType.subOptions.find(
            subOption =>
              subOption.value.fullId ===
              occupationEarnings!.workPatternType?.selectedSubOption
          );
          if (subWorkPatternType) {
            request.workPatternType = subWorkPatternType.value;
          }
          if (!_isEmpty(occupationEarnings?.rotatingPattern.workPatternWeek1)) {
            request.workPatternWeek1 = getWorkWeekPattern(
              occupationEarnings!.rotatingPattern.workPatternWeek1!
            );
          }

          if (!_isEmpty(occupationEarnings?.rotatingPattern.workPatternWeek2)) {
            request.workPatternWeek2 = getWorkWeekPattern(
              occupationEarnings!.rotatingPattern.workPatternWeek2!
            );
          }

          if (!_isEmpty(occupationEarnings?.rotatingPattern.workPatternWeek3)) {
            request.workPatternWeek3 = getWorkWeekPattern(
              occupationEarnings!.rotatingPattern.workPatternWeek3!
            );
          }

          if (!_isEmpty(occupationEarnings?.rotatingPattern.workPatternWeek4)) {
            request.workPatternWeek4 = getWorkWeekPattern(
              occupationEarnings!.rotatingPattern.workPatternWeek4!
            );
          }
        }
      }
    }
  }

  return request;
};
