/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  AsteriskInfo,
  FormGroup,
  TextInput,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage,
  FormButton,
  Form
} from 'fineos-common';
import { Field, Formik } from 'formik';
import { Row, Col } from 'antd';

import { AddressInput, PhoneInput, PhoneTypeInput } from '../../../../shared';
import { IntakePanel, IntakePanelFooter } from '../../common';

import {
  nonEstablishedEmployeeDetailsValidation,
  initialNonEstablishedEmployeeDetailsFormValue,
  NonEstablishedEmployeeDetailsFormValue
} from './employee-details.form';
import styles from './employee-details.module.scss';

type NonEstablishedEmployeeDetailsProps = {
  customInitialValues: NonEstablishedEmployeeDetailsFormValue | null;
  onFinish: (values: NonEstablishedEmployeeDetailsFormValue) => void;
};

const useThemedStyles = createThemedStyles(theme => ({
  employeeDetailsHeader: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  subHeader: {
    color: theme.colorSchema.neutral7,
    ...textStyleToCss(theme.typography.panelTitle)
  }
}));

export const NonEstablishedEmployeeDetails: React.FC<NonEstablishedEmployeeDetailsProps> = ({
  customInitialValues,
  onFinish
}) => {
  const themedStyles = useThemedStyles();

  return (
    <IntakePanel
      header={
        <FormattedMessage
          id="INTAKE.EMPLOYEE_DETAILS.HEADER"
          as="h1"
          className={classNames(
            themedStyles.employeeDetailsHeader,
            styles.header
          )}
        />
      }
    >
      <AsteriskInfo className={styles.asterisk} />
      <Formik
        validationSchema={nonEstablishedEmployeeDetailsValidation}
        initialValues={
          customInitialValues || initialNonEstablishedEmployeeDetailsFormValue
        }
        validateOnMount={true}
        onSubmit={(values: NonEstablishedEmployeeDetailsFormValue) => {
          onFinish(values);
        }}
      >
        <Form>
          <Row gutter={32} data-test-el="intake-employee-details">
            <Col span={24}>
              <FormattedMessage
                id="INTAKE.EMPLOYEE_DETAILS.NEW_DESCRIPTION"
                as="p"
              />
            </Col>

            <Col span={12}>
              <FormGroup
                isRequired={true}
                label={<FormattedMessage id="COMMON.FIELDS.FIRST_NAME_LABEL" />}
                data-test-el="first-name-field"
              >
                <Field name="firstName" component={TextInput} />
              </FormGroup>
            </Col>

            <Col span={12}>
              <FormGroup
                isRequired={true}
                label={<FormattedMessage id="COMMON.FIELDS.LAST_NAME_LABEL" />}
                data-test-el="last-name-field"
              >
                <Field name="lastName" component={TextInput} />
              </FormGroup>
            </Col>

            <Col span={24}>
              <FormGroup
                label={<FormattedMessage id="COMMON.FIELDS.EMAIL_LABEL" />}
                isOptional={true}
                data-test-el="email-field"
              >
                <Field name="email" component={TextInput} />
              </FormGroup>
            </Col>

            <Col span={24}>
              <FormattedMessage
                id="INTAKE.EMPLOYEE_DETAILS.PHONE_NUMBER.HEADER"
                as="h2"
                className={classNames(themedStyles.subHeader, styles.subHeader)}
              />
            </Col>

            <Col span={24} className={styles.phoneType}>
              <PhoneTypeInput
                name="phoneType"
                data-test-el="phone-type-dropdown"
              />
            </Col>

            <Col span={24}>
              <PhoneInput name="preferredPhoneNumber" />
            </Col>

            <Col span={24}>
              <FormattedMessage
                id="INTAKE.EMPLOYEE_DETAILS.PRIMARY_ADDRESS.HEADER"
                as="h2"
                className={classNames(themedStyles.subHeader, styles.subHeader)}
              />

              <AddressInput name="address" />
            </Col>
          </Row>

          <IntakePanelFooter
            nextButton={
              <FormattedMessage
                as={FormButton}
                htmlType="submit"
                shouldSubmitForm={false}
                id="INTAKE.PROCEED"
              />
            }
          />
        </Form>
      </Formik>
    </IntakePanel>
  );
};
