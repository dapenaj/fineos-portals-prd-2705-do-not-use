/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as Yup from 'yup';
import { BaseDomain, BasePhone } from 'fineos-js-api-client';
import { newEmailValidationSchema } from 'fineos-common';

import {
  initialAddressInputValue,
  addressInputValidation,
  AddressInputFormValue
} from '../../../../shared/ui/address';
import {
  initialPhoneInputValue,
  phoneInputValidation
} from '../../../../shared';

export type NonEstablishedEmployeeDetailsFormValue = {
  firstName: string;
  lastName: string;
  email: string;
  address: AddressInputFormValue;
  preferredPhoneNumber: BasePhone;
  phoneType: BaseDomain | null;
};

export const nonEstablishedEmployeeDetailsValidation = Yup.object({
  firstName: Yup.string()
    .trim()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED'),
  lastName: Yup.string()
    .trim()
    .required('FINEOS_COMMON.VALIDATION.REQUIRED'),
  email: newEmailValidationSchema.clone(),
  address: addressInputValidation,
  preferredPhoneNumber: phoneInputValidation
});

export const initialNonEstablishedEmployeeDetailsFormValue: NonEstablishedEmployeeDetailsFormValue = {
  firstName: '',
  lastName: '',
  email: '',
  address: initialAddressInputValue,
  preferredPhoneNumber: initialPhoneInputValue,
  phoneType: null
};
