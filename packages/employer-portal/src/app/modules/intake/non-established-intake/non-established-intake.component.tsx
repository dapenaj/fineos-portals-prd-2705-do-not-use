/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState, useContext } from 'react';
import {
  useAsync,
  FormattedMessage,
  ContentRenderer,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { NotificationEformRequest } from 'fineos-js-api-client';
import { isEqual as _isEqual, assign as _assign } from 'lodash';
import moment from 'moment';

import { Config } from '../../../../app/config';
import {
  PageLayout,
  usePreloadEnumDomain,
  EnumDomain,
  useEnumDomain
} from '../../../shared';
import {
  IntakeSubmission,
  InitialRequest,
  InitialRequestFormValue,
  IntakeErrorAssessment,
  IntakeWrapper
} from '../common';
import { useIntakeLock } from '../intake.service';

import {
  NonEstablishedOccupationAndEarnings,
  initialNonEstablishedOccupationAndEarningsFormValue,
  NonEstablishedOccupationAndEarningsFormValue
} from './occupation-and-earnings';
import {
  NonEstablishedEmployeeDetails,
  NonEstablishedEmployeeDetailsFormValue
} from './employee-details';
import { addNonEstablishedIntake } from './non-established-intake.api';
import { NonEstablishedIntakeValue } from './non-established-intake.form';
import { buildNonEstablishedIntakeEForm } from './non-established-intake-eform-builder';
import { NonEstablishedSuccessAssessment } from './success-assessment';

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.displayTitle)
  }
}));

type PartialNonEstablishedIntakeValue = Partial<NonEstablishedIntakeValue>;

export const NonEstablishedIntake: React.FC = () => {
  const config = useContext(Config);
  const dayLength = config.configurableWorkPattern.FIXED_DAY_LENGTH;
  const [formValue, setFormValue] = useState<PartialNonEstablishedIntakeValue>(
    {}
  );
  const { domains: weekDayNamesTypeDomains } = useEnumDomain(
    EnumDomain.WEEK_DAYS,
    {
      shouldSkipUnknownFiltering: true
    }
  );
  usePreloadEnumDomain(EnumDomain.NOTIFICATION_REASON);

  const [
    intakeRequest,
    setIntakeRequest
  ] = useState<NotificationEformRequest | null>(null);

  const {
    state: {
      value: isIntakeSaved,
      error: failedSaveIntake,
      isPending: isSavingIntake
    }
  } = useAsync(addNonEstablishedIntake, [intakeRequest!], {
    shouldExecute: () => !!intakeRequest
  });

  const handleOnSubmit = ({
    employeeDetails,
    initialRequest,
    occupationEarnings
  }: NonEstablishedIntakeValue) => {
    setIntakeRequest(
      buildNonEstablishedIntakeEForm({
        employeeDetails,
        initialRequest,
        occupationEarnings: _isEqual(
          occupationEarnings,
          initialNonEstablishedOccupationAndEarningsFormValue(
            weekDayNamesTypeDomains,
            dayLength
          )
        )
          ? undefined
          : occupationEarnings
      })
    );
  };

  const isSubmitted = Boolean(intakeRequest);
  useIntakeLock(isSubmitted);
  const themedStyles = useThemedStyles();

  return !isSubmitted ? (
    <IntakeWrapper>
      <NonEstablishedEmployeeDetails
        customInitialValues={formValue.employeeDetails || null}
        onFinish={(values: NonEstablishedEmployeeDetailsFormValue) => {
          setFormValue(_assign({}, formValue, { employeeDetails: values }));
        }}
      />

      <NonEstablishedOccupationAndEarnings
        customInitialValues={formValue.occupationEarnings || null}
        onFinish={(values: NonEstablishedOccupationAndEarningsFormValue) => {
          setFormValue(_assign({}, formValue, { occupationEarnings: values }));
        }}
        weekDayNamesTypeDomains={weekDayNamesTypeDomains}
        dayLength={dayLength}
      />

      <InitialRequest
        customInitialValues={formValue.initialRequest || null}
        onFinish={(values: InitialRequestFormValue) => {
          setFormValue(_assign({}, formValue, { initialRequest: values }));
        }}
        onSubmit={(values: InitialRequestFormValue) => {
          handleOnSubmit(
            _assign(formValue, {
              initialRequest: values
            }) as NonEstablishedIntakeValue
          );
        }}
        jobStartDate={
          formValue.occupationEarnings?.dateOfHire
            ? moment(formValue.occupationEarnings?.dateOfHire)
            : null
        }
      />
    </IntakeWrapper>
  ) : (
    <PageLayout showBackgroundImage={true}>
      <ContentRenderer isLoading={isSavingIntake} isEmpty={false}>
        {isIntakeSaved && (
          <IntakeSubmission
            title={
              <FormattedMessage
                id="INTAKE.NON_ESTABLISHED.SUCCESS.TITLE"
                as="h1"
                className={themedStyles.title}
              />
            }
            showSuccessTick={true}
          >
            <NonEstablishedSuccessAssessment intakeRequest={intakeRequest!} />
          </IntakeSubmission>
        )}
        {failedSaveIntake && (
          <IntakeSubmission
            title={<FormattedMessage id="INTAKE.ERROR.TITLE" />}
          >
            <IntakeErrorAssessment />
          </IntakeSubmission>
        )}
      </ContentRenderer>
    </PageLayout>
  );
};
