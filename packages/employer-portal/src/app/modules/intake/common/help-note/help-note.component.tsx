/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import classNames from 'classnames';
import {
  createThemedStyles,
  textStyleToCss,
  ConfigurableText,
  Icon,
  FormattedMessage
} from 'fineos-common';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';

import styles from './help-note.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  note: {
    backgroundColor: theme.colorSchema.primaryColor,
    color: theme.colorSchema.neutral1
  },
  bigText: {
    ...textStyleToCss(theme.typography.pageTitle)
  }
}));

export const HelpNote = () => {
  const themedStyles = useThemedStyles();

  return (
    <div className={classNames(styles.note, themedStyles.note)}>
      <strong className={classNames(styles.bigText, themedStyles.bigText)}>
        <ConfigurableText id="INTAKE.HELP_NOTE.HEADER" />
      </strong>

      <p>
        <ConfigurableText id="INTAKE.HELP_NOTE.BODY" />
      </p>

      <div className={themedStyles.bigText}>
        <Icon
          icon={faPhoneAlt}
          className={classNames()}
          iconLabel={
            <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.PHONE_ICON_LABEL" />
          }
        />{' '}
        <strong>
          <ConfigurableText id="INTAKE.HELP_NOTE.PHONE" />
        </strong>
      </div>
    </div>
  );
};
