/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Panel,
  UiButton,
  createThemedStyles,
  textStyleToCss,
  FormattedMessage
} from 'fineos-common';
import classNames from 'classnames';

import styles from './intake-submission.module.scss';

type IntakeSubmissionProps = {
  title: React.ReactNode;
  showSuccessTick?: boolean;
};

const useThemedStyles = createThemedStyles(theme => ({
  title: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.displayTitle)
  },
  tick: {
    backgroundImage: `url(${theme?.images?.greenCheckIntake})`
  }
}));

export const IntakeSubmission: React.FC<IntakeSubmissionProps> = ({
  title,
  showSuccessTick = false,
  children
}) => {
  const themedStyles = useThemedStyles();

  return (
    <Panel className={styles.wrapper}>
      {showSuccessTick && (
        <div
          className={classNames(styles.tick, themedStyles.tick)}
          data-test-el="intake-success-tick"
        />
      )}

      <div className={classNames(styles.title, themedStyles.title)}>
        {title}
      </div>

      {children}

      <div className={styles.footer}>
        <FormattedMessage
          as={UiButton}
          to="/"
          className={styles.button}
          id="FINEOS_COMMON.GENERAL.CLOSE"
        />
      </div>
    </Panel>
  );
};
