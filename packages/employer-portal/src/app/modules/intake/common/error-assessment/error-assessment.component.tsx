/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  ConfigurableText,
  createThemedStyles,
  lightenColor,
  textStyleToCss,
  Icon,
  FormattedMessage
} from 'fineos-common';
import { Row, Col } from 'antd';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import styles from './error-assessment.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  wrapper: {
    backgroundColor: lightenColor(theme.colorSchema, 'pendingColor')
  },
  phoneWrapper: {
    ...textStyleToCss(theme.typography.pageTitle)
  }
}));

export const IntakeErrorAssessment: React.FC = () => {
  const themedStyles = useThemedStyles();

  return (
    <Row className={classNames(themedStyles.wrapper, styles.wrapper)}>
      <Col>
        <div>
          <ConfigurableText id="INTAKE.ERROR.BODY" />
        </div>
        <div
          className={classNames(themedStyles.phoneWrapper, styles.phoneWrapper)}
        >
          <Icon
            icon={faPhoneAlt}
            className={styles.phoneIcon}
            iconLabel={
              <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.PHONE_ICON" />
            }
          />
          <ConfigurableText id="INTAKE.ERROR.PHONE" as="strong" />
        </div>
        <div>
          <ConfigurableText id="INTAKE.ERROR.TIME" />
        </div>
      </Col>
    </Row>
  );
};
