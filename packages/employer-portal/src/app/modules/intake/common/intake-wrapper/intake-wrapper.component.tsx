/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, {
  useEffect,
  useRef,
  useState,
  useMemo,
  useCallback
} from 'react';
import { PageContentHeader, Steps, FormattedMessage } from 'fineos-common';

import { PageLayout, useLock, BlockNavigation } from '../../../../shared';
import { HelpNote } from '../help-note';
import { IntakeContext } from '../intake-context';
import { StepFormValuesExtractor } from '../intake-context/intake-context.type';
import { ProceedModal } from '../proceed-modal';

import { intakeSteps } from './intake-steps';

type IntakeWrapperProps = {
  children: React.ReactElement<{
    onFinish: (val: unknown) => void;
  }>[];
};

const isPromise = (maybePromise: unknown): maybePromise is Promise<void> =>
  maybePromise !== null &&
  typeof maybePromise === 'object' &&
  typeof (maybePromise as Promise<void>).then === 'function';

/**
 * This component helps to handle intake flow by encapsulating navigation logic
 */
export const IntakeWrapper: React.FC<IntakeWrapperProps> = ({ children }) => {
  const isIntakeStarted = useRef(false);
  const stepFormValuesExtractorRef = useRef<StepFormValuesExtractor>(
    () => null
  );
  const [currentStep, setCurrentStep] = useState(0);
  const [isProceeding, setIsProceeding] = useState(false);
  const [maxAvailableStep, setMaxAvailableStep] = useState(0);
  const navigationLock = useLock();
  const internalNavigationLock = useLock();
  const [blockedTransaction, setBlockedTransaction] = useState<
    null | ((shouldPersist: boolean) => unknown)
  >(null);

  const isFinalStep = currentStep === intakeSteps.length - 1;

  if (!isIntakeStarted.current && currentStep > 0) {
    isIntakeStarted.current = true;
  }

  useEffect(() => {
    if (currentStep > maxAvailableStep) {
      setMaxAvailableStep(currentStep);
    }
    window.scrollTo(0, 0);
  }, [currentStep, maxAvailableStep]);

  const goToPreviousStep = useCallback(() => {
    setCurrentStep(step => step - 1);
  }, [setCurrentStep]);

  const goToNextStep = useCallback(() => {
    setCurrentStep(step => step + 1);
  }, [setCurrentStep]);
  const isFirstStep = currentStep === 0;

  const currentChild = children[currentStep];
  const intakeContext = useMemo(
    () => ({
      navigationLock,
      internalNavigationLock,
      onBack: () => {
        if (navigationLock.isLocked) {
          setBlockedTransaction(() => goToPreviousStep);
        } else {
          goToPreviousStep();
        }
      },
      isFirstStep,
      registerStepFormValuesExtractor(
        stepFormValuesExtractor: StepFormValuesExtractor
      ) {
        stepFormValuesExtractorRef.current = stepFormValuesExtractor;
      },
      isIntakeStarted: isIntakeStarted.current
    }),
    [
      navigationLock,
      internalNavigationLock,
      setBlockedTransaction,
      goToPreviousStep,
      isFirstStep
    ]
  );

  if (currentChild) {
    const originalOnFinish = currentChild.props.onFinish;
    const enhancedPanel = React.cloneElement(currentChild, {
      onFinish: (val: unknown) => {
        if (originalOnFinish) {
          originalOnFinish(val);
          goToNextStep();
        }
      }
    });

    return (
      <IntakeContext.Provider value={intakeContext}>
        <BlockNavigation
          when={isIntakeStarted.current || navigationLock.isLocked}
          message={<FormattedMessage id="INTAKE.ABANDON_INTAKE_MODAL" />}
        />

        <PageLayout
          showBackgroundImage={false}
          contentHeader={
            <PageContentHeader>
              <Steps
                steps={intakeSteps}
                current={currentStep}
                maxAvailableStep={maxAvailableStep}
                isNotAccessible={true}
                isStepsDisabled={internalNavigationLock.isLocked}
                onChange={(step: number) => {
                  if (navigationLock.isLocked) {
                    setBlockedTransaction(() => (shouldPersist: boolean) => {
                      if (
                        navigationLock.releaseLockFn &&
                        !isFinalStep &&
                        shouldPersist
                      ) {
                        return navigationLock
                          .releaseLockFn()
                          .then(() => setCurrentStep(step));
                      } else {
                        setCurrentStep(step);
                      }
                    });
                  } else {
                    setCurrentStep(step);
                  }
                }}
              />
            </PageContentHeader>
          }
          sideBar={<HelpNote />}
        >
          {enhancedPanel}

          <ProceedModal
            isVisible={blockedTransaction !== null}
            isProceeding={isProceeding}
            onProceed={shouldPersist => {
              if (shouldPersist) {
                const stepFormValues = stepFormValuesExtractorRef.current();

                originalOnFinish(stepFormValues);
              }

              const result = blockedTransaction!(shouldPersist);

              if (isPromise(result)) {
                setIsProceeding(true);

                result.then(() => {
                  setIsProceeding(false);
                  setBlockedTransaction(null);
                });
              } else {
                setBlockedTransaction(null);
              }
            }}
            onCancel={() => setBlockedTransaction(null)}
          />
        </PageLayout>
      </IntakeContext.Provider>
    );
  }

  return null;
};
