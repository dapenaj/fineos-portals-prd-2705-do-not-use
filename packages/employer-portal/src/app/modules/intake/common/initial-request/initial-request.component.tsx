/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Col, Radio, Row } from 'antd';
import classNames from 'classnames';
import {
  AsteriskInfo,
  ContentRenderer,
  FormattedMessage,
  Form,
  FormGroup,
  FormRadioInput,
  DatePickerInput,
  TextArea,
  FormButton,
  ButtonType,
  createThemedStyles,
  textStyleToCss
} from 'fineos-common';
import { Field, Formik } from 'formik';
import { Moment } from 'moment';

import { useNotificationReasons } from '../../notification-reasons.hook';
import {
  initialRequestFormValidation,
  initialRequestFormValue,
  InitialRequestFormValue,
  IntakePanel,
  IntakePanelFooter
} from '..';
import {
  EnumDomain,
  getEnumDomainTranslation
} from '../../../../shared/enum-domain';

import styles from './initial-request.module.scss';

const useThemedStyles = createThemedStyles(theme => ({
  header: {
    color: theme.colorSchema.primaryColor,
    ...textStyleToCss(theme.typography.panelTitle)
  },
  text: {
    color: theme.colorSchema.neutral8,
    ...textStyleToCss(theme.typography.baseText)
  }
}));

type InitialRequestProps = {
  customInitialValues: InitialRequestFormValue | null;
  // this is here rather for right interface
  onFinish: (data: InitialRequestFormValue) => void;
  onSubmit?: (data: InitialRequestFormValue) => void;
  jobStartDate: Moment | null;
};

export const InitialRequest: React.FC<InitialRequestProps> = ({
  customInitialValues,
  onSubmit,
  jobStartDate
}) => {
  const { isLoadingReasons, notificationReasons } = useNotificationReasons();
  const handleSubmit = (data: InitialRequestFormValue) => {
    if (onSubmit) {
      onSubmit(data);
    }
  };

  const themedStyles = useThemedStyles();
  return (
    <IntakePanel
      header={
        <FormattedMessage
          id="INTAKE.INITIAL_REQUEST.HEADER"
          as="h1"
          className={classNames(themedStyles.header, styles.header)}
        />
      }
    >
      <ContentRenderer isLoading={isLoadingReasons} isEmpty={false}>
        <AsteriskInfo className={styles.asterisk} />
        <Formik
          initialValues={customInitialValues || initialRequestFormValue}
          enableReinitialize={true}
          validationSchema={initialRequestFormValidation(jobStartDate)}
          onSubmit={handleSubmit}
          validateOnMount={true}
        >
          <Form>
            <Row data-test-el="intake-initial-request">
              <Col>
                <FormGroup
                  isRequired={true}
                  label={
                    <FormattedMessage id="INTAKE.INITIAL_REQUEST.REASON" />
                  }
                  data-test-el="notification-reason-field"
                >
                  <Field
                    component={FormRadioInput}
                    name="notificationReason"
                    isVertical={true}
                  >
                    {notificationReasons.map(reason => {
                      return (
                        <Radio
                          className={styles.radio}
                          value={reason}
                          key={reason.fullId}
                        >
                          <FormattedMessage
                            className={themedStyles.text}
                            id={getEnumDomainTranslation(
                              EnumDomain.NOTIFICATION_REASON,
                              reason
                            )}
                            defaultMessage={reason.name}
                          />
                        </Radio>
                      );
                    })}
                  </Field>
                </FormGroup>
              </Col>
              <Col>
                <FormGroup
                  label={
                    <FormattedMessage id="INTAKE.INITIAL_REQUEST.NOTIFICATION_LAST_WORKING_DAY" />
                  }
                  isRequired={true}
                  data-test-el="notification-last-working-day-field"
                >
                  <Field
                    name="notificationLastWorkingDay"
                    component={DatePickerInput}
                    disabledDate={(currentDate: Moment | null) =>
                      jobStartDate?.isValid() && currentDate?.isValid()
                        ? currentDate.isBefore(jobStartDate)
                        : false
                    }
                  />
                </FormGroup>
              </Col>
              <Col>
                <FormGroup
                  label={
                    <FormattedMessage id="INTAKE.INITIAL_REQUEST.NOTIFICATION_DESCRIPTION" />
                  }
                  data-test-el="notification-description-field"
                >
                  <Field name="notificationDescription" component={TextArea} />
                </FormGroup>
              </Col>
            </Row>

            <IntakePanelFooter
              nextButton={
                <FormattedMessage
                  as={FormButton}
                  id="INTAKE.FINISH"
                  htmlType="submit"
                  buttonType={ButtonType.DANGER}
                />
              }
            />
          </Form>
        </Formik>
      </ContentRenderer>
    </IntakePanel>
  );
};
