/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { BaseDomain } from 'fineos-js-api-client';
import * as Yup from 'yup';
import moment, { Moment } from 'moment';

export type InitialRequestFormValue = {
  notificationReason: BaseDomain | null;
  notificationLastWorkingDay: string | null;
  notificationDescription?: string;
};

export const initialRequestFormValue: InitialRequestFormValue = {
  notificationReason: null,
  notificationLastWorkingDay: null,
  notificationDescription: ''
};

export const initialRequestFormValidation = (jobStartDate: Moment | null) =>
  Yup.object({
    notificationReason: Yup.object()
      .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      .nullable(),
    notificationLastWorkingDay: Yup.string()
      .test(
        'is-before',
        'INTAKE.INITIAL_REQUEST.VALIDATION.LAST_WORKING_DAY',
        (currentDate: string) =>
          jobStartDate?.isValid() && moment(currentDate)?.isValid()
            ? !moment(currentDate).isBefore(jobStartDate)
            : true
      )
      .required('FINEOS_COMMON.VALIDATION.REQUIRED')
      .nullable()
  });
