/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useCallback, useContext, useEffect } from 'react';
import classNames from 'classnames';
import { Row, Col } from 'antd';
import {
  Button,
  ButtonType,
  FormattedMessage,
  Link,
  Icon
} from 'fineos-common';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { useFormikContext } from 'formik';
import { isEmpty as _isEmpty } from 'lodash';

import { IntakeContext } from '../intake-context';

import styles from './intake-panel-footer.module.scss';

type IntakePanelFooterProps = {
  nextButton: React.ReactElement<React.ComponentProps<typeof Button>>;
};

export const IntakePanelFooter: React.FC<IntakePanelFooterProps> = ({
  nextButton
}) => {
  const intakeContext = useContext(IntakeContext);

  // formikContext might not be defined, which is ok
  const formikContext = useFormikContext();

  const touchedNames = Object.keys(formikContext?.touched || {});
  const errorsNames = Object.keys(formikContext?.errors || {});
  // internal navigation blocked only if touched fields are invalid
  const isInternalNavigationDisabled = touchedNames.some(touchedName =>
    errorsNames.includes(touchedName)
  );

  const submitForm = useCallback(
    () => formikContext && formikContext.submitForm(),
    // eslint-disable-next-line
    [formikContext?.submitForm]
  );

  const hasToLockNavigation =
    formikContext?.dirty || !_isEmpty(formikContext?.touched);

  useEffect(() => {
    if (intakeContext) {
      intakeContext.registerStepFormValuesExtractor(
        () => formikContext?.values || null
      );
    }
    // eslint-disable-next-line
  }, [intakeContext, formikContext?.values]);

  useEffect(() => {
    if (intakeContext) {
      if (isInternalNavigationDisabled) {
        intakeContext.internalNavigationLock.lock();
      } else {
        intakeContext.internalNavigationLock.unlock();
      }
    }
    // eslint-disable-next-line
  }, [intakeContext, isInternalNavigationDisabled]);

  useEffect(() => {
    if (intakeContext) {
      if (hasToLockNavigation) {
        intakeContext.navigationLock.lock(submitForm);
      } else {
        intakeContext.navigationLock.unlock();
      }
    }
    // eslint-disable-next-line
  }, [intakeContext, formikContext?.dirty]);

  return (
    <Row gutter={32} className={styles.footer}>
      <Col>{nextButton}</Col>

      <Col
        className={classNames(styles.secondaryButtons, {
          [styles.noBackButton]: intakeContext?.isFirstStep
        })}
      >
        {!intakeContext?.isFirstStep && (
          <FormattedMessage id="NAVIGATION.BACK">
            {(extraProps, text) => (
              <Button
                onClick={intakeContext?.onBack}
                buttonType={ButtonType.LINK}
                {...extraProps}
              >
                <Icon
                  icon={faArrowLeft}
                  className={styles.backButtonIcon}
                  iconLabel={
                    <FormattedMessage id="FINEOS_COMMON.ICON_LABEL.BACK_ICON_LABEL" />
                  }
                />
                {text}
              </Button>
            )}
          </FormattedMessage>
        )}

        <FormattedMessage as={Link} to="/" id="INTAKE.ABANDON_INTAKE" />
      </Col>
    </Row>
  );
};
