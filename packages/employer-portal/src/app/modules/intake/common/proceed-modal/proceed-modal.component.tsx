/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Button,
  ButtonType,
  FormattedMessage,
  Modal,
  ModalFooter
} from 'fineos-common';

import styles from './proceed-modal.module.scss';

type ProceedModalProps = {
  isVisible: boolean;
  isProceeding: boolean;
  onProceed: (shouldPersist: boolean) => void;
  onCancel: () => void;
};

export const ProceedModal: React.FC<ProceedModalProps> = ({
  isVisible,
  isProceeding,
  onProceed,
  onCancel
}) => (
  <Modal
    ariaLabelId="INTAKE.PROCEED_MODAL.HEADER"
    onCancel={onCancel}
    header={<FormattedMessage id="INTAKE.PROCEED_MODAL.HEADER" />}
    visible={isVisible}
  >
    <FormattedMessage
      id="INTAKE.PROCEED_MODAL.BODY"
      as="div"
      className={styles.body}
    />

    <ModalFooter>
      <FormattedMessage
        as={Button}
        loading={isProceeding}
        id="INTAKE.PROCEED_MODAL.YES"
        onClick={() => {
          onProceed(true);
        }}
      />
      <FormattedMessage
        as={Button}
        buttonType={ButtonType.LINK}
        id="INTAKE.PROCEED_MODAL.NO"
        onClick={() => {
          onProceed(false);
        }}
      />
    </ModalFooter>
  </Modal>
);
