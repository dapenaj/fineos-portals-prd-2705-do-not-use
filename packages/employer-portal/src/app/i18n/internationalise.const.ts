/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
export const LANGUAGE_EXTENSION_FOR_MOMENT: { [key: string]: string } = {
  en: 'en-gb',
  gom: 'gom-latn',
  hy: 'hy-am',
  oc: 'oc-lnc',
  pa: 'pa-in',
  tl: 'tl-ph',
  zh: 'zh-cn'
};

export const LANGUAGE_EXTENSION_FOR_ANT: { [key: string]: string } = {
  ar: 'ar_EG',
  az: 'az_AZ',
  bg: 'bg_BG',
  by: 'by_BY',
  ca: 'ca_ES',
  cs: 'cs_CZ',
  da: 'da_DK',
  de: 'de_DE',
  el: 'el_GR',
  en: 'en_US',
  es: 'es_ES',
  et: 'et_EE',
  fa: 'fa_IR',
  fi: 'fi_FI',
  fr: 'fr_FR',
  ga: 'ga_IE',
  gl: 'gl_ES',
  he: 'he_IL',
  hi: 'hi_IN',
  hr: 'hr_HR',
  hu: 'hu_HU',
  hy: 'hy_AM',
  id: 'id_ID',
  is: 'is_IS',
  it: 'it_IT',
  ja: 'ja_JP',
  kmr: 'kmr_IQ',
  kn: 'kn_IN',
  ko: 'ko_KR',
  ku: 'ku_IQ',
  lt: 'lt_LT',
  lv: 'lv_LV',
  mk: 'mk_MK',
  mn: 'mn_MN',
  ms: 'ms_MY',
  nb: 'nb_NO',
  ne: 'ne_NP',
  nl: 'nl_NL',
  pl: 'pl_PL',
  pt: 'pt_PT',
  ro: 'ro_RO',
  ru: 'ru_RU',
  sk: 'sk_SK',
  sl: 'sl_SI',
  sr: 'sr_RS',
  sc: 'sc_SE',
  ta: 'ta_IN',
  th: 'th_TH',
  tr: 'tr_TR',
  uk: 'uk_UA',
  vi: 'vi_VN',
  zh: 'zh_CN'
};
