/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useCallback, useEffect, useState } from 'react';
import { flattenMessages, Formatting } from 'fineos-common';
import { IntlProvider } from 'react-intl';
import { ConfigProvider } from 'antd';
import { Locale } from 'antd/lib/locale-provider';
import moment from 'moment';
import { ApiConfigProps } from 'fineos-js-api-client';

import { PortalEnvConfigService } from '../shared';
import { LanguageContext } from '../shared/layouts/language/language.context';
import {
  LANGUAGE_EXTENSION_FOR_MOMENT,
  LANGUAGE_EXTENSION_FOR_ANT
} from './internationalise.const';

type InternationaliseProps = {
  supportedLanguage: string;
  decimalSeparator: string;
};

export const Internationalise: React.FC<InternationaliseProps> = React.memo(
  ({ children, supportedLanguage, decimalSeparator }) => {
    const [messages, setMessages] = useState<Record<string, string>>();
    const [language, setLanguage] = useState<string>(supportedLanguage);
    const [decimalSeparatorTemp, setDecimalSeparatorTemp] = useState<string>(
      decimalSeparator
    );
    const [languageForAnt, setLanguageForAnt] = useState<Locale>();
    const [dateTime, setDateTime] = useState<string>();

    const updateLanguage = useCallback(async (newLanguage: string) => {
      const portalEnvConfigService = PortalEnvConfigService.getInstance();
      async function getLocales(languageExtensionForAnt: {
        [key: string]: string;
      }) {
        const {
          decimalSeparator: decimalSeparatorConfig,
          dates: { displayDateTimeFormat }
        } = await portalEnvConfigService.fetchPortalEnvConfig();
        setDecimalSeparatorTemp(decimalSeparatorConfig[newLanguage]);
        setDateTime(displayDateTimeFormat[newLanguage]);
        const tempLocales = await portalEnvConfigService.fetchLocaleData(
          newLanguage
        );
        const localeResultTemp = await import(
          `antd/lib/locale/${languageExtensionForAnt[newLanguage]}.js`
        );
        const languageForMoment = LANGUAGE_EXTENSION_FOR_MOMENT[newLanguage]
          ? LANGUAGE_EXTENSION_FOR_MOMENT[newLanguage]
          : newLanguage;
        await import(`moment/locale/${languageForMoment}`);
        moment.locale(languageForMoment);
        window.document.documentElement.lang = newLanguage;
        setLanguageForAnt(localeResultTemp.default);
        setMessages({
          ...tempLocales
        });
      }
      await getLocales(LANGUAGE_EXTENSION_FOR_ANT);
    }, []);

    useEffect(() => {
      updateLanguage(supportedLanguage);
    }, [supportedLanguage, updateLanguage]);

    return !!messages &&
      !!languageForAnt &&
      languageForAnt.locale === language ? (
      <LanguageContext.Provider
        value={{
          language,
          setLanguage: async (language: string) => {
            await updateLanguage(language);
            setLanguage(language);
          },
          decimalSeparator: decimalSeparatorTemp
        }}
      >
        <ConfigProvider locale={languageForAnt}>
          <IntlProvider locale={language} messages={flattenMessages(messages)}>
            <Formatting.Provider
              value={{
                date: ApiConfigProps.displayDateFormat,
                dateTime: dateTime || ApiConfigProps.displayDateTimeFormat,
                currency: ApiConfigProps.currencyCode
              }}
            >
              {children}
            </Formatting.Provider>
          </IntlProvider>
        </ConfigProvider>
      </LanguageContext.Provider>
    ) : null;
  }
);
