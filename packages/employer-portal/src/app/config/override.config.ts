/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  AuthenticationStrategy,
  SdkConfig,
  ApiConfig
} from 'fineos-js-api-client';

export const overrideApiConfigsWithMocks = () => {
  Object.assign(ApiConfig.getInstance().config, {
    enableTwilio: false,
    dates: {
      dateOnlyFormat: 'YYYY-MM-DD',
      displayDateFormat: 'MMM DD, YYYY',
      dateTimeFormat: 'YYYY-MM-DDTHH:mm:ss',
      displayDateTimeFormat: 'MMM DD, YYYY - h:mm A'
    },
    currencyCode: 'USD',
    decimalSeparator: '.'
  });
};

export const overrideSdkConfigsWithMocks = () => {
  Object.assign(SdkConfig.getInstance().config, {
    authenticationStrategy: AuthenticationStrategy.NONE,
    authorizationRoles: {
      portalUser: 'erportal-erPortal1-Claims-customer-portal-Employer_Role'
    },
    apiUrls: {
      root: `http://${window.location.hostname}:3001/api/v1`
    },
    employerViewpointUrl: 'http://www.google.com',
    debug: {
      userId: 'demof110',
      displayUserId: 'demof110@fineos.com',
      displayRoles: ['erportal-erPortal1-Claims-customer-portal-Employer_Role']
    },
    login: {
      url: '/'
    },
    logout: {
      url: `/auth/logout`
    }
  });
};

export const overrideSdkAndApiConfigs = () => {
  if (process.env.REACT_APP_PORTAL_COOKIE) {
    document.cookie = process.env.REACT_APP_PORTAL_COOKIE;
  }

  if (process.env.REACT_APP_PORTAL_ENV_JSON) {
    Object.assign(
      ApiConfig.getInstance().config,
      JSON.parse(process.env.REACT_APP_PORTAL_ENV_JSON)
    );
  } else {
    overrideApiConfigsWithMocks();
  }

  if (process.env.REACT_APP_ENV_JSON) {
    Object.assign(
      SdkConfig.getInstance().config,
      JSON.parse(process.env.REACT_APP_ENV_JSON)
    );
  } else {
    overrideSdkConfigsWithMocks();
  }
};
