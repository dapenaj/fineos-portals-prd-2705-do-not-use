/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { getFilteredAbsencePeriodDecisions } from '../../../../app/shared';

import {
  ABSENCE_FOR_TIME_TAKEN_FIXTURE,
  ABSENCE_PERIOD_DECISIONS_FIXTURE
} from '../../../fixtures/absence-periods.fixture';

describe('AbsencePeriodDecisionsUtil', () => {
  it('should filter out period decisions with the unwanted leavePlanType', async () => {
    const periodDecision =
      ABSENCE_PERIOD_DECISIONS_FIXTURE[0].absencePeriodDecisions;

    expect(
      getFilteredAbsencePeriodDecisions(periodDecision).decisions
    ).toHaveLength(periodDecision.decisions.length - 1);

    expect(
      getFilteredAbsencePeriodDecisions(ABSENCE_FOR_TIME_TAKEN_FIXTURE)
        .decisions
    ).toHaveLength(ABSENCE_FOR_TIME_TAKEN_FIXTURE.decisions.length - 1);
  });
});
