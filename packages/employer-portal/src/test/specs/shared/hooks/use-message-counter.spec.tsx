/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';
import { act, renderHook } from '@testing-library/react-hooks';
import { CaseMessage } from 'fineos-js-api-client';
import { SharedState, MessagesPermissions } from 'fineos-common';
import { wait } from '@testing-library/react';

import { MESSAGES_FIXTURE } from '../../../fixtures/messages.fixture';
import { ContextWrapper } from '../../../common/utils';
import { useMessageCounter } from '../../../../app/shared';
import * as messageApi from '../../../../app/shared/api/message.api';

jest.mock('../../../../app/shared/api/message.api', () => ({
  fetchUnreadMessagesCount: jest.fn()
}));

const fetchUnreadMessagesCountMock = messageApi.fetchUnreadMessagesCount as jest.Mock;

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

describe('useMessageCounter', () => {
  const renderHookWrappedWithSharedState = (
    fun: () => {
      unreadMessagesCount: number;
      syncMessages: (messages: CaseMessage[]) => void;
    }
  ) =>
    renderHook(() => fun(), {
      wrapper: ({ children }) => (
        <SharedState>
          <ContextWrapper permissions={messagesPermissions}>
            {children}
          </ContextWrapper>
        </SharedState>
      )
    });

  const mockResponse = MESSAGES_FIXTURE.filter(
    ({ readByGroupClient }) => readByGroupClient
  ).length;

  test('should call fetchUnreadMessagesCount', async () => {
    fetchUnreadMessagesCountMock.mockReturnValueOnce(
      Promise.resolve(mockResponse)
    );

    await act(async () => {
      renderHookWrappedWithSharedState(useMessageCounter);
    });

    expect(fetchUnreadMessagesCountMock).toHaveBeenCalledTimes(1);
  });

  test('should return total unreadMessagesCount if API call was successfull', async () => {
    fetchUnreadMessagesCountMock.mockReturnValueOnce(
      Promise.resolve(mockResponse)
    );

    const data = renderHookWrappedWithSharedState(fetchUnreadMessagesCountMock);
    const result = await data.result.current;

    expect(result).toStrictEqual(mockResponse);
  });

  test('should increment unreadMessagesCount by one', async () => {
    fetchUnreadMessagesCountMock.mockReturnValue(Promise.resolve(mockResponse));

    const hook = renderHookWrappedWithSharedState(useMessageCounter);
    await act(wait);
    const { syncMessages } = hook.result.current;

    const updatedMessages = [
      { ...MESSAGES_FIXTURE[0], readByGroupClient: false },
      { ...MESSAGES_FIXTURE[1], readByGroupClient: false },
      { ...MESSAGES_FIXTURE[2], readByGroupClient: false },
      { ...MESSAGES_FIXTURE[3], readByGroupClient: false }
    ];
    const newUnreadMessagesCount = 4;

    await act(() => {
      syncMessages(MESSAGES_FIXTURE);
      return wait();
    });
    await act(() => {
      syncMessages(updatedMessages);
      return wait();
    });

    const { unreadMessagesCount } = hook.result.current;
    expect(unreadMessagesCount).toStrictEqual(newUnreadMessagesCount);
  });

  test('should decrement unreadMessagesCount by one', async () => {
    fetchUnreadMessagesCountMock.mockReturnValue(Promise.resolve(mockResponse));

    const hook = renderHookWrappedWithSharedState(useMessageCounter);
    await act(wait);
    const { syncMessages } = hook.result.current;

    const updatedMessages = [
      { ...MESSAGES_FIXTURE[0], readByGroupClient: true },
      { ...MESSAGES_FIXTURE[1], readByGroupClient: true },
      { ...MESSAGES_FIXTURE[2], readByGroupClient: true },
      { ...MESSAGES_FIXTURE[3], readByGroupClient: false }
    ];
    const newUnreadMessagesCount = 1;

    await act(() => {
      syncMessages(MESSAGES_FIXTURE);
      return wait();
    });
    await act(() => {
      syncMessages(updatedMessages);
      return wait();
    });

    const { unreadMessagesCount } = hook.result.current;
    expect(unreadMessagesCount).toStrictEqual(newUnreadMessagesCount);
  });

  test('should not change unreadMessagesCount', async () => {
    fetchUnreadMessagesCountMock.mockReturnValue(Promise.resolve(mockResponse));

    const hook = renderHookWrappedWithSharedState(useMessageCounter);
    await act(wait);
    const { syncMessages } = hook.result.current;

    const updatedMessages = [
      { ...MESSAGES_FIXTURE[0], readByGroupClient: true },
      { ...MESSAGES_FIXTURE[1], readByGroupClient: false },
      { ...MESSAGES_FIXTURE[2], readByGroupClient: true },
      { ...MESSAGES_FIXTURE[3], readByGroupClient: false }
    ];
    const newUnreadMessagesCount = 2;

    await act(() => {
      syncMessages(MESSAGES_FIXTURE);
      return wait();
    });
    await act(() => {
      syncMessages(updatedMessages);
      return wait();
    });

    const { unreadMessagesCount } = hook.result.current;
    expect(unreadMessagesCount).toStrictEqual(newUnreadMessagesCount);
  });
});
