/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';
import { act, renderHook } from '@testing-library/react-hooks';
import { BaseDomain } from 'fineos-js-api-client';
import { ApiError, SharedState } from 'fineos-common';

import {
  useEnumDomain,
  EnumDomain,
  resetEnumDomainCache
} from '../../../../app/shared/enum-domain';
import * as enumInstanceApi from '../../../../app/shared/enum-domain/enum-instance.api';

jest.mock('../../../../app/shared/enum-domain/enum-instance.api', () => ({
  fetchEnumInstances: jest.fn()
}));

const fetchEnumInstancesMock = enumInstanceApi.fetchEnumInstances as jest.Mock;

describe('useEnumDomain', () => {
  const renderHookWrappedWithSharedState = (
    fun: (
      param: EnumDomain
    ) => {
      domains: BaseDomain[];
      isLoadingDomains: boolean;
      loadingDomainsError: Readonly<Error | ApiError> | null;
    },
    param: EnumDomain
  ) =>
    renderHook(() => fun(param), {
      wrapper: ({ children }) => <SharedState>{children}</SharedState>
    });

  const mockEnumInstances = [
    {
      name: 'Administration',
      domainId: 145,
      fullId: 145,
      domainName: 'domainName1'
    }
  ] as BaseDomain[];

  beforeEach(resetEnumDomainCache);
  afterEach(resetEnumDomainCache);

  test('should call fetchEnumInstances with given domainId', async () => {
    fetchEnumInstancesMock.mockReturnValueOnce(
      Promise.resolve(mockEnumInstances)
    );

    await act(async () => {
      renderHookWrappedWithSharedState(
        useEnumDomain,
        EnumDomain.OCCUPATION_CATEGORY
      );
    });

    expect(fetchEnumInstancesMock).toHaveBeenCalledWith(
      EnumDomain.OCCUPATION_CATEGORY
    );
    expect(fetchEnumInstancesMock).toHaveBeenCalledTimes(1);
  });

  test('should return enumInstances if API call was successfull', async () => {
    fetchEnumInstancesMock.mockReturnValueOnce(
      Promise.resolve(mockEnumInstances)
    );
    const expectedResult = {
      domains: mockEnumInstances,
      isLoadingDomains: false,
      loadingDomainsError: null
    };
    const { result, waitForNextUpdate } = renderHookWrappedWithSharedState(
      useEnumDomain,
      EnumDomain.WORK_PATTERN_TYPE
    );

    expect(result.current.isLoadingDomains).toBe(true);

    await waitForNextUpdate();

    expect(result.current).toStrictEqual(expectedResult);
  });

  test('should return error if API call was unsuccessfull', async () => {
    const mockError = new Error('test');
    const expectedResult = {
      domains: [],
      isLoadingDomains: false,
      loadingDomainsError: mockError
    };
    fetchEnumInstancesMock.mockImplementationOnce(() =>
      Promise.reject(mockError)
    );

    const { result, waitForNextUpdate } = renderHookWrappedWithSharedState(
      useEnumDomain,
      EnumDomain.NOTIFICATION_REASON
    );

    await waitForNextUpdate();

    expect(result.current).toStrictEqual(expectedResult);
  });

  test('should not call API when result is already cached', async () => {
    fetchEnumInstancesMock.mockReturnValue(Promise.resolve(mockEnumInstances));
    const expectedResult = {
      domains: mockEnumInstances,
      isLoadingDomains: false,
      loadingDomainsError: null
    };

    const firstCall = renderHookWrappedWithSharedState(
      useEnumDomain,
      EnumDomain.EARNING_FREQUENCY
    );

    await firstCall.waitForNextUpdate();

    const timesCalled = fetchEnumInstancesMock.mock.calls.length;

    const secondCall = renderHookWrappedWithSharedState(
      useEnumDomain,
      EnumDomain.EARNING_FREQUENCY
    );

    await secondCall.waitForNextUpdate();

    expect(fetchEnumInstancesMock).toHaveBeenCalledTimes(timesCalled);
    expect(secondCall.result.current).toStrictEqual(expectedResult);
  });
});
