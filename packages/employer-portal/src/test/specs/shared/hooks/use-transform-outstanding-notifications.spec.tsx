/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { ContextWrapper } from '../../../common/utils';
import {
  useTransformOutstandingNotifications,
  MappedNotificationKey
} from '../../../../app/shared/export-excel-data/transform-outstanding-notifications';
import { NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';

describe('useTransformOutstandingNotifications', () => {
  const Comp = ({ propertiesToRender }) => {
    const dataToRender = useTransformOutstandingNotifications(
      [NOTIFICATIONS_FIXTURE[0]],
      propertiesToRender
    );

    return <span>{JSON.stringify(dataToRender)}</span>;
  };

  test('should return data for all properties', () => {
    render(
      <ContextWrapper>
        <Comp
          propertiesToRender={[
            MappedNotificationKey.NAME,
            MappedNotificationKey.JOB_TITLE,
            MappedNotificationKey.GROUP,
            MappedNotificationKey.RETURN_DATE,
            MappedNotificationKey.CASE,
            MappedNotificationKey.REASON,
            MappedNotificationKey.CREATED_DATE,
            MappedNotificationKey.NOTIFICATION_DATE,
            MappedNotificationKey.DISABILITY,
            MappedNotificationKey.ORGANISATION_UNIT,
            MappedNotificationKey.WORK_SITE
          ]}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        `${NOTIFICATIONS_FIXTURE[0].customer?.firstName} ${NOTIFICATIONS_FIXTURE[0].customer?.lastName} - ${NOTIFICATIONS_FIXTURE[0].customer.id}`,
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(NOTIFICATIONS_FIXTURE[0].customer.jobTitle, {
        exact: false
      })
    ).toBeInTheDocument();
    expect(
      screen.getByText(NOTIFICATIONS_FIXTURE[0].adminGroup, { exact: false })
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        NOTIFICATIONS_FIXTURE[0].expectedRTWDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(NOTIFICATIONS_FIXTURE[0].caseNumber, { exact: false })
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'ENUM_DOMAINS.NOTIFICATION_REASON.ACCIDENT_OR_TREATMENT_REQUIRED_FOR_AN_INJURY',
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        NOTIFICATIONS_FIXTURE[0].createdDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        NOTIFICATIONS_FIXTURE[0].notificationDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.YES', { exact: false })
    ).toBeInTheDocument();
    expect(
      screen.getByText(NOTIFICATIONS_FIXTURE[0].customer.organisationUnit, {
        exact: false
      })
    ).toBeInTheDocument();
    expect(
      screen.getByText(NOTIFICATIONS_FIXTURE[0].customer.workSite, {
        exact: false
      })
    ).toBeInTheDocument();
  });

  test('should render for not all properties', () => {
    render(
      <ContextWrapper>
        <Comp
          propertiesToRender={[
            MappedNotificationKey.NAME,
            MappedNotificationKey.JOB_TITLE
          ]}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        `${NOTIFICATIONS_FIXTURE[0].customer?.firstName} ${NOTIFICATIONS_FIXTURE[0].customer?.lastName} - ${NOTIFICATIONS_FIXTURE[0].customer.id}`,
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(NOTIFICATIONS_FIXTURE[0].customer.jobTitle, {
        exact: false
      })
    ).toBeInTheDocument();
    expect(
      screen.queryByText(NOTIFICATIONS_FIXTURE[0].adminGroup, { exact: false })
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        NOTIFICATIONS_FIXTURE[0].expectedRTWDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(NOTIFICATIONS_FIXTURE[0].caseNumber, { exact: false })
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        'ENUM_DOMAINS.NOTIFICATION_REASON.ACCIDENT_OR_TREATMENT_REQUIRED_FOR_AN_INJURY',
        { exact: false }
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        NOTIFICATIONS_FIXTURE[0].createdDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        NOTIFICATIONS_FIXTURE[0].notificationDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.GENERAL.YES', { exact: false })
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(NOTIFICATIONS_FIXTURE[0].customer.organisationUnit, {
        exact: false
      })
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(NOTIFICATIONS_FIXTURE[0].customer.workSite, {
        exact: false
      })
    ).not.toBeInTheDocument();
  });
});
