/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  GroupClientPermissions,
  ManageNotificationPermissions
} from 'fineos-common';

import { UploadDocumentLink } from '../../../../app/shared/ui/upload-document-link';
import {
  ContextWrapper,
  waitModalClosed,
  uploadFile
} from '../../../common/utils';
import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import * as uploadApiModule from '../../../../app/shared/ui/upload-document-link/upload-document-link.api';
import * as documentApiModule from '../../../../app/shared/api/document.api';
import { DOCUMENTS_FIXTURE } from '../../../fixtures/documents.fixture';
import { render } from '../../../common/custom-render';

jest.mock(
  '../../../../app/shared/ui/upload-document-link/upload-document-link.api',
  () => ({
    uploadDocument: jest.fn(),
    setDocumentAsReceived: jest.fn()
  })
);

jest.mock('../../../../app/shared/api/document.api', () => ({
  markDocumentAsRead: jest.fn()
}));

const uploadDocument = uploadApiModule.uploadDocument as jest.Mock;
const setDocumentAsReceived = uploadApiModule.setDocumentAsReceived as jest.Mock;
const markDocumentAsRead = documentApiModule.markDocumentAsRead as jest.Mock;

describe('UploadDocumentLink', () => {
  const uploadDocumentLink = (permissions: GroupClientPermissions[] = []) => (
    <ContextWrapper permissions={permissions}>
      <UploadDocumentLink
        outstandingInfo={OUTSTANDING_INFORMATION_FIXTURE[0]}
        caseId="NTN-747"
        onDocumentMarkRead={jest.fn()}
      />
    </ContextWrapper>
  );

  test('should render', () => {
    const { getByTestId } = render(uploadDocumentLink());

    expect(getByTestId('upload-document-link')).toBeInTheDocument();
  });

  test('should open and close the document upload modal', async () => {
    const { getByTestId } = render(uploadDocumentLink());

    const uploadLink = getByTestId('upload-document-link');

    expect(uploadLink).toBeInTheDocument();

    await act(async () => {
      userEvent.click(uploadLink);
      await wait();
    });

    const cancelBtn = getByTestId('cancel-document-upload-btn');

    expect(getByTestId('file-upload')).toBeInTheDocument();
    expect(cancelBtn).toBeInTheDocument();

    await act(async () => {
      userEvent.click(cancelBtn);
      await wait();
    });

    await waitModalClosed();
  });

  // @TODO: fixme
  xtest('should upload a document successfully', async () => {
    uploadDocument.mockReturnValueOnce(Promise.resolve(DOCUMENTS_FIXTURE[1]));
    setDocumentAsReceived.mockReturnValueOnce(
      Promise.resolve(OUTSTANDING_INFORMATION_FIXTURE[0])
    );
    markDocumentAsRead.mockReturnValueOnce({
      ...DOCUMENTS_FIXTURE[1],
      isRead: true
    });

    const { getByTestId, getByType, getByText } = render(
      uploadDocumentLink([
        ManageNotificationPermissions.ADD_CASES_OUTSTANDING_INFORMATION_RECEIVED,
        ManageNotificationPermissions.MARK_READ_CASE_DOCUMENT
      ])
    );

    const uploadLink = getByTestId('upload-document-link');

    expect(uploadLink).toBeInTheDocument();

    await act(async () => {
      userEvent.click(uploadLink);
      await wait();
    });

    await uploadFile(getByType('file'));

    await act(async () => {
      userEvent.click(getByText('FINEOS_COMMON.UPLOAD.UPLOAD_BUTTON'));
      // first await goes for the upload
      await wait();
      // second await goes for mark as read
      await wait();
    });

    expect(getByText('DOCUMENT.RECEIVED_SUCCESS')).toBeInTheDocument();

    await act(async () => {
      userEvent.click(getByText('FINEOS_COMMON.GENERAL.CLOSE'));
      await wait();
    });

    await waitModalClosed();
  });
});
