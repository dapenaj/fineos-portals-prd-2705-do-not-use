/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { GroupClientAddress } from 'fineos-js-api-client';

import { Address } from '../../../../../app/shared';
import { ContextWrapper } from '../../../../common/utils';

describe('Address', () => {
  const usaAddress = {
    premiseNo: '123',
    addressLine1: 'Main street',
    addressLine2: `Where it's at`,
    addressLine3: 'Two turn tables',
    addressLine4: 'And a microphone',
    addressLine5: 'Somewhere',
    addressLine6: 'CA',
    addressLine7: 'Way up high',
    postCode: 'AB123456',
    country: { name: 'USA' }
  } as GroupClientAddress;

  const nonUSAAddress = {
    premiseNo: '123',
    addressLine1: 'Main street',
    addressLine2: `Where it's at`,
    addressLine3: 'Two turn tables',
    addressLine4: 'And a microphone',
    addressLine5: 'Somewhere',
    addressLine6: 'CA',
    addressLine7: 'Way up high',
    postCode: 'AB123456',
    country: { name: 'Ireland' }
  } as GroupClientAddress;

  test('should render a USA address', () => {
    render(
      <ContextWrapper>
        <Address address={usaAddress}>test@test.com</Address>
      </ContextWrapper>
    );

    expect(screen.getByTestId('address')).toHaveTextContent(
      `Main streetWhere it's atTwo turn tablesAnd a microphone, CAAB123456ENUM_DOMAINS.COUNTRIES.USA`
    );
  });

  test('should render USA address without addressLine6 when not provided', () => {
    const address = Object.create(usaAddress);
    address.addressLine6 = '';
    render(
      <ContextWrapper>
        <Address address={address}>test@test.com</Address>
      </ContextWrapper>
    );

    expect(screen.getByTestId('address')).toHaveTextContent(
      /^Main streetWhere it's atTwo turn tablesAnd a microphoneAB123456ENUM_DOMAINS.COUNTRIES.USA$/
    );
  });

  test('should render a non USA address', () => {
    render(
      <ContextWrapper>
        <Address address={nonUSAAddress}>test@test.com</Address>
      </ContextWrapper>
    );

    expect(screen.getByTestId('address')).toHaveTextContent(
      /Main streetWhere it's atTwo turn tablesAnd a microphoneSomewhereCAWay up highAB123456/
    );
    expect(screen.getByTestId('address')).toHaveTextContent(
      /ENUM_DOMAINS.COUNTRIES.IRELAND/
    );
  });
});
