/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import * as Yup from 'yup';
import each from 'jest-each';
import { Formik } from 'formik';
import userEvent from '@testing-library/user-event';

import {
  render,
  screen,
  act,
  wait,
  fireEvent
} from '../../../../common/custom-render';
import {
  ContextWrapper,
  getAntSelectPlaceholder,
  isAntSelectOptionNotExistsByContent,
  selectAntOptionByText
} from '../../../../common/utils';
import { createFormPartialValidationTesting } from '../../../../common/assertion/validation.assertion';
import {
  AddressInput,
  initialAddressInputValue,
  addressInputValidation
} from '../../../../../app/shared/ui/address';

describe('AddressInput', () => {
  each([
    [
      'address-line1-field',
      'COMMON.ADDRESS.ADDRESS_LINE1*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    ['address-line1-field', 'COMMON.ADDRESS.ADDRESS_LINE1*', 'a', ''],
    ['address-line2-field', 'COMMON.ADDRESS.ADDRESS_LINE2', '', ''],
    ['address-line3-field', 'COMMON.ADDRESS.ADDRESS_LINE3', '', ''],
    ['address-city-field', 'COMMON.ADDRESS.CITY', '', ''],
    ['address-city-field', 'COMMON.ADDRESS.CITY', '', ''],
    [
      'address-post-code-field',
      'COMMON.ADDRESS.ZIP_CODE*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    ['address-post-code-field', 'COMMON.ADDRESS.ZIP_CODE*', 'a', ''],
    [
      'address-post-code-field',
      'COMMON.ADDRESS.ZIP_CODE*',
      '1234567890123456789012345',
      ''
    ],
    [
      'address-post-code-field',
      'COMMON.ADDRESS.ZIP_CODE*',
      '12345678901234567890123456',
      'COMMON.VALIDATION.MAX_ZIP_CODE'
    ]
  ]).test(
    'in USA address %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: {
        scope: initialAddressInputValue
      },
      validationSchema: Yup.object({
        scope: addressInputValidation
      }),
      Component: () => <AddressInput name="scope" />
    })
  );

  test('should not allow to select Unknown for country fields', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: initialAddressInputValue
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      await isAntSelectOptionNotExistsByContent(
        screen.getByLabelText('COMMON.ADDRESS.COUNTRY*'),
        'ENUM_DOMAINS.PLEASE_SELECT'
      )
    ).toBe(true);
  });

  test('should not allow to select Unknown for US states', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: initialAddressInputValue
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      getAntSelectPlaceholder(screen.getByLabelText('COMMON.ADDRESS.STATE*'))
    ).toHaveTextContent('ENUM_DOMAINS.PLEASE_SELECT');
    expect(
      await isAntSelectOptionNotExistsByContent(
        screen.getByLabelText('COMMON.ADDRESS.STATE*'),
        'ENUM_DOMAINS.PLEASE_SELECT'
      )
    ).toBe(true);
  });

  each([
    [
      'address-line1-field',
      'COMMON.ADDRESS.ADDRESS_LINE1*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    ['address-line1-field', 'COMMON.ADDRESS.ADDRESS_LINE1*', 'a', ''],
    ['address-line2-field', 'COMMON.ADDRESS.ADDRESS_LINE2', '', ''],
    ['address-line3-field', 'COMMON.ADDRESS.ADDRESS_LINE3', '', ''],
    ['address-city-field', 'COMMON.ADDRESS.CITY', '', ''],
    [
      'address-post-code-field',
      'COMMON.ADDRESS.POSTAL_CODE*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    [
      'address-post-code-field',
      'COMMON.ADDRESS.POSTAL_CODE*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    [
      'address-post-code-field',
      'COMMON.ADDRESS.POSTAL_CODE*',
      '1234567890123456789012345',
      'COMMON.VALIDATION.CANADA_POSTAL_CODE_FORMAT'
    ],
    ['address-post-code-field', 'COMMON.ADDRESS.POSTAL_CODE*', 'a1D3f4', '']
  ]).test(
    'in Canada address %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: {
        scope: {
          ...initialAddressInputValue,
          country: {
            name: 'Canada'
          }
        }
      },
      validationSchema: Yup.object({
        scope: addressInputValidation
      }),
      Component: () => <AddressInput name="scope" />
    })
  );

  test('should not allow to select Unknown for Canada provinces', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: initialAddressInputValue
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    await act(wait);

    await selectAntOptionByText(
      screen.getByLabelText('COMMON.ADDRESS.COUNTRY*'),
      'ENUM_DOMAINS.COUNTRIES.CANADA'
    );

    expect(
      getAntSelectPlaceholder(screen.getByLabelText('COMMON.ADDRESS.PROVINCE*'))
    ).toHaveTextContent('ENUM_DOMAINS.PLEASE_SELECT');
  });

  test('should render non-USA address simply as all fields', () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: {
              ...initialAddressInputValue,
              addressLine1: 'Address 1',
              country: {
                name: 'Mexico'
              }
            }
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE1*')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE2')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE3')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE4')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE5')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE6')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.ADDRESS_LINE7')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.ADDRESS.POSTAL_CODE*')
    ).toBeInTheDocument();
  });

  each([
    ['COMMON.ADDRESS.ADDRESS_LINE1*'],
    ['COMMON.ADDRESS.ADDRESS_LINE2'],
    ['COMMON.ADDRESS.ADDRESS_LINE3'],
    ['COMMON.ADDRESS.ADDRESS_LINE5'],
    ['COMMON.ADDRESS.ADDRESS_LINE7']
  ]).test('should validate input length for %s', async fieldLabel => {
    const inputValue = 'testValue'.repeat(5);
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: {
              ...initialAddressInputValue,
              country: {
                name: 'Mexico'
              }
            }
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    userEvent.type(screen.getByLabelText(fieldLabel), inputValue);
    fireEvent.blur(screen.getByLabelText(fieldLabel));

    await act(wait);

    expect(screen.getByLabelText(fieldLabel)).not.toBeValid();
    expect(
      screen.getByText('FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH')
    ).toBeInTheDocument();
  });

  test('should validate length for city', async () => {
    const inputValue = 'testValue'.repeat(5);
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: initialAddressInputValue
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    userEvent.type(screen.getByLabelText('COMMON.ADDRESS.CITY'), inputValue);
    fireEvent.blur(screen.getByLabelText('COMMON.ADDRESS.CITY'));

    await act(wait);

    expect(screen.getByLabelText('COMMON.ADDRESS.CITY')).not.toBeValid();
    expect(
      screen.getByText('FINEOS_COMMON.VALIDATION.MAX_ADDRESS_LENGTH')
    ).toBeInTheDocument();
  });
});
