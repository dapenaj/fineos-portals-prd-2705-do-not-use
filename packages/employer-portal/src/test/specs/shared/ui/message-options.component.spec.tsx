/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { MessagesPermissions, Theme } from 'fineos-common';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { MESSAGES_FIXTURE } from '../../../fixtures/messages.fixture';
import { MOCKED_THEME_FIXTURE } from '../../../fixtures/mocked-theme.fixture';
import { ContextWrapper } from '../../../common/utils';
import { MessageOptions } from '../../../../app/shared/ui/message-options';

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

describe('MessageOptions', () => {
  test('should render ellipsis icon', () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <MessageOptions
            message={MESSAGES_FIXTURE[1]}
            onMessageEdit={jest.fn()}
          />
        </Theme.Provider>
      </ContextWrapper>
    );

    expect(screen.getByTestId('mark-message-icon')).toBeInTheDocument();
  });

  test('on click should display popover with "Mark as Read" text when message is unread', () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <MessageOptions
            message={MESSAGES_FIXTURE[1]}
            onMessageEdit={jest.fn()}
          />
        </Theme.Provider>
      </ContextWrapper>
    );

    const icon = screen.getByTestId('mark-message-icon');
    userEvent.click(icon);

    expect(screen.getByTestId('mark-message-popover').innerHTML).toContain(
      'NOTIFICATIONS.ALERTS.MESSAGES.MARK_AS_READ'
    );
  });

  test('on click should display popover with "Mark as Unread" text when message is read', () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <MessageOptions
            message={MESSAGES_FIXTURE[0]}
            onMessageEdit={jest.fn()}
          />
        </Theme.Provider>
      </ContextWrapper>
    );

    const icon = screen.getByTestId('mark-message-icon');
    userEvent.click(icon);

    expect(screen.getByTestId('mark-message-popover').innerHTML).toContain(
      'NOTIFICATIONS.ALERTS.MESSAGES.MARK_AS_UNREAD'
    );
  });
});
