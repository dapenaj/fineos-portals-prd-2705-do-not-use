/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import * as Yup from 'yup';
import each from 'jest-each';

import { createFormPartialValidationTesting } from '../../../../common/assertion/validation.assertion';
import {
  initialPhoneInputValue,
  phoneInputValidation,
  PhoneInput
} from '../../../../../app/shared';

describe('PhoneInput', () => {
  each([
    ['phone-int-code-field', 'COMMON.PHONE.COUNTRY_CODE', '', ''],
    [
      'phone-int-code-field',
      'COMMON.PHONE.COUNTRY_CODE',
      'ds',
      'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
    ],
    [
      'phone-int-code-field',
      'COMMON.PHONE.COUNTRY_CODE',
      '.',
      'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
    ],
    ['phone-int-code-field', 'COMMON.PHONE.COUNTRY_CODE', '1234567890', ''],
    [
      'phone-int-code-field',
      'COMMON.PHONE.COUNTRY_CODE',
      '12345678901',
      'COMMON.VALIDATION.MAX_INTL_CODE'
    ],
    [
      'phone-area-code-field',
      'COMMON.PHONE.AREA_CODE*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    [
      'phone-area-code-field',
      'COMMON.PHONE.AREA_CODE*',
      'ds',
      'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
    ],
    [
      'phone-area-code-field',
      'COMMON.PHONE.AREA_CODE*',
      '.',
      'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
    ],
    ['phone-area-code-field', 'COMMON.PHONE.AREA_CODE*', '1234567890', ''],
    [
      'phone-area-code-field',
      'COMMON.PHONE.AREA_CODE*',
      '12345678901',
      'COMMON.VALIDATION.MAX_AREA_CODE'
    ],
    [
      'phone-phone-no-field',
      'COMMON.PHONE.PHONE_NO*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    [
      'phone-phone-no-field',
      'COMMON.PHONE.PHONE_NO*',
      'ds',
      'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
    ],
    [
      'phone-phone-no-field',
      'COMMON.PHONE.PHONE_NO*',
      '.',
      'FINEOS_COMMON.VALIDATION.NOT_NUMERIC'
    ],
    [
      'phone-phone-no-field',
      'COMMON.PHONE.PHONE_NO*',
      '12345678901234567890',
      ''
    ],
    [
      'phone-phone-no-field',
      'COMMON.PHONE.PHONE_NO*',
      '123456789012345678901',
      'COMMON.VALIDATION.MAX_TELEPHONE'
    ]
  ]).test(
    'in %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: {
        scope: initialPhoneInputValue
      },
      validationSchema: Yup.object({
        scope: phoneInputValidation
      }),
      Component: () => <PhoneInput name="scope" />
    })
  );
});
