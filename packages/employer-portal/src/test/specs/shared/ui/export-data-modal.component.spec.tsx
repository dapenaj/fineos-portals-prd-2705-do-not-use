/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, render, screen, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  GroupClientNotification,
  GroupClientNotificationService
} from 'fineos-js-api-client';
import { IntlProvider } from 'react-intl';

import { ContextWrapper, waitModalClosed } from '../../../common/utils';
import { ExportDataModal } from '../../../../app/shared/ui/export-data-modal/export-data-modal.component';
import { Config } from '../../../../app/config';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';

describe('ExportDataModal', () => {
  const mockLocales = {
    test: 'test'
  };

  beforeEach(() => {
    jest
      .spyOn(
        GroupClientNotificationService.getInstance(),
        'searchNotifications'
      )
      .mockReturnValue(new Promise(() => []));
  });
  test('should render', async () => {
    render(
      <Config.Provider value={CONFIG_FIXTURE}>
        <ContextWrapper>
          <IntlProvider locale="en" messages={mockLocales}>
            <ExportDataModal<GroupClientNotification>
              data={NOTIFICATIONS_FIXTURE as any}
              fileNameTranslationId="test"
            />
          </IntlProvider>
        </ContextWrapper>
      </Config.Provider>
    );

    await act(wait);

    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.TRIGGER_ICON_LABEL')
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText('EXPORT_OUTSTANDING_LIST.TRIGGER_ICON_LABEL')
    );

    await act(wait);

    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.MODAL_HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.FILE_FORMAT.XLSX')
    ).toBeInTheDocument();
    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.FILE_FORMAT.XLS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.FILE_FORMAT.CSV')
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.DOWNLOAD_BUTTON')
    ).toBeInTheDocument();
  });

  test('should close modal', async () => {
    render(
      <Config.Provider value={CONFIG_FIXTURE}>
        <ContextWrapper>
          <IntlProvider locale="en" messages={mockLocales}>
            <ExportDataModal<GroupClientNotification>
              data={NOTIFICATIONS_FIXTURE as any}
              fileNameTranslationId="test"
            />
          </IntlProvider>
        </ContextWrapper>
      </Config.Provider>
    );

    await act(wait);

    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.TRIGGER_ICON_LABEL')
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText('EXPORT_OUTSTANDING_LIST.TRIGGER_ICON_LABEL')
    );

    await act(wait);

    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.MODAL_HEADER')
    ).toBeInTheDocument();

    userEvent.click(screen.queryByText('FINEOS_COMMON.GENERAL.CANCEL')!);

    await waitModalClosed();
  });

  test('should disable download button when no data', async () => {
    const { rerender } = render(
      <Config.Provider value={CONFIG_FIXTURE}>
        <ContextWrapper>
          <IntlProvider locale="en" messages={mockLocales}>
            <ExportDataModal<GroupClientNotification>
              data={NOTIFICATIONS_FIXTURE as any}
              fileNameTranslationId="test"
            />
          </IntlProvider>
        </ContextWrapper>
      </Config.Provider>
    );

    await act(wait);

    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.BUTTON_TRIGGER')
    ).not.toBeDisabled();

    rerender(
      <Config.Provider value={CONFIG_FIXTURE}>
        <ContextWrapper>
          <IntlProvider locale="en" messages={mockLocales}>
            <ExportDataModal<GroupClientNotification>
              data={[]}
              fileNameTranslationId="test"
            />
          </IntlProvider>
        </ContextWrapper>
      </Config.Provider>
    );

    await act(wait);

    expect(
      screen.getByText('EXPORT_OUTSTANDING_LIST.BUTTON_TRIGGER')
    ).toBeDisabled();
  });
});
