/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, screen, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MessagesPermissions, Theme } from 'fineos-common';
import { fireEvent } from '@testing-library/dom';

import { render } from '../../../common/custom-render';
import { MOCKED_THEME_FIXTURE } from '../../../fixtures/mocked-theme.fixture';
import { MESSAGES_FIXTURE } from '../../../fixtures/messages.fixture';
import {
  ContextWrapper,
  mockTranslations,
  buildTranslationWithParams,
  resolveTranslationWithParams
} from '../../../common/utils';
import {
  FormikWithResponseHandling,
  MessageDetails
} from '../../../../app/shared/';
import { formValidation } from '../../../../app/shared/ui/message-details/message-details.form';
import * as apiModule from '../../../../app/shared/api/message.api';

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

const translations = mockTranslations(
  buildTranslationWithParams(
    'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_COUNTER',
    ['remainingCharacters']
  )
);

jest.mock('../../../../app/shared/api/message.api', () => ({
  addMessage: jest.fn()
}));
const addMessage = apiModule.addMessage as jest.Mock;

describe('MessageDetails', () => {
  test('should render message content', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails currentMessage={MESSAGES_FIXTURE[0]} />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText(MESSAGES_FIXTURE[0].narrative)).toBeInTheDocument();
  });

  test('should render editable reply textarea in case message is sent by carrier', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails currentMessage={MESSAGES_FIXTURE[0]} />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByPlaceholderText(
        'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
      )
    ).toBeInTheDocument();
  });

  test('shouldnt render editable reply textarea in case message is sent by employer', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails
              currentMessage={{
                ...MESSAGES_FIXTURE[0],
                msgOriginatesFromPortal: true
              }}
            />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.queryByPlaceholderText(
        'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
      )
    ).not.toBeInTheDocument();
  });

  test('should render counter for remaining characters in message input', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails currentMessage={MESSAGES_FIXTURE[0]} />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_COUNTER')
    ).toBeInTheDocument();
  });

  test('should change counter state when message input changes', async () => {
    render(
      <ContextWrapper
        permissions={messagesPermissions}
        translations={translations}
      >
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails currentMessage={MESSAGES_FIXTURE[0]} />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(wait);

    const maxLength = 4000;
    expect(
      screen.getByText(
        resolveTranslationWithParams(
          'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_COUNTER',
          [['remainingCharacters', maxLength]]
        )
      )
    ).toBeInTheDocument();

    const messageInput = screen.getByPlaceholderText(
      'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
    );

    const text = 'test';
    userEvent.type(messageInput, text);

    await act(wait);

    expect(messageInput).toHaveValue(text);
    expect(
      screen.getByText(
        resolveTranslationWithParams(
          'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_COUNTER',
          [['remainingCharacters', maxLength - text.length]]
        )
      )
    ).toBeInTheDocument();
  });

  test('should mark message input as invalid immediately and show max length error, when characters exceed allowed limit', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails currentMessage={MESSAGES_FIXTURE[0]} />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    const messageInput = screen.getByPlaceholderText(
      'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
    );

    await act(async () => {
      await userEvent.type(messageInput, 'test'.repeat(1001));
      return wait();
    });

    expect(messageInput).not.toBeValid();

    expect(
      screen.getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.REPLY_MESSAGE_MAX_LENGTH'
      )
    ).toBeInTheDocument();
  });

  test('should mark message input as valid when input is correctly filled in', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <FormikWithResponseHandling
            onSubmit={jest.fn()}
            initialValues={{}}
            onReset={jest.fn()}
            validationSchema={formValidation}
          >
            <MessageDetails currentMessage={MESSAGES_FIXTURE[0]} />
          </FormikWithResponseHandling>
        </Theme.Provider>
      </ContextWrapper>
    );

    const messageInput = screen.getByPlaceholderText(
      'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
    );

    await act(async () => {
      await userEvent.type(messageInput, 'test5');
      fireEvent.blur(messageInput);
      return wait();
    });

    expect(messageInput).toBeValid();
  });
});
