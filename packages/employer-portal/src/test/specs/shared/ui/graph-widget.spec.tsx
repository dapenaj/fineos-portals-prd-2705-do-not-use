/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait, within } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { ContextWrapper } from '../../../common/utils';
import { GraphWidget } from '../../../../app/shared';

const mockGraphData = [
  { label: 'First Group', value: 10, filterValues: ['test1'] },
  { label: 'Second Group', value: 7, filterValues: ['test2'] },
  { label: 'Third Group', value: 13, filterValues: ['test3'] },
  { label: 'Fourth Group', value: 4, filterValues: ['test4'] },
  { label: 'Fifth Group', value: 17, filterValues: ['test5'] },
  { label: 'Sixth Group', value: 3, filterValues: ['test6'] }
];

describe('Graph widget', () => {
  test('should render graph widget with donut, if there is any data', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={mockGraphData}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('donut')).toBeInTheDocument();
  });

  test('should render filters', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={mockGraphData}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('filter1')).toBeInTheDocument();
    expect(screen.getByText('filter2')).toBeInTheDocument();
  });

  test('should render active view details button, if there is any data', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={mockGraphData}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    const button = within(
      screen.getByTestId('graph-widget-footer')
    ).getByTestId('WIDGET.VIEW_DETAILS');

    expect(button).toBeInTheDocument();
    expect(button).toBeEnabled();
  });

  test('shouldnt render graph widget footer, if there is no data/error', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={[]}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.queryByTestId('graph-widget-footer')).not.toBeInTheDocument();
  });

  test('should render spinner if data is loading', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={mockGraphData}
          isLoading={true}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('spinner')).toBeInTheDocument();
  });

  test('should render "No data" message, if there is no data', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={[]}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('No data')).toBeInTheDocument();
  });

  test('should render something went wrong, when fetching data is not successful', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          error={new Error('error')}
          data={[]}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      await screen.findByTestId('something-went-wrong')
    ).toBeInTheDocument();
  });

  test('should call onViewDetails on slice click', async () => {
    const mockOnViewDetails = jest.fn();
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <GraphWidget
          data={mockGraphData}
          isLoading={false}
          filters={[
            <div key="filter1">filter1</div>,
            <div key="filter2">filter2</div>
          ]}
          emptyMessage={<div>No data</div>}
          onViewDetails={mockOnViewDetails}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getAllByTestId('donut-slice')[0]);

    expect(mockOnViewDetails).toHaveBeenCalledWith(
      mockGraphData[4].filterValues
    );
  });
});
