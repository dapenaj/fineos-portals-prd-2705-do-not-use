/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { EVPPermissions } from 'fineos-common';

import { PageLayout } from '../../../../app/shared/layouts';
import { render, screen } from '../../../common/custom-render';
import { ContextWrapper, mockTranslations } from '../../../common/utils';

describe('PageLayout', () => {
  const translations = mockTranslations(
    'NAVIGATION.HOME',
    'NAVIGATION.BILLING'
  );
  test('should render a home link as first in navigation', () => {
    render(
      <ContextWrapper translations={translations}>
        <PageLayout>Test</PageLayout>
      </ContextWrapper>
    );

    expect(screen.getByTestId('nav-bar').querySelector('a')!.href).toBe(
      'http://localhost/'
    );
  });

  xtest('should render a link to billing portal if user has permissions', async () => {
    // TODO - fix this test - the <PageLayout> component is causing a request to be triggered that is not handled properly.
    // As a result, when this test finishes, that request is never resolved and the resulting error causes the next test to fail.
    // It is unclear what this request is.
    render(
      <ContextWrapper
        translations={translations}
        permissions={[EVPPermissions.ACCESS_BILLING_PORTAL]}
      >
        <PageLayout>Test</PageLayout>
      </ContextWrapper>
    );

    expect(screen.getByText('NAVIGATION.BILLING')).toBeVisible();
  });

  test('should not render a link to billing portal if user doesnt have permissions', async () => {
    render(
      <ContextWrapper translations={translations}>
        <PageLayout>Test</PageLayout>
      </ContextWrapper>
    );

    expect(screen.queryByText('NAVIGATION.BILLING')).not.toBeInTheDocument();
  });
});
