/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { ManageNotificationPermissions } from 'fineos-common';

import { EmptyItem } from '../../../../../app/shared/layouts/search/item';
import { lock, unlock } from '../../../../../app/modules/intake/intake.service';
import { ContextWrapper, mockTranslations } from '../../../../common/utils';
import {
  SearchResult,
  SearchType
} from '../../../../../app/shared/layouts/search/search.type';

describe('EmptyItem', () => {
  const translations = mockTranslations(
    'HEADER.SEARCH.NO_NOTIFICATION_FOUND',
    'HEADER.SEARCH.NO_EMPLOYEE_FOUND',
    'HEADER.SEARCH.NON_ESTABLISHED_INTAKE_LINK'
  );

  test('should render an empty item', () => {
    const { getByTestId } = render(
      <ContextWrapper translations={translations}>
        <EmptyItem
          searchResult={{ type: SearchType.NOTIFICATION } as SearchResult}
          performedQuery={'test'}
        />
      </ContextWrapper>
    );

    expect(getByTestId('search-empty-item')).toBeInTheDocument();
  });

  test('should render an empty item with ManageNotificationPermissions.ADD_EFORM', () => {
    unlock();
    const { getByTestId } = render(
      <ContextWrapper
        translations={translations}
        permissions={[ManageNotificationPermissions.ADD_EFORM]}
      >
        <EmptyItem
          searchResult={{ type: SearchType.EMPLOYEE } as SearchResult}
          performedQuery={'test'}
        />
      </ContextWrapper>
    );

    expect(getByTestId('non-established-intake-link')).toBeInTheDocument();
  });

  test('should not render an empty item with ManageNotificationPermissions.ADD_EFORM during Intake', () => {
    lock();
    const { queryByTestId } = render(
      <ContextWrapper
        translations={translations}
        permissions={[ManageNotificationPermissions.ADD_EFORM]}
      >
        <EmptyItem
          searchResult={{ type: SearchType.EMPLOYEE } as SearchResult}
          performedQuery={'test'}
        />
      </ContextWrapper>
    );

    expect(
      queryByTestId('non-established-intake-link')
    ).not.toBeInTheDocument();
  });
});
