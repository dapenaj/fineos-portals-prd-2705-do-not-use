/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, fireEvent, within, screen } from '@testing-library/react';
import { wait } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import {
  ViewNotificationPermissions,
  ViewCustomerDataPermissions,
  CANCEL_KEY
} from 'fineos-common';
import { advanceBy } from 'jest-date-mock';

import {
  ContextWrapper,
  getAntSelectBySelectedText,
  getByAriaLabelledBy,
  mockTranslations,
  buildTranslationWithParams,
  releaseEventLoop,
  selectAntOptionByText,
  resolveTranslationWithParams
} from '../../../../common/utils';
import { Search } from '../../../../../app/shared/layouts/search/search.component';
import * as searchDataModule from '../../../../../app/shared/layouts/search/search.api';
import { SearchType } from '../../../../../app/shared/layouts/search/search.type';

describe('Search', () => {
  const translations = mockTranslations(
    buildTranslationWithParams('HEADER.SEARCH.NO_EMPLOYEE_FOUND', ['query']),
    buildTranslationWithParams('HEADER.SEARCH.NO_NOTIFICATION_FOUND', [
      'query'
    ]),
    buildTranslationWithParams('HEADER.SEARCH.TOO_MANY_RESULTS', [
      'query',
      'maxTotalMatches'
    ]),
    buildTranslationWithParams('HEADER.SEARCH.RESULTS_LENGTH', [
      'query',
      'resultsLength'
    ])
  );

  beforeEach(() => {
    jest.spyOn(searchDataModule, 'search');
  });

  describe('when user has no permission', () => {
    test('search should not be rendered', () => {
      render(
        <ContextWrapper>
          <Search />
        </ContextWrapper>
      );

      expect(screen.queryByTestId('search-form')).not.toBeInTheDocument();
    });
  });

  describe('when user has only ViewNotificationPermissions.VIEW_NOTIFICATIONS', () => {
    test('should render search', () => {
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      expect(screen.queryByTestId('search-form')).toBeInTheDocument();
    });

    test('should render message that search dedicated for notifications', () => {
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      expect(
        screen.queryByText('HEADER.SEARCH.NOTIFICATION_SEARCH')
      ).toBeInTheDocument();
      expect(
        screen.queryByText('HEADER.SEARCH.EMPLOYEE_SEARCH')
      ).not.toBeInTheDocument();
    });

    test('should perform notifications search', async () => {
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.NOTIFICATION,
          items: []
        })
      );
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.NOTIFICATION_SEARCH'),
        '2'
      );

      await wait(() => {
        expect(searchDataModule.search).toHaveBeenCalledWith(
          SearchType.NOTIFICATION,
          'NTN-2'
        );
      });
    });

    test('should perform notifications search with replacing just a number with prefixed one', async () => {
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.NOTIFICATION,
          items: []
        })
      );
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.NOTIFICATION_SEARCH'),
        '2'
      );

      await wait(() => {
        expect(searchDataModule.search).toHaveBeenCalledWith(
          SearchType.NOTIFICATION,
          'NTN-2'
        );
      });
    });

    test('should not perform notification search when less then 1 character typed', async () => {
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.NOTIFICATION_SEARCH'),
        ''
      );

      await wait(() => {
        expect(searchDataModule.search).not.toHaveBeenCalled();
      });
    });

    test('should handle CANCEL_KEY as reset text to NTN- prefix', () => {
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      const input = screen.getByLabelText(
        'HEADER.SEARCH.NOTIFICATION_SEARCH'
      ) as HTMLInputElement;

      userEvent.type(input, '12');

      act(() => {
        fireEvent.keyDown(input, { key: CANCEL_KEY });
      });

      expect(input.value).toBe('NTN-');
    });

    test('should show empty message if nothing return from API', async () => {
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.NOTIFICATION,
          items: []
        })
      );
      render(
        <ContextWrapper
          translations={translations}
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.NOTIFICATION_SEARCH'),
        '12'
      );

      await wait(() => {
        expect(
          screen.getByText(
            resolveTranslationWithParams(
              'HEADER.SEARCH.NO_NOTIFICATION_FOUND',
              [['query', 'NTN-12']]
            )
          )
        ).toBeInTheDocument();
      });
    });

    test('should handle 400 error from the API', async () => {
      (searchDataModule.search as jest.Mock).mockImplementationOnce(() =>
        Promise.reject({ status: 400 })
      );
      render(
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.NOTIFICATION_SEARCH'),
        'NTN-12'
      );

      await wait(() => {
        expect(
          screen.getByText('HEADER.SEARCH.REFINE_SEARCH')
        ).toBeInTheDocument();
      });
    });
  });

  describe('when user has only ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST and VIEW_CUSTOMER', () => {
    test('should render search', () => {
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      expect(screen.queryByTestId('search-form')).toBeInTheDocument();
    });

    test('should render message that search dedicated for employee', () => {
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      expect(
        screen.queryByText('HEADER.SEARCH.NOTIFICATION_SEARCH')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('HEADER.SEARCH.EMPLOYEE_SEARCH')
      ).toBeInTheDocument();
    });

    test('should perform employee search with at least 3 characters typed in', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aga'
      );

      await wait(() => {
        expect(searchDataModule.search).toHaveBeenCalledWith(
          SearchType.EMPLOYEE,
          'aga'
        );
      });
    });

    test('should perform employee search after 500 delay', async () => {
      jest.useFakeTimers();

      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aga'
      );

      act(() => {
        advanceBy(500);
        jest.advanceTimersByTime(500);
      });

      await act(async () => {
        const whenReleased = releaseEventLoop();

        jest.runAllTimers();

        await whenReleased;
      });

      expect(searchDataModule.search).toHaveBeenCalledWith(
        SearchType.EMPLOYEE,
        'aga'
      );

      jest.useRealTimers();
    });

    test('should not perform notification search when less then 1 character typed', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'ag'
      );

      await wait(() => {
        expect(searchDataModule.search).not.toHaveBeenCalled();
      });
    });

    test('should handle CANCEL_KEY as remove all from search bar', () => {
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      const input = screen.getByLabelText(
        'HEADER.SEARCH.EMPLOYEE_SEARCH'
      ) as HTMLInputElement;

      userEvent.type(input, 'aga');

      act(() => {
        fireEvent.keyDown(input, { key: CANCEL_KEY });
      });

      expect(input.value).toBe('');
    });

    test('should show empty message if nothing return from API', async () => {
      jest.useFakeTimers();
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.EMPLOYEE,
          items: []
        })
      );
      render(
        <ContextWrapper
          translations={translations}
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aga'
      );

      act(() => {
        advanceBy(500);
        jest.advanceTimersByTime(500);
      });

      await act(async () => {
        const whenReleased = releaseEventLoop();

        jest.runAllTimers();
        await whenReleased;
      });

      expect(
        screen.getByText(
          resolveTranslationWithParams('HEADER.SEARCH.NO_EMPLOYEE_FOUND', [
            ['query', 'aga']
          ])
        )
      ).toBeInTheDocument();

      jest.useRealTimers();
    });

    test('should not limit results and do not display "too many results" information', async () => {
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.EMPLOYEE,
          items: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }]
        })
      );
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aga'
      );

      await wait(() => {
        expect(
          screen.queryByText(
            resolveTranslationWithParams('HEADER.SEARCH.TOO_MANY_RESULTS', [
              ['query', 'aga'],
              ['maxTotalMatches', 5]
            ])
          )
        ).not.toBeInTheDocument();
      });
    });

    test('should display number of results', async () => {
      jest.useFakeTimers();
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.EMPLOYEE,
          items: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }]
        })
      );

      render(
        <ContextWrapper
          translations={translations}
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );
      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aba'
      );

      act(() => {
        advanceBy(500);
        jest.advanceTimersByTime(500);
      });

      await act(async () => {
        const whenReleased = releaseEventLoop();

        jest.runAllTimers();
        await whenReleased;
      });

      expect(
        screen.getByText(
          resolveTranslationWithParams('HEADER.SEARCH.RESULTS_LENGTH', [
            ['query', 'aba'],
            ['resultsLength', 5]
          ])
        )
      ).toBeInTheDocument();

      jest.useRealTimers();
    });

    test('should limit visible search results and display "too many results" information', async () => {
      jest.useFakeTimers();
      (searchDataModule.search as jest.Mock).mockReturnValueOnce(
        Promise.resolve({
          type: SearchType.EMPLOYEE,
          items: [
            { id: 1 },
            { id: 2 },
            { id: 3 },
            { id: 4 },
            { id: 5 },
            { id: 6 }
          ]
        })
      );

      render(
        <ContextWrapper
          translations={translations}
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );
      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aba'
      );

      act(() => {
        advanceBy(500);
        jest.advanceTimersByTime(500);
      });

      await act(async () => {
        const whenReleased = releaseEventLoop();

        jest.runAllTimers();
        await whenReleased;
      });

      expect(
        screen.queryByText(
          resolveTranslationWithParams('HEADER.SEARCH.TOO_MANY_RESULTS', [
            ['query', 'aba'],
            ['maxTotalMatches', 5]
          ])
        )
      ).toBeInTheDocument();

      jest.useRealTimers();
    });
  });

  describe('when user has all permissions', () => {
    test('should have employee search as default', () => {
      const { queryByText } = render(
        <ContextWrapper
          translations={translations}
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      expect(
        queryByText('HEADER.SEARCH.NOTIFICATION_SEARCH')
      ).not.toBeInTheDocument();
      expect(queryByText('HEADER.SEARCH.EMPLOYEE_SEARCH')).toBeInTheDocument();
    });

    test('should allow to switch search type', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER,
            ViewNotificationPermissions.VIEW_NOTIFICATIONS
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      await selectAntOptionByText(
        screen.getByTestId('search-type'),
        'HEADER.SEARCH.NOTIFICATION_SEARCH'
      );

      expect(
        within(screen.getByTestId('search-form')).queryAllByText(
          'HEADER.SEARCH.NOTIFICATION_SEARCH'
        )
      ).toHaveLength(3);
      expect(
        within(screen.getByTestId('search-form')).queryAllByText(
          'HEADER.SEARCH.EMPLOYEE_SEARCH'
        )
      ).toHaveLength(2);
    });

    test('should erase search query when change search type', async () => {
      const { container } = render(
        <ContextWrapper
          permissions={[
            ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
            ViewCustomerDataPermissions.VIEW_CUSTOMER,
            ViewNotificationPermissions.VIEW_NOTIFICATIONS
          ]}
        >
          <Search />
        </ContextWrapper>
      );

      userEvent.type(
        screen.getByLabelText('HEADER.SEARCH.EMPLOYEE_SEARCH'),
        'aga'
      );

      await selectAntOptionByText(
        screen.getByTestId('search-type'),
        'HEADER.SEARCH.NOTIFICATION_SEARCH'
      );

      expect(screen.getByDisplayValue('NTN-')).toBeInTheDocument();
    });
  });
});
