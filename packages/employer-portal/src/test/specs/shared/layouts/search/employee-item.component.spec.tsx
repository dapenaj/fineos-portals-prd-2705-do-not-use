/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import {
  CreateNotificationPermissions,
  ViewCustomerDataPermissions
} from 'fineos-common';

import { EmployeeItem } from '../../../../../app/shared/layouts/search/item';
import { ContextWrapper, mockTranslations } from '../../../../common/utils';
import { CUSTOMER_SUMMARY_FIXTURE } from '../../../../fixtures/customer.fixture';
import { lock, unlock } from '../../../../../app/modules/intake/intake.service';

describe('EmployeeItem', () => {
  const translations = mockTranslations(
    'HEADER.SEARCH.ESTABLISHED_INTAKE_LINK'
  );

  test('should render a search employee item', () => {
    const { getByTestId } = render(
      <ContextWrapper translations={translations}>
        <EmployeeItem
          item={CUSTOMER_SUMMARY_FIXTURE[0]}
          closePopup={() => jest.fn()}
        />
      </ContextWrapper>
    );

    expect(getByTestId('search-item')).toBeInTheDocument();
  });

  test('should not render a search employee item with CreateNotificationPermissions.ADD_NOTIFICATION during intake', () => {
    lock();
    const { queryByTestId } = render(
      <ContextWrapper
        permissions={[
          CreateNotificationPermissions.ADD_NOTIFICATION,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
        ]}
        translations={translations}
      >
        <EmployeeItem
          item={CUSTOMER_SUMMARY_FIXTURE[0]}
          closePopup={() => jest.fn()}
        />
      </ContextWrapper>
    );

    expect(queryByTestId('established-intake-link')).not.toBeInTheDocument();
  });

  test('should render a search employee item with CreateNotificationPermissions.ADD_NOTIFICATION', () => {
    unlock();
    const { getByTestId } = render(
      <ContextWrapper
        permissions={[
          CreateNotificationPermissions.ADD_NOTIFICATION,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
        ]}
        translations={translations}
      >
        <EmployeeItem
          item={CUSTOMER_SUMMARY_FIXTURE[0]}
          closePopup={() => jest.fn()}
        />
      </ContextWrapper>
    );

    expect(getByTestId('established-intake-link')).toBeInTheDocument();
  });

  test('should not render a search employee item without ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO permission', () => {
    const { queryByTestId } = render(
      <ContextWrapper
        permissions={[CreateNotificationPermissions.ADD_NOTIFICATION]}
        translations={translations}
      >
        <EmployeeItem
          item={CUSTOMER_SUMMARY_FIXTURE[0]}
          closePopup={() => jest.fn()}
        />
      </ContextWrapper>
    );

    expect(queryByTestId('established-intake-link')).not.toBeInTheDocument();
  });
});
