/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { AuthenticationStrategy, SdkConfigProps } from 'fineos-js-api-client';
import { advanceTo } from 'jest-date-mock';
import moment from 'moment';

import { AuthenticationService } from './../../../../app/shared/authentication';

describe('Authentication Service', () => {
  const service = AuthenticationService.getInstance();

  describe('for AuthenticationStrategy.NONE', () => {
    beforeEach(() => {
      jest
        .spyOn(service, 'authStrategy', 'get')
        .mockReturnValue(AuthenticationStrategy.NONE);
    });

    it('should return isAuthenticated true', () => {
      expect(service.isAuthenticated).toBe(true);
    });

    it('should set insecure strategy on the serviceFactory', () => {
      jest
        .spyOn(SdkConfigProps, 'debugUserId', 'get')
        .mockReturnValue('test-user-id');

      service.saveCredentials();

      expect(service.serviceFactory.strategy.getUserId()).toEqual(
        'test-user-id'
      );
    });
  });

  describe('for AuthenticationStrategy.COGNITO', () => {
    const setParams = () => {
      service.accessKeyId = 'test-access-key-id';
      service.secretKey = 'test-secret';
      service.sessionToken = 'test-token';
      service.userId = 'test-id+';
      service.role = 'test-role';
    };

    beforeEach(() => {
      advanceTo(new Date(2017, 6, 15, 13, 45));

      jest
        .spyOn(service, 'authStrategy', 'get')
        .mockReturnValue(AuthenticationStrategy.COGNITO);
    });

    it('should return isAuthenticated true if all params have been provided and token is valid', () => {
      setParams();
      service.expiration = moment([2017, 6, 15, 13, 46]).toISOString();
      expect(service.isAuthenticated).toBe(true);
    });

    it('should set cognito strategy on the serviceFactory using the params from the url', () => {
      document.cookie = `portal-cookie=${JSON.stringify({
        uid: 'uid',
        r: 'r',
        ak: 'ak',
        sk: 'sk',
        st: 'st',
        exp: 'exp'
      })}`;
      service.saveCredentials();

      expect(service.serviceFactory.strategy.getUserId()).toEqual('uid');
      expect(service.displayRoles[0]).toEqual('demoRole');
    });
  });
});
