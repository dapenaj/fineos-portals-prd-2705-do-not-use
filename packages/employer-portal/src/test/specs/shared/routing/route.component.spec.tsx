/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { MemoryRouter, Switch } from 'react-router';
import {
  atom,
  PermissionsScope,
  useSharedState,
  ViewCustomerDataPermissions,
  GroupClientPermissions
} from 'fineos-common';

import { render, screen, act, wait } from '../../../common/custom-render';
import { Route } from '../../../../app/shared/routing';
import { Internationalise } from '../../../../app/i18n/internationalise';
import { PortalEnvConfigService } from '../../../../app/shared/portal-env-config/portal-env-config.service';
import { getMockTranslations } from '../../../common/utils';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

describe('Route', () => {
  const service = PortalEnvConfigService.getInstance();

  beforeEach(() => {
    jest
      .spyOn(service, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
    jest
      .spyOn(service, 'fetchLocaleData')
      .mockResolvedValue(getMockTranslations());
  });
  const renderRouting = ({
    globalPermissions = [] as GroupClientPermissions[],
    targetChildren = <p>Target page</p>
  }) =>
    render(
      <PermissionsScope.Provider
        value={{ permissions: new Set(globalPermissions), logout: jest.fn() }}
      >
        <Internationalise supportedLanguage="en" decimalSeparator=".">
          <MemoryRouter initialEntries={['/target']} initialIndex={0}>
            <Switch>
              <Route
                path="/"
                exact={true}
                render={() => <p>Fallback page</p>}
              />
              <Route
                path="/target"
                permissions={globalPermissions}
                render={() => targetChildren}
              />
            </Switch>
          </MemoryRouter>
        </Internationalise>
      </PermissionsScope.Provider>
    );

  const sharedStateAtom = atom({
    key: 'shared',
    default: 'Shared state'
  });

  const SharedStateConsumer = () => {
    const [sharedState] = useSharedState(sharedStateAtom);
    return <p>{sharedState}</p>;
  };

  test('should render route, if no permission provided', async () => {
    renderRouting({});
    await act(wait);

    expect(screen.getByText('Target page')).toBeInTheDocument();
  });

  test('should render fallback route, if not satisfy permissions', async () => {
    renderRouting({
      globalPermissions: [ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO],
      targetChildren: <p>Fallback page</p>
    });
    await act(wait);

    expect(screen.getByText('Fallback page')).toBeInTheDocument();
  });

  test('should render route, if permission provided and satisfied', async () => {
    renderRouting({
      globalPermissions: [ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]
    });
    await act(wait);

    expect(screen.getByText('Target page')).toBeInTheDocument();
  });

  test('should mount shared state context', async () => {
    renderRouting({
      targetChildren: <SharedStateConsumer />
    });
    await act(wait);

    expect(screen.getByText('Shared state')).toBeInTheDocument();
  });
});
