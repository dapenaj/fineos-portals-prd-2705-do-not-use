/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';

import { ContextWrapper } from '../../../common/utils';
import { WidgetHelpInfoPopover } from '../../../../app/modules/my-dashboard/widget-shared/widget-help-info-popover';

describe('WidgetHelpInfoPopover', () => {
  test('should render help info icon', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <WidgetHelpInfoPopover content="testContent" />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON')
    ).toBeInTheDocument();
  });

  test('should render help info content', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <WidgetHelpInfoPopover content="testContent" />
      </ContextWrapper>
    );

    await act(wait);

    const icon = screen.getByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON');

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.CLOSE_INFO_ICON')
    ).toBeInTheDocument();

    expect(screen.getByText('testContent')).toBeInTheDocument();
  });
});
