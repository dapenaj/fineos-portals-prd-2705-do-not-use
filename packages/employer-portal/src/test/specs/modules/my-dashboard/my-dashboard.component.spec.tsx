/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { within } from '@testing-library/dom';
import { ViewNotificationPermissions } from 'fineos-common';

import { ContextWrapper } from '../../../common/utils';
import * as outstandingNotificationApiModule from '../../../../app/modules/outstanding-notifications/outstanding-notifications.api';
import * as notificationApiModule from '../../../../app/shared/api/notifications.api';
import { MyDashboard } from '../../../../app/modules/my-dashboard/my-dashboard.component';
import { PortalEnvConfigService } from '../../../../app/shared';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

describe('MyDashboard', () => {
  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(outstandingNotificationApiModule, 'fetchOutstandingNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    jest
      .spyOn(notificationApiModule, 'fetchNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValueOnce(Promise.resolve(portalEnvConfig));
  });

  test('should render outstanding notifications widget', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByTestId('outstanding-notifications-widget')
    ).toBeInTheDocument();
  });

  test('should render employees expected to return to work widget', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();
  });

  test('should render notifications created widget', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.NOTIFICATIONS_CREATED_FILTER_CATEGORY')
    ).toBeInTheDocument();
  });

  test('should render decisions-made widget with permissions', () => {
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <MyDashboard />
      </ContextWrapper>
    );

    expect(screen.getByTestId('decisions-made-widget')).toBeInTheDocument();
  });

  test('should render Show/hide panels button', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('WIDGET.SHOW_HIDE_PANELS')).toBeInTheDocument();
  });

  test('should open Dashboard Customization Panel with list of options', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const button = screen.getByText('WIDGET.SHOW_HIDE_PANELS');

    expect(button).toBeInTheDocument();

    userEvent.click(button);

    await act(wait);

    expect(
      within(screen.getByTestId('panel-customization-drawer')).getByText(
        'WIDGET.SHOW_HIDE_PANELS'
      )
    ).toBeInTheDocument();

    const optionList = screen.getByTestId('panel-customization-list');
    expect(
      within(optionList).getByText(
        'WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK'
      )
    ).toBeInTheDocument();

    expect(
      within(optionList).getByText('WIDGET.NOTIFICATIONS_CREATED')
    ).toBeInTheDocument();
  });

  test('should disable employees expected to return to work widget', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const button = screen.getByText('WIDGET.SHOW_HIDE_PANELS');

    expect(button).toBeInTheDocument();

    userEvent.click(button);

    await act(wait);

    expect(
      within(screen.getByTestId('panel-customization-drawer')).getByText(
        'WIDGET.SHOW_HIDE_PANELS'
      )
    ).toBeInTheDocument();

    const expectedRTWOption = within(
      screen.getByTestId('panel-customization-list')
    ).getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK');

    expect(expectedRTWOption).toBeInTheDocument();

    const expectedRTWSwitch = screen.getByTestId(
      'panel-customization-item-employeesExpectedToRTW'
    );

    userEvent.click(expectedRTWSwitch);

    expect(expectedRTWSwitch).not.toBeChecked();

    const saveChangesButton = within(
      screen.getByTestId('panel-customization-drawer')
    ).getByText('FINEOS_COMMON.GENERAL.SAVE_CHANGES');

    expect(saveChangesButton).toBeInTheDocument();

    userEvent.click(saveChangesButton);

    await act(wait);

    expect(
      screen.queryByTestId('expected-to-rtw-widget')
    ).not.toBeInTheDocument();
  });

  test('should disable notifications created widget', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const button = screen.getByText('WIDGET.SHOW_HIDE_PANELS');

    expect(button).toBeInTheDocument();

    userEvent.click(button);

    await act(wait);

    expect(
      within(screen.getByTestId('panel-customization-drawer')).getByText(
        'WIDGET.SHOW_HIDE_PANELS'
      )
    ).toBeInTheDocument();

    const notificationsCreatedOption = within(
      screen.getByTestId('panel-customization-list')
    ).getByText('WIDGET.NOTIFICATIONS_CREATED');

    expect(notificationsCreatedOption).toBeInTheDocument();

    const notificationsCreatedSwitch = screen.getByTestId(
      'panel-customization-item-notificationsCreated'
    );

    userEvent.click(notificationsCreatedSwitch);

    expect(notificationsCreatedSwitch).not.toBeChecked();

    const saveChangesButton = within(
      screen.getByTestId('panel-customization-drawer')
    ).getByText('FINEOS_COMMON.GENERAL.SAVE_CHANGES');

    expect(saveChangesButton).toBeInTheDocument();

    userEvent.click(saveChangesButton);

    await act(wait);

    expect(
      screen.queryByTestId('notifications-created-widget')
    ).not.toBeInTheDocument();
  });

  test('should correctly handle localStorage for dashboardConfig', async () => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(() => null),
        setItem: jest.fn(() => null),
        removeItem: jest.fn(() => null)
      },
      writable: true
    });

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <MyDashboard helpInfoEnabled={true} />
      </ContextWrapper>
    );

    userEvent.click(screen.getByText('WIDGET.SHOW_HIDE_PANELS'));

    await act(wait);

    expect(window.localStorage.getItem).toHaveBeenCalled();

    const optionSwitch = screen.getByTestId(
      'panel-customization-item-notificationsCreated'
    );

    userEvent.click(optionSwitch);

    const saveChangesButton = within(
      screen.getByTestId('panel-customization-drawer')
    ).getByText('FINEOS_COMMON.GENERAL.SAVE_CHANGES');

    userEvent.click(saveChangesButton);

    await wait();

    expect(window.localStorage.setItem).toHaveBeenCalled();
  });
});
