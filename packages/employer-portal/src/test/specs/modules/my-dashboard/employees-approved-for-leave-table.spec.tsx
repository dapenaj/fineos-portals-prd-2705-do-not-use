/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';

import moment from 'moment';
import { render, screen } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';

import { ContextWrapper } from '../../../common/utils';
import { EmployeesApprovedForLeaveTable } from '../../../../app/modules/my-dashboard/employees-approved-for-leave/employees-approved-for-leave-widget-drawer//employees-approved-for-leave-table.component';
import { EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE } from '../../../fixtures/employees-approved-for-leave.fixture';

describe('Employees approved for leave table', () => {
  const mockStartDate = moment()
    .startOf('day')
    .format('YYYY-MM-DD');
  const mockEndDate = moment()
    .startOf('day')
    .format('YYYY-MM-DD');

  test('should render table with employees who are approved to leave', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveTable
          isPending={false}
          columnFilters={{}}
          sortedData={EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE}
          visibleEmployeesApprovedForLeave={
            EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE
          }
          onColumnFilter={jest.fn()}
          onClearFilters={jest.fn()}
          apiFilters={{
            startDate: mockStartDate,
            endDate: mockEndDate
          }}
        />
      </ContextWrapper>
    );

    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(4);
  });

  test('should render table with columns and proper labels', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveTable
          isPending={false}
          columnFilters={{} as any}
          sortedData={EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE}
          visibleEmployeesApprovedForLeave={
            EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE
          }
          onColumnFilter={jest.fn()}
          onClearFilters={jest.fn()}
          apiFilters={{
            startDate: mockStartDate,
            endDate: mockEndDate
          }}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText('WIDGET.TABLE_LABELS.EMPLOYEE')
    ).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.GROUP')).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.REASON')).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.CASE')).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.TABLE_LABELS.LEAVE_TYPE')
    ).toBeInTheDocument();
    const tableHeaders = document.querySelectorAll('thead th');
    expect(tableHeaders).toHaveLength(6);
  });

  test('should render table column filters', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveTable
          isPending={false}
          columnFilters={{} as any}
          sortedData={EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE}
          visibleEmployeesApprovedForLeave={
            EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE
          }
          onColumnFilter={jest.fn()}
          onClearFilters={jest.fn()}
          apiFilters={{
            startDate: mockStartDate,
            endDate: mockEndDate
          }}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.ADMIN_GROUP_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.REASON_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.NOTIFICATION_FILTER_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.LEAVE_TYPE_LABEL')
    ).toBeInTheDocument();
  });
});
