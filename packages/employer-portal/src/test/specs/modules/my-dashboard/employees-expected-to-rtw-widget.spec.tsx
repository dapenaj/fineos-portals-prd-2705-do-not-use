/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  render,
  act,
  screen,
  wait,
  within,
  fireEvent
} from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { ViewNotificationPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { ContextWrapper, selectAntOptionByText } from '../../../common/utils';
import { EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import * as notificationApiModule from '../../../../app/shared/api/notifications.api';
import { EmployeesExpectedToRTWWidget } from '../../../../app/modules/my-dashboard/employees-expected-to-rtw-widget';
import { useDateFilters } from '../../../../app/modules/my-dashboard/widget-shared';
import { DateFilterType } from '../../../../app/modules/my-dashboard/dashboard.type';
import { DateFilter, PortalEnvConfigService } from '../../../../app/shared';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

jest.mock('../../../../app/shared/api/notifications.api', () => ({
  fetchNotifications: jest.fn()
}));

const fetchNotifications = notificationApiModule.fetchNotifications as jest.Mock;

describe('Employees expected to return to work widget', () => {
  const renderHookWrapped = (
    fun: () => {
      futureFilterOptions: {
        value: DateFilter;
        title: string;
      }[];
      pastFilterOptions: {
        value: DateFilter;
        title: string;
      }[];
      DATE_FILTERS: DateFilterType;
    }
  ) =>
    renderHook(() => fun(), {
      wrapper: ({ children }) => <ContextWrapper>{children}</ContextWrapper>
    });

  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
  });

  test('should render widget filters with proper label', async () => {
    fetchNotifications.mockReturnValue(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();
    expect(screen.getByTestId('expected-to-rtw-filter')).toBeInTheDocument();
  });

  test('should open drawer with employees expected to return to work', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();
    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);
  });

  test('should close drawer with employees expected to return to work', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    expect(
      await screen.findByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();
    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);

    userEvent.click(
      within(await screen.findByTestId('expected-to-rtw-drawer')).getByText(
        'FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL'
      )
    );

    fireEvent.transitionEnd(screen.getByTestId('drawer-container'));

    expect(
      screen.queryByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).not.toBeInTheDocument();
  });

  test('should open drawer on slice click', async () => {
    const firstNotification =
      EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE[0];
    const secondNotification =
      EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE[1];
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: [{ ...firstNotification }, { ...secondNotification }],
        total: 2
      })
    );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getAllByTestId('donut-slice')[0]);

    await act(wait);

    expect(screen.getByTestId('expected-to-rtw-drawer')).toBeInTheDocument();
    expect(
      within(screen.getByTestId('expected-to-rtw-drawer')).getByText(
        firstNotification.customer.jobTitle
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('expected-to-rtw-drawer')).queryByText(
        secondNotification.customer.jobTitle
      )
    ).not.toBeInTheDocument();
  });

  test('should filter employees expected to rtw notifications inside widget', async () => {
    const spy = fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );
    const hook = renderHookWrapped(() => useDateFilters());
    const { DATE_FILTERS } = hook.result.current;
    await act(wait);

    await selectAntOptionByText(
      screen.getByTestId('expected-to-rtw-filter'),
      'WIDGET.FILTER_OPTIONS.TODAY'
    );
    expect(spy).toHaveBeenLastCalledWith({
      expectedRTW: DATE_FILTERS[DateFilter.TODAY]
    });
    await selectAntOptionByText(
      screen.getByTestId('expected-to-rtw-filter'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      expectedRTW: DATE_FILTERS[DateFilter.THIS_WEEK]
    });
    await selectAntOptionByText(
      screen.getByTestId('expected-to-rtw-filter'),
      'WIDGET.FILTER_OPTIONS.NEXT_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      expectedRTW: DATE_FILTERS[DateFilter.NEXT_WEEK]
    });
  });

  test('should filter employees expected to rtw notifications inside drawer', async () => {
    const spy = fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await selectAntOptionByText(
      screen.getByTestId('expected-to-rtw-drawer-filter'),
      'WIDGET.FILTER_OPTIONS.TODAY'
    );
    const hook = renderHookWrapped(() => useDateFilters());
    const { DATE_FILTERS } = hook.result.current;
    expect(spy).toHaveBeenLastCalledWith({
      expectedRTW: DATE_FILTERS[DateFilter.TODAY]
    });
    await selectAntOptionByText(
      screen.getByTestId('expected-to-rtw-drawer-filter'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      expectedRTW: DATE_FILTERS[DateFilter.THIS_WEEK]
    });
    await selectAntOptionByText(
      screen.getByTestId('expected-to-rtw-drawer-filter'),
      'WIDGET.FILTER_OPTIONS.NEXT_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      expectedRTW: DATE_FILTERS[DateFilter.NEXT_WEEK]
    });
  });

  test('should filter notifications by employee', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('filter-customer')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerFirstName'
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerLastName'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.VALIDATION.NAME_REQUIRED')
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerFirstName'
      ),
      'Kylo'
    );
    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'Ren'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(6);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by case number', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.NOTIFICATION_FILTER_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      ),
      'NTN-24'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(1);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by job title', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.JOB_TITLE_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.JOB_TITLE_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.JOB_TITLE_PLACEHOLDER'
      ),
      'Master'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by admin group', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK')
    ).toBeInTheDocument();

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.ADMIN_GROUP_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.ADMIN_GROUP_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.ADMIN_GROUP_PLACEHOLDER'
      ),
      'group1'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should render help info icon for widget for employees expected to return to work if helpInfoEnabled is on', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('expected-to-rtw-widget')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for widget for employees expected to return to work if helpInfoEnabled is off', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('expected-to-rtw-widget')).queryByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).not.toBeInTheDocument();
  });

  test('should render help info content for widget for employees expected to return to work', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const icon = within(screen.getByTestId('expected-to-rtw-widget')).getByText(
      'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
    );

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      within(screen.getByTestId('expected-to-rtw-widget')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.EMPLOYEES_ERTW_TITLE')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.EMPLOYEES_ERTW_PANEL')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.DISPLAY_BY_GROUP')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.EMPLOYEES_ERTW_FILTER_GROUP')
    ).toBeInTheDocument();
  });

  test('should render help info icon for drawer for employees expected to return to work', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await act(wait);

    expect(
      within(screen.getByTestId('expected-to-rtw-drawer')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for drawer for employees expected to return to work if helpInfoEnabled is off', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    await act(wait);

    expect(
      within(screen.getByTestId('expected-to-rtw-drawer')).queryByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).not.toBeInTheDocument();
  });

  test('should render help info content for drawer for employees expected to return to work', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE,
        total: EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('expected-to-rtw-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(
        /WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK_DRAWER_HEADER/
      )
    ).toBeInTheDocument();

    const icon = within(screen.getByTestId('expected-to-rtw-drawer')).getByText(
      'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
    );

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      screen.getByTestId('HELP_INFO_DASHBOARDS.EMPLOYEES_ERTW_TITLE')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.EMPLOYEES_ERTW_DETAILS_LIST')
    ).toBeInTheDocument();
  });
});
