/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';
import { GroupClientNotification } from 'fineos-js-api-client';

import { ContextWrapper } from '../../../common/utils';
import { NOTIFICATIONS_CREATED_FIXTURE } from '../../../fixtures/notifications.fixture';
import * as notificationApiModule from '../../../../app/shared/api/notifications.api';
import { NotificationsCreatedTable } from '../../../../app/modules/my-dashboard/notifications-created-widget/notifications-created-table.component';
import { initialTableFilters } from '../../../../app/shared';

jest.mock('../../../../app/shared/api/notifications.api', () => ({
  fetchNotifications: jest.fn()
}));

describe('Notifications created table', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchNotifications')
      .mockReturnValue(new Promise(jest.fn()));
  });

  test('should render table with Notifications created', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedTable
          isPending={false}
          notifications={
            (NOTIFICATIONS_CREATED_FIXTURE as unknown) as GroupClientNotification[]
          }
          filteredNotifications={
            (NOTIFICATIONS_CREATED_FIXTURE as unknown) as GroupClientNotification[]
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);
  });

  test('should render table with columns and proper labels', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedTable
          isPending={false}
          notifications={
            (NOTIFICATIONS_CREATED_FIXTURE as unknown) as GroupClientNotification[]
          }
          filteredNotifications={
            (NOTIFICATIONS_CREATED_FIXTURE as unknown) as GroupClientNotification[]
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.TABLE_LABELS.EMPLOYEE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.TABLE_LABELS.JOB_TITLE')
    ).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.GROUP')).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.REASON')).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.TABLE_LABELS.CREATION_DATE')
    ).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.CASE')).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.DI')).toBeInTheDocument();

    // should render 7 table headers
    const tableHeaders = document.querySelectorAll('thead th');
    expect(tableHeaders).toHaveLength(7);
  });
});
