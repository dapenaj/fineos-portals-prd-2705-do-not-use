/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import { Select, FormattedMessage } from 'fineos-common';

import { WidgetFilter } from '../../../../app/modules/my-dashboard/widget-shared';
import { ContextWrapper } from '../../../common/utils';

describe('WidgetFilter', () => {
  const translations = {
    en: {
      TEST: 'testLabel'
    }
  };
  const mockInputProps = {
    field: { name: 'test-dropdown-input' },
    form: {}
  } as any;
  const mockOptions = [{ value: 'test1', title: 'test1' }];
  test('should render', () => {
    render(
      <ContextWrapper translations={translations}>
        <WidgetFilter
          selectInput={
            <Select {...mockInputProps} optionElements={mockOptions} />
          }
          label={<FormattedMessage id="en.TEST" />}
        />
      </ContextWrapper>
    );

    expect(screen.getByText(translations.en.TEST)).toBeInTheDocument();
    expect(screen.getByTestId('select')).toBeInTheDocument();
  });

  test('should link input with label', () => {
    render(
      <ContextWrapper translations={translations}>
        <WidgetFilter
          selectInput={
            <Select {...mockInputProps} optionElements={mockOptions} />
          }
          label={<FormattedMessage id="en.TEST" />}
        />
      </ContextWrapper>
    );

    expect(document.querySelector('label')).toHaveAttribute('for');
    expect(document.querySelector('input')).toHaveAttribute('id');
    expect(document.querySelector('label')!.getAttribute('for')).toEqual(
      document.querySelector('input')!.getAttribute('id')
    );
  });
});
