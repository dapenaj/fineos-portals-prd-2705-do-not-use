/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import { AbsenceEvent } from 'fineos-js-api-client';

import { ContextWrapper } from '../../../common/utils';
import { useLeavesDecidedDownloadData } from '../../../../app/modules/my-dashboard/decisions-made-widget/decisions-made-widget-drawer/use-leaves-decided-download-data.hook';
import { ABSENCE_EVENTS_FIXTURE } from '../../../fixtures/absence-events.fixture';

describe('useLeavesDecidedDownoadData', () => {
  test('should render correct data', () => {
    const Comp = ({ leavesDecided }) => {
      const downloadData = useLeavesDecidedDownloadData(leavesDecided);
      return <>{JSON.stringify(downloadData)}</>;
    };

    render(
      <ContextWrapper>
        <Comp leavesDecided={[ABSENCE_EVENTS_FIXTURE[0]]} />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        `${ABSENCE_EVENTS_FIXTURE[0].employee.firstName} ${ABSENCE_EVENTS_FIXTURE[0].employee.lastName} - ${ABSENCE_EVENTS_FIXTURE[0].employee.id}`,
        { exact: false }
      )
    ).toBeInTheDocument();

    expect(
      screen.getByText(ABSENCE_EVENTS_FIXTURE[0].adminGroup, { exact: false })
    ).toBeInTheDocument();

    expect(
      screen.getByText(
        ABSENCE_EVENTS_FIXTURE[0].notificationReason.name
          .toUpperCase()
          .replace(/ /g, '_'),
        { exact: false }
      )
    ).toBeInTheDocument();

    expect(
      screen.getByText(
        ABSENCE_EVENTS_FIXTURE[0].eventDate.format('DD-MM-YYYY'),
        { exact: false }
      )
    ).toBeInTheDocument();

    expect(
      screen.getByText(ABSENCE_EVENTS_FIXTURE[0].notificationCase.id, {
        exact: false
      })
    ).toBeInTheDocument();

    expect(
      screen.getByText(
        ABSENCE_EVENTS_FIXTURE[0].eventName.toUpperCase().replace(/ /g, '_'),
        { exact: false }
      )
    ).toBeInTheDocument();
  });
});
