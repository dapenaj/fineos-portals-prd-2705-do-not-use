/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';

import * as notificationApiModule from '../../../../app/shared/api/notifications.api';
import { fetchEnhancedAbsencePeriodDecisions } from '../../../../app/modules/notification/notification.api';
import { EmployeesExpectedToRTWTable } from '../../../../app/modules/my-dashboard/employees-expected-to-rtw-widget/employees-expected-to-rtw-table.component';
import { initialTableFilters } from '../../../../app/shared';
import { ContextWrapper } from '../../../common/utils';
import { EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import { ABSENCE_PERIOD_DECISIONS_FIXTURE } from '../../../fixtures/absence-periods.fixture';

jest.mock('../../../../app/shared/api/notifications.api', () => ({
  fetchNotifications: jest.fn()
}));

jest.mock('../../../../app/modules/notification/notification.api', () => ({
  fetchEnhancedAbsencePeriodDecisions: jest.fn()
}));

const fetchEnhancedAbsencePeriodDecisionsMock = fetchEnhancedAbsencePeriodDecisions as jest.Mock;

describe('Employees expected to return to work table', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchNotifications')
      .mockReturnValue(new Promise(jest.fn()));
  });

  test('should render table with employees expected to return to work', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWTable
          isPending={false}
          filteredNotifications={
            EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);
  });

  test('should render table with columns and proper labels', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWTable
          isPending={false}
          filteredNotifications={
            EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.TABLE_LABELS.EMPLOYEE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.TABLE_LABELS.JOB_TITLE')
    ).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.GROUP')).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.TABLE_LABELS.RETURN_DATE')
    ).toBeInTheDocument();
    expect(screen.getByText('WIDGET.TABLE_LABELS.CASE')).toBeInTheDocument();

    const tableHeaders = document.querySelectorAll('thead th');
    // render 5 column headers with labels and 1 empty for view calendar column
    expect(tableHeaders).toHaveLength(6);
  });

  test('should render table column filters', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWTable
          isPending={false}
          filteredNotifications={
            EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.JOB_TITLE_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.ADMIN_GROUP_ICON_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.FILTERS.NOTIFICATION_FILTER_ICON_LABEL')
    ).toBeInTheDocument();
  });

  test('should render absence calendar button', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWTable
          isPending={false}
          filteredNotifications={
            EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getAllByText('WIDGET.ABSENCE_CALENDAR.VIEW_CALENDAR')[0]
    ).toBeInTheDocument();
  });

  test('should open absence calendar and render it with legend', async () => {
    const spy = fetchEnhancedAbsencePeriodDecisionsMock.mockReturnValueOnce(
      Promise.resolve(ABSENCE_PERIOD_DECISIONS_FIXTURE)
    );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesExpectedToRTWTable
          isPending={false}
          filteredNotifications={
            EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE
          }
          onClearFilters={jest.fn}
          onColumnFilter={jest.fn}
          columnFilters={initialTableFilters}
        />
      </ContextWrapper>
    );

    await act(wait);

    const absenceCalendarButton = screen.getAllByText(
      'WIDGET.ABSENCE_CALENDAR.VIEW_CALENDAR'
    )[0];

    expect(absenceCalendarButton).toBeInTheDocument();
    userEvent.click(absenceCalendarButton);

    await act(wait);

    expect(spy).toHaveBeenCalled();

    await act(wait);

    expect(screen.getByText('WIDGET.ABSENCE_CALENDAR.LEGEND'));
    expect(screen.getByTestId('absence-calendar-legend-today'));
    expect(
      screen.getByText('WIDGET.ABSENCE_CALENDAR.EXPECTED_RETURN_TO_WORK_DATE')
    );
    expect(
      screen.getByRole('button', { name: 'WIDGET.ABSENCE_CALENDAR.TODAY' })
    );
  });
});
