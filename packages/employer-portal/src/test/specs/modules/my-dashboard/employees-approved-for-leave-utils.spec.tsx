/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { flagEmployeesApprovedForLeaveAsGroupRow } from '../../../../app/modules/my-dashboard/employees-approved-for-leave/employees-approved-for-leave-widget-drawer/employees-approved-for-leave-widget-drawer.utils';
import { EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE } from '../../../fixtures/employees-approved-for-leave.fixture';

describe('given an ordered array by employee name, flagEmployeesApprovedForLeaveAsGroupRow', () => {
  it('should flag elements that the next employee in the array has the same name as the current one', () => {
    const employees = flagEmployeesApprovedForLeaveAsGroupRow(
      EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE
    );

    expect(employees[2].isInGroupWithNext).toEqual(true);
    expect(employees[3].isInGroupWithNext).toEqual(false);
  });

  it('should flag elements that the previous employee in the array has the same name as the current one', () => {
    const employees = flagEmployeesApprovedForLeaveAsGroupRow(
      EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE
    );

    expect(employees[2].isInGroupWithPrevious).toEqual(false);
    expect(employees[3].isInGroupWithPrevious).toEqual(true);
  });

  it('should not flag isolated employees', () => {
    const employees = flagEmployeesApprovedForLeaveAsGroupRow(
      EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE
    );

    expect(employees[0].isInGroupWithPrevious).toEqual(false);
    expect(employees[1].isInGroupWithPrevious).toEqual(false);
    expect(employees[1].isInGroupWithNext).toEqual(false);
    expect(employees[1].isInGroupWithNext).toEqual(false);
  });
});
