/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import React from 'react';
import { render, act, screen, wait, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { GroupClientAbsenceService } from 'fineos-js-api-client';
import { ViewNotificationPermissions } from 'fineos-common';

import { ContextWrapper, selectAntOptionByText } from '../../../common/utils';
import { EmployeesApprovedForLeaveWidget } from '../../../../app/modules/my-dashboard/employees-approved-for-leave';
import { EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE } from '../../../fixtures/employees-approved-for-leave.fixture';

describe('Employees approved for leave widget', () => {
  const groupClientAbsenceService = GroupClientAbsenceService.getInstance();

  test('should render empty state', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(Promise.resolve([]));

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);
    expect(
      screen.getByText('WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_EMPTY')
    ).toBeInTheDocument();
  });

  test('should render error state', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(Promise.reject([]));

    render(
      <ContextWrapper>
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.MESSAGE')
    ).toBeInTheDocument();
  });

  test('should render widget', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_TOTAL')
    ).toBeInTheDocument();
  });

  test('should render widget filters with proper label', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('employees-approved-for-leave-filter-date')
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_TOTAL_FILTER_CATEGORY'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('employees-approved-for-leave-filter-category')
    ).toBeInTheDocument();
  });

  test('should filter employees on leave by category', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const ncWidgetDonutSlices = within(
      screen.getByTestId('employees-approved-for-leave')
    ).getAllByTestId('donut-slice');

    // filter is already set to "REASON"
    expect(ncWidgetDonutSlices[0]).toHaveTextContent('2');
    expect(ncWidgetDonutSlices[0]).toHaveTextContent(
      'ENUM_DOMAINS.NOTIFICATION_REASON.BONDING_WITH_A_NEW_CHILD'
    );
    expect(ncWidgetDonutSlices[1]).toHaveTextContent('1');
    expect(ncWidgetDonutSlices[1]).toHaveTextContent(
      'ENUM_DOMAINS.NOTIFICATION_REASON.ACCIDENT_OR_TREATMENT_REQUIRED_FOR_AN_INJURY'
    );

    // change filter to "GROUP"
    await selectAntOptionByText(
      screen.getByTestId('employees-approved-for-leave-filter-category'),
      'WIDGET.FILTER_OPTIONS.GROUP'
    );
    expect(ncWidgetDonutSlices[0]).toHaveTextContent('1');
    expect(ncWidgetDonutSlices[0]).toHaveTextContent('Madrid-IT');
    expect(ncWidgetDonutSlices[1]).toHaveTextContent('1');
    expect(ncWidgetDonutSlices[1]).toHaveTextContent('Dublin-IT');

    // change filter to "LEAVE_TYPE"
    await selectAntOptionByText(
      screen.getByTestId('employees-approved-for-leave-filter-category'),
      'WIDGET.FILTER_OPTIONS.LEAVE_TYPE'
    );
    expect(ncWidgetDonutSlices[0]).toHaveTextContent('3');
    expect(ncWidgetDonutSlices[0]).toHaveTextContent(
      'WIDGET.LEAVE_TYPE.CONTINUOUS_TIME'
    );
  });

  test('should render help info icon for widget for employees expected for leave work if helpInfoEnabled is on', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('employees-approved-for-leave')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for widget for employees expected for leave work if helpInfoEnabled is off', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('employees-approved-for-leave')).queryByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).not.toBeInTheDocument();
  });

  test('should filter employees on leave inside widget', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);
    expect(screen.getByText('WIDGET.FILTER_OPTIONS.TODAY')).toBeInTheDocument();
    await selectAntOptionByText(
      await screen.findByTestId('employees-approved-for-leave-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
    expect(
      screen.getAllByText('WIDGET.FILTER_OPTIONS.THIS_WEEK')[0]
    ).toBeInTheDocument();
    await selectAntOptionByText(
      await screen.findByTestId('employees-approved-for-leave-filter-date'),
      'WIDGET.FILTER_OPTIONS.NEXT_WEEK'
    );
    expect(
      screen.getAllByText('WIDGET.FILTER_OPTIONS.NEXT_WEEK')[0]
    ).toBeInTheDocument();
    await selectAntOptionByText(
      await screen.findByTestId('employees-approved-for-leave-filter-date'),
      'WIDGET.FILTER_OPTIONS.WEEK_AFTER_NEXT'
    );
    expect(
      screen.getAllByText('WIDGET.FILTER_OPTIONS.WEEK_AFTER_NEXT')[0]
    ).toBeInTheDocument();
  });

  test('should open drawer after clicking on view details button', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    expect(
      screen.queryByText(
        /WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_FILTER_DATE_DRAWER_HEADER/
      )
    ).not.toBeInTheDocument();

    userEvent.click(await screen.findByText('WIDGET.VIEW_DETAILS'));

    expect(
      await screen.findByText(
        /WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_FILTER_DATE_DRAWER_HEADER/
      )
    ).toBeInTheDocument();
  });

  test('should open drawer after clicking on donut graph slice', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.queryByTestId(
        /WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_FILTER_DATE_DRAWER_HEADER/
      )
    ).not.toBeInTheDocument();

    userEvent.click(
      screen.getByText(
        'ENUM_DOMAINS.NOTIFICATION_REASON.BONDING_WITH_A_NEW_CHILD',
        {
          exact: false
        }
      )
    );

    await act(wait);

    expect(
      screen.getByTestId(
        /WIDGET.EMPLOYEES_APPROVED_FOR_LEAVE_FILTER_DATE_DRAWER_HEADER/
      )
    ).toBeInTheDocument();
  });

  test('should make a request on changing filter in drawer', async () => {
    const employeesOnLeaveSpy = jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    expect(employeesOnLeaveSpy).toHaveBeenCalledTimes(1);

    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));

    await act(wait);

    expect(
      screen.getByTestId('employees-approved-for-leave-filter')
    ).toBeInTheDocument();

    await selectAntOptionByText(
      screen.getByTestId('employees-approved-for-leave-filter'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );

    expect(employeesOnLeaveSpy).toHaveBeenCalledTimes(2);
  });

  test('should change date filter select value in widget when changed in drawer', async () => {
    jest
      .spyOn(groupClientAbsenceService, 'fetchEmployeesOnLeave')
      .mockReturnValueOnce(
        Promise.resolve(EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE)
      );

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.EMPLOYEES_LEAVE_HOURS]}
      >
        <EmployeesApprovedForLeaveWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(
        screen.getByTestId('employees-approved-for-leave-filter-date')
      ).getByText('WIDGET.FILTER_OPTIONS.TODAY')
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));

    await act(wait);

    expect(
      within(
        screen.getByTestId('employees-approved-for-leave-filter-date')
      ).getByText('WIDGET.FILTER_OPTIONS.TODAY')
    ).toBeInTheDocument();

    await selectAntOptionByText(
      screen.getByTestId('employees-approved-for-leave-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );

    userEvent.click(
      screen.getByText('FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL')
    );

    await selectAntOptionByText(
      screen.getByTestId('employees-approved-for-leave-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
  });
});
