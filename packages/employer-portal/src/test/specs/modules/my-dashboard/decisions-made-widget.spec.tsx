/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import { within } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';
import { AbsenceEventsService } from 'fineos-js-api-client';

import { DecisionsMadeWidget } from '../../../../app/modules/my-dashboard/decisions-made-widget';
import { ContextWrapper, selectAntOptionByText } from '../../../common/utils';
import { ABSENCE_EVENTS_FIXTURE } from '../../../fixtures/absence-events.fixture';

describe('DecisionsMadeWidget', () => {
  const absenceEventsService = AbsenceEventsService.getInstance();

  test('should render empty state', async () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.resolve([]));
    render(
      <ContextWrapper>
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);
    expect(screen.getByText('WIDGET.DECISIONS_MADE_EMPTY')).toBeInTheDocument();
  });

  test('should render error state', async () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.reject([]));

    render(
      <ContextWrapper>
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.MESSAGE')
    ).toBeInTheDocument();
  });

  test('filters should have selected correct values on init', () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValueOnce(Promise.resolve([]));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    expect(
      within(screen.getByTestId('decisions-made-filter-date')).getByText(
        'WIDGET.FILTER_OPTIONS.TODAY'
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('decisions-made-filter-category')).getByText(
        'WIDGET.FILTER_OPTIONS.DECISION'
      )
    ).toBeInTheDocument();
  });

  test('should fetch data on date filter change', async () => {
    const absenceEventsSpy = jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValueOnce(Promise.resolve([]));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    expect(absenceEventsSpy).toHaveBeenCalledTimes(2);

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );

    expect(absenceEventsSpy).toHaveBeenCalledTimes(4);

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-date'),
      'WIDGET.FILTER_OPTIONS.PREV_WEEK'
    );

    expect(absenceEventsSpy).toHaveBeenCalledTimes(6);
  });

  test('should display correctly filtered data', async () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.resolve(ABSENCE_EVENTS_FIXTURE));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.DECISIONS_MADE_LEAVE_REQUEST_APPROVED_LABEL', {
        exact: false
      })
    ).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.DECISIONS_MADE_LEAVE_REQUEST_DENIED_LABEL', {
        exact: false
      })
    ).toBeInTheDocument();

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-category'),
      'WIDGET.FILTER_OPTIONS.REASON'
    );

    expect(
      screen.getByText(
        'ENUM_DOMAINS.NOTIFICATION_REASON.ACCIDENT_OR_TREATMENT_REQUIRED_FOR_AN_INJURY',
        { exact: false }
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'ENUM_DOMAINS.NOTIFICATION_REASON.PREGNANCY_BIRTH_OR_RELATED_MEDICAL_TREATMENT',
        { exact: false }
      )
    ).toBeInTheDocument();

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-category'),
      'WIDGET.FILTER_OPTIONS.GROUP'
    );

    expect(
      screen.getByText(ABSENCE_EVENTS_FIXTURE[0].adminGroup, { exact: false })
    ).toBeInTheDocument();
    expect(
      screen.getByText(ABSENCE_EVENTS_FIXTURE[1].adminGroup, { exact: false })
    ).toBeInTheDocument();
  });

  test('should open drawer after clicking on view details button', async () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.resolve(ABSENCE_EVENTS_FIXTURE));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    expect(
      screen.queryByText(/WIDGET.DECISIONS_MADE_FILTER_DATE_DRAWER_HEADER/)
    ).not.toBeInTheDocument();

    userEvent.click(await screen.findByText('WIDGET.VIEW_DETAILS'));

    expect(
      await screen.findByText(/WIDGET.DECISIONS_MADE_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();
  });

  test('should open drawer after clicking on donut graph slice', async () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.resolve(ABSENCE_EVENTS_FIXTURE));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.queryByTestId('drawer')).not.toBeInTheDocument();

    userEvent.click(
      screen.getByText('WIDGET.DECISIONS_MADE_LEAVE_REQUEST_DENIED_LABEL', {
        exact: false
      })
    );
  });

  test('should make a request on changing filter in drawer', async () => {
    const absenceEventsSpy = jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.resolve(ABSENCE_EVENTS_FIXTURE));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(absenceEventsSpy).toHaveBeenCalledTimes(2);

    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );

    expect(absenceEventsSpy).toHaveBeenCalledTimes(4);
  });

  test('should change date filter select value in widget when changed in drawer', async () => {
    jest
      .spyOn(absenceEventsService, 'fetchAbsenceEvents')
      .mockReturnValue(Promise.resolve(ABSENCE_EVENTS_FIXTURE));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_EVENTS
        ]}
      >
        <DecisionsMadeWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('decisions-made-filter-date')).getByText(
        'WIDGET.FILTER_OPTIONS.TODAY'
      )
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));

    await act(wait);

    expect(
      within(screen.getByTestId('decisions-made-filter-date')).getByText(
        'WIDGET.FILTER_OPTIONS.TODAY'
      )
    ).toBeInTheDocument();

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );

    userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CLOSE'));

    await selectAntOptionByText(
      screen.getByTestId('decisions-made-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
  });
});
