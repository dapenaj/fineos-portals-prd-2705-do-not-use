/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  render,
  act,
  screen,
  wait,
  within,
  fireEvent
} from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { ViewNotificationPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { ContextWrapper, selectAntOptionByText } from '../../../common/utils';
import * as notificationApiModule from '../../../../app/shared/api/notifications.api';
import { NotificationsCreatedWidget } from '../../../../app/modules/my-dashboard/notifications-created-widget';
import { NOTIFICATIONS_CREATED_FIXTURE } from '../../../fixtures/notifications.fixture';
import { useDateFilters } from '../../../../app/modules/my-dashboard/widget-shared';
import { DateFilterType } from '../../../../app/modules/my-dashboard/dashboard.type';
import { DateFilter } from '../../../../app/shared';

jest.mock('../../../../app/shared/api/notifications.api', () => ({
  fetchNotifications: jest.fn()
}));

const fetchNotifications = notificationApiModule.fetchNotifications as jest.Mock;

describe('Notifications created widget', () => {
  const renderHookWrapped = (
    fun: () => {
      futureFilterOptions: {
        value: DateFilter;
        title: string;
      }[];
      pastFilterOptions: {
        value: DateFilter;
        title: string;
      }[];
      DATE_FILTERS: DateFilterType;
    }
  ) =>
    renderHook(() => fun(), {
      wrapper: ({ children }) => <ContextWrapper>{children}</ContextWrapper>
    });
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchNotifications')
      .mockReturnValue(new Promise(jest.fn()));
  });

  test('should render widget filters with proper label', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('notifications-created-filter-date')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WIDGET.NOTIFICATIONS_CREATED_FILTER_CATEGORY')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('notifications-created-filter-category')
    ).toBeInTheDocument();
  });

  test('should filter notifications by category', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const ncWidgetDonutSlices = within(
      screen.getByTestId('notifications-created-widget')
    ).getAllByTestId('donut-slice');

    // filter is already set to "disability related"
    expect(ncWidgetDonutSlices[0]).toHaveTextContent('5');
    expect(ncWidgetDonutSlices[0]).toHaveTextContent(
      'WIDGET.NOTIFICATIONS_CREATED_DISABILITY_RELATED'
    );
    expect(ncWidgetDonutSlices[1]).toHaveTextContent('5');
    expect(ncWidgetDonutSlices[1]).toHaveTextContent(
      'WIDGET.NOTIFICATIONS_CREATED_NON_DISABILITY_RELATED'
    );

    // change filter to "reason"
    await selectAntOptionByText(
      screen.getByTestId('notifications-created-filter-category'),
      'WIDGET.FILTER_OPTIONS.REASON'
    );

    // labels have updated
    expect(ncWidgetDonutSlices[0]).toHaveTextContent('7');
    expect(ncWidgetDonutSlices[0]).toHaveTextContent(
      'ENUM_DOMAINS.NOTIFICATION_REASON.ACCIDENT'
    );
    expect(ncWidgetDonutSlices[1]).toHaveTextContent('3');
    expect(ncWidgetDonutSlices[1]).toHaveTextContent(
      'ENUM_DOMAINS.NOTIFICATION_REASON.PREGNANCY'
    );
  });

  test('should open drawer with notifications created', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();
    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);
  });

  test('should close drawer with notifications created', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    const viewDetailsButton = within(
      await screen.findByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    expect(
      await screen.findByText(
        /WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/
      )
    ).toBeInTheDocument();
    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);

    userEvent.click(await screen.findByText('FINEOS_COMMON.GENERAL.CLOSE'));

    fireEvent.transitionEnd(await screen.findByTestId('drawer-container'));

    expect(
      screen.queryByText(
        /WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/
      )
    ).not.toBeInTheDocument();
  });

  test('should open drawer on slice click when filtering set to reason', async () => {
    const firstNotification = NOTIFICATIONS_CREATED_FIXTURE[0];
    const secondNotification = NOTIFICATIONS_CREATED_FIXTURE[3];
    const mockTranslations = {
      'ENUM_DOMAINS.NOTIFICATION_REASON.PREGNANCY_BIRTH_OR_RELATED_MEDICAL_TREATMENT':
        'Pregnancy, birth or related medical treatment'
    };
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: [firstNotification, secondNotification],
        total: 2
      })
    );

    render(
      <ContextWrapper
        translations={mockTranslations}
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    await selectAntOptionByText(
      screen.getByTestId('notifications-created-filter-category'),
      'WIDGET.FILTER_OPTIONS.REASON'
    );

    // second slice should be Pregnancy
    userEvent.click(screen.getAllByTestId('donut-slice')[1]);

    await act(wait);

    expect(
      screen.getByTestId('notifications-created-drawer')
    ).toBeInTheDocument();
    expect(
      screen.queryByText(firstNotification.customer.jobTitle)
    ).not.toBeInTheDocument();
    expect(
      screen.getByText(secondNotification.customer.jobTitle)
    ).toBeInTheDocument();
  });

  test('should open drawer on slice click when filtering set to disability related', async () => {
    const firstNotification = {
      ...NOTIFICATIONS_CREATED_FIXTURE[0],
      subCases: [
        {
          ...NOTIFICATIONS_CREATED_FIXTURE[0].subCases[0],
          caseType: 'Group Disability Claim'
        }
      ]
    };
    const secondNotification = {
      ...NOTIFICATIONS_CREATED_FIXTURE[2],
      subCases: [
        {
          ...NOTIFICATIONS_CREATED_FIXTURE[2].subCases[0],
          caseType: 'Absence Case'
        }
      ]
    };
    const mockTranslations = {
      'WIDGET.NOTIFICATIONS_CREATED_DISABILITY_RELATED': 'Disability-related'
    };
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: [firstNotification, secondNotification],
        total: 2
      })
    );

    render(
      <ContextWrapper
        translations={mockTranslations}
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getAllByTestId('donut-slice')[0]);

    await act(wait);

    expect(
      screen.getByTestId('notifications-created-drawer')
    ).toBeInTheDocument();
    expect(
      screen.getByText(firstNotification.customer.jobTitle)
    ).toBeInTheDocument();
    expect(
      screen.queryByText(secondNotification.customer.jobTitle)
    ).not.toBeInTheDocument();
  });

  test('should filter notifications created inside widget', async () => {
    const spy = fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    await selectAntOptionByText(
      screen.getByTestId('notifications-created-filter-date'),
      'WIDGET.FILTER_OPTIONS.TODAY'
    );
    const hook = renderHookWrapped(() => useDateFilters());
    const { DATE_FILTERS } = hook.result.current;
    expect(spy).toHaveBeenLastCalledWith({
      notificationsCreated: DATE_FILTERS[DateFilter.TODAY]
    });
    await selectAntOptionByText(
      screen.getByTestId('notifications-created-filter-date'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      notificationsCreated: DATE_FILTERS[DateFilter.THIS_WEEK]
    });
    await selectAntOptionByText(
      screen.getByTestId('notifications-created-filter-date'),
      'WIDGET.FILTER_OPTIONS.PREV_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      notificationsCreated: DATE_FILTERS[DateFilter.PREV_WEEK]
    });
  });

  test('should filter notifications created inside drawer', async () => {
    const spy = fetchNotifications.mockReturnValue(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      await screen.findByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await selectAntOptionByText(
      await screen.findByTestId('notifications-created-drawer-filter'),
      'WIDGET.FILTER_OPTIONS.TODAY'
    );
    const hook = renderHookWrapped(() => useDateFilters());
    const { DATE_FILTERS } = hook.result.current;
    expect(spy).toHaveBeenLastCalledWith({
      notificationsCreated: DATE_FILTERS[DateFilter.TODAY]
    });
    await act(wait);

    await selectAntOptionByText(
      await screen.findByTestId('notifications-created-drawer-filter'),
      'WIDGET.FILTER_OPTIONS.THIS_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      notificationsCreated: DATE_FILTERS[DateFilter.THIS_WEEK]
    });
    await act(wait);

    await selectAntOptionByText(
      await screen.findByTestId('notifications-created-drawer-filter'),
      'WIDGET.FILTER_OPTIONS.PREV_WEEK'
    );
    expect(spy).toHaveBeenLastCalledWith({
      notificationsCreated: DATE_FILTERS[DateFilter.PREV_WEEK]
    });
  });

  test('should filter notifications by employee', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      await screen.findByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('filter-customer')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerFirstName'
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerLastName'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.VALIDATION.NAME_REQUIRED')
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerFirstName'
      ),
      'Kylo'
    );
    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'Ren'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(6);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by case number', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.NOTIFICATION_FILTER_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      ),
      'NTN-24'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(1);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by job title', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.JOB_TITLE_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.JOB_TITLE_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.JOB_TITLE_PLACEHOLDER'
      ),
      'Leader'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(6);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by admin group', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.ADMIN_GROUP_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.ADMIN_GROUP_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.ADMIN_GROUP_PLACEHOLDER'
      ),
      'group1'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by reason', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText(
      'NOTIFICATIONS.FILTERS.REASON_ICON_LABEL'
    );

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    const checkboxOption = within(
      screen.getByTestId('table-checkbox-filter')
    ).getByText(
      'ENUM_DOMAINS.NOTIFICATION_REASON.PREGNANCY_BIRTH_OR_RELATED_MEDICAL_TREATMENT'
    );

    expect(checkboxOption).toBeInTheDocument();

    // set a filter value
    checkboxOption.parentElement?.parentElement?.click();

    await act(wait);

    userEvent.click(
      within(screen.getByTestId('table-checkbox-filter')).getByText(
        'FINEOS_COMMON.GENERAL.OK'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(3);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by DI', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText('NOTIFICATIONS.FILTERS.DI_ICON_LABEL');

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    const checkboxOption = within(
      screen.getByTestId('table-radio-filter')
    ).getByText('FINEOS_COMMON.GENERAL.YES');

    expect(checkboxOption).toBeInTheDocument();

    // set a filter value
    checkboxOption.parentElement?.parentElement?.click();

    await act(wait);

    userEvent.click(
      within(screen.getByTestId('table-radio-filter')).getByText(
        'FINEOS_COMMON.GENERAL.OK'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(5);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should filter notifications by non-DI', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    const filterIcon = screen.getByText('NOTIFICATIONS.FILTERS.DI_ICON_LABEL');

    expect(filterIcon).toBeInTheDocument();

    userEvent.click(filterIcon);
    await act(wait);
    const checkboxOption = within(
      screen.getByTestId('table-radio-filter')
    ).getByText('FINEOS_COMMON.GENERAL.NO');

    expect(checkboxOption).toBeInTheDocument();

    // set a filter value
    checkboxOption.parentElement?.parentElement?.click();

    await act(wait);

    userEvent.click(
      within(screen.getByTestId('table-radio-filter')).getByText(
        'FINEOS_COMMON.GENERAL.OK'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(5);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();
  });

  test('should render help info icon for widget for notifications-created if helpInfoEnabled is on', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('notifications-created-widget')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for widget for notifications-created if helpInfoEnabled is off', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.getByTestId('notifications-created-widget')).queryByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).not.toBeInTheDocument();
  });

  test('should render help info content for widget for notifications-created', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const icon = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON');

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      within(screen.getByTestId('notifications-created-widget')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.NOTIF_CREATED_TITLE')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.NOTIF_CREATED_PANEL')
    ).toBeInTheDocument();

    expect(
      screen.getByText(
        'HELP_INFO_DASHBOARDS.NOTIF_CREATED_FILTER_DISABILITY_RELATED'
      )
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.NOTIF_CREATED_FILTER_REASON')
    ).toBeInTheDocument();
  });

  test('should render help info icon for drawer for notifications-created if helpInfoEnabled is on', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    expect(
      within(screen.getByTestId('notifications-created-drawer')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for drawer for notifications-created if helpInfoEnabled is off', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={false} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    await act(wait);

    expect(
      within(screen.getByTestId('notifications-created-drawer')).queryByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).not.toBeInTheDocument();
  });

  test('should render help info content for drawer for notifications-created', async () => {
    fetchNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: NOTIFICATIONS_CREATED_FIXTURE,
        total: NOTIFICATIONS_CREATED_FIXTURE.length
      })
    );
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
      >
        <NotificationsCreatedWidget helpInfoEnabled={true} />
      </ContextWrapper>
    );

    await act(wait);

    const viewDetailsButton = within(
      screen.getByTestId('notifications-created-widget')
    ).getByText('WIDGET.VIEW_DETAILS');

    expect(viewDetailsButton).toBeInTheDocument();

    userEvent.click(viewDetailsButton);

    await act(wait);

    expect(
      screen.getByText(/WIDGET.NOTIFICATIONS_CREATED_FILTER_DATE_DRAWER_HEADER/)
    ).toBeInTheDocument();

    const icon = within(
      screen.getByTestId('notifications-created-drawer')
    ).getByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON');

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      screen.getByTestId('HELP_INFO_DASHBOARDS.NOTIF_CREATED_TITLE')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.NOTIF_CREATED_DETAILS_LIST')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.FILTER_BY_DI')
    ).toBeInTheDocument();

    expect(
      screen.getByText(
        'HELP_INFO_DASHBOARDS.NOTIF_CREATED_DETAILS_LIST_FILTER_DISABILITY_RELATED'
      )
    ).toBeInTheDocument();
  });
});
