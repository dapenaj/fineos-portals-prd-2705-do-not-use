/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';
import { Router } from 'react-router';
import { createMemoryHistory as createHistory } from 'history';
import {
  ViewCustomerDataPermissions,
  SkipToContentProvider
} from 'fineos-common';

import {
  render,
  wait,
  within,
  act,
  RenderResult
} from '../../../../common/custom-render';
import {
  ContextWrapper,
  selectTodayDate,
  releaseEventLoop
} from '../../../../common/utils';
import { NonEstablishedIntake } from '../../../../../app/modules/intake';
import { ENUM_INSTANCES_FIXTURE } from '../../../../fixtures/enum-instances.fixture';
import * as notificationHookModule from '../../../../../app/modules/intake/notification-reasons.hook';
import * as nonEstablishedIntakeApi from '../../../../../app/modules/intake/non-established-intake/non-established-intake.api';
import { PortalEnvConfigService } from '../../../../../app/shared';
import portalEnvConfig from '../../../../../../public/config/portal.env.json';

import {
  fillRequiredEmployeeDetailsFields,
  fillRequiredInitialRequestFields
} from './non-established-intake.utils';

jest.mock(
  '../../../../../app/modules/intake/notification-reasons.hook',
  () => ({
    useNotificationReasons: jest.fn()
  })
);

jest.mock(
  '../../../../../app/modules/intake/non-established-intake/non-established-intake.api.ts',
  () => ({
    addNonEstablishedIntake: jest.fn()
  })
);

const fetchNotificationReasons = notificationHookModule.useNotificationReasons as jest.Mock;

const addNonEstablishedIntake = nonEstablishedIntakeApi.addNonEstablishedIntake as jest.Mock;

describe('NonEstablishedIntake', () => {
  beforeEach(() => {
    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-05-01').valueOf());

    jest.setTimeout(30000);
    jest.spyOn(nonEstablishedIntakeApi, 'addNonEstablishedIntake');

    fetchNotificationReasons.mockReturnValue({
      isLoadingReasons: false,
      notificationReasons: ENUM_INSTANCES_FIXTURE
    });

    addNonEstablishedIntake.mockReturnValue(Promise.resolve({ eFormId: 1 }));
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
  });

  test('should submit intake', async () => {
    const { getSubmitButtonByText } = render(
      <SkipToContentProvider>
        <ContextWrapper>
          <NonEstablishedIntake />
        </ContextWrapper>
      </SkipToContentProvider>
    );

    await wait();

    await act(() => {
      userEvent.click(screen.getByText('INTAKE.PROCEED'));
      return wait();
    });

    expect(
      within(screen.getByTestId('first-name-field')).getByTestId('text-input')
    ).not.toBeValid();

    await fillRequiredEmployeeDetailsFields({
      getByTestId: screen.getByTestId
    } as RenderResult);

    await userEvent.type(
      within(screen.getByTestId('email-field')).getByTestId('text-input'),
      'abc@fineos.com'
    );

    await userEvent.type(
      within(screen.getByTestId('address-line2-field')).getByTestId(
        'text-input'
      ),
      'addressLine2'
    );

    await userEvent.type(
      within(screen.getByTestId('address-line3-field')).getByTestId(
        'text-input'
      ),
      'addressLine3'
    );

    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    expect(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toHaveLength(2);

    userEvent.type(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE'),
      'engineer'
    );

    await selectTodayDate(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE')
    );

    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    expect(screen.getAllByText('INTAKE.INITIAL_REQUEST.HEADER')).toHaveLength(
      2
    );

    await act(() => {
      userEvent.click(screen.getByText('INTAKE.FINISH'));
      return releaseEventLoop();
    });

    expect(screen.getByLabelText('Administration')).toBeInTheDocument();

    await fillRequiredInitialRequestFields({
      getByLabelText: screen.getByLabelText
    } as RenderResult);

    userEvent.click(getSubmitButtonByText('INTAKE.FINISH'));

    await wait();

    expect(
      nonEstablishedIntakeApi.addNonEstablishedIntake
    ).toHaveBeenCalledWith({
      address:
        'addressLine1, addressLine2, addressLine3, city, AL, postCode, USA.',
      dateOfHire: expect.any(String),
      email: 'abc@fineos.com',
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'engineer',
      notificationLastWorkingDay: '2020-05-01',
      notificationReason: {
        domainId: 145,
        domainName: 'domainName1',
        fullId: 145,
        name: 'Administration'
      },
      phoneNumber: 'Cell 00-12345'
    });

    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.SUB_TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.EMPLOYER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.EMPLOYEE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.PHYSICIAN')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.WHAT_HAPPENS_NEXT_TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'INTAKE.NON_ESTABLISHED.SUCCESS.WHAT_HAPPENS_NEXT_MESSAGE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.NON_ESTABLISHED.SUCCESS.PRIVACY_NOTICE')
    ).toBeInTheDocument();
    expect(screen.getByText('FINEOS_COMMON.GENERAL.CLOSE')).toBeInTheDocument();
  });

  test('should handle error response on submit', async () => {
    addNonEstablishedIntake.mockImplementation(() =>
      Promise.reject(new Error())
    );

    const { getSubmitButtonByText } = render(
      <SkipToContentProvider>
        <ContextWrapper>
          <NonEstablishedIntake />
        </ContextWrapper>
      </SkipToContentProvider>
    );

    await wait();

    await fillRequiredEmployeeDetailsFields({
      getByTestId: screen.getByTestId
    } as RenderResult);

    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    userEvent.type(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE'),
      'engineer'
    );

    await selectTodayDate(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE')
    );

    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    await fillRequiredInitialRequestFields({
      getByLabelText: screen.getByLabelText
    } as RenderResult);

    userEvent.click(getSubmitButtonByText('INTAKE.FINISH'));

    await wait();

    expect(
      screen.queryByText('INTAKE.INITIAL_REQUEST.HEADER')
    ).not.toBeInTheDocument();

    expect(screen.getByText('INTAKE.ERROR.BODY')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ERROR.PHONE')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ERROR.TIME')).toBeInTheDocument();
  });

  test('should block navigation when user try to leave intake during filling', async () => {
    const getUserConfirmation = jest.fn((message, callback) => {
      callback(false);
    });
    const history = createHistory({
      getUserConfirmation
    });
    render(
      <SkipToContentProvider>
        <ContextWrapper
          permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
        >
          <Router history={history}>
            <NonEstablishedIntake />
          </Router>
        </ContextWrapper>
      </SkipToContentProvider>
    );

    userEvent.type(
      within(screen.getByTestId('first-name-field')).getByTestId('text-input'),
      'firstName'
    );
    expect(screen.getByText('NAVIGATION.MY_DASHBOARD')).toBeInTheDocument();

    userEvent.click(screen.getByText('NAVIGATION.MY_DASHBOARD'));

    expect(getUserConfirmation).toHaveBeenCalled();
  });

  test('should not block navigation when user try to leave intake without any changes', async () => {
    const getUserConfirmation = jest.fn((message, callback) => {
      callback(false);
    });
    const history = createHistory({
      getUserConfirmation
    });
    render(
      <SkipToContentProvider>
        <ContextWrapper
          permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
        >
          <Router history={history}>
            <NonEstablishedIntake />
          </Router>
        </ContextWrapper>
      </SkipToContentProvider>
    );

    expect(screen.getByText('NAVIGATION.MY_DASHBOARD')).toBeInTheDocument();

    userEvent.click(screen.getByText('NAVIGATION.MY_DASHBOARD'));

    expect(getUserConfirmation).not.toHaveBeenCalled();
  });
});
