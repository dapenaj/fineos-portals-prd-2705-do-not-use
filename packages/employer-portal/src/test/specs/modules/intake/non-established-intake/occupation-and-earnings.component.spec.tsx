/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { wait, act, fireEvent, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BaseDomain } from 'fineos-js-api-client';

import { within, render } from '../../../../common/custom-render';
import {
  ContextWrapper,
  selectAntOptionByText,
  selectTodayDate,
  releaseEventLoop
} from '../../../../common/utils';
import { NonEstablishedOccupationAndEarnings } from '../../../../../app/modules/intake/non-established-intake/occupation-and-earnings';

describe('NonEstablishedOccupationAndEarnings', () => {
  beforeEach(() => {
    jest.setTimeout(30000);
  });

  test('should render', async () => {
    render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={jest.fn()}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.FREQUENCY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ABANDON_INTAKE')).toBeInTheDocument();
  });

  test('should submit', async () => {
    const mockOnFinish = jest.fn();

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={mockOnFinish}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    await wait();

    await act(() => {
      userEvent.click(screen.getByText('INTAKE.PROCEED'));
      return releaseEventLoop();
    });

    expect(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE')
    ).toBeValid();

    userEvent.type(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE'),
      'engineer'
    );

    await selectTodayDate(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE')
    );

    userEvent.type(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS'),
      '1000'
    );

    await selectAntOptionByText(
      screen
        .getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.FREQUENCY')
        .closest('[data-test-el="dropdown-input"]') as HTMLElement,
      'ENUM_DOMAINS.EARNING_FREQUENCY.WEEKLY'
    );

    userEvent.click(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    );

    await wait();

    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    expect(mockOnFinish).toHaveBeenCalledWith({
      dateOfHire: expect.anything(),
      earnings: {
        amount: 1000,
        frequency: {
          fullId: 7520001,
          id: '7520001',
          name: 'Weekly'
        }
      },
      fixedPattern: {
        includeWeekend: false,
        isNonStandardWeek: false,
        workWeek: []
      },
      jobTitle: 'engineer',
      rotatingPattern: {
        workPatternWeek1: [],
        workPatternWeek2: [],
        workPatternWeek3: [],
        workPatternWeek4: []
      },
      variablePattern: null,
      workPatternType: null
    });
  });

  test('should display fixed working hours', async () => {
    const mockOnFinish = jest.fn();
    render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={mockOnFinish}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.queryByText('WORK_PATTERN.NON_STANDARD_WORKING_WEEK')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.MONDAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.TUESDAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.WEDNESDAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.THURSDAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.FRIDAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.OCCUPATION_AND_EARNINGS.INCLUDE_WEEKEND')
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId('SATURDAY-input')).not.toBeInTheDocument();
    expect(screen.queryByTestId('SUNDAY-input')).not.toBeInTheDocument();

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.FIXED'
    );

    expect(
      screen.getByText('WORK_PATTERN.NON_STANDARD_WORKING_WEEK')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.MONDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.TUESDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.WEDNESDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.THURSDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.FRIDAY')
    ).toBeInTheDocument();

    userEvent.click(screen.getByTestId('non-standard-working-week-switch'));

    await wait();

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.INCLUDE_WEEKEND')
    ).toBeInTheDocument();

    userEvent.click(screen.getByTestId('include-weekend-switch'));

    await wait();

    expect(screen.getByTestId('SATURDAY-input')).toBeInTheDocument();
    expect(screen.getByTestId('SUNDAY-input')).toBeInTheDocument();
  });

  test('should display rotating working hours', async () => {
    render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={jest.fn()}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    await wait();

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.ROTATING'
    );

    userEvent.click(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.WEEKS_NUMBER')[0]
    );

    await wait();

    expect(
      screen.getAllByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.MONDAY')
    ).toHaveLength(2);
    expect(
      screen.getAllByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.TUESDAY')
    ).toHaveLength(2);
    expect(
      screen.getAllByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.WEDNESDAY')
    ).toHaveLength(2);
    expect(
      screen.getAllByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.THURSDAY')
    ).toHaveLength(2);
    expect(
      screen.getAllByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.FRIDAY')
    ).toHaveLength(2);
  });

  test('should display variable working hours', async () => {
    render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={jest.fn()}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.queryByText(
        'INTAKE.OCCUPATION_AND_EARNINGS.DESCRIPTION_FOR_VARIABLE_WORKING_PATTERNS'
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        'INTAKE.OCCUPATION_AND_EARNINGS.RECORD_WORKING_PATTERN'
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN_HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.GENERAL.OK')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION_PLACEHOLDER'
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN_REPEATS')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_DAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION'
      )
    ).not.toBeInTheDocument();

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.VARIABLE'
    );

    expect(
      screen.getByPlaceholderText(
        'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION_PLACEHOLDER'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN_REPEATS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_DAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION')
    ).toBeInTheDocument();

    userEvent.click(screen.getByTestId('work-pattern-repeats'));

    await wait();

    expect(screen.getByTestId('number-of-weeks-field')).toBeInTheDocument();

    userEvent.click(screen.getByTestId('work-day-is-same-length'));

    await wait();

    expect(screen.getByTestId('day-length-field')).toBeInTheDocument();
  });

  test('should validate variable working hours', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={jest.fn()}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    await wait();

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.VARIABLE'
    );

    await wait();

    userEvent.click(screen.getByTestId('work-pattern-repeats'));

    expect(screen.getByTestId('number-of-weeks-field')).toBeInTheDocument();

    userEvent.click(screen.getByTestId('work-day-is-same-length'));
    userEvent.type(
      screen.getByLabelText('INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*'),
      '00'
    );
    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    expect(
      screen.getByText('FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT')
    ).toBeInTheDocument();
  });

  test('should validate contractual earnings for dots and comma', async () => {
    // Note: for all of these tests, a dot ('.') is the decimal seperator
    render(
      <ContextWrapper>
        <NonEstablishedOccupationAndEarnings
          customInitialValues={null}
          onFinish={jest.fn()}
          weekDayNamesTypeDomains={[] as BaseDomain[]}
          dayLength={{ hours: 8, minutes: 0 }}
        />
      </ContextWrapper>
    );

    // extracted so code is easier to scan through
    const labelText = 'INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS';
    const invalidPrecisionText = 'FINEOS_COMMON.VALIDATION.INVALID_PRECISION';
    const notNumericText = 'FINEOS_COMMON.VALIDATION.NOT_NUMERIC';
    const moreThanZeroText = 'FINEOS_COMMON.VALIDATION.MORE_THAN_ZERO';
    const frequecyLabelText = 'INTAKE.OCCUPATION_AND_EARNINGS.FREQUENCY';

    const setInputValue = (value: string) => {
      userEvent.clear(screen.getByLabelText(labelText));
      userEvent.type(screen.getByLabelText(labelText), value);
      fireEvent.blur(screen.getByLabelText(labelText));
      userEvent.click(screen.getByText('INTAKE.PROCEED'));
      // wait();
    };

    // invalid precision - too many characters after the dot
    setInputValue('10.000001');
    await wait();
    expect(screen.getByLabelText(labelText)).not.toBeValid();
    expect(screen.getByText(invalidPrecisionText)).toBeInTheDocument();

    // this is valid - antd InputNumber will remove the comma
    setInputValue('10,000001');
    await wait();
    expect(screen.getByLabelText(labelText)).toBeValid();

    // any text will be replaced with the previous numeric value - however the message appears while in focus
    userEvent.type(screen.getByLabelText(labelText), 'test');
    await wait();
    expect(screen.getByLabelText(labelText)).not.toBeValid();
    expect(screen.getByText(notNumericText)).toBeInTheDocument();

    setInputValue('0');
    await wait();
    expect(screen.getByLabelText(labelText)).not.toBeValid();
    expect(screen.getByText(moreThanZeroText)).toBeInTheDocument();

    setInputValue('100');
    await wait();
    expect(screen.getByLabelText(labelText)).toBeValid();
    expect(screen.getByLabelText(frequecyLabelText)).not.toBeValid();

    // antd InputNumber will strip out the trailing dot or comma
    setInputValue('100.');
    await wait();
    expect(screen.getByLabelText(labelText)).toBeValid();
    expect(screen.getByLabelText(frequecyLabelText)).not.toBeValid();

    // antd InputNumber will strip out any non-decimalSeparator chars (like commas)
    setInputValue('100,');
    await wait();
    expect(screen.getByLabelText(labelText)).toBeValid();
    expect(screen.getByLabelText(frequecyLabelText)).not.toBeValid();

    // antd InputNumber will strip out any non-decimalSeparator chars (like commas)
    setInputValue('100,,11');
    await wait();
    expect(screen.getByLabelText(labelText)).toBeValid();
    expect(screen.getByLabelText(frequecyLabelText)).not.toBeValid();

    setInputValue('100,111');
    await wait();
    expect(screen.getByLabelText(labelText)).toBeValid();
    expect(screen.getByLabelText(frequecyLabelText)).not.toBeValid();

    setInputValue('10.,110,11');
    await wait();
    expect(screen.getByLabelText(labelText)).not.toBeValid();
    expect(screen.getByText(invalidPrecisionText)).toBeInTheDocument();

    setInputValue('11.100,111');
    await wait();
    expect(screen.getByLabelText(labelText)).not.toBeValid();
    expect(screen.getByText(invalidPrecisionText)).toBeInTheDocument();

    setInputValue('11.100,11');
    await wait();
    expect(screen.getByLabelText(labelText)).not.toBeValid();
    expect(screen.getByText(invalidPrecisionText)).toBeInTheDocument();
  });
});
