/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment from 'moment';
import userEvent from '@testing-library/user-event';

import { render, act, wait, screen } from '../../../../common/custom-render';
import {
  ContextWrapper,
  selectTodayDate,
  releaseEventLoop
} from '../../../../common/utils';
import {
  InitialRequest,
  InitialRequestFormValue
} from '../../../../../app/modules/intake/common';

describe('InitialRequest', () => {
  test('shoudl show error for notificationLastWorkingDay', async () => {
    const { rerender } = render(
      <ContextWrapper>
        <InitialRequest
          customInitialValues={{} as InitialRequestFormValue}
          onFinish={jest.fn()}
          jobStartDate={moment().subtract(2, 'day')}
        />
      </ContextWrapper>
    );
    await act(wait);

    await selectTodayDate(
      screen.getByLabelText(
        `INTAKE.INITIAL_REQUEST.NOTIFICATION_LAST_WORKING_DAY*`
      )
    );
    rerender(
      <ContextWrapper>
        <InitialRequest
          customInitialValues={{} as InitialRequestFormValue}
          onFinish={jest.fn()}
          jobStartDate={moment().add(2, 'day')}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByText('INTAKE.FINISH'));
      return releaseEventLoop();
    });

    expect(
      screen.getByTestId(`INTAKE.INITIAL_REQUEST.VALIDATION.LAST_WORKING_DAY`)
    ).toBeInTheDocument();
  });
});
