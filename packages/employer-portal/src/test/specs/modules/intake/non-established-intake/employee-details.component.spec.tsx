/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import * as Yup from 'yup';
import each from 'jest-each';
import userEvent from '@testing-library/user-event';
import { wait, screen } from '@testing-library/dom';

import { ContextWrapper, releaseEventLoop } from '../../../../common/utils';
import { createFormPartialValidationTesting } from '../../../../common/assertion/validation.assertion';
import {
  NonEstablishedEmployeeDetails,
  nonEstablishedEmployeeDetailsValidation,
  initialNonEstablishedEmployeeDetailsFormValue
} from '../../../../../app/modules/intake/non-established-intake/employee-details';
import { render, act, RenderResult } from '../../../../common/custom-render';
import { PortalEnvConfigService } from '../../../../../app/shared';
import portalEnvConfig from '../../../../../../public/config/portal.env.json';

import { fillRequiredEmployeeDetailsFields } from './non-established-intake.utils';

describe('NonEstablishedEmployeeDetails', () => {
  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
  });
  test('should render', async () => {
    render(
      <ContextWrapper>
        <NonEstablishedEmployeeDetails
          customInitialValues={null}
          onFinish={jest.fn()}
        />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.NEW_DESCRIPTION')
    ).toBeInTheDocument();
    expect(
      screen.getByText('COMMON.FIELDS.FIRST_NAME_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('COMMON.FIELDS.LAST_NAME_LABEL')
    ).toBeInTheDocument();
    expect(screen.getByText('COMMON.FIELDS.EMAIL_LABEL')).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_NUMBER.HEADER')
    ).toBeInTheDocument();
    expect(screen.getByText('COMMON.PHONE.COUNTRY_CODE')).toBeInTheDocument();
    expect(screen.getByText('COMMON.PHONE.AREA_CODE')).toBeInTheDocument();
    expect(screen.getByText('COMMON.PHONE.PHONE_NO')).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PRIMARY_ADDRESS.HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('COMMON.ADDRESS.ADDRESS_LINE1')
    ).toBeInTheDocument();
    expect(
      screen.getByText('COMMON.ADDRESS.ADDRESS_LINE2')
    ).toBeInTheDocument();
    expect(
      screen.getByText('COMMON.ADDRESS.ADDRESS_LINE3')
    ).toBeInTheDocument();
    expect(screen.getByText('COMMON.ADDRESS.CITY')).toBeInTheDocument();
    expect(screen.getByText('COMMON.ADDRESS.STATE')).toBeInTheDocument();
    expect(screen.getByText('COMMON.ADDRESS.ZIP_CODE')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.PROCEED')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ABANDON_INTAKE')).toBeInTheDocument();
  });

  each([
    [
      'first-name-field',
      'COMMON.FIELDS.FIRST_NAME_LABEL*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    ['first-name-field', 'COMMON.FIELDS.FIRST_NAME_LABEL*', 'a', ''],
    [
      'last-name-field',
      'COMMON.FIELDS.LAST_NAME_LABEL*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    ['last-name-field', 'COMMON.FIELDS.LAST_NAME_LABEL*', 'a', ''],
    ['email-field', 'COMMON.FIELDS.EMAIL_LABEL (optional)', '', ''],
    [
      'email-field',
      'COMMON.FIELDS.EMAIL_LABEL (optional)',
      'a',
      'FINEOS_COMMON.VALIDATION.NOT_EMAIL'
    ],
    ['email-field', 'COMMON.FIELDS.EMAIL_LABEL (optional)', 'a@c.pl', '']
  ]).test(
    'in %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: { scope: initialNonEstablishedEmployeeDetailsFormValue },
      validationSchema: Yup.object({
        scope: nonEstablishedEmployeeDetailsValidation
      }),
      Component: () => (
        <NonEstablishedEmployeeDetails
          customInitialValues={null}
          onFinish={jest.fn()}
        />
      )
    })
  );
  xtest('should submit form', async () => {
    const mockOnFinish = jest.fn();

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <NonEstablishedEmployeeDetails
          customInitialValues={null}
          onFinish={mockOnFinish}
        />
      </ContextWrapper>
    );

    await wait();

    await act(() => {
      userEvent.click(screen.getByText('INTAKE.PROCEED'));
      return releaseEventLoop();
    });

    expect(screen.getByTestId('first-name-field')).not.toBeValid();

    await fillRequiredEmployeeDetailsFields({
      getByTestId: screen.getByTestId
    } as RenderResult);
    userEvent.click(getSubmitButtonByText('INTAKE.PROCEED'));

    await wait();

    expect(mockOnFinish).toBeCalledWith({
      address: {
        addressLine1: 'addressLine1',
        addressLine2: '',
        addressLine3: '',
        addressLine4: 'city',
        addressLine5: '',
        addressLine6: 'AL',
        addressLine7: '',
        country: { id: '672007', fullId: 672007, name: 'USA' },
        postCode: 'postCode',
        premiseNo: ''
      },
      email: '',
      firstName: 'firstName',
      lastName: 'lastName',
      phoneType: {
        domainId: 51,
        fullId: 1660001,
        id: '1660001',
        name: 'Cell'
      },
      preferredPhoneNumber: {
        areaCode: '00',
        intCode: '',
        telephoneNo: '12345'
      }
    });
  });
});
