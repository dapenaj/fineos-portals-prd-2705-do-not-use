import userEvent from '@testing-library/user-event';

import { wait, within, RenderResult } from '../../../../common/custom-render';
import {
  selectAntOptionByText,
  selectTodayDate
} from '../../../../common/utils';

export const fillRequiredEmployeeDetailsFields = async ({
  getByTestId
}: RenderResult) => {
  userEvent.type(
    within(getByTestId('first-name-field')).getByTestId('text-input'),
    'firstName'
  );

  userEvent.type(
    within(getByTestId('last-name-field')).getByTestId('text-input'),
    'lastName'
  );

  userEvent.type(
    within(getByTestId('phone-area-code-field')).getByTestId('text-input'),
    '00'
  );

  userEvent.type(
    within(getByTestId('phone-phone-no-field')).getByTestId('text-input'),
    '12345'
  );

  userEvent.type(
    within(getByTestId('address-line1-field')).getByTestId('text-input'),
    'addressLine1'
  );

  userEvent.type(
    within(getByTestId('address-city-field')).getByTestId('text-input'),
    'city'
  );

  userEvent.type(
    within(getByTestId('address-post-code-field')).getByTestId('text-input'),
    'postCode'
  );

  await selectAntOptionByText(
    within(getByTestId('address-state-field')).getByTestId('dropdown-input'),
    'AL'
  );
};

export const fillRequiredInitialRequestFields = async ({
  getByLabelText
}: RenderResult) => {
  userEvent.click(getByLabelText('Administration'));

  await selectTodayDate(
    getByLabelText(`INTAKE.INITIAL_REQUEST.NOTIFICATION_LAST_WORKING_DAY*`)
  );

  return wait();
};
