import React from 'react';
import {
  ViewCustomerDataPermissions,
  ViewOccupationPermissions,
  ManageCustomerDataPermissions,
  SkipToContentProvider
} from 'fineos-common';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';
import { createMemoryHistory as createHistory } from 'history';
import { Router } from 'react-router';

import {
  render,
  wait,
  act,
  RenderResult
} from '../../../../common/custom-render';
import { ContextWrapper, releaseEventLoop } from '../../../../common/utils';
import {
  OccupationEarning,
  PortalEnvConfigService
} from '../../../../../app/shared';
import { EstablishedIntake } from '../../../../../app/modules/intake';
import {
  CUSTOMER_INFO_FIXTURE,
  PHONE_NUMBERS_FIXTURE,
  COMMUNICATIONS_FIXTURE
} from '../../../../fixtures/customer.fixture';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../fixtures/customer-occupations.fixture';
import { CONTRACTUAL_EARNINGS_FIXTURE } from '../../../../fixtures/contractual-earnings.fixture';
import { ENUM_INSTANCES_FIXTURE } from '../../../../fixtures/enum-instances.fixture';
import { OUTSTANDING_NOTIFICATIONS_FIXTURE } from '../../../../fixtures/notifications.fixture';
import * as personalDetailsApiModule from '../../../../../app/shared/api/customer.api';
import * as phoneCallsApiModule from '../../../../../app/modules/intake/established-intake/employee-details/phone-calls/phone-calls.api';
import * as employeeDetailsApiModule from '../../../../../app/modules/intake/established-intake/employee-details/employee-details.api';
import * as establishedIntakeApiModule from '../../../../../app/modules/intake/established-intake/established-intake.api';
import * as contractualEarningsApiModule from '../../../../../app/shared/api/contractual-earnings.api';
import * as notificationHookModule from '../../../../../app/modules/intake/notification-reasons.hook';
import { fillRequiredInitialRequestFields } from '../non-established-intake/non-established-intake.utils';
import portalEnvConfig from '../../../../../../public/config/portal.env.json';

jest.mock(
  '../../../../../app/modules/intake/notification-reasons.hook',
  () => ({
    useNotificationReasons: jest.fn()
  })
);

jest.mock(
  '../../../../../app/modules/intake/established-intake/established-intake.api',
  () => ({
    addEstablishedIntake: jest.fn(),
    fetchCustomerOccupationByDate: jest.fn()
  })
);

const fetchNotificationReasons = notificationHookModule.useNotificationReasons as jest.Mock;

const addEstablishedIntake = establishedIntakeApiModule.addEstablishedIntake as jest.Mock;

describe('EstablishedIntake', () => {
  const permissions = [
    ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
    ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
    ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
    ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE,
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES
  ];

  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-05-01').valueOf());

    jest
      .spyOn(personalDetailsApiModule, 'fetchCustomerInfo')
      .mockReturnValue(Promise.resolve(CUSTOMER_INFO_FIXTURE));

    jest
      .spyOn(phoneCallsApiModule, 'fetchCommunicationPreferencePhoneNumbers')
      .mockReturnValue(Promise.resolve(PHONE_NUMBERS_FIXTURE));

    jest
      .spyOn(establishedIntakeApiModule, 'fetchCustomerOccupationByDate')
      .mockReturnValue(
        Promise.resolve(
          (CUSTOMER_OCCUPATIONS_FIXTURE[0] as unknown) as OccupationEarning
        )
      );

    jest
      .spyOn(contractualEarningsApiModule, 'fetchContractualEarnings')
      .mockReturnValue(Promise.resolve(CONTRACTUAL_EARNINGS_FIXTURE[0]));

    jest
      .spyOn(employeeDetailsApiModule, 'linkCustomerCommunicationPreference')
      .mockReturnValue(Promise.resolve(COMMUNICATIONS_FIXTURE[0]));

    jest
      .spyOn(employeeDetailsApiModule, 'linkCustomerCommunicationPreference')
      .mockReturnValue(Promise.resolve(COMMUNICATIONS_FIXTURE[0]));

    fetchNotificationReasons.mockReturnValue({
      isLoadingReasons: false,
      notificationReasons: ENUM_INSTANCES_FIXTURE
    });

    addEstablishedIntake.mockReturnValue(
      Promise.resolve(OUTSTANDING_NOTIFICATIONS_FIXTURE[0])
    );
  });

  test('should render', async () => {
    render(
      <SkipToContentProvider>
        <ContextWrapper permissions={permissions}>
          <EstablishedIntake employeeId="1" />
        </ContextWrapper>
      </SkipToContentProvider>
    );

    await wait();

    expect(screen.getAllByText('INTAKE.EMPLOYEE_DETAILS.HEADER')).toHaveLength(
      2
    );
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.INITIAL_REQUEST.HEADER')
    ).toBeInTheDocument();
    expect(screen.getByText('INTAKE.HELP_NOTE.HEADER')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.HELP_NOTE.BODY')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.HELP_NOTE.PHONE')).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER')
    ).toBeInTheDocument();
    expect(screen.getByText('INTAKE.CONFIRM_AND_PROCEED')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ABANDON_INTAKE')).toBeInTheDocument();
  });

  test('should submit intake', async () => {
    const getUserConfirmation = jest.fn((message, callback) => {
      callback(false);
    });
    const history = createHistory({
      getUserConfirmation
    });

    const { getSubmitButtonByText } = render(
      <SkipToContentProvider>
        <ContextWrapper permissions={permissions}>
          <EstablishedIntake employeeId="1" />
        </ContextWrapper>
      </SkipToContentProvider>
    );

    await wait();

    expect(screen.queryByText('NAVIGATION.BACK')).not.toBeInTheDocument();

    userEvent.click(getSubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED'));

    await wait();

    expect(screen.getByText('NAVIGATION.BACK')).toBeInTheDocument();
    expect(screen.getByText('NAVIGATION.BACK')).toBeEnabled();

    expect(screen.getAllByText('INTAKE.EMPLOYEE_DETAILS.HEADER')).toHaveLength(
      1
    );
    expect(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toHaveLength(2);

    userEvent.click(screen.getByRole('button', { name: /INTAKE.PROCEED/i }));

    await wait();

    expect(screen.getByText('NAVIGATION.BACK')).toBeInTheDocument();
    expect(screen.getByText('NAVIGATION.BACK')).toBeEnabled();

    expect(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toHaveLength(1);
    expect(screen.getAllByText('INTAKE.INITIAL_REQUEST.HEADER')).toHaveLength(
      2
    );

    await act(() => {
      userEvent.click(screen.getByText('INTAKE.FINISH'));
      return releaseEventLoop();
    });

    await fillRequiredInitialRequestFields({
      getByLabelText: screen.getByLabelText
    } as RenderResult);

    userEvent.click(getSubmitButtonByText('INTAKE.FINISH'));

    await wait();

    expect(
      screen.queryByText('INTAKE.EMPLOYEE_DETAILS.HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.INITIAL_REQUEST.HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.HELP_NOTE.HEADER')
    ).not.toBeInTheDocument();
    expect(screen.queryByText('INTAKE.HELP_NOTE.BODY')).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.HELP_NOTE.PHONE')
    ).not.toBeInTheDocument();

    expect(
      screen.getByText('INTAKE.ESTABLISHED.SUCCESS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.ESTABLISHED.SUCCESS.SUB_TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.ESTABLISHED.SUCCESS.NOTIFICATION_REFERENCE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.ESTABLISHED.SUCCESS.NOTIFICATION_REFERENCE_INFO')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.ESTABLISHED.SUCCESS.ADDITIONAL_DETAILS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.ESTABLISHED.SUCCESS.PRIVACY_NOTICE')
    ).toBeInTheDocument();
  });

  test('should handle error response on submit', async () => {
    addEstablishedIntake.mockImplementation(() => Promise.reject(new Error()));

    const { getSubmitButtonByText } = render(
      <SkipToContentProvider>
        <ContextWrapper permissions={permissions}>
          <EstablishedIntake employeeId="1" />
        </ContextWrapper>
      </SkipToContentProvider>
    );

    await wait();

    userEvent.click(getSubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED'));

    await wait();

    userEvent.click(screen.getByRole('button', { name: /INTAKE.PROCEED/i }));

    await fillRequiredInitialRequestFields({
      getByLabelText: screen.getByLabelText
    } as RenderResult);

    expect(screen.queryByText('INTAKE.ERROR.BODY')).not.toBeInTheDocument();
    expect(screen.queryByText('INTAKE.ERROR.PHONE')).not.toBeInTheDocument();
    expect(screen.queryByText('INTAKE.ERROR.TIME')).not.toBeInTheDocument();

    userEvent.click(getSubmitButtonByText('INTAKE.FINISH'));

    await wait();

    expect(screen.getByText('INTAKE.ERROR.BODY')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ERROR.PHONE')).toBeInTheDocument();
    expect(screen.getByText('INTAKE.ERROR.TIME')).toBeInTheDocument();

    expect(
      screen.queryByText('INTAKE.EMPLOYEE_DETAILS.HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.INITIAL_REQUEST.HEADER')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.HELP_NOTE.HEADER')
    ).not.toBeInTheDocument();
    expect(screen.queryByText('INTAKE.HELP_NOTE.BODY')).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.HELP_NOTE.PHONE')
    ).not.toBeInTheDocument();
  });

  test('should block navigation when user try to leave intake during filling', async () => {
    const getUserConfirmation = jest.fn((message, callback) => {
      callback(false);
    });
    const history = createHistory({
      getUserConfirmation
    });

    const { getSubmitButtonByText } = render(
      <SkipToContentProvider>
        <ContextWrapper permissions={permissions}>
          <Router history={history}>
            <EstablishedIntake employeeId="1" />
          </Router>
        </ContextWrapper>
      </SkipToContentProvider>
    );

    await wait();

    expect(screen.queryByText('NAVIGATION.BACK')).not.toBeInTheDocument();

    userEvent.click(getSubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED'));

    await wait();

    expect(screen.getByText('NAVIGATION.BACK')).toBeInTheDocument();
    expect(screen.getByText('NAVIGATION.BACK')).toBeEnabled();

    expect(screen.getAllByText('INTAKE.EMPLOYEE_DETAILS.HEADER')).toHaveLength(
      1
    );
    expect(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toHaveLength(2);

    userEvent.click(screen.getByRole('button', { name: /INTAKE.PROCEED/i }));

    await wait();

    expect(screen.getByText('NAVIGATION.BACK')).toBeInTheDocument();
    expect(screen.getByText('NAVIGATION.BACK')).toBeEnabled();

    expect(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toHaveLength(1);
    expect(screen.getAllByText('INTAKE.INITIAL_REQUEST.HEADER')).toHaveLength(
      2
    );
    expect(screen.getByText('NAVIGATION.MY_DASHBOARD')).toBeInTheDocument();

    userEvent.click(screen.getByText('NAVIGATION.MY_DASHBOARD'));

    expect(getUserConfirmation).toHaveBeenCalled();
  });
});
