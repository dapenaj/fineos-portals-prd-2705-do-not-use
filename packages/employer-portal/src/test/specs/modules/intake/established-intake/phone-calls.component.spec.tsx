/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { Formik } from 'formik';
import { noop as _noop } from 'lodash';
import { ManageCustomerDataPermissions } from 'fineos-common';

import {
  buildTranslationWithParams,
  ContextWrapper,
  mockTranslations
} from '../../../../common/utils';
import { PhoneCalls } from '../../../../../app/modules/intake/established-intake/employee-details/phone-calls';

describe('PhoneCalls', () => {
  const translations = mockTranslations(
    'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER',
    'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.ADD_PHONE_DESCRIPTION',
    'COMMON.PHONE.COUNTRY_CODE',
    'COMMON.PHONE.AREA_CODE',
    'COMMON.PHONE.PHONE_NO',
    'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.EDIT_PREFERENCES',
    buildTranslationWithParams(
      'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.CONTACT_PHONE',
      ['phone']
    )
  );

  test('should render with ExistingPhoneNumbers', async () => {
    const { getByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES
        ]}
      >
        <Formik
          initialValues={{
            phoneNumbers: [
              { intCode: '353', areaCode: '1', telephoneNo: '234567' }
            ],
            preferredPhoneNumberIndex: 0
          }}
          onSubmit={_noop}
        >
          <PhoneCalls hasPhoneNumbers={true} />
        </Formik>
      </ContextWrapper>
    );

    expect(
      getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER')
    ).toBeInTheDocument();
    expect(
      getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.CONTACT_PHONE', {
        exact: false
      })
    ).toHaveTextContent('353-1-234567');
    expect(
      getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.EDIT_PREFERENCES')
    ).toBeInTheDocument();
  });

  test('should render with NewPhoneNumber', () => {
    const { getByText, getByLabelText } = render(
      <ContextWrapper translations={translations}>
        <Formik
          initialValues={{
            phoneNumbers: [
              { intCode: '353', areaCode: '1', telephoneNo: '234567' }
            ],
            preferredPhoneNumberIndex: 0
          }}
          onSubmit={_noop}
        >
          <PhoneCalls hasPhoneNumbers={false} />
        </Formik>
      </ContextWrapper>
    );

    expect(
      getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER')
    ).toBeInTheDocument();
    expect(
      getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.ADD_PHONE_DESCRIPTION')
    ).toBeInTheDocument();

    expect(getByLabelText('COMMON.PHONE.COUNTRY_CODE')).toBeInTheDocument();
    expect(getByLabelText('COMMON.PHONE.AREA_CODE*')).toBeInTheDocument();
    expect(getByLabelText('COMMON.PHONE.PHONE_NO*')).toBeInTheDocument();
  });
});
