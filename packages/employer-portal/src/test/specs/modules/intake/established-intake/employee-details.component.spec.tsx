/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  ConcurrencyUpdateError,
  GroupClientCustomerService
} from 'fineos-js-api-client';
import {
  ViewCustomerDataPermissions,
  ManageCustomerDataPermissions
} from 'fineos-common';
import { GroupClientPhoneNumber } from 'fineos-js-api-client';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';

import { render, wait } from '../../../../common/custom-render';
import {
  buildTranslationWithParams,
  ContextWrapper,
  mockTranslations
} from '../../../../common/utils';
import { EstablishedEmployeeDetails } from '../../../../../app/modules/intake/established-intake/employee-details';
import {
  CUSTOMER_INFO_FIXTURE,
  PHONE_NUMBERS_FIXTURE,
  COMMUNICATIONS_FIXTURE
} from '../../../../fixtures/customer.fixture';
import { PHONES_FIXTURE } from '../../../../fixtures/contact-details.fixture';
import * as personalDetailsApiModule from '../../../../../app/shared/api/customer.api';
import * as phoneCallsApiModule from '../../../../../app/modules/intake/established-intake/employee-details/phone-calls/phone-calls.api';
import * as employeeDetailsApiModule from '../../../../../app/modules/intake/established-intake/employee-details/employee-details.api';

describe('EstablishedEmployeeDetails', () => {
  let addEmployeePhoneNumberSpy: jest.SpyInstance;
  let linkCustomerCommunicationPreferenceSpy: jest.SpyInstance;
  const service = GroupClientCustomerService.getInstance();

  const translations = mockTranslations(
    buildTranslationWithParams(
      'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.CONTACT_PHONE',
      ['phone']
    )
  );

  beforeEach(() => {
    jest.setTimeout(30000);
    jest
      .spyOn(personalDetailsApiModule, 'fetchCustomerInfo')
      .mockReturnValue(Promise.resolve(CUSTOMER_INFO_FIXTURE));

    jest
      .spyOn(phoneCallsApiModule, 'fetchCommunicationPreferencePhoneNumbers')
      .mockReturnValue(Promise.resolve(PHONE_NUMBERS_FIXTURE));

    jest
      .spyOn(phoneCallsApiModule, 'fetchCustomerPhoneNumbers')
      .mockReturnValue(Promise.resolve(PHONE_NUMBERS_FIXTURE));

    addEmployeePhoneNumberSpy = jest
      .spyOn(employeeDetailsApiModule, 'addEmployeePhoneNumber')
      .mockReturnValue(Promise.resolve(PHONES_FIXTURE[0]));

    linkCustomerCommunicationPreferenceSpy = jest
      .spyOn(employeeDetailsApiModule, 'linkCustomerCommunicationPreference')
      .mockReturnValue(Promise.resolve(COMMUNICATIONS_FIXTURE[0]));
  });

  test('should render empty when user has no permissions', () => {
    render(
      <ContextWrapper>
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.HEADER')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.EMPLOYEE_DETAILS.EXISTING_DESCRIPTION')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER')
    ).not.toBeInTheDocument();
  });

  test('should not allow employee information edits without permissions', async () => {
    render(
      <ContextWrapper
        permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.EXISTING_DESCRIPTION')
    ).toBeInTheDocument();
  });

  test('should render only phone numbers panel', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.EXISTING_DESCRIPTION')
    ).toBeInTheDocument();

    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.CONTACT_PHONE', {
        exact: false
      })
    ).toHaveTextContent('353-1-234567');
    expect(
      getSubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED')
    ).toBeInTheDocument();
    expect(getSubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED')).toBeEnabled();

    expect(
      screen.queryByText('INTAKE.EMPLOYEE_DETAILS.FULL_NAME')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('COMMON.ADDRESS.SECTION_HEADER')
    ).not.toBeInTheDocument();
  });

  test('should render phone details fetched from fetchCustomerPhoneNumbers', async () => {
    jest
      .spyOn(phoneCallsApiModule, 'fetchCommunicationPreferencePhoneNumbers')
      .mockReturnValue(Promise.resolve([]));

    jest
      .spyOn(phoneCallsApiModule, 'fetchCustomerPhoneNumbers')
      .mockReturnValue(
        Promise.resolve([
          {
            intCode: '987',
            areaCode: '654',
            telephoneNo: '321'
          }
        ] as GroupClientPhoneNumber[])
      );

    render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.CONTACT_PHONE', {
        exact: false
      })
    ).toHaveTextContent('987-654-321');
  });

  test('should render new phone number form', async () => {
    jest.mock(
      '../../../../../app/shared/enum-domain/enum-instance.api',
      () => ({
        fetchEnumInstances: jest
          .fn()
          .mockImplementation(() => Promise.resolve([]))
      })
    );
    jest
      .spyOn(phoneCallsApiModule, 'fetchCommunicationPreferencePhoneNumbers')
      .mockReturnValue(Promise.resolve([]));

    jest
      .spyOn(phoneCallsApiModule, 'fetchCustomerPhoneNumbers')
      .mockReturnValue(Promise.resolve([]));

    render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText(
        'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.ADD_PHONE_DESCRIPTION'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText('ENUM_DOMAINS.PHONE_TYPE.CELL')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.PHONE.COUNTRY_CODE')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.PHONE.AREA_CODE*')
    ).toBeInTheDocument();
    expect(screen.getByLabelText('COMMON.PHONE.PHONE_NO*')).toBeInTheDocument();
  });

  test('should render new phone number form if customer phone numbers API fails', async () => {
    jest.mock(
      '../../../../../app/shared/enum-domain/enum-instance.api',
      () => ({
        fetchEnumInstances: jest
          .fn()
          .mockImplementation(() => Promise.resolve([]))
      })
    );
    jest
      .spyOn(phoneCallsApiModule, 'fetchCommunicationPreferencePhoneNumbers')
      .mockReturnValue(Promise.resolve([]));

    jest
      .spyOn(phoneCallsApiModule, 'fetchCustomerPhoneNumbers')
      .mockReturnValue(Promise.reject({}));

    render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText(
        'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.ADD_PHONE_DESCRIPTION'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText('ENUM_DOMAINS.PHONE_TYPE.CELL')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.PHONE.COUNTRY_CODE')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('COMMON.PHONE.AREA_CODE*')
    ).toBeInTheDocument();
    expect(screen.getByLabelText('COMMON.PHONE.PHONE_NO*')).toBeInTheDocument();
  });

  test('should change phone number and link it', async () => {
    jest
      .spyOn(phoneCallsApiModule, 'fetchCommunicationPreferencePhoneNumbers')
      .mockReturnValue(
        Promise.resolve([
          PHONE_NUMBERS_FIXTURE[0],
          { ...PHONE_NUMBERS_FIXTURE[0], id: '222' }
        ])
      );

    const onFinishSpy = jest.fn();

    const { getSubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={onFinishSpy} />
      </ContextWrapper>
    );

    await wait();

    const radios = [...screen.getAllByTestId('radio-input')];

    userEvent.click(radios[radios.length - 1]);

    await wait();

    userEvent.click(getSubmitButtonByText('INTAKE.SAVE_AND_PROCEED'));

    await wait();

    expect(addEmployeePhoneNumberSpy).not.toBeCalled();

    expect(linkCustomerCommunicationPreferenceSpy).toBeCalledWith('id-4', {
      id: '222',
      relName: '',
      resource: 'PhoneNumber'
    });
  });

  test('should add a new phone number and link it', async () => {
    const onFinishSpy = jest.fn();

    const { getSubmitButtonByText, querySubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_COMMUNICATIONPREFERENCES_GETSINGLE
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={onFinishSpy} />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.queryByText(
        'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.USE_DIFFERENT_PHONE'
      )
    ).not.toBeInTheDocument();

    userEvent.click(
      screen.getByText('INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.EDIT_PREFERENCES')
    );

    userEvent.click(
      screen.getByText(
        'INTAKE.EMPLOYEE_DETAILS.PHONE_CALLS.USE_DIFFERENT_PHONE'
      )
    );

    expect(
      querySubmitButtonByText('INTAKE.SAVE_AND_PROCEED')
    ).not.toBeInTheDocument();

    const radios = [...screen.getAllByTestId('radio-input')];

    userEvent.click(radios[radios.length - 1]);

    await wait();

    userEvent.click(screen.getByText('INTAKE.SAVE_AND_PROCEED'));

    expect(
      querySubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED')
    ).not.toBeInTheDocument();

    userEvent.type(screen.getByLabelText('COMMON.PHONE.AREA_CODE*'), '1');

    userEvent.type(screen.getByLabelText('COMMON.PHONE.PHONE_NO*'), '4');

    await wait();

    userEvent.click(getSubmitButtonByText('INTAKE.SAVE_AND_PROCEED'));

    await wait();

    expect(addEmployeePhoneNumberSpy).toBeCalledWith('id-4', {
      areaCode: '1',
      contactMethod: {
        domainId: 51,
        fullId: 1660001,
        id: '1660001',
        name: 'Cell'
      },
      intCode: '',
      telephoneNo: '4'
    });

    expect(linkCustomerCommunicationPreferenceSpy).toBeCalledWith('id-4', {
      id: '1',
      relName: '',
      resource: 'PhoneNumber'
    });

    expect(onFinishSpy).toBeCalled();
  });

  test('should call onFinish when no changes were made', async () => {
    const onFinishSpy = jest.fn();

    const { getSubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={onFinishSpy} />
      </ContextWrapper>
    );

    await wait();

    expect(screen.getByText('INTAKE.CONFIRM_AND_PROCEED')).toBeEnabled();

    userEvent.click(getSubmitButtonByText('INTAKE.CONFIRM_AND_PROCEED'));

    await wait();

    expect(onFinishSpy).toBeCalled();
  });

  test('should display data concurrency modal when concurrency error returned', async () => {
    jest
      .spyOn(service, 'editCustomerInfo')
      .mockRejectedValue(new ConcurrencyUpdateError({ test: 'test' }));

    const { getSubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
      'test'
    );
    userEvent.click(getSubmitButtonByText('INTAKE.SAVE_AND_PROCEED'));

    await wait();

    expect(
      screen.getByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
    ).toBeInTheDocument();
    expect(
      screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
    ).toBeInTheDocument();
  });

  test('should not display data concurrency modal when concurrency error not returned', async () => {
    const spy = jest
      .spyOn(service as any, 'editCustomerInfo')
      .mockReturnValue(Promise.resolve({ test: 'test' }));

    const { getSubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
      'test'
    );
    userEvent.click(getSubmitButtonByText('INTAKE.SAVE_AND_PROCEED'));

    await wait();

    expect(spy).toHaveBeenCalled();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
    ).not.toBeInTheDocument();
  });

  test('should display error notification and no concurrency modal when error other than concurrency occurred', async () => {
    const spy = jest
      .spyOn(service, 'editCustomerInfo')
      .mockRejectedValue(new Error('test error'));

    const { getSubmitButtonByText } = render(
      <ContextWrapper
        translations={translations}
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION
        ]}
      >
        <EstablishedEmployeeDetails employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
      'test'
    );
    userEvent.click(getSubmitButtonByText('INTAKE.SAVE_AND_PROCEED'));

    await wait();

    expect(spy).toHaveBeenCalled();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
    ).not.toBeInTheDocument();

    expect(
      screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
    ).toBeInTheDocument();
  });
});
