/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import userEvent from '@testing-library/user-event';
import { fireEvent, wait, screen } from '@testing-library/dom';

import { ContextWrapper } from '../../../../common/utils';
import {
  AddressInput,
  initialAddressInputValue,
  addressInputValidation
} from '../../../../../app/shared/ui/address';
import { render, act } from '../../../../common/custom-render';

describe('Enum Select', () => {
  test('should allow search in Dropdown', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{
            scope: {
              ...initialAddressInputValue,
              addressLine1: 'Address 1',
              country: {
                name: 'Ireland'
              }
            }
          }}
          validationSchema={Yup.object({
            scope: addressInputValidation
          })}
          onSubmit={jest.fn()}
        >
          <AddressInput name="scope" />
        </Formik>
      </ContextWrapper>
    );

    expect(
      screen.getByLabelText('COMMON.ADDRESS.COUNTRY*')
    ).toBeInTheDocument();

    await wait();

    userEvent.click(screen.getByLabelText('COMMON.ADDRESS.COUNTRY*'));

    await wait();

    await act(() => {
      userEvent.type(
        screen.getByLabelText('COMMON.ADDRESS.COUNTRY*'),
        'Australia'
      );
    });

    await wait();

    fireEvent.keyDown(screen.getByLabelText('COMMON.ADDRESS.COUNTRY*'), {
      key: 'Enter',
      code: 'Enter'
    });

    await wait();

    expect(screen.getByLabelText('COMMON.ADDRESS.COUNTRY*')).toHaveValue(
      'Australia'
    );
  });
});
