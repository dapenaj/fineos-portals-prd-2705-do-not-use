/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewOccupationPermissions } from 'fineos-common';

import { ContextWrapper } from '../../../../common/utils';
import { EstablishedOccupationAndEarnings } from '../../../../../app/modules/intake/established-intake/occupation-and-earnings';
import { CONTRACTUAL_EARNINGS_FIXTURE } from '../../../../fixtures/contractual-earnings.fixture';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../fixtures/customer-occupations.fixture';
import { OccupationEarning } from '../../../../../app/shared';
import * as establishedIntakeApiModule from '../../../../../app/modules/intake/established-intake/established-intake.api';
import * as contractualEarningsApiModule from '../../../../../app/shared/api/contractual-earnings.api';

describe('EstablishedOccupationAndEarnings', () => {
  beforeEach(() => {
    jest
      .spyOn(establishedIntakeApiModule, 'fetchCustomerOccupationByDate')
      .mockReturnValue(
        Promise.resolve(
          (CUSTOMER_OCCUPATIONS_FIXTURE[0] as unknown) as OccupationEarning
        )
      );

    jest
      .spyOn(contractualEarningsApiModule, 'fetchContractualEarnings')
      .mockReturnValue(Promise.resolve(CONTRACTUAL_EARNINGS_FIXTURE[0]));
  });

  test('should render empty when user has no permissions', async () => {
    const { queryByText, getByText } = render(
      <ContextWrapper>
        <EstablishedOccupationAndEarnings employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    expect(
      getByText('INTAKE.OCCUPATION_AND_EARNINGS.HEADER')
    ).toBeInTheDocument();

    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.EXISTING_DESCRIPTION')
    ).not.toBeInTheDocument();
    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.JOB_TITLE')
    ).not.toBeInTheDocument();
    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS')
    ).not.toBeInTheDocument();
    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.SALARY')
    ).not.toBeInTheDocument();
    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.DATE_OF_HIRE')
    ).not.toBeInTheDocument();

    expect(getByText('INTAKE.PROCEED')).toBeInTheDocument();
    expect(getByText('INTAKE.ABANDON_INTAKE')).toBeInTheDocument();
  });

  test('should render values', async () => {
    const { getByText } = render(
      <ContextWrapper
        permissions={[
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
        ]}
      >
        <EstablishedOccupationAndEarnings employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(getByText('Master')).toBeInTheDocument();
    expect(
      getByText('INTAKE.OCCUPATION_AND_EARNINGS.SHOW_EARNINGS')
    ).toBeInTheDocument();
    expect(getByText('15-01-2020')).toBeInTheDocument();
  });

  test('should toggle earnings', async () => {
    const { queryByText, getByText } = render(
      <ContextWrapper
        permissions={[
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
        ]}
      >
        <EstablishedOccupationAndEarnings employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.HIDE_EARNINGS')
    ).not.toBeInTheDocument();

    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.SALARY')
    ).not.toBeInTheDocument();

    userEvent.click(getByText('INTAKE.OCCUPATION_AND_EARNINGS.SHOW_EARNINGS'));

    expect(
      getByText('INTAKE.OCCUPATION_AND_EARNINGS.HIDE_EARNINGS')
    ).toBeInTheDocument();

    expect(
      getByText('INTAKE.OCCUPATION_AND_EARNINGS.SALARY')
    ).toBeInTheDocument();

    userEvent.click(getByText('INTAKE.OCCUPATION_AND_EARNINGS.HIDE_EARNINGS'));

    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.HIDE_EARNINGS')
    ).not.toBeInTheDocument();

    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.SALARY')
    ).not.toBeInTheDocument();
  });

  test('should hide earnings when user has no permission', async () => {
    const { queryByText } = render(
      <ContextWrapper
        permissions={[ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS]}
      >
        <EstablishedOccupationAndEarnings employeeId="1" onFinish={jest.fn()} />
      </ContextWrapper>
    );

    await wait();

    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.EARNINGS')
    ).not.toBeInTheDocument();
    expect(
      queryByText('INTAKE.OCCUPATION_AND_EARNINGS.SHOW_EARNINGS')
    ).not.toBeInTheDocument();
  });

  test('should call onFinish', async () => {
    const onFinishSpy = jest.fn();

    const { getByText } = render(
      <ContextWrapper
        permissions={[
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
        ]}
      >
        <EstablishedOccupationAndEarnings
          employeeId="1"
          onFinish={onFinishSpy}
        />
      </ContextWrapper>
    );

    await wait();

    expect(getByText('INTAKE.PROCEED')).toBeEnabled();

    userEvent.click(getByText('INTAKE.PROCEED'));

    await wait();

    expect(onFinishSpy).toBeCalled();
  });
});
