/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';

import * as notificationApiModule from '../../../../app/modules/outstanding-notifications/outstanding-notifications.api';
import { Home } from '../../../../app/modules/home/home.component';
import { Config } from '../../../../app/config';
import { ContextWrapper } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { PortalEnvConfigService } from '../../../../app/shared';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

describe('Home', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchOutstandingNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValueOnce(Promise.resolve(portalEnvConfig));
  });

  test('should render the welcome messages if user has NO access to ViewNotificationPermissions.VIEW_NOTIFICATIONS', async () => {
    render(
      <ContextWrapper permissions={[]}>
        <Home />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('WELCOME_MESSAGE.TITLE')).toBeInTheDocument();
    expect(screen.getByText('WELCOME_MESSAGE.SUBTITLE')).toBeInTheDocument();
  });

  test('should render outstanding notifications if user has permissions and showDashboard=false', async () => {
    render(
      <Config.Provider
        value={{ ...CONFIG_FIXTURE, features: { showDashboard: false } }}
      >
        <ContextWrapper
          permissions={[ViewNotificationPermissions.VIEW_NOTIFICATIONS]}
        >
          <Home />
        </ContextWrapper>
      </Config.Provider>
    );

    await act(wait);

    expect(
      screen.queryByTestId('outstanding-notifications')
    ).toBeInTheDocument();
    expect(
      screen.queryByTitle('PAGE_TITLES.MY_DASHBOARD')
    ).not.toBeInTheDocument();
  });
});
