/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { MessageIcon } from '../../../../app/modules/messages/message-icon';
import { MESSAGES_FIXTURE } from '../../../fixtures/messages.fixture';
import { ContextWrapper } from '../../../common/utils';

describe('MessageIcon', () => {
  const messageSentFromPortal = {
    ...MESSAGES_FIXTURE[0],
    msgOriginatesFromPortal: true
  };
  const messageSentFromCarrier = {
    ...MESSAGES_FIXTURE[0],
    msgOriginatesFromPortal: false
  };

  it('should render MESSAGE.EMPLOYER icon if originated from portal', () => {
    render(
      <ContextWrapper>
        <MessageIcon message={messageSentFromPortal} />
      </ContextWrapper>
    );

    expect(screen.getByLabelText('MESSAGE.EMPLOYER icon')).toBeInTheDocument();
    expect(screen.getByText('MESSAGES.EMPLOYER_MESSAGE')).toBeInTheDocument();
  });

  it('should render MESSAGE.CARRIER icon if not originated from portal', () => {
    render(
      <ContextWrapper>
        <MessageIcon message={messageSentFromCarrier} />
      </ContextWrapper>
    );

    expect(screen.getByLabelText('MESSAGE.CARRIER icon')).toBeInTheDocument();
    expect(screen.getByText('MESSAGES.CARRIER_MESSAGE')).toBeInTheDocument();
  });
});
