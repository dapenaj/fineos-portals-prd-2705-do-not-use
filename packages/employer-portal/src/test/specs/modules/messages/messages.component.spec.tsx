/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait, within } from '@testing-library/react';
import { MessagesPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { ContextWrapper, selectAntOptionByText } from '../../../common/utils';
import { MESSAGES_FIXTURE } from '../../../fixtures/messages.fixture';
import { Messages } from '../../../../app/modules/messages/messages.component';
import * as apiModule from '../../../../app/shared/api/message.api';
import { PortalEnvConfigService } from '../../../../app/shared';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

jest.mock('../../../../app/shared/api/message.api', () => ({
  fetchMessages: jest.fn(),
  fetchUnreadMessagesCount: jest.fn().mockReturnValue(Promise.resolve(0)),
  editMessage: jest.fn()
}));

const fetchMessages = apiModule.fetchMessages as jest.Mock;

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

describe('Messages', () => {
  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
  });

  test('should show something went wrong in case of error', async () => {
    fetchMessages.mockReturnValueOnce(Promise.reject(new Error('error')));

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Messages />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      await screen.findByTestId('something-went-wrong')
    ).toBeInTheDocument();
  });

  test('should show messages if returned', async () => {
    fetchMessages.mockReturnValueOnce(
      Promise.resolve({
        data: [MESSAGES_FIXTURE[0]],
        total: MESSAGES_FIXTURE.length
      })
    );

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Messages />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      within(screen.getByTestId('message-list')).getByText(
        MESSAGES_FIXTURE[0].subject
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('message-list')).getByText(
        MESSAGES_FIXTURE[0].narrative
      )
    ).toBeInTheDocument();
  });

  test('should request messages sorted by sort value', async () => {
    const spy = fetchMessages.mockReturnValue(
      Promise.resolve({
        data: MESSAGES_FIXTURE,
        total: MESSAGES_FIXTURE.length
      })
    );

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Messages />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    await act(wait);

    await selectAntOptionByText(
      screen.getByTestId('message-sort-select'),
      'COMMON.SORT_BY_DATE_SELECT.ASC'
    );
    expect(spy).toHaveBeenLastCalledWith('ALL', 'ASC');

    await selectAntOptionByText(
      screen.getByTestId('message-sort-select'),
      'COMMON.SORT_BY_DATE_SELECT.DESC'
    );
    expect(spy).toHaveBeenLastCalledWith('ALL', 'DESC');
  });

  test('should request messages filtered by filter value', async () => {
    const spy = fetchMessages.mockReturnValue(
      Promise.resolve({
        data: MESSAGES_FIXTURE,
        total: MESSAGES_FIXTURE.length
      })
    );

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Messages />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    await act(wait);

    await selectAntOptionByText(
      screen.getByTestId('message-filter-select'),
      'MESSAGES.FILTER.OPTIONS.UNREAD'
    );
    expect(spy).toHaveBeenLastCalledWith('UNREAD', 'DESC');

    await selectAntOptionByText(
      screen.getByTestId('message-filter-select'),
      'MESSAGES.FILTER.OPTIONS.INCOMING'
    );
    expect(spy).toHaveBeenLastCalledWith('INCOMING', 'DESC');

    await selectAntOptionByText(
      screen.getByTestId('message-filter-select'),
      'MESSAGES.FILTER.OPTIONS.OUTGOING'
    );
    expect(spy).toHaveBeenLastCalledWith('OUTGOING', 'DESC');

    await selectAntOptionByText(
      screen.getByTestId('message-filter-select'),
      'MESSAGES.FILTER.OPTIONS.ALL'
    );
    expect(spy).toHaveBeenLastCalledWith('ALL', 'DESC');
  });

  describe('message selection', () => {
    beforeEach(() => {
      fetchMessages.mockReturnValue(
        Promise.resolve({
          data: MESSAGES_FIXTURE,
          total: MESSAGES_FIXTURE.length
        })
      );
    });

    const hasListAnySelectedMessage = (messageList: HTMLElement[]) =>
      Array.from(messageList).some(message =>
        Array.from(message.classList).some(className =>
          className.startsWith('messageActive')
        )
      );
    const isMessageSelected = (message: HTMLElement) =>
      Array.from(message.classList).some(className =>
        className.startsWith('messageActive')
      );

    test('should initially select message', async () => {
      render(
        <ContextWrapper permissions={messagesPermissions}>
          <Messages />
        </ContextWrapper>
      );

      await act(wait);

      const messages = await screen.findAllByTestId('message-list-item');

      expect(messages.length).toBe(MESSAGES_FIXTURE.length);
      expect(isMessageSelected(messages[0])).toBe(true);
    });

    test('should select message after sorting change', async () => {
      render(
        <ContextWrapper permissions={messagesPermissions}>
          <Messages />
        </ContextWrapper>
      );

      await act(wait);

      await selectAntOptionByText(
        screen.getByTestId('message-sort-select'),
        'COMMON.SORT_BY_DATE_SELECT.ASC'
      );

      expect(
        hasListAnySelectedMessage(
          await screen.findAllByTestId('message-list-item')
        )
      ).toBe(true);
    });

    test('should select first message after filtering change', async () => {
      render(
        <ContextWrapper permissions={messagesPermissions}>
          <Messages />
        </ContextWrapper>
      );

      await act(wait);

      const messages = await screen.findAllByTestId('message-list-item');

      expect(isMessageSelected(messages[0])).toBe(true);

      await selectAntOptionByText(
        screen.getByTestId('message-filter-select'),
        'MESSAGES.FILTER.OPTIONS.OUTGOING'
      );

      expect(isMessageSelected(messages[0])).toBe(true);
    });
  });

  test('should reset message input after changing filter', async () => {
    fetchMessages.mockReturnValue(
      Promise.resolve({
        data: MESSAGES_FIXTURE,
        total: MESSAGES_FIXTURE.length
      })
    );
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Messages />
      </ContextWrapper>
    );

    await act(wait);

    const messageInput = screen.getByPlaceholderText(
      'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
    );

    const text = '12345';
    userEvent.type(messageInput, text);

    await act(wait);

    expect(messageInput.innerHTML).toEqual(text);

    await selectAntOptionByText(
      screen.getByTestId('message-filter-select'),
      'MESSAGES.FILTER.OPTIONS.INCOMING'
    );

    await act(wait);

    expect(
      within(await screen.findByRole('dialog')).queryByText(
        'USER_ACTIONS.LEAVE_PAGE'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(await screen.findByRole('dialog')).queryByText(
        'FINEOS_COMMON.GENERAL.YES'
      )!
    );

    await act(wait);

    expect(
      screen.getByPlaceholderText(
        'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
      ).innerHTML
    ).toEqual('');
  });

  test('should reset message input after changing sort', async () => {
    fetchMessages.mockReturnValue(
      Promise.resolve({
        data: MESSAGES_FIXTURE,
        total: MESSAGES_FIXTURE.length
      })
    );
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Messages />
      </ContextWrapper>
    );

    await act(wait);

    const messageInput = screen.getByPlaceholderText(
      'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
    );

    const text = '12345';
    userEvent.type(messageInput, text);

    await act(wait);

    expect(messageInput.innerHTML).toEqual(text);

    await selectAntOptionByText(
      screen.getByTestId('message-sort-select'),
      'COMMON.SORT_BY_DATE_SELECT.ASC'
    );

    await act(wait);

    expect(
      within(await screen.findByRole('dialog')).queryByText(
        'USER_ACTIONS.LEAVE_PAGE'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(await screen.findByRole('dialog')).queryByText(
        'FINEOS_COMMON.GENERAL.YES'
      )!
    );

    await act(wait);

    expect(
      screen.getByPlaceholderText(
        'NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER'
      ).innerHTML
    ).toEqual('');
  });
});
