/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';
import { Formik } from 'formik';

import { ContextWrapper } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import { OUTSTANDING_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import * as notificationApiModule from '../../../../app/modules/outstanding-notifications/outstanding-notifications.api';
import * as outstandingInformationApiModule from '../../../../app/shared/api/document.api';
import { Config } from '../../../../app/config';
import { OutstandingNotificationActions } from '../../../../app/modules/outstanding-notifications/outstanding-notification-actions';

jest.mock(
  '../../../../app/modules/outstanding-notifications/outstanding-notifications.api',
  () => ({
    fetchOutstandingNotifications: jest.fn()
  })
);

jest.mock('../../../../app/shared/api/document.api.ts', () => ({
  fetchOutstandingInformation: jest.fn()
}));

const fetchOutstandingInformation = outstandingInformationApiModule.fetchOutstandingInformation as jest.Mock;
const fetchOutstandingNotifications = notificationApiModule.fetchOutstandingNotifications as jest.Mock;

describe('OutstandingNotificationsActions', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchOutstandingNotifications')
      .mockReturnValue(new Promise(jest.fn()));
  });

  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {}
  };

  const OutstandingNotificationActionsWrapper = props => (
    <Config.Provider
      value={{
        ...CONFIG_FIXTURE,
        features: { showDashboard: false, helpInfoEnabled: true }
      }}
    >
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
      >
        <Formik {...mockFormikProps}>
          <OutstandingNotificationActions {...props} />
        </Formik>
      </ContextWrapper>
    </Config.Provider>
  );

  test('should render outstanding notifications actions', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    fetchOutstandingInformation.mockReturnValue(
      Promise.resolve([OUTSTANDING_INFORMATION_FIXTURE[1]])
    );
    render(
      <OutstandingNotificationActionsWrapper
        record={OUTSTANDING_NOTIFICATIONS_FIXTURE[0]}
      />
    );

    await act(wait);

    expect(
      screen.getByTestId('outstanding-notification-actions')
    ).toBeInTheDocument();

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.EXPAND_ICON_LABEL')
    ).toBeInTheDocument();
  });

  test('should render outstanding notifications action button with "action for me" icon', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    fetchOutstandingInformation.mockReturnValue(
      Promise.resolve([OUTSTANDING_INFORMATION_FIXTURE[0]])
    );
    render(
      <OutstandingNotificationActionsWrapper
        record={OUTSTANDING_NOTIFICATIONS_FIXTURE[0]}
      />
    );

    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME_ICON')
    ).toBeInTheDocument();
  });

  test('should render outstanding notifications action button without "action for me" icon', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    fetchOutstandingInformation.mockReturnValue(
      Promise.resolve([OUTSTANDING_INFORMATION_FIXTURE[1]])
    );
    render(
      <OutstandingNotificationActionsWrapper
        record={OUTSTANDING_NOTIFICATIONS_FIXTURE[0]}
      />
    );

    await act(wait);

    expect(
      screen.queryByText('OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME_ICON')
    ).not.toBeInTheDocument();
  });

  test('should show "view actions" when row is not expanded', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    fetchOutstandingInformation.mockReturnValue(
      Promise.resolve([OUTSTANDING_INFORMATION_FIXTURE[1]])
    );
    render(
      <OutstandingNotificationActionsWrapper
        record={OUTSTANDING_NOTIFICATIONS_FIXTURE[0]}
        expanded={false}
      />
    );

    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('OUTSTANDING_NOTIFICATIONS.HIDE_ACTIONS')
    ).not.toBeInTheDocument();
  });

  test('should show "hide actions" when row is expanded', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    fetchOutstandingInformation.mockReturnValue(
      Promise.resolve([OUTSTANDING_INFORMATION_FIXTURE[1]])
    );
    render(
      <OutstandingNotificationActionsWrapper
        record={OUTSTANDING_NOTIFICATIONS_FIXTURE[0]}
        expanded={true}
      />
    );

    await act(wait);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.HIDE_ACTIONS')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).not.toBeInTheDocument();
  });

  test('should call onExpand when clicking "View Actions"', async () => {
    const spy = jest.fn();
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    fetchOutstandingInformation.mockReturnValue(
      Promise.resolve([OUTSTANDING_INFORMATION_FIXTURE[1]])
    );
    render(
      <OutstandingNotificationActionsWrapper
        record={OUTSTANDING_NOTIFICATIONS_FIXTURE[0]}
        onExpand={spy}
      />
    );

    await act(wait);

    const viewActionsButton = screen.queryByText(
      'OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS'
    );
    userEvent.click(viewActionsButton);
    expect(spy).toHaveBeenCalled();
  });
});
