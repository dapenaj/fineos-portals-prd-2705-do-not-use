/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, within, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';
import { Formik } from 'formik';

import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import { ContextWrapper } from '../../../common/utils';
import { OUTSTANDING_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { OutstandingNotificationsWidget } from '../../../../app/modules/outstanding-notifications';
import * as outstandingInformationApiModule from '../../../../app/shared/api/document.api';
import * as notificationApiModule from '../../../../app/modules/outstanding-notifications/outstanding-notifications.api';
import { Config } from '../../../../app/config';
import { WidgetProps } from '../../../../app/modules/my-dashboard/dashboard.type';

jest.mock(
  '../../../../app/modules/outstanding-notifications/outstanding-notifications.api',
  () => ({
    fetchOutstandingNotifications: jest.fn()
  })
);

jest.mock('../../../../app/shared/api/document.api.ts', () => ({
  fetchOutstandingInformation: jest.fn()
}));

const fetchOutstandingInformation = outstandingInformationApiModule.fetchOutstandingInformation as jest.Mock;
const fetchOutstandingNotifications = notificationApiModule.fetchOutstandingNotifications as jest.Mock;

describe('OutstandingNotifications', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchOutstandingNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    jest
      .spyOn(outstandingInformationApiModule, 'fetchOutstandingInformation')
      .mockReturnValue(Promise.resolve(OUTSTANDING_INFORMATION_FIXTURE));
  });

  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {}
  };

  const openCustomerFilter = async () => {
    userEvent.click(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL'
      )
    );
    await act(wait);
  };

  const openNotificationFilter = async () => {
    userEvent.click(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_FILTER_ICON_LABEL'
      )
    );
    await act(wait);
  };

  const OutstandingNotifications = ({ helpInfoEnabled }: WidgetProps) => (
    <Config.Provider value={CONFIG_FIXTURE}>
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
      >
        <Formik {...mockFormikProps}>
          <OutstandingNotificationsWidget helpInfoEnabled={helpInfoEnabled} />
        </Formik>
      </ContextWrapper>
    </Config.Provider>
  );

  test('should render outstanding notifications widget', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: OUTSTANDING_NOTIFICATIONS_FIXTURE.length
      })
    );

    render(<OutstandingNotifications helpInfoEnabled={true} />);

    expect(screen.getByTestId('spinner')).toBeInTheDocument();
    await act(wait);
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      screen.getByTestId('outstanding-notifications-widget')
    ).toBeInTheDocument();

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.WIDGET_TITLE')
    ).toBeInTheDocument();
  });

  test('should render outstanding notifications widget empty state', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: [],
        total: [].length
      })
    );

    render(<OutstandingNotifications helpInfoEnabled={true} />);

    expect(screen.getByTestId('spinner')).toBeInTheDocument();
    await act(wait);
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      screen.getByTestId('outstanding-notifications-widget')
    ).toBeInTheDocument();

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.EMPTY_STATE')
    ).toBeInTheDocument();
  });

  test('should show something went wrong in case of error', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.reject(new Error('error'))
    );

    render(<OutstandingNotifications helpInfoEnabled={true} />);

    expect(screen.getByTestId('spinner')).toBeInTheDocument();
    await act(wait);
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      await screen.findByTestId('something-went-wrong')
    ).toBeInTheDocument();
  });

  test('should render drawer with outstanding notifications', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(screen.getByTestId('outstanding-notifications')).toBeInTheDocument();
    expect(screen.getByTestId('table')).toBeInTheDocument();
    expect(
      screen.getAllByTestId('outstanding-notification-actions')
    ).toHaveLength(10);

    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);
  });

  test('render with actions', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    const actions = screen.getAllByTestId('outstanding-notification-actions');
    expect(actions).toHaveLength(10);

    userEvent.click(actions[0]);
    await act(wait);

    expect(
      screen.getByTestId('outstanding-notification-expanded-row')
    ).toBeInTheDocument();
  });

  test('should expand outstanding notifications with actions', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    const actions = screen.getAllByTestId('outstanding-notification-actions');
    expect(actions).toHaveLength(10);

    userEvent.click(actions[0]);
    await act(wait);

    expect(
      screen.getByTestId('outstanding-notification-expanded-row')
    ).toBeInTheDocument();

    expect(fetchOutstandingInformation).toHaveBeenCalled();

    expect(
      screen.getByTestId('outstanding-information-list')
    ).toBeInTheDocument();

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.DOWNLOAD_INFO')
    ).toBeInTheDocument();

    expect(
      screen.getAllByTestId('upload-outstanding-information-item')
    ).toHaveLength(2);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.NOTIFICATIONS_INFO')
    ).toBeInTheDocument();

    userEvent.click(actions[0]);
    await act(wait);

    expect(fetchOutstandingInformation).toHaveBeenCalled();

    OUTSTANDING_INFORMATION_FIXTURE.forEach(el => {
      expect(screen.getByText(el.informationType)).toBeInTheDocument();
    });

    expect(
      screen.getAllByTestId('upload-outstanding-information-item')
    ).toHaveLength(2);

    expect(
      screen.getByTestId('upload-outstanding-information-icon')
    ).toBeInTheDocument();

    expect(
      screen.getByTestId('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED')
    ).toBeInTheDocument();
  });

  test('should allow filtering of results by caseNumber', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render table
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(screen.getByTestId('outstanding-notifications')).toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();

    // open the "Notification" filter
    await openNotificationFilter();
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      ),
      'NTN-2'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(5);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();

    // clear the filter via filter controls
    await openNotificationFilter();
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByText(
        'FINEOS_COMMON.GENERAL.RESET'
      )
    );
    await act(wait);

    // should see table, with orig results
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
  });

  test('should have a min limit on filtering by notification id', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    await openNotificationFilter();
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      )
    ).toBeInTheDocument();
  });

  test('should allow filtering of results by employee name', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render table
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(screen.getByTestId('outstanding-notifications')).toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();

    // open the "Employee" filter
    await openCustomerFilter();
    expect(screen.getByTestId('filter-customer')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerFirstName'
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerLastName'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.VALIDATION.NAME_REQUIRED')
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerFirstName'
      ),
      'Sam'
    );
    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'Neil'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();

    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );

    // clear the filter via filter controls
    await openCustomerFilter();
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('reset-button')
    );
    await act(wait);

    // should see table, with orig results
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
  });

  test('should have a min limit on filtering by employee name', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render table
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(await screen.findByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    await openCustomerFilter();
    expect(screen.getByTestId('filter-customer')).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerFirstName'
      ),
      'Sam'
    );
    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'N'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.getByText(
        'OUTSTANDING_NOTIFICATIONS.VALIDATION.MIN_TEXTAREA_LENGTH'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'Nev'
    );

    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );

    expect(
      await screen.findByText(
        'OUTSTANDING_NOTIFICATIONS.VALIDATION.MIN_TEXTAREA_LENGTH'
      )
    ).not.toBeInTheDocument();
  });

  test('should allow clearing filters from the header', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render table
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();

    // open the "Employee" filter and set a filter value
    userEvent.click(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL'
      )
    );
    await act(wait);

    const filter = within(screen.getByTestId('filter-customer'));
    userEvent.type(filter.getByTestId('input-customerFirstName'), 'Sam');
    userEvent.type(filter.getByTestId('input-customerLastName'), 'Neil');
    userEvent.click(filter.getByTestId('search-button'));
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getAllByTestId('filter-items-count-tag')[1]).getByLabelText(
        'WIDGET.FILTER_COUNT_TAG_CLOSE_ICON_LABEL'
      )
    );
    await act(wait);

    // should see table, with orig results
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
  });

  test('should redirect to employee outstanding notifications page after clicking on employee name', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    const { customer } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];

    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(
      (screen.getAllByRole('link', {
        name: /obi wan kenobi/i
      })[0] as HTMLAnchorElement).href
    ).toBe(`${window.location}profile/${customer.id}`);
  });

  test('should render help info icon for outstanding notifications widget if helpInfoEnabled is on', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);

    expect(
      within(screen.getByTestId('outstanding-notifications-widget')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for outstanding notifications widget if helpInfoEnabled is off', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={false} />);

    await act(wait);

    expect(
      within(
        screen.getByTestId('outstanding-notifications-widget')
      ).queryByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON')
    ).not.toBeInTheDocument();
  });

  test('should render help info content for outstanding notifications widget', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);

    const icon = within(
      screen.getByTestId('outstanding-notifications-widget')
    ).getByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON');

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      within(screen.getByTestId('outstanding-notifications-widget')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();

    expect(
      screen.getByTestId('HELP_INFO_DASHBOARDS.OUTSTANDING_NOTIFICATIONS_TITLE')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.MAIN_TITLE')
    ).toBeInTheDocument();
  });

  test('should render help info icon for outstanding notifications drawer if helpInfoEnabled is on', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).toBeInTheDocument();
  });

  test('should not render help info icon for outstanding notifications drawer if helpInfoEnabled is off', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={false} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    expect(
      within(screen.getByTestId('outstanding-notifications')).queryByText(
        'HELP_INFO_DASHBOARDS.HELP_INFO_ICON'
      )
    ).not.toBeInTheDocument();
  });

  test('should render help info content for outstanding notifications drawer', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotifications helpInfoEnabled={true} />);

    await act(wait);
    userEvent.click(screen.getByText('WIDGET.VIEW_DETAILS'));
    await act(wait);

    const icon = within(
      screen.getByTestId('outstanding-notifications')
    ).getByText('HELP_INFO_DASHBOARDS.HELP_INFO_ICON');

    expect(icon).toBeInTheDocument();

    userEvent.click(icon.parentElement!);

    await act(wait);

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.OUTSTANDING_NOTIFICATIONS_TITLE')
    ).toBeInTheDocument();

    expect(
      screen.getByText('HELP_INFO_DASHBOARDS.MAIN_TITLE')
    ).toBeInTheDocument();
  });
});
