/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  render,
  act,
  within,
  wait,
  screen,
  waitForElementToBeRemoved,
  fireEvent
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';
import { Formik } from 'formik';

import { ContextWrapper } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import { OUTSTANDING_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import { OutstandingNotifications } from '../../../../app/modules/outstanding-notifications';
import * as notificationApiModule from '../../../../app/modules/outstanding-notifications/outstanding-notifications.api';
import * as outstandingInformationApiModule from '../../../../app/shared/api/document.api';
import { Config } from '../../../../app/config';

jest.mock(
  '../../../../app/modules/outstanding-notifications/outstanding-notifications.api',
  () => ({
    fetchOutstandingNotifications: jest.fn()
  })
);

jest.mock('../../../../app/shared/api/document.api.ts', () => ({
  fetchOutstandingInformation: jest.fn()
}));

const fetchOutstandingInformation = outstandingInformationApiModule.fetchOutstandingInformation as jest.Mock;
const fetchOutstandingNotifications = notificationApiModule.fetchOutstandingNotifications as jest.Mock;

describe.skip('OutstandingNotifications', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchOutstandingNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    jest
      .spyOn(outstandingInformationApiModule, 'fetchOutstandingInformation')
      .mockReturnValue(Promise.resolve(OUTSTANDING_INFORMATION_FIXTURE));
  });

  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {}
  };

  const openCustomerFilter = async () => {
    userEvent.click(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL'
      )
    );
    await act(wait);
  };

  const openNotificationFilter = async () => {
    userEvent.click(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_FILTER_ICON_LABEL'
      )
    );
    await act(wait);
  };

  const OutstandingNotificationsWrapper = () => (
    <Config.Provider
      value={{
        ...CONFIG_FIXTURE,
        features: { showDashboard: false, helpInfoEnabled: true }
      }}
    >
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
      >
        <Formik {...mockFormikProps}>
          <OutstandingNotifications />
        </Formik>
      </ContextWrapper>
    </Config.Provider>
  );

  test('should show something went wrong in case of error', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.reject(new Error('error'))
    );
    render(<OutstandingNotificationsWrapper />);

    expect(screen.getByTestId('spinner')).toBeInTheDocument();
    await act(wait);
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      await screen.findByTestId('something-went-wrong')
    ).toBeInTheDocument();
  });

  test('should render outstanding notifications', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    expect(screen.getByTestId('outstanding-notifications')).toBeInTheDocument();
    expect(screen.getByTestId('table')).toBeInTheDocument();
    expect(
      screen.getAllByTestId('outstanding-notification-actions')
    ).toHaveLength(10);

    const rows = document.querySelectorAll('tbody tr');
    expect(rows).toHaveLength(10);
  });

  test('should render outstanding notifications header', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotificationsWrapper />);

    await act(wait);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
  });

  test('should render outstanding notifications with actions', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    const actions = screen.getAllByTestId('outstanding-notification-actions');
    expect(actions).toHaveLength(10);

    userEvent.click(actions[0]);
    await act(wait);

    expect(
      screen.getByTestId('outstanding-notification-expanded-row')
    ).toBeInTheDocument();

    expect(fetchOutstandingInformation).toHaveBeenCalled();
  });

  test('should expand outstanding notifications with actions', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );

    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    const actions = await screen.findAllByTestId(
      'outstanding-notification-actions'
    );
    expect(actions).toHaveLength(10);

    userEvent.click(actions[0]);
    await act(wait);

    expect(
      screen.getByTestId('outstanding-notification-expanded-row')
    ).toBeInTheDocument();

    expect(fetchOutstandingInformation).toHaveBeenCalled();

    expect(
      screen.getByTestId('outstanding-information-list')
    ).toBeInTheDocument();

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.NOTIFICATIONS_INFO')
    ).toBeInTheDocument();

    userEvent.click(actions[0]);
    await act(wait);

    expect(fetchOutstandingInformation).toHaveBeenCalled();

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.DOWNLOAD_INFO')
    ).toBeInTheDocument();

    expect(
      screen.getAllByTestId('upload-outstanding-information-item')
    ).toHaveLength(2);

    OUTSTANDING_INFORMATION_FIXTURE.forEach(el => {
      expect(screen.getByText(el.informationType)).toBeInTheDocument();
    });

    expect(
      screen.getAllByTestId('upload-outstanding-information-item')
    ).toHaveLength(2);

    expect(
      screen.getByTestId('upload-outstanding-information-icon')
    ).toBeInTheDocument();

    expect(
      screen.getByTestId('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED')
    ).toBeInTheDocument();
  });

  test('should allow filtering of results by caseNumber', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render initial table
    render(<OutstandingNotificationsWrapper />);

    await act(wait);
    expect(screen.getByTestId('outstanding-notifications')).toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();

    // open the "Notification" filter
    await openNotificationFilter();
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      ),
      'NTN-2'
    );
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByTestId(
        'search-button'
      )
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(5);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();

    // clear the filter via filter controls
    await openNotificationFilter();
    userEvent.click(
      within(screen.getByTestId('table-input-filter')).getByText(
        'FINEOS_COMMON.GENERAL.RESET'
      )
    );
    await act(wait);

    // should see table, with orig results
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
  });

  test('should have a min limit on filtering by notification id', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    await openNotificationFilter();
    expect(screen.getByTestId('table-input-filter')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('table-input-filter')).getByPlaceholderText(
        'NOTIFICATIONS.FILTERS.NOTIFICATION_ID_PLACEHOLDER'
      )
    ).toBeInTheDocument();
  });

  test('should allow filtering of results by employee name', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render initial table
    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    expect(screen.getByTestId('outstanding-notifications')).toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();

    // open the "Employee" filter
    await openCustomerFilter();
    expect(screen.getByTestId('filter-customer')).toBeInTheDocument();

    // set a filter value
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerFirstName'
      )
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('filter-customer')).queryByTestId(
        'input-customerLastName'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.VALIDATION.NAME_REQUIRED')
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerFirstName'
      ),
      'Sam'
    );
    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'Neil'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();

    // clear the filter via filter controls
    await openCustomerFilter();
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('reset-button')
    );
    await act(wait);

    // should see table, with orig results
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
  });

  test('should have a min limit on filtering by employee name', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render table
    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    await openCustomerFilter();
    expect(screen.getByTestId('filter-customer')).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerFirstName'
      ),
      'Sam'
    );
    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'N'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.getByText(
        'OUTSTANDING_NOTIFICATIONS.VALIDATION.MIN_TEXTAREA_LENGTH'
      )
    ).toBeInTheDocument();

    userEvent.type(
      within(screen.getByTestId('filter-customer')).getByTestId(
        'input-customerLastName'
      ),
      'Nev'
    );
    userEvent.click(
      within(screen.getByTestId('filter-customer')).getByTestId('search-button')
    );
    await act(wait);

    expect(
      screen.queryByText(
        'OUTSTANDING_NOTIFICATIONS.VALIDATION.MIN_TEXTAREA_LENGTH'
      )
    ).not.toBeInTheDocument();
  });

  test('should allow clearing filters from the header', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    // render initial table
    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);

    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();

    // open the "Employee" filter and set a filter value
    userEvent.click(
      within(screen.getByTestId('outstanding-notifications')).getByText(
        'NOTIFICATIONS.FILTERS.CUSTOMER_FILTER_ICON_LABEL'
      )
    );
    await act(wait);

    const filter = within(screen.getByTestId('filter-customer'));
    userEvent.type(filter.getByTestId('input-customerFirstName'), 'Sam');
    userEvent.type(filter.getByTestId('input-customerLastName'), 'Neil');
    userEvent.click(filter.getByTestId('search-button'));
    await act(wait);

    // results should be filtered
    expect(document.querySelectorAll('tbody tr')).toHaveLength(2);
    expect(
      await screen.findByText(
        'FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT'
      )
    ).toBeInTheDocument();

    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );

    userEvent.click(
      within(screen.getAllByTestId('filter-items-count-tag')[1]).getByLabelText(
        'WIDGET.FILTER_COUNT_TAG_CLOSE_ICON_LABEL'
      )
    );
    await act(wait);

    // should see table, with orig results
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('FINEOS_COMMON.FILTER_ITEMS_COUNT_TAG.SHOWING_COUNT')
    ).not.toBeInTheDocument();
    expect(document.querySelectorAll('tbody tr')).toHaveLength(10);
  });

  test('should redirect to employee outstanding notifications page after clicking on employee name', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );

    const { customer } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];

    render(<OutstandingNotificationsWrapper />);

    await act(wait);

    expect(
      (screen.getAllByRole('link', {
        name: /obi wan kenobi/i
      })[0] as HTMLAnchorElement).href
    ).toBe(`${window.location}profile/${customer.id}`);
  });

  it('should display a dropdown with for me options', async () => {
    fetchOutstandingNotifications.mockResolvedValue({
      data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
      total: 25
    });

    render(<OutstandingNotificationsWrapper />);

    await waitForElementToBeRemoved(() => screen.getByTestId('spinner'));

    expect(
      screen.queryByTestId('outstanding-notification-filter-spinner')
    ).not.toBeInTheDocument();

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.ACTIONS_FILTER.ALL')
    ).toBeInTheDocument();

    fetchOutstandingNotifications.mockReset();
  });

  it('should display 10 items by default with ALL for me filter', async () => {
    fetchOutstandingNotifications.mockResolvedValue({
      data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
      total: 25
    });

    render(<OutstandingNotificationsWrapper />);

    await waitForElementToBeRemoved(() => screen.getByTestId('spinner'));

    expect(
      screen.queryAllByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).toHaveLength(10);

    fetchOutstandingNotifications.mockReset();
  });

  it('should display only FOR ME items', async () => {
    fetchOutstandingNotifications.mockResolvedValue({
      data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
      total: 25
    });

    render(<OutstandingNotificationsWrapper />);

    await waitForElementToBeRemoved(() => screen.getByTestId('spinner'));

    // Initially there are 10 elements on screen
    expect(
      screen.queryAllByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).toHaveLength(10);

    fireEvent.mouseDown(
      screen.getByTestId('outstanding-notification-filter-selector')
        .firstElementChild as Element
    );
    fireEvent.click(
      screen.getByTestId('OUTSTANDING_NOTIFICATIONS.ACTIONS_FILTER.FOR_ME')
    );

    // As many ForMe icon as For me notifications + 1 (because the table title with for me icon)
    // After selecting FOR ME there are no elements, because the mock data has any for me notification
    expect(
      await screen.findAllByText(
        'OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME_ICON'
      )
    ).toHaveLength(1);
    expect(
      screen.queryAllByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).toHaveLength(0);

    fetchOutstandingNotifications.mockReset();
  });

  it('should display only FOR OTHERS items', async () => {
    fetchOutstandingNotifications.mockResolvedValue({
      data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
      total: 25
    });

    render(<OutstandingNotificationsWrapper />);

    await waitForElementToBeRemoved(() => screen.getByTestId('spinner'));

    // Initially there are 10 elements on screen
    expect(
      screen.queryAllByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).toHaveLength(10);

    fireEvent.mouseDown(
      screen.getByTestId('outstanding-notification-filter-selector')
        .firstElementChild as Element
    );
    fireEvent.click(
      screen.getByTestId('OUTSTANDING_NOTIFICATIONS.ACTIONS_FILTER.FOR_OTHERS')
    );

    // As many ForMe icon as For me notifications + 1 (because the table title with for me icon)
    // There should be all for others because the mock data hasn't any for me
    expect(
      await screen.findAllByText(
        'OUTSTANDING_NOTIFICATIONS.ACTIONS_FOR_ME_ICON'
      )
    ).toHaveLength(11);
    expect(
      screen.queryAllByText('OUTSTANDING_NOTIFICATIONS.VIEW_ACTIONS')
    ).toHaveLength(10);

    fetchOutstandingNotifications.mockReset();
  });
});
