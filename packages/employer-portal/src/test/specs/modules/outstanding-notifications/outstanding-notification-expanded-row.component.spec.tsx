/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, wait, screen } from '@testing-library/react';
import {
  ViewNotificationPermissions,
  ManageNotificationPermissions
} from 'fineos-common';
import { Formik } from 'formik';

import {
  ContextWrapper,
  resolveTranslationWithParams
} from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import { Config } from '../../../../app/config';
import { OutstandingNotificationExpandedRow } from '../../../../app/modules/outstanding-notifications/outstanding-notification-expanded-row';

describe('OutstandingNotificationExpandedRow', () => {
  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {}
  };

  const OutstandingNotificationExpandedRowWrapper = props => (
    <Config.Provider
      value={{
        ...CONFIG_FIXTURE,
        features: { showDashboard: false, helpInfoEnabled: true }
      }}
    >
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST,
          ManageNotificationPermissions.UPLOAD_CASE_BASE64_DOCUMENT
        ]}
      >
        <Formik {...mockFormikProps}>
          <OutstandingNotificationExpandedRow {...props} />
        </Formik>
      </ContextWrapper>
    </Config.Provider>
  );

  test('should render outstanding notification expanded row', async () => {
    render(<OutstandingNotificationExpandedRowWrapper />);

    await act(wait);

    expect(
      screen.getByTestId('outstanding-notification-expanded-row')
    ).toBeInTheDocument();

    expect(
      resolveTranslationWithParams(
        'OUTSTANDING_NOTIFICATIONS.NOTIFICATIONS_INFO',
        [['value', OUTSTANDING_INFORMATION_FIXTURE.length]]
      )
    );

    expect(
      screen.getByTestId('outstanding-information-list')
    ).toBeInTheDocument();
  });
});
