/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, wait, screen } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';
import { Formik } from 'formik';

import { ContextWrapper } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import { OUTSTANDING_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import * as notificationApiModule from '../../../../app/modules/outstanding-notifications/outstanding-notifications.api';
import * as outstandingInformationApiModule from '../../../../app/shared/api/document.api';
import { Config } from '../../../../app/config';
import { OutstandingNotificationsHeader } from '../../../../app/modules/outstanding-notifications/outstanding-notifications-header';

jest.mock(
  '../../../../app/modules/outstanding-notifications/outstanding-notifications.api',
  () => ({
    fetchOutstandingNotifications: jest.fn()
  })
);

const fetchOutstandingNotifications = notificationApiModule.fetchOutstandingNotifications as jest.Mock;

describe('OutstandingNotificationsHeader', () => {
  beforeEach(() => {
    jest
      .spyOn(notificationApiModule, 'fetchOutstandingNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    jest
      .spyOn(outstandingInformationApiModule, 'fetchOutstandingInformation')
      .mockReturnValue(Promise.resolve(OUTSTANDING_INFORMATION_FIXTURE));
  });

  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {}
  };

  const OutstandingNotificationsHeaderWrapper = props => (
    <Config.Provider
      value={{
        ...CONFIG_FIXTURE,
        features: { showDashboard: false, helpInfoEnabled: true }
      }}
    >
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
      >
        <Formik {...mockFormikProps}>
          <OutstandingNotificationsHeader {...props} />
        </Formik>
      </ContextWrapper>
    </Config.Provider>
  );

  test('should render outstanding notifications header', async () => {
    const count = 1;
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(<OutstandingNotificationsHeaderWrapper count={count} />);

    await act(wait);

    expect(screen.getByTestId('header-count')).toBeInTheDocument();
    expect(screen.getByTestId('header-count')).toHaveTextContent(
      count.toString()
    );
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
  });

  test('should render outstanding notifications header with explanatory text', async () => {
    fetchOutstandingNotifications.mockReturnValueOnce(
      Promise.resolve({
        data: OUTSTANDING_NOTIFICATIONS_FIXTURE,
        total: 25
      })
    );
    render(
      <OutstandingNotificationsHeaderWrapper
        count={1}
        showExplanatoryText={true}
      />
    );

    await act(wait);

    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('OUTSTANDING_NOTIFICATIONS.WIDGET_MESSAGE')
    ).toBeInTheDocument();
  });
});
