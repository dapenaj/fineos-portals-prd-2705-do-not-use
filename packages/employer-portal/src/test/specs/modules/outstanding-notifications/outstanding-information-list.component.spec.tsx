/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, wait, screen } from '@testing-library/react';
import {
  ViewNotificationPermissions,
  ManageNotificationPermissions
} from 'fineos-common';
import { Formik } from 'formik';

import { render } from '../../../common/custom-render';
import { ContextWrapper } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { OUTSTANDING_INFORMATION_FIXTURE } from '../../../fixtures/outstanding-information.fixture';
import { OUTSTANDING_NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import { Config } from '../../../../app/config';
import { OutstandingInformationList } from '../../../../app/shared';

describe('OutstandingInformationList', () => {
  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {}
  };

  const OutstandingInformationListWrapper = props => (
    <Config.Provider
      value={{
        ...CONFIG_FIXTURE,
        features: { showDashboard: false, helpInfoEnabled: true }
      }}
    >
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST,
          ManageNotificationPermissions.UPLOAD_CASE_BASE64_DOCUMENT,
          ManageNotificationPermissions.ADD_CASES_OUTSTANDING_INFORMATION_RECEIVED,
          ManageNotificationPermissions.MARK_READ_CASE_DOCUMENT
        ]}
      >
        <Formik {...mockFormikProps}>
          <OutstandingInformationList {...props} />
        </Formik>
      </ContextWrapper>
    </Config.Provider>
  );

  test('should render outstanding information list', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={OUTSTANDING_INFORMATION_FIXTURE}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.DOWNLOAD_INFO')
    ).toBeInTheDocument();

    expect(
      screen.getAllByTestId('upload-outstanding-information-item')
    ).toHaveLength(2);
  });

  test('should render outstanding information list with actions for me', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={[OUTSTANDING_INFORMATION_FIXTURE[0]]}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );

    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.ICON_LABEL.LIST_ICON_LABEL_WITH_ACTIONS')
    ).toBeInTheDocument();
  });

  test('should render outstanding information list without actions for me', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[2];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={[OUTSTANDING_INFORMATION_FIXTURE[0]]}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );
    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.ICON_LABEL.LIST_ICON_LABEL')
    ).toBeInTheDocument();
  });

  test('should render outstanding information list with affirmative icon', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={[OUTSTANDING_INFORMATION_FIXTURE[1]]}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );

    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.ICON_LABEL.AFFIRMATIVE_ICON_LABEL')
    ).toBeInTheDocument();
  });

  test('should render upload document button if user has permission to upload', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={OUTSTANDING_INFORMATION_FIXTURE}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );

    await act(wait);

    expect(screen.getByTestId('upload-document-link')).toBeInTheDocument();
  });

  test('should render upload document button with uploaded status', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={OUTSTANDING_INFORMATION_FIXTURE}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED')
    ).toBeInTheDocument();
  });

  test('should render upload document button with not uploaded status', async () => {
    const spy = jest.fn();
    const { customer, id } = OUTSTANDING_NOTIFICATIONS_FIXTURE[0];
    render(
      <OutstandingInformationListWrapper
        outstandingInfos={[OUTSTANDING_INFORMATION_FIXTURE[0]]}
        customer={customer}
        caseId={id}
        refreshOutstandingInfo={spy}
      />
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD')
    ).toBeInTheDocument();
  });
});
