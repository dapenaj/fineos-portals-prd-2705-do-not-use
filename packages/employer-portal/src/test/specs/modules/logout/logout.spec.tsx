/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Route, Switch } from 'react-router-dom';

import { ContextWrapper, releaseEventLoop } from '../../../common/utils';
import { LogoutAction, LogoutPage } from '../../../../app/modules/logout';
import * as logoutApi from '../../../../app/shared/api/logout.api';

describe('Logout', () => {
  beforeEach(() => {
    jest.spyOn(logoutApi, 'logout');
  });

  test('render logout modal', async () => {
    (logoutApi.logout as jest.Mock).mockReturnValueOnce(Promise.resolve([]));
    render(
      <ContextWrapper>
        <Switch>
          <Route path="/logout" exact={true} component={LogoutPage} />

          <Route exact={true} path="/" render={() => <LogoutAction />} />
        </Switch>
      </ContextWrapper>
    );
    expect(screen.getByText('USER_ACTIONS.LOGOUT')).toBeInTheDocument();
    await act(async () => {
      userEvent.click(screen.getByText('USER_ACTIONS.LOGOUT'));
      await releaseEventLoop();
    });
    expect(
      screen.getByText('COMMON.SESSION.TAKE_ME_LOGIN')
    ).toBeInTheDocument();
    expect(screen.getByText('LOGOUT.LOGGED_BODY')).toBeInTheDocument();
    expect(screen.getByText('LOGOUT.USER_LOGGED_OUT')).toBeInTheDocument();
  });
});
