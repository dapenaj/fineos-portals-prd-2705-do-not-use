/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen, fireEvent, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  ContextWrapper,
  selectAntOptionByText
} from '../../../../common/utils';
import { LeavePeriodChange } from '../../../../../app/modules/notification/leave-period-change/leave-period-change.component';
import * as leavePeriodChangeApi from '../../../../../app/modules/notification/leave-period-change/edit-leave-period/edit-leave-period.component.api';
import * as LeavePeriodChangeFixture from '../../../../fixtures/leave-period-change.fixture';

describe('LeavePeriodChange', () => {
  test('should render CancelWithdrawForm if no displayMode prop provided', async () => {
    render(
      <ContextWrapper>
        <LeavePeriodChange
          decisions={LeavePeriodChangeFixture.EMPTY_DECISIONS_FIXTURE}
          onEditLeavePeriodChange={jest.fn()}
          isEditLeavePeriod={true}
        />
      </ContextWrapper>
    );

    expect(screen.getByTestId('leave-period-change-form')).toBeInTheDocument();
  });

  test('should close drawer', async () => {
    const mockToggleModalVisible = jest.fn();

    render(
      <ContextWrapper>
        <LeavePeriodChange
          decisions={LeavePeriodChangeFixture.EMPTY_DECISIONS_FIXTURE}
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
        />
      </ContextWrapper>
    );

    fireEvent.click(screen.getAllByText('FINEOS_COMMON.GENERAL.CANCEL')[0]);

    expect(mockToggleModalVisible).toHaveBeenCalled();
  });

  test('should perform success flow', async () => {
    spyOn(leavePeriodChangeApi, 'changeLeavePeriods').and.returnValue(
      Promise.resolve(LeavePeriodChangeFixture.SINGLE_DECISION_PERIODS_FIXTURE)
    );
    const mockOnToggleModalVisible = jest.fn();
    render(
      <ContextWrapper>
        <LeavePeriodChange
          decisions={LeavePeriodChangeFixture.SINGLE_DECISION_PERIODS_FIXTURE}
          onEditLeavePeriodChange={mockOnToggleModalVisible}
          isEditLeavePeriod={true}
        />
      </ContextWrapper>
    );

    await selectAntOptionByText(
      screen.getByTestId('dropdown-input'),
      'LEAVE_PERIOD_CHANGE.SELECT_OPTION_REQUEST'
    );

    userEvent.click(
      screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.OK/i })
    );

    await wait();

    expect(
      screen.getByText('LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_HEAD')
    ).toBeInTheDocument();
    expect(
      screen.getByText('LEAVE_PERIOD_CHANGE.SUBMIT_SUCCESS_MESSAGE')
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.CLOSE/i })
    );

    expect(mockOnToggleModalVisible).toHaveBeenCalled();
  });
});
