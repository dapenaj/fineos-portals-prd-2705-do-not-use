/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';

import { render, act, screen } from '../../../../common/custom-render';
import { EditLeavePeriod } from '../../../../../app/modules/notification/leave-period-change/edit-leave-period/edit-leave-period.component';
import { ContextWrapper } from '../../../../common/utils';
import * as LeavePeriodChangeFixture from '../../../../fixtures/leave-period-change.fixture';

describe('LeavePeriodChangeForm', () => {
  const promise = Promise.resolve();
  const mockToggleModalVisible = jest.fn(() => promise);
  const mockHandleModeChange = jest.fn(() => promise);

  test('should render', async () => {
    render(
      <ContextWrapper>
        <EditLeavePeriod
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
          onFormSubmit={mockHandleModeChange}
          decisions={LeavePeriodChangeFixture.DECISIONS_FIXTURE}
        />
      </ContextWrapper>
    );

    expect(
      await screen.findByText('LEAVE_PERIOD_CHANGE.FORM_HEAD')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('LEAVE_PERIOD_CHANGE.MORE_THAN_ONE_PERIOD_LABEL')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('FINEOS_COMMON.GENERAL.SELECT_ALL')
    ).toBeInTheDocument();
    expect(document.querySelectorAll('.ant-checkbox-wrapper')).toHaveLength(3);
    expect(
      await screen.findByText('LEAVE_PERIOD_CHANGE.REASON_FOR_REMOVAL_LABEL')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('LEAVE_PERIOD_CHANGE.DETAILS_LABEL')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('FINEOS_COMMON.GENERAL.OK')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
  });

  test('should properly disable when one decision', async () => {
    render(
      <ContextWrapper>
        <EditLeavePeriod
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
          onFormSubmit={mockHandleModeChange}
          decisions={LeavePeriodChangeFixture.SINGLE_DECISION_PERIODS_FIXTURE}
        />
      </ContextWrapper>
    );

    await act(() => promise);

    expect(document.querySelectorAll('.ant-checkbox-wrapper')).toHaveLength(1);
    expect(
      screen.queryByText('FINEOS_COMMON.GENERAL.SELECT_ALL')
    ).not.toBeInTheDocument();
    expect(document.querySelector('.ant-checkbox-input')).toBeDisabled();
  });

  test('should select all checkboxes after clicking "Select all" button', async () => {
    render(
      <ContextWrapper>
        <EditLeavePeriod
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
          onFormSubmit={mockHandleModeChange}
          decisions={LeavePeriodChangeFixture.DECISIONS_FIXTURE}
        />
      </ContextWrapper>
    );

    await act(() => promise);

    const selectAllButton = screen.getByTestId('checkbox-select-all');

    userEvent.click(selectAllButton);

    await act(() => promise);

    expect(document.querySelectorAll('.ant-checkbox-checked')).toHaveLength(3);

    userEvent.click(selectAllButton);

    await act(() => promise);

    expect(document.querySelectorAll('.ant-checkbox-checked')).toHaveLength(0);

    expect(selectAllButton).not.toBeChecked();
  });

  test('should validate reason field', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditLeavePeriod
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
          onFormSubmit={mockHandleModeChange}
          decisions={LeavePeriodChangeFixture.DECISIONS_FIXTURE}
        />
      </ContextWrapper>
    );

    await act(() => promise);

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    expect(await screen.findByRole('combobox')).toBeInvalid();
    expect(
      await screen.findByText('FINEOS_COMMON.VALIDATION.REQUIRED')
    ).toBeInTheDocument();
  });

  test('should close', async () => {
    const { rerender } = render(
      <ContextWrapper>
        <EditLeavePeriod
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
          onFormSubmit={mockHandleModeChange}
          decisions={LeavePeriodChangeFixture.EMPTY_DECISIONS_FIXTURE}
        />
      </ContextWrapper>
    );

    await act(() => promise);

    userEvent.click(await screen.findByText('FINEOS_COMMON.GENERAL.CANCEL'));

    expect(mockToggleModalVisible).toHaveBeenCalledTimes(1);

    rerender(
      <ContextWrapper>
        <EditLeavePeriod
          onEditLeavePeriodChange={mockToggleModalVisible}
          isEditLeavePeriod={true}
          onFormSubmit={mockHandleModeChange}
          decisions={LeavePeriodChangeFixture.EMPTY_DECISIONS_FIXTURE}
        />
      </ContextWrapper>
    );

    await act(() => promise);

    userEvent.click(
      await screen.findByText(
        'FINEOS_COMMON.ICON_LABEL.DRAWER_CLOSE_ICON_LABEL'
      )
    );

    expect(mockToggleModalVisible).toHaveBeenCalledTimes(2);
  });
});
