/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';

import {
  parseDataToApiRequestFormat,
  getDecisionsToRemove
} from '../../../../../app/modules/notification/leave-period-change/edit-leave-period/edit-leave-period.utils';
import * as LeavePeriodChangeFixture from '../../../../fixtures/leave-period-change.fixture';

describe('EditLeavePeriodUtils', () => {
  test('mapLeavePeriods', () => {
    expect(
      parseDataToApiRequestFormat(
        LeavePeriodChangeFixture.FORM_VALUE_FIXTURE,
        LeavePeriodChangeFixture.FLATTEN_DECISIONS_FIXTURE
      )
    ).toEqual({
      'ABS-1': [
        {
          startDate: moment(new Date('2010-01-05')),
          endDate: moment(new Date('2010-01-10'))
        }
      ],
      'ABS-2': [
        {
          startDate: moment(new Date('2010-01-15')),
          endDate: moment(new Date('2010-01-25'))
        }
      ]
    });
  });

  test('getDecisionsToRemove for cases', () => {
    expect(
      getDecisionsToRemove(
        LeavePeriodChangeFixture.EMPTY_ENHANCED_GROUP_CLIENT_PERIOD_DECISION
      )
    ).toEqual([]);
    const decisionsForRemoval = getDecisionsToRemove(
      LeavePeriodChangeFixture.ENHANCED_GROUP_CLIENT_PERIOD_DECISION
    );

    expect(decisionsForRemoval).toHaveLength(3);
    expect(decisionsForRemoval[0]).toEqual({
      absence: { id: 'NTN-63-ABS-01' },
      period: {
        endDate: moment(new Date('2020-04-09')),
        leavePlan: {
          id: '99174fc9-e0da-45a8-95d6-600b13028492',
          adjudicationStatus: 'Accepted',
          name: 'civic duty plan'
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          decisionStatus: 'Approved',
          qualifier1: 'Jury',
          qualifier2: '',
          reasonName: 'Civic Duty'
        },
        startDate: moment(new Date('2020-04-05')),
        type: 'Reduced Schedule',
        parentPeriodReference: '',
        timeDecisionStatus: 'Approved'
      },
      statusCategory: 'Approved'
    });
    expect(decisionsForRemoval[1]).toEqual({
      absence: { id: 'NTN-63-ABS-01' },
      period: {
        endDate: moment(new Date('2020-03-15')),
        leavePlan: {
          id: '99174fc9-e0da-45a8-95d6-600b13028492',
          adjudicationStatus: 'Accepted',
          name: 'civic duty plan'
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          decisionStatus: 'Approved',
          qualifier1: 'Jury',
          qualifier2: '',
          reasonName: 'Civic Duty'
        },
        startDate: moment(new Date('2020-03-08')),
        parentPeriodReference: '',
        status: 'Episodic',
        timeDecisionStatus: '',
        type: 'Episodic'
      },
      statusCategory: 'Approved'
    });
    expect(decisionsForRemoval[2]).toEqual({
      absence: { id: 'NTN-63-ABS-01' },
      period: {
        endDate: moment(new Date('2020-02-06')),
        leavePlan: {
          adjudicationStatus: 'Accepted',
          id: '99174fc9-e0da-45a8-95d6-600b13028492',
          name: 'civic duty plan'
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          decisionStatus: 'Approved',
          qualifier1: 'Jury',
          qualifier2: '',
          reasonName: 'Civic Duty'
        },
        startDate: moment(new Date('2020-02-02')),
        parentPeriodReference: '',
        status: 'Known',
        timeDecisionStatus: 'Approved',
        type: 'Time off period'
      },
      statusCategory: 'Approved'
    });
  });
});
