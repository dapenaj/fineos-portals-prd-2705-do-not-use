/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, screen, wait, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ConcurrencyUpdateError } from 'fineos-js-api-client';

import { render } from '../../../common/custom-render';
import { ContextWrapper } from '../../../common/utils';
import { ControlUserAccess } from '../../../../app/modules/control-access-user';
import * as controlAccessUserApiModule from '../../../../app/modules/control-access-user/control-access-user.api';
import {
  GROUP_CLIENT_USERS_FIXTURE,
  GROUP_CLIENT_USERS_UPDATED_FIXTURE
} from '../../../fixtures/group-client-user.fixture';
import {
  AuthenticationService,
  PortalEnvConfigService
} from '../../../../app/shared';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

describe('ControlUserAccess', () => {
  beforeEach(() => {
    jest.spyOn(controlAccessUserApiModule, 'fetchUsers');
    jest.spyOn(controlAccessUserApiModule, 'updateUser');
    AuthenticationService.getInstance().displayUserId = '1';
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValueOnce(Promise.resolve(portalEnvConfig));
  });

  test('render empty site', async () => {
    (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
      Promise.resolve([])
    );
    render(
      <ContextWrapper>
        <ControlUserAccess />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('CONTROL_USER_ACCESS.EMPTY')).toBeInTheDocument();
  });

  test('render user list with enabled user', async () => {
    (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
      Promise.resolve([GROUP_CLIENT_USERS_FIXTURE[0]])
    );
    render(
      <ContextWrapper>
        <ControlUserAccess />
      </ContextWrapper>
    );
    await act(wait);

    expect(screen.getByTestId('control-access-user-title')).toBeInTheDocument();
    expect(screen.getByTestId('control-access-user-list')).toBeInTheDocument();
    expect(screen.getByText('Jane Doe')).toBeInTheDocument();
    expect(screen.getByText('HR')).toBeInTheDocument();
    expect(
      screen.getByText('CONTROL_USER_ACCESS.DISABLE_USER')
    ).toBeInTheDocument();
  });

  test('render user list with off user', async () => {
    (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
      Promise.resolve([GROUP_CLIENT_USERS_FIXTURE[1]])
    );
    render(
      <ContextWrapper>
        <ControlUserAccess />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('control-access-user-title')).toBeInTheDocument();
    expect(screen.getByTestId('control-access-user-list')).toBeInTheDocument();
    expect(screen.getByText('Jane Doe')).toBeInTheDocument();
    expect(
      screen.getByText(`CONTROL_USER_ACCESS.DISABLED`)
    ).toBeInTheDocument();
    expect(screen.getByText('HR')).toBeInTheDocument();
    expect(
      screen.getByText('CONTROL_USER_ACCESS.ENABLE_USER')
    ).toBeInTheDocument();
  });

  test('switch user ENABLED', async () => {
    (controlAccessUserApiModule.updateUser as jest.Mock).mockReturnValueOnce(
      Promise.resolve(GROUP_CLIENT_USERS_UPDATED_FIXTURE)
    );
    (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
      Promise.resolve(GROUP_CLIENT_USERS_FIXTURE)
    );
    const { rerender } = render(
      <ContextWrapper>
        <ControlUserAccess />
      </ContextWrapper>
    );

    userEvent.click(await screen.findByText('CONTROL_USER_ACCESS.ENABLE_USER'));

    expect(
      screen.queryByText('CONTROL_USER_ACCESS.ENABLE_USER')
    ).not.toBeInTheDocument();
    expect(
      await screen.findByText('CONTROL_USER_ACCESS.ENABLED_USER_INFO')
    ).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <ControlUserAccess />
      </ContextWrapper>
    );

    expect(
      screen.getAllByText('CONTROL_USER_ACCESS.DISABLE_USER')
    ).toHaveLength(2);

    userEvent.click(screen.getAllByText('CONTROL_USER_ACCESS.DISABLE_USER')[0]);

    expect(
      within(await screen.findByRole('dialog')).getByText(
        'CONTROL_USER_ACCESS.CHANGE_PERMISSIONS'
      )
    ).toBeInTheDocument();
    expect(
      within(await screen.findByRole('dialog')).getByText(
        'FINEOS_COMMON.GENERAL.YES'
      )
    ).toBeInTheDocument();
    expect(
      within(await screen.findByRole('dialog')).getByText(
        'FINEOS_COMMON.GENERAL.NO'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(await screen.findByRole('dialog')).getByText(
        'FINEOS_COMMON.GENERAL.YES'
      )
    );

    expect(
      screen.queryByText('CONTROL_USER_ACCESS.DISABLED_USER_INFO')
    ).not.toBeInTheDocument();
  });

  describe('Concurrency', () => {
    test('should display data concurrency modal when concurrency error returned', async () => {
      (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
        Promise.resolve(GROUP_CLIENT_USERS_FIXTURE)
      );
      (controlAccessUserApiModule.updateUser as jest.Mock).mockRejectedValueOnce(
        new ConcurrencyUpdateError({ test: 'test' })
      );

      render(
        <ContextWrapper>
          <ControlUserAccess />
        </ContextWrapper>
      );

      await act(wait);

      await act(() => {
        userEvent.click(
          screen.getAllByText('CONTROL_USER_ACCESS.DISABLE_USER')[0]
        );
        return wait();
      });

      await act(() => {
        userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.YES'));
        return wait();
      });

      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).toBeInTheDocument();
    });

    test('should not display data concurrency modal when concurrency error not returned', async () => {
      (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
        Promise.resolve(GROUP_CLIENT_USERS_FIXTURE)
      );
      (controlAccessUserApiModule.updateUser as jest.Mock).mockReturnValueOnce(
        Promise.resolve(GROUP_CLIENT_USERS_FIXTURE[0])
      );

      render(
        <ContextWrapper>
          <ControlUserAccess />
        </ContextWrapper>
      );

      await act(wait);

      await act(() => {
        userEvent.click(
          screen.getAllByText('CONTROL_USER_ACCESS.ENABLE_USER')[0]
        );
        return wait();
      });

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();
    });

    test('should refresh data in form when concurrency occurred', async () => {
      const changedUsers = GROUP_CLIENT_USERS_FIXTURE;
      // todo revisit this test, check if this is necessary changedUsers[1].enabled = false;
      (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValue(
        Promise.resolve(GROUP_CLIENT_USERS_FIXTURE)
      );
      (controlAccessUserApiModule.updateUser as jest.Mock).mockRejectedValueOnce(
        new ConcurrencyUpdateError(changedUsers)
      );

      render(
        <ContextWrapper>
          <ControlUserAccess />
        </ContextWrapper>
      );

      await act(wait);

      expect(
        screen.getByText('CONTROL_USER_ACCESS.DISABLE_USER')
      ).toBeInTheDocument();
      expect(
        screen.getByText('CONTROL_USER_ACCESS.ENABLE_USER')
      ).toBeInTheDocument();

      await act(() => {
        userEvent.click(screen.getByText('CONTROL_USER_ACCESS.DISABLE_USER'));
        return wait();
      });

      await act(() => {
        userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.YES'));
        return wait();
      });

      await act(() => {
        userEvent.click(
          screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
        );
        return wait();
      });

      expect(
        screen.getAllByText('CONTROL_USER_ACCESS.ENABLE_USER')
      ).toHaveLength(1);
      expect(
        screen.queryByText('CONTROL_USER_ACCESS.DISABLE_USER')
      ).not.toBeInTheDocument();
    });

    test('should display response error modal and no concurrency modal when error other than concurrency occurred', async () => {
      (controlAccessUserApiModule.fetchUsers as jest.Mock).mockReturnValueOnce(
        Promise.resolve(GROUP_CLIENT_USERS_FIXTURE)
      );
      (controlAccessUserApiModule.updateUser as jest.Mock).mockRejectedValueOnce(
        { status: '404' }
      );

      render(
        <ContextWrapper>
          <ControlUserAccess />
        </ContextWrapper>
      );

      await act(wait);

      await act(() => {
        userEvent.click(
          screen.getAllByText('CONTROL_USER_ACCESS.ENABLE_USER')[0]
        );
        return wait();
      });

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();

      expect(
        screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).toBeInTheDocument();
    });
  });
});
