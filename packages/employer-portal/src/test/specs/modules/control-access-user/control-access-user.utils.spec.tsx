import { AuthenticationService } from '../../../../app/shared/authentication/authentication.service';
import { GROUP_CLIENT_USERS_FIXTURE } from '../../../fixtures/group-client-user.fixture';
import {
  usersWithoutCurrentUser,
  getStatusToSet
} from '../../../../app/modules/control-access-user/control-access-user.utils';
import { UserStatusName } from '../../../../app/modules/control-access-user/control-user-access.enum';

describe('ControlAccessUserUtils', () => {
  describe('usersWithoutCurrentUser', () => {
    AuthenticationService.getInstance().displayUserId =
      GROUP_CLIENT_USERS_FIXTURE[0].userReferenceIdentifier;

    test('should return list without current user', () => {
      expect(
        usersWithoutCurrentUser(GROUP_CLIENT_USERS_FIXTURE)
      ).toStrictEqual([GROUP_CLIENT_USERS_FIXTURE[1]]);
    });
  });

  describe('getStatusToSet', () => {
    const mockStatusDomains = [
      {
        domainId: 1001,
        fullId: 32032001,
        name: 'Active',
        domainName: 'Account Status'
      },
      {
        domainId: 1001,
        fullId: 32032002,
        name: 'Disabled - Manually',
        domainName: 'Account Status'
      }
    ];
    test('should return correct domain', () => {
      expect(
        getStatusToSet(mockStatusDomains, UserStatusName.ACTIVE)
      ).toStrictEqual(mockStatusDomains[0]);
    });
  });
});
