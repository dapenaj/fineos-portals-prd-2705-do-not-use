/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { wait } from '@testing-library/dom';

import { ContextWrapper } from '../../../../common/utils';
import { ActualAbsencePeriods } from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protected-leave-card-body/actual-absence-periods/actual-absence-periods.component';
import { PERIODS_NUMBER_THRESHOLD } from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protected-leave-card-body/actual-absence-periods/actual-absence-periods.type';
import { ACTUAL_ABSENCE_PERIODS_FIXTURE } from '../../../../fixtures/actual-absence-periods.fixture';

const generateAbsencePeriods = (length: number) =>
  new Array(length)
    .fill(ACTUAL_ABSENCE_PERIODS_FIXTURE[0])
    .map((period, index) => ({
      ...period,
      id: index + 1
    }));
describe('ActualAbsencePeriods', () => {
  test('should render', () => {
    const periodsQuantity = PERIODS_NUMBER_THRESHOLD - 1;
    const mockActualAbsencePeriods = generateAbsencePeriods(periodsQuantity);
    const mockLeavePlanName = 'testName';
    render(
      <ContextWrapper>
        <ActualAbsencePeriods
          leavePlanName={mockLeavePlanName}
          actualAbsencePeriods={mockActualAbsencePeriods}
          leaveRequestReasonName="test"
        />
      </ContextWrapper>
    );

    expect(screen.getAllByTestId('actual-absence-period-date')).toHaveLength(
      periodsQuantity
    );
    expect(
      screen.getAllByTestId('actual-absence-period-duration')
    ).toHaveLength(periodsQuantity);
    expect(
      screen.getAllByTestId('actual-absence-period-decision')
    ).toHaveLength(periodsQuantity);
    expect(
      screen.queryByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON'
      )
    ).not.toBeInTheDocument();
  });

  test('should display correct modal title', async () => {
    const periodsQuantity = PERIODS_NUMBER_THRESHOLD + 1;
    const mockActualAbsencePeriods = generateAbsencePeriods(periodsQuantity);
    const mockLeaveRequestReasonName = 'mockLeaveRequestReasonNAme';
    const mockLeavePlanName = 'mockLeavePlanName';

    render(
      <ContextWrapper>
        <ActualAbsencePeriods
          leavePlanName={mockLeavePlanName}
          actualAbsencePeriods={mockActualAbsencePeriods}
          leaveRequestReasonName={mockLeaveRequestReasonName}
        />
      </ContextWrapper>
    );

    expect(
      document.querySelector(
        '[aria-label="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON_ARIA_LABEL"]'
      )
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON'
      )
    );

    await act(wait);
    expect(
      screen.getByTestId('actual-absence-periods-modal-title')
    ).toHaveTextContent(`${mockLeavePlanName} - ${mockLeaveRequestReasonName}`);
  });

  test('should render no more than 5 elements', async () => {
    const periodsQuantity = PERIODS_NUMBER_THRESHOLD + 1;
    const mockActualAbsencePeriods = generateAbsencePeriods(periodsQuantity);
    const mockLeavePlanName = 'testName';
    render(
      <ContextWrapper>
        <ActualAbsencePeriods
          leavePlanName={mockLeavePlanName}
          actualAbsencePeriods={mockActualAbsencePeriods}
          leaveRequestReasonName="test"
        />
      </ContextWrapper>
    );

    await act(wait);

    const limitedTable = within(
      screen.getAllByTestId('limited-absence-periods-table')[0]
    );
    expect(
      limitedTable.getAllByTestId('actual-absence-period-date')
    ).toHaveLength(PERIODS_NUMBER_THRESHOLD);
    expect(
      limitedTable.getAllByTestId('actual-absence-period-duration')
    ).toHaveLength(PERIODS_NUMBER_THRESHOLD);
    expect(
      limitedTable.getAllByTestId('actual-absence-period-decision')
    ).toHaveLength(PERIODS_NUMBER_THRESHOLD);
    expect(
      document.querySelector(
        '[aria-label="NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON_ARIA_LABEL"]'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.VIEW_MORE_BUTTON'
      )
    ).toBeInTheDocument();
  });
});
