/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED 
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment from 'moment';
import { act, render, screen } from '@testing-library/react';
import { wait } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';
import { ActualAbsencePeriodsService } from 'fineos-js-api-client';

import { ContextWrapper } from '../../../../common/utils';
import { IntermittentDecisionProperties } from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protected-leave-card-body/intermittent-decision-properties.component';
import { LEAVE_PLAN_FIXTURE } from '../../../../fixtures/leave-plan.fixture';
import { ACTUAL_ABSENCE_PERIODS_FIXTURE } from '../../../../fixtures/actual-absence-periods.fixture';

describe('IntermittentDecisionProperties', () => {
  const service = ActualAbsencePeriodsService.getInstance();
  test('should render', () => {
    render(
      <ContextWrapper>
        <IntermittentDecisionProperties
          leavePlan={LEAVE_PLAN_FIXTURE[0]}
          decision={LEAVE_PLAN_FIXTURE[0].decisions[0]}
          actuals={[]}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON')
    ).toBeInTheDocument();
    expect(
      screen.getByText('DECISION_STATUS_CATEGORY.PENDING')
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.HIDE_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    ).not.toBeInTheDocument();
  });

  test('should display show/hide button', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={LEAVE_PLAN_FIXTURE[0]}
          decision={LEAVE_PLAN_FIXTURE[0].decisions[0]}
          actuals={ACTUAL_ABSENCE_PERIODS_FIXTURE}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    );

    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.HIDE_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    ).toBeInTheDocument();

    await act(wait);
  });

  test('should display actualAbsencePeriods if during decision period', async () => {
    const mockLeavePlan = {
      ...LEAVE_PLAN_FIXTURE[0],
      decisions: [
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[0],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[0].period,
            startDate: moment().subtract(6),
            endDate: moment().subtract(3)
          }
        }
      ]
    };
    const mockActualAbsencePeriods = [
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(5)
      },
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(4)
      }
    ];

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={mockLeavePlan}
          decision={mockLeavePlan.decisions[0]}
          actuals={mockActualAbsencePeriods}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    );

    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DATE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DURATION'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DECISION'
      )
    ).toBeInTheDocument();

    expect(
      screen.getAllByTestId('actual-absence-period-duration')
    ).toHaveLength(ACTUAL_ABSENCE_PERIODS_FIXTURE.length);
    expect(screen.getAllByTestId('actual-absence-period-date')).toHaveLength(
      ACTUAL_ABSENCE_PERIODS_FIXTURE.length
    );
    expect(
      screen.getAllByTestId('actual-absence-period-decision')
    ).toHaveLength(ACTUAL_ABSENCE_PERIODS_FIXTURE.length);

    await act(wait);
  });

  test('should display actualAbsencePeriods if one of them falls outside decision', async () => {
    const mockLeavePlan = {
      ...LEAVE_PLAN_FIXTURE[0],
      decisions: [
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[0],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[0].period,
            startDate: moment().subtract(6, 'days'),
            endDate: moment().subtract(3, 'days')
          }
        }
      ]
    };
    const mockActualAbsencePeriods = [
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(8, 'days')
      },
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(4, 'days')
      }
    ];

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={mockLeavePlan}
          decision={mockLeavePlan.decisions[0]}
          actuals={mockActualAbsencePeriods}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    );

    expect(
      screen.getAllByTestId('actual-absence-period-duration')
    ).toHaveLength(ACTUAL_ABSENCE_PERIODS_FIXTURE.length);
    expect(screen.getAllByTestId('actual-absence-period-date')).toHaveLength(
      ACTUAL_ABSENCE_PERIODS_FIXTURE.length
    );
    expect(
      screen.getAllByTestId('actual-absence-period-decision')
    ).toHaveLength(ACTUAL_ABSENCE_PERIODS_FIXTURE.length);

    await act(wait);
  });

  test('should display actualAbsencePeriods if both of them fall outside decision', async () => {
    const mockLeavePlan = {
      ...LEAVE_PLAN_FIXTURE[0],
      decisions: [
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[0],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[0].period,
            startDate: moment().subtract(6, 'days'),
            endDate: moment().subtract(3, 'days')
          }
        }
      ]
    };
    const mockActualAbsencePeriods = [
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(2, 'days')
      },
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(10, 'days')
      }
    ];

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={mockLeavePlan}
          decision={mockLeavePlan.decisions[0]}
          actuals={mockActualAbsencePeriods}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    );

    expect(
      screen.getAllByTestId('actual-absence-period-duration')
    ).toHaveLength(ACTUAL_ABSENCE_PERIODS_FIXTURE.length);
    expect(screen.getAllByTestId('actual-absence-period-date')).toHaveLength(
      ACTUAL_ABSENCE_PERIODS_FIXTURE.length
    );
    expect(
      screen.getAllByTestId('actual-absence-period-decision')
    ).toHaveLength(ACTUAL_ABSENCE_PERIODS_FIXTURE.length);

    await act(wait);
  });

  test('should correctly find the nearest decision and display actualAbsencePeriod in it', async () => {
    const mockLeavePlan = {
      ...LEAVE_PLAN_FIXTURE[0],
      decisions: [
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[0],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[0].period,
            startDate: moment().subtract(7, 'days'),
            endDate: moment().subtract(5, 'days')
          }
        },
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[1],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[1].period,
            startDate: moment().subtract(17, 'days'),
            endDate: moment().subtract(15, 'days')
          }
        }
      ]
    };
    const mockActualAbsencePeriods = [
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(8, 'days')
      },
      {
        ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
        actualDate: moment().subtract(14, 'days')
      }
    ];

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={mockLeavePlan}
          decision={mockLeavePlan.decisions[0]}
          actuals={mockActualAbsencePeriods}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    );

    expect(
      screen.getAllByTestId('actual-absence-period-duration')
    ).toHaveLength(2);
    expect(screen.getAllByTestId('actual-absence-period-date')).toHaveLength(2);
    expect(
      screen.getAllByTestId('actual-absence-period-decision')
    ).toHaveLength(2);
  });

  test('should add period to decision first in the calendar', async () => {
    const periodDate = moment().subtract(5, 'days');
    const mockLeavePlan = {
      ...LEAVE_PLAN_FIXTURE[0],
      decisions: [
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[0],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[0].period,
            startDate: moment().subtract(7, 'days')
          }
        },
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[1],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[1].period,
            startDate: moment().subtract(3, 'days')
          }
        }
      ]
    };

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={mockLeavePlan}
          decision={mockLeavePlan.decisions[0]}
          actuals={[
            {
              ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
              actualDate: periodDate
            }
          ]}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    ).toBeInTheDocument();
  });

  test('should not add period to decision not first in the calendar', async () => {
    const periodDate = moment().subtract(5, 'days');
    jest.spyOn(service, 'fetchActualAbsencePeriods').mockReturnValue(
      Promise.resolve([
        {
          ...ACTUAL_ABSENCE_PERIODS_FIXTURE[0],
          actualDate: periodDate
        }
      ])
    );
    const mockLeavePlan = {
      ...LEAVE_PLAN_FIXTURE[0],
      decisions: [
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[0],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[0].period,
            startDate: moment().subtract(3, 'days')
          }
        },
        {
          ...LEAVE_PLAN_FIXTURE[0].decisions[1],
          period: {
            ...LEAVE_PLAN_FIXTURE[0].decisions[1].period,
            startDate: moment().subtract(7, 'days')
          }
        }
      ]
    };

    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <IntermittentDecisionProperties
          leavePlan={mockLeavePlan}
          decision={mockLeavePlan.decisions[0]}
          actuals={[]}
        />
      </ContextWrapper>
    );

    await act(wait);
    expect(
      screen.queryByText(
        'NOTIFICATIONS.JOB_PROTECTED_LEAVE.SHOW_ACTUAL_ABSENCE_PERIODS_BUTTON'
      )
    ).not.toBeInTheDocument();
  });
});
