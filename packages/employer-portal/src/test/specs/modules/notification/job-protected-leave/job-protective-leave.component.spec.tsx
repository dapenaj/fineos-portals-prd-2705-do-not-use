/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, RenderResult, act, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ViewNotificationPermissions } from 'fineos-common';

import { EnhancedAbsencePeriodDecisions } from '../../../../../app/shared';
import { ContextWrapper, releaseEventLoop } from '../../../../common/utils';
import {
  EMPTY_DECISIONS_FIXTURE,
  CONCLUDED_ASSESSMENTS_FIXTURE,
  PENDING_ASSESSMENTS_FIXTURE,
  ABSENCE_PERIOD_DECISIONS_FIXTURE
} from '../../../../fixtures/absence-periods.fixture';
import { JobProtectedLeave } from '../../../../../app/modules/notification/job-protected-leave';
import { ActualAbsencePeriodsService } from 'fineos-js-api-client';

describe('Job Protected Leave', () => {
  const permissions = [
    ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
    ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS
  ];

  let enhancedAbsencePeriodDecisions:
    | EnhancedAbsencePeriodDecisions[]
    | null = null;

  afterEach(() => {
    enhancedAbsencePeriodDecisions = null;
  });

  describe('without permissions', () => {
    test('should not be rendered', () => {
      enhancedAbsencePeriodDecisions = [];

      const { queryByTestId } = render(
        <ContextWrapper>
          <JobProtectedLeave
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(
        queryByTestId('job-protected-leave-panel')
      ).not.toBeInTheDocument();
    });
  });

  describe('with empty absence period decisions', () => {
    test('should not be rendered', () => {
      enhancedAbsencePeriodDecisions = [];

      const { queryByTestId } = render(
        <ContextWrapper permissions={permissions}>
          <JobProtectedLeave
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(
        queryByTestId('job-protected-leave-panel')
      ).not.toBeInTheDocument();
    });
  });

  describe('with empty decisions on the absence period decision', () => {
    test('should render the job protected leave panel with the empty warning state', () => {
      enhancedAbsencePeriodDecisions = EMPTY_DECISIONS_FIXTURE;

      const { queryByTestId, queryByText } = render(
        <ContextWrapper permissions={permissions}>
          <JobProtectedLeave
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(queryByTestId('job-protected-leave-panel')).toBeInTheDocument();
      expect(queryByTestId('warning-state')).toBeInTheDocument();
      expect(
        queryByText('NOTIFICATIONS.JOB_PROTECTED_LEAVE.EMPTY')
      ).toBeInTheDocument();
    });
  });

  describe('with decisions without leave plan names', () => {
    test('should render the job protected leave panel with the concluded assessments', () => {
      enhancedAbsencePeriodDecisions = CONCLUDED_ASSESSMENTS_FIXTURE;

      const { queryByTestId } = render(
        <ContextWrapper permissions={permissions}>
          <JobProtectedLeave
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(queryByTestId('job-protected-leave-panel')).toBeInTheDocument();
      expect(queryByTestId('warning-state')).toBeInTheDocument();

      /**
       * For some reason these values are failing to be queried but exist in the rendered html
       */
      // expect(
      //   queryByText('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ASSESSMENT_CONCLUDED_TITLE')
      // ).toBeInTheDocument();
      // expect(
      //   queryByText('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ASSESSMENT_CONCLUDED_FOOTER')
      // ).toBeInTheDocument();
    });

    test('should render the job protected leave panel with the pending assessments', () => {
      enhancedAbsencePeriodDecisions = PENDING_ASSESSMENTS_FIXTURE;

      const { queryByTestId, queryByText } = render(
        <ContextWrapper permissions={permissions}>
          <JobProtectedLeave
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(queryByTestId('job-protected-leave-panel')).toBeInTheDocument();
      expect(queryByTestId('empty-state')).toBeInTheDocument();
      expect(
        queryByText('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ASSESSMENT_PENDING')
      ).toBeInTheDocument();
    });
  });

  describe('with decisions with leave plan names', () => {
    let jobProtectedLeave: RenderResult;

    beforeEach(() => {
      const service = ActualAbsencePeriodsService.getInstance();
      jest
        .spyOn(service, 'fetchActualAbsencePeriods')
        .mockReturnValue(Promise.resolve([]));
      enhancedAbsencePeriodDecisions = ABSENCE_PERIOD_DECISIONS_FIXTURE;

      jobProtectedLeave = render(
        <ContextWrapper permissions={permissions}>
          <JobProtectedLeave
            enhancedAbsencePeriodDecisions={enhancedAbsencePeriodDecisions}
            isLoading={false}
          />
        </ContextWrapper>
      );
    });

    test('should render the job protected leave plans', () => {
      const { queryByTestId, queryAllByTestId } = jobProtectedLeave;

      expect(queryByTestId('job-protected-leave-cards')).toBeInTheDocument();
      expect(queryByTestId('empty-state')).not.toBeInTheDocument();
      expect(queryAllByTestId('collapsible-card')).toHaveLength(1);
    });

    test('should render a job protected leave plan collapsed', () => {
      const { queryByTestId, queryAllByTestId } = jobProtectedLeave;

      const element = queryAllByTestId('collapsible-card')[0];

      expect(queryByTestId('job-protected-leave-cards')).toBeInTheDocument();
      expect(element).toBeInTheDocument();

      expect(
        queryByTestId('job-protected-leave-plan-header')
      ).toHaveTextContent('Federal FMLA');

      const statuses = queryAllByTestId('status-label');

      expect(statuses[0]).toHaveTextContent(
        'DECISION_STATUS_CATEGORY.APPROVED'
      );
      expect(statuses[0]).toHaveTextContent('(2)');

      expect(statuses[1]).toHaveTextContent('DECISION_STATUS_CATEGORY.PENDING');
      expect(statuses[1]).toHaveTextContent('(1)');
    });

    test('should render a job protected leave plan collapsed with proper sorting by date', async () => {
      const { getByTestId, queryAllByTestId, container } = jobProtectedLeave;

      await act(() => {
        userEvent.click(container.querySelector('.ant-collapse-header')!);
        return releaseEventLoop();
      });

      const items = within(getByTestId('collapsible-card')).getAllByTestId(
        'property-item'
      );

      expect(
        within(items[8]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEAVE_BEGINS');
      expect(
        within(items[8]).queryByTestId('property-item-value')
      ).toHaveTextContent('01-01-2020');

      expect(
        within(items[7]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON');
      expect(
        within(items[7]).queryByTestId('property-item-value')
      ).toHaveTextContent('Pregnancy/Maternity - Postpartum disability');

      expect(
        within(items[6]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.APPROVED_BETWEEN');
      expect(
        within(items[6]).queryByTestId('property-item-value')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.BETWEEN_DATES');

      expect(
        within(items[5]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON');
      expect(
        within(items[5]).queryByTestId('property-item-value')
      ).toHaveTextContent('Pregnancy/Maternity - Termination');

      expect(
        within(items[4]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.APPROVED_THROUGH');
      expect(
        within(items[4]).queryByTestId('property-item-value')
      ).toHaveTextContent('06-06-2020');

      expect(
        within(items[3]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.PERIOD_BEGINS');
      expect(
        within(items[3]).queryByTestId('property-item-value')
      ).toHaveTextContent('05-02-2020');

      expect(
        within(items[2]).queryByTestId('property-item-label')
      ).toHaveTextContent('NOTIFICATIONS.JOB_PROTECTED_LEAVE.ABSENCE_REASON');
      expect(
        within(items[2]).queryByTestId('property-item-value')
      ).toHaveTextContent('Pregnancy/Maternity - ');

      expect(
        within(items[1]).queryByTestId('property-item-label')
      ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
      expect(
        within(items[1]).queryByTestId('property-item-value')
      ).toHaveTextContent('01-111-123456');

      expect(
        within(items[0]).queryByTestId('property-item-label')
      ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
      expect(
        within(items[0]).queryByTestId('property-item-value')
      ).toHaveTextContent('Kylo Ren');

      const statuses = queryAllByTestId('status-label');

      expect(statuses[0]).toHaveTextContent(
        'DECISION_STATUS_CATEGORY.APPROVED'
      );

      expect(statuses[1]).toHaveTextContent(
        'DECISION_STATUS_CATEGORY.APPROVED'
      );

      expect(statuses[2]).toHaveTextContent('DECISION_STATUS_CATEGORY.PENDING');
    });
  });
});
