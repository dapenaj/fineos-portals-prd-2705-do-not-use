/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment from 'moment';
import { act, render, screen, within } from '@testing-library/react';
import { wait } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import {
  buildTranslationWithParams,
  ContextWrapper,
  mockTranslations,
  resolveTranslationWithParams
} from '../../../../common/utils';
import { ActualAbsencePeriodsTable } from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protected-leave-card-body/actual-absence-periods/actual-absence-periods-table/actual-absence-periods-table.component';
import { PERIODS_NUMBER_THRESHOLD } from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protected-leave-card-body/actual-absence-periods/actual-absence-periods.type';
import { ACTUAL_ABSENCE_PERIODS_FIXTURE } from '../../../../fixtures/actual-absence-periods.fixture';

describe('ActualAbsencePeriodsTable', () => {
  const period = ACTUAL_ABSENCE_PERIODS_FIXTURE[0];
  const mockMixedDatesPeriods = [
    {
      ...period,
      id: '1',
      actualDate: moment().subtract(4),
      managerAccepted: {
        ...period.managerAccepted,
        name: 'Yes'
      }
    },
    {
      ...period,
      id: '2',
      actualDate: moment().subtract(2),
      managerAccepted: {
        ...period.managerAccepted,
        name: 'No'
      }
    },
    {
      ...period,
      id: '3',
      actualDate: moment().subtract(3)
    },
    {
      ...period,
      id: '4',
      actualDate: moment().subtract(1),
      managerAccepted: {
        ...period.managerAccepted,
        name: 'Yes'
      }
    },
    {
      ...period,
      id: '5',
      actualDate: moment().subtract(5),
      managerAccepted: {
        ...period.managerAccepted,
        name: 'No'
      }
    },
    {
      ...period,
      id: '6',
      actualDate: moment().subtract(6)
    }
  ];
  describe('should render sorted dates', () => {
    test('should display only 5 dates', () => {
      render(
        <ContextWrapper>
          <ActualAbsencePeriodsTable
            actualAbsencePeriods={mockMixedDatesPeriods}
          />
        </ContextWrapper>
      );

      expect(screen.getAllByTestId('actual-absence-period-date')).toHaveLength(
        PERIODS_NUMBER_THRESHOLD
      );
      expect(
        screen.getAllByTestId('actual-absence-period-duration')
      ).toHaveLength(PERIODS_NUMBER_THRESHOLD);
      expect(
        screen.getAllByTestId('actual-absence-period-decision')
      ).toHaveLength(PERIODS_NUMBER_THRESHOLD);
    });

    describe('table sorting', () => {
      const getSubtractedDate = (subtractValue: number) =>
        moment()
          .subtract(subtractValue)
          .format('DD-MM-YYYY');
      test('should sort results in simple version of table', () => {
        render(
          <ContextWrapper>
            <ActualAbsencePeriodsTable
              actualAbsencePeriods={mockMixedDatesPeriods}
            />
          </ContextWrapper>
        );
        const sortResults = screen.getAllByTestId('actual-absence-period-date');

        expect(sortResults[0]).toHaveTextContent(getSubtractedDate(1));
        expect(sortResults[1]).toHaveTextContent(getSubtractedDate(2));
        expect(sortResults[2]).toHaveTextContent(getSubtractedDate(3));
        expect(sortResults[3]).toHaveTextContent(getSubtractedDate(4));
        expect(sortResults[4]).toHaveTextContent(getSubtractedDate(5));
      });

      test('should sort results in full version of table', () => {
        render(
          <ContextWrapper>
            <ActualAbsencePeriodsTable
              actualAbsencePeriods={mockMixedDatesPeriods}
              isSimpleVersion={false}
            />
          </ContextWrapper>
        );
        const sortResults = screen.getAllByTestId('actual-absence-period-date');

        expect(sortResults).toHaveLength(mockMixedDatesPeriods.length);
        expect(sortResults[0]).toHaveTextContent(getSubtractedDate(6));
        expect(sortResults[1]).toHaveTextContent(getSubtractedDate(5));
        expect(sortResults[2]).toHaveTextContent(getSubtractedDate(4));
        expect(sortResults[3]).toHaveTextContent(getSubtractedDate(3));
        expect(sortResults[4]).toHaveTextContent(getSubtractedDate(2));
        expect(sortResults[4]).toHaveTextContent(getSubtractedDate(1));
      });

      test('should filter results in full version of table', async () => {
        render(
          <ContextWrapper>
            <ActualAbsencePeriodsTable
              actualAbsencePeriods={mockMixedDatesPeriods}
              isSimpleVersion={false}
            />
          </ContextWrapper>
        );

        userEvent.click(
          screen.getByText(
            'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DECISION_FILTER_ICON'
          )
        );

        await act(wait);

        userEvent.click(
          within(screen.getByRole('dialog')).getByTestId('Unknown')
        );
        userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.OK'));

        await act(wait);

        expect(
          screen.getAllByTestId('actual-absence-period-date')
        ).toHaveLength(2);

        userEvent.click(
          screen.getByText(
            'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.COLUMN_HEAD_DECISION_FILTER_ICON'
          )
        );
        userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.RESET'));

        expect(
          screen.getAllByTestId('actual-absence-period-date')
        ).toHaveLength(mockMixedDatesPeriods.length);

        await act(wait);
      });

      describe('time units display', () => {
        const translations = mockTranslations(
          buildTranslationWithParams(
            'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_MINUTES',
            ['minutes']
          ),
          buildTranslationWithParams(
            'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_HOURS',
            ['hours']
          ),
          buildTranslationWithParams(
            'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_DAYS',
            ['days']
          )
        );

        test('should correctly display minutes', async () => {
          const mockActualAbsencePeriods = {
            ...period,
            episodePeriodDuration: 30,
            episodePeriodBasis: {
              ...period.episodePeriodBasis,
              name: 'Minutes'
            }
          };
          const { rerender } = render(
            <ContextWrapper translations={translations}>
              <ActualAbsencePeriodsTable
                actualAbsencePeriods={[mockActualAbsencePeriods]}
              />
            </ContextWrapper>
          );

          await act(wait);

          expect(
            screen.getByTestId('actual-absence-period-duration')
          ).toHaveTextContent(
            `${(resolveTranslationWithParams(
              'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_HOURS',
              [['hours', '0']]
            ),
            resolveTranslationWithParams(
              'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_MINUTES',
              [['minutes', '30']]
            ))}`
          );

          rerender(
            <ContextWrapper translations={translations}>
              <ActualAbsencePeriodsTable
                actualAbsencePeriods={[
                  {
                    ...mockActualAbsencePeriods,
                    episodePeriodDuration: 90
                  }
                ]}
              />
            </ContextWrapper>
          );

          expect(
            screen.getByTestId('actual-absence-period-duration')
          ).toHaveTextContent(
            `${(resolveTranslationWithParams(
              'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_HOURS',
              [['hours', '1']]
            ),
            resolveTranslationWithParams(
              'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_MINUTES',
              [['minutes', '30']]
            ))}`
          );
        });

        test('should correctly display hours', () => {
          const mockActualAbsencePeriods = {
            ...period,
            episodePeriodDuration: 2,
            episodePeriodBasis: {
              ...period.episodePeriodBasis,
              name: 'Hours'
            }
          };

          render(
            <ContextWrapper translations={translations}>
              <ActualAbsencePeriodsTable
                actualAbsencePeriods={[mockActualAbsencePeriods]}
              />
            </ContextWrapper>
          );

          expect(
            screen.getByTestId('actual-absence-period-duration')
          ).toHaveTextContent(
            resolveTranslationWithParams(
              'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_HOURS',
              [['hours', '2']]
            )
          );
        });

        test('should correctly display days', () => {
          const mockActualAbsencePeriods = {
            ...period,
            episodePeriodDuration: 2,
            episodePeriodBasis: {
              ...period.episodePeriodBasis,
              name: 'Days'
            }
          };

          render(
            <ContextWrapper translations={translations}>
              <ActualAbsencePeriodsTable
                actualAbsencePeriods={[mockActualAbsencePeriods]}
              />
            </ContextWrapper>
          );

          expect(
            screen.getByTestId('actual-absence-period-duration')
          ).toHaveTextContent(
            resolveTranslationWithParams(
              'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.DURATION_INFORMATION_DAYS',
              [['days', '2']]
            )
          );
        });

        test('should correctly handle invalid period basis', () => {
          const mockActualAbsencePeriods = {
            ...period,
            episodePeriodDuration: 2,
            episodePeriodBasis: {
              ...period.episodePeriodBasis,
              name: 'Please Select'
            }
          };

          render(
            <ContextWrapper translations={translations}>
              <ActualAbsencePeriodsTable
                actualAbsencePeriods={[mockActualAbsencePeriods]}
              />
            </ContextWrapper>
          );

          expect(
            screen.getByTestId('actual-absence-period-duration')
          ).toHaveTextContent(
            'NOTIFICATIONS.JOB_PROTECTED_LEAVE.ACTUAL_ABSENCE_PERIODS.NO_TIME_ENTERED'
          );
        });
      });
    });
  });
});
