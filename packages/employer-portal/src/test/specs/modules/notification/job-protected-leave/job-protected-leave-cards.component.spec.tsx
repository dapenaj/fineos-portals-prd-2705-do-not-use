/*-------------------------------------------------------------------------------------------
FINEOS Corporation
FINEOS House, EastPoint Business Park,
Dublin 3, Ireland

(c) Copyright FINEOS Corporation.
ALL RIGHTS RESERVED 

The software and information contained herein are proprietary to, and comprise valuable
trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
software and information. This software should only be furnished subject to a written
license agreement and may only be used, copied, transmitted, and stored in accordance
with the terms of such license and with the inclusion of the above copyright notice.
If there is no written License Agreement between you and FINEOS Corporation, then you
have received this software in error and should be returned to FINEOS Corporation or
destroyed immediately, and you should also notify FINEOS Corporation. This software and
information or any other copies thereof may not be provided or otherwise made available
to any person who is not authorized to receive it pursuant to a written license Agreement
executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, render, screen } from '@testing-library/react';
import { ActualAbsencePeriodsService } from 'fineos-js-api-client';
import { ViewNotificationPermissions } from 'fineos-common';

import { ContextWrapper, releaseEventLoop } from '../../../../common/utils';

import { LEAVE_PLAN_FIXTURE } from '../../../../fixtures/leave-plan.fixture';
import { JobProtectedLeaveCards } from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protected-leave-cards.component';
import { wait } from '@testing-library/dom';

describe('JobProtectedLeaveCards', () => {
  const service = ActualAbsencePeriodsService.getInstance();

  beforeEach(() => {
    jest
      .spyOn(service, 'fetchActualAbsencePeriods')
      .mockReturnValue(Promise.resolve([]));
  });

  test('should call fetchActualAbsencePeriods', async () => {
    const spy = jest
      .spyOn(service, 'fetchActualAbsencePeriods')
      .mockReturnValue(Promise.resolve([] as any));
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_ACTUAL_ABSENCE_PERIODS]}
      >
        <JobProtectedLeaveCards
          leavePlans={LEAVE_PLAN_FIXTURE}
          caseHandler={{} as any}
        />
      </ContextWrapper>
    );

    expect(spy).toHaveBeenCalled();

    await act(wait);
  });
});
