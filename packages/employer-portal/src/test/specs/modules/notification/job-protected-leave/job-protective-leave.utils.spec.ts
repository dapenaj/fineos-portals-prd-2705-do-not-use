/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { StatusType, StatusPattern } from 'fineos-common';

import { equalToMoment } from '../../../../common/utils';
import {
  DecisionStatusCategory,
  JobProtectedLeaveStatus,
  JobProtectedAssessmentStatus
} from '../../../../../app/shared';
import {
  getDecisionStatusLabelType,
  getDecisionStatusLabel,
  getDecisionPattern
} from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave-cards/job-protective-leave-cards.util';
import {
  groupByLeavePlanName,
  groupByNoLeavePlanName
} from '../../../../../app/modules/notification/job-protected-leave/job-protected-leave.util';
import {
  DECISION_STATUS_VARIATIONS_FIXTURE,
  CONSECUTIVE_STATUS_VARIATIONS_FIXTURE,
  DECISIONS_WITHOUT_PLAN_NAME_FIXTURE
} from '../../../../fixtures/absence-periods.fixture';

describe('Job Protected Leave Util', () => {
  describe('groupByLeavePlanName', () => {
    test('should determine the correct DecisionStatusCategory', () => {
      const result = groupByLeavePlanName(DECISION_STATUS_VARIATIONS_FIXTURE);

      expect(result[0].decisions.length).toBe(4);

      const expected: DecisionStatusCategory[] = [
        DecisionStatusCategory.APPROVED,
        DecisionStatusCategory.CANCELLED,
        DecisionStatusCategory.DENIED,
        DecisionStatusCategory.PENDING
      ];

      result[0].decisions.forEach(({ statusCategory }, index) =>
        expect(statusCategory).toBe(expected[index])
      );
    });

    test('should merge consecutive periods', () => {
      const result = groupByLeavePlanName(
        CONSECUTIVE_STATUS_VARIATIONS_FIXTURE
      );

      expect(CONSECUTIVE_STATUS_VARIATIONS_FIXTURE.length).toBe(11);
      expect(result[0].decisions.length).toBe(3);

      expect(result[0].decisions[0].period.startDate).toEqual(
        equalToMoment(
          CONSECUTIVE_STATUS_VARIATIONS_FIXTURE[0].period.startDate.toString()
        )
      );

      expect(result[0].decisions[0].period.endDate).toEqual(
        equalToMoment(
          CONSECUTIVE_STATUS_VARIATIONS_FIXTURE[3].period.endDate.toString()
        )
      );
    });
  });

  describe('groupByNoLeavePlanName', () => {
    test('should determine the correct Decision without plan name', () => {
      const result = groupByNoLeavePlanName(
        DECISIONS_WITHOUT_PLAN_NAME_FIXTURE
      );

      expect(result.length).toBe(3);

      const expected: JobProtectedAssessmentStatus[] = [
        JobProtectedAssessmentStatus.DENIED,
        JobProtectedAssessmentStatus.OTHER,
        JobProtectedAssessmentStatus.PENDING
      ];

      result.forEach(({ status }, index) =>
        expect(status).toBe(expected[index])
      );
    });
  });
});

describe('Job Protective Leave Cards Util', () => {
  describe('DecisionStatusCategory', () => {
    const decisions = [
      DecisionStatusCategory.APPROVED,
      DecisionStatusCategory.DENIED,
      DecisionStatusCategory.PENDING,
      DecisionStatusCategory.CANCELLED,
      DecisionStatusCategory.SKIPPED
    ];

    test('should return the correct StatusType', () => {
      const expectations = [
        StatusType.SUCCESS,
        StatusType.DANGER,
        StatusType.WARNING,
        StatusType.NEUTRAL,
        StatusType.BLANK
      ];

      decisions.forEach((decision, index) =>
        expect(getDecisionStatusLabelType(decision)).toBe(expectations[index])
      );
    });

    test('should return the correct label', () => {
      const expectations = [
        'DECISION_STATUS_CATEGORY.APPROVED',
        'DECISION_STATUS_CATEGORY.DENIED',
        'DECISION_STATUS_CATEGORY.PENDING',
        'DECISION_STATUS_CATEGORY.CANCELLED',
        DecisionStatusCategory.SKIPPED
      ];

      decisions.forEach((decision, index) =>
        expect(getDecisionStatusLabel(decision)).toBe(expectations[index])
      );
    });
  });

  describe('JobProtectedLeaveStatus', () => {
    const statuses = [
      JobProtectedLeaveStatus.EPISODIC,
      JobProtectedLeaveStatus.REDUCED_SCHEDULE,
      JobProtectedLeaveStatus.TIME_OFF_PERIOD
    ];

    test('should return the correct StatusPattern', () => {
      const expectations = [
        StatusPattern.ANGLED,
        StatusPattern.HALVED,
        StatusPattern.FULL
      ];

      statuses.forEach((status, index) =>
        expect(getDecisionPattern(status)).toBe(expectations[index])
      );
    });
  });
});
