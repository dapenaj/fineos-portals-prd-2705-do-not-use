/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { StatusType } from 'fineos-common';

import { ACCOMMODATIONS_FIXTURE } from '../../../../fixtures/accommodations.fixture';
import {
  getWorkplaceAccommodationStatusLabel,
  getWorkplaceAccommodationStatus
} from '../../../../../app/modules/notification/workplace-accommodation/workplace-accommodation.util';
import {
  PendingOptionsStage,
  WorkplaceAccommodationPhase,
  WorkplaceAccommodationStatusLabel,
  PendingAssessmentStage,
  NotAccommodatedStage,
  AccommodatedStage,
  PendingRecognizedStage
} from '../../../../../app/shared';

describe('util methods', () => {
  test('getWorkplaceAccommodationStatusLabel', async () => {
    expect(
      getWorkplaceAccommodationStatusLabel(
        PendingOptionsStage.EXPLORE_OPTIONS,
        WorkplaceAccommodationPhase.ASSESSMENT,
        ACCOMMODATIONS_FIXTURE[0]
      )
    ).toEqual(WorkplaceAccommodationStatusLabel.PENDING_OPTIONS);
    expect(
      getWorkplaceAccommodationStatusLabel(
        PendingAssessmentStage.GATHER_ADDITIONAL_INFO,
        WorkplaceAccommodationPhase.ASSESSMENT,
        ACCOMMODATIONS_FIXTURE[0]
      )
    ).toEqual(WorkplaceAccommodationStatusLabel.PENDING_IN_ASSESSMENT);
    expect(
      getWorkplaceAccommodationStatusLabel(
        NotAccommodatedStage.CLOSED_INEFFECTIVE,
        WorkplaceAccommodationPhase.CLOSED,
        ACCOMMODATIONS_FIXTURE[0]
      )
    ).toEqual(WorkplaceAccommodationStatusLabel.ACCOMMODATED);
    expect(
      getWorkplaceAccommodationStatusLabel(
        AccommodatedStage.ACCOMMODATED,
        WorkplaceAccommodationPhase.COMPLETION,
        ACCOMMODATIONS_FIXTURE[0]
      )
    ).toEqual(WorkplaceAccommodationStatusLabel.ACCOMMODATED);
    expect(
      getWorkplaceAccommodationStatusLabel(
        AccommodatedStage.MOVE_TO_CLOSED,
        WorkplaceAccommodationPhase.MONITORING,
        ACCOMMODATIONS_FIXTURE[0]
      )
    ).toEqual(WorkplaceAccommodationStatusLabel.PENDING_IMPLEMENTING);
    expect(
      getWorkplaceAccommodationStatusLabel(
        PendingRecognizedStage.START,
        WorkplaceAccommodationPhase.REGISTRATION,
        ACCOMMODATIONS_FIXTURE[0]
      )
    ).toEqual(WorkplaceAccommodationStatusLabel.PENDING_RECOGNIZED);
  });

  test('getWorkplaceAccommodationStatus', async () => {
    expect(
      getWorkplaceAccommodationStatus(
        WorkplaceAccommodationStatusLabel.PENDING_IN_ASSESSMENT
      )
    ).toEqual(StatusType.WARNING);
    expect(
      getWorkplaceAccommodationStatus(
        WorkplaceAccommodationStatusLabel.ACCOMMODATED
      )
    ).toEqual(StatusType.SUCCESS);
    expect(
      getWorkplaceAccommodationStatus(
        WorkplaceAccommodationStatusLabel.NOT_ACCOMMODATED
      )
    ).toEqual(StatusType.DANGER);
  });
});
