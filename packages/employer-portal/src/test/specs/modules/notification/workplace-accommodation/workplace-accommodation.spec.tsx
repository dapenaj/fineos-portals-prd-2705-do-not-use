/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { render, act, fireEvent } from '@testing-library/react';
import React from 'react';
import { ViewNotificationPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { WorkplaceAccommodation } from '../../../../../app/modules/notification/workplace-accommodation';
import {
  ContextWrapper,
  mockTranslations,
  releaseEventLoop
} from '../../../../common/utils';
import { ACCOMMODATIONS_FIXTURE } from '../../../../fixtures/accommodations.fixture';
import { NOTIFICATIONS_FIXTURE } from '../../../../fixtures/notifications.fixture';

describe('workplace-accommodation-card', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.MANAGED_BY',
    'NOTIFICATIONS.CREATED_ON',
    'NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATION_REQUEST',
    'NOTIFICATIONS.ACCOMMODATIONS.PANEL_TITLE',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.NOT_ACCOMMODATED',
    'NOTIFICATIONS.ACCOMMODATIONS.LIMITATIONS',
    'NOTIFICATIONS.ACCOMMODATIONS.NOTIFIED_BY',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS',
    'NOTIFICATIONS.ACCOMMODATIONS.SHOW_REASON',
    'NOTIFICATIONS.ACCOMMODATIONS.HIDE_REASON',
    'NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATIONS_PROPOSED',
    'NOTIFICATIONS.ACCOMMODATIONS.REQUEST_DATE',
    'NOTIFICATIONS.ACCOMMODATIONS.DESCRIPTION',
    'NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATION_DATE',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_ASSESSMENT'
  );

  const permissions = [ViewNotificationPermissions.VIEW_ACCOMMODATION_DETAILS];

  describe('without permissions', () => {
    test('should not be rendered', () => {
      const { queryByTestId } = render(
        <ContextWrapper>
          <WorkplaceAccommodation
            isLoading={false}
            accommodations={[]}
            notification={NOTIFICATIONS_FIXTURE[0]}
          />
        </ContextWrapper>
      );

      expect(
        queryByTestId('workplace-accommodation-panel')
      ).not.toBeInTheDocument();
    });
  });

  describe('with permissions', () => {
    test('should render collapsed', async () => {
      const { queryByTestId, getAllByTestId } = render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <WorkplaceAccommodation
            isLoading={false}
            accommodations={ACCOMMODATIONS_FIXTURE}
            notification={NOTIFICATIONS_FIXTURE[0]}
          />
        </ContextWrapper>
      );

      expect(queryByTestId('panel-header')).toHaveTextContent(
        'NOTIFICATIONS.ACCOMMODATIONS.PANEL_TITLE'
      );
      expect(getAllByTestId('status-label')[0]).toHaveTextContent(
        'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.NOT_ACCOMMODATED'
      );
      expect(getAllByTestId('status-label')[1]).toHaveTextContent(
        'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_ASSESSMENT'
      );
    });

    test('should render expanded', async () => {
      const { queryByTestId, container, getByText } = render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <WorkplaceAccommodation
            isLoading={false}
            accommodations={ACCOMMODATIONS_FIXTURE}
            notification={NOTIFICATIONS_FIXTURE[0]}
          />
        </ContextWrapper>
      );

      await act(() => {
        userEvent.click(container.querySelector('.ant-collapse-header')!);
        return releaseEventLoop();
      });

      expect(queryByTestId('accommodation-properties')).toHaveTextContent(
        'Color Blindness'
      );

      await act(() => {
        fireEvent.click(
          getByText(content => content.startsWith('Work from home'))
        );
        return releaseEventLoop();
      });

      expect(
        queryByTestId('proposed-accommodation-properties')
      ).toBeInTheDocument();
    });

    test('show reason', async () => {
      const { queryByTestId, container } = render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <WorkplaceAccommodation
            isLoading={false}
            accommodations={ACCOMMODATIONS_FIXTURE}
            notification={NOTIFICATIONS_FIXTURE[0]}
          />
        </ContextWrapper>
      );

      await act(() => {
        userEvent.click(container.querySelector('.ant-collapse-header')!);
        return releaseEventLoop();
      });

      await act(() => {
        userEvent.click(queryByTestId('show-reason')!);
        return releaseEventLoop();
      });
      expect(queryByTestId('closed-accommodation-reason')).toBeInTheDocument();

      await act(() => {
        userEvent.click(queryByTestId('hide-reason')!);
        return releaseEventLoop();
      });
      expect(
        queryByTestId('closed-accommodation-reason')
      ).not.toBeInTheDocument();
    });
  });
});
