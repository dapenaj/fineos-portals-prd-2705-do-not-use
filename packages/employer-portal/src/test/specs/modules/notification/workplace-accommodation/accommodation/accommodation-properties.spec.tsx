/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, within } from '@testing-library/react';

import { AccommodationProperties } from '../../../../../../app/modules/notification/workplace-accommodation/accommodation/accommodation-properties.component';
import { ContextWrapper, mockTranslations } from '../../../../../common/utils';
import { ACCOMMODATIONS_FIXTURE } from '../../../../../fixtures/accommodations.fixture';

describe('accommodation-properties', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.ACCOMMODATIONS.LIMITATIONS',
    'NOTIFICATIONS.ACCOMMODATIONS.NOTIFIED_BY',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.ACCOMMODATED',
    'NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATIONS_PROPOSED',
    'NOTIFICATIONS.ACCOMMODATIONS.EMPTY'
  );

  test('should render properties', () => {
    const { getByText, queryByText, getAllByTestId } = render(
      <ContextWrapper translations={translations}>
        <AccommodationProperties
          accommodation={ACCOMMODATIONS_FIXTURE[0]}
          statusLabel="NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.ACCOMMODATED"
        />
      </ContextWrapper>
    );

    const items = getAllByTestId('property-item');

    expect(items.length).toBe(3);

    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.ACCOMMODATIONS.LIMITATIONS');
    expect(
      within(items[0]).getByTestId('property-item-value')
    ).toHaveTextContent('- Color Blindness');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.ACCOMMODATIONS.NOTIFIED_BY');
    expect(
      within(items[1]).getByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.ACCOMMODATIONS.STATUS');
    expect(
      within(items[2]).getByTestId('property-item-value')
    ).toHaveTextContent(
      'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.ACCOMMODATED'
    );

    expect(
      getByText('NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATIONS_PROPOSED')
    ).toBeInTheDocument();
    expect(
      queryByText('NOTIFICATIONS.ACCOMMODATIONS.EMPTY')
    ).not.toBeInTheDocument();
  });

  test('should render empty properties', () => {
    const { getByText, getAllByTestId } = render(
      <ContextWrapper translations={translations}>
        <AccommodationProperties
          accommodation={ACCOMMODATIONS_FIXTURE[1]}
          statusLabel="NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.ACCOMMODATED"
        />
      </ContextWrapper>
    );

    const items = getAllByTestId('property-item');

    expect(items.length).toBe(3);

    expect(
      within(items[0]).queryByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.ACCOMMODATIONS.LIMITATIONS');
    expect(
      within(items[0]).queryByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(getByText('NOTIFICATIONS.ACCOMMODATIONS.EMPTY')).toBeInTheDocument();
  });
});
