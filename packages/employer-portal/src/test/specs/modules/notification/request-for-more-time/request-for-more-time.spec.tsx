/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, wait, fireEvent, act, screen } from '@testing-library/react';

import {
  ContextWrapper,
  releaseEventLoop,
  selectDateRange
} from '../../../../common/utils';
import { RequestForMoreTime } from '../../../../../app/modules/notification/request-for-more-time/request-for-more-time.component';
import * as LeavePeriodChangeFixture from '../../../../fixtures/leave-period-change.fixture';
import * as requestForMoreTimeModalApiModule from '../../../../../app/modules/notification/request-for-more-time/request-for-more-time-modal/request-for-more-time-modal.api';

jest.mock(
  '../../../../../app/modules/notification/request-for-more-time/request-for-more-time-modal/request-for-more-time-modal.api',
  () => ({
    leavePeriodChangeRequest: jest.fn()
  })
);

const leavePeriodChangeRequest = requestForMoreTimeModalApiModule.leavePeriodChangeRequest as jest.Mock;

describe('RequestForMoreTime', () => {
  test('should render', async () => {
    render(
      <ContextWrapper>
        <RequestForMoreTime
          absenceId="1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={jest.fn()}
        />
      </ContextWrapper>
    );

    // wait to avoid Formik's warning
    await wait();

    expect(screen.getByText('REQUEST_FOR_MORE_TIME.TITLE')).toBeInTheDocument();
    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.EXTRA_TIME_LABEL')
    ).toBeInTheDocument();
    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.DESCRIPTION_LABEL')
    ).toBeInTheDocument();
  });

  test('should render success modal', async () => {
    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-01-01').valueOf());

    leavePeriodChangeRequest.mockReturnValueOnce(Promise.resolve({}));

    render(
      <ContextWrapper>
        <RequestForMoreTime
          absenceId="1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={jest.fn()}
        />
      </ContextWrapper>
    );
    await wait();

    await selectDateRange(screen.getByTestId('range-picker'), [
      '2020-01-02',
      '2020-01-03'
    ]);

    expect(screen.getByTestId('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();

    await act(() => {
      fireEvent.submit(screen.getByTestId('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.SUCCESS_HEAD')
    ).toBeInTheDocument();
    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.SUCCESS_MESSAGE')
    ).toBeInTheDocument();
  });
});
