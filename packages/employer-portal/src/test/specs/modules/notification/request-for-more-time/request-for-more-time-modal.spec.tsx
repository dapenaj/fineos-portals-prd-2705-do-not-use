/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';
import moment from 'moment';

import { render, wait, act } from '../../../../common/custom-render';
import {
  ContextWrapper,
  selectDateRange,
  releaseEventLoop
} from '../../../../common/utils';
import { RequestForMoreTimeModal } from '../../../../../app/modules/notification/request-for-more-time/request-for-more-time-modal/request-for-more-time-modal.component';
import * as apiModule from '../../../../../app/modules/notification/request-for-more-time/request-for-more-time-modal/request-for-more-time-modal.api';
import * as LeavePeriodChangeFixture from '../../../../fixtures/leave-period-change.fixture';

jest.mock(
  '../../../../../app/modules/notification/request-for-more-time/request-for-more-time-modal/request-for-more-time-modal.api',
  () => ({
    leavePeriodChangeRequest: jest.fn()
  })
);

const leavePeriodChangeRequest = apiModule.leavePeriodChangeRequest as jest.Mock;

describe('RequestForMoreTimeModal', () => {
  test('should render', async () => {
    const { getByTestId, getByText } = render(
      <ContextWrapper>
        <RequestForMoreTimeModal
          absenceId="ABS-1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={jest.fn()}
          onFormSubmit={jest.fn()}
        />
      </ContextWrapper>
    );
    // wait to avoid Formik's warning
    await wait();

    expect(getByTestId('request-for-more-time-modal')).toBeInTheDocument();
    expect(getByText('REQUEST_FOR_MORE_TIME.TITLE')).toBeInTheDocument();
    expect(
      getByText('REQUEST_FOR_MORE_TIME.EXTRA_TIME_LABEL')
    ).toBeInTheDocument();
    expect(getByTestId('range-picker')).toBeInTheDocument();
    expect(
      getByText('REQUEST_FOR_MORE_TIME.DESCRIPTION_LABEL')
    ).toBeInTheDocument();
    expect(getByText('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();
    expect(getByText('FINEOS_COMMON.GENERAL.CANCEL')).toBeInTheDocument();
  });

  test('should cancel modal', async () => {
    const mockOnClose = jest.fn();

    const { getByText } = render(
      <ContextWrapper>
        <RequestForMoreTimeModal
          absenceId="ABS-1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={mockOnClose}
          onFormSubmit={jest.fn()}
        />
      </ContextWrapper>
    );

    // wait to avoid Formik's warning
    await wait();

    userEvent.click(getByText('FINEOS_COMMON.GENERAL.CANCEL'));

    expect(mockOnClose).toHaveBeenCalled();
  });

  test('should validate form', async () => {
    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-01-01').valueOf());

    const { getByText, getByTestId, getByLabelText } = render(
      <ContextWrapper>
        <RequestForMoreTimeModal
          absenceId="ABS-1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={jest.fn()}
          onFormSubmit={jest.fn()}
        />
      </ContextWrapper>
    );

    await wait();

    await act(() => {
      userEvent.click(getByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(getByTestId('range-picker')).not.toBeValid();

    await selectDateRange(getByTestId('range-picker'), [
      '2020-01-02',
      '2020-01-04'
    ]);
    await act(() => {
      userEvent.click(getByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    await wait();

    await selectDateRange(getByTestId('range-picker'), [
      '2020-01-02',
      '2020-01-03'
    ]);

    userEvent.type(
      getByLabelText('REQUEST_FOR_MORE_TIME.DESCRIPTION_LABEL'),
      'reason'
    );
  });

  test('should submit', async () => {
    leavePeriodChangeRequest.mockReturnValueOnce(Promise.resolve());

    const mockOnFormSubmit = jest.fn();

    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-01-01').valueOf());

    const { getByLabelText, getByTestId, getSubmitButtonByText } = render(
      <ContextWrapper>
        <RequestForMoreTimeModal
          absenceId="ABS-1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={jest.fn()}
          onFormSubmit={mockOnFormSubmit}
        />
      </ContextWrapper>
    );

    await wait();

    await selectDateRange(getByTestId('range-picker'), [
      '2020-01-02',
      '2020-01-03'
    ]);

    userEvent.type(
      getByLabelText('REQUEST_FOR_MORE_TIME.DESCRIPTION_LABEL'),
      'reason'
    );

    await wait();

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await wait();

    expect(mockOnFormSubmit).toHaveBeenCalled();
    expect(leavePeriodChangeRequest).toHaveBeenCalledWith(
      'ABS-1',
      expect.objectContaining({
        additionalNotes: 'reason',
        changeRequestPeriods: [
          {
            endDate: expect.any(moment),
            startDate: expect.any(moment)
          }
        ],
        reason: 'Add time for unknown Absence Reason'
      })
    );
  });

  test('should handle error', async () => {
    leavePeriodChangeRequest.mockImplementationOnce(() =>
      Promise.reject(new Error())
    );

    const mockOnFormSubmit = jest.fn();

    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-01-01').valueOf());

    const {
      getByText,
      queryByText,
      getByTestId,
      getSubmitButtonByText
    } = render(
      <ContextWrapper>
        <RequestForMoreTimeModal
          absenceId="ABS-1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD
          }
          onCancel={jest.fn()}
          onFormSubmit={mockOnFormSubmit}
        />
      </ContextWrapper>
    );

    await wait();

    await selectDateRange(getByTestId('range-picker'), [
      '2020-01-02',
      '2020-01-03'
    ]);

    await wait();

    expect(
      queryByText('FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_GENERAL')
    ).not.toBeInTheDocument();

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await wait();

    expect(mockOnFormSubmit).not.toHaveBeenCalled();

    expect(
      getByText('FINEOS_COMMON.ASYNC_ERRORS.MESSAGE_GENERAL')
    ).toBeInTheDocument();
  });

  test('should render proper periods in range picker', async () => {
    jest
      .spyOn(Date, 'now')
      .mockImplementation(() => new Date('2020-01-01').valueOf());

    const { getByTestId, getByTitle } = render(
      <ContextWrapper>
        <RequestForMoreTimeModal
          absenceId="ABS-1"
          absencePeriod={
            LeavePeriodChangeFixture.ABSENCE_PERIODS_FIXTURE_MULTIPLE_PERIODS[0]
          }
          onCancel={jest.fn()}
          onFormSubmit={jest.fn()}
        />
      </ContextWrapper>
    );

    await wait();

    userEvent.click(getByTestId('range-picker').querySelector('input')!);

    // January 1st and before
    expect(getByTitle('2019-12-31')).toHaveClass('ant-picker-cell-disabled');
    expect(getByTitle('2020-01-01')).not.toHaveClass(
      'ant-picker-cell-disabled'
    );

    // Approved period - January 2nd, 3rd and 4th
    const approved1Cell = getByTitle('2020-01-02');
    const approved1 = approved1Cell.querySelector('div');

    expect(approved1Cell).toHaveClass('ant-picker-cell-disabled');
    expect(approved1).toHaveClass('periodStart');
    expect(approved1?.className).toMatch(/green/);
    expect(approved1).toHaveClass('semi-filled');
    expect(approved1).not.toHaveClass('periodEnd');

    const approved2Cell = getByTitle('2020-01-03');
    const approved2 = approved2Cell.querySelector('div');

    expect(approved2Cell).toHaveClass('ant-picker-cell-disabled');
    expect(approved2).not.toHaveClass('periodStart');
    expect(approved2?.className).toMatch(/green/);
    expect(approved2).toHaveClass('semi-filled');
    expect(approved2).not.toHaveClass('periodEnd');

    const approved3Cell = getByTitle('2020-01-04');
    const approved3 = approved3Cell.querySelector('div');

    expect(approved3Cell).toHaveClass('ant-picker-cell-disabled');
    expect(approved3).not.toHaveClass('periodStart');
    expect(approved3?.className).toMatch(/green/);
    expect(approved3).toHaveClass('semi-filled');
    expect(approved3).toHaveClass('periodEnd');

    // Pending period - January 5th, 6th and 7th
    const pending1Cell = getByTitle('2020-01-05');
    const pending1 = pending1Cell.querySelector('div');

    expect(pending1Cell).toHaveClass('ant-picker-cell-disabled');
    expect(pending1).toHaveClass('periodStart');
    expect(pending1?.className).toMatch(/yellow/);
    expect(pending1).toHaveClass('stripes');
    expect(pending1).not.toHaveClass('periodEnd');

    const pending2Cell = getByTitle('2020-01-06');
    const pending2 = pending2Cell.querySelector('div');

    expect(pending2Cell).toHaveClass('ant-picker-cell-disabled');
    expect(pending2).not.toHaveClass('periodStart');
    expect(pending2?.className).toMatch(/yellow/);
    expect(pending2).toHaveClass('stripes');
    expect(pending2).not.toHaveClass('periodEnd');

    const pending3Cell = getByTitle('2020-01-07');
    const pending3 = pending3Cell.querySelector('div');

    expect(pending3Cell).toHaveClass('ant-picker-cell-disabled');
    expect(pending3).not.toHaveClass('periodStart');
    expect(pending3?.className).toMatch(/yellow/);
    expect(pending3).toHaveClass('stripes');
    expect(pending3).toHaveClass('periodEnd');

    // Declined period - January 8th
    const declined1Cell = getByTitle('2020-01-08');
    const declined1 = declined1Cell.querySelector('div');

    expect(declined1Cell).toHaveClass('ant-picker-cell-disabled');
    expect(declined1).toHaveClass('periodStart');
    expect(declined1?.className).toMatch(/red/);
    expect(declined1).toHaveClass('filled');
    expect(declined1).toHaveClass('periodEnd');

    // Closed period - January 9th
    const closed1Cell = getByTitle('2020-01-09');
    const closed1 = closed1Cell.querySelector('div');

    expect(closed1Cell).not.toHaveClass('ant-picker-cell-disabled');
    expect(closed1).not.toHaveClass('periodStart');
    expect(closed1).not.toHaveClass('periodEnd');

    // No period - January 10th
    expect(getByTitle('2020-01-10')).not.toHaveClass(
      'ant-picker-cell-disabled'
    );
  });
});
