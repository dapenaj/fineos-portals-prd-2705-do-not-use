/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ContextWrapper } from '../../../../common/utils';
import { RequestForMoreTimePopover } from '../../../../../app/modules/notification/request-for-more-time/request-for-more-time-popover/request-for-more-time-popover.component';

describe('RequestForMoreTimePopover', () => {
  const testNotificationReason = {
    domainId: 123,
    fullId: 123,
    name: 'test',
    domainName: 'test'
  };

  test('should render', async () => {
    render(
      <ContextWrapper>
        <RequestForMoreTimePopover
          onConfirm={jest.fn()}
          notificationReason={testNotificationReason}
        >
          test
        </RequestForMoreTimePopover>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.queryByText('REQUEST_FOR_MORE_TIME.POPOVER_HEADER')
    ).not.toBeInTheDocument();

    userEvent.click(screen.getByText('test'));

    await wait();

    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.POPOVER_HEADER')
    ).toBeInTheDocument();
    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.POPOVER_BODY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('REQUEST_FOR_MORE_TIME.POPOVER_PROCEED')
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
  });

  test('should confirm', async () => {
    const mockOnConfirm = jest.fn();

    render(
      <ContextWrapper>
        <RequestForMoreTimePopover
          onConfirm={mockOnConfirm}
          notificationReason={testNotificationReason}
        >
          test
        </RequestForMoreTimePopover>
      </ContextWrapper>
    );

    await wait();

    userEvent.click(screen.getByText('test'));

    await wait();

    expect(
      screen.getByTestId('request-for-more-time-popover-open')
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('REQUEST_FOR_MORE_TIME.POPOVER_PROCEED'));

    await wait();

    expect(
      screen.queryByText('request-for-more-time-popover-open')
    ).not.toBeInTheDocument();
    expect(mockOnConfirm).toBeCalled();
  });

  test('should cancel', async () => {
    render(
      <ContextWrapper>
        <RequestForMoreTimePopover
          onConfirm={jest.fn()}
          notificationReason={testNotificationReason}
        >
          test
        </RequestForMoreTimePopover>
      </ContextWrapper>
    );

    userEvent.click(screen.getByText('test'));

    await wait();

    expect(
      screen.getByTestId('request-for-more-time-popover-open')
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

    await wait();

    expect(
      screen.queryByText('request-for-more-time-popover-open')
    ).not.toBeInTheDocument();
  });
});
