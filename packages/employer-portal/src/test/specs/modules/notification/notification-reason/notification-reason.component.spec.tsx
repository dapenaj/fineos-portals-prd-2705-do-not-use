/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { ContextWrapper } from '../../../../common/utils';
import { NotificationReason } from '../../../../../app/modules/notification/notification-reason';
import { NOTIFICATIONS_FIXTURE } from '../../../../fixtures/notifications.fixture';

describe('NotificationReason', () => {
  const unknownReason = 'Unknown';
  const accidentOrTreatmentReason = 'Accident or treatment';
  const mockTranslations = {
    'ENUM_DOMAINS.NOTIFICATION_REASON.UNKNOWN': unknownReason,
    'ENUM_DOMAINS.NOTIFICATION_REASON.ACCIDENT_OR_TREATMENT_REQUIRED_FOR_AN_INJURY': accidentOrTreatmentReason
  };
  test('should render value other than Unknown', () => {
    render(
      <ContextWrapper translations={mockTranslations}>
        <NotificationReason
          reason={NOTIFICATIONS_FIXTURE[0].notificationReason.name}
        />
      </ContextWrapper>
    );

    expect(screen.getByText(accidentOrTreatmentReason)).toBeInTheDocument();
  });

  test('should render Unknown value', () => {
    render(
      <ContextWrapper translations={mockTranslations}>
        <NotificationReason reason={unknownReason} />
      </ContextWrapper>
    );

    expect(screen.getByText(unknownReason)).toBeInTheDocument();
  });
});
