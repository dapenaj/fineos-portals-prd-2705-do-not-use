/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import { ContextWrapper, mockTranslations } from '../../../../common/utils';
import { Timeline } from '../../../../../app/modules/notification/timeline';
import timelineStyles from '../../../../../app/modules/notification/timeline/timeline.module.scss';
import { TIMELINE_ACCOMMODATIONS_FIXTURE } from '../../../../fixtures/accommodations.fixture';

describe('Timeline', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.TIMELINE.TITLE',
    'NOTIFICATIONS.TIMELINE.GANTT_TITLE',
    'NOTIFICATIONS.TYPE.WORKPLACE_ACCOMMODATION',
    'NOTIFICATIONS.TIMELINE.SHOW_CAPTION',
    'NOTIFICATIONS.TIMELINE.CLOSE_CAPTION',
    'NOTIFICATIONS.TYPE.TEMPORARY_ACCOMMODATIONS'
  );

  test('should not show timeline if no data provided', () => {
    render(
      <ContextWrapper translations={translations}>
        <Timeline
          enhancedAbsencePeriodDecisions={[]}
          claimBenefits={[]}
          paidLeaveBenefits={[]}
          accommodations={[]}
          isLoading={false}
        />
      </ContextWrapper>
    );

    expect(screen.queryByTestId('timeline')).not.toBeInTheDocument();
  });

  test('should have possibility to toggle legend', () => {
    render(
      <ContextWrapper translations={translations}>
        <Timeline
          enhancedAbsencePeriodDecisions={[]}
          claimBenefits={[]}
          paidLeaveBenefits={[]}
          accommodations={TIMELINE_ACCOMMODATIONS_FIXTURE}
          isLoading={false}
        />
      </ContextWrapper>
    );

    expect(
      screen
        .getByTestId('timeline-legend')
        .classList.contains(timelineStyles.open)
    ).toBe(false);

    fireEvent.click(screen.getByText('NOTIFICATIONS.TIMELINE.SHOW_CAPTION'));

    expect(
      screen
        .getByTestId('timeline-legend')
        .classList.contains(timelineStyles.open)
    ).toBe(true);

    fireEvent.click(screen.getByText('NOTIFICATIONS.TIMELINE.CLOSE_CAPTION'));

    expect(
      screen
        .getByTestId('timeline-legend')
        .classList.contains(timelineStyles.open)
    ).toBe(false);
  });
});
