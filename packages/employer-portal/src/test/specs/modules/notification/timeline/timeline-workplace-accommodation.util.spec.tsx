/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  MultipleTokens,
  GanttEntryShape,
  GanttEntryColor
} from 'fineos-common';

import { buildTimelineWorkplaceAccommodationsRows } from '../../../../../app/modules/notification/timeline/timeline-workplace-accommodation.util';
import { TIMELINE_ACCOMMODATIONS_FIXTURE } from '../../../../fixtures/accommodations.fixture';
import { equalToJsx, equalToMoment } from '../../../../common/utils';

describe('buildTimelineWorkplaceAccommodationsRows', () => {
  test('should build workplace accommodations gantt rows', () => {
    expect(
      buildTimelineWorkplaceAccommodationsRows(TIMELINE_ACCOMMODATIONS_FIXTURE)
    ).toEqual([
      {
        id: 'workplaceAccommodation-A001-0',
        title: equalToJsx(MultipleTokens, {
          tokens: ['Work from home', '1 day per week']
        }),
        entries: [
          {
            id: 'workplaceAccommodation-A001-0-entry',
            color: GanttEntryColor.RED,
            shape: GanttEntryShape.FILLED,
            startDate: equalToMoment('1999-12-31T00:00:00.000Z'),
            endDate: equalToMoment('2000-01-19T00:00:00.000Z'),
            tooltip: null
          }
        ]
      }
    ]);
  });
});
