/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  ConfigurableText,
  GanttEntryColor,
  GanttEntryShape,
  FormattedMessage
} from 'fineos-common';
import moment from 'moment';

import { buildTimelineJobProtectedLeaveRows } from '../../../../../app/modules/notification/timeline/timeline-job-protected-leave.util';
import { equalToJsx, equalToMoment } from '../../../../common/utils';
import {
  ABSENCE_PERIOD_DECISIONS_FIXTURE,
  CONCLUDED_ASSESSMENTS_FIXTURE,
  PENDING_ASSESSMENTS_FIXTURE
} from '../../../../fixtures/absence-periods.fixture';
import { EnhancedAbsencePeriodDecisions } from '../../../../../app/shared/types';

describe('buildTimelineJobProtectedLeaveRows', () => {
  test('should build gantt group rows', () => {
    expect(
      buildTimelineJobProtectedLeaveRows([
        ...CONCLUDED_ASSESSMENTS_FIXTURE,
        ...PENDING_ASSESSMENTS_FIXTURE
      ])
    ).toEqual([]);

    expect(
      buildTimelineJobProtectedLeaveRows(ABSENCE_PERIOD_DECISIONS_FIXTURE)
    ).toEqual([
      {
        id: 'jobProtectedLeaveGroup-ABS-2',
        title: equalToJsx(FormattedMessage, {
          id: 'NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE'
        }),
        rows: [
          {
            id: 'Federal FMLA',
            title: 'Federal FMLA',
            entries: [
              {
                color: GanttEntryColor.GREEN,
                id: 'jobProtectedLeave-ABS-2-Federal FMLA-0',
                shape: GanttEntryShape.SEMI_FILLED,
                startDate: equalToMoment('2020-02-05T00:00:00.000Z'),
                endDate: equalToMoment('2020-06-06T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_APPROVED_REDUCED'
                })
              },
              {
                id: 'jobProtectedLeave-ABS-2-Federal FMLA-1',
                color: GanttEntryColor.GREEN,
                shape: GanttEntryShape.STRIPES,
                startDate: equalToMoment('2020-01-01T00:00:00.000Z'),
                endDate: equalToMoment('2020-12-08T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_APPROVED_INTERMITTENT'
                })
              },
              {
                id: 'jobProtectedLeave-ABS-2-Federal FMLA-2',
                color: GanttEntryColor.YELLOW,
                shape: GanttEntryShape.FILLED,
                startDate: equalToMoment('2020-01-01T00:00:00.000Z'),
                endDate: equalToMoment('2020-03-25T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_PENDING_CONTINUOUS'
                })
              }
            ]
          }
        ]
      }
    ]);

    expect(
      buildTimelineJobProtectedLeaveRows([
        ...ABSENCE_PERIOD_DECISIONS_FIXTURE,
        ...[
          {
            absenceId: 'ABS-1',
            caseHandler: {
              name: 'Kylo Ren',
              telephoneNo: '01-111-123456',
              emailAddress: 'kyloren@thefirstorder.com'
            },
            absencePeriodDecisions: {
              startDate: moment(new Date('2020-02-05')),
              endDate: moment(new Date('2020-06-06')),
              decisions: [
                {
                  absence: {
                    id: '1',
                    caseReference: 'ABS-1'
                  },
                  employee: {
                    id: 'EMP-1',
                    name: 'Luke Skywalker'
                  },
                  period: {
                    periodReference: '',
                    parentPeriodReference: '',
                    relatedToEpisodic: false,
                    startDate: moment(new Date('2020-02-05')),
                    endDate: moment(new Date('2020-06-06')),
                    balanceDeduction: 0,
                    timeRequested: '',
                    timeDecisionStatus: 'Approved',
                    timeDecisionReason: '',
                    type: 'Reduced Schedule',
                    status: 'Approved',
                    leavePlan: {
                      paidLeaveCaseId: '',
                      name: 'Federal FMLA',
                      shortName: 'FMLA',
                      adjudicationStatus: 'Accepted'
                    },
                    leaveRequest: {
                      reasonName: 'Pregnancy/Maternity',
                      decisionStatus: 'Approved',
                      qualifier1: 'Prenatal Care'
                    }
                  }
                }
              ]
            }
          } as EnhancedAbsencePeriodDecisions
        ]
      ])
    ).toEqual([
      {
        id: 'jobProtectedLeaveGroup-ABS-2',
        title: equalToJsx(FormattedMessage, {
          id: 'NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE'
        }),
        rows: [
          {
            id: 'Federal FMLA',
            title: 'Federal FMLA',
            entries: [
              {
                color: GanttEntryColor.GREEN,
                id: 'jobProtectedLeave-ABS-2-Federal FMLA-0',
                shape: GanttEntryShape.SEMI_FILLED,
                startDate: equalToMoment('2020-02-05T00:00:00.000Z'),
                endDate: equalToMoment('2020-06-06T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_APPROVED_REDUCED'
                })
              },
              {
                id: 'jobProtectedLeave-ABS-2-Federal FMLA-1',
                color: GanttEntryColor.GREEN,
                shape: GanttEntryShape.STRIPES,
                startDate: equalToMoment('2020-01-01T00:00:00.000Z'),
                endDate: equalToMoment('2020-12-08T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_APPROVED_INTERMITTENT'
                })
              },
              {
                id: 'jobProtectedLeave-ABS-2-Federal FMLA-2',
                color: GanttEntryColor.YELLOW,
                shape: GanttEntryShape.FILLED,
                startDate: equalToMoment('2020-01-01T00:00:00.000Z'),
                endDate: equalToMoment('2020-03-25T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_PENDING_CONTINUOUS'
                })
              }
            ]
          }
        ]
      },
      {
        id: 'jobProtectedLeaveGroup-ABS-1',
        title: equalToJsx(FormattedMessage, {
          id: 'NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE'
        }),
        rows: [
          {
            id: 'Federal FMLA',
            title: 'Federal FMLA',
            entries: [
              {
                color: GanttEntryColor.GREEN,
                id: 'jobProtectedLeave-ABS-1-Federal FMLA-0',
                shape: GanttEntryShape.SEMI_FILLED,
                startDate: equalToMoment('2020-02-05T00:00:00.000Z'),
                endDate: equalToMoment('2020-06-06T00:00:00.000Z'),
                tooltip: equalToJsx(ConfigurableText, {
                  id:
                    'NOTIFICATIONS.TIMELINE.TOOLTIPS.JOB_PROTECTED_LEAVE_APPROVED_REDUCED'
                })
              }
            ]
          }
        ]
      }
    ]);
  });
});
