/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GanttEntryColor, GanttEntryShape } from 'fineos-common';
import { createIntl } from 'react-intl';

import { buildTimelineWageReplacementRows } from '../../../../../app/modules/notification/timeline/timeline-wage-replacement.util';
import { CLAIM_DISABILITY_BENEFITS_FIXTURE } from '../../../../fixtures/claim-disability-benefits.fixture';
import { PAID_LEAVE_BENEFITS_FIXTURE } from '../../../../fixtures/paid-leave-benefits.fixture';

describe('buildTimelineWageReplacementRows', () => {
  test('should build wage replacement gantt rows', () => {
    const rows = buildTimelineWageReplacementRows(
      CLAIM_DISABILITY_BENEFITS_FIXTURE,
      PAID_LEAVE_BENEFITS_FIXTURE,
      createIntl({ locale: 'en' })
    );

    expect(rows).toEqual([
      {
        id: 'wageReplacement-Short Term Disability 2-0',
        title: 'Short Term Disability 2',
        entries: [
          {
            id: 'wageReplacement-Short Term Disability 2-0-0-entry',
            color: GanttEntryColor.YELLOW,
            shape: GanttEntryShape.FILLED,
            label: 'DISABILITY_BENEFIT_STATUS.PENDING',
            timeless: true
          },
          {
            id: 'wageReplacement-Short Term Disability 2-0-1-entry',
            color: GanttEntryColor.YELLOW,
            shape: GanttEntryShape.FILLED,
            label: 'DISABILITY_BENEFIT_STATUS.PENDING',
            timeless: true
          }
        ]
      },
      {
        id: 'wageReplacement-Short Term Disability 3-1',
        title: 'Short Term Disability 3',
        entries: [
          {
            id: 'wageReplacement-Short Term Disability 3-1-0-entry',
            color: GanttEntryColor.YELLOW,
            shape: GanttEntryShape.FILLED,
            label: 'DISABILITY_BENEFIT_STATUS.PENDING',
            timeless: true
          }
        ]
      },
      {
        id: 'wageReplacement-Short Term Disability 1-0',
        title: 'Short Term Disability 1',
        entries: [
          {
            id: 'wageReplacement-Short Term Disability 1-0-0-entry',
            label: 'DISABILITY_BENEFIT_STATUS.PENDING',
            color: GanttEntryColor.YELLOW,
            shape: GanttEntryShape.SEMI_FILLED,
            timeless: true
          },
          {
            id: 'wageReplacement-Short Term Disability 1-0-1-entry',
            label: 'DISABILITY_BENEFIT_STATUS.PENDING',
            color: GanttEntryColor.YELLOW,
            shape: GanttEntryShape.SEMI_FILLED,
            timeless: true
          }
        ]
      },
      {
        id: 'wageReplacement-Short Term Disability 1-1',
        title: 'Short Term Disability 2',
        entries: [
          {
            id: 'wageReplacement-Short Term Disability 1-1-0-entry',
            label: 'DISABILITY_BENEFIT_STATUS.PENDING',
            color: GanttEntryColor.YELLOW,
            shape: GanttEntryShape.SEMI_FILLED,
            timeless: true
          }
        ]
      }
    ]);
  });
});
