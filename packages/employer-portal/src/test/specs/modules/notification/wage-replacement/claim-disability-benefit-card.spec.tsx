/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import moment from 'moment';
import { DisabilityBenefit } from 'fineos-js-api-client';
import { ViewNotificationPermissions } from 'fineos-common';
import { cloneDeep as _cloneDeep } from 'lodash';

import { ContextWrapper, releaseEventLoop } from '../../../../common/utils';
import { PAYMENTS_FIXTURE } from '../../../../fixtures/payments.fixture';
import { DISABILITY_BENEFIT_FIXTURE } from '../../../../fixtures/group-client-claim.fixture';
import { BenefitGroup } from '../../../../../app/shared/types';
import { WageReplacementCards } from '../../../../../app/modules/notification/wage-replacement/wage-replacement-cards';
import * as paymentsApiModule from '../../../../../app/modules/notification/latest-payments/latest-payments.api';

describe('ClaimDisabilityBenefit Card', () => {
  const permissions = [
    ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST,
    ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_BENEFIT
  ];

  const groupedClaimBenefits = [
    {
      name: 'STD Benefit',
      benefitId: 1,
      status: 'Decided',
      stageName: 'Approved',
      benefitHandler: 'Benny Smith',
      benefitHandlerPhoneNo: '1-234-567890',
      benefitCaseType: 'STD Benefit',
      benefits: [
        {
          benefitCaseType: 'STD Benefit',
          benefitHandler: 'Benny Smith',
          benefitHandlerEmailAddress: '',
          benefitHandlerPhoneNo: '1-234-567890',
          benefitId: 1,
          benefitIncurredDate: moment('2019-01-20'),
          benefitRightCategory: 'Lump Sum Benefit',
          creationDate: moment('2019-01-10'),
          customerName: 'bob bobbington',
          description: '',
          policyReferences: '',
          stageName: 'Approved',
          status: 'Decided',
          extensionAttributes: [],
          claimId: 'CLM01'
        }
      ]
    }
  ] as BenefitGroup[];

  beforeEach(() => {
    jest.spyOn(paymentsApiModule, 'fetchPayments');
  });

  test('should render collapsed', async () => {
    const { getByTestId, getAllByTestId } = render(
      <ContextWrapper permissions={permissions}>
        <WageReplacementCards benefitGroup={groupedClaimBenefits} />
      </ContextWrapper>
    );

    const element = getAllByTestId('collapsible-card');

    expect(getByTestId('wage-replacement-cards')).toBeInTheDocument();
    expect(element[0]).toBeInTheDocument();

    expect(getByTestId('disability-benefit-header-name')).toHaveTextContent(
      'DISABILITY_BENEFIT_TYPE.STD_ABBR Benefit'
    );
    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.APPROVED (1)'
    );
  });

  test('should render groups', async () => {
    const extendedGroups = _cloneDeep(groupedClaimBenefits);

    extendedGroups[0].benefits = [
      ...extendedGroups[0].benefits,
      {
        ...extendedGroups[0].benefits[0],
        benefitId: 2,
        claimId: 'CLM02'
      },
      {
        ...extendedGroups[0].benefits[0],
        benefitId: 3,
        claimId: 'CLM03'
      }
    ];

    const { getByTestId, getAllByTestId, queryAllByTestId, container } = render(
      <ContextWrapper permissions={permissions}>
        <WageReplacementCards benefitGroup={extendedGroups} />
      </ContextWrapper>
    );

    const element = getAllByTestId('collapsible-card');

    expect(getByTestId('wage-replacement-cards')).toBeInTheDocument();
    expect(element[0]).toBeInTheDocument();

    expect(getByTestId('disability-benefit-header-name')).toHaveTextContent(
      'DISABILITY_BENEFIT_TYPE.STD_ABBR Benefit'
    );
    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.APPROVED (3)'
    );

    expect(queryAllByTestId('card-body-with-status')).toHaveLength(0);

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getAllByTestId('card-body-with-status')).toHaveLength(3);
  });

  test('should render expanded without disability details', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );
    const { getByTestId, container, getAllByTestId } = render(
      <ContextWrapper permissions={permissions}>
        <WageReplacementCards benefitGroup={groupedClaimBenefits} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getAllByTestId('collapsible-card')[0]).toBeInTheDocument();

    const items = within(getAllByTestId('collapsible-card')[0]).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('disability-benefit-header-name')).toHaveTextContent(
      'DISABILITY_BENEFIT_TYPE.STD_ABBR Benefit'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')
    ).toHaveTextContent('Benny Smith');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')
    ).toHaveTextContent('1-234-567890');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE');
    expect(
      within(items[2]).getByTestId('property-item-value')
    ).toHaveTextContent('10-01-2019');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[3]).getByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.EXPECTED_RESOLUTION');
    expect(
      within(items[4]).getByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.APPROVED'
    );
  });

  test('should render expanded with disability details', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );

    const benefit = _cloneDeep(groupedClaimBenefits) as BenefitGroup[];

    benefit[0].benefits[0].benefitRightCategory = 'Recurring Benefit';
    benefit[0].benefits[0].disabilityBenefit = DISABILITY_BENEFIT_FIXTURE;

    const { getByTestId, container, getAllByTestId } = render(
      <ContextWrapper permissions={permissions}>
        <WageReplacementCards benefitGroup={benefit} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getAllByTestId('collapsible-card')[0]).toBeInTheDocument();

    const items = within(getAllByTestId('collapsible-card')[0]).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('disability-benefit-header-name')).toHaveTextContent(
      'DISABILITY_BENEFIT_TYPE.STD_ABBR Benefit'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')?.textContent
    ).toEqual('Benny Smith');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')?.textContent
    ).toEqual('1-234-567890');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE');
    expect(
      within(items[2]).getByTestId('property-item-value')?.textContent
    ).toEqual('13-07-2019');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[3]).getByTestId('property-item-value')?.textContent
    ).toEqual('13-07-2019');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.EXPECTED_RESOLUTION');
    expect(
      within(items[4]).getByTestId('property-item-value')?.textContent
    ).toEqual('13-07-2019');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.APPROVED'
    );
  });

  test('should render expanded with no values', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );

    const benefitGroup = [
      {
        name: 'STD Benefit',
        benefitId: 1,
        status: '',
        stageName: '',
        benefitHandler: '',
        benefitHandlerPhoneNo: '',
        benefitCaseType: 'STD Benefit',
        benefits: [
          {
            benefitCaseType: 'STD Benefit',
            benefitHandler: '',
            benefitHandlerEmailAddress: '',
            benefitHandlerPhoneNo: '',
            benefitId: 1,
            benefitIncurredDate: moment('2019-01-20'),
            benefitRightCategory: 'Recurring Benefit',
            creationDate: moment('2019-01-20'),
            customerName: '',
            description: '',
            policyReferences: '',
            stageName: '',
            status: '',
            extensionAttributes: [],
            claimId: '',
            disabilityBenefit: {} as DisabilityBenefit
          }
        ]
      }
    ] as BenefitGroup[];

    const { getByTestId, getAllByTestId, container } = render(
      <ContextWrapper permissions={permissions}>
        <WageReplacementCards benefitGroup={benefitGroup} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getAllByTestId('collapsible-card')[0]).toBeInTheDocument();

    const items = within(getAllByTestId('collapsible-card')[0]).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('disability-benefit-header-name')).toHaveTextContent(
      'DISABILITY_BENEFIT_TYPE.STD_ABBR Benefit'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE');
    expect(
      within(items[2]).getByTestId('property-item-value')?.textContent
    ).toEqual('20-01-2019');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[3]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.EXPECTED_RESOLUTION');
    expect(
      within(items[4]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.CLOSED'
    );
  });
});
