/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import moment from 'moment';
import { DisabilityBenefit } from 'fineos-js-api-client';
import { ViewNotificationPermissions } from 'fineos-common';
import { cloneDeep as _cloneDeep } from 'lodash';

import {
  mockTranslations,
  ContextWrapper,
  releaseEventLoop
} from '../../../../common/utils';
import { DISABILITY_BENEFIT_FIXTURE } from '../../../../fixtures/group-client-claim.fixture';
import {
  PaidLeaveBenefit,
  ABSENCE_PERIOD_TYPE,
  BenefitGroup
} from '../../../../../app/shared/types';
import { WageReplacementCards } from '../../../../../app/modules/notification/wage-replacement/wage-replacement-cards';
import * as paymentsApiModule from '../../../../../app/modules/notification/latest-payments/latest-payments.api';
import { PAYMENTS_FIXTURE } from '../../../../fixtures/payments.fixture';

describe('PaidLeaveBenefit Card', () => {
  const translations = mockTranslations(
    'DISABILITY_BENEFIT_STATUS.DENIED',
    'DISABILITY_BENEFIT_STATUS.CLOSED',
    'FINEOS_COMMON.HANDLER_INFO.MANAGED_BY',
    'FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER',
    'NOTIFICATIONS.WAGE_REPLACEMENT.ABSENCE_REASON',
    'NOTIFICATIONS.WAGE_REPLACEMENT.ENTITLEMENT_BEGINS',
    'NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE',
    'NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH',
    'PAYMENTS.LATEST_PAYMENTS'
  );

  const permissions = [
    ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST,
    ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_BENEFIT
  ];

  const groupedPaidLeaveBenefit = [
    {
      name: 'Leave Plan Name',
      benefitId: 1,
      status: 'Decided',
      stageName: 'Denied',
      benefitHandler: 'Benny Smith',
      benefitHandlerPhoneNo: '1-234-567890',
      benefitCaseType: 'Leave Plan Name',
      benefits: [
        {
          benefitCaseType: 'Short Term Disability',
          benefitHandler: 'Benny Smith',
          benefitHandlerEmailAddress: '',
          benefitHandlerPhoneNo: '1-234-567890',
          benefitId: 1,
          benefitIncurredDate: moment('2019-01-20'),
          benefitRightCategory: 'Lump Sum Benefit',
          creationDate: moment('2019-01-10'),
          customerName: 'bob bobbington',
          description: '',
          policyReferences: '',
          stageName: 'Denied',
          status: 'Decided',
          extensionAttributes: [],
          paidLeaveCaseId: 'PLC01',
          leavePlanName: 'Leave Plan Name',
          reasonName: 'Reason Name',
          type: 'Reduced Schedule'
        }
      ]
    }
  ] as BenefitGroup<any>[];

  beforeEach(() => {
    jest.spyOn(paymentsApiModule, 'fetchPayments');
  });

  test('should render collapsed', async () => {
    const { getByTestId } = render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <WageReplacementCards benefitGroup={groupedPaidLeaveBenefit} />
      </ContextWrapper>
    );

    const element = getByTestId('collapsible-card');

    expect(getByTestId('wage-replacement-cards')).toBeInTheDocument();
    expect(element).toBeInTheDocument();

    expect(getByTestId('paid-leave-benefit-header-name')).toHaveTextContent(
      'Leave Plan Name'
    );
    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.DENIED'
    );
  });

  test('should render expanded without disability details', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );
    const { getByTestId, container } = render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <WageReplacementCards benefitGroup={groupedPaidLeaveBenefit} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getByTestId('collapsible-card')).toBeInTheDocument();

    const items = within(getByTestId('collapsible-card')).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('paid-leave-benefit-header-name')).toHaveTextContent(
      'Leave Plan Name'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')
    ).toHaveTextContent('Benny Smith');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')
    ).toHaveTextContent('1-234-567890');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.ABSENCE_REASON');
    expect(
      within(items[2]).getByTestId('property-item-value')
    ).toHaveTextContent('Reason Name');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE');
    expect(
      within(items[3]).getByTestId('property-item-value')
    ).toHaveTextContent('10-01-2019');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[4]).getByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.DENIED'
    );
  });

  test('should render expanded with disability details', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );
    const benefit = _cloneDeep(groupedPaidLeaveBenefit) as BenefitGroup<any>[];

    benefit[0].benefits[0].benefitRightCategory = 'Recurring Benefit';
    benefit[0].benefits[0].disabilityBenefit = DISABILITY_BENEFIT_FIXTURE;

    const { getByTestId, container } = render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <WageReplacementCards benefitGroup={benefit} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getByTestId('collapsible-card')).toBeInTheDocument();

    const items = within(getByTestId('collapsible-card')).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('paid-leave-benefit-header-name')).toHaveTextContent(
      'Leave Plan Name'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')?.textContent
    ).toEqual('Benny Smith');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')?.textContent
    ).toEqual('1-234-567890');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.ABSENCE_REASON');
    expect(
      within(items[2]).getByTestId('property-item-value')?.textContent
    ).toEqual('Reason Name');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE');
    expect(
      within(items[3]).getByTestId('property-item-value')?.textContent
    ).toEqual('13-07-2019');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[4]).getByTestId('property-item-value')?.textContent
    ).toEqual('13-07-2019');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.DENIED'
    );
  });

  test('should render expanded with different attributes for INTERMITTENT periods', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );
    const benefit = _cloneDeep(groupedPaidLeaveBenefit) as BenefitGroup<any>[];

    (benefit[0].benefits[0] as PaidLeaveBenefit).type =
      ABSENCE_PERIOD_TYPE.INTERMITTENT;

    const { getByTestId, container } = render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <WageReplacementCards benefitGroup={benefit} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getByTestId('collapsible-card')).toBeInTheDocument();

    const items = within(getByTestId('collapsible-card')).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('paid-leave-benefit-header-name')).toHaveTextContent(
      'Leave Plan Name'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')?.textContent
    ).toEqual('Benny Smith');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')?.textContent
    ).toEqual('1-234-567890');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.ABSENCE_REASON');
    expect(
      within(items[2]).getByTestId('property-item-value')
    ).toHaveTextContent('Reason Name');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.ENTITLEMENT_BEGINS');
    expect(
      within(items[3]).getByTestId('paid-leave-tooltip')
    ).toBeInTheDocument();
    expect(
      within(items[3]).getByTestId('property-item-value')
    ).toHaveTextContent('10-01-2019');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[4]).getByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.DENIED'
    );
  });

  test('should render expanded with no values', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );
    const benefitGroup = [
      {
        name: 'Leave Plan Name',
        benefitId: 1,
        status: '',
        stageName: '',
        benefitHandler: '',
        benefitHandlerPhoneNo: '',
        benefitCaseType: 'Short Term Disability',
        benefits: [
          {
            benefitCaseType: 'Short Term Disability',
            benefitHandler: '',
            benefitHandlerEmailAddress: '',
            benefitHandlerPhoneNo: '',
            benefitId: 1,
            benefitIncurredDate: moment('2019-01-20'),
            benefitRightCategory: 'Recurring Benefit',
            creationDate: moment('2019-01-20'),
            customerName: '',
            description: '',
            policyReferences: '',
            stageName: '',
            status: '',
            extensionAttributes: [],
            paidLeaveCaseId: 'PLC01',
            leavePlanName: 'Leave Plan Name',
            reasonName: '',
            type: '',
            disabilityBenefit: {} as DisabilityBenefit
          }
        ]
      }
    ] as BenefitGroup<any>[];

    const { getByTestId, container } = render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <WageReplacementCards benefitGroup={benefitGroup} />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(container.querySelector('.ant-collapse-header')!);
      return releaseEventLoop();
    });

    expect(getByTestId('collapsible-card')).toBeInTheDocument();

    const items = within(getByTestId('collapsible-card')).getAllByTestId(
      'property-item'
    );

    // card head properties
    expect(getByTestId('paid-leave-benefit-header-name')).toHaveTextContent(
      'Leave Plan Name'
    );
    expect(
      within(items[0]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.MANAGED_BY');
    expect(
      within(items[0]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    expect(
      within(items[1]).getByTestId('property-item-label')
    ).toHaveTextContent('FINEOS_COMMON.HANDLER_INFO.PHONE_NUMBER');
    expect(
      within(items[1]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    // card body properties
    expect(
      within(items[2]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.ABSENCE_REASON');
    expect(
      within(items[2]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    expect(
      within(items[3]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.BENEFIT_START_DATE');
    expect(
      within(items[3]).getByTestId('property-item-value')?.textContent
    ).toEqual('20-01-2019');

    expect(
      within(items[4]).getByTestId('property-item-label')
    ).toHaveTextContent('NOTIFICATIONS.WAGE_REPLACEMENT.APPROVED_THROUGH');
    expect(
      within(items[4]).getByTestId('property-item-value')?.textContent
    ).toEqual('-');

    expect(getByTestId('status-label')).toHaveTextContent(
      'DISABILITY_BENEFIT_STATUS.CLOSED'
    );
  });
});
