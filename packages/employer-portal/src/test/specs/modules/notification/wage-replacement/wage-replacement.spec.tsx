/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';

import { mockTranslations, ContextWrapper } from '../../../../common/utils';
import { WageReplacement } from '../../../../../app/modules/notification/wage-replacement';
import { PAID_LEAVE_BENEFITS_FIXTURE } from '../../../../fixtures/paid-leave-benefits.fixture';
import { CLAIM_DISABILITY_BENEFITS_FIXTURE } from '../../../../fixtures/claim-disability-benefits.fixture';

describe('WageReplacement', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.WAGE_REPLACEMENT.PANEL_TITLE',
    'NOTIFICATIONS.WAGE_REPLACEMENT.EMPTY',
    'DISABILITY_BENEFIT_STATUS.PENDING',
    'DISABILITY_BENEFIT_STATUS.CLOSED'
  );

  describe('without permissions', () => {
    test('should not be rendered', () => {
      const { queryByTestId } = render(
        <ContextWrapper>
          <WageReplacement
            claimBenefits={[]}
            paidLeaveBenefits={[]}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
    });
  });

  describe('with VIEW_CLAIMS_BENEFITS_LIST permission', () => {
    const permissions = [ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST];

    test('should render an empty state if no data is passed in', () => {
      const { queryByTestId, getByText, getByTestId } = render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <WageReplacement
            claimBenefits={[]}
            paidLeaveBenefits={[]}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
      expect(getByTestId('empty-state')).toBeInTheDocument();
      expect(
        getByText('NOTIFICATIONS.WAGE_REPLACEMENT.EMPTY')
      ).toBeInTheDocument();
    });

    test('should render benefit cards', () => {
      const { queryByTestId, getAllByTestId } = render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <WageReplacement
            claimBenefits={[CLAIM_DISABILITY_BENEFITS_FIXTURE[0]]}
            paidLeaveBenefits={[PAID_LEAVE_BENEFITS_FIXTURE[0]]}
            isLoading={false}
          />
        </ContextWrapper>
      );

      expect(getAllByTestId('wage-replacement')).toHaveLength(2);
      expect(queryByTestId('empty-state')).not.toBeInTheDocument();
      expect(getAllByTestId('collapsible-card')).toHaveLength(2);
    });
  });
});
