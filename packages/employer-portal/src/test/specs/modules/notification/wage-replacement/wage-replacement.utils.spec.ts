/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { StatusType, StatusPattern } from 'fineos-common';

import {
  convertStatusToDisplayStatus,
  getStatusTypeByStatus,
  getStatusPatternForAbsencePeriodType
} from '../../../../../app/modules/notification/wage-replacement/wage-replacement.util';
import {
  WageReplacementStatus,
  ABSENCE_PERIOD_TYPE
} from '../../../../../app/shared/types';
import { getPaidLeaveCasesFromPeriodDecisions } from '../../../../../app/modules/notification/notification.util';
import { ABSENCE_PERIOD_BENEFITS_FIXTURE } from '../../../../fixtures/absence-period-benefits.fixture';

describe('wage replacement utils', () => {
  test('convertStatusToDisplayStatus should return the correct status', () => {
    expect(convertStatusToDisplayStatus('Pending', '')).toBe('Pending');
    expect(convertStatusToDisplayStatus('Decided', 'Approved')).toBe(
      'Approved'
    );
    expect(convertStatusToDisplayStatus('Decided', 'Denied')).toBe('Denied');
    expect(convertStatusToDisplayStatus('Decided', '')).toBe('Decided');
    expect(convertStatusToDisplayStatus('Closed', '')).toBe('Closed');
    // forcing wrong values will default to Closed
    expect(convertStatusToDisplayStatus('' as WageReplacementStatus, '')).toBe(
      'Closed'
    );
  });

  test('getStatusTypeByStatus should convert status text into a StatusType', () => {
    expect(getStatusTypeByStatus('Pending')).toBe(StatusType.WARNING);
    expect(getStatusTypeByStatus('Approved')).toBe(StatusType.SUCCESS);
    expect(getStatusTypeByStatus('Denied')).toBe(StatusType.DANGER);
    expect(getStatusTypeByStatus('Decided')).toBe(StatusType.BLANK);
    expect(getStatusTypeByStatus('Closed')).toBe(StatusType.NEUTRAL);
  });

  test('getStatusPatternForAbsencePeriodType should returnn the correct pattern', () => {
    expect(
      getStatusPatternForAbsencePeriodType(ABSENCE_PERIOD_TYPE.INTERMITTENT)
    ).toBe(StatusPattern.ANGLED);
    expect(
      getStatusPatternForAbsencePeriodType(ABSENCE_PERIOD_TYPE.REDUCED_SCHEDULE)
    ).toBe(StatusPattern.HALVED);
    expect(
      getStatusPatternForAbsencePeriodType(ABSENCE_PERIOD_TYPE.FIXED)
    ).toBe(StatusPattern.FULL);
    // forcing wrong values will default to FULL
    expect(
      getStatusPatternForAbsencePeriodType('' as ABSENCE_PERIOD_TYPE)
    ).toBe(StatusPattern.FULL);
  });

  test('getPaidLeaveCasesFromPeriodDecisions', () => {
    expect(getPaidLeaveCasesFromPeriodDecisions(null)).toEqual([]);
    expect(getPaidLeaveCasesFromPeriodDecisions([])).toEqual([]);
    expect(
      getPaidLeaveCasesFromPeriodDecisions(ABSENCE_PERIOD_BENEFITS_FIXTURE)
    ).toEqual([
      {
        id: 'PLC01',
        name: 'Paid Leave Case 01',
        reasonName: 'Reason Name 1',
        type: 'Reduced Schedule'
      },
      {
        id: 'PLC02',
        name: 'Paid Leave Case 02',
        reasonName: 'Reason Name 2',
        type: 'Episodic'
      },
      {
        id: 'PLC03',
        name: 'Paid Leave Case 03',
        reasonName: 'Reason Name 3',
        type: 'Time off period'
      },
      {
        id: 'PLC04',
        name: 'Paid Leave Case 04',
        reasonName: 'Reason Name 4',
        type: 'Reduced Schedule'
      }
    ]);
  });
});
