/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import { render, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  ReakitPopover,
  ReakitPopoverDisclosure,
  ReakitUsePopoverState
} from 'fineos-common';

import { ContextWrapper } from '../../../../../common/utils';
import { ActionsDropdown } from '../../../../../../app/modules/notification/notification-summary/actions-dropdown/actions-dropdown.component';

describe('Actions Dropdown', () => {
  test('should not render if there are no menu items', () => {
    const { queryByText } = render(
      <ContextWrapper>
        <ActionsDropdown menuItems={[]} />
      </ContextWrapper>
    );

    expect(
      queryByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
    ).not.toBeInTheDocument();
  });

  test('should not render if there are no visible menu items', () => {
    const menuItems = [
      {
        title: <>test item #1</>,
        visible: false,
        action: jest.fn()
      },
      {
        title: <>test item #2</>,
        visible: false,
        action: jest.fn()
      }
    ];

    const { queryByText } = render(
      <ContextWrapper>
        <ActionsDropdown menuItems={menuItems} />
      </ContextWrapper>
    );

    expect(
      queryByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
    ).not.toBeInTheDocument();
  });

  test('should render menu items and call action', () => {
    const menuItems = [
      {
        title: <>test item #1</>,
        visible: true,
        action: jest.fn()
      },
      {
        title: <>test item #2</>,
        visible: true,
        action: jest.fn()
      }
    ];

    const { getByText, queryByTestId, getByTestId, getAllByRole } = render(
      <ContextWrapper>
        <ActionsDropdown menuItems={menuItems} />
      </ContextWrapper>
    );

    expect(
      getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
    ).toBeInTheDocument();

    expect(queryByTestId('actions-dropdown')).not.toBeInTheDocument();

    userEvent.click(
      getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
    );

    expect(getByTestId('actions-dropdown')).toBeInTheDocument();

    expect(getAllByRole('menuitem')).toHaveLength(2);
    expect(getAllByRole('menuitem')[0]).toHaveTextContent('test item #1');
    expect(getAllByRole('menuitem')[1]).toHaveTextContent('test item #2');

    userEvent.click(getAllByRole('menuitem')[0].firstElementChild);

    expect(menuItems[0].action).toHaveBeenCalled();

    // closed dropdown is still in the document, need to check by class
    expect(queryByTestId('actions-dropdown')).not.toHaveClass(
      'ant-dropdown-open'
    );
  });

  test('should render only visible menu items', () => {
    const menuItems = [
      {
        title: <>test item #1</>,
        visible: false,
        action: jest.fn()
      },
      {
        title: <>test item #2</>,
        visible: false,
        action: jest.fn()
      },
      {
        title: <>test item #3</>,
        visible: true,
        action: jest.fn()
      }
    ];

    const { getByText, getAllByRole } = render(
      <ContextWrapper>
        <ActionsDropdown menuItems={menuItems} />
      </ContextWrapper>
    );

    userEvent.click(
      getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
    );

    expect(getAllByRole('menuitem')).toHaveLength(1);
    expect(getAllByRole('menuitem')[0]).toHaveTextContent('test item #3');
  });

  test('should render popover and call action', () => {
    const menuItems = [
      {
        title: <>test item #1</>,
        visible: true,
        action: jest.fn(),
        popover: popoverMock
      }
    ];

    const { getByText, getAllByRole, queryByTestId } = render(
      <ContextWrapper>
        <ActionsDropdown menuItems={menuItems} />
      </ContextWrapper>
    );

    userEvent.click(
      getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
    );

    expect(getAllByRole('menuitem')).toHaveLength(1);
    expect(getAllByRole('menuitem')[0]).toHaveTextContent('test item #1');

    act(() => {
      userEvent.click(getAllByRole('menuitem')[0].firstElementChild);
    });

    act(() => {
      userEvent.click(getByText('confirm'));
    });

    expect(menuItems[0].action).toHaveBeenCalled();

    // closed dropdown is still in the document, need to check by class
    expect(queryByTestId('actions-dropdown')).not.toHaveClass(
      'ant-dropdown-open'
    );
  });
  const popoverMock: React.FC = ({ children, onConfirm }: any) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const popover = ReakitUsePopoverState();
    return (
      <>
        <ReakitPopoverDisclosure {...popover}>
          <div>{children}</div>
        </ReakitPopoverDisclosure>
        <ReakitPopover {...popover}>
          <div data-test-el="popover">
            <button onClick={onConfirm}>confirm</button>
          </div>
        </ReakitPopover>
      </>
    );
  };
});
