/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, render, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ManageNotificationPermissions } from 'fineos-common';
import {
  AbsenceCaseStatus,
  CaseHandler,
  SubCase,
  SubCaseType
} from 'fineos-js-api-client';

import { ContextWrapper } from '../../../../common/utils';
import { NotificationSummary } from '../../../../../app/modules/notification/notification-summary';
import { NOTIFICATIONS_FIXTURE } from '../../../../fixtures/notifications.fixture';
import { DISABILITY_CLAIMS_FIXTURE } from '../../../../fixtures/disability-claims.fixture';
import {
  ENHANCED_GROUP_CLIENT_PERIOD_DECISION,
  SUMMARY_ENHANCED_GROUP_CLIENT_PERIOD_DECISION_FIXTURE_INVALID_FIRST_DAY
} from '../../../../fixtures/leave-period-change.fixture';

const props = {
  notification: NOTIFICATIONS_FIXTURE[0],
  disabilityClaim: DISABILITY_CLAIMS_FIXTURE,
  enhancedAbsencePeriodDecisions: ENHANCED_GROUP_CLIENT_PERIOD_DECISION
};

describe('Notification summary', () => {
  test('render basic case only with notification date and pending status', async () => {
    render(
      <ContextWrapper>
        <NotificationSummary
          notification={NOTIFICATIONS_FIXTURE[0]}
          disabilityClaims={[]}
          isJobProtectedLeaveVisible={false}
          enhancedAbsencePeriodDecisions={[]}
        />
      </ContextWrapper>
    );

    const notificationSummary = screen.getByTestId('notification-summary');

    expect(notificationSummary).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.CASE_PROGRESS.PENDING')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON')
    ).toBeInTheDocument();
  });

  test('render with first day missing and CLOSED status', async () => {
    render(
      <ContextWrapper>
        <NotificationSummary
          notification={NOTIFICATIONS_FIXTURE[2]}
          disabilityClaims={props.disabilityClaim}
          isJobProtectedLeaveVisible={false}
          enhancedAbsencePeriodDecisions={props.enhancedAbsencePeriodDecisions}
        />
      </ContextWrapper>
    );

    const notificationSummary = screen.getByTestId('notification-summary');

    expect(notificationSummary).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.CASE_PROGRESS.CLOSED')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING')
    ).toBeInTheDocument();
  });

  test('render with first day missing and accident date and DECIDED status', async () => {
    render(
      <ContextWrapper>
        <NotificationSummary
          notification={NOTIFICATIONS_FIXTURE[3]}
          disabilityClaims={props.disabilityClaim}
          isJobProtectedLeaveVisible={false}
          enhancedAbsencePeriodDecisions={props.enhancedAbsencePeriodDecisions}
        />
      </ContextWrapper>
    );

    const notificationSummary = screen.getByTestId('notification-summary');

    expect(notificationSummary).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.CASE_PROGRESS.DECIDED')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACCIDENT_DATE')
    ).toBeInTheDocument();
  });

  test('render basic case only with absencePeriodDecisions and status SOME_DECISIONS', async () => {
    render(
      <ContextWrapper>
        <NotificationSummary
          notification={NOTIFICATIONS_FIXTURE[4]}
          disabilityClaims={[]}
          isJobProtectedLeaveVisible={false}
          enhancedAbsencePeriodDecisions={props.enhancedAbsencePeriodDecisions}
        />
      </ContextWrapper>
    );

    const notificationSummary = screen.getByTestId('notification-summary');

    expect(notificationSummary).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.CASE_PROGRESS.SOME_DECISIONS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACCIDENT_DATE')
    ).not.toBeInTheDocument();
  });

  test('render basic case only with disabilityClaim', async () => {
    render(
      <ContextWrapper>
        <NotificationSummary
          notification={NOTIFICATIONS_FIXTURE[5]}
          disabilityClaims={props.disabilityClaim}
          isJobProtectedLeaveVisible={false}
          enhancedAbsencePeriodDecisions={props.enhancedAbsencePeriodDecisions}
        />
      </ContextWrapper>
    );

    const notificationSummary = screen.getByTestId('notification-summary');

    expect(notificationSummary).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.CASE_PROGRESS.UNDETERMINED')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACCIDENT_DATE')
    ).toBeInTheDocument();
  });

  test('render the first day missing work and remove invalid dates', () => {
    render(
      <ContextWrapper>
        <NotificationSummary
          notification={NOTIFICATIONS_FIXTURE[5]}
          disabilityClaims={props.disabilityClaim}
          isJobProtectedLeaveVisible={false}
          enhancedAbsencePeriodDecisions={
            SUMMARY_ENHANCED_GROUP_CLIENT_PERIOD_DECISION_FIXTURE_INVALID_FIRST_DAY
          }
        />
      </ContextWrapper>
    );
    const notificationSummary = screen.getByTestId('notification-summary');

    expect(notificationSummary).toBeInTheDocument();

    expect(
      screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING')
    ).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.CASE_PROGRESS.UNDETERMINED')
    ).toBeInTheDocument();
    expect(screen.getByText('14-02-2020')).toBeInTheDocument();
  });

  describe('Actions menu', () => {
    test('should not render actions button when there are no absence period decisions', () => {
      render(
        <ContextWrapper
          permissions={[
            ManageNotificationPermissions.ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST
          ]}
        >
          <NotificationSummary
            notification={{ ...props.notification, subCases: [] }}
            disabilityClaims={props.disabilityClaim}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={null}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
      ).not.toBeInTheDocument();
    });

    test('should not render actions button when there are more absence period decisions then one', () => {
      render(
        <ContextWrapper
          permissions={[
            ManageNotificationPermissions.ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST
          ]}
        >
          <NotificationSummary
            notification={{
              ...props.notification,
              subCases: [
                {
                  id: 'ABS-2',
                  caseNumber: 'ABS-2',
                  caseType: SubCaseType.ABSENCE_TYPE,
                  caseHandler: {
                    id: '1',
                    name: 'Kylo Ren',
                    telephoneNo: '01-111-123456',
                    emailAddress: 'kyloren@thefirstorder.com'
                  } as CaseHandler,
                  status: AbsenceCaseStatus.REGISTRATION as AbsenceCaseStatus
                } as SubCase,
                {
                  id: 'ABS-3',
                  caseNumber: 'ABS-2',
                  caseType: SubCaseType.ABSENCE_TYPE,
                  caseHandler: {
                    id: '1',
                    name: 'Kylo Ren',
                    telephoneNo: '01-111-123456',
                    emailAddress: 'kyloren@thefirstorder.com'
                  } as CaseHandler,
                  status: AbsenceCaseStatus.REGISTRATION as AbsenceCaseStatus
                } as SubCase
              ]
            }}
            disabilityClaims={props.disabilityClaim}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={props.enhancedAbsencePeriodDecisions.concat(
              SUMMARY_ENHANCED_GROUP_CLIENT_PERIOD_DECISION_FIXTURE_INVALID_FIRST_DAY
            )}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
      ).not.toBeInTheDocument();
    });

    test('should not render actions button when there are no available menu items', () => {
      render(
        <ContextWrapper>
          <NotificationSummary
            notification={props.notification}
            disabilityClaims={props.disabilityClaim}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={
              props.enhancedAbsencePeriodDecisions
            }
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
      ).not.toBeInTheDocument();
    });

    test('should render action menu button', () => {
      render(
        <ContextWrapper
          permissions={[
            ManageNotificationPermissions.ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST
          ]}
        >
          <NotificationSummary
            notification={props.notification}
            disabilityClaims={props.disabilityClaim}
            isJobProtectedLeaveVisible={true}
            enhancedAbsencePeriodDecisions={
              props.enhancedAbsencePeriodDecisions
            }
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')
      ).toBeInTheDocument();

      expect(screen.queryByTestId('actions-dropdown')).not.toBeInTheDocument();

      userEvent.click(
        screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')!
      );

      expect(screen.getByTestId('actions-dropdown')).toBeInTheDocument();

      expect(screen.getAllByRole('menuitem').length).toBeGreaterThanOrEqual(1);
    });

    test('should open leave-period-change section after clicking an option in actions menu', async () => {
      render(
        <ContextWrapper
          permissions={[
            ManageNotificationPermissions.ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST
          ]}
        >
          <NotificationSummary
            notification={props.notification}
            disabilityClaims={props.disabilityClaim}
            isJobProtectedLeaveVisible={true}
            enhancedAbsencePeriodDecisions={
              props.enhancedAbsencePeriodDecisions
            }
          />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')!
      );

      userEvent.click(
        await screen.findByText('LEAVE_PERIOD_CHANGE.DROPDOWN_OPTION')
      );

      expect(
        await screen.findByText('LEAVE_PERIOD_CHANGE.FORM_HEAD')
      ).toBeInTheDocument();

      userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

      await act(wait);

      expect(
        document.querySelector('.ant-drawer-open')
      ).not.toBeInTheDocument();
    });

    test('should open request-for-more-time section after clicking an option in actions menu', async () => {
      render(
        <ContextWrapper
          permissions={[
            ManageNotificationPermissions.ADD_ABSENCE_LEAVEPERIODS_CHANGE_REQUEST
          ]}
        >
          <NotificationSummary
            notification={props.notification}
            disabilityClaims={props.disabilityClaim}
            isJobProtectedLeaveVisible={true}
            enhancedAbsencePeriodDecisions={
              props.enhancedAbsencePeriodDecisions
            }
          />
        </ContextWrapper>
      );

      await act(wait);

      userEvent.click(
        screen.getByText('NOTIFICATIONS.NOTIFICATION_SUMMARY.ACTION_BUTTON')!
      );
      await act(wait);

      userEvent.click(
        screen.getByText('REQUEST_FOR_MORE_TIME.DROPDOWN_OPTION')
      );
      await act(wait);

      expect(
        screen.getByText('REQUEST_FOR_MORE_TIME.POPOVER_HEADER')
      ).toBeInTheDocument();

      userEvent.click(
        screen.getByText('REQUEST_FOR_MORE_TIME.POPOVER_PROCEED')
      );
      await act(wait);

      expect(
        screen.getByText('REQUEST_FOR_MORE_TIME.TITLE')
      ).toBeInTheDocument();

      userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

      await wait(() => {
        expect(
          screen.queryByText('REQUEST_FOR_MORE_TIME.TITLE')
        ).not.toBeInTheDocument();
      });
    });
  });

  describe('Expected RTW field', () => {
    test('should be visible', () => {
      const { rerender } = render(
        <ContextWrapper>
          <NotificationSummary
            notification={NOTIFICATIONS_FIXTURE[0]}
            disabilityClaims={[]}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={[]}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          'NOTIFICATIONS.NOTIFICATION_SUMMARY.EXPECTED_RETURN_TO_WORK'
        )
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          NOTIFICATIONS_FIXTURE[0].expectedRTWDate.format('DD-MM-YYYY')
        )
      ).toBeInTheDocument();

      rerender(
        <ContextWrapper>
          <NotificationSummary
            notification={{
              ...NOTIFICATIONS_FIXTURE[0],
              subCases: [NOTIFICATIONS_FIXTURE[0].subCases[1]]
            }}
            disabilityClaims={[]}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={
              props.enhancedAbsencePeriodDecisions
            }
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          'NOTIFICATIONS.NOTIFICATION_SUMMARY.EXPECTED_RETURN_TO_WORK'
        )
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          NOTIFICATIONS_FIXTURE[0].expectedRTWDate.format('DD-MM-YYYY')
        )
      ).toBeInTheDocument();
    });
    test('should not be visible', () => {
      const { rerender } = render(
        <ContextWrapper>
          <NotificationSummary
            notification={{
              ...NOTIFICATIONS_FIXTURE[0],
              subCases: [NOTIFICATIONS_FIXTURE[0].subCases[2]]
            }}
            disabilityClaims={[]}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={
              props.enhancedAbsencePeriodDecisions
            }
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText(
          'NOTIFICATIONS.NOTIFICATION_SUMMARY.EXPECTED_RETURN_TO_WORK'
        )
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText(
          NOTIFICATIONS_FIXTURE[0].expectedRTWDate.format('DD-MM-YYYY')
        )
      ).not.toBeInTheDocument();

      rerender(
        <ContextWrapper>
          <NotificationSummary
            notification={{
              ...NOTIFICATIONS_FIXTURE[0],
              subCases: [NOTIFICATIONS_FIXTURE[0].subCases[1]]
            }}
            disabilityClaims={[]}
            isJobProtectedLeaveVisible={false}
            enhancedAbsencePeriodDecisions={[]}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText(
          'NOTIFICATIONS.NOTIFICATION_SUMMARY.EXPECTED_RETURN_TO_WORK'
        )
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText(
          NOTIFICATIONS_FIXTURE[0].expectedRTWDate.format('DD-MM-YYYY')
        )
      ).not.toBeInTheDocument();
    });
  });
});
