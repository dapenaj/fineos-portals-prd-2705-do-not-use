/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, screen, wait, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MessagesPermissions } from 'fineos-common';

import { render } from '../../../../common/custom-render';
import {
  ContextWrapper,
  mockTranslations,
  buildTranslationWithParams
} from '../../../../common/utils';
import {
  NewMessageForm,
  formValidation
} from '../../../../../app/modules/notification/alerts/alerts-messages-tab/new-message-form';
import * as apiModule from '../../../../../app/shared/api/message.api';
import { FormikWithResponseHandling } from '../../../../../app/shared';

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

const translations = mockTranslations(
  buildTranslationWithParams('FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS', [
    'remainingCharacters'
  ])
);

jest.mock('../../../../../app/shared/api/message.api', () => ({
  addMessage: jest.fn()
}));
const addMessage = apiModule.addMessage as jest.Mock;

describe('NewMessageForm', () => {
  test('should render 2 inputs with labels for subject and message fields', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*')
    ).toBeInTheDocument();
  });

  test('should render counter for remaining characters in message input', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS')
    ).toBeInTheDocument();
  });

  test('should change counter state when message input changes', async () => {
    render(
      <ContextWrapper
        permissions={messagesPermissions}
        translations={translations}
      >
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    await act(wait);

    const maxLength = 4000;
    expect(
      screen.getByText(
        `FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS[remainingCharacters:${maxLength}]`
      )
    ).toBeInTheDocument();

    const messageInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'
    );

    const text = 'test';
    userEvent.type(messageInput, text);

    await act(wait);

    expect(messageInput).toHaveValue(text);
    expect(
      screen.getByText(
        `FINEOS_COMMON.TEXT_AREA.REMAINING_CHARACTERS[remainingCharacters:${maxLength -
          text.length}]`
      )
    ).toBeInTheDocument();
  });

  test('should hide counter when error message for message input is displayed', async () => {
    render(
      <ContextWrapper
        permissions={messagesPermissions}
        translations={translations}
      >
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const messageInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'
    );

    await act(async () => {
      await userEvent.type(messageInput, '');
      fireEvent.blur(messageInput);
      return wait();
    });

    expect(
      screen.getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MIN_LENGTH'
      )
    ).toBeInTheDocument();

    const maxLength = 4000;
    expect(
      screen.queryByText(
        `NOTIFICATIONS.ALERTS.MESSAGES.REMAINING_CHARACTERS[remainingCharacters:${maxLength}]`
      )
    ).not.toBeInTheDocument();
  });

  test('should mark subject input as invalid and show min length error, when subject input doesnt have at least 5 characters', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const subjectInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'
    );

    await act(async () => {
      await userEvent.type(subjectInput, '');
      fireEvent.blur(subjectInput);
      return wait();
    });

    expect(subjectInput).not.toBeValid();

    expect(
      screen.getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MIN_SUBJECT_LENGTH'
      )
    ).toBeInTheDocument();
  });

  test('should mark subject input as invalid and show max length error, when characters exceed allowed limit', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const subjectInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'
    );

    await act(async () => {
      await userEvent.type(
        subjectInput,
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et mag. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et mag'
      );
      userEvent.click(
        getSubmitButtonByText('NOTIFICATIONS.ALERTS.MESSAGES.SEND')
      );
      return wait();
    });

    expect(subjectInput).not.toBeValid();

    expect(
      screen.getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.MAX_SUBJECT_LENGTH'
      )
    ).toBeInTheDocument();
  });

  test('should mark subject input as valid when input is correctly filled in', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const subjectInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'
    );

    await act(async () => {
      await userEvent.type(subjectInput, 'test5');
      userEvent.click(
        getSubmitButtonByText('NOTIFICATIONS.ALERTS.MESSAGES.SEND')
      );
      return wait();
    });

    expect(subjectInput).toBeValid();
  });

  test('should mark message input as invalid and show min length error, when message input doesnt have at least 5 characters', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const messageInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'
    );

    await act(async () => {
      await userEvent.type(messageInput, '');
      fireEvent.blur(messageInput);
      return wait();
    });

    expect(messageInput).not.toBeValid();

    expect(
      screen.getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MIN_LENGTH'
      )
    ).toBeInTheDocument();
  });

  test('should mark message input as invalid immediately and show max length error, when characters exceed allowed limit', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const messageInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'
    );

    await act(async () => {
      await userEvent.type(messageInput, 'test'.repeat(1001));
      return wait();
    });

    expect(messageInput).not.toBeValid();

    expect(
      screen.getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.VALIDATION.NEW_MESSAGE_MAX_LENGTH'
      )
    ).toBeInTheDocument();
  });

  test('should mark message input as valid when input is correctly filled in', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper permissions={messagesPermissions}>
        <FormikWithResponseHandling
          onSubmit={jest.fn()}
          initialValues={{}}
          onReset={jest.fn()}
          validationSchema={formValidation}
        >
          <NewMessageForm onClose={jest.fn} onMessageAdd={addMessage} />
        </FormikWithResponseHandling>
      </ContextWrapper>
    );

    const messageInput = screen.getByLabelText(
      'NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'
    );

    await act(async () => {
      await userEvent.type(messageInput, 'test5');
      userEvent.click(
        getSubmitButtonByText('NOTIFICATIONS.ALERTS.MESSAGES.SEND')
      );
      return wait();
    });

    expect(messageInput).toBeValid();
  });
});
