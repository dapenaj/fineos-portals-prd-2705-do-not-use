/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { ManageNotificationPermissions } from 'fineos-common';
import { render, act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  ContextWrapper,
  mockTranslations,
  releaseEventLoop
} from '../../../../common/utils';
import { DownloadDocumentLink } from '../../../../../app/modules/notification/alerts/alerts-documents-tab/download-document-link';
import * as downloadApiModule from '../../../../../app/modules/notification/alerts/alerts-documents-tab/download-document-link/download-document-link.api';
import * as documentApiModule from '../../../../../app/shared/api/document.api';
import { DOCUMENTS_FIXTURE } from '../../../../fixtures/documents.fixture';

jest.mock(
  '../../../../../app/modules/notification/alerts/alerts-documents-tab/download-document-link/download-document-link.api',
  () => ({
    downloadDocument: jest.fn()
  })
);

jest.mock('../../../../../app/shared/api/document.api', () => ({
  markDocumentAsRead: jest.fn()
}));

const downloadDocument = downloadApiModule.downloadDocument as jest.Mock;
const markDocumentAsRead = documentApiModule.markDocumentAsRead as jest.Mock;

describe('DownloadDocumentLink', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.ALERTS.DOCUMENTS.DOWNLOAD_ERROR'
  );

  const mock = jest.fn();

  test('should render', async () => {
    render(
      <ContextWrapper translations={translations}>
        <DownloadDocumentLink
          document={DOCUMENTS_FIXTURE[1]}
          className="blah"
          documentMarkedAsRead={mock}
        />
      </ContextWrapper>
    );

    expect(screen.getByTestId('document-download-btn')).toBeInTheDocument();
  });

  test('should download a document and mark it as read', async () => {
    window.URL.createObjectURL = jest.fn();

    downloadDocument.mockReturnValueOnce(
      Promise.resolve({
        fileName: 'test',
        fileExtension: 'json',
        base64EncodedFileContents: 'eyJhIjogMX0=',
        contentType: 'text/json'
      })
    );

    markDocumentAsRead.mockReturnValue({
      ...DOCUMENTS_FIXTURE[1],
      isRead: true
    });

    render(
      <ContextWrapper
        translations={translations}
        permissions={[ManageNotificationPermissions.MARK_READ_CASE_DOCUMENT]}
      >
        <DownloadDocumentLink
          key="0"
          document={DOCUMENTS_FIXTURE[1]}
          className="blah"
          documentMarkedAsRead={mock}
        />
      </ContextWrapper>
    );

    expect(screen.getByTestId('document-download-btn')).toBeInTheDocument();

    await act(async () => {
      userEvent.click(screen.getByTestId('document-download-btn'));
      return releaseEventLoop();
    });

    await releaseEventLoop();

    expect(documentApiModule.markDocumentAsRead).toHaveBeenCalled();
  });
});
