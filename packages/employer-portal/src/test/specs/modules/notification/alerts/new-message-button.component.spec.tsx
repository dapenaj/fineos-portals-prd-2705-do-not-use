/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { MessagesPermissions, Theme } from 'fineos-common';
import { screen, within, act, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { render } from '../../../../common/custom-render';
import { MOCKED_THEME_FIXTURE } from '../../../../fixtures/mocked-theme.fixture';
import { MESSAGES_FIXTURE } from '../../../../fixtures/messages.fixture';
import { ContextWrapper } from '../../../../common/utils';
import { NewMessageButton } from '../../../../../app/modules/notification/alerts/alerts-messages-tab/new-message-button';
import * as apiModule from '../../../../../app/shared/api/message.api';

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

jest.mock('../../../../../app/shared/api/message.api', () => ({
  addMessage: jest.fn()
}));
const addMessage = apiModule.addMessage as jest.Mock;

describe('NewMessageButton', () => {
  test('should render new message button', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <NewMessageButton onMessageAdd={addMessage} />
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByRole('button', {
        name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      })
    ).toBeInTheDocument();
  });

  test('should open drawer and render new message form on new message button click', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <NewMessageButton onMessageAdd={addMessage} />
        </Theme.Provider>
      </ContextWrapper>
    );

    const button = screen.getByRole('button', {
      name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
    });

    userEvent.click(button);

    await act(wait);

    expect(
      within(screen.getByTestId('new-message-drawer')).getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      )
    ).toBeInTheDocument();
  });

  test('should close new message drawer on cancel right away, when both fields are invalid', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <NewMessageButton onMessageAdd={addMessage} />
        </Theme.Provider>
      </ContextWrapper>
    );

    const button = screen.getByRole('button', {
      name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
    });

    userEvent.click(button);

    await act(wait);

    expect(
      within(screen.getByTestId('new-message-drawer')).getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      )
    ).toBeInTheDocument();

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'FINEOS_COMMON.GENERAL.CANCEL'
        })
      );
      return wait();
    });

    expect(screen.queryByTestId('new-message-drawer')).not.toBeInTheDocument();
  });

  test('should display additional popup to confirm closing drawer when at least one field is filled in correctly', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <NewMessageButton onMessageAdd={addMessage} />
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
        })
      );
      return wait();
    });

    expect(
      within(screen.getByTestId('new-message-drawer')).getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      )
    ).toBeVisible();

    await act(async () => {
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'),
        'test5'
      );
      return wait();
    });

    userEvent.click(
      screen.getByRole('button', {
        name: 'FINEOS_COMMON.GENERAL.CANCEL'
      })
    );

    expect(
      within(await screen.findByRole('dialog')).queryByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING'
      )
    ).toBeInTheDocument();
  });

  test('should close drawer on "Yes" click on additional popup', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <NewMessageButton onMessageAdd={addMessage} />
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
        })
      );
      return wait();
    });

    await act(wait);

    expect(
      within(screen.getByTestId('new-message-drawer')).getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      )
    ).toBeVisible();

    await act(async () => {
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'),
        'test5'
      );
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'),
        'test5'
      );
      return wait();
    });

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'FINEOS_COMMON.GENERAL.CANCEL'
        })
      );
      return wait();
    });

    await act(wait);

    expect(
      within(screen.getByRole('dialog')).queryByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getByRole('dialog')).queryByText(
        'FINEOS_COMMON.GENERAL.YES'
      )!
    );

    await act(wait);

    expect(screen.queryByTestId('new-message-drawer')).not.toBeInTheDocument();
  });

  test('should not close drawer on "No" click on additional popup', async () => {
    render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <NewMessageButton onMessageAdd={addMessage} />
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
        })
      );
      return wait();
    });

    expect(
      within(screen.getByTestId('new-message-drawer')).getByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      )
    ).toBeVisible();

    await act(async () => {
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'),
        'test5'
      );
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'),
        'test5'
      );
      return wait();
    });

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'FINEOS_COMMON.GENERAL.CANCEL'
        })
      );
      return wait();
    });

    await act(wait);

    expect(
      within(screen.getByRole('dialog')).queryByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.CANCEL_SENDING'
      )
    ).toBeInTheDocument();

    userEvent.click(
      within(screen.getByRole('dialog')).queryByText(
        'FINEOS_COMMON.GENERAL.NO'
      )!
    );

    await act(wait);

    expect(
      within(screen.getByTestId('new-message-drawer')).queryByText(
        'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
      )
    ).toBeInTheDocument();
  });

  test.skip('should display error modal in case adding message fails because of the api error', async () => {
    addMessage.mockRejectedValueOnce({ status: '404' });
    const { getSubmitButtonByText } = render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <ContextWrapper permissions={messagesPermissions}>
            <Theme.Provider value={MOCKED_THEME_FIXTURE}>
              <NewMessageButton onMessageAdd={addMessage} />
            </Theme.Provider>
          </ContextWrapper>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
        })
      );
      return wait();
    });

    await act(async () => {
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'),
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit'
      );
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'),
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et mag'
      );
      await userEvent.click(
        getSubmitButtonByText('NOTIFICATIONS.ALERTS.MESSAGES.SEND')
      );
      return wait();
    });

    await act(wait);

    expect(
      await screen.findByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
    ).toBeInTheDocument();

    await act(wait);

    expect(
      await screen.findByText(
        'FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE'
      )
    ).toBeInTheDocument();
  });

  test.skip('should display "message sent" notification in case adding message succeeds', async () => {
    addMessage.mockReturnValueOnce(Promise.resolve(MESSAGES_FIXTURE[0]));
    const { getSubmitButtonByText } = render(
      <ContextWrapper permissions={messagesPermissions}>
        <Theme.Provider value={MOCKED_THEME_FIXTURE}>
          <ContextWrapper permissions={messagesPermissions}>
            <Theme.Provider value={MOCKED_THEME_FIXTURE}>
              <NewMessageButton onMessageAdd={addMessage} />
            </Theme.Provider>
          </ContextWrapper>
        </Theme.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: 'NOTIFICATIONS.ALERTS.MESSAGES.NEW_MESSAGE'
        })
      );
      return wait();
    });

    await act(async () => {
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.SUBJECT_LABEL*'),
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit'
      );
      await userEvent.type(
        screen.getByLabelText('NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_LABEL*'),
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et mag'
      );
      await userEvent.click(
        getSubmitButtonByText('NOTIFICATIONS.ALERTS.MESSAGES.SEND')
      );
      return wait();
    });

    await act(wait);

    expect(
      await screen.findByText('NOTIFICATIONS.ALERTS.MESSAGES.MESSAGE_SENT')
    ).toBeInTheDocument();
  });
});
