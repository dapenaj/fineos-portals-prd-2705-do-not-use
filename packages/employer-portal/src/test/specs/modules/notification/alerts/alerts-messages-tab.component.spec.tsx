/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Tabs, MessagesPermissions } from 'fineos-common';
import { render, act, wait, screen } from '@testing-library/react';

import { ContextWrapper } from '../../../../common/utils';
import { AlertsMessagesTab } from '../../../../../app/modules/notification/alerts/alerts-messages-tab';
import * as apiModule from '../../../../../app/shared/api/message.api';
import { MESSAGES_FIXTURE } from '../../../../fixtures/messages.fixture';

jest.mock('../../../../../app/shared/api/message.api', () => ({
  fetchMessagesByCaseId: jest.fn(),
  fetchUnreadMessagesCount: jest
    .fn()
    .mockReturnValue(Promise.resolve({ data: [], totalSize: 0 }))
}));

const fetchMessagesByCaseId = apiModule.fetchMessagesByCaseId as jest.Mock;

const AlertsMessagesContainer = () => (
  <Tabs>
    <AlertsMessagesTab tab={''} key={''} caseId={'id-1'} />
  </Tabs>
);

const messagesPermissions = [
  MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
  MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
  MessagesPermissions.ADD_CASE_WEB_MESSAGE,
  MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
];

describe('AlertsMessagesTab', () => {
  test('should show empty state message if no messages available', async () => {
    fetchMessagesByCaseId.mockReturnValueOnce(Promise.resolve([]));

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <AlertsMessagesContainer />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.MESSAGES.EMPTY')
    ).toBeInTheDocument();
  });

  test('should show something went wrong in case of error', async () => {
    fetchMessagesByCaseId.mockReturnValueOnce(
      Promise.reject(new Error('error'))
    );

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <AlertsMessagesContainer />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(screen.getByTestId('something-went-wrong')).toBeInTheDocument();
  });

  test('should show messages if returned', async () => {
    fetchMessagesByCaseId.mockReturnValueOnce(
      Promise.resolve(MESSAGES_FIXTURE)
    );

    render(
      <ContextWrapper permissions={messagesPermissions}>
        <AlertsMessagesContainer />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    MESSAGES_FIXTURE.forEach(message => {
      expect(screen.getByText(message.subject)).toBeInTheDocument();
      expect(screen.getByText(message.narrative)).toBeInTheDocument();
    });
  });
});
