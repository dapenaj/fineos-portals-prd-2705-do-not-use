/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CustomerSummary } from 'fineos-js-api-client';
import {
  Tabs,
  ViewNotificationPermissions,
  ManageNotificationPermissions
} from 'fineos-common';
import { render, act, screen, wait, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  buildTranslationWithParams,
  ContextWrapper,
  mockTranslations,
  releaseEventLoop
} from '../../../../common/utils';
import { AlertsOutstandingTab } from '../../../../../app/modules/notification/alerts/alerts-outstanding-tab';
import { useOutstandingInfo } from '../../../../../app/modules/notification/alerts/use-outstanding-info.hook';
import * as apiModule from '../../../../../app/shared/api/document.api';
import { PortalEnvConfigService } from '../../../../../app/shared';
import portalEnvConfig from '../../../../../../public/config/portal.env.json';

jest.mock('../../../../../app/shared/api/document.api', () => ({
  fetchOutstandingInformation: jest.fn()
}));

const fetchOutstandingInformation = apiModule.fetchOutstandingInformation as jest.Mock;

const AlertsOutstandingTabContainer = () => {
  const state = useOutstandingInfo('id-1');
  const mock = jest.fn();
  return (
    <Tabs>
      <AlertsOutstandingTab
        tab={''}
        key={''}
        caseId={'id-1'}
        handleRefreshOutstandingInfo={mock}
        customer={
          { firstName: 'firstName', lastName: 'lastName' } as CustomerSummary
        }
        {...state}
      />
    </Tabs>
  );
};

describe('AlertsOutstandingTab', () => {
  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
  });
  const translations = mockTranslations(
    'NOTIFICATIONS.ALERTS.OUTSTANDING.EMPTY',
    buildTranslationWithParams('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOAD', [
      'sourcePartyName'
    ]),
    buildTranslationWithParams('NOTIFICATIONS.ALERTS.OUTSTANDING.UPLOADED', [
      'sourcePartyName'
    ]),
    'NOTIFICATIONS.ALERTS.DOWNLOAD_INFO'
  );

  test('should show empty message if no documents available', async () => {
    fetchOutstandingInformation.mockReturnValueOnce(Promise.resolve([]));

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
        translations={translations}
      >
        <AlertsOutstandingTabContainer />
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).toBeInTheDocument();

    await act(() => releaseEventLoop());
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.OUTSTANDING.EMPTY')
    ).toBeInTheDocument();
  });

  test('should show something went wrong in case of error', async () => {
    fetchOutstandingInformation.mockReturnValueOnce(
      Promise.reject(new Error('error'))
    );

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
        translations={translations}
      >
        <AlertsOutstandingTabContainer />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();

    await act(() => releaseEventLoop());
    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(screen.queryByTestId('something-went-wrong')).toBeInTheDocument();
  });

  test('should show documents if returned', async () => {
    fetchOutstandingInformation.mockReturnValueOnce(
      Promise.resolve([
        {
          sourcePartyName: 'Limited',
          uploadCaseNumber: '1',
          informationType: 'Upload 1',
          infoReceived: false
        },
        {
          sourcePartyName: 'Limited',
          uploadCaseNumber: '2',
          informationType: 'Upload 2',
          infoReceived: true
        }
      ])
    );

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST,
          ManageNotificationPermissions.UPLOAD_CASE_BASE64_DOCUMENT
        ]}
        translations={translations}
      >
        <AlertsOutstandingTabContainer />
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).toBeInTheDocument();

    await act(() => releaseEventLoop());
    await act(wait);

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    userEvent.click(within(screen.getByRole('button')).getByText('Upload 1'));
    await act(wait);

    expect(
      within(screen.getByTestId('upload-document-modal')).getByText('Upload 1')
    ).toBeInTheDocument();

    expect(screen.getByText('Upload 2')).toBeInTheDocument();
    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.DOWNLOAD_INFO')
    ).toBeInTheDocument();
  });
});
