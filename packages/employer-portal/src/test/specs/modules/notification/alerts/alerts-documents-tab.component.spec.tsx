/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Tabs, ViewNotificationPermissions } from 'fineos-common';
import { render, act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  ContextWrapper,
  mockTranslations,
  releaseEventLoop
} from '../../../../common/utils';
import { AlertsDocumentsTab } from '../../../../../app/modules/notification/alerts/alerts-documents-tab';
import * as documentsApiModule from '../../../../../app/modules/notification/alerts/alerts-documents-tab/alerts-documents-tab.api';
import * as downloadApiModule from '../../../../../app/modules/notification/alerts/alerts-documents-tab/download-document-link/download-document-link.api';
import { DOCUMENTS_FIXTURE } from '../../../../fixtures/documents.fixture';

jest.mock(
  '../../../../../app/modules/notification/alerts/alerts-documents-tab/alerts-documents-tab.api',
  () => ({
    fetchDocuments: jest.fn()
  })
);

jest.mock(
  '../../../../../app/modules/notification/alerts/alerts-documents-tab/download-document-link/download-document-link.api',
  () => ({
    downloadDocument: jest.fn()
  })
);

const fetchDocuments = documentsApiModule.fetchDocuments as jest.Mock;
const downloadDocument = downloadApiModule.downloadDocument as jest.Mock;

describe('AlertsDocumentTab', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.ALERTS.DOCUMENTS.EMPTY',
    'NOTIFICATIONS.ALERTS.DOCUMENTS.DOWNLOAD_ERROR'
  );

  test('should show empty message if no documents available', async () => {
    render(
      <ContextWrapper translations={translations}>
        <Tabs>
          <AlertsDocumentsTab
            tab=""
            key=""
            caseId="id-1"
            documents={[]}
            isLoadingDocuments={false}
            fetchDocumentsError={null}
            refreshDocuments={fetchDocuments}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(
      screen.queryByText('NOTIFICATIONS.ALERTS.DOCUMENTS.EMPTY')
    ).toBeInTheDocument();
  });

  test('should show something went wrong in case of error', async () => {
    render(
      <ContextWrapper translations={translations}>
        <Tabs>
          <AlertsDocumentsTab
            tab=""
            key=""
            caseId="id-1"
            documents={[]}
            isLoadingDocuments={false}
            fetchDocumentsError={new Error('error')}
            refreshDocuments={fetchDocuments}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(screen.queryByTestId('something-went-wrong')).toBeInTheDocument();
  });

  test('should show documents if returned', async () => {
    render(
      <ContextWrapper translations={translations}>
        <Tabs>
          <AlertsDocumentsTab
            tab=""
            key=""
            caseId="id-1"
            documents={DOCUMENTS_FIXTURE}
            isLoadingDocuments={false}
            fetchDocumentsError={null}
            refreshDocuments={fetchDocuments}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    expect(screen.queryByText('Document 1')).toBeInTheDocument();
    expect(screen.queryByText('05-04-2019')).toBeInTheDocument();

    expect(screen.queryByText('Document 2')).toBeInTheDocument();
    expect(screen.queryByText('05-05-2019')).toBeInTheDocument();
  });

  test('should allow documents to be downloaded', async () => {
    render(
      <ContextWrapper
        translations={translations}
        permissions={[ViewNotificationPermissions.VIEW_CASES_DOWNLOAD_DOCUMENT]}
      >
        <Tabs>
          <AlertsDocumentsTab
            tab=""
            key=""
            caseId="id-1"
            documents={DOCUMENTS_FIXTURE}
            isLoadingDocuments={false}
            fetchDocumentsError={null}
            refreshDocuments={downloadDocument.mockRejectedValueOnce(
              Promise.resolve({
                fileName: 'test',
                fileExtension: 'json',
                base64EncodedFileContents: 'eyJhIjogMX0=',
                contentType: 'text/json'
              })
            )}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();

    const downloadLinks = screen.queryAllByTestId('document-download-btn');

    expect(downloadLinks).toHaveLength(2);

    userEvent.click(downloadLinks[0]);

    await act(() => releaseEventLoop());

    expect(downloadDocument).toHaveBeenCalled();
  });
});
