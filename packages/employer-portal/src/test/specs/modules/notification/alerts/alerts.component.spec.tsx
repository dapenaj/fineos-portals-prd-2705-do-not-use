/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { CustomerSummary } from 'fineos-js-api-client';
import { act, render, within, screen, wait } from '@testing-library/react';
import {
  ViewNotificationPermissions,
  MessagesPermissions
} from 'fineos-common';

import { ContextWrapper } from '../../../../common/utils';
import { Alerts } from '../../../../../app/modules/notification/alerts';

jest.mock(
  '../../../../../app/modules/notification/alerts/alerts-documents-tab/alerts-documents-tab.api',
  () => ({
    fetchDocuments: jest.fn().mockReturnValue(Promise.resolve([]))
  })
);
jest.mock('../../../../../app/shared/api/document.api.ts', () => ({
  fetchOutstandingInformation: jest.fn().mockReturnValue(
    Promise.resolve([
      {
        sourcePartyName: 'Limited',
        uploadCaseNumber: '1',
        informationType: 'Upload 1',
        infoReceived: false
      },
      {
        sourcePartyName: 'Limited',
        uploadCaseNumber: '1',
        informationType: 'Upload 2',
        infoReceived: true
      }
    ])
  )
}));
jest.mock('../../../../../app/shared/api/message.api.ts', () => ({
  fetchMessagesByCaseId: jest.fn().mockReturnValue(
    Promise.resolve([
      {
        id: '1',
        subject: 'Lorem ipsum dolor sit amet',
        narrative: 'Nam fringilla viverra nisi ut tristique.',
        msgOriginatesFromPortal: true,
        readByGroupClient: true,
        case: {
          id: 'NTN-24',
          caseReference: 'string',
          caseType: 'string'
        }
      },
      {
        id: '2',
        subject: 'Nunc imperdiet lectus dolor',
        narrative: 'Mauris vel diam id mi sit amet mauris.',
        msgOriginatesFromPortal: true,
        readByGroupClient: false,
        case: {
          id: 'NTN-24',
          caseReference: 'string',
          caseType: 'string'
        }
      }
    ])
  ),
  fetchUnreadMessagesCount: jest
    .fn()
    .mockReturnValue(Promise.resolve({ data: [], totalSize: 0 }))
}));

describe('Alerts', () => {
  const customer = {
    firstName: 'firstName',
    lastName: 'lastName'
  } as CustomerSummary;

  test('should not render if has no permission', () => {
    render(
      <ContextWrapper>
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    expect(
      screen.queryByTestId('alerts-documents-tab')
    ).not.toBeInTheDocument();
  });

  test('should render document tab if it has ViewNotificationPermissions.VIEW_CASES_DOCUMENTS_LIST permission', async () => {
    render(
      <ContextWrapper
        permissions={[ViewNotificationPermissions.VIEW_CASES_DOCUMENTS_LIST]}
      >
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.ALERTS.DOCUMENTS.EMPTY')
    ).toBeInTheDocument();
  });

  test('should render document tab if it has ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST permission', async () => {
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
      >
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.queryByTestId('alerts-outstanding-tab')).toBeInTheDocument();
  });

  test('should render messages tab if it has all web-messages permissions', async () => {
    render(
      <ContextWrapper
        permissions={[
          MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
          MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
          MessagesPermissions.ADD_CASE_WEB_MESSAGE,
          MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
        ]}
      >
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.queryByTestId('NOTIFICATIONS.ALERTS.MESSAGES.TITLE')
    ).toBeInTheDocument();
  });

  test('should not render messages tab if it doesnt have all web-messages permissions', async () => {
    render(
      <ContextWrapper permissions={[]}>
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.queryByTestId('NOTIFICATIONS.ALERTS.MESSAGES.TITLE')
    ).not.toBeInTheDocument();
  });

  test('should render a label for messages tab which contains amount of unread messages', async () => {
    render(
      <ContextWrapper
        permissions={[
          MessagesPermissions.VIEW_WEB_MESSAGES_LIST,
          MessagesPermissions.VIEW_CASES_WEB_MESSAGES_LIST,
          MessagesPermissions.ADD_CASE_WEB_MESSAGE,
          MessagesPermissions.MARK_READ_CASE_WEB_MESSAGE
        ]}
      >
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.queryByTestId('messages-tab-with-label')!).queryByTestId(
        'text-badge-label'
      )!.innerHTML
    ).toEqual('1');
  });

  test('should render a label for outstanding tab which contains required amount of actions', async () => {
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_CASES_OUTSTANDING_ITEMS_LIST
        ]}
      >
        <Alerts caseId="1" customer={customer} />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      within(screen.queryByTestId('outstanding-tab-with-label')!).queryByTestId(
        'text-badge-label'
      )!.innerHTML
    ).toEqual('1');
  });
});
