/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';
import { GroupClientNotification, CustomerSummary } from 'fineos-js-api-client';

import { NotificationHeader } from '../../../../../app/modules/notification/notification-header';
import { ContextWrapper, mockTranslations } from '../../../../common/utils';

const props = {
  notificationId: '310665',
  notification: {
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary
  } as GroupClientNotification
};

describe('NotificationHeader', () => {
  const translations = mockTranslations(
    'EMPLOYEE.FALLBACK.UNKNOWN_JOB_TITLE',
    'EMPLOYEE.FALLBACK.UNKNOWN_UNIT',
    'EMPLOYEE.FALLBACK.UNKNOWN_LOCATION'
  );

  test('render', () => {
    render(
      <ContextWrapper translations={translations}>
        <NotificationHeader
          notificationId={props.notificationId}
          notification={props.notification}
        />
      </ContextWrapper>
    );

    const element = screen.queryByTestId('notification-header');
    expect(element).toBeInTheDocument();

    expect(screen.queryByText('jobTitle')).toBeInTheDocument();
    expect(screen.queryByText('organisationUnit')).toBeInTheDocument();
    expect(screen.queryByText('workSite')).toBeInTheDocument();
  });
});
