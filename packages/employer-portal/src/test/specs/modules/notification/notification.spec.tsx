/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, RenderResult } from '@testing-library/react';
import { ViewNotificationPermissions } from 'fineos-common';

import {
  ContextWrapper,
  mockTranslations,
  releaseEventLoop
} from '../../../common/utils';
import { Notification } from '../../../../app/modules/notification';
import * as notificationApiModule from '../../../../app/modules/notification/notification.api';
import { NOTIFICATIONS_FIXTURE } from '../../../fixtures/notifications.fixture';
import { ABSENCE_PERIOD_DECISIONS_FIXTURE } from '../../../fixtures/absence-periods.fixture';
import { ACCOMMODATIONS_FIXTURE } from '../../../fixtures/accommodations.fixture';
import { DISABILITY_CLAIMS_FIXTURE } from '../../../fixtures/disability-claims.fixture';
import { CLAIM_DISABILITY_BENEFITS_FIXTURE } from '../../../fixtures/claim-disability-benefits.fixture';
import { PAID_LEAVE_BENEFITS_FIXTURE } from '../../../fixtures/paid-leave-benefits.fixture';
import { PortalEnvConfigService } from '../../../../app/shared';

const props = {
  notificationId: 'NTN-747'
};

jest.mock('../../../../app/modules/notification/notification.api', () => ({
  fetchNotification: jest.fn(),
  fetchEnhancedAbsencePeriodDecisions: jest.fn(),
  fetchAccommodationCases: jest.fn(),
  fetchDisabilityClaims: jest.fn(),
  fetchClaimDisabilityBenefits: jest.fn(),
  fetchPaidLeaveBenefits: jest.fn()
}));

const fetchNotification = notificationApiModule.fetchNotification as jest.Mock;
const fetchEnhancedAbsencePeriodDecisions = notificationApiModule.fetchEnhancedAbsencePeriodDecisions as jest.Mock;
const fetchAccommodationCases = notificationApiModule.fetchAccommodationCases as jest.Mock;
const fetchDisabilityClaims = notificationApiModule.fetchDisabilityClaims as jest.Mock;
const fetchClaimDisabilityBenefits = notificationApiModule.fetchClaimDisabilityBenefits as jest.Mock;
const fetchPaidLeaveBenefits = notificationApiModule.fetchPaidLeaveBenefits as jest.Mock;

describe('Notification detail', () => {
  const translations = mockTranslations(
    'NOTIFICATIONS.TIMELINE.TITLE',
    'NOTIFICATIONS.TIMELINE.SHOW_CAPTION',
    'NOTIFICATIONS.TIMELINE.GANTT_TITLE',
    'NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE',
    'NOTIFICATIONS.TYPE.WORKPLACE_ACCOMMODATION',
    'NAVIGATION.HOME',
    'DISABILITY_BENEFIT_STATUS.PENDING',
    'EMPLOYEE.FALLBACK.UNKNOWN_JOB_TITLE',
    'EMPLOYEE.FALLBACK.UNKNOWN_UNIT',
    'EMPLOYEE.FALLBACK.UNKNOWN_LOCATION',
    'NOTIFICATIONS.NOTIFICATION_SUMMARY.TITLE',
    'NOTIFICATIONS.NOTIFICATION_SUMMARY.CASE_PROGRESS',
    'HEADER.SEARCH.NOTIFICATION_SEARCH',
    'NOTIFICATIONS.NOTIFICATION_SUMMARY.NOTIFIED_ON',
    'NOTIFICATIONS.NOTIFICATION_SUMMARY.FIRST_DAY_MISSING',
    'NOTIFICATIONS.JOB_PROTECTED_LEAVE.PANEL_TITLE',
    'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEGEND.INTERMITTENT_TIME',
    'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEGEND.REDUCED_SCHEDULE',
    'NOTIFICATIONS.JOB_PROTECTED_LEAVE.LEGEND.CONTINUOUS_TIME',
    'DECISION_STATUS_CATEGORY.APPROVED',
    'DECISION_STATUS_CATEGORY.PENDING',
    'NOTIFICATIONS.TYPE.WAGE_REPLACEMENT',
    'NOTIFICATIONS.ACCOMMODATIONS.PANEL_TITLE',
    'NOTIFICATIONS.ACCOMMODATIONS.ACCOMMODATION_REQUEST',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.NOT_ACCOMMODATED',
    'NOTIFICATIONS.WAGE_REPLACEMENT.PANEL_TITLE',
    'NOTIFICATIONS.WAGE_REPLACEMENT.EMPTY',
    'NOTIFICATIONS.ACCOMMODATIONS.STATUS_LABEL.PENDING_ASSESSMENT',
    'NOTIFICATIONS.CASE_PROGRESS.PENDING',
    'NOTIFICATIONS.CASE_PROGRESS.UNDETERMINED',
    'NOTIFICATIONS.NOTIFICATION_SUMMARY.ACCIDENT_DATE'
  );

  describe('should render and make api requests based on permissions', () => {
    let notificationDetail: RenderResult;

    beforeEach(() => {
      const portalEnvConfigService = PortalEnvConfigService.getInstance();
      jest
        .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
        .mockReturnValueOnce(
          Promise.resolve({
            supportedLanguages: ['en'],
            decimalSeparator: { en: '.' }
          })
        );
    });
    const renderWithPermissions = (
      permissions: ViewNotificationPermissions[] = [],
      notificationId: string = props.notificationId
    ) =>
      render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <Notification notificationId={notificationId} />
        </ContextWrapper>
      );

    test('with no permissions', () => {
      notificationDetail = renderWithPermissions();
      const { queryByTestId } = notificationDetail;

      expect(queryByTestId('notification')).not.toBeInTheDocument();
      expect(queryByTestId('notification-summary')).not.toBeInTheDocument();
      expect(queryByTestId('job-protected-leave')).not.toBeInTheDocument();
      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
      expect(
        queryByTestId('workplace-accommodation-panel')
      ).not.toBeInTheDocument();
    });

    test('with view notifications', async () => {
      fetchNotification.mockReturnValueOnce(
        Promise.resolve(NOTIFICATIONS_FIXTURE[0])
      );

      notificationDetail = renderWithPermissions([
        ViewNotificationPermissions.VIEW_NOTIFICATIONS
      ]);

      const { getByTestId, queryByTestId } = notificationDetail;

      await act(() => {
        return releaseEventLoop();
      });

      expect(getByTestId('notification')).toBeInTheDocument();
      expect(getByTestId('notification-summary')).toBeInTheDocument();
      expect(queryByTestId('job-protected-leave')).not.toBeInTheDocument();
      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
      expect(
        queryByTestId('workplace-accommodation-panel')
      ).not.toBeInTheDocument();
    });

    test('with view absence period decisions', async () => {
      fetchNotification.mockReturnValueOnce(
        Promise.resolve(NOTIFICATIONS_FIXTURE[0])
      );

      fetchEnhancedAbsencePeriodDecisions.mockReturnValueOnce(
        Promise.resolve(ABSENCE_PERIOD_DECISIONS_FIXTURE)
      );

      notificationDetail = renderWithPermissions([
        ViewNotificationPermissions.VIEW_NOTIFICATIONS,
        ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS
      ]);

      const { getByTestId, queryByTestId } = notificationDetail;

      await act(() => {
        return releaseEventLoop();
      });
      expect(getByTestId('notification')).toBeInTheDocument();
      expect(getByTestId('notification-summary')).toBeInTheDocument();
      expect(getByTestId('job-protected-leave')).toBeInTheDocument();
      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
    });

    test('with view claims benefits list', async () => {
      fetchNotification.mockReturnValueOnce(
        Promise.resolve(NOTIFICATIONS_FIXTURE[0])
      );

      fetchDisabilityClaims.mockReturnValueOnce(
        Promise.resolve(DISABILITY_CLAIMS_FIXTURE)
      );

      fetchClaimDisabilityBenefits.mockRejectedValueOnce(
        Promise.resolve([CLAIM_DISABILITY_BENEFITS_FIXTURE[0]])
      );

      fetchPaidLeaveBenefits.mockRejectedValueOnce(
        Promise.resolve(PAID_LEAVE_BENEFITS_FIXTURE)
      );

      fetchEnhancedAbsencePeriodDecisions.mockRejectedValueOnce(
        Promise.resolve(ABSENCE_PERIOD_DECISIONS_FIXTURE)
      );

      notificationDetail = renderWithPermissions([
        ViewNotificationPermissions.VIEW_NOTIFICATIONS,
        ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
        ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST,
        ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_BENEFIT
      ]);

      const { getByTestId, queryByTestId } = notificationDetail;

      await act(() => {
        return releaseEventLoop();
      });

      expect(getByTestId('notification')).toBeInTheDocument();
      expect(getByTestId('notification-summary')).toBeInTheDocument();
      expect(queryByTestId('job-protected-leave')).not.toBeInTheDocument();
      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
      expect(
        queryByTestId('workplace-accommodation-panel')
      ).not.toBeInTheDocument();
    });

    test('with view accommodation details', async () => {
      fetchNotification.mockReturnValueOnce(
        Promise.resolve(NOTIFICATIONS_FIXTURE[0])
      );

      fetchEnhancedAbsencePeriodDecisions.mockReturnValueOnce(
        Promise.resolve(ABSENCE_PERIOD_DECISIONS_FIXTURE)
      );

      fetchAccommodationCases.mockReturnValue(
        Promise.resolve(ACCOMMODATIONS_FIXTURE)
      );

      notificationDetail = renderWithPermissions([
        ViewNotificationPermissions.VIEW_NOTIFICATIONS,
        ViewNotificationPermissions.VIEW_ACCOMMODATION_DETAILS,
        ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS
      ]);

      const { getByTestId, queryByTestId } = notificationDetail;

      await act(() => {
        return releaseEventLoop();
      });

      expect(getByTestId('notification')).toBeInTheDocument();
      expect(getByTestId('notification-summary')).toBeInTheDocument();
      expect(getByTestId('timeline')).toBeInTheDocument();
      expect(getByTestId('job-protected-leave')).toBeInTheDocument();
      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
    });

    test('should rerender with the correct values', async () => {
      fetchNotification.mockReturnValueOnce(
        Promise.resolve(NOTIFICATIONS_FIXTURE[0])
      );
      fetchEnhancedAbsencePeriodDecisions.mockReturnValueOnce(
        Promise.resolve(ABSENCE_PERIOD_DECISIONS_FIXTURE)
      );

      fetchDisabilityClaims.mockReturnValueOnce(
        Promise.resolve(DISABILITY_CLAIMS_FIXTURE)
      );

      fetchClaimDisabilityBenefits.mockReturnValueOnce(
        Promise.resolve(CLAIM_DISABILITY_BENEFITS_FIXTURE)
      );

      fetchPaidLeaveBenefits.mockReturnValueOnce(
        Promise.resolve(PAID_LEAVE_BENEFITS_FIXTURE)
      );

      fetchAccommodationCases.mockReturnValue(
        Promise.resolve(ACCOMMODATIONS_FIXTURE)
      );

      const permissions = [
        ViewNotificationPermissions.VIEW_NOTIFICATIONS,
        ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
        ViewNotificationPermissions.VIEW_CLAIMS_BENEFITS_LIST,
        ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_BENEFIT,
        ViewNotificationPermissions.VIEW_ACCOMMODATION_DETAILS,
        ViewNotificationPermissions.VIEW_CLAIMS_READ_DISABILITY_DETAILS
      ];

      const { getByTestId, queryByTestId, getAllByTestId, rerender } = render(
        <ContextWrapper translations={translations} permissions={permissions}>
          <Notification notificationId={props.notificationId} />
        </ContextWrapper>
      );
      await act(() => {
        return releaseEventLoop();
      });

      const wageReplacements = getAllByTestId('wage-replacement');
      expect(getByTestId('notification')).toBeInTheDocument();
      expect(getByTestId('notification-summary')).toBeInTheDocument();
      expect(getByTestId('timeline')).toBeInTheDocument();
      expect(getByTestId('job-protected-leave')).toBeInTheDocument();
      expect(wageReplacements[0]).toBeInTheDocument();
      expect(wageReplacements[1]).toBeInTheDocument();
      expect(wageReplacements.length).toBe(2);
      expect(
        queryByTestId('workplace-accommodation-panel')
      ).toBeInTheDocument();

      fetchNotification.mockReturnValueOnce(
        Promise.resolve(NOTIFICATIONS_FIXTURE[1])
      );

      fetchEnhancedAbsencePeriodDecisions.mockReturnValueOnce(
        Promise.resolve([])
      );

      fetchAccommodationCases.mockReturnValue(Promise.resolve([]));

      fetchDisabilityClaims.mockReturnValueOnce(Promise.resolve([]));

      fetchClaimDisabilityBenefits.mockReturnValueOnce(Promise.resolve([]));

      fetchPaidLeaveBenefits.mockReturnValueOnce(Promise.resolve([]));

      rerender(
        <ContextWrapper translations={translations} permissions={permissions}>
          <Notification notificationId={'NTN-11'} />
        </ContextWrapper>
      );

      await act(() => {
        return releaseEventLoop();
      });

      expect(getByTestId('notification')).toBeInTheDocument();
      expect(getByTestId('notification-summary')).toBeInTheDocument();
      expect(queryByTestId('timeline')).not.toBeInTheDocument();
      expect(queryByTestId('job-protected-leave')).not.toBeInTheDocument();
      expect(queryByTestId('wage-replacement')).not.toBeInTheDocument();
      expect(
        queryByTestId('workplace-accommodation-panel')
      ).not.toBeInTheDocument();
    });
  });
});
