/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen } from '@testing-library/react';
import { ViewPaymentsPermissions } from 'fineos-common';

import {
  mockTranslations,
  ContextWrapper,
  releaseEventLoop
} from '../../../../common/utils';
import { LatestPayments } from '../../../../../app/modules/notification/latest-payments';
import * as paymentsApiModule from '../../../../../app/modules/notification/latest-payments/latest-payments.api';
import { PAYMENTS_FIXTURE } from '../../../../fixtures/payments.fixture';

describe('Latest Payments', () => {
  const permissions = [ViewPaymentsPermissions.VIEW_CLAIMS_PAYMENTS_LIST];
  const translations = mockTranslations(
    'PAYMENTS.LATEST_PAYMENTS',
    'PAYMENTS.PAYMENTS_VIEW_HISTORY_LINK'
  );

  beforeEach(() => {
    jest.spyOn(paymentsApiModule, 'fetchPayments');
  });

  test('should not be rendered without permissions', () => {
    render(
      <ContextWrapper>
        <LatestPayments
          benefitId={'asdf'}
          claimId={'CLM-1'}
          benefitCaseType="Short Term Disability"
        />
      </ContextWrapper>
    );

    expect(
      screen.queryByText('PAYMENTS.LATEST_PAYMENTS')
    ).not.toBeInTheDocument();
  });

  test('should not be rendered without payments', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve([])
    );

    render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <LatestPayments
          benefitId={'asdf'}
          claimId={'CLM-1'}
          benefitCaseType="Short Term Disability"
        />
      </ContextWrapper>
    );

    await act(() => {
      return releaseEventLoop();
    });

    expect(
      screen.queryByText('PAYMENTS.LATEST_PAYMENTS')
    ).not.toBeInTheDocument();
  });

  test('should render', async () => {
    (paymentsApiModule.fetchPayments as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENTS_FIXTURE)
    );

    render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <LatestPayments
          benefitId={'asdf'}
          claimId={'CLM-1'}
          benefitCaseType="Short Term Disability"
        />
      </ContextWrapper>
    );

    await act(() => {
      return releaseEventLoop();
    });

    expect(screen.getByText('PAYMENTS.LATEST_PAYMENTS')).toBeInTheDocument();
  });
});
