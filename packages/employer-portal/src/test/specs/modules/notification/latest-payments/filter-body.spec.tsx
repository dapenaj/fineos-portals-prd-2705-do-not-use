/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Formik } from 'formik';

import { FilterBody } from '../../../../../app/modules/notification/latest-payments/payments-history/filter-body';
import {
  ContextWrapper,
  mockTranslations,
  releaseEventLoop,
  buildTranslationWithParams,
  resolveTranslationWithParams
} from '../../../../common/utils';
import DEFAULT_CONFIG from '../../../../../app/shared/client-config/default-config.json';
import {
  PAYMENTS_FIXTURE,
  GROUP_CLIENT_PAYMENTS_FIXTURE
} from '../../../../fixtures/payments.fixture';

describe('FilterBody', () => {
  const translations = mockTranslations(
    'PAYMENTS.FILTER_INFO',
    'PAYMENTS.RESET_FILTER',
    'PAYMENTS.EMPTY_STATE',
    'PAYMENTS.FILTER_DATE',
    'PAYMENTS.DATE_RANGE',
    'PAYMENTS.FROM',
    'PAYMENTS.TO',
    'PAYMENTS.APPLY_FILTER',
    buildTranslationWithParams('PAYMENTS.PAYMENTS_INFO', [
      'visiblePayments',
      'allPayments'
    ])
  );

  test('should render', async () => {
    render(
      <ContextWrapper translations={translations}>
        <FilterBody
          benefitCaseType="Short Term Disability"
          payments={GROUP_CLIENT_PAYMENTS_FIXTURE}
          claimId="CLM-2"
        />
      </ContextWrapper>
    );
    await act(async () => {
      await releaseEventLoop();
    });
    expect(screen.queryByText('PAYMENTS.FILTER_INFO')).not.toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.FILTER_DATE')).toBeInTheDocument();
  });

  test('should render empty state', async () => {
    render(
      <ContextWrapper translations={translations}>
        <FilterBody
          benefitCaseType="Short Term Disability"
          payments={[]}
          claimId="CLM-2"
        />
      </ContextWrapper>
    );
    await act(async () => {
      await releaseEventLoop();
    });
    expect(screen.getByText('PAYMENTS.FILTER_DATE')).toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.EMPTY_STATE')).toBeInTheDocument();
  });

  test('should render correct header', () => {
    const sizeList = DEFAULT_CONFIG.configurableSizeList.PAYMENTS_HISTORY;
    const mockPayment = PAYMENTS_FIXTURE[0];
    const { rerender } = render(
      <ContextWrapper translations={translations}>
        <FilterBody
          benefitCaseType="Short Term Disability"
          payments={new Array(sizeList + 1).fill(mockPayment)}
          claimId="CLM-2"
        />
      </ContextWrapper>
    );
    expect(
      screen.getByText(
        resolveTranslationWithParams('PAYMENTS.PAYMENTS_INFO', [
          ['visiblePayments', sizeList],
          ['allPayments', sizeList + 1]
        ])
      )
    ).toBeInTheDocument();

    rerender(
      <ContextWrapper translations={translations}>
        <FilterBody
          benefitCaseType="Short Term Disability"
          payments={new Array(sizeList - 1).fill(mockPayment)}
          claimId="CLM-2"
        />
      </ContextWrapper>
    );
    expect(
      screen.getByText(
        resolveTranslationWithParams('PAYMENTS.PAYMENTS_INFO', [
          ['visiblePayments', sizeList - 1],
          ['allPayments', sizeList - 1]
        ])
      )
    ).toBeInTheDocument();
  });

  test('should filter and reset filter', async () => {
    render(
      <ContextWrapper translations={translations}>
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <FilterBody
            benefitCaseType="Short Term Disability"
            payments={GROUP_CLIENT_PAYMENTS_FIXTURE}
            claimId="CLM-2"
          />
        </Formik>
      </ContextWrapper>
    );

    expect(screen.getAllByTestId('payment-header')).toHaveLength(
      GROUP_CLIENT_PAYMENTS_FIXTURE.length
    );

    userEvent.click(
      screen.getByRole('button', { name: /PAYMENTS.FILTER_DATE/ })
    );

    await act(async () => {
      await releaseEventLoop();
    });

    expect(document.querySelector('input[name="from"]')!).toBeInTheDocument();

    userEvent.click(document.querySelector('input[name="from"]')!);
    userEvent.click(screen.getAllByText('15')[0]);

    userEvent.click(document.querySelector('input[name="until"]')!);
    userEvent.click(screen.getAllByText('16')[1]);

    await act(async () => {
      await releaseEventLoop();
    });

    userEvent.click(screen.getByText(/PAYMENTS.APPLY_FILTER/));

    await act(async () => {
      await releaseEventLoop();
    });

    userEvent.click(
      screen.getByRole('button', { name: /PAYMENTS.RESET_FILTER/ })
    );

    await act(async () => {
      await releaseEventLoop();
    });

    expect(screen.getAllByTestId('payment-header')).toHaveLength(
      GROUP_CLIENT_PAYMENTS_FIXTURE.length
    );
  });
});
