/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';

import { DateRangeDrawer } from '../../../../../app/modules/notification/latest-payments/payments-history/date-range-drawer';
import { render, wait, act, screen } from '../../../../common/custom-render';
import { ContextWrapper } from '../../../../common/utils';

describe('DateRangeDrawer', () => {
  test('render', async () => {
    render(
      <ContextWrapper>
        <DateRangeDrawer
          isVisible={true}
          onClose={jest.fn()}
          onFilter={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(screen.getByText('PAYMENTS.DATE_RANGE')).toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.FROM')).toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.TO')).toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.APPLY_FILTER')).toBeInTheDocument();
    expect(screen.getByText('FINEOS_COMMON.GENERAL.CLOSE')).toBeInTheDocument();
  });

  test('should show required error if fields are not filled in', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <DateRangeDrawer
          isVisible={true}
          onClose={jest.fn()}
          onFilter={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(getSubmitButtonByText('PAYMENTS.APPLY_FILTER'));
    await act(wait);

    expect(
      screen.getAllByText('FINEOS_COMMON.VALIDATION.REQUIRED')
    ).toHaveLength(2);
  });

  test('should filter', async () => {
    const mockOnFilter = jest.fn();
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <DateRangeDrawer
          isVisible={true}
          onClose={jest.fn()}
          onFilter={mockOnFilter}
        />
      </ContextWrapper>
    );

    await act(wait);

    // TODO debug and fix selectDate and apply it here instead
    userEvent.click(screen.getByTestId('from'));
    userEvent.click(screen.getAllByText('15')[0]);
    await act(wait);

    // TODO debug and fix selectDate and apply it here instead
    userEvent.click(screen.getByTestId('until'));
    userEvent.click(screen.getAllByText('16')[1]);
    await act(wait);

    expect(screen.getByTestId('from')).toBeValid();
    expect(screen.getByTestId('until')).toBeValid();

    userEvent.click(getSubmitButtonByText('PAYMENTS.APPLY_FILTER'));
    await act(wait);

    expect(mockOnFilter).toHaveBeenCalled();
  });
});
