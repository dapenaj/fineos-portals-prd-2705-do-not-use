/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act } from '@testing-library/react';
import { ViewPaymentsPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { PaymentBreakdown } from '../../../../../app/modules/notification/latest-payments';
import {
  mockTranslations,
  ContextWrapper,
  releaseEventLoop
} from '../../../../common/utils';
import { GROUP_CLIENT_SINGLE_PAYMENT_FIXTURE } from '../../../../fixtures/payments.fixture';
import { PAYMENT_LINES_FIXTURE } from '../../../../fixtures/payment-lines.fixture';

describe('Payment Breakdown', () => {
  const permissions = [ViewPaymentsPermissions.VIEW_CLAIMS_PAYMENT_LINES_LIST];
  const translations = mockTranslations(
    'PAYMENTS.DETAILS_PLUS',
    'PAYMENTS.PAID_TO',
    'PAYMENTS.START_DATE',
    'PAYMENTS.END_DATE',
    'PAYMENTS.DETAILS_MINUS',
    'PAYMENTS.NET_PAYMENT_AMOUNT'
  );

  test('render', async () => {
    const { getByText, getAllByText } = render(
      <ContextWrapper permissions={permissions} translations={translations}>
        <PaymentBreakdown
          payment={GROUP_CLIENT_SINGLE_PAYMENT_FIXTURE[0]}
          paymentLines={PAYMENT_LINES_FIXTURE}
        />
      </ContextWrapper>
    );

    expect(getAllByText('Federal Income Tax')[0]).toBeInTheDocument();
    expect(getByText('PAYMENTS.NET_PAYMENT_AMOUNT')).toBeInTheDocument();
    expect(getByText('PAYMENTS.DETAILS_PLUS')).toBeInTheDocument();

    await act(() => {
      userEvent.click(getByText('PAYMENTS.DETAILS_PLUS')!);
      return releaseEventLoop();
    });
    expect(getByText('PAYMENTS.DETAILS_MINUS')).toBeInTheDocument();
    expect(getByText('PAYMENTS.PAID_TO')).toBeInTheDocument();
  });
});
