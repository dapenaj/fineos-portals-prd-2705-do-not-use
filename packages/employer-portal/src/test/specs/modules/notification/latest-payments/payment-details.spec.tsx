/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen } from '@testing-library/react';

import { PaymentDetails } from '../../../../../app/modules/notification/latest-payments/payment';
import { ContextWrapper, mockTranslations } from '../../../../common/utils';
import { PAYMENTS_FIXTURE } from '../../../../fixtures/payments.fixture';

describe('PaymentDetails', () => {
  const translations = mockTranslations(
    'PAYMENTS.PAID_TO',
    'PAYMENTS.PAYMENT_METHOD',
    'PAYMENTS.EFFECTIVE_DATE',
    'PAYMENTS.PERIOD_START_DATE',
    'PAYMENTS.PERIOD_END_DATE'
  );

  test('render not recurring', () => {
    const { getByText, queryByText } = render(
      <ContextWrapper translations={translations}>
        <PaymentDetails payment={PAYMENTS_FIXTURE[0]} />
      </ContextWrapper>
    );
    expect(screen.getAllByText('PAYMENTS.PAID_TO')).toHaveLength(2);
    expect(getByText('PAYMENTS.PAYMENT_METHOD')).toBeInTheDocument();
    expect(getByText('PAYMENTS.EFFECTIVE_DATE')).toBeInTheDocument();
    expect(getByText('01-01-2050')).toBeInTheDocument();
    expect(queryByText('PAYMENTS.PERIOD_START_DATE')).not.toBeInTheDocument();
    expect(queryByText('PAYMENTS.PERIOD_END_DATE')).not.toBeInTheDocument();
  });

  test('render recurring', () => {
    const { queryByText, getByText } = render(
      <ContextWrapper translations={translations}>
        <PaymentDetails payment={PAYMENTS_FIXTURE[1]} />
      </ContextWrapper>
    );
    expect(screen.getAllByText('PAYMENTS.PAID_TO')).toHaveLength(2);
    expect(getByText('PAYMENTS.PAYMENT_METHOD')).toBeInTheDocument();
    expect(getByText('PAYMENTS.PERIOD_START_DATE')).toBeInTheDocument();
    expect(getByText('PAYMENTS.PERIOD_END_DATE')).toBeInTheDocument();
    expect(getByText('10-01-2050')).toBeInTheDocument();
    expect(getByText('15-01-2050')).toBeInTheDocument();
    expect(queryByText('PAYMENTS.EFFECTIVE_DATE')).not.toBeInTheDocument();
  });
});
