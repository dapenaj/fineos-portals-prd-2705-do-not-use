/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { GroupClientPayment } from 'fineos-js-api-client';

import { PaymentsHistory } from '../../../../../app/modules/notification/latest-payments/payments-history';
import { ContextWrapper } from '../../../../common/utils';
import { PAYMENTS_FIXTURE } from '../../../../fixtures/payments.fixture';

describe('PaymentsHistory', async () => {
  test('render', async () => {
    render(
      <ContextWrapper>
        <PaymentsHistory
          benefitCaseType="Short Term Disability"
          payments={PAYMENTS_FIXTURE as GroupClientPayment[]}
          claimId="CLM-2"
        />
      </ContextWrapper>
    );
    await act(wait);

    expect(
      screen.queryByText('PAYMENTS.PAYMENTS_VIEW_HISTORY_LINK')
    ).toBeInTheDocument();
  });

  test('open history modal', async () => {
    render(
      <ContextWrapper>
        <PaymentsHistory
          benefitCaseType="Short Term Disability"
          payments={PAYMENTS_FIXTURE as GroupClientPayment[]}
          claimId="CLM-2"
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('PAYMENTS.PAYMENTS_VIEW_HISTORY_LINK')
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('PAYMENTS.PAYMENTS_VIEW_HISTORY_LINK')!);
    await act(wait);

    expect(
      screen.getByText('PAYMENTS.PAYMENTS_VIEW_HISTORY')
    ).toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.PAYMENTS_INFO')).toBeInTheDocument();
    expect(screen.getByText('PAYMENTS.FILTER_DATE')).toBeInTheDocument();
    expect(screen.getByText('FINEOS_COMMON.GENERAL.CLOSE')).toBeInTheDocument();
  });
});
