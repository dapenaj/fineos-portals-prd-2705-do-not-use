/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, fireEvent, act, screen } from '@testing-library/react';

import { PaymentMethodTooltip } from '../../../../../app/modules/notification/latest-payments/payment/payment-details/payment-method-tooltip';
import { ContextWrapper, mockTranslations } from '../../../../common/utils';
import { PAYMENTS_FIXTURE } from '../../../../fixtures/payments.fixture';

describe('PaymentTooltip', () => {
  const translations = mockTranslations(
    'PAYMENTS.PAID_TO',
    'PAYMENTS.BANK_INSTITUTION',
    'PAYMENTS.ACCOUNT_NUMBER',
    'PAYMENTS.CHECK_NUMBER'
  );
  test('render', () => {
    render(
      <ContextWrapper>
        <PaymentMethodTooltip payment={PAYMENTS_FIXTURE[0]} />
      </ContextWrapper>
    );
    expect(screen.getByTestId('tooltip')).toBeInTheDocument();
    expect(screen.getByTestId('payment-info-icon')).toBeInTheDocument();
  });

  test('content showing', async () => {
    const { rerender } = render(
      <ContextWrapper translations={translations}>
        <PaymentMethodTooltip payment={PAYMENTS_FIXTURE[0]} />
      </ContextWrapper>
    );
    act(() => {
      fireEvent.mouseEnter(screen.getByTestId('payment-info-icon'));
    });

    expect(await screen.findByText('PAYMENTS.PAID_TO')).toBeInTheDocument();
    expect(
      await screen.findByText('PAYMENTS.BANK_INSTITUTION')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('PAYMENTS.ACCOUNT_NUMBER')
    ).toBeInTheDocument();
    expect(
      await screen.findByText(PAYMENTS_FIXTURE[0].nominatedPayeeName)
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        PAYMENTS_FIXTURE[0].accountTransferInfo.bankInstituteName!
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        PAYMENTS_FIXTURE[0].accountTransferInfo.bankAccountNumber!
      )
    ).toBeInTheDocument();

    rerender(
      <ContextWrapper translations={translations}>
        <PaymentMethodTooltip payment={PAYMENTS_FIXTURE[1]} />
      </ContextWrapper>
    );
    act(() => {
      fireEvent.mouseEnter(screen.getByTestId('payment-info-icon'));
    });

    expect(await screen.findByText('PAYMENTS.PAID_TO')).toBeInTheDocument();
    expect(
      await screen.findByText('PAYMENTS.CHECK_NUMBER')
    ).toBeInTheDocument();
    expect(
      await screen.findByText(PAYMENTS_FIXTURE[1].nominatedPayeeName)
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        PAYMENTS_FIXTURE[1].chequePaymentInfo.chequeNumber!
      )
    ).toBeInTheDocument();
  });
});
