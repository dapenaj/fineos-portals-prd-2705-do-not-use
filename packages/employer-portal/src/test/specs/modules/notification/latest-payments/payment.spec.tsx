/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, fireEvent } from '@testing-library/react';
import { ViewPaymentsPermissions } from 'fineos-common';
import { noop as _noop } from 'lodash';
import userEvent from '@testing-library/user-event';

import {
  mockTranslations,
  ContextWrapper,
  releaseEventLoop
} from '../../../../common/utils';
import { Payment } from '../../../../../app/modules/notification/latest-payments';
import { GROUP_CLIENT_SINGLE_PAYMENT_FIXTURE } from '../../../../fixtures/payments.fixture';
import * as paymentsApiModule from '../../../../../app/modules/notification/latest-payments/latest-payments.api';
import { PAYMENT_LINES_FIXTURE } from '../../../../fixtures/payment-lines.fixture';

describe('Payment Breakdown', () => {
  const translations = mockTranslations(
    'PAYMENTS.NET_PAYMENT_AMOUNT',
    'PAYMENTS.SHOW_BREAKDOWN',
    'PAYMENTS.HIDE_BREAKDOWN',
    'PAYMENTS.PAID_TO',
    'PAYMENTS.PAYMENT_METHOD',
    'PAYMENTS.EFFECTIVE_DATE',
    'PAYMENTS.DETAILS_PLUS'
  );
  const permissions = [ViewPaymentsPermissions.VIEW_CLAIMS_PAYMENT_LINES_LIST];

  beforeEach(() => {
    jest.spyOn(paymentsApiModule, 'fetchPaymentLines');
  });

  test('render', async () => {
    (paymentsApiModule.fetchPaymentLines as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PAYMENT_LINES_FIXTURE)
    );

    const { getByText, queryByText, queryByTestId } = render(
      <ContextWrapper translations={translations} permissions={permissions}>
        <Payment
          claimId={'CLM-1'}
          payment={GROUP_CLIENT_SINGLE_PAYMENT_FIXTURE[0]}
        />
      </ContextWrapper>
    );

    expect(getByText('$5,342.65')).toBeInTheDocument();

    await act(async () => {
      fireEvent.click(getByText('$5,342.65')!);
      return releaseEventLoop();
    });
    expect(getByText('PAYMENTS.SHOW_BREAKDOWN')).toBeInTheDocument();
    expect(queryByText('PAYMENTS.NET_PAYMENT_AMOUNT')).not.toBeInTheDocument();

    await act(() => {
      userEvent.click(queryByTestId('show-payment-breakdown')!);
      return releaseEventLoop();
    });
    expect(getByText('PAYMENTS.NET_PAYMENT_AMOUNT')).toBeInTheDocument();
    expect(getByText('PAYMENTS.HIDE_BREAKDOWN')).toBeInTheDocument();
  });
});
