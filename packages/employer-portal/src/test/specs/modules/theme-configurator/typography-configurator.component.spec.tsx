/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, act } from '@testing-library/react';

import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import {
  ContextWrapper,
  mockTranslations,
  releaseEventLoop
} from '../../../common/utils';
import { TypographyConfigurator } from '../../../../app/modules/theme-configurator/typography-configurator/typography-configurator.component';

describe('TypographyConfigurator', () => {
  const translations = mockTranslations(
    'THEME_CONFIGURATOR.TYPOGRAPHY.HEAD',
    'THEME_CONFIGURATOR.TYPOGRAPHY.ADVANCED_MODE',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_FAMILY_LABEL',
    'FINEOS_COMMON.GENERAL.LOAD',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_WEIGHT_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_SIZE_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.LINE_HEIGHT_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_FAMILY_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_NORMAL'
  );
  test('should render', async () => {
    const { getByText } = render(
      <ContextWrapper translations={translations}>
        <TypographyConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={jest.fn}
        />
      </ContextWrapper>
    );

    expect(getByText('THEME_CONFIGURATOR.TYPOGRAPHY.HEAD')).toBeInTheDocument();
  });

  test('should call onConfigChange', async () => {
    const onMockConfigChange = jest.fn();
    const { getAllByText, getAllByLabelText } = render(
      <ContextWrapper translations={translations}>
        <TypographyConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={onMockConfigChange}
        />
      </ContextWrapper>
    );

    await act(async () => {
      await userEvent.type(
        getAllByLabelText('THEME_CONFIGURATOR.TYPOGRAPHY.FONT_FAMILY_LABEL')[0],
        'Girrasol'
      );
      userEvent.click(getAllByText('FINEOS_COMMON.GENERAL.LOAD')[0]);
      return releaseEventLoop();
    });

    expect(onMockConfigChange).toHaveBeenCalled();
  });
});
