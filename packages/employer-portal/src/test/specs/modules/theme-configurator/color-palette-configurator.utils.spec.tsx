/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import * as utils from '../../../../app/modules/theme-configurator/color-palette-configurator/color-palette-configurator.utils';

describe('ColorPaletteConfiguratorUtils', () => {
  test('should call toColor() and return correct value', () => {
    expect(utils.toColor('var(--theme-color-neutral1, #ffffff)')).toBe(
      '#ffffff'
    );
    expect(utils.toColor('#ffffff')).toBe('#ffffff');
  });

  test('should call toRgbaColor() and return correct value', () => {
    expect(utils.toRgbaColor('var(--theme-color-neutral1, #ffffff)')).toEqual([
      255,
      255,
      255,
      1
    ]);
    expect(utils.toRgbaColor('#ffffff')).toEqual([255, 255, 255, 1]);
    expect(utils.toRgbaColor('string')).toEqual([0, 0, 0, 1]);
  });

  test('should call makeColorBright() amd return correct value', () => {
    expect(utils.makeColorBright('var(--theme-color-neutral1, #ffffff) ')).toBe(
      'rgba(255, 255, 255, 0.15)'
    );
    expect(utils.makeColorBright('#ffffff')).toBe('rgba(255, 255, 255, 0.15)');
  });
});
