/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ContextWrapper } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { ImageConfigurator } from '../../../../app/modules/theme-configurator/image-configurator/image-configurator.component';

describe('ImageConfigurator', () => {
  test('should render', () => {
    const { getAllByRole, getByText } = render(
      <ContextWrapper>
        <ImageConfigurator
          currentImages={CONFIG_FIXTURE.theme.images!}
          onConfigChange={jest.fn()}
          onCustomImageAdd={jest.fn()}
        />
      </ContextWrapper>
    );
    expect(getByText('THEME_CONFIGURATOR.IMAGES.HEAD')).toBeInTheDocument();
    expect(
      getAllByRole('button', { name: 'FINEOS_COMMON.GENERAL.LOAD' })
    ).toHaveLength(Object.keys(CONFIG_FIXTURE.theme.images!).length);
  });

  test('should open and close change image modal', async () => {
    render(
      <ContextWrapper>
        <ImageConfigurator
          currentImages={CONFIG_FIXTURE.theme.images!}
          onConfigChange={jest.fn()}
          onCustomImageAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    act(() => {
      userEvent.click(screen.getAllByText('FINEOS_COMMON.GENERAL.LOAD')[0]);
    });

    expect(
      screen.getByText('THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_MODAL_TITLE')
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('FINEOS_COMMON.UPLOAD.CANCEL'));
  });
});
