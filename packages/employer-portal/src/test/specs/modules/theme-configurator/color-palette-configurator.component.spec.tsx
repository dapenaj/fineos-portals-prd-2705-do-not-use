/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import { ContextWrapper, mockTranslations } from '../../../common/utils';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { ColorPaletteConfigurator } from '../../../../app/modules/theme-configurator/color-palette-configurator/color-palette-configurator.component';

describe('ColorPaletteConfigurator', () => {
  const translations = mockTranslations('THEME_CONFIGURATOR.COLORS.HEAD');

  test('should render', () => {
    const { getByText } = render(
      <ContextWrapper translations={translations}>
        <ColorPaletteConfigurator
          currentColorSchema={CONFIG_FIXTURE.theme.colorSchema}
          onConfigChange={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(getByText('THEME_CONFIGURATOR.COLORS.HEAD')).toBeInTheDocument();
    expect(document.querySelectorAll('[class^=groupHead]').length).toBe(
      Object.keys(CONFIG_FIXTURE.theme.colorSchema).length
    );
  });

  test('should append and remove style tag from document head', () => {
    const styleSelector = 'style#color-debug';
    const { getByText } = render(
      <ContextWrapper translations={translations}>
        <ColorPaletteConfigurator
          currentColorSchema={CONFIG_FIXTURE.theme.colorSchema}
          onConfigChange={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(document.head.querySelectorAll(styleSelector).length).toBe(0);

    fireEvent.click(getByText('primaryColor'));
    expect(document.head.querySelectorAll(styleSelector).length).toBe(1);

    fireEvent.click(getByText('primaryColor'));
    expect(document.head.querySelectorAll(styleSelector).length).toBe(0);
  });

  test('should call onColorSchemaChange', () => {
    const mockOnConfigChange = jest.fn();
    render(
      <ContextWrapper translations={translations}>
        <ColorPaletteConfigurator
          currentColorSchema={CONFIG_FIXTURE.theme.colorSchema}
          onConfigChange={mockOnConfigChange}
        />
      </ContextWrapper>
    );
    fireEvent.change(document.querySelectorAll('[class*=InputColor]')[0]);

    expect(mockOnConfigChange).toHaveBeenCalled();
  });
});
