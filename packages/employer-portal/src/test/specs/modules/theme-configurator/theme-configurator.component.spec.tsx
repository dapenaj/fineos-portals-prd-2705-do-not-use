/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import each from 'jest-each';
import { render, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ThemeConfigurator } from '../../../../app/modules/theme-configurator/theme-configurator.component';
import { ContextWrapper, mockTranslations } from '../../../common/utils';
import {
  EMPTY_CONFIG_FIXTURE,
  CONFIG_FIXTURE
} from '../../../fixtures/theme-configurator.fixture';

describe('ThemeConfigurator', () => {
  const sectionNames = [
    'THEME_CONFIGURATOR.NAVBAR.COLORS',
    'THEME_CONFIGURATOR.NAVBAR.TYPOGRAPHY',
    'THEME_CONFIGURATOR.NAVBAR.IMAGES',
    'THEME_CONFIGURATOR.NAVBAR.HEADER',
    'THEME_CONFIGURATOR.NAVBAR.REVERT',
    'THEME_CONFIGURATOR.NAVBAR.EXPORT'
  ];

  const translations = mockTranslations(
    ...sectionNames,
    'THEME_CONFIGURATOR.TYPOGRAPHY.ADVANCED_MODE',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_FAMILY_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_WEIGHT_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_SIZE_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.LINE_HEIGHT_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_LABEL',
    'THEME_CONFIGURATOR.TYPOGRAPHY.FONT_STYLE_NORMAL',
    'THEME_CONFIGURATOR.TEXT.SEARCH_LABEL',
    'THEME_CONFIGURATOR.TEXT.SEARCH_MODE_TITLE',
    'THEME_CONFIGURATOR.TEXT.SEARCH_MODE_TEXT',
    'THEME_CONFIGURATOR.TEXT.CHANGE_BUTTON',
    'FINEOS_COMMON.GENERAL.LOAD',
    'THEME_CONFIGURATOR.HEADER.PAGE_TITLE',
    'THEME_CONFIGURATOR.HEADER.TITLE_SEPARATOR',
    'THEME_CONFIGURATOR.COLORS.HEAD',
    'THEME_CONFIGURATOR.TYPOGRAPHY.HEAD',
    'THEME_CONFIGURATOR.IMAGES.HEAD',
    'THEME_CONFIGURATOR.TEXT.HEAD',
    'THEME_CONFIGURATOR.HEADER.HEAD'
  );

  describe('should render', () => {
    each(sectionNames).test('section: %s', section => {
      const { getByText } = render(
        <ContextWrapper translations={translations}>
          <ThemeConfigurator
            isHighContrastOn={false}
            config={CONFIG_FIXTURE}
            onConfigChange={jest.fn()}
          />
        </ContextWrapper>
      );
      expect(getByText(section)).toBeInTheDocument();
    });
  });

  test('should open colors section', async () => {
    const { getByRole, findByText } = render(
      <ContextWrapper translations={translations}>
        <ThemeConfigurator
          isHighContrastOn={false}
          config={CONFIG_FIXTURE}
          onConfigChange={jest.fn()}
        />
      </ContextWrapper>
    );

    fireEvent.click(
      getByRole('button', { name: /THEME_CONFIGURATOR.NAVBAR.COLORS/ })
    );
    expect(
      await findByText('THEME_CONFIGURATOR.COLORS.HEAD')
    ).toBeInTheDocument();
  });

  test('should open typography section', async () => {
    const { getByRole, findByText } = render(
      <ContextWrapper translations={translations}>
        <ThemeConfigurator
          isHighContrastOn={false}
          config={CONFIG_FIXTURE}
          onConfigChange={jest.fn()}
        />
      </ContextWrapper>
    );

    fireEvent.click(
      getByRole('button', { name: /THEME_CONFIGURATOR.NAVBAR.TYPOGRAPHY/ })
    );
    expect(
      await findByText('THEME_CONFIGURATOR.TYPOGRAPHY.HEAD')
    ).toBeInTheDocument();
  });

  test('should open images section', async () => {
    const { getByRole, findByText } = render(
      <ContextWrapper translations={translations}>
        <ThemeConfigurator
          isHighContrastOn={false}
          config={CONFIG_FIXTURE}
          onConfigChange={jest.fn()}
        />
      </ContextWrapper>
    );

    fireEvent.click(
      getByRole('button', { name: /THEME_CONFIGURATOR.NAVBAR.IMAGES/ })
    );
    expect(
      await findByText('THEME_CONFIGURATOR.IMAGES.HEAD')
    ).toBeInTheDocument();
  });

  test('should open header section', async () => {
    const { getByRole, findByText } = render(
      <ContextWrapper translations={translations}>
        <ThemeConfigurator
          isHighContrastOn={false}
          config={CONFIG_FIXTURE}
          onConfigChange={jest.fn()}
        />
      </ContextWrapper>
    );

    fireEvent.click(
      getByRole('button', { name: /THEME_CONFIGURATOR.NAVBAR.HEADER/ })
    );
    expect(
      await findByText('THEME_CONFIGURATOR.HEADER.HEAD')
    ).toBeInTheDocument();
  });

  test('should call onConfigChange', async () => {
    const mockOnConfigChange = jest.fn();
    const { getByText, findByLabelText } = render(
      <ContextWrapper translations={translations}>
        <ThemeConfigurator
          isHighContrastOn={false}
          config={CONFIG_FIXTURE}
          onConfigChange={mockOnConfigChange}
        />
      </ContextWrapper>
    );
    fireEvent.click(getByText('THEME_CONFIGURATOR.NAVBAR.HEADER'));
    userEvent.type(
      await findByLabelText('THEME_CONFIGURATOR.HEADER.PAGE_TITLE'),
      'test'
    );

    expect(mockOnConfigChange).toHaveBeenCalled();
  });

  test('should reset config', () => {
    const mockOnConfigChange = jest.fn();
    const { getByText } = render(
      <ContextWrapper translations={translations}>
        <ThemeConfigurator
          isHighContrastOn={false}
          config={CONFIG_FIXTURE}
          onConfigChange={mockOnConfigChange}
        />
      </ContextWrapper>
    );

    fireEvent.click(getByText('THEME_CONFIGURATOR.NAVBAR.HEADER'));
    userEvent.type(document.querySelectorAll('.ant-input')[0], 'test');

    expect(mockOnConfigChange).toHaveBeenCalled();

    fireEvent.click(getByText('THEME_CONFIGURATOR.NAVBAR.REVERT'));

    expect(mockOnConfigChange).toHaveBeenCalledWith(CONFIG_FIXTURE);
  });
});
