/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';

import {
  render,
  fireEvent,
  screen,
  act,
  wait
} from '../../../common/custom-render';
import { CONFIG_FIXTURE } from '../../../fixtures/theme-configurator.fixture';
import { ContextWrapper, waitModalClosed } from '../../../common/utils';
import { HeaderConfigurator } from '../../../../app/modules/theme-configurator/header-configurator/header-configurator.component';

describe('HeaderConfigurator', () => {
  test('should render', () => {
    render(
      <ContextWrapper>
        <HeaderConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={jest.fn}
          onCustomLogoAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText('THEME_CONFIGURATOR.HEADER.HEAD')
    ).toBeInTheDocument();
    expect(
      screen.getByText('THEME_CONFIGURATOR.HEADER.TITLE_SEPARATOR')
    ).toBeInTheDocument();
    expect(
      screen.getByText('THEME_CONFIGURATOR.IMAGE_CONFIGURATOR.LOGO')
    ).toBeInTheDocument();
  });

  test('should change page title', () => {
    const mockOnConfigChange = jest.fn();
    render(
      <ContextWrapper>
        <HeaderConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={mockOnConfigChange}
          onCustomLogoAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.type(
      screen.getByLabelText('THEME_CONFIGURATOR.HEADER.PAGE_TITLE'),
      'test'
    );
    expect(mockOnConfigChange).toHaveBeenLastCalledWith({
      ...CONFIG_FIXTURE.theme.header,
      hideTitle: false,
      titleSeparator: undefined,
      title: 'test'
    });
  });

  test('should change title separator', () => {
    const mockOnConfigChange = jest.fn();
    render(
      <ContextWrapper>
        <HeaderConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={mockOnConfigChange}
          onCustomLogoAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.type(
      screen.getByLabelText('THEME_CONFIGURATOR.HEADER.TITLE_SEPARATOR'),
      'testSeparator'
    );

    expect(mockOnConfigChange).toHaveBeenLastCalledWith({
      ...CONFIG_FIXTURE.theme.header,
      hideTitle: true,
      titleSeparator: 'testSeparator'
    });
  });

  test('should change title font size', () => {
    const mockOnConfigChange = jest.fn();
    const changedFontSize = 15;
    render(
      <ContextWrapper>
        <HeaderConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={mockOnConfigChange}
          onCustomLogoAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.clear(
      screen.getByLabelText('THEME_CONFIGURATOR.HEADER.TITLE_FONT_SIZE')
    );
    userEvent.type(
      screen.getByLabelText('THEME_CONFIGURATOR.HEADER.TITLE_FONT_SIZE'),
      changedFontSize.toString()
    );

    expect(mockOnConfigChange).toHaveBeenCalledWith({
      ...CONFIG_FIXTURE.theme.header,
      titleStyles: {
        ...CONFIG_FIXTURE.theme.header.titleStyles,
        fontSize: changedFontSize
      }
    });
  });

  test('should change logo alt text', () => {
    const mockOnConfigChange = jest.fn();
    render(
      <ContextWrapper>
        <HeaderConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={mockOnConfigChange}
          onCustomLogoAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    fireEvent.change(
      screen.getByLabelText('THEME_CONFIGURATOR.HEADER.LOGO_ALT_TEXT'),
      { target: { value: 'test alt' } }
    );

    expect(mockOnConfigChange).toHaveBeenCalledWith({
      ...CONFIG_FIXTURE.theme.header,
      logoAltText: 'test alt'
    });
  });

  test('should open and close change logo modal', async () => {
    render(
      <ContextWrapper>
        <HeaderConfigurator
          currentTheme={CONFIG_FIXTURE.theme}
          onConfigChange={jest.fn()}
          onCustomLogoAdd={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.LOAD'));
      return wait();
    });

    expect(
      screen.getByText('THEME_CONFIGURATOR.IMAGES.IMAGE_UPLOAD_MODAL_TITLE')
    ).toBeInTheDocument();

    await act(() => {
      userEvent.click(screen.getByText('FINEOS_COMMON.UPLOAD.CANCEL'));
      return wait();
    });
  });
});
