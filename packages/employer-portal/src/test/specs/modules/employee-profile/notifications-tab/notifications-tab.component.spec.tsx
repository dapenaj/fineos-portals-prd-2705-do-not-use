/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen } from '@testing-library/react';
import { Tabs } from 'fineos-common';
import moment from 'moment';
import { wait } from '@testing-library/dom';

import * as notificationApiModule from '../../../../../app/modules/employee-profile/notifications-tab/notifications-tab.api';
import { NotificationsTab } from '../../../../../app/modules/employee-profile/notifications-tab';
import {
  ContextWrapper,
  selectAntOptionByText
} from '../../../../common/utils';

describe('NotificationsTab', () => {
  const notificationWithSubCaseType = (subCaseType: string) => ({
    data: [
      {
        caseNumber: 'NTN-22',
        notificationReason: {
          name: 'Pregnancy, birth or related medical treatment'
        },
        subCases: [{ caseType: subCaseType }],
        createdDate: moment('2020-01-30T07:53:15.852Z'),
        customer: { id: 'id-3', name: 'Jane Doe' }
      }
    ],
    total: 1
  });

  beforeEach(() => {
    jest.spyOn(notificationApiModule, 'fetchNotifications');
  });

  test('should show empty message if no notifications available', async () => {
    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({ data: [], total: 0 })
    );

    render(
      <ContextWrapper>
        <Tabs>
          <NotificationsTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(
      screen.getByText('PROFILE.NOTIFICATIONS_TAB.EMPTY_STATE')
    ).toBeInTheDocument();
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
  });

  test('should show notifications if available', async () => {
    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve(notificationWithSubCaseType(''))
    );

    render(
      <ContextWrapper>
        <Tabs>
          <NotificationsTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    expect(screen.queryByTestId('spinner')).toBeInTheDocument();

    await act(wait);

    expect(screen.getByText('30-01-2020')).toBeInTheDocument();

    expect(
      screen.getByText(
        'ENUM_DOMAINS.NOTIFICATION_REASON.PREGNANCY_BIRTH_OR_RELATED_MEDICAL_TREATMENT'
      )
    ).toBeInTheDocument();

    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
  });

  test('should render proper entitlement label for Group Disability Claim subcase', async () => {
    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve(notificationWithSubCaseType('Group Disability Claim'))
    );

    render(
      <ContextWrapper>
        <Tabs>
          <NotificationsTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.TYPE.WAGE_REPLACEMENT')
    ).toBeInTheDocument();

    await act(wait);
  });

  test('should render proper entitlement label for Absence Case subcase', async () => {
    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve(notificationWithSubCaseType('Absence Case'))
    );

    render(
      <ContextWrapper>
        <Tabs>
          <NotificationsTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.TYPE.JOB_PROTECTED_LEAVE')
    ).toBeInTheDocument();

    await act(wait);
  });

  test('should render proper entitlement label for Accommodation Case subcase', async () => {
    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve(notificationWithSubCaseType('Accommodation Case'))
    );

    render(
      <ContextWrapper>
        <Tabs>
          <NotificationsTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('NOTIFICATIONS.TYPE.WORKPLACE_ACCOMMODATION')
    ).toBeInTheDocument();

    await act(wait);
  });

  test('should sort notifications', async () => {
    const earlierDate = moment().subtract(3, 'days');
    const laterDate = moment().subtract(2, 'days');
    const mockNotifications = [
      {
        caseNumber: 'NTN-22',
        notificationReason: {
          name: 'Pregnancy, birth or related medical treatment'
        },
        subCases: [{ caseType: '' }],
        createdDate: earlierDate,
        customer: { id: 'id-3', name: 'Jane Doe' }
      },
      {
        caseNumber: 'NTN-22',
        notificationReason: {
          name: 'Pregnancy, birth or related medical treatment'
        },
        subCases: [{ caseType: '' }],
        createdDate: laterDate,
        customer: { id: 'id-3', name: 'Jane Doe' }
      }
    ];

    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({ data: mockNotifications, total: 0 })
    );

    render(
      <ContextWrapper>
        <Tabs>
          <NotificationsTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByLabelText(
        'PROFILE.NOTIFICATIONS_TAB.NOTIFICATIONS_SORTING_LABEL'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText('COMMON.SORT_BY_DATE_SELECT.DESC')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('COMMON.SORT_BY_DATE_SELECT.ASC')
    ).not.toBeInTheDocument();
    expect(screen.getAllByTestId('notified-on-date')[0]).toHaveTextContent(
      laterDate.format('DD-MM-YYYY')
    );
    expect(screen.getAllByTestId('notified-on-date')[1]).toHaveTextContent(
      earlierDate.format('DD-MM-YYYY')
    );

    await selectAntOptionByText(
      screen.getByTestId('select'),
      'COMMON.SORT_BY_DATE_SELECT.ASC'
    );

    expect(screen.getAllByTestId('notified-on-date')[0]).toHaveTextContent(
      earlierDate.format('DD-MM-YYYY')
    );
    expect(screen.getAllByTestId('notified-on-date')[1]).toHaveTextContent(
      laterDate.format('DD-MM-YYYY')
    );

    await act(wait);
  });
});
