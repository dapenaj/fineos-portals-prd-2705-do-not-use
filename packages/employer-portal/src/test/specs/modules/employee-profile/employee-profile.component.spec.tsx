/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, wait, screen } from '@testing-library/react';
import {
  ViewNotificationPermissions,
  ViewCustomerDataPermissions,
  ViewOccupationPermissions,
  CreateNotificationPermissions
} from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { ContextWrapper } from '../../../common/utils';
import { EmployeeProfile } from '../../../../app/modules/employee-profile';
import * as employeeApiModule from '../../../../app/modules/employee-profile/employee-profile.api';
import * as notificationApiModule from '../../../../app/modules/employee-profile/notifications-tab/notifications-tab.api';
import { PortalEnvConfigService } from '../../../../app/shared';
import portalEnvConfig from '../../../../../public/config/portal.env.json';

describe('EmployeeProfile', () => {
  beforeEach(() => {
    jest.spyOn(employeeApiModule, 'fetchCustomer');
    jest
      .spyOn(employeeApiModule, 'fetchMostRecentCustomerOccupation')
      .mockReturnValue(new Promise(jest.fn()));
    jest
      .spyOn(notificationApiModule, 'fetchNotifications')
      .mockReturnValue(new Promise(jest.fn()));
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValueOnce(Promise.resolve(portalEnvConfig));
  });

  test('should load employee details', async () => {
    (employeeApiModule.fetchCustomer as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        id: '1',
        firstName: 'Jane',
        lastName: 'Doe'
      })
    );

    render(
      <ContextWrapper>
        <EmployeeProfile employeeId={'1'} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('Jane Doe')).toBeInTheDocument();
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
  });

  test('should render notification tab if has access to ViewNotificationPermissions.VIEW_NOTIFICATIONS', async () => {
    (employeeApiModule.fetchCustomer as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        id: '1',
        firstName: 'Jane',
        lastName: 'Doe'
      })
    );

    (notificationApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 0,
        data: []
      })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER,
          ViewNotificationPermissions.VIEW_NOTIFICATIONS
        ]}
      >
        <EmployeeProfile employeeId={'1'} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByRole('tablist')).toBeInTheDocument();
    expect(
      screen.getByText('PROFILE.NOTIFICATIONS_TAB.EMPTY_STATE')
    ).toBeInTheDocument();
  });

  test('should render employee details tab if has access to ViewCustomerDataPermissions.VIEW_CUSTOMER', async () => {
    (employeeApiModule.fetchCustomer as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        id: '1',
        firstName: 'Jane',
        lastName: 'Doe'
      })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_NOTIFICATIONS,
          ViewCustomerDataPermissions.VIEW_CUSTOMER,
          ViewCustomerDataPermissions.VIEW_CUSTOMERS_LIST,
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
        ]}
      >
        <EmployeeProfile employeeId={'1'} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByRole('tablist')).toBeInTheDocument();
    expect(
      screen.getByText('PROFILE.EMPLOYEE_DETAILS_TAB.TITLE')
    ).toBeInTheDocument();
  });

  test('should render employee profile actions with the required permissions', async () => {
    (employeeApiModule.fetchCustomer as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        id: '1',
        firstName: 'Jane',
        lastName: 'Doe'
      })
    );

    render(
      <ContextWrapper
        permissions={[
          CreateNotificationPermissions.ADD_NOTIFICATION,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO
        ]}
      >
        <EmployeeProfile employeeId={'1'} />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('PROFILE.ACTIONS')).toBeInTheDocument();

    await act(async () => {
      userEvent.click(screen.getByText('PROFILE.ACTIONS'));
      return wait();
    });

    expect(
      screen.getByText('HEADER.SEARCH.ESTABLISHED_INTAKE_LINK')
    ).toBeInTheDocument();
  });

  xtest('should render time taken tab if has access to ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS and ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY', async () => {
    (employeeApiModule.fetchCustomer as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        id: '1',
        firstName: 'Jane',
        lastName: 'Doe'
      })
    );

    const { queryByRole, queryByTestId } = render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <EmployeeProfile employeeId={'1'} />
      </ContextWrapper>
    );

    await act(wait);

    expect(queryByRole('tablist')).toBeInTheDocument();
    expect(queryByTestId('time-taken-tab')).toBeInTheDocument();
  });
});
