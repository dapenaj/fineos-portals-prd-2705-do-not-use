import React from 'react';
import userEvent from '@testing-library/user-event';

import { render, act, wait, screen } from '../../../../../common/custom-render';
import { ContextWrapper, releaseEventLoop } from '../../../../../common/utils';
import { AddEmailModal } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/add-email-modal/add-email-modal.component';

describe('AddEmailModal', () => {
  test('should render', async () => {
    render(
      <ContextWrapper>
        <AddEmailModal
          isVisible={true}
          onModalClose={jest.fn()}
          onEmailPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.TITLE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.ADD_EMAIL_LABEL'
      )
    ).toBeInTheDocument();
  });

  test('should validate', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <AddEmailModal
          isVisible={true}
          onModalClose={jest.fn()}
          onEmailPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(screen.getByRole('textbox')).not.toBeValid();
    userEvent.type(screen.getByRole('textbox'), 'test@test.pl');

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(screen.getByRole('textbox')).toBeValid();
    userEvent.type(screen.getByRole('textbox'), 'test@test');

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(screen.getByRole('textbox')).not.toBeValid();

    userEvent.type(
      screen.getByRole('textbox'),
      'Loremoipsumidoloresituamet.gconsectetueriadipiscinguelitoiAeneanhjommodohigulamegetgdolorreAeneanbmassawaCumusTestTest@gmail.com'
    );

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(screen.getByRole('textbox')).not.toBeValid();
  });

  test('should call onModalClose', async () => {
    const mockOnClose = jest.fn();
    const { rerender } = render(
      <ContextWrapper>
        <AddEmailModal
          isVisible={true}
          onModalClose={mockOnClose}
          onEmailPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByText('FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL')
      );
      return releaseEventLoop();
    });

    expect(mockOnClose).toHaveBeenCalledTimes(1);

    rerender(
      <ContextWrapper>
        <AddEmailModal
          isVisible={true}
          onModalClose={mockOnClose}
          onEmailPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByText('FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL')
      );
      return releaseEventLoop();
    });

    expect(mockOnClose).toHaveBeenCalledTimes(2);
  });

  test('should call onEmailPass', async () => {
    const mockOnEmailPass = jest.fn();
    const mockOnModalClose = jest.fn();
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <AddEmailModal
          isVisible={true}
          onModalClose={mockOnModalClose}
          onEmailPass={mockOnEmailPass}
        />
      </ContextWrapper>
    );

    userEvent.type(screen.getByRole('textbox'), 'test@test.com');
    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(mockOnEmailPass).toHaveBeenCalledWith({
      emailAddress: 'test@test.com'
    });
    expect(mockOnModalClose).toHaveBeenCalledTimes(1);
  });
});
