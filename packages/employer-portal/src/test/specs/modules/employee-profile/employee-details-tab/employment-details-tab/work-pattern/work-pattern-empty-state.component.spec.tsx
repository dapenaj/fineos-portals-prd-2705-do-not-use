/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen, within } from '@testing-library/react';
import { ManageOccupationPermissions } from 'fineos-common';

import { ContextWrapper } from '../../../../../../common/utils';
import { UnsetWorkPattern } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/work-pattern/unset-work-pattern.component';

describe('WorkPatternEmptyState', () => {
  test('should render the empty state for work pattern', async () => {
    render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.ADD_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
        ]}
      >
        <UnsetWorkPattern onSetMode={jest.fn()} customerId="1" />
      </ContextWrapper>
    );

    expect(await screen.findByText('WORK_PATTERN.EMPTY')).toBeInTheDocument();

    expect(
      within(screen.getByRole('button')).getByText(
        'WORK_PATTERN.ADD_WORK_PATTERN'
      )
    ).toBeInTheDocument();
  });
});
