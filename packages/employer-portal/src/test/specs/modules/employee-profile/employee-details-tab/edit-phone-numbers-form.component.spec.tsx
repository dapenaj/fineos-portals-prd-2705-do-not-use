/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';
import { ManageCustomerDataPermissions } from 'fineos-common';
import {
  ConcurrencyUpdateError,
  GroupClientCustomerService
} from 'fineos-js-api-client';

import {
  render,
  act,
  within,
  wait,
  screen
} from '../../../../common/custom-render';
import {
  PHONE_NUMBERS_FIXTURE,
  CUSTOMER_SUMMARY_FIXTURE
} from '../../../../fixtures/customer.fixture';
import { ContextWrapper } from '../../../../common/utils';
// tslint:disable-next-line: max-line-length
import { EditPhoneNumbersForm } from '../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/contact-details';

describe('EditPhoneNumbersForm', () => {
  const customer = CUSTOMER_SUMMARY_FIXTURE;
  const phoneNumbers = PHONE_NUMBERS_FIXTURE;

  test('render', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditPhoneNumbersForm
          customerId={customer[0].id}
          phoneNumbers={phoneNumbers}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK')
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
    expect(screen.getAllByTestId('dropdown-input').length).toBe(1);
    expect(screen.getAllByTestId('text-input').length).toBe(3);
  });

  test('should cancel edit emails form', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();

    render(
      <ContextWrapper>
        <EditPhoneNumbersForm
          customerId={customer[0].id}
          phoneNumbers={phoneNumbers}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

    await act(wait);

    expect(mockCloseForm).toHaveBeenCalledTimes(1);
  });

  test('should add and delete phone numbers', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    render(
      <ContextWrapper
        permissions={[
          ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS,
          ManageCustomerDataPermissions.REMOVE_CUSTOMER_PHONE_NUMBERS
        ]}
      >
        <EditPhoneNumbersForm
          customerId={customer[0].id}
          phoneNumbers={phoneNumbers}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );
    expect(screen.getAllByTestId('dropdown-input').length).toBe(1);
    expect(screen.getAllByTestId('text-input').length).toBe(3);

    userEvent.click(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_PHONE')
    );
    userEvent.type(
      within(screen.getAllByTestId('phone-area-code-field')[1]).getByTestId(
        'text-input'
      ),
      '123'
    );
    userEvent.type(
      within(screen.getAllByTestId('phone-phone-no-field')[1]).getByTestId(
        'text-input'
      ),
      '45678'
    );

    expect(screen.getAllByTestId('delete-phone-number').length).toBe(2);
    expect(screen.getAllByTestId('dropdown-input').length).toBe(2);
    expect(screen.getAllByTestId('text-input').length).toBe(6);

    await act(() => {
      userEvent.click(screen.getAllByTestId('delete-phone-number')[0]);
      return wait();
    });

    expect(screen.getAllByTestId('dropdown-input').length).toBe(1);
    expect(screen.getAllByTestId('text-input').length).toBe(3);
  });

  test('should edit phone numbers', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    render(
      <ContextWrapper
        permissions={[
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
        ]}
      >
        <EditPhoneNumbersForm
          customerId={customer[0].id}
          phoneNumbers={phoneNumbers}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    expect(
      within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
        'text-input'
      )
    ).toHaveValue('1');

    await act(() => {
      userEvent.type(
        within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
          'text-input'
        ),
        '23'
      );
      return wait();
    });

    expect(
      within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
        'text-input'
      )
    ).toHaveValue('123');
  });

  describe('Concurrency', () => {
    const service = GroupClientCustomerService.getInstance();
    const refreshedAreaCode = '555';

    test('should display concurrency modal when concurrency error returned', async () => {
      jest.spyOn(service, 'editCustomerPhoneNumber').mockRejectedValue(
        new ConcurrencyUpdateError({
          ...phoneNumbers[0],
          areaCode: refreshedAreaCode
        })
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
          ]}
        >
          <EditPhoneNumbersForm
            customerId={customer[0].id}
            phoneNumbers={phoneNumbers}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      userEvent.type(
        within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
          'text-input'
        ),
        '1111'
      );

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).toBeInTheDocument();
    });

    test('should not display error modal when no error returned', async () => {
      jest
        .spyOn(service, 'editCustomerPhoneNumber')
        .mockReturnValueOnce(Promise.resolve(phoneNumbers[0]));

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
          ]}
        >
          <EditPhoneNumbersForm
            customerId={customer[0].id}
            phoneNumbers={phoneNumbers}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      userEvent.type(
        within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
          'text-input'
        ),
        '1111'
      );

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).not.toBeInTheDocument();
    });

    test('should display api response error modal when other error returned', async () => {
      jest
        .spyOn(service, 'editCustomerPhoneNumber')
        .mockRejectedValueOnce({ status: '404' });

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
          ]}
        >
          <EditPhoneNumbersForm
            customerId={customer[0].id}
            phoneNumbers={phoneNumbers}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      userEvent.type(
        within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
          'text-input'
        ),
        '1111'
      );
      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();
      expect(
        screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).toBeInTheDocument();
    });

    test('should refresh data when concurrency occurred', async () => {
      jest.spyOn(service, 'editCustomerPhoneNumber').mockRejectedValueOnce(
        new ConcurrencyUpdateError({
          elements: [
            phoneNumbers[0],
            {
              ...phoneNumbers[1],
              areaCode: refreshedAreaCode
            }
          ]
        })
      );
      jest.spyOn(service, 'fetchCustomerPhoneNumbers').mockReturnValueOnce(
        Promise.resolve({
          elements: [
            {
              ...phoneNumbers[0],
              areaCode: '888'
            }
          ]
        } as any)
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
          ]}
        >
          <EditPhoneNumbersForm
            customerId={customer[0].id}
            phoneNumbers={phoneNumbers}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      await act(wait);

      userEvent.type(
        within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
          'text-input'
        ),
        '1111'
      );

      expect(
        screen.queryByDisplayValue(refreshedAreaCode)
      ).not.toBeInTheDocument();

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      await act(() => {
        userEvent.click(
          screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
        );
        return wait();
      });

      expect(screen.getByDisplayValue(refreshedAreaCode)).toBeInTheDocument();
    });

    test('should call onShouldRefreshChange with truthy param when cancel after concurrency', async () => {
      const mockOnShouldRefreshChange = jest.fn();
      jest.spyOn(service, 'editCustomerPhoneNumber').mockRejectedValue(
        new ConcurrencyUpdateError({
          elements: [phoneNumbers[0], { areaCode: refreshedAreaCode }]
        })
      );
      jest.spyOn(service, 'fetchCustomerPhoneNumbers').mockReturnValueOnce(
        Promise.resolve({
          elements: [
            {
              ...phoneNumbers[0],
              areaCode: '888'
            }
          ]
        } as any)
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS
          ]}
        >
          <EditPhoneNumbersForm
            customerId={customer[0].id}
            phoneNumbers={phoneNumbers}
            onShouldRefreshChange={mockOnShouldRefreshChange}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      userEvent.type(
        within(screen.getAllByTestId('phone-area-code-field')[0]).getByTestId(
          'text-input'
        ),
        '1111'
      );

      expect(
        screen.queryByDisplayValue(refreshedAreaCode)
      ).not.toBeInTheDocument();

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      await act(() => {
        userEvent.click(
          screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
        );
        return wait();
      });

      await act(() => {
        userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));
        return wait();
      });

      expect(mockOnShouldRefreshChange).toHaveBeenCalledWith(true, [
        { areaCode: '1', id: '1', intCode: '353', telephoneNo: '234567' },
        { areaCode: '555' }
      ]);
    });
  });
});
