import React from 'react';
import userEvent from '@testing-library/user-event';
import { Formik } from 'formik';
import { ManageCustomerDataPermissions } from 'fineos-common';

import { render, wait, screen } from '../../../../../common/custom-render';
import { ContextWrapper } from '../../../../../common/utils';
import { EMPTY_CONTEXT_VALUES_FIXTURE } from '../../../../../fixtures/communication-preferences.fixture';
import { EMAILS_FIXTURE } from '../../../../../fixtures/contact-details.fixture';
import { CommunicationPreferencesContext } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.context';
import { SelectEmail } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/select-email/select-email.component';

describe('SelectEmail', () => {
  const mockContextValue = {
    emailAddresses: EMAILS_FIXTURE
  };
  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {
      selectedPhoneNumberIndex: 0
    }
  };
  test('should render', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <Formik {...mockFormikProps}>
            <SelectEmail onEmailPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );
    await wait();

    expect(
      screen.getByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.LABEL'
      )
    ).toBeInTheDocument();
  });

  test('should render emails', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectEmail onEmailPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText(EMAILS_FIXTURE[0].emailAddress)
    ).toBeInTheDocument();
    expect(
      screen.getByText(EMAILS_FIXTURE[1].emailAddress)
    ).toBeInTheDocument();
  });

  test('should display buttion with permissions', async () => {
    const { rerender } = render(
      <ContextWrapper
        permissions={[ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS]}
      >
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectEmail onEmailPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByRole('button', {
        name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON/
      })
    ).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectEmail onEmailPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    expect(
      screen.queryByRole('button', {
        name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON/
      })
    ).not.toBeInTheDocument();
  });

  test('should open add-email-modal', async () => {
    render(
      <ContextWrapper
        permissions={[ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS]}
      >
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectEmail onEmailPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await wait();

    userEvent.click(
      screen.getByRole('button', {
        name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.DIFFERENT_EMAIL_BUTTON/
      })
    );

    await wait();

    expect(screen.getByTestId('add-email-modal')).toBeInTheDocument();
  });
});
