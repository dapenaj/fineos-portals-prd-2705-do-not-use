import React from 'react';
import { render, screen } from '@testing-library/react';
import each from 'jest-each';
import { CustomerAbsenceEmployment } from 'fineos-js-api-client';
import { ManageOccupationPermissions } from 'fineos-common';

import {
  buildTranslationWithParams,
  ContextWrapper,
  mockTranslations,
  resolveTranslationWithParams
} from '../../../../../../common/utils';
import { EligibilityCriteria } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/eligibility-criteria/eligibility-criteria.component';
import { ABSENCE_EMPLOYMENT_FIXTURE } from '../../../../../../fixtures/absence-employment.fixture';

describe('EligibilityCriteria', () => {
  test('should render elements with full data', () => {
    render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT,
          ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT
        ]}
      >
        <EligibilityCriteria
          employeeId="test"
          absenceEmployment={ABSENCE_EMPLOYMENT_FIXTURE[0]}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.TITLE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.YOUR_EMPLOYEE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EDIT_BUTTON'
      )
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EMPTY_STATE'
      )
    ).not.toBeInTheDocument();
  });

  test('should render empty state if no displayable data', () => {
    const mockAbsenceEmployment = {
      ...ABSENCE_EMPLOYMENT_FIXTURE[0],
      withinFMLACriteria: null,
      workingAtHome: null,
      keyEmployee: null,
      hoursWorkedPerYear: null,
      employmentWorkState: {
        name: 'Unknown'
      },
      occupationQualifiers: []
    } as any;
    render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT,
          ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT
        ]}
      >
        <EligibilityCriteria
          employeeId="test"
          absenceEmployment={mockAbsenceEmployment}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.TITLE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.EMPTY_STATE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    ).toBeInTheDocument();
  });

  test('should not display button if has no ADD and EDIT permission', () => {
    const mockAbsenceEmployment = {
      ...ABSENCE_EMPLOYMENT_FIXTURE[0],
      withinFMLACriteria: null,
      workingAtHome: null,
      keyEmployee: null,
      hoursWorkedPerYear: null,
      employmentWorkState: {
        name: 'Unknown'
      },
      occupationQualifiers: []
    } as any;
    const { rerender } = render(
      <ContextWrapper
        permissions={[ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT]}
      >
        <EligibilityCriteria
          employeeId="test"
          absenceEmployment={mockAbsenceEmployment}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    ).not.toBeInTheDocument();

    rerender(
      <ContextWrapper
        permissions={[ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT]}
      >
        <EligibilityCriteria
          employeeId="test"
          absenceEmployment={mockAbsenceEmployment}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    ).not.toBeInTheDocument();
  });

  test('should display spinner when loading data', () => {
    render(
      <ContextWrapper>
        <EligibilityCriteria
          employeeId="test"
          absenceEmployment={{} as CustomerAbsenceEmployment}
          isAbsenceEmploymentPending={true}
        />
      </ContextWrapper>
    );

    expect(screen.getByTestId('spinner')).toBeInTheDocument();
  });

  describe('information-with-icons section', () => {
    each([
      [
        true,
        false,
        false,
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_WITH_FMLA_TRUE'
      ],
      [
        false,
        true,
        false,
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_AT_HOME_TRUE'
      ],
      [
        false,
        false,
        true,
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.IS_KEY_EMPLOYEE_TRUE'
      ]
    ]).test(
      'should display correct content when withinFMLACriteria: %s, keyEmployee: %s, workingAtHome: %s',
      (withinFMLACriteria, workingAtHome, keyEmployee, visibleContent) => {
        const mockAbsenceEmployment = {
          ...ABSENCE_EMPLOYMENT_FIXTURE[0],
          withinFMLACriteria,
          keyEmployee,
          workingAtHome
        };

        render(
          <ContextWrapper>
            <EligibilityCriteria
              employeeId="test"
              absenceEmployment={
                mockAbsenceEmployment as CustomerAbsenceEmployment
              }
              isAbsenceEmploymentPending={false}
            />
          </ContextWrapper>
        );

        expect(screen.getByText(visibleContent)).toBeInTheDocument();
      }
    );

    test('should render with correct value when all info is false', () => {
      const mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        withinFMLACriteria: false,
        keyEmployee: false,
        workingAtHome: false
      };

      render(
        <ContextWrapper>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={
              mockAbsenceEmployment as CustomerAbsenceEmployment
            }
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_WITH_FMLA_FALSE'
        )
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORKS_AT_HOME_FALSE'
        )
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.IS_KEY_EMPLOYEE_FALSE'
        )
      ).toBeInTheDocument();
      expect(screen.getAllByRole('img')).toHaveLength(3);
    });
  });

  describe('text-informations-section', () => {
    const translations = mockTranslations(
      buildTranslationWithParams(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.HOURS_PER_YEAR',
        ['hoursPerYear']
      ),
      buildTranslationWithParams(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORK_STATE',
        ['workState']
      ),
      buildTranslationWithParams(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.CBA_NUMBER',
        ['CBANumber']
      ),
      buildTranslationWithParams(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.OCCUPATION_QUALIFIERS',
        ['occupationQualifiers']
      )
    );

    test('should correctly display hours worked per year', () => {
      let mockAbsenceEmployment = ABSENCE_EMPLOYMENT_FIXTURE[0];
      const { rerender } = render(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          resolveTranslationWithParams(
            'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.HOURS_PER_YEAR',
            [['hoursPerYear', mockAbsenceEmployment.hoursWorkedPerYear]]
          )
        )
      );

      mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        hoursWorkedPerYear: 0
      };

      rerender(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          resolveTranslationWithParams(
            'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.HOURS_PER_YEAR',
            [['hoursPerYear', mockAbsenceEmployment.hoursWorkedPerYear]]
          )
        )
      );

      mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        hoursWorkedPerYear: null
      } as any;

      rerender(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.HOURS_PER_YEAR',
          { exact: false }
        )
      ).not.toBeInTheDocument();
    });

    test('should correctly display work state', () => {
      let mockAbsenceEmployment = ABSENCE_EMPLOYMENT_FIXTURE[0];

      const { rerender } = render(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(screen.getByText('ENUM_DOMAINS.US_STATES.NY')).toBeInTheDocument();
      expect(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORK_STATE',
          { exact: false }
        )
      ).toBeInTheDocument();

      mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        employmentWorkState: {
          name: 'Unknown'
        } as any
      };

      rerender(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(screen.getByText('-')).toBeInTheDocument();
      expect(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORK_STATE',
          { exact: false }
        )
      ).toBeInTheDocument();

      mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        employmentWorkState: null
      } as any;

      rerender(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.WORK_STATE',
          { exact: false }
        )
      ).not.toBeInTheDocument();
    });

    test('should correctly display CBA', () => {
      let mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        cbaValue: '123'
      };

      const { rerender } = render(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          resolveTranslationWithParams(
            'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.CBA_NUMBER',
            [['CBANumber', mockAbsenceEmployment.cbaValue]]
          )
        )
      );

      mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        cbaValue: null
      } as any;

      rerender(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.CBA_NUMBER',
          { exact: false }
        )
      ).not.toBeInTheDocument();
    });

    test('should correctly display occupation qualifiers', () => {
      let mockAbsenceEmployment = ABSENCE_EMPLOYMENT_FIXTURE[0];

      const { rerender } = render(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          resolveTranslationWithParams(
            'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.OCCUPATION_QUALIFIERS',
            [['occupationQualifiers', 'test1, test2']]
          )
        )
      );

      mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        occupationQualifiers: []
      };
      rerender(
        <ContextWrapper translations={translations}>
          <EligibilityCriteria
            employeeId="test"
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.queryByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.OCCUPATION_QUALIFIERS',
          { exact: false }
        )
      ).not.toBeInTheDocument();
    });
  });
});
