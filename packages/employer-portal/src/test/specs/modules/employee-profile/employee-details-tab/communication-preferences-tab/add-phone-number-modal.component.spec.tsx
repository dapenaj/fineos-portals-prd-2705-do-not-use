import React from 'react';
import { render, act } from '@testing-library/react';
import { wait } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { ContextWrapper, releaseEventLoop } from '../../../../../common/utils';
import { AddPhoneNumberModal } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/add-phone-number-modal/add-phone-number-modal.component';
import { screen } from '../../../../../common/custom-render';

describe('AddPhoneNumberModal', () => {
  test('should render', async () => {
    render(
      <ContextWrapper>
        <AddPhoneNumberModal
          isVisible={true}
          onModalClose={jest.fn()}
          onPhoneNumberPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.ADD_PHONE_NUMBER'
      )
    ).toBeInTheDocument();
    expect(screen.getByText('COMMON.PHONE.COUNTRY_CODE')).toBeInTheDocument();
    expect(screen.getByText('COMMON.PHONE.AREA_CODE')).toBeInTheDocument();
    expect(screen.getByText('COMMON.PHONE.PHONE_NO')).toBeInTheDocument();
  });

  test('should call onModalClose', async () => {
    const mockOnClose = jest.fn();
    const { rerender } = render(
      <ContextWrapper>
        <AddPhoneNumberModal
          isVisible={true}
          onModalClose={mockOnClose}
          onPhoneNumberPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.CANCEL/ })
      );
      return releaseEventLoop();
    });

    expect(mockOnClose).toHaveBeenCalledTimes(1);

    await wait();

    rerender(
      <ContextWrapper>
        <AddPhoneNumberModal
          isVisible={true}
          onModalClose={mockOnClose}
          onPhoneNumberPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(
        screen.getByText('FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL')
      );
      return releaseEventLoop();
    });

    expect(mockOnClose).toHaveBeenCalledTimes(2);
  });

  test('should validate', async () => {
    render(
      <ContextWrapper>
        <AddPhoneNumberModal
          isVisible={true}
          onModalClose={jest.fn()}
          onPhoneNumberPass={jest.fn()}
        />
      </ContextWrapper>
    );

    await wait();

    userEvent.type(screen.getAllByRole('textbox')[0], '111');
    userEvent.type(screen.getAllByRole('textbox')[1], '111');

    await act(() => {
      userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.OK'));
      return releaseEventLoop();
    });

    expect(screen.getAllByRole('textbox')[2]).not.toBeValid();
  });

  test('should call onPhoneNumberPass', async () => {
    const mockOnEmailPass = jest.fn();
    const mockOnModalClose = jest.fn();
    const expectedOutput = {
      areaCode: '222',
      contactMethod: {
        domainId: 51,
        fullId: 1660001,
        id: '1660001',
        name: 'Cell'
      },
      exDirectory: true,
      extension: '',
      intCode: '111',
      telephoneNo: '333'
    };

    await wait();

    render(
      <ContextWrapper>
        <AddPhoneNumberModal
          isVisible={true}
          onModalClose={mockOnModalClose}
          onPhoneNumberPass={mockOnEmailPass}
        />
      </ContextWrapper>
    );

    userEvent.type(screen.getAllByRole('textbox')[0], '111');
    userEvent.type(screen.getAllByRole('textbox')[1], '222');
    userEvent.type(screen.getAllByRole('textbox')[2], '333');
    await act(() => {
      userEvent.click(
        screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.OK/ })
      );
      return releaseEventLoop();
    });

    expect(mockOnEmailPass).toHaveBeenCalledWith(expectedOutput);
    expect(mockOnModalClose).toHaveBeenCalledTimes(1);
  });
});
