/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { BaseDomain, EnumInstance } from 'fineos-js-api-client';
import moment from 'moment';

import { scaledAmount, unscaledAmount } from '../../../../../../../app/shared';
import {
  parsedOccupation,
  parsedContractualEarnings,
  parsedAbsenceEmployment
} from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/current-occupation/edit-employment-details';
import { EmploymentDetailsForm } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/current-occupation/edit-employment-details/edit-employment-details-form.type';
import { CONTRACTUAL_EARNINGS_FIXTURE } from '../../../../../../fixtures/contractual-earnings.fixture';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../../../fixtures/customer-occupations.fixture';
import { ABSENCE_EMPLOYMENT_FIXTURE } from '../../../../../../fixtures/absence-employment.fixture';

describe('EditEmploymentDetailsUtils', () => {
  describe('parsedOccupation', () => {
    test('should correctly transform occupation along with passed formData', () => {
      const mockUpdatedOccupation = {
        jobTitle: 'test',
        jobStartDate: moment(new Date('2018-05-20')),
        jobEndDate: moment(new Date('2020-05-20')),
        hrsWorkedPerWeek: '40',
        occupationCategory: { name: 'testOccupation' } as EnumInstance,
        earnings: {}
      };
      const result = parsedOccupation(
        mockUpdatedOccupation as any,
        CUSTOMER_OCCUPATIONS_FIXTURE[0]
      );
      expect(result.jobTitle).toBe('test');
      expect(result.jobStartDate).toEqual(moment(new Date('2018-05-20')));
      expect(result.jobEndDate).toEqual(moment(new Date('2020-05-20')));
      expect(result.hrsWorkedPerWeek).toBe('40');
      expect(result.employmentCat!.name).toBe('testOccupation');
    });
  });

  describe('parsedContractualEarnings', () => {
    const mockEarningTypeDomains = [{ name: 'Wages' }] as BaseDomain[];
    const mockEarningsType = {
      domainId: 223,
      fullId: 7136001,
      name: 'Pre-Disability',
      domainName: 'EarningsType'
    };
    const mockFrequency = {
      name: 'Yearly',
      domainId: 235,
      fullId: 7520005,
      domainName: 'EarningsBasis'
    };
    const mockFormData = {
      earnings: {
        amount: 7000,
        frequency: mockFrequency
      }
    };

    test('should return correct value when editing contractual earnings', () => {
      const result = parsedContractualEarnings(
        mockFormData as EmploymentDetailsForm,
        CONTRACTUAL_EARNINGS_FIXTURE[0],
        mockEarningTypeDomains,
        '.'
      );

      expect(result).toStrictEqual({
        id: CONTRACTUAL_EARNINGS_FIXTURE[0].id,
        amount: {
          amountMinorUnits: 700000,
          currency: '---',
          scale: 2
        },
        earningsType: mockEarningsType,
        effectiveDate: CONTRACTUAL_EARNINGS_FIXTURE[0].effectiveDate,
        endDate: CONTRACTUAL_EARNINGS_FIXTURE[0].endDate,
        frequency: mockFrequency
      });
    });
  });

  describe('parsedAbsenceEmployment', () => {
    test('should return correct value', () => {
      const mockAbsenceEmployment = ABSENCE_EMPLOYMENT_FIXTURE[0];
      const mockFormData = {
        isAdjustedDateOfHireVisible: true,
        adjustedHireDate: '2020-10-10',
        employmentType: {
          fullId: 216704000,
          name: 'Permanent'
        }
      } as any;
      const result = parsedAbsenceEmployment(
        mockFormData,
        mockAbsenceEmployment
      );

      expect(result).toEqual({
        id: mockAbsenceEmployment.id,
        adjustedHireDate: mockFormData.adjustedHireDate,
        employmentType: mockFormData.employmentType,
        cbaValue: mockAbsenceEmployment.cbaValue,
        hoursWorkedPerYear: mockAbsenceEmployment.hoursWorkedPerYear,
        keyEmployee: mockAbsenceEmployment.keyEmployee,
        withinFMLACriteria: mockAbsenceEmployment.withinFMLACriteria,
        workingAtHome: mockAbsenceEmployment.workingAtHome,
        employmentWorkState: mockAbsenceEmployment.employmentWorkState,
        employmentClassification: mockAbsenceEmployment.employmentClassification
      });
    });
  });

  describe('scale functions', () => {
    const decimalValue = 700.1;
    const intValue = 70010;

    test('scaledAmount should return value', () => {
      expect(scaledAmount(decimalValue, 2, '.')).toBe(intValue);
    });

    test('unscaledAmount should return value', () => {
      expect(unscaledAmount(intValue, 2)).toBe(decimalValue);
    });
  });
});
