/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';
import { ManageCustomerDataPermissions } from 'fineos-common';
import {
  ConcurrencyUpdateError,
  GroupClientCustomerService
} from 'fineos-js-api-client';

import { render, wait, act, screen } from '../../../../common/custom-render';
import {
  CUSTOMER_SUMMARY_FIXTURE,
  EMAILS_FIXTURE
} from '../../../../fixtures/customer.fixture';
import { ContextWrapper } from '../../../../common/utils';
import { EditEmailsForm } from '../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/contact-details';

describe('EditEmploymentDetails', () => {
  const customer = CUSTOMER_SUMMARY_FIXTURE;
  const emails = EMAILS_FIXTURE;

  test('render', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditEmailsForm
          customerId={customer[0].id}
          emails={emails}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK')
    ).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
    expect(screen.getAllByTestId('text-input').length).toBe(2);
  });

  test('should cancel edit emails form', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    render(
      <ContextWrapper>
        <EditEmailsForm
          customerId={customer[0].id}
          emails={emails}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

    await act(wait);

    expect(mockCloseForm).toHaveBeenCalledTimes(1);
  });

  test('should add and delete emails', async () => {
    const testEmail = 'test@gmail.com';
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    const { getSubmitButtonByText } = render(
      <ContextWrapper
        permissions={[
          ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS,
          ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS,
          ManageCustomerDataPermissions.REMOVE_CUSTOMER_EMAILS
        ]}
      >
        <EditEmailsForm
          customerId={customer[0].id}
          emails={emails}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    expect(screen.getAllByTestId('delete-email').length).toBe(2);
    expect(screen.getAllByTestId('text-input').length).toBe(2);

    userEvent.click(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_EMAIL')
    );
    userEvent.type(screen.getAllByTestId('text-input')[2], testEmail);

    expect(screen.getAllByTestId('text-input').length).toBe(3);

    userEvent.click(screen.getAllByTestId('delete-email')[0]);

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    expect(
      screen.queryByDisplayValue('obiwankenobi@tatooine.com')
    ).not.toBeInTheDocument();
    expect(screen.getByDisplayValue(testEmail)).toBeInTheDocument();
  });

  test('should edit emails', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    const { getSubmitButtonByText } = render(
      <ContextWrapper
        permissions={[ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS]}
      >
        <EditEmailsForm
          customerId={customer[0].id}
          emails={emails}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    expect(screen.getAllByTestId('text-input')[0]).toHaveValue(
      'obiwankenobi@tatooine.com'
    );

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    expect(mockOnEditFinish).toHaveBeenCalledTimes(1);
  });

  test('should disable "add another" link when email is invalid', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    const initialEmailsValue = [{ id: '', emailAddress: '' }];
    render(
      <ContextWrapper
        permissions={[
          ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS,
          ManageCustomerDataPermissions.REMOVE_CUSTOMER_EMAILS
        ]}
      >
        <EditEmailsForm
          customerId={customer[0].id}
          emails={initialEmailsValue}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_EMAIL')
    ).toBeDisabled();
  });

  test('should enable "add another email" link when email is valid', async () => {
    const mockCloseForm = jest.fn();
    const mockOnEditFinish = jest.fn();
    render(
      <ContextWrapper
        permissions={[
          ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS,
          ManageCustomerDataPermissions.REMOVE_CUSTOMER_EMAILS
        ]}
      >
        <EditEmailsForm
          customerId={customer[0].id}
          emails={emails}
          onShouldRefreshChange={jest.fn()}
          onFormClose={mockCloseForm}
          onEditFinish={mockOnEditFinish}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_ANOTHER_EMAIL')
    ).toBeEnabled();
  });

  describe('Concurrency', () => {
    const service = GroupClientCustomerService.getInstance();
    const refreshedEmail = 'refreshedEmail@test.com';

    const submitFormAfterEmailEdit = async (
      getAllByTestId: (...args: any[]) => any,
      getSubmitButtonByText: (...args: any[]) => any
    ) => {
      userEvent.clear(getAllByTestId('text-input')[0]);
      userEvent.type(getAllByTestId('text-input')[0], 'test@test.com');

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });
    };

    test('should display concurrency modal when concurrency error returned', async () => {
      jest.spyOn(service, 'editCustomerEmail').mockRejectedValue(
        new ConcurrencyUpdateError({
          ...emails[0],
          emailAddress: refreshedEmail
        })
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS]}
        >
          <EditEmailsForm
            customerId={customer[0].id}
            emails={emails}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      await submitFormAfterEmailEdit(
        screen.getAllByTestId,
        getSubmitButtonByText
      );

      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).toBeInTheDocument();
    });

    test('should not display error modal when no error returned', async () => {
      jest
        .spyOn(service, 'editCustomerEmail')
        .mockReturnValue(Promise.resolve(emails[0]));

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS]}
        >
          <EditEmailsForm
            customerId={customer[0].id}
            emails={emails}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      await submitFormAfterEmailEdit(
        screen.getAllByTestId,
        getSubmitButtonByText
      );

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).not.toBeInTheDocument();
    });

    test('should display api response error modal when other error returned', async () => {
      jest
        .spyOn(service, 'editCustomerEmail')
        .mockRejectedValue({ status: '404' });

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS]}
        >
          <EditEmailsForm
            customerId={customer[0].id}
            emails={emails}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      await submitFormAfterEmailEdit(
        screen.getAllByTestId,
        getSubmitButtonByText
      );

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();
      expect(
        screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).toBeInTheDocument();

      await act(wait);
    });

    test('should refresh data when concurrency occurred', async () => {
      jest.spyOn(service, 'editCustomerEmail').mockRejectedValue(
        new ConcurrencyUpdateError({
          elements: [
            emails[0],
            {
              emailAddress: refreshedEmail
            }
          ]
        })
      );
      jest.spyOn(service, 'fetchCustomerEmailAddresses').mockReturnValueOnce(
        Promise.resolve({
          elements: [
            {
              ...emails[0],
              areaCode: 'oldMail@test.com'
            }
          ]
        } as any)
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS]}
        >
          <EditEmailsForm
            customerId={customer[0].id}
            emails={emails}
            onShouldRefreshChange={jest.fn()}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      userEvent.clear(screen.getAllByTestId('text-input')[0]);
      userEvent.type(screen.getAllByTestId('text-input')[0], 'test@test.com');

      expect(
        screen.queryByDisplayValue(refreshedEmail)
      ).not.toBeInTheDocument();

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      await act(() => {
        userEvent.click(
          screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
        );
        return wait();
      });

      expect(screen.getByDisplayValue(refreshedEmail)).toBeInTheDocument();

      await act(wait);
    });

    test('should call mockOnShouldRefreshChange when cancel after concurrency', async () => {
      const mockOnShouldRefreshChange = jest.fn();
      jest.spyOn(service, 'editCustomerEmail').mockRejectedValue(
        new ConcurrencyUpdateError({
          elements: [emails[0], { emailAddress: 'test@test.com' }]
        })
      );
      jest.spyOn(service, 'fetchCustomerEmailAddresses').mockReturnValueOnce(
        Promise.resolve({
          elements: [emails[0], { emailAddress: refreshedEmail }]
        } as any)
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS]}
        >
          <EditEmailsForm
            customerId={customer[0].id}
            emails={emails}
            onShouldRefreshChange={mockOnShouldRefreshChange}
            onFormClose={jest.fn()}
            onEditFinish={jest.fn()}
          />
        </ContextWrapper>
      );

      userEvent.clear(screen.getAllByTestId('text-input')[0]);
      userEvent.type(screen.getAllByTestId('text-input')[0], 'test@test.com');

      userEvent.clear(screen.getAllByTestId('text-input')[0]);
      userEvent.type(screen.getAllByTestId('text-input')[0], 'test@test.com');

      expect(
        screen.queryByDisplayValue(refreshedEmail)
      ).not.toBeInTheDocument();

      await act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
        return wait();
      });

      await act(() => {
        userEvent.click(
          screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
        );
        return wait();
      });

      userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

      expect(mockOnShouldRefreshChange).toHaveBeenCalledWith(true, [
        { emailAddress: 'obiwankenobi@tatooine.com', id: '1' },
        { emailAddress: 'test@test.com' }
      ]);
    });
  });
});
