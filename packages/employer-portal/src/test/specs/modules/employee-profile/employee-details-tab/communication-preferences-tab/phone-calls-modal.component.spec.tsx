import React from 'react';
import userEvent from '@testing-library/user-event';

import {
  render,
  wait,
  waitForElementToBeRemoved,
  act,
  screen
} from '../../../../../common/custom-render';
import { ContextWrapper } from '../../../../../common/utils';
import { CommunicationPreferencesContext } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.context';
import { EMPTY_CONTEXT_VALUES_FIXTURE } from '../../../../../fixtures/communication-preferences.fixture';
import {
  EMAILS_FIXTURE,
  PHONES_FIXTURE
} from '../../../../../fixtures/contact-details.fixture';
import { PhoneCallsModal } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/direct-correspondence/phone-calls-modal/phone-calls-modal.component';

describe('PhoneCallsModal', () => {
  test('should render', async () => {
    const mockContextValue = {
      phoneNumbers: PHONES_FIXTURE,
      communicationPreferences: {
        directCorrespondence: {
          phoneNumbers: [PHONES_FIXTURE[0]]
        }
      }
    };
    const { rerender } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <PhoneCallsModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('add-phone-number-form')).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <PhoneCallsModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.queryByTestId('select-phone-number')).toBeInTheDocument();
  });

  test('should validate selectable list', async () => {
    const mockContextValue = {
      ...EMPTY_CONTEXT_VALUES_FIXTURE,
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    };
    const validationText =
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.VALIDATION.PHONE';

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <PhoneCallsModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    expect(await screen.findByText(validationText)).toBeInTheDocument();
  });

  test('should not display select-phone-number after submiting with no phone number', async () => {
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <PhoneCallsModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    await act(() => {
      userEvent.type(screen.getAllByRole('textbox')[0], '111');
      userEvent.type(screen.getAllByRole('textbox')[1], '222');
      userEvent.type(screen.getAllByRole('textbox')[2], '333');
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      return wait();
    });

    expect(screen.queryByTestId('select-phone-number')).not.toBeInTheDocument();
  });

  test('should call onModalClose', async () => {
    const mockOnClose = jest.fn();
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <PhoneCallsModal
            isVisible={true}
            employeeId="1"
            onModalClose={mockOnClose}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    await act(() => {
      userEvent.click(
        screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.CANCEL/ })
      );
      return wait();
    });

    expect(mockOnClose).toHaveBeenCalledTimes(1);
  });

  test('should render with undefined data', async () => {
    const mockContextValue = {
      communicationPreferences: undefined,
      phoneNumbers: undefined,
      emailAddresses: undefined
    };

    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <PhoneCallsModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('add-phone-number-form')).toBeInTheDocument();
  });
});
