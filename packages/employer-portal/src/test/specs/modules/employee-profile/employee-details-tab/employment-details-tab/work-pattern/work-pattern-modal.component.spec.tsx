/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { WeekBasedWorkPattern } from 'fineos-js-api-client';
import { FormattedMessage } from 'fineos-common';

import {
  render,
  wait,
  act,
  within
} from '../../../../../../common/custom-render';
import {
  ContextWrapper,
  selectAntOptionByText
} from '../../../../../../common/utils';
import { WorkPatternModal } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/work-pattern/work-pattern-modal.component';
import * as workPatternApiModule from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/work-pattern/work-pattern.api';
import { WORK_PATTERNS } from '../../../../../../fixtures/work-pattern.fixture';

describe('WorkPatternModalContent', () => {
  test('should render add work pattern modal', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(
          Promise.resolve(WORK_PATTERNS[3] as WeekBasedWorkPattern)
        )
      );
    render(
      <ContextWrapper>
        <WorkPatternModal onSetMode={jest.fn()} customerId="1" />
      </ContextWrapper>
    );

    await act(wait);
    userEvent.click(
      within(screen.getByTestId('edit-work-pattern-button')).getByText(
        'WORK_PATTERN.EDIT_WORK_PATTERN'
      )
    );
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();
    expect(screen.getByText('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
  });

  test('should save fixed work pattern and close modal', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(WORK_PATTERNS[2] as WeekBasedWorkPattern)
      );
    jest
      .spyOn(workPatternApiModule, 'editWeekBasedWorkPattern')
      .mockReturnValue(Promise.resolve('test') as any);

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <WorkPatternModal onSetMode={jest.fn()} customerId="1" />
      </ContextWrapper>
    );

    await act(wait);
    userEvent.click(
      within(screen.getByTestId('edit-work-pattern-button')).getByText(
        'WORK_PATTERN.EDIT_WORK_PATTERN'
      )
    );

    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();

    expect(
      screen.getByText('WORK_PATTERN.NON_STANDARD_WORKING_WEEK')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.FRIDAY')
    ).toBeInTheDocument();

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();
  });

  test('should save rotating work pattern and close modal', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(WORK_PATTERNS[1] as WeekBasedWorkPattern)
      );
    jest
      .spyOn(workPatternApiModule, 'editWeekBasedWorkPattern')
      .mockReturnValue(Promise.resolve('test') as any);
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <WorkPatternModal onSetMode={jest.fn()} customerId="1" />
      </ContextWrapper>
    );

    await act(wait);
    userEvent.click(
      within(screen.getByTestId('edit-work-pattern-button')).getByText(
        'WORK_PATTERN.EDIT_WORK_PATTERN'
      )
    );
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.ROTATING'
    );

    await act(wait);
    userEvent.click(
      screen.getAllByText('INTAKE.OCCUPATION_AND_EARNINGS.WEEKS_NUMBER')[0]
    );
    await act(wait);

    expect(
      screen.getByText(
        'ENUM_DOMAINS.WORK_PATTERN_TYPE.ROTATING, WORK_PATTERN.TYPE_DESCRIPTION.ROTATING'
      )
    ).toBeInTheDocument();

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();
  });

  test('should save variable work pattern and close modal', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(WORK_PATTERNS[0] as WeekBasedWorkPattern)
      );
    jest
      .spyOn(workPatternApiModule, 'editWeekBasedWorkPattern')
      .mockReturnValue(Promise.resolve('test') as any);
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <WorkPatternModal onSetMode={jest.fn()} customerId="1" />
      </ContextWrapper>
    );

    await act(wait);
    userEvent.click(
      within(screen.getByTestId('edit-work-pattern-button')).getByText(
        'WORK_PATTERN.EDIT_WORK_PATTERN'
      )
    );
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.VARIABLE'
    );
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN_REPEATS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_DAY')
    ).toBeInTheDocument();

    userEvent.type(
      screen.getByLabelText(
        'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION*'
      ),
      'test title'
    );
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();
  });

  test('should add new work pattern', async () => {
    jest
      .spyOn(workPatternApiModule, 'addWeekBasedWorkPattern')
      .mockReturnValue(
        Promise.resolve(WORK_PATTERNS[2] as WeekBasedWorkPattern)
      );
    render(
      <ContextWrapper>
        <WorkPatternModal
          onSetMode={jest.fn()}
          customerId="1"
          triggerButtonName={
            <FormattedMessage id="WORK_PATTERN.ADD_WORK_PATTERN" />
          }
          modalHeader={<FormattedMessage id="WORK_PATTERN.ADD_WORK_PATTERN" />}
        />
      </ContextWrapper>
    );

    await act(wait);
    userEvent.click(
      within(screen.getByRole('button')).getByText(
        'WORK_PATTERN.ADD_WORK_PATTERN'
      )
    );
    await act(wait);

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.WORK_PATTERN')
    ).toBeInTheDocument();
    expect(screen.getByText('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
  });
});
