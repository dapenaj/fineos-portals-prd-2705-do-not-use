import React from 'react';
import userEvent from '@testing-library/user-event';
import each from 'jest-each';

import {
  render,
  act,
  wait,
  waitForElementToBeRemoved,
  screen
} from '../../../../../common/custom-render';
import { ContextWrapper } from '../../../../../common/utils';
import {
  EMPTY_CONTEXT_VALUES_FIXTURE,
  CONTEXT_VALUES_FIXTURE
} from '../../../../../fixtures/communication-preferences.fixture';
import { CommunicationPreferencesContext } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.context';
import { WrittenCorrespondenceModal } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/written-correspondence/written-correspondence-modal/written-correspondence-modal.component';
import {
  PHONES_FIXTURE,
  EMAILS_FIXTURE
} from '../../../../../fixtures/contact-details.fixture';

describe('WrittenCorrespondenceModal', () => {
  const componentText = {
    paperRadioLabel:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_PAPER',
    portalRadioLabel:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_TYPE_PORTAL',
    smsRadioLabel:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_SMS',
    emailRadioLabel:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_EMAIL',
    correspondenceDescription:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.CORRESPONDENCE_DESCRIPTION',
    typeDescription:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE_MODAL.NOTIFICATION_TYPE_QUESTION'
  };

  test('should render', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText(componentText.paperRadioLabel)).toBeInTheDocument();
    expect(
      screen.getByText(componentText.portalRadioLabel)
    ).toBeInTheDocument();
  });

  test('should display portal communication section after checking PORTAL communication', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
      return wait();
    });

    expect(
      screen.getByText(componentText.correspondenceDescription)
    ).toBeInTheDocument();
    expect(screen.getByText(componentText.typeDescription)).toBeInTheDocument();
  });

  test('should display correct correct section with phoneNumbers and emails', async () => {
    const mockContextValue = {
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE,
      communicationPreferences: {
        writtenCorrespondence: {
          phoneNumbers: [PHONES_FIXTURE[0]],
          emailAddresses: [PHONES_FIXTURE[0]]
        }
      }
    };
    const { rerender } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
      userEvent.click(screen.getByLabelText(componentText.smsRadioLabel));
      return wait();
    });

    expect(screen.getByTestId('select-phone-number')).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
      userEvent.click(screen.getByLabelText(componentText.emailRadioLabel));
      return wait();
    });

    expect(screen.getByTestId('select-email')).toBeInTheDocument();
  });

  test('should display correct section without phone numbers and emails', async () => {
    const { rerender } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
    userEvent.click(screen.getByLabelText(componentText.smsRadioLabel));

    await act(wait);

    expect(screen.getByTestId('add-phone-number-form')).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
      userEvent.click(screen.getByLabelText(componentText.emailRadioLabel));
      return wait();
    });

    expect(screen.getByTestId('add-email-form')).toBeInTheDocument();
  });

  test('should validate portalCommunicationType', async () => {
    const mockContextValue = {
      ...EMPTY_CONTEXT_VALUES_FIXTURE,
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    };
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    expect(
      await screen.findByText(
        'FINEOS_COMMON.VALIDATION.REQUIRED_SELECTED_OPTION'
      )
    ).toBeInTheDocument();
  });

  test('should validate selectable list', async () => {
    const mockContextValue = {
      ...EMPTY_CONTEXT_VALUES_FIXTURE,
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    };
    const validationText =
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.VALIDATION';

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
    userEvent.click(screen.getByLabelText(componentText.emailRadioLabel));
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await act(wait);

    expect(
      screen.getByText('FINEOS_COMMON.VALIDATION.REQUIRED_SELECTED_OPTION')
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText(EMAILS_FIXTURE[0].emailAddress, { exact: false })
    );
    expect(
      await waitForElementToBeRemoved(() =>
        screen.queryByText(`${validationText}.EMAIL`)
      )
    );

    userEvent.click(screen.getByLabelText(componentText.smsRadioLabel));
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    expect(
      await screen.findByText(`${validationText}.PHONE`)
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText(PHONES_FIXTURE[0].telephoneNo, { exact: false })
    );

    expect(
      await waitForElementToBeRemoved(() =>
        screen.queryByText(`${validationText}.PHONE`)
      )
    );
  });

  test('should not preselect communication options if preferences are choosen only in other context', async () => {
    const mockContextValue = {
      ...CONTEXT_VALUES_FIXTURE,
      communicationPreferences: {
        ...CONTEXT_VALUES_FIXTURE.communicationPreferences,
        writtenCorrespondence: {
          phoneNumbers: [],
          emailAddresses: []
        }
      },
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    } as any;

    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByLabelText(componentText.paperRadioLabel)
    ).toHaveAttribute('checked');
  });

  each([
    ['', '', EMAILS_FIXTURE[0].emailAddress, EMAILS_FIXTURE[0].emailAddress],
    ['', EMAILS_FIXTURE[0].emailAddress, '', EMAILS_FIXTURE[0].emailAddress],
    [
      '',
      EMAILS_FIXTURE[0].emailAddress,
      EMAILS_FIXTURE[1].emailAddress,
      EMAILS_FIXTURE[1].emailAddress
    ]
  ]).test(
    'When directCorrespondence email set to: "%s", notificationOfUpdates email set to: "%s" and writtenCorrespondence email set to: "%s", following email should be selected by default: "%s".',
    async (
      directCorrespondenceEmail,
      notificationOfUpdatesEmail,
      writtenCorrespondenceEmail,
      expectedEmail
    ) => {
      const mockContextValue = {
        ...CONTEXT_VALUES_FIXTURE,
        phoneNumbers: PHONES_FIXTURE,
        emailAddresses: EMAILS_FIXTURE,
        communicationPreferences: {
          ...CONTEXT_VALUES_FIXTURE.communicationPreferences,
          directCorrespondence: {
            phoneNumbers: [],
            emailAddresses: directCorrespondenceEmail
              ? [{ emailAddress: directCorrespondenceEmail }]
              : []
          },
          notificationOfUpdates: {
            phoneNumbers: [],
            emailAddresses: notificationOfUpdatesEmail
              ? [{ emailAddress: notificationOfUpdatesEmail }]
              : []
          },
          writtenCorrespondence: {
            phoneNumbers: [],
            emailAddresses: writtenCorrespondenceEmail
              ? [{ emailAddress: writtenCorrespondenceEmail }]
              : []
          }
        }
      } as any;

      render(
        <ContextWrapper>
          <CommunicationPreferencesContext.Provider value={mockContextValue}>
            <WrittenCorrespondenceModal
              isVisible={true}
              employeeId="1"
              onModalClose={jest.fn()}
            />
          </CommunicationPreferencesContext.Provider>
        </ContextWrapper>
      );

      await act(wait);

      userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
      userEvent.click(screen.getByLabelText(componentText.emailRadioLabel));

      await act(wait);

      expect(screen.getByText(expectedEmail).closest('label')).toHaveClass(
        'ant-radio-wrapper-checked'
      );
    }
  );

  test('should always select preferred email/phone after changing preference communication type', async () => {
    const mockContextValue = {
      ...CONTEXT_VALUES_FIXTURE,
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    };

    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getByLabelText(componentText.portalRadioLabel));
    userEvent.click(screen.getByLabelText(componentText.smsRadioLabel));

    await act(wait);

    expect(document.querySelectorAll('.phoneNumberCard label')[0]).toHaveClass(
      'ant-radio-wrapper-checked'
    );

    userEvent.click(document.querySelectorAll('.phoneNumberCard label')[1]);

    await act(wait);

    expect(
      document.querySelectorAll('.phoneNumberCard label')[0]
    ).not.toHaveClass('ant-radio-wrapper-checked');

    userEvent.click(screen.getByLabelText(componentText.emailRadioLabel));
    userEvent.click(screen.getByLabelText(componentText.smsRadioLabel));

    await act(wait);

    expect(document.querySelectorAll('.phoneNumberCard label')[0]).toHaveClass(
      'ant-radio-wrapper-checked'
    );
  });

  test('should call onModalClose', async () => {
    const mockOnModalClose = jest.fn();
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={mockOnModalClose}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.CANCEL/ })
    );

    await act(wait);

    expect(mockOnModalClose).toHaveBeenCalledTimes(1);
  });

  test('should render with undefined data', async () => {
    const mockContextValue = {
      communicationPreferences: undefined,
      phoneNumbers: undefined,
      emailAddresses: undefined
    };

    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <WrittenCorrespondenceModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText(componentText.paperRadioLabel)).toBeInTheDocument();
    expect(
      screen.getByText(componentText.portalRadioLabel)
    ).toBeInTheDocument();
  });
});
