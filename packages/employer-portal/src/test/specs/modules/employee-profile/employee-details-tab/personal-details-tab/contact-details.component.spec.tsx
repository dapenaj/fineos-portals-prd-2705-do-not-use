/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, within, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ManageCustomerDataPermissions } from 'fineos-common';

import { ContextWrapper } from '../../../../../common/utils';
import { ContactDetails } from '../../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/contact-details/contact-details.component';
import {
  EMAILS_FIXTURE,
  PHONES_FIXTURE
} from '../../../../../fixtures/contact-details.fixture';
import { CUSTOMER_SUMMARY_FIXTURE } from '../../../../../fixtures/customer.fixture';

const permissions = [
  ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS,
  ManageCustomerDataPermissions.ADD_CUSTOMER_EMAILS,
  ManageCustomerDataPermissions.UPDATE_CUSTOMER_PHONE_NUMBERS,
  ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS
];

describe('ContactDetails', () => {
  test('should render', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <ContactDetails
          customerId={CUSTOMER_SUMMARY_FIXTURE[0].id}
          emails={[]}
          phones={[]}
          onChangedEmail={jest.fn()}
          onChangedPhones={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.EMAILS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS')
    ).toBeInTheDocument();
    expect(
      screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE')
    ).toBeInTheDocument();
    expect(screen.getAllByTestId('property-item-value')[0]).toHaveTextContent(
      '-'
    );
    expect(screen.getAllByTestId('property-item-value')[1]).toHaveTextContent(
      '-'
    );
  });

  test('should render contact details', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <ContactDetails
          customerId={CUSTOMER_SUMMARY_FIXTURE[0].id}
          emails={EMAILS_FIXTURE}
          phones={PHONES_FIXTURE}
          onChangedEmail={jest.fn()}
          onChangedPhones={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getAllByTestId('property-item-value')[0].children
    ).toHaveLength(EMAILS_FIXTURE.length);
    expect(
      screen.getAllByTestId('property-item-value')[1].children
    ).toHaveLength(PHONES_FIXTURE.length);
  });

  test('should open edit email modal', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <ContactDetails
          customerId={CUSTOMER_SUMMARY_FIXTURE[0].id}
          emails={EMAILS_FIXTURE}
          phones={PHONES_FIXTURE}
          onChangedEmail={jest.fn()}
          onChangedPhones={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      within(screen.getByTestId('email-actions-button')).getByText(
        'PROFILE.PERSONAL_DETAILS_TAB.EDIT_EMAILS'
      )
    );

    await act(wait);

    expect(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.EDIT_EMAILS')
    ).toHaveLength(2);
  });

  test('should open add email modal', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <ContactDetails
          customerId={CUSTOMER_SUMMARY_FIXTURE[0].id}
          emails={[]}
          phones={[]}
          onChangedEmail={jest.fn()}
          onChangedPhones={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_EMAIL')[0]
    );

    await act(wait);

    expect(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_EMAIL')
    ).toHaveLength(2);
  });

  test('should open edit phone numbers modal', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <ContactDetails
          customerId={CUSTOMER_SUMMARY_FIXTURE[0].id}
          emails={EMAILS_FIXTURE}
          phones={PHONES_FIXTURE}
          onChangedEmail={jest.fn()}
          onChangedPhones={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      within(screen.getByTestId('phone-number-actions-button')).getByText(
        'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PHONE_NUMBERS'
      )
    );

    await act(wait);

    expect(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.EDIT_PHONE_NUMBERS')
    ).toHaveLength(2);
  });

  test('should open add phone number modal', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <ContactDetails
          customerId={CUSTOMER_SUMMARY_FIXTURE[0].id}
          emails={[]}
          phones={[]}
          onChangedEmail={jest.fn()}
          onChangedPhones={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE')[0]
    );

    await act(wait);

    expect(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.ADD_PHONE')
    ).toHaveLength(2);
  });
});
