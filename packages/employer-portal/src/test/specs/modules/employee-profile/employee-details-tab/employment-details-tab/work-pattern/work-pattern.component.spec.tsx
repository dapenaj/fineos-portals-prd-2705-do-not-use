/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { act, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { WeekBasedWorkPattern } from 'fineos-js-api-client';
import { ManageOccupationPermissions } from 'fineos-common';

import { render } from '../../../../../../common/custom-render';
import {
  ContextWrapper,
  selectAntOptionByText
} from '../../../../../../common/utils';
import { WorkPattern } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/work-pattern';
import * as workPatternApiModule from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/work-pattern/work-pattern.api';
import { WORK_PATTERNS } from '../../../../../../fixtures/work-pattern.fixture';
import { WorkPatternBasis } from '../../../../../../../app/shared/work-pattern';
import { within } from '../../../../../../common/custom-render';

describe('WorkPattern', () => {
  test('should render the empty state for work pattern', async () => {
    render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.ADD_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
        ]}
      >
        <WorkPattern
          customerId="1"
          occupation={{ workPatternBasis: WorkPatternBasis.UNKNOWN } as any}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('WORK_PATTERN.HEADER')).toBeInTheDocument();
    expect(
      within(screen.getByTestId('add-work-pattern-button')).getByText(
        'WORK_PATTERN.ADD_WORK_PATTERN'
      )
    ).toBeInTheDocument();
  });

  test('should render the empty state for work pattern and show confirm modal for variable pattern type', async () => {
    jest
      .spyOn(workPatternApiModule, 'editVariableWorkPattern')
      .mockReturnValue(Promise.resolve(1));
    const { getSubmitButtonByText } = render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.ADD_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
        ]}
      >
        <WorkPattern
          customerId="1"
          occupation={{ workPatternBasis: WorkPatternBasis.UNKNOWN } as any}
        />
      </ContextWrapper>
    );
    await act(wait);

    expect(screen.getByText('WORK_PATTERN.HEADER')).toBeInTheDocument();

    userEvent.click(
      within(screen.getByTestId('add-work-pattern-button')).getByText(
        'WORK_PATTERN.ADD_WORK_PATTERN'
      )
    );
    await act(wait);

    await selectAntOptionByText(
      within(screen.getByTestId('work-pattern-field')).getByTestId(
        'dropdown-input'
      ),
      'ENUM_DOMAINS.WORK_PATTERN_TYPE.VARIABLE'
    );

    await act(wait);
    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION')
    ).toBeInTheDocument();

    userEvent.type(
      screen.getByLabelText(
        'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION*'
      ),
      'test title'
    );

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
    await act(wait);

    expect(
      screen.getByText('WORK_PATTERN.SUBMIT_SUCCESS_MESSAGE')
    ).toBeInTheDocument();
  });

  test('should render the variable work pattern', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(
          Promise.resolve(WORK_PATTERNS[0] as WeekBasedWorkPattern)
        )
      );

    render(
      <ContextWrapper>
        <WorkPattern
          customerId="1"
          occupation={{ workPatternBasis: WorkPatternBasis.OTHER } as any}
        />
      </ContextWrapper>
    );
    await act(wait);

    expect(
      screen.getByText('WORK_PATTERN.VARIABLE_WORK_PATTERN')
    ).toBeInTheDocument();
  });

  test('should render the rotating work pattern', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(WORK_PATTERNS[1] as WeekBasedWorkPattern)
      );

    render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.EDIT_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
        ]}
      >
        <WorkPattern
          customerId="1"
          occupation={
            { workPatternBasis: WorkPatternBasis.WEEK_BASED, id: '1' } as any
          }
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('WORK_PATTERN.HEADER')).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.EMPLOYEE_WORK_PATTERN_ROTATING')
    ).toBeInTheDocument();
    expect(screen.getByTestId('week-1-MONDAY-input')).toBeInTheDocument();
    expect(screen.getByTestId('week-2-MONDAY-input')).toBeInTheDocument();
    expect(
      within(screen.getByTestId('edit-work-pattern-button')).getByText(
        'WORK_PATTERN.EDIT_WORK_PATTERN'
      )
    ).toBeInTheDocument();
  });

  test('should render the fixed work pattern', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(WORK_PATTERNS[2] as WeekBasedWorkPattern)
      );

    render(
      <ContextWrapper
        permissions={[
          ManageOccupationPermissions.EDIT_CUSTOMERS_CUSTOMER_OCCUPATIONS_REGULARWEEKLYWORKPATTERN
        ]}
      >
        <WorkPattern
          customerId="1"
          occupation={
            { workPatternBasis: WorkPatternBasis.WEEK_BASED, id: '1' } as any
          }
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('week-1-MONDAY-input')).toBeInTheDocument();
    expect(screen.getByText('WORK_PATTERN.HEADER')).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.EMPLOYEE_WORK_PATTERN_FIXED')
    ).toBeInTheDocument();
    expect(
      within(screen.getByTestId('edit-work-pattern-button')).getByText(
        'WORK_PATTERN.EDIT_WORK_PATTERN'
      )
    ).toBeInTheDocument();
  });
});
