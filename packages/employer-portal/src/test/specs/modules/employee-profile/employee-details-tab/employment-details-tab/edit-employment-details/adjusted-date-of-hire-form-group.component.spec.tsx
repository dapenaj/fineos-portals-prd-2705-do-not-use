/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment from 'moment';
import { Formik } from 'formik';
import { ManageOccupationPermissions } from 'fineos-common';

import {
  render,
  fireEvent,
  screen
} from '../../../../../../common/custom-render';
import {
  ContextWrapper,
  mockTranslations,
  selectDate
} from '../../../../../../common/utils';
import { AdjustedDateOfHireFormGroup } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/current-occupation/edit-employment-details/adjusted-date-of-hire-form-group/adjusted-date-of-hire-form-group.component';

const getDatepickerDayInMonth = (startDay: number) => {
  const dayOfMonth = moment().weekday();
  return dayOfMonth < startDay ? moment().add(1, 'day') : moment();
};

describe('AdjustedDateOfHireFormGroup', () => {
  const translations = mockTranslations(
    'EDIT_EMPLOYMENT_DETAILS.ADJUSTED_HIRE_DATE.POPOVER',
    'ADJUSTED_DATE_OF_HIRE_AFTER_TODAY_WARNING',
    'JOB_END_DATE_AFTER_ADJUSTED_DATE_WARNING'
  );
  test('should render', () => {
    render(
      <ContextWrapper>
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <AdjustedDateOfHireFormGroup />
        </Formik>
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ADJUSTED_DATE_OF_HIRE'
      )
    ).toBeInTheDocument();
  });

  test('should display popover after mouse enter', async () => {
    render(
      <ContextWrapper translations={translations}>
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <AdjustedDateOfHireFormGroup />
        </Formik>
      </ContextWrapper>
    );
    fireEvent.mouseEnter(
      document.querySelector(
        '[aria-label="FINEOS_COMMON.ICON_LABEL.QUESTION_MARK_ICON_LABEL"]'
      )!
    );

    expect(
      await screen.findByText(
        'EDIT_EMPLOYMENT_DETAILS.ADJUSTED_HIRE_DATE.POPOVER'
      )
    ).toBeInTheDocument();
  });

  test('should disable input', () => {
    const { rerender } = render(
      <ContextWrapper
        permissions={[ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT]}
      >
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <AdjustedDateOfHireFormGroup isDisabled={true} />
        </Formik>
      </ContextWrapper>
    );

    expect(screen.getByTestId('adjusted-hire-date-picker')).toBeDisabled();

    rerender(
      <ContextWrapper
        permissions={[ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT]}
      >
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <AdjustedDateOfHireFormGroup isDisabled={false} />
        </Formik>
      </ContextWrapper>
    );

    expect(screen.getByTestId('adjusted-hire-date-picker')).not.toBeDisabled();
  });

  test('should show future date warning', async () => {
    const mockJobEndDate = moment()
      .add(3, 'days')
      .format('YYYY-MM-DD');
    const tomorrow = moment()
      .add(2, 'days')
      .startOf('day')
      .format('YYYY-MM-DD');
    render(
      <ContextWrapper
        translations={translations}
        permissions={[ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT]}
      >
        <Formik
          onSubmit={jest.fn()}
          initialValues={{
            jobEndDate: mockJobEndDate
          }}
        >
          <AdjustedDateOfHireFormGroup />
        </Formik>
      </ContextWrapper>
    );

    await selectDate(screen.getByTestId('adjusted-hire-date-picker'), tomorrow);

    expect(
      screen.getByText('ADJUSTED_DATE_OF_HIRE_AFTER_TODAY_WARNING')
    ).toBeInTheDocument();
  });

  test('should show adjusted hire date is after jobEndDate warning', async () => {
    const adjustedHireDate = getDatepickerDayInMonth(2)
      .subtract(2, 'days')
      .startOf('day')
      .format('YYYY-MM-DD');
    const jobEndDate = moment()
      .subtract(3, 'days')
      .startOf('day')
      .format('YYYY-MM-DD');
    render(
      <ContextWrapper
        translations={translations}
        permissions={[ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT]}
      >
        <Formik
          onSubmit={jest.fn()}
          initialValues={{
            jobEndDate
          }}
        >
          <AdjustedDateOfHireFormGroup />
        </Formik>
      </ContextWrapper>
    );

    await selectDate(
      screen.getByTestId('adjusted-hire-date-picker'),
      adjustedHireDate
    );

    expect(
      screen.getByText('JOB_END_DATE_AFTER_ADJUSTED_DATE_WARNING')
    ).toBeInTheDocument();
  });

  test('should show only adjusted hire date is after jobEndDate warning when both warning conditions are true', async () => {
    const mockJobEndDate = moment()
      .add(1, 'days')
      .format('YYYY-MM-DD');
    const tomorrow = moment()
      .add(2, 'days')
      .startOf('day')
      .format('YYYY-MM-DD');
    render(
      <ContextWrapper
        translations={translations}
        permissions={[ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT]}
      >
        <Formik
          onSubmit={jest.fn()}
          initialValues={{
            jobEndDate: mockJobEndDate
          }}
        >
          <AdjustedDateOfHireFormGroup />
        </Formik>
      </ContextWrapper>
    );

    await selectDate(screen.getByTestId('adjusted-hire-date-picker'), tomorrow);

    expect(
      screen.queryByText('FUTURE_ADJUSTED_DATE_OF_HIRE_WARNING')
    ).not.toBeInTheDocument();
    expect(
      screen.getByText('JOB_END_DATE_AFTER_ADJUSTED_DATE_WARNING')
    ).toBeInTheDocument();
  });
});
