/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import moment from 'moment';
import userEvent from '@testing-library/user-event';
import {
  ConcurrencyUpdateError,
  GroupClientCustomerService
} from 'fineos-js-api-client';
import {
  ViewOccupationPermissions,
  ManageOccupationPermissions
} from 'fineos-common';
import { Formik } from 'formik';

import {
  render,
  act,
  wait,
  fireEvent,
  screen
} from '../../../../../../common/custom-render';
import {
  ContextWrapper,
  selectAntOptionByText,
  selectDate,
  selectTodayDate,
  checkIsConcurrencyModalVisible
} from '../../../../../../common/utils';
import { EditEmploymentDetails } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/current-occupation/edit-employment-details';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../../../fixtures/customer-occupations.fixture';
import { CONTRACTUAL_EARNINGS_FIXTURE } from '../../../../../../fixtures/contractual-earnings.fixture';
import { ABSENCE_EMPLOYMENT_FIXTURE } from '../../../../../../fixtures/absence-employment.fixture';

describe('EditEmploymentDetails', () => {
  const service = GroupClientCustomerService.getInstance();
  const occupation = CUSTOMER_OCCUPATIONS_FIXTURE[1];
  const contractualEarnings = CONTRACTUAL_EARNINGS_FIXTURE[0];
  const absenceEmployment = ABSENCE_EMPLOYMENT_FIXTURE[0];

  const EditEmploymentDetailsWrapper = () => (
    <Formik onSubmit={jest.fn()} initialValues={{}}>
      <EditEmploymentDetails
        titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.OCCUPATION_TITLE_EDIT"
        buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON"
        buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON_ARIA_LABEL"
        employeeId="test"
      />
    </Formik>
  );

  test('should render all elements', async () => {
    render(
      <ContextWrapper
        permissions={[
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
          ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
        ]}
      >
        <EditEmploymentDetailsWrapper />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
      )
    );

    expect(
      await screen.findByText('FINEOS_COMMON.GENERAL.OK')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_START_DATE*/
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_END_DATE/
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
      )
    ).toBeInTheDocument();
  });

  test('should have attribute aria-invalid when input field is invalid', async () => {
    render(
      <ContextWrapper>
        <EditEmploymentDetailsWrapper />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
      )
    );

    fireEvent.change(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
      ),
      { target: { value: '' } }
    );

    userEvent.click(await screen.findByText('FINEOS_COMMON.GENERAL.OK'));

    expect(await screen.findByTestId('text-input')).toHaveAttribute(
      'aria-invalid'
    );
  });

  test('should show adjusted date of hire form group', async () => {
    render(
      <ContextWrapper>
        <EditEmploymentDetailsWrapper />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
      )
    );

    expect(
      screen.queryByTestId('adjusted-date-of-hire-form-group')
    ).not.toBeInTheDocument();

    userEvent.click(await screen.findByTestId('switch-input'));

    expect(
      await screen.findByTestId('adjusted-date-of-hire-form-group')
    ).toBeInTheDocument();
  });

  describe('Concurrency', () => {
    beforeEach(() => {
      jest
        .spyOn(service, 'fetchCustomerOccupations')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [occupation] } as any)
        );

      jest
        .spyOn(service, 'fetchCustomerContractualEarnings')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [contractualEarnings] } as any)
        );
    });

    test('should display data concurrency modal when concurrency error returned for occupations', async () => {
      jest
        .spyOn(service, 'editCustomerOccupation')
        .mockRejectedValueOnce(
          new ConcurrencyUpdateError({ elements: [occupation] })
        );
      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [contractualEarnings] } as any)
        );
      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test job title'
      );

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await checkIsConcurrencyModalVisible();
    });

    test('should display data concurrency modal when concurrency error returned for earnings', async () => {
      jest
        .spyOn(service, 'editCustomerOccupation')
        .mockRejectedValueOnce(
          new ConcurrencyUpdateError({ elements: [occupation] })
        );
      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockRejectedValueOnce(
          new ConcurrencyUpdateError({ elements: [contractualEarnings] })
        );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test job title'
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        ),
        '800'
      );
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await checkIsConcurrencyModalVisible();
    });

    test('should not display data concurrency modal when concurrency error not returned', async () => {
      jest
        .spyOn(service, 'editCustomerOccupation')
        .mockReturnValue(Promise.resolve({ test: 'test' } as any));

      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockReturnValue(Promise.resolve({ test: 'test' } as any));

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test job title'
      );
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();
    });

    test('should refresh data in form for occupations when concurrency occurred', async () => {
      const refreshedTitle = 'refreshed title';
      const occupationRefresh = jest
        .spyOn(service, 'editCustomerOccupation')
        .mockRejectedValue(
          new ConcurrencyUpdateError({
            elements: [
              {
                ...occupation,
                jobTitle: refreshedTitle
              }
            ]
          })
        );
      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockRejectedValueOnce(
          new ConcurrencyUpdateError({ elements: [contractualEarnings] })
        );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test job title'
      );

      await selectTodayDate(await screen.findByTestId('job-start-date-picker'));
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        )
      ).not.toHaveValue(refreshedTitle);

      await checkIsConcurrencyModalVisible();

      userEvent.click(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      );

      expect(occupationRefresh).toHaveBeenCalled();

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        )
      ).toHaveValue(refreshedTitle);
    });

    test('should refresh data in form for earnings when concurrency occurred', async () => {
      jest.spyOn(service, 'editCustomerOccupation').mockRejectedValueOnce(
        new ConcurrencyUpdateError({
          elements: [occupation]
        })
      );
      jest.spyOn(service, 'editCustomerContractualEarnings').mockRejectedValue(
        new ConcurrencyUpdateError({
          elements: [CONTRACTUAL_EARNINGS_FIXTURE[2]]
        })
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test job title'
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        ),
        '1000'
      );
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await checkIsConcurrencyModalVisible();

      userEvent.click(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toHaveValue('800');
    });

    test('should refresh data in form for absence employment when concurrency occurred', async () => {
      const refreshedAdjustedHireDate = moment().subtract(5, 'days');
      const initiallyChangedAdjutedHireDate = moment()
        .subtract(1, 'days')
        .format('YYYY-MM-DD');
      jest.spyOn(service, 'fetchCustomerAbsenceEmployment').mockReturnValueOnce(
        Promise.resolve({
          ...absenceEmployment,
          adjustedHireDate: moment()
        } as any)
      );
      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [contractualEarnings] } as any)
        );
      jest.spyOn(service, 'editCustomerAbsenceEmployment').mockRejectedValue(
        new ConcurrencyUpdateError({
          ...absenceEmployment,
          adjustedHireDate: refreshedAdjustedHireDate
        })
      );
      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT,
            ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT,
            ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await selectDate(
        await screen.findByTestId('adjusted-hire-date-picker'),
        initiallyChangedAdjutedHireDate
      );
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await checkIsConcurrencyModalVisible();

      userEvent.click(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByTestId('adjusted-hire-date-picker')
      ).toHaveValue(refreshedAdjustedHireDate.format('DD-MM-YYYY'));
    });

    test('should refresh data when both occupations and earnings concurrency', async () => {
      const refreshedJobTitle = 'refreshed job title';
      jest.spyOn(service, 'editCustomerOccupation').mockRejectedValueOnce(
        new ConcurrencyUpdateError({
          elements: [
            {
              ...occupation,
              jobTitle: refreshedJobTitle
            }
          ]
        })
      );
      jest.spyOn(service, 'editCustomerContractualEarnings').mockRejectedValue(
        new ConcurrencyUpdateError({
          elements: [CONTRACTUAL_EARNINGS_FIXTURE[2]]
        })
      );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test job title'
      );
      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        ),
        '800'
      );

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      userEvent.click(
        await screen.findByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        )
      ).toHaveValue(refreshedJobTitle);
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toHaveValue('800');
    });

    test('should display error response modal and no concurrency modal when error other than concurrency occurred', async () => {
      jest
        .spyOn(service, 'editCustomerOccupation')
        .mockRejectedValue({ status: '404' });

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        ),
        'test title'
      );

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();

      expect(
        await screen.findByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).toBeInTheDocument();
    });
  });

  describe('Error response handling', () => {
    beforeEach(() => {
      jest
        .spyOn(service, 'fetchCustomerOccupations')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [occupation] } as any)
        );

      jest
        .spyOn(service, 'fetchCustomerContractualEarnings')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [contractualEarnings] } as any)
        );
    });
    test('should show modal when one of requests failed', async () => {
      jest
        .spyOn(service, 'editCustomerOccupation')
        .mockRejectedValueOnce(occupation);
      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [contractualEarnings] } as any)
        );

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await act(wait);

      userEvent.type(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'),
        'test job title'
      );

      await act(() => {
        fireEvent.change(
          screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'),
          { target: { value: 9000 } }
        );
        return wait();
      });

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await act(wait);

      expect(
        screen.getByText(
          'FINEOS_COMMON.SOMETHING_WENT_WRONG.PARTIAL_API_ERROR_MESSAGE'
        )
      ).toBeInTheDocument();
    });

    test('should show modal when all requests failed', async () => {
      jest
        .spyOn(service, 'editCustomerOccupation')
        .mockRejectedValueOnce(occupation);
      jest
        .spyOn(service, 'editCustomerContractualEarnings')
        .mockRejectedValueOnce(contractualEarnings);

      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await act(wait);

      userEvent.type(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'),
        'test job title'
      );

      await act(() => {
        fireEvent.change(
          screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'),
          { target: { value: 9000 } }
        );
        return wait();
      });

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await act(wait);

      expect(
        screen.getByText(
          'FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE'
        )
      ).toBeInTheDocument();
    });
  });

  describe('hrsWorkedPerWeek validation', () => {
    test('should correctly round hrsWorkedPerWeek to one digit after decimal', async () => {
      render(
        <ContextWrapper>
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
        ),
        '40.5555'
      );
      fireEvent.blur(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
        )
      ).toHaveValue('40.6');
    });

    test('should change hrsWorkedPerWeek field value to 0 if no value', async () => {
      render(
        <ContextWrapper
          permissions={[ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      fireEvent.change(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
        ),
        { target: { value: '0' } }
      );
      fireEvent.blur(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_WORKED_PER_WEEK*'
        )
      ).toHaveValue('0.0');
    });
  });

  describe('Contractual earnings validation', () => {
    test('should show contractual earnings as invalid', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );
      await act(wait);

      await act(() => {
        fireEvent.change(
          screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'),
          { target: { value: 123456789012345 } }
        );
        return wait();
      });

      expect(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS')
      ).not.toBeValid();

      fireEvent.change(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'),
        { target: { value: 500.555 } }
      );
      await act(wait);

      expect(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS')
      ).not.toBeValid();
    });

    test('should show earnings frequency as invalid', async () => {
      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await wait();

      fireEvent.change(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'),
        { target: { value: 500 } }
      );

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await wait();

      expect(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.FREQUENCY')
      ).not.toBeValid();
    });

    test('should display Contractual Earnings only with proper permissions and data', async () => {
      const { rerender } = render(
        <ContextWrapper>
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        screen.queryByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS')
      ).not.toBeInTheDocument();

      rerender(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toBeInTheDocument();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).not.toBeDisabled();

      rerender(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toBeInTheDocument();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toBeDisabled();

      rerender(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toBeInTheDocument();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).not.toBeDisabled();

      rerender(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toBeInTheDocument();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).toBeDisabled();
    });

    test('should send request when adjustedHireDate visibility changes', async () => {
      jest
        .spyOn(service, 'fetchCustomerOccupations')
        .mockReturnValueOnce(
          Promise.resolve({ elements: [occupation] } as any)
        );
      jest.spyOn(service, 'fetchCustomerAbsenceEmployment').mockReturnValueOnce(
        Promise.resolve({
          ...absenceEmployment,
          adjustedHireDate: moment()
        } as any)
      );
      const mockEditAbsenceEmployment = jest.spyOn(
        service,
        'editCustomerAbsenceEmployment'
      );
      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
            ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT,
            ManageOccupationPermissions.EDIT_ABSENCE_EMPLOYMENT,
            ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await act(wait);

      userEvent.click(screen.getByTestId('switch-input'));
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await act(wait);

      expect(mockEditAbsenceEmployment).toHaveBeenCalledTimes(1);
    });

    test('should show correct validation for initial earnings amount value', async () => {
      const { getSubmitButtonByText } = render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await selectAntOptionByText(
        await screen.findByTestId('earning-frequency'),
        'ENUM_DOMAINS.EARNING_FREQUENCY.WEEKLY'
      );

      act(() => {
        userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      });

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      ).not.toBeValid();
    });

    test('should display invalid precision message', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        ),
        '11.111.01'
      );

      fireEvent.blur(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      );

      expect(
        await screen.findByText('FINEOS_COMMON.VALIDATION.INVALID_PRECISION')
      ).toBeInTheDocument();

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        ),
        '10.000001'
      );
      fireEvent.blur(
        await screen.getByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      );

      expect(
        await screen.findByText('FINEOS_COMMON.VALIDATION.INVALID_PRECISION')
      ).toBeInTheDocument();

      userEvent.type(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'),
        '10.000,.01'
      );
      fireEvent.blur(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS')
      );

      expect(
        await screen.findByText('FINEOS_COMMON.VALIDATION.INVALID_PRECISION')
      ).toBeInTheDocument();
    });

    test('should display not numeric validation message', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.ADD_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        ),
        '100.'
      );
      fireEvent.blur(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.EARNINGS'
        )
      );

      expect(
        await screen.findByText('FINEOS_COMMON.VALIDATION.NOT_NUMERIC')
      ).toBeInTheDocument();
    });
  });

  describe('Job title validation', () => {
    test('should show job title input as valid', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await act(async () => {
        userEvent.type(
          await screen.findByLabelText(
            'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
          ),
          'President of Sales of the Northern District of Southern Germany'
        );
        return wait();
      });

      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
        )
      ).toBeValid();
    });

    test.skip('should show job title input as invalid and show validation message', async () => {
      render(
        <ContextWrapper
          permissions={[
            ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS,
            ManageOccupationPermissions.EDIT_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
          ]}
        >
          <EditEmploymentDetailsWrapper />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await act(async () => {
        userEvent.type(
          await screen.findByLabelText(
            'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*'
          ),
          'President of Sales of the Northern District of Southern Germany for the Protection of Wild Crocodiles'
        );
        return wait();
      });

      expect(
        screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*')
      ).not.toBeValid();
    });
  });

  describe('Job start date validation', () => {
    const elementWithoutPermissions = (
      <ContextWrapper>
        <EditEmploymentDetailsWrapper />
      </ContextWrapper>
    );

    test('should make start date input invalid and show validation message', async () => {
      render(elementWithoutPermissions);

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      const startDate = moment()
        .add(1, 'days')
        .startOf('day')
        .format('YYYY-MM-DD');
      await selectDate(
        await screen.findByTestId('job-start-date-picker'),
        startDate
      );

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_START_DATE*/
        )
      ).not.toBeValid();

      expect(
        await screen.findByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.START_DATE_VALIDATION'
        )
      ).toBeInTheDocument();
    });

    test('should show start date input as valid when selecting today', async () => {
      render(elementWithoutPermissions);

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await selectTodayDate(await screen.findByTestId('job-start-date-picker'));

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_START_DATE*/
        )
      ).toBeValid();
    });

    test('should show start date input as valid when selecting day in the past', async () => {
      render(elementWithoutPermissions);

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      const startDate = moment()
        .subtract(1, 'days')
        .startOf('day')
        .format('YYYY-MM-DD');
      await selectDate(
        await screen.findByTestId('job-start-date-picker'),
        startDate
      );

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_START_DATE*/
        )
      ).toBeValid();
    });
  });

  describe('Job end date validation', () => {
    const elementWithoutPermissions = (
      <ContextWrapper
        permissions={[ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS]}
      >
        <EditEmploymentDetailsWrapper />
      </ContextWrapper>
    );

    test('should make end date input invalid and show validation message', async () => {
      render(elementWithoutPermissions);

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      const endDate = moment()
        .subtract(1, 'days')
        .startOf('day')
        .format('YYYY-MM-DD');
      await selectDate(
        await screen.findByTestId('job-end-date-picker'),
        endDate
      );

      userEvent.click(await screen.findByText('FINEOS_COMMON.GENERAL.OK'));

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_END_DATE/
        )
      ).not.toBeValid();

      expect(
        await screen.findByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.END_DATE_VALIDATION'
        )
      ).toBeInTheDocument();
    });

    test('should show end date input as invalid when selecting today', async () => {
      render(elementWithoutPermissions);

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      await selectTodayDate(await screen.findByTestId('job-end-date-picker'));

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_END_DATE/
        )
      ).not.toBeValid();

      expect(
        await screen.findByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.END_DATE_VALIDATION'
        )
      ).toBeInTheDocument();
    });

    test('should show end date input as valid when selecting day in the future', async () => {
      render(elementWithoutPermissions);

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
        )
      );

      const endDate = moment()
        .add(1, 'days')
        .startOf('day')
        .format('YYYY-MM-DD');

      await selectDate(
        await screen.findByTestId('job-end-date-picker'),
        endDate
      );

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_END_DATE/
        )
      ).toBeValid();
    });
  });
});
