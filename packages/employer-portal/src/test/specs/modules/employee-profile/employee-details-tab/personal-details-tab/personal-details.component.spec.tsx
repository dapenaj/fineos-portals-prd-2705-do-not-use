/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, screen, wait, act, within } from '@testing-library/react';
import { ManageCustomerDataPermissions } from 'fineos-common';
import userEvent from '@testing-library/user-event';

import { PersonalDetails } from '../../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/personal-details/personal-details.component';
import { ContextWrapper } from '../../../../../common/utils';
import { CUSTOMER_INFO_FIXTURE } from '../../../../../fixtures/customer.fixture';

describe('PersonalDetails', () => {
  const permissions = [
    ManageCustomerDataPermissions.UPDATE_CUSTOMER_INFORMATION
  ];

  test('should render', async () => {
    render(
      <ContextWrapper>
        <PersonalDetails
          customer={CUSTOMER_INFO_FIXTURE}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(
        `${CUSTOMER_INFO_FIXTURE.firstName} ${CUSTOMER_INFO_FIXTURE.lastName}`
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        `${CUSTOMER_INFO_FIXTURE.address.addressLine4}, ${CUSTOMER_INFO_FIXTURE.address.addressLine6}`
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(CUSTOMER_INFO_FIXTURE.address.addressLine1)
    ).toBeInTheDocument();
    expect(
      screen.getByText(CUSTOMER_INFO_FIXTURE.address.addressLine2)
    ).toBeInTheDocument();
    expect(
      screen.getByText(CUSTOMER_INFO_FIXTURE.address.addressLine3)
    ).toBeInTheDocument();
    expect(screen.getByText('30-01-1990')).toBeInTheDocument();
  });

  test('should be able to open and close form modal', async () => {
    render(
      <ContextWrapper permissions={permissions}>
        <PersonalDetails
          customer={CUSTOMER_INFO_FIXTURE}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      within(screen.getByTestId('edit-personal-details-button')).getByText(
        'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS'
      )
    );

    await act(wait);

    expect(
      within(screen.getByTestId('edit-personal-details-modal')).getByText(
        'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS'
      )
    ).toBeInTheDocument();

    userEvent.click(
      screen.getByText('FINEOS_COMMON.ICON_LABEL.MODAL_CLOSE_ICON_LABEL')
    );

    expect(
      within(screen.getByTestId('edit-personal-details-button')).getByText(
        'PROFILE.PERSONAL_DETAILS_TAB.EDIT_PERSONAL_DETAILS'
      )
    ).toBeInTheDocument();
  });
});
