/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Formik } from 'formik';
import { render, act, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BaseDomain } from 'fineos-js-api-client';

import { ContextWrapper } from '../../../../../../common/utils';
import { screen } from '../../../../../../common/custom-render';
import { initialNonEstablishedOccupationAndEarningsFormValue } from '../../../../../../../app/modules/intake/non-established-intake/occupation-and-earnings';
import { FixedPattern } from '../../../../../../../app/shared/work-pattern';

describe('FixedPattern', () => {
  const dayLength = {
    hours: 7,
    minutes: 30
  };

  test('render', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={initialNonEstablishedOccupationAndEarningsFormValue(
            [],
            dayLength
          )}
          onSubmit={data => {
            jest.fn();
          }}
        >
          <FixedPattern
            nameScope="fixedPattern"
            weekDayNamesTypeDomains={[] as BaseDomain[]}
          />
        </Formik>
      </ContextWrapper>
    );

    expect(
      screen.getByText('WORK_PATTERN.NON_STANDARD_WORKING_WEEK')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.MONDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.TUESDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.WEDNESDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.THURSDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.FRIDAY')
    ).toBeInTheDocument();
  });

  test('render weekend', async () => {
    const domains = [
      {
        name: 'Monday',
        fullId: 5408001,
        domainId: 169,
        domainName: 'General DayOfWeek'
      },
      {
        name: 'Tuesday',
        fullId: 5408002,
        domainId: 169,
        domainName: 'General DayOfWeek'
      },
      {
        name: 'Wednesday',
        fullId: 5408003,
        domainId: 169,
        domainName: 'General DayOfWeek'
      },
      {
        name: 'Thursday',
        fullId: 5408004,
        domainId: 169,
        domainName: 'General DayOfWeek'
      },
      {
        name: 'Friday',
        fullId: 5408005,
        domainId: 169,
        domainName: 'General DayOfWeek'
      },
      {
        name: 'Saturday',
        fullId: 5408005,
        domainId: 169,
        domainName: 'General DayOfWeek'
      },
      {
        name: 'Sunday',
        fullId: 5408005,
        domainId: 169,
        domainName: 'General DayOfWeek'
      }
    ];
    render(
      <ContextWrapper>
        <Formik
          initialValues={initialNonEstablishedOccupationAndEarningsFormValue(
            domains as BaseDomain[],
            dayLength
          )}
          onSubmit={data => {
            jest.fn();
          }}
        >
          <FixedPattern
            nameScope="fixedPattern"
            weekDayNamesTypeDomains={domains as BaseDomain[]}
          />
        </Formik>
      </ContextWrapper>
    );

    expect(
      screen.getByText('WORK_PATTERN.NON_STANDARD_WORKING_WEEK')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.SATURDAY')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.SUNDAY')
    ).not.toBeInTheDocument();

    await act(() => {
      userEvent.click(screen.getByTestId('non-standard-working-week-switch'));

      return wait();
    });

    expect(screen.getByTestId('include-weekend-switch')).toBeInTheDocument();

    await act(() => {
      userEvent.click(screen.getByTestId('include-weekend-switch'));

      return wait();
    });

    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.SATURDAY')
    ).toBeInTheDocument();
    expect(
      screen.getByText('WORK_PATTERN.WEEK_DAYS_SHORTCUT.SUNDAY')
    ).toBeInTheDocument();
  });
});
