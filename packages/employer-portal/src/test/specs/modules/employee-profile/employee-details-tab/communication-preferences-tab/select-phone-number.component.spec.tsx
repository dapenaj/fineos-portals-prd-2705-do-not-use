import React from 'react';
import userEvent from '@testing-library/user-event';
import { Formik } from 'formik';
import { ManageCustomerDataPermissions } from 'fineos-common';

import { render, wait, act, screen } from '../../../../../common/custom-render';
import { ContextWrapper } from '../../../../../common/utils';
import { PHONES_FIXTURE } from '../../../../../fixtures/contact-details.fixture';
import { CommunicationPreferencesContext } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.context';
import { SelectPhoneNumber } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/select-phone-number/select-phone-number.component';
import { PhoneType } from '../../../../../../app/shared';

describe('SelectPhoneNumber', () => {
  const mockContextValue = {
    phoneNumbers: PHONES_FIXTURE
  };
  const mockFormikProps = {
    onSubmit: jest.fn(),
    initialValues: {
      selectedPhoneNumberIndex: 0
    }
  };
  test('should render', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber onPhoneNumberPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.NUMBER_LABEL'
      )
    ).toBeInTheDocument();
  });

  test('should render phone numbers', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber onPhoneNumberPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getAllByRole('radio')).toHaveLength(PHONES_FIXTURE.length);
  });

  test('should render filtered phone numbers', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber
              onPhoneNumberPass={jest.fn()}
              allowedPhoneTypeNames={[PhoneType.MOBILE]}
            />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getAllByRole('radio')).toHaveLength(1);
  });

  test('should display button with permissions', async () => {
    const { rerender } = render(
      <ContextWrapper
        permissions={[ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS]}
      >
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber onPhoneNumberPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByRole('button', {
        name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON/
      })
    ).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber onPhoneNumberPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.queryByRole('button', {
        name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON/
      })
    ).not.toBeInTheDocument();
  });

  test('should open add-phone-number-modal', async () => {
    render(
      <ContextWrapper
        permissions={[ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS]}
      >
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber onPhoneNumberPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByRole('button', {
        name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON/
      })
    );

    await act(wait);

    expect(screen.getByTestId('add-phone-number-modal')).toBeInTheDocument();
  });

  // TODO: Fix failing test (FEP-1819)
  xtest('should auto-scroll', async () => {
    const mockScrollIntoView = jest.fn();
    window.HTMLElement.prototype.scrollIntoView = mockScrollIntoView;
    const { getSubmitButtonByText } = render(
      <ContextWrapper
        permissions={[ManageCustomerDataPermissions.ADD_CUSTOMER_PHONE_NUMBERS]}
      >
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <Formik {...mockFormikProps}>
            <SelectPhoneNumber onPhoneNumberPass={jest.fn()} />
          </Formik>
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    act(() => {
      userEvent.click(
        screen.getByRole('button', {
          name: /PROFILE.COMMUNICATION_PREFERENCES_TAB.PHONE_MODAL.DIFFERENT_NUMBER_BUTTON/
        })
      );
    });

    userEvent.type(screen.getAllByRole('textbox')[0], '111');
    userEvent.type(screen.getAllByRole('textbox')[1], '222');
    userEvent.type(screen.getAllByRole('textbox')[2], '333');
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    expect(mockScrollIntoView).toHaveBeenCalled();
  });
});
