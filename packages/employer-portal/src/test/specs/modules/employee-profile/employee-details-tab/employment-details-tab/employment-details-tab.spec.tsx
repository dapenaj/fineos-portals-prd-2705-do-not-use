import React from 'react';
import { render, screen, act, wait, within } from '@testing-library/react';
import {
  ViewCustomerDataPermissions,
  ViewOccupationPermissions,
  Tabs
} from 'fineos-common';
import { WeekBasedWorkPattern } from 'fineos-js-api-client';

import { ContextWrapper } from '../../../../../common/utils';
import { EmploymentDetailsTab } from '../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/employment-details-tab.component';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../../fixtures/customer-occupations.fixture';
import * as employmentDetailsTabApi from '../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/employment-details-tab.api';
import * as workPatternApiModule from '../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/work-pattern/work-pattern.api';
import { WORK_PATTERNS } from '../../../../../fixtures/work-pattern.fixture';

describe('EmploymentDetailsTab', () => {
  const mockEmployeeId = 'id-111';

  test('should render with all elements', async () => {
    render(
      <ContextWrapper
        permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
      >
        <Tabs>
          <EmploymentDetailsTab
            employeeId={mockEmployeeId}
            isOccupationPending={false}
            occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
            onOccupationChange={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    const propertyItem = screen.getByTestId('property-item');

    expect(
      screen.getByText('PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE')
    ).toBeInTheDocument();
    expect(
      within(propertyItem).getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EMPLOYEE_ID'
      )
    ).toBeInTheDocument();
    expect(within(propertyItem).getByText(mockEmployeeId)).toBeInTheDocument();
  });

  test('should not fetch absenceEmployment without permissions', async () => {
    const spy = spyOn(
      employmentDetailsTabApi,
      'fetchAbsenceEmployment'
    ).and.returnValue(Promise.resolve());

    render(
      <ContextWrapper>
        <Tabs>
          <EmploymentDetailsTab
            employeeId={mockEmployeeId}
            isOccupationPending={false}
            occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
            onOccupationChange={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(spy).not.toHaveBeenCalled();
  });

  test('should fetch absenceEmployment with permissions', async () => {
    const spy = spyOn(
      employmentDetailsTabApi,
      'fetchAbsenceEmployment'
    ).and.returnValue(Promise.resolve());

    render(
      <ContextWrapper
        permissions={[ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT]}
      >
        <Tabs>
          <EmploymentDetailsTab
            employeeId={mockEmployeeId}
            isOccupationPending={false}
            occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
            onOccupationChange={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(spy).toHaveBeenCalled();
  });

  test('should render eligibility criteria section', () => {
    render(
      <ContextWrapper
        permissions={[ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT]}
      >
        <Tabs>
          <EmploymentDetailsTab
            employeeId={mockEmployeeId}
            isOccupationPending={false}
            occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
            onOccupationChange={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.TITLE'
      )
    ).toBeInTheDocument();
  });

  test('should not render eligibility criteria section without permissions', () => {
    render(
      <ContextWrapper>
        <Tabs>
          <EmploymentDetailsTab
            employeeId={mockEmployeeId}
            isOccupationPending={false}
            occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
            onOccupationChange={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.TITLE'
      )
    ).not.toBeInTheDocument();
  });

  test('should render work pattern', async () => {
    jest
      .spyOn(workPatternApiModule, 'fetchWorkPattern')
      .mockReturnValue(
        Promise.resolve(
          Promise.resolve(WORK_PATTERNS[3] as WeekBasedWorkPattern)
        )
      );
    render(
      <ContextWrapper>
        <Tabs>
          <EmploymentDetailsTab
            employeeId={mockEmployeeId}
            isOccupationPending={false}
            occupation={{} as any}
            onOccupationChange={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.queryByText('WORK_PATTERN.HEADER')).not.toBeInTheDocument();
  });
});
