import React from 'react';
import userEvent from '@testing-library/user-event';
import {
  ConcurrencyUpdateError,
  CustomerAbsenceEmployment,
  GroupClientCustomerService
} from 'fineos-js-api-client';

import {
  render,
  act,
  wait,
  screen,
  fireEvent
} from '../../../../../../common/custom-render';
import {
  ContextWrapper,
  checkIsConcurrencyModalVisible
} from '../../../../../../common/utils';
import { EditEligibilityCriteria } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/eligibility-criteria/edit-eligibility-criteria/edit-eligibility-criteria.component';
import * as employeeApiModule from '../../../../../../../app/modules/employee-profile/employee-profile.api';
import { ABSENCE_EMPLOYMENT_FIXTURE } from '../../../../../../fixtures/absence-employment.fixture';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../../../fixtures/customer-occupations.fixture';

describe('EditEligibilityCriteriaComponent', () => {
  const service = GroupClientCustomerService.getInstance();

  test('should render elements', async () => {
    render(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    );

    // labels
    expect(
      await screen.findByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WITHIN_FMLA_RADIUS'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORKS_AT_HOME'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.KEY_EMPLOYEE'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORK_STATE')
    ).toBeInTheDocument();
    expect(
      await screen.findByText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER')
    ).toBeInTheDocument();
    expect(2).toBe(2);

    // inputs
    expect(
      await screen.findByLabelText(
        /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WITHIN_FMLA_RADIUS/
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORKS_AT_HOME'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.KEY_EMPLOYEE'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORK_STATE'
      )
    ).toBeInTheDocument();
    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
      )
    ).toBeInTheDocument();
  });

  test('should validate hours worked per year', async () => {
    const invalidHoursValue = 366 * 24;
    const invalidTypeValue = 'string value';
    const { rerender } = render(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    );

    userEvent.type(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      ),
      invalidHoursValue.toString()
    );
    fireEvent.blur(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      )
    );

    await act(wait);

    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      )
    ).not.toBeValid();
    expect(
      screen.getByText(
        'FINEOS_COMMON.VALIDATION.MAX_WORKING_HOURS_PER_YEAR_AMOUNT'
      )
    ).toBeInTheDocument();

    rerender(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.type(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      ),
      invalidTypeValue
    );
    fireEvent.blur(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      )
    );

    await act(wait);

    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      )
    ).not.toBeValid();
    expect(
      screen.getByText('FINEOS_COMMON.VALIDATION.NOT_NUMERIC')
    ).toBeInTheDocument();
  });

  test('should validate cbaNumber', async () => {
    const invalidCBAlengthText = 'test'.repeat(10);
    render(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    );

    userEvent.type(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
      ),
      invalidCBAlengthText
    );
    fireEvent.blur(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
      )
    );

    await act(wait);

    expect(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
      )
    ).not.toBeValid();
    expect(
      screen.getByText('FINEOS_COMMON.VALIDATION.MAX_CBA_LENGTH')
    ).toBeInTheDocument();
  });

  test('should correctly add', async () => {
    jest
      .spyOn(employeeApiModule, 'fetchMostRecentCustomerOccupation')
      .mockResolvedValueOnce(CUSTOMER_OCCUPATIONS_FIXTURE[0]);

    const mockHoursPerYear = 1000;
    const mockCBANumber = 'test';
    const mockEmployeeId = 'testId';
    const expectedCalledWith = {
      keyEmployee: false,
      withinFMLACriteria: false,
      workingAtHome: false,
      cbaValue: mockCBANumber,
      employeeWorkState: undefined,
      hoursWorkedPerYear: mockHoursPerYear,
      employmentClassification: undefined,
      employmentType: undefined,
      employmentWorkState: {
        fullId: 4416000,
        id: '4416000',
        name: 'Unknown'
      }
    };
    const addAbsenceSpy = spyOn(service, 'addCustomerAbsenceEmployment');

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    );

    userEvent.type(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
      ),
      mockHoursPerYear.toString()
    );

    userEvent.type(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
      ),
      mockCBANumber
    );

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await act(wait);

    expect(addAbsenceSpy).toHaveBeenCalledWith(
      mockEmployeeId,
      CUSTOMER_OCCUPATIONS_FIXTURE[0].id,
      expectedCalledWith
    );
  });

  test('should correctly edit', async () => {
    const mockAbsenceEmployment = {
      ...ABSENCE_EMPLOYMENT_FIXTURE[0],
      adjustedHireDate: '2020-08-08'
    } as any;
    const mockEmployeeId = 'testId';
    const mockCBA = 'mock CBA number';
    const expectedCalledWith = {
      keyEmployee: mockAbsenceEmployment.keyEmployee,
      withinFMLACriteria: mockAbsenceEmployment.withinFMLACriteria,
      workingAtHome: mockAbsenceEmployment.workingAtHome,
      hoursWorkedPerYear: mockAbsenceEmployment.hoursWorkedPerYear,
      employmentWorkState: mockAbsenceEmployment.employmentWorkState,
      employmentType: {
        domainId: 6772,
        fullId: 216704000,
        name: 'Please Select'
      },
      employmentClassification: {
        fullId: 216640000,
        name: 'Please Select'
      },
      cbaValue: mockCBA,
      adjustedHireDate: '2020-08-08'
    };
    const editAbsenceSpy = spyOn(service, 'editCustomerAbsenceEmployment');
    jest
      .spyOn(employeeApiModule, 'fetchMostRecentCustomerOccupation')
      .mockResolvedValueOnce(CUSTOMER_OCCUPATIONS_FIXTURE[0]);
    jest
      .spyOn(service, 'fetchCustomerAbsenceEmployment')
      .mockResolvedValue(mockAbsenceEmployment);

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    );

    await act(wait);

    userEvent.type(
      await screen.findByLabelText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
      ),
      mockCBA
    );
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await act(wait);

    expect(editAbsenceSpy).toHaveBeenCalledWith(
      mockEmployeeId,
      CUSTOMER_OCCUPATIONS_FIXTURE[0].id,
      { ...expectedCalledWith, id: '14453-14' }
    );
  });

  test('should not call request when no changes in the form', async () => {
    jest
      .spyOn(employeeApiModule, 'fetchMostRecentCustomerOccupation')
      .mockResolvedValueOnce(CUSTOMER_OCCUPATIONS_FIXTURE[0]);
    jest
      .spyOn(service, 'fetchCustomerAbsenceEmployment')
      .mockResolvedValue(ABSENCE_EMPLOYMENT_FIXTURE[0]);

    const editAbsenceSpy = spyOn(service, 'editCustomerAbsenceEmployment');
    const addAbsenceSpy = spyOn(service, 'addCustomerAbsenceEmployment');

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditEligibilityCriteria
          titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
          buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
          buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
          employeeId="testId"
        />
      </ContextWrapper>
    );

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
      )
    );

    await act(wait);

    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await act(wait);

    expect(editAbsenceSpy).not.toBeCalled();
    expect(addAbsenceSpy).not.toBeCalled();
  });

  describe('Concurrency', () => {
    beforeEach(() => {
      jest
        .spyOn(employeeApiModule, 'fetchMostRecentCustomerOccupation')
        .mockResolvedValueOnce(CUSTOMER_OCCUPATIONS_FIXTURE[0]);
      jest
        .spyOn(service, 'fetchCustomerAbsenceEmployment')
        .mockResolvedValueOnce(ABSENCE_EMPLOYMENT_FIXTURE[0]);
    });

    test('should refresh data in form after concurrency', async () => {
      const mockConcurrencyErrorData = {
        withinFMLACriteria: true,
        keyEmployee: true,
        workingAtHome: true,
        hoursWorkedPerYear: 1234,
        cbaValue: 'refreshed value'
      } as CustomerAbsenceEmployment;
      jest
        .spyOn(service, 'editCustomerAbsenceEmployment')
        .mockRejectedValueOnce(
          new ConcurrencyUpdateError(mockConcurrencyErrorData)
        );

      const { getSubmitButtonByText } = render(
        <ContextWrapper>
          <EditEligibilityCriteria
            titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
            buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
            buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
            employeeId="testId"
          />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
        )
      );

      await act(wait);

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
        ),
        'edited CBA number'
      );

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await checkIsConcurrencyModalVisible();

      userEvent.click(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
        )
      );

      await act(wait);

      expect(
        await screen.findByLabelText(
          /PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WITHIN_FMLA_RADIUS/
        )
      ).toBeChecked();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.WORKS_AT_HOME'
        )
      ).toBeChecked();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.KEY_EMPLOYEE'
        )
      ).toBeChecked();
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.HOURS_PER_YEAR'
        )
      ).toHaveValue(mockConcurrencyErrorData.hoursWorkedPerYear.toString());
      expect(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
        )
      ).toHaveValue(mockConcurrencyErrorData.cbaValue);
    });

    test('should display error response modal and no concurrency modal when error other than concurrency occurred', async () => {
      jest
        .spyOn(service, 'editCustomerAbsenceEmployment')
        .mockRejectedValueOnce({ status: '404' });

      const { getSubmitButtonByText } = render(
        <ContextWrapper>
          <EditEligibilityCriteria
            titleId="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.ELIGIBILITY_TITLE_ADD"
            buttonId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON"
            buttonAriaLabelId="PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON_ARIA_LABEL"
            employeeId="testId"
          />
        </ContextWrapper>
      );

      userEvent.click(
        screen.getByText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.ELIGIBILITY_CRITERIA_CARD.ADD_BUTTON'
        )
      );

      await act(wait);

      userEvent.type(
        await screen.findByLabelText(
          'PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.CBA_NUMBER'
        ),
        'edited CBA number'
      );

      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await act(wait);

      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).not.toBeInTheDocument();

      expect(
        screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
      ).toBeInTheDocument();
    });
  });
});
