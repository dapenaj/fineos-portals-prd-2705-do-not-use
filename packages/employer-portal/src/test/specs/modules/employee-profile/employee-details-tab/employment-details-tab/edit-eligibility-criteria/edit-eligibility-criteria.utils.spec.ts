/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { CustomerAbsenceEmployment } from 'fineos-js-api-client';

import { EditEligibilityCriteriaForm } from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/eligibility-criteria/edit-eligibility-criteria/edit-eligibility-criteria.type';
import {
  filteredCheckboxValues,
  isAbsenceEmploymentNotChanged,
  parsedEligibilityCriteria
} from '../../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/eligibility-criteria/edit-eligibility-criteria/edit-eligibility-criteria.utils';
import { ABSENCE_EMPLOYMENT_FIXTURE } from '../../../../../../fixtures/absence-employment.fixture';

describe('EditEligibilityCriteriaUtils', () => {
  describe('filteredCheckboxValues', () => {
    test('should return filtered CHECKBOX_FIELDS array', () => {
      const expectedArray = ['withinFMLACriteria', 'workingAtHome'];
      const mockAbsenceEmployment = {
        withinFMLACriteria: true,
        workingAtHome: true,
        keyEmployee: false
      } as CustomerAbsenceEmployment;

      expect(filteredCheckboxValues(mockAbsenceEmployment)).toEqual(
        expectedArray
      );
    });

    test('should return empty array when all boolean values are false', () => {
      const expectedArray: string[] = [];
      const mockAbsenceEmployment = {
        withinFMLACriteria: false,
        workingAtHome: false,
        keyEmployee: false
      } as CustomerAbsenceEmployment;

      expect(filteredCheckboxValues(mockAbsenceEmployment)).toEqual(
        expectedArray
      );
    });
  });

  describe('isAbsenceEmploymentNotChanged', () => {
    test('should return true if absence is not changed', () => {
      const mockEdited = {
        checkboxValues: ['withinFMLACriteria', 'workingAtHome'],
        employmentWorkState: {
          name: 'NY'
        },
        hoursWorkedPerYear: 1000,
        cbaValue: 'test'
      } as EditEligibilityCriteriaForm;
      const mockFetched = {
        withinFMLACriteria: true,
        workingAtHome: true,
        keyEmployee: false,
        employmentWorkState: {
          name: 'NY'
        },
        hoursWorkedPerYear: 1000,
        cbaValue: 'test'
      } as CustomerAbsenceEmployment;

      expect(isAbsenceEmploymentNotChanged(mockEdited, mockFetched)).toBe(true);
    });

    test('should return false if absence is changed', () => {
      const mockEdited = {
        checkboxValues: ['withinFMLACriteria'],
        employmentWorkState: {
          name: 'NY'
        },
        hoursWorkedPerYear: 1000,
        cbaValue: 'test'
      } as EditEligibilityCriteriaForm;
      const mockFetched = {
        withinFMLACriteria: true,
        workingAtHome: true,
        keyEmployee: false,
        employmentWorkState: {
          name: 'NY'
        },
        hoursWorkedPerYear: 1000,
        cbaValue: 'test'
      } as CustomerAbsenceEmployment;

      expect(isAbsenceEmploymentNotChanged(mockEdited, mockFetched)).toBe(
        false
      );

      mockFetched.workingAtHome = false;
      mockFetched.cbaValue = 'different value';

      expect(isAbsenceEmploymentNotChanged(mockEdited, mockFetched)).toBe(
        false
      );
    });
  });

  describe('parsedEligibilityCriteria', () => {
    const mockEditedAbsenceEmployment = {
      checkboxValues: ['withinFMLACriteria', 'workingAtHome'],
      cbaValue: 'test',
      hoursWorkedPerYear: 1000,
      employmentWorkState: ABSENCE_EMPLOYMENT_FIXTURE[0].employmentWorkState
    } as any;
    const mockFetchedAbsenceEmployment = {
      id: 'test',
      adjustedHireDate: '2020-08-08',
      employmentClassification:
        ABSENCE_EMPLOYMENT_FIXTURE[0].employmentClassification,
      employmentType: ABSENCE_EMPLOYMENT_FIXTURE[0].employmentType
    } as any;
    test('should parse data for added eligibility criteria with non empty absence employment', () => {
      const expected = {
        id: 'test',
        adjustedHireDate: '2020-08-08',
        employmentClassification: {
          fullId: 216640000,
          name: 'Please Select'
        },
        employmentType: {
          domainId: 6772,
          fullId: 216704000,
          name: 'Please Select'
        },
        withinFMLACriteria: true,
        workingAtHome: true,
        keyEmployee: false,
        cbaValue: 'test',
        hoursWorkedPerYear: 1000,
        employmentWorkState: {
          name: 'NY',
          domainId: 138,
          fullId: 4416033
        }
      };

      expect(
        parsedEligibilityCriteria(
          mockEditedAbsenceEmployment,
          mockFetchedAbsenceEmployment
        )
      ).toEqual(expected);
    });

    test('should parse data for added eligibility criteria with empty absence employment', () => {
      const expected = {
        adjustedHireDate: undefined,
        cbaValue: 'test',
        employmentClassification: undefined,
        employmentType: undefined,
        employmentWorkState: {
          domainId: 138,
          fullId: 4416033,
          name: 'NY'
        },
        hoursWorkedPerYear: 1000,
        keyEmployee: false,
        withinFMLACriteria: true,
        workingAtHome: true
      };

      expect(
        parsedEligibilityCriteria(mockEditedAbsenceEmployment, null)
      ).toEqual(expected);
    });
  });
});
