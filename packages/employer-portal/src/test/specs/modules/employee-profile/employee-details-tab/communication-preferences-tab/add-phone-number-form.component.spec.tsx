import React from 'react';
import { render } from '@testing-library/react';
import { Formik, Form } from 'formik';

import { ContextWrapper } from '../../../../../common/utils';
import { AddPhoneNumberForm } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/add-phone-number-form/add-phone-number-form.component';
import { screen } from '../../../../../common/custom-render';

describe('AddPhoneNumberForm', () => {
  test('should render', () => {
    render(
      <ContextWrapper>
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <Form>
            <AddPhoneNumberForm />
          </Form>
        </Formik>
      </ContextWrapper>
    );
    expect(screen.getByTestId('add-phone-number-form')).toBeInTheDocument();
  });
});
