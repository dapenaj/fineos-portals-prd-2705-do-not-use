import React from 'react';
import { render } from '@testing-library/react';
import { Formik, Form } from 'formik';

import { screen } from '../../../../../common/custom-render';
import { ContextWrapper } from '../../../../../common/utils';
import { AddEmailForm } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/add-email-form/add-email-form.component';

describe('AddEmailForm', () => {
  test('should render', async () => {
    render(
      <ContextWrapper>
        <Formik onSubmit={jest.fn()} initialValues={{}}>
          <Form>
            <AddEmailForm />
          </Form>
        </Formik>
      </ContextWrapper>
    );
    expect(
      await screen.findByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.EMAIL_MODAL.ADD_EMAIL_LABEL'
      )
    ).toBeInTheDocument();
    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });
});
