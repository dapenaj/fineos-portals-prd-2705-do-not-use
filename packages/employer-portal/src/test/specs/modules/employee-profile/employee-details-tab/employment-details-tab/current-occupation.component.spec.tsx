import React from 'react';
import each from 'jest-each';
import { render, screen, act, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  CustomerAbsenceEmployment,
  CustomerOccupation
} from 'fineos-js-api-client';
import {
  ViewOccupationPermissions,
  ManageOccupationPermissions
} from 'fineos-common';
import * as common from 'fineos-common';

import {
  buildTranslationWithParams,
  ContextWrapper,
  mockTranslations
} from '../../../../../common/utils';
import * as contractualEarningsApi from '../../../../../../app/shared/api/contractual-earnings.api';
import { CurrentOccupation } from '../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/current-occupation/current-occupation.component';
import { getEarningsFrequency } from '../../../../../../app/shared';
import { CONTRACTUAL_EARNINGS_FIXTURE } from '../../../../../fixtures/contractual-earnings.fixture';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../../fixtures/customer-occupations.fixture';
import { ABSENCE_EMPLOYMENT_FIXTURE } from '../../../../../fixtures/absence-employment.fixture';

describe('CurrentOccupation', () => {
  const translations = mockTranslations(
    buildTranslationWithParams(
      'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.WORK_BASIS',
      ['workBasis']
    ),
    buildTranslationWithParams(
      'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPLOYMENT_TYPE',
      ['employmentType']
    ),
    buildTranslationWithParams(
      'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADJUSTED_DATE_OF_HIRE',
      ['adjustedHireDate']
    )
  );
  const {
    jobTitle,
    jobStartDate,
    jobEndDate,
    employmentCat,
    hrsWorkedPerWeek
  } = CUSTOMER_OCCUPATIONS_FIXTURE[0];
  beforeEach(() => {
    jest.spyOn(contractualEarningsApi, 'fetchContractualEarnings');
    jest.spyOn(common, 'useAsync');
  });

  const mockEmployeeId = 'ID-111';
  test('should render fixed component elements', () => {
    render(
      <ContextWrapper
        permissions={[ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT]}
      >
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={{} as any}
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.TITLE'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADD_BUTTON'
      )
    ).toBeInTheDocument();
  });
  each([
    [
      'job title and start date',
      '',
      jobStartDate,
      jobEndDate,
      employmentCat,
      hrsWorkedPerWeek,
      'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.JOB_TITLE_AND_START_DATE'
    ],
    [
      'job title and start date',
      jobTitle,
      '',
      '',
      employmentCat,
      hrsWorkedPerWeek,
      'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.JOB_TITLE_AND_START_DATE'
    ],
    [
      'hours worked per week',
      jobTitle,
      jobStartDate,
      jobEndDate,
      employmentCat,
      '',
      'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.HOURS_PER_WEEK'
    ]
  ]).test(
    `should not display %s, when values: { jobTitle: %s, jobStartDate: %s, jobEndDate: %s, employmentCategory: %s, hrsWorkedPerWeek: %s }`,
    (
      desc,
      employeeJobTitle,
      employeeJobStartDate,
      employeeJobEndDate,
      employeeEmploymentCategory,
      employeeHrsWorkedPerWeek,
      unpresentSection
    ) => {
      render(
        <ContextWrapper>
          <CurrentOccupation
            employeeId={mockEmployeeId}
            occupation={
              {
                jobTitle: employeeJobTitle,
                jobStartDate: employeeJobStartDate,
                jobEndDate: employeeJobEndDate,
                employmentCat: { name: employeeEmploymentCategory.name } as any,
                hrsWorkedPerWeek: employeeHrsWorkedPerWeek
              } as CustomerOccupation
            }
            isOccupationPending={false}
            absenceEmployment={{} as any}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(screen.queryByText(unpresentSection)).not.toBeInTheDocument();
    }
  );

  each([
    [true, false, false],
    [false, true, false],
    [false, false, true],
    [true, true, false],
    [false, true, true],
    [true, false, true],
    [true, true, true]
  ]).test(
    'should correctly display spinner when isOccupationPending is: %s, isContractualEarningsPending is %s and isAbsenceEmploymentPending is: %s.',
    (
      isOccupationPending,
      isContractualEarningsPending,
      isAbsenceEmploymentPending
    ) => {
      (common.useAsync as jest.Mock).mockReturnValueOnce({
        state: {
          value: {} as any,
          isPending: isContractualEarningsPending
        }
      });

      render(
        <ContextWrapper>
          <CurrentOccupation
            employeeId={mockEmployeeId}
            occupation={{} as any}
            isOccupationPending={isOccupationPending}
            absenceEmployment={{} as any}
            isAbsenceEmploymentPending={isAbsenceEmploymentPending}
          />
        </ContextWrapper>
      );

      expect(screen.getByTestId('spinner')).toBeInTheDocument();
    }
  );

  test('should properly display contractual earnings', async () => {
    (contractualEarningsApi.fetchContractualEarnings as jest.Mock).mockResolvedValue(
      CONTRACTUAL_EARNINGS_FIXTURE[0]
    );

    render(
      <ContextWrapper
        permissions={[
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATION_CONTRACTUAL_EARNINGS
        ]}
      >
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0] as CustomerOccupation}
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.CONTRACTUAL_EARNINGS_AMOUNT'
      )
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        `PROFILE.EMPLOYMENT_DETAILS_TAB.EARNINGS_FREQUENCIES.${getEarningsFrequency(
          CONTRACTUAL_EARNINGS_FIXTURE[0].frequency.name
        )}`
      )
    ).toBeInTheDocument();
  });

  test('should correctly render work basis', async () => {
    const { rerender } = render(
      <ContextWrapper translations={translations}>
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={
            {
              ...CUSTOMER_OCCUPATIONS_FIXTURE[0],
              employmentCat: {
                name: 'Unknown'
              }
            } as CustomerOccupation
          }
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByText('-')).toBeInTheDocument();

    rerender(
      <ContextWrapper translations={translations}>
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText('ENUM_DOMAINS.OCCUPATION_CATEGORY.PART_TIME')
    ).toBeInTheDocument();
  });

  each(['Permanent', 'Temporary', 'Seasonal', 'Contract']).test(
    'should correctly display %s employment type',
    employmentTypeName => {
      const mockAbsenceEmployment = {
        ...ABSENCE_EMPLOYMENT_FIXTURE[0],
        employmentType: {
          ...ABSENCE_EMPLOYMENT_FIXTURE[0].employmentType,
          name: employmentTypeName
        }
      };

      render(
        <ContextWrapper
          translations={translations}
          permissions={[ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT]}
        >
          <CurrentOccupation
            employeeId={mockEmployeeId}
            occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
            isOccupationPending={false}
            absenceEmployment={mockAbsenceEmployment}
            isAbsenceEmploymentPending={false}
          />
        </ContextWrapper>
      );

      expect(
        screen.getByText(
          `PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPLOYMENT_TYPE_NAME.${employmentTypeName.toUpperCase()}`
        )
      ).toBeInTheDocument();
    }
  );

  test('should correctly display adjusted date of hire', () => {
    render(
      <ContextWrapper
        translations={translations}
        permissions={[ViewOccupationPermissions.VIEW_ABSENCE_EMPLOYMENT]}
      >
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
          isOccupationPending={false}
          absenceEmployment={ABSENCE_EMPLOYMENT_FIXTURE[0]}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(screen.getByText('08-08-2020')).toBeInTheDocument();
  });

  test('shoult not show information without permissions', () => {
    const mockAbsenceEmployment = {
      ...ABSENCE_EMPLOYMENT_FIXTURE[0],
      employmentType: {
        name: 'Permanent'
      }
    } as CustomerAbsenceEmployment;
    render(
      <ContextWrapper translations={translations}>
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
          isOccupationPending={false}
          absenceEmployment={mockAbsenceEmployment}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADJUSTED_DATE_OF_HIRE'
      )
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPLOYMENT_TYPE_NAME.PERMANENT'
      )
    ).not.toBeInTheDocument();
  });

  test('should open edit form', async () => {
    render(
      <ContextWrapper>
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={CUSTOMER_OCCUPATIONS_FIXTURE[0]}
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EDIT_BUTTON'
      )
    );

    await act(wait);

    expect(screen.getByText('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
    expect(
      screen.getByLabelText('PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE*')
    ).toBeInTheDocument();
  });

  test('should render empty content if no data', () => {
    (contractualEarningsApi.fetchContractualEarnings as jest.Mock).mockResolvedValue(
      {
        frequecy: {} as any,
        amount: {} as any
      }
    );

    render(
      <ContextWrapper
        permissions={[ManageOccupationPermissions.ADD_ABSENCE_EMPLOYMENT]}
      >
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={
            {
              jobTitle: '',
              jobStartDate: '',
              jobEndDate: '',
              employmentCat: { name: 'Unknown', domainId: 145 } as any,
              hrsWorkedPerWeek: ''
            } as any
          }
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.EMPTY_STATE'
      )
    ).toBeInTheDocument();
  });

  test('should not render `add` button when no permissions', () => {
    render(
      <ContextWrapper>
        <CurrentOccupation
          employeeId={mockEmployeeId}
          occupation={{} as any}
          isOccupationPending={false}
          absenceEmployment={{} as any}
          isAbsenceEmploymentPending={false}
        />
      </ContextWrapper>
    );

    expect(
      screen.queryByText(
        'PROFILE.EMPLOYMENT_DETAILS_TAB.CURRENT_OCCUPATION_CARD.ADD_BUTTON'
      )
    ).not.toBeInTheDocument();
  });
});
