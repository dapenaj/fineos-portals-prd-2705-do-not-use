/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Tabs,
  ViewCustomerDataPermissions,
  ViewOccupationPermissions
} from 'fineos-common';

import { render, act, wait, screen } from '../../../../common/custom-render';
import { ContextWrapper } from '../../../../common/utils';
import { EmployeeDetailsTab } from '../../../../../app/modules/employee-profile/employee-details-tab';
import { CUSTOMER_OCCUPATIONS_FIXTURE } from '../../../../fixtures/customer-occupations.fixture';

jest.mock('../../../../../app/shared/enum-domain/enum-instance.api', () => ({
  fetchEnumInstances: jest.fn().mockImplementation(() => Promise.resolve([]))
}));

jest.mock(
  '../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/current-occupation/edit-employment-details/edit-employment-details.api',
  () => ({
    editCustomerEmploymentDetails: jest
      .fn()
      .mockImplementation((customerId, data) => Promise.resolve(data))
  })
);

describe('EmployeeDetailsTab', () => {
  const occupation = CUSTOMER_OCCUPATIONS_FIXTURE[1];

  test('should render the personal details tab', async () => {
    render(
      <ContextWrapper
        permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
      >
        <Tabs>
          <EmployeeDetailsTab
            employeeId="1"
            key="test"
            tab="test"
            isOccupationPending={false}
            occupation={occupation}
            onOccupationChange={jest.fn()}
            onCustomerUpdate={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);
    expect(
      screen.queryAllByText('PROFILE.PERSONAL_DETAILS_TAB.TITLE')
    ).toHaveLength(2);
    expect(
      screen.queryByText('PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE')
    ).not.toBeInTheDocument();
    expect(screen.queryByText('WORK_PATTERN.EMPTY')).not.toBeInTheDocument();
  });

  test('should render the employment details tab', async () => {
    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS
        ]}
      >
        <Tabs>
          <EmployeeDetailsTab
            employeeId="1"
            key="test"
            tab="test"
            occupation={occupation}
            onOccupationChange={jest.fn()}
            isOccupationPending={false}
            onCustomerUpdate={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.TITLE')
    ).toHaveLength(2);
    expect(
      screen.getByText('PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.queryByText('PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE')
    ).not.toBeInTheDocument();
  });

  test('should render the communication preferences tab', async () => {
    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ViewOccupationPermissions.VIEW_CUSTOMER_OCCUPATIONS,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_COMMUNICATION_PREFERENCES
        ]}
      >
        <Tabs>
          <EmployeeDetailsTab
            employeeId="1"
            key="test"
            tab="test"
            occupation={occupation}
            onOccupationChange={jest.fn()}
            isOccupationPending={false}
            onCustomerUpdate={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getAllByText('PROFILE.PERSONAL_DETAILS_TAB.TITLE')
    ).toHaveLength(2);
    expect(
      screen.getByText('PROFILE.EMPLOYMENT_DETAILS_TAB.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE')
    ).toBeInTheDocument();
  });
});
