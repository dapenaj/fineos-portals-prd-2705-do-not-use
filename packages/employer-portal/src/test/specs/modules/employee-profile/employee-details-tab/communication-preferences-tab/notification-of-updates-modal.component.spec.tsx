import React from 'react';
import userEvent from '@testing-library/user-event';

import {
  render,
  act,
  wait,
  waitForElementToBeRemoved,
  screen
} from '../../../../../common/custom-render';
import { ContextWrapper } from '../../../../../common/utils';
import { CommunicationPreferencesContext } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.context';
import { NotificationOfUpdatesModal } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/notification-of-updates/notification-of-updates-modal/notification-of-updates-modal.component';
import {
  EMPTY_CONTEXT_VALUES_FIXTURE,
  CONTEXT_VALUES_FIXTURE
} from '../../../../../fixtures/communication-preferences.fixture';
import {
  PHONES_FIXTURE,
  EMAILS_FIXTURE
} from '../../../../../fixtures/contact-details.fixture';
import { PortalEnvConfigService } from '../../../../../../app/shared';
import portalEnvConfig from '../../../../../../../public/config/portal.env.json';

describe('NotificationsOfUpdatesModal', () => {
  beforeEach(() => {
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValueOnce(Promise.resolve(portalEnvConfig));
  });
  const componentText = {
    mainTypeQuestion:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.NOTIFICATION_TYPE_QUESTION',
    sendEmailLabel:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_EMAIL',
    sendSmsLabel:
      'PROFILE.COMMUNICATION_PREFERENCES_TAB.NOTIFICATIONS_AND_UPDATES_MODAL.SEND_SMS'
  };

  test('should render', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(componentText.mainTypeQuestion)
    ).toBeInTheDocument();
    expect(screen.getByText(componentText.sendEmailLabel)).toBeInTheDocument();
    expect(screen.getByText(componentText.sendSmsLabel)).toBeInTheDocument();
    expect(screen.getByText('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
  });

  test('should show forms when no phoneNumbers/emailAddresses', async () => {
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(screen.getByLabelText(componentText.sendEmailLabel));
    await act(wait);
    expect(screen.getByTestId('add-email-form')).toBeInTheDocument();

    userEvent.click(screen.getByLabelText(componentText.sendSmsLabel));
    await act(wait);
    expect(screen.getByTestId('add-phone-number-form')).toBeInTheDocument();
  });

  // I am skipping this test for now, because it keeps failing randomly,
  // I suspect its related to async actions and timeouts TODO fix it
  xtest('should validate selectable list', async () => {
    const mockContextValue = {
      ...EMPTY_CONTEXT_VALUES_FIXTURE,
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    };
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.sendEmailLabel));
      userEvent.click(screen.getByLabelText(componentText.sendSmsLabel));
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    expect(
      await screen.findAllByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.WRITTEN_CORRESPONDENCE.VALIDATION.PHONE'
      )
    ).toHaveLength(1);
    expect(
      await screen.findAllByText(
        'FINEOS_COMMON.VALIDATION.REQUIRED_AT_LEAST_ONE_OPTION'
      )
    ).toHaveLength(1);
  });

  xtest('should validate', async () => {
    const validationText = 'FINEOS_COMMON.VALIDATION.REQUIRED';
    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.sendEmailLabel));
      return wait();
    });

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    await act(() => {
      userEvent.type(screen.getAllByRole('textbox')[0], 'aa@aa.com');
      return wait();
    });

    expect(screen.queryByText(validationText)).not.toBeInTheDocument();

    await act(() => {
      userEvent.click(screen.getByLabelText(componentText.sendSmsLabel));
      return wait();
    });

    expect(screen.getByTestId('phone-type-dropdown')).toHaveClass(
      'ant-select-disabled'
    );

    await act(() => {
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));
      return wait();
    });

    userEvent.type(screen.getAllByRole('textbox')[1], '111');
    userEvent.type(screen.getAllByRole('textbox')[2], '111');
    userEvent.type(screen.getAllByRole('textbox')[3], '111');

    await act(wait);
    expect(
      await waitForElementToBeRemoved(() =>
        screen.queryAllByText(validationText)
      )
    );
  });

  test('should not preselect communication options if preferences are choosen only in other context', async () => {
    const mockContextValue = {
      ...CONTEXT_VALUES_FIXTURE,
      communicationPreferences: {
        ...CONTEXT_VALUES_FIXTURE.communicationPreferences,
        notificationOfUpdates: {
          phoneNumbers: [],
          emailAddresses: []
        }
      },
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    } as any;

    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(document.querySelectorAll('.ant-checkbox-checked').length).toBe(0);
  });

  test('should preselect email', async () => {
    const getMockContextValue = (
      directCorrespondenceEmail: string,
      notificationOfUpdatesEmail: string,
      writtenCorrespondenceEmail: string
    ) =>
      ({
        ...CONTEXT_VALUES_FIXTURE,
        phoneNumbers: PHONES_FIXTURE,
        emailAddresses: EMAILS_FIXTURE,
        communicationPreferences: {
          ...CONTEXT_VALUES_FIXTURE.communicationPreferences,
          directCorrespondence: {
            phoneNumbers: [],
            emailAddresses: directCorrespondenceEmail
              ? [{ emailAddress: directCorrespondenceEmail }]
              : []
          },
          notificationOfUpdates: {
            phoneNumbers: [],
            emailAddresses: notificationOfUpdatesEmail
              ? [{ emailAddress: notificationOfUpdatesEmail }]
              : []
          },
          writtenCorrespondence: {
            phoneNumbers: [],
            emailAddresses: writtenCorrespondenceEmail
              ? [{ emailAddress: writtenCorrespondenceEmail }]
              : []
          }
        }
      } as any);

    const { rerender } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={getMockContextValue('', EMAILS_FIXTURE[0].emailAddress, '')}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(EMAILS_FIXTURE[0].emailAddress).closest('label')
    ).toHaveClass('ant-radio-wrapper-checked');

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={getMockContextValue('', '', EMAILS_FIXTURE[0].emailAddress)}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(EMAILS_FIXTURE[0].emailAddress).closest('label')
    ).toHaveClass('ant-radio-wrapper-checked');

    rerender(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={getMockContextValue(
            '',
            EMAILS_FIXTURE[0].emailAddress,
            EMAILS_FIXTURE[1].emailAddress
          )}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(EMAILS_FIXTURE[0].emailAddress).closest('label')
    ).toHaveClass('ant-radio-wrapper-checked');
  });

  test('should always select preferred email/phone after changing preference communication type', async () => {
    const mockContextValue = {
      ...CONTEXT_VALUES_FIXTURE,
      phoneNumbers: PHONES_FIXTURE,
      emailAddresses: EMAILS_FIXTURE
    };

    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider value={mockContextValue}>
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(document.querySelectorAll('.ant-checkbox-checked')).toHaveLength(2);
    expect(document.querySelectorAll('.ant-radio-checked')).toHaveLength(2);

    expect(document.querySelectorAll('.phoneNumberCard label')[0]).toHaveClass(
      'ant-radio-wrapper-checked'
    );

    userEvent.click(document.querySelectorAll('.phoneNumberCard label')[1]);

    await act(wait);

    expect(
      document.querySelectorAll('.phoneNumberCard label')[0]
    ).not.toHaveClass('ant-radio-wrapper-checked');

    // Uncheck both checkboxes and check them again.
    userEvent.click(screen.getByLabelText(componentText.sendEmailLabel));
    userEvent.click(screen.getByLabelText(componentText.sendSmsLabel));
    userEvent.click(screen.getByLabelText(componentText.sendEmailLabel));
    userEvent.click(screen.getByLabelText(componentText.sendSmsLabel));

    await act(wait);

    expect(document.querySelectorAll('.phoneNumberCard label')[0]).toHaveClass(
      'ant-radio-wrapper-checked'
    );
  });

  test('should call onModalClose', async () => {
    const mockOnClose = jest.fn();
    render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={EMPTY_CONTEXT_VALUES_FIXTURE}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={mockOnClose}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    userEvent.click(
      screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.CANCEL/ })
    );

    await act(wait);

    expect(mockOnClose).toHaveBeenCalled();
  });

  test('should render with undefined data', async () => {
    const mockContextValue = {
      communicationPreferences: undefined,
      phoneNumbers: undefined,
      emailAddresses: undefined
    };

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <CommunicationPreferencesContext.Provider
          value={mockContextValue as any}
        >
          <NotificationOfUpdatesModal
            isVisible={true}
            employeeId="1"
            onModalClose={jest.fn()}
          />
        </CommunicationPreferencesContext.Provider>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(componentText.mainTypeQuestion)
    ).toBeInTheDocument();
    expect(screen.getByText(componentText.sendEmailLabel)).toBeInTheDocument();
    expect(screen.getByText(componentText.sendSmsLabel)).toBeInTheDocument();
    expect(
      getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK')
    ).toBeInTheDocument();
    expect(
      screen.getByRole('button', { name: /FINEOS_COMMON.GENERAL.CANCEL/ })
    ).toBeInTheDocument();
  });
});
