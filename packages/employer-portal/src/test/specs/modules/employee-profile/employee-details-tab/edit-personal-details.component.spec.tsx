/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import userEvent from '@testing-library/user-event';
import {
  ConcurrencyUpdateError,
  GroupClientCustomerService
} from 'fineos-js-api-client';

import { render, wait, act, screen } from '../../../../common/custom-render';
import { EditPersonalDetailsForm } from '../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/personal-details';
import { CUSTOMER_INFO_FIXTURE } from '../../../../fixtures/customer.fixture';
import { ContextWrapper } from '../../../../common/utils';

describe('EditEmploymentDetails', () => {
  const customer = CUSTOMER_INFO_FIXTURE;
  const service = GroupClientCustomerService.getInstance();

  test('render', () => {
    render(
      <ContextWrapper>
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={jest.fn()}
          onEditFinish={jest.fn()}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    expect(screen.getByText('FINEOS_COMMON.GENERAL.OK')).toBeInTheDocument();
    expect(
      screen.getByText('FINEOS_COMMON.GENERAL.CANCEL')
    ).toBeInTheDocument();
    expect(screen.getAllByTestId('text-input').length).toBe(7);
    expect(screen.getAllByTestId('date-picker').length).toBe(1);
    expect(screen.getAllByTestId('dropdown-input').length).toBe(2);
  });

  test('should cancel personal details form', async () => {
    const mockCloseForm = jest.fn();
    render(
      <ContextWrapper>
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={mockCloseForm}
          onEditFinish={jest.fn()}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.click(screen.getByText('FINEOS_COMMON.GENERAL.CANCEL'));

    await wait();

    expect(mockCloseForm).toHaveBeenCalledTimes(1);
  });

  test('should display data concurrency modal when concurrency error returned', async () => {
    jest
      .spyOn(service, 'editCustomerInfo')
      .mockRejectedValue(new ConcurrencyUpdateError({ test: 'test' }));

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={jest.fn()}
          onEditFinish={jest.fn()}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    try {
      userEvent.type(
        screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
        'firstName'
      );
      userEvent.type(
        screen.getByLabelText('COMMON.FIELDS.LAST_NAME_LABEL*'),
        'lastName'
      );
      userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

      await wait();
    } catch {
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.TITLE')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
      ).toBeInTheDocument();
      expect(
        screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
      ).toBeInTheDocument();
    }
  });

  test('should not display data concurrency modal when concurrency error not returned', async () => {
    jest
      .spyOn(service, 'editCustomerInfo')
      .mockReturnValue(Promise.resolve({ test: 'test' } as any));

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={jest.fn()}
          onEditFinish={jest.fn()}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
      'firstName'
    );
    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.LAST_NAME_LABEL*'),
      'lastName'
    );
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await wait();

    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
    ).not.toBeInTheDocument();
  });

  test('should refresh data in form when concurrency occurred', async () => {
    const mockCustomerRefresh = jest.fn();
    jest
      .spyOn(service, 'editCustomerInfo')
      .mockRejectedValue(
        new ConcurrencyUpdateError({ firstName: 'test', lastName: 'test' })
      );

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={jest.fn()}
          onEditFinish={jest.fn()}
          onCustomerRefresh={mockCustomerRefresh}
        />
      </ContextWrapper>
    );

    expect(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*')
    ).not.toHaveValue('test');
    expect(
      screen.getByLabelText('COMMON.FIELDS.LAST_NAME_LABEL*')
    ).not.toHaveValue('test');

    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
      'firstName'
    );
    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.LAST_NAME_LABEL*'),
      'lastName'
    );
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await wait();

    expect(
      screen.getByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).toBeInTheDocument();
    expect(
      screen.getByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
    ).toBeInTheDocument();
    expect(
      screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON'));

    await wait();

    expect(mockCustomerRefresh).toHaveBeenCalledTimes(1);
    expect(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*')
    ).toHaveValue('test');
    expect(screen.getByLabelText('COMMON.FIELDS.LAST_NAME_LABEL*')).toHaveValue(
      'test'
    );
  });

  test('should display error response modal and no concurrency modal when error other than concurrency occurred', async () => {
    jest
      .spyOn(service, 'editCustomerInfo')
      .mockRejectedValue({ status: '404' });

    const { getSubmitButtonByText } = render(
      <ContextWrapper>
        <EditPersonalDetailsForm
          customer={customer}
          onFormClose={jest.fn()}
          onEditFinish={jest.fn()}
          onCustomerRefresh={jest.fn()}
        />
      </ContextWrapper>
    );

    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.FIRST_NAME_LABEL*'),
      'firstName'
    );
    userEvent.type(
      screen.getByLabelText('COMMON.FIELDS.LAST_NAME_LABEL*'),
      'lastName'
    );
    userEvent.click(getSubmitButtonByText('FINEOS_COMMON.GENERAL.OK'));

    await act(wait);

    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.TITLE')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.DESCRIPTION')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('DATA_CONCURRENCY_MODAL.REFRESH_BUTTON')
    ).not.toBeInTheDocument();

    expect(
      screen.getByText('FINEOS_COMMON.SOMETHING_WENT_WRONG.TITLE')
    ).toBeInTheDocument();
  });
});
