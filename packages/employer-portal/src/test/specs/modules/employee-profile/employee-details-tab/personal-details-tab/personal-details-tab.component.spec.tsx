/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, within, wait, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  ViewCustomerDataPermissions,
  ManageCustomerDataPermissions,
  Tabs
} from 'fineos-common';

import { ContextWrapper } from '../../../../../common/utils';
import { PersonalDetailsTab } from '../../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab';
import * as personalDetailsApiModule from '../../../../../../app/shared/api/customer.api';
import {
  CUSTOMER_INFO_FIXTURE,
  EMAILS_FIXTURE,
  PHONE_NUMBERS_FIXTURE
} from '../../../../../fixtures/customer.fixture';
import * as editEmailsApiModule from '../../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/contact-details/edit-emails';

jest.mock(
  '../../../../../../app/modules/employee-profile/employee-details-tab/personal-details-tab/contact-details/edit-emails/edit-emails.api',
  () => ({
    editCustomerEmail: jest.fn()
  })
);

const editCustomerEmail = editEmailsApiModule.editCustomerEmail as jest.Mock;

describe('PersonalDetailsTab', () => {
  beforeEach(() => {
    jest.setTimeout(10000);
    jest
      .spyOn(personalDetailsApiModule, 'fetchCustomerInfo')
      .mockReturnValue(Promise.resolve(CUSTOMER_INFO_FIXTURE));

    jest.spyOn(personalDetailsApiModule, 'fetchEmailAddresses');

    jest.spyOn(personalDetailsApiModule, 'fetchPhoneNumbers');
  });

  test('should render the personal details tab with all properties', async () => {
    (personalDetailsApiModule.fetchEmailAddresses as jest.Mock).mockReturnValueOnce(
      Promise.resolve(EMAILS_FIXTURE)
    );

    (personalDetailsApiModule.fetchPhoneNumbers as jest.Mock).mockReturnValueOnce(
      Promise.resolve(PHONE_NUMBERS_FIXTURE)
    );

    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_EMAIL_ADDRESSES,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
        ]}
      >
        <Tabs>
          <PersonalDetailsTab
            employeeId="1"
            key="test"
            tab="test"
            onCustomerUpdate={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('personal-details-tab')).toBeInTheDocument();

    const items = screen.getAllByTestId('property-item');

    expect(items).toHaveLength(5);

    expect(
      within(items[0]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.FULL_NAME');
    expect(
      within(items[0]).queryByTestId('property-item-value')
    ).toHaveTextContent('Obi Wan Kenobi');

    expect(
      within(items[1]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.DATE_OF_BIRTH');
    expect(
      within(items[1]).queryByTestId('property-item-value')
    ).toHaveTextContent('30-01-1990');

    expect(
      within(items[2]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.ADDRESS');
    expect(
      within(items[2]).queryByTestId('property-item-value')
    ).toHaveTextContent(
      `Main streetWhere it's atTwo turn tablesAnd a microphone, CAAB123456`
    );

    expect(
      within(items[3]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.EMAILS');
    expect(
      within(items[3]).queryByTestId('property-item-value')
    ).toHaveTextContent('obiwankenobi@tatooine.com');

    expect(
      within(items[4]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS');
    expect(
      within(items[4]).queryByTestId('property-item-value')
    ).toHaveTextContent('353-1-234567');
  });

  test('should not render the personal details tab with email addresses or phone numbers', async () => {
    render(
      <ContextWrapper
        permissions={[ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO]}
      >
        <Tabs>
          <PersonalDetailsTab
            employeeId="1"
            key="test"
            tab="test"
            onCustomerUpdate={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(screen.getByTestId('personal-details-tab')).toBeInTheDocument();

    const items = screen.getAllByTestId('property-item');

    expect(items).toHaveLength(5);

    expect(
      within(items[0]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.FULL_NAME');
    expect(
      within(items[0]).queryByTestId('property-item-value')
    ).toHaveTextContent('Obi Wan Kenobi');

    expect(
      within(items[1]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.DATE_OF_BIRTH');
    expect(
      within(items[1]).queryByTestId('property-item-value')
    ).toHaveTextContent('30-01-1990');

    expect(
      within(items[2]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.ADDRESS');
    expect(
      within(items[2]).queryByTestId('property-item-value')
    ).toHaveTextContent(
      `Main streetWhere it's atTwo turn tablesAnd a microphone, CAAB123456`
    );

    expect(
      within(items[3]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.EMAILS');
    expect(
      within(items[3]).queryByTestId('property-item-value')
    ).toHaveTextContent('-');

    expect(
      within(items[4]).queryByTestId('property-item-label')
    ).toHaveTextContent('PROFILE.PERSONAL_DETAILS_TAB.PHONE_NUMBERS');
    expect(
      within(items[4]).queryByTestId('property-item-value')
    ).toHaveTextContent('-');
  });

  test('should handle error response on submit', async () => {
    (personalDetailsApiModule.fetchEmailAddresses as jest.Mock).mockReturnValueOnce(
      Promise.resolve(EMAILS_FIXTURE)
    );

    editCustomerEmail.mockImplementation(() =>
      Promise.reject({ status: '404' })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_INFO,

          ManageCustomerDataPermissions.UPDATE_CUSTOMER_EMAILS,

          ViewCustomerDataPermissions.VIEW_CUSTOMER_EMAIL_ADDRESSES,

          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
        ]}
      >
        <Tabs>
          <PersonalDetailsTab
            employeeId="1"
            key="test"
            tab="test"
            onCustomerUpdate={jest.fn()}
          />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    await act(() => {
      userEvent.click(
        screen.getByText('PROFILE.PERSONAL_DETAILS_TAB.EDIT_EMAILS')
      );

      return wait();
    });

    await act(() => {
      userEvent.type(screen.getAllByTestId('text-input')[0], 'e');

      return wait();
    });

    await act(() => {
      userEvent.click(screen.getByTestId('submit-edited-emails'));

      return wait();
    });

    expect(screen.queryByText('test@gmail.com')).not.toBeInTheDocument();

    await act(wait);

    expect(
      screen.getByText(
        'FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE'
      )
    ).toBeInTheDocument();
  });
});
