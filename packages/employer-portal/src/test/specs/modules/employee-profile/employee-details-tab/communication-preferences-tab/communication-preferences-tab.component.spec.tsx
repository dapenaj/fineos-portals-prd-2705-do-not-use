/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, act, screen, wait } from '@testing-library/react';
import { ViewCustomerDataPermissions, Tabs } from 'fineos-common';

import { ContextWrapper } from '../../../../../common/utils';
// tslint:disable-next-line
import { CommunicationPreferencesTab } from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab';
// tslint:disable-next-line
import * as communicationPreferencesApiModule from '../../../../../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.api';
import { COMMUNICATIONS_FIXTURE } from '../../../../../fixtures/customer.fixture';

describe('CommunicationPreferencesTab', () => {
  beforeEach(() => {
    jest.spyOn(
      communicationPreferencesApiModule,
      'fetchCommunicationPreferences'
    );
  });

  test('should render the CommunicationPreferencesTab with correspondence', async () => {
    (communicationPreferencesApiModule.fetchCommunicationPreferences as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        directCorrespondence: COMMUNICATIONS_FIXTURE[0],
        notificationOfUpdates: COMMUNICATIONS_FIXTURE[1],
        writtenCorrespondence: COMMUNICATIONS_FIXTURE[2]
      })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_COMMUNICATION_PREFERENCES
        ]}
      >
        <Tabs>
          <CommunicationPreferencesTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText('PROFILE.COMMUNICATION_PREFERENCES_TAB.TITLE')
    ).toBeInTheDocument();
  });

  test('should not render Phone section without permissions', async () => {
    (communicationPreferencesApiModule.fetchCommunicationPreferences as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        directCorrespondence: COMMUNICATIONS_FIXTURE[0],
        notificationOfUpdates: COMMUNICATIONS_FIXTURE[1],
        writtenCorrespondence: COMMUNICATIONS_FIXTURE[2]
      })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_COMMUNICATION_PREFERENCES
        ]}
      >
        <Tabs>
          <CommunicationPreferencesTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.queryByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.TITLE'
      )
    ).not.toBeInTheDocument();
  });

  test('should render Phone section with correct permissions', async () => {
    (communicationPreferencesApiModule.fetchCommunicationPreferences as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        directCorrespondence: COMMUNICATIONS_FIXTURE[0],
        notificationOfUpdates: COMMUNICATIONS_FIXTURE[1],
        writtenCorrespondence: COMMUNICATIONS_FIXTURE[2]
      })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewCustomerDataPermissions.VIEW_CUSTOMER_COMMUNICATION_PREFERENCES,
          ViewCustomerDataPermissions.VIEW_CUSTOMER_PHONE_NUMBERS
        ]}
      >
        <Tabs>
          <CommunicationPreferencesTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await act(wait);

    expect(
      screen.getByText(
        'PROFILE.COMMUNICATION_PREFERENCES_TAB.DIRECT_CORRESPONDENCE.TITLE'
      )
    ).toBeInTheDocument();
  });
});
