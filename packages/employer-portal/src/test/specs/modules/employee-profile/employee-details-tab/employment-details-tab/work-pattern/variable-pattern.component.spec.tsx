/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { render, act, wait } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import each from 'jest-each';

import { ContextWrapper } from '../../../../../../common/utils';
import { screen } from '../../../../../../common/custom-render';
import {
  VariablePattern,
  variablePatternValidation
} from '../../../../../../../app/shared/work-pattern';
import { variablePatternFormValue } from '../../../../../../../app/shared/work-pattern/variable-pattern/variable-pattern.form';
import { createFormPartialValidationTesting } from '../../../../../../common/assertion/validation.assertion';

describe('VariablePattern', () => {
  each([
    [
      'additional-info-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION*',
      '',
      'WORK_PATTERN.VALIDATION.ADDITIONAL_INFORMATION_VALIDATION'
    ],
    [
      'additional-info-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.ADDITIONAL_INFORMATION*',
      'a',
      ''
    ]
  ]).test(
    'in %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: { scope: variablePatternFormValue },
      validationSchema: Yup.object({
        scope: variablePatternValidation.clone()
      }),
      Component: () => <VariablePattern nameScope="scope" />
    })
  );

  each([
    [
      'number-of-weeks-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.NUMBER_OF_WEEKS*',
      '',
      'FINEOS_COMMON.VALIDATION.REQUIRED'
    ],
    [
      'number-of-weeks-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.NUMBER_OF_WEEKS*',
      '1',
      ''
    ]
  ]).test(
    'when workPatternRepeatsWeekly enabled, then in %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: {
        scope: { ...variablePatternFormValue, workPatternRepeatsWeekly: true }
      },
      validationSchema: Yup.object({
        scope: variablePatternValidation.clone()
      }),
      Component: () => <VariablePattern nameScope="scope" />
    })
  );

  each([
    ['day-length-field', 'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*', '', ''],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      'fdfdfdf',
      'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT'
    ],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      '21:22',
      ''
    ],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      '23:00',
      ''
    ],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      '00:59',
      ''
    ],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      '21:60',
      'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT'
    ],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      '24:00',
      'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT'
    ],
    [
      'day-length-field',
      'INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH*',
      '00:00',
      'FINEOS_COMMON.VALIDATION.VALID_TIME_FORMAT'
    ]
  ]).test(
    'when workDayIsSameLength enabled, then in %s for field with label %s and value set to %s next error message "%s" should appear',
    createFormPartialValidationTesting({
      initialValues: {
        scope: { ...variablePatternFormValue, workDayIsSameLength: true }
      },
      validationSchema: Yup.object({
        scope: variablePatternValidation.clone()
      }),
      Component: () => <VariablePattern nameScope="scope" />
    })
  );

  test('should allow to select number of weeks if workPatternRepeatsWeekly selected', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{ scope: variablePatternFormValue }}
          validationSchema={Yup.object({
            scope: variablePatternValidation.clone()
          })}
          onSubmit={jest.fn()}
        >
          <VariablePattern nameScope="scope" />
        </Formik>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByTestId('work-pattern-repeats'));

      return wait();
    });

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.NUMBER_OF_WEEKS')
    ).toBeInTheDocument();

    await act(() => {
      userEvent.click(screen.getByTestId('work-day-is-same-length'));

      return wait();
    });

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH')
    ).toBeInTheDocument();
  });

  test('should allow to select day length if workDayIsSameLength selected', async () => {
    render(
      <ContextWrapper>
        <Formik
          initialValues={{ scope: variablePatternFormValue }}
          validationSchema={Yup.object({
            scope: variablePatternValidation.clone()
          })}
          onSubmit={jest.fn()}
        >
          <VariablePattern nameScope="scope" />
        </Formik>
      </ContextWrapper>
    );

    await act(() => {
      userEvent.click(screen.getByTestId('work-day-is-same-length'));

      return wait();
    });

    expect(
      screen.getByText('INTAKE.OCCUPATION_AND_EARNINGS.DAY_LENGTH')
    ).toBeInTheDocument();
  });
});
