import React from 'react';
import { render, screen } from '@testing-library/react';

import { ContextWrapper } from '../../../../../common/utils';
import { EmploymentDataCard } from '../../../../../../app/modules/employee-profile/employee-details-tab/employment-details-tab/employment-data-card/employment-data-card.component';

describe('EmploymentDataCard', () => {
  const mockTitle = 'test title';
  const mockEmptyContent = 'test empty content';
  const mockButtonText = 'test action button';
  const mockChidren = 'test children';

  test('should correctly render elements', () => {
    render(
      <ContextWrapper>
        <EmploymentDataCard
          title={<p>{mockTitle}</p>}
          emptyContent={<p>{mockEmptyContent}</p>}
          actionButton={<button>{mockButtonText}</button>}
          isLoading={false}
        >
          <>
            <p>{mockChidren}</p>
            <button>{mockButtonText}</button>
          </>
        </EmploymentDataCard>
      </ContextWrapper>
    );

    expect(screen.getByText(mockTitle)).toBeInTheDocument();
    expect(screen.queryByText(mockEmptyContent)).not.toBeInTheDocument();
    expect(screen.getByText(mockButtonText)).toBeInTheDocument();
    expect(screen.getByText(mockChidren)).toBeInTheDocument();
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
  });

  test('should render spinner when loading', () => {
    render(
      <ContextWrapper>
        <EmploymentDataCard
          title={<p>{mockTitle}</p>}
          emptyContent={<p>{mockEmptyContent}</p>}
          actionButton={<button>{mockButtonText}</button>}
          isLoading={true}
        >
          <p>{mockChidren}</p>
        </EmploymentDataCard>
      </ContextWrapper>
    );

    expect(screen.getByText(mockTitle)).toBeInTheDocument();
    expect(screen.queryByText(mockEmptyContent)).not.toBeInTheDocument();
    expect(screen.queryByText(mockChidren)).not.toBeInTheDocument();
    expect(screen.getByTestId('spinner')).toBeInTheDocument();
  });
});
