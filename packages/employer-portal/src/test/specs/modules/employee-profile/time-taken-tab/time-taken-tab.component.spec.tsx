/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { render, wait, screen } from '@testing-library/react';
import { Tabs, ViewNotificationPermissions } from 'fineos-common';

import { TimeTakenTab } from '../../../../../app/modules/employee-profile/time-taken-tab';
import * as timeTakenApiModule from '../../../../../app/modules/employee-profile/time-taken-tab/time-taken.api';
import * as notificationsApiModule from '../../../../../app/modules/employee-profile/notifications-tab/notifications-tab.api';
import { ContextWrapper } from '../../../../common/utils';
import { ABSENCE_FOR_TIME_TAKEN_FIXTURE } from '../../../../fixtures/absence-periods.fixture';
import {
  EMPLOYEE_LEAVE_BALANCE_ROLLING_FIXTURE,
  EMPLOYEE_LEAVE_BALANCE_ROLLING_FORWARD_FIXTURE,
  EMPLOYEE_LEAVE_BALANCE_FIXED_YEAR_FIXTURE,
  EMPLOYEE_LEAVE_BALANCE_CALENDAR_YEAR_FIXTURE
} from '../../../../fixtures/employee-leave-balance.fixture';
import { NOTIFICATIONS_FIXTURE } from '../../../../fixtures/notifications.fixture';

describe('TimeTakenTab', () => {
  beforeEach(() => {
    jest.spyOn(timeTakenApiModule, 'fetchAbsencePeriodDecisions');
    jest.spyOn(timeTakenApiModule, 'fetchLeaveAvailability');
    jest.spyOn(notificationsApiModule, 'fetchNotifications');
  });

  test('should show empty message if no abscence notifications', async () => {
    (notificationsApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 0,
        data: []
      })
    );

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <Tabs>
          <TimeTakenTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    expect(timeTakenApiModule.fetchAbsencePeriodDecisions).not.toBeCalled();
    expect(
      screen.getByText('PROFILE.TIME_TAKEN_TAB.EMPTY_STATE')
    ).toBeInTheDocument();
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
  });

  test('should show empty message if no time taken available', async () => {
    (notificationsApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 1,
        data: NOTIFICATIONS_FIXTURE
      })
    );
    (timeTakenApiModule.fetchAbsencePeriodDecisions as jest.Mock).mockReturnValueOnce(
      Promise.resolve({})
    );
    (timeTakenApiModule.fetchLeaveAvailability as jest.Mock).mockReturnValueOnce(
      Promise.resolve([])
    );
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <Tabs>
          <TimeTakenTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByText('PROFILE.TIME_TAKEN_TAB.EMPTY_STATE')
    ).toBeInTheDocument();
    expect(screen.queryByTestId('spinner')).not.toBeInTheDocument();
  });

  test('should show list of LeavePlans if no time taken rolling back type', async () => {
    (notificationsApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 1,
        data: NOTIFICATIONS_FIXTURE
      })
    );
    (timeTakenApiModule.fetchAbsencePeriodDecisions as jest.Mock).mockReturnValue(
      Promise.resolve(ABSENCE_FOR_TIME_TAKEN_FIXTURE)
    );
    (timeTakenApiModule.fetchLeaveAvailability as jest.Mock).mockReturnValue(
      Promise.resolve(EMPLOYEE_LEAVE_BALANCE_ROLLING_FIXTURE)
    );

    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <Tabs>
          <TimeTakenTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByTestId('PROFILE.TIME_TAKEN_TAB.TIME_AVAILABLE')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(
        'PROFILE.TIME_TAKEN_TAB.TOTAL_PLAN_ENTITLEMENT.VALUE.ROLLING'
      )
    ).toBeInTheDocument();
    expect(screen.getByText('Rolling Back')).toBeInTheDocument();
  });

  test('should show list of LeavePlans if no time taken calendar year type', async () => {
    (notificationsApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 1,
        data: NOTIFICATIONS_FIXTURE
      })
    );
    (timeTakenApiModule.fetchAbsencePeriodDecisions as jest.Mock).mockReturnValue(
      Promise.resolve(ABSENCE_FOR_TIME_TAKEN_FIXTURE)
    );
    (timeTakenApiModule.fetchLeaveAvailability as jest.Mock).mockReturnValue(
      Promise.resolve(EMPLOYEE_LEAVE_BALANCE_CALENDAR_YEAR_FIXTURE)
    );
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <Tabs>
          <TimeTakenTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByTestId('PROFILE.TIME_TAKEN_TAB.TIME_AVAILABLE')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(
        'PROFILE.TIME_TAKEN_TAB.TOTAL_PLAN_ENTITLEMENT.VALUE.YEAR'
      )
    ).toBeInTheDocument();
    expect(screen.getByText('Calendar Year')).toBeInTheDocument();
  });

  test('should show list of LeavePlans if no time taken fixed year type', async () => {
    (notificationsApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 1,
        data: NOTIFICATIONS_FIXTURE
      })
    );
    (timeTakenApiModule.fetchAbsencePeriodDecisions as jest.Mock).mockReturnValue(
      Promise.resolve(ABSENCE_FOR_TIME_TAKEN_FIXTURE)
    );
    (timeTakenApiModule.fetchLeaveAvailability as jest.Mock).mockReturnValue(
      Promise.resolve(EMPLOYEE_LEAVE_BALANCE_FIXED_YEAR_FIXTURE)
    );
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <Tabs>
          <TimeTakenTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByTestId('PROFILE.TIME_TAKEN_TAB.TIME_AVAILABLE')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(
        'PROFILE.TIME_TAKEN_TAB.TOTAL_PLAN_ENTITLEMENT.VALUE.YEAR'
      )
    ).toBeInTheDocument();
    expect(screen.getByText('Fixed Year')).toBeInTheDocument();
  });

  test('should show list of LeavePlans if no time taken rolling forward type', async () => {
    (notificationsApiModule.fetchNotifications as jest.Mock).mockReturnValueOnce(
      Promise.resolve({
        total: 1,
        data: NOTIFICATIONS_FIXTURE
      })
    );
    (timeTakenApiModule.fetchAbsencePeriodDecisions as jest.Mock).mockReturnValue(
      Promise.resolve(ABSENCE_FOR_TIME_TAKEN_FIXTURE)
    );
    (timeTakenApiModule.fetchLeaveAvailability as jest.Mock).mockReturnValue(
      Promise.resolve(EMPLOYEE_LEAVE_BALANCE_ROLLING_FORWARD_FIXTURE)
    );
    render(
      <ContextWrapper
        permissions={[
          ViewNotificationPermissions.VIEW_ABSENCE_PERIOD_DECISIONS,
          ViewNotificationPermissions.VIEW_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY
        ]}
      >
        <Tabs>
          <TimeTakenTab employeeId="1" key="test" tab="test" />
        </Tabs>
      </ContextWrapper>
    );

    await wait();

    expect(
      screen.getByTestId('PROFILE.TIME_TAKEN_TAB.TIME_AVAILABLE')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(
        'PROFILE.TIME_TAKEN_TAB.TOTAL_PLAN_ENTITLEMENT.VALUE.ROLLING'
      )
    ).toBeInTheDocument();
    expect(screen.getByText('Rolling Forward')).toBeInTheDocument();
  });
});
