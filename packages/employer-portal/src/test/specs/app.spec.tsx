/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { render, act, RenderResult } from '@testing-library/react';
import { ConfiguratorPermissions } from 'fineos-common';

import * as appApiModule from '../../app/app.api';
import App from '../../app/app';
import { releaseEventLoop, getMockTranslations } from '../common/utils';
import defaultConfig from '../../app/shared/client-config/default-config.json';
import { PortalEnvConfigService } from '../../app/shared';
import portalEnvConfig from '../../../public/config/portal.env.json';

describe('App', () => {
  let mock: MockAdapter;

  beforeEach(() => {
    mock = new MockAdapter(axios);
    const portalEnvConfigService = PortalEnvConfigService.getInstance();
    jest
      .spyOn(portalEnvConfigService, 'fetchLocaleData')
      .mockReturnValueOnce(Promise.resolve(getMockTranslations()));
    jest
      .spyOn(portalEnvConfigService, 'fetchPortalEnvConfig')
      .mockReturnValue(Promise.resolve(portalEnvConfig));
  });

  afterEach(() => {
    mock.restore();
  });

  test('should load config with permissions and render app', async () => {
    mock.onGet('/config/client-config.json').reply(200, {
      ...defaultConfig,
      theme: {
        icons: null,
        header: {
          logo: '/test-logo.png',
          logoAltText: 'Employer Portal logo'
        }
      }
    });
    const permissionsSpy = jest
      .spyOn(appApiModule, 'fetchPermissions')
      .mockReturnValueOnce(Promise.resolve(new Set()));

    let getByAltText: RenderResult['getByAltText'];

    await act(async () => {
      const renderResult = render(<App />);
      getByAltText = renderResult.getByAltText;
      await releaseEventLoop();
    });

    expect(
      (getByAltText!('Employer Portal logo') as HTMLImageElement).src
    ).toContain('/test-logo.png');
    expect(permissionsSpy).toHaveBeenCalled();
  });

  test('should render app with default config if app failed', async () => {
    mock.onGet('/config/client-config.json').networkError();
    jest
      .spyOn(appApiModule, 'fetchPermissions')
      .mockReturnValueOnce(Promise.resolve(new Set()));

    let getByAltText: RenderResult['getByAltText'];

    await act(async () => {
      const renderResult = render(<App />);
      getByAltText = renderResult.getByAltText;
      await releaseEventLoop();
    });

    expect(
      (getByAltText!('Employer Portal logo') as HTMLImageElement).src
    ).toContain(defaultConfig.theme.header.logo);
  });

  test('should render app if permissions failed', async () => {
    mock.onGet('/config/client-config.json').reply(200, {
      ...defaultConfig,
      theme: {
        header: {
          logo: '/test-logo.png',
          logoAltText: 'Employer Portal logo'
        }
      }
    });
    const permissionsSpy = jest
      .spyOn(appApiModule, 'fetchPermissions')
      .mockImplementationOnce(() => Promise.reject(new Error('test')));

    let getByAltText: RenderResult['getByAltText'];

    await act(async () => {
      const renderResult = render(<App />);
      getByAltText = renderResult.getByAltText;
      await releaseEventLoop();
    });

    expect(
      (getByAltText!('Employer Portal logo') as HTMLImageElement).src
    ).toContain(defaultConfig.theme.header.logo);
    expect(permissionsSpy).toHaveBeenCalled();
  });

  test('should render theme configurator if has permissions', async () => {
    jest
      .spyOn(appApiModule, 'fetchPermissions')
      .mockReturnValueOnce(
        Promise.resolve(new Set([ConfiguratorPermissions.VIEW_CONFIGURATOR]))
      );

    let getByTestId: RenderResult['getByTestId'];

    await act(async () => {
      const renderResult = render(<App />);
      getByTestId = renderResult.getByTestId;
      await releaseEventLoop();
    });
    expect(getByTestId('theme-configurator')).toBeInTheDocument();
  });

  test('should not render theme configurator if has no permissions', async () => {
    jest
      .spyOn(appApiModule, 'fetchPermissions')
      .mockReturnValueOnce(Promise.resolve(new Set()));

    let queryByTestId: RenderResult['queryByTestId'];

    await act(async () => {
      const renderResult = render(<App />);
      queryByTestId = renderResult.queryByTestId;
      await releaseEventLoop();
    });
    expect(queryByTestId('theme-configurator')).not.toBeInTheDocument();
  });
});
