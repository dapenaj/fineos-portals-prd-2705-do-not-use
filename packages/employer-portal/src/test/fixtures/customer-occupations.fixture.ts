/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { CustomerOccupation } from 'fineos-js-api-client';
import moment from 'moment';

export const CUSTOMER_OCCUPATIONS_FIXTURE = [
  {
    id: 'CO-1',
    orgUnitName: 'Jedi',
    worksiteName: 'Tatooine',
    workPatternBasis: 'Other',
    altEmploymentCat: {
      domainId: 167,
      fullId: 5344000,
      name: 'Unknown',
      domainName: 'OCOccupation AltEmploymentCat'
    },
    daysWorkedPerWeek: '5',
    employeeIdentifier: '',
    empLocationCode: {
      domainId: 166,
      fullId: 5312048,
      name: 'NY',
      domainName: 'OCOccupation EmpLocationCode'
    },
    employmentCat: {
      domainId: 145,
      fullId: 4640000,
      name: 'Part time',
      domainName: 'Employment Category'
    },
    employmentStatus: {
      domainId: 175,
      fullId: 5600003,
      name: 'Active',
      domainName: 'OCOccupation EmploymentStatus'
    },
    employmentTitle: {
      domainId: 177,
      fullId: 5664000,
      name: 'Unknown',
      domainName: 'OCOccupation EmploymentTitle'
    },
    endPosCode: {
      domainId: 162,
      fullId: 5184000,
      name: 'Unknown',
      domainName: 'OCOccupation End Position Code'
    },
    endPosReason: '',
    hrsWorkedPerWeek: '40',
    jobDesc: '',
    jobEndDate: moment(new Date('2020-02-13')),
    jobStartDate: moment(new Date('2020-01-15')),
    jobStrenuous: {
      domainId: 160,
      fullId: 5120000,
      name: 'Unknown',
      domainName: 'OCOccupation Job Strenuousness'
    },
    jobTitle: 'Master',
    overrideDaysWorkedPerWeek: false,
    remarks: '',
    workSchDesc: ''
  },
  {
    id: 'CO-1',
    orgUnitName: 'The Sith',
    worksiteName: 'Starkiller Base',
    workPatternBasis: 'Other',
    altEmploymentCat: {
      domainId: 167,
      fullId: 5344000,
      name: 'Unknown',
      domainName: 'OCOccupation AltEmploymentCat'
    },
    daysWorkedPerWeek: '5',
    employeeIdentifier: '',
    empLocationCode: {
      domainId: 166,
      fullId: 5312048,
      name: 'NY',
      domainName: 'OCOccupation EmpLocationCode'
    },
    employmentCat: {
      domainId: 145,
      fullId: 4640000,
      name: 'employmentCat',
      domainName: 'Employment Category'
    },
    employmentStatus: {
      domainId: 175,
      fullId: 5600003,
      name: 'Active',
      domainName: 'OCOccupation EmploymentStatus'
    },
    employmentTitle: {
      domainId: 177,
      fullId: 5664000,
      name: 'Unknown',
      domainName: 'OCOccupation EmploymentTitle'
    },
    endPosCode: {
      domainId: 162,
      fullId: 5184000,
      name: 'Unknown',
      domainName: 'OCOccupation End Position Code'
    },
    endPosReason: '',
    hrsWorkedPerWeek: '40',
    jobDesc: '',
    jobEndDate: moment().add(3, 'days'),
    jobStartDate: moment().subtract(3, 'days'),
    jobStrenuous: {
      domainId: 160,
      fullId: 5120000,
      name: 'Unknown',
      domainName: 'OCOccupation Job Strenuousness'
    },
    jobTitle: 'Leader',
    overrideDaysWorkedPerWeek: false,
    remarks: '',
    workSchDesc: ''
  },
  {
    id: 'CO-1',
    orgUnitName: 'The Sith',
    worksiteName: 'Starkiller Base',
    workPatternBasis: 'Other',
    altEmploymentCat: {
      domainId: 167,
      fullId: 5344000,
      name: 'Unknown',
      domainName: 'OCOccupation AltEmploymentCat'
    },
    daysWorkedPerWeek: '5',
    employeeIdentifier: '',
    empLocationCode: {
      domainId: 166,
      fullId: 5312048,
      name: 'NY',
      domainName: 'OCOccupation EmpLocationCode'
    },
    employmentCat: {
      domainId: 145,
      fullId: 4640000,
      name: 'employmentCat',
      domainName: 'Employment Category'
    },
    employmentStatus: {
      domainId: 175,
      fullId: 5600003,
      name: 'Active',
      domainName: 'OCOccupation EmploymentStatus'
    },
    employmentTitle: {
      domainId: 177,
      fullId: 5664000,
      name: 'Unknown',
      domainName: 'OCOccupation EmploymentTitle'
    },
    endPosCode: {
      domainId: 162,
      fullId: 5184000,
      name: 'Unknown',
      domainName: 'OCOccupation End Position Code'
    },
    endPosReason: '',
    hrsWorkedPerWeek: '40',
    jobDesc: '',
    jobEndDate: null,
    jobStartDate: moment(new Date('2019-02-27')),
    jobStrenuous: {
      domainId: 160,
      fullId: 5120000,
      name: 'Unknown',
      domainName: 'OCOccupation Job Strenuousness'
    },
    jobTitle: 'Leader',
    overrideDaysWorkedPerWeek: false,
    remarks: '',
    workSchDesc: ''
  },
  {
    id: 'CO-1',
    orgUnitName: 'The Sith',
    worksiteName: 'Starkiller Base',
    workPatternBasis: 'Other',
    altEmploymentCat: {
      domainId: 167,
      fullId: 5344000,
      name: 'Unknown',
      domainName: 'OCOccupation AltEmploymentCat'
    },
    daysWorkedPerWeek: '5',
    employeeIdentifier: '',
    empLocationCode: {
      domainId: 166,
      fullId: 5312048,
      name: 'NY',
      domainName: 'OCOccupation EmpLocationCode'
    },
    employmentCat: {
      domainId: 145,
      fullId: 4640000,
      name: 'employmentCat',
      domainName: 'Employment Category'
    },
    employmentStatus: {
      domainId: 175,
      fullId: 5600003,
      name: 'Active',
      domainName: 'OCOccupation EmploymentStatus'
    },
    employmentTitle: {
      domainId: 177,
      fullId: 5664000,
      name: 'Unknown',
      domainName: 'OCOccupation EmploymentTitle'
    },
    endPosCode: {
      domainId: 162,
      fullId: 5184000,
      name: 'Unknown',
      domainName: 'OCOccupation End Position Code'
    },
    endPosReason: '',
    hrsWorkedPerWeek: '40',
    jobDesc: '',
    jobEndDate: moment().add(3, 'days'),
    jobStartDate: moment(),
    jobStrenuous: {
      domainId: 160,
      fullId: 5120000,
      name: 'Unknown',
      domainName: 'OCOccupation Job Strenuousness'
    },
    jobTitle: 'Leader',
    overrideDaysWorkedPerWeek: false,
    remarks: '',
    workSchDesc: ''
  }
] as CustomerOccupation[];
