/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientEmailAddress,
  GroupClientPhoneNumber
} from 'fineos-js-api-client';

export const EMAILS_FIXTURE = [
  {
    id: '1',
    emailAddress: 'test1@test1.test.com'
  },
  {
    id: '2',
    emailAddress: 'test2@test2.test.com'
  },
  {
    id: '3',
    emailAddress: 'test3@test3.test.com'
  },
  {
    id: '4',
    emailAddress: 'test4@test4.test.com'
  },
  {
    id: '5',
    emailAddress: 'test5@test5.test.com'
  }
] as GroupClientEmailAddress[];
export const PHONES_FIXTURE = [
  {
    id: '1',
    contactMethod: {
      domainId: 51,
      fullId: 1660001,
      name: 'Cell',
      domainName: 'Contact Medium'
    },
    areaCode: '12',
    extension: '',
    intCode: '',
    telephoneNo: '123456',
    exDirectory: false
  },
  {
    id: '2',
    contactMethod: {
      domainId: 51,
      fullId: 1660001,
      name: 'Cell',
      domainName: 'Contact Medium'
    },
    areaCode: '12',
    extension: '',
    intCode: '',
    telephoneNo: '654321',
    exDirectory: false
  },
  {
    id: '3',
    intCode: '1',
    areaCode: '12',
    telephoneNo: '123-123-123',
    contactMethod: {
      domainId: 51,
      fullId: 1632004,
      name: 'Mobile',
      domainName: 'Contact Medium'
    }
  },
  {
    id: '4',
    intCode: '2',
    areaCode: '23',
    telephoneNo: '234-234-234',
    contactMethod: {
      domainId: 51,
      fullId: 1632001,
      name: 'Phone',
      domainName: 'Contact Medium'
    }
  },
  {
    id: '5',
    contactMethod: {
      domainId: 51,
      fullId: 1660001,
      name: 'Cell',
      domainName: 'Contact Medium'
    },
    areaCode: '12',
    extension: '',
    intCode: '',
    telephoneNo: '611111',
    exDirectory: false
  }
] as GroupClientPhoneNumber[];
