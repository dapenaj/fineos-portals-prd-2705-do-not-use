/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { PaidLeaveBenefit } from '../../app/shared';

import { DISABILITY_BENEFIT_FIXTURE } from './group-client-claim.fixture';

export const PAID_LEAVE_BENEFITS_FIXTURE = [
  {
    disabilityBenefit: DISABILITY_BENEFIT_FIXTURE,
    paidLeaveCaseId: 'PLC-1',
    leavePlanName: 'Short Term Disability 1',
    reasonName: 'Reason Name 1',
    type: 'Reduced Schedule',
    benefitCaseType: 'Short Term Disability 1',
    status: 'Pending',
    benefitId: 1
  },
  {
    disabilityBenefit: DISABILITY_BENEFIT_FIXTURE,
    paidLeaveCaseId: 'PLC-2',
    leavePlanName: 'Short Term Disability 1',
    reasonName: 'Reason Name 1',
    type: 'Reduced Schedule',
    benefitCaseType: 'Short Term Disability 1',
    status: 'Pending',
    benefitId: 2
  },
  {
    disabilityBenefit: DISABILITY_BENEFIT_FIXTURE,
    paidLeaveCaseId: 'PLC-3',
    leavePlanName: 'Short Term Disability 2',
    reasonName: 'Reason Name 1',
    type: 'Reduced Schedule',
    benefitCaseType: 'Short Term Disability 1',
    status: 'Pending',
    benefitId: 3
  }
] as PaidLeaveBenefit[];
