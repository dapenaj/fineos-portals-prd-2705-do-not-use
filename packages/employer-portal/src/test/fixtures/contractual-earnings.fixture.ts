/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { CustomerContractualEarnings } from 'fineos-js-api-client';
import moment from 'moment';

export const CONTRACTUAL_EARNINGS_FIXTURE = [
  {
    id: '11650-108',
    amount: {
      amountMinorUnits: 70000,
      currency: '---',
      scale: 2
    },
    earningsType: {
      domainId: 223,
      fullId: 7136001,
      name: 'Pre-Disability',
      domainName: 'EarningsType'
    },
    effectiveDate: moment(new Date('2019-02-27')),
    endDate: moment(new Date('2020-02-14')),
    frequency: {
      domainId: 235,
      fullId: 7520001,
      name: 'Weekly',
      domainName: 'EarningsBasis'
    }
  },
  {
    id: '11650-108',
    amount: {
      amountMinorUnits: 11111,
      currency: '---',
      scale: 2
    },
    earningsType: {
      domainId: 223,
      fullId: 7136001,
      name: 'Pre-Disability',
      domainName: 'EarningsType'
    },
    effectiveDate: moment(new Date('2019-02-27')),
    endDate: moment(new Date('2020-02-14')),
    frequency: {
      domainId: 235,
      fullId: 7520001,
      name: 'Weekly',
      domainName: 'EarningsBasis'
    }
  },
  {
    id: '11650-108',
    amount: {
      amountMinorUnits: 80000,
      currency: '---',
      scale: 2
    },
    earningsType: {
      domainId: 223,
      fullId: 7136001,
      name: 'Pre-Disability',
      domainName: 'EarningsType'
    },
    effectiveDate: moment(new Date('2019-02-27')),
    endDate: moment(new Date('2020-02-14')),
    frequency: {
      name: 'Yearly',
      domainId: 235,
      fullId: 7520005,
      domainName: 'EarningsBasis'
    }
  },
  {
    id: '11650-108',
    amount: {
      amountMinorUnits: 11111,
      currency: '---',
      scale: 2
    },
    earningsType: {
      domainId: 223,
      fullId: 7136001,
      name: 'Pre-Disability',
      domainName: 'EarningsType'
    },
    effectiveDate: moment(new Date('2019-02-27')),
    endDate: moment(new Date('2020-02-14')),
    frequency: {
      name: 'Unknown',
      domainId: 235,
      fullId: 7520005,
      domainName: 'EarningsBasis'
    }
  }
] as CustomerContractualEarnings[];
