/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import {
  GroupClientAbsence,
  GroupClientEmployee,
  GroupClientLeavePlan,
  GroupClientLeaveRequest
} from 'fineos-js-api-client';

import { EnhancedDecision } from '../../app/shared';

export const LEAVE_PLAN_FIXTURE = [
  {
    leavePlanName: 'Test leave plane',
    decisions: [
      {
        absence: {
          id: '1',
          caseReference: 'ABS-2'
        } as GroupClientAbsence,
        employee: {
          id: 'EMP-3',
          name: 'Qui Gon Jinn'
        } as GroupClientEmployee,
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment(new Date('2020-01-01')),
          endDate: moment(new Date('2020-08-12')),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Episodic',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: '',
            name: '',
            shortName: '',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Child Bonding',
            decisionStatus: 'Denied'
          } as GroupClientLeaveRequest
        },
        statusCategory: 'Pending'
      } as EnhancedDecision,
      {
        absence: {
          id: '2',
          caseReference: 'ABS-2'
        } as GroupClientAbsence,
        employee: {
          id: 'EMP-3',
          name: 'Qui Gon Jinn'
        } as GroupClientEmployee,
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment(new Date('2020-01-01')),
          endDate: moment(new Date('2020-08-12')),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Episodic',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: '',
            name: '',
            shortName: '',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Child Bonding',
            decisionStatus: 'Denied'
          } as GroupClientLeaveRequest
        },
        statusCategory: 'Pending'
      } as EnhancedDecision
    ]
  }
];

export const ABSENCE_PERIOD_DECISIONS_FIXTURE = [
  {
    absence: {
      id: '1',
      caseReference: 'ABS-2'
    },
    employee: {
      id: 'EMP-1',
      name: 'Luke Skywalker'
    },
    period: {
      periodReference: '',
      parentPeriodReference: '',
      relatedToEpisodic: false,
      startDate: moment(new Date('2020-02-05')),
      endDate: moment(new Date('2020-06-06')),
      balanceDeduction: 0,
      timeRequested: '',
      timeDecisionStatus: 'Approved',
      timeDecisionReason: '',
      type: 'Reduced Schedule',
      status: 'Approved',
      leavePlan: {
        paidLeaveCaseId: '',
        name: 'Federal FMLA',
        shortName: 'FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved',
        qualifier1: 'Prenatal Care'
      }
    },
    statusCategory: 'Pending'
  },
  {
    absence: {
      id: '2',
      caseReference: 'ABS-2'
    },
    employee: {
      id: 'EMP-2',
      name: 'Darth Vader'
    },
    period: {
      periodReference: '',
      parentPeriodReference: '',
      relatedToEpisodic: false,
      startDate: moment(new Date('2020-01-01')),
      endDate: moment(new Date('2020-12-08')),
      balanceDeduction: 0,
      timeRequested: '',
      timeDecisionStatus: 'Time Available',
      timeDecisionReason: '',
      type: 'Episodic',
      status: 'Episodic',
      leavePlan: {
        paidLeaveCaseId: '',
        name: 'Federal FMLA',
        shortName: 'FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved',
        qualifier1: 'Other'
      }
    },
    statusCategory: 'Pending'
  } as any,
  {
    absence: {
      id: '3',
      caseReference: 'ABS-2'
    },
    employee: {
      id: 'EMP-3',
      name: 'Qui Gon Jinn'
    },
    period: {
      periodReference: '',
      parentPeriodReference: '1',
      relatedToEpisodic: false,
      startDate: moment(new Date('2020-01-01')),
      endDate: moment(new Date('2020-06-01')),
      balanceDeduction: 0,
      timeRequested: '',
      timeDecisionStatus: '',
      timeDecisionReason: '',
      type: 'Time off period',
      status: 'Cancelled',
      leavePlan: {
        paidLeaveCaseId: '',
        name: 'Federal FMLA',
        shortName: 'FMLA',
        category: 'Paid'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        qualifier1: 'Postnatal Disability'
      }
    },
    statusCategory: 'Pending'
  } as any
] as EnhancedDecision[];
