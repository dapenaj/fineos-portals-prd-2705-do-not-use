/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import { AbsenceEvent } from 'fineos-js-api-client';

export const ABSENCE_EVENTS_FIXTURE = [
  {
    id: 'id-1',
    employee: {
      id: 'employee id-1',
      firstName: 'Matthew',
      lastName: 'Cartoon',
      initials: 'MC'
    },
    adminGroup: 'Engineering | Tampa, FL',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationCase: {
      caseReference: 'NTN-1',
      id: 'id-1'
    },
    eventName: 'Leave Request Approved',
    eventDate: moment('2020-02-10')
  } as AbsenceEvent,
  {
    id: 'id-2',
    employee: {
      id: 'employee id-1',
      firstName: 'Matthew',
      lastName: 'Cartoon',
      initials: 'MC'
    },
    adminGroup: 'Sales | Tampa, FL',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason'
    },
    notificationCase: {
      caseReference: 'NTN-1',
      id: 'id-1'
    },
    eventName: 'Leave Request Denied',
    eventDate: moment('2020-02-10')
  } as AbsenceEvent
];
