/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { Payment, GroupClientPayment } from 'fineos-js-api-client';
import moment from 'moment';

export const PAYMENTS_FIXTURE = [
  {
    paymentId: 'PYM-1',
    casePayment: 'asdf',
    claimId: 'CLM-1',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: null,
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Adhoc',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    benefitCaseNumber: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: undefined,
    rootCaseNumber: 'asdf'
  } as Payment,
  {
    paymentId: 'CLM-2',
    casePayment: 'asdf',
    claimId: 'asdf',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-10'),
    periodEndDate: moment('2050-01-15'),
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Recurring',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    benefitCaseNumber: 'asdf',
    accountTransferInfo: undefined,
    chequePaymentInfo: { chequeNumber: '0891274' },
    rootCaseNumber: 'asdf'
  } as Payment,
  {
    paymentId: 'CLM-2',
    casePayment: 'asdf',
    claimId: 'asdf',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: moment('2050-01-01'),
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Recurring',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    benefitCaseNumber: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: undefined,
    rootCaseNumber: 'asdf'
  } as Payment,
  {
    paymentId: 'CLM-2',
    casePayment: 'asdf',
    claimId: 'asdf',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: moment('2050-01-01'),
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Recurring',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    benefitCaseNumber: 'asdf',
    accountTransferInfo: undefined,
    chequePaymentInfo: { chequeNumber: '0891274' },
    rootCaseNumber: 'asdf'
  } as Payment,
  {
    paymentId: 'CLM-2',
    casePayment: 'asdf',
    claimId: 'asdf',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: moment('2050-01-01'),
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Recurring',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    benefitCaseNumber: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: undefined,
    rootCaseNumber: 'asdf'
  } as Payment,
  {
    paymentId: 'CLM-2',
    casePayment: 'asdf',
    claimId: 'asdf',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: moment('2050-01-01'),
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Recurring',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    benefitCaseNumber: 'asdf',
    accountTransferInfo: undefined,
    chequePaymentInfo: { chequeNumber: '0891274' },
    rootCaseNumber: 'asdf'
  } as Payment
];

export const GROUP_CLIENT_SINGLE_PAYMENT_FIXTURE = [
  {
    paymentId: 'PYM-1',
    benefitCaseNumber: 'BNF-1',
    rootCaseNumber: 'RT-1',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: null,
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Adhoc',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: { chequeNumber: '0891274' },
    dateInterfaceRecordCreated: moment('2050-01-01'),
    benefitCaseTypeName: 'asdf',
    benefitRightTypeName: 'asdf'
  } as GroupClientPayment
];

export const GROUP_CLIENT_PAYMENTS_FIXTURE = [
  {
    paymentId: 'PYM-1',
    benefitCaseNumber: 'BNF-1',
    rootCaseNumber: 'RT-1',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: null,
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Adhoc',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: { chequeNumber: '0891274' },
    dateInterfaceRecordCreated: moment('2050-01-01'),
    benefitCaseTypeName: 'asdf',
    benefitRightTypeName: 'asdf'
  } as GroupClientPayment,
  {
    paymentId: 'PYM-1',
    benefitCaseNumber: 'BNF-1',
    rootCaseNumber: 'RT-1',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: null,
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Adhoc',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: { chequeNumber: '0891274' },
    dateInterfaceRecordCreated: moment('2050-01-01'),
    benefitCaseTypeName: 'asdf',
    benefitRightTypeName: 'asdf'
  } as GroupClientPayment,
  {
    paymentId: 'PYM-1',
    benefitCaseNumber: 'BNF-1',
    rootCaseNumber: 'RT-1',
    paymentDate: moment('2050-01-01'),
    periodStartDate: moment('2050-01-01'),
    periodEndDate: null,
    effectiveDate: moment('2050-01-01'),
    paymentType: 'Adhoc',
    payeeName: 'fsafd',
    paymentMethod: 'Check',
    paymentAmount: '5342.65',
    paymentAddress: 'asdf',
    nominatedPayeeName: 'asdf',
    accountTransferInfo: {
      bankAccountNumber: '0',
      bankCode: 'bankCode',
      bankBranchSortCode: 'bankBranchSortCode',
      bankAccountType: 'bankAccountType',
      bankInstituteName: 'jo'
    },
    chequePaymentInfo: { chequeNumber: '0891274' },
    dateInterfaceRecordCreated: moment('2050-01-01'),
    benefitCaseTypeName: 'asdf',
    benefitRightTypeName: 'asdf'
  } as GroupClientPayment
];
