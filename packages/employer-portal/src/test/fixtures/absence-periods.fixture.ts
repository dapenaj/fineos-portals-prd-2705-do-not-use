/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientPeriodDecisions,
  CaseHandler,
  Decision,
  GroupClientAbsence,
  GroupClientEmployee,
  GroupClientLeavePlan,
  GroupClientLeaveRequest,
  GroupClientPeriod
} from 'fineos-js-api-client';
import moment from 'moment';

import {
  EnhancedAbsencePeriodDecisions,
  PeriodType,
  PeriodStatus
} from '../../app/shared';

export const SUMMARY_ABSENCE_PERIOD_DECISIONS_FIXTURE = [
  {
    decisions: [
      {
        period: {
          startDate: moment(new Date('2019-01-01')),
          type: PeriodType.TIME_OFF,
          status: PeriodStatus.EPISODIC
        } as GroupClientPeriod
      }
    ] as Decision[]
  } as GroupClientPeriodDecisions
] as GroupClientPeriodDecisions[];

export const EMPTY_DECISIONS_FIXTURE = [
  {
    absenceId: 'ABS-1',
    caseHandler: {} as CaseHandler,
    absencePeriodDecisions: {
      decisions: [] as Decision[]
    } as GroupClientPeriodDecisions
  }
] as EnhancedAbsencePeriodDecisions[];

export const ABSENCE_FOR_TIME_TAKEN_FIXTURE = {
  startDate: moment(new Date('2020-02-02')),
  endDate: moment(new Date('2020-04-09')),
  decisions: [
    {
      absence: {
        id: 'NTN-63-ABS-01'
      },
      period: {
        parentPeriodReference: '',
        startDate: moment(new Date('2020-04-05')),
        endDate: moment(new Date('2020-04-09')),
        timeDecisionStatus: 'Approved',
        type: 'Reduced Schedule',
        leavePlan: {
          id: '99174fc9-e0da-45a8-95d6-600b13028492',
          name: 'civic duty plan',
          adjudicationStatus: 'Accepted',
          calculationPeriodMethod: 'Rolling Back'
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          reasonName: 'Civic Duty',
          qualifier1: 'Jury',
          qualifier2: '',
          decisionStatus: 'Approved'
        }
      }
    } as Decision,
    {
      absence: {
        id: 'NTN-63-ABS-01'
      },
      period: {
        parentPeriodReference: '',
        startDate: moment(new Date('2020-02-02')),
        endDate: moment(new Date('2020-02-06')),
        timeDecisionStatus: 'Approved',
        type: 'Time off period',
        status: 'Known',
        leavePlan: {
          id: '99174fc9-e0da-45a8-95d6-600b13028492',
          name: 'civic duty plan',
          adjudicationStatus: 'Accepted',
          calculationPeriodMethod: 'Rolling Back',
          leavePlanType: {
            fullId: 216128011,
            leavePlanName: 'civic duty plan'
          }
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          reasonName: 'Civic Duty',
          qualifier1: 'Jury',
          qualifier2: '',
          decisionStatus: 'Approved'
        }
      }
    } as Decision,
    {
      absence: {
        id: 'NTN-63-ABS-01'
      },
      period: {
        parentPeriodReference: '',
        startDate: moment(new Date('2020-02-02')),
        endDate: moment(new Date('2020-02-06')),
        timeDecisionStatus: 'Approved',
        type: 'Time off period',
        status: 'Known',
        leavePlan: {
          id: '23b0634b-8bf6-4b20-a13a-0b52f0d81845',
          name: 'FMLA Covid Limit',
          shortName: 'FMLA Covid Limit',
          adjudicationStatus: 'Accepted',
          calculationPeriodMethod: 'Rolling Back',
          leavePlanType: {
            fullId: 216128014,
            leavePlanName: 'Informational'
          }
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          reasonName: 'Civic Duty',
          qualifier1: 'Jury',
          qualifier2: '',
          decisionStatus: 'Approved'
        }
      }
    } as Decision,
    {
      absence: {
        id: 'NTN-63-ABS-01'
      },
      period: {
        parentPeriodReference: '',
        startDate: moment(new Date('2020-03-08')),
        endDate: moment(new Date('2020-03-15')),
        timeDecisionStatus: '',
        type: 'Episodic',
        status: 'Episodic',
        leavePlan: {
          id: '99174fc9-e0da-45a8-95d6-600b13028492',
          name: 'civic duty plan',
          adjudicationStatus: 'Accepted',
          calculationPeriodMethod: 'Rolling Back'
        },
        leaveRequest: {
          id: 'PL:14432:0000000403',
          reasonName: 'Civic Duty',
          qualifier1: 'Jury',
          qualifier2: '',
          decisionStatus: 'Approved'
        }
      }
    } as Decision
  ] as Decision[]
} as GroupClientPeriodDecisions;

export const CONCLUDED_ASSESSMENTS_FIXTURE = [
  {
    absenceId: 'ABS-1',
    caseHandler: {} as CaseHandler,
    absencePeriodDecisions: {
      startDate: moment(new Date('2020-01-01')),
      endDate: moment(new Date('2020-08-12')),
      decisions: [
        {
          absence: {
            id: '5',
            caseReference: 'ABS-2'
          } as GroupClientAbsence,
          employee: {
            id: 'EMP-3',
            name: 'Qui Gon Jinn'
          } as GroupClientEmployee,
          period: {
            periodReference: '',
            parentPeriodReference: '',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-01-01')),
            endDate: moment(new Date('2020-08-12')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: '',
            timeDecisionReason: '',
            type: 'Reduced Schedule',
            status: 'Episodic',
            leavePlan: {
              paidLeaveCaseId: '',
              name: '',
              shortName: '',
              calculationPeriodMethod: ''
            } as GroupClientLeavePlan,
            leaveRequest: {
              reasonName: 'Child Bonding',
              decisionStatus: 'Denied'
            } as GroupClientLeaveRequest
          }
        }
      ]
    }
  }
] as EnhancedAbsencePeriodDecisions[];

export const PENDING_ASSESSMENTS_FIXTURE = [
  {
    absenceId: 'ABS-1',
    caseHandler: {} as CaseHandler,
    absencePeriodDecisions: {
      startDate: moment(new Date('2020-01-01')),
      endDate: moment(new Date('2020-08-12')),
      decisions: [
        {
          absence: {
            id: '5',
            caseReference: 'ABS-2'
          } as GroupClientAbsence,
          employee: {
            id: 'EMP-3',
            name: 'Qui Gon Jinn'
          } as GroupClientEmployee,
          period: {
            periodReference: '',
            parentPeriodReference: '',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-01-01')),
            endDate: moment(new Date('2020-08-12')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: '',
            timeDecisionReason: '',
            type: 'Reduced Schedule',
            status: 'Episodic',
            leavePlan: {
              paidLeaveCaseId: '',
              name: '',
              shortName: '',
              calculationPeriodMethod: ''
            } as GroupClientLeavePlan,
            leaveRequest: {
              reasonName: 'Child Bonding',
              decisionStatus: 'Pending'
            } as GroupClientLeaveRequest
          }
        }
      ]
    }
  }
] as EnhancedAbsencePeriodDecisions[];

export const ABSENCE_PERIOD_DECISIONS_FIXTURE = [
  {
    absenceId: 'ABS-2',
    caseHandler: {
      name: 'Kylo Ren',
      telephoneNo: '01-111-123456',
      emailAddress: 'kyloren@thefirstorder.com'
    },
    absencePeriodDecisions: {
      startDate: moment(new Date('2020-02-05')),
      endDate: moment(new Date('2020-06-06')),
      decisions: [
        {
          absence: {
            id: '1',
            caseReference: 'ABS-2'
          },
          employee: {
            id: 'EMP-1',
            name: 'Luke Skywalker'
          },
          period: {
            periodReference: '',
            parentPeriodReference: '',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-02-05')),
            endDate: moment(new Date('2020-06-06')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: 'Approved',
            timeDecisionReason: '',
            type: 'Reduced Schedule',
            status: 'Approved',
            leavePlan: {
              paidLeaveCaseId: '',
              name: 'Federal FMLA',
              shortName: 'FMLA',
              adjudicationStatus: 'Accepted'
            },
            leaveRequest: {
              reasonName: 'Pregnancy/Maternity',
              decisionStatus: 'Approved',
              qualifier1: 'Prenatal Care'
            }
          }
        },
        {
          absence: {
            id: '2',
            caseReference: 'ABS-2'
          },
          employee: {
            id: 'EMP-2',
            name: 'Darth Vader'
          },
          period: {
            periodReference: '',
            parentPeriodReference: '',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-01-01')),
            endDate: moment(new Date('2020-12-08')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: 'Time Available',
            timeDecisionReason: '',
            type: 'Episodic',
            status: 'Episodic',
            leavePlan: {
              paidLeaveCaseId: '',
              name: 'Federal FMLA',
              shortName: 'FMLA',
              adjudicationStatus: 'Accepted',
              leavePlanType: {
                fullId: 216128012,
                leavePlanName: 'Federal FMLA'
              }
            },
            leaveRequest: {
              reasonName: 'Pregnancy/Maternity',
              decisionStatus: 'Approved',
              qualifier1: 'Other'
            }
          }
        },
        {
          absence: {
            id: '3',
            caseReference: 'ABS-2'
          },
          employee: {
            id: 'EMP-3',
            name: 'Qui Gon Jinn'
          },
          period: {
            periodReference: '',
            parentPeriodReference: '1',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-01-01')),
            endDate: moment(new Date('2020-06-01')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: '',
            timeDecisionReason: '',
            type: 'Time off period',
            status: 'Cancelled',
            leavePlan: {
              paidLeaveCaseId: '',
              name: 'Federal FMLA',
              shortName: 'FMLA',
              category: 'Paid'
            },
            leaveRequest: {
              reasonName: 'Pregnancy/Maternity',
              qualifier1: 'Postnatal Disability'
            }
          }
        },
        {
          absence: {
            id: '4',
            caseReference: 'ABS-2'
          },
          employee: {
            id: 'EMP-3',
            name: 'Qui Gon Jinn'
          },
          period: {
            periodReference: '',
            parentPeriodReference: '',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-01-01')),
            endDate: moment(new Date('2020-03-25')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: 'Pending',
            timeDecisionReason: '',
            type: 'Time off period',
            status: '',
            leavePlan: {
              paidLeaveCaseId: '',
              name: 'Federal FMLA',
              shortName: 'FMLA',
              category: 'Paid',
              adjudicationStatus: 'Accepted',
              leavePlanType: {
                fullId: 216128012,
                leavePlanName: 'Federal FMLA'
              }
            },
            leaveRequest: {
              reasonName: 'Pregnancy/Maternity',
              decisionStatus: 'Approved',
              qualifier1: 'Postnatal Disability'
            }
          }
        },
        {
          absence: {
            id: '4',
            caseReference: 'ABS-2'
          },
          employee: {
            id: 'EMP-3',
            name: 'Qui Gon Jinn'
          },
          period: {
            periodReference: '',
            parentPeriodReference: '',
            relatedToEpisodic: false,
            startDate: moment(new Date('2020-01-01')),
            endDate: moment(new Date('2020-03-25')),
            balanceDeduction: 0,
            timeRequested: '',
            timeDecisionStatus: 'Pending',
            timeDecisionReason: '',
            type: 'Time off period',
            status: '',
            leavePlan: {
              id: '23b0634b-8bf6-4b20-a13a-0b52f0d81845',
              name: 'FMLA Covid Limit',
              shortName: 'FMLA Covid Limit',
              leavePlanType: {
                fullId: 216128014,
                leavePlanName: 'Informational'
              },
              paidLeaveCaseId: '',
              category: 'Non-Paid',
              adjudicationStatus: 'Undecided'
            },
            leaveRequest: {
              reasonName: 'Pregnancy/Maternity',
              decisionStatus: 'Approved',
              qualifier1: 'Postnatal Disability'
            }
          }
        }
      ]
    }
  }
] as EnhancedAbsencePeriodDecisions[];

export const DECISION_STATUS_VARIATIONS_FIXTURE = [
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-01')),
      endDate: moment(new Date('2020-02-03')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'Federal FMLA'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Pending'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-05')),
      endDate: moment(new Date('2020-02-07')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'Federal FMLA'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Cancelled'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-09')),
      endDate: moment(new Date('2020-02-11')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'Federal FMLA'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Denied'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-13')),
      endDate: moment(new Date('2020-02-15')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'Federal FMLA',
        adjudicationStatus: 'Rejected'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-17')),
      endDate: moment(new Date('2020-02-19')),
      timeDecisionStatus: 'Pending',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'Federal FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-21')),
      endDate: moment(new Date('2020-02-23')),
      timeDecisionStatus: 'Approved',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'Federal FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-25')),
      endDate: moment(new Date('2020-02-27')),
      timeDecisionStatus: 'Denied',
      type: 'Reduced Schedule',
      status: 'Approved',
      leavePlan: {
        name: 'Federal FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-02-29')),
      endDate: moment(new Date('2020-03-02')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Cancelled',
      leavePlan: {
        name: 'Federal FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-04')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Episodic',
      leavePlan: {
        name: 'Federal FMLA',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Pregnancy/Maternity',
        decisionStatus: 'Approved'
      }
    }
  }
] as Decision[];

export const CONSECUTIVE_STATUS_VARIATIONS_FIXTURE = [
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Episodic',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Denied'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-07')),
      endDate: moment(new Date('2020-03-08')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Other',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Denied'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-06-09')),
      endDate: moment(new Date('2020-06-12')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Approved',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Denied'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Episodic',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Pending'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Episodic',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Cancelled'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Rejected'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Undecided'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: 'Denied',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Cancelled',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Approved'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: 'FinCorp Plan',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Approved'
      }
    }
  }
] as Decision[];

export const DECISIONS_WITHOUT_PLAN_NAME_FIXTURE = [
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: '',
      leavePlan: {
        name: '',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Projected'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Episodic',
      leavePlan: {
        name: '',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Denied'
      }
    }
  },
  {
    period: {
      parentPeriodReference: '',
      startDate: moment(new Date('2020-03-01')),
      endDate: moment(new Date('2020-03-06')),
      timeDecisionStatus: '',
      type: 'Reduced Schedule',
      status: 'Episodic',
      leavePlan: {
        name: '',
        adjudicationStatus: 'Accepted'
      },
      leaveRequest: {
        reasonName: 'Child Bonding',
        decisionStatus: 'Other'
      }
    }
  }
] as Decision[];
