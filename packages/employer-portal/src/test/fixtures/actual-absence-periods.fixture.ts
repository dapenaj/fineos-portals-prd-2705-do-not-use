/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import { ActualAbsencePeriod } from 'fineos-js-api-client';

export const ACTUAL_ABSENCE_PERIODS_FIXTURE = [
  {
    id: '14449-64',
    actualDate: moment(new Date('2020-01-14')),
    episodePeriodDuration: 2,
    episodePeriodBasis: {
      domainId: 6757,
      fullId: 216224002,
      name: 'Hours',
      domainName: 'TimeUnits'
    },
    additionalNotes: '',
    status: {
      domainId: 6821,
      fullId: 218272003,
      name: 'Pending',
      domainName: 'DecisionStatus'
    },
    managerAccepted: {
      domainId: 2500,
      fullId: 80000000,
      name: 'Unknown',
      domainName: 'YesNoUnknown'
    },
    reportingPartyName: 'Ms Claimant Ten'
  } as any,
  {
    id: '14449-64',
    actualDate: moment(new Date('2020-01-14')),
    episodePeriodDuration: 2,
    episodePeriodBasis: {
      domainId: 6757,
      fullId: 216224002,
      name: 'Hours',
      domainName: 'TimeUnits'
    },
    additionalNotes: '',
    status: {
      domainId: 6821,
      fullId: 218272003,
      name: 'Pending',
      domainName: 'DecisionStatus'
    },
    managerAccepted: {
      domainId: 2500,
      fullId: 80000000,
      name: 'Unknown',
      domainName: 'YesNoUnknown'
    },
    reportingPartyName: 'Ms Claimant Ten'
  } as any
] as ActualAbsencePeriod[];
