import { CustomerAbsenceEmployment } from 'fineos-js-api-client';
import moment from 'moment';

export const ABSENCE_EMPLOYMENT_FIXTURE = [
  {
    id: '14453-14',
    adjustedHireDate: moment(new Date('2020-08-08')),
    cbaValue: '',
    withinFMLACriteria: true,
    hoursWorkedPerYear: 5555,
    keyEmployee: false,
    workingAtHome: false,
    employmentType: {
      domainId: 6772,
      fullId: 216704000,
      name: 'Please Select'
    },
    employmentWorkState: {
      domainId: 138,
      fullId: 4416033,
      name: 'NY'
    },
    employmentClassification: {
      fullId: 216640000,
      name: 'Please Select'
    },
    occupationQualifiers: [
      {
        id: '1',
        qualifierDescription: 'test1'
      },
      {
        id: '2',
        qualifierDescription: 'test2'
      }
    ]
  }
] as CustomerAbsenceEmployment[];

export const EMPTY_ABSENCE_EMPLOYMENT_FIXTURE = {
  id: '14453-14',
  adjustedHireDate: null,
  cbaValue: null,
  withinFMLACriteria: null,
  hoursWorkedPerYear: null,
  keyEmployee: null,
  workingAtHome: null,
  employmentType: {
    domainId: 6772,
    fullId: 216704000,
    name: 'Please Select'
  },
  employmentWorkState: {
    domainId: 138,
    fullId: 4416033,
    name: 'Unknown'
  },
  employmentClassification: {
    domainId: 6770,
    fullId: 216640000,
    name: 'Please Select'
  },
  occupationQualifiers: []
} as any;
