/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { ClientConfig } from '../../app/shared';

export const EMPTY_CONFIG_FIXTURE = {
  theme: {
    colorSchema: {}
  }
} as ClientConfig;

export const CONFIG_FIXTURE = {
  theme: {
    colorSchema: {
      primaryColor: '#932b6a',
      primaryColorContrast: '#ffffff',
      callToActionColor: '#e27816',
      primaryAction: '#007bc2',
      successColor: '#35b558',
      pendingColor: '#ffc20e',
      attentionColor: '#f04c3f',
      infoColor: '#23b7d6',
      neutral9: '#333333',
      neutral8: '#666666',
      neutral7: '#999999',
      neutral6: '#b3b3b3',
      neutral5: '#d2d0d5',
      neutral4: '#e9e7ec',
      neutral3: '#e6e6e6',
      neutral2: '#f4f5f6',
      neutral1: '#ffffff',

      graphColorOne: '#932b6a',
      graphColorTwo: '#00aabc',
      graphColorThree: '#ffc20e',
      graphColorFour: '#35b558',
      graphColorFive: '#f6861f',
      graphColorSix: '#708dd8'
    },
    typography: {
      displayTitle: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 36,
        lineHeight: 44
      },
      displaySmall: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 24,
        lineHeight: 30
      },
      pageTitle: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 24,
        lineHeight: 30
      },
      panelTitle: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 25
      },
      baseText: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 19
      },
      baseTextSemiBold: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 14,
        lineHeight: 19
      },
      smallText: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 16
      },
      smallTextSemiBold: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 12,
        lineHeight: 16
      },
      baseTextUpperCase: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 19,
        textTransform: 'uppercase'
      },
      smallTextUpperCase: {
        fontFamily: '"Open Sans"',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 11,
        lineHeight: 15,
        textTransform: 'uppercase'
      }
    },
    header: {
      logo: '/assets/img/ER_signature_white.svg',
      hideTitle: true
    },
    images: {
      somethingWentWrong: '/assets/img/something_went_wrong_grey.svg',
      somethingWentWrongColor: '/assets/img/something_went_wrong_color.svg',
      uploadFail: '/assets/img/upload_fail.svg',
      uploadSuccess: '/assets/img/upload_success.svg',
      welcomeBackground: '/assets/img/hexagons_pattern_ER.svg',
      greenCheckIntake: '/assets/img/green_check_intake.svg',
      fnolRequestAcknowledged: '/assets/img/fnol_request_acknowledged.svg'
    },
    highContrastMode: {
      colorSchema: {
        primaryColor: '#932b6a',
        callToActionColor: '#e27816',
        primaryAction: '#007bc2',
        successColor: '#35b558',
        pendingColor: '#ffc20e',
        attentionColor: '#f04c3f',
        infoColor: '#23b7d6',

        neutral9: '#333333',
        neutral8: '#666666',
        neutral7: '#999999',
        neutral6: '#b3b3b3',
        neutral5: '#d2d0d5',
        neutral4: '#e9e7ec',
        neutral3: '#e6e6e6',
        neutral2: '#f4f5f6',
        neutral1: '#ffffff',

        graphColorOne: '#932b6a',
        graphColorTwo: '#00aabc',
        graphColorThree: '#ffc20e',
        graphColorFour: '#35b558',
        graphColorFive: '#f6861f',
        graphColorSix: '#708dd8'
      },
      typography: {
        displayTitle: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 300,
          fontSize: 36,
          lineHeight: 44
        },
        displaySmall: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 300,
          fontSize: 24,
          lineHeight: 30
        },
        pageTitle: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 600,
          fontSize: 24,
          lineHeight: 30
        },
        panelTitle: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 'normal',
          fontSize: 18,
          lineHeight: 25
        },
        baseText: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 'normal',
          fontSize: 14,
          lineHeight: 19
        },
        baseTextSemiBold: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 600,
          fontSize: 14,
          lineHeight: 19
        },
        smallText: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 'normal',
          fontSize: 12,
          lineHeight: 16
        },
        smallTextSemiBold: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 600,
          fontSize: 12,
          lineHeight: 16
        },
        baseTextUpperCase: {
          fontFamily: '"Open Sans"',
          fontStyle: 'normal',
          fontWeight: 'normal',
          fontSize: 14,
          lineHeight: 19,
          textTransform: 'uppercase'
        },
        smallTextUpperCase: {
          fontFamily: '"Roboto"',
          fontStyle: 'italic',
          fontWeight: 'normal',
          fontSize: 11,
          lineHeight: 15,
          textTransform: 'uppercase'
        }
      },
      header: {
        logo: '',
        hideTitle: true
      },
      icons: {
        'MESSAGES.EMPLOYER': `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" aria-label="MESSAGE.EMPLOYER icon">
          <path fill="currentColor" d="M248 8C111 8 0" />
        </svg>`,
        'MESSAGES.CARRIER': `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" aria-label="MESSAGE.CARRIER icon">
          <path fill="currentColor" d="M248 8C111 8 0" />
        </svg>`
      }
    }
  },
  configurableSizeList: {
    PAYMENTS_HISTORY: 12
  },
  configurableSessionTimeoutSeconds: {
    SESSION_TIMEOUT_SECONDS: 30
  },
  configurableWorkPattern: {
    FIXED_DAY_LENGTH: { hours: 8, minutes: 0 }
  },
  features: {
    showDashboard: true,
    helpInfoEnabled: true
  }
} as ClientConfig;

export const IMAGE_BASE_64_FIXTURE = {
  name: '6c95173fc0d581a7ea12df04d78611f2b0ec8667',
  type: 'jpg',
  file:
    '/9j/4AAQSkZJRgABAQAASABIAAD/4QCgRXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgExAAIAAAAHAAAAWodpAAQAAAABAAAAYgAAAAAAAABIAAAAAQAAAEgAAAABR29vZ2xlAAAABJAAAAcAAAAEMDIyMKABAAMAAAABAAEAAKACAAQAAAABAAAAm6ADAAQAAAABAAAAdAAAAAD/wAARCAB0AJsDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9sAQwACAgICAgIDAgIDBAIDAwQFBAQEBAUGBQUFBQUGBwYGBgYGBgcHBwcHBwcHCQkJCQkJCgoKCgoMDAwMDAwMDAwM/9sAQwECAgIDAwMFAwMFDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/90ABAAK/9oADAMBAAIRAxEAPwD9sPNuGuCAwPPBFW3vrm2Debls/jXCXmsuQTGjpj+Jc1XtfEt1G3+kMJY/9rg/yrZRucXPY7U3EWp/unUoR6ipl08wqUtMOT1U8GueTxHpjfNkJ7kZP51ai8SaRayF7i5GWHytkf0xT1WyEpLqzTZWEfl39qcf3uv51yN9ZaCZ/OJaFs8FeK1b/XFvUxDMjq3dSRXC3U1vbebK82SB0znk9PWtKd+opvsdFb2yPIr2581Y8kbz1HFR6xdxoMlvLO3hAO54rgT4xkhAS5haNF4DLlePyrL1Xx1bzILa1zdMSOQPuj6nrW6g27mbmkjcl1C6g+Xyy+eR3p0V6AhmYbn756j2rB0rVbW6dDdR3CMTglRkD8TW/qsMdjai8gzMjcgMACPyH9atys7GerVy3BrFncwvBKrOx6HHT2ritQ0hDdeYGQqTnaat2mtm9YKYvso/vAYz+XFWLq7Rf+Xfzm/565ojNqRVk1qc5LZvFIu2JNuMZHX8afbPLbH93H1br1qfZJcyl24Qdl5qJo9hMisYwvbdzXT7UzlTXQ7PTtTtYkDTgqQMgN0FTtr0Kgsu2Pn8TXnj3cczbDK2e4zx+dOMDOp8psgDJ+bp+nNZy7lxfQ6q51+ZzmN2Uema56bVb5wSQdueteeeJPGPhnwwu/WNZt7c84iV/MlyB02L3+uK8U139oeJQYPDNjLeEdJrtvLX6iNeT+JFZTxdGmtWWqNSeyPqaPUmc7pFZscc9qVtf0qNij3ESMOoMiZH61+e2vfE/wAfeIg0V5qstnA//LGyAt0/F1/eH/vqvOZLGOZzLNH57ucs7kszH1JJJJrzamcRT9xXOungJNas/9D9PrnUr0sTBJt+h4/UVnzXGqSWouYZlulDmOVcDMb4yAee4xiupXTfMGFlVAeg21jT6W9hqccEz7otXxFtwQFmiGUfjgBhxnNbTqpWZwU6Tls9Tkp9UvGUxk7fZRg/41gyXM+Tu3nHc/8A169Sm8MSbv8ASoHhJ7qQenBGKy7rwhbycxyeRn++QK6Y1omPs20ef2/iLULY+XGRIB2Y1aPiu7lVoZUWJjgk5yDj36iugfwbKG+WaOX2BA/U1RufCF0M+Wm4+zKf61ftYdSeSZRXxbBKnl6jbR3q4x8jYplvfaFv8yG3FsGPCyPx+eKoT+E9WUkm2Ix3xjiqL+FtdKlltmkHYCj2sejGlLqj0aLxVpUKhfJMG3HCHch981ej8TaLJA3mAtGeq7i36YrxKfTNatm2zW0kQ/SnwWF+43CCUD/Z5z+VDUHrcaclpY9Qt/EXg+6n+y2V2I5XD/upAQGERCvzjAIPGOvpWvA2lyuWcqMdFVzj8jXzvN4dv7fxHDq8k82Fik/c+UoLHI2ndn+AZHvXRrqF7F90yoPXgU42ezFJ2ex7RLcaaqbYm8s46+tY6JaFmeU7wenOPzrye813Uba3EtpDJqMhdUKBgMbuMknjFb9g7yxo1wjiQgeYobgN3Hp1pP1GpHUXVvaPkwr5bdjkEfpVBFurdw4lBA56Y6VYPkxp5aEwn/eB/rVdLZncKzMdxxnB4z3PFNVNLMpLW6Pzd1dBNr2p3HBeS8nJY9TmQ9WPJqERDAbpz1PT8TXsEnw5c6nqLztJIi3FyVYjarDJII/P9K6e28D+HrW2eKZBwkfzsyqTnBPLEV8rVs5O53/Wox0hF3PAI7OSXJjjafaMkICxx64Gc12umeCdVvLGK5+yTLvyQChHAYgV7GqeBdKmuZX1CBXk2qE85CEwT90Rlq9U8PReH9U0a2vbScywuGVW+cZ2MUPBAPVT9ay5YlKvUe0D/9H9ppbO267wCPfP4fWvLviRd6Xo9vYNd30VpLeTPa2YZgBJdfLIiKO7EIcV8A3n7W3jnUdTZtL1nw/pNusW1rcQSGZZQoG8SySSjbnJKlO/3q9i+GHjuH4j6JbH4u+MNJ8TXdpfLc29jYILSKCSIfu5C6ASM7A5PIUcLg43HCp70dWcNGk4T5kfb2mSrf20FyNnmSxJIwbBIbGGH4Nkmrs9leSE+SbY47EKP51wXn6ZON8MpZdoYFWYjkcfdJ6/rUhsoTD5gk4zgk7sClGei95F8i5mdHc2WsoFHmWse87VG5AC3YVVMGup1a3OO++P8jxXKXlo0cDSW4F02cbFYZ2577sZA64rFlstSlGF09uWAX94mNvq3vj0FW6lt2PlR3EV7rFwHaIwARSNEdzRLllxnGRyOeD3qtPc6oxPmtag+0kQ/kK4zU9KisbZ7u7dYo4lBZmJ2r/M9eB71xts9lql3NaW29JYFV5VkjZGRHzscqRkq2DjFS6se5apytomenT6lFERFfzW0eR2dW/UVlz3un+S81vd2+1WCkdW+buB3FeUajf6bZQzXYvFe3gYo88SsyBgcEHCk5B4P86wtP8AEGj66pk0jUBqSKA5aLcVAPQhioBB7Yq1VhYOSr0R6BqV0jX0TK1vOvlSjO3gFipGffg1z1w7gExxW+4DPy8GsaC/t76Mz2V4t5FuZNyPuG5CVYZHGVYEEZ6isl9Y02Sc2639s8gO3b5qFg3Tb1654I61ccTFdDN0aktLaj9UvdUMSxwoUdnQIQoIXLDOQPbuaG1O/XpFj3YDFIlrdXCtLFsZc7dzP5ZJHXA74+tYupNNppRZ90W8EqyOXUgHBIIz39q3WKh1RzypTtodFHqusn7gVSf89TSXWsazHDK0s6xGONm+ZgMYU8jnj2rznVL7+0rf7JfyPdQ/3fNkQ8+jRsjD868O8YfD621K1uZPD16umX7IRF/aSSahaqwzgMrSJJjP+2cehrR100+WIU4a+9IybnX9Sv5pDdapLcuXYENOcksx7Z9KjURuwaVhJjjJIJ/xr86/iPpfxY8CazJd+Orf7MX4t76yw1huYgYhmijiaInOAJVUnOFLE1q6t4nk8N6/a6rZIFKWKMyMzmNicZLpuAY47nmvHng5dT1I1o8unQ/QdwkaZ3IGxwN4yT2GM1oeFf2vPh58NtCt/BXiGQ3GoaY0yztI6ByZZnmUENyMLIB9BXwzp/xS1+9jsLgWNhLaT3axboSwlCA4JIZvlOfrmvn34ta9dxfETWktLdbiISx7XaXaTmGM8jB71j9XafvFQxEZI//S5+D4K/BXXGW10+88V2LTFUG+2tmJI6BpEmQ89zmvqb4c/s06p4V1KHULU32t6eLcW81readBzAuSm14breGGT8x3ZBrF+G/he98Q3F7eWqeV/Z0IlaMkb3wedgHXYOWr6r8YatD8PvAV14p1PURoug6Zbebc6jPKQsmF3FtxI3Z7enTtXNCqptRkQ1a/Keb6p4u+Cvg7U/7A8Ta8/ge+ZcpBcT3Nk7KOMoJMo6rxypIFd/4V8c/AqZC4+If24ZwqC/hdOO5LEEn6ivxc+Kn7fegeIdQlsNN8MzeLNMVion1MxxF067o43zIATjG4L9K5DRvGHw6+NV9HfaV4S0+11CJYlnt5SsDwxxnLrDLGrqsM6kh3UlgP4SeK0qYenHVL8RR5noz+hNNT8C3v7zRfHFq6E5CTrBKB/wACRgTWrarZu3za9pt6DyDGhQnP/AwK/AG9+Fn2cyTaRcugLnyooLt12p/32uSKxIbPxTpM4/4neo2KA/eNxOMDP94swrlUqEt0aunNdT97PiBonjCDTLPVvDunR+K7W3vYZ547aQb2iTduwryEEqxV9vU7eBnFeEW/xQ1TUNd8QeJrPRNT8gyW2kW7vGYiZlZjcFlkXfEsW4ffUE8soIHPzD+zj8YbrwHM+gza1q3ia31EtcPEzPdRROuN0jPyY1ORgE9sgYBr6u/4XB4OttYuNSR4bO9v1jW5Y4VpVjzsLA8HGSMgZrmqw5H7p2UK3LHlZxtl8c9F8LaH4zt9a0yex/4Rya7vCkke2KS2aIzKySyBA8eOC3QHPpXSaV8Zvhxa+G/D9tp9kkn9r26eVbWqR7lt3jEkjoQwyqKw5z1PFeiWPxG0bWE8t0glT5d4mjRkePuox3x061rNZeA9VeOe60qwneNSsb+SgKK3ULjoD0OK5faJb6HT7ZNXa63PNfhd8RfgS+j6tpkFg2kW/hu6ngurbISWNniFyZGDYAaQShhyc5znrXz9rHhz4EePLXTfh/8ADi/fwxHq0x8TNc3Lxi6us3DSStBJGcly33vmAC896+om+C3wXnvdW1WHw9b6fea7EkOo3FqxikuVjQxp5hUfMVQ4BJyBXnl3+yB8GJ9O8NafpUmq+Hx4SbOky217IZbdcbTGXZgZEZeCrkgjrQqq7nVGtSjzOK1f5ljXP7N0WSZLhvtdvZ20cUIfOd7D/WbhkM2cEcgZrk9XvbZrlbSKZQLeNU2nA5xlvTqa9O1D4MXdxvaDVY7jJ4EjSRg4+7uwrLn1wa4rxJ8JVsZLi71DxTp1nMsf2qaFkcuiM2NwRQXZN3AbHNddOutjwqtCV3ZHGbIpMnIPHY8V8dfHX44eIvh34rh8OaJoM9xbG2SZ9QltbqaF5ZGIEUJgQqxjAy+WB+YYHXH3ZB8HPFsjR/2ZqNlfpIhdX/fIm0AH5i6AKSGGATkjpVa6+C3xGzlo7e7GSQFuImA4xxvPFdNOvFPVmcIODu4n5sS/FbxN418KPdHQE1OwwbXWdNurG6BO/hZ4vMCl4ZM7cqpaNiM5GSvyv8QVmWWW/W1mtLIQMsZdJNsSg8JI7ou0jpluvav22m+EPxDjQp/ZBnHP3ZYDn2wG6V574j+DPiy+tZbHUvDct1bTjY0bJGwYH1wcV2qtCWpjN6u0T8X9Fk8m0s5BO0ZS73Fc4bG84BHHAHQ+lVPG8dvqXim/vZVZmldCSqEg4jUdfwr6h+MH7OfiL4bRnxHbaZc6PoiTDezBWW3LEBVbBJ8o9mPC+wFfMurrqU2pTyQyT7CwxsAK8ADjis3G72Jp2Wx//9P1PQPG8WkX0EmmXn2GVZhIsqnBIAwV9gw49x1r56/4KOfG/W/FHwg8MeBNLYQ6el4JNTWIYEjg5UMo42lsHHTmuWu5NYjyY0Ee3/bP6YxXmPje11LxPp7WOqxLdJ0wxPIHT0544Oc14tCaVROR0Siuh+cl5DD/AGbEJreO0uMEja2ZZB3eUHoB2r0z4Sz65oV5P4j0qOWVrcRoFQZMhZxuTB4IKZz7V3h+C2jpeGeS4u2QsWMReNsc527zFuI5OSSW98816Bp+lrpdumm6bbf2fbxDCImTz3JY8szd2PNevKpCUXZ3MbNPU7a5+Jc9189tp91ahv7wAP5VhzePdVSPywZlXOdrYP8A9ep7fQbi6HzF8HtzWlH4GWU5KEn16fnmvOWH6mqrLY4K48b6km57G5u9Ikb5We0kMLsP9ooRkfWuPfxFr0ly1xJrGoTsT1mlZ8j0z/KvdE+HlmT++RgPVRk/yq4vww0uYZibn0frWqXLo2KUr7HCeHPin8QNEZW03VZtq9FkbeMfQ19DeFf2oPiHp+xdRijv1x1VirfkeK8vf4ZSRZMMRbHAK5/WmL4OurVclMEf3uKmUaUt0JSktmfb/h39quSUIdStJ7QHjPDDj/dzXtWi/tG+H7pEE1yICx/jyv8AOvzDt9LvrVhK0LZP9xiMfkRW8JFKYvFmQ+7Mf51ySw9LozdTn1R+sdl8ZNCukDJdxtn/AGua5/xD41XU71Zng07xDZBChtZl2yAHhj5rfI2f7rcYr8v4bu3txizmlXJ7kqOvfBBrb0zxDe22sWcV5cTfYhdRpdOkpGIN4EhXJPIHNZKlyu8WaKpdXsfqX4S8ZJpWiWunedGiWxkVIzIZPLiZy0cauxJxEp2DPYDHGK7FPHlkRxMo/EV8y+IfCXwtu9D1i+8C+MBNcw2s01hu1FHR9ikoXRl3OuQQQGBJ71+fPgz40fEPUdHivdflTSppY4pFRNxwHjViGDEMrKSQVPSiFKdS8oim1F2Z+z7+OLM5/er+dc/feObFZlXzwu1SfvY68da/Kv8A4XLqysFn1UnsRGvzZPA5J6ZIz7ZrGuPitdXCs11qdwS3yBVAGR9frXRTp1ImU5waPu34sePPC+oaHeaTq7wXVrfQtBPA2CJEcEEEdDxX59eEfgP8Kdb0C31K8n1yaSR50V7e/mWJo4pniiKhZAOY1XJ7nJPNebePviZb2lh5lu8kcrTBZp5jmRFPQKOgyTyQOleFx/GjxLo6/wBnabIptoSfL47MSx/Umu+LqNHM4xWx/9SjLpMc2UETHHbAGKx5vCUUrZaNcejNk/0rRfV9QCcBlHuB/UioTqV1twzkt6CvJVNG3MzIHgvTWOGiRT3zjp+Rq5D4J0YEkqrAe3T9KvR3M8r/AD5B65JGfwq8txLGv7zBXpuU5/OqtbYV+5np4V0JMbYjIx6H7o/P/wCtVuPw1paZRiMjnnt/wL/61cj8RW1S78B67aeG5WTVZbOVbNkcIVmKnaQx4GD612Wh30kGmWVtIn+kR20KSF2BJcIAxJ5zk5pyvbcFZdCSPw9Yl9vkNtAzuzhfwyOaik8O2hOEVY+Mkd8euT/Sumt7q5jjYSTfuienH86putruJVimOdwP889fwrJtl+pkR6TZQ/6pnkbpxkgf99AD9ar3eiWt0m2SHcy9Pf8AkP1rUe4SMEjdIvcoQT+QqodVtUJ8xjIwX7vJJH0AJrKzLVjlksvD76n/AGRLJDBe7dwhLYYqe/fv71qT+DZQNsSeYOwwCPz5q+l9b3al44UAZfvyYVh3xgjeBWCby782RLWWS8jOeIWAjXtgnOTUOD7minrY5rVvDlrZBmuBHbyfwkMCcj1UHp+X1rzPVNEupLUNM6T/AL1HlSH5GkjDZZF+ZgCw45I+td9aa5b3euTeH7yxe1uVAkVZo5F8xGzhgWGCvBwwODXVzaZp8Snbbb3I+YgZHP0yP1qVBJ80SpTlbkex+e3i3wXqn/CzZNb8M6BdLosMsckDXKW4bcE+bYhllaJTKSygHg89TXXfY/Fdycz2LMCc4Mvl9+4XOT719jSeEbG5QzyS+Uc5wPf6VzF9aeF9Nuv7On1e1huvlJhkmQSgP93KkhgGwdvHODXU8S3ZbM51SV7LY+ak8O6gf3t1bfZsHIHmscn34wakbSNRA+WAt75yQP0Ir6DvNCaN98MihSR2PI9hgiqv2BdxSaFXwev3f54pe3kuhPskfOl/4V1DUo2juIWn3f31VsY9+CR9a5//AIVFcTfvPsa/Nz9w/wDxYr60t9MsQxOHST2XcP0yauCwRRjzgMdtkn/xNNYp9R+zR//V4uZDjMxJ56rnA9c1U8wZIgRmx/fYiokvAQIJcMF5D525A7n1NRy3cZfDLlh/Eo6/4V522hoXbd4z8xl2HptHb862LYWpYtLtOOmf55rm2lGMy4Re3G41DHNMxfzcEdFz8vHuB1piv0Om1LUdNhsLuRn8tYYnLMeE6dz7VIL+1ICghMqCp67gR1H1ry3xrca1/YD2+gWY1O6mIQRM/lRhOrZIBODUfh/VNc+wKmuW0NvKRkQwuSkZ7rvwGI9OnFEo6XC+tj1h9UjhKqYwOMZPJNTnUYdgYgr/AL3T/GuDhvNRkG2NBDH7Hk/j1NaUJjXBlOCOxrNotvU6QXazliiEORw68D2470i2kcMG+5ZXkHXjByf51lRXc5ysWFHQA9Pw96mUpbDzL+XBPT5tzflUco+ZE0v9n3RWGe2ErKcrgchsf1rRhtLZfKBzaknhFrIW/wDmZ4dyx8nLDoen41IlyqZIkbf1y3zKfqP8KPUqCc5KEN2eD6fqV/rHxUj/ALS1HS9Ti00yW1nbw7FvI9iFgLnaxJI+gxkZr3/c758tQucklef0NfDcdz4j0Hxtd+J7e3jupJLmcgbiilZPkJAxnpjgmvT7P4o+Isq0lmI0xzH5gYH3GRkfnXDPG4aOnMfbx4EzrEJSpUfv0PoOaNvLZlka2fsWB215D8R9U1vWtS8PeGdLtUtLOxuxcXt6LcSLJIxUBGfqUKZ3DoGK9war/wDCzL+42rJanJ/usDjH1Ax+FeH+JdfnPxBh1wNcLbiS1ZlRnCnaGBBUHaxGe4NFPEYerLSepx4zhHOMBTc8Th2k/R/lc+yTDBsEcaiD+H0GRj17UG1typAfzG7nggfSvPbPxNLe8okojPou3I/EcV1EWrXfl/uIGUYGWYL0H15/Wu32XZnyDk77Gq+igoHi+QtyG6HHeqos7nGFDyD1z1qm2oapNjzLf5e3zFSR68cCpPtmoD7sOB/vmly9x8x//9byu6jFxA7uSDEhdcYGGA4PSsbR7mWVsuQRgHGOOa3pP+Pab/rk38q5rQ+v4CvNkWjqncqw2gD8KyZ4lRmmGSxI6kkfzrTl+8Ko3X3D9RVIXUzJLueUFC2xT2WliOzO3vjOaq9/xqyvU1MtmN7E0lzLGVCHG79PpXWWiKI1Yjc23O48muLn+9HXbWv+qX/d/wAKTBbDNQlZIUKYUvwSBzXOHMVxlCckAkk5PNb+pf6mL61gzf8AHwPoKQyzLd3AAUOQODWpaOzbc9QCc96w5uo/Ctmy7f7p/lWNX4ZHXgf94pf4l+Z866ig8yXPzfOev+0xBrLeJcBhxjAGMD+la+o/6yX/AH//AGc1mP8Ac/EV+er4j+6aKXs4ei/IgiG7h/nBOMHtjpTHtoZXYOuc5B/Hv7GpIf8A2Y08f6w/U/zq6babsVOEWrNHuXhU/a9Asp5QCzR4OOnHTrWx5MU0zI4+VMEAEjn1PP8A9asfwZ/yLNh/uH+dbsP/AB8y/QV95Qb9kvkfxBnsYxzCuoqy55fmDFwGIYkDoO1LHM+wdKST7rVHH9wVcjzD/9k='
};

export const SINGLE_CUSTOM_IMAGE_BASE_64_FIXTURE = {
  test1: {
    name: 'test',
    type: 'jpg',
    file: '/9j/4AA'
  }
};

export const CUSTOM_IMAGES_BASE_64_FIXTURE = {
  test1: {
    name: 'test',
    type: 'jpg',
    file: '/9j/4AA'
  },
  test2: {
    name: 'test',
    type: 'jpg',
    file: '/9j/4AA'
  }
};
