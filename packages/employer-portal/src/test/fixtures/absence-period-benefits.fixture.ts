/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientLeavePlan,
  GroupClientLeaveRequest
} from 'fineos-js-api-client';
import moment from 'moment';

import { EnhancedGroupClientPeriodDecision } from '../../app/shared';

export const ABSENCE_PERIOD_BENEFITS_FIXTURE: EnhancedGroupClientPeriodDecision[] = [
  {
    absenceId: 'ABS-1',
    startDate: moment('2019-01-01'),
    endDate: moment('2019-12-31'),
    decisions: [
      {
        absence: {
          id: '1',
          caseReference: 'CLM-4'
        },
        employee: {
          id: '2',
          name: 'employee'
        },
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment('2019-01-01'),
          endDate: moment(''),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Reduced Schedule',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: 'PLC01',
            name: 'Paid Leave Case 01',
            shortName: 'PLC 01',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Reason Name 1'
          } as GroupClientLeaveRequest
        }
      },
      {
        absence: {
          id: '2',
          caseReference: 'CLM-4'
        },
        employee: {
          id: '2',
          name: 'employee'
        },
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment('2019-01-01'),
          endDate: moment(''),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Reduced Schedule',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: 'PLC01',
            name: 'Paid Leave Case 01',
            shortName: 'PLC 01',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Reason Name 1'
          } as GroupClientLeaveRequest
        }
      },
      {
        absence: {
          id: '1',
          caseReference: 'CLM-4'
        },
        employee: {
          id: '2',
          name: 'employee'
        },
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment('2019-01-01'),
          endDate: moment(''),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Episodic',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: 'PLC02',
            name: 'Paid Leave Case 02',
            shortName: 'PLC 02',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Reason Name 2'
          } as GroupClientLeaveRequest
        }
      },
      {
        absence: {
          id: '1',
          caseReference: 'CLM-4'
        },
        employee: {
          id: '2',
          name: 'employee'
        },
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment('2019-01-01'),
          endDate: moment(''),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Time off period',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: 'PLC03',
            name: 'Paid Leave Case 03',
            shortName: 'PLC 03',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Reason Name 3'
          } as GroupClientLeaveRequest
        }
      },
      {
        absence: {
          id: '1',
          caseReference: 'CLM-4'
        },
        employee: {
          id: '2',
          name: 'employee'
        },
        period: {
          periodReference: '',
          parentPeriodReference: '',
          relatedToEpisodic: false,
          startDate: moment('2019-01-01'),
          endDate: moment(''),
          balanceDeduction: 0,
          timeRequested: '',
          timeDecisionStatus: '',
          timeDecisionReason: '',
          type: 'Reduced Schedule',
          status: 'Episodic',
          leavePlan: {
            paidLeaveCaseId: 'PLC04',
            name: 'Paid Leave Case 04',
            shortName: 'PLC 04',
            calculationPeriodMethod: ''
          } as GroupClientLeavePlan,
          leaveRequest: {
            reasonName: 'Reason Name 4'
          } as GroupClientLeaveRequest
        }
      }
    ]
  }
];
