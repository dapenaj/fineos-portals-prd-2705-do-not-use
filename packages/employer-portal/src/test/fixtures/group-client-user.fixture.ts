/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { GroupClientUser } from 'fineos-js-api-client';

export const GROUP_CLIENT_USERS_FIXTURE = [
  {
    id: '1',
    userReferenceIdentifier: '1',
    nameOfUser: 'Jane Doe',
    role: {
      name: 'HR'
    },
    enabled: true,
    status: {
      domainId: 1001,
      fullId: 32032001,
      name: 'Active',
      domainName: 'Account Status'
    }
  } as GroupClientUser,
  {
    id: '2',
    userReferenceIdentifier: '2',
    nameOfUser: 'Jane Doe',
    role: {
      name: 'HR'
    },
    enabled: false,
    status: {
      domainId: 1001,
      fullId: 32032002,
      name: 'Disabled - Manually',
      domainName: 'Account Status'
    }
  } as GroupClientUser
];

export const GROUP_CLIENT_USERS_UPDATED_FIXTURE = {
  id: '2',
  nameOfUser: 'Jane Doe',
  role: {
    name: 'HR'
  },
  enabled: true,
  status: {
    domainId: 1001,
    fullId: 32032001,
    name: 'Active',
    domainName: 'Account Status'
  }
} as GroupClientUser;

export const groupClientUsersUpdatedToEnabledFixture = {
  id: '1',
  nameOfUser: 'Jane Doe',
  role: {
    name: 'HR'
  },
  enabled: false,
  status: {
    domainId: 1001,
    fullId: 32032002,
    name: 'Disabled - Manually',
    domainName: 'Account Status'
  }
} as GroupClientUser;
