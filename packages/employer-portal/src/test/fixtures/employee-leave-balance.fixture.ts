/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { EmployeeLeaveBalance } from 'fineos-js-api-client';
import moment from 'moment';

import {
  EmployeeLeaveBalanceLeavePlan,
  CalculationMethod
} from '../../app/modules/employee-profile/time-taken-tab/time-taken.api';

export const EMPLOYEE_LEAVE_BALANCE_ROLLING_FIXTURE = [
  {
    leavePlanName: 'leavePlanName',
    employeeLeaveBalance: {
      approvedTime: 0,
      pendingTime: 21.2,
      availableBalance: 2,
      timeBasis: 'Weeks',
      availabilityPeriodStartDate: moment('2019-05-30'),
      availabilityPeriodEndDate: moment('2020-05-29'),
      timeEntitlement: 20,
      timeWithinPeriod: 12,
      timeWithinPeriodBasis: 'Months',
      notificationMessage:
        'The approximate balance provides an indication of your balance under this leave plan and is for general information purposes only. This balance considers your leave requests within the highlighted months. We recommend that you submit a leave request for an accurate assessment of available time under each leave plan.'
    } as EmployeeLeaveBalance,
    calculationMethod: CalculationMethod.ROLLING_BACK,
    endDate: moment()
  }
] as EmployeeLeaveBalanceLeavePlan[];

export const EMPLOYEE_LEAVE_BALANCE_ROLLING_FORWARD_FIXTURE = [
  {
    leavePlanName: 'leavePlanName',
    employeeLeaveBalance: {
      approvedTime: 0,
      pendingTime: 21.2,
      availableBalance: 2,
      timeBasis: 'Weeks',
      availabilityPeriodStartDate: moment('2019-05-30'),
      availabilityPeriodEndDate: moment('2020-05-29'),
      timeEntitlement: 20,
      timeWithinPeriod: 12,
      timeWithinPeriodBasis: 'Months',
      notificationMessage:
        'The approximate balance provides an indication of your balance under this leave plan and is for general information purposes only. This balance considers your leave requests within the highlighted months. We recommend that you submit a leave request for an accurate assessment of available time under each leave plan.'
    } as EmployeeLeaveBalance,
    calculationMethod: CalculationMethod.ROLLING_FORWARD,
    endDate: moment()
  }
] as EmployeeLeaveBalanceLeavePlan[];

export const EMPLOYEE_LEAVE_BALANCE_CALENDAR_YEAR_FIXTURE = [
  {
    leavePlanName: 'civic duty plan',
    employeeLeaveBalance: {
      approvedTime: 0,
      pendingTime: 21.2,
      availableBalance: 10,
      timeBasis: 'Weeks',
      availabilityPeriodStartDate: moment('2019-05-30'),
      availabilityPeriodEndDate: moment('2020-05-29'),
      timeEntitlement: 20,
      timeWithinPeriod: 12,
      timeWithinPeriodBasis: 'Months',
      notificationMessage:
        'The approximate balance provides an indication of your balance under this leave plan and is for general information purposes only. This balance considers your leave requests within the highlighted months. We recommend that you submit a leave request for an accurate assessment of available time under each leave plan.'
    } as EmployeeLeaveBalance,
    calculationMethod: CalculationMethod.CALENDAR_YEAR,
    endDate: moment()
  }
] as EmployeeLeaveBalanceLeavePlan[];

export const EMPLOYEE_LEAVE_BALANCE_FIXED_YEAR_FIXTURE = [
  {
    leavePlanName: 'civic duty plan',
    employeeLeaveBalance: {
      approvedTime: 0,
      pendingTime: 21.2,
      availableBalance: 10,
      timeBasis: 'Weeks',
      availabilityPeriodStartDate: moment('2019-05-30'),
      availabilityPeriodEndDate: moment('2020-05-29'),
      timeEntitlement: 20,
      timeWithinPeriod: 12,
      timeWithinPeriodBasis: 'Months',
      notificationMessage:
        'The approximate balance provides an indication of your balance under this leave plan and is for general information purposes only. This balance considers your leave requests within the highlighted months. We recommend that you submit a leave request for an accurate assessment of available time under each leave plan.'
    } as EmployeeLeaveBalance,
    calculationMethod: CalculationMethod.FIXED_YEAR,
    endDate: moment()
  }
] as EmployeeLeaveBalanceLeavePlan[];
