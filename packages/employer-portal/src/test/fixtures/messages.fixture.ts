/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import { CaseMessage } from 'fineos-js-api-client';

export const MESSAGES_FIXTURE = [
  {
    id: '1',
    subject: 'Lorem ipsum dolor sit amet',
    narrative:
      'Nam fringilla viverra nisi ut tristique. Aenean condimentum eget diam sit amet viverra. Sed eget luctus elit. Proin vitae libero sed est dignissim tincidunt.',
    contactDateTime: moment(new Date(2019, 3, 5, 12)),
    msgOriginatesFromPortal: false,
    readByGroupClient: true,
    rootCase: {
      id: 'string',
      caseReference: 'string',
      caseType: 'string'
    },
    customer: {
      id: 'string',
      firstName: 'string',
      lastName: 'string'
    },
    case: {
      id: 'NTN-24',
      caseReference: 'string',
      caseType: 'string'
    }
  },
  {
    id: '2',
    subject: 'Nunc imperdiet lectus dolor',
    narrative:
      'Mauris vel diam id mi ultricies condimentum vitae sit amet mauris. Maecenas quis faucibus sem, non vulputate nulla. Etiam condimentum, metus at rutrum posuere, purus orci tempor dui, ac mollis enim lorem congue nulla.',
    contactDateTime: moment(new Date(2020, 3, 5, 12)),
    msgOriginatesFromPortal: true,
    readByGroupClient: false,
    rootCase: {
      id: 'string',
      caseReference: 'string',
      caseType: 'string'
    },
    customer: {
      id: 'string',
      firstName: 'string',
      lastName: 'string'
    },
    case: {
      id: 'NTN-24',
      caseReference: 'string',
      caseType: 'string'
    }
  },
  {
    id: '3',
    subject: 'Lorem ipsum dolor',
    narrative:
      'Nam fringilla viverra nisi ut tristique. Aenean condimentum ed eget luctus elit. Proin vitae libero sed est dignissim tincidunt.',
    contactDateTime: moment(new Date(2021, 3, 5, 12)),
    msgOriginatesFromPortal: true,
    readByGroupClient: true,
    rootCase: {
      id: 'string',
      caseReference: 'string',
      caseType: 'string'
    },
    customer: {
      id: 'string',
      firstName: 'string',
      lastName: 'string'
    },
    case: {
      id: 'NTN-24',
      caseReference: 'string',
      caseType: 'string'
    }
  },
  {
    id: '4',
    subject: 'Nunc imperdiet lectus',
    narrative:
      'Mauris vel diam id mi ultricies condimentum vitae sit amet mauris. Maecenas quis faucibus semm, metus at rutrum posuere, purus orci tempor dui, ac mollis enim lorem congue nulla.',
    contactDateTime: moment(new Date(2018, 3, 5, 12)),
    msgOriginatesFromPortal: true,
    readByGroupClient: false,
    rootCase: {
      id: 'string',
      caseReference: 'string',
      caseType: 'string'
    },
    customer: {
      id: 'string',
      firstName: 'string',
      lastName: 'string'
    },
    case: {
      id: 'NTN-24',
      caseReference: 'string',
      caseType: 'string'
    }
  }
] as CaseMessage[];
