/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  Decision,
  GroupClientPeriodDecisions,
  GroupClientPeriod
} from 'fineos-js-api-client';
import moment from 'moment';

import { LeavePeriodChange } from '../../app/modules/notification/types';
import {
  EnhancedAbsencePeriodDecisions,
  EnhancedGroupClientPeriodDecision,
  PeriodType,
  PeriodStatus
} from '../../app/shared';

export const EMPTY_ENHANCED_GROUP_CLIENT_PERIOD_DECISION = [] as EnhancedAbsencePeriodDecisions[];

export const SUMMARY_ENHANCED_GROUP_CLIENT_PERIOD_DECISION_FIXTURE_INVALID_FIRST_DAY = [
  {
    absenceId: 'NTN-63-ABS-01',
    absencePeriodDecisions: {
      startDate: moment(new Date('2020-02-02')),
      endDate: moment(new Date('2020-04-09')),
      decisions: [
        {
          period: {
            startDate: moment(''),
            type: PeriodType.TIME_OFF,
            status: PeriodStatus.INTERMITTENT,
            relatedToEpisodic: false,
            leavePlan: {
              id: '99174fc9-e0da-45a8-95d6-600b13028492',
              name: 'civic duty plan',
              adjudicationStatus: 'Accepted'
            },
            leaveRequest: {
              id: 'PL:14432:0000000403',
              reasonName: 'Civic Duty',
              qualifier1: 'Jury',
              qualifier2: '',
              decisionStatus: 'Approved'
            }
          } as GroupClientPeriod
        },
        {
          period: {
            startDate: moment(new Date('2020-02-14')),
            type: PeriodType.TIME_OFF,
            status: PeriodStatus.INTERMITTENT,
            relatedToEpisodic: false,
            leavePlan: {
              id: '99174fc9-e0da-45a8-95d6-600b13028492',
              name: 'civic duty plan',
              adjudicationStatus: 'Accepted'
            },
            leaveRequest: {
              id: 'PL:14432:0000000403',
              reasonName: 'Civic Duty',
              qualifier1: 'Jury',
              qualifier2: '',
              decisionStatus: 'Approved'
            }
          } as GroupClientPeriod
        }
      ]
    }
  } as EnhancedAbsencePeriodDecisions
] as EnhancedAbsencePeriodDecisions[];

export const ENHANCED_GROUP_CLIENT_PERIOD_DECISION = [
  {
    absenceId: 'NTN-63-ABS-01',
    absencePeriodDecisions: {
      startDate: moment(new Date('2020-02-02')),
      endDate: moment(new Date('2020-04-09')),
      decisions: [
        {
          absence: {
            id: 'NTN-63-ABS-01'
          },
          period: {
            parentPeriodReference: '',
            startDate: moment(new Date('2020-04-05')),
            endDate: moment(new Date('2020-04-09')),
            timeDecisionStatus: 'Approved',
            type: 'Reduced Schedule',
            leavePlan: {
              id: '99174fc9-e0da-45a8-95d6-600b13028492',
              name: 'civic duty plan',
              adjudicationStatus: 'Accepted'
            },
            leaveRequest: {
              id: 'PL:14432:0000000403',
              reasonName: 'Civic Duty',
              qualifier1: 'Jury',
              qualifier2: '',
              decisionStatus: 'Approved'
            }
          }
        } as Decision,
        {
          absence: {
            id: 'NTN-63-ABS-01'
          },
          period: {
            parentPeriodReference: '',
            startDate: moment(new Date('2020-02-02')),
            endDate: moment(new Date('2020-02-06')),
            timeDecisionStatus: 'Approved',
            type: 'Time off period',
            status: 'Known',
            leavePlan: {
              id: '99174fc9-e0da-45a8-95d6-600b13028492',
              name: 'civic duty plan',
              adjudicationStatus: 'Accepted'
            },
            leaveRequest: {
              id: 'PL:14432:0000000403',
              reasonName: 'Civic Duty',
              qualifier1: 'Jury',
              qualifier2: '',
              decisionStatus: 'Approved'
            }
          }
        } as Decision,
        {
          absence: {
            id: 'NTN-63-ABS-01'
          },
          period: {
            parentPeriodReference: '',
            startDate: moment(new Date('2020-03-08')),
            endDate: moment(new Date('2020-03-15')),
            timeDecisionStatus: '',
            type: 'Episodic',
            status: 'Episodic',
            leavePlan: {
              id: '99174fc9-e0da-45a8-95d6-600b13028492',
              name: 'civic duty plan',
              adjudicationStatus: 'Accepted'
            },
            leaveRequest: {
              id: 'PL:14432:0000000403',
              reasonName: 'Civic Duty',
              qualifier1: 'Jury',
              qualifier2: '',
              decisionStatus: 'Approved'
            }
          }
        } as Decision
      ] as Decision[]
    } as GroupClientPeriodDecisions
  }
] as EnhancedAbsencePeriodDecisions[];

export const EMPTY_DECISIONS_FIXTURE = [] as Decision[];

export const SINGLE_DECISION_PERIODS_FIXTURE = [
  {
    absence: {
      id: 'ABS-1'
    },
    period: {
      startDate: moment(new Date('2010-01-05')),
      endDate: moment(new Date('2010-01-05')),
      leaveRequest: { reasonName: 'Reason Name 1' }
    }
  }
] as Decision[];

export const DECISIONS_FIXTURE = [
  {
    absence: {
      id: 'ABS-1'
    },
    period: {
      startDate: moment(new Date('2010-01-05')),
      endDate: moment(new Date('2010-01-05')),
      leaveRequest: { reasonName: 'Reason Name 1' }
    }
  },
  {
    absence: {
      id: 'ABS-1'
    },
    period: {
      startDate: moment(new Date('2010-01-05')),
      endDate: moment(new Date('2010-01-05')),
      leaveRequest: { reasonName: 'Reason Name 1' }
    }
  }
] as Decision[];

export const FLATTEN_DECISIONS_FIXTURE = [
  {
    absence: {
      id: 'ABS-1'
    },
    period: {
      startDate: moment(new Date('2010-01-05')),
      endDate: moment(new Date('2010-01-10'))
    }
  },
  {
    absence: {
      id: 'ABS-2'
    },
    period: {
      startDate: moment(new Date('2010-01-15')),
      endDate: moment(new Date('2010-01-25'))
    }
  }
] as Decision[];

export const FORM_VALUE_FIXTURE = {
  reason: 'Employee Request',
  additionalNotes: 'test notes',
  changeRequestPeriods: [0, 1]
} as LeavePeriodChange<number[]>;

export const ABSENCE_PERIOD_FIXTURE_SINGLE_PERIOD = {
  absenceId: 'ABS-1',
  decisions: [
    {
      period: {
        startDate: moment(new Date('2010-01-05')),
        endDate: moment(new Date('2010-01-05')),
        type: 'Reduced Schedule',
        status: 'Approved',
        leaveRequest: {
          reasonName: 'Child Bonding'
        }
      }
    }
  ] as Decision[]
} as EnhancedGroupClientPeriodDecision;

export const ABSENCE_PERIODS_FIXTURE_MULTIPLE_PERIODS = [
  {
    absenceId: 'ABS-1',
    decisions: [
      {
        period: {
          startDate: moment(new Date('2020-01-02')),
          endDate: moment(new Date('2020-01-04')),
          type: 'Reduced Schedule',
          status: 'Approved',
          timeDecisionStatus: 'Approved',
          parentPeriodReference: '',
          leaveRequest: {
            reasonName: 'Child Bonding',
            decisionStatus: 'Approved'
          },
          leavePlan: {
            name: 'Name',
            adjudicationStatus: 'Accepted'
          }
        }
      },
      {
        period: {
          startDate: moment(new Date('2020-01-05')),
          endDate: moment(new Date('2020-01-07')),
          type: 'Episodic',
          status: 'Pending',
          parentPeriodReference: '',
          leaveRequest: {
            reasonName: 'Child Bonding',
            decisionStatus: 'Pending'
          },
          leavePlan: {
            name: 'Name'
          }
        }
      },
      {
        period: {
          startDate: moment(new Date('2020-01-08')),
          endDate: moment(new Date('2020-01-08')),
          type: 'Time off period',
          status: 'Denied',
          parentPeriodReference: '',
          leaveRequest: {
            reasonName: 'Child Bonding',
            decisionStatus: 'Denied'
          },
          leavePlan: {
            name: 'Name'
          }
        }
      },
      {
        period: {
          startDate: moment(new Date('2020-01-09')),
          endDate: moment(new Date('2020-01-09')),
          type: 'Reduced Schedule',
          status: 'Cancelled',
          leaveRequest: {
            reasonName: 'Child Bonding',
            decisionStatus: 'Cancelled'
          },
          leavePlan: {
            name: 'Name 1'
          }
        }
      }
    ] as Decision[]
  }
] as EnhancedGroupClientPeriodDecision[];
