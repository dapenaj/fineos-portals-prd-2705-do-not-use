import {
  GroupClientPhoneNumber,
  CommunicationPreference,
  GroupClientEmailAddress,
  BaseDomain
} from 'fineos-js-api-client';

import { FullCommunicationPreference } from '../../app/modules/employee-profile/employee-details-tab/communication-preferences-tab/communication-preferences-tab.enum';

import { EMAILS_FIXTURE, PHONES_FIXTURE } from './contact-details.fixture';

export const EMPTY_NOTIFICATION_OF_UPDATES_FIXTURE = {
  phoneNumbers: [] as GroupClientPhoneNumber[],
  emailAddresses: [] as GroupClientEmailAddress[]
} as CommunicationPreference;

export const NOTIFICATION_OF_UPDATES_FIXTURE = {
  phoneNumbers: PHONES_FIXTURE,
  emailAddresses: EMAILS_FIXTURE
} as CommunicationPreference;

export const WRITTEN_CORRESPONDENCE_FIXTURE = {
  phoneNumbers: PHONES_FIXTURE,
  emailAddresses: EMAILS_FIXTURE
} as CommunicationPreference;

export const EMPTY_WRITTEN_CORRESPONDENCE_FIXTURE = {
  phoneNumbers: [] as GroupClientPhoneNumber[],
  emailAddresses: [] as GroupClientEmailAddress[]
} as CommunicationPreference;

export const EMPTY_CONTEXT_VALUES_FIXTURE = {
  emailAddresses: [],
  phoneNumbers: [],
  communicationPreferences: {
    directCorrespondence: {
      id: '1',
      contactContext: {} as BaseDomain,
      phoneNumbers: [],
      emailAddresses: []
    } as CommunicationPreference,
    notificationOfUpdates: {
      id: '2',
      contactContext: {} as BaseDomain,
      phoneNumbers: [],
      emailAddresses: []
    } as CommunicationPreference,
    writtenCorrespondence: {
      id: '3',
      contactContext: {} as BaseDomain,
      phoneNumbers: [],
      emailAddresses: []
    } as CommunicationPreference
  } as FullCommunicationPreference,
  onCommunicationPreferencesEdit: jest.fn(),
  onEmailAdd: jest.fn(),
  onPhoneNumberAdd: jest.fn()
};

export const CONTEXT_VALUES_FIXTURE = {
  emailAddresses: EMAILS_FIXTURE,
  phoneNumbers: PHONES_FIXTURE,
  communicationPreferences: {
    directCorrespondence: {
      id: '1',
      contactContext: {} as BaseDomain,
      phoneNumbers: [PHONES_FIXTURE[0]],
      emailAddresses: [EMAILS_FIXTURE[0]]
    } as CommunicationPreference,
    notificationOfUpdates: {
      id: '2',
      contactContext: {} as BaseDomain,
      phoneNumbers: [PHONES_FIXTURE[0]],
      emailAddresses: [EMAILS_FIXTURE[0]]
    } as CommunicationPreference,
    writtenCorrespondence: {
      id: '3',
      contactContext: {} as BaseDomain,
      phoneNumbers: [PHONES_FIXTURE[0]],
      emailAddresses: [EMAILS_FIXTURE[0]]
    } as CommunicationPreference
  } as FullCommunicationPreference,
  onCommunicationPreferencesEdit: jest.fn(),
  onEmailAdd: jest.fn(),
  onPhoneNumberAdd: jest.fn()
};
