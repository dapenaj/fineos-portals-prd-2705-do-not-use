/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import {
  Benefit,
  DisabilityBenefitSummary,
  DisabilityBenefit
} from 'fineos-js-api-client';

export const DISABILITY_BENEFIT_FIXTURE: DisabilityBenefit = {
  accidentBasisOfMaxBenefitPeriod: 'string',
  accidentEliminationPeriod: 0,
  accidentMaxBenefitPeriod: 0,
  adviceToPayOverride: 'string',
  amountType: 'string',
  approvedThroughDate: moment('2019-07-13'),
  basisOfAccidentEliminationPeriod: 'string',
  basisOfEliminationPeriod: 'string',
  basisOfLateEnrollmentPeriod: 'string',
  basisOfMaxBenefitPeriod: 'string',
  basisOfMinBenefitPeriod: 'string',
  basisOfMinimumQualifyPeriod: 'string',
  basisOfPolicyWaitingPeriod: 'string',
  benefitEndDate: moment('2019-07-13'),
  benefitIncurredDate: moment('2019-07-13'),
  benefitStartDate: moment('2019-07-13'),
  benefitType: 'string',
  brokerAuthorisationFlag: true,
  checkCutting: 'string',
  earliestDateForClaimPayment: moment('2019-07-13'),
  eliminationPeriod: 0,
  employeeContributionPercentage: 0,
  employeeContributionStatus: 'string',
  employerContributionPercentage: 0,
  expectedResolutionDate: moment('2019-07-13'),
  frequencyAmount: 'string',
  hospitalBasisOfEliminationPeriod: 'string',
  hospitalBasisOfMaxBenefitPeriod: 'string',
  hospitalEliminationPeriod: 0,
  hospitalMaxBenefitPeriod: 0,
  hospitalizationClauseApplies: true,
  initialNotificationDate: moment('2019-07-13'),
  isReimbursement: true,
  isUnderwritten: true,
  lateEnrollmentPeriod: 0,
  latestDateForClaimPayment: moment('2019-07-13'),
  maxBenefitPeriod: 0,
  minBenefitPeriod: 0,
  minimumQualifyPeriod: 0,
  notificationReceivedDate: moment('2019-07-13'),
  overrideClaimIncurredDate: true,
  periodType: 'string',
  policyWaitingPeriod: 0,
  sourceOfRequest: 'string',
  startDateOfBenefitForClaim: moment('2019-07-13'),
  extensionAttributes: []
};

export const BENEFITS_FIXTURE = [
  {
    benefitCaseType: 'Short Term Disability 1',
    benefitHandler: 'Benny Smith',
    benefitHandlerEmailAddress: '',
    benefitHandlerPhoneNo: '1-234-567890',
    benefitId: 1,
    benefitIncurredDate: moment('2019-01-20'),
    benefitRightCategory: 'Recurring Benefit',
    creationDate: moment('2019-01-10'),
    customerName: 'bob bobbington',
    description: '',
    policyReferences: '',
    stageName: '',
    status: 'Pending',
    extensionAttributes: []
  },
  {
    benefitCaseType: 'Short Term Disability 2',
    benefitHandler: 'Jimmy Smith',
    benefitHandlerEmailAddress: '',
    benefitHandlerPhoneNo: '1-234-567890',
    benefitId: 2,
    benefitIncurredDate: moment('2019-01-21'),
    benefitRightCategory: 'Lump Sum Benefit',
    creationDate: moment('2019-01-11'),
    customerName: 'bob bobbington',
    description: '',
    policyReferences: '',
    stageName: '',
    status: 'Pending',
    extensionAttributes: []
  }
] as Benefit[];

export const DISABILITY_BENEFIT_SUMMARY_FIXTURE = {
  benefitSummary: BENEFITS_FIXTURE[0],
  disabilityBenefit: DISABILITY_BENEFIT_FIXTURE
} as DisabilityBenefitSummary;
