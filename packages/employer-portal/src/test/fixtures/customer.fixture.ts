/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  GroupClientCustomerInfo,
  GroupClientEmailAddress,
  GroupClientPhoneNumber,
  CommunicationPreference,
  CustomerSummary
} from 'fineos-js-api-client';
import moment from 'moment';

export const CUSTOMER_INFO_FIXTURE = {
  id: 'id-4',
  firstName: 'Obi Wan',
  lastName: 'Kenobi',
  dateOfBirth: moment(new Date('1990-01-30')),
  address: {
    premiseNo: '123',
    addressLine1: 'Main street',
    addressLine2: `Where it's at`,
    addressLine3: 'Two turn tables',
    addressLine4: 'And a microphone',
    addressLine5: 'Somewhere',
    addressLine6: 'CA',
    addressLine7: 'Way up high',
    postCode: 'AB123456',
    country: { name: 'USA' }
  }
} as GroupClientCustomerInfo;

export const EMAILS_FIXTURE = [
  {
    id: '1',
    emailAddress: 'obiwankenobi@tatooine.com'
  },
  {
    id: '2',
    emailAddress: 'obi.wan.kenobi@starwars.com'
  }
] as GroupClientEmailAddress[];

export const PHONE_NUMBERS_FIXTURE = [
  {
    id: '1',
    intCode: '353',
    areaCode: '1',
    telephoneNo: '234567'
  }
] as GroupClientPhoneNumber[];

export const COMMUNICATIONS_FIXTURE = [
  {
    id: '9312000',
    contactContext: {
      name: 'CP-1'
    },
    phoneNumbers: [
      {
        intCode: '353',
        areaCode: '1',
        telephoneNo: '234567'
      }
    ],
    emailAddresses: [
      {
        emailAddress: 'obiwankenobi@tatooine.com'
      },
      {
        emailAddress: 'obi.wan.kenobi@starwars.com'
      }
    ]
  },
  {
    id: '9312001',
    contactContext: {
      name: 'CP-1'
    },
    phoneNumbers: [
      {
        intCode: '353',
        areaCode: '1',
        telephoneNo: '234567'
      }
    ],
    emailAddresses: [
      {
        emailAddress: 'obiwankenobi@tatooine.com'
      },
      {
        emailAddress: 'obi.wan.kenobi@starwars.com'
      }
    ]
  },
  {
    id: '9312002',
    contactContext: {
      name: 'CP-1'
    },
    phoneNumbers: [
      {
        intCode: '353',
        areaCode: '1',
        telephoneNo: '234567'
      }
    ],
    emailAddresses: [
      {
        emailAddress: 'obiwankenobi@tatooine.com'
      },
      {
        emailAddress: 'obi.wan.kenobi@starwars.com'
      }
    ]
  }
] as CommunicationPreference[];

export const CUSTOMER_SUMMARY_FIXTURE = [
  {
    customerNo: 'CN-1',
    firstName: 'Obi Wan',
    lastName: 'Kenobi',
    jobTitle: 'Master',
    workSite: 'Tatooine',
    organisationUnit: 'Jedi',
    id: '1'
  }
] as CustomerSummary[];
