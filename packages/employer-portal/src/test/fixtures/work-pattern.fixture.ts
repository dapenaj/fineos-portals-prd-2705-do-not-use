/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { WeekBasedWorkPattern, BaseDomain } from 'fineos-js-api-client';
import moment from 'moment';

export const WORK_PATTERNS = [
  {
    patternStartDate: moment('2020-01-01'),
    patternStatus: { name: 'patternStatus' },
    workPatternType: {
      id: '8864005',
      name: 'Variable',
      domainId: 277,
      fullId: 8864005,
      domainName: 'WorkPatternType'
    } as BaseDomain,
    workWeekStarts: { name: 'workWeekStarts' },
    workPatternDays: [
      {
        dayOfWeek: {
          name: 'dayOfWeek'
        },
        hours: 1,
        minutes: 2,
        weekNumber: 3
      }
    ]
  } as WeekBasedWorkPattern,
  {
    patternStartDate: moment('2020-01-01'),
    patternStatus: { name: 'patternStatus' },
    workPatternType: {
      id: '8864002',
      name: '2 weeks Rotating',
      domainId: 277,
      fullId: 8864002,
      domainName: 'WorkPatternType'
    } as BaseDomain,
    workWeekStarts: { name: 'workWeekStarts' },
    workPatternDays: [
      {
        dayOfWeek: {
          name: 'dayOfWeek'
        },
        hours: 1,
        minutes: 2,
        weekNumber: 3
      }
    ]
  } as WeekBasedWorkPattern,
  {
    patternStartDate: moment('2020-01-01'),
    patternStatus: { name: 'patternStatus' },
    workPatternType: {
      id: '8864001',
      name: 'Fixed',
      domainId: 277,
      fullId: 8864001,
      domainName: 'WorkPatternType'
    } as BaseDomain,
    workWeekStarts: { name: 'workWeekStarts' },
    workPatternDays: [
      {
        dayOfWeek: {
          name: 'dayOfWeek'
        },
        hours: 1,
        minutes: 2,
        weekNumber: 3
      }
    ]
  } as WeekBasedWorkPattern
] as WeekBasedWorkPattern[];
