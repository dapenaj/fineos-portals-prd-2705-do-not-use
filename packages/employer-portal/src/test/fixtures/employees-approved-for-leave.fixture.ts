/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/
import { EmployeesOnLeave } from 'fineos-js-api-client';

import {
  EmployeesApprovedForLeaveType,
  LeavesInfoType
} from '../../app/modules/my-dashboard/employees-approved-for-leave/employees-approved-for-leave-widget.enums';

export const EMPLOYEES_APPROVED_FOR_LEAVE_FIXTURE = [
  {
    id: '345',
    firstName: 'Antonio',
    lastName: 'Antonio',
    notificationCases: [
      {
        id: '1',
        caseNumber: 'NTN-1',
        adminGroup: 'Madrid-IT',
        notificationReason: {
          domainId: 290,
          fullId: 9280004,
          name: 'Bonding with a new child (adoption/ foster care/ newborn)',
          domainName: 'NotificationReason'
        },
        absenceDays: [
          {
            date: '2021-04-05',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-06',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-07',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-08',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-09',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          }
        ]
      } as any,
      {
        id: '2',
        caseNumber: 'NTN-1',
        adminGroup: 'Madrid-IT',
        notificationReason: {
          name: 'Accident or treatment required for an injury',
          domainId: 290,
          fullId: 9280001,
          domainName: 'NotificationReason'
        },
        absenceDays: [
          {
            date: '2021-04-05',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-06',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    id: '346',
    firstName: 'Giovanni',
    lastName: 'Giovanni',
    notificationCases: [
      {
        id: '1',
        caseNumber: 'NTN-2',
        adminGroup: 'Dublin-IT',
        notificationReason: {
          domainId: 290,
          fullId: 9280004,
          name: 'Bonding with a new child (adoption/ foster care/ newborn)',
          domainName: 'NotificationReason'
        },
        absenceDays: [
          {
            date: '2021-04-05',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-06',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-07',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-08',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-09',
            leaveApproveDetails: [
              {
                leaveType: {
                  domainId: 6811,
                  fullId: 217952001,
                  name: 'Time off period',
                  domainName: 'AbsencePeriodType'
                },
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          },
          {
            date: '2021-04-09',
            leaveApproveDetails: [
              {
                leaveType: 'Episodic',
                timeApproved: '08',
                timeApprovedBasis: {
                  domainId: 6758,
                  fullId: 216256002,
                  name: 'Hours',
                  domainName: 'LengthBasis'
                }
              }
            ]
          }
        ]
      }
    ]
  }
] as EmployeesOnLeave[];

export const EMPLOYEES_APPROVED_FOR_LEAVE_DRAWER_FIXTURE = [
  {
    id: '0-0',
    adminGroup: 'Madrid-IT',
    notificationCase: { id: 'NTN-1' },
    employee: {
      id: '345',
      firstName: 'Mathew',
      lastName: 'Cartoon'
    },
    notificationReason: {
      name: 'Accident or treatment required for an injury',
      domainId: 290,
      fullId: 9280001,
      domainName: 'NotificationReason'
    },
    leavesInfo: [
      {
        hoursApproved: 40,
        leaveType: 'Time off period',
        timeApproved: null
      }
    ] as LeavesInfoType[],
    leaveType: 'Time off'
  } as EmployeesApprovedForLeaveType,
  {
    id: '0-1',
    adminGroup: 'Madrid-IT',
    notificationCase: { id: 'NTN-2' },
    employee: {
      id: '346',
      firstName: 'Antonio',
      lastName: 'Antonio'
    },
    notificationReason: {
      name: 'Accident or treatment required for an injury',
      domainId: 290,
      fullId: 9280001,
      domainName: 'NotificationReason'
    },
    leavesInfo: [
      {
        hoursApproved: 40,
        leaveType: 'Time off period',
        timeApproved: null
      }
    ] as LeavesInfoType[],
    leaveType: 'Time off'
  } as EmployeesApprovedForLeaveType,
  {
    id: '1-0',
    adminGroup: 'Dublin-IT',
    notificationCase: { id: 'NTN-3' },
    employee: {
      id: '347',
      firstName: 'Obi Wan',
      lastName: 'Kenobi'
    },
    notificationReason: {
      name: 'Accident or treatment required for an injury',
      domainId: 290,
      fullId: 9280001,
      domainName: 'NotificationReason'
    },
    leavesInfo: [
      {
        hoursApproved: 40,
        leaveType: 'Reduced Schedule',
        timeApproved: null
      }
    ] as LeavesInfoType[],
    leaveType: 'Reduced Schedule'
  } as EmployeesApprovedForLeaveType,
  {
    id: '1-1',
    adminGroup: 'Dublin-IT',
    notificationCase: { id: 'NTN-5' },
    employee: {
      id: '347',
      firstName: 'Obi Wan',
      lastName: 'Kenobi'
    },
    notificationReason: {
      name: 'Accident or treatment required for an injury',
      domainId: 290,
      fullId: 9280001,
      domainName: 'NotificationReason'
    },
    leavesInfo: [
      {
        hoursApproved: 40,
        leaveType: 'Episodic',
        timeApproved: null
      }
    ] as LeavesInfoType[],
    leaveType: 'Episodic'
  } as EmployeesApprovedForLeaveType
] as EmployeesApprovedForLeaveType[];
