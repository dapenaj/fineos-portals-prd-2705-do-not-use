/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import { GroupClientAccommodationCaseSummary } from 'fineos-js-api-client';

export const ACCOMMODATIONS_FIXTURE = [
  {
    id: 'A001',
    caseReference: '',
    notificationDate: moment('1999-12-31'),
    caseHandler: {
      name: 'John Stevens',
      phoneNumber: '1-234-4567000',
      emailAddress: ''
    },
    pregnancyRelated: '',
    decisionDate: moment('1999-12-31'),
    phase: 'Completion',
    stage: 'Request Invalid Correspondence',
    closureReasons: [
      'Accommodation creates or does not eliminate direct threat to self or others'
    ],
    accommodations: [
      {
        accommodationCategory: 'Work from home',
        accommodationType: '1 day per week',
        accommodationDescription: 'description',
        source: '',
        createDate: moment('1999-12-31'),
        acceptedDate: moment('1999-12-31'),
        implementedDate: moment('1999-12-31'),
        endDate: moment('')
      }
    ],
    limitations: ['Color Blindness'],
    notificationCase: {
      id: 'N001',
      caseReference: ''
    },
    employee: {
      id: 'E001',
      name: 'Edward Norton'
    }
  },
  {
    id: 'A002',
    caseReference: '',
    notificationDate: moment('1999-12-31'),
    caseHandler: {
      name: 'John Stevens',
      telephoneNo: '1-234-4567000',
      emailAddress: ''
    },
    pregnancyRelated: '',
    decisionDate: moment('1999-12-31'),
    phase: 'Assessment',
    stage: 'Evaluate Accommodation Options',
    closureReasons: [],
    accommodations: [],
    limitations: [],
    notificationCase: {
      id: 'N001',
      caseReference: ''
    },
    employee: {
      id: 'E001',
      name: 'Edward Norton'
    }
  }
] as GroupClientAccommodationCaseSummary[];

export const TIMELINE_ACCOMMODATIONS_FIXTURE = [
  {
    id: 'A001',
    caseReference: '',
    notificationDate: moment(new Date('1999-12-31')),
    caseHandler: {
      name: 'John Stevens',
      phoneNumber: '1-234-4567000',
      emailAddress: ''
    },
    pregnancyRelated: '',
    decisionDate: moment(new Date('1999-12-31')),
    phase: 'Completion',
    stage: 'Request Invalid Correspondence',
    closureReasons: [
      'Accommodation creates or does not eliminate direct threat to self or others'
    ],
    accommodations: [
      {
        accommodationCategory: 'Work from home',
        accommodationType: '1 day per week',
        accommodationDescription: 'description',
        source: '',
        createDate: moment(new Date('1999-12-31')),
        acceptedDate: moment(new Date('1999-12-31')),
        implementedDate: moment(new Date('1999-12-31')),
        endDate: moment(new Date('2000-01-19'))
      }
    ],
    limitations: ['Color Blindness'],
    notificationCase: {
      id: 'N001',
      caseReference: ''
    },
    employee: {
      id: 'E001',
      name: 'Edward Norton'
    }
  }
] as GroupClientAccommodationCaseSummary[];
