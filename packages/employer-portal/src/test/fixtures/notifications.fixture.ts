/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import moment from 'moment';
import {
  CustomerSummary,
  SubCase,
  GroupClientNotification,
  CaseHandler,
  AbsenceCaseStatus,
  ClaimCaseStatus,
  AccommodationCaseStatus
} from 'fineos-js-api-client';

export const NOTIFICATIONS_FIXTURE = [
  {
    caseNumber: 'NTN-24',
    notificationReason: {
      domainId: 1,
      fullId: 101,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    adminGroup: 'test admin group',
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary,
    subCases: [
      {
        id: '11',
        caseType: 'Group Disability Claim',
        status: ClaimCaseStatus.PENDING as ClaimCaseStatus
      },
      {
        id: 'ABS-2',
        caseNumber: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          id: '1',
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        } as CaseHandler,
        status: AbsenceCaseStatus.REGISTRATION as AbsenceCaseStatus
      },
      {
        id: 'ACC-1',
        caseType: 'Accommodation Case',
        status: AccommodationCaseStatus.REGISTRATION as AccommodationCaseStatus
      }
    ] as SubCase[],
    expectedRTWDate: moment('2020-06-22'),
    createdDate: moment('2020-03-01'),
    notificationDate: moment('2020-04-01')
  } as GroupClientNotification,
  {
    notificationReason: {
      domainId: 1,
      fullId: 101,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary,
    subCases: [] as SubCase[]
  } as GroupClientNotification,
  {
    notificationReason: {
      domainId: 1,
      fullId: 101,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary,
    subCases: [
      {
        id: '11',
        caseType: 'Group Disability Claim',
        status: ClaimCaseStatus.CLOSED as ClaimCaseStatus
      }
    ] as SubCase[]
  } as GroupClientNotification,
  {
    notificationReason: {
      domainId: 1,
      fullId: 101,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary,
    subCases: [
      {
        id: '11',
        caseType: 'Group Disability Claim',
        status: ClaimCaseStatus.DECIDED as ClaimCaseStatus
      },
      {
        id: 'ABS-2',
        caseNumber: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          id: '1',
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        } as CaseHandler,
        status: AbsenceCaseStatus.MANAGED_LEAVE as AbsenceCaseStatus
      }
    ] as SubCase[]
  } as GroupClientNotification,
  {
    notificationReason: {
      domainId: 1,
      fullId: 101,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary,
    subCases: [
      {
        id: '11',
        caseType: 'Group Disability Claim',
        status: ClaimCaseStatus.CLOSED as ClaimCaseStatus
      },
      {
        id: 'ABS-2',
        caseNumber: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          id: '1',
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        } as CaseHandler,
        status: AbsenceCaseStatus.REGISTRATION as AbsenceCaseStatus
      }
    ] as SubCase[]
  } as GroupClientNotification,
  {
    notificationReason: {
      domainId: 1,
      fullId: 101,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    customer: {
      firstName: 'firstName',
      lastName: 'lastName',
      jobTitle: 'jobTitle',
      organisationUnit: 'organisationUnit',
      workSite: 'workSite'
    } as CustomerSummary,
    subCases: [
      {
        id: '11',
        caseType: 'Group Disability Claim'
      },
      {
        id: 'ABS-2',
        caseNumber: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          id: '1',
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        } as CaseHandler
      }
    ] as SubCase[]
  } as GroupClientNotification
];

export const OUTSTANDING_NOTIFICATIONS_FIXTURE = [
  {
    id: 'NTN-24',
    caseNumber: 'NTN-24',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-12-25'),
    createdDate: moment('2019-01-25T07:53:15.852Z'),
    adminGroup: 'The Sith',
    status: 'Approved',
    caseHandler: {
      name: 'Captain Phasma',
      telephoneNo: '01-555-123456',
      emailAddress: 'phasma@kudos4troopers.com'
    },
    notifiedBy: {
      name: 'Other Notifier'
    },
    customerId: 'id-4',
    subCases: [
      {
        id: 'ABS-3',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-4',
      customerNo: '4',
      firstName: 'Obi Wan',
      lastName: 'Kenobi',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Master',
      workSite: 'Tatooine',
      organisationUnit: 'Jedi',
      address: {
        premiseNo: '123',
        addressLine1: 'Main street',
        addressLine2: "Where it's at",
        addressLine3: 'Two turn tables',
        addressLine4: 'And a microphone',
        addressLine5: 'Somewhere',
        addressLine6: 'CA',
        addressLine7: 'Way up high',
        postCode: 'AB123456',
        country: {
          name: 'USA'
        },
        classExtensionInformation: []
      },
      name: 'Obi Wan Kenobi'
    }
  },
  {
    id: 'NTN-23',
    caseNumber: 'NTN-23',
    customerId: 'id-5',
    customerNo: '5',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-24T07:53:15.852Z'),
    createdDate: moment('2020-01-24T07:53:15.852Z'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-10',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      },
      {
        id: 'ABS-1',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Red Mantle',
          telephoneNo: '01-111-123456',
          emailAddress: 'redmantle@theguildofcalamitousintent.com'
        }
      }
    ],
    customer: {
      id: 'id-5',
      customerNo: '5',
      firstName: 'Mathew',
      lastName: 'Cartoon',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Comics',
      organisationUnit: 'Art Studio',
      name: 'Mathew Cartoon'
    }
  },
  {
    id: 'NTN-22',
    caseNumber: 'NTN-22',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-23T07:53:15.852Z'),
    createdDate: moment('2020-01-23T07:53:15.852Z'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Sam Neil',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      customerNo: '3',
      firstName: 'Sam',
      lastName: 'Neil',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Sam Neil'
    }
  },
  {
    id: 'NTN-21',
    caseNumber: 'NTN-21',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-22T07:53:15.852Z'),
    createdDate: moment('2020-01-22T07:53:15.852Z'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Sam',
      lastName: 'Neil',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Sam Neil'
    }
  },
  {
    id: 'NTN-20',
    caseNumber: 'NTN-20',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-21T07:53:15.852Z'),
    createdDate: moment('2020-01-21T07:53:15.852Z'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-19',
    caseNumber: 'NTN-19',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-20T07:53:15.852Z'),
    createdDate: moment('2020-01-20T07:53:15.852Z'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-18',
    caseNumber: 'NTN-18',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-12-19'),
    createdDate: moment('2020-01-19T07:53:15.852Z'),
    adminGroup: 'The Sith',
    status: 'Approved',
    caseHandler: {
      name: 'Captain Phasma',
      telephoneNo: '01-555-123456',
      emailAddress: 'phasma@kudos4troopers.com'
    },
    customerId: 'id-4',
    subCases: [
      {
        id: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-4',
      firstName: 'Obi Wan',
      lastName: 'Kenobi',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Master',
      workSite: 'Tatooine',
      organisationUnit: 'Jedi',
      address: {
        premiseNo: '123',
        addressLine1: 'Main street',
        addressLine2: "Where it's at",
        addressLine3: 'Two turn tables',
        addressLine4: 'And a microphone',
        addressLine5: 'Somewhere',
        addressLine6: 'CA',
        addressLine7: 'Way up high',
        postCode: 'AB123456',
        country: {
          name: 'USA'
        },
        classExtensionInformation: []
      },
      name: 'Obi Wan Kenobi'
    }
  },
  {
    id: 'NTN-17',
    caseNumber: 'NTN-17',
    customerId: 'id-5',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-18T07:53:15.852Z'),
    createdDate: moment('2020-01-18T07:53:15.852Z'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-10',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      },
      {
        id: 'ABS-1',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Red Mantle',
          telephoneNo: '01-111-123456',
          emailAddress: 'redmantle@theguildofcalamitousintent.com'
        }
      }
    ],
    customer: {
      id: 'id-5',
      customerNo: '5',
      firstName: 'Mathew',
      lastName: 'Cartoon',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Comics',
      organisationUnit: 'Art Studio',
      name: 'Mathew Cartoon'
    }
  },
  {
    id: 'NTN-16',
    caseNumber: 'NTN-16',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-17T07:53:15.852Z'),
    createdDate: moment('2020-01-17T07:53:15.852Z'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-15',
    caseNumber: 'NTN-15',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-16T07:53:15.852Z'),
    createdDate: moment('2020-01-16T07:53:15.852Z'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [],
    customer: {
      id: 'id-3',
      customerNo: '3',
      firstName: 'Kyle',
      lastName: 'Reno',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  }
];

export const EMPLOYEES_EXPECTED_TO_RTW_NOTIFICATIONS_FIXTURE = ([
  {
    id: 'NTN-24',
    caseNumber: 'NTN-24',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    expectedRTWDate: moment('2021-01-03'),
    notificationDate: moment('2020-12-25'),
    createdDate: moment('2020-01-25T07:53:15.852Z'),
    adminGroup: 'group1',
    status: 'Approved',
    caseHandler: {
      name: 'Captain Phasma',
      telephoneNo: '01-555-123456',
      emailAddress: 'phasma@kudos4troopers.com'
    },
    subCases: [
      {
        id: 'ABS-3',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-4',
      firstName: 'Aaa',
      lastName: 'Bbb',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Master',
      workSite: 'Tatooine',
      organisationUnit: 'Jedi',
      address: {
        premiseNo: '123',
        addressLine1: 'Main street',
        addressLine2: "Where it's at",
        addressLine3: 'Two turn tables',
        addressLine4: 'And a microphone',
        addressLine5: 'Somewhere',
        addressLine6: 'CA',
        addressLine7: 'Way up high',
        postCode: 'AB123456',
        country: {
          name: 'USA'
        },
        classExtensionInformation: []
      },
      name: 'Obi Wan Kenobi'
    }
  },
  {
    id: 'NTN-23',
    caseNumber: 'NTN-23',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2015-01-24T07:53:15.852Z'),
    createdDate: moment('2015-01-24T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-10',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      },
      {
        id: 'ABS-1',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Red Mantle',
          telephoneNo: '01-111-123456',
          emailAddress: 'redmantle@theguildofcalamitousintent.com'
        }
      }
    ],
    customer: {
      id: 'id-5',
      firstName: 'Mathew',
      lastName: 'Cartoon',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Comics',
      organisationUnit: 'Art Studio',
      name: 'Mathew Cartoon'
    }
  },
  {
    id: 'NTN-22',
    caseNumber: 'NTN-22',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-23T07:53:15.852Z'),
    createdDate: moment('2020-01-23T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-21',
    caseNumber: 'NTN-21',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-22T07:53:15.852Z'),
    createdDate: moment('2020-01-22T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-20',
    caseNumber: 'NTN-20',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-21T07:53:15.852Z'),
    createdDate: moment('2020-01-21T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-19',
    caseNumber: 'NTN-19',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-20T07:53:15.852Z'),
    createdDate: moment('2020-01-20T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-18',
    caseNumber: 'NTN-18',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-12-19'),
    createdDate: moment('2020-01-19T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    adminGroup: 'group1',
    status: 'Approved',
    caseHandler: {
      name: 'Captain Phasma',
      telephoneNo: '01-555-123456',
      emailAddress: 'phasma@kudos4troopers.com'
    },
    subCases: [
      {
        id: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-4',
      firstName: 'Obi Wan',
      lastName: 'Kenobi',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Master',
      workSite: 'Tatooine',
      organisationUnit: 'Jedi',
      address: {
        premiseNo: '123',
        addressLine1: 'Main street',
        addressLine2: "Where it's at",
        addressLine3: 'Two turn tables',
        addressLine4: 'And a microphone',
        addressLine5: 'Somewhere',
        addressLine6: 'CA',
        addressLine7: 'Way up high',
        postCode: 'AB123456',
        country: {
          name: 'USA'
        },
        classExtensionInformation: []
      },
      name: 'Obi Wan Kenobi'
    }
  },
  {
    id: 'NTN-17',
    caseNumber: 'NTN-17',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-18T07:53:15.852Z'),
    createdDate: moment('2020-01-18T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-10',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      },
      {
        id: 'ABS-1',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Red Mantle',
          telephoneNo: '01-111-123456',
          emailAddress: 'redmantle@theguildofcalamitousintent.com'
        }
      }
    ],
    customer: {
      id: 'id-5',
      firstName: 'Mathew',
      lastName: 'Cartoon',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Comics',
      organisationUnit: 'Art Studio',
      name: 'Mathew Cartoon'
    }
  },
  {
    id: 'NTN-16',
    caseNumber: 'NTN-16',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-17T07:53:15.852Z'),
    createdDate: moment('2020-01-17T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-15',
    caseNumber: 'NTN-15',
    notifiedBy: null,
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-16T07:53:15.852Z'),
    createdDate: moment('2020-01-16T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  }
  // this mock will be fixed when API is ready
] as unknown) as GroupClientNotification[];

export const NOTIFICATIONS_CREATED_FIXTURE = [
  {
    id: 'NTN-24',
    caseNumber: 'NTN-24',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-12-25'),
    createdDate: moment('2020-01-25T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    adminGroup: 'group1',
    status: 'Approved',
    caseHandler: {
      name: 'Captain Phasma',
      telephoneNo: '01-555-123456',
      emailAddress: 'phasma@kudos4troopers.com'
    },
    customerId: 'id-4',
    subCases: [
      {
        id: 'ABS-3',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-4',
      firstName: 'Obi Wan',
      lastName: 'Kenobi',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Tatooine',
      organisationUnit: 'Jedi',
      address: {
        premiseNo: '123',
        addressLine1: 'Main street',
        addressLine2: "Where it's at",
        addressLine3: 'Two turn tables',
        addressLine4: 'And a microphone',
        addressLine5: 'Somewhere',
        addressLine6: 'CA',
        addressLine7: 'Way up high',
        postCode: 'AB123456',
        country: {
          name: 'USA'
        },
        classExtensionInformation: []
      },
      name: 'Obi Wan Kenobi'
    }
  },
  {
    id: 'NTN-23',
    caseNumber: 'NTN-23',
    customerId: 'id-5',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-24T07:53:15.852Z'),
    createdDate: moment('2020-01-24T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    adminGroup: 'group1',
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-10',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      },
      {
        id: 'ABS-1',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Red Mantle',
          telephoneNo: '01-111-123456',
          emailAddress: 'redmantle@theguildofcalamitousintent.com'
        }
      }
    ],
    customer: {
      id: 'id-5',
      customerNo: '5',
      firstName: 'Mathew',
      lastName: 'Cartoon',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Comics',
      organisationUnit: 'Art Studio',
      name: 'Mathew Cartoon'
    }
  },
  {
    id: 'NTN-22',
    caseNumber: 'NTN-22',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-23T07:53:15.852Z'),
    createdDate: moment('2020-01-23T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-21',
    caseNumber: 'NTN-21',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-22T07:53:15.852Z'),
    createdDate: moment('2020-01-22T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-20',
    caseNumber: 'NTN-20',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-21T07:53:15.852Z'),
    createdDate: moment('2020-01-21T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-19',
    caseNumber: 'NTN-19',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-20T07:53:15.852Z'),
    createdDate: moment('2020-01-20T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-18',
    caseNumber: 'NTN-18',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-12-19'),
    createdDate: moment('2020-01-19T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    adminGroup: 'The Sith',
    status: 'Approved',
    caseHandler: {
      name: 'Captain Phasma',
      telephoneNo: '01-555-123456',
      emailAddress: 'phasma@kudos4troopers.com'
    },
    customerId: 'id-4',
    subCases: [
      {
        id: 'ABS-2',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-4',
      firstName: 'Obi Wan',
      lastName: 'Kenobi',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Master',
      workSite: 'Tatooine',
      organisationUnit: 'Jedi',
      address: {
        premiseNo: '123',
        addressLine1: 'Main street',
        addressLine2: "Where it's at",
        addressLine3: 'Two turn tables',
        addressLine4: 'And a microphone',
        addressLine5: 'Somewhere',
        addressLine6: 'CA',
        addressLine7: 'Way up high',
        postCode: 'AB123456',
        country: {
          name: 'USA'
        },
        classExtensionInformation: []
      },
      name: 'Obi Wan Kenobi'
    }
  },
  {
    id: 'NTN-17',
    caseNumber: 'NTN-17',
    customerId: 'id-5',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-18T07:53:15.852Z'),
    createdDate: moment('2020-01-18T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    subCases: [
      {
        id: 'CLM-10',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      },
      {
        id: 'ABS-1',
        caseType: 'Absence Case',
        caseHandler: {
          name: 'Red Mantle',
          telephoneNo: '01-111-123456',
          emailAddress: 'redmantle@theguildofcalamitousintent.com'
        }
      }
    ],
    customer: {
      id: 'id-5',
      firstName: 'Mathew',
      lastName: 'Cartoon',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Animator',
      workSite: 'Comics',
      organisationUnit: 'Art Studio',
      name: 'Mathew Cartoon'
    }
  },
  {
    id: 'NTN-16',
    caseNumber: 'NTN-16',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Accident or treatment required for an injury',
      domainName: 'NotificationReason'
    },
    notificationDate: moment('2020-01-17T07:53:15.852Z'),
    createdDate: moment('2020-01-17T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-4',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  },
  {
    id: 'NTN-15',
    caseNumber: 'NTN-15',
    notificationReason: {
      domainId: 290,
      fullId: 9280003,
      name: 'Pregnancy, birth or related medical treatment',
      domainName: 'NotificationReason',
      _links: {
        domain: 'http://api.server.context/enum-domains/290',
        instance:
          'http://api.server.context/enum-domains/290/enum-instances/9280003'
      }
    },
    notificationDate: moment('2020-01-16T07:53:15.852Z'),
    createdDate: moment('2020-01-16T07:53:15.852Z'),
    expectedRTWDate: moment('2021-01-03'),
    status: 'Approved',
    customerId: 'id-3',
    subCases: [
      {
        id: 'CLM-14',
        caseType: 'Group Disability Claim',
        caseHandler: {
          name: 'Kylo Ren',
          telephoneNo: '01-111-123456',
          emailAddress: 'kyloren@thefirstorder.com'
        }
      }
    ],
    customer: {
      id: 'id-3',
      firstName: 'Kylo',
      lastName: 'Ren',
      dateOfBirth: '1990-01-30T07:53:15.852Z',
      jobTitle: 'Leader',
      workSite: 'Starkiller Base',
      organisationUnit: 'The Sith',
      name: 'Kylo Ren'
    }
  }
];
