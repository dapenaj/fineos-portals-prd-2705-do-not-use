/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Formik } from 'formik';
import {
  render,
  within,
  fireEvent,
  act,
  wait,
  waitForDomChange,
  getByText as globalGetByText
} from '@testing-library/react';
import { noop as _noop } from 'lodash';
import userEvent from '@testing-library/user-event';

import { ContextWrapper } from '../utils';

type CreateFormPartValidationAssertionOptions = {
  translations?: React.ComponentProps<typeof ContextWrapper>['translations'];
  initialValues: React.ComponentProps<typeof Formik>['initialValues'];
  validationSchema: React.ComponentProps<typeof Formik>['validationSchema'];
  Component: React.ComponentType;
};

const getAntSelectDropdown = () =>
  document.body.querySelector('.ant-select-dropdown') as HTMLElement;

const getInput = (input: HTMLElement): HTMLElement => {
  if (input.tagName === 'DIV' && !input.hasAttribute('role')) {
    return input.querySelector('[role]') as HTMLElement;
  }
  return input;
};

const typeValue = async (input: HTMLElement, value: string) => {
  if (input.tagName === 'INPUT' || input.tagName === 'TEXTAREA') {
    await act(async () => {
      await userEvent.type(input, value);
      fireEvent.blur(input);
      return wait();
    });
  } else if (input.tagName === 'DIV') {
    const role = input.getAttribute('role');
    if (role === 'combobox') {
      // ant select
      await act(() => {
        userEvent.click(input);
        return wait();
      });

      if (value) {
        await act(async () => {
          userEvent.click(globalGetByText(getAntSelectDropdown(), value));

          await waitForDomChange({
            container: getAntSelectDropdown()
          });
        });
      } else {
        await act(async () => {
          fireEvent.blur(input);
          await waitForDomChange({
            container: getAntSelectDropdown()
          });
        });
      }
    } else {
      throw new Error('typedValue called with not supported role ' + role);
    }
  } else {
    throw new Error(
      'typedValue called with not supported tag ' + input.tagName
    );
  }
};

export const createFormPartialValidationTesting = ({
  translations = {},
  initialValues,
  validationSchema,
  Component
}: CreateFormPartValidationAssertionOptions) => async (
  testId: string,
  fieldLabel: string,
  value: string,
  errorMessage: string
) => {
  const { getByTestId } = render(
    <ContextWrapper
      translations={{
        ...translations,
        'FINEOS_COMMON.GENERAL.OPTIONAL_LABEL': '(optional)'
      }}
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={_noop}
      >
        <Component />
      </Formik>
    </ContextWrapper>
  );

  await wait();

  const wrapper = within(getByTestId(testId));
  const input = getInput(
    within(getByTestId(testId)).getByLabelText(fieldLabel)
  );

  await typeValue(input, value);

  if (errorMessage) {
    if (input.tagName === 'INPUT' || input.tagName === 'TEXTAREA') {
      expect(input).toBeInvalid();
    } else {
      expect(input).toHaveAttribute('aria-invalid', 'true');
    }
    expect(wrapper.getByText(errorMessage)).toBeInTheDocument();
  } else {
    if (input.tagName === 'INPUT' || input.tagName === 'TEXTAREA') {
      expect(input).toBeValid();
    } else {
      expect(input).not.toHaveAttribute('aria-invalid', 'true');
    }
  }
};
