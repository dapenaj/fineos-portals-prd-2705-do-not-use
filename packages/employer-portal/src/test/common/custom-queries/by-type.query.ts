/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland
  
  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED
  
  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import {
  queryHelpers,
  buildQueries,
  Matcher,
  MatcherOptions
} from '@testing-library/react';

export const queryAllByType = (
  container: HTMLElement,
  id: Matcher,
  options: MatcherOptions
) => queryHelpers.queryAllByAttribute('type', container, id, options);

const getMultipleError = (container: HTMLElement, id: Matcher) =>
  `Found multiple elements with the type attribute of: ${id}`;
const getMissingError = (container: HTMLElement, id: Matcher) =>
  `Unable to find an element with the type attribute of: ${id}`;

const [
  queryByType,
  getAllByType,
  getByType,
  findAllByType,
  findByType
] = buildQueries(queryAllByType, getMultipleError, getMissingError);

export { queryByType, getAllByType, getByType, findAllByType, findByType };
