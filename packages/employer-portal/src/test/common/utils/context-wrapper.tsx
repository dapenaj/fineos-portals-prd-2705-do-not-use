/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Theme,
  GroupClientPermissions,
  PermissionsScope,
  Formatting,
  SharedState,
  flattenMessages
} from 'fineos-common';
import { MemoryRouter } from 'react-router';
import { IntlProvider } from 'react-intl';

import { MOCKED_THEME_FIXTURE } from '../../fixtures/mocked-theme.fixture';
import { FINEOS_COMMON_FIXTURE } from '../../fixtures/fineos-common.fixture';
import { getMockTranslations } from '../../common/utils';
import { LanguageContext } from '../../../app/shared/layouts/language/language.context';

type ContextWrapperProps = {
  translations?: Record<string, string>;
  permissions?: GroupClientPermissions[];
};

export const ContextWrapper: React.FC<ContextWrapperProps> = ({
  children,
  permissions = [],
  translations = {}
}) => {
  const parsedTranslations = getMockTranslations();
  const localesToPass = {
    ...parsedTranslations,
    ...translations,
    ...FINEOS_COMMON_FIXTURE
  };

  return (
    <Theme.Provider value={MOCKED_THEME_FIXTURE}>
      <PermissionsScope.Provider
        value={{ permissions: new Set(permissions), logout: jest.fn() }}
      >
        <MemoryRouter>
          <SharedState>
            <LanguageContext.Provider
              value={{
                language: 'en',
                setLanguage: () => {},
                decimalSeparator: '.'
              }}
            >
              <IntlProvider
                locale="en"
                messages={flattenMessages(localesToPass)}
              >
                <Formatting.Provider
                  value={{
                    date: 'DD-MM-YYYY',
                    dateTime: 'YYYY-MM-DDTHH:mm:ss',
                    currency: 'USD'
                  }}
                >
                  {children}
                </Formatting.Provider>
              </IntlProvider>
            </LanguageContext.Provider>
          </SharedState>
        </MemoryRouter>
      </PermissionsScope.Provider>
    </Theme.Provider>
  );
};
