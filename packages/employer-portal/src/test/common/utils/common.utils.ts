/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import { queryHelpers, screen } from '@testing-library/react';
import { isObject as _isObject } from 'lodash';
import { commonLibraryTranslations } from 'fineos-common';

import * as employerPortalTranslations from '../../../../src/app/i18n/locale-data/en.json';
export const releaseEventLoop = (): Promise<void> =>
  new Promise<void>(f => setTimeout(f));

export const mockTranslations = (
  ...translations: (string | Record<string, string>)[]
) =>
  translations.reduce((acc: Record<string, string>, curr) => {
    if (typeof curr === 'string') {
      acc[curr] = curr;
    } else {
      for (const id in curr) {
        if (curr.hasOwnProperty(id)) {
          acc[id] = curr[id];
        }
      }
    }
    return acc;
  }, {});

const parseTranslationsToMocks = (
  locales: Record<string, string | object>,
  temporaryElements: string[] = []
): string[] => {
  const formatToString = (...elementsToSeparate: string[]) =>
    elementsToSeparate.join('.');
  return Object.keys(locales).reduce(
    (acc: string[], curr: string) =>
      _isObject(locales[curr])
        ? [
            ...acc,
            ...parseTranslationsToMocks((locales as any)[curr], [
              ...temporaryElements,
              curr
            ])
          ]
        : [...acc, formatToString(...temporaryElements, curr)],
    []
  );
};

export const getMockTranslations = () =>
  mockTranslations(
    ...parseTranslationsToMocks(commonLibraryTranslations),
    ...parseTranslationsToMocks(employerPortalTranslations)
  );

export const buildTranslationWithParams = (
  translation: string,
  params: string[]
): Record<string, string> => ({
  [translation]: `${translation}[${params
    .map(param => `${param}:{${param}}`)
    .join(';')}]`
});

export const resolveTranslationWithParams = (
  translation: string,
  params: [string, string | number][]
) =>
  `${translation}[${params
    .map(([paramName, paramValue]) => `${paramName}:${paramValue}`)
    .join(';')}]`;

// normally this shouldn't be used only for weird cases when id of labelledby is placed on parent node
export const getByAriaLabelledBy = queryHelpers.queryByAttribute.bind(
  null,
  'aria-labelledby'
);

export const getByLabelTextWithSuffix = (label: string, suffixLabel: string) =>
  screen.getByLabelText(label + suffixLabel);
