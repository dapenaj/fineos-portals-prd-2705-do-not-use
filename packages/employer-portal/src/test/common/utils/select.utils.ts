/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import userEvent from '@testing-library/user-event';

import { within, getByText, wait, act, waitForElement } from '../custom-render';

const openOptionsMenu = async (selectInput: HTMLElement) => {
  const input =
    selectInput.tagName !== 'INPUT'
      ? selectInput.querySelector('input')!
      : selectInput;
  const dropdown = input.closest('.ant-select')!;
  const popupId = input.getAttribute('aria-controls') as string;
  const existingPopup = document.getElementById(popupId);

  await act(async () => {
    userEvent.click(dropdown!.querySelector('.ant-select-selector')!);

    if (existingPopup) {
      await wait();
    } else {
      await waitForElement(() => document.getElementById(popupId));
    }
  });

  return document.getElementById(popupId)!.parentNode as HTMLElement;
};

export const selectAntOptionByText = (
  selectInput: HTMLElement,
  content: string
) => {
  return act(async () => {
    const popupContainer = await openOptionsMenu(selectInput);

    const options = within(popupContainer).getAllByText(content);
    // there maybe duplicates
    const selectOption = options[options.length - 1];
    userEvent.click(selectOption);
    return wait();
  });
};

export const isAntSelectOptionNotExistsByContent = async (
  selectInput: HTMLElement,
  content: string
): Promise<boolean> => {
  const popupContainer = await openOptionsMenu(selectInput);
  return within(popupContainer).queryByText(content) === null;
};

export const getAntSelectBySelectedText = (
  container: HTMLElement,
  content: string
) =>
  getByText(container, content)
    .closest('.ant-select')!
    .querySelector('input')!;

export const getAntSelectPlaceholder = (selectInput: HTMLElement) =>
  selectInput
    .closest('.ant-select')!
    .querySelector('.ant-select-selection-placeholder');
