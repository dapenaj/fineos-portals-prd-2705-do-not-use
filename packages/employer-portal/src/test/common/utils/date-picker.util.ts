/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import userEvent from '@testing-library/user-event';

import { act, getByTitle, wait } from '../custom-render';

export const selectDate = (datePicker: HTMLElement, date: string) => {
  return act(async () => {
    userEvent.click(datePicker);

    await wait();

    userEvent.click(
      getByTitle(
        document.querySelector('.ant-picker-content') as HTMLElement,
        date
      )
    );
    return wait();
  });
};

export const selectTodayDate = (datePicker: HTMLElement) => {
  return act(async () => {
    userEvent.click(datePicker);

    await wait();

    userEvent.click(document.querySelector('.ant-picker-cell-today')!);
    return wait();
  });
};

export const selectDateRange = (
  rangePicker: HTMLElement,
  ranges: [string, string]
) => {
  return act(async () => {
    userEvent.click(rangePicker.querySelector('input')!);

    await wait();

    const element = document.querySelector(
      '.ant-picker-content'
    ) as HTMLElement;

    userEvent.click(getByTitle(element, ranges[0]));
    userEvent.click(getByTitle(element, ranges[1]));
    return wait();
  });
};
