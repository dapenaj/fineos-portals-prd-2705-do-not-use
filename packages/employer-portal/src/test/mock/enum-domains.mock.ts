// global enum-domain mock, as it may be frustrating mock in separate tests
jest.mock('../../app/shared/enum-domain/enum-instance.api', () => ({
  fetchEnumInstances: jest.fn().mockImplementation((enumDomainId: number) => {
    if (enumDomainId === 138) {
      return Promise.resolve([
        {
          id: '4416000',
          name: 'Unknown',
          fullId: 4416000
        },
        {
          id: '4416002',
          name: 'AK',
          fullId: 4416002
        },
        { id: '4416001', name: 'AL', fullId: 4416001 },
        {
          id: '4416004',
          name: 'AR',
          fullId: 4416004
        },
        { id: '4416003', name: 'AZ', fullId: 4416003 },
        {
          id: '4416005',
          name: 'CA',
          fullId: 4416005
        },
        { id: '4416006', name: 'CO', fullId: 4416006 },
        {
          id: '4416007',
          name: 'CT',
          fullId: 4416007
        },
        { id: '4416009', name: 'DC', fullId: 4416009 },
        {
          id: '4416008',
          name: 'DE',
          fullId: 4416008
        },
        { id: '4416010', name: 'FL', fullId: 4416010 },
        {
          id: '4416011',
          name: 'GA',
          fullId: 4416011
        },
        { id: '4416012', name: 'HI', fullId: 4416012 },
        {
          id: '4416016',
          name: 'IA',
          fullId: 4416016
        },
        { id: '4416013', name: 'ID', fullId: 4416013 },
        {
          id: '4416014',
          name: 'IL',
          fullId: 4416014
        },
        { id: '4416015', name: 'IN', fullId: 4416015 },
        {
          id: '4416017',
          name: 'KS',
          fullId: 4416017
        },
        { id: '4416018', name: 'KY', fullId: 4416018 },
        {
          id: '4416019',
          name: 'LA',
          fullId: 4416019
        },
        { id: '4416022', name: 'MA', fullId: 4416022 },
        {
          id: '4416021',
          name: 'MD',
          fullId: 4416021
        },
        { id: '4416020', name: 'ME', fullId: 4416020 },
        {
          id: '4416023',
          name: 'MI',
          fullId: 4416023
        },
        { id: '4416024', name: 'MN', fullId: 4416024 },
        {
          id: '4416026',
          name: 'MO',
          fullId: 4416026
        },
        { id: '4416025', name: 'MS', fullId: 4416025 },
        {
          id: '4416027',
          name: 'MT',
          fullId: 4416027
        },
        { id: '4416034', name: 'NC', fullId: 4416034 },
        {
          id: '4416035',
          name: 'ND',
          fullId: 4416035
        },
        { id: '4416028', name: 'NE', fullId: 4416028 },
        {
          id: '4416030',
          name: 'NH',
          fullId: 4416030
        },
        { id: '4416031', name: 'NJ', fullId: 4416031 },
        {
          id: '4416032',
          name: 'NM',
          fullId: 4416032
        },
        { id: '4416029', name: 'NV', fullId: 4416029 },
        {
          id: '4416033',
          name: 'NY',
          fullId: 4416033
        },
        { id: '4416036', name: 'OH', fullId: 4416036 },
        {
          id: '4416037',
          name: 'OK',
          fullId: 4416037
        },
        { id: '4416038', name: 'OR', fullId: 4416038 },
        {
          id: '4416039',
          name: 'PA',
          fullId: 4416039
        },
        { id: '4416040', name: 'RI', fullId: 4416040 },
        {
          id: '4416041',
          name: 'SC',
          fullId: 4416041
        },
        { id: '4416042', name: 'SD', fullId: 4416042 },
        {
          id: '4416043',
          name: 'TN',
          fullId: 4416043
        },
        { id: '4416044', name: 'TX', fullId: 4416044 },
        {
          id: '4416045',
          name: 'UT',
          fullId: 4416045
        },
        { id: '4416047', name: 'VA', fullId: 4416047 },
        {
          id: '4416046',
          name: 'VT',
          fullId: 4416046
        },
        { id: '4416048', name: 'WA', fullId: 4416048 },
        {
          id: '4416050',
          name: 'WI',
          fullId: 4416050
        },
        { id: '4416049', name: 'WV', fullId: 4416049 },
        {
          id: '4416051',
          name: 'WY',
          fullId: 4416051
        },
        { id: '4416053', name: 'AA', fullId: 4416053 },
        {
          id: '4416054',
          name: 'AE',
          fullId: 4416054
        },
        { id: '4416055', name: 'AP', fullId: 4416055 },
        {
          id: '4416058',
          name: 'AS',
          fullId: 4416058
        },
        { id: '4416060', name: 'FM', fullId: 4416060 },
        {
          id: '4416057',
          name: 'GU',
          fullId: 4416057
        },
        {
          id: '4416061',
          name: 'MH',
          fullId: 4416061
        },
        {
          id: '4416059',
          name: 'MP',
          fullId: 4416059
        },
        {
          id: '4416052',
          name: 'PR',
          fullId: 4416052
        },
        {
          id: '4416062',
          name: 'PW',
          fullId: 4416062
        },
        {
          id: '4416056',
          name: 'VI',
          fullId: 4416056
        },
        {
          id: '4416063',
          name: 'UM',
          fullId: 4416063
        }
      ]);
    }
    if (enumDomainId === 235) {
      return Promise.resolve([
        {
          id: '7520000',
          name: 'Unknown',
          fullId: 7520000
        },
        {
          id: '7520001',
          name: 'Weekly',
          fullId: 7520001
        },
        {
          id: '7520002',
          name: 'Bi-Weekly',
          fullId: 7520002
        },
        {
          id: '7520003',
          name: 'Semi-Monthly',
          fullId: 7520003
        },
        {
          id: '7520004',
          name: 'Monthly',
          fullId: 7520004
        },
        {
          id: '7520005',
          name: 'Yearly',
          fullId: 7520005
        }
      ]);
    }
    if (enumDomainId === 277) {
      return Promise.resolve([
        {
          id: '8864001',
          name: 'Fixed',
          fullId: 8864001
        },
        {
          id: '8864002',
          name: '2 weeks Rotating',
          fullId: 8864002
        },
        {
          id: '8864003',
          name: '3 weeks Rotating',
          fullId: 8864003
        },
        {
          id: '8864004',
          name: '4 weeks Rotating',
          fullId: 8864004
        },
        {
          id: '8864005',
          name: 'Variable',
          fullId: 8864005
        },
        {
          id: '8864006',
          name: 'Weekly Work Pattern',
          fullId: 8864006
        }
      ]);
    }
    if (enumDomainId === 21) {
      return Promise.resolve([
        {
          id: '672007',
          name: 'USA',
          fullId: 672007
        },
        {
          id: '672010',
          name: 'Canada',
          fullId: 672010
        },
        {
          id: '672001',
          name: 'Ireland',
          fullId: 672001
        }
      ]);
    }
    if (enumDomainId === 51) {
      return Promise.resolve([
        {
          id: '1632001',
          name: 'Phone',
          domainId: 51,
          fullId: 1632001
        },
        {
          id: '1632004',
          name: 'Mobile',
          domainId: 51,
          fullId: 1632004
        },
        {
          id: '1660001',
          name: 'Cell',
          domainId: 51,
          fullId: 1660001
        }
      ]);
    }
    if (enumDomainId === 169) {
      return Promise.resolve([
        {
          name: 'Monday',
          id: '169',
          fullId: 5408001
        },
        {
          id: '5408002',
          name: 'Tuesday',
          fullId: 5408002
        },
        {
          id: '5408003',
          name: 'Wednesday',
          fullId: 5408003
        },
        {
          id: '5408004',
          name: 'Thursday',
          fullId: 5408004
        },
        {
          id: '5408005',
          name: 'Friday',
          fullId: 5408005
        },
        {
          id: '5408006',
          name: 'Saturday',
          fullId: 5408006
        },
        {
          id: '5408007',
          name: 'Sunday',
          fullId: 5408007
        }
      ]);
    }
    return Promise.resolve([]);
  })
}));
