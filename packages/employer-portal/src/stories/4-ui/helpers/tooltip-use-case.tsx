import React from 'react';
import { Tooltip, Icon } from 'fineos-common';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

export const TooltipUseCase = () => (
  <Tooltip tooltipContent="test content">
    <Icon
      style={{ cursor: 'pointer' }}
      icon={faQuestionCircle}
      iconLabel="question-circle"
      size="2x"
    />
  </Tooltip>
);
