import React from 'react';
import {
  Button,
  ButtonType,
  ToastType,
  useToast,
  FormattedMessage
} from 'fineos-common';

export const ToastUseCase = () => {
  const { show } = useToast();

  return (
    <>
      <p
        style={{
          display: 'flex',
          justifyContent: 'space-evenly',
          width: '100%'
        }}
      >
        <Button
          buttonType={ButtonType.GHOST}
          onClick={() =>
            show(
              ToastType.ERROR,
              <FormattedMessage
                id="SOME_TRANSLATION"
                defaultMessage="Error message"
              />
            )
          }
        >
          Error toast
        </Button>

        <Button
          buttonType={ButtonType.DANGER}
          onClick={() =>
            show(
              ToastType.WARNING,
              <FormattedMessage
                id="SOME_TRANSLATION"
                defaultMessage="Warning message"
              />
            )
          }
        >
          Warning toast
        </Button>

        <Button
          buttonType={ButtonType.DASHED}
          onClick={() =>
            show(
              ToastType.INFO,
              <FormattedMessage
                id="SOME_TRANSLATION"
                defaultMessage="Info message"
              />
            )
          }
        >
          Info toast
        </Button>

        <Button
          buttonType={ButtonType.PRIMARY}
          onClick={() =>
            show(
              ToastType.SUCCESS,
              <FormattedMessage
                id="SOME_TRANSLATION"
                defaultMessage="Success message"
              />
            )
          }
        >
          Success toast
        </Button>
      </p>
    </>
  );
};
