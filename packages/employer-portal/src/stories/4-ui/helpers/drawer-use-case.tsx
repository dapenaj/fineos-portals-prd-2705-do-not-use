import React from 'react';
import {
  Button,
  Drawer,
  DrawerProps,
  Popconfirm,
  Icon,
  createThemedStyles
} from 'fineos-common';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const useThemedStyles = createThemedStyles(theme => ({
  drawer: {
    '& .ant-drawer-close': {
      padding: 0
    }
  }
}));

export const DrawerWidth = {
  NARROW: 480,
  MEDIUM: 640,
  WIDE: 820,
  EXTRA_WIDE: 1000
};

export const DrawerUseCase = (
  props: DrawerProps & { id: string | number | undefined }
) => {
  const [isVisible, setIsVisible] = React.useState(false);
  const isIconPopconfirm = props.id === 'popconfirm';
  const themedStyles = useThemedStyles();
  return (
    <>
      <Button onClick={() => setIsVisible(true)}>{`Open ${props.id ||
        ''} drawer`}</Button>
      <Drawer
        {...props}
        isVisible={isVisible}
        onClose={() => !isIconPopconfirm && setIsVisible(false)}
        className={isIconPopconfirm ? themedStyles.drawer : ''}
      />
    </>
  );
};
