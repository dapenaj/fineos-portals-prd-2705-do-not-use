/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import { Field } from 'formik';
import * as Yup from 'yup';
import { Row, Col } from 'antd';
import {
  Button,
  ButtonType,
  Form,
  FormattedMessage,
  FormGroup,
  TextArea,
  TextInput
} from 'fineos-common';

import { FormikWithResponseHandling } from '../../../app/shared/formik-with-response-handling';

export const ValidationExample = () => (
  <FormikWithResponseHandling
    initialValues={{
      name: '',
      message: ''
    }}
    validationSchema={Yup.object({
      name: Yup.string().required('FINEOS_COMMON.VALIDATION.REQUIRED'),
      message: Yup.string().max(10, 'Should be less than 10 characters')
    })}
    onSubmit={() => console.log('submit')}
  >
    <Form>
      <FormGroup isRequired={true} label="Name">
        <Field name="name" component={TextInput} placeholder="Name" />
      </FormGroup>

      <FormGroup
        label="Message"
        counterMaxLength={10}
        immediateErrorMessages={['Should be less than 10 characters']}
        counterMessageId="NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_COUNTER"
      >
        <Field
          name="message"
          component={TextArea}
          placeholder={
            <FormattedMessage id="NOTIFICATIONS.ALERTS.MESSAGES.REPLY_MESSAGE_PLACEHOLDER" />
          }
        />
      </FormGroup>
      <Row justify="end">
        <Col>
          <Button htmlType="submit" buttonType={ButtonType.PRIMARY}>
            Save
          </Button>
        </Col>
      </Row>
    </Form>
  </FormikWithResponseHandling>
);
