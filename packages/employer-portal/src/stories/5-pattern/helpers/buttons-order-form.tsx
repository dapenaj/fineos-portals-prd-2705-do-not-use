/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React from 'react';
import {
  Button,
  ButtonType,
  FormattedMessage,
  FormGroup,
  TextInput
} from 'fineos-common';
import { Row, Col } from 'antd';
import { Field, Form } from 'formik';

import { FormikWithResponseHandling } from '../../../app/shared/formik-with-response-handling';

export const ButtonsOrderForm = () => (
  <FormikWithResponseHandling
    initialValues={{}}
    onSubmit={() => console.log('submit')}
  >
    <Form>
      <p>
        You can play around here with focus (<strong>Tab</strong> - moves focus
        forward,
        <strong>Shift + Tab</strong> moves focus back.
      </p>

      <FormGroup
        label={
          <FormattedMessage id="PROFILE.EMPLOYMENT_DETAILS_TAB.EDIT.JOB_TITLE" />
        }
      >
        <Field name="jobTitle" component={TextInput} />
      </FormGroup>
      <Row justify="end">
        <Col
          style={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row-reverse'
          }}
        >
          <Button buttonType={ButtonType.PRIMARY}>OK</Button>

          <Button buttonType={ButtonType.LINK} style={{ marginRight: '1rem' }}>
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  </FormikWithResponseHandling>
);
