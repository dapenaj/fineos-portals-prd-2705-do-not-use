/*-------------------------------------------------------------------------------------------
  FINEOS Corporation
  FINEOS House, EastPoint Business Park,
  Dublin 3, Ireland

  (c) Copyright FINEOS Corporation.
  ALL RIGHTS RESERVED

  The software and information contained herein are proprietary to, and comprise valuable
  trade secrets of, FINEOS Corporation, which intends to preserve as trade secrets such
  software and information. This software should only be furnished subject to a written
  license agreement and may only be used, copied, transmitted, and stored in accordance
  with the terms of such license and with the inclusion of the above copyright notice.
  If there is no written License Agreement between you and FINEOS Corporation, then you
  have received this software in error and should be returned to FINEOS Corporation or
  destroyed immediately, and you should also notify FINEOS Corporation. This software and
  information or any other copies thereof may not be provided or otherwise made available
  to any person who is not authorized to receive it pursuant to a written license Agreement
  executed with FINEOS Corporation.
-------------------------------------------------------------------------------------------*/

import React, { useState } from 'react';
import {
  Button,
  ButtonType,
  FormGroup,
  DropdownInput,
  FormattedMessage
} from 'fineos-common';
import { ConcurrencyUpdateError } from 'fineos-js-api-client';
import { Row, Col } from 'antd';
import { Form, Field } from 'formik';

import { FormikWithResponseHandling } from '../../../app/shared';

export const FormModalError = () => {
  const [partialError, setPartialError] = useState(false);

  return (
    <FormikWithResponseHandling
      initialValues={{
        errorType: 'concurrency'
      }}
      genericErrorModalMessage={
        partialError ? (
          <FormattedMessage id="FINEOS_COMMON.SOMETHING_WENT_WRONG.PARTIAL_API_ERROR_MESSAGE" />
        ) : (
          <FormattedMessage id="FINEOS_COMMON.SOMETHING_WENT_WRONG.DEFAULT_API_ERROR_MESSAGE" />
        )
      }
      onSubmit={({ errorType }) => {
        switch (errorType) {
          case 'concurrency':
            throw new ConcurrencyUpdateError({
              errorType
            });
          case 'generic':
            throw new Error();
          case 'partial':
            setPartialError(true);
            throw new Error();
        }
      }}
    >
      <Form style={{ width: '10em', margin: '0 auto' }}>
        <Row>
          <FormGroup label="Select error type">
            <Field
              name="errorType"
              component={DropdownInput}
              placeholder="Please select"
              optionElements={[
                'concurrency',
                'generic',
                'partial'
              ].map(title => ({ value: title, title }))}
            />
          </FormGroup>
        </Row>
        <Row justify="end">
          <Col>
            <Button buttonType={ButtonType.PRIMARY} htmlType="submit">
              Save
            </Button>
          </Col>
        </Row>
      </Form>
    </FormikWithResponseHandling>
  );
};
