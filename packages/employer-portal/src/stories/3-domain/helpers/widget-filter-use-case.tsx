import React, { useState } from 'react';
import { FormattedMessage, Select } from 'fineos-common';

import {
  useDateFilters,
  WidgetFilter
} from '../../../app/modules/my-dashboard/widget-shared';
import styles from '../../../app/modules/my-dashboard/my-dashboard.module.scss';
import { useWidgetThemedStyles } from '../../../app/modules/my-dashboard/dashboard-widget.utils';
import { DateFilter } from '../../../app/shared';

export const WidgetFilterUseCase = () => {
  const { futureFilterOptions } = useDateFilters();
  const [selectValue, setSelectValue] = useState<DateFilter>(
    futureFilterOptions[0].value
  );
  const themedStyles = useWidgetThemedStyles();
  return (
    <WidgetFilter
      label={
        <FormattedMessage
          id="WIDGET.EMPLOYEES_EXPECTED_TO_RETURN_TO_WORK"
          className={themedStyles.filterText}
        />
      }
      selectInput={
        <Select
          bordered={false}
          value={selectValue}
          data-test-el="expected-to-rtw-filter"
          dropdownClassName={styles.selectDropdown}
          className={themedStyles.select}
          onChange={(value: unknown) => setSelectValue(value as DateFilter)}
          name="filter"
          optionElements={futureFilterOptions}
          dropdownMatchSelectWidth={false}
        />
      }
    />
  );
};
