const puppeteer = require('puppeteer');
const path = require('path');
const { spawn } = require('child_process');

const PORTAL_ARG = '--portal=';
const COMMAND_ARG = '--command=';
const USER_ARG = '--user=';
const NO_SIGNATURE_ARG = '--no-signature';

const portalArgEntry = process.argv.find(arg => arg.startsWith(PORTAL_ARG));
const commandArgEntry = process.argv.find(arg => arg.startsWith(COMMAND_ARG));
const userArgEntry = process.argv.find(arg => arg.startsWith(USER_ARG));
const noSignature = process.argv.some(arg => arg === NO_SIGNATURE_ARG);

if (!portalArgEntry) {
  throw new Error('Portal need to be provided via --portal=PORTAL_NAME');
}

if (!commandArgEntry) {
  throw new Error('Command for starting app need to be provided via --command="COMMAND"');
}

const portalPathName = portalArgEntry.substr(PORTAL_ARG.length)
  .replace(path.sep, '');

let envConfig;

try {
  envConfig = require(`fineos-common/e2e/employer-portal/testData/generatedTestData/${portalPathName}/env.json`);
} catch (e) {
  throw new Error(`Unknown portal "${
    portalArgEntry
  }", please ensure that data for this portal exists in fineos-common/e2e/employer-portal/testData/generatedTestData directory`);
}

const command = commandArgEntry.substr(COMMAND_ARG.length);
const userNamePattern = userArgEntry ? userArgEntry.substr(USER_ARG.length) : '';

const user = envConfig.users.filter(({ username }) => username.toLowerCase().includes(userNamePattern.toLowerCase()))[0];

if (!user) {
  throw new Error(`Cannot find any user for portal ${envConfig.appName} by "${userNamePattern}" pattern`);
}

async function getJsonConfig(page, url) {
  console.log('fetching URL: ', url)
  await page.goto(url);
  return page.evaluate(() => JSON.stringify(JSON.parse(document.body.innerText)));
}

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto(envConfig.url);
  await page.waitForSelector('#idp_OtherRpRadioButton');

  await page.click('#idp_OtherRpRadioButton');

  const options = await page.evaluate(() =>
    Array.from(document.querySelectorAll('#idp_RelyingPartyDropDownList option'))
      .map(el => ({ value: el.value, name: el.innerText.trim() })));
  const portalOption = options.find(({ name }) => envConfig.appName === name);
  await page.select('#idp_RelyingPartyDropDownList', portalOption.value);

  console.log('Starting sign in to portal: ', portalOption.name, 'as', user.username);

  await page.click('#idp_SignInButton')

  await page.waitForSelector('#userNameInput');
  await page.type('#userNameInput', user.username);
  await page.type('#passwordInput', user.password);

  console.log('Submitting user name and password');
  await page.click('#submitButton');

  await page.waitForSelector('#root');
  const appBaseUrl = page.url().replace(/\W+$/, '');
  const env = {};

  if (!noSignature) {
    env.REACT_APP_PORTAL_COOKIE = await page.evaluate(() => document.cookie);
    console.log('Sign in cookie:', env.REACT_APP_PORTAL_COOKIE.substr(0, 60) + '...');
  } else {
    console.log('No signature mode, secured cookie not extracted');
  }

  env.REACT_APP_ENV_JSON = await getJsonConfig(page, `${appBaseUrl}/config/env.json`);
  env.REACT_APP_PORTAL_ENV_JSON = await getJsonConfig(page, `${appBaseUrl}/config/portal.env.json`);

  await browser.close();

  const commandForOS = process.platform === 'win32' ? 'yarn.cmd' : 'yarn';
  
  spawn(commandForOS, ['run', command], {
    stdio: 'inherit',
    env:  Object.assign({}, process.env, env)
  });
})();
