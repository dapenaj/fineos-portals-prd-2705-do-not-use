const path = require('path');
const fs = require('fs');
const archiver = require('archiver');

function readParam(paramName) {
  const args = process.argv.slice(1);
  const passedParamName = `--${paramName}=`;

  for (const arg of args) {
    if (arg.startsWith(passedParamName)) {
      return arg.substr(passedParamName.length);
    }
  }

  throw new Error(`${passedParamName} not provided`);
}

// setup for archiver
function generateZip() {
  try {
    var output = fs.createWriteStream(`${outputPath}/${targetName}.zip`);
    var archive = archiver('zip', { zlib: { level: 9 } });

    // set up event listeners
    output.on('close', () => {
      console.log(
        `[zip.js] ${targetPath} has been zipped into ${outputPath}/${targetName}.zip`
      );
    });
    archive.on('warning', err => {
      if (err.code === 'ENOENT') {
        console.log('[zip.js] Warning while generating zip file:', err);
      } else {
        throw err;
      }
    });
    archive.on('error', err => {
      console.log('[zip.js] Error while generating zip file:', err);
      throw err;
    });

    archive.pipe(output);

    // add the target dir to the zip
    archive.directory(`${targetPath}/`, false);

    // generate zip
    archive.finalize();
  } catch (e) {
    console.log('[zip.js] Unexpected error occurred', e);
  }
}

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebook/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolvePath = relativePath => path.resolve(appDirectory, relativePath);

// read params and resolve paths
const targetDir = readParam('target');
const targetName = readParam('name');
const targetPath = resolvePath(targetDir);
const outputPath = resolvePath('output');

// create the output dir if it doesn't exist
fs.mkdir(outputPath, { recursive: true }, err => {
  if (err) {
    console.log('[zip.js] Warning while generating directories:', err);
    throw err;
  } else {
    // if no error when generating directories, then continue
    generateZip();
  }
});
