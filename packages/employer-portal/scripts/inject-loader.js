const fs = require('fs');
const cheerio = require('cheerio');

function readParam(paramName) {
  const args = process.argv.slice(1);
  const passedParamName = `--${paramName}=`;

  for (const arg of args) {
    if (arg.startsWith(passedParamName)) {
      return arg.substr(passedParamName.length);
    }
  }

  throw new Error(`${passedParamName} not provided`);
}

const ENCODING = 'utf8';

const BUILD_INFO_VAR_NAME = '___FINEOS_BUNDLES';

const loaderPath = readParam('loader-path');
const targetHtml = readParam('target-html');

const htmlFileContent = fs.readFileSync(targetHtml, ENCODING);
const $ = cheerio.load(htmlFileContent);

const scripts$ = $('body script[src]');
const scriptUrls = [];

scripts$.each(function () {
  const script$ = $(this);
  scriptUrls.push(script$.attr('src'));
});

scripts$.remove();

const scriptUrlsTag = $('<script type="text/javascript">'
  + `window[${JSON.stringify(BUILD_INFO_VAR_NAME)}] = ${JSON.stringify(scriptUrls)};`
  + '</script>');
const scriptLoaderTag = $(`<script type="text/javascript" src="${loaderPath}"></script>`);

$('body')
  .append(scriptUrlsTag)
  .append(scriptLoaderTag);

fs.writeFileSync(targetHtml, $.html(), ENCODING);
