import React, { useEffect, useMemo, useRef } from 'react';
import {
  loadThemeFonts,
  loadThemeIcons,
  SharedState,
  Theme,
  PermissionsScope,
  AdminPermissions,
  MessagesPermissions,
  ConfiguratorPermissions,
  CreateNotificationPermissions,
  ManageCustomerDataPermissions,
  ManageNotificationPermissions,
  ManageOccupationPermissions,
  ManagePaymentPermissions,
  ViewCustomerDataPermissions,
  ViewNotificationPermissions,
  ViewOccupationPermissions,
  ViewPaymentsPermissions,
  Formatting
} from 'fineos-common';
import { ApiConfigProps } from 'fineos-js-api-client';
import { MemoryRouter } from 'react-router';
import { DocsContainer } from '@storybook/addon-docs/blocks';

import 'fineos-common/dist/index.css';

import defaultConfig from '../src/app/shared/client-config/default-config.json';
import { Config } from '../src/app/config';
import { Internationalise } from '../src/app/i18n/internationalise';
import { ThemeConfigurator } from '../src/app/modules/theme-configurator';

import { mockFetchAndApi } from './mock-fetch';

export const AppDocsContainer = ({ children, context }) => (
  <DocsContainer context={context}>
    <MemoryRouter>
      <Config.Provider value={defaultConfig}>
        <Theme.Provider value={defaultConfig.theme}>
          <Internationalise supportedLanguage="en" decimalSeparator=".">
            <Formatting.Provider
              value={{
                date: ApiConfigProps.displayDateFormat,
                dateTime: ApiConfigProps.displayDateTimeFormat,
                currency: ApiConfigProps.currencyCode
              }}
            >
              {children}
            </Formatting.Provider>
          </Internationalise>
        </Theme.Provider>
      </Config.Provider>
    </MemoryRouter>
  </DocsContainer>
);

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: { expanded: true },
  docs: {
    container: AppDocsContainer
  }
};

mockFetchAndApi();

export const decorators = [
  Story => {
    const storyConfigRef = useRef(defaultConfig);
    const permissionsContextValue = useMemo(
      () => ({
        permissions: new Set([
          Object.values(AdminPermissions),
          Object.values(MessagesPermissions),
          Object.values(ConfiguratorPermissions),
          Object.values(CreateNotificationPermissions),
          Object.values(ManageCustomerDataPermissions),
          Object.values(ManageNotificationPermissions),
          Object.values(ManageOccupationPermissions),
          Object.values(ManagePaymentPermissions),
          Object.values(ViewCustomerDataPermissions),
          Object.values(ViewNotificationPermissions),
          Object.values(ViewOccupationPermissions),
          Object.values(ViewPaymentsPermissions)
        ]),
        logout: () => null
      }),
      []
    );

    useEffect(() => {
      loadThemeFonts(storyConfig.theme, document.head, {
        display: 'swap'
      });
      loadThemeIcons(storyConfig.theme).then(theme => {
        storyConfigRef.current = {
          ...storyConfig,
          theme
        };
      });
    }, []);
    const storyConfig = storyConfigRef?.current;
    return (
      <SharedState>
        <PermissionsScope.Provider value={permissionsContextValue}>
          <MemoryRouter>
            <Config.Provider value={storyConfig}>
              <Theme.Provider value={storyConfig.theme}>
                <Internationalise supportedLanguage="en" decimalSeparator=".">
                  <Formatting.Provider
                    value={{
                      date: ApiConfigProps.displayDateFormat,
                      dateTime: ApiConfigProps.displayDateTimeFormat,
                      currency: ApiConfigProps.currencyCode
                    }}
                  >
                    <div
                      style={{
                        display: 'grid',
                        gridTemplateColumns: '1fr max-content',
                        height: '100%'
                      }}
                    >
                      <div
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          height: '100%'
                        }}
                      >
                        <Story />
                      </div>
                      <ThemeConfigurator
                        isHighContrastOn={false}
                        isFixed={false}
                        config={storyConfig}
                        onConfigChange={newConfig => {
                          storyConfigRef.current = newConfig;
                        }}
                      />
                    </div>
                  </Formatting.Provider>
                </Internationalise>
              </Theme.Provider>
            </Config.Provider>
          </MemoryRouter>
        </PermissionsScope.Provider>
      </SharedState>
    );
  }
];
