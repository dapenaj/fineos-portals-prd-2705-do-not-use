import { SdkConfig } from 'fineos-js-api-client';

import { overrideApiConfigsWithMocks, overrideSdkConfigsWithMocks } from '../src/app/config';
import { AuthenticationService } from '../src/app/shared';
import enumInstances from '../src/mock-data/enum-instances.json';

const mocks = [{
  path: /enum-domains\/(?<enumDomainId>\d+)\/enum-instances$/,
  resolver: ({ enumDomainId }) => ({
    elements: enumInstances.enumInstances
      .filter(({ domainId }) => domainId === parseInt(enumDomainId, 10))
  })
}];

const buildMockLookup = (mockRegistry) => {
  return (url) => {
    const mockEntry = mockRegistry.find(({ path }) => path.test(url));

    if (mockEntry) {
      const { path, resolver } = mockEntry;
      return { data: resolver(path.exec(url).groups) };
    }

    return null;
  };
};

const mockLookup = buildMockLookup(mocks);

export const mockFetchAndApi = () => {
  overrideApiConfigsWithMocks();
  overrideSdkConfigsWithMocks();

  AuthenticationService.getInstance().saveCredentials();

  // override global fetch and connect to our mock server logic
  const originalFetch = window.fetch;
  window.fetch = (requestInfo, ...args) => {
    const targetUrl = typeof requestInfo === 'string' ? requestInfo : requestInfo.url;
    const BASE_URL = SdkConfig.getPath(['apiUrls', 'root'])

    if (targetUrl.startsWith(BASE_URL)) {
      const result = mockLookup(targetUrl);
      if (result !== null) {
        console.log('Request to API found, and can be mocked');
        return new Promise(f =>
            // emulate API delay
            setTimeout(() => f(result.data), 1500)
          )
          .then(finalData => ({
            headers: {},
            ok: true,
            status: 200,
            statusText: 'OK',
            type: 'basic',
            json() {
              return Promise.resolve(finalData);
            },
            text() {
              return Promise.resolve(JSON.stringify(finalData));
            }
          }));
      } else {
        console.log('Request to API found, but no mock provided');
        return originalFetch(requestInfo, ...args);
      }
    }
    return originalFetch(requestInfo, ...args);
  };
};
