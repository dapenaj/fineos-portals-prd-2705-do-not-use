@employer-portal @C115-F02
Feature: C115-F02 – Detailed Breakdown of Notification

  @C115-F02-01 @notification-header @automated @smoke @sanity
  Scenario: 1.1 Notification header layout
    Given I log in to the application
    When I open notification page
    Then the notification header contains: 'Notification ID, Employee Name, Job Title, Organisation Unit, Work Site'
    And the notification summary card contains 'Case Progress, Notification Reason, Notified on' fields

  @C115-F02-02 @notification-header @automated @smoke @sanity
  Scenario: 1.2 Employee name hyperlink
    Given I log in to the application
    When I open notification page
    And I click on the Employee Name in notification header
    Then the system opens the Employee Details page

  @C115-F02-03 @notification-header @manual @cannotGenerateTestData
  Scenario Outline: 1.3-5 Default Value for missing fields
    Given I log in to the application
    And notifications are available for user with undefined "<field>"
    When I open notification page
    Then I see field "<field>" in page header has default value: "<defaultValue>"

    Examples:
      | field             | defaultValue      |
      | Job Title         | Job Title Unknown |
      | Organisation Unit | Unit Unknown      |
      | Work Site         | Location Unknown  |

  @C115-F02-04 @notification-header @AC1.1 @sanity @automated @regression
  Scenario: 1.3-5 Correct data populated in notification header
    Given I log in to the application
    And I open notification with "complete information" and test data "@C115-F02-09-TD-14"
    When I check that notification has "job-title", "organisation-unit", "work-site"
    Then All fields in notification header are populated with corresponding data


  @C115-F02-06 @FAPI-9708-AC1 @notification-summary @automated @regression
  Scenario Outline: 2.2 Notification of type 'Accident or treatment required for an injury'
    Given I log in to the application
    When I open notification with "Accident or treatment required for an injury" and test data "<testData>"
    Then the notification summary card contains additional field "Accident date"
    And the notification summary card contains additional field "Expected return to work"
    And the field "Accident date" shows "<accidentDate>" when "<numberOfDates>" dates available
    And the field "Expected return to work" shows "<ertwDate>"
    And the field "Expected return to work" field is highlighted

    Examples:
      | numberOfDates | accidentDate  | ertwDate | testData          |
      | more than 1   | earliest date | date     | @C115-F02-06-TD-1 |
      | 0             | -             | -        | @C115-F02-06-TD-3 |

    @smoke
    Examples:
      | numberOfDates | accidentDate | ertwDate | testData          |
      | 1             | date         | date     | @C115-F02-06-TD-2 |


  @C115-F02-07 @FAPI-9708-AC1 @notification-summary @automated
  Scenario Outline: 2.3 Notification with Group Disability Claim
    Given I log in to the application
    When I open notification with "at least one Group Disability Claim" and test data "<testData>"
    Then the notification summary card contains additional field "First day missing work"
    And the notification summary card contains additional field "Expected return to work"
    And the field "First day missing work" shows "<fdmwDate>" when "<numberOfDates>" dates available
    And the field "Expected return to work" shows "<ertwDate>"
    And the field "Expected return to work" field is highlighted

    @regression
    Examples:
      | numberOfDates | fdmwDate      | ertwDate | testData          |
      | more than 1   | earliest date | date     | @C115-F02-07-TD-1 |
      | 0             | -             | -        | @C115-F02-07-TD-3 |

    @smoke
    Examples:
      | numberOfDates | fdmwDate | ertwDate | testData          |
      | 1             | date     | date     | @C115-F02-07-TD-2 |

  @C115-F02-08 @FAPI-9708-AC1 @notification-summary @automated @regression
  Scenario Outline: 2.3 Notification with Absence case with a continuous leave request
    Given I log in to the application
    When I open notification with "at least one Absence case with a continuous leave request" and test data "<testData>"
    Then the notification summary card contains additional field "First day missing work"
    And the notification summary card contains additional field "Expected return to work"
    And the field "First day missing work" shows the start date of "<fdmwDate>" job protected leave period when "<numberOfDates>" periods available
    And the field "Expected return to work" shows "<ertwDate>"
    And the field "Expected return to work" field is highlighted

    Examples:
      | numberOfDates | fdmwDate      | ertwDate | testData          |
      | more than 1   | earliest date | date     | @C115-F02-08-TD-1 |
      | 1             | date          | date     | @C115-F02-08-TD-2 |
      | 1             | date          | -        | @C115-F02-08-TD-3 |



# pick as many examples as possible (minimum 1 case per mapped status)
  @C115-F02-09 @notification-case-progress
  Scenario Outline: Sub-case status mapping
    Given I log in to the application
    When I open notification with "one <type> in status <status>" and test data "<testData>"
    Then the notification summary card "Case progress" field text contains "<caseProgress>"

    #Missing test data
    @automated @regression
    Examples:
      | type               | status                  | caseProgress                    | mappedStatus | testData           |
 #     | Absence Case       | Registration            | We are considering your request | Pending      | @C115-F02-09-TD-2  |
      | Absence Case       | Adjudication            | We are considering your request | Pending      | @C115-F02-09-TD-3  |
      | Accommodation Case | Registration            | We are considering your request | Pending      | @C115-F02-09-TD-6  |
      | Accommodation Case | Assessment              | We are considering your request | Pending      | @C115-F02-09-TD-7  |
      | Accommodation Case | Monitoring              | We are considering your request | Pending      | @C115-F02-09-TD-8  |
#      | GDC Case           | Pending                 | We are considering your request | Pending      | @C115-F02-09-TD-10 |
      | GDC Case           | Notification Incomplete | We are considering your request | Pending      | @C115-F02-09-TD-11 |
#      | GDC Case           | Pending Claim           | We are considering your request | Pending      | @C115-F02-09-TD-12 |
      | GDC Case           | Open                    | We are considering your request | Pending      | @C115-F02-09-TD-13 |
      | Absence Case       | Managed Leave           | Your request has been decided   | Decided      | @C115-F02-09-TD-14 |
      | Absence Case       | Completion              | Your request has been decided   | Decided      | @C115-F02-09-TD-15 |
      | Accommodation Case | Completion              | Your request has been decided   | Decided      | @C115-F02-09-TD-16 |
      | GDC Case           | Decided                 | Your request has been decided   | Decided      | @C115-F02-09-TD-17 |
      | Absence Case       | Closed                  | Closed                          | Closed       | @C115-F02-09-TD-18 |
      | Accommodation Case | Closed                  | Closed                          | Closed       | @C115-F02-09-TD-19 |
#      | GDC Case           | Closed                  | Closed                          | Closed       | @C115-F02-09-TD-20 |

    @cannotGenerateTestData
    Examples:
      | type               | status  | caseProgress                    | mappedStatus | testData          |
      | Absence Case       | Unknown | We are considering your request | Pending      | @C115-F02-09-TD-1 |
      | Absence Case       | Extend  | We are considering your request | Pending      | @C115-F02-09-TD-4 |
      | Accommodation Case | Unknown | We are considering your request | Pending      | @C115-F02-09-TD-5 |
      | GDC Case           | Unknown | We are considering your request | Pending      | @C115-F02-09-TD-9 |


  @C115-F02-10 @notification-case-progress
  Scenario Outline: Sub-case status mapping - multiple cases
    Given I log in to the application
    When I open notification with "1 sub-case with <mappedStatus1> and 1 sub-case with <mappedStatus2>" and test data "<testData>"
    Then the notification summary card "Case progress" field text contains "<caseProgress>"

    @automated @regression
    Examples:
      | mappedStatus1 | mappedStatus2 | caseProgress | testData          |
      | Closed        | Closed        | Closed       | @C115-F02-10-TD-1 |

    @cannotGenerateTestData
    Examples:
      | Pending | Pending | We are considering your request        | @C115-F02-10-TD-1 |
      | Pending | Decided | We've made some decisions on your case | @C115-F02-10-TD-2 |
      | Pending | Closed  | We've made some decisions on your case | @C115-F02-10-TD-3 |
      | Decided | Decided | Your request has been decided          | @C115-F02-10-TD-4 |
      | Decided | Closed  | Your request has been decided          | @C115-F02-10-TD-5 |
