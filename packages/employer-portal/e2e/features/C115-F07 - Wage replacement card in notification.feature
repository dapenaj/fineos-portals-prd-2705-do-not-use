@employer-portal @C115-F07
Feature: C115-F07 - Wage replacement card (in-notification)

  @C115-F07-01 @wage-replacement
  Scenario Outline: Wage replacement card visibility
    Given I log in to the application
    When I open notification with "<case>" and test data "<testData>"
    Then I verify that the 'Wage Replacement' card is "<visibility>"

    @smoke @automated
    Examples:
      | case                                 | visibility | testData          |
      | Group Disability Claim with benefits | visible    | @C115-F07-01-TD-1 |
      | Paid Leave with benefits             | visible    | @C115-F07-01-TD-2 |

    @regression @automated
    Examples:
      | case                                    | visibility  | testData          |
      | no Group Disability Claim or Paid Leave | not visible | @C115-F07-01-TD-3 |


  @C115-F07-02 @wage-replacement @automated @regression
  Scenario Outline: Wage replacement card - empty state
    Given I log in to the application
    When I open notification with "<case>" and test data "<testData>"
    Then I verify that the 'Wage Replacement' card is "<cardVisibility>"
    And I verify that the 'Wage Replacement' empty placeholder is "<emptyContainerVisibility>"

    Examples:
      | case                                    | cardVisibility | emptyContainerVisibility | testData          |
      | Group Disability Claim without benefits | not visible    | visible                  | @C115-F07-02-TD-1 |
      | Paid Leave with benefits                | visible        | not visible              | @C115-F07-02-TD-2 |
      | Paid Leave without benefits             | not visible    | not visible              | @C115-F07-02-TD-3 |
      | no Group Disability Claim or Paid Leave | not visible    | not visible              | @C115-F07-02-TD-4 |


  @C115-F07-03 @wage-replacement @smoke @automated @sanity
  Scenario Outline: Wage replacement card - default list view
    Given I log in to the application
    When I open notification with "<case>" and test data "<testData>"
    Then I "can" see list of collapsed items in "Wage Replacement" card
    And each collapsed item on 'Wage Replacement' card has "<fields>" fields

    Examples:
      | case                                 | testData          | fields                                                        |
      | Group Disability Claim with benefits | @C115-F07-03-TD-1 | Benefit Case Type, Benefit Status, Graphical Status Indicator |
      | Paid Leave with benefits             | @C115-F07-03-TD-2 | Leave Plan Name, Benefit Status, Graphical Status Indicator   |


  @C115-F07-04 @wage-replacement @automated @regression @sanity
  Scenario Outline: Wage replacement card - graphical indicator
    Given I log in to the application
    When I open notification with "<description>" and test data "<testData>"
    Then I see "Wage Replacement" item with status "<status>" and "<colour>" indicator color

    Examples:
      | status   | colour | description                    | testData          |
      | Approved | Green  | Wage Replacement Approved item | @C115-F07-04-TD-1 |
      | Pending  | Yellow | Wage Replacement Pending item  | @C115-F07-04-TD-2 |
      | Decided  | White  | Wage Replacement Decided item  | @C115-F07-04-TD-3 |
      | Denied   | Red    | Wage Replacement Denied item   | @C115-F07-04-TD-4 |
      | Closed   | Grey   | Wage Replacement Closed item   | @C115-F07-04-TD-5 |


  # expected order by creationDate is: the newest date should be on the top
  @C115-F07-05 @wage-replacement @automated @regression
  Scenario: Wage replacement card - sorting order
    Given I log in to the application
    When I open notification with "few benefits"
    And I expand "Wage Replacement" details for all available items
    Then I see the "Wage Replacement" items list is sorted by "Benefit start date"

  # expected order by creationDate is: the newest date should be on the top
  @C115-F07-05-01 @AC3.5 @wage-replacement @manual @sanity @cannotGenerateData
  Scenario: Wage replacement card - sorting order
    Given I log in to the application
    When I open notification with "few benefits"
    And I expand "Wage Replacement" details for all available items
    Then I see the list is sorted by Benefit Creation Date and then Benefit Type/Leave Plan Name for same dates


  @C115-F07-06 @wage-replacement @benefit-details @smoke @automated
  Scenario Outline: Wage replacement card - benefit details view
    Given I log in to the application
    When I open notification with "<case>" and test data "<testData>"
    And I expand "Wage Replacement" details for first item
    Then I see first expanded "Wage Replacement" item has "<fields>" details
    And I see first expanded "Wage Replacement" item has 'status text' and 'graphical status indicator'

    Examples:
      | case                   | testData          | fields                                                                              |
      | Group Disability Claim | @C115-F07-06-TD-1 | Managed by, Phone number, Benefit start date, Approved through, Expected Resolution |
      | Paid Leave             | @C115-F07-06-TD-2 | Managed by, Phone number, Approved through, Absence reason                          |


  @C115-F07-07 @wage-replacement @benefit-details @automated @regression
  Scenario Outline: Wage replacement card - benefit details view for Paid Leave
    Given I log in to the application
    When I open notification with "Paid Leave and <option> absence period" and test data "<testData>"
    And I expand "Wage Replacement" details for first item
    Then I see first expanded "Wage Replacement" item with leave period type "<option>" has "<fields>" fields

    Examples:
      | option            | fields             | testData          |
      | Fixed             | Benefit start date | @C115-F07-07-TD-1 |
      | Reduced Schedule  | Benefit start date | @C115-F07-07-TD-2 |
      | Intermittent time | Entitlement begins | @C115-F07-07-TD-3 |


  @C115-F07-08 @AC4.5 @wage-replacement @benefit-details @automated @regression
  Scenario: Wage replacement card - empty benefit start date message
    Given I log in to the application
    When I open notification with "Paid Leave and Intermittent time absence period" and test data "@C115-F07-07-TD-3"
    When I expand "Wage Replacement" details for first item
    Then hover message on the "Entitlement Begins" label is: "From this date, your employee is entitled to receive proportional pay for the periods they've been approved to be out of work"


  @C115-F07-09 @wage-replacement @permissions @manual
  Scenario Outline: Wage replacement user without permissions
    Given I log in to the application as a user "without" permissions to "<secureAction>"
    When I open notification with "<case>"
    Then I verify that the wage replacement card is "not visible"

    Examples:
      | case                                           | secureAction                                           |
      | Group Disability Claim with benefits           | URL_GET_GROUPCLIENT_CLAIMS_BENEFITS                    |
      | Group Disability Claim with recurring benefits | URL_GET_GROUPCLIENT_CLAIMS_DISABILITYBENEFIT_GETSINGLE |
      | Paid Leave with benefits                       | URL_GET_GROUPCLIENT_CLAIMS_BENEFITS                    |
      | Paid Leave with benefits                       | URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS     |


  @C115-F07-10 @wage-replacement @permissions @manual
  Scenario Outline: Wage replacement user with partial permissions
    Given I log in to the application as a user "without" permissions to "<secureAction>"
    When I open notification with "Group Disability Claim and Paid Leave with benefits"
    Then I verify that the wage replacement card doesn't contain "<benefit>"

    Examples:
      | benefit             | secureAction                                           |
      | paid leave benefits | URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS     |
      | recurring benefits  | URL_GET_GROUPCLIENT_CLAIMS_DISABILITYBENEFIT_GETSINGLE |
