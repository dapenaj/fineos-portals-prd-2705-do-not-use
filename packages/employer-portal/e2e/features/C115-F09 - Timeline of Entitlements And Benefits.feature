@employer-portal @C115-F09
Feature: C115-F09 - Timeline of Entitlements & Benefits

  @C115-F09-01 @timeline @AC1.1 @automated @regression
  Scenario Outline: Timeline card visibility
    Given I log in to the application
    When I open notification with "<case>"
    Then I verify that the 'Timeline' card is "<visibility>"

    #Missing test data
    Examples:
      | case                               | visibility  |
      | only Permanent Accommodation cases | not visible |
      | incomplete Intake (no sub-cases)   | not visible |
      | Child Absence Cases                | visible     |
#      | claims                             | visible     |
      | temporary Accommodation cases      | visible     |
#      | mixed Accommodation cases          | visible     |


  @C115-F09-02 @timeline @AC2.1 @automated @regression
  Scenario: Timeline default view
    Given I log in to the application
    When I open notification with "Child Absence Cases"
    Then I verify that the 'Timeline' card is expanded
    When I collapse 'Timeline' card
    Then I verify that the 'Timeline' card is collapsed
    When I expand 'Timeline' card
    Then I verify that the 'Timeline' card is expanded

  @C115-F09-03 @timeline @AC2.2 @AC2.3 @automated @regression
  Scenario: Timeline organisation
    Given I log in to the application
    When I open notification with "Child Absence Cases"
    Then I verify that the timeline card is showing current date
    And I can display legend by clicking 'Show caption' button
    And I can hide legend by clicking 'Hide caption' button


  @C115-F09-04 @timeline @job-protection @AC3.1 @permissions @manual
  Scenario: Timeline for job protected leave - user with permissions
    Given I log in to the application as a user "with" permissions to "URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS"
    When I open notification with "child absence case"
    Then I "can" see "Job Protected Leave" section on the timeline


  @C115-F09-05 @timeline @job-protection @AC3.2 @permissions @manual
  Scenario: Timeline for job protected leave - user without permissions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS"
    When I open notification with "child absence case"
    Then I "can't" see "Job Protected Leave" section on the timeline


  @C115-F09-06 @timeline @job-protection @AC4.1 @automated @regression @sanity
  Scenario: Timeline for job protected leave - multiple absences
    Given I log in to the application
    When I open notification with "Multiple Child Absence Cases"
    Then number of "Job Protected Leave" sections is equal to number of absence cases
    When I expand 'Timeline' "Job Protected Leave" section
    Then text of lines in expanded "Job Protected Leave" section should match text of leave plans in the absence case


  @C115-F09-06-01 @timeline @job-protection @AC4.2 @manual @sanity
  Scenario: Timeline for job protected leave - leave periods
    Given I log in to the application
    When I open notification with "child absence case"
    And I expand timeline section for "job protected leave"
    Then each Leave Plan line is represented as a separate timeline
    And for each Leave Plan I can see all requested leave periods displayed on the timeline
    And the start date for each period bar is Leave Period start date
    And the end date for each period bar is Leave Period end date


  @C115-F09-07 @timeline @job-protection @AC4.3 @manual
  Scenario Outline: Timeline for job protected leave - graphical distinguish on period types
    Given I log in to the application
    And I open notification with "child absence case"
    When I expand timeline section for "job protected leave"
    Then I can see "<lineStyle>" line for period of type "<periodType>"

    Examples:
      | lineStyle  | periodType        |
      | solid      | continuous time   |
      | slant      | intermittent time |
      | horizontal | reduced schedule  |


  @C115-F09-07-01 @timeline @job-protection @AC4.3 @manual @sanity
  Scenario Outline: Timeline for job protected leave - graphical distinguish on period status
    Given I log in to the application
    And I open notification with "child absence case"
    When I expand timeline section for "job protected leave"
    Then I can see "<lineStyle>" line for period status "<periodStatus>"

    Examples:
      | lineStyle | periodStatus |
      | green     | approved     |
      | yellow    | pending      |
      | red       | denied       |
      | grey      | cancelled    |


  @C115-F09-08 @timeline @job-protection @AC4.4 @AC5.3 @manual
  Scenario: Timeline for job protected leave - collapsed view
    Given I log in to the application
    And I open notification with "child absence case"
    When I collapse timeline section for "job protected leave"
    Then I can see all the periods in the Job Protected Leave section represented on a single timeline
    And Overlapping periods base on the following order: Approved, Pending, Declined or Cancelled
    And I shouldn't see popover when hover over collapsed time bar


  @C115-F09-09 @timeline @job-protection @AC5.1 @AC5.2 @AC5.4 @manual
  Scenario Outline: Timeline for job protected leave - popover
    Given I log in to the application
    And I open notification with "child absence cases"
    When I expand timeline section for "job protected leave"
    And I hover on period "<periodType>" on status "<status>"
    Then I "can" see popover wit following information "<popoverText>"
    And popover message is customisable

    Examples:
      | status    | periodType        | popoverText                                                                                  |
      | pending   | continuous time   | The employee 'has requested' to 'take continuous leave' from -StartDate- to -EndDate-        |
      | approved  | reduced schedule  | The employee 'is approved' to 'to work on a reduced schedule' from -StartDate- to -EndDate-  |
      | cancelled | intermittent time | This request to 'take intermittent leave' from -StartDate- to -EndDate- 'has been cancelled' |
      | denied    | intermittent time | This request to 'take intermittent leave' from -StartDate- to -EndDate- 'has been denied'    |


  @C115-F09-10 @timeline @wage-replacement @AC6.1 @permissions @manual
  Scenario Outline: Timeline for wage replacement - user with permissions
    Given I log in to the application as a user "with" permissions to "URL_GET_GROUPCLIENT_CLAIMS_BENEFITS"
    When I open notification with "<benefit>" benefit
    Then I "<canOrNot>" see "Wage Replacement" section on the timeline

    Examples:
      | benefit                                 | canOrNot |
      | Group Disability                        | can      |
      | Paid Leave                              | can      |
      | no Group Disability Claim or Paid Leave | can't    |


  @C115-F09-11 @timeline @wage-replacement @AC6.2 @permissions @manual
  Scenario Outline: Timeline for wage replacement - user without permissions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CLAIMS_BENEFITS"
    When I open notification with "<benefit>" benefit
    Then I "can't" see "Wage Replacement" section on the timeline

    Examples:
      | benefit          |
      | Group Disability |
      | Paid Leave       |


  @C115-F09-12 @timeline @wage-replacement @AC7.1 @AC7.2 @automated @regression
  Scenario Outline: Timeline for wage replacement - multiple items
    Given I log in to the application
    When I open notification with "<benefit> with benefits"
    And I expand 'Timeline' "Wage Replacement" section
    Then text of lines in expanded "Wage Replacement" section should match text of leave plans in the absence case

    Examples:
      | benefit                |
      | Group Disability Claim |
      | Paid Leave             |


  @C115-F09-13 @timeline @wage-replacement @AC7.4 @manual
  Scenario Outline: Timeline for wage replacement - graphical distinguish on item type
    Given I log in to the application
    And I open notification with "Paid Leave" benefit
    When I expand timeline section for "wage replacement"
    Then I can see "<lineStyle>" line for "<type>" absence

    Examples:
      | lineStyle  | type              |
      | solid      | continuous time   |
      | slant      | intermittent time |
      | horizontal | reduced schedule  |


  @C115-F09-13-01 @timeline @wage-replacement @AC7.4 @manual
  Scenario Outline: Timeline for wage replacement - graphical distinguish on item status
    Given I log in to the application
    And I open notification with "Paid Leave" benefit
    When I expand timeline section for "wage replacement"
    Then I can see "<lineStyle>" line for absence with "<status>" status

    Examples:
      | lineStyle | status   |
      | green     | approved |


  @C115-F09-14 @timeline @wage-replacement @AC7.3 @manual
  Scenario Outline: Timeline for wage replacement - timeline circles
    Given I log in to the application
    And I open notification with "Group Disability" benefit
    When I expand timeline section for "wage replacement"
    Then I can see "<display>" for benefit of category "<benefitRightCategory>" and status "<status>"

    Examples:
      | status    | benefitRightCategory | display                                   |
      | approved  | Recurring Benefit    | time bar                                  |
      | pending   | Lump Sum Benefit     | coloured circle on creation date          |
      | denied    |                      | fixed circle at the beginning of timeline |
      | cancelled |                      | fixed circle at the beginning of timeline |
      | closed    |                      | fixed circle at the beginning of timeline |


  @C115-F09-15 @timeline @wage-replacement @AC7.5 @AC8.3 @manual @cannotGenerateTestData
  Scenario: Timeline for wage replacement - collapsed view
    Given I log in to the application
    And I open notification with "Paid Leave" benefit
    When I collapse timeline section for "wage replacement"
    Then I can see all the periods in the Paid Leave section represented on a single timeline
    And Overlapping periods base on the following order: Approved, Pending, Declined or Cancelled
    And I shouldn't see popover when hover over collapsed time bar


  @C115-F09-16 @timeline @wage-replacement @AC7.6 @AC7.7 @permissions @manual
  Scenario Outline: Timeline for wage replacement - detailed view permissions
    Given I log in to the application as a user "without" permissions to "<secureAction>"
    When I open notification with "<benefit>" benefit
    Then I "can't" see "Wage Replacement" section on the timeline

    Examples:
      | benefit    | secureAction                                           |
      | paid leave | URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS     |
      | recurring  | URL_GET_GROUPCLIENT_CLAIMS_DISABILITYBENEFIT_GETSINGLE |


  @C115-F09-17 @timeline @wage-replacement @AC8.1 @AC8.2 @AC8.4 @manual
  Scenario Outline: Timeline for wage replacement - popover
    Given I log in to the application
    And I open notification with "Group Disability Benefit"
    When I expand timeline section for "wage replacement"
    And I hover on "<benefitRightCategory>" timeline/coloured circle with "<status>" status
    Then I "<canOrNot>" see popover wit following information "<popoverText>"
    And popover message is customisable


    Examples:
      | status    | benefitRightCategory | canOrNot | popoverText                                                                             |
      | pending   | any                  | can't    | -                                                                                       |
      | denied    | any                  | can't    | -                                                                                       |
      | approved  | Recurring Benefit    | can      | The employee is certified to receive wage replacement between -StartDate- and -EndDate- |
      | approved  | Lump Sum Benefit     | can      | The employee is certified to receive wage replacement                                   |
      | cancelled | any                  | can      | Cancelled                                                                               |
      | closed    | any                  | can      | Closed                                                                                  |


  @C115-F09-18 @timeline @temporary-accommodations @AC9.1 @permissions @manual
  Scenario Outline: Timeline for temporary accommodations - user with permissions
    Given I log in to the application as a "<user>" user
    When I open notification with "Temporary Accommodation Case" in "Accommodated" status
    Then I "can" see "Temporary Accommodations" section on the timeline

    Examples:
      | user               |
      | Absence Customer   |
      | Absence Supervisor |


  @C115-F09-19 @timeline @temporary-accommodations @AC9.2 @permissions @manual
  Scenario: Timeline for temporary accommodations - user without permissions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_ABSENCE_ACCOMMODATIONCASES_GETSINGLE"
    When I open notification with "Temporary Accommodation Case" in "Accommodated" status
    Then I "can't" see "Temporary Accommodations" section on the timeline


  @C115-F09-20 @timeline @temporary-accommodations @AC10.1 @AC10.2 @automated @regression
  Scenario: Timeline for temporary accommodations - multiple items
    Given I log in to the application
    When I open notification with "Multiple Temporary Accommodation Cases"
    And I expand 'Timeline' "Temporary accommodations" section
    Then text of lines in expanded "Temporary accommodations" section should match text of leave plans in the absence case
    And the label shown for each line is amalgamation "Accommodation Category | Accommodation Type"


  @C115-F09-20-01 @timeline @temporary-accommodations @AC10.3 @manual
  Scenario: Timeline for temporary accommodations - accommodation bars
    Given I log in to the application
    When I open notification with "Temporary Accommodation Case" in "Accommodated" status
    And I expand timeline section for "temporary accommodations"
    Then graphical representation of each Temporary Accommodation is a green time bar
    And accommodation bar start date is Accommodation Implemented Date
    And accommodation bar end date is Accommodation End Date


  @C115-F09-21 @timeline @temporary-accommodations @AC10.4 @AC11.2 @manual
  Scenario: Timeline for temporary accommodations - collapsed view
    Given I log in to the application
    When I open notification with "Temporary Accommodation Case" in "Accommodated" status
    When I collapse timeline section for "temporary accommodations"
    Then I can see all the periods in the temporary accommodations section represented on a single timeline
    And overlapping periods are merged to a single bar
    And I shouldn't see popover when hover over collapsed time bar


  @C115-F09-22 @timeline @temporary-accommodations @AC11.1, @AC11.3 @manual
  Scenario: Timeline for temporary accommodations - popover
    Given I log in to the application
    When I open notification with "Temporary Accommodation Case" in "Accommodated" status
    When I expand timeline section for "temporary accommodations"
    And I hover on the timeline
    Then I "can" see popover wit following information "The accommodation is in place from -StartDate- to -EndDate-"
    And popover message is customisable


  @C115-F09-23 @timeline @smoke @automated
  Scenario: Timeline for all case types
    Given I log in to the application
    When I open notification with "Temporary Accommodation case, Paid Leave case and Group Disability Claim" and test data "@C115-F09-23-TD-1"
    Then I verify that the 'Timeline' card is "visible"
    And I "can" see "Temporary accommodations" section on the timeline
    And I "can" see "Wage Replacement" section on the timeline
    And I "can" see "Job Protected Leave" section on the timeline
