@employer-portal @C122-F01
Feature: C122-F01 - Update employee Contact Details

  @C122-F01-01 @AC1.1 @AC2.1 @permissions @manual
  Scenario: Default View of Employee Details page
    Given I log in to the application as a user "with" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMERINFO"
    When I open Employee Details page
    Then I verify that Personal Details tab is selected by default


  @C122-F01-03 @AC2.2 @AC2.3 @smoke @automated
  Scenario: Personal Details fields
    Given I log in to the application
    When I open Employee details page "withNotifications"
    And I open "Employee Details" tab
    Then I can see "Full Name, Date of Birth, Address, Email(s), Phone Number(s)" fields on Personal Details section
    And the Employee’s Full Name is displayed in the format [First Name] [Last Name]


  @C122-F01-04 @AC2.4 @AC2.5 @automated @regression @sanity
  Scenario Outline: Personal Details address
    Given I log in to the application
    When I open Employee details page "of employee <location>" using EmployeeId
    And I open "Employee Details" tab
    Then the "Address" of employee "<location>" should be presented in the following format "<expectedFormat>"
    And the "Address" do not have blank lines

    Examples:
      | location     | expectedFormat                                            |
      | from USA     | [Address Lines 1-3], [City][State], [Zip Code], [Country] |
      | not from USA | [Address Lines 1-7], [Zip Code], [Country]                |


  @C122-F01-05 @AC2.6 @automated @regression @sanity
  Scenario: Personal Details emails
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    Then I can see the list of "email addresses" registered for my employee


  @C122-F01-06 @AC2.7 @automated @regression @sanity
  Scenario: Personal Details phone numbers
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    Then I can see the list of "phone numbers" registered for my employee
    And each number should be presented in following format "[International Code]-[Area Code]-[Telephone Number]"


  @C122-F01-07 @AC2.8 @permissions @regression @manual
  Scenario: Permissions to view Personal Details emails
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES"
    When I open Employee Details page
    Then I can see that the emails section is blank


  @C122-F01-08 @AC2.9 @permissions @regression @manual
  Scenario: Permissions to view Personal Details phone numbers
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS"
    When I open Employee Details page
    Then I can see that the phone numbers section is blank


  @C122-F01-09 @AC3.0 @permissions @regression @manual
  Scenario: Permissions to view Contact Preferences
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES"
    When I open Employee Details page
    Then I verify that Contact Preferences section is not visible
    And I can't see any navigation leading to Contact Preferences section


  @C122-F01-11 @AC3.2 @AC3.3 @AC3.4 @automated @regression 
  Scenario Outline: Communication Preferences - phone calls message
    Given I log in to the application
    When I open Employee details page "of employee with <expectedState> phone calls" using EmployeeId
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    Then the preferred contact phone number is <expectedState> in "Phone calls" section with correct text <phoneNumberFormat>

    Examples:
      | expectedState | phoneNumberFormat |
      | set           | and format        |
      | not set       |                   |


  @C122-F01-13 @AC4.1 @AC4.2 @AC4.3 @AC4.4 @AC4.5 @automated @smoke
  Scenario Outline: Contact Preferences - alerts message
    Given I log in to the application
    When I open Employee details page "<subscribed> to alerts <method>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    Then the preferred contact <method> is <expectedState> in "Alerts" section with correct text <format>

    Examples:
      | subscribed     | expectedState | method            | format     |
      | subscribed     | set           | via SMS           | and format |
      | subscribed     | set           | via email         | and format |
      | subscribed     | set           | via email and SMS | and format |
      | not subscribed | not set       |                   |            |


  @C122-F01-15 @AC5.1 @AC5.2 @AC5.3 @AC5.4 @automated @smoke
  Scenario Outline: Contact Preferences - written correspondence
    Given I log in to the application
    When I open Employee details page "subscribed to written correspondence <option>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    Then the "Written correspondence" section subscribed "<option>" has correct text

    Examples:
      | option    |
      | via post  |
      | via email |
      | via SMS   |
      # at the moment user is able to select only cell number as a phone number for SMS option
      #| via the portal - SMS - not a mobile number | Your employee is currently set to receive paper correspondence from us via post                                                                            |


  @C122-F01-16 @smoke @automated
  @C122-F01-10 @AC3.1
  Scenario: for contact preferences with max number of details and check all fields
    Given I log in to the application
    When I open "Communication Preferences" of employee with defined "direct communication, alerts, written correspondence"
    Then I should see all "Communication Preferences" sections populated correctly with "Phone calls, Alerts, updates and reminders, Written correspondence"
