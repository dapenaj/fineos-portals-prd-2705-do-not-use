@employer-portal @C119-F01
Feature: C119-F01 - Integrated Non Established Intake

  @C119-F01A-01 @permissions @AC1 @AC2 @manual
  Scenario Outline: Employee not established - Permissions to create intake
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_NOTIFICATIONS_ADD"
    When I search for "not existing" employee
    Then I can see empty results list
    And the link to create notification intake on behalf of an employee is "<visibility>"

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |


  @C119-F01A-02 @AC3 @smoke @automated
  Scenario: Employee not established - employee's details
    Given I log in to the application
    When I open intake creator for "not existing" employee
    Then I should see "Employee's Details" section
    And the page contains following mandatory fields "First name, Last name, Area code, Phone number, Address line 1, Country, City, State, Zip code"
    And the page contains following optional fields for not existing employee "Email, Country code, Address line 2, Address line 3"

  @C119-F01A-02B @AC3 @regression @automated
  Scenario Outline: Employee not established - employee's details address fields
    Given I log in to the application
    When I open intake creator for "not existing" employee
    Then I should see "Employee's Details" section
    When I select "<country>" in the country dropdown field
    Then I should see empty address fields: "<address fields>"

    Examples:
      | country       | address fields                                                                                                       |
      | United States | Address line 1,Address line 2,Address line 3,City,State,Zip code                                                     |
      | Canada        | Address line 1,Address line 2,Address line 3,City,Province,Postal code                                               |
      | Netherlands   | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Ireland       | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Sweden        | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Australia     | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | New Zealand   | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Mexico        | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Singapore     | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Hong Kong     | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |
      | Japan         | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code |


  @C119-F01A-03 @AC3 @automated @sanity @regression
  Scenario Outline: Employee not established - employee's details - proceed without filling mandatory fields for US employee
    Given I log in to the application
    And I open intake creator for "not existing" employee
    When I try to submit intake without filling "<field>" field
    Then I verify that "Proceed" button is enabled
    And I shouldn't be able to proceed to "Occupation & Earnings" section

    Examples:
      | field                            |
      | firstName                        |
      | lastName                         |
      | preferredPhoneNumber areaCode    |
      | preferredPhoneNumber telephoneNo |
      | address addressLine1             |
      | address addressLine4             |
      | State                            |
      | address postCode                 |

  @C119-F01A-03B @AC3 @regression @automated
  Scenario Outline: Employee not established - employee's details - proceed without filling mandatory fields for Canadian employee
    Given I log in to the application
    And I open intake creator for "not existing" employee
    When I select "Canada" in the country dropdown field
    When I try to submit intake without filling "<field>" field
    Then I verify that "Proceed" button is enabled
    And I shouldn't be able to proceed to "Occupation & Earnings" section

    Examples:
      | field                            |
      | firstName                        |
      | lastName                         |
      | preferredPhoneNumber areaCode    |
      | preferredPhoneNumber telephoneNo |
      | address addressLine1             |
      | address addressLine4             |
      | State                            |
      | address postCode                 |

  @C119-F01A-03C @AC3 @regression @automated
  Scenario Outline: Employee not established - employee's details - proceed without filling mandatory fields for non-USA non-Canadian employee
    Given I log in to the application
    And I open intake creator for "not existing" employee
    When I select "Ireland" in the country dropdown field
    When I try to submit intake without filling "<field>" field
    Then I verify that "Proceed" button is enabled
    And I shouldn't be able to proceed to "Occupation & Earnings" section

    Examples:
      | field                            |
      | firstName                        |
      | lastName                         |
      | preferredPhoneNumber.areaCode    |
      | preferredPhoneNumber.telephoneNo |
      | address.addressLine1             |
      | address.postCode                 |


  @C119-F01A-04 @AC5 @smoke @automated
  Scenario: Employee not established - occupation and earnings
    Given I log in to the application
    And I open intake creator for "not existing" employee
    When I proceed to "(2) Occupation & Earnings" section
    Then the "Occupation & Earnings" page contains following optional fields "Job title, Date of hire, Earnings, Frequency, Work pattern"
    And I verify that "Frequency" drop down contains options: "Please select, per week, per month, per year"
    And I verify that "Work pattern" drop down contains options: "Please select, Fixed, Rotating, Variable"


  @C119-F01A-05 @AC6 @smoke @automated
  Scenario: Employee not established - work pattern fixed, standard hours
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date
    When I select Work pattern as "Fixed"
    Then I verify the number of hours is 8:00 per day and the employee works a standard week (5d)
    And I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section


  @C119-F01A-06 @AC7 @regression @automated
  Scenario: Employee not established - work pattern fixed, non-standard hours
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date
    And I select Work pattern as "fixed"
    When I switch "on" 'Non standard working week'
    Then I should see input fields to provide own working hours for each of "5" days in work pattern
    When I fill custom work hours fields
    Then I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section


  @C119-F01A-07 @AC7 @regression @automated
  Scenario: Employee not established - work pattern fixed, non-standard hours + weekends
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date
    And I select Work pattern as "fixed"
    When I switch "on" 'Non standard working week'
    And I switch "on" Include weekend
    Then I should see input fields to provide own working hours for each of "7" days in work pattern
    When I fill custom work hours fields
    Then I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section


  @C119-F01A-08 @AC8 @regression @automated
  Scenario Outline: Employee not established - work pattern rotating
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date of hire
    When I select Work pattern as "Rotating"
    And I select radiobutton "<radio>"
    Then I verify that I can provide own working hours for each of "<count>" days in work pattern
    When I fill custom work hours fields
    Then I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section

    Examples:
      | radio   | count |
      | 2 Weeks | 14    |
      | 3 Weeks | 21    |
      | 4 Weeks | 28    |


  @C119-F01A-09 @AC9 @regression @automated
  Scenario: Employee not established - work pattern variable
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    When I select Work pattern as "variable"
    Then I should see pattern description input field
    And I see "2" switch item

  @C119-F01A-10 @AC9 @regression @automated
  Scenario Outline: Employee not established - record work pattern - positive path
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select Work pattern as "variable"
    And I set pattern repeats switch to "<patternRepeatsSwitch>" providing value "<repeatValue>"
    And I set day length same switch to "<dayLengthSameSwitch>" providing value "<lengthValue>"
    When I fill additional information field
    Then I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section

    Examples:
      | patternRepeatsSwitch | repeatValue | dayLengthSameSwitch | lengthValue |
      | off                  |             | off                 |             |
      | on                   | 3           | off                 |             |
      | on                   | 5           | on                  | 4:00        |
      | off                  |             | on                  | 8:00        |


  @C119-F01A-12 @AC9 @regression @automated
  Scenario Outline: Employee not established - record work pattern - wrong values
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select Work pattern as "variable"
    When I set pattern repeats switch to "<switch1>" providing value "<repeat>"
    And I set day length same switch to "<switch2>" providing value "<length>"
    And I "<action>" additional information field
    Then I verify that "Proceed" button is enabled
    And I shouldn't be able to proceed to "Initial Request" section
    And I can see the error message "<message>"

    Examples:
      | switch1 | repeat | switch2 | length | action | message                                              |
      | on      |        | off     |        | fill   | Required                                             |
      | on      | xyz    | off     |        | fill   | Required                                             |
      | on      | 5      | on      | 24:00  | fill   | Please enter a valid time format (for example 07:30) |
      | on      | 5      | on      | 05:60  | fill   | Please enter a valid time format (for example 07:30) |
      | on      | 5      | on      | xyz    | fill   | Please enter a valid time format (for example 07:30) |
      | off     |        | on      |        | fill   | Please enter a valid time format (for example 07:30) |
      | off     |        | off     |        | empty  | Fields marked with an asterisk (*) are mandatory     |


  @C119-F01A-12B @AC9 @manual
  Scenario Outline: Check error validation for fixed/rotating work pattern
    Given I  log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select Work pattern as "<workPatternType>"
    When I set input fields number "00:00" hours per day for "all" days
    And I shouldn't be able to proceed to "Initial Request" section
    Then I can see the error message "You must enter time in hours and minutes for at least one day in the working pattern"
    When I clear input fields number of hours per day for for "all" days
    Then I shouldn't be able to proceed to "Initial Request" section
    And I can see the error message "You must enter time in hours and minutes for at least one day in the working pattern"
    When I set input field number "24:00" hours per day for "random" day
    And  I tab out of the field
    Then I can see the error message "Hours must be between 0 and 23"
    And I shouldn't be able to proceed to "Initial Request" section
    When I set input field number "08:60" hours per day for "same random" day
    And I tab out of the field
    Then I can see the error message "Minutes must be between 0 and 59"
    And I shouldn't be able to proceed to "Initial Request" section
    And I set input field number "random" hours per day for "same random" day
    Then I should be able to proceed to "Initial Request" section

    Examples:
      | workPatternType |
      | fixed           |
      | rotating        |


  @C119-F01A-13 @AC4 @smoke @automated
  Scenario: Employee not established - initial request
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date
    When I select Work pattern as "Fixed"
    And I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section
    Then I should see list of notification reasons radio buttons
    And the initial request page labels "have" "*" on the fields "Reason,Notification Last Working Day"
    And the initial request page labels "do not have" "*" on the fields "Notification Description"

  @C119-F01A-14 @AC4 @regression @automated
  Scenario: Employee not established - initial request - proceed without the reason checked
    Given I log in to the application
    And I open intake creator for "not existing" employee
    When I proceed to "(2) Occupation & Earnings" section
    And I select random date
    When I select Work pattern as "Fixed"
    And I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section
    And I select random last day of hire
    And I try to submit intake without selecting reason
    Then I verify that "Finish and submit" button is enabled
    And I shouldn't be able to submit intake


  @C119-F01A-15 @AC10 @AC13 @automated
  Scenario Outline: Employee not established - acknowledgement screen - successful processing
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date of hire
    When I select Work pattern as "Fixed"
    And I verify that "Proceed" button is enabled
    And I should be able to proceed to "Initial Request" section
    And I select random last day of hire
    And I select "<reason>" notification reason
    When I submit intake
    Then I should see acknowledgement screen
    When I close acknowledgement screen
    Then My Dashboard page with available notifications is loaded

    @smoke
    Examples:
      | reason                                      |
      | Accident / treatment required for an injury |

    @regression
    Examples:
      | reason                                                        |
      | Sickness / time required for a medical treatment or procedure |
      | Pregnancy, birth or related medical treatment                 |
      | Child bonding                                                 |
      | Caring for a family member                                    |
      | Time required for another reason                              |
      | Accommodation required to remain at work                      |
      | Wellness                                                      |

  @C119-F01A-16 @AC10 @AC13 @manual
  Scenario: Employee not established - acknowledgement screen - fail on submitting
    * I customize the error message on the submission screen
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "Initial Request" section
    And I select "random" notification reason
    When I submit intake, but request finish with error
    Then I should see acknowledgement screen
    And I should see customized error message


  @C119-F01A-17 @AC11 @AC13 @manual
  Scenario: Employee not established - closing acknowledgement screen
    * I customize the text on the submission screen
    Given I log in to the application
    When I submit intake for "not existing" employee
    Then I should see acknowledgement screen
    And I should see customized text
    When I close acknowledgement screen
    Then My Dashboard page with available notifications is loaded


  @C119-F01A-18 @AC12 @regression @automated
  Scenario Outline: Employee not established - cancel intake creation
    Given I log in to the application
    And I open intake creator for "not existing" employee
    When I click <button> button to cancel intake
    Then No intake will be created
    And My Dashboard page with available notifications is loaded
    When I open intake creator for "not existing" employee
    And I fill all mandatory fields
    And I click <button> button to cancel intake
    Then I should see confirmation popup asking me if I really want to abandon intake
    When I choose "Cancel" on confirmation popup
    Then The abandon intake popup disappears
    And I should be able to continue intake creation

    Examples:
      | button       |
      | My Dashboard |
      | Abandon      |


  @C119-F01A-19 @regression @automated
  Scenario Outline: Employee not established - Steps panel navigation - skip step
    Given I log in to the application
    When I open intake creator for "not existing" employee
    Then I cannot jump to "<step>" using steps navigation panel

    Examples:
      | step                      |
      | (2) Occupation & Earnings |
      | (3) Initial Request       |


  @C119-F01A-20 @automated @smoke
  Scenario Outline: Employee not established - Steps panel navigation - mandatory fields filled - skip step
    Given I log in to the application
    When I open intake creator for "not existing" employee
    And I fill all mandatory fields
    Then I cannot jump to "<step>" using steps navigation panel

    Examples:
      | step                      |
      | (2) Occupation & Earnings |
      | (3) Initial Request       |


  @C119-F01A-21 @automated @smoke @sanity
  Scenario Outline: Employee not established - Steps panel navigation - jump back
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date of hire
    When I select Work pattern as "Fixed"
    And I should be able to proceed to "Initial Request" section
    Then I can jump to step "<step>" using steps navigation panel

    Examples:
      | step                      |
      | (1) Employee's Details    |
      | (2) Occupation & Earnings |


  @C119-F01A-22 @regression @automated
  Scenario Outline: Employee not established - back button
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I fill all mandatory fields
    And I proceed to "<step>" section
    When I click "back" button on "<step>"
    Then I should land on "<previousStep>" page

    Examples:
      | step                      | previousStep              |
      | (2) Occupation & Earnings | (1) Employee's Details    |
      | (3) Initial Request       | (2) Occupation & Earnings |


  @C119-F01A-23 @regression @automated
  Scenario: Employee not established - back button on initial page
    Given I log in to the application
    When I open intake creator for "not existing" employee
    Then I shouldn't see back button
    And I should see "Abandon intake" button


  @C119-F01A-24 @regression @automated
  Scenario Outline: Employee not established - jump to step without saving changes
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date of hire
    When I select Work pattern as "Fixed"
    And I should be able to proceed to "Initial Request" section
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    And I edit the address fields for not existing employee and confirm
    When I try to jump to step "(3) Initial Request" using steps navigation panel
    Then I should see "How do you want to proceed" modal
    When I click "<choice>" button
    Then I should land on "<page>" page
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    Then I "<visibility>" see changes I made in intake flow for not existing employee

    Examples:
      | choice                 | page                   | visibility |
      | NO, discard my changes | (3) Initial Request    | can        |
      | X (closing popup)      | (1) Employee's Details | can        |

  @C119-F01A-25 @regression @automated
  Scenario: Employee not established - jump to step with saving changes
    Given I log in to the application
    And I open intake creator for "not existing" employee
    And I proceed to "(2) Occupation & Earnings" section
    And I select random date of hire
    When I select Work pattern as "Fixed"
    And I should be able to proceed to "Initial Request" section
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    And I edit the address fields for not existing employee and confirm
    When I try to jump to step "(3) Initial Request" using steps navigation panel
    Then I should see "How do you want to proceed" modal
    When I click "YES, save my changes" button
    Then I should land on "(3) Initial Request" page
    Then a change of the Employee’s details should be stored in system
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    Then I "can" see changes I made in intake flow for not existing employee
