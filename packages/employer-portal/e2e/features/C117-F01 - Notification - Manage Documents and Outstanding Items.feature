@employer-portal @C117-F01
Feature: C117-F01 - Notification - Manage Documents and Outstanding Items

  @C117-F01-01 @AC1.1 @permissions @manual
  Scenario Outline: Checking permissions to see Alerts card

    Given I log in to the application as a user "<outstandingPermission>" permission to URL_GET_GROUPCLIENT_CASES_OUTSTANDINGINFORMATION and "<documentPermission>" permission to URL_GET_GROUPCLIENT_CASES_DOCUMENTS
    When I open random notification
    Then I verify that the Alerts card is "<visibility>"

    Examples:
      | outstandingPermission | documentPermission | visibility  |
      | without               | without            | not visible |
      | without               | with               | visible     |
      | with                  | without            | visible     |
      | with                  | with               | visible     |


  @C117-F01-02 @AC2.1 @permissions @manual
  Scenario Outline: Checking permissions to see Outstanding section
    Given I log in to the application as a user "<withPermissions>" permissions to "URL_GET_GROUPCLIENT_CASES_OUTSTANDINGINFORMATION"
    When I open random notification
    Then I verify that the Outstanding tab on Alerts card is "<visibility>"

    Examples:
      | withPermissions | visibility  |
      | without         | not visible |
      | with            | visible     |


  @C117-F01-03 @AC2.2 @manual
  Scenario: Something went wrong message on Outstanding tab
    Given I log in to the application
    When I open random notification
    And outstanding-information endpoint does not return a result of 'success'
    Then I verify that the Outstanding tab shows 'Something went wrong' information
    And the error message on tab can be customised


  @C117-F01-04 @AC2.3 @automated @regression @sanity
  Scenario: Empty state on Outstanding tab
    Given I log in to the application
    When I open notification with "no outstanding items"
    Then I verify that the "Outstanding" tab shows empty message


  @C117-F01-05 @AC2.4 @automated @regression @sanity
  Scenario Outline: List of items on Outstanding tab
    Given I log in to the application
    When I open notification with "at least one item waiting <itemType>"
    Then I can see at least one item waiting "<itemType>" with all fields

    Examples:
      | itemType         |
      | to be provided   |
      | for verification |


  @C117-F01-06 @AC2.4 @AC2.5 @automated @regression
  Scenario: List of waiting to be provided items
    Given I log in to the application
    When I open notification with "at least one item waiting to be provided"
    Then each 'not-provided' item contains a coloured bar to the left of the card
    And each 'not-provided' item contains 'orange dot' icon
    And each not-provided item contains information on the source of the item
    And only provided items are included in the orange count in 'Outstanding' tab


  @C117-F01-07 @permissions @AC2.6 @regression @manual
  Scenario Outline: Permissions for uploading item waiting to be provided
    Given I log in to the application as a user "<withPermissions>" permissions to "URL_POST_GROUPCLIENT_CASES_DOCUMENT_BASE64UPLOAD"
    When I open notification with "item waiting to be provided"
    Then each not-provided item contains "<expectedField>" field
    And I "<canUpload>" upload missing file

    Examples:
      | withPermissions | expectedField                       | canUpload |
      | with            | hyperlink with name of the document | can       |
      | without         | name of the document                | can't     |


  @C117-F01-08 @AC2.4 @AC2.7 @automated @regression
  Scenario: List of items provided and awaiting verification
    Given I log in to the application
    When I open notification with "at least one item waiting for verification"
    Then each provided item contains the name of the document
    And each provided item contains 'green tick' icon
    And each provided item contains text 'Provided and awaiting verification'

  @C117-F01-09 @AC2.8 @manual
  Scenario: Big number of outstanding items to be displayed
    Given I log in to the application
    When I open notification with 10 "outstanding items"
    Then I can see scroll bar that enables me to see all of the items on Outstanding tab


  @C117-F01-10 @AC3.1 @automated @regression
  Scenario: Upload files popup
    Given I log in to the application
    When I open notification with "at least one item waiting to be provided"
    And I click on missing file hyperlink
    Then the "Upload Document" popup is visible


  @C117-F01-11 @AC3.1 @manual @regression
  Scenario: Upload files popup - browse files method
    Given I log in to the application
    When I open notification with "item waiting to be provided"
    And I click on missing file hyperlink
    And I click the “Browse Files” button on upload popup
    Then I should see a browser window enabling me to select a file to upload
    And I should be able to upload file in supported format


  @C117-F01-12 @AC3.1 @manual @regression
  Scenario: Upload files popup - drag & drop method
    Given I log in to the application
    When I open notification with "item waiting to be provided"
    And I click on missing file hyperlink
    And I drag file on the drop area
    Then I should be able to upload file in supported format


  @C117-F01-13 @AC3.2 @regression @automated
  Scenario: Upload files popup - wrong file extension
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then I see "Notifications with outstanding requirements" drawer is opened
    When I find and open notification with document to upload
    And I click on missing file hyperlink
    Then the "Upload Document" popup is visible
    When I upload "uploadFile.js" document
    Then I can see "invalid extension" error
    And the "Upload Document" popup is still visible

    #Known bug related to FEP-415 - "SDK changes required to implement upload progress bar"
  @C117-F01-14 @AC3.3 @AC3.4 @manual @sanity
  Scenario: Upload files popup - uploading progress bar
    Given I log in to the application
    When I open notification with "item waiting to be provided"
    And I choose to upload missing file with "supported" extension
    And I click 'Upload Document' button on upload popup
    Then the upload process will start
    And I should see upload progress on green progress bar


  @C117-F01-15 @AC3.5 @AC3.6 @AC3.8 @manual
  Scenario: Successful upload files with automatic verification process
    Given I log in to the application
    When I open notification with "item waiting to be provided"
    And I upload file with automatic verification process
    Then I should see popup with adding document confirmation
    And popup message is customizable
    When I close status popup
    Then Outstanding tab is refreshed - uploaded item no longer shown
    And the document is shown on the Documents tab
    And if I have permission to URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD, document is marked as read by me

  @C117-F01-16 @AC3.5 @AC3.6 @AC3.8 @automated @regression
  Scenario: Successful upload files with manual verification process
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then I see "Notifications with outstanding requirements" drawer is opened
    When I find and open notification with document to upload
    And I click on missing file hyperlink
    Then the "Upload Document" popup is visible
    When I upload "flower.jpeg" document
    And I "accept" "Upload Document" pop up
    Then I can see "success" modal
    When I "close" "success" modal
    Then the "success" modal closes
    And I see that outstanding item is marked as provided in alerts section
    When I click "Documents" tab
    Then Uploaded document is visible on the documents list

  @C117-F01-17 @AC3.5 @AC3.7 @permissions @manual
  Scenario: Successful upload files - outstanding items list not updated
    Given I log in to the application as a user "without" permissions to "URL_POST_GROUPCLIENT_CASES_OUTSTANDINGINFORMATIONRECEIVED_ADD"
    When I open notification with "item waiting to be provided"
    And I upload file
    And the outstanding items list has not been updated
    Then I should see popup telling me that the document has been uploaded but that the outstanding items list has not been updated
    And popup message is customizable
    When I close status popup
    Then I verify that uploaded document stays unchanged on Outstanding tab
    And the document is shown on the Documents tab
    And if I have permission to URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD, document is marked as read by me


  @C117-F01-18 @AC3.9 @manual
  Scenario: Upload file failed
    Given I log in to the application
    When I open notification with "item waiting to be provided"
    And I upload file
    And upload process failed
    Then I should see popup telling me that the document could not be uploaded
    And popup message is customizable
    When I close status popup
    Then I verify that uploaded document stays unchanged on Outstanding tab
    And the document is not shown on the Documents tab


  @C117-F01-19 @AC3.10 @automated @regression
  Scenario: Upload files cancel
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then I see "Notifications with outstanding requirements" drawer is opened
    When I find and open notification with document to upload
    And I click on missing file hyperlink
    Then the "Upload Document" popup is visible
    When I upload "flower.jpeg" document
    And I "cancel" "Upload Document" pop up
    Then the "Upload Document" popup closes
    And I see that outstanding item is not marked as provided in alerts section
    When I click "Documents" tab
    Then Uploaded document is not visible on the documents list


  # imposible to execute, at the moment can't click 'Cancel' when the upload is in progress
  @C117-F01-20 @AC3.10 @manual
  Scenario: Upload files cancel during upload
    Given I log in to the application
    When I open notification with "item waiting to be provided"
    And I click on missing file hyperlink
    And I choose to upload missing file with "supported" extension
    And I click 'Upload Document' button on upload popup
    And I click 'Cancel' button on upload popup
    Then upload is stopped
    And upload popup should be closed
    And uploaded document stays unchanged on Outstanding tab
    And the document is not shown on the Documents tab


  @C117-F01-21 @AC4.1 @permissions @manual
  Scenario Outline: Checking permissions to see Documents
    Given I log in to the application as a user "<withPermissions>" permissions to "URL_GET_GROUPCLIENT_CASES_DOCUMENTS"
    When I open random notification
    Then I verify that the Documents tab on Alerts card is "<visibility>"

    Examples:
      | withPermissions | visibility  |
      | without         | not visible |
      | with            | visible     |


  @C117-F01-22 @AC4.2 @manual
  Scenario: Something went wrong message - documents tab
    Given I log in to the application
    When I open random notification
    And documents endpoint does not return a result of 'success'
    Then I verify that the Documents tab shows 'Something went wrong' information
    And the error message on tab can be customised


  @C117-F01-23 @AC4.3 @cannotGenerateTestData
  Scenario: Empty state of Outstanding tab
    Given I log in to the application
    When I open notification with "no documents"
    And I click "Documents" tab
    Then I verify that the "Documents" tab shows empty message


  @C117-F01-24 @AC4.4 @manual @regression
  Scenario: List of items on Documents tab
    Given I log in to the application
    When I open notification with "documents in read and unread state"
    Then I can see a list of all the Documents for the Notification and its sub cases, whether read or unread


  @C117-F01-25 @AC4.5
  Scenario Outline: Read and unread documents
    Given I log in to the application
    When I open notification with "documents in <state> state"
    And I click "Documents" tab
    And I read last document
    Then each "<state>" document contains appropriate icon reflecting its state
    And each document contains date on which it was linked to the case
    And each "<state>" document "<contains>" coloured bar to the left of the card

    @automated @regression
    Examples:
      | state | contains        |
      | read  | doesn't contain |

    @manual @regression
    Examples:
      | state  | contains |
      | unread | contains |


  @C117-F01-26 @permissions @AC4.6 @AC5.1 @manual
  Scenario Outline: Permissions for downloading documents
    Given I log in to the application as a user "<withPermissions>" permissions to "URL_GET_GROUPCLIENT_CASES_DOCUMENTS_BASE64DOWNLOAD"
    When I open notification with "documents in read and unread state"
    Then each document contains "<expectedField>" field
    And I "<canDownload>" download "<state>" document

    Examples:
      | withPermissions | expectedField                       | canDownload | state  |
      | with            | hyperlink with name of the document | can         | read   |
      | with            | hyperlink with name of the document | can         | unread |
      | without         | name of the document                | can't       | -      |


  @C117-F01-27 @AC4.7 @manual
  Scenario: Big number of documents to be displayed
    Given I log in to the application
    When I open notification with 10 "documents"
    Then I can see scroll bar that enables me to see all of the items on Documents tab


  @C117-F01-28 @AC5.1 @AC5.3 @AC5.4 @manual
  Scenario: Successful downloading of document
    Given I log in to the application
    When I open notification with "documents in read and unread state"
    And I click on unread document hyperlink
    Then the document is downloaded successfully
    And if I have permission to URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD, document is marked as read by me


  @C117-F01-29 @smoke @automated
  Scenario: Open notification with outstanding & already uploaded docs
    Given I log in to the application
    When I open notification with "both outstanding items & documents" and test data "@C117-F01-29"
    Then I verify the list of "Outstanding Items" is not empty
    And I verify the list of "Documents" is not empty

