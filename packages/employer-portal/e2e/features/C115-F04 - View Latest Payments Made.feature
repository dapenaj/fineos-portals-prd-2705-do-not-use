@employer-portal @C115-F04
Feature: C115-F04 - View Latest Payments Made

  @C115-F04-01 @payments @permissions @AC1.1 @manual
  Scenario Outline: Latest payments section - user without permissions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CLAIMS_PAYMENTS"
    And I open notification including "<benefit>" "with" payments
    When I expand benefit
    Then I verify that the payments section is "not visible"

    Examples:
      | benefit                        |
      | Group Disability Claim benefit |
      | Paid Leave benefit             |

  @C115-F04-02 @payments @AC1.2 @AC1.3 @automated @regression
  Scenario Outline: Latest payments section - user with permissions
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    Then I verify that the payments section is "<visibility>"

    Examples:
      | benefit                                         | visibility  | testData          |
      | Group Disability Claim benefit with payments    | visible     | @C115-F04-02-TD-1 |
      | Paid Leave benefit with payments                | visible     | @C115-F04-02-TD-2 |
      | Group Disability Claim benefit without payments | not visible | @C117-F04-24-TD-1 |
      | Paid Leave benefit without payments             | not visible | @C115-F04-02-TD-3 |

  @C115-F04-03 @payments @AC1.4 @automated @regression
  Scenario Outline: Latest payments section - payments sorting
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I expand 'Latest Payments' section
    Then I verify that payments are sorted basing on payment date in descending order

    Examples:
      | benefit                              | testData          |
      | Group Disability Claim with benefits | @C115-F04-03-TD-1 |
      | Paid Leave with benefit              | @C115-F04-03-TD-2 |


  @C115-F04-04 @payments @AC1.4 @automated @regression
  Scenario Outline: Latest payments section - payments list length
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    Then I can see <visiblePayments> payments
    When I close 'Latest Payments' history
    And I expand 'Latest Payments' section
    Then I can see 4 most recent payments

    Examples:
      | benefit                        | visiblePayments | testData          |
      | Group Disability Claim benefit | 10              | @C115-F04-04-TD-1 |
      | Paid Leave benefit             | 10              | @C115-F04-04-TD-2 |


  @C115-F04-05 @payments @AC1.5 @smoke @automated @sanity
  Scenario Outline: Latest payments section - default view of payments list
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I expand 'Latest Payments' section
    Then each payment is collapsed by default and has 'Payment Amount, Payment Date' fields

    Examples:
      | benefit                                      | testData          |
      | Group Disability Claim benefit with payments | @C115-F04-05-TD-1 |
      | Paid Leave benefit with payments             | @C115-F04-05-TD-2 |


  @C115-F04-06 @payments @AC1.6 @regression @automated
  Scenario Outline: Latest payments section - default view of latest payments section
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    Then latest payments section is collapsed by default

    Examples:
      | benefit                                      | testData          |
      | Group Disability Claim benefit with payments | @C115-F04-05-TD-1 |
      | Paid Leave benefit with payments             | @C115-F04-05-TD-2 |


  @C115-F04-07 @payments @AC1.7 @AC1.8 @AC1.9
  Scenario Outline: Latest payments section - expanded payment
    Given I log in to the application
    And I open notification with "Group Disability Claim benefit with <type> payments" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I expand 'Latest Payments' section
    And I expand last payment
    Then latest payment has "<fields>" fields

    @smoke @automated
    Examples:
      | type      | fields                                                       | testData          |
      | Recurring | Paid to:, Payment Method, Period Start Date, Period End Date | @C115-F04-07-TD-1 |
      | Adhoc     | Paid to:, Payment Method, Effective Date                     | @C115-F04-07-TD-2 |

    @cannotGenerateTestData
    Examples:
      | type                                     | fields                                                      |
      | Expense                                  | Paid To, Payment Method, Effective Date                     |
      | Lump Sum                                 | Paid To, Payment Method, Effective Date                     |
      | Service                                  | Paid To, Payment Method, Effective Date                     |
      | Unknown Type with start and end dates    | Paid To, Payment Method, Period Start Date, Period End Date |
      | Unknown Type without start and end dates | Paid To, Payment Method, Effective Date                     |


  @C115-F04-08 @payments @AC1.10 @permissions @manual
  Scenario Outline: Latest payments section - no permissions to see payment breakdown
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CLAIMS_PAYMENTLINES"
    And I open notification including "<benefit>" with payments
    When I expand benefit, latest payments section and payment
    Then I cannot see payment breakdown link

    Examples:
      | benefit                        |
      | Group Disability Claim benefit |
      | Paid Leave benefit             |


  @C115-F04-09 @payments @AC1.11 @permissions @manual
  Scenario Outline: Latest payments section - payment breakdown section
    Given I log in to the application as a user "with" permissions to "URL_GET_GROUPCLIENT_CLAIMS_PAYMENTLINES"
    And I open notification including "<benefit>" with payments
    When I expand benefit, latest payments section and payment
    Then payment breakdown section is collapsed by default
    And I can expand and collapse payment breakdown section

    Examples:
      | benefit                        |
      | Group Disability Claim benefit |
      | Paid Leave benefit             |


  @C115-F04-10 @payments @AC1.12 @AC1.13 @automated @regression
  Scenario Outline: Latest payments section - payment breakdown - list of adjustments
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    And I expand "Wage Replacement" details for first item
    And I expand 'Latest Payments' section
    And I expand last payment
    When I show 'Payments Breakdown'
    Then I see a list of adjustments used to calculate the 'Net Payment Amount'
    And each Adjustment has Adjustment Type Name and Adjustment Type Amount
    And the 'Net Payment Amount' is the payment amount used for the payment line

    Examples:
      | benefit                              | testData          |
      | Group Disability Claim with benefits | @C115-F04-10-TD-1 |
      | Paid Leave with benefits             | @C115-F04-10-TD-2 |


  @C115-F04-11 @payments @AC1.14 @AC1.15 @smoke @automated @sanity
  Scenario: Latest payments section - adjustments details
    Given I log in to the application
    And I open notification with "Group Disability Claim benefit with payments" and test data "@C115-F04-11-TD-1"
    When I expand "Wage Replacement" details for first item
    And I expand 'Latest Payments' section
    And I expand last payment
    When I show 'Payments Breakdown'
    Then I can expand details for adjustment with payee specified
    And Adjustment details contains following "Start Date, End Date" fields


  @C115-F04-12 @payments @AC1.16 @automated @regression
  Scenario Outline: Payments history - payments history hyperlink
    Given I log in to the application
    And I open notification with "with <paymentsCount> payments" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    Then 'View payment history' button is <visibility>

    Examples:
      | paymentsCount | testData          | visibility  |
      | 5             | @C115-F04-12-TD-1 | visible     |
      | 4             | @C115-F04-12-TD-2 | not visible |


  @C115-F04-13 @payments @AC1.17 @AC1.18 @AC1.19 @automated @regression
  Scenario Outline: Payments history - payments list
    Given I log in to the application
    And I open notification with "<benefit> with 13 payments" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    Then 'View Payment History' for "<name>" benefit lists 12 of <numberOfAllPayments> payments
    And I can see history payments list filtered in descending order

    Examples:
      | benefit                                         | numberOfAllPayments | name                       | testData          |
      | Group Disability Claim benefit with 13 payments | 13                  | STD Benefit                | @C115-F04-13-TD-1 |
      | Paid Leave benefit with 13 payments             | 13                  | Absence Paid Leave Benefit | @C115-F04-13-TD-2 |


  @C115-F04-14 @payments @AC1.20 @automated @regression
  Scenario Outline: Payments history - payments list items
    Given I log in to the application
    And I open notification with "<benefit> with 13 payments" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    Then each payment on the list is collapsed
    And each payment has 'Payment Amount and Payment Date' fields

    Examples:
      | benefit                        | testData          |
      | Group Disability Claim benefit | @C115-F04-14-TD-1 |
      | Paid Leave benefit             | @C115-F04-14-TD-2 |


  @C115-F04-15 @payments @AC1.21 @automated @regression
  Scenario Outline: Payments history - expanded payment
    Given I log in to the application
    And I open notification with "Group Disability Claim with <type> payments" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    And I expand "<type>" history payment
    Then latest history payment has "<fields>" fields

    Examples:
      | type      | fields                                                      | testData          |
      | Recurring | Paid to, Payment Method, Period Start Date, Period End Date | @C115-F04-15-TD-1 |
      | Adhoc     | Paid to, Payment Method, Effective Date                     | @C115-F04-15-TD-2 |


  @C115-F04-16 @payments @AC1.21 @permissions @manual
  Scenario Outline: Payments history - no permissions to see payment breakdown
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CLAIMS_PAYMENTLINES"
    And I open notification including "<benefit>" with payments
    When I expand benefit, open payment history section and expand payment details
    Then I cannot see payment breakdown link

    Examples:
      | benefit                        |
      | Group Disability Claim benefit |
      | Paid Leave benefit             |


  @C115-F04-17 @payments @AC1.21 @permissions @manual
  Scenario Outline: Payments history - payment breakdown section
    Given I log in to the application as a user "with" permissions to "URL_GET_GROUPCLIENT_CLAIMS_PAYMENTLINES"
    And I open notification including "<benefit>" with payments
    When I expand benefit, open payment history section and expand payment details
    Then payment breakdown section is collapsed by default
    And I can expand and collapse payment breakdown section

    Examples:
      | benefit                        |
      | Group Disability Claim benefit |
      | Paid Leave benefit             |


  @C115-F04-18 @payments @AC1.21 @automated @regression @sanity
  Scenario Outline: Payments history - payment breakdown - list of adjustments
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    And I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    And I expand last history payment
    When I show 'Payments Breakdown' for history payment
    And I see a list of all adjustments used to calculate the 'Net Payment Amount'
    And each history Adjustment has Adjustment Type Name and Adjustment Type Amount
    And the 'Net Payment Amount' is the history payment amount used for the payment line

    Examples:
      | benefit                              | testData          |
      | Group Disability Claim with benefits | @C115-F04-18-TD-1 |
      | Paid Leave with benefits             | @C115-F04-18-TD-2 |


  @C115-F04-19 @payments @AC1.21
  Scenario Outline: Payments history - adjustments details
    Given I log in to the application
    And I open notification with "Notification with payments and <specifiedOrNot> payee" and test data "<testData>"
    And I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    And I expand last history payment
    When I show 'Payments Breakdown' for history payment
    And I show last history Adjustment Details
    And adjustment details contains following information "<fields>"

    @automated @regression
    Examples:
      | specifiedOrNot | fields               | testData          |
      | specified      | Start Date, End Date | @C115-F04-19-TD-1 |

    @cannotGenerateTestData
    Examples:
      | not specified | Start Date, End Date | @C115-F04-19-TD-2 |


  # Comment on filtering: we agreed that filtering wil be done inline, not in the popup as described in feature file
  @C115-F04-20 @payments @AC1.22 @smoke @automated
  Scenario Outline: Payments history - filtering results
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
    And I filter payments by dates
    And I apply filter
    #Bug FEP-560
    Then I can see payments list is filtered correctly
    When I expand last history payment
    And I show 'Payments Breakdown' for history payment
    Then I see a list of all adjustments used to calculate the 'Net Payment Amount'

    Examples:
      | benefit                        | testData          |
      | Paid Leave benefit             | @C115-F04-20-TD-1 |
      | Group Disability Claim benefit | @C115-F04-20-TD-2 |


  # Comment on filtering: we agreed that filtering wil be done inline, not in the popup as described in feature file
  @C115-F04-21 @payments @AC1.23 @AC1.24 @automated @regression
  Scenario Outline: Payments history - reset filtering results
    Given I log in to the application
    And I open notification with "<benefit>" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I open 'Latest Payments' history
      #Bug FEP-560
    And I filter payments by dates
    And I apply filter
    Then I can see payments list is filtered correctly
    When I reset filter
    Then 'View Payment History' for "<paymentName>" benefit lists <visiblePayments> of <allPayments> payments
    Then I can see <visiblePayments> payments
    And I can see history payments list filtered in descending order

    Examples:
      | benefit                              | paymentName                | visiblePayments | allPayments | testData          |
      | Group Disability Claim with benefits | STD Benefit                | 10              | 10          | @C115-F04-21-TD-1 |
      | Paid Leave with benefits             | Absence Paid Leave Benefit | 10              | 10          | @C115-F04-21-TD-2 |


  #FEP-1222 [Chrome] ERP-6 NTN-142 tootltip doesn't display anything
  @C115-F04-22 @payments @AC1.25 @AC1.26 @AC1.27 @AC1.28
  Scenario Outline: Payment Method Popover
    Given I log in to the application
    And I open notification with "including benefit with <paymentMethod> payment method" and test data "<testData>"
    When I expand "Wage Replacement" details for first item
    And I expand 'Latest Payments' section
    And I expand last payment
    When I hover on 'i' icon located in Payment Method field
    Then I can see payment method popover with following text "<expectedTooltipText>"

    @cannotGenerateTestData
    Examples:
      | paymentMethod   | expectedTooltipText  |
      | Nominated Payee | Nominated Payee name |

    @automated @regression
    Examples:
      | paymentMethod | expectedTooltipText              | testData          |
      | transfer      | Bank institution, Account number | @C115-F04-22-TD-1 |
      | check-based   | Check number                     | @C115-F04-22-TD-2 |


  @C115-F04-23 @payments @AC1.7 @AC1.8 @AC1.9 @automated @regression
  Scenario: Latest payments section - mixed benefits types
    Given I log in to the application
    And I open notification with "including STD Benefit & NY-PFL benefits benefit with payments" and test data "@C115-F04-23"
    When I expand "Wage Replacement" details for first "STD Benefit" item
    And I expand 'Latest Payments' section for first "STD Benefit" item
    Then I verify that "STD Benefit" benefit payments are sorted basing on payment date in descending order
    When I expand "Wage Replacement" details for first "LTD Benefit" item
    And I expand 'Latest Payments' section for first "LTD Benefit" item
    Then I verify that "LTD Benefit" benefit payments are sorted basing on payment date in descending order
