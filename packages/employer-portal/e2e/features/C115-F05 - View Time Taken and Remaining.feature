@employer-portal @C115-F05
Feature: C115-F05 - View Time Taken and Remaining


  @C115-F05-01 @AC1 @permissions @manual @regression
  Scenario Outline: View Time Taken tab - user without permissions
    Given I log in to the application as a user "without" permissions to "<permissionName>"
    When I open Employee Profile page of "employee who has approved absence(s) connected to leave plan(s)"
    Then I "cannot" see the Time Taken tab on the profile page

    Examples:
      | permissionName                                           |
      | URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS       |
      | URL_GET_GROUPCLIENT_ABSENCE_LEAVEPLANS_LEAVEAVAILABILITY |


  @C115-F05-02 @AC1 @AC2 @sanity @automated @smoke
  Scenario Outline: View Time Taken tab - time off not used under any leave plans
    Given I log in to the application
    When I open Employee details page "<case>"
    And I open "Time Taken" tab
    Then I should see empty state placeholder

    Examples:
      | case                                                 |
      | employee who has no absence cases                    |
      | employee with absence not suitable to any leave plan |


  @C115-F05-03 @AC3 @automated @regression
  Scenario: View Time Taken tab - 2 absences with different leave plans
    Given I log in to the application
    And I open Employee details page "employee who has 2 absence cases connected to 2 different leave plans" using EmployeeId
    And I check the notifications ordered as more recent notification
    When I open "Time Taken" tab
    Then I see "2" leave plans for which the employee has reported time off
    And The list of Leave Plans is ordered by the plan with the most recent leave requests


  @C115-F05-04 @AC3 @automated @regression
  Scenario: View Time Taken tab - 2 leave plans applicable for same absence
    Given I log in to the application
    And I open Employee details page "employee who has approved absence(s) connected to leave plan(s)" using EmployeeId
    When I open "Time Taken" tab
    Then I see "2" leave plans for which the employee has reported time off
    And The list of leave plans is ordered alphabetically

  @C115-F05-05 @AC4 @AC5 @smoke @sanity @automated
  Scenario Outline: View Time Taken tab - details on each leave plan
    Given I log in to the application
    When I open Employee details page "employee who has approved absence(s) connected to leave plan(s)"
    And I open "Time Taken" tab
    Then I verify that each Leave Plan section has following fields "<fields>"
    And I verify that each Leave Plan section has "Leave Plan Name" and "Leave Plan description"

    Examples:
      | fields                                                                                  |
      | Time available, Approved time, Pending time, Total plan entitlement, Calculation method |


  @C115-F05-06 @AC4 @AC5 @manual @regression
  Scenario: View Time Taken tab - customize tab name
    Given I log in to the application
    * I customize View Time Taken tab name
    When I open Employee Profile page of "employee who has approved absence(s) connected to leave plan(s)"
    Then I verify I can see customized text on View Time Taken tab


  # time used, future time requested fields are no longer available
  @C115-F05-07 @AC5 @manual @cannotGenerateTestData
  Scenario: View Time Taken tab - time used & requested
    Given I log in to the application
    And I open Employee Profile page of "employee who has approved/pending absences in past and future"
    When I open "Time Taken" tab
    Then I see that each Leave Plan has 'Time used' calculated within the plan's time frame = Total approved or pending periods dated before today
    And I see each Leave Plan has 'Time Requested' calculated within the plan's time frame = Total approved or pending future dated periods

  # timeline is not available
  @C115-F05-08 @AC6 @manual @cannotGenerateTestData
  Scenario Outline: View Time Taken timeline - entitlement time frame
    Given I log in to the application
    And I open Employee Profile page of "employee who has absence applicable to leave plan that use "<method>" calculation method"
    When I open "Time Taken" tab
    Then I verify that the entitlement box size and position on the timeline fits to the relevant leave plan total entitlement and its calculation method

    Examples:
      | method          |
      | Rolling back    |
      | Rolling forward |
      | Calendar year   |
      | Fixed year      |

  # timeline is not available
  @C115-F05-09 @AC6 @manual @cannotGenerateTestData
  Scenario: View Time Taken timeline - presentation of different period types
    Given I log in to the application
    And I open Employee Profile page of "employee who has approved periods of all types(fixed, episodic, reduced schedule) distributed by months"
    When I open "Time Taken" tab
    Then I verify that each type of period is shown on the timeline (square for each started week of absence)
    And I see that each period is shown only on applicable Leave Plan's timeline


  # timeline is not available
  @C115-F05-10 @AC6 @manual @cannotGenerateTestData
  Scenario: View Time Taken timeline - presentation of different period statuses
    Given I log in to the application
    And I open Employee Profile page of "employee who has periods in all statuses (approved, pending, denied, cancelled) distributed by months"
    When I open "Time Taken" tab
    Then I verify that only Approved and Pending periods are shown on the timeline (square for each started week of absence)
    And I see that each period is shown only on applicable Leave Plan's timeline


  # The calculations are not done
  @C115-F05-11 @AC6 @manual @cannotGenerateTestData
  Scenario Outline: View Time Taken timeline - leave plans balance
    Given I log in to the application
    And I open Employee Profile page of "employee who has absence applicable to leave plan that use "<method>" calculation method"
    When I open "Time Taken" tab
    Then I see the current balance of a leave plan for a relevant time frame and according the leave plan’s calculation method

    Examples:
      | method          |
      | Rolling back    |
      | Rolling forward |
      | Calendar year   |
      | Fixed year      |


  @C115-F05-12 @AC6 @manual @regression
  Scenario Outline: View Time Taken timeline - calculation date
    Given I log in to the application
    And I open Employee Profile page of "employee who has absence applicable to leave plan that use "<method>" calculation method"
    When I open "Time Taken" tab
    Then I verify that calculation date is "<calculationDate>"

    Examples:
      | method          | calculationDate                                              |
      | Rolling back    | last valid time off day (not cancelled, withdrawn or denied) |
      | Rolling forward | calculationDate parameter is not sent                        |
      | Calendar year   | calculationDate parameter is not sent                        |
      | Fixed year      | calculationDate parameter is not sent                        |


  # timeline is not available
  @C115-F05-13 @AC7 @manual @cannotGenerateTestData
  Scenario: View Time Taken timeline - colour representation of periods
    Given I log in to the application
    And I open Employee Profile page of "employee who has approved/pending absences in past and future"
    Then I can see weeks, where no leave requests have been reported, are represented by 1 square, painted light grey
    And I can see weeks back of today’s date are represented by 1 square painted dark grey
    And I can see weeks that are future dated (includes today) are represented by 1 square painted dark pink


  # timeline is not available
  @C115-F05-14 @AC7 @manual @cannotGenerateTestData
  Scenario Outline: View Time Taken timeline - period length representation within 1 calendar week
    Given I log in to the application
    When I open Employee Profile page of "employee who has approved <length> absence that suits in 1 calendar week"
    Then I can see the period is represented by 1 square and it is located properly on timeline

    Examples:
      | length |
      | 1 hour |
      | 1 day  |
      | 7 days |

  # timeline is not available
  @C115-F05-15 @AC7 @manual @cannotGenerateTestData
  Scenario: View Time Taken timeline - period length representation within 2 calendar weeks
    Given I log in to the application
    When I open Employee Profile page of "employee who has approved 7 days absence that not suits in 1 calendar week"
    Then I can see the period is represented by 2 squares and it is located properly on timeline


  # timeline is not available
  @C115-F05-16 @AC8 @manual @cannotGenerateTestData
  Scenario: View Time Taken timeline - hovering on periods squares
    Given I log in to the application
    And I open Employee Profile page of "employee who has approved absence(s) connected to leave plan(s)"
    When I hover over a period represent a leave request in the timeline
    Then I expect to see a pop up with the case reference number and the leave reason for that request


  @C115-F05-17 @AC9 @automated @regression @sanity
  Scenario Outline: View Time Taken tab - hovering on calculation period icon
    Given I log in to the application
    When I open Employee details page "employee who has absence applicable to leave plan that use <method> calculation method"
    And I open "Time Taken" tab
    And I hover over the help icon next to the "<method>" calculation method
    Then I expect to see an calculation explanation for "<method>" calculation method

    Examples:
      | method          |
      | Rolling Back    |
      | Rolling Forward |
      | Calendar Year   |
      | Fixed Year      |
