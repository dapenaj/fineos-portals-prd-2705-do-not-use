@employer-portal @C119-F01
Feature: C119-F01 - Integrated Established Intake

  @C119-F01B-01 @permissions @AC14 @manual
  Scenario Outline: Existing employee - Permissions to create intake - search results
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_NOTIFICATIONS_ADD"
    When I search for "existing" employee
    Then I can see a list of items that match my search query
    And the button to create notification intake is "<visibility>" next to the employee name

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |


  @C119-F01B-02 @permissions @AC15 @manual
  Scenario Outline: Existing employee - permissions to see employee's details
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMERINFO"
    When I search for "existing" employee
    Then I can see a list of items that match my search query
    And the button to create notification intake is "<visibility>" next to the employee name

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |


  @C119-F01B-03 @permissions @AC16 @manual
  Scenario Outline: Existing employee - permissions to see employee's earnings
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS"
    When I open intake creator for "existing" employee
    Then I verify that "Employee Earnings" section is "<visibility>"

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |


  @C119-F01B-04 @permissions @AC17 @manual
  Scenario Outline: Existing employee - permissions to edit employee's details
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMERINFO_EDIT"
    When I open intake creator for "existing" employee
    Then the link to edit "Employee Details" section is "<visibility>"

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |


  @C119-F01B-05 @AC19 @smoke @automated
  Scenario: Existing employee - view employee's details
    Given I log in to the application
    When I open intake creator for "existing" employee with all personal details defined
    Then I should see employee's details: "Full Name, Address line 1, Address line 2, Address line 3, City, State, Phone calls preference"


  @C119-F01B-06 @AC20 @smoke @automated
  Scenario: Existing employee - edit employee's details
    Given I log in to the application
    When I open intake creator "editable" using employee id
    And I edit the address fields with random data and confirm
    Then I verify that "Save and proceed" button is enabled
    When I click on "Save and proceed" button
    Then a change of the Employee’s details should be stored in system
    When I click the "My Dashboard" page link
    And I "accept" alert
    And I open intake creator "editable" using employee id
    Then I can see changes I made in intake flow

  @C119-F01B-06B @AC20 @automated @regression
  Scenario Outline: Existing employee - select country address in employee's address details
    Given I log in to the application
    When I open intake creator "of employee with job end date" using employee id
    When I select "<country>" in the country dropdown field in edit details page
    Then I should see empty address fields: "<address fields>"
    And I fill mandatory "<address fields>" including the "<postalZipCode>" postal/zip code
    Then I verify that the address is updated on the page
    When I click on "Save and proceed" button
    When I click the "My Dashboard" page link
    And I "accept" alert
    And I open intake creator "of employee with job end date" using employee id
    Then I verify that the address is updated on the page

    Examples:
      | country       | address fields                                                                                                       | postalZipCode |
      | United States | Address line 1,Address line 2,Address line 3,City,State,Zip code                                                     | 41567         |
      | Canada        | Address line 1,Address line 2,Address line 3,City,Province,Postal code                                               | A1B6T7        |
      | Ireland       | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 123456        |
      | Sweden        | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 1234567       |
      | New Zealand   | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 1234          |
      | Mexico        | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 12345         |
      | Singapore     | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 123456        |
      | Hong Kong     | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 12345         |
      | Japan         | Address line 1,Address line 2,Address line 3,Address line 4,Address line 5,Address line 6,Address line 7,Postal code | 12345         |

  @C119-F01B-06C @AC20 @automated
  Scenario Outline: Existing employee - check mandatory address fields validation in employee's address details
    Given I log in to the application
    When I open intake creator "of employee with job end date" using employee id
    #And I choose to edit employee details with Request Change button
    When I select "<country>" in the country dropdown field in edit details page
    When I click on "Save and proceed" button
    Then I verify the required error displays under the mandatory address fields "<address fields>"
    And I fill mandatory "<address fields>" including the "<postalZipCode>" postal/zip code
    When I click on "Save and proceed" button
    Then a change of the Employee’s details should be stored in system
    When I click the "My Dashboard" page link
    And I "accept" alert
    When I open intake creator "of employee from USA" using employee id
    Then I verify that the address is updated on the page

    @regression
    Examples:
      | country       | address fields                           | postalZipCode |
      | United States | Address line 1,City,State,Zip code       | 41567         |
      | Canada        | Address line 1,City,Province,Postal code | A1B6T7        |

    @notSupported
    Examples:
      | country     | address fields             | postalZipCode |
      | Netherlands | Address line 1,Postal code | 1234AB        |


  @C119-F01B-07 @AC21 @AC22 @automated @regression
  Scenario Outline: Existing employee - edit employee's contact preference
    Given I log in to the application
    When I open intake creator "of employee from USA" using employee id
    And I choose to edit employee contact preference by "<method>"
    Then I verify that "<button>" button is enabled
    When I click on "<button>" button
    When I click the "My Dashboard" page link
    And I "accept" alert
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I open Communication Preferences of the same employee
    Then I can see changes in the phone numbers I made in intake flow

    Examples:
      | method                                                          | button              |
      | Add a new phone as the preferred contact method                 | Save and proceed    |
      | Choose an existing phone number as the preferred contact method | Confirm and proceed |

# On the UI the "Date of hire" field is called "Job started date"
  @C119-F01B-09 @AC23 @smoke @automated @sanity
  Scenario: Existing employee - occupation and earnings
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I select random phone number
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"


  @C119-F01B-10 @AC25 @smoke @automated
  Scenario: Existing employee - earnings hidden by default
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I select random phone number
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the earnings details are hidden by default
    And I can click on the Show Earnings link to see the value


  @C119-F01B-10-1 @smoke @automated
  Scenario: Employee not established - initial request
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I select random phone number
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    When I proceed to "(3) Initial Request" section for existing employee
    Then I should see list of notification reasons radio buttons
    And the initial request page labels "have" "*" on the fields "Reason,Notification Last Working Day"
    And the initial request page labels "do not have" "*" on the fields "Notification Description"

  @C119-F01B-11 @AC26 @AC28 @automated
  Scenario Outline: Existing employee - acknowledgement screen - successful processing
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I select random phone number
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    And I proceed to "(3) Initial Request" section for existing employee
    And I select "<reason>" notification reason
    And I select random date
    When I submit intake
    Then I should see acknowledgement screen
    And acknowledgement screen should contain reference to notification ID "NTN-"
    When I close acknowledgement screen
    Then My Dashboard page with available notifications is loaded

    @smoke
    Examples:
      | reason                                      |
      | Accident / treatment required for an injury |

    @regression
    Examples:
      | reason                                                        |
      | Sickness / time required for a medical treatment or procedure |
      | Pregnancy, birth or related medical treatment                 |
      | Child bonding                                                 |
      | Caring for a family member                                    |
      | Time required for another reason                              |
      | Accommodation required to remain at work                      |


  @C119-F01B-12 @AC26 @AC29 @manual
  Scenario: Existing employee - acknowledgement screen - request failed
    * I customize the error message on the submission screen
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I proceed to "Initial Request" section
    And I select "<reason>" notification reason
    When I submit intake, but request finish with error
    Then I should see acknowledgement screen
    And I should see customized error message


  @C119-F01B-13 @regression @automated
  Scenario: Existing employee - initial request - proceed without the reason checked
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I select random phone number
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    And I proceed to "(3) Initial Request" section for existing employee
    And I select random date
    And I try to submit intake without selecting reason
    Then I verify that "Finish and submit" button is enabled
    And I shouldn't be able to submit intake


  @C119-F01B-14 @regression @automated
  Scenario: Existing employee - proceed without the contact preferences selected
    Given I log in to the application
    And I open intake creator for "existing employee with no contact preference"
    When I try to submit intake without selecting contact preferences
    Then I verify that "Confirm and proceed" button is enabled
    And I shouldn't be able to proceed to "Occupation & Earnings" section

  @C119-F01B-15 @AC27 @regression @automated
  Scenario Outline: Existing employee - cancel intake creation - accepting confirmation popup
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I edit employee name
    When I click <button> button to cancel intake
    Then I should see confirmation popup asking me if I really want to abandon intake
    When I choose "OK" on confirmation popup
    Then No intake will be created
    And My Dashboard page with available notifications is loaded

    Examples:
      | button       |
      | My Dashboard |
      | Abandon      |


  @C119-F01B-15-01 @AC27 @regression @automated
  Scenario Outline: Existing employee - cancel intake creation - rejecting confirmation popup
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I edit employee name
    When I click <button> button to cancel intake
    Then I should see confirmation popup asking me if I really want to abandon intake
    When I choose "Cancel" on confirmation popup
    Then The abandon intake popup disappears
    And I should be able to continue intake creation

    Examples:
      | button       |
      | My Dashboard |
      | Abandon      |

  @C119-F01B-16 @AC28 @manual
  Scenario: Existing employee - closing acknowledgement screen
    * I customize the text on the submission screen
    Given I log in to the application
    When I submit intake for "existing" employee
    Then I should see acknowledgement screen
    And I should see customized text
    When I close acknowledgement screen
    Then My Dashboard page with available notifications is loaded


  @C119-F01B-17 @regression @automated
  Scenario Outline: Existing employee - Steps panel navigation - skip step
    Given I log in to the application
    When I open intake creator for "existing" employee
    Then I cannot jump to "<step>" using steps navigation panel

    Examples:
      | step                      |
      | (2) Occupation & Earnings |
      | (3) Initial Request       |


  @C119-F01B-18 @regression @automated
  Scenario Outline: Existing employee - Steps panel navigation - mandatory fields filled - skip step
    Given I log in to the application
    When I open intake creator for "existing" employee
    Then I cannot jump to "<step>" using steps navigation panel

    Examples:
      | step                      |
      | (2) Occupation & Earnings |
      | (3) Initial Request       |


  @C119-F01B-19 @regression @automated
  Scenario Outline: Existing employee - Steps panel navigation - jump back
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I select random phone number
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    When I proceed to "(3) Initial Request" section for existing employee
    Then I can jump to step "<step>" using steps navigation panel

    Examples:
      | step                      |
      | (1) Employee's Details    |
      | (2) Occupation & Earnings |


  @C119-F01B-20 @regression @automated
  Scenario Outline: Existing employee - back button
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I proceed to "<step>" section for existing employee
    When I click "back" button on "<step>"
    Then I should land on "<previousStep>" page

    Examples:
      | step                      | previousStep              |
      | (2) Occupation & Earnings | (1) Employee's Details    |
      | (3) Initial Request       | (2) Occupation & Earnings |


  @C119-F01B-21 @regression @automated
  Scenario: Existing employee - back button on initial page
    Given I log in to the application
    When I open intake creator for "existing" employee
    Then I shouldn't see back button
    And I should see "Abandon intake" button


  @C119-F01B-22 @regression @automated
  Scenario Outline: Existing employee - jump to step without saving changes
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    And I proceed to "(3) Initial Request" section for existing employee
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    And I edit the address fields with random data and confirm
    When I try to jump to step "(3) Initial Request" using steps navigation panel
    Then I should see "How do you want to proceed" modal
    When I click "<choice>" button
    Then I should land on "<page>" page
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    And I "<visibility>" see the changes I made in intake flow


    Examples:
      | choice                 | page                   | visibility |
      | NO, discard my changes | (3) Initial Request    | can not    |
      | X (closing popup)      | (1) Employee's Details | can not    |

  @C119-F01B-23 @regression @automated
  Scenario: Existing employee - jump to step with saving changes
    Given I log in to the application
    And I open intake creator for "existing" employee
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    And I proceed to "(3) Initial Request" section for existing employee
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    And I edit the address fields with random data and confirm
    When I try to jump to step "(3) Initial Request" using steps navigation panel
    Then I should see "How do you want to proceed" modal
    When I click "YES, save my changes" button
    Then I should land on "(3) Initial Request" page
    Then a change of the Employee’s details should be stored in system
    And I can jump to step "(1) Employee's Details" using steps navigation panel
    Then I can see changes I made in intake flow
