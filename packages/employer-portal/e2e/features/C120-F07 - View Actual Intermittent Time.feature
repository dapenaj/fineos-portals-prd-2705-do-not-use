@employer-portal @C120-F07
Feature: C120-F07 - View Actual Intermittent Time

  @C120-F07-01 @AC1.1 @permissions @manual
  Scenario Outline: Displaying 'Show latest episodes' link depending on the permissions
    Given I log in to the application as a user "<withOrNotPermission>" permissions to "URL_GET_GROUPCLIENT_ACTUAL_TIME_RECORDED"
    And I open notification with "job protected leave with periods containing and not containing actuals"
    When I expand job protected leave card period "<withOrNotActuals>" actuals
    Then I "<canOrNot>" see the 'Show latest episodes' link

    Examples:
      | withOrNotPermission | withOrNotActuals | canOrNot |
      | with                | with             | can      |
      | without             | with             | can't    |
      | with                | without          | can't    |
      | without             | without          | can't    |

  @C120-F07-02 @AC1.1 @manual
  Scenario Outline: Displaying 'Show latest episodes' link depending on the actuals related to the period
    Given I log in to the application
    When I open notification with "<notificationDetails>"
    Then I can see the actuals date that is outside of the leave periods "<action>"
    And I can see the 'Show latest episodes' for this period

    Examples:
      | notificationDetails                                                                                                     | action                                                     |
      | job protected leave with actuals date that is outside of the leave periods                                              | is assigned to the closest period                          |
      | job protected leave with actuals date that is outside of the leave periods and it is the same distance from two periods | is assigned to the period that falls first on the calendar |

  @C120-F07-03 @AC2.1 @AC2.2 @AC2.3 @automated @smoke
  Scenario Outline: Displaying a list of actual time taken content with 'Show latest episodes'
    Given I log in to the application
    And I open notification with "job protected leave with periods containing actuals"
    When I expand job protected leave card period "with" actuals
    And I click "Show latest episodes" link on an intermittent period
    Then I see a list of actual time taken
    And for each item on the list I see Episode date(s), Episode duration, Decision values
    And the Episode date(s) is displayed in format mmm dd, yyyy on the list
    And I see the Decision on the list contains a "<colour>" icon representing the "<status>" status

    Examples:
      | status   | colour |
      | Approved | green  |
      | Pending  | yellow |
      | Declined | red    |

  @C120-F07-04 @AC2.2 @manual
  Scenario Outline: Displaying 'Episode duration' details
    Given I log in to the application
    And I open notification with "job protected leave with periods containing actuals"
    When I expand job protected leave card period "with" actuals
    And I click 'Show latest episodes' link on an intermittent period
    Then I see a list of actual time taken
    When the 'episodicPeriodBasis' value is set to "<episodicPeriodBasis>"
    Then I see the 'Episode duration' value contains "<episodeDuration>"

    Examples:
      | episodicPeriodBasis | episodeDuration   |
      | Minutes             | hours and minutes |
      | Hours               | hours             |
      | Day(s)              | day(s)            |

  @C120-F07-05 @AC2.4 @automated @smoke @sanity
  Scenario Outline: Displaying 'View more' link for 'Show latest episodes' list
    Given I log in to the application
    And I open notification with "<withANum>"
    When I expand job protected leave card period "<withANum>" actuals
    And I click "Show latest episodes" link on an intermittent period
    Then I see a list of actual time taken
    And I "<canOrNot>" see the 'View more' link under the list

    Examples:
      | withANum                     | canOrNot |
      | with more than 5 episodes    | can      |
      | with no more than 5 episodes | can't    |

  @C120-F07-06 @AC2.5 @automated @smoke
  Scenario: Hiding a list of actual time taken content with 'Hide latest episodes'
    Given I log in to the application
    And I open notification with "job protected leave with periods containing actuals"
    When I expand job protected leave card period "with" actuals
    And I click "Show" latest episodes link on an intermittent period
    Then I see a list of actual time taken
    When I click "Hide" latest episodes link on an intermittent period
    Then I see the list of actual time taken is collapsed and hidden
    And the link name is changed from 'Hide latest episodes' to 'Show latest episodes'

  @C120-F07-07 @AC3.1 @AC3.2 @AC3.3 @AC3.4 @automated @smoke
  Scenario Outline: Displaying 'View more' details for 'Show latest episodes' list
    Given I log in to the application
    And I open notification with "with more than 5 episodes"
    When I expand job protected leave card period "with more than 5 episodes" actuals
    And I click "Show latest episodes" link on an intermittent period
    Then I see a list of actual time taken
    When I click View more link under the list
    Then a pop-up is displayed with a list of all the actual time taken for that period
    And the title of the pop-up is in the form [leave plan name] - [absence reason]
    And for each item in the pop-up I see Episode date(s), Episode duration, Decision values
    And the Episode date(s) is displayed in format mmm dd, yyyy in the pop-up
    And I see the Decision in the pop-up contains a "<colour>" icon representing the "<status>" status

    Examples:
      | status   | colour |
      | Approved | green  |
      | Pending  | yellow |
      | Declined | red    |

  @C120-F07-08 @AC3.5 @automated @smoke @sanity
  Scenario: Actuals pop-up sorting
    Given I log in to the application
    And I open notification with "job protected leave with periods containing actuals"
    When I expand job protected leave card period "with more than 5 episodes" actuals
    And I click "Show latest episodes" link on an intermittent period
    Then I see a list of actual time taken
    When I click View more link under the list
    Then a pop-up is displayed with a list of all the actual time taken for that period
    And the list of items is sorted by Episode date(s) in "descending" order by default
    When I click the Episode date(s) column name
    And I click the Episode date(s) column name
    Then the list of items is sorted by Episode date(s) in "ascending" order
    When I click the Episode date(s) column name
    Then the list of items is sorted by Episode date(s) in "descending" order

  @C120-F07-09 @AC3.6 @automated @smoke @sanity
  Scenario Outline: Actuals pop-up filtering
    Given I log in to the application
    And I open notification with "job protected leave with periods containing actuals"
    When I expand job protected leave card period "with more than 5 episodes" actuals
    And I click "Show latest episodes" link on an intermittent period
    Then I see a list of actual time taken
    When I click View more link under the list
    Then a pop-up is displayed with a list of all the actual time taken for that period
    When I click the filter icon next to the Decision column name
    Then I can filter the items by the "<decision>" Decision
    And I verify the results on the list are correctly filtered by "<decision>"

    Examples:
      | decision |
      | Approved |
      | Pending  |
      | Declined |

  @C120-F07-10 @AC3.7 @AC3.8 @manual
  Scenario Outline: Actuals pop-up scrolling and closing
    Given I log in to the application
    And I open notification with "job protected leave with periods containing actuals"
    When I expand job protected leave card period "with more than 5 episodes" actuals
    And I click 'Show latest episodes' link on an intermittent period
    Then I see a list of actual time taken
    When I click 'View more' link under the list
    Then a pop-up is displayed with a list of all the actual time taken for that period
    And in case of a lot of items on the list I can scroll up and down the results to see all of them
    When I click "<closeBtn>" on the actuals pop-up
    Then The actuals pop-up is closed
    And I am returned to Job Protection Card on the notification page

    Examples:
      | closeBtn     |
      | close X      |
      | close button |

