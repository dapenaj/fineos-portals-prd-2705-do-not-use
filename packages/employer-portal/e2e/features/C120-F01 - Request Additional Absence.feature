@employer-portal @C120-F01
Feature: C120-F01 - Request Additional Absence, Allow employer to request additional absence time, where the employee needs more time off work

  @C120-F01-01 @permissions @AC1 @manual @regression
  Scenario Outline: Displaying option to request additional absence - user without permissions
    Given I log in to the application as a user "without" permissions to "<permission>"
    And I open notification page
    When I click on the actions menu options on the notification page
    Then I verify that the menu action "Request for more time" is "not visible" in the dropdown
    Examples:
      | permission                                                 |
      | URL_POST_GROUPCLIENT_ABSENCE_LEAVEPERIODSCHANGEREQUEST_ADD |


    # requires URL_POST_GROUPCLIENT_ABSENCE_LEAVEPERIODSCHANGEREQUEST_ADD permission
  @C120-F01-02 @permissions @AC1 @automated @sanity @smoke
  Scenario: Displaying option to request additional absence - user with permissions
    Given I log in to the application
    And I open notification with "single absence cases"
    When I click on the actions menu options on the notification page
    Then I verify that the menu action "Request for more time" is "visible" in the dropdown


  @C120-F01-04 @C120-F01-05 @AC2 @smoke @automated
  Scenario Outline: Request Additional Absence pop-up - accepting/cancelling
    Given I log in to the application
    And I open notification with "single absence cases" and test data "@C120-F01-04-TD-1"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    Then I should see popup informing that additional time might not be the applicable
    When I "<popupAction>" additional time popup
    Then I verify that Additional Absence form is "<isVisible>"

    Examples:
      | popupAction | isVisible   |
      | accept      | visible     |
      | cancel      | not visible |


  @C120-F01-06 @AC3 @smoke @automated
  Scenario: Request Additional Absence - Form fields
    Given I log in to the application
    And I open notification with "single absence case" and test data "@C120-F01-06-TD-1"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I should see all mandatory and optional fields of 'Additional Absence' form

  @C120-F01-07 @AC3 @automated @smoke
  Scenario: Request Additional Absence - Proceed without filling mandatory fields
    Given I log in to the application
    And I open notification with "single absence cases"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    Then I should see popup informing that additional time might not be the applicable
    When I "accept" additional time popup
    Then I verify that Additional Absence form is "visible"
    When I type following text "Time off reason description test reason" in the "Time off reason description" field
    And I "accept" "Request for more time" time modal
    Then "required period validation" error message is visible
    And I verify that Additional Absence form is "visible"


  @C120-F01-08 @AC3 @smoke @automated
  Scenario: Request Additional Absence - Cancel request
    Given I log in to the application
    And I open notification with "single absence cases"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    Then I should see popup informing that additional time might not be the applicable
    When I "accept" additional time popup
    Then I verify that Additional Absence form is "visible"
    When I "cancel" "Request for more time" time modal
    Then "Request for more time" confirmation popup is "not visible"
    And I verify that Additional Absence form is "not visible"


  @C120-F01-09 @AC5 @smoke @automated @sanity
  Scenario: Request Additional Absence - Acknowledgment screen
    Given I log in to the application
    And I open notification with "single absence case" and test data "@C120-F01-09-TD-1"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I should see all mandatory and optional fields of 'Additional Absence' form
    When I select additional time in not occupied time slot
    And I "accept" "Request for more time" time modal
    Then I should see acknowledgment screen

  @C120-F01-10 @AC4 @AC13 @regression @automated
  Scenario: Request Additional Absence - Happy path scenario
    Given I log in to the application
    And I open notification with "single absence case" and test data "@C120-F01-09-TD-1"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I should see all mandatory and optional fields of 'Additional Absence' form
    When I select additional time in not occupied time slot
    And I "accept" "Request for more time" time modal
    Then I should see acknowledgment screen

  @C120-F01-11 @manual
  Scenario: Request Additional Absence - Request failure
    Given I log in to the application
    And I open notification with "single absence case"
    And I open Request Additional Absence form
    And I select additional time in not occupied time slot
    When I select "OK" button on Additional Absence form but error occurred during sending request
    Then I should see error message handled correctly


  @C120-F01-12 @permissions @AC6 @manual @regression
  Scenario: Request Additional Absence - no permissions to absence decisions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS"
    And I open notification with "single absence case"
    And I open Request Additional Absence form
    When I open date range picker
    Then I should not see the periods marked on the calendar
    And I "can" proceed with additional absence request


  @C120-F01-13 @AC7 @manual @sanity
  Scenario: Request Additional Absence - today's date
    Given I log in to the application
    And I open notification with "single absence case"
    And I open Request Additional Absence form
    When I open date range picker
    Then I should see today’s date highlighted on the calendar


  @C120-F01-16 @AC8 @AC9
  Scenario Outline: Request Additional Absence - existing periods types and statuses
    Given I log in to the application
    And I open notification with "single <status> absence case with  <type> period type" and test data "<testData>"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I should see all mandatory and optional fields of 'Additional Absence' form
    When I open date range picker
    Then existing period should be represented as "<expectedColor>" "<expectedGradientType>" time bar displayed between correct start & end dates

    @automated @regression
    Examples:
      | type             | expectedGradientType  | status   | expectedColor | testData          |
      | Time off         | solid line            | approved | green         | @C120-F01-16-TD-1 |
      | Reduced Schedule | horizontal split line | declined | red           | @C120-F01-16-TD-2 |
      | Episodic         | striped line          | pending  | yellow        | @C120-F01-16-TD-3 |

    @cannotGenerateTestData
    Examples:
      | type                  | expectedColor | expectedGradientType | testData |
      | Please select         |               | solid line           |          |
      | Incapacity            |               | solid line           |          |
      | Office Visit          |               | solid line           |          |
      | Incapacity Episodic   |               | striped line         |          |
      | Office Visit Episodic |               | striped line         |          |


  @C120-F01-17 @AC10 @manual
  Scenario: Request Additional Absence - displaying overlapping periods
    Given I log in to the application
    And I open notification with "single absence case" and periods with all statuses
    And I open Request Additional Absence form
    When I open date range picker
    Then I should see the periods displayed in order based on status, starting from top: "Approved, Pending, Denied"


  @C120-F01-18 @AC11 @automated @regression
  Scenario Outline: Request Additional Absence - selecting overlapping period
    Given I log in to the application
    And I open notification with "single absence case with status <status>"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I should see all mandatory and optional fields of 'Additional Absence' form
    When I open date range picker
    Then I verify I cannot add additional absence that overlaps existing one

    Examples:
      | status   |
      | approved |
      | pending  |
      | declined |


  @C120-F01-19 @AC12 @automated @regression
  Scenario: Request Additional Absence - date range picker switching months
    Given I log in to the application
    And I open notification with "single absence cases"
    When I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I should see all mandatory and optional fields of 'Additional Absence' form
    When I open date range picker
    Then by default I see the current month when the calendar is shown
    And I verify I am able to navigate to the "3" "next" months from today’s date
    And I verify I am able to navigate to the "2" "previous" months from today’s date


  @C120-F01-20 @manual
  Scenario: Request Additional Absence - Displaying leaves longer than week
    Given I log in to the application
    And I open notification with "single absence case containing at least 1 weekend"
    And I open Request Additional Absence form
    When I open date range picker
    Then I should see one time bar representing leave
    And  time bar should begins at start date and finish on end date


  @C120-F01-21 @manual
  Scenario: Request Additional Absence - One of leave plans denied
    Given I log in to the application
    And I open notification with "approved single absence case where one of leave plans is denied"
    And I open Request Additional Absence form
    When I open date range picker
    Then I should see time bar in "approved" state because approved periods should overlap other states


  @C120-F01-22 @manual
  Scenario: Request Additional Absence - Intermittent absence requests with reported actuals
    Given I log in to the application
    And I open notification with "Intermittent absence requests with reported actuals"
    And I open Request Additional Absence form
    When I open date range picker
    Then I verify that reported actual time is not displayed
    And I should see the intermittent period


  @C120-F01-23 @AC11 @regression @automated
  Scenario: Request Additional Absence - Absence requests that have been withdrawn/ cancelled
    Given I log in to the application
    And I open notification with "cancelled period" and test data "@C120-F04-11"
    When I expand all collapsible cards
    And I can see the cancelled periods in the 'Job-protected Leave' drawer
    And I select "Request for more time" from Actions menu on the 'Notification Summary' card
    And I "accept" additional time popup
    Then I check cancelled period does not appear in the date picker
    
