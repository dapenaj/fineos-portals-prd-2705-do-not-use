@employer-portal @C122-F02
Feature: C122-F02 - Manage Employee Work Details PART A

  @C122-F02-01 @AC3.1 @permissions @manual
  Scenario: Permission to view the employee’s Employment Details
    Given I log in to the application as a user "with" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_GETSINGLE"
    When I open Employee Details
    Then I "can" see "Job Title, Organisation Unit, Work Site" fields below the Employee name


  @C122-F02-02 @AC3.3 @permissions @manual @regression
  Scenario Outline: Permission to view the employee’s Employment Details
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS"
    When I open Employee Details
    Then I verify that Employment Details section is "<visibility>"
    And I <canOrNot> see navigation leading to Employment Details section

    Examples:
      | withOrWithout | visibility  | canOrNot |
      | with          | visible     | can      |
      | without       | not visible | can't    |

  @automated @sanity @regression
    @C122-F02-03 @AC2.1 @AC2.2
    @C122-F02D-01 @AC1.1 @AC1.2 @AC1.3 @AC1.4
  Scenario Outline: Current Occupation layout validation
    Given I log in to the application
    When I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    Then I verify that "Employment Details" section is shown
    And the Employment Details "Current occupation" card should "have" following fields "<fields>"
    And the Employment Details "Current occupation" card should "not have" following fields "<no fields>"

    Examples:
      | employee                                      | fields                                                                                                                                                                                                       | no fields                                                        |
      | with all current occupation details           | Your employee holds the job title of, from, until, Their adjusted date of hire is, Their employment type is:, They work on, basis, Their average hours worked per week are:, Their contractual earnings are: | Work pattern, Employment status                                  |
      | without job end date and contractual earnings | Your employee holds the job title of, since, Their adjusted date of hire is, Their employment type is:, They work on, basis, Their average hours worked per week are:                                        | Work pattern, Employment status, Their contractual earnings are: |

  @C122-F02-04 @AC2.3 @AC2.4 @regression @automated
  Scenario Outline: Employment Details - job end date, earnings
    Given I log in to the application
    When I open Employee details page "<employee>"
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    Then I verify that "Employment Details" section is shown
    And the Employment Details "<cardName>" card should "have" following fields "<fieldName>"
    And the "<fieldName>" data should be presented in format: "<expectedFormat>"

    @sanity
    Examples:
      | employee                  | fieldName                       | expectedFormat      | cardName           |
      | of employee with earnings | Their contractual earnings are: | [Amount][Frequency] | Current occupation |

    Examples:
      | employee                      | fieldName                            | expectedFormat                       | cardName             |
      | of employee with job end date | Your employee holds the job title of | [Job title] from [Date] until [Date] | Current occupation   |
      | of employee with job end date | Their US work state is               | [State]                              | Eligibility criteria |


  @C122-F02-06 @AC2.4 @manual
  Scenario: Employment Details - earnings - multiple occupations
    Given I log in to the application
    When I open Employment Details page of user who has multiple occupations sets
    Then I should see the earnings from the most recent occupation whose dateJobBegan is closest to today’s date but not later than today’s date


  @C122-F02-07 @AC2.4 @manual @regression
  Scenario: Employment Details - earnings - multiple earnings
    Given I log in to the application
    When I open Employment Details page of user who has multiple earnings sets
    Then I should see the earnings with the whose effectiveDate is closest to today’s date but not later than today’s date


  @C122-F02-08 @AC2.5 @AC2.6 @permissions @manual @regression
  Scenario Outline: Employment Details - earnings permissions and appearance
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS"
    When I open Employment Details page of user who "<hasOrNot>" defined earnings
    Then I verify that Earnings field is "<visibility>"

    Examples:
      | withOrWithout | hasOrNot | visibility  |
      | without       | has      | not visible |
      | with          | hasn't   | not visible |
      | with          | has      | visible     |
