@employer-portal @C117-F06
Feature: C117-F06 - Send & Receive Web Messages

  @C117-F06-01 @AC1.1 @permissions @manual
  Scenario Outline: Check the permissions to see the Messages page and Messages card
    Given I log in to the application"<withOrWithout1>" permissions to "URL_GET_GROUPCLIENT_WEBMESSAGES"
    And I have as a user "<withOrWithout2>" permission to "URL_GET_GROUPCLIENT_CASES_WEBMESSAGES"
    And I have as a user "<withOrWithout3>" permission to "URL_POST_GROUPCLIENT_CASES_WEBMESSAGES_ADD"
    And I have as a user "<withOrWithout4>" permission to "URL_POST_GROUPCLIENT_CASES_WEBMESSAGES_EDIT"
    When I open random notification
    Then I verify that the "Messages" main navigation bar menu is "<visibility>"
    And I verify that the "Messages" tab on Alerts card is "<visibility>"

    Examples:
      | withOrWithout1 | withOrWithout2 | withOrWithout3 | withOrWithout4 | visibility  |
      | with           | with           | with           | with           | visible     |
      | without        | with           | with           | with           | not visible |
      | with           | without        | with           | with           | not visible |
      | with           | with           | without        | with           | not visible |
      | with           | with           | with           | without        | not visible |

  @C117-F06-02 @AC1.2 @manual
  Scenario: Check the empty state on Messages card
    Given I log in to the application
    And I open notification with "0" web messages
    When I click the "Messages" tab on the Alerts card
    Then I verify the "Messages" tab shows empty message "There are no message in this notification yet"
    And I verify the "Messages" tab shows the "New Message" button


  @C117-F06-03 @AC1.3 @AC1.4 @manual
  Scenario: Check the badge disappears when clicking the last remaining unread message
    Given I log in to the application
    When I open notification with "1" unread web message and at least "1" read web messages sent to/by logged in user
    Then I verify the "Messages" tab name on the Alerts card a colored circle badge with count "1" is "visible"
    When I click on the "unread" web message
    Then I verify the Display/Reply web message popup opens
    When I click on the "close X" icon
    And I verify the Display/Reply web message popup closes
    And I verify the "Messages" tab name on the Alerts card a colored circle badge is "not visible"
    And I verify that after the "Messages" navigation menu name a colored circle badge is "not visible"


  @C117-F06-04 @AC1.5 @automated @smoke
  Scenario: Check messages are sorted by Contact Time with most recent first on Messages card
    Given I log in to the application
    When I open notification with "at least 3 web messages" and test data "@C117-F06-04"
    And I click "Messages" tab
    And I add up to "3" web messages
    Then I verify if messages are sorted correctly by contact date time in "descending" order

  @C117-F06-05 @AC1.6 @AC1.7 @manual
  Scenario: Check each web message in list contains Sender, Subject, Contact Date, and Preview first 2 lines on Messages card
    Given I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    When I click the "Messages" tab on the Alerts card
    Then I verify the "Messages" tab shows a list of web messages
    And I verify each message has a Sender badge icon
    And I verify each message has a "Web Message Subject" field
    And I verify each message has a "Contact Date" field
    And I verify each message has a "Preview" field with first 2 lines/few words of message

  @C117-F06-06 @AC1.7 @manual @regression
  Scenario: Check the sender badge is configurable for employer and carrier on Messages card
    Given I customize the "Employer" icon configuration for the deployment
    And I customize the "Carrier" icon configuration for the deployment to be different from the Carrier logo
    And I log in to the application as any user
    And I open notification with at least "1" web messages sent by the "user" and at least "1" web messages sent by "Carrier"
    When I click the "Messages" tab on the Alerts card
    Then I verify all web messages sent by "user" have a Sender badge icon matching the customized "Employer" icon
    And I verify all web messages sent by "Carrier" have a Sender badge icon matching the customized "Carrier" icon
    And I verify "Sender" badge icon of type "Carrier" does not change the Carrier logo on at the navigation bar

  @C117-F06-07 @AC1.8 @automated @regression
  Scenario: Check user is able to clearly distinguish between unread and read Web Messages on Messages card
    Given I log in to the application
    When I open notification with "at least 2 web messages" and test data "@C117-F06-04"
    And I click "Messages" tab
    And I add up to "2" web messages
    And I mark first message as "unread"
    And I mark last message as "read"
    Then I verify only unread web messages have highlighted left side bar line and the Sender badge is not greyed out


  @C117-F06-08 @AC1.9 @automated @regression
  Scenario: Check the Contact Date is in default format MMM DD, YYYY on Messages card
    Given I log in to the application
    When I open notification with "at least 2 web messages" and test data "@C117-F06-04"
    And I click "Messages" tab
    And I add up to "2" web messages
    Then I verify each message has a "Contact Date" field Displayed in the format "MMM DD, YYYY" without the message sent time
    And the notification summary Notified on date has "MMM DD, YYYY" format


  @C117-F06-09 @AC1.9 @manual
  Scenario: Check the Contact Date format is configurable and matches the whole portal format on Messages card
    Given I customize the date time format configuration for the deployment to be "dd.mm.yyyy"
    And I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    When I click the "Messages" tab on the Alerts card
    Then I verify each message has a "Contact Date" field Displayed in the format "dd.mm.yyyy" without the message sent time
    And I verify the configurable Contact Date format matches Notification date format in the Notification summary

  @C117-F06-10 @AC1.10 @automated @regression
  Scenario: Check the web message is marked as unread by clicking mark as unread action and the badges count increases
    Given I log in to the application
    And I open notification with "at least 3 web message" and test data "@C117-F06-04"
    And I click "Messages" tab
    And I add up to "3" web messages
    And I mark first message as "read"
    When I mark first message as "unread"
    And I mark last message as "unread"
    And I record the "Messages" tab badges count
    And I record the "Messages" navigation menu name badges count
    And I mark first message as "read"
    Then I verify first web message is marked as read
    And I verify the "Messages" tab badge decreases by 1
    And I verify the "Messages" navigation menu name badge decreases by 1
    When I record the "Messages" tab badges count
    And I record the "Messages" navigation menu name badges count
    And I mark first message as "unread"
    Then I verify first web message is marked as unread
    And I verify the "Messages" tab badge increases by 1
    And I verify the "Messages" navigation menu name badge increases by 1

  @C117-F06-11 @AC1.11 @manual
  Scenario: Check the Alerts panel with the New Message button is sticky on notification page
    Given I log in to the application
    And I open notification with at least 3 cards such as Job-protected Leave, Wage Replacement or Workplace Accommodation
    When I click the "Messages" tab on the Alerts card
    Then I verify the "Messages" tab shows the "New Message" button
    And I record the Alerts panel position in the browser
    And I record the "New Message" position in the browser
    When I scroll down to the "2nd" card of the notification
    Then I verify the Alerts panel position and "New Message" button position stay fixed and does not scroll
    When I scroll down to the "last" card of the notification
    Then I verify the Alerts panel position and "New Message" button position stay fixed and does not scroll
    When I scroll up to top of the notification
    Then I verify the Alerts panel position and "New Message" button position stay fixed and does not scroll

  @C117-F06-12 @AC2.1 @AC2.2 @AC2.7 @regression @automated
  Scenario: Check the New Message popup opens when clicking New Message button
    Given I log in to the application
    When I open notification with "at least 3 cards" and test data "@C117-F06-12"
    And I click "Messages" tab
    When I click the "New Message" button
    Then the "New Message" popup opens
    And the "New Message" popup labels "have" "*" on the fields "Subject, Message"
    And I enter "abcd" text in the "Message" "New Message" field
    And the "New Message" popup shows text info about message remaining characters


  @C117-F06-13 @AC2.2 @AC2.3 @regression @automated
  Scenario Outline: Check the Send button error validation for min chars for both Subject and Message fields on New Message popup
    Given I log in to the application
    When I open notification page
    And I click "Messages" tab
    And I click the "New Message" button
    Then the "New Message" popup opens
    When I enter "<subjectMsg>" text in the "Subject" "New Message" field
    And I enter "<messageMsg>" text in the "Message" "New Message" field
    And I tab to the next field
    Then I "<canOrNotSubject>" see the validation error "Please enter at least 5 characters" under the "Subject" field
    And I "<canOrNotMessage>" see the validation error "Please enter at least 5 characters" under the "Message" field

    Examples:
      | subjectMsg | messageMsg | canOrNotSubject | canOrNotMessage |
      |            |            | can             | can             |
      | 1234       | 1234       | can             | can             |
      | 1234       | 12345      | can             | can't           |
      | 12345      | 1234       | can't           | can             |
      | 12345      | 12345      | can't           | can't           |

  @C117-F06-14 @AC2.4 @automated @regression
  Scenario Outline: Check the Cancel button closes and shows confirm popup when at least 5 chars in either Subject or Message
    Given I log in to the application
    When I open notification page
    And I click "Messages" tab
    And I click the "New Message" button
    Then the "New Message" popup opens
    When I "<close>" "New Message" modal
    Then the "New Message" popup closes
    When I click the "New Message" button
    Then the "New Message" popup opens
    When I enter "12345" text in the "<messageField>" "New Message" field
    And I "<close>" "New Message" modal
    Then I verify the confirm "cancel" popup opens with customizable text from Theme Configurator
    When I click "no" button
    Then I verify the confirm "cancel" popup closes
    And I verify the "<messageField>" field contains the text "12345"
    When I "<close>" "New Message" modal
    And I click "yes" button
    Then I verify the confirm "cancel" popup closes
    And the "New Message" popup closes
    And I verify no web message is sent to the server

    Examples:
      | close   | messageField |
      | cancel  | Subject      |
      | cancel  | Message      |
      | close X | Subject      |
      | close X | Message      |

  @C117-F06-15 @AC2.5 @AC1.8 @regression @automated
  Scenario: Check the Send button on New Message popup sends the web message which is automatically marked as read
    Given I log in to the application
    When I open notification page
    And I click "Messages" tab
    And I click the "New Message" button
    Then the "New Message" popup opens
    When I enter "random" text in the "Subject" "New Message" field
    And I enter "random" text in the "Message" "New Message" field
    And I "send" "New Message" modal
    Then "Message sent" alert is visible
    When the "New Message" popup closes
    Then I verify the list of web messages is refreshed using an animation with the new message highlighted in yellow temporary
    And I can see new read message with correct content is first one on the list

  @C117-F06-16 @AC2.6 @manual
  Scenario: Check the standard error message on New Message popup
    Given I log in to the application
    And I open random notification
    And I click the "Messages" tab on the Alerts card
    When I click the "New Message" button
    Then I verify the New Message popup opens
    When I enter "Connection Error" in the Subject field
    And I enter "This message should NOT be sent." in the Message field
    And I disconnect the internet connection
    And I click the "Send" button
    Then I verify the Something went wrong popup opens with text "We are unable to send your message. Please try again in a few minutes."


  @C117-F06-17-01 @AC2.7 @automated @regression
  Scenario Outline: Check handling of max number of characters in Messages field on New Message popup - type
    Given I log in to the application
    When I open notification page
    And I click "Messages" tab
    And I click the "New Message" button
    Then the "New Message" popup opens
    When I enter "random" text in the "Subject" "New Message" field
    And I type "<numOfChars>" random characters in the "Message" "New Message" field
    Then I verify under the "Message" field shows "<remainingNumOfChars> characters remaining"
    When I "send" "New Message" modal
    Then "Message sent" alert is visible
    When the "New Message" popup closes
    Then I can see new read message with correct content is first one on the list

    Examples:
      | numOfChars | remainingNumOfChars |
      | 10         | 3990                |
      | 4000       | 0                   |

  @C117-F06-17-02 @AC2.7 @AC2.8 @automated @regression
  Scenario Outline: Check handling of max number of characters in Messages and Subject fields on New Message popup - exceed max chars number
    Given I log in to the application
    When I open notification page
    And I click "Messages" tab
    And I click the "New Message" button
    Then the "New Message" popup opens
    When I type "<typedCharsNumber>" random characters in the "<testedField>" "New Message" field
    And I enter "random" text in the "<requiredField>" "New Message" field
    Then I "can" see the validation error "Please enter a maximum of <maxCharsNumber> characters to send this message" under the "<testedField>" field
    When I remove "1" character from the "<testedField>" "New Message" field
    Then I "can't" see the validation error "Please enter a maximum of <maxCharsNumber> characters to send this message" under the "<testedField>" field
    When I "send" "New Message" modal
    Then "Message sent" alert is visible
    When the "New Message" popup closes
    Then I can see new read message with correct content is first one on the list

    Examples:
      | testedField | maxCharsNumber | typedCharsNumber | requiredField |
      | Message     | 4000           | 4001             | Subject       |
      | Subject     | 196            | 197              | Message       |


  @C117-F06-17-03 @AC2.7 @manual
  Scenario Outline: Check handling of max number of characters in Messages field on New Message popup - paste
    Given I log in to the application
    And I open random notification
    And I click the "Messages" tab on the Alerts card
    When I click the "New Message" button
    Then I verify the New Message popup opens
    And I verify under the "Message" field shows "4000 characters remaining"
    When I "<action>" "<numOfChars>" number of characters in the "Message" field
    Then I verify there are the "numOfChars" characters in the "Message" field
    And I verify under the "Message" field shows "<message>"
    When I click the "Send" button
    Then I verify the New Message popup closes and returns to the notification page "<occursOrNotOccurs>"
    And I verify under the "Message" field shows "<message>" is "<visibility>"
    And I verify the web message "<isOrIsNot>" sent to the server

    Examples:
      | action | numOfChars | message                     | occursOrNotOccurs | visibility  | isOrIsNot |
      | paste  | 10         | 3990 characters remaining   | occurs            | not visible | is        |
      | paste  | 4000       | 0 characters remaining      | occurs            | not visible | is        |
      | paste  | 4010       | Max 4000 chars length error | not occurs        | visible     | is not    |


  @C117-F06-19 @AC3.1 @AC3.3 @AC3.4 @manual
  Scenario Outline: Check the Display/Reply web message popup opens when clicking on message in list
    Given I log in to the application
    And I open notification with at least "4" web messages of "read, unread" sent to/by logged in user
    And I click the "Messages" tab on the Alerts card
    When I click on a "<readOrUnread>" web message sent by "<senderType>"
    Then I verify the Display/Reply web message popup opens
    And I verify the message has a Sender badge icon of "<senderType>" icon and "<colorType>"
    And I verify the Display/Reply web message popup shows the "Sender, Subject, Contact Date, full message content" fields
    And I verify the Display/Reply web message popup's the "Reply" editable field with placeholder "Reply to this message" is "<visibility>"
    And I verify the Display/Reply web message popup shows max characters remaining text "4000 characters remaining"
    And I verify the Display/Reply web message popup contains the "<actionIcons>" action icons

    Examples:
      | senderType | readOrUnread | colorType  | actionIcons | visibility  |
      | Employer   | unread       | colored    | X           | not visible |
      | Carrier    | unread       | colored    | Send, X     | visible     |
      | Employer   | read         | greyed out | X           | not visible |
      | Carrier    | read         | greyed out | Send, X     | visible     |

  @C117-F06-20 @AC3.2 @AC3.8 @automated @regression
  Scenario: Check the web message is marked as read and the badges count decreases by opening and closing
    Given I log in to the application
    And I open notification with "at least 3 web message" and test data "@C117-F06-04"
    And I click "Messages" tab
    And I add up to "3" web messages
    And I mark first message as "read"
    When I mark first message as "unread"
    And I mark last message as "unread"
    And I record the "Messages" tab badges count
    And I record the "Messages" navigation menu name badges count
    And I click the "first" web message
    Then I verify the Display/Reply web message popup opens
    When I "close X" "Message" drawer
    Then I verify first web message is marked as read
    And I verify the "Messages" tab badge decreases by 1
    And I verify the "Messages" navigation menu name badge decreases by 1

  @C117-F06-21 @AC3.5 @manual
  Scenario: Check the Subject field format and is non-editable on the Display/Reply message popup
    Given I log in to the application
    And I open notification with at least "1" web messages sent to/by logged in user
    And I click the "Messages" tab on the Alerts card
    When I click on "random" web message in the list of messages
    Then I verify the Display/Reply web message popup opens
    And I verify the Display/Reply web message popup shows the "Subject" field formatted as "RE: [Message subject]"
    And I cannot type "test" in the Subject field

  @C117-F06-22 @AC3.6 @manual
  Scenario: Check the Reply area can be stretched vertically up and down on the Display/Reply message popup
    Given I log in to the application
    And I open notification with at least "1" web messages sent to/by logged in user
    And I click the "Messages" tab on the Alerts card
    When I click on "random" web message in the list of messages
    And I verify the Display/Reply web message popup opens
    Then I verify the Display/Reply web message popup shows the reply stretch button
    When I drag "up" the reply stretch button by "any number of" pixels
    Then I verify the Replay text area height stretches up by the "same number of" pixels
    When I drag "down" the reply stretch button by "any number of" pixels
    Then I verify the Replay text area height stretches down by the "same number of" pixels

  @C117-F06-23 @AC3.7 @manual
  Scenario: Check the Send button does not submit until min chars are entered in the reply message field on Display/Reply message popup
    Given I log in to the application
    And I open notification with at least "1" web messages sent to/by logged in user
    And I click the "Messages" tab on the Alerts card
    When I click on "random" web message in the list of messages
    Then I verify the Display/Reply web message popup opens
    When I click the "Send" icon
    Then I verify the validation error "Please enter at least 5 characters" shows under the reply message field
    When I enter "1234" in the reply message field
    And I click the "Send" icon
    Then I verify the validation error "Please enter at least 5 characters" shows under the reply message field
    When I clear the reply message field
    And I enter "12345" in the reply message field
    And I click the "Send" icon
    Then I verify the Display/Reply web message popup closes
    And I verify the web message is sent to the server

  @C117-F06-24 @AC3.8 @automated @regression
  Scenario: Check the close X button closes the Display/Reply message popup
    Given I log in to the application
    And I open notification with "at least 3 web message" and test data "@C117-F06-04"
    And I click "Messages" tab
    And I add up to "3" web messages
    When I click the "random" web message
    Then I verify the Display/Reply web message popup opens
    When I "close X" "Message" drawer
    Then I verify the Display/Reply web message popup closes

  @C117-F06-25 @AC3.9 @regression @manual
  Scenario: Check the Send button sends and automatically marked as read for the Display/Reply message popup
    Given I log in to the application
    And I open notification with at least "1" web messages sent to logged in user
    And I click the "Messages" tab on the Alerts card
    When I click on "random" "Carrier" web message in the list of messages
    Then I verify the Display/Reply web message popup opens
    When I type "10" number of characters in the "Reply message" field
    And I click the "Send" button
    Then I verify the web message is sent to the server
    And I verify the Display/Reply web message popup and returns to the notification page
    And I verify the list of web messages is refreshed using an animation with the new message highlighted in yellow temporary
    And I verify the new web message has no highlighted left red line and the Sender badge is greyed out

  @C117-F06-26 @AC3.10 @manual
  Scenario Outline: Check handling of max number of number of characters in the reply message field on Display/Reply message popup
    Given I log in to the application
    And I open notification with at least "1" web messages sent to logged in user
    And I click the "Messages" tab on the Alerts card
    When I click on "random" "Carrier" web message in the list of messages
    Then I verify the Display/Reply web message popup opens
    And I verify under the "Reply message" field shows "4000 characters remaining"
    When I "<action>" "<numOfChars>" number of characters in the "Reply message" field
    Then I verify there are the "numOfChars" characters in the "Reply message" field
    And I verify under the "Reply message" field shows "<message>"
    When I click the "Send" icon
    Then I verify the Display/Reply web message popup closes and returns to the notification page "<occursOrNotOccurs>"
    And I verify under the "Reply message" field shows "<message>" is "<visibility>"
    And I verify the web message "<isOrIsNot>" sent to the server

    Examples:
      | action | numOfChars | message                     | occursOrNotOccurs | visibility  | isOrIsNot |
      | type   | 10         | 3990 characters remaining   | occurs            | not visible | is        |
      | type   | 4000       | 0 characters remaining      | occurs            | not visible | is        |
      | type   | 4001       | Max 4000 chars length error | not occurs        | visible     | is not    |
      | paste  | 10         | 3990 characters remaining   | occurs            | not visible | is        |
      | paste  | 4000       | 0 characters remaining      | occurs            | not visible | is        |
      | paste  | 4010       | Max 4000 chars length error | not occurs        | visible     | is not    |

  @C117-F06-27 @4.1 @manual @sanity
  Scenario Outline: Check the badge is visible when there is at least 1 unread message
    Given I log in to the application
    When I open notification with at least "<numUnreadMsgs>" unread web messages sent to/by logged in user
    Then I verify that after the "Messages" main navigation bar name menu a colored circle badge with count "<numUnreadMsgs>" is "<visibility>"
    When I click on "Messages" in the main navigation bar
    Then I verify that after the recent messages name panel a colored circle badge with count "<numUnreadMsgs>" is "<visibility>"

    Examples:
      | numUnreadMsgs | visibility  |
      | 1             | visible     |
      | 0             | not visible |

  @C117-F06-28 @AC4.2 @regression @manual
  Scenario Outline: Check the messages page contents and if 1st message is unread then mark as read on Messages page
    Given I log in to the application as "user1"
    And I open notification with "4" messages with "1st" "<readOrUnread>" message and "2" messages sent to/by "user1" and "2" messages sent to/by "user2"
    When I click on "Messages" in the main navigation bar
    Then I verify "2" web messages in the list are sent to "user1" and by "user1" via the Employer Portal "are" displayed
    And I verify "2" web messages in the list are sent to "user2" and by "user2" via the Employer Portal "are" displayed
    And I verify the "1st" web message is opened and contents are displayed
    And I verify the "1st" "<readOrUnread>" web message has no highlighted left red line and the Sender badge is greyed out
    And I verify an API server call "<isOrIsNot>" sent to the server to mark the first message as "read"

    Examples:
      | readOrUnread | isOrIsNot |
      | unread       | is        |
      | read         | is not    |

  @C117-F06-29 @AC4.3 @manual
  Scenario: Check messages are sorted by Contact Time with most recent first on Messages page
    Given I log in to the application
    And I open notification with at least "2" web messages with different Contact Times sent to/by logged in user
    When I click on "Messages" in the main navigation bar
    Then I verify the "Messages" list of messages shows messages sorted by "Contact Time" in "descending" order

  @C117-F06-30 @AC4.4 @manual
  Scenario: Check the empty state on Messages page
    Given I log in to the application
    And I open notification with "0" web messages
    When I click on "Messages" in the main navigation bar
    Then I verify the "Messages" page shows empty message "You don't have any messages"

  @C117-F06-31 @AC4.5 @smoke @automated
  Scenario: Check each message fields on the list of messages on Message page
    Given I log in to the application
    When I click the "Messages" page link
    Then I verify each message on the list shows the "sender, subject, date, narrative, relates to, employee" fields


  @C117-F06-32 @AC4.6 @automated @smoke
  Scenario: Check clicking the Relates to link on the Message page opens notification page with that case ID
    Given I log in to the application
    When I click the "Messages" page link
    And I click the "Relates to" hyperlink on the random web message in the list
    Then I should land on "Notification Details" page

  @C117-F06-33 @AC4.7 @automated @smoke
  Scenario: Check clicking the Employee link on the Message page opens Employee page with that name
    Given I log in to the application
    When I click the "Messages" page link
    And I click the "Employee" hyperlink on the random web message in the list
    Then I should land on "Employee Profile" page

  @C117-F06-34 @AC4.8 @manual
  Scenario: Check the sender badge is configurable for employer and carrier on the Messages page
    Given I customize the "Employer" icon configuration for the deployment
    And I customize the "Carrier" icon configuration for the deployment to be different from the Carrier logo
    And I log in to the application
    And I open notification with at least "1" web messages sent by the "user" and at least "1" web messages sent by "Carrier"
    When I click on "Messages" in the main navigation bar
    Then I verify all web messages sent by "user" have a Sender badge icon matching the customized "Employer" icon
    And I verify all web messages sent by "Carrier" have a Sender badge icon matching the customized "Carrier" icon
    And I verify "Sender" badge icon of type "Carrier" does not change the Carrier logo on at the navigation bar

  @C117-F06-35 @AC4.9 @manual @sanity
  Scenario: Check user is able to clearly distinguish between unread and read Web Messages on the Messages page
    Given I log in to the application
    And I open notification with at least "1" read web messages and at least "1" unread web messages sent to/by logged in user
    When I click on "Messages" in the main navigation bar
    Then I verify all "unread" web messages have a highlighted left red line and the Sender badge is colored
    And I verify all "read" web messages have no highlighted left red side bar line and the Sender badge is greyed out

  @C117-F06-36 @AC4.10 @manual
  Scenario: Check the Contact Date is in default format mmmm dd, yyyy on the Messages page
    Given I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    When I click on "Messages" in the main navigation bar
    Then I verify each message has a "Contact Date" field Displayed in the format "mmm dd, yyyy" without the message sent time
    And I verify the configurable Contact Date format matches Notification date format in the Notification summary

  @C117-F06-37 @AC4.10 @manual
  Scenario: Check the Contact Date format is configurable and matches the whole portal format on the Messages page
    Given I customize the date time format configuration for the deployment to be "dd.mm.yyyy"
    And I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    When I click on "Messages" in the main navigation bar
    Then I verify each message has a "Contact Date" field Displayed in the format "dd.mm.yyyy" without the message sent time
    And I verify the configurable Contact Date format matches Notification date format in the Notification summary

  @C117-F06-38 @AC4.11 @manual
  Scenario: Check the web message is marked as unread by clicking mark as unread on Messages page and the badges count increase
    Given I log in to the application
    And I open notification with at least "1" read web message sent to/by logged in user
    And I record the "Messages" navigation menu name badges count
    When I click on "Messages" in the main navigation bar
    Then I verify all "read" web messages have no highlighted left red side bar line and the Sender badge is greyed out
    When I click the message menu on the "1st" "read" web message
    And I click the "Mark as unread" action
    Then I verify that previsouly "read" web message changes to have a highlighted left red line and the Sender badge is colored
    And I verify the "Messages" navigation menu name badge increases by 1

  @C117-F06-39 @AC4.12 @regression @automated
  Scenario: Check filtering of list of web message on the Messages page
    Given I log in to the application
    When I click the "Messages" page link
    And I mark random message as "unread"
    Then I verify the "Showing" dropdown shows option "All" messages
    And I verify "All" messages show in the list
    And I verify the first message is highlighted, opened and contents are displayed
    When I select "Incoming" in the "Showing" dropdown
    Then I verify the "Showing" dropdown shows option "Incoming" messages
    And I verify only "Incoming" messages show in the list
    And I verify the first message is highlighted, opened and contents are displayed
    When I select "Outgoing" in the "Showing" dropdown
    Then I verify the "Showing" dropdown shows option "Outgoing" messages
    And I verify only "Outgoing" messages show in the list
    And I verify the first message is highlighted, opened and contents are displayed
    When I select "Unread" in the "Showing" dropdown
    Then I verify the "Showing" dropdown shows option "Unread" messages
    And I verify only "Unread" messages show in the list
    And I verify the first message is highlighted, opened and contents are displayed
    When I select "All" in the "Showing" dropdown
    Then I verify the "Showing" dropdown shows option "All" messages
    And I verify only "All" messages show in the list
    And I verify the first message is highlighted, opened and contents are displayed


  @C117-F06-40 @AC4.13 @automated @regression
  Scenario: Check sorting of list of web message on the Messages page
    Given I log in to the application
    When I click the "Messages" page link
    Then I verify the "Sorting by" dropdown shows "Newest first" option
    And I verify messages are sorted by "Contact Date Time" in "descending"order
    When I select "Oldest first" option in the "Sorting by" dropdown
    Then I verify the "Sorting by" dropdown shows "Oldest first" option
    And I verify messages are sorted by "Contact Date Time" in "ascending"order
    When I select "Newest first" option in the "Sorting by" dropdown
    Then I verify the "Sorting by" dropdown shows "Newest first" option
    And I verify messages are sorted by "Contact Date Time" in "descending"order

  @C117-F06-41 @AC4.14 @manual @sanity
  Scenario Outline: Check opened messages fields and clicking Relates to hyperlink opens notification case ID on Messages page
    Given I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    When I click on "Messages" in the main navigation bar
    Then I verify "1st" web message in the opened panel shows the "<listMessageFields>" fields with content
    When I click on "2nd" web message in the list of messages
    Then I verify the web message in the opened panel shows the "<listMessageFields>" fields with new content
    When I click the "Relates to" hyperlink on the "2nd" web message in the list
    Then I verify the Notification page opens with the matching recorded notification case ID

    Examples:
      | listMessageFields                                                                            |
      | Sender, Web Message subject, Contact Date with time, Message full text, Relates to hyperlink |

  @C117-F06-42 @AC4.15 @manual
  Scenario Outline: Check reply field and send icon when clicking on message in list of different Sender types
    Given I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    And I click on "Messages" in the main navigation bar
    When I click on web message sent by "<senderType>"
    Then I verify the web message in the opened panel's the "Reply" editable field with placeholder "Reply to this message" is "<visibility>"
    And I verify the web message in the opened panel's Send icon is "<visibility>"

    Examples:
      | senderType | visibility  |
      | Employer   | visible     |
      | Carrier    | not visible |

  @C117-F06-43 @AC4.16 @manual
  Scenario: Check the Reply area can be stretched vertically up and down on the message opened panel on Messages page
    Given I log in to the application
    And I open notification with at least "1" web messages sent to/by logged in user
    When I click on "Messages" in the main navigation bar
    Then I verify the web message opened panel shows the reply stretch button
    When I drag "up" the reply stretch button by "any number of" pixels
    Then I verify the Replay text area height stretches "up" by the "same number of" pixels
    When I drag "down" the reply stretch button by "any number of" pixels
    Then I verify the Replay text area height stretches "down" by the "same number of" pixels

  @C117-F06-44 @AC4.17 @manual
  Scenario: Check click on another message shows confirm popup when 1st message has with at least 5 chars in reply field
    Given I log in to the application
    And I open notification with at least "2" web messages sent to/by logged in user
    And I click on "Messages" in the main navigation bar
    When I click on "1st" web message in the list of messages
    And I enter "12345" in the "Reply message" field
    When I click on "2nd" web message in the list of messages
    Then I verify the confirm "move to another message" popup opens with customizable text from Theme Configurator
    When I click the "No" button
    Then I verify the confirm "move to another message" popup closes
    And I verify "1st" web message in the opened panel shows "Reply message" field shows "12345"
    When I click on "2nd" web message in the list of messages
    Then I verify the confirm "move to another message" popup opens with customizabled text from Theme Configurator
    When I click the "Yes" button
    Then I verify "2nd" web message in the opened panel shows new content
    And I verify the reply web message is not sent to the server
    When I click on "1st" web message in the list of messages
    Then I verify "1st" web message in the opened panel shows "Reply message" field shows ""
    When I enter "1234" in the "Reply message" field
    And I click on "2nd" web message in the list of messages
    Then I verify the confirm "move to another message" popup does not open
    And I verify "2nd" web message in the opened panel shows new content

  @C117-F06-45 @AC4.18 @manual
  Scenario: Check the Send button on New Message popup sends the web message which is automatically marked as read
    Given I log in to the application
    And I open notification at least "1" web messages sent to/by logged in user
    And I click on "Messages" in the main navigation bar
    When I click on "random" web message in the list of messages
    Then I verify the web message in the opened panel with new content
    And I record the "Message subject" in the Subject field
    And I enter "This is a successful reply message" in the "Reply message" field
    When I click the "Send" icon
    Then I verify the web message is sent to the server
    And I verify the list of web messages is refreshed using an animation with the new message highlighted in yellow temporary
    And I verify the new web message has no highlighted left red line and the Sender badge is greyed out
    And I verify the new web message has the Subject "RE: [Message subject]" containing the recorded "Message subject"

  @C117-F06-46 @AC4.18 @manual
  Scenario: Check the Send button does not submit until min chars are entered in the reply msg field on the Messages page
    Given I log in to the application
    And I open notification with at least "1" web messages sent to/by logged in user
    And I click on "Messages" in the main navigation bar
    And I click on "1st" web message in the list of messages
    And I type "4" number of characters in the "Reply message" field
    When I click the "Send" icon
    Then I verify the validation error "Please enter at least 5 characters" shows under the reply message field
    When I type "1" number of characters in the "Reply message" field
    And I click the "Send" icon
    Then I verify the web message is sent to the server


  @C117-F06-47 @AC4.19 @manual
  Scenario Outline: Check handling of max number of characters in reply message field on Messages page
    Given I log in to the application
    And I open notification at least "1" web messages sent to/by logged in user
    And I click on "Messages" in the main navigation bar
    When I click on "random" web message in the list of messages
    Then I verify the web message in the opened panel with new content
    And I verify under the "Reply message" field shows "4000 characters remaining"
    When I "<action>" "<numOfChars>" number of characters in the "Reply message" field
    Then I verify there are the "numOfChars" characters in the "Reply message" field
    And I verify under the "Reply message" field shows "<message>"
    When I click the "Send" icon
    Then I verify the list of web messages "<isOrIsNot>" refreshed using an animation with the new message highlighted in yellow temporary
    And I verify the new web message has no highlighted left red line and the Sender badge is greyed out "<occursOrNotOccurs>"
    And I verify under the "Reply message" field shows "<message>" is "<visibility>"
    And I verify the web message "<isOrIsNot>" sent to the server

    Examples:
      | action | numOfChars | message                     | visibility  | isOrIsNot | occursOrNotOccurs |
      | type   | 10         | 3990 characters remaining   | not visible | is        | occurs            |
      | type   | 4000       | 0 characters remaining      | not visible | is        | occurs            |
      | type   | 4001       | Max 4000 chars length error | visible     | is not    | not occurs        |
      | paste  | 10         | 3990 characters remaining   | not visible | is        | occurs            |
      | paste  | 4000       | 0 characters remaining      | not visible | is        | occurs            |
      | paste  | 4010       | Max 4000 chars length error | visible     | is not    | not occurs        |
