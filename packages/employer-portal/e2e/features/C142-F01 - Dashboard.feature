@employer-portal @C142-F01
Feature: C142-F01 - Dashboard

  @C142-F01-01 @AC1.1 @AC1.2 @manual
    @C142-F03-02 @AC2.1
  Scenario Outline: Dashboard personalisation
    Given I log in to the application
    And the browser cookie "<existsOrNot>"
    And the dashboard personalisation is turned "<onOff>"
    When My Dashboard page is loaded
    Then I see the "Notifications with outstanding requirements" panel
    And I "<seeOrNot>" the "Show/Hide panels" button
    And I "<seeOrNot>" "<panels>" with at most 2 panels per row
    And I verify the order of the displayed other panels fits to the order presented on the "Show/Hide panels" drawer
    And I "<seeOrNot>" backend requests related to "<panels>"

    Examples:
      | existsOrNot   | onOff | seeOrNot  | panels                          |
      | doesn't exist | off   | don't see | any other panels                |
      | doesn't exist | on    | see       | all other panels                |
      | exists        | off   | don't see | any other panels                |
      | exists        | on    | see       | other panels stored by a cookie |

  @C142-F01-02 @AC1.1 @AC1.2 @manual
  Scenario Outline: Dashboard personalisation - stored personalisation cookie
    Given I log in to the application with cookies allowed for the website
    And the dashboard personalisation is turned "on"
    And My Dashboard page is loaded
    When I customize the displayed panels via the "Show/Hide panels" drawer to see "<number>" of panels
    And I click "Save changes" button on "Show/hide panels" drawer
    Then I verify the "Show/hide panels" drawer is closed
    And I verify that the panels are customized
    When I log out the application
    And I see a loading spinner with message "We're logging you out..." for a moment
    And I see popup informing that user was successfully log out
    And I confirm popup by clicking "Take me to login" button
    And I sign out current user on Fineoscloud IDP
    And I log in to the application
    Then I verify that the panels "are" customized
    When I log out the application
    And I see a loading spinner with message "We're logging you out..." for a moment
    And I see popup informing that user was successfully log out
    And I confirm popup by clicking "Take me to login" button
    And I sign out current user on Fineoscloud IDP
    And I clear the browser cookies for the website
    And I log in to the application
    Then I verify that the panels "are not" customized and shows default panels

    Examples:
      | number |
      | 0      |
      | 1      |
      | 2      |

  @C142-F01-03 @AC1.2 @automated @smoke
  Scenario: Dashboard - display default panels
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click the "Show/hide panels" button
    Then the "Show/hide panels" modal opens
    And the "Employees returning to work" switch is enabled
    And the "Notifications created" switch is enabled
    And "Employees returning to work" widget is visible
    And "Notifications created" widget is visible

  @C142-F01-04 @AC1.2 @manual
  Scenario Outline: Edit displayed Dashboard panels - cancellation
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    And My Dashboard page is loaded
    When I customize the displayed panels via the "Show/Hide panels" drawer
    And I click "<cancel>" button on "Show/hide panels" drawer
    Then I verify the "Show/hide panels" drawer is closed
    And I verify that the panels "are not" customized

    Examples:
      | cancel  |
      | close X |
      | Cancel  |

  @C142-F01-05 @AC1.3 @permissions @manual
    @C142-F01-01-01 @AC1
    @C142-F03-01 @AC1.1
  Scenario Outline: Notifications with outstanding requirements - permission to view
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_NOTIFICATIONS"
    And the dashboard personalisation is turned "on"
    When My Dashboard page is loaded
    Then I "<seeOrNot>" Notifications with outstanding requirements panel
    And I "<seeOrNot>" a total number of notifications with outstanding requirements
    And I "<seeOrNot>" a View Details link to open a drawer with the details of outstanding requirements
    And I "<seeOrNot>" a Show/hide panels button
    And I "<seeOrNot>" all other panels

    Examples:
      | withOrWithout | seeOrNot  |
      | with          | see       |
      | without       | don't see |

  @C142-F01-06 @AC1.3 @AC1.4 @AC1.5 @manual @regression @sanity
    @C142-F02-03 @AC3
    @C142-F03-04 @AC.2.2
  Scenario Outline: Graph panels - panels statuses
    Given I log in to the application as a user with "<number>" "<widget>"
    And the dashboard personalisation is turned "on"
    When My Dashboard page is loading
    Then I see a spinner before the "<widget>" panel is loaded
    And I verify a panel with "<widget>" in "<panelState>" is displayed
    And I see a "<message>" text inside the "<widget>" widget

    Examples:
      | widget                                      | number      | panelState      | message                                     |
      | Notifications with outstanding requirements | 0           | empty state     | There are no outstanding notifications      |
      | Notifications with outstanding requirements | more than 0 | non empty state |                                             |
      | Employees expected to return to work        | 0           | empty state     | No employees are expected to return to work |
      | Employees expected to return to work        | more than 0 | non empty state |                                             |
      | Notifications created                       | 0           | empty state     | No notifications were created               |
      | Notifications created                       | more than 0 | non empty state |                                             |
      | View leaves decided                         | 0           | empty state     | No leaves were decided during this period   |
      | View leaves decided                         | more than 0 | non empty state |                                             |
      | Employees approved for leave                | 0           | empty state     | No employees are approved to leave          |
      | Employees approved for leave                | more than 0 | non empty state |                                             |

  @C142-F01-07 @AC1.3 @AC1.4 @AC1.5 @manual @sanity
    @C142-F02-02 @AC2
    @C142-F03-05 @AC2.2
  Scenario Outline: Graph panels - loading error
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page is loading
    And I turn off my network connection
    And An error occurs during loading the "<widget>" panel
    Then I verify the "<widget>" panel shows a Something went wrong content

    Examples:
      | widget                                      |
      | Notifications with outstanding requirements |
      | Employees expected to return to work        |
      | Notifications created                       |
      | Leave Decision Panel                        |
      | Employees approved for leave                |

  @C142-F01-08 @AC1.4 @AC1.5 @manual
    @C142-F02-04 @AC4
    @C142-F03-06 @AC2.2 @AC2.3
  Scenario Outline: Graph panels - panel details display
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page is loaded
    And I open the date range selector in "<widget>" panel
    Then I see the default range is "Today's date"
    When I select a "<dateRange>" in "<widget>" panel
    Then I verify a panel with "<widget>" in "non empty state" is displayed
    And the "<widget>" panel contains a graph
    And the graph shows all the "<widget>" filtered by "<dataType>" in a "<dateRange>" date range
    And the graph data are grouped by "<field>" criteria
    And the graph data don't show more than 6 slices
    And I see a "View Details" link to open the drawer with the "<widget>" details
    And the graph data are ordered by decreasing size
    And the color scheme is consistent

    Examples:
      | dateRange                     | widget                       | dataType                              | field                |
      | Today's date                  | Employees returning to work  | employee expected return to work date | Administration group |
      | Current week (Mon to Sun)     | Employees returning to work  | employee expected return to work date | Administration group |
      | Current month                 | Employees returning to work  | employee expected return to work date | Administration group |
      | Next week (Mon to Sun)        | Employees returning to work  | employee expected return to work date | Administration group |
      | Next month                    | Employees returning to work  | employee expected return to work date | Administration group |
      | Today's date                  | Notifications created        | notification created date             | Disability-related   |
      | Current week                  | Notifications created        | notification created date             | Disability-related   |
      | Current month                 | Notifications created        | notification created date             | Disability-related   |
      | Last week (Mon to Sun)        | Notifications created        | notification created date             | Disability-related   |
      | Last month                    | Notifications created        | notification created date             | Disability-related   |
      | Today's date                  | View leaves decided          | leave decision                        | Decision             |
      | Current week (Mon to Sun)     | View leaves decided          | leave decision                        | Decision             |
      | Last week (Mon to Sun)        | View leaves decided          | leave decision                        | Decision             |
      | Week before last (Mon to Sun) | View leaves decided          | leave decision                        | Decision             |
      | Today's date                  | Employees approved for leave | time approved                         | Reason               |
      | Current week                  | Employees approved for leave | time approved                         | Reason               |
      | Next week (Mon to Sun)        | Employees approved for leave | time approved                         | Reason               |
      | Week after next (Mon to Sun)  | Employees approved for leave | time approved                         | Reason               |

  @C142-F01-09 @AC1.4 @manual @regression
  Scenario Outline: Employees returning to work - graph grouping
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page is loaded
    When I select a "<date range>" in "Employees returning to work" panel
    Then I verify a panel with "Employees returning to work" in "non empty state" is displayed
    And the "Employees returning to work" panel contains a graph
    And the graph data don't show more than 6 slices
    And I "<seeOrNot>" the graph data contains a group called Others

    Examples:
      | date range                                                                                       | seeOrNot  |
      | any range in which employees from 7 or more administration groups are expected to return to work | see       |
      | any range in which employees from 6 or less administration groups are expected to return to work | don't see |

  @C142-F01-10 @AC1.5 @manual
  Scenario Outline: Notifications created - grouping criteria
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page with available notifications is loaded
    And I open the grouping criteria selector in the "Notifications created" panel
    Then I see the default grouping criteria is "Disability-related"
    And I select a "<grouping criteria>" grouping criteria the "Notifications created" panel
    Then I verify the "Notifications created" graph data are grouped by "<grouping criteria>" criteria
    When I click "View Details" to see notifications created list
    Then I verify only notifications with at least one group disability claim subcase are marked as disability-related

    Examples:
      | grouping criteria  |
      | Disability-related |
      | Reason             |

  @C142-F01-11 @AC1.6 @manual
  @C142-F03-09 @AC2.5
  Scenario: Dashboard - graphs colours customisation
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page with available notifications is loaded
    When I customise the "6" extra graph colours palette via theme configurator
    Then I verify the graphs colours are updated accordingly

  @C142-F01-12 @AC2.1 @AC2.2 @AC2.3 @automated @smoke
  Scenario Outline: Notifications with outstanding requirements - drawer display
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And  I click "View details" to see notifications list with outstanding requirements
    Then I see "Notifications with outstanding requirements" drawer is opened
    And I verify the drawer title is a "number of notifications" followed by "notifications with outstanding requirements"
    Then I verify each element of the list contains "<fields>"
    And I verify "Notification Date" is in format "MMM DD, YYYY"

    Examples:
      | fields                                                                                                                                               |
      | Notification ID, Notification Reason, Employee Name - Employee Id, Job Title, Organization Unit, Work site, Notification Creation Date, View Actions |

  @C142-F01-13-01 @AC2.4 @AC3.4 @AC4.4
    @C117-F04-09 @AC9 @smoke @automated
  Scenario Outline: Graph panels - sorting the drawers lists
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View details" on "<widget>" widget
    Then The notifications list is sorted by "<defaultField>" with "ascending" order by default
    When I sort the list by "<field>" with "ascending" order
    Then The notifications list is sorted by "<field>" with "ascending" order
    When I sort the list by "<field>" with "descending" order
    Then The notifications list is sorted by "<field>" with "descending" order

    Examples:
      | widget                                      | field       | defaultField |
      | Notifications with outstanding requirements | Notified on | Notified on  |


  @C142-F01-13-02 @AC2.4 @AC3.4 @AC4.4
    @C117-F04-09 @AC9 @regression @automated
    @C142-F02-13 @AC15
  Scenario Outline: Graph panels - sorting the drawers lists
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "<widget>" date period with available results
    And I click "View details" on "<widget>" widget
    Then The <dataType> list is sorted by "<defaultField>" with "<defaultOrder>" order by default
    When I sort the list by "<field>" with "ascending" order
    Then The <dataType> list is sorted by "<field>" with "ascending" order
    When I sort the list by "<field>" with "descending" order
    Then The <dataType> list is sorted by "<field>" with "descending" order

    Examples:
      | widget                               | field                   | defaultField            | dataType      | defaultOrder |
      | Employees expected to return to work | Employee                | Expected return to work | employees     | ascending    |
      | Employees expected to return to work | Job title               | Expected return to work | employees     | ascending    |
      | Employees expected to return to work | Group                   | Expected return to work | employees     | ascending    |
      | Employees expected to return to work | Expected return to work | Expected return to work | employees     | ascending    |
      | Employees expected to return to work | Case                    | Expected return to work | employees     | ascending    |
      | Notifications created                | Employee                | Creation date           | notifications | ascending    |
      | Notifications created                | Job title               | Creation date           | notifications | ascending    |
      | Notifications created                | Group                   | Creation date           | notifications | ascending    |
      | Notifications created                | Reason                  | Creation date           | notifications | ascending    |
      | Notifications created                | Creation date           | Creation date           | notifications | ascending    |
      | Notifications created                | Case                    | Creation date           | notifications | ascending    |
      | Leaves decided                       | Employee                | Decided on              | leaves        | descending   |
      | Leaves decided                       | Group                   | Decided on              | leaves        | descending   |
      | Leaves decided                       | Reason                  | Decided on              | leaves        | descending   |
      | Leaves decided                       | Decided on              | Decided on              | leaves        | descending   |
      | Leaves decided                       | Case                    | Decided on              | leaves        | descending   |

# @C142-F01-14 @AC2.5 - Notifications with outstanding requirements - drawer filtering - already covered by @C117-F04-07, @C117-F04-08, @C117-F04-08-02, @C117-F04-08-01

  @C142-F01-15-01 @AC2.6 @AC2.7 @AC3.7 @AC3.8 @AC4.7 @AC4.8 @smoke @automated
  Scenario Outline: Graph panels - employee and notification hyperlinks
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View details" on "<widget>" widget
    And I click on the first "<hyperlink>" link on the "Notifications with outstanding requirements" list entry
    Then I should land on "<expectedLandingPage>" page

    Examples:
      | widget                                      | hyperlink                   | expectedLandingPage  |
      | Notifications with outstanding requirements | Employee Name - Employee Id | Employee Profile     |
      | Notifications with outstanding requirements | Notification Id             | Notification Details |

  # BUG FEP-2409
  @C142-F01-15-02 @AC2.6 @AC2.7 @AC3.7 @AC3.8 @AC4.7 @AC4.8 @regression @automated
    @C142-F02-15 @AC18 @AC19
    @C142-F03-18 @AC3.6 @AC3.7
  Scenario Outline: Graph panels - employee and notification hyperlinks
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "<widget>" date period with available results
    And I click "View details" on "<widget>" widget
    And I click on the first "<hyperlink>" link on the "Notifications with outstanding requirements" list entry
    Then I should land on "<expectedLandingPage>" page

    Examples:
      | widget                               | hyperlink                   | expectedLandingPage  |
      | Employees expected to return to work | Employee Name - Employee Id | Employee Profile     |
      | Employees expected to return to work | Notification Id             | Notification Details |
      | Notifications created                | Employee Name - Employee Id | Employee Profile     |
      | Notifications created                | Notification Id             | Notification Details |
      | Leaves decided                       | Employee Name - Employee Id | Employee Profile     |
      | Leaves decided                       | Notification Id             | Notification Details |
      | Employees approved for leave         | Employee Name - Employee Id | Employee Profile     |
      | Employees approved for leave         | Notification Id             | Notification Details |

  @C142-F01-16 @AC2.8 @manual @regression @sanity
  Scenario: Notifications with outstanding requirements - Actions for me (orange icon)
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View Details" to see notifications list with outstanding requirements
    Then I see an "Actions for me" labeled column with an orange circle
    And I verify an orange circle is displayed next to "View actions" caption only in case of any action required from me

# @C142-F01-17 @AC2.9 - Notifications with outstanding requirements - uploading files - already covered by @C117-F04-13

  @C142-F01-18 @AC2.10 @AC3.10 @AC4.9 @manual
    @C142-F03-19 @AC3.8
  # AUTOMATION NOTE: @AC2.10 @AC2.11 - Notifications with outstanding requirements - pagination - already covered by @C117-F04-04, @C117-F04-04-02, @C117-F04-05, @C117-F04-06
  Scenario Outline: Graph panels - pagination
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    When I choose a 10 page size
    And I navigate to page <initialPage> on "<widget>" list
    And I navigate to page <page> on "<widget>" list
    Then I see page number <activePage> is active
    And I see page number <inactivePage> is inactive
    And the number of "<widget>" on current page is between 1 and 10

    Examples:
      | widget                                      | page     | activePage | inactivePage | initialPage |
      | Notifications with outstanding requirements | next     | 2          | 1            | 1           |
      | Notifications with outstanding requirements | previous | 1          | 2            | 2           |
      | Notifications with outstanding requirements | 1        | 1          | 2            | 2           |
      | Notifications with outstanding requirements | 2        | 2          | 1            | 1           |
      | Employees expected to return to work        | next     | 2          | 1            | 1           |
      | Employees expected to return to work        | previous | 1          | 2            | 2           |
      | Employees expected to return to work        | 1        | 1          | 2            | 2           |
      | Employees expected to return to work        | 2        | 2          | 1            | 1           |
      | Notifications created                       | next     | 2          | 1            | 1           |
      | Notifications created                       | previous | 1          | 2            | 2           |
      | Notifications created                       | 1        | 1          | 2            | 2           |
      | Notifications created                       | 2        | 2          | 1            | 1           |
      | Leaves decided                              | next     | 2          | 1            | 1           |
      | Leaves decided                              | previous | 1          | 2            | 2           |
      | Leaves decided                              | 1        | 1          | 2            | 2           |
      | Leaves decided                              | 2        | 2          | 1            | 1           |
      | Employees approved for leave                | next     | 2          | 1            | 1           |
      | Employees approved for leave                | previous | 1          | 2            | 2           |
      | Employees approved for leave                | 1        | 1          | 2            | 2           |
      | Employees approved for leave                | 2        | 2          | 1            | 1           |

  @C142-F01-19 @AC2.11 @AC3.11 @AC4.10 @manual
    @C142-F03-19 @AC3.8
  # AUTOMATION NOTE: @AC2.10 @AC2.11 - Notifications with outstanding requirements - pagination - already covered by @C117-F04-04, @C117-F04-04-02, @C117-F04-05, @C117-F04-06
  Scenario Outline: Graph panels - pagination size
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    When I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    And I should be able to navigate through a list of "<widget>" pages
    When I choose a 50 page size
    Then I should see correct number of pages in paging navigation panel
    When I choose a 100 page size
    Then I should see correct number of pages in paging navigation panel
    When I choose a 20 page size
    Then I should see correct number of pages in paging navigation panel
    When I choose a 10 page size
    Then I should see correct number of pages in paging navigation panel

    Examples:
      | widget                                      |
      | Notifications with outstanding requirements |
      | Employees expected to return to work        |
      | Notifications created                       |
      | Leaves decided                              |
      | Employees approved for leave                |

  @C142-F01-20 @AC2.10 @AC2.11 @AC3.10 @AC3.11 @AC4.9 @AC4.10 @manual
    @C142-F03-19 @AC3.8
  # AUTOMATION NOTE: @AC2.10 @AC2.11 - Notifications with outstanding requirements - pagination - already covered by @C117-F04-04, @C117-F04-04-02, @C117-F04-05, @C117-F04-06
  Scenario Outline: Graph panels - pagination with small list of items
    Given Small number of "<widget>" is available in the system
    And I log in to the application
    When I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    And I can't see navigation panel containing links to other pages

    Examples:
      | widget                                      |
      | Notifications with outstanding requirements |
      | Employees expected to return to work        |
      | Notifications created                       |
      | Leaves decided                              |
      | Employees approved for leave                |

  @C142-F01-21 @AC2.10 @AC2.11 @AC3.10 @AC3.11 @AC4.9 @AC4.10 @manual
    @C142-F03-19 @AC3.8
  # AUTOMATION NOTE: @AC2.10 @AC2.11 - Notifications with outstanding requirements - pagination - already covered by @C117-F04-04, @C117-F04-04-02, @C117-F04-05, @C117-F04-06
  Scenario Outline: Graph panels - pagination disabled navigation
    Given Big number of "<widget>" is available in the system
    And I log in to the application
    When I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    When I navigate to "<page>" on "<widget>" list
    Then navigation panel link to "<target>" is disabled

    Examples:
      | widget                                      | page  | target        |
      | Notifications with outstanding requirements | 2     | current page  |
      | Notifications with outstanding requirements | first | previous page |
      | Notifications with outstanding requirements | last  | next page     |
      | Notifications created                       | 2     | current page  |
      | Notifications created                       | first | previous page |
      | Notifications created                       | last  | next page     |
      | Employees expected to return to work        | 2     | current page  |
      | Employees expected to return to work        | first | previous page |
      | Employees expected to return to work        | last  | next page     |
      | Leaves decided                              | 2     | current page  |
      | Leaves decided                              | first | previous page |
      | Leaves decided                              | last  | next page     |
      | Employees approved for leave                | 2     | current page  |
      | Employees approved for leave                | first | previous page |
      | Employees approved for leave                | last  | next page     |

  @C142-F01-22 @AC2.10 @AC3.9 @AC4.9 @manual @regression
    @C142-F02-16 @AC20
    @C142-F03-20 @AC3.9
  Scenario Outline: Graph panels - download a spreadsheet
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    When I click "Download this list"
    Then I See a "Select file format" pop up
    When I download a spreadsheet in "<format>" format
    Then I verify a file is downloaded in "<format>" format
    And I verify a spreadsheet contains all items from the "<widget>" list
    And I verify that each spreadsheet entry has "<fields>" fields
    And I verify "<dateField>" contains date(s) in format "mmm dd, yyyy"
    And I verify "Employee" is in format "Name + ID"
    And I verify the rows are sorted in "<order>" order by "<field>"

    Examples:
      | widget                                      | format | fields                                                                               | dateField         | order      | field             |
      | Notifications with outstanding requirements | XLS    | Case, Reason, Employee, Job title, Department, Org Unit, Worksite, Notification Date | Notification Date | ascending  | Notification Date |
      | Notifications with outstanding requirements | XLSX   | Case, Reason, Employee, Job title, Department, Org Unit, Worksite, Notification Date | Notification Date | ascending  | Notification Date |
      | Notifications with outstanding requirements | CSV    | Case, Reason, Employee, Job title, Department, Org Unit, Workstie, Notification Date | Notification Date | ascending  | Notification Date |
      | Employees expected to return to work        | XLS    | Employee, Job title, Group, Return Date, Case                                        | Return Date       | ascending  | Return Date       |
      | Employees expected to return to work        | XLSX   | Employee, Job title, Group, Return Date, Case                                        | Return Date       | ascending  | Return Date       |
      | Employees expected to return to work        | CSV    | Employee, Job title, Group, Return Date, Case                                        | Return Date       | ascending  | Return Date       |
      | Notifications created                       | XLS    | Employee, Job title, Group, Reason, Creation Date, Case, Disability Case             | Creation Date     | ascending  | Creation Date     |
      | Notifications created                       | XLSX   | Employee, Job title, Group, Reason, Creation Date, Case, Disability Case             | Creation Date     | ascending  | Creation Date     |
      | Notifications created                       | CSV    | Employee, Job title, Group, Reason, Creation Date, Case, Disability Case             | Creation Date     | ascending  | Creation Date     |
      | View leaves decided                         | XLS    | Employee, Group, Reason, Decided on, Case, Decision                                  | Decided on        | descending | Decided on        |
      | View leaves decided                         | XLSX   | Employee, Group, Reason, Decided on, Case, Decision                                  | Decided on        | descending | Decided on        |
      | View leaves decided                         | CSV    | Employee, Group, Reason, Decided on, Case, Decision                                  | Decided on        | descending | Decided on        |
      | Employees approved for leave                | XLS    | Employee, Group, Case, Reason, Leave type, Time approved <date range>                | Time approved     | ascending  | Employee          |
      | Employees approved for leave                | XLSX   | Employee, Group, Case, Reason, Leave type, Time approved <date range>                | Time approved     | ascending  | Employee          |
      | Employees approved for leave                | CSV    | Employee, Group, Case, Reason, Leave type, Time approved <date range>                | Time approved     | ascending  | Employee          |

  @C142-F01-23 @AC2.12 @AC3.12 @AC4.11 @manual
    @C142-F02-16 @AC20
    @C142-F03-20 @AC3.9
  Scenario Outline: Graph panels - download a spreadsheet cancellation
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    When I click "Download this list"
    Then I See a "Select file format" pop up
    When I click "<button>" button
    Then I see the "Select file format" pop up is closed
    And I am redirected to "<widget>" drawer
    And I verify no file is downloaded

    Examples:
      | widget                                      | button  |
      | Notifications with outstanding requirements | Close X |
      | Notifications with outstanding requirements | Cancel  |
      | Employees expected to return to work        | Close X |
      | Employees expected to return to work        | Cancel  |
      | Notifications created                       | Close X |
      | Notifications created                       | Cancel  |
      | View leaves decided                         | Close X |
      | View leaves decided                         | Cancel  |
      | Employees approved for leave                | Close X |
      | Employees approved for leave                | Cancel  |

  @C142-F01-24-01 @AC2.13 @AC3.13 @AC4.12 @smoke
  Scenario Outline: Graph panels - drawer cancellation
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View details" on "<widget>" widget
    Then I see "Notifications with outstanding requirements" drawer is opened
    When I "<action>" "<widget>" drawer
    Then I see "Notifications with outstanding requirements" drawer is closed
    And My Dashboard page with available notifications is loaded

    @automated
    Examples:
      | widget                                      | action  |
      | Notifications with outstanding requirements | close   |
      | Notifications with outstanding requirements | close X |

  @C142-F01-24-02 @AC2.13 @AC3.13 @AC4.12
   @C142-F03-21 @AC3.10 @smoke
  Scenario Outline: Graph panels - drawer cancellation
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "<period>" on "<widget>" widget
    And I click "View details" on "<widget>" widget
    Then I see "<widget>" drawer is opened
    When I "<action>" "<widget>" drawer
    Then I see "<widget>" drawer is closed
    And My Dashboard page with available notifications is loaded

    @automated
    Examples:
      | widget                               | action  | period    |
      | Employees expected to return to work | close   | next week |
      | Employees expected to return to work | close X | next week |
      | Notifications created                | close   | this week |
      | Notifications created                | close X | this week |
      | Leaves decided                       | close   | this week |
      | Leaves decided                       | close X | this week |
      | Employees approved for leave         | close   |next week  |
      | Employees approved for leave         | close X |next week  |

  @C142-F01-25 @AC3.1 @AC3.2 @AC4.1 @AC4.2
    @C142-F01-28 @AC3.6 @AC4.6
    @C142-F02-09 @AC9 @AC12
    @C142-F03-15 @AC3.2 @AC3.3
  # on the last step expected title content is in lower case - widget value has to be parsed to lower case
  Scenario Outline: Graph panels - view Details, date ranges
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "<filter>" on "<widget>" widget
    And I click "View details" on "<widget>" widget
    Then I see a list of "<widget>" in a "<filter>" date range
    And I see the drawer title has correct text and number of filtered "<dataType>"

    @regression @automated
    Examples:
      | widget                               | dataType      | filter          |
      | Employees expected to return to work | employees     | this week |
      | Employees expected to return to work | employees     | next week       |
      | Employees expected to return to work | employees     | week after next |
      | Notifications created                | notifications | this week       |
      | Notifications created                | notifications | last week |
#      | Leaves decided                  | leaves        | today            |
      | Leaves decided                  | leaves        | this week        |
#      | Leaves decided                  | leaves        | last week        |
#      | Leaves decided                  | leaves        | week before last |
 #     | Employees approved for leave         | employees AFL| today            |
      | Employees approved for leave         | employees AFL| this week        |
      | Employees approved for leave         | employees AFL| next week        |
      | Employees approved for leave         | employees AFL| week after next  |

    @regression @cannotGenerateTestData
    Examples:
      | widget                               | dataType      | filter           |
      | Employees expected to return to work | employees     | week after next  |
      | Employees expected to return to work | employees     | today            |
      | Notifications created                | notifications | week before last |
      | Notifications created                | notifications | today            |


  @C142-F01-26 @AC3.3 @AC4.3 @manual
    @C142-F02-12 @AC13
    @C142-F03-16 @AC3.4
  Scenario Outline: Graph panels - drawers lists details
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View Details" on a "<widget>"
    Then I see a list items contains "<fields>" fields
    And I see that rows have max 2 lines of description in "<columns>"
    And for long descriptions I see ellipsis in the description
    And I verify I can see a full description by the tooltip
    And I verify the title contains the total number of "<widget>"

    Examples:
      | widget                               | fields                                                                                             | columns                                   |
      | Employees expected to return to work | Employee Name - Employee Id, Job title, Group, Return to Work Date, Case Number, View calendar     | Group, Job title                          |
      | Notifications created                | Employee Name - Employee Id, Job title, Group, Reason, Creation Date, Case Number, Disability Case | Group, Reason, Job title                  |
      | View leaves decided                  | Employee Name - Employee Id, Group, Leave reason, Decided on, Case, Decision                       | Group, Reason                             |
      | Employees approved for leave         | Employee name - Employee Id, Group, Case, Reason, Leave type, Time approved <date range>           | Group, Reason, Time approved <date range> |

  @C142-F01-27 @AC3.5 @AC4.5 @automated @regression
#    @C142-F02-14 @AC16 @AC17
  # Filtering by the Reason and Disability case has a form of a multi select, rest is an input field
  Scenario Outline: Graph panels - filtering the drawers lists
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "<widget>" date period with available results
    And I click "View details" on "<widget>" widget
    And I filter the <dataType> by "<field>"
    And I apply "<field>" filter
    Then The <dataType> list is filtered correctly by "<field>" field

    Examples:
      | widget                               | field               | dataType      |
      | Employees expected to return to work | Employee First Name | employees     |
      | Employees expected to return to work | Employee Last Name  | employees     |
      | Employees expected to return to work | Job title           | employees     |
      | Employees expected to return to work | Group               | employees     |
      | Employees expected to return to work | Case                | employees     |
      | Notifications created                | Employee First Name | employees     |
      | Notifications created                | Employee Last Name  | notifications |
      | Notifications created                | Job title           | notifications |
      | Notifications created                | Group               | notifications |
      | Notifications created                | Reason              | notifications |
      | Notifications created                | Case                | notifications |
      | Notifications created                | DI                  | notifications |
      | Leaves decided                       | Employee First Name | leaves        |
      | Leaves decided                       | Employee Last Name  | leaves        |
      | Leaves decided                       | Group               | leaves        |
      | Leaves decided                       | Case                | leaves        |
      | Leaves decided                       | Reason              | leaves        |
      | Leaves decided                       | Decision            | leaves        |

  @C142-F01-29 @AC3.9 @manual @regression
  Scenario Outline: Employees returning to work - calendar details
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View Details" on a "Employees expected to return to work"
    And I click "View calendar" on the "Employees expected to return to work" list entry with a "<type>" leave type in status "<status>"
    Then I see an absence calendar is displayed
    And I see the calendar highlights all time taken related to the notification case
    And I see the highlighted time taken is displayed in "<color>" colors
    And I see the highlighted time taken is displayed in "<gradient type>" gradient type
    And I see the calendar highlights the expected return to work date with a red square around it
    And I see the calendar by default shows a month with the expected return to work date

    Examples:
      | type             | gradient type         | status   | color  |
      | Continuous Time  | solid line            | approved | green  |
      | Reduced Schedule | horizontal split line | declined | red    |
      | Episodic         | striped line          | pending  | yellow |


  @C142-F01-30 @AC3.9 @automated @smoke
  Scenario: Employees returning to work - calendar navigation
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "Employees expected to return to work" date period with available results
    And I click "View details" on "Employees expected to return to work" widget
    And I click random "View Calendar" button
    Then I see an absence calendar is displayed
    And I see the calendar by default shows a month with the expected return to work date
    And I verify I am able to navigate to the "3" "next" months from today’s date
    And I verify I am able to navigate to the "2" "previous" months from today’s date
    And I verify I am able to navigate to the "3" "next" years from today’s date
    And I verify I am able to navigate to the "4" "previous" years from today’s date

  @C142-F01-31 @AC3.9 @manual
  Scenario: Employees returning to work - calendar overlapping periods
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I click "View Details" on a "Employees expected to return to work"
    And I click "View calendar" on the "Employees expected to return to work" list entry
    And The clicked entry has overlapping absence periods in different statuses
    Then I see an absence calendar is displayed
    And I should see the periods displayed in order based on status, starting from top: "Approved, Pending, Denied"

  @C142-F01-32 @manual @regression
    @C142-F02-08 @AC8
    @C142-F03-07 @AC2.2
  Scenario Outline: Graph panels - interactive slices
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    And the "<widget>" widget is filtered by "<filter>"
    When I click a slice of a "<widget>" widget
    Then Then I see "<widget>" drawer is opened
    And I see the "<widget>" data list fits to "<date range>" date range
    And I verify that only items from a clicked slice are displayed on the list

    Examples:
      | widget                               | filter             |
      | Employees expected to return to work |                    |
      | Notifications created                | disability related |
      | Notifications created                | reason             |
      | View leaves decided                  | decision           |
      | View leaves decided                  | reason             |
      | View leaves decided                  | group              |
      | Employess approved for leave         | reason             |
      | Employess approved for leave         | group              |
      | Employess approved for leave         | leave type         |
