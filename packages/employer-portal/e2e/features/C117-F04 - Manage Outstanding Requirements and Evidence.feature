@employer-portal @C117-F04
Feature: C117-F04 - Manage Outstanding Requirements and Evidence

  @C117-F04-01 @AC1 @permissions @manual
  Scenario Outline: User without permissions
    Given Notifications with outstanding requirements "are" available in the system
    When I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_NOTIFICATIONS"
    Then I verify I can see "<view>"

    Examples:
      | withOrWithout | view                                                                            |
      | with          | My Dashboard screen with a panel Notifications with outstanding requirements    |
      | without       | My Dashboard screen without a panel Notifications with outstanding requirements |


  @C117-F04-02 @AC2 @regression @automated
  Scenario: Empty list of notifications with outstanding documents
    Given I log in to the application with "user without notifications"
    Then My Dashboard page without available notifications is loaded

  @FAPI-9736-AC1.1 @FAPI-9707  @regression @manual
  Scenario Outline: Check splash explanatory text on the My Dashboard page
    Given I log in to the application using webapp version "<version>"
    When "<mainPage>" page with available notifications is loaded with showing dashboard is "<showDashboard>"
    Then I "<canOrCannot>" see the splash display title "Welcome to your Total Leave Employer Portal"
    And I "<canOrCannot>" see the splash small title "Leave Management"
    And I can see the Explanatory Text is in the Notifications with outstanding requirements panel
    And the text is "This is a listing of all the cases for which information or documentation needs to be provided – by you, the employee or medical provider"

    Examples:
      | version | canOrCannot | mainPage     | showDashboard |
      | 3.X     | can         | Home         | false         |
      | 4.X     | can         | Home         | false         |
      | 4.X     | cannot      | My Dashboard | true          |


  @FAPI-9736-AC1.3 @manual
  Scenario: Configuring the base text size changes the explanatory text size proportionally
    Given I log in to the application using webapp version
    And I have permission to "URL_EMPLOYERPORTAL_THEMECONFIGURATION"
    When My Dashboard page with available notifications is loaded
    Then I can see the "Explanatory Text" is in the Notifications with outstanding requirements panel
    When I click the "Typography" button on the Theme Configurator bar
    And I set the "Base text" font size "30"
    Then I can see "Explanatory Text" changed proportionally to the base text size

  @C117-F04-03 @AC3 @regression @automated @sanity
  Scenario Outline: Notification details - list item
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then I verify each element of the list contains "<fields>"
    And notifications counter shows correct number of Notifications

    Examples:
      | fields                                                                                                                                 |
      | Notification ID, Notification Reason, Employee Name - Employee Id, Job Title, Organization Unit, Work site, Notification Creation Date |


  @C117-F04-04 @AC4 @AC5 @regression @automated
  Scenario: Notification list pagination - big list of items
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then navigation panel containing links to other pages is shown
    And I should be able to navigate through a list of notification pages
    When I choose a 50 page size
    Then I should see correct number of pages in paging navigation panel
    When I choose a 100 page size
    Then I should see correct number of pages in paging navigation panel
    When I choose a 20 page size
    Then I should see correct number of pages in paging navigation panel
    When I choose a 10 page size
    Then I should see correct number of pages in paging navigation panel


  @C117-F04-04-02 @AC4 @AC5 @regression @automated
  Scenario Outline: Notification list pagination - validating pages navigation
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then navigation panel containing links to other pages is shown
    When I choose a 10 page size
    And I navigate to page <initialPage> on notifications list
    And I navigate to page <page> on notifications list
    Then I see page number <activePage> is active
    And I see page number <inactivePage> is inactive
    And the number of notifications on current page is between 1 and 10

    Examples:
      | page     | activePage | inactivePage | initialPage |
      | next     | 2          | 1            | 1           |
      | previous | 1          | 2            | 2           |
      | 1        | 1          | 2            | 2           |
      | 2        | 2          | 1            | 1           |


  @C117-F04-05 @AC5 @sanity @automated @regression
  Scenario: Notification list pagination - small list of items
    Given I log in to the application with "user with small amount of cases"
    And My Dashboard page with available notifications is loaded
    When I click "View details" to see notifications list with outstanding requirements
    And Small number of notifications with outstanding requirements is available in the system
    Then I can't see navigation panel containing links to other pages


  @C117-F04-06 @AC4 @AC6 @manual
  Scenario Outline: Notification list pagination - disabled navigation
    Given Big number of notifications with outstanding requirements is available in the system
    When I log in to the application
    And My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I navigate to "<page>" on notifications list
    Then navigation panel link to "<target>" is disabled

    Examples:
      | page  | target        |
      | 2     | current page  |
      | first | previous page |
      | last  | next page     |


  @C117-F04-07 @AC7 @AC8 @regression @automated
  @C142-F01-14 @AC2.5
  Scenario Outline: Notification list filtering
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I filter the list of notifications by "<filter>" with test data "<testData>"
    And I apply "<filter>" filter
    Then The notification list field "<filter>" should only contains elements matching following search criteria

    Examples:
      | filter              | testData            |
      | Notification ID     | @C117-F04-07        |
      | Employee First Name | partialEmployeeData |
      | Employee Last Name  | partialEmployeeData |


  @C117-F04-08 @AC8 @AC10 @regression @automated
  @C142-F01-14 @AC2.5
  Scenario Outline: Notification list resetting filter - close filtered results badge
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I filter the list of notifications by "<filter>" with test data "<testData>"
    And I apply "<filter>" filter
    Then I can reset filter by closing the badge
    And notifications counter shows correct number of Notifications

    Examples:
      | filter              | testData            |
      | Notification ID     | @C117-F04-08        |
      | Employee First Name | partialEmployeeData |
      | Employee Last Name  | partialEmployeeData |

    #reset button must be replaced by cancel button
  @C117-F04-08-02 @AC8 @AC10 @regression @automated
  @C142-F01-14 @AC2.5
  Scenario Outline: Notification list cancel filter
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I filter the list of notifications by "<filter>" with test data "<testData>"
    And I apply "<filter>" filter
    And I filter the list of notifications by "<filter>" with test data "<testData>"
    And I cancel "<filter>" filter
    And notifications counter shows correct number of Notifications

    Examples:
      | filter              | testData            |
      | Notification ID     | @C117-F04-08        |
      | Employee First Name | partialEmployeeData |
      | Employee Last Name  | partialEmployeeData |


  @C117-F04-08-01 @AC8 @AC10 @regression @automated
  @C142-F01-14 @AC2.5
  Scenario Outline: Notification list - filter badge numbers
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I filter the list of notifications by "<filter>" with test data "<testData>"
    And I apply "<filter>" filter
    Then I can see badge displaying the correct number of filtered items vs total number of items

    Examples:
      | filter              | testData            |
      | Notification ID     | @C117-F04-08        |
      | Employee First Name | partialEmployeeData |
      | Employee Last Name  | partialEmployeeData |


  # @C117-F04-09 @AC9 - Notifications with outstanding requirements - Graph panels - sorting the drawers lists - covered by @C142-F01-13


  @C117-F04-10 @AC11 @permissions @manual
  Scenario Outline: Checking permissions to see to view outstanding documents
    Given Notifications with outstanding requirements "are" available in the system
    When I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CASES_OUTSTANDINGINFORMATION"
    And My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then I verify that the Show/Hide actions on right side of each notification item is "<visibility>"

    Examples:
      | withOrWithout | visibility  |
      | without       | not visible |
      | with          | visible     |


  @C117-F04-11 @AC12 @sanity @automated @regression
  Scenario: Expanded list of actions
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on "Show Actions" on random notification
    Then I should see a list of outstanding requirements and the number of outstanding items


  @C117-F04-12 @AC13 @permissions @manual
  Scenario Outline: Permissions for uploading item waiting to be provided
    Given Notifications with outstanding requirements "are" available in the system
    And I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CASES_DOCUMENT_BASE64UPLOAD"
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    Then each not-provided item contains "<expectedField>" field
    And I "<canUpload>" upload missing file by clicking on file name

    Examples:
      | withOrWithout | expectedField                       | canUpload |
      | with          | hyperlink with name of the document | can       |
      | without       | name of the document                | can't     |


  @C117-F04-13 @AC13 @regression @automated
  @C142-F01-17 @AC2.9
  Scenario: Upload files popup
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    Then I see "Notifications with outstanding requirements" drawer is opened
    And I click on "Show Actions" on random notification with hyperlink
    And I click on missing file hyperlink
    Then the "Upload Document" popup is visible
    When I upload "cat.png" document
    And I "accept" "Upload Document" pop up
    Then I can see "success" modal
    When I "close" "success" modal
    Then the "success" modal closes
    And I see that outstanding item is marked as provided

  @C117-F04-14 @AC13 @manual
  Scenario: Upload files popup - browse files method
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I click on missing file hyperlink
    And I click the “Browse Files” button on upload popup
    Then I should see a browser window enabling me to select a file to upload
    And I should be able to upload file in supported format


  @C117-F04-15 @AC13 @manual
  Scenario: Upload files popup - drag & drop method
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I click on missing file hyperlink
    And I drag file on the drop area
    Then I should be able to upload file in supported format


  @C117-F04-16 @AC13 @manual
  Scenario: Upload files popup - wrong file extension
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I choose to upload missing file with "not supported" extension
    Then I should see information that file format is not supported
    And I should be able to repeat try with correct file (upload windows stay opened)


  @C117-F04-17 @AC13 @manual
  Scenario: Upload files popup - uploading progress bar
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I choose to upload missing file with "supported" extension
    And I click 'Upload Document' button on upload popup
    Then the upload process will start
    And I should see upload progress on green progress bar

  @C117-F04-18 @AC13 @manual
  Scenario: Successful upload files with automatic verification process
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I upload file with automatic verification process
    Then I should see popup with adding document confirmation
    When I close status popup
    Then Outstanding notification item is refreshed - uploaded item no longer shown
    And if I have permission to URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD, document is marked as read by me


  @C117-F04-19 @AC13 @manual
  Scenario: Successful upload files with manual verification process
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I upload file with manual verification process
    Then I should see popup with adding document confirmation
    And popup message is customizable
    When I close status popup
    Then Outstanding notification item is refreshed - uploaded item shown as provided and awaiting verification
    And if I have permission to URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD, document is marked as read by me


  @C117-F04-20 @AC13 @permissions @manual
  Scenario: Successful upload files - outstanding items list not updated
    Given I log in to the application as a user "without" permissions to "URL_POST_GROUPCLIENT_CASES_OUTSTANDINGINFORMATIONRECEIVED_ADD"
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I upload file
    And the outstanding items list has not been updated
    Then I should see popup telling me that the document has been uploaded but that the outstanding items list has not been updated
    When I close status popup
    Then I verify that uploaded document status stays unchanged in the notification
    And if I have permission to URL_POST_GROUPCLIENT_CASES_DOCUMENT_MARKREAD, document is marked as read by me


  @C117-F04-21 @AC13 @manual
  Scenario: Upload file failed
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I upload file
    And upload process failed
    Then I should see popup telling me that the document could not be uploaded
    And popup message is customizable
    When I close status popup
    Then I verify that uploaded document status stays unchanged in the notification


  @C117-F04-22 @AC13 @manual
  Scenario: Upload files cancel
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I click on missing file hyperlink
    And I choose to upload missing file with "supported" extension
    And I click 'Cancel' button on upload popup
    Then upload popup should be closed
    And I verify that uploaded document status stays unchanged in the notification


  @C117-F04-23 @AC13 @manual
  Scenario: Upload files cancel during upload
    Given I log in to the application
    When My Dashboard page with available notifications is loaded
    And I click "View details" to see notifications list with outstanding requirements
    And I click on Show/Hide actions on random notification
    And I click on missing file hyperlink
    And I choose to upload missing file with "supported" extension
    And I click 'Upload Document' button on upload popup
    And I click 'Cancel' button on upload popup
    Then upload is stopped
    And upload popup should be closed
    And I verify that uploaded document status stays unchanged in the notification


  @C117-F04-24 @smoke @automated
  Scenario Outline: Find test notification - check fields and available actions
    Given I log in to the application
    And I click "View details" to see notifications list with outstanding requirements
    And I filter the list of notifications by "Notification ID" with test data "<testData>"
    And I apply "Notification ID" filter
    Then I verify each element of the list contains "<fields>"
    When I click on Show/Hide actions on notification "<testData>"
    Then I should see a list of outstanding requirements and the number of outstanding items for notification "<testData>"

    Examples:
      | fields                                                                                                                                 | testData          |
      | Notification ID, Notification Reason, Employee Name - Employee Id, Job Title, Organization Unit, Work site, Notification Creation Date | @C117-F04-24-TD-1 |
