@employer-portal @C122-F02C
Feature: C122-F02 - Manage Employee Work Details PART C

  @C122-F02C-01 @AC1.1 @AC1.2 @permissions @manual @regression
  Scenario Outline: Permission to view and edit the employee's Contractual Earnings
    Given I log in to the application as a user "<withOrWithout1>" permmission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS"
    And I have as a user "<withOrWithout2>" permmission to "<permissionEditOrAdd>"
    When I open Employment Details page of user who "<hasOrNot>" defined earnings
    And I click Edit Employment Details button
    And I "<canOrNot1>" view "Earnings, Frequency" values on Edit current occupation pop-up
    And I "<canOrNot2>" edit "Earnings, Frequency" values on Edit current occupation pop-up
    And I click "OK" button on Edit Employment Details pop-up
    Then I verify that "Earnings, Frequency" fields on Employment Details page "<canOrNot2>" be seen to be updated

    Examples:
    | hasOrNot | withOrWithout1 | withOrWithout2 | permissionEditOrAdd                                                         | canOrNot1 | canOrNot2 |
    | has      | with           | with           | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_EDIT | can       | can       |
    | has      | with           | without        | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_EDIT | can       | can't     |
    | has      | without        | with           | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_EDIT | can't     | can't     |
    | has      | without        | without        | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_EDIT | can't     | can't     |
    | hasn't   | with           | with           | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_ADD  | can       | can       |
    | hasn't   | with           | without        | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_ADD  | can       | can't     |
    | hasn't   | without        | with           | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_ADD  | can't     | can't     |
    | hasn't   | without        | without        | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_CONTRACTUALEARNINGS_ADD  | can't     | can't     |

#  There are two other related test cases which were moved to C122-F02 - Manage Employee Work Details PART B
#  to avoid code redundancy:
#  @C122-F02C-03-01 @AC1.4 @AC1.5
#  @C122-F02C-03-02 @AC1.4 @AC1.5
