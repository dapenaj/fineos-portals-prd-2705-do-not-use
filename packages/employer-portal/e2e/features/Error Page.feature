@employer-portal @ErrorPage
Feature: Error Page

  @ErrorPage @automated @smoke @sanity
  Scenario: Display error page
    Given I log in to the application
    And I open error page
    Then the error page web title is displayed
    Then the error page header is displayed
    And the error message title is displayed
    And the error message text is displayed
    And the error image is displayed