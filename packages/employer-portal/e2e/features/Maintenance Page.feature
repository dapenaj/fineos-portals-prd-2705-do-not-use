@employer-portal @MaintenancePage
Feature: Maintenance Page

  @MaintenancePage @manual
  Scenario: Display maintenance page
    Given the application is built
    When I go to the folder "/fineos-portals/packages/employer-portal/maintenance"
    Then I see the maintenance page file "index.html"
    When I edit "maintenance clouds" image src to "./img/maintenance_clouds.svg"> in maintenance page "index.html"
    And I edit "Employer Portal logo" image src to "./img/ER_signature_white.svg"> in maintenance page "index.html"
    And I open "index.html" file in the browser
    Then I see the maintenance page web title "Maintenance"
    And I see the text "Apologies for the inconvenience but we're performing some maintenance at the moment."
    And I see the text "Please return later, we'll be back soon."
    And I see the "Employer Portal logo" image
    And I see the "maintenance clouds" image
