@employer-portal @C122-F02D
Feature: C122-F02 - Manage Employee Work Details PART D

  @C122-F02D-02 @AC1.5 @AC1.6 @regression @automated
  Scenario Outline: Eligibility criteria for leave management layout validation
    Given I log in to the application
    When I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    Then the Employment Details "Eligibility criteria" card should "have" following fields "<fields>"

    Examples:
      | employee                                                             | fields                                                                                                                                                                                                                                                       |
      | with all eligibility criteria for leave management                   | Works within FMLA distance, Works at home, Is a key employee, They've worked a total of, hours in the last 12 months, Their US work state is, They are under the Collective Bargaining Agreement (CBA) of, Their occupation qualifiers are:                  |
      | of non key employee that not works at home and without FMLA distance | Doesn't work within FMLA distance, Doesn't work at home, Isn't a key employee, They've worked a total of, hours in the last 12 months, Their US work state is, They are under the Collective Bargaining Agreement (CBA) of, Their occupation qualifiers are: |

  @C122-F02D-03 @AC1.7 @AC1.8 @AC1.9 @sanity @manual
  Scenario Outline: Not defined employment details validation
    Given I log in to the application
    When I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    Then I "can" see a "<section>" subsection
    And I see a message "<message>" in "<section>" section
    And I see an "Add details" link in "<section>" section

    @cannotGenerateTestData
    Examples:
      | employee                                               | section            | message                                                                               |
      | of employee without any details for Current occupation | Current occupation | We don't have any information about this employee's current occupation in our system. |

    Examples:
      | employee                                                                      | section                                                                       | message                                                                                                      |
      | of employee without any details for Eligibility criteria for leave management | Eligibility criteria for leave management of employee without any details for | We don't have any information about this employee's eligibility criteria for leave management in our system. |

  @C122-F02D-04 @AC1.10 @AC1.11 @permissions @manual
  Scenario Outline: Validating the permissions to view fields in employment details sections
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT"
    And I have no permission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_ADD"
    And I have no permission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_EDIT"
    When I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    Then I "<canOrNot>" see a "<section>" subsection
    And the Employment Details "<section>" card should not contain fields "<fields>"
    When I "<canOrNot>" click "Edit" for "<section>" subsection
    And the Edit "<section>" popup "<isOrNot>" displayed
    Then the Employment Details "<section>" pop-up should contain disabled fields "<fields>"

    Examples:
      | employee                                                                             | canOrNot | section                                   | fields                                                                                                                                                                                                                                               | isOrNot |
      | of employee with all current occupation details                                      | can      | Current occupation                        | Their adjusted date of hire is, Their employment type is:                                                                                                                                                                                            | is      |
      | of key employee works at home and within FMLA distance and other eligibility details | can't    | Eligibility criteria for leave management | Works within FMLA distance, Works at home, Is a key employee, They've worked a total of, hours in the last 12 months, Their work state is, (USA only), They are under the Collective Bargaining Agreement (CBA) of, Their occupation qualifiers are: | isn't   |

  # change agreed with Eoghan - only user with VIEW, ADD and EDIT permissions can edit eligibility criteria
  @C122-F02D-05 @AC1.12 @AC1.13 @permissions @manual
  Scenario Outline: Validating the permissions to add/edit absence employment information
    Given I log in to the application
    And I "<haveOrNotAdd>" a permission to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_ADD"
    And I "<haveOrNotEdit>" a permission to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_EDIT"
    And I have a permission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT"
    When I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    Then I "<canOrNot>" see an "<link>" link in "Eligibility criteria for leave management" section

    Examples:
      | haveOrNotAdd | haveOrNotEdit | employee                                                                             | canOrNot | link         |
      | not have     | not have      | of key employee works at home and within FMLA distance and other eligibility details | can't    | Edit details |
      | have         | not have      | of key employee works at home and within FMLA distance and other eligibility details | can't    | Edit details |
      | not have     | have          | of key employee works at home and within FMLA distance and other eligibility details | can't    | Add details  |
      | have         | have          | of key employee works at home and within FMLA distance and other eligibility details | can      | Add details  |
      | not have     | not have      | of employee without any eligibility details                                          | can't    | Edit details |
      | have         | not have      | of employee without any eligibility details                                          | can't    | Edit details |
      | not have     | have          | of employee without any eligibility details                                          | can't    | Add details  |
      | have         | have          | of employee without any eligibility details                                          | can      | Add details  |

  # change agreed with Eoghan - only user with VIEW, ADD and EDIT permissions can edit eligibility criteria
  @C122-F02D-07 @AC2.2 @AC2.3 @permissions @manual
  Scenario Outline: Edit current occupation permissions validation
    Given I log in to the application
    And I "<haveOrNotAdd>" a permission to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_ADD"
    And I "<haveOrNotEdit>" a permission to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT_EDIT"
    And I have a permission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_ABSENCEEMPLOYMENT"
    And I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    When I click the "Edit" link under the "Current occupation" section
    Then the Edit "Current occupation" popup opens
    And I "<canOrNot>" edit "Employment type" field
    And I "<canOrNot>" edit "Adjusted date of hire" field

    Examples:
      | haveOrNotAdd | haveOrNotEdit | employee                                        | canOrNot |
      | not have     | not have      | of employee with all current occupation details | can't    |
      | have         | not have      | of employee with all current occupation details | can't    |
      | not have     | have          | of employee with all current occupation details | can't    |
      | have         | have          | of employee with all current occupation details | can      |
      | not have     | not have      | of employee without any eligibility details     | can't    |
      | have         | not have      | of employee without any eligibility details     | can't    |
      | not have     | have          | of employee without any eligibility details     | can't    |
      | have         | have          | of employee without any eligibility details     | can      |

  @C122-F02D-08 @AC2.4 @automated @regression
  Scenario Outline: Adjusted date of hire visibility
    Given I log in to the application
    And I open Employee details page "via the portal email" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Current occupation" section
    And the Edit "Current occupation" popup opens
    When I switch "<onOff>" add adjusted date of hire
    Then I "<canOrNot>" see "Adjusted date of hire" field

    Examples:
      | onOff | canOrNot |
      | on    | can      |
      | off   | canNot   |

  @C122-F02D-09 @AC2.5 @AC2.6 @AC2.7 @sanity @automated @smoke
  Scenario: Edit current occupation help icons validation
    Given I log in to the application
    And I open Employee details page "with all current occupation details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Current occupation" section
    And the Edit "Current occupation" popup opens
    When I hover a help icon next to "Job start date" field
    Then I see a popover with customizable text from the theme configurator with information regarding "Job start date"
    When I hover a help icon next to "Add adjusted date of hire" field
    Then I see a popover with customizable text from the theme configurator with information regarding "Adjusted date of hire"
    When I hover a help icon next to "Job end date" field
    Then I see a popover with customizable text from the theme configurator with information regarding "Job end date"

  @C122-F02D-10 @AC2.8 @AC2.9 @AC2.10 @automated @smoke
  Scenario: Adjusted date of hire dates validation
    Given I log in to the application
    And I open Employee details page "not subscribed to alerts via email" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Current occupation" section
    And the Edit "Current occupation" popup opens
    And I switch "on" add adjusted date of hire
    And I enter a future date after today to "Adjusted date of hire" field
    Then I see the message Adjusted date of hire "afterToday" warning
    When I enter a date after the "Job end date" to "Adjusted date of hire" field
    Then I see the message Adjusted date of hire "afterJobEndDate" warning

  @C122-F02D-11 @AC2.12 @manual
  Scenario: Edit Current occupation OK button with no data change validation
    Given I log in to the application
    And I open Employee details page "of employee with all current occupation details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Current occupation" section
    And the Edit "Current occupation" popup opens
    When I click the OK button
    Then no API call is send
    And the 'Edit Current occupation' popup closes
    And I see the "Current occupation" section with unchanged data

  @C122-F02D-12 @AC2.13 @manual
  Scenario Outline: Edit Current occupation Cancel button test
    Given I log in to the application
    And I open Employee details page "of employee with all current occupation details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Current occupation" section
    And the Edit "current occupation" popup opens
    When I edit values for the available fields
    And I "<button>" 'Edit current occupation' popup
    Then no API call is send
    And the 'Edit Current occupation' popup closes
    And I see the "Current occupation" section with unchanged data

    Examples:
      | button  |
      | cancel  |
      | close X |

  @C122-F02D-13 @AC2.14 @AC2.15 @manual @regression
  Scenario Outline: Edit current occupation save failure validation
    Given I log in to the application
    And I open Employee details page "of employee with all current occupation details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Current occupation" section
    And the Edit "Current occupation" popup opens
    When I edit values for the available fields
    And I click the OK button
    And "<oneOrAll>" of the called APIs fails because of "<error>"
    Then I see the "<error message>" pop-up
    When I click "<button>" on the pop-up
    Then the 'Edit Current occupation' popup is "<closedOrNot>"
    And I see "<view>" data "<areOrNot>" refreshed

    Examples:
      | oneOrAll | error                        | error message                                                                                                  | button  | closedOrNot | view                           | areOrNot |
      | one      | concurrency error            | standard concurrency error                                                                                     | Refresh | not closed  | Edit Current occupation pop-up | are      |
      | one      | error other than concurrency | We were not able to update all the information that you requested to change. Please try again in a few minutes | Close   | closed      | Current occupation section     | are      |
      | all      | error other than concurrency | It's our fault, not yours. Please try again in a few minutes.                                                  | Close   | not closed  | Edit Current occupation pop-up | are not  |

  @C122-F02D-14 @AC3.1 @AC3.2 @AC3.3 @manual
  Scenario Outline: Add/Edit eligibility criteria for leave management pop-up layout validation
    Given I log in to the application
    And I open Employee details page "<employee>" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    When I click the "<addOrEdit>" link under the "Eligibility criteria for leave management" section
    Then the "<addOrEdit>" "Eligibility criteria for leave management" popup is displayed
    And I see following fields "Works within FMLA radius, Works at home, Key employee, Actual hours worked in last 12 months, Work state (USA only), Collective Bargaining Agreement (CBA)"

    Examples:
      | employee                                                                             | addOrEdit |
      | of key employee works at home and within FMLA distance and other eligibility details | Edit      |
      | of employee without any eligibility details                                          | Add       |

  @C122-F02D-15 @AC3.4 @sanity @manual
  Scenario: Edit eligibility criteria for leave management help icons validation
    Given I log in to the application
    And I open Employee details page "of key employee works at home and within FMLA distance and other eligibility details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Eligibility criteria for leave management" section
    And the "Edit" "Eligibility criteria for leave management" popup is displayed
    When I hover over the help icon next to "Works within FMLA radius" field
    Then I see a popover with a customizable text from the theme configurator "To be eligible for Family and Medical Leave Act (FMLA) leave, an employee must work at a location that has 50 employees within a 75-mile radius."

  @C122-F02D-16 @AC3.6 @manual
  Scenario: Clear CBA field validation
    Given I log in to the application
    And I open Employee details page "of key employee works at home and within FMLA distance and other eligibility details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Eligibility criteria for leave management" section
    And the "Edit" "Eligibility criteria for leave management" popup is displayed
    When I click X icon next to populated CBA field value
    Then I see a CBA field value is replaced with a placeholder text "None"

  @C122-F02D-17 @AC3.7 @regression @automated
  Scenario: Ability to edit and save Eligibility criteria for leave management
    Given I log in to the application
    And I open Employee details page "withNotifications" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    When I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I randomly "check/uncheck" "Within FMLA radius" checkbox
    And I randomly "check/uncheck" "Works at home" checkbox
    And I randomly "check/uncheck" "Key employee" checkbox
    And I enter random test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter random test data in the 'Eligibility' popup "CBA" field
    And I choose random "Work state"
    And I "accept" 'Edit Eligibility criteria' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section

  @C122-F02D-18 @AC3.8 @manual
  Scenario: Edit eligibility criteria for leave management OK button with no changes validation
    Given I log in to the application
    And I open Employee details page "of key employee works at home and within FMLA distance and other eligibility details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Eligibility criteria for leave management" section
    And the Edit "Eligibility criteria for leave management" popup is displayed
    When I click the OK button
    Then no API call is send
    And the 'Eligibility criteria for leave management' popup closes
    And I see "Eligibility criteria for leave management" section with unchanged data

  @C122-F02D-19 @AC3.9 @manual
  Scenario Outline: Edit eligibility criteria for leave management Cancel button test
    Given I log in to the application
    And I open Employee details page "of key employee works at home and within FMLA distance and other eligibility details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Eligibility criteria for leave management" section
    And the Edit "Eligibility criteria for leave management" popup is displayed
    When I edit values for the available fields
    And I "<button>" 'Eligibility criteria for leave management' popup
    Then the 'Eligibility criteria for leave management' popup closes
    And no API call is send
    And I see "Eligibility criteria for leave management" section with unchanged data

    Examples:
      | button  |
      | cancel  |
      | close X |

  @C122-F02D-20 @manual @regression
  Scenario Outline: Concurrency error handling for eligibility criteria for leave management
    Given I log in to the application
    When I open Employee details page "of key employee works at home and within FMLA distance and other eligibility details" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I click the "Edit" link under the "Eligibility criteria for leave management" section
    Then the Edit "Eligibility criteria for leave management" popup opens
    When I edit values for the available fields
    And I click the OK button
    And The called API fails because of "<error>"
    Then I see the "<error message>" pop-up
    When I click "<button>" on the pop-up
    Then the 'Edit eligibility criteria for leave management' popup is "not closed"
    And I see "Edit Current occupation pop-up" data are not refreshed

    Examples:
      | error                        | error message                                                 | button  | 
      | error other than concurrency | It's our fault, not yours. Please try again in a few minutes. | Close   | 
