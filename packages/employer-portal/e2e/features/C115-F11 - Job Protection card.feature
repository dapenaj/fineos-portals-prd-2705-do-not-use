@employer-portal @C115-F11
Feature: C115-F11 - Job Protection card

  @C115-F11-01 @job-protection @permissions @AC1.2 @smoke @automated @sanity
  Scenario: Job Protection card - user with permissions
    Given I log in to the application
    When I open notification with "Accident or treatment required for an injury" and test data "@C115-F11-01-TD-1"
    Then I verify that the 'Job Protected Leave' card is "visible"


  @C115-F11-02 @job-protection @permissions @AC1.2 @manual
  Scenario: Job Protection card - user without permissions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS"
    When I open notification with "child absence cases"
    Then I verify that the 'Job Protected Leave' card is "not visible"


  @C115-F11-03 @job-protection @AC1.1 @automated @smoke
  Scenario Outline: Job Protection card - notification with no child absence cases
    Given I log in to the application
    When I open notification with "<case>"
    Then I verify that the 'Job Protected Leave' card is "<visibility>"

    Examples:
      | case                             | visibility  |
      | Permanent Accommodation Cases    | not visible |
      | Incomplete Intake (no sub-cases) | not visible |
      | Child Absence Cases              | visible     |
      | GDC Without Leave Cases          | not visible |
      | Mixed Accommodation Cases        | not visible |

  @C115-F11-04 @job-protection @AC2.1 @regression @cannotGenerateTestData
  Scenario: Job Protection card - leave plans organisation
    Given I log in to the application
    When I open notification with "Absence with multiple Leave Plans"
    Then I verify that the periods are organized by Leave Plan grouping
    And leaves within each Leave Plan are sorted by Start Date then by the Absence Reason


  @C115-F11-05 @job-protection @AC2.2 @AC3.2 @AC3.3 @AC3.5 @regression @automated
  Scenario Outline: Job Protection card - empty state - no plans
    Given I log in to the application
    When I open notification with "Child leaves with no applicable Leave Plan"
    Then I should see job protection card in empty state with leaves list
    And leaves without Leave Plan are sorted by Decision Status
    And each leave with end date "<endDateAvailability>" should be formatted as follows "<textFormat>"
    And I can customize messages on no leave plans section

    Examples:
      | endDateAvailability | textFormat                                                      |
      | available           | -AbsenceReason- starting on -StartDate- and ending on -EndDate- |


  @C115-F11-06 @job-protection @AC2.3 @regression @automated
  Scenario: Job Protection card - basic merging leaves
    Given I log in to the application
    When I open notification with "consecutive child leaves with same Leave Plan Name, Absence Reason, Absence Period Type and Status Category" and test data "@C115-F11-06"
    Then I verify that the periods are merged into one leave
    And start date is the start date of the first period and end date is the end date of the last period


  @C115-F11-07 @job-protection @AC3.1 @AC3.3 @AC3.5 @manual
  Scenario Outline: Job Protection card - empty state - assessment ongoing
    Given I log in to the application
    When I open notification with "child absence assessment ongoing"
    Then I should see job protection card in empty state with assessment details
    And each leave with end date "<endDateAvailability>" should be formatted as follows "<textFormat>"
    And I can customize assessment ongoing messages

    Examples:
      | endDateAvailability | textFormat                                                      |
      | available           | -AbsenceReason- starting on -StartDate- and ending on -EndDate- |


  @C115-F11-08 @job-protection @AC3.4 @AC3.5 @manual
  Scenario: Job Protection card - empty state - alternative view
    Given I log in to the application
    When I open notification with "child leaves with no periods defined"
    Then I should see job protection card in alternative empty state
    And I can customize alternative empty state messages


  @C115-F11-09 @job-protection @AC4.1 @AC4.2 @smoke @automated
  Scenario: Job Protection card - Leave Plans collapsed view
    Given I log in to the application
    When I open notification with "Accident or treatment required for an injury" and test data "@C115-F11-09-TD-1"
    Then I "can" see list of collapsed items in "Job Protected Leave" card
    And each collapsed case on 'Job Protected Leave' card has "Name, Status, Graphical Status Indicator" fields
    And I can see a 'Job Protected Leave' card legend presenting graphical patterns: "Continuous Time, Intermittent Time, Reduced Schedule"


  @C115-F11-11 @job-protection @AC4.3 @AC4.4 @AC4.5 @regression @automated
  Scenario: Job Protection card - Paid Leave Plan Indicator popover
    Given I log in to the application
    When I open notification with "child leave plan with wage replacement entitlement" and test data "@C115-F11-11"
    Then I can see the Paid Leave Plan Indicator next to plan name
    When I hover on the Paid Leave Plan Indicator
    Then I should see popover with message: "This leave plan might also entitle you to wage replacement"
    And I can customize message on Paid Leave Plan Indicator popover


  @C115-F11-12 @job-protection @AC5.1 @AC5.2 @AC5.3 @AC5.4 @AC5.5 @AC5.6 @automated @sanity
  Scenario Outline: Job Protection card - Leave Plan details for pending periods
    Given I log in to the application
    When I open notification with "Pending child leave periods of type <leavePeriodType>" and test data "@C115-F11-12"
    Then I see Job Protected Leave period of type “<leavePeriodType>" has “Absence reason, <fields>" fields
    And I see Job Protected Leave period of type “<leavePeriodType>" has 'status text' and 'graphical status indicator'
    And I see first expanded "Job Protected Leave" item has "Managed by, Phone number" details

    @smoke
    Examples:
      | leavePeriodType | fields                          |
      | continuous time | Leave begins, Requested through |

    @regression
    Examples:
      | leavePeriodType   | fields                           |
      | reduced schedule  | Period begins, Requested through |
      | intermittent time | Requested between                |


  @C115-F11-13 @job-protection @AC5.1 @AC5.2 @AC5.3 @AC5.4 @AC5.5 @AC5.6 @regression @automated
  Scenario Outline: Job Protection card - Leave Plan details for approved periods
    Given I log in to the application
    When I open notification with "Approved child leave periods of type <leavePeriodType>" and test data "@C115-F11-13"
    Then I see Job Protected Leave period of type “<leavePeriodType>" has “Absence reason, <fields>" fields
    And I see Job Protected Leave period of type “<leavePeriodType>" has 'status text' and 'graphical status indicator'
    And I see first expanded "Job Protected Leave" item has "Managed by, Phone number" details

    Examples:
      | leavePeriodType   | fields                          |
      | continuous time   | Leave begins, Approved through  |
      | reduced schedule  | Period begins, Approved through |
      | intermittent time | Approved between                |


  @C115-F11-14 @job-protection @AC5.2 @AC5.7 @regression @automated
  Scenario Outline: Job Protection card - Leave period status
    Given I log in to the application
    When I open notification with "child leave plan containing leave period of type <leavePeriodType> and status <status>" and test data "<testData>"
    Then status bar at the side of "<status>" leave section has "<color>" color and gradient indicator for "<leavePeriodType>" period type
    And the status indicator of "<status>" "<leavePeriodType>" leave has "<color>" color

    Examples:
      | status    | color  | leavePeriodType   | testData        |
      | Approved  | Green  | reduced schedule  | @C115-F11-14-01 |
      | Pending   | Yellow | intermittent time | @C115-F11-14-02 |
      | Denied    | Red    | continuous time   | @C115-F11-14-03 |
      | Cancelled | Grey   | continuous time   | @C115-F11-14-04 |


  @C115-F11-15 @job-protection @AC2.3 @manual
  Scenario Outline: Job Protection card - Status mapping
    Given I log in to the application
    When I open notification with "child leave plan containing leave period with <decisionStatus>, <adjudicationStatus>, <timeDecisionStatus>, <PeriodStatus>"
    And I expand Leave Plan
    Then Status Indicator displays "<mappedStatus>" status

    Examples:
      | decisionStatus | adjudicationStatus | timeDecisionStatus    | PeriodStatus | mappedStatus |
      | Pending        | -                  | -                     | -            | Pending      |
      | Projected      | -                  | -                     | -            | Pending      |
      | In Review      | -                  | -                     | -            | Pending      |
      | Cancelled      | -                  | -                     | -            | Cancelled    |
      | Denied         | -                  | -                     | -            | Denied       |
      | Approved       | Rejected           | -                     | -            | Denied       |
      | Approved       | Accepted           | Pending               | -            | Pending      |
      | Approved       | Accepted           | Pending Certification | -            | Pending      |
      | Approved       | Accepted           | Approved              | -            | Approved     |
      | Approved       | Accepted           | Time Available        | -            | Approved     |
      | Approved       | Accepted           | Certified             | -            | Approved     |
      | Approved       | Accepted           | Denied                | -            | Denied       |
      | Approved       | Accepted           |                       | Cancelled    | Cancelled    |
      | Approved       | Accepted           |                       | Episodic     | Approved     |


  @C115-F11-16 @job-protection @AC2.3 @manual
  Scenario Outline: Job Protection card - Status mapping depending on adjacent periods
    Given I log in to the application
    When I open notification with "child leave plan containing approved leave period with not Cancelled or Episodic periodStatus and adjacent periods with <kind> Status Category, Period Type, Reason and Plan Name"
    And I expand Leave Plan
    Then the period in the middle is "<action>"

    Examples:
      | kind  | action                       |
      | same  | merged with adjacent periods |
      | other | deleted                      |


  @C115-F11-17 @job-protection @AC2.3 @manual
  Scenario Outline: Job Protection card - impossible merge
    Given I log in to the application
    When I open notification with consecutive child leaves and "<case>"
    And I expand Leave Plan
    Then I verify that the periods are "not merged"

    Examples:
      | case                  |
      | mixed period types    |
      | mixed status category |
      | mixed Reason name     |
      | mixed leave Plan name |


  @C115-F11-18 @job-protection @AC2.3 @manual
  Scenario: Job Protection card - merge of month leave
    Given I log in to the application
    When I open notification with approved monthly child leave plan
    And I expand Leave Plan
    Then I verify that the atomic periods are merged into one leave
