@employer-portal @HighContrastMode
Feature: High Contrast Mode

  @HighContrastMode-01 @automated @smoke
  Scenario Outline: Test toggle on/off high contrast mode and close the high contrast mode menu - click to toggle
    Given I log in to the application with "HR admin user"
    And set high contrast mode is "<highContrastMode1>" if not set
    When I re-login the application with "HR admin user"
    Then My Dashboard page with available notifications is loaded
    And I see the accessibility icon visible on the right side of the Top Bar
    And I see a total number of notifications with outstanding requirements with high contrast color "<highContrastMode1>"
    When I click high contrast mode icon
    Then The high contrast mode modal opens
    And The high contrast mode switch is "<highContrastMode1>"
    When I turn "<highContrastMode2>" high contrast mode using mouse click
    Then The high contrast mode switch is "<highContrastMode2>"
    And I see a total number of notifications with outstanding requirements with high contrast color "<highContrastMode2>"
    When I click high contrast mode icon
    Then The high contrast mode modal closes

    Examples:
      | highContrastMode1 | highContrastMode2 |
      | off               | on                |
      | on                | off               |


  @HighContrastMode-02 @automated @smoke
  Scenario Outline: Test toggle on/off high contrast mode and close the high contrast mode menu - tab and enter to toggle
    Given I log in to the application with "HR admin user"
    And set high contrast mode is "<highContrastMode1>" if not set
    When I re-login the application with "HR admin user"
    Then My Dashboard page with available notifications is loaded
    And I see the accessibility icon visible on the right side of the Top Bar
    And I see a total number of notifications with outstanding requirements with high contrast color "<highContrastMode1>"
    When I tab to high contrast mode icon and press enter
    Then The high contrast mode modal opens
    And The high contrast mode switch is "<highContrastMode1>"
    When I turn "<highContrastMode2>" high contrast mode using enter
    Then The high contrast mode switch is "<highContrastMode2>"
    And I see a total number of notifications with outstanding requirements with high contrast color "<highContrastMode2>"
    When I press escape button
    Then The high contrast mode modal closes

    Examples:
      | highContrastMode1 | highContrastMode2 |
      | off               | on                |
      | on                | off               |


  @HighContrastMode-03 @automated @regression
  Scenario: Test that users high contrast mode settings are independent of each other
    Given I log in to the application with "HR test user"
    And set high contrast mode is "off" if not set
    When I re-login the application with "HR admin user"
    And I click high contrast mode icon
    Then The high contrast mode modal opens
    When I turn "on" high contrast mode using mouse click
    Then The high contrast mode switch is "on"
    And I see a total number of notifications with outstanding requirements with high contrast color "on"
    When I re-login the application with "HR test user"
    And I click high contrast mode icon
    Then The high contrast mode modal opens
    And The high contrast mode switch is "off"
    And I see a total number of notifications with outstanding requirements with high contrast color "off"
    When I re-login the application with "HR admin user"
    And I click high contrast mode icon
    Then The high contrast mode modal opens
    And The high contrast mode switch is "on"
    And I see a total number of notifications with outstanding requirements with high contrast color "on"
    And I turn "off" high contrast mode using mouse click


  @HighContrastMode-04 @manual
  Scenario: Test configuring high contrast mode in the Theme Configurator and exporting the file
    Given I log in to the application using webapp version
    And I have permission to "URL_EMPLOYERPORTAL_THEMECONFIGURATION"
    And My Dashboard page is loaded
    When I "click" the accessibility icon
    Then the high contrast menu opens
    When I switch "off" the high contrast mode toggle
    And I click the "Colors" button on the Theme Configurator bar
    And I set the primary color to "blue"
    And I record the "blue" hex value
    Then I see the Top Bar is "blue"
    When I "click" the accessibility icon
    And the high contrast menu opens
    And I switch "on" the high contrast mode toggle
    And I click the "Colors" button on the Theme Configurator bar
    And I set the primary color to "green"
    And I record the "green" hex value
    Then I see the Top Bar is "green"
    When I "click" the accessibility icon
    And the high contrast menu opens
    And I switch "off" the high contrast mode toggle
    Then I see the Top Bar is "blue"
    When I click the "Export" button on the Theme Configurator bar
    And I open the theme zip file
    And I open the client-config.json in the config folder
    Then I see the theme var(--theme-color-primaryColor) is the recorded "blue" hex value
    And I see the highContrastMode var(--theme-color-primaryColor) is the recorded "green" hex value
    