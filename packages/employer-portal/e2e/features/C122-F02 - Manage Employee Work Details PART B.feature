@employer-portal @C122-F02B
Feature: C122-F02 - Manage Employee Work Details PART B


  @C122-F02B-01 @AC1.1 @permissions @manual
  Scenario Outline: Permission to edit the employee’s Employment Details
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_EDIT"
    And I open Employee Details page
    When I click on Employment Details tab
    Then I can see the Edit link is "<visibility>" on the Employment Details section

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @smoke @automated @sanity
    @C122-F02B-02 @AC1.2
    @C122-F02C-02 @AC1.3
  Scenario Outline: Click edit link to open edit Current occupation popup
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    And the 'Edit Current occupation' popup contains the editable fields "<requiredFields>, <notRequiredFields>"
    And the 'Edit Current occupation' popup labels "have" '*' on the fields "<requiredFields>"
    And the 'Edit Current occupation' popup labels "do not have" '*' on the fields "<notRequiredFields>"
    And I verify "Earnings frequency" select has following "Please select,per week,per month,per year" options

    Examples:
      | requiredFields                                   | notRequiredFields                                                      |
      | Job title, Job start date, Hours worked per week | Employment type, earnings, Full or part time, Job end date |


  @C122-F02B-03 @AC1.3 @manual
  Scenario: Click on Occupation category to see a list of all available work categories
    Given I log in to the application
    And I open Employee Details page
    And I click on Employment Details tab
    When I click the Edit link
    Then the Edit Employment Details popup opens
    When I click on "Occupation category" field
    Then the dropdown should contain a list of all available work categories from "/enum-domains/{enumDomainId}/enum-instances"

  @smoke @automated @sanity
  @C122-F02B-04 @AC1.4
  @C122-F02C-03-01 @AC1.4 @AC1.5
  @C122-F02D-06 @AC2.1 @AC2.11
  Scenario: Click OK on the Current occupation popup with all fields
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    And I see 'Current occupation' data in edit mode is the same like in display mode
    When I enter random test data in the 'Current occupation' popup "Job title" field
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    And I enter random test data in the 'Current occupation' popup "Job end date" field
    And I enter random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose random test data in the "Employment type" select field
    And I choose random test data in the "Full/part time" select field
    And I choose random test data in the "Earnings frequency" select field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I "check" "Adjusted date of hire" switch
    And I enter random test data in the 'Current occupation' popup "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field


  @C122-F02B-05 @AC1.4 @regression @automated
  Scenario: OK button is disabled on the Current occupation popup for empty required field values
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    When I click the Edit link
    Then the 'Edit Current occupation' popup opens
    And I see the OK button is enabled
    When I clear the "Job title" field
    Then I see the OK button is enabled
    When I enter random test data in the 'Current occupation' popup "Job title" field
    Then I see the OK button is enabled
    When I clear the "Job start date" field
    Then I see the OK button is enabled
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    Then I see the OK button is enabled
    When I "accept" 'Edit Current occupation' popup
    And the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field


  @C122-F02B-06 @AC1.4 @regression @automated
  Scenario: OK button is enabled on the Current occupation popup for invalid field values
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    When I click the Edit link
    Then the 'Edit Current occupation' popup opens
    And I see the OK button is enabled
    When I enter "-10" in the "Hours worked per week" field
    Then I see the OK button is enabled
    And I see an invalid number error message for the "Hours worked per week" field
    When I enter "badtext" in the "Hours worked per week" field
    Then I see the OK button is enabled
    And I see an invalid non-numeric error message for the "Hours worked per week" field
    When I enter "-10" in the "Earnings" field
    Then I see an Value must be greater than zero error message for the "Contractual earnings" field
    When I enter "badtest" in the "Earnings" field
    Then I see an invalid non-numeric error message for the "Contractual earnings" field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I do not see an invalid data error message for the "Contractual earnings" field
    When I enter "50.627" in the "Hours worked per week" field
    Then I see the OK button is enabled
    And I do not see an invalid data error message for the "Hours worked per week" field
    When I enter "50.62" in the "Hours worked per week" field
    Then I see the OK button is enabled
    And I do not see an invalid data error message for the "Hours worked per week" field
    When I "accept" 'Edit Current occupation' popup
    And the 'Edit Current occupation' popup closes
    And I see on the Employment Details tab "50.6" in the "Their average hours worked per week are:" field

  @C122-F02B-07 @AC1.4 @manual
  Scenario Outline: Click OK on the Edit Employment Details popup with required fields with existing employment end date
    Given I log in to the application
    And I open Employee Details page
    And I click on Employment Details tab
    And I record the field values on the Employment Details tab for comparison
    And I click the Edit link
    And the Edit Employment Details popup opens
    And I see the "Job title" field matches the Employment Details tab "Job title" field recorded value
    And I see the "Job start date" field matches the Employment Details tab "Date of hire" field recorded value
    And I see the "Job end date" field matches the Employment Details tab "Employment End date" field recorded value
    And I see the "Occupation category" field matches the Employment Details tab "Work category" field recorded value
    And I see the "Hours worked per week" field matches the Employment Details tab "Hours worked per week" field recorded value
    And I enter "<jobTitle>" in the "Job title" field
    And I select "<jobStartDate>" in the "Job start date" field
    When I click the OK button
    Then the input values are sent to the carrier
    And the Edit Employment Details popup closes
    And I see on the Employment Details tab "<jobTitle>" in the "Job title" field
    And I see on the Employment Details tab "<jobStartDate>" in the "Date of hire" field
    And I see on the Employment Details tab "<unchangedFields>" field values unchanged from the recorded field values
    And I do not see on the Employment Details tab the "Employment end date" field

    Examples:
      | jobTitle   | jobStartDate | unchangedFields                                          |
      | Head of IT | Oct 21, 2000 | Job end date, Occupation category, Hours worked per week |


  @automated @regression
    @C122-F02B-08 @AC1.5
    @C122-F02C-03-02 @AC1.4 @AC1.5
  Scenario Outline: Click Cancel/Close X on the Current occupation popup
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    And I see 'Current occupation' data in edit mode is the same like in display mode
    When I enter random test data in the 'Current occupation' popup "Job title" field
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    And I enter random test data in the 'Current occupation' popup "Job end date" field
    And I enter random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose random test data in the "Full/part time" select field
    And I choose random test data in the "Earnings frequency" select field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I "<button>" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I verify that fields on Employment Details page are not updated

    Examples:
      | button  |
      | cancel  |
      | close X |
