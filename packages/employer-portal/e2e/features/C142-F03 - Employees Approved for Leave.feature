@employer-portal @C142-F03
Feature: C142-F03 - Employees approved for leave

  # @C142-F03-01 @AC1.1 covered by @C142-F01-05

  @C142-F03-01-01 @AC1.1 @permissions @manual
  Scenario Outline: Employees approved for leave - view permission
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_ABSENCE_EMPLOYEESAPPROVEDLEAVE"
    When My Dashboard page is loaded
    Then I "<seeOrNot>" Employees approved for leave panel

    Examples:
      | withOrWithout | seeOrNot  |
      | with          | see       |
      | without       | don't see |

  # @C142-F03-02 @AC2.1 covered by @C142-F01-01

  @C142-F03-03 @AC2.1 @smoke @tba @sanity
  Scenario Outline: Employees approved for leave - display order
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page is loaded
    And I click the "Show/hide panels"
    Then the "Show/hide panels" modal opens
    And I see "Employees approved for leave" is the last displayed option
    When the "Employees approved for leave" switch is "<status>"
    Then "Employees approved for leave" widget is "<visibility>"

    Examples:
      | status   | visibility |
      | enabled  | shown      |
      | disabled | hidden     |

  # @C142-F03-04 @AC2.2 covered by @C142-F01-06

  # @C142-F03-05 @AC2.2 covered by @C142-F01-07

  # @C142-F03-06 @AC2.2 @AC2.3 covered by @C142-F01-08

  # @C142-F03-07 @AC2.2 covered by @C142-F01-32

  @C142-F03-08 @AC2.4 @smoke @tba @sanity
  Scenario Outline: Employees approved for leave - grouping criteria
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page with available notifications is loaded
    And I open the grouping criteria selector in the "Employees approved for leave" panel
    Then I see the default grouping criteria is "Reason"
    And I select a "<grouping criteria>" grouping criteria the "Employees approved for leave" panel
    Then I verify the "Employees approved for leave" graph data are grouped by "<grouping criteria>" criteria

    Examples:
      | grouping criteria  |
      | Reason             |
      | Group              |
      | Leave type         |

  # @C142-F03-09 @AC2.5 covered by @C142-F01-11

  @C142-F03-10 @AC2.6 @tba @regression
    @FAPI-9957-01 @AC1
    @C142-F02-10 @AC10 @AC21
  Scenario Outline: My Dashboard - panels explanation icons
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page with available notifications is loaded
    And I click a tooltip help icon for a "<widget>" widget
    Then I see the pop-up with expected content "<helpText>" explaining the functionality
    When I click X close button on "<widget>" explanation pop-up
    Then I verify the "<widget>" explanation pop-up is closed

    Examples:
      | widget                                      | helpText                                                                                                                                                                                                                                                                                                                                                                                                                         |
      | Notifications with outstanding requirements | Notifications with outstanding requirements This is a listing of all the cases for which information or documentation needs to be provided - by you, the employee or medical provider.                                                                                                                                                                                                                                           |
      | Employees expected to return to work        | Employees expected to return to work full-time This is a listing of the employees who are expected to return to work full-time in the timeframe selected. Part-time returns are not listed here. Display by group: Shows the number of employees returning by administrative groups.                                                                                                                                             |
      | Notifications created                       | Notifications created This is a listing of the cases which have been initiated in the timeframe selected. Filter by disability-related: Shows the number of cases that contain a disability component (such as STD) versus the cases that are Absence or Accommodation-related only. Filter by reason: Shows the number of cases created by the different notification reasons.                                                  |
      | Leaves decided                              | Leaves decided This is a listing of the time-off requests which have been decided in the timeframe selected. Disability decisions are not listed here. Filter by decision: Shows the number of leaves decided by the decision type (approved vs. denied) Filter by reason: Shows the number of leaves decided by notification reason Filter by group: Shows the number of leaves decided by administrative groups                |
      | Employees approved for leave                | Employees approved for leave This is a listing of employees who have been approved to be off-work in the timeframe selected. Filter by leave type: Shows the number of leaves decided by their type (continuous, reduced schedule or intermittent) Filter by reason: Shows the number of leaves approved by notification reason Filter by group: Shows the number of employees approved for leave by their administrative groups |

  @C142-F03-11 @AC2.6 @AC3.1 @tba @regression
    @FAPI-9957-02 @AC1
    @C142-F02-10 @AC10 @AC21
  Scenario Outline: Dashboards view details - explanation icons
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page with available notifications is loaded
    And I click "View Details" on a "<widget>"
    Then I see a "<widget>" drawer is opened
    When I click a tooltip help icon
    Then I see the pop-up with expected content "<helpText>" explaining the functionality
    When I click X close button on "<widget>" explanation pop-up
    Then I verify the "<widget>" explanation pop-up is closed

    Examples:
      | widget                                      | helpText                                                                                                                                                                                                                 |
      | Notifications with outstanding requirements | Notifications with outstanding requirements This is a listing of all the cases for which information or documentation needs to be provided - by you, the employee or medical provider.                                   |
      | Employees expected to return to work        | Employees expected to return to work full-time This is a listing of the employees who are expected to return to work full-time in the timeframe selected. Part-time returns are not listed here.                         |
      | Notifications created                       | Notifications created This is a listing of the cases which have been initiated in the timeframe selected. Filter by disability-related: The "DI?" column marks cases which contain a disability component (such as STD). |
      | Leaves decided                              | Leaves decided This is a listing of the time-off requests which have been decided in the timeframe selected. Disability decisions are not listed here.                                                                   |
      | Employees approved for leave                | Employees approved for leave This is a listing of how long each employee have been approved to be off-work in the timeframe selected. To view full details about each leave, navigate to the cases.                      |

  @C142-F03-12 @AC2.6 @manual
    @FAPI-9957-03 @AC3
    @C142-F02-10 @AC10 @AC21
  # Executing this test may require a web proxy or the local build
  Scenario Outline: My Dashboard - disabling help icons
    Given I log in to the application
    Then I verify that "helpIconEnabled" value in "client-config.json" is set to "true" by default
    When I set "helpIconEnabled" value in "client-config.json" to "<trueOrFalse>"
    And My Dashboard page with available notifications is loaded
    Then I "<seeOrNot>" help icons next to the dashboard panels
    And I "<seeOrNot>" help icons after opening "View details" for each existing panel

    Examples:
      | trueOrFalse | seeOrNot  |
      | true        | see       |
      | false       | don't see |

  @C142-F03-13 @AC2.6 @manual
    @FAPI-9957-04 @AC3
    @C142-F02-10 @AC10 @AC21
  # Executing this test may require a web proxy or the local build
  Scenario: My Dashboard - disabled dashboard view
    Given I log in to the application
    When I set "showDashboard" value in "client-config.json" to "false"
    And My Dashboard page with available notifications is loaded
    Then I "don't see" help icon next to the notifications with outstanding requirements

  @FAPI-9957-05 @AC4 @manual
    @C142-F02-10 @AC10 @AC21
  Scenario Outline: My Dashboard - help icons in high contrast mode
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When set high contrast mode is "<onOff>" if not set
    Then I see dashboard help icons with high contrast color "<onOff>"
    And I see help icons with high contrast color "<onOff>" after opening "View details" for each existing panel

    Examples:
      | onOff |
      | on    |
      | off   |

#  @C142-F03-15 @AC3.2 @AC3.3 covered by @C142-F01-25/@C142-F01-28

#  @C142-F03-16 @AC3.4 covered by @C142-F01-26

  @C142-F03-17 @AC3.5 @manual @sanity
  Scenario Outline: Employees approved for leave drawer - employee centric and standard views
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    And there "<areOrNot>" several leave types or notifications for a single employee
    When I click "View Details" on a "Employees approved for leave"
    Then I see a "Employees approved for leave" drawer is opened
    And I see a "<view>" view

    Examples:
      | areOrNot | view             |
      | are      | employee centric |
      | aren't   | standard         |


#  @C142-F03-18 @AC3.6 @AC3.7 covered by @C142-F01-15-02

#  @C142-F03-19 @AC3.8 covered by @C142-F01-18/@C142-F01-19/@C142-F01-20/@C142-F01-21

#  @C142-F03-20 @AC3.9 covered by @C142-F01-22/@C142-F01-23

#  @C142-F03-21 @AC3.10 covered by C142-F01-24-02

  @C142-F03-22 @AC3.4 @AC4.1 @manual @sanity
  Scenario Outline: Employees approved for leave sorting - employee centric and standard views
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "Employees approved for leave" date period with available results
    And I click "View details" on "Employees approved for leave" widget
    Then The "employees" list is sorted by "Employee" with "ascending" order by default
    And I see the data are presented in "employee centric" view
    When I sort the list by "<field>" with "ascending" order
    Then The "employees" list is sorted by "<field>" with "ascending" order
    And I see the data are presented in "<view>" view
    When I sort the list by "<field>" with "descending" order
    Then The "employees" list is sorted by "<field>" with "descending" order
    And I see the data are presented in "<view>" view

    Examples:
      | field      | view             |
      | Employee   | employee centric |
      | Group      | employee centric |
      | Case       | standard         |
      | Reason     | standard         |
      | Leave Type | standard         |

  @C142-F03-23 @AC3.4 @AC4.2 @AC4.3 @manual @smoke
  Scenario Outline: Employees approved for leave filtering - employee centric and standard views
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "Employees approved for leave" date period with available results
    And I click "View details" on "Employees approved for leave" widget
    And I filter the results by "<field>"
    And I apply "<field>" filter
    Then The "employees" list is filtered correctly by "<field>" field
    And I see the data are presented in "<view>" view
    And I see a description "Showing filtered results <number of employees in filtered results> of <total employees for selected time period>" at the top of the drawer

    Examples:
      | field               | view             |
      | Employee First Name | employee centric |
      | Employee Last Name  | employee centric |
      | Group               | employee centric |
      | Case                | standard         |
      | Reason              | standard         |
      | Leave type          | standard         |

  @C142-F03-24 @AC4.4 @manual @regression
  Scenario Outline: Employees approved for leave - filters and sorting combined
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "Employees approved for leave" date period with available results
    And I click "View details" on "Employees approved for leave" widget
    And I filter the results by "<filter field>"
    And I apply "<filter field>" filter
    And I filter the results by "<filter field 2>"
    And I apply "<filter field 2>" filter
    And I sort the list by "<sorting field>" with "<order>" order
    Then The "employees" list is filtered correctly by "<filter field>" field
    And The "employees" list is filtered correctly by "<filter field 2>" field
    And The "employees" list is sorted by "<sorting field>" with "<order>" order
    And I see the data are presented in "<view>" view

    Examples:
      | filter field        | filter field 2   | sorting field | order      | view             |
      | Employee First Name | Group            | Employee      | descending | employee centric |
      | Employee First Name | Group            | Case          | ascending  | standard         |
      | Employee Last Name  | Group            | Group         | ascending  | employee centric |
      | Group               | Case             | Employee      | descending | standard         |
      | Case                | Leave type       | Employee      | ascending  | standard         |
      | Reason              | Case             | Case          | ascending  | standard         |
      | Leave type          | Group            | Reason        | descending | standard         |

  @C142-F03-25 @AC4.5 @manual @sanity
  Scenario Outline: Employees approved for leave - removing single filter
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "Employees approved for leave" date period with available results
    And I click "View details" on "Employees approved for leave" widget
    And I filter the results by "<filter field>"
    And I apply "<filter field>" filter
    And I filter the results by "<filter field 2>"
    And I apply "<filter field 2>" filter
    And I reset filter of the "<filter field 2">
    Then The "employees" list is filtered correctly by "<filter field>" field
    And I see the data are presented in "<view>" view

    Examples:
      | filter field        | filter field 2     | view             |
      | Employee First Name | Group              | employee centric |
      | Employee Last Name  | Reason             | employee centric |
      | Group               | Leave type         | standard         |
      | Case                | Employee Last Name | standard         |
      | Reason              | Leave type         | standard         |
      | Leave type          | Case               | standard         |

  @C142-F03-26 @AC4.5 @manual @sanity
  Scenario Outline: Employees approved for leave - removing all filters
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I select "Employees approved for leave" date period with available results
    And I click "View details" on "Employees approved for leave" widget
    And I filter the results by "<filter field>"
    And I apply "<filter field>" filter
    And I filter the results by "<filter field 2>"
    And I apply "<filter field 2>" filter
    And I sort the list by "<sorting field>" with "<order>" order
    And I click "close X" button on the filter pop-up
    Then I see all the filters are removed
    And I see the data are presented in "<view>" view

    Examples:
      | filter field        | filter field 2     | view             | sorting field | order      |
      | Employee First Name | Group              | employee centric | Employee      | ascending  |
      | Employee Last Name  | Reason             | employee centric | Group         | descending |
      | Group               | Leave type         | standard         | Case          | ascending  |
      | Case                | Employee Last Name | employee centric | Employee      | descending |
      | Reason              | Leave type         | standard         | Leave type    | ascending  |
      | Leave type          | Case               | standard         | Reason        | descending |
