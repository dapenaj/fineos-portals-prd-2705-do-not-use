@employer-portal @C119-F02
Feature: C119-F02 - Search and List Notifications for Employee

  @C119-F02-01 @search @AC2 @automated @regression
  Scenario: Search modes and default state
    When I log in to the application
    Then I "can" see search section located on the Top Bar
    And I see that search mode is set to Employee by default
    And I can switch between search modes: "Employee search, Notification search"


  @C119-F02-02 @search @AC3 @automated @regression
  Scenario: Notifications prefix
    Given I log in to the application
    When I switch quick search mode to "Notification search"
    Then I can see "NTN-" prefix in quick search input field
    And I can type notification ID
    And I can see a list of items that match my search query


  @C119-F02-03 @search @AC4 @automated @regression
  Scenario Outline: Notifications and Employee search call
    Given I log in to the application
    When I switch quick search mode to "<searchMode>"
    And I partially type search query for "<searchTarget>" and test data "<initialSearchQuery>"
    Then Search is started once I stop typing (with a 500 ms delay)
    And I can continue typing or change the search string for "<searchTarget>" and test data "<continueSearchQuery>"
    And I can see a list of items that match my search query

    Examples:
      | searchMode          | searchTarget        | initialSearchQuery      | continueSearchQuery             |
      | Notification search | Notification ID     | partialNotificationData | partialNotificationDataContinue |
      | Employee search     | Employee First Name | partialEmployeeData     | partialEmployeeDataContinue     |
      | Employee search     | Employee Last Name  | partialEmployeeData     | partialEmployeeDataContinue     |
      | Employee search     | Employee ID         | partialEmployeeData     | partialEmployeeDataContinue     |


  @C119-F02-04 @search @AC4 @AC5 @AC7 @AC9 @AC9.1 @AC10 @automated @regression
  Scenario Outline: Max number of search results
    Given I log in to the application
    When I partially type search query for "<searchTarget>" and test data "<description>"
    Then I can see results list is limited to max 5 results
    And The result list contains message indicating that too many records are returned by the search
    And I can continue typing or change the search string for "<searchTarget>" and test data "<description>"

    Examples:
      | searchTarget        | description             |
      | Notification ID     | partialNotificationData |
      | Employee First Name | partialEmployeeData     |


  @C119-F02-05 @search @AC5 @AC7 @automated @regression @sanity
  Scenario Outline: Employee and Notifications search results
    Given I log in to the application
    When I partially type search query for "<searchTarget>" and test data "<description>"
    Then I can see a list of items that match my search query
    And Each "<searchTarget>" search result contains "<fields>" fields
    When I click on first search result on results page
    Then I should land on "<page>" page
    And I verify that correct "<page>" data was loaded

    Examples:
      | searchTarget    | fields                                                                        | page                 | description             |
      | Notification ID | Notification ID, Notification Reason, Employee First Name, Employee Last Name | Notification Details | partialNotificationData |
      | Employee ID     | First Name, Last Name, Job Title, Organisation Unit, Work Site                | Employee Profile     | partialEmployeeData     |


  @C119-F02-06 @search @AC11 @AC12 @automated @regression @sanity
  Scenario Outline: No match in search results
    Given I log in to the application
    When I incorrectly type search query for "<searchTarget>" and test data "<description>"
    Then I can see empty results list
    And The result list contains message indicating that no records are returned by the "<searchTarget>" search

    Examples:
      | searchTarget    | description             |
      | Notification ID | notExistingNotification |
      | Employee ID     | notExistingEmployee     |


  @C119-F02-07 @employee-profile @notifications-list @AC14 @AC15 @automated @smoke
  Scenario: Employee Profile Page layout
    Given I log in to the application
    When I open Employee details page "withNotifications"
    Then Top section of employee profile page contains "First Name, Last Name" fields
    And Notifications tab is selected by default


  @C119-F02-08 @employee-profile @notifications-list @AC16 @AC18 @automated @regression
  Scenario: Employee Profile full list of Notifications
    Given I log in to the application
    When I open Employee details page "withNotifications"
    Then Each notification item contains "Notification ID, Notification Creation Date, Notification Reason" fields
    When I click first notification link
    Then I should land on "Notification Details" page
    And I verify that correct "Notification Details" data was loaded

  @C119-F02-09 @FAPI-9709-AC2.2 @employee-profile @notifications-list @AC20 @automated @regression
  Scenario: Employee Profile empty list of Notifications with no sort by dropdown
    Given I log in to the application
    When I open Employee details page "withoutNotifications"
    Then I can see information that no notification is available for the user
    And I can't see the "Sorting by" select

  @FAPI-9709-AC2.1 @automated @smoke
  Scenario: Employee Profile, check sorting of list of notifications
    Given I log in to the application
    When I open Employee details page "withNotifications" using EmployeeId
    Then I verify the "Sorting by" dropdown shows "Newest first" option
    And The notifications list is sorted by "Notified on date" with "descending" order by default
    When I select "Oldest first" option in the "Sorting by" select
    Then I verify the "Sorting by" dropdown shows "Oldest first" option
    And The notifications list is sorted by "Notified on date" with "ascending" order
    When I select "Newest first" option in the "Sorting by" select
    Then I verify the "Sorting by" dropdown shows "Newest first" option
    And The notifications list is sorted by "Notified on date" with "descending" order


  @C119-F02-10 @employee-profile @notifications-list @AC17 @automated @regression
  Scenario Outline: Employee Profile list of Notifications - Entitlement label
    Given I log in to the application
    When I open Employee details page "with notification containing <option>"
    Then I can see "<option>" on notification containing "<label>"

    Examples:
      | option                     | label                   |
      | Child Claims Cases         | Wage Replacement        |
      | Child Accommodations Cases | Workplace Accommodation |
      | Child Absence Case         | Job Protected Leave     |
      | no Child Cases             |                         |


  @C119-F02-11 @search @permissions @manual @AC1
  Scenario: Search component - user without permissions to view employee profile and notifications
    When I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS and URL_GET_GROUPCLIENT_NOTIFICATIONS"
    Then I "can't" see search section located on the Top Bar


  @C119-F02-12 @employee-profile @permissions @manual @AC13
  Scenario Outline: Search component - User without permissions to view employee profile
    When I log in to the application as a user "without" permissions to "<permission>"
    Then I shouldn't see search for "Employee" option
    And I "can't" proceed to any employee profile if I have no permission "URL_GET_GROUPCLIENT_CUSTOMERS_GETSINGLE"

    Examples:
      | permission                              |
      | URL_GET_GROUPCLIENT_CUSTOMERS           |
      | URL_GET_GROUPCLIENT_CUSTOMERS_GETSINGLE |


  @C119-F02-13 @employee-profile @notifications-list @AC15 @AC19 @manual
  Scenario: Employee Profile - long notifications list
    Given I log in to the application
    When I open Employee details page "with" ">10" notifications
    Then I should see scrollable notification list with all available notifications


  @C119-F02-14 @employee-profile @permissions @manual @AC10.1
  Scenario: Search component - User without permissions to view notifications
    When I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_NOTIFICATIONS"
    Then I shouldn't see search for "Notification" option
    And I "can't" proceed to any notification


  @C119-F02-15 @employee-profile @smoke @automated @AC6 @AC8
  Scenario Outline: Employee Profile Page layout
    Given I log in to the application
    And I switch quick search mode to "<searchMode>"
    And I type search query for "<searchTarget>"
    Then I should see results list for selected target
    When I click on first search result on results page
    Then I should land on "<page>" page

    Examples:
      | searchMode          | searchTarget       | page                 |
      | Employee search     | Employee Last Name | Employee Profile     |
      | Notification search | Notification ID    | Notification Details |
