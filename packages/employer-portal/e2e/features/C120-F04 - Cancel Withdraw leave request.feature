@employer-portal @C120-F04
Feature: C120-F04 - Cancel-Withdraw request, Allow employer to request the removal of existing leave periods for an employee

  @C120-F04-01 @permissions @AC1.1 @manual
  Scenario Outline: Displaying option to remove leave period - user without permissions
    Given I log in to the application as a user "without" permissions to "<permission>"
    And I open notification with "child absence case and at least one non cancelled leave period"
    When I click on the actions menu options on the notification page
    Then I verify that the menu action "Request a leave to be removed" is "not visible" in the dropdown
    Examples:
      | permission                                                 |
      | URL_GET_GROUPCLIENT_ABSENCE_ABSENCEPERIODDECISIONS         |
      | URL_POST_GROUPCLIENT_ABSENCE_LEAVEPERIODSCHANGEREQUEST_ADD |


  @C120-F04-02 @AC1.1 @automated @regression @sanity
  Scenario Outline: Display action request a leave to be removed - user with permissions
    Given I log in to the application
    And I open notification with "child absence case and exact one leave period with <status> status" and test data "<testData>"
    When I click on the actions menu options on the notification page
    Then I verify that the menu action "Request a leave to be removed" is "<visibility>" in the dropdown
    Examples:
      | status    | visibility  | testData       |
      | cancelled | not visible | @C120-F04-02-1 |
      | approved  | visible     | @C120-F04-02-2 |
      | pending   | visible     | @C120-F04-02-3 |
      | denied    | visible     | @C120-F04-02-4 |

  @C120-F04-03 @permissions @AC1.1 @automated @regression
  Scenario Outline: Display action request a leave to be removed - user with permissions
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_ABSENCE_LEAVEPERIODSCHANGEREQUEST_ADD"
    And I open notification with "child absence case and one leave period with <status> status" and test data "<testData>"
    When I click on the actions menu options on the notification page
    Then I verify that the menu action "Request a leave to be removed" is "<visibility>" in the dropdown
    Examples:
      | status    | visibility  | testData       |
      | cancelled | not visible | @C120-F04-02-1 |
      | approved  | visible     | @C120-F04-02-2 |
      | pending   | visible     | @C120-F04-02-3 |
      | denied    | visible     | @C120-F04-02-4 |


  @C120-F04-04 @AC1.1 @automated @regression
  Scenario: Display action request a leave to be removed - multiple periods
    Given I log in to the application
    And I open notification with "child absence case and multiple leave periods" and test data "@C120-F04-04"
    When I click on the actions menu options on the notification page
    Then I verify that the menu action "Request a leave to be removed" is "visible" in the dropdown


  @C120-F04-05 @AC2.1 @automated @regression
  Scenario: List of periods that can be removed - excluding cancelled periods
    Given I log in to the application
    And I open notification with "child absence case and multiple leave periods with statuses cancelled, approved, pending, denied" and test data "@C120-F04-05"
    When I expand all collapsible cards
    And I open "Request a leave to be removed" popup
    Then I verify that only periods with status different than "cancelled" considered on the list and test data "@C120-F04-05"


  @C120-F04-06 @AC2.1 @automated @regression
  Scenario: List of periods that can be removed - excluding periods without leave plan
    Given I log in to the application
    And I open notification with "child absence case and leaves without leave plan name defined" and test data "@C120-F04-06"
    When I click on the actions menu options on the notification page
    Then I verify that the menu option request a leave to be removed is not visible


  @C120-F04-07 @AC2.1 @automated @regression
  Scenario: List of periods that can be removed - merging periods
    Given I log in to the application
    And I open notification with "child absence case that takes more than week" and test data "@C120-F01-18-TD-3"
    When I open "Request a leave to be removed" popup
    Then I verify that only one period can be removed


  @C120-F04-08 @AC2.1 @automated @regression
  Scenario: List of periods that can be removed - one absence, multiple leave plans
    Given I log in to the application
    And I open notification with "Paid Leave multiple leave plans" and test data "@C115-F02-08-TD-2"
    When I open "Request a leave to be removed" popup
    Then I verify that only one period can be removed


  @C120-F04-09 @AC2.1 @automated @regression
  Scenario: List of periods that can be removed - one absence, multiple period types
    Given I log in to the application
    And I open notification with "child absence case and multiple period types (reduced schedule, intermittent, continuous)" and test data "@C120-F04-09"
    When I expand all collapsible cards
    And I open "Request a leave to be removed" popup
    Then I verify that each period on the list contains correct start & end date, period type and absence reason for test data "@C120-F04-09"


    # change forced by C120-F01 AC1.1
  @C120-F04-10 @AC2.1 @automated @regression
  @C120-F01-03 @AC1.1
  Scenario: List of periods that can be removed - multiple absences
    Given I log in to the application
    And I open notification with "multiple child absence cases" and test data "@C120-F04-10"
    Then I verify that the Actions menu is not visible


  @C120-F04-11 @AC2.2 @smoke @automated
  Scenario: List of periods - period representation
    Given I log in to the application
    And I open notification with "child absence case and multiple leave periods with statuses cancelled, approved, pending, denied" and test data "@C120-F04-11"
    When I open "Request a leave to be removed" popup
    Then I verify that each period on the list contains "Leave period start date, Leave period end date, Leave period type, Leave period reason" fields


  @C120-F04-12 @AC2.3 @AC3.1 @automated @regression
  Scenario Outline: List of periods - selecting periods to be removed
    Given I log in to the application
    And I open notification with "child absence case and multiple leave periods" and test data "@C120-F04-12"
    And I open "Request a leave to be removed" popup
    And I select "<selection>" to remove
    When I complete cancellation request and submit
    Then I should see Success screen

    Examples:
      | selection        |
      | single period    |
      | multiple periods |


  @C120-F04-13 @AC2.3 @AC2.4 @AC2.5 @smoke @automated @sanity
  Scenario: Popup fields
    Given I log in to the application
    And I open notification with "child absence case" and test data "@C120-F04-13"
    When I open "Request a leave to be removed" popup
    Then I can see mandatory field Reason for removal
    And I can see optional field "Any additional details you want to give us?"


  @C120-F04-14 @AC2.3 @AC2.4 @AC3.1 @automated @regression
  Scenario Outline: Proceed without mandatory fields selected
    Given I log in to the application
    And I open notification with "child absence case and multiple leave periods" and test data "@C120-F04-14"
    And I open "Request a leave to be removed" popup
    When I skip "<field>" and proceed to submit cancellation request
    When I select first period "<field>"
    Then I can see the OK button enabled
    Examples:
      | field             |
      | periods selection |


  @C120-F04-15 @AC3.2 @automated @regression
  Scenario Outline: Success screen
    Given I log in to the application
    And I open notification with "child absence case and leave period with <status> status" and test data "<testData>"
    When I open "Request a leave to be removed" popup
    And I select "single period" to remove
    When I complete cancellation request and submit
    Then I should see Success screen
    And message displayed after a successful request should be customizable
    When I close confirmation screen
    Then I should be land on notification page
    Examples:
      | status   | testData       |
      | approved | @C120-F04-15-1 |
      | pending  | @C120-F04-15-2 |
      | denied   | @C120-F04-15-3 |


  @C120-F04-16 @AC3.3 @automated @regression
  Scenario: Cancelling request
    Given I log in to the application
    And I open notification with "child absence case" and test data "@C120-F04-16"
    When I open "Request a leave to be removed" popup
    And I select cancel button on "Request a leave to be removed" popup
    Then I should be land on notification page

  @C120-F04-17 @AC2.1 @automated @regression
  Scenario: Merging leave plans -- 1 leave period + 2 leave plans
    Given I log in to the application
    And I open notification with "leave period with 2 leave plans applicable (same start & end dates & absence reason)" and test data "@C120-F04-17"
    When I expand all collapsible cards
    And I open "Request a leave to be removed" popup
    Then I verify I can see "1" item(s) on periods list
    And I verify that each period on the list contains correct start & end date, period type and absence reason for test data "@C120-F04-17"


  @C120-F04-18 @AC2.1 @automated @regression
  Scenario: Merging leave plans -- 2 leave periods
    Given I log in to the application
    And I open notification with "2 leave periods (different start & end dates & absence reason)" and test data "@C120-F04-17"
    When I expand all collapsible cards
    And I open "Request a leave to be removed" popup
    Then I verify I can see "1" item(s) on periods list
    And I verify that each period on the list contains correct start & end date, period type and absence reason for test data "@C120-F04-17"
