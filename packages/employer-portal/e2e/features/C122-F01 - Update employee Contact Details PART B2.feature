@employer-portal @C122-F01B2
Feature: C122-F01 - Update employee Contact Details part B continuation from AC5.1

  @C122-F01B2-01 @AC5.1 @permissions @manual
  Scenario Outline: Permission to view an employee’s phone numbers and to change their communication preferences
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK"
    When I open Communication Preferences page
    Then I can see the "Edit preference" link under the "Phone number(s)" section is "<visibility>" on the Personal Details tab

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B2-02 @AC5.2 @permissions @manual
  Scenario Outline:  open Communication Preferences page of employee without permissions
    Given I log in to the application as a user "without" permissions to "<permissions>"
    When I open Communication Preferences page of employee with no registered specific contact phone number with at least "1" phone numbers in personal details tab
    Then I see on the Communication Preferences page the "Phone Calls" section is "not visible"

    Examples:
      | permissions                                                                                                                                               |
      | URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_EDIT |

  @C122-F01B2-02-01 @AC5.2 @permissions @manual
  Scenario Outline:  open Communication Preferences page of employee with no registered specific contact phone number with at least 1 phone numbers
    Given I log in to the application as a user "with" permissions to "<permissions>"
    When I open Communication Preferences of employee with no primary phone contact number and at least "1" phone numbers in the personal details tab
    Then I see on the Communication Preferences page the "Phone Calls" section is "visible"
    And I see on the "Phone Calls" section the text "From time to time, we may wish to contact your employee by phone. We will call them on the number of their preference"
    And I see on the "Phone Calls" section the "Add phone number" link

    Examples:
      | permissions                                                                                                                                               |
      | URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_EDIT |

  @C122-F01B2-03 @AC5.2 @permissions @manual
  Scenario Outline: open Communication Preferences page of employee with no registered specific contact phone number with 0 phone numbers
    Given I log in to the application as a user "with" permissions to "<permissions>"
    When I open Communication Preferences of employee with no primary phone contact number and "0" phone numbers in personal details tab
    Then I see on the Communication Preferences page the "Phone Calls" section is "visible"
    And I see on the "Phone Calls" section the text "From time to time, we may wish to contact your employee by phone. We will call them on the number of their preference"
    And I see on the "Phone Calls" section the "Add phone number" link

    Examples:
      | permissions                                                                                                                                               |
      | URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD |

  @C122-F01B2-04 @AC5.3 @permissions @manual
  Scenario Outline: In alerts section, show Edit Preference link beneath the text describing the employee’s subscriptions
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES"
    When I open Communication Preferences page of employee "<subscribed>" to alerts via "<method>"
    Then I can see the "Edit preference" link under the "Alerts, updates and reminder" section is "<visibility>"

    Examples:
      | subscribed     | visibility  | method |
      | subscribed     | visible     | SMS    |
      | subscribed     | visible     | email  |
      | not subscribed | not visible | -      |

  @C122-F01B2-05 @AC5.4 @permissions @manual
  Scenario Outline: In alerts section, show question asking would you like to notified of updates and reminders
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES"
    When I open Communication Preferences page of employee "<subscribed>" to alerts via "<method>"
    Then I can see the "Alerts, updates and reminder" section is "<visibility>"
    And I see on the "Alerts, updates and reminder" section the text "Would your employee like to be notified of updates and reminders of due dates relating to requests and payments via email or SM"
    And I see on the "Alerts, updates and reminder" section the "They want to receive alerts" link

    Examples:
      | subscribed     | visibility  | method |
      | subscribed     | not visible | SMS    |
      | subscribed     | not visible | email  |
      | not subscribed | visible     | -      |

  @C122-F01B2-06 @AC5.5 @permissions @manual
  Scenario Outline: In alerts section, show Edit Preference link in the Written Correspondence section
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES"
    When I open Communication Preferences page
    Then I can see the "Edit preference" link in the "Written Correspondence" section is "<visibility>"

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B2-07 @AC6.1 @automated @sanity @regression
  Scenario: Open the Add new email popup for Alerts and updates
    Given I log in to the application
    When I open personal details of employee "subscribed to alerts via email" and at least one email address in the personal details tab
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I click the "They want to use a different email" link
    Then the "Add new email" popup opens

  @C122-F01B2-08 @AC6.2 @automated @regression
  Scenario Outline: Add correct formatted email address in the Add new email popup
    Given I log in to the application
    When I open personal details of employee "subscribed to alerts via email" and at least one email address in the personal details tab
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I record the list of emails for comparison
    And I click the "They want to use a different email" link
    Then the "Add new email" popup opens
    When I enter "<emailAddress>" test data in last email text box
    And I "accept" "Add new email" popup
    Then the "Add new email" popup closes
    And I see on the Alerts and updates popup "<emailAddress>" added to the list of emails from the recorded values

    Examples:
      | emailAddress                   |
      | john.smith@fineos.com          |
      | email@subdomain.example.com    |
      | firstname+lastname@example.com |
      | 1234567890@example.com         |
      | email@example-one.com          |
      | _______@example.com            |
      | email@example.name             |
      | email@example.museum           |
      | email@example.co.jp            |
      | firstname-lastname@example.com |

  @C122-F01B2-09 @AC6.3 @automated @regression
  Scenario Outline: Add incorrect formatted email address in the Add new email popup
    Given I log in to the application
    When I open personal details of employee "subscribed to alerts via email" and at least one email address in the personal details tab
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I click the "They want to use a different email" link
    Then the "Add new email" popup opens
    When I type "<emailAddress>" in the "Email" input field
    And I "accept" "Add new email" popup
    Then I see a validation message "Please type your email address in the format myname@domain.com" is "visible"

    Examples:
      | emailAddress                  |
      | plainaddress                  |
      | #@%^%#$@#$@#.com              |
      | @example.com                  |
      | Joe Smith <email@example.com> |
      | email.example.com             |
      | email@example@example.com     |
      | .email@example.com            |
      | email.@example.com            |
      | email..email@example.com      |
      | あいうえお@example.com             |
      | email@example.com (Joe Smith) |
      | email@example                 |
      | email@111.222.333.44444       |
      | email@example..com            |
      | Abc..123@example.com          |

  @C122-F01B2-10 @AC6.4 @automated @regression
  Scenario Outline: Click Cancel/Close X on Add new email popup for Alerts and updates
    Given I log in to the application
    And I open personal details of employee "subscribed to alerts via email" and at least one email address in the personal details tab
   And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Alerts, updates and reminders section
    And the "Alerts and updates" popup opens
    When I click the "They want to use a different email" link
    Then the "Add new email" popup opens
    And I type "john.smith@fineos.com" in the "Email" input field
    And I "<button>" "Add new email" popup
    And I see on the Alerts and updates popup list of emails has not been included "john.smith@fineos.com"
    
    Examples:
      | button  |
      | cancel  |
      | close X |

  @C122-F01B2-11 @AC6.4 @manual
  Scenario:  Add new email address with missing data in the Add new email popup
    Given I log in to the application
    And I open Communication Preferences page of employee "subscribed" to alerts via "email"
    And I click the "Edit preference" link under the "Send them an email" section
    And the Alerts and updates popup opens
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    When I enter "john.smith@fineos.com" in the "Email" field
    Then I see the OK button is enabled
    When I clear the "Email" field
#    step seems to be no longer valid there's general agreement that the buttons are always enabled
    Then I see the OK button is disabled
    When I enter "mary.anne@fineos.com" in the "Email" field
    Then I see the OK button is enabled

  @C122-F01B2-12 @AC7.1 @AC7.2 @automated @regression
  Scenario: Open the Add new phone number popup and check the phone contact type values for Alerts and updates
    Given I log in to the application
    When I open personal details of employee "subscribed to alerts via SMS" and at least one email address in the personal details tab
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I click the "They want to use a different phone number" link
    Then the Add new phone number popup opens with elements "Add new phone number title, Phone Contact Type, Country code, Area code, Phone number, OK, Cancel, Close X"
    And Add new phone number popup labels "have" "*" on the field "Area code"
    And Add new phone number popup labels "have" "*" on the field "Phone no"
    And Add new phone number popup labels "do not have" "*" on the field "Country code"
    And I see "Cell Phone" option is selected in Phone Contact Type field
    And I see Phone Contact Type select is "disabled"

  @C122-F01B2-13 @AC7.3 @automated @regression
  Scenario: Add new phone number with invalid phone number in the Add new phone number popup
    Given I log in to the application
    When I open personal details of employee "subscribed to alerts via SMS"
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I click the "They want to use a different phone number" link
    And I enter badtext!@#!@ number in the "Phone number" field
    And I click the OK button
    Then "not numeric" phone number error messages is "visible"
    And "required" phone number error messages is "visible"
    When I enter is$ number in the "Area code" field
    When I enter this@ number in the "Country code" field
    And I click the OK button
    Then "not numeric" phone number error messages is "visible"
    When I enter 12 number in the "Country code" field
    And I click the OK button
    Then "not numeric" phone number error messages is "visible"
    When I enter 445 number in the "Area code" field
    And I click the OK button
    Then "not numeric" phone number error messages is "visible"
    When I enter 55577888 number in the "Phone number" field
    And I click the OK button
    Then the Add new phone number popup closes


  @C122-F01B2-14 @AC7.3 @manual
  Scenario: Add new phone number with missing required fields in the Add new phone number popup
    Given I log in to the application
    And I open Communication Preferences of employee "subscribed" to alerts via "SMS"
    And I click the "Edit preference" link under the "Send them a phone number" section
    And the Alerts and updates popup opens
    When I click the "They want to use a different phone number" link under the "Select a phone number" section
    Then the Add new phone number popup opens
    And I select "Landline" in the "Phone Contact Type" field
    And I enter "48" in the "Country code" field
    And I enter "277" in the "Area code" field
    And I enter "888999" in the "Phone number" field
    Then I see the OK button is enabled
    When I clear the "Area code" field
#   step seems to be no longer valid there's general agreement that the buttons are always enabled
    Then I see the OK button is disabled
    When I enter "277" in the "Area code" field
    Then I see the OK button is enabled
    When I clear the "Phone number" field
#   step seems to be no longer valid there's general agreement that the buttons are always enabled
    Then I see the OK button is disabled
    When I enter "888999" in the "Phone number" field
    Then I see the OK button is enabled

  @C122-F01B2-15 @AC7.4 @manual
  Scenario Outline: Click Cancel/Close X on Add new phone number popup
    Given I log in to the application
    And I open Communication Preferences of employee "subscribed" to alerts via "SMS"
    And I click the "Edit preference" link under the "Send them a phone number" section
    And the Alerts and updates popup opens
    And I record the list of phone numbers in the "Select a phone number" field for comparison
    When I click the "They want to use a different phone number" link under the "Select a phone number" section
    Then the Add new phone number popup opens
    And I select a valid "random value" in the "Phone Contact Type" field
    And I enter a valid "random value" in the "Country code" field
    And I enter a valid "random value" in the "Area code" field
    And I enter a valid "random value" in the "Phone number" field
    When I click the "<button>" button
    Then the Add new phone number popup closes
    And I see on the Alerts and updates popup list of phone numbers unchanged from the recorded values

    Examples:
      | button  |
      | Cancel  |
      | Close X |

  @C122-F01B2-16 @AC8.1 @AC8.3 @smoke @automated
  Scenario: Click Edit Preference link in the Phone Calls section with primary phone contact number
    Given I log in to the application
    And I open personal details of employee with at least "one phone number and a primary phone contact number"
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open Communication Preferences of the same employee
    And I record the phone number in the "Phone Calls" section for comparison
    When I click the "Edit preference" link in the Phone calls section
    Then the "Phone calls" popup opens
    And the Phone Calls popup should contain list of radio buttons with phone numbers in format "[International Code]-[Area Code]-[Telephone Number]"
    And the Phone Calls popup should contain the label "Select a phone number" on the list of phone numbers
    And the Phone Calls popup should show the recorded phone number of the Phone Calls as selected in the list of phone numbers
    And the Phone Calls popup should contain the recorded phone numbers of the personal details in the list of phone numbers
    And the Phone Calls popup should contain the buttons "OK, Cancel, Close X"
    And the Phone Calls popup should contain the title "Phone calls"
    And the Phone Calls popup should contain the "They want to use a different phone number" link

  @C122-F01B2-17 @AC8.2 @AC8.3 @automated @regression
  Scenario Outline: Click Add phone number link in the Phone Calls section with no primary phone contact number
    Given I log in to the application
    And I open personal details of employee with at least "<numberOfPhoneNumbers> phone number and no primary phone contact number"
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open Communication Preferences of the same employee
    When I click the "Add phone number" link in the Phone calls section
    Then the "Phone calls" popup opens
    And the Phone Calls popup should contain list of radio buttons with phone numbers in format "[International Code]-[Area Code]-[Telephone Number]"
    And the Phone Calls popup should contain the label "Select a phone number" on the list of phone numbers
    And the Phone Calls popup should show <isNumberSelected> phone number in the list of phone numbers
    And the Phone Calls popup should contain the recorded phone numbers of the personal details in the list of phone numbers
    And the Phone Calls popup should contain the buttons "OK, Cancel, Close X"
    And the Phone Calls popup should contain the title "Phone calls"
    And the Phone Calls popup should contain the "They want to use a different phone number" link

    Examples:
      | numberOfPhoneNumbers | isNumberSelected |
      | one                  | selected         |
      | more than one        | not selected     |

  @C122-F01B2-18 @AC8.4 @manual
  Scenario: Select primary phone contact number in the Phone Calls popup
    Given I log in to the application
    And I open personal details of employee with at least "2" phone numbers and no primary phone contact number
    And I record the "Phone Numbers(s)" field value on the Personal Details tab for comparison
    And I open Communication Preferences of the same employee
    When I click the "Add phone number" link in the Phone calls section
    Then the Phone Calls popup opens
    And the Phone Calls popup should show no selected phone number in the list of phone numbers
    When I select the "first" phone number radio button
    Then I see the "first" phone number radio button is "selected"
    When I select the "second" phone number radio button
    Then I see the "second" phone number radio button is "selected"
    And I see the "first" phone number radio button is "not selected"

  @C122-F01B2-19 @AC8.5 @permissions @manual
  Scenario Outline: Permission to view "They want to use a different phone number" link in the  Phone Calls popup
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    And I open Communication Preferences page of employee with a primary phone contact number
    When I click the "Edit preference" link in the Phone calls section
    Then the Phone Calls popup opens
    And I can see the "They want to use a different phone number" link is "<visibility>" on the Phone Calls popup

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B2-20 @AC8.6 @automated @smoke
  Scenario: Click "They want to use a different phone number" link and add new phone number
    Given I log in to the application
    And I open personal details of employee with at least "of employee with phone numbers"
    And I open "Employee Details" tab
    And I open Communication Preferences of the same employee
    And I set random "Phone number" in the Phone calls section if not set
    And I click the "Edit preference" link in the Phone calls section
    And the "Phone calls" popup opens
    And I record the list of phone numbers for comparison
    When I click the "They want to use a different phone number" link
    Then the Add new phone number popup opens with elements "Add new phone number title, Phone Contact Type, Country code, Area code, Phone number, OK, Cancel, Close X"
    When I select Landline in the Phone Contact Type field
    And I enter random number in the "country code" field
    And I enter random number in the "area code" field
    And I enter random number in the "phone number" field
    And I click the OK button
    Then the Add new phone number popup closes
    And I see the phone number "country code,area code,phone number" is selected and added to the recorded list of phone numbers in the Phone Calls popup

  @C122-F01B2-21 @AC8.7 @manual
  Scenario Outline: Click Cancel/Close X on Phone calls popup
    Given I log in to the application
    And I open Communication Preferences of employee with no primary phone contact number and at least "1" phone numbers in the personal details tab
    And I click the "Add phone number" link in the Phone calls section
    And the Phone Calls popup opens
    And I select the "first" phone number radio button
    When I click the "<button>" button
    Then the Phone Calls popup closes
    And I see on the Communication Preferences page no primary phone contact number on the Phone Calls section
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page no primary phone contact number on the Phone Calls section

    Examples:
      | button  |
      | Cancel  |
      | Close X |

  @C122-F01B2-22 @AC8.8 @AC8.10 @manual
  Scenario Outline: Click OK on Phone Calls popup when phone number is selected with no previous phone contact number
    Given I log in to the application
    And I open Communication Preferences of employee with no primary phone contact number and at least "1" phone numbers in the personal details tab
    And I click the "Add phone number" link in the Phone calls section
    And the Phone Calls popup opens
    And I select the "first" phone number radio button
    And I record the selected phone number for comparison
    When I click the OK button
    And the Phone Calls popup closes
    Then I see on the Phone Calls section the text "<message>"
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see on the Phone Calls section the text "<message>"

    Examples:
      | message                                                                                                        |
      | From time to time, we may wish to contact your employee by phone. We will call them on <selected phone number> |

  @C122-F01B2-23 @AC8.9 @manual
  Scenario: Prevent closing the Phone Calls popup when no primary phone contact number has been selected
    Given I log in to the application
    And I open Communication Preferences of employee with no primary phone contact number and at least "1" phone numbers in the personal details tab
    And I click the "Add phone number" link in the Phone calls section
    And the Phone Calls popup opens
    When I click the OK button
    And I see an error message prompting to select a phone number on the Phone Calls popup
    And I select the "first" phone number radio button
    When I click the OK button
    Then the Phone Calls popup closes

  @C122-F01B2-24 @AC9.1 @AC9.2 @9.3 @manual
  Scenario: Click OK after entering a new phone number on the Add New Phone Number popup
    Given I log in to the application
    And I open Communication Preferences of employee with "0" phone numbers in the personal details tab
    When I click the "Add phone number" link in the Phone calls section
    Then the Add new phone number popup opens
    And I select "Landline" in the "Phone Contact Type" field
    And I enter "44" in the "Country code" field
    And I enter "555" in the "Area code" field
    And I enter "777888" in the "Phone number" field
    When I click the OK button
    Then the Add new phone number popup closes
    And I see on the "Phone Calls" section the text "From time to time, we may wish to contact your employee by phone. We will call them on 44-555-777888"
    And I do not see on the "Phone Calls" section any other text message
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page the primary phone contact number "44-555-777888" on the Phone Calls section

  @C122-F01B2-25 @AC10.1 @permissions @manual
  Scenario Outline: In alerts section, show Edit Preference link beneath the text describing the employee’s subscriptions
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES"
    When I open Communication Preferences page of employee "<subscribed>" to alerts via "<method>"
    Then I can see the "Edit preference" link under the "Alerts, updates and reminder" section is "<visibility>"

    Examples:
      | subscribed     | visibility  | method |
      | subscribed     | visible     | SMS    |
      | subscribed     | visible     | email  |
      | not subscribed | not visible | -      |

  @C122-F01B2-26 @AC10.2 @permissions @manual
  Scenario Outline: In alerts section, show question asking would you like to notified of updates and reminders
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_COMMUNICATIONPREFERENCES_LINK, URL_GET_GROUPCLIENT_CUSTOMERS_PHONENUMBERS, URL_GET_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES"
    When I open Communication Preferences page of employee "<subscribed>" to alerts via "<method>"
    Then I can see the "Alerts, updates and reminder" section is "<visibility>"
    And I see on the "Alerts, updates and reminder" section the text "Would your employee like to be notified of updates and reminders of due dates relating to requests and payments via email or SM"
    And I see on the "Alerts, updates and reminder" section the "They want to receive alerts" link

    Examples:
      | subscribed     | visibility  | method |
      | subscribed     | not visible | SMS    |
      | subscribed     | not visible | email  |
      | not subscribed | visible     | -      |

  @C122-F01B2-27 @AC11.1  @AC11.2 @automated @smoke
  Scenario Outline: Click on correct link to Open Alerts and updates popup
    Given I log in to the application
    When I open personal details of employee "<subscribed>" and at least one email address in the personal details tab
    And I open "Employee Details" tab
    And I open Communication Preferences of the same employee
    And I click the "<linkName>" link in Alerts, updates and reminders section
    And the "Alerts and updates" popup opens

    Examples:
      | subscribed                     | linkName                    |
      | subscribed to alerts via SMS   | Edit preference             |
      | subscribed to alerts via email | Edit preference             |
      | not subscribed                 | They want to receive alerts |

  @C122-F01B2-28 @AC11.3 @automated @sanity @regression
  Scenario: Open Alerts and updates popup of employee not subscribed to alerts
    Given I log in to the application
    When I open personal details of employee "not subscribed to alerts"
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "They want to receive alerts" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    And the Alerts and updates popup should contain the not selected checkbox "Send them an email"
    And the Alerts and updates popup should contain the not selected checkbox "Send them an SMS"

  @C122-F01B2-29 @AC11.3 @smoke @automated
  Scenario: Open Alerts and updates popup of employee subscribed to alerts via email
    Given I log in to the application
    And I open personal details of employee "subscribed to alerts via email" and at least one email address in the personal details tab
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    When I open Communication Preferences of the same employee
    And I record the preferred email address in the Alerts and updates section for comparison
    And I click the "Edit preference" link in Alerts, updates and reminders section
    And the "Alerts and updates" popup opens
    And the Alerts and updates popup should contain the selected checkbox "Send them an email"
    And the Alerts and updates popup should contain the recorded list of emails with the recorded selected preferred email address
    And the Alerts and updates popup should contain the label "Select an email" on the list of emails
    And the Alerts and updates popup should contain the not selected checkbox "Send them an SMS"
    And the Alerts and updates popup should contain the buttons "OK, Cancel, X"

  @C122-F01B2-30 @AC11.3 @automated @regression
  Scenario: Open Alerts and updates popup of employee subscribed to alerts via SMS
    Given I log in to the application
    And I open personal details of employee "subscribed to alerts via SMS" and at least one phone number in the personal details tab
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    And the Employee has at least 2 "Landline" "Phone numbers"
    And the Employee has at least 2 "Cell Phone" "Phone numbers"
    And I record the list of Landline phone numbers for comparison of the Phone numbers section of the personal details tab
    And I record the list of Cell Phone phone numbers for comparison of the Phone numbers section of the personal details tab
    And I "accept" "Edit Phone numbers" popup
    And the "Edit Phone numbers" popup closes
    And I record the list of phone numbers for comparison of the Phone numbers section of the personal details tab
    When I open Communication Preferences of the same employee
    And I record the preferred phone number in the Alerts and updates section for comparison
    And I click the "Edit preference" link in Alerts, updates and reminders section
    And the "Alerts and updates" popup opens
    Then the Alerts and updates popup should contain the not selected checkbox "Send them an email"
    And the Alerts and updates popup should contain the selected checkbox "Send them an SMS"
    And the Alerts and updates popup should contain the recorded list of mobile phone numbers with the recorded selected preferred mobile phone number
    And the Alerts and updates popup should not contain the recorded list of landline phone numbers
    And the Alerts and updates popup should contain the label "Select a phone number" on the list of phone numbers
    And the Alerts and updates popup should contain the buttons "OK, Cancel, Close X"

  @C122-F01B2-31 @AC11.3 @automated @regression
  Scenario: Open Alerts and updates popup of employee subscribed to alerts via SMS and email
    Given I log in to the application
    And I open personal details of employee "subscribed to alerts via email and SMS" and at least one phone number in the personal details tab
    And I open "Employee Details" tab
    And I record the list of phone numbers for comparison of the Phone numbers section of the personal details tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    When I open Communication Preferences of the same employee
    And I record the preferred phone number in the Alerts and updates section for comparison
    And I click the "Edit preference" link in Alerts, updates and reminders section
    And the "Alerts and updates" popup opens
    Then the Alerts and updates popup should contain the selected checkbox "Send them an email"
    And the Alerts and updates popup should contain the selected checkbox "Send them an SMS"
    And the Alerts and updates popup should contain the recorded list of mobile phone numbers with the recorded selected preferred mobile phone number
    And the Alerts and updates popup should contain the recorded list of emails with the recorded selected preferred email address
    And the Alerts and updates popup should contain the label "Select a phone number" on the list of phone numbers
    And the Alerts and updates popup should contain the label "Select an email" on the list of emails
    And the Alerts and updates popup should contain the buttons "OK, Cancel, Close X"


  @C122-F01B2-32 @AC11.4 @automated @regression
  Scenario: Select Send them an email checkbox in Alerts and updates popup with preferred email address
    Given I log in to the application
    When I open personal details of employee "not subscribed to alerts via email" and at least 1 email address in the personal details tab and preferred email address
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    And I open "Communication Preferences" tab
    And I record the preferred email address in the Written Correspondence for comparison
    And I click the "They want to receive alerts" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I "select" the "Send them an email" checkbox in "Alerts and updates"
    Then I see the the recorded list of "emails" from the personal details tab
    And I see the preferred "email address" is "marked" in "Alerts and updates"

  @C122-F01B2-33 @AC11.4 @automated @regression
  Scenario: Select Send them an email checkbox in Alerts and updates popup with no preferred email address
    Given I log in to the application
    When I open personal details of employee "not subscribed to alerts via email" and at least 1 email address in the personal details tab and no preferred email address
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    And I open "Communication Preferences" tab
    And I click the "They want to receive alerts" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I "select" the "Send them an email" checkbox in "Alerts and updates"
    Then the Alerts and updates popup should contain the recorded list of emails


  @C122-F01B2-34 @AC11.4 @AC11.5 @AC11.6 @permissions @manual
  Scenario: Add email address to Alerts and updates popup with at least 1 registered email
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    When I open Communication Preferences page of employee "not subscribed" to alerts and at least "1" email addresses in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I select the "Send them an email" checkbox
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    And I enter "susan.walters@gmai.com" in the Email field
    When I click the OK button
    And the Add new email popup closes
    Then I see on the Alerts and updates popup "susan.walters@gmai.com" "selected" and added to the list of emails from the recorded values

  @C122-F01B2-35 @AC11.4 @permissions @manual
  Scenario: Add email address to Alerts and updates popup with no registered emails
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    When I open Communication Preferences page of employee "not subscribed" to alerts and "0" email addresses in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I select the "Send them an email" checkbox
    And I enter "@@@fineos.com" in the "Add an email" field
    When I click the OK button
    Then I see the invalid email error "Please type your employee’s email address in the format myname@domain.com"
    And I enter "bob.smith@@fineos.com" in the "Add an email" field
    When I click the OK button
    Then I do see the invalid email error "Please type your employee’s email address in the format myname@domain.com"
    And the Alerts and updates popup closes

  @C122-F01B2-36 @AC11.7 @manual
  Scenario: Deselect Send them an email checkbox in Alerts and updates popup with no preferred email address
    Given I log in to the application
    And I open personal details of employee "not subscribed" to alerts and at least "1" email addresses in the personal details tab and no preferred email address
    And I record the list of emails for comparison
    When I open Communication Preferences page of employee "not subscribed" to alerts
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    When I select the "Send them an email" checkbox
    Then I see the the recorded list of emails from the personal details tab with no emails selected
    And I select the "first" email address radio button
    When I deselect the "Send them an email" checkbox
    Then the list of emails is hidden
    When I select the "Send them an email" checkbox
    Then I see the the recorded list of emails from the personal details tab with no emails selected

  @C122-F01B2-37 @AC11.8 @automated @regression
  Scenario: Select Send them an SMS checkbox in Alerts and updates popup with preferred mobile phone number
    Given I log in to the application
    When I open personal details of employee "not subscribed to alerts via SMS" and at least 1 mobile phone numbers in the personal details tab and a preferred mobile phone number
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open "Communication Preferences" tab
    And I record the preferred phone number in the Written Correspondence for comparison
    And I click the "They want to receive alerts" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I "select" the "Send them an SMS" checkbox in "Alerts and updates"
    Then I see the the recorded list of "phone numbers" from the personal details tab
    And I see the preferred "phone number" is "marked" in "Alerts and updates"

  @C122-F01B2-38 @AC11.8 @automated @regression
  Scenario: Select Send them an SMS checkbox in Alerts and updates popup with no preferred mobile phone number
    Given I log in to the application
    When I open personal details of employee "not subscribed to alerts" and at least 1 phone number in the personal details tab and no preferred phone number
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open "Communication Preferences" tab
    And I click the "They want to receive alerts" link in Alerts, updates and reminders section
    When I "select" the "Send them an SMS" checkbox in "Alerts and updates"
    Then I see the the recorded list of "phone numbers" from the personal details tab
    And I see the preferred "phone number" is "not marked" in "Alerts and updates"


  @C122-F01B2-39 @AC11.8 @AC11.9 @AC11.10 @permissions @manual
  Scenario: Add mobile phone number to Alerts and updates popup with at least 1 registered mobile phone number
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    When I open personal details of employee "not subscribed" to alerts and at least "1" mobile phone numbers in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I "select" the "Send them an SMS" checkbox in "Alerts and updates"
    And I record the list of mobile phone numbers in the "Select a phone number" field for comparison
    When I click the "They want to use a different phone number" link under the "Select a phone number" section
    Then the Add new phone number popup opens
    And I see "Cell phone" in the disabled "Phone Contact Type" field
    And I enter "22" in the "Country code" field
    And I enter "333" in the "Area code" field
    And I enter "555777" in the "Phone number" field
    When I click the OK button
    And the Add new phone number popup closes
    Then I see on the Alerts and updates popup "22-333-555777" selected and added to the list of mobile phone numbers from the recorded values

  @C122-F01B2-40 @AC11.8 @permissions @manual
  Scenario: Add mobile phone number to Alerts and updates popup with no registered mobile phone numbers
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERSS_ADD"
    When I open personal details of employee "not subscribed" to alerts and "0" mobile phone numbers in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I "select" the "Send them an SMS" checkbox in "Alerts and updates"
    And I see "Cell phone" in the disabled "Phone Contact Type" field
    And I enter "@@-!!!-???%%%" in the "Add a phone number" field
    When I click the OK button
    Then I see the invalid phone number error "Please type your employee’s phone number in the format [International Code]-[Area Code]-[Telephone Number]"
    And I enter "11-222-333999" in the "Add a phone number" field
    When I click the OK button
    Then the Alerts and updates popup closes

  @C122-F01B2-41 @AC11.11 @automated @regression
  Scenario: Deselect Send them an SMS checkbox in Alerts and updates popup with no preferred mobile phone number
    Given I log in to the application
    When I open personal details of employee "not subscribed" and at least 2 mobile phone numbers in the personal details tab and no preferred mobile phone number
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open "Communication Preferences" tab
    And I click the "They want to receive alerts" link in Alerts, updates and reminders section
    Then the "Alerts and updates" popup opens
    When I "select" the "Send them an SMS" checkbox in "Alerts and updates"
    Then I see the the recorded list of "phone numbers" from the personal details tab
    And I select the "first" "phone number" radio button
    When I "deselect" the "Send them an SMS" checkbox in "Alerts and updates"
    Then the list of "phone number" is hidden
    When I "select" the "Send them an SMS" checkbox in "Alerts and updates"
    Then I see the the recorded list of "phone numbers" from the personal details tab
    And I see the preferred "phone number" is "not marked" in "Alerts and updates"

  @C122-F01B2-42 @AC11.12 @manual
  Scenario: Click OK after adding mobile phone number to Alerts and updates popup with at least 1 registered mobile phone number
    Given I log in to the application
    When I open personal details of employee "not subscribed" to alerts and at least "1" mobile phone numbers in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I select the "Send them an SMS" checkbox
    And I record the list of mobile phone numbers in the "Select a phone number" field for comparison
    When I click the "They want to use a different phone number" link under the "Select a phone number" section
    Then the Add new phone number popup opens
    And I see "Cell phone" in the disabled "Phone Contact Type" field
    And I enter "11" in the "Country code" field
    And I enter "333" in the "Area code" field
    And I enter "555777" in the "Phone number" field
    When I click the OK button
    And the Add new phone number popup closes
    Then I see on the Alerts and updates popup "11-333-555777" selected and added to the list of mobile phone numbers from the recorded values
    When I click the OK button
    And the Alerts and updates popup closes
    And the input values are sent to the carrier
    Then I see on the Alerts, updates and reminders section updated with the mobile phone number "11-333-555777"
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see "Your employee will be notified of updates and reminders relating to their requests and payments via SMS to 11-333-555777" on the Alerts, updates and reminders section

  @C122-F01B2-43 @AC11.12 @manual
  Scenario: Click OK after adding email address to Alerts and updates popup with at least 1 registered email
    Given I log in to the application
    When I open personal details of employee "not subscribed" to alerts and at least "1" email addresses in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I select the "Send them an email" checkbox
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    And I enter "steve.walters@gmai.com" in the Email field
    When I click the OK button
    And the Add new email popup closes
    Then I see on the Alerts and updates popup "steve.walters@gmail.com" selected and added to the list of emails from the recorded values
    When I click the OK button
    And the Alerts and updates popup closes
    And the input values are sent to the carrier
    Then I see on the Alerts, updates and reminders section updated with the email "steve.walters@gmail.com"
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see "Your employee will be notified of updates and reminders relating to their requests and payments via email to steve.walters@gmail.com" on the Alerts, updates and reminders section

  @C122-F01B2-44 @AC11.13 @manual
  Scenario Outline: Click Cancel/Close X on Alerts and updates popup
    Given I log in to the application
    When I open personal details of employee "not subscribed" to alerts and at least "1" email addresses in the personal details tab
    And I click the "They want to receive alerts" link in "Alerts, updates and reminder" section
    And the Alerts and updates popup opens
    And I select the "Send them an email" checkbox
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    And I enter "steve.walters@gmai.com" in the Email field
    When I click the OK button
    And the Add new email popup closes
    Then I see on the Alerts and updates popup "steve.walters@gmail.com" selected and added to the list of emails from the recorded values
    When I click the "<button>" button
    And the Alerts and updates popup closes
    Then I see on the Communication Preferences page no email on the  Alerts, updates and reminders section
    When I click the "My Dashboard" page link
    And I open Personal details page of the same employee
    Then I do not see on "steve.walters@gmail.com" added to the list of emails from the recorded values
    When I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page no email on the  Alerts, updates and reminders section

    Examples:
      | button  |
      | Cancel  |
      | Close X |

  @C122-F01B2-45 @AC12.1 @automated @sanity @regression
  Scenario: Open Written Correspondence popup via post
    Given I log in to the application
    When I open personal details of employee "subscribed to written correspondence via post"
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    When I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    And the Written Correspondence popup should contain the "selected" radio button "Send them paper correspondence via post"
    And the Written Correspondence popup should contain the "not selected" radio button "Send them correspondence only through the portal"

  @C122-F01B2-46 @AC12.1 @smoke @automated
  Scenario: Open Written Correspondence popup via the portal - email
    Given I log in to the application
    And I open personal details of employee who set written correspondence option "via the portal email"
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    When I open Communication Preferences of the same employee
    When I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    And the Written Correspondence popup should contain the "not selected" radio button "Send them paper correspondence via post"
    And the Written Correspondence popup should contain the "selected" radio button "Send them correspondence only through the portal"
    And the Written Correspondence popup should contain the "selected" radio button "Send them an email"
    And the Written Correspondence popup should contain the "not selected" radio button "Send them an SMS"
    And the Written Correspondence popup should contain the recorded selected preferred email address
    And the Written Correspondence popup should contain all registered emails addresses from the personal contact tab Emails section
    And the Written Correspondence popup should contain the buttons "OK, Cancel, Close X"

  @C122-F01B2-47 @AC12.1 @smoke @automated
  Scenario: Open Written Correspondence popup via the portal - SMS
    Given I log in to the application
    And I open personal details of employee who set written correspondence option "via the portal SMS"
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    And I record the list of Landline phone numbers for comparison of the Phone numbers section of the personal details tab
    And I record the list of Cell Phone phone numbers for comparison of the Phone numbers section of the personal details tab
    And I "accept" "Edit Phone numbers" popup
    And the "Edit Phone numbers" popup closes
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    When I open Communication Preferences of the same employee
    And I set communication via "sms"
    And I record the preferred phone number in the Written Correspondence for comparison
    When I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    And the Written Correspondence popup should contain the "not selected" radio button "Send them paper correspondence via post"
    And the Written Correspondence popup should contain the "selected" radio button "Send them correspondence only through the portal"
    And the Written Correspondence popup should contain the "not selected" radio button "Send them an email"
    And the Written Correspondence popup should contain the "selected" radio button "Send them an SMS"
    And the Written Correspondence popup should contain the recorded selected preferred phone number
    And the Written Correspondence popup should contain all registered mobile phone numbers from the personal contact tab Phone numbers section
    And the Written Correspondence popup should contain no registered landline phone numbers from the personal contact tab Phone numbers section
    And the Written Correspondence popup should contain the buttons "OK, Cancel, Close X"

  @C122-F01B2-48 @AC12.2 @automated @regression
  Scenario: Open Written Correspondence popup can select post or through the portal
    Given I log in to the application
    When I open personal details of employee who set written correspondence option "subscribed to written correspondence via post"
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open "Communication Preferences" tab
    When I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    And I select the "Send them paper correspondence via post" radio button
    And the Written Correspondence popup should contain the "selected" radio button "Send them paper correspondence via post"
    And the Written Correspondence popup should contain the "not selected" radio button "Send them correspondence only through the portal"
    When I select the "Send them correspondence only through the portal" radio button
    Then the Written Correspondence popup should contain the "not selected" radio button "Send them paper correspondence via post"
    And the Written Correspondence popup should contain the "selected" radio button "Send them correspondence only through the portal"


  @C122-F01B2-49 @AC12.3 @automated @regression
  Scenario: Open Written Correspondence popup can select receive correspondence either by email or SMS
    Given I log in to the application
    When I open personal details of employee who set written correspondence option "subscribed to written correspondence via post"
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I open "Communication Preferences" tab
    When I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    When I select the "Send them correspondence only through the portal" radio button
    Then the Written Correspondence popup should contain the "not selected" radio button "Send them an email"
    And the Written Correspondence popup should contain the "not selected" radio button "Send them an SMS"
    When I select the "Send them an email" checkbox in Written correspondence
    Then the Written Correspondence popup should contain the "selected" radio button "Send them an email"
    And the Written Correspondence popup should contain the "not selected" radio button "Send them an SMS"
    When I select the "Send them an SMS" checkbox in Written correspondence
    Then the Written Correspondence popup should contain the "not selected" radio button "Send them an email"
    And the Written Correspondence popup should contain the "selected" radio button "Send them an SMS"


  @C122-F01B2-50 @AC12.4 @12.5 @manual
  Scenario: Select Send them an email radio button in Written Correspondence popup with preferred email address and can choose from the email addresses
    Given I log in to the application
    And I open personal details of employee with written correspondence "via post" and at least "1" email addresses in the personal details tab and a preferred email address
    And I record the list of emails for comparison
    And I open Communication Preferences page of the same employee
    And I record the preferred email address in the Alerts, updates and reminders for comparison
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    When I select the "Send them an email" radio button
    Then I see the Email section with recorded list of emails from the personal details tab with the recorded "selected" preferred email address
    When I select the "first" unselected email address radio button
    Then I see the "first" unselected email address radio button becomes selected
    Then I see the recorded preferred email address is "not selected"

  @C122-F01B2-51 @AC12.4 @12.5 @12.8 @manual
  Scenario: Select Send them an email radio button in Written Correspondence popup with no preferred email address and can choose from the email addresses
    Given I log in to the application
    And I open personal details of employee with written correspondence "via SMS" and at least "1" email addresses and "1" phone number in the personal details tab and no preferred email address
    And I record the list of emails for comparison
    And I open Communication Preferences page of the same employee
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I see the SMS section with list of phone numbers with 1 phone number selected
    When I select the "Send them an email" radio button
    Then I see the Email section with recorded list of emails from the personal details tab with no emails selected
    And I see the SMS section with list of phone numbers collapse
    When I select the "first" unselected email address radio button
    Then I see the "first" unselected email address radio button becomes selected
    When I select the "Send them an phone number" radio button
    Then I see the Email section with list of emails collapse
    And I see the SMS section with list of phone numbers with no phone number selected
    When I selected "first" unselected phone number radio button
    Then I see the "first" unselected phone number radio button becomes selected
    When I select the "Send them an email" radio button
    Then I see the Email section with recorded list of emails from the personal details tab with no emails selected
    And I see the SMS section with list of phone numbers collapse

  @C122-F01B2-52 @AC12.4 @12.5 @AC12.6 @12.7 @permissions @manual
  Scenario: Add email address to Written Correspondence popup with at least 1 registered email
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    When I open Communication Preferences page of employee with written correspondence "via post" and at least "1" email addresses in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    When I select the "Send them an email" radio button
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    And I enter "susan.walters@gmai.com" in the Email field
    When I click the OK button
    And the Add new email popup closes
    Then I see on the Written Correspondence popup "susan.walters@gmai.com" "selected" and added to the list of emails from the recorded values

  @C122-F01B2-53 @AC12.4 @12.5 @AC12.6 @12.7 @permissions @manual
  Scenario: Add email address to Written Correspondence popup  with no registered emails
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    When I open Communication Preferences page of employee with written correspondence "via post" and "0" email addresses in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    When I select the "Send them an email" radio button
    And I enter "@@@fineos.com" in the "Add an email" field
    When I click the OK button
    Then I see the invalid email error "Please type your employee’s email address in the format myname@domain.com"
    And I enter "bob.smith@fineos.com" in the "Add an email" field
    When I click the OK button
    Then the Written Correspondence popup closes

  @C122-F01B2-54 @AC12.8 @manual
  Scenario Outline: Select Send them an phone radio button in Written Correspondence popup with preferred mobile phone number in Phone Calls or Alerts
    Given I log in to the application
    And I open personal details of employee with written correspondence "via post" and at least "1" mobile phone numbers in the personal details tab and a "<section>" preferred mobile phone number
    And I record the list of mobile phone numbers for comparison
    And I open Communication Preferences page of the same employee
    And I record the preferred mobile phone number in the "<section>" for comparison
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    When I select the "Send them an phone number" radio button
    Then I see the SMS section with recorded list of mobile phone numbers from the personal details tab with the recorded "selected" preferred phone number

    Examples:
      | section                       |
      | Phone Calls                   |
      | Alerts, updates and reminders |

  @C122-F01B2-55 @AC12.8 @automated @regression
  Scenario: Select Send them an phone radio button in Written Correspondence popup with preferred landline phone number in Phone Calls
    Given I log in to the application
    When I open personal details of employee "subscribed to written correspondence via post and a preferred landline phone number"
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    And the Employee has at least 1 "Landline" "Phone numbers"
    And the Employee has at least 1 "Cell Phone" "Phone numbers"
    And I record the list of Landline phone numbers for comparison of the Phone numbers section of the personal details tab
    And I record the list of Cell Phone phone numbers for comparison of the Phone numbers section of the personal details tab
    And I "accept" "Edit Phone numbers" popup
    And the "Edit Phone numbers" popup closes
    When I open "Communication Preferences" tab
    And I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    When I select the "Send them correspondence only through the portal" radio button
    And I select the "Send them an SMS" checkbox in Written correspondence
    Then the Written Correspondence popup should contain no registered landline phone numbers from the personal contact tab Phone numbers section
    And I see the preferred "phone number" is "not marked" in "Written correspondence"


  @C122-F01B2-56 @AC12.8 @12.9 @12.10 @permissions @manual
  Scenario: Add phone number to  Written Correspondence popup with at least 1 registered mobile phone number
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    When I open Communication Preferences page of employee with written correspondence "via post" and at least "1" mobile phone number in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    When I select the "Send them a SMS" radio button
    And I record the list of phone numbers in the "Select a phone number" field for comparison
    When I click the "They want to use a different phone number" link under the "Select a phone number" section
    Then the Add new phone number popup opens
    And I see "Cell phone" in the disabled "Phone Contact Type" field
    And I enter "22" in the "Country code" field
    And I enter "333" in the "Area code" field
    And I enter "555777" in the "Phone number" field
    When I click the OK button
    And the Add new phone number popup closes
    Then I see on the Written Correspondence popup "22-333-555777" selected and added to the list of mobile phone numbers from the recorded values

  @C122-F01B2-57 @AC12.8 @permissions @manual
  Scenario: Add email address to Written Correspondence popup with no registered mobile phone numbers
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    When I open Communication Preferences page of employee with written correspondence "via post" and "0" phone numbers in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    When I select the "Send them a phone number" radio button
    And I see "Cell phone" in the disabled "Phone Contact Type" field
    And I enter "@@-!!!-???%%%" in the "Add a phone number" field
    When I click the OK button
    Then I see the invalid phone number error "Please type your employee’s phone number in the format [International Code]-[Area Code]-[Telephone Number]"
    And I enter "11-222-333999" in the "Add a phone number" field
    When I click the OK button
    Then the Written Correspondence  popup closes

  @C122-F01B2-58 @AC12.11 @automated @regression
  Scenario: Check OK button validation in in Written Correspondence popup with emails and phone numbers
    Given I log in to the application
    And I open personal details of employee "of employee subscribed to written correspondence via post with not set phone calls"
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    And the Employee has at least 2 "Any" "Phone numbers"
    And I "accept" "Edit Phone numbers" popup
    And the "Edit Phone numbers" popup closes
    And I click the "Edit" link under the "Emails" section
    And the Employee has "at least" 2 "Emails"
    And I "accept" 'Edit emails' popup
    And the 'Edit emails' popup closes
    When I open "Communication Preferences" tab
    And I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    When I select the "Send them correspondence only through the portal" radio button
    And I select the "Send them an email" checkbox in Written correspondence
    And I "accept" "Written correspondence" time modal
    Then I can see a email validation message
    When I select the "first" "email address" radio button
    Then I can't see a email validation message
    When I select the "Send them an SMS" checkbox in Written correspondence
    And I "accept" "Written correspondence" time modal
    Then I can see a phone validation message
    When I select the "first" "phone number" radio button
    Then I can't see a phone validation message


  @C122-F01B2-59 @AC12.11 @automated @regression
  Scenario: Check OK button validation in in Written Correspondence popup with no emails and no phone numbers
    Given I log in to the application
    When I open personal details of employee "subscribed to written correspondence via post" and 0 email addresses and 0 mobile phone numbers in the personal details tab
    And I open "Employee Details" tab
    And I open "Communication Preferences" tab
    And I click the "Edit preference" link in Written Correspondence section
    Then the "Written correspondence" popup opens
    When I select the "Send them correspondence only through the portal" radio button
    And I "accept" "Written correspondence" time modal
    Then I see a validation message "Please select at least one option" is "visible"
    When I select the "Send them an email" checkbox in Written correspondence
    Then I see a validation message "Please select at least one option" is "not visible"
    And I see a validation message "Required" is "visible" for field "email"
    When I enter "email@example.com" test data in last email text box
    Then I see a validation message "Required" is "not visible" for field "email"
    When I select the "Send them an SMS" checkbox in Written correspondence
    Then I see a validation message "Please select at least one option" is "not visible"
    And I see a validation message "Required" is "visible" for field "area code"
    And I see a validation message "Required" is "visible" for field "phone no"
    When I enter random number in the "area code" field
    And I enter random number in the "phone number" field
    Then I see a validation message "Required" is "not visible" for field "area code"
    And I see a validation message "Required" is "not visible" for field "phone no"

  @C122-F01B2-60 @AC12.12 @manual
  Scenario: Click OK after adding email address to Written Correspondence popup with at least 1 registered email
    Given I log in to the application
    And I open Communication Preferences page of employee with written correspondence "via post" and at least "1" email addresses in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    And I select the "Send them an email" radio button
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    And I enter "steve.walters@gmai.com" in the Email field
    When I click the OK button
    And the Add new email popup closes
    Then I see on the Written Correspondence popup "steve.walters@gmail.com" selected and added to the list of emails from the recorded values
    When I click the OK button
    And the Written Correspondence popup closes
    And the input values are sent to the carrier
    Then I see on the Written Correspondence section updated with the email "steve.walters@gmail.com"
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page the email "steve.walters@gmail.com" on the Written Correspondence section

  @C122-F01B2-61 @AC12.12 @manual
  Scenario: Click OK after adding mobile phone number to Written Correspondence popup with at least 1 mobile phone numbers
    Given I log in to the application
    And I open Communication Preferences page of employee with written correspondence "via post" and at least "1" mobile phone numbers in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    And I select the "Send them an SMS" radio button
    And I record the list of mobile phone numbers in the "Select a phone number" field for comparison
    When I click the "They want to use a different phone number" link under the "Select phone number" section
    Then the Add new phone number popup opens
    And I see "Cell phone" in the disabled "Phone Contact Type" field
    And I enter "11" in the "Country code" field
    And I enter "333" in the "Area code" field
    And I enter "555777" in the "Phone number" field
    When I click the OK button
    And the Add new phone number popup closes
    Then I see on the Written Correspondence popupp "11-333-555777" selected and added to the list of mobile phone numbers from the recorded values
    When I click the OK button
    And the Written Correspondence popup closes
    And the input values are sent to the carrier
    Then I see on the Communication Preferences page the mobile phone number "11-333-555777" on the Written Correspondence section section
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page the mobile phone number "11-333-555777" on the Written Correspondence section section

  @C122-F01B2-62 @AC12.13 @manual
  Scenario Outline: Click Cancel/Close X on Written Correspondence popup
    Given I log in to the application
    And I open Communication Preferences page of employee with written correspondence "via post" and at least "1" email addresses in the personal details tab
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence through the portal" radio button
    And I select the "Send them an email" radio button
    And I record the list of emails in the "Select an email" field for comparison
    When I click the "They want to use a different email" link under the "Select an email" section
    Then the Add new email popup opens
    And I enter "steve.walters@gmai.com" in the Email field
    When I click the OK button
    And the Add new email popup closes
    Then I see on the Written Correspondence popup "steve.walters@gmail.com" selected and added to the list of emails from the recorded values
    When I click the "<button>" button
    And the Written Correspondence popup closes
    Then I see on the Communication Preferences page no email on the Written Correspondence popup section
    When I click the "My Dashboard" page link
    And I open Personal details page of the same employee
    Then I do not see on "steve.walters@gmail.com" added to the list of emails from the recorded values
    When I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page no email on the Written Correspondence section

    Examples:
      | button  |
      | Cancel  |
      | Close X |

  @C122-F01B2-63 @AC12.11 @manual
  Scenario: Switching from via portal to via post on the Written Correspondence
    Given I log in to the application
    And I open Communication Preferences page of employee with written correspondence "via portal" and "1" preferred mobile phone numbers
    And I click the "Edit preference" link in "Written Correspondence" section
    And the Written Correspondence popup opens
    And I select the "Send them correspondence via post" radio button
    When I click the OK button
    Then the Written Correspondence popup closes
    And the input values are sent to the carrier
    And I see on the Communication Preferences page "Your employee is currently set to receive paper correspondence from us via post" on the Written Correspondence popup section
    When I click the "My Dashboard" page link
    And I open Communication Preferences page of the same employee
    Then I see on the Communication Preferences page "Your employee is currently set to receive paper correspondence from us via post" on the Written Correspondence popup section
