@employer-portal @C122-F01B
Feature: C122-F01 - Update employee Contact Details part B

  @C122-F01B-01 @AC1.1 @permissions @manual
  Scenario Outline: Permission to edit basic information
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMERINFO_EDIT"
    When I open Employee Details page
    Then I can see the "Edit" link under the "Name, Date of Birth, Address" section is "<visibility>" on the Personal Details tab

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B-02 @AC1.2 @permissions @manual
  Scenario Outline: Permission to edit email address of an employee 1 or more emails
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_EDIT"
    When I open personal details of employee with at least "1" email addresses
    Then I can see the "Edit" link under the "Email(s)" section is "<visibility>" on the Personal Details tab

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B-03 @AC1.2 @permissions @manual
  Scenario Outline: Permission to edit email address of an employee with no emails
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    When I open personal details of employee with "0" email addresses
    Then I can see the "Add email" link under the "Email(s)" section is "<visibility>" on the Personal Details tab

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B-04 @AC1.3 @permissions @manual
  Scenario Outline: Permission to edit new phone number with 1 or more phone numbers
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_EDIT"
    When I open personal details of employee with at least "1" phone numbers
    Then I can see the "Edit" link under the "Phone number(s)" section is "<visibility>" on the Personal Details tab

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B-05 @AC1.3  @permissions @manual
  Scenario Outline: Permission to edit new phone number with no phone numbers
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    When I open personal details of employee with "0" phone numbers
    Then I can see the "Edit" link under the "Phone number(s)" section is "<visibility>" on the Personal Details tab

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C122-F01B-06  @AC2.1 @AC2.2 @AC2.3
  Scenario Outline: Open the Edit person details popup
    Given I log in to the application
    When I open Employee details page "<location>" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And the 'Edit personal details' popup contains following "editable" fields "First name, Last name, Date of birth"
    And the 'Edit personal details' popup contains following "<isEditable>" fields "<addressFields>"

    @smoke @automated
    Examples:
      | location             | isEditable | addressFields                                                                  |
      | of employee from USA | editable   | Country, Address line 1, Address line 2, Address line 3, City, State, Zip code |

    @automated @regression
    Examples:
      | location                 | isEditable | addressFields                                                                                                                        |
      | of employee from Canada  | editable   | Country, Address line 1, Address line 2, Address line 3, City, Province, Postal code                                                 |
      | of employee from Ireland | editable   | Country, Address line 1, Address line 2, Address line 3, Address line 4, Address line 5, Address line 6, Address line 7, Postal code |

  @C122-F01B-07 @AC2.4 @automated @regression
  Scenario: Click states to see dropdown of all valid state codes for 51 states and 14 territories
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I click "State" field
    Then I see a dropdown list of "63" "State" codes with a 2 characters abbreviation

  @C122-F01B-07B  @AC2.4 @automated @regression
  Scenario: Click provinces to see dropdown of all valid province codes for 10 provinces and 3 territories
    Given I log in to the application
    When I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I select "Canada" test data in the "Country" field
    Then the 'Edit personal details' popup contains following "editable" fields "First name, Last name, Date of birth"
    When I click "Province" field
    Then I see a dropdown list of "13" "Province" codes with a 2 characters abbreviation
    And the 'Edit personal details' popup contains following "editable" fields "Country, Address line 1, Address line 2, Address line 3, City, Province, Postal code"

  @C122-F01B-08 @AC2.5 @regression @automated
  Scenario Outline: Click Cancel/Close X on Edit person details popup
    Given I log in to the application
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    And I "<button>" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I verify that fields on Edit Personal Details page are not updated

    Examples:
      | button  |
      | cancel  |
      | close X |

  @smoke @automated
  @C122-F01B-09 @AC2.6 @sanity
  Scenario: Edit person details with valid data of employee with US address
    Given I log in to the application
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field

  @C122-F01B-10 @AC2.6 @regression @automated
  Scenario: Edit person details with valid data of employee with Canadian address
    Given I log in to the application
    And I open Employee details page "of employee from Canada" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "Province" field
    And I enter random test data in the "Postal code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "Province, Postal code" data is saved in the 'Personal Details' tab in the "Address" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from Canada" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "Province, Postal code" data is saved in the 'Personal Details' tab in the "Address" field


  @C122-F01B-10B @AC2.6 @automated @regression
  Scenario: Edit person details with valid data of employee with Irish address
    Given I log in to the application
    And I open Employee details page "of employee from Ireland" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "Address line 4" field
    And I enter random test data in the "Address line 5" field
    And I enter random test data in the "Address line 6" field
    And I enter random test data in the "Address line 7" field
    And I enter random test data in the "Postal code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3, Address line 4, Address line 5, Address line 6" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "Postal code" data is saved in the 'Personal Details' tab in the "Address" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from Ireland" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3, Address line 4, Address line 5, Address line 6" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "Postal code" data is saved in the 'Personal Details' tab in the "Address" field


  @C122-F01B-11 @AC2.6 @automated @regression
  Scenario: Edit person details with invalid data of employee with US address
    Given I log in to the application
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I type "Apr 21, 2040" value into "Date of birth" field
    Then I see "Date of birth" field was not changed

  @C122-F01B-11B @AC2.6 @automated @regression
  Scenario: Edit person details with invalid postal code of employee with Canadian address
    Given I log in to the application
    And I open Employee details page "of employee from Canada" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I type "12345" value into "Postal code" field
    And I "accept" 'Edit Personal Details' popup
    Then I see "Postal code should be of the format A9A9A9." error message for the invalid field values of "Postal code" on the Edit personal details popup
    When I "cancel" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I verify that fields on Edit Personal Details page are not updated

  @C122-F01B-12 @AC2.7 @automated @regression
  Scenario Outline: Edit person details with missing required data from USA
    Given I log in to the application
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I clear "<fieldName>" field
    And I "accept" 'Edit Personal Details' popup
    Then the "Required" error is "visible" under the "<fieldName>" field
    When I enter random test data in the "<fieldName>" field
    Then the "Required" error is "not visible" under the "<fieldName>" field
    When I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes

    Examples:
      | fieldName      |
      | First name     |
      | Last name      |
      | Date of birth  |
      | Address line 1 |
      | City           |
      | Zip code       |


  @C122-F01B-12B @AC2.7 @automated @regression
  Scenario Outline: Edit person details with missing required data from Canada
    Given I log in to the application
    And I open Employee details page "of employee from Canada" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I clear "<fieldName>" field
    And I "accept" 'Edit Personal Details' popup
    Then the "Required" error is "visible" under the "<fieldName>" field
    When I enter random test data in the "<fieldName>" field
    Then the "Required" error is "not visible" under the "<fieldName>" field
    When I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes

    Examples:
      | fieldName      |
      | First name     |
      | Last name      |
      | Date of birth  |
      | Address line 1 |
      | City           |
      | Postal code    |

  @C122-F01B-12C @AC2.7 @automated @regression
  Scenario Outline: Edit person details with missing required data from Ireland
    Given I log in to the application
    And I open Employee details page "of employee from Ireland" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I see 'Personal details' data in edit mode is the same like in display mode
    When I clear "<fieldName>" field
    And I "accept" 'Edit Personal Details' popup
    Then the "Required" error is "visible" under the "<fieldName>" field
    When I enter random test data in the "<fieldName>" field
    Then the "Required" error is "not visible" under the "<fieldName>" field
    When I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes

    Examples:
      | fieldName      |
      | First name     |
      | Last name      |
      | Date of birth  |
      | Address line 1 |
      | Postal code    |


  @C122-F01B-13 @AC3.1 @automated @regression
  Scenario: Open the Edit emails popup with list of email addresses
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Emails" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "at least" 1 "Email"
    Then the Edit emails should contain list of email addresses from the recorded values in editable "Email" fields

  @C122-F01B-14 @AC3.2 @automated @regression
  Scenario: Open the Edit emails popup with 0 email addresses
    Given I log in to the application
    When I open Employee details page "of employee without email address" using EmployeeId
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    And I click the "Add" link under the "Emails" section
    Then the "Add" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "exactly" 0 "Emails"


  @C122-F01B-15 @AC3.3 @AC3.4 @permissions @manual
  Scenario Outline: Permission to delete email address
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_REMOVE"
    And I open personal details of employee with at least "<NumberOfEmails>" email addresses
    And I click the "Edit" link under the "Email(s)" section
    When the Edit emails popup opens
    Then I can see beside each email address a delete icon is "<visibility>"

    Examples:
      | NumberOfEmails | withOrWithout | visibility  |
      | 2              | with          | visible     |
      | 1              | with          | not visible |
      | 2              | without       | not visible |


  @C122-F01B-17 @AC3.6  @permissions @manual
  Scenario: Edit user with no emails
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    And I open personal details of employee with "0" email addresses
    When I click the "Add email" link under the "Email(s)" section
    Then the Edit emails popup opens
    And the Edit emails popup should contain the editable text box "Email" field
    And the Edit emails popup should contain the title "Add email"

  @C122-F01B-18 @AC3.6 @permissions @manual
  Scenario: Edit user with 1 or more emails
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_EMAILADDRESSES_ADD"
    And I open personal details of employee with at least "1" email addresses
    When I click the "Edit" link under the "Email(s)" section
    Then the Edit emails popup opens
    And the Edit emails popup should contain "+Add another" link under the list of emails

  @C122-F01B-19 @AC3.7 @automated @regression
  Scenario: Type email address when no email addresses shows add another link
    Given I log in to the application
    When I open Employee details page "of employee without email address" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Add" link under the "Emails" section
    Then the "Add" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "exactly" 0 "Emails"
    When I enter random test data in random email text box
    Then I see the "+Add another" link is "enabled" under the email text box field

  @C122-F01B-20 @AC3.8 @automated @regression
  Scenario: Click add another link shows new text box
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "at least" 1 "Email"
    When I click the "+Add another" email link
    Then I see another the email text box field added to the list of emails


  @C122-F01B-21 @AC3.9 @automated @regression
  Scenario Outline: Click Cancel/Close X on edit email popup
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I enter random test data in random email text box
    And I "<button>" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I verify that Email fields on Edit Personal Details page are not updated

    Examples:
      | button  |
      | cancel  |
      | close X |

  @C122-F01B-22 @AC3.10 @automated @regression
  Scenario: Click OK button on edit emails popup after adding correct formatted email address
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I enter random test data in random email text box
    Then Invalid email error messages is "not visible"
    When I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field


  @smoke @automated
  @C122-F01B-23 @AC3.10
  Scenario: Click OK button on edit emails popup after editing correct formatted email address - random email
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Emails" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "at least" 3 "Emails"
    When I enter random test data in random email text box
    Then Invalid email error messages is "not visible"
    When I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field

  @C122-F01B-23-01 @AC3.10 @automated
  Scenario Outline: Click OK button on edit emails popup after editing correct formatted email address
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Emails" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "at least" 3 "Emails"
    When I enter "<emailAddress>" test data in last email text box
    Then Invalid email error messages is "not visible"
    When I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see that "<emailAddress>" data is saved in the 'Personal Details' tab in the "Email(s)" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    Then I see that "<emailAddress>" data is saved in the 'Personal Details' tab in the "Email(s)" field

    @regression
    Examples:
      | emailAddress                   |
      | email@subdomain.example.com    |
      | firstname+lastname@example.com |
      | 1234567890@example.com         |
      | email@example-one.com          |
      | _______@example.com            |
      | email@example.name             |
      | email@example.museum           |
      | email@example.co.jp            |
      | firstname-lastname@example.com |


  @C122-F01B-24 @AC3.5 @AC3.10 @automated @regression
  Scenario: Click OK button on edit emails popup after deleting email address
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I record the list of email address for comparison of the Emails section of the personal details tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Emails" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "at least" 2 "Emails"
    When I click the delete icon beside random email address
    Then I see the deleted email address is removed from the list of emails on the Edit emails popup
    When I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see the deleted email address is removed from the list of "Email(s)" on the 'Personal Details' tab
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    Then I see the deleted email address is removed from the list of "Email(s)" on the 'Personal Details' tab


  @C122-F01B-25 @AC3.10 @manual
  Scenario: Click OK button on edit emails popup after deleting email address and disconnecting the internet connection
    Given I log in to the application
    And I open personal details of employee with at least "1" email addresses
    And I click the "Edit" link under the "Email(s)" section
    And the Edit emails popup opens
    And I click the delete icon beside any email address
    And I see the deleted email address is removed from the list of emails on the Edit emails popup
    And I click the "OK" button
    And the deleted email address is sent to the carrier
    And the Edit emails popup closes
    When I disconnect the internet connection
    Then I see on the Personal Details tab the error "We were not able to update all the information that you requested to change. Please try again in a few minutes"

  @C122-F01B-26 @AC3.11 @automated @regression
  Scenario Outline: Click OK after editing incorrect formatted email address
    Given I log in to the application
    When I open Employee details page "of employee with email addresses" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Emails" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    And the Employee has "at least" 1 "Email"
    When I enter "<emailAddress>" test data in last email text box
    And I "accept" 'Edit emails' popup
    Then Invalid email error messages is "visible"

    Examples:
      | emailAddress                  |
      | plainaddress                  |
      | #@%^%#$@#$@#.com              |
      | @example.com                  |
      | Joe Smith <email@example.com> |
      | email.example.com             |
      | email@example@example.com     |
      | あいうえお@example.com             |
      | email@example.com (Joe Smith) |
      | email@example                 |
      | email@111.222.333.44444       |
      | .email@example.com            |
      | email.@example.com            |
      | email..email@example.com      |
      | email@example..com            |
      | Abc..123@example.com          |

  @C122-F01B-27 @AC4.1 @automated @regression
  Scenario: Open the Edit phone numbers popup with list of phone numbers
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Edit "Phone numbers" popup for each phone number item should contain the editable "Country code,Area code,Phone number" fields
    And the Edit "Phone numbers" popup for each phone number item should contain the editable "Phone Contact Type" select
    And the Edit "Phone numbers" popup labels "have" "*" on the fields "Area code,Phone number"
    And the Edit "Phone numbers" popup labels "do not have" "*" on the fields "Country code"
    And the "Phone numbers" should contain list of phone numbers from the recorded values in editable "Phone numbers" fields


  @C122-F01B-28 @AC4.2 @automated @regression @sanity
  Scenario: Open the edit new phone number popup with no phone
    Given I log in to the application
    When I open Employee details page "of employee without phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Add" link under the "Phone Numbers" section
    Then the "Add" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Edit "Phone numbers" popup for each phone number item should contain the editable "Country code,Area code,Phone number" fields
    And the Edit "Phone numbers" popup for each phone number item should contain the editable "Phone Contact Type" select
    And the Edit "Phone numbers" popup labels "have" "*" on the fields "Area code,Phone number"
    And the Edit "Phone numbers" popup labels "do not have" "*" on the fields "Country code"

  @C122-F01B-29 @AC4.3 @automated @regression @sanity
  Scenario: Open the edit new phone number popup
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Employee has at least 1 "Any" "Phone number"
    When I click "Phone Contact Type" select
    Then the dropdown should contain "Landline" and "Cell Phone"

  @C122-F01B-30 @AC4.4 @AC4.5 @permissions @manual
  Scenario Outline: Permission to delete phone numbers
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_REMOVE"
    And I open personal details of employee with at least "<NumberOfPhoneNumbers>" phone numbers
    And I click the "Edit" link under the "Phone Numbers(s)" section
    When the Edit phone numbers popup opens
    Then I can see beside each phone number a delete icon is "<visibility>"

    Examples:
      | NumberOfPhoneNumbers | withOrWithout | visibility  |
      | 2                    | with          | visible     |
      | 1                    | with          | not visible |
      | 2                    | without       | not visible |

  @C122-F01B-31 @AC4.6 @4.10 @automated @regression
  Scenario: Click delete icon on list of phone numbers
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Employee has at least 4 "Any" "Phone numbers"
    When I click the delete icon beside random phone number
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I see the deleted phone number is removed from the list of "Phone number(s)" on the "Personal Details" tab
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    Then I see the deleted phone number is removed from the list of "Phone number(s)" on the "Personal Details" tab

  @C122-F01B-32 @AC4.7 @permissions @manual
  Scenario Outline: Edit user with no phone numbers
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    And I open personal details of employee with "0" phone numbers
    And I click the "Add phone number" link under the "Phone Numbers(s)" section
    When the Edit phone numbers popup opens
    Then the Edit phone number popup phone number item should contain the empty editable "<fields>" fields

    Examples:
      | fields                                                    |
      | Phone Contact Type, Country code, Area code, Phone number |

  @C122-F01B-33 @AC4.7 @permissions @manual
  Scenario: Edit user with 1 or more phone numbers
    Given I log in to the application as a user "with" permissions to "URL_POST_GROUPCLIENT_CUSTOMERS_PHONENUMBERS_ADD"
    And I open personal details of employee with at least "1" phone numbers
    And I click the "Edit" link under the "Phone Numbers(s)" section
    Then the Edit phone numbers popup opens
    And the Edit phone numbers popup should contain "+Add another" link under the list of phone numbers

  @C122-F01B-34 @AC4.8 @automated @regression
  Scenario: Type enter or tab after entering new phone number to show Add another link
    Given I log in to the application
    When I open Employee details page "of employee without phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Add" link under the "Phone Numbers" section
    Then the "Add" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And I enter random number in the "random" "Area code, Phone number" fields and select "Landline" phone type
    Then I see the "+Add another" link is "enabled" under the phone number text box field
    When I clear "Area code" field
    Then I see the "+Add another" link is "disabled" under the phone number text box field
    When I enter random number in the "random" "Area code" fields and select "Landline" phone type
    Then I see the "+Add another" link is "enabled" under the phone number text box field


  @C122-F01B-35 @AC4.8 @automated @regression
  Scenario: Click +Add another link on edit phone numbers
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Employee has at least 1 "Any" "Phone number"
    When I click the "+Add another" phone number link
    Then I see another the phone number empty text boxes fields added to the list of phone numbers
    And the Edit "Phone numbers" popup for each phone number item should contain the editable "Country code,Area code,Phone number" fields
    And the Edit "Phone numbers" popup for each phone number item should contain the editable "Phone Contact Type" select


  @C122-F01B-36 @AC4.9 @automated @regression
  Scenario Outline: Click Cancel/Close X on edit phone number popup
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I record the Phone Numbers field value on the Personal Details tab for comparison
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Employee has at least 1 "Any" "Phone number"
    When I enter random number in the "random" "Country code, Area code, Phone number" fields and select "Cell Phone" phone type
    And I "<button>" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I verify that "Phone number(s)" fields on Edit Personal Details page are not updated

    Examples:
      | button  |
      | cancel  |
      | close X |

  @C122-F01B-37 @AC4.10 @automated @regression
  Scenario Outline: Click OK after adding a new phone number
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    And the Employee has at least 3 "Any" "Phone numbers"
    When I click the "+Add another" phone number link
    And I enter random number in the "last" "Country code, Area code, Phone number" fields and select "<phoneContactType>" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I see that generated "Country code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Area code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Phone number" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "Country code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Area code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Phone number" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    When I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I click the delete icon beside random phone number
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes

    Examples:
      | phoneContactType |
      | Landline         |
      | Cell Phone       |

  @C122-F01B-38 @AC4.10 @automated
  Scenario Outline: Click OK on the edit phone number popup with valid data
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When the Employee has at least 3 "Any" "Phone numbers"
    And I enter random number in the "random" "Country code, Area code, Phone number" fields and select "<phoneContactType>" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I see that generated "Country code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Area code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Phone number" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    When I click the "My Dashboard" page link
    And I open Employee details page "of employee from USA" using EmployeeId
    And I open "Employee Details" tab
    Then I see that generated "Country code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Area code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Phone number" data is saved in the 'Personal Details' tab in the "Phone number(s)" field

    @smoke
    Examples:
      | phoneContactType |
      | Landline         |

    @regression
    Examples:
      | phoneContactType |
      | Cell Phone       |

  @C122-F01B-39 @AC4.10 @automated @regression
  Scenario Outline: Click OK on the edit phone number popup with invalid data
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When the Employee has at least 3 "Any" "Phone numbers"
    And I enter "<invalidFieldValue>" test data in last "<fieldLabel>" text box
    And I "accept" "Edit Phone numbers" popup
    Then "not numeric" phone number error messages is "visible"
    When  I enter "<validFieldValue>" test data in last "<fieldLabel>" text box
    Then "not numeric" phone number error messages is "not visible"
    When I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes

    Examples:
      | fieldLabel   | invalidFieldValue | validFieldValue |
      | Phone number | badtext!@#!@      | 09870987        |
      | Country code | is$               | 12              |
      | Area code    | this@             | 445             |


  @C122-F01B-40 @AC4.11 @automated @regression
  Scenario Outline: Edit phone number with missing required data
    Given I log in to the application
    When I open Employee details page "of employee with phone numbers" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When the Employee has at least 3 "Any" "Phone numbers"
    And I enter "<invalidFieldValue>" test data in last "<fieldLabel>" text box
    And I "accept" "Edit Phone numbers" popup
    Then "required" phone number error messages is "visible"
    When  I enter "<validFieldValue>" test data in last "<fieldLabel>" text box
    Then "required" phone number error messages is "not visible"
    When I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes

    Examples:
      | fieldLabel   | invalidFieldValue | validFieldValue |
      | Phone number |                   | 09870987        |
      | Area code    |                   | 445             |
