@employer-portal @C115-F10
Feature: C115-F10 - Accommodation detail card (in-notification)

  @C115-F10-01 @accommodation-card
  Scenario Outline: Accommodation card visibility
    Given I log in to the application
    When I open notification with "<case>"
    Then I verify that the 'Workplace Accommodation' card is "<visibility>"

    @smoke @automated
    Examples:
      | case                     | visibility |
      | Child Accommodation Case | visible    |

    @regression @automated
    Examples:
      | case                        | visibility  |
      | no Child Accommodation Case | not visible |

  @C115-F10-02 @accommodation-card @smoke @automated @sanity @FAPI-9708-AC1
  Scenario: Accommodation card - default list view
    Given I log in to the application
    When I open notification with "Child Accommodation Case" and test data "@C115-F10-01-TD-2"
    Then the notification summary card does not contain additional field "Expected return to work"
    And I "can" see list of collapsed items in "Workplace Accommodation" card
    And each collapsed item on 'Workplace Accommodation' card has "Notification Date, Status, Graphical Status Indicator" fields
    When I expand "Workplace Accommodation" details for first item
    Then I see first expanded "Workplace Accommodation" item has "Created on, Managed by, Phone number, Limitation(s), Notified by" details
    And I see first expanded "Workplace Accommodation" item has 'status text' and 'graphical status indicator'

  @C115-F10-03 @accommodation-card @manual
  Scenario Outline: Accommodation status mapping
    Given I log in to the application
    When I open notification with "Accommodation Case" in "<phase>" phase and "<stage>" stage and "<option>"
    Then I can see graphical indicator on accommodation has "<colour>" colour
    And accommodation status is "<status>"

    Examples:
      | colour | status                        | phase        | stage                                                               | option                                       |
      | Yellow | Pending \| In assessment      | Assessment   | Gather Additional Information                                       | no accommodations proposed                   |
      | Yellow | Pending \| In assessment      | Assessment   | Generate Determination Notice Accommodation Accepted Correspondence | no accommodations proposed                   |
      | Yellow | Pending \| In assessment      | Assessment   | Evaluate Accommodation Options                                      | no accommodations proposed                   |
      | Yellow | Pending \| In assessment      | Assessment   | Move to Implement Accommodation                                     | no accommodations proposed                   |
      | Yellow | Pending \| Exploring options  | Assessment   | Explore Additional Options                                          | no accommodations proposed                   |
      | Red    | Not Accommodated              | Closed       | Closed Ineffective                                                  | no accommodations proposed                   |
      | Green  | Accommodated                  | Closed       | Close as Effective                                                  | accommodations with acceptance date proposed |
      | Green  | Accommodated                  | Closed       | Move to Closed Effective                                            | accommodations with acceptance date proposed |
      | Red    | Not Accommodated              | Closed       | Close as Ineffective                                                | no accommodations proposed                   |
      | Red    | Not Accommodated              | Closed       | Closed                                                              | no accommodations proposed                   |
      | Red    | Not Accommodated              | Closed       | Generate Accommodation Closed Ineffective Correspondence            | no accommodations proposed                   |
      | Red    | Not Accommodated              | Closed       | Move to Closed Ineffective                                          | no accommodations proposed                   |
      | Red    | Not Accommodated              | Closed       | Generate Accommodation Closed Effective                             | no accommodations proposed                   |
      | Green  | Accommodated                  | Closed       | Closed Effective                                                    | accommodations with acceptance date proposed |
      | Green  | Accommodated                  | Completion   | Move to Monitoring                                                  | accommodations with acceptance date proposed |
      | Green  | Accommodated                  | Completion   | Accommodated                                                        | accommodations with acceptance date proposed |
      | Green  | Accommodated                  | Completion   | Move to Closed as Accommodated                                      | accommodations with acceptance date proposed |
      | Green  | Accommodated                  | Completion   | Generate Accommodation Monitoring                                   | accommodations with acceptance date proposed |
      | Red    | Not Accommodated              | Completion   | Move to Closed as Not Accommodated                                  | no accommodations proposed                   |
      | Red    | Not Accommodated              | Completion   | Generate Determination Notice Not Accommodated                      | no accommodations proposed                   |
      | Red    | Not Accommodated              | Completion   | Request Invalid Correspondence                                      | no accommodations proposed                   |
      | Red    | Not Accommodated              | Completion   | Generate Accommodation Case Closed Correspondence                   | no accommodations proposed                   |
      | Red    | Not Accommodated              | Completion   | Request Invalid                                                     | no accommodations proposed                   |
      | Red    | Not Accommodated              | Completion   | Not Accommodated                                                    | no accommodations proposed                   |
      | Red    | Not Accommodated              | Completion   | Generate Invalid Request                                            | no accommodations proposed                   |
      | Green  | Accommodated                  | Monitoring   | Monitoring                                                          | accommodations with acceptance date proposed |
      | Yellow | Pending \| Implementing       | Monitoring   | Implement Accommodation                                             | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Request Validated                                                   | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Request Submitted                                                   | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Start                                                               | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Intake in Progress                                                  | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Generate Accommodation Acknowledgment                               | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Intake Complete                                                     | accommodations with acceptance date proposed |
      | Yellow | Pending \| Request recognized | Registration | Request Recognized                                                  | accommodations with acceptance date proposed |

  @C115-F10-05 @accommodation-card @accommodations-proposed @smoke @automated @sanity
  Scenario: Accommodations proposed list
    Given I log in to the application
    When I open notification with "Child Accommodation Case"
    And I expand "Workplace Accommodation" details for first item
    Then I can see first details item has list of collapsed items in 'Accommodations Proposed' section
    And each collapsed accommodation proposal has 'Accommodation Category | Accommodation Type'

  @C115-F10-06 @accommodation-card @accommodations-proposed @automated @regression @sanity
  Scenario: Accommodations proposed empty list
    Given I log in to the application
    When I open notification with "empty list of Proposed Accommodation"
    Then I verify that the 'Workplace Accommodation' card is "visible"
    And I expand "Workplace Accommodation" details for first item
    Then I can see placeholder for accommodations list


  @C115-F10-07 @accommodation-card @accommodations-proposed @automated @regression
  Scenario Outline: Accommodations proposed details
    Given I log in to the application
    When I open notification with "<type> Proposed Accommodation"
    And I expand "Workplace Accommodation" details for first item
    And I expand first 'Proposed Accommodation'
    Then I see first expanded "Workplace Accommodation" item has "Description, Request date, Accommodation date" details
    And I verify Accommodation Date field contains "<dateType>" date

    Examples:
      | type      | dateType |
      | permanent | single   |
      | temporary | range    |


  @C115-F10-08 @accommodation-card @automated @regression
  Scenario: Not Accommodated case reason
    Given I log in to the application
    When I open notification with "Proposed Accommodation containing reasons list"
    And I expand "Workplace Accommodation" details for first item with "Not Accommodated" status
    Then I can see 'Show reason' button
    When I show reasons list using 'Show reason' button
    Then I can see 'Hide reason' button
    And I can see expanded reasons list
    When I hide reasons list using 'Hide reason' button
    Then I can see 'Show reason' button
    And I can't see expanded reasons list


  @C115-F10-09 @accommodation-card @permissions @manual
  Scenario: Accommodation card visibility - user without permissions
    Given I log in to the application as a user "without" permissions to "URL_GET_GROUPCLIENT_ABSENCE_ACCOMMODATIONCASES_GETSINGLE"
    When I open notification with "Child Accommodation Case"
    Then I verify that the accommodation card is "not visible"
