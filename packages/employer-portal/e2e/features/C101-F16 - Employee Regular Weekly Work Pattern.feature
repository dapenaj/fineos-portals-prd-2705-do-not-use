@employer-portal @C101-F16
Feature: C101-F16 - Employee Regular Weekly Work Pattern

  @C101-F16-01 @AC1.1 @AC2.6 @permissions @manual
  Scenario Outline: Permission to view the regular weekly work pattern under Current Occupation section of Employment Details
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN"
    And I open Employee Details page
    When I click on Employment Details tab
    Then I can see the regular weekly work pattern section is "<visibility>" under the Current Occupation section

    Examples:
      | withOrWithout | visibility  |
      | with          | visible     |
      | without       | not visible |

  @C101-F16-02 @AC2.1 @automated @regression
  Scenario Outline: Check work pattern section message and content for employee with/without work pattern
    Given I log in to the application
    And I open Employee details page "with <workPatternType> work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    Then I can see correct message for "<workPatternType> work pattern" in "Work pattern" section

    Examples:
      | workPatternType |
      | empty           |
      | fixed           |
      | rotating        |
      | variable        |

  @C101-F16-03 @AC2.2 @manual
  Scenario Outline: Check work pattern for showing add or edit links, pattern details, and please call message
    Given I log in to the application as a user with permission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN"
    And I have as a user "with" permission to "<permission>"
    And I open Employee Details page of employee "with" "<workPatternType>" work pattern
    When I click on Employment Details tab
    Then I can see the regular weekly work pattern section shows line "Employee has a variable work pattern."
    And I can see the regular weekly work pattern section's message  "Please call us on 555-xxxx if you require further information"
    And I can see the regular weekly work pattern section's details is "not visible"
    And I can see the regular weekly work pattern section's "<linkAction>" link is "not visible"

    Examples:
      | workPatternType | permission                                                                       | linkAction   |
      | variable        | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_ADD  | Add pattern  |
      | non-weekly      | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_ADD  | Add pattern  |
      | variable        | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_EDIT | Edit pattern |
      | non-weekly      | URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_EDIT | Edit pattern |

  @C101-F16-03B @AC2.2 @manual
  Scenario: Check variable work pattern info message is configurable in the Theme configurator
    Given I log in to the application as a user with permission to "URL_EMPLOYERPORTAL_THEMECONFIGURATION"
    And I open Employee Details page of employee "with" "variable" work pattern
    When I click on Employment Details tab
    Then I can see the regular weekly work pattern section shows line "Employee has a variable work pattern."
    And I can see the regular weekly work pattern section's message  "Please call us on 555-xxxx if you require further information"
    When I edit in the theme configurator panel the variable work pattern line text to "Employee has an irregular work pattern. Please call us on 777-9000 if more info is needed"
    Then I can see the regular weekly work pattern section shows line "Employee has an irregular work pattern."
    And I can see the regular weekly work pattern section's message  "Please call us on 777-9000 if more info is needed"


  @C101-F16-04-01 @AC2.4 @AC2.5 @automated @regression
  Scenario Outline: Check work pattern rows and columns data with time and without time shows en dash – Rotating work pattern
    Given I log in to the application
    And I open Employee details page "with work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Rotating" option
    And I set "<numberOfWeeks> Weeks" option
    And I set input fields number "00:00" hours per day for "even" days
    And I set input fields number "12:59" hours per day for "odd" days
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the "Work pattern" section with "<numberOfWeeks>" rows of a week each with row title "Week " plus row number
    And I can see the "Work pattern" section with "<numberOfWeeks>" rows of a week each with columns "Mon, Tue, Wed, Thu, Fri, Sat, Sun"
    And I can see the "<numberOfWeeks>" rows of a week each with following times "12h 59m – 12h 59m – 12h 59m – 12h 59m"

    Examples:
      | numberOfWeeks |
      | 2             |
      | 3             |
      | 4             |

  @C101-F16-04-02 @AC2.4 @AC2.5 @automated @regression
  Scenario: Check work pattern rows and columns data with time and without time shows en dash – Rotating work pattern
    Given I log in to the application
    And I open Employee details page "with work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Fixed" option
    And I switch "on" 'Non standard working week'
    And I switch "on" Include weekend
    And I set input fields number "00:00" hours per day for "even" days
    And I set input fields number "12:59" hours per day for "odd" days
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the "Work pattern" week without title "Week " plus row number
    And I can see the "Work pattern" section with "1" row of a week with columns "Mon, Tue, Wed, Thu, Fri, Sat, Sun"
    And I can see the "1" row of a week with following times "12h 59m – 12h 59m – 12h 59m – 12h 59m"

  @C101-F16-05 @AC2.7 @A2.3 @manual
  Scenario Outline: Check permissions for the add and edit links with/without regular work week work patterns
    Given I log in to the application as a user with permission to "URL_GET_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN"
    And I have as a user "<withOrWithout1>" permission to URL_POST_GROUPCLIENT_CUSTOMERS_CUSTOMEROCCUPATIONS_REGULARWEEKLYWORKPATTERN_"<addOrEditAction>"
    And I open Employee Details page of employee "<withOrWithout2>" "<workPatternType>" work pattern
    When I click on Employment Details tab
    Then I can see the regular weekly work pattern section's "<linkAction>" link is "<visibility>"

    Examples:
      | withOrWithout1 | addOrEditAction | withOrWithout2 | workPatternType | linkAction   | visibility  |
      | with           | ADD             | without        | fixed           | Add pattern  | visible     |
      | without        | ADD             | without        | fixed           | Add pattern  | not visible |
      | with           | EDIT            | with           | fixed           | Edit pattern | visible     |
      | without        | EDIT            | with           | fixed           | Edit pattern | not visible |
      | with           | ADD             | without        | rotating        | Add pattern  | visible     |
      | without        | ADD             | without        | rotating        | Add pattern  | not visible |
      | with           | EDIT            | with           | rotating        | Edit pattern | visible     |
      | without        | EDIT            | with           | rotating        | Edit pattern | not visible |

  @C101-F16-06 @AC3.1 @automated @sanity @regression
  Scenario Outline: Check edit work pattern popup for fixed with standard work week/non standard work week
    Given I log in to the application
    And I open Employee details page "with fixed work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Fixed" option
    And I switch "on" 'Non standard working week'
    And I switch "<includeWeekend>" Include weekend
    And I set input fields number "08:00" hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    When I edit "Work pattern" card in 'Employment Details'
    Then I can see the Work pattern drop down with "Fixed" option selected
    And I see that "Work pattern" drop down contains options: "Fixed Rotating Variable"
    And I can see the "non standard working week" switch is "<onOrOff>"
    And I can see the "include weekend" switch is "<visibility>"
    And I can see "1" week row shows with columns "<week>"
    And I can see "1" row of a week with following times "<expectedTimes>"


    Examples:
      | includeWeekend | onOrOff | visibility  | week                              | expectedTimes                             |
      | on             | on      | on          | Mon, Tue, Wed, Thu, Fri, Sat, Sun | 08:00,08:00,08:00,08:00,08:00,08:00,08:00 |
      | off            | off     | not visible | Mon, Tue, Wed, Thu, Fri           | 8h 0m,8h 0m,8h 0m,8h 0m,8h 0m             |

  @C101-F16-07 @AC3.1 @automated @smoke @sanity
  Scenario Outline: Check edit work pattern popup for rotating work pattern
    Given I log in to the application
    And I open Employee details page "with rotating work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Rotating" option
    And I set "<numberOfWeeks> Weeks" option
    And I set input fields number "08:00" hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    When I edit "Work pattern" card in 'Employment Details'
    Then I can see the Work pattern drop down with "Rotating" option selected
    And I see that "Work pattern" drop down contains options: "Fixed Rotating Variable"
    And I can see "<numberOfWeeks>" weeks rows show with columns "Mon, Tue, Wed, Thu, Fri, Sat, Sun"
    And I can see "<numberOfWeeks>" rows of a week with following times "08:00,08:00,08:00,08:00,08:00,08:00,08:00"

    Examples:
      | numberOfWeeks |
      | 2             |
      | 3             |
      | 4             |


  @C101-F16-08 @AC3.2 @automated @regression
  Scenario: Check add work pattern popup opens with fixed work pattern by default
    Given I log in to the application
    And I open Employee details page "with empty work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    And I add "Work pattern" card in 'Employment Details'
    Then the 'Add Work Pattern' popup opens
    Then I can see the Work pattern drop down with "Fixed" option selected
    And I see that "Work pattern" drop down contains options: "Fixed Rotating Variable"
    And I can see the "non standard working week" switch is "off"
    And I can see the "include weekend" switch is "not visible"
    And I can see "1" week row shows with columns "Mon, Tue, Wed, Thu, Fri"
    And I can see "1" row of a week with following times "8h 0m,8h 0m,8h 0m,8h 0m,8h 0m"


  @C101-F16-09 @AC3.3 @automated @regression @sanity
  Scenario: Check variable work pattern on opening add work pattern
    Given I log in to the application
    And I open Employee details page "with empty work pattern" using EmployeeId
    When I open "Employee Details" tab
    And I open "Employment Details" tab
    And I add "Work pattern" card in 'Employment Details'
    Then the 'Add Work Pattern' popup opens
    When I select "Variable" option
    Then I should see pattern description input field
    And I can see the "pattern repeats" switch is "off"
    And I can see the "day length same" switch is "off"
    And "Number of weeks" input is "not visible"
    And "Day length" input is "not visible"
    And I switch "on" pattern repeats
    And "Number of weeks" input is "visible"
    And "Day length" input is "not visible"
    And I switch "on" day length same
    And "Number of weeks" input is "visible"
    And "Day length" input is "visible"


  @C101-F16-10 @AC3.3 @regression @automated
  Scenario: Check adding variable work pattern without required fields values and invalid data
    Given I log in to the application
    When I open Employee details page "with work pattern" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Variable" option
    And I "accept" 'Edit work pattern' popup
    Then I verify the "not provided information" error with text "Please provide additional information to describe the work pattern" displays for "Pattern description"
    When I type "Test description" in the "pattern description" field
    And I set "pattern repeats" switch to "on"
    And I "accept" 'Edit work pattern' popup
    Then I verify the "required" error with text "required" displays for "Number of weeks"
    When I set "pattern repeats" switch to "off"
    And I set "day length same" switch to "on"
    And I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I type "24:00" in the "day length" field
    And I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I type "00:60" in the "day length" field
    And I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I set "day length same" switch to "off"
    And I set "pattern repeats" switch to "on"
    And I type "12" in the "number of weeks" field
    And I tab to next field
    Then I see "8" in the "number of weeks" field
    When I type "0" in the "number of weeks" field
    And I tab to next field
    Then I see "2" in the "number of weeks" field

  @C101-F16-11 @AC3.3 @regression @manual
  Scenario Outline: Check adding variable work pattern
    Given I log in to the application
    And I open Employee Details page of employee without "any" work pattern
    And I click on Employment Details tab
    And I record the work pattern details
    When I click Add pattern link
    Then I can see the Add Work Pattern popup opens
    When I select Work pattern as "Variable"
    And I set pattern repeats switch to "<patternRepeatsSwitch>" providing value "<repeatValue>"
    And I set day length same switch to "<dayLengthSameSwitch>" providing value "<lengthValue>"
    And I fill "Pattern description" field
    And I click "OK" on "Add Work Pattern" popup
    Then I can see the work pattern information is sent to the server
    And I can see the Add Work Pattern popup closes
    And I can see the "Work pattern submitted" confirmation popup opens
    When I click "OK" on "Work pattern submitted" popup
    Then I can see the "Work pattern submitted" confirmation popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section

    Examples:
      | patternRepeatsSwitch | repeatValue | dayLengthSameSwitch | lengthValue |
      | off                  |             | off                 |             |
      | on                   | 3           | off                 |             |
      | on                   | 5           | on                  | 4:00        |
      | off                  |             | on                  | 8:00        |

  @C101-F16-12 @AC3.3 @regression @automated
  Scenario Outline: Check editing variable work pattern
    Given I log in to the application
    When I open Employee details page "with work pattern" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Variable" option
    And I set "pattern repeats" switch to "<patternRepeatsSwitch>"
    And I type "<repeatValue>" in the "number of weeks" field if visible
    And I set "day length same" switch to "<dayLengthSameSwitch>"
    And I type "<lengthValue>" in the "day length" field if visible
    And I type "<patternDescription>" in the "pattern description" field
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the "Work pattern submitted" confirmation popup opens
    When I "close" "Work pattern submitted" confirmation popup
    Then I can see the "Work pattern submitted" confirmation popup closes
    And I can see the "Work pattern" section is not updated

    Examples:
      | patternRepeatsSwitch | repeatValue | dayLengthSameSwitch | lengthValue | patternDescription                                       |
      | off                  |             | off                 |             | Pattern Repeats Switch: off, Day Length Same Switch: off |
      | on                   | 3           | off                 |             | Pattern Repeats Switch: on, Day Length Same Switch: off  |
      | on                   | 5           | on                  | 4:00        | Pattern Repeats Switch: on, Day Length Same Switch: on   |
      | off                  |             | on                  | 8:00        | Pattern Repeats Switch: off, Day Length Same Switch: on  |

  @C101-F16-13 @AC3.3 @automated @regression
  Scenario Outline: Check cancel editing/adding variable work pattern does not send it to the server
    Given I log in to the application
    When I open Employee details page "with <workPatternType> work pattern" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I <action> "Work pattern" card in 'Employment Details'
    Then the '<mode> Work Pattern' popup opens
    When I select "Variable" option
    And I set pattern repeats switch to "on" providing value "3"
    And I set day length same switch to "on" providing value "8:00"
    And I type "pattern description txt" in the "pattern description" field
    And I "<closeAction>" '<mode> work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the "Work pattern" section is not updated

    Examples:
      | closeAction | workPatternType | action | mode |
      | cancel      | fixed           | edit   | Edit |
      | close X     | fixed           | edit   | Edit |
      | cancel      | empty           | add    | Add  |
      | close X     | empty           | add    | Add  |

  @C101-F16-14 @AC3.4 @manual
  Scenario Outline: Check select fixed work pattern switching on/off toggles with add/edit pattern link
    Given I log in to the application
    And I open Employee Details page of employee "<withOrWithout>" "fixed" work pattern as "standard work week" with all days set to "8h 0m"
    And I click on Employment Details tab
    When I click "<linkAction>" link
    Then I can see the "<popupType>" popup opens
    And I can see the Work pattern drop down with "Fixed" option selected
    And I can see the "Non standard working week" toggle switched "off"
    And I can see the row shows with columns "Mon, Tue, Wed, Thu, Fri"
    And I can see the row input fields number of hours is "08:00" per day for "Mon, Tue, Wed, Thu, Fri"
    When I switch "on" 'Non standard working week'
    Then I can see the row shows with columns "Mon, Tue, Wed, Thu, Fri"
    And I can see the row input fields number of hours is "hh:mm" per day for "Mon, Tue, Wed, Thu, Fri"
    And I can see the "Include week" toggle switched "off"
    When I switch "on" 'Include week'
    Then I can see the row input fields number of hours is "hh:mm" per day for "Sat, Sun"
    When I switch "off" 'Include week'
    Then I cannot see the row input fields number of hours is "hh:mm" per day for "Sat, Sun"
    When I switch "off" 'Non standard working week'
    Then I can see the row input fields number of hours is "hh:mm" per day for "Mon, Tue, Wed, Thu, Fri"

    Examples:
      | withOrWithout | popupType         | linkAction   |
      | with          | Edit Work Pattern | Edit pattern |
      | without       | Add Work Pattern  | Add pattern  |

  @C101-F16-15 @AC3.5 @manual
  Scenario Outline: Check select rotating work pattern with add/edit pattern link
    Given I  log in to the application
    And I open Employee Details page of employee "<withOrWithout>" "fixed" work pattern
    And I click on Employment Details tab
    When I click "<linkAction>" link
    Then I can see the "<popupType>" popup opens
    When I select Work pattern as "Rotating"
    And I can see radiobutton "2 weeks" selected
    And I select radiobutton "<radio>"
    Then I can see an animation while loading the week rows
    And I can see "<numberOfRows>" rows of a week each with title "Week " plus row number
    And I can see "<numberOfRows>" rows of a week each with columns "Mon, Tue, Wed, Thu, Fri, Sat, Sun"
    And I can see "<numberOfRows>" rows of a week each with input fields number of hours is "hh:mm" per day for "all" days

    Examples:
      | radio   | numberOfRows | withOrWithout | popupType         | linkAction   |
      | 2 weeks | 2            | with          | Edit Work Pattern | Edit pattern |
      | 3 weeks | 3            | with          | Edit Work Pattern | Edit pattern |
      | 4 weeks | 4            | with          | Edit Work Pattern | Edit pattern |
      | 2 weeks | 2            | without       | Add Work Pattern  | Add pattern  |
      | 3 weeks | 3            | without       | Add Work Pattern  | Add pattern  |
      | 4 weeks | 4            | without       | Add Work Pattern  | Add pattern  |


  @C101-F16-16-01 @AC3.6 @regression @automated
  Scenario: Check error validation for edit fixed work pattern
    Given I log in to the application
    When I open Employee details page "with work pattern" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Fixed" option
    Then I set "non standard working week" switch to "on"
    When I set input fields number "08:00" hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    When I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I set "non standard working week" switch to "on"
    And I set input fields number "00:00" hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then I verify the "at least one filled" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I clear input fields number of hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then I verify the "at least one filled" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    And I set input fields number "24:00" hours per day for "random" day
    And I tab to next field
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I set input fields number "08:60" hours per day for "same random" day
    And I tab to next field
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I set input fields number "05:10" hours per day for "same random" day
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section

  @C101-F16-16-02 @AC3.6 @regression @automated
  Scenario Outline: Check error validation for edit rotating work pattern
    Given I log in to the application
    When I open Employee details page "with work pattern" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I select "Rotating" option
    And I set "<numberOfWeeks> Weeks" option
    And I set input fields number "08:00" hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    When I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I set input fields number "00:00" hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then I verify the "at least one filled" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I clear input fields number of hours per day for "all" days
    And I "accept" 'Edit work pattern' popup
    Then I verify the "at least one filled" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I set input fields number "24:00" hours per day for "random" day
    And I tab to next field
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I set input fields number "08:60" hours per day for "same random" day
    And I tab to next field
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I "accept" 'Edit work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I set input fields number "<validTime>" hours per day for "same random" day
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section


    Examples:
      | numberOfWeeks | validTime |
      | 2             | 02:03     |
      | 3             | 10:09     |
      | 4             | 23:59     |

  @C101-F16-16 @AC3.6 @regression @manual
  Scenario Outline: Check error validation for add fixed/rotating work pattern
    Given I log in to the application
    When I open Employee details page "without work pattern" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I open "Add Work pattern" card in 'Employment Details'
    Then the 'Add Work Pattern' popup opens
    When I select a variable "<workPatternType>" option
    When I set input fields number "00:00" hours per day for "all" days
    And I "accept" 'Add work pattern' popup
    Then I verify the "at least one filled" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I clear input fields number of hours per day for "all" days
    And I "accept" 'Add work pattern' popup
    Then I verify the "at least one filled" error with text "Please enter a valid time format (for example 07:30)" displays for "Day length"
    When I set input fields number "24:00" hours per day for "random" day
    And I tab to next field
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    And I "accept" 'Add work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I set input fields number "08:60" hours per day for "same random" day
    And I tab to next field
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    And I "accept" 'Add work pattern' popup
    Then I verify the "invalid time format" error with text "Please enter a valid time format (for example 07:30)" displays for "Working days"
    When I set input field number valid random hours per day for "same random" day
    And I "accept" 'Add work pattern' popup
    Then the 'Add Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section

    Examples:
      | workPatternType  |
      | Fixed            |
      | 2 Weeks Rotating |

  @C101-F16-17 @AC3.7 @manual
  Scenario Outline: Check OK button behavior on editing/adding fixed or rotating work patterns
    Given I log in to the application
    And I open Employee Details page of employee "<withOrWithout>" "<workPatternType>" work pattern
    And I click on Employment Details tab
    And I record the work pattern details
    When I click "<linkAction>" link
    Then I can see the "<popupType>" popup opens
    When I select Work pattern as "<workPatternType>"
    And I set input field number "valid random" hours per day for "random" day
    And I click "OK" on "<popupType>" popup
    Then I can see the work pattern information is sent to the server
    And I can see the "<popupType>" popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section

    Examples:
      | workPatternType | withOrWithout | popupType         | linkAction   |
      | fixed           | with          | Edit Work Pattern | Edit pattern |
      | fixed           | without       | Add Work Pattern  | Add pattern  |
      | rotating        | with          | Edit Work Pattern | Edit pattern |
      | rotating        | without       | Add Work Pattern  | Add pattern  |

  @C101-F16-18 @AC3.7 @manual
  Scenario Outline: Check OK button behavior with error condition
    Given I log in to the application
    And I open Employee Details page of employee "<withOrWithout>" "fixed" work pattern
    And I click on Employment Details tab
    And I record the work pattern details
    When I click "<linkAction>" link
    Then I can see the "<popupType>" popup opens
    When I select Work pattern as "<workPatternType>"
    And I set input field number "valid random" hours per day for "random" day
    And I disconnect the internet connection
    And I click "OK" on "<popupType>" popup
    Then I verify the Something went wrong popup opens
    When I click OK button on "Something went wrong" popup
    Then I can see the "<popupType>" popup closes
    When I click "Cancel" on "<popupType>" popup
    Then I can see the Employment Details tab is not updated with changes to the work pattern section

    Examples:
      | withOrWithout | popupType         | linkAction   |
      | with          | Edit Work Pattern | Edit pattern |
      | without       | Add Work Pattern  | Add pattern  |

  @C101-F16-19 @AC3.8 @manual
  Scenario Outline: Check cancel editing/adding fixed/rotating work pattern does not send it to the server
    Given I log in to the application
    And I open Employee Details page of employee "<withOrWithout>" "<workPatternType>" work pattern
    And I click on Employment Details tab
    And I record the work pattern details
    When I click "<linkAction>" link
    Then I can see the "<popupType>" popup opens
    When I select Work pattern as "<workPatternType>"
    And I set input field number "valid random" hours per day for "random" day
    And I click "<button>" on "<popupType>" popup
    Then I can see the work pattern information is not sent to the server
    And I can see the "<popupType>" popup closes
    And I can see the Employment Details tab is not updated with changes to the work pattern section

    Examples:
      | workPatternType | button  | withOrWithout | popupType         | linkAction   |
      | fixed           | cancel  | with          | Edit Work Pattern | Edit pattern |
      | rotating        | close X | with          | Edit Work Pattern | Edit pattern |
      | rotating        | cancel  | without       | Add Work Pattern  | Add pattern  |
      | fixed           | close X | without       | Add Work Pattern  | Add pattern  |
