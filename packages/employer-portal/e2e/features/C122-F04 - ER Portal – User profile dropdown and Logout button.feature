@employer-portal @C122-F04
Feature: C122-F04 - ER Portal – User profile dropdown & Logout button

# There is no feature doc to this feature, so there are no AC related tags

  @C122-F04-01 @regression @automated
  Scenario: Successful logout
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I log out the application
    Then I see a loading spinner with message "We're logging you out..." for a moment
    And I see popup informing that user was successfully log out
    When I confirm popup by clicking "Take me to login" button
    Then I am redirected to login page

  @C122-F04-02 @regression @manual @sanity
  Scenario: Network issue during logout
    Given I log in to the application
    When I turn off my network connection
    And I select "Logout" option from user profile dropdown
    Then I see a toast pop-up with "An error occurred. Please try again later." message

  # Currently session is valid for 1 hour
  @C122-F04-03 @manual
  Scenario: Logout with session expired
    Given I log in to the application
    And I wait until my session will be expired
    When I select "Logout" option from user profile dropdown
    Then I see a loading spinner with message "We're logging you out..." for a moment
    And I see "You are logged out" pop-up with a "Take me to login" button
    When I click on "Take me to login" button
    Then I am redirected to login page

  @C122-F04-04 @regression @automated
  Scenario: Refresh during logout
    Given I log in to the application
    And My Dashboard page with available notifications is loaded
    When I log out the application
    Then I see a loading spinner with message "We're logging you out..." for a moment
    And I see popup informing that user was successfully log out
    When I refresh the page
    Then I am redirected to login page

  @C122-F04-05 @manual @regression
  Scenario Outline: Access My Dashboard page after logging out
    Given I log in to the application
    And I select "Logout" option from user profile dropdown
    And I see a loading spinner with message "We're logging you out..." for a moment
    And I see "You are logged out" pop-up
    And I click on "Take me to login" button
    And I am redirected to login page
    When I try to go back to My Dashboard page with "<back option>"
    Then I stay on login page

    Examples:
      | back option                         |
      | browser back button                 |
      | direct url to the My Dashboard page |
