@employer-portal @C121-F05 @manage-ep-user-access
Feature: C121-F05 - Allow Employer Portal Admin user to disable an employer Portal user

  @C121-F05-01 @AC1.1 @automated @sanity @regression
  Scenario: View the list of Employer Portal users
    Given I log in to the application with "HR admin user"
    When I navigate to Control Access page
    Then I can view the portal users belonging to Organisation


  @C121-F05-02 @AC1.2 @permissions @manual @sanity
  Scenario: Non HR Admin user cannot view the list of Employer Portal users
    When I log in to the application as "HR User"
    Then I verify I cannot navigate to Control Access page


  @C121-F05-03 @AC1.3 @automated @regression
  Scenario Outline: List of Employer Portal users - single item view
    Given I log in to the application with "HR admin user"
    When I navigate to Control Access page
    And I make sure that "user without permissions" is "<expectedStatus>"
    Then I see user item consist of 'User name, Role and modify status link'

    Examples:
      | expectedStatus |
      | enabled        |
      | disabled       |


  @C121-F05-05 @AC1.4 @AC1.5 @automated @regression
  Scenario Outline: Control Access page - filtering users
    Given I log in to the application with "HR admin user"
    When I navigate to Control Access page
    And I use filter to search "<user>" user name or its part
    Then I can see "<result>" list of users
    And I can clear the filter to reset list to original view

    Examples:
      | user                | result             |
      | logged in           | empty              |
      | existing            | correctly filtered |
      | erpoertal.user10000 | empty              |


  @C121-F05-06 @AC2.1 @A.C2.3 @2.6 @smoke @automated
  Scenario Outline: Control Access page - disable user access
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user without permissions" is "enabled"
    When I click 'disable user' on a "user without permissions"
    And I "<action>" popup to disable user
    Then I "<alertShown>" see alert confirming "user without permissions" access was "disabled"
    And I can see "<status>" access for chosen "user without permissions" on users list
    When I log out the application
    Then I see a loading spinner with message "We're logging you out..." for a moment
    And I see popup informing that user was successfully log out
    When I confirm popup by clicking "Take me to login" button
    And I sign out current user on Fineoscloud IDP
    And I log in to the application with "user without permissions"
    Then I can see user is "<status>"

    Examples:
      | action  | status   | alertShown |
      | confirm | disabled | can        |
      | cancel  | enabled  | cannot     |


  @C121-F05-07 @AC2.2 @regression @automated
  Scenario: Control Access page - disable user confirmation dialog
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user without permissions" is "enabled"
    When I click 'disable user' on a "user without permissions"
    Then the popup to disable user opens


  @C121-F05-08 @AC2.3 @AC2.5 @automated @regression
  Scenario Outline: Control Access page - alert confirming changing access
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user without permissions" is "<initialStatus>"
    When I "<action>" "user without permissions" user
    Then I "can" see alert confirming "user without permissions" access was "<finalStatus>"

    Examples:
      | action  | initialStatus | finalStatus |
      | disable | enabled       | disabled    |
      | enable  | disabled      | enabled     |


  @C121-F05-09 @AC2.4 @AC2.5 @2.6 @automated @regression
  Scenario: Control Access page - enable user access
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user without permissions" is "disabled"
    When I "enable" "user without permissions" user
    Then I "can" see alert confirming "user without permissions" access was "enabled"
    And I can see "enabled" access for chosen "user without permissions" on users list
    When I log out the application
    Then I see a loading spinner with message "We're logging you out..." for a moment
    And I see popup informing that user was successfully log out
    When I confirm popup by clicking "Take me to login" button
    And I sign out current user on Fineoscloud IDP
    And I log in to the application with "user without permissions"
    Then I can see user is "enabled"


  @C121-F05-10 @AC2.7 @AC3.1 @3.2 @manual
  Scenario Outline: Control Access page - error during changing user access
    Given I log in to the application as "HR Admin"
    And I navigate to Control Access page
    When I "<action>" portal access for a HR user
    And the portal access update request has not been successful
    Then the error message is presented to the user
    And the error message remain on the page until user close it
    And I can see unchanged access for chosen HR user on users list

    Examples:
      | action  |
      | disable |
      | enable  |

