@employer-portal @ConcurrencyErr
Feature: Concurrency error modal

# No functionality doc was provided for this feature so there are no AC tags

  @ConcurrencyErr-01 @FEP-1041 @automated @regression
  Scenario: Edit employee personal details - successful save
    Given I log in to the application with "HR admin user"
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter the same random test data in the "First name" field
    And I enter the same random test data in the "Last name" field
    And I enter the same random test data in the "Date of birth" field
    And I enter the same random test data in the "Address line 1" field
    And I enter the same random test data in the "Address line 2" field
    And I enter the same random test data in the "Address line 3" field
    And I enter the same random test data in the "City" field
    And I select the same random test data in the "State" field
    And I enter the same random test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field
    When I switch to "first" window
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field

  @ConcurrencyErr-02 @FEP-1041 @automated @regression
  Scenario: Edit employee personal details on profile page and then on intake flow - successful save
    Given I log in to the application with "HR admin user"
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    When I log in to the application with "HR test user" using new browser window
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    When I enter the same random test data in the "First name" field
    And I enter the same random test data in the "Last name" field
    And I enter the same random test data in the "Address line 1" field
    And I enter the same random test data in the "Address line 2" field
    And I enter the same random test data in the "Address line 3" field
    And I enter the same random test data in the "City" field
    And I select the same random test data in the "State" field
    And I enter the same random test data in the "Zip code" field
    And I switch to "first" window
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field
    When I switch to "second" window
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"

  @ConcurrencyErr-03 @FEP-1041 @automated @regression
  Scenario: Edit employee personal details intake flow and then on profile page - successful save
    Given I log in to the application with "HR admin user"
    When I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    And I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter the same random test data in the "First name" field
    And I enter the same random test data in the "Last name" field
    And I enter the same random test data in the "Address line 1" field
    And I enter the same random test data in the "Address line 2" field
    And I enter the same random test data in the "Address line 3" field
    And I enter the same random test data in the "City" field
    And I select the same random test data in the "State" field
    And I enter the same random test data in the "Zip code" field
    When I switch to "first" window
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"
    When I switch to "second" window
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field

  @ConcurrencyErr-04 @FEP-1041 @automated @regression
  Scenario: Edit personal details on intake flow - successful save
    Given I log in to the application with "HR admin user"
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    When I log in to the application with "HR test user" using new browser window
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    And I edit the address fields with random data and confirm
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"
    When I switch to "first" window
    And I edit the address fields with the same random data and confirm
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"


  @ConcurrencyErr-05-01 @FEP-1042 @automated @regression
  Scenario: Edit employee employment details (Current Occupation) - successful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    When I enter random test data in the 'Current occupation' popup "Job title" field
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    And I enter random test data in the 'Current occupation' popup "Job end date" field
    And I enter random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose random test data in the "Employment type" select field
    And I choose random test data in the "Full/part time" select field
    And I choose random test data in the "Earnings frequency" select field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I "check" "Adjusted date of hire" switch
    And I enter random test data in the 'Current occupation' popup "Adjusted date of hire" field
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    When I enter the same random test data in the 'Current occupation' popup "Job title" field
    And I enter the same random test data in the 'Current occupation' popup "Job start date" field
    And I enter the same random test data in the 'Current occupation' popup "Job end date" field
    And I enter the same random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose the same random test data in the "Employment type" select field
    And I choose the same random test data in the "Full/part time" select field
    And I choose the same random test data in the "Earnings frequency" select field
    And I enter the same random test data in the 'Current occupation' popup "Earnings" field
    And I "check" "Adjusted date of hire" switch
    And I enter the same random test data in the 'Current occupation' popup "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field
    When I switch to "first" window
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field


  @ConcurrencyErr-05-02 @FEP-1042 @automated @regression
  Scenario: Edit employee employment details (Eligibility criteria) - successful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I randomly "check/uncheck" "Within FMLA radius" checkbox
    And I randomly "check/uncheck" "Works at home" checkbox
    And I randomly "check/uncheck" "Key employee" checkbox
    And I enter random test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter random test data in the 'Eligibility' popup "CBA" field
    And I choose random "Work state"
    When I log in to the application with "HR test user" using new browser window
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I the same randomly "check/uncheck" "Within FMLA radius" checkbox
    And I the same randomly "check/uncheck" "Works at home" checkbox
    And I the same randomly "check/uncheck" "Key employee" checkbox
    And I enter the same random test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter the same random test data in the 'Eligibility' popup "CBA" field
    And I choose the same random "Work state"
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section
    When I switch to "first" window
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section


  @ConcurrencyErr-05-03 @FEP-1042 @automated @regression
  Scenario: Edit employee employment details (Work Pattern) - successful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "another employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I set "non standard working week" switch to "on"
    And I set "random" hours per day for "random" day
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "another employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I set "non standard working week" switch to "on"
    And I set "the same random" hours per day for "same random" day
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section
    When I switch to "first" window
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section


  @ConcurrencyErr-06 @FEP-1043 @automated @regression
  Scenario: Edit employee personal details - unsuccessful save
    Given I log in to the application with "HR admin user"
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I enter random test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    When I switch to "first" window
    And I enter "Test First Name" test data in the "First name" field
    And I enter "Test Last Name" test data in the "Last name" field
    And I enter "Feb 12, 2020" test data in the "Date of birth" field
    And I enter "Test Ave" test data in the "Address line 1" field
    And I enter "Test Street" test data in the "Address line 2" field
    And I enter "Test Building no" test data in the "Address line 3" field
    And I enter "Test City" test data in the "City" field
    And I enter "79070" test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And "Personal Details" "First name, Last name, Date of birth, Address line 1, Address line 2, Address line 3, City, Zip code" fields were refreshed correctly
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field

  @ConcurrencyErr-06B @FEP-1043 @automated @regression
  Scenario Outline: Edit employee personal details - unsuccessful save with close pressed
    Given I log in to the application with "HR admin user"
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I enter random test data in the "Zip code" field
    And I select random test data in the "State" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    When I switch to "first" window
    And I enter "Test First Name" test data in the "First name" field
    And I enter "Test Last Name" test data in the "Last name" field
    And I enter "Feb 12, 2020" test data in the "Date of birth" field
    And I enter "Test Ave" test data in the "Address line 1" field
    And I enter "Test Street" test data in the "Address line 2" field
    And I enter "Test Building no" test data in the "Address line 3" field
    And I enter "Test City" test data in the "City" field
    And I enter "79070" test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And "Personal Details" "First name, Last name, Date of birth, Address line 1, Address line 2, Address line 3, City, Zip code" fields were refreshed correctly
    When I "<close>" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field

    Examples:
      | close   |
      | cancel  |
      | close X |

  @ConcurrencyErr-07 @FEP-1043 @automated @regression
  Scenario: Edit employee personal details on profile page and then on intake flow - unsuccessful save
    Given I log in to the application with "HR admin user"
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    When I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I enter random test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    When I switch to "first" window
    And I edit the address fields with random data and confirm
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I see "Intake flow" "Address line 1, Address line 2, Address line 3, City, Zip Code" fields were refreshed with data set in personal details
    When I choose to edit employee details with Request Change button
    And I edit the address fields with random data and confirm
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"

  @ConcurrencyErr-08 @FEP-1043 @automated @regression
  Scenario: Edit employee personal details on profile page and intake flow - unsuccessful save
    Given I log in to the application with "HR admin user"
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    When I click the "Edit" link under the "Name, Date of Birth, Address" section
    Then the 'Edit personal details' popup opens
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I enter random test data in the "Zip code" field
    When I log in to the application with "HR test user" using new browser window
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    And I edit the address fields with random data and confirm
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"
    When I switch to "first" window
    And I "accept" 'Edit Personal Details' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    Then the 'Edit personal details' popup opens
    And I see "Personal details" "Address line 1,Address line 2,Address line 3" fields were refreshed with data set in intake flow
    When I enter random test data in the "First name" field
    And I enter random test data in the "Last name" field
    And I enter random test data in the "Date of birth" field
    And I enter random test data in the "Address line 1" field
    And I enter random test data in the "Address line 2" field
    And I enter random test data in the "Address line 3" field
    And I enter random test data in the "City" field
    And I select random test data in the "State" field
    And I enter random test data in the "Zip code" field
    And I "accept" 'Edit Personal Details' popup
    Then the 'Edit personal details' popup closes
    And I see that generated "First name, Last name" data is saved in the 'Personal Details' tab in the "Full Name" field
    And I see that generated "Date of birth" data is saved in the 'Personal Details' tab in the "Date of birth" field
    And I see that generated "Address line 1, Address line 2, Address line 3" data is saved in the 'Personal Details' tab in the "Address" field
    And I see that generated "State, Zip Code" data is saved in the 'Personal Details' tab in the "Address" field

  @ConcurrencyErr-09 @FEP-1043 @automated @regression
  Scenario: Edit personal details on intake flow - unsuccessful save
    Given I log in to the application with "HR admin user"
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    When I log in to the application with "HR test user" using new browser window
    And I open intake creator for "employee used to test concurrency error" employee
    And I choose to edit employee details with Request Change button
    And I edit the address fields with random data and confirm
    When I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"
    When I switch to "first" window
    And I edit the "Address line1" field with "Test addres line 11" data and confirm
    And I edit the "Address line2" field with "Test addres line 22" data and confirm
    And I edit the "Address line3" field with "Test addres line 33" data and confirm
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I see "Intake flow" "Address line 1, Address line 2, Address line 3" fields were refreshed correctly
    When I choose to edit employee details with Request Change button
    And I edit the address fields with random data and confirm
    And I proceed to "(2) Occupation & Earnings" section for existing employee
    Then the section contains following fields "Job title, Date of hire, Earnings"

  @ConcurrencyErr-10-01 @FEP-1044 @automated @regression
  Scenario: Edit employee employment details (Current Occupation) - unsuccessful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    When I enter random test data in the 'Current occupation' popup "Job title" field
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    And I enter random test data in the 'Current occupation' popup "Job end date" field
    And I enter random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose random test data in the "Employment type" select field
    And I choose random test data in the "Full/part time" select field
    And I choose random test data in the "Earnings frequency" select field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I "check" "Adjusted date of hire" switch
    And I enter random test data in the 'Current occupation' popup "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field
    When I switch to "first" window
    And I enter "Test job title" in the "Job title" field
    And I enter "Feb 01, 2019" in the "Job start date" field
    And I enter "Jan 11, 2039" in the "Job end date" field
    And I enter "33" in the "Hours worked per week" field
    And I enter "102.3" in the "Earnings" field
    And I choose "Seasonal" option in the "Employment type" select field
    And I choose "Part time" option in the "Full/part time" select field
    And I choose "per month" option in the "Earnings frequency" select field
    And I "check" "Adjusted date of hire" switch
    And I enter "Nov 10, 2019" in the "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And "Current Occupation" "Job title,Job start date,Job end date,Hours worked per week,Earnings,Adjusted date of hire" fields were refreshed correctly
    And "Current Occupation" "Employment type,Full/part time,Earnings frequency" select fields were refreshed correctly
    When I enter random test data in the 'Current occupation' popup "Job title" field
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    And I enter random test data in the 'Current occupation' popup "Job end date" field
    And I enter random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose random test data in the "Employment type" select field
    And I choose random test data in the "Full/part time" select field
    And I choose random test data in the "Earnings frequency" select field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I "check" "Adjusted date of hire" switch
    And I enter random test data in the 'Current occupation' popup "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field

  @ConcurrencyErr-10-02 @FEP-1044 @automated @regression
  Scenario: Edit employee employment details (Eligibility criteria) - unsuccessful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I log in to the application with "HR test user" using new browser window
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I randomly "check/uncheck" "Within FMLA radius" checkbox
    And I randomly "check/uncheck" "Works at home" checkbox
    And I randomly "check/uncheck" "Key employee" checkbox
    And I enter random test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter random test data in the 'Eligibility' popup "CBA" field
    And I choose random "Work state"
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section
    When I switch to "first" window
    And I "check" "Within FMLA radius" checkbox
    And I "uncheck" "Works at home" checkbox
    And I "check" "Key employee" checkbox
    And I enter "35" test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter "0298472" test data in the 'Eligibility' popup "CBA" field
    And I "accept" 'Edit Current occupation' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    Then "Eligibility Criteria" "Actual hours worked,CBA,Work state" fields were refreshed correctly
    And "Eligibility Criteria" "Within FMLA radius,Works at home,Key employee" checkboxes were refreshed correctly
    When I randomly "check/uncheck" "Within FMLA radius" checkbox
    And I randomly "check/uncheck" "Works at home" checkbox
    And I randomly "check/uncheck" "Key employee" checkbox
    And I enter random test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter random test data in the 'Eligibility' popup "CBA" field
    And I choose random "Work state"
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section


  @ConcurrencyErr-10-03 @FEP-1044 @automated @regression
  Scenario: Edit employee employment details (Work pattern) - unsuccessful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "another employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "another employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I set "non standard working week" switch to "on"
    And I set "random" hours per day for "random" day
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section
    When I switch to "first" window
    And I set "non standard working week" switch to "on"
    And I set input fields number "12:43" hours per day for "same random" day
    Then I "accept" 'Edit work pattern' popup
    And I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    When I set "random" hours per day for "random" day
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section


  @ConcurrencyErr-10B-01 @FEP-1044 @automated @regression
  Scenario Outline: Edit employee employment details (Current Occupation) -  unsuccessful save with close pressed
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Current occupation" card in 'Employment Details'
    Then the 'Edit Current occupation' popup opens
    When I enter random test data in the 'Current occupation' popup "Job title" field
    And I enter random test data in the 'Current occupation' popup "Job start date" field
    And I enter random test data in the 'Current occupation' popup "Job end date" field
    And I enter random test data in the 'Current occupation' popup "Hours worked per week" field
    And I choose random test data in the "Employment type" select field
    And I choose random test data in the "Full/part time" select field
    And I choose random test data in the "Earnings frequency" select field
    And I enter random test data in the 'Current occupation' popup "Earnings" field
    And I "check" "Adjusted date of hire" switch
    And I enter random test data in the 'Current occupation' popup "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field
    When I switch to "first" window
    And I enter "Test job title" in the "Job title" field
    And I enter "Feb 01, 2019" in the "Job start date" field
    And I enter "Jan 11, 2039" in the "Job end date" field
    And I enter "33" in the "Hours worked per week" field
    And I enter "102.3" in the "Earnings" field
    And I choose "Seasonal" option in the "Employment type" select field
    And I choose "Part time" option in the "Full/part time" select field
    And I choose "per month" option in the "Earnings frequency" select field
    And I "check" "Adjusted date of hire" switch
    And I enter "Nov 10, 2019" in the "Adjusted date of hire" field
    And I "accept" 'Edit Current occupation' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And "Current Occupation" "Job title,Job start date,Job end date,Hours worked per week,Earnings,Adjusted date of hire" fields were refreshed correctly
    And "Current Occupation" "Employment type,Full/part time,Earnings frequency" select fields were refreshed correctly
    When I "<close>" 'Edit Current occupation' popup
    Then the 'Edit Current occupation' popup closes
    And I see that generated "Job title" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job start date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Job end date" data is saved in the 'Employment Details' tab in the "Your employee holds the job title of" field
    And I see that generated "Hours worked per week" data is saved in the 'Employment Details' tab in the "Their average hours worked per week are:" field
    And I see that generated "Employment type" data is saved in the 'Employment Details' tab in the "Their employment type is:" field
    And I see that generated "Full/part time" data is saved in the 'Employment Details' tab in the "They work on" field
    And I see that generated "Earnings frequency" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Earnings" data is saved in the 'Employment Details' tab in the "Their contractual earnings are:" field
    And I see that generated "Adjusted date of hire" data is saved in the 'Employment Details' tab in the "Their adjusted date of hire is" field

    Examples:
      | close   |
      | cancel  |
      | close X |


  @ConcurrencyErr-10B-02 @FEP-1044 @automated @regression
  Scenario Outline: Edit employee employment details (Eligibility criteria) -  unsuccessful save with close pressed
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I log in to the application with "HR test user" using new browser window
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Eligibility criteria" card in 'Employment Details'
    Then the 'Edit Eligibility criteria' popup opens
    When I randomly "check/uncheck" "Within FMLA radius" checkbox
    And I randomly "check/uncheck" "Works at home" checkbox
    And I randomly "check/uncheck" "Key employee" checkbox
    And I enter random test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter random test data in the 'Eligibility' popup "CBA" field
    And I choose random "Work state"
    And I "accept" 'Edit Current occupation' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section
    When I switch to "first" window
    And I "check" "Within FMLA radius" checkbox
    And I "uncheck" "Works at home" checkbox
    And I "check" "Key employee" checkbox
    And I enter "35" test data in the 'Eligibility' popup "Actual hours worked" field
    And I enter "0298472" test data in the 'Eligibility' popup "CBA" field
    And I "accept" 'Edit Current occupation' popup
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    Then "Eligibility Criteria" "Actual hours worked,CBA,Work state" fields were refreshed correctly
    And "Eligibility Criteria" "Within FMLA radius,Works at home,Key employee" checkboxes were refreshed correctly
    When I "<close>" 'Edit Current occupation' popup
    Then the 'Edit Eligibility criteria' popup closes
    And I see that generated "Work state" data is saved in the 'Employment Details' tab in the "Their US work state is" field
    And I see that generated "CBA" data is saved in the 'Employment Details' tab in the "They are under the Collective Bargaining Agreement (CBA) of" field
    And I see that generated "Actual hours worked" data is saved in the 'Employment Details' tab in the "They've worked a total of" field
    And I see that "Within FMLA radius" checkbox is saved on the "Eligibility criteria" section
    And I see that "Works at home" checkbox is saved on the "Eligibility criteria" section
    And I see that "Key employee" checkbox is saved on the "Eligibility criteria" section

    Examples:
      | close   |
      | cancel  |
      | close X |


  @ConcurrencyErr-10B-03 @FEP-1044 @automated @regression
  Scenario Outline: Edit employee employment details (Work pattern) -  unsuccessful save with close pressed
    Given I log in to the application with "HR admin user"
    When I open Employee details page "another employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "another employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I open "Employment Details" tab
    And I edit "Work pattern" card in 'Employment Details'
    Then the 'Edit Work Pattern' popup opens
    When I set "non standard working week" switch to "on"
    And I set "random" hours per day for "random" day
    And I "accept" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section
    When I switch to "first" window
    And I set "non standard working week" switch to "on"
    And I set input fields number "12:43" hours per day for "same random" day
    Then I "accept" 'Edit work pattern' popup
    And I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    When I "<close>" 'Edit work pattern' popup
    Then the 'Edit Work Pattern' popup closes
    And I can see the Employment Details tab is updated with changes to the work pattern section

    Examples:
      | close   |
      | cancel  |
      | close X |


  @ConcurrencyErr-12 @FEP-1045 @automated @regression
  Scenario Outline: Disabling/Enabling user - successful save
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user with small amount of cases" is "<initialState>"
    And I log in to the application with "HR test user" using new browser window
    And I navigate to Control Access page
    And I make sure that "user with small amount of cases" is "<initialState>"
    When I "<action>" "user with small amount of cases" user
    Then I can see "<finalState>" access for chosen "user with small amount of cases" on users list
    When I switch to "first" window
    And I "<action>" "user with small amount of cases" user
    Then I can see "<finalState>" access for chosen "user with small amount of cases" on users list

    Examples:
      | action  | initialState | finalState |
      | disable | enabled      | disabled   |
      | enable  | disabled     | enabled    |


  @ConcurrencyErr-13 @FEP-1046 @automated @regression
  Scenario: Disabling user - unsuccessful save
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user with small amount of cases" is "enabled"
    And I log in to the application with "HR test user" using new browser window
    And I navigate to Control Access page
    And I make sure that "user with small amount of cases" is "enabled"
    And I "disable" "user with small amount of cases" user
    And I "enable" "user with small amount of cases" user
    When I switch to "first" window
    And I "disable" "user with small amount of cases" user
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I can see "enabled" access for chosen "user with small amount of cases" on users list
    When I "disable" "user with small amount of cases" user
    Then I can see "disabled" access for chosen "user with small amount of cases" on users list
    And I "enable" "user with small amount of cases" user

  @ConcurrencyErr-14 @FEP-1046 @automated @regression
  Scenario: Enabling user - unsuccessful save
    Given I log in to the application with "HR admin user"
    And I navigate to Control Access page
    And I make sure that "user with small amount of cases" is "disabled"
    When I log in to the application with "HR test user" using new browser window
    And I navigate to Control Access page
    And I switch to "first" window
    And I "enable" "user with small amount of cases" user
    And I "disable" "user with small amount of cases" user
    Then I "can" see alert confirming "user with small amount of cases" access was "disabled"
    And I can see "disabled" access for chosen "user with small amount of cases" on users list
    When I switch to "second" window
    And I "enable" "user with small amount of cases" user
    Then I can see concurrency error pop up
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I can see "disabled" access for chosen "user with small amount of cases" on users list
    When I "enable" "user with small amount of cases" user
    Then I can see "enabled" access for chosen "user with small amount of cases" on users list

  @ConcurrencyErr-15 @FEP-1047 @automated @regression
  Scenario: Edit phone number - successful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I enter random number in the "last" "Country code,Area code,Phone number" fields and select "Cell Phone" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    When I switch to "first" window
    And I enter the same random number in the "last" "Country code,Area code,Phone number" fields and select "Cell Phone" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I see that generated "Country code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Area code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Phone number" data is saved in the 'Personal Details' tab in the "Phone number(s)" field


  @ConcurrencyErr-16 @FEP-1048 @automated @regression
  Scenario Outline: Edit phone number - unsuccessful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I enter random number in the "last" "<fieldName>" fields and select "Cell Phone" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    When I switch to "first" window
    And  I enter "778" test data in last "<fieldName>" text box
    And I "accept" "Edit Phone numbers" popup
    Then I can see concurrency error pop up
    And the "Edit" "Phone numbers" popup is still visible
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I verify last "<fieldName>" input is refreshed
    When I enter random number in the "last" "<fieldName>" fields and select "Cell Phone" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I see that generated "<fieldName>" data is saved in the 'Personal Details' tab in the "Phone number(s)" field

    Examples:
      | fieldName    |
      | Area code    |
      | Country code |
      | Phone number |


  @ConcurrencyErr-16B @FEP-1048 @automated @regression
  Scenario Outline: Edit phone number - unsuccessful save with close pressed
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Phone Numbers" section
    Then the "Edit" "Phone numbers" popup opens with "OK, Cancel, Close X" fields and correct title
    When I enter random number in the "last" "Country code, Area code, Phone number" fields and select "Cell Phone" phone type
    And I "accept" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    When I switch to "first" window
    And  I enter "09870987" test data in last "Phone number" text box
    And  I enter "9876" test data in last "Country code" text box
    And  I enter "87" test data in last "Area code" text box
    And I "accept" "Edit Phone numbers" popup
    Then I can see concurrency error pop up
    And the "Edit" "Phone numbers" popup is still visible
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I verify last "Area code" input is refreshed
    And I verify last "Country code" input is refreshed
    And I verify last "Phone number" input is refreshed
    When I "<close>" "Edit Phone numbers" popup
    Then the "Edit Phone numbers" popup closes
    And I see that generated "Country code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Area code" data is saved in the 'Personal Details' tab in the "Phone number(s)" field
    And I see that generated "Phone number" data is saved in the 'Personal Details' tab in the "Phone number(s)" field

    Examples:
      | close   |
      | cancel  |
      | close X |

  @ConcurrencyErr-17 @FEP-1049 @automated @regression
  Scenario: Edit email - successful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I enter random test data in last email text box
    And I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    When I switch to "first" window
    And I enter the same random test data in last email text box
    And I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field


  @ConcurrencyErr-18 @FEP-1050 @automated @regression
  Scenario: Edit email - unsuccessful save
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I enter random test data in last email text box
    And I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    When I switch to "first" window
    And I enter "abcd@efg1.com" test data in last email text box
    And I "accept" 'Edit emails' popup
    Then I can see concurrency error pop up
    And the "Edit" "Emails" popup is still visible
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I verify email address inputs are refreshed
    When I enter random test data in last email text box
    And I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field

  @ConcurrencyErr-18B @FEP-1050 @automated @regression
  Scenario Outline: Edit email - unsuccessful save with close pressed
    Given I log in to the application with "HR admin user"
    When I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I log in to the application with "HR test user" using new browser window
    And I open Employee details page "employee used to test concurrency error" using EmployeeId
    And I open "Employee Details" tab
    And I click the "Edit" link under the "Emails" section
    Then the "Edit" "Email" popup opens with "Email(s), OK, Cancel, Close X" fields and correct title
    When I enter random test data in last email text box
    And I "accept" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    When I switch to "first" window
    And I enter "abcd@efg1.com" test data in last email text box
    And I "accept" 'Edit emails' popup
    Then I can see concurrency error pop up
    And the "Edit" "Emails" popup is still visible
    When I click Refresh on the concurrency error pop up
    Then I see the concurrency error pop up closes
    And I verify email address inputs are refreshed
    When I "<close>" 'Edit emails' popup
    Then the 'Edit emails' popup closes
    And I see that generated "Email" data is saved in the 'Personal Details' tab in the "Email(s)" field

    Examples:
      | close   |
      | cancel  |
      | close X |