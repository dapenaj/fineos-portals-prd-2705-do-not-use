@employer-portal @C142-F02
Feature: C142-F02 - View Leaves Decided

  @C142-F02-01 @AC1 @permissions @manual
  Scenario Outline: View Leaves Decided - permission to view
    Given I log in to the application as a user "<withOrWithout>" permissions to "URL_GET_GROUPCLIENT_ABSENCES_EVENTS"
    When My Dashboard page is loaded
    Then I "<seeOrNot>" View Leaves Decided panel

    Examples:
      | withOrWithout | seeOrNot  |
      | with          | see       |
      | without       | don't see |

  # @C142-F02-01-01 @AC1 covered by @C142-F01-05

  # @C142-F02-02 @AC2 covered by @C142-F01-07

  # @C142-F02-03 @AC3 covered by @C142-F01-06

  # @C142-F02-04 @AC4 covered by @C142-F01-08

  @C142-F02-05 @AC4 @manual
  Scenario Outline: Dashboard - customizable date ranges
    Given I log in to the application
    And I customize date ranges texts for My Dashboard page
    And My Dashboard page is loaded
    When I open the date range selector in "<widget>" panel
    Then I verify the date range dropdown options are customized

    Examples:
      | widget                               |
      | Employees expected to return to work |
      | Notifications created                |
      | View leave decided                   |

  @C142-F02-06 @AC5 @AC6 @manual @sanity
  Scenario Outline: View leaves decided - grouping criteria
    Given I log in to the application
    And the dashboard personalisation is turned "on"
    When My Dashboard page with available notifications is loaded
    And I open the grouping criteria selector in the "View leaves decided" panel
    Then I see the default grouping criteria is "Decision"
    And I select a "<grouping criteria>" grouping criteria the "View leaves decided" panel
    Then I verify the "View leaves decided" graph data are grouped by "<grouping criteria>" criteria
    And I verify that the "View leaves decided" graph data contains only leaves approved or denied in selected date range

    Examples:
      | grouping criteria |
      | Decision          |
      | Reason            |
      | Group             |

  @C142-F02-07 @AC7 @regression @sanity @automated
  Scenario Outline: View leaves decided - graph total number
    Given I log in to the application
    And "Leaves decided" widget is "visible"
    And My Dashboard page with available leaves decided is loaded
    When I open the grouping criteria selector in the "View leaves decided" panel
    And I select a "<grouping criteria>" grouping criteria the "View leaves decided" panel
    Then I verify the graph total number fits to the sum of particular slices amounts

    Examples:
      | grouping criteria |
      | Decision          |
      | Reason            |
      | Group             |

  # @C142-F02-08 @AC8 covered by @C142-F01-32

  # @C142-F02-09 @AC9 @AC12 covered by @C142-F01-25/@C142-F01-28

  # @C142-F02-10 @AC10 @AC21 covered by @FAPI-9957-01 @FAPI-9957-02 @FAPI-9957-03 @FAPI-9957-04 @FAPI-9957-05

  @C142-F02-11 @AC11 @AC14 @regression @sanity @automated
  Scenario: View leaves decided - default sorting
    Given I log in to the application
    And "Leaves decided" widget is "visible"
    And I select "this week" on "Leaves decided" widget
    And I click "View details" on "Leaves decided" widget
    And I see a list of "Leaves decided" in a "this week" date range
    Then The leaves list is sorted by "decision date" with "descending" order by default
    And I verify the leaves with the same date are ordered alphabetically by the employee's first name
    And I verify Decision column shows a green tick for approved leaves
    And I verify Decision column shows a red cross for denied leaves

  # @C142-F02-12 @AC13 covered by @C142-F01-26

  # @C142-F02-13 @AC15 covered by @C142-F01-13-02

  # @C142-F02-14 @AC16 @AC17 covered by @C142-F01-27

  # @C142-F02-15 @AC18 @AC19 covered by @C142-F01-15-02

  # @C142-F02-16 @AC20 covered by @C142-F01-22/@C142-F01-23
