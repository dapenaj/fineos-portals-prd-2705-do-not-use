@employer-portal @C115-F08
Feature: C115-F08 - Employer Portal Furniture

  @C115-F08-01 @manual @regression @sanity
  Scenario: My Dashboard layout
    Given I log in to the application as "HR Admin"
    When My Dashboard page with available notifications is loaded
    Then I verify My Dashboard consist of 3 sections: Top Bar, Main Menu and Main Content Area
    And I verify Top Bar consist of: Carrier Logo, Web Application Title, Search Component, Logged-in User Profile and Action Menu
    And All elements displayed within the Top Bar do not spread beyond the maximum width of the Main Content Area
    And Top Bar is “sticky” on every Employer Portal page, so that it is always visible to all users
    And The first and left-most menu item is a hyperlink entitled “My Dashboard” that will always navigate the logged-in user to the My Dashboard Screen
    And All elements displayed within the Main Menu do not spread beyond the maximum width of the Main Content Area
    And Main Menu is “sticky” on every Employer Portal page, so that it is always visible to all users
    And I can see a Backdrop Panel located immediately beneath the Main Menu
    And Backdrop Panel always takes up 100% of the viewing screen width
    And Main Content area is always be centred on the page
    And Main Content area includes a 32-pixel gutter on the left and right-hand side
    And Main Content area (including the 32-pixel gutter on each side), have an overall maximum width of 1300 pixels


  @C115-F08-02 @customization @manual @regression
  Scenario: My Dashboard Page customization
    Given I log in to the application
    When I customize the look and feel of the app via the theme configurator
    Then I can define a custom background colour to the Top Bar
    And I can include my own corporate logo image in the Top Bar
    And I can customize text, text style, text size and text colour of Web Application Title
    And I can customize background colour of the user’s initials
    And I can customize text size, colour and style of logged-in user’s initials, first and last names
    And I can customize text size, colour, style and background color of Main Menu
    And I can customize the colour of the Backdrop Panel
