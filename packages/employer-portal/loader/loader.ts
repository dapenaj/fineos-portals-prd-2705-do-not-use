import S3 from 'aws-sdk/clients/s3';
import cookie from 'cookie';

import {
  AuthenticationCookieParam,
  EXPIRED_SESSION
} from '../src/app/shared/authentication/authentication.type';

const error = document.createElement('P');
const errorMessage = document.createTextNode(
  'Unfortunately, the application cannot be loaded right now. Please try to refresh the page, if the problem persists contact your system administrator.'
);
error.appendChild(errorMessage);

const request = new XMLHttpRequest();
request.open('GET', 'config/env.json');

request.onload = () => {
  if (request.status === 200) {
    const {
      authenticationStrategy,
      login: { url },
      aws: { region, secureResourceEnvPrefix, secureBucketName },
      monitoring: { licenseKey, applicationID }
    } = JSON.parse(request.responseText);
    const AUTHENTICATION_IS_REQUIRED: boolean =
      authenticationStrategy !== 'noAuthentication';
    const cookieValue = cookie.parse(document.cookie);

    if (!cookieValue['portal-cookie'] && AUTHENTICATION_IS_REQUIRED) {
      window.location = url;
      return;
    }

    const portalCookie = JSON.parse(
      cookieValue['portal-cookie'].replace(/\\"/g, '"')
    );

    const accessKeyId = portalCookie[AuthenticationCookieParam.ACCESS_KEY_ID];
    const secretAccessKey = portalCookie[AuthenticationCookieParam.SECRET_KEY];
    const sessionToken = portalCookie[AuthenticationCookieParam.SESSION_TOKEN];
    const expiration = portalCookie[AuthenticationCookieParam.EXPIRATION];

    if (
      expiration === sessionStorage.getItem(EXPIRED_SESSION) ||
      (!(accessKeyId && secretAccessKey && sessionToken) &&
        AUTHENTICATION_IS_REQUIRED)
    ) {
      window.location = url;
      return;
    }

    const s3 = new S3({
      accessKeyId,
      secretAccessKey,
      sessionToken,
      region
    });

    const appendScript = (src: string) => {
      const scriptTag = document.createElement('script');
      scriptTag.async = false;

      // REMOVE UNTIL FINEOS IS READY
      // const key = secureResourceEnvPrefix + src;
      // scriptTag.src = AUTHENTICATION_IS_REQUIRED
      //   ? s3.getSignedUrl('getObject', {
      //       Bucket: secureBucketName,
      //       Key: key
      //     })
      //   : src;

      scriptTag.src = src;

      document.body.appendChild(scriptTag);
    };

    for (const bundleUrl of (window as any).___FINEOS_BUNDLES) {
      appendScript(bundleUrl);
    }
  } else {
    document.body.appendChild(error);
    return;
  }
};

request.onerror = () => {
  document.body.appendChild(error);
  return;
};

request.send();
