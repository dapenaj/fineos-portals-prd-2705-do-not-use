const path = require('path');

module.exports = {
  entry: './loader.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  output: {
    filename: 'loader.js',
    path: path.resolve(__dirname, '..', 'build')
  }
};
