# Employer Portal configuration (version 21.2)

Employer portal UI can be customised via `/client-config.json` and `/portal.env.json` files which are loaded during application loading. A static maintenance page can also be customised.

The next section will describe recent changes introduced in v21.2. This will be followed by a description of each config file in detail, along with a description of how to customise the maintenance page.

## What's New in v21.2

The following configuration changes were introduced in v21.2:

- Additional localisation config inside `/portal.env.json` to specify per-language options for:
  - decimalSeparator
  - displayDateTimeFormat
- A configurable maintenance page

## Portal Env Config

This is small config file `/portal.env.json` which define few configuration options, and in general looks like this:

```json
{
  "dates": {
    "dateOnlyFormat": "YYYY-MM-DD",
    "displayDateFormat": "MMM DD, YYYY",
    "dateTimeFormat": "YYYY-MM-DDTHH:mm:ss",
    "displayDateTimeFormat": {
      "en": "MMM DD, YYYY - h:mm A"
    }
  },
  "currencyCode": "USD",
  "decimalSeparator": { "en": "." },
  "supportedLanguages": ["en"],
  "uploadFile": {
    "maxSize": "1MB",
    "allowedExtensions": [
      ".xml",
      ".txt",
      ".doc",
      ".pdf",
      ".ppt",
      ".xls",
      ".jpg",
      ".jpeg",
      ".png",
      ".vsd",
      ".msg",
      ".bmp",
      ".docx",
      ".xlsx",
      ".xlsm",
      ".pptx"
    ]
  }
}
```

Let's go though each configuration option.

- `dates.dateOnlyFormat` - this is the API date we'll use if no config can be found matches what is configured in dev severs
  (formatting is based on [Moment.js](https://momentjs.com/docs/#/displaying/))
- `dates.displayDateFormat` - this is how date should be normally displayed in UI
  (formatting is based on [Moment.js](https://momentjs.com/docs/#/displaying/))
- `dates.displayDateTimeFormat` - this is how date and time should be normally displayed in UI. An option must be specified for each supported language.
  (formatting is based on [Moment.js](https://momentjs.com/docs/#/displaying/))
- `currencyCode` - current currency code which would be used for money data
- `decimalSeparator` - the decimal separator that is used for displaying/inputting numbers in a specific language. An option must be specified for each supported language.
- `supportedLanguages` - list of supported languages
- `uploadFile.maxSize` - max size for the file upload
- `uploadFile.allowedExtensions` - list of allowed extensions for the file upload

## Client Config

Main UI customisation is placed in `/client-config.json` file, which is also may be called as Client Config.

Client config define 3 things:

1. Theme
2. Size lists
3. Features

### Client Config UI Configurator

There is an option to configure UI in real-time in the browser via UI Configurator.

In order to use, user should has URL_EMPLOYERPORTAL_THEMECONFIGURATION permission.

UI Configurator allow to set all Theme and Custom Messages in user friendly interface, with possibility to export it in valid `client-config.json`.

![UI Configurator](./img/ui-configurator.png)

![UI Configurator - Custom Color](./img/ui-configurator-custom-color.png)

### Theme

#### Color Schema

In employer portal, almost all [colors in hex](#hex-color) can be configured by providing `theme.colorSchema` in client config.
Example of palette:

```json
{
  "theme": {
    "colorSchema": {
      "primaryColor": "#932b6a",
      "primaryColorContrast": "#ffffff",
      "callToActionColor": "#e27816",
      "primaryAction": "#007bc2",
      "successColor": "#35b558",
      "pendingColor": "#ffc20e",
      "attentionColor": "#f04c3f",
      "infoColor": "#23b7d6",
      "neutral9": "#333333",
      "neutral8": "#666666",
      "neutral7": "#999999",
      "neutral6": "#b3b3b3",
      "neutral5": "#d2d0d5",
      "neutral4": "#e9e7ec",
      "neutral3": "#e6e6e6",
      "neutral2": "#f4f5f6",
      "neutral1": "#ffffff",
      "graphColorOne": "#932b6a",
      "graphColorTwo": "#00aabc",
      "graphColorThree": "#ffc20e",
      "graphColorFour": "#35b558",
      "graphColorFive": "#f6861f",
      "graphColorSix": "#708dd8"
    }
  }
}
```

Next colors can be defined:

- `primaryColor`, default value `#932b6a`
- `primaryColorContrast`, optional color which can be used for text placed on background with primary color, default is `#ffffff`
- `callToActionColor`, default value `#e27816`
- `primaryAction`, default value `#007bc2`
- `successColor`, default value `#35b558`
- `pendingColor`, default value `#ffc20e`
- `attentionColor`, default value `#f04c3f`
- `infoColor`, default value `#23b7d6`

- `neutral9`, default value `#333333`
- `neutral8`, default value `#666666`
- `neutral7`, default value `#999999`
- `neutral6` (disabledTextColor), default value `#b3b3b3`
- `neutral5`, default value `#d2d0d5`
- `neutral4`, default value `#e9e7ec`
- `neutral3` (disabledBackgroundColor), default value `#e6e6e6`
- `neutral2` (backgroundColor), default value `#f4f5f6`
- `neutral1`, default value `#ffffff`

- `graphColorOne`, default value `#932b6a`
- `graphColorTwo`, default value `#00aabc`
- `graphColorThree`, default value `#ffc20e`
- `graphColorFour`, default value `#35b558`
- `graphColorFive`, default value `#f6861f`
- `graphColorSix`, default value `#708dd8`

#### Typography

In employer portal defined several fonts which is used in different parts of the system.
It can be customized by specifying it in `theme.typography`. Here's example:

```json
{
  "theme": {
    "typography": {
      "displayTitle": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 300,
        "fontSize": 36,
        "lineHeight": 44
      },
      "displaySmall": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 300,
        "fontSize": 24,
        "lineHeight": 30
      },
      "pageTitle": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 600,
        "fontSize": 24,
        "lineHeight": 30
      },
      "panelTitle": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 18,
        "lineHeight": 25
      },
      "baseText": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 14,
        "lineHeight": 22
      },
      "baseTextSemiBold": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 600,
        "fontSize": 14,
        "lineHeight": 22
      },
      "smallText": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 12,
        "lineHeight": 16
      },
      "smallTextSemiBold": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 600,
        "fontSize": 12,
        "lineHeight": 16
      },
      "baseTextUpperCase": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 14,
        "lineHeight": 19,
        "textTransform": "uppercase"
      },
      "smallTextUpperCase": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 11,
        "lineHeight": 15,
        "textTransform": "uppercase"
      }
    }
  }
}
```

Next fonts can be defined via [text styles objects](#text-styles), default values for them can be found in example above:

- `displayTitle`
- `displaySmall`
- `pageTitle`
- `panelTitle`
- `baseText`
- `baseTextSemiBold`
- `baseTextUpperCase`
- `smallText`
- `smallTextSemiBold`
- `smallTextUpperCase`

#### Header

Theme allow to change how employer portal header looks like, by providing configuration object in `theme.header`.
Example:

```json
{
  "theme": {
    "header": {
      "logo": "/logo.svg",
      "hideTitle": true,
      "titleSeparator": "-",
      "titleColor": "#00ff00",
      "titleStyles": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 11,
        "lineHeight": 15,
        "textTransform": "uppercase"
      }
    }
  }
}
```

Next values can be configured:

- `logo` - logo which would be displayed in the top header, please keep in mind that it should have a small height.
  No default value, this is required.
- `title` - text that should be shown after logo in the header and actually used
  as site title (will also appear in browser tab/window title). No default value, but not required.
- `titleSeparator` - separator which should be between `logo` and `title`, if not
  provided then title would be shown next to logo. Such separator can be `|` (vertical bar), or `-` (dash)
- `titleColor` - title [color in hex](#hex-color), optional value.
  If not provided then `neutral1` would be used if header background is dark, or `neutral9` if background is bright.
- `titleStyles` - title [text styles](#text-styles), optional value, if not provided then `baseText` would be applied

#### Theme Objects

##### Text styles

Text styles is a JSON object which allow to define several properties of the text. Example of such object:

```json
{
  "fontFamily": "\"Open Sans\"",
  "fontStyle": "normal",
  "fontWeight": "normal",
  "fontSize": 11,
  "lineHeight": 15,
  "textTransform": "uppercase"
}
```

Here's explanation of each property.

- `fontFamily` - font family name which is available on [Google Fonts](https://fonts.google.com/). Better not use a lot of fonts in theme, but stick to one.
- `fontStyle` - there are 2 valid options can be provided `normal` and `italic`. _Italic text_; normal text. Default value is `normal`.
- `fontWeight` - font weight.
- `fontSize` - font size in pixels.
- `lineHeight` - line height in pixels.
- `textTransform` - allow to force text transformation, allow values: `capitalize`, `uppercase`, `lowercase` and `none` (default).

##### HEX color

A color hex code is a way of specifying color using hexadecimal values.
The code itself is a hex triplet, which represents three separate values that specify the levels of the component colors.
The code starts with a hash sign (#) and is followed by six hex values or three hex value pairs (for example, #AFD645).

#### Images

Theme allow to customize some of images in Employer portal, by providing them in `theme.images`.
Image should be provided by it's URL. Example:

```json
{
  "theme": {
    "images": {
      "somethingWentWrong": "/assets/img/something_went_wrong_grey.svg",
      "somethingWentWrongColor": "/assets/img/something_went_wrong_color.svg",
      "uploadFail": "/assets/img/upload_fail.svg",
      "uploadSuccess": "/assets/img/upload_success.svg",
      "welcomeBackground": "/assets/img/hexagons_pattern_ER.svg",
      "greenCheckIntake": "/assets/img/green_check_intake.svg",
      "fnolRequestAcknowledged": "/assets/img/fnol_request_acknowledged.svg",
      "timeout": "/assets/img/timeout.svg",
      "dataConcurrency": "/assets/img/concurrency.svg"
    }
  }
}
```

Currently supported next custom images:

- `somethingWentWrong`, default image:

![Something went wrong image](../public/assets/img/something_went_wrong_grey.svg)

- `somethingWentWrongColor`, default image:

![Something went wrong image](../public/assets/img/something_went_wrong_color.svg)

- `uploadFail`, default image:

![Upload fail image](../public/assets/img/upload_fail.svg)

- `uploadSuccess`, default image:

![Upload success image](../public/assets/img/doc_received.svg)

- `welcomeBackground`, default image:

![Hexagons](../public/assets/img/hexagons_pattern_ER.svg)

- `fnolRequestAcknowledged`, default image:

![FNOL request acknowledged image](../public/assets/img/fnol_request_acknowledged.svg)

- `greenCheckIntake`, default image:

![Green Check Intake](../public/assets/img/green_check_intake.svg)

- `timeout`, default image:

![Data Concurrency](../public/assets/img/timeout.svg)

- `dataConcurrency`, default image:

![Data Concurrency](../public/assets/img/concurrency.svg)

#### Icons

Theme allow to customize some of icons in Employer portal, by providing them in `theme.icons`. Icon should be provided by it's URL to target SVG. Loading SVG is a subject of CORS (Cross-Origin Resource Sharing) policy.

SVG should not include any styling via CSS, scripts, external images or fonts (even if it would be provided, portal would strip it out), the reason for this is security, cause scripts/CSS/fonts maybe used for different attacks. SVG icon can have 1 custom color via currentColor (you can read more about this technique can be found here [https://css-tricks.com/cascading-svg-fill-color/](https://css-tricks.com/cascading-svg-fill-color/)).

Example of icons config:

```json
{
  "theme": {
    "icons": {
      "MESSAGES.EMPLOYER": "/assets/icon/user-circle.svg",
      "MESSAGES.CARRIER": "/assets/icon/avatar_FINSURE_ER.svg"
    }
  }
}
```

Currently supported next custom icons:

`MESSAGES.EMPLOYER`, default icon:
![/assets/icon/user-circle.svg](../public/assets/icon/user-circle.svg)

`MESSAGES.CARRIER`, default icon:
![/assets/icon/avatar_FINSURE_ER.svg](../public/assets/icon/avatar_FINSURE_ER.svg)

#### High Contrast Mode

The Employer Portal also supports a high contrast mode theme. This extra theme is defined as a nested theme, by providing a theme object in `theme.highContrastMode`. This nested theme has all of the same attributes as defined in the previous sections.

Example of high contrast mode config:

```json
{
  "theme": {
    "highContrastMode": {
      "colorSchema": {...},
      "typography": {...},
      "header": {...},
      "images": {...}
    }
  }
}
```

### Size List

It also possible to configure some lists size.

- Payments History - this limit number of payments shown in payment history. This can be configured via `configurableSizeList.PAYMENTS_HISTORY` value in `client-config.json`. Example:

```json
{
  "configurableSizeList": {
    "PAYMENTS_HISTORY": 12
  }
}
```

### Session Timeout

It also possible to configure session timeout.

- Session handling - Decides how much time the dialogue will appear before the end of the session. This can be configured via `configurableSessionTimeoutSeconds.SESSION_TIMEOUT_SECONDS` value in `client-config.json`. Example:

```json
{
  "configurableSessionTimeoutSeconds": {
    "SESSION_TIMEOUT_SECONDS": 30
  }
}
```

### Default fixed work pattern day length

It is possible to configure day length

- This can be configured via `configurableSessionTimeoutSeconds.SESSION_TIMEOUT_SECONDS` value in `client-config.json`. Example:

```json
{
  "configurableWorkPattern": {
    "FIXED_DAY_LENGTH": {
      "hours": 8,
      "minutes": 0
    }
  }
}
```

### Features

It also possible to configure the availability of some features

- Dashboard - this indicates whether the Dashboard page should be available. If `true` then the Dashboard page is displayed instead of the Home page. If `false`, then the Home page (with a list of Outstanding Notifications) is shown instead. This can be configured via `features.showDashboard` value in `client-config.json`. Example:

```json
{
  "features": {
    "showDashboard": true
  }
}
```

- HelpInfo - this indicates whether the Dashboard page should include additional help info to provide additional description to the user. If `true` then the Dashboard page has additional help info icons. If `false`, then no help info icons are displayed to the user. This can be configured via `features.helpInfoEnabled` value in `client-config.json`. Example:

```json
{
  "features": {
    "helpInfoEnabled": true
  }
}
```

### Full client-config.json Example

```json
{
  "theme": {
    "colorSchema": {
      "primaryColor": "#932b6a",
      "primaryColorContrast": "#ffffff",
      "callToActionColor": "#e27816",
      "primaryAction": "#007bc2",
      "successColor": "#35b558",
      "pendingColor": "#ffc20e",
      "attentionColor": "#f04c3f",
      "infoColor": "#23b7d6",
      "neutral9": "#333333",
      "neutral8": "#666666",
      "neutral7": "#999999",
      "neutral6": "#b3b3b3",
      "neutral5": "#d2d0d5",
      "neutral4": "#e9e7ec",
      "neutral3": "#e6e6e6",
      "neutral2": "#f4f5f6",
      "neutral1": "#ffffff",
      "graphColorOne": "#932b6a",
      "graphColorTwo": "#00aabc",
      "graphColorThree": "#ffc20e",
      "graphColorFour": "#35b558",
      "graphColorFive": "#f6861f",
      "graphColorSix": "#708dd8"
    },
    "typography": {
      "displayTitle": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 300,
        "fontSize": 36,
        "lineHeight": 44
      },
      "displaySmall": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 300,
        "fontSize": 24,
        "lineHeight": 30
      },
      "pageTitle": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 600,
        "fontSize": 24,
        "lineHeight": 30
      },
      "panelTitle": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 18,
        "lineHeight": 25
      },
      "baseText": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 14,
        "lineHeight": 22
      },
      "baseTextSemiBold": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 600,
        "fontSize": 14,
        "lineHeight": 22
      },
      "smallText": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 12,
        "lineHeight": 16
      },
      "smallTextSemiBold": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": 600,
        "fontSize": 12,
        "lineHeight": 16
      },
      "baseTextUpperCase": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 14,
        "lineHeight": 19,
        "textTransform": "uppercase"
      },
      "smallTextUpperCase": {
        "fontFamily": "\"Open Sans\"",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontSize": 11,
        "lineHeight": 15,
        "textTransform": "uppercase"
      }
    },
    "header": {
      "logo": "/assets/img/ER_signature_white.svg",
      "logoAltText": "Employer Portal logo",
      "hideTitle": true
    },
    "images": {
      "somethingWentWrong": "/assets/img/something_went_wrong_grey.svg",
      "somethingWentWrongColor": "/assets/img/something_went_wrong_color.svg",
      "uploadFail": "/assets/img/upload_fail.svg",
      "uploadSuccess": "/assets/img/upload_success.svg",
      "welcomeBackground": "/assets/img/hexagons_pattern_ER.svg",
      "greenCheckIntake": "/assets/img/green_check_intake.svg",
      "fnolRequestAcknowledged": "/assets/img/fnol_request_acknowledged.svg",
      "timeout": "/assets/img/timeout.svg",
      "dataConcurrency": "/assets/img/concurrency.svg"
    },
    "icons": {
      "MESSAGES.EMPLOYER": "/assets/icon/user-circle.svg",
      "MESSAGES.CARRIER": "/assets/icon/avatar_FINSURE_ER.svg"
    },
    "highContrastMode": {
      "colorSchema": {
        "primaryColor": "#932b6a",
        "primaryColorContrast": "#ffffff",
        "callToActionColor": "#B05E11",
        "primaryAction": "#0067A3",
        "successColor": "#35b558",
        "pendingColor": "#ffc20e",
        "attentionColor": "#d11d10",
        "infoColor": "#23b7d6",
        "neutral9": "#2b2b2b",
        "neutral8": "#666666",
        "neutral7": "#666666",
        "neutral6": "#767676",
        "neutral5": "#949494",
        "neutral4": "#e9e7ec",
        "neutral3": "#e6e6e6",
        "neutral2": "#f4f5f6",
        "neutral1": "#ffffff",
        "graphColorOne": "#932b6a",
        "graphColorTwo": "#00aabc",
        "graphColorThree": "#ffc20e",
        "graphColorFour": "#35b558",
        "graphColorFive": "#f6861f",
        "graphColorSix": "#708dd8"
      },
      "typography": {
        "displayTitle": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": 300,
          "fontSize": 36,
          "lineHeight": 44
        },
        "displaySmall": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": 300,
          "fontSize": 24,
          "lineHeight": 30
        },
        "pageTitle": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": 600,
          "fontSize": 24,
          "lineHeight": 30
        },
        "panelTitle": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": "normal",
          "fontSize": 18,
          "lineHeight": 25
        },
        "baseText": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": "normal",
          "fontSize": 14,
          "lineHeight": 22
        },
        "baseTextSemiBold": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": 600,
          "fontSize": 14,
          "lineHeight": 22
        },
        "smallText": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": "normal",
          "fontSize": 12,
          "lineHeight": 16
        },
        "smallTextSemiBold": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": 600,
          "fontSize": 12,
          "lineHeight": 16
        },
        "baseTextUpperCase": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": "normal",
          "fontSize": 14,
          "lineHeight": 19,
          "textTransform": "uppercase"
        },
        "smallTextUpperCase": {
          "fontFamily": "\"Open Sans\"",
          "fontStyle": "normal",
          "fontWeight": "normal",
          "fontSize": 11,
          "lineHeight": 15,
          "textTransform": "uppercase"
        }
      },
      "header": {
        "logo": "/assets/img/ER_signature_white.svg",
        "logoAltText": "Employer Portal logo",
        "hideTitle": true
      },
      "images": {
        "somethingWentWrong": "/assets/img/something_went_wrong_grey.svg",
        "somethingWentWrongColor": "/assets/img/something_went_wrong_color.svg",
        "uploadFail": "/assets/img/upload_fail.svg",
        "uploadSuccess": "/assets/img/upload_success.svg",
        "welcomeBackground": "/assets/img/hexagons_pattern_ER.svg",
        "greenCheckIntake": "/assets/img/green_check_intake.svg",
        "fnolRequestAcknowledged": "/assets/img/fnol_request_acknowledged.svg",
        "timeout": "/assets/img/timeout.svg",
        "dataConcurrency": "/assets/img/concurrency.svg"
      }
    }
  },
  "configurableSizeList": {
    "PAYMENTS_HISTORY": 12
  },
  "configurableSessionTimeoutSeconds": {
    "SESSION_TIMEOUT_SECONDS": 30
  },
  "configurableWorkPattern": {
    "FIXED_DAY_LENGTH": {
      "hours": 8,
      "minutes": 0
    }
  },
  "features": {
    "showDashboard": true,
    "helpInfoEnabled": true
  }
}
```

## Maintenance Page

The maintenance page is a static page that can be manually customised.

The page can be found inside the `/employer-portal/maintenance/` directory.

- `index.html` - the html file for the maintenance page.
- `/img` - a directory of images displayed in the page.

Inline CSS is used for minimal page styling and can be modified directly in the `index.html` file. Any changes to the image files will also need to be reflected in `index.html`.

![Maintenance Page](./img/maintenance-page.png)

Default text is hardcoded within the `index.html` file. An additional text message can be displayed beneath this default text.

For steps on how to configure this additional text, and to start and stop the maintenance window, please refer to the release note for _FCENG-3751 Portals “Maintenance Window” Start/Stop IAC Plays_ under [](https://platform-docs.fineos.com/iac/v3.5.0/portals/maintenance_window/maintenance_window_overview.html)
