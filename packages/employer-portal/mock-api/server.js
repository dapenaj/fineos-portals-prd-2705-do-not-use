// for documentation go to https://github.com/typicode/json-server
const lodash = require('lodash');
const jsonServer = require('json-server');
const moment = require('moment');

const absenceEmployment = require('../src/mock-data/absence-employment.json');
const absenceEvents = require('../src/mock-data/absence-events.json');
const actualAbsencePeriods = require('../src/mock-data/actual-absence-periods.json');
const benefits = require('../src/mock-data/benefits.json');
const claims = require('../src/mock-data/claims.json');
const documents = require('../src/mock-data/documents.json');
const notifications = require('../src/mock-data/notifications.json');
const accommodations = require('../src/mock-data/accommodation-cases.json');
const customers = require('../src/mock-data/customers.json');
const absencePeriodDecisions = require('../src/mock-data/absence-period-decisions.json');
const readDisabilityDetails = require('../src/mock-data/read-disability-details.json');
const outstandingInformation = require('../src/mock-data/outstanding-information.json');
const payments = require('../src/mock-data/payments.json');
const paymentLines = require('../src/mock-data/payment-lines.json');
const customerOccupations = require('../src/mock-data/customer-occupations.json');
const contractualEarnings = require('../src/mock-data/contractual-earnings.json');
const phoneNumbers = require('../src/mock-data/phone-numbers.json');
const emailAddresses = require('../src/mock-data/email-addresses.json');
const endpointPermissions = require('../src/mock-data/endpoint-permissions.json');
const communicationPreferences = require('../src/mock-data/communication-preferences.json');
const users = require('../src/mock-data/users.json');
const base64Downloads = require('../src/mock-data/base-64-downloads.json');
const enumInstances = require('../src/mock-data/enum-instances.json');
const eForms = require('../src/mock-data/eForms.json');
const employeeLeaveBalance = require('../src/mock-data/employee-leave-balance.json');
const timeTakenAbsenceData = require('../src/mock-data/time-taken-absence-period-decisions.json');
const workPatternData = require('../src/mock-data/work-pattern.json');
const webMessages = require('../src/mock-data/web-messages.json');
const groupPermissions = require('../src/mock-data/group-permissions.json');
const employeesApprovedForLeaves = require('../src/mock-data/employees-approved-for-leave.json');

const dbObj = {
  ...absenceEmployment,
  ...absenceEvents,
  ...actualAbsencePeriods,
  ...claims,
  ...benefits,
  ...notifications,
  ...documents,
  ...accommodations,
  ...customers,
  ...absencePeriodDecisions,
  ...readDisabilityDetails,
  ...outstandingInformation,
  ...payments,
  ...paymentLines,
  ...customerOccupations,
  ...contractualEarnings,
  ...phoneNumbers,
  ...emailAddresses,
  ...endpointPermissions,
  ...communicationPreferences,
  ...users,
  ...base64Downloads,
  ...enumInstances,
  ...eForms,
  ...employeeLeaveBalance,
  ...timeTakenAbsenceData,
  ...workPatternData,
  ...webMessages,
  ...groupPermissions,
  ...employeesApprovedForLeaves
};

// CONFIGURATION
const server = jsonServer.create();

const getDbInitialState = () => lodash.cloneDeep(dbObj);

const router = jsonServer.router(getDbInitialState());
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 3001;
const apiPrefix = '/api/v1';

server.use(middlewares);
server.use(jsonServer.bodyParser);

// GENERAL REQUEST HANDLING

// Reset
server.post(`${apiPrefix}/reset`, (req, res) => {
  router.db.setState(getDbInitialState());

  res.sendStatus(200);
});

// SPECIFIC ENDPOINTS
server.get(`${apiPrefix}/groupClient/notifications`, (req, res) => {
  let notificationsStream;
  let elements;

  // if paginated, we'll handle this differently
  const hasPagination =
    !!(req.query.pageIndex || req.query.pageIndex === 0) &&
    !!req.query.pageSize;

  const hasExpectedRTWDate =
    !!req.query['expectedRTWDate._ge'] || !!req.query['expectedRTWDate._le'];
  const hasCreatedDate =
    !!req.query['createdDate._ge'] || !!req.query['createdDate._le'];

  if (hasPagination) {
    // figure out sorting params
    const sortParam = req.query.sort || 'createdDate';
    const [sortProp, sortDir] = sortParam.startsWith('-')
      ? [sortParam.substring(1), 'desc']
      : [sortParam, 'asc'];

    notificationsStream = router.db
      .get('notifications')
      .orderBy(sortProp, sortDir)
      .value();

    // only supporting caseNumber filtering for now
    if (req.query['caseNumber._startsWith']) {
      notificationsStream = notificationsStream.filter(notification =>
        notification.caseNumber.startsWith(req.query['caseNumber._startsWith'])
      );
    }

    const pageIndex = req.query.pageIndex
      ? parseInt(req.query.pageIndex, 10)
      : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize, 10) : 10;
    const start = pageIndex * pageSize;
    const end = start + pageSize;

    elements = notificationsStream.slice(start, end);
  } else if (!hasPagination && hasExpectedRTWDate) {
    const startDate = req.query['expectedRTWDate._ge'];
    const endDate = req.query['expectedRTWDate._le'];

    notificationsStream = router.db
      .get('notifications')
      .map(notification => ({
        // dynamically mapping expectedRTWDate to date range within month
        ...notification,
        expectedRTWDate: moment()
          .add(Math.floor(Math.random() * 30), 'day')
          .format('YYYY-MM-DD')
      }))
      .filter(
        ({ expectedRTWDate }) =>
          moment(expectedRTWDate).isSameOrAfter(moment(startDate)) &&
          moment(expectedRTWDate).isSameOrBefore(moment(endDate))
      )
      .value();
    elements = notificationsStream;
  } else if (!hasPagination && hasCreatedDate) {
    const startDate = req.query['createdDate._ge'];
    const endDate = req.query['createdDate._le'];

    notificationsStream = router.db
      .get('notifications')
      .map(notification => ({
        // dynamically mapping createdDate to date range within month
        ...notification,
        createdDate: moment()
          .startOf('month')
          .add(Math.floor(Math.random() * 30), 'day')
          .format('YYYY-MM-DD')
      }))
      .filter(
        ({ createdDate }) =>
          moment(createdDate).isSameOrAfter(moment(startDate)) &&
          moment(createdDate).isSameOrBefore(moment(endDate))
      )
      .value();
    elements = notificationsStream;
  } else {
    if (req.query['customer.id']) {
      notificationsStream = router.db
        .get('notifications')
        .filter({ customerId: req.query['customer.id'] });
    } else if (req.query['caseNumber._startsWith']) {
      notificationsStream = router.db
        .get('notifications')
        .filter(notification =>
          notification.caseNumber.startsWith(
            req.query['caseNumber._startsWith']
          )
        );
    } else {
      notificationsStream = router.db.get('notifications');
    }

    elements = notificationsStream;
  }

  if (elements) {
    const notificationsWithCustomer = elements.map(notification => ({
      ...notification,
      customer: router.db
        .get('customers')
        .filter({
          id: notification.customerId
        })
        .map(customer => ({
          ...customer,
          name: `${customer.firstName} ${customer.lastName}`
        }))
        .head()
        .value()
    }));

    res.status(200).json({
      elements: notificationsWithCustomer,
      totalSize: notificationsStream.length
    });
  } else {
    return res.status(200).json({ elements: [], totalSize: 0 });
  }
});

server.get(`${apiPrefix}/groupClient/notifications/:id`, (req, res) => {
  const notification = router.db
    .get('notifications')
    .find({ caseNumber: req.params.id })
    .value();

  if (notification) {
    res.status(200).json({
      ...notification,
      customer: router.db
        .get('customers')
        .filter({
          id: notification.customerId
        })
        .map(customer => ({
          ...customer,
          name: `${customer.firstName} ${customer.lastName}`
        }))
        .head()
        .value()
    });
  } else {
    return res.status(404).json({});
  }
});

server.post(`${apiPrefix}/groupClient/startNotification`, (req, res) => {
  const { id, ...notification } = router.db
    .get('notifications')
    .first()
    .value();

  res.status(200).json({ notificationId: id, ...notification });
});

server.post(
  `${apiPrefix}/groupClient/absences/absence-cases/:id/leave-periods-change-request`,
  (req, res) => {
    const { body } = req;
    if (!body.reason || !body.changeRequestPeriods) {
      res.status(400).send({
        error: 'Incorrect POST request data'
      });
      return;
    }
    res.status(200).json(body);
  }
);

server.get(
  `${apiPrefix}/groupClient/absences/absence-period-decisions`,
  (req, res) => {
    const absencePeriodDecisionsData = router.db
      .get('absencePeriodDecisions')
      .value();

    const timeTakenAbsence = router.db.get('timeTakenAbsenceData').value();

    if (req.query.absenceId === 'ABS-1') {
      res.status(403).json({
        status: 403,
        message: 'Forbidden',
        json: {}
      });
      return;
    }

    const queriedAbsencePeriodDecisions = absencePeriodDecisionsData.find(
      periodDecision => periodDecision.id === req.query.absenceId
    );
    const defaultAbsencePeriodDecisions = absencePeriodDecisionsData.find(
      periodDecision => periodDecision.id === 'default'
    );

    const resultWithAbsenceId = queriedAbsencePeriodDecisions
      ? queriedAbsencePeriodDecisions.response
      : defaultAbsencePeriodDecisions.response;

    const result = req.query.customerId
      ? timeTakenAbsence
      : resultWithAbsenceId;

    if (result) {
      res.status(200).json(result);
    } else {
      return res.status(404).json([]);
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/absences/:customerId/leave-plans/:leavePlanId/leave-availability`,
  (req, res) => {
    const employeeLeaveBalanceData = router.db
      .get('employeeLeaveBalance')
      .value();

    const result = employeeLeaveBalanceData.filter(
      leaveBalance => leaveBalance.leavePlanId === req.params.leavePlanId
    );

    if (result) {
      res.status(200).json(result[0]);
    } else {
      return res.status(404).json([]);
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/claims/:id/readDisabilityDetails`,
  (req, res) => {
    const readDisabilityDetailsData = router.db
      .get('readDisabilityDetails')
      .find(
        readDisabilityDetail =>
          readDisabilityDetail.claimSummary.claimId === req.params.id
      )
      .value();

    if (readDisabilityDetailsData) {
      res.status(200).json(readDisabilityDetailsData);
    } else {
      return res.status(404).json({});
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/absences/accommodation-cases/:accommodationCaseId`,
  (req, res) => {
    const accommodationCaseInfo = router.db
      .get('accommodation-cases')
      .find({ id: req.params.accommodationCaseId })
      .value();
    if (accommodationCaseInfo) {
      res.status(200).json(accommodationCaseInfo);
    }
  }
);

server.get(`${apiPrefix}/groupClient/endpoint-permissions`, (req, res) => {
  res.json({
    elements: router.db.get('permissions').value()
  });
});

server.get(`${apiPrefix}/groupClient/permission-groups`, (req, res) => {
  res.json({
    elements: router.db.get('groupPermissions').value()
  });
});

server.get(`${apiPrefix}/groupClient/claims/:claimId/benefits`, (req, res) => {
  try {
    const benefitsArr = router.db
      .get('benefits')
      .find({ claimId: req.params.claimId })
      .value().benefits;
    if (benefitsArr) {
      res.status(200).json(benefitsArr);
    } else {
      return res.status(404).json({});
    }
  } catch (e) {
    return res.status(404).json({});
  }
});

server.get(
  `${apiPrefix}/groupClient/claims/:claimId/benefits/:benefitId/readDisabilityBenefit`,
  (req, res) => {
    try {
      const disabilityBenefitResponse = router.db
        .get('readDisabilityBenefits')
        .find({ benefitId: req.params.benefitId })
        .value().response;
      if (disabilityBenefitResponse) {
        res.status(200).json(disabilityBenefitResponse);
      } else {
        return res.status(404).json({});
      }
    } catch (e) {
      return res.status(404).json({});
    }
  }
);

server.get(`${apiPrefix}/groupClient/customers`, (req, res) => {
  if (req.query._crossField) {
    const query = req.query._crossField.toLowerCase();
    const customersData = router.db
      .get('customers')
      .filter(
        customer =>
          customer.firstName.toLowerCase().includes(query) ||
          customer.lastName.toLowerCase().includes(query) ||
          customer.id.toLowerCase().includes(query)
      )
      .map(customer => ({
        ...customer,
        customerNo: customer.id
      }))
      .value();

    if (customersData) {
      res
        .status(200)
        .json({ elements: customersData, totalSize: customersData.length });
    } else {
      return res.status(404).json({ elements: [], totalSize: 0 });
    }
  } else {
    return res.status(404).json({ elements: [], totalSize: 0 });
  }
});

server.get(`${apiPrefix}/groupClient/customers/:customerId`, (req, res) => {
  const customerInfo = router.db
    .get('customers')
    .find({ id: req.params.customerId })
    .value();
  if (customerInfo) {
    res.status(200).json(customerInfo);
  } else {
    return res.status(404).json({});
  }
});

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/customer-info`,
  (req, res) => {
    const customerInfo = router.db
      .get('customers')
      .find({ id: req.params.customerId })
      .value();
    if (customerInfo) {
      res
        .status(200)
        .header('ETag', req.params.customerId)
        .json(customerInfo);
    } else {
      return res.status(404).json({});
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-info/edit`,
  (req, res) => {
    const customerInfo = router.db
      .get('customers')
      .find({ id: req.params.customerId })
      .assign(req.body)
      .write();

    res.status(200).json(customerInfo);
  }
);

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/phone-numbers`,
  (req, res) => {
    const elements = router.db.get('phoneNumbers').value();

    if (elements) {
      res.status(200).json({ elements, totalSize: elements.length });
    } else {
      return res.status(404).json({ elements: [], totalSize: 0 });
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/phone-numbers/:phoneNumbersId/edit`,
  (req, res) => {
    const { phoneNumbersId } = req.params;
    const phoneNumber = router.db
      .get('phone-numbers')
      .find(({ id }) => phoneNumbersId === id)
      .assign(req.body)
      .write();

    return res.status(200).json(phoneNumber);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/phone-numbers`,
  (req, res) => {
    const numberId = Math.floor(Math.random() * 100).toString();

    const phoneNumberList = router.db.get('phoneNumbers').value();
    const addedPhoneNumber = { ...req.body, id: numberId };
    router.db
      .get('phoneNumbers')
      .assign([addedPhoneNumber, ...phoneNumberList])
      .write();

    return res.status(200).json(addedPhoneNumber);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/phone-numbers/:phoneNumberId/remove`,
  (req, res) => {
    const { phoneNumberId } = req.params;
    const phoneNumber = router.db.get('phoneNumbers').removeById(phoneNumberId);

    return res.status(200).json(phoneNumber);
  }
);

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/email-addresses`,
  (req, res) => {
    const elements = router.db.get('emailAddresses').value();

    if (elements) {
      res.status(200).json({ elements, totalSize: elements.length });
    } else {
      return res.status(404).json({ elements: [], totalSize: 0 });
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/email-addresses/:emailAddressId/edit`,
  (req, res) => {
    const { emailAddressId } = req.params;
    const email = router.db
      .get('email-addresses')
      .find(({ id }) => emailAddressId === id)
      .assign(req.body)
      .write();

    return res.status(200).json(email);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/email-addresses`,
  (req, res) => {
    const emailId = Math.floor(Math.random() * 100).toString();
    const addedEmail = { ...req.body, id: emailId };
    const emailAddressList = router.db.get('emailAddresses').value();
    router.db
      .get('emailAddresses')
      .assign([...emailAddressList, addedEmail])
      .write();

    return res.status(200).json(addedEmail);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/email-addresses/:emailAddressId/remove`,
  (req, res) => {
    const { emailAddressId } = req.params;
    const email = router.db.get('email-addresses').removeById(emailAddressId);

    return res.status(200).json(email);
  }
);

server.get(
  `${apiPrefix}/groupClient/cases/:notificationId/outstanding-information`,
  (req, res) => {
    res.status(200).json(
      router.db
        .get('outstandingInformation')
        .filter(
          outstandingInfo =>
            outstandingInfo.uploadCaseNumber === req.params.notificationId
        )
        .value()
    );
  }
);

server.post(
  `${apiPrefix}/groupClient/cases/:notificationId/outstanding-information-received`,
  (req, res) => {
    res.status(200).json(
      router.db
        .get('outstandingInformation')
        .first()
        .value()
    );
  }
);

server.get(
  `${apiPrefix}/groupClient/cases/:notificationId/documents`,
  (req, res) => {
    res.status(200).json(
      router.db
        .get('documents')
        .filter(
          document =>
            document.caseId === req.params.notificationId ||
            (req.query.includeChildCases === 'true' &&
              document.rootCaseId === req.params.notificationId)
        )
        .value()
    );
  }
);

server.post(
  `${apiPrefix}/groupClient/cases/:notificationId/documents/base64Upload/:documentType`,
  (req, res) => {
    res.status(200).json(
      router.db
        .get('documents')
        .first()
        .value()
    );
  }
);

server.get(
  `${apiPrefix}/groupClient/cases/:notificationId/documents/:documentId/base64Download`,
  (req, res) => {
    res.status(200).json(router.db.get('base64Downloads').value());
  }
);

server.post(
  `${apiPrefix}/groupClient/cases/:notificationId/documents/:documentId/markread`,
  (req, res) => {
    const doc = router.db
      .get('documents')
      .find({ documentId: Number(req.params.documentId) })
      .value();

    if (doc) {
      res.status(200).json({ ...doc, isRead: true });
    } else {
      return res.status(404).json({});
    }
  }
);

server.get(`${apiPrefix}/groupClient/claims`, res => {
  const claimInfo = router.db.get('claims').value();
  if (claimInfo) {
    res.status(200).json(claimInfo);
  } else {
    return res.status(404).json({});
  }
});

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations`,
  (req, res) => {
    const elements = router.db.get('customerOccupations').value();

    if (elements) {
      res.status(200).json({ elements, totalSize: elements.length });
    } else {
      return res.status(404).json({ elements: [], totalSize: 0 });
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/edit`,
  (req, res) => {
    const { customerOccupationId } = req.params;
    const occupation = router.db
      .get('customerOccupations')
      .find(({ id }) => customerOccupationId === id)
      .assign(req.body)
      .write();

    return res.status(200).json(occupation);
  }
);

server.get(`${apiPrefix}/groupClient/claims/:claimId/payments`, (req, res) => {
  const paymentsInfo = router.db
    .get('payments')
    .find({ claimId: req.params.claimId })
    .value().response;
  if (paymentsInfo) {
    res.status(200).json(paymentsInfo);
  } else {
    return res.status(404).json({});
  }
});

server.get(
  `${apiPrefix}/groupClient/claims/:claimId/payments/:paymentId/paymentLines`,
  (req, res) => {
    const paymentLinesInfo = router.db
      .get('paymentLines')
      .find({ claimId: req.params.claimId, paymentId: req.params.paymentId })
      .value().response;
    if (paymentLinesInfo) {
      res.status(200).json(paymentLinesInfo);
    } else {
      return res.status(404).json({});
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/contractual-earnings`,
  (req, res) => {
    const elements = router.db.get('contractualEarnings').value();

    if (elements) {
      const toSend = [
        {
          ...elements[0],
          amount: {
            ...elements[0].amount,
            scale: 2
          }
        }
      ];
      res.status(200).json({ elements: toSend, totalSize: toSend.length });
    } else {
      return res.status(404).json({ elements: [], totalSize: 0 });
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/contractual-earnings`,
  (req, res) => {
    const contractualEarningsData = router.db.get('contractualEarnings');
    const dataToSave = {
      id: Math.random()
        .toString()
        .substring(2, 4),
      ...req.body
    };
    contractualEarningsData
      .assign([...contractualEarningsData, dataToSave])
      .write();

    res
      .status(200)
      .header('ETag', Math.random() * 100)
      .json({
        ...dataToSave,
        amount: {
          ...req.body.amount,
          scale: 2
        }
      });
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/contractual-earnings/:contractualEarningsId/edit`,
  (req, res) => {
    const { contractualEarningsId } = req.params;
    const contractualEarningsData = router.db.get('contractualEarnings');
    const selectedIndex = contractualEarningsData
      .findIndex(earning => earning.id === contractualEarningsId)
      .value();
    contractualEarningsData.remove({ id: contractualEarningsId }).write();

    if (selectedIndex > -1) {
      contractualEarningsData
        .assign([
          { id: contractualEarningsId, ...req.body },
          ...contractualEarningsData
        ])
        .write();

      res.status(200).json({
        ...req.body,
        id: contractualEarningsId,
        amount: {
          ...req.body.amount,
          scale: 2
        }
      });
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/communication-preferences`,
  (req, res) => {
    const elements = router.db.get('communicationPreferences').value();

    if (elements) {
      res.status(200).json({ elements, totalSize: elements.length });
    } else {
      return res.status(404).json({ elements: [], totalSize: 0 });
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/communication-preferences/:communicationPreferenceId`,
  (req, res) => {
    const communicationPreference = router.db
      .get('communicationPreferences')
      .find(({ id }) => id === req.params.communicationPreferenceId)
      .value();

    if (communicationPreference) {
      res.status(200).json(communicationPreference);
    } else {
      return res.status(404).json({});
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/communication-preferences/:communicationPreferenceId/link`,
  (req, res) => {
    const communicationPreference = router.db
      .get('communicationPreferences')
      .first()
      .value();

    const emailPreference = router.db
      .get('emailAddresses')
      .find(({ id }) => id === req.body.id)
      .value();
    const phonePreference = router.db
      .get('phoneNumbers')
      .find(({ id }) => id === req.body.id)
      .value();
    const responseBody = {
      ...communicationPreference,
      emailAddresses: req.body.resource === 'Email' ? [emailPreference] : [],
      phoneNumbers: req.body.resource === 'PhoneNumber' ? [phonePreference] : []
    };
    return res.status(200).json(responseBody);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/communication-preferences/:communicationPreferenceId/link`,
  (req, res) => {
    const communicationPreference = router.db
      .get('communicationPreferences')
      .find(({ id }) => id === req.params.communicationPreferenceId)
      .value();

    res.status(200).json(communicationPreference);
  }
);

server.get(`${apiPrefix}/groupClient/group-client-users`, (req, res) => {
  const elements = router.db.get('users').value();

  if (elements) {
    res.status(200).json({
      elements,
      totalSize: elements.length,
      meta: {
        etags: elements.reduce(
          (acc, curr) => ({ ...acc, [curr.id]: `${curr.id}-etag` }),
          {}
        )
      }
    });
  } else {
    return res
      .status(404)
      .json({ elements: [], totalSize: 0, meta: { etags: {} } });
  }
});

server.post(
  `${apiPrefix}/groupClient/group-client-users/:groupClientUserId/edit`,
  (req, res) => {
    const groupClientUser = router.db
      .get('users')
      .filter(({ id }) => id === req.params.groupClientUserId)
      .map(user => ({
        ...user,
        enabled: req.body.enabled
      }))
      .head()
      .value();

    if (groupClientUser) {
      res.status(200).json(groupClientUser);
    } else {
      return res.status(404).json({});
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/group-client-users/:groupClientUserId/edit`,
  (req, res) => {
    const groupClientUser = router.db
      .get('users')
      .filter(({ id }) => id === req.params.groupClientUserId)
      .map(user => ({
        ...user,
        enabled: req.body.enabled
      }))
      .head()
      .value();

    if (groupClientUser) {
      res.status(200).json(groupClientUser);
    } else {
      return res.status(404).json({});
    }
  }
);

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/absence-employment`,
  (req, res) => {
    const absenceEmploymentData = router.db.get('absenceEmployment').value();
    res.status(200).json(absenceEmploymentData);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/absence-employment`,
  (req, res) => {
    const absenceEmploymentData = router.db.get('absenceEmployment');
    const currentDate = new Date().toLocaleDateString();
    absenceEmploymentData
      .assign({
        ...req.body,
        id: currentDate,
        adjustedHireDate: currentDate
      })
      .write();

    res.status(200).json(req.body);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/absence-employment/edit`,
  (req, res) => {
    const absenceEmploymentData = router.db.get('absenceEmployment');
    const editedData = {
      ...absenceEmploymentData.value(),
      ...req.body
    };
    absenceEmploymentData.assign(editedData).write();

    res.status(200).json(editedData);
  }
);

server.get(
  `${apiPrefix}/enum-domains/:enumDomainId/enum-instances`,
  (req, res) => {
    const enumInstancesData = router.db
      .get('enumInstances')
      .filter(({ domainId }) => Number(req.params.enumDomainId) === domainId)
      .value();
    if (!enumInstancesData) {
      return res.status(404).json({});
    }

    return res.status(200).json({
      elements: enumInstancesData,
      totalSize: enumInstancesData.length
    });
  }
);

server.post(`${apiPrefix}/groupClient/eforms/:type`, (req, res) => {
  const eForm = router.db
    .get('eForms')
    .first()
    .value();

  if (!eForm) {
    return res.status(404).json({});
  }

  return res.status(200).json(eForm);
});

server.post(`${apiPrefix}/auth/logout`, (req, res) => {
  return res.status(200).json({});
});

server.get(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/regular-weekly-work-pattern`,
  (req, res) => {
    const workPattern = router.db.get('workPatternData').value();

    res.status(200).json(workPattern);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/regular-weekly-work-pattern`,
  (req, res) => {
    const workPatternDataData = router.db.get('workPatternData');
    const newData = {
      ...workPatternDataData.value(),
      ...req.body
    };

    return res.status(200).json(newData);
  }
);

server.get(`${apiPrefix}/groupClient/web-messages`, (req, res) => {
  const data = router.db.get('webMessages').value();
  const { query } = req;

  const filteredData = data.filter(el => {
    if (
      query.hasOwnProperty('readByGroupClient') &&
      query.readByGroupClient === 'false'
    ) {
      return !el.readByGroupClient;
    }
    if (query.hasOwnProperty('msgOriginatesFromPortal')) {
      if (query.msgOriginatesFromPortal === 'false') {
        return !el.msgOriginatesFromPortal;
      }
      if (query.msgOriginatesFromPortal === 'true') {
        return el.msgOriginatesFromPortal;
      }
    }
    return el;
  });

  if (query.hasOwnProperty('countOnly') && query.countOnly === 'true') {
    return res.status(200).json({
      totalSize: filteredData.length
    });
  }

  const sortedData = filteredData.sort((a, b) => {
    if (query.sort.startsWith('-')) {
      return moment(b.contactDateTime).diff(moment(a.contactDateTime));
    } else {
      return moment(a.contactDateTime).diff(moment(b.contactDateTime));
    }
  });

  const pageSize = +query.pageSize;
  const index = +query.pageIndex * pageSize;
  const limitedData = sortedData.slice(index, index + pageSize);
  const hasMoreElements = index + pageSize <= limitedData.length;

  res.status(200).json({
    elements: limitedData,
    totalSize: data.length,
    hasMoreElements
  });
});

server.get(
  `${apiPrefix}/groupClient/cases/:notificationId/web-messages`,
  (req, res) => {
    const data = router.db
      .get('webMessages')
      .filter(webMessage => webMessage.case.id === req.params.notificationId)
      .value();
    res.status(200).json({ elements: data, totalSize: data.length });
  }
);

server.post(
  `${apiPrefix}/groupClient/cases/:notificationId/web-messages/:webMessageId/edit`,
  (req, res) => {
    const messages = router.db.get('webMessages');
    const hasMessage = messages.some({ id: req.params.webMessageId }).value();
    if (hasMessage) {
      const newMessages = messages
        .value()
        .map(message =>
          req.params.webMessageId === message.id
            ? { ...message, ...req.body }
            : message
        );
      messages.assign(newMessages).write();
      res
        .status(200)
        .json(messages.find({ id: req.params.webMessageId }).value());
    } else {
      return res.status(404).json({});
    }
  }
);

server.post(
  `${apiPrefix}/groupClient/cases/:notificationId/web-messages`,
  (req, res) => {
    const messages = router.db.get('webMessages');
    const message = messages.first().value();
    const notification = router.db
      .get('notifications')
      .find(
        notificationTemp => notificationTemp.id === req.params.notificationId
      )
      .value();
    const { subject, narrative } = req.body;
    const newMessage = {
      ...message,
      id: String(Date.now()),
      subject,
      narrative,
      contactDateTime: moment().format('YYYY-MM-DDTHH:mm:ss'),
      readByGroupClient: true,
      msgOriginatesFromPortal: true,
      case: {
        id: req.params.notificationId,
        caseReference: req.params.notificationId,
        caseType: (notification.subCases[0] || {}).caseType
      },
      rootCase: {
        id: req.params.notificationId,
        caseReference: req.params.notificationId,
        caseType: (notification.subCases[0] || {}).caseType
      }
    };

    if (!newMessage) {
      return res.status(404).json({});
    }
    messages.assign([...messages, newMessage]).write();
    return res.status(200).json(newMessage);
  }
);

server.post(
  `${apiPrefix}/groupClient/customers/:customerId/customer-occupations/:customerOccupationId/regular-weekly-work-pattern/edit`,
  (req, res) => {
    const workPatternDataData = router.db.get('workPatternData');
    const editedData = {
      ...workPatternDataData.value(),
      ...req.body
    };

    return res.status(200).json(editedData);
  }
);

server.get(
  `${apiPrefix}/groupClient/absence/absences/:absenceId/actual-absence-periods`,
  (req, res) => {
    const actualAbsencePeriodsData = router.db.get('actualAbsencePeriods');
    return res.status(200).json({
      elements: actualAbsencePeriodsData,
      totalSize: actualAbsencePeriodsData.length
    });
  }
);

server.get(`${apiPrefix}/groupClient/absence/events`, (req, res) => {
  const lessEqualDateQueryParam = 'eventDate._le';
  const greaterEqualDateQueryParam = 'eventDate._ge';
  const { eventName, ...dateFilterQueries } = req.query;
  const absenceEventsData = router.db
    .get('absenceEvents')
    .filter(absenceEvent => absenceEvent.eventName === eventName)
    .map(absenceEvent => ({
      ...absenceEvent,
      eventDate: moment()
        .startOf('month')
        .add(Math.floor(Math.random() * 30), 'day')
        .format('YYYY-MM-DD')
    }))
    .value();
  let filteredData = absenceEventsData;

  if (Object.keys(dateFilterQueries).includes(lessEqualDateQueryParam)) {
    const newValue = filteredData.filter(data =>
      moment(data.eventDate).isBefore(
        moment(dateFilterQueries[lessEqualDateQueryParam])
      )
    );

    filteredData = newValue;
  }

  if (Object.keys(dateFilterQueries).includes(greaterEqualDateQueryParam)) {
    const newValue = filteredData.filter(data =>
      moment(data.eventDate).isAfter(
        moment(dateFilterQueries[greaterEqualDateQueryParam])
      )
    );

    filteredData = newValue;
  }

  res
    .status(200)
    .json({ elements: filteredData, totalSize: filteredData.length });
});

server.get(
  `${apiPrefix}/groupClient/absence/employees-approved-leaves`,
  (req, res) => {
    const employeesApprovedForLeavesData = router.db
      .get('employeesApprovedForLeaves')
      .value();

    res.status(200).json({
      elements: employeesApprovedForLeavesData,
      totalSize: employeesApprovedForLeavesData.length
    });
  }
);

server.use(
  jsonServer.rewriter({
    [`${apiPrefix}/groupClient/*`]: `${apiPrefix}/$1`
  })
);

// DEPLOYMENT OF SERVER
server.use(`${apiPrefix}`, router);
server.listen(port, () => {
  // tslint:disable-next-line
  console.log(`Mock API (json-server) is running on port ${port}`);
});
