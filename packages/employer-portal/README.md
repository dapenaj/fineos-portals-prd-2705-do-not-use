# Employer Portal

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Development](#development)
- [Production](#production)
- [Testing](#Testing)
- [Styling](#styling)
- [Deploy](#deploy)

## Prerequisites

This [React](https://reactjs.org) (16.13.1) project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Install [Node.js®](https://nodejs.org/en/download/) and [yarn](https://yarnpkg.com) if they are not already on your machine. Node version 12.13.1 was used for this application. see `.nvmrc` file.

## Installation

**NOTE:** this depends upon elements that are not published publicly - you will need to set up your `.npmrc` appropriately to use a local npm repo that has these components installed

e.g. for a local nexus this could be

```
registry=http://atlanta.sonalake.corp:8081/nexus/content/groups/npm-all/
```

**NOTE:** In sake of connecting to registry above, active VPN connection is required.

Stored either in `.npmrc` or `~/.npmrc`.

To begin process of installation:

```
yarn install
```

## Development

Project is provided with mock-api, which allows testing app behaviour with locally defined API data. To start mock-api, in `./fineos-portals/packages/employer-portal` run:

```
yarn mock-api
```

or to enable auto-reload:

```
yarn mock-api:watch
```

The project has a language file compilation from two files. If you make any changes to [language].json while the application is running, you must run the script in `./fineos-portals/packages/employer-portal`:

```
yarn build:language
```

or to enable auto-reload:

```
yarn build:language:watch
```

A static maintenance page is available inside the `employer-portal/maintenance` directory. As part of the build process, this directory is compressed into a zip file. This zip file is then placed inside the `build` directory. There is also a separate command to perform this action outside of the build process:

```
yarn build:maintenance-page
```

Employer portal has dependencies on `fineos-common` (`./fineos-portals/packages/fineos-common`), which contains common components and functions used in the project. In case of any changes made inside `fineos-common`, changes will be visible only after rebuilding all `fineos-common` packages:

in `./fineos-portals/packages/fineos-common` run:

```
yarn build:lib
```

Employer portal has dependencies on `fineos-js-api-client` which provides all api-communication services and send/received data formatters.

To start the project, in `./fineos-portals/packages/employer-portal` run:

```
yarn start
```

This will open a development server on http://localhost:3000/

After commiting any changes, dev version of `employer-portal` and/or `fineos-common` should be increased.

in `fineos-portals` run:

```
yarn bump-version
```

**NOTE:** Script above executes also `git commit` command

## Production

in `./fineos-portals/packages/employer-portal` run:

```
yarn build
```

This will generate a `./build` folder in `./fineos-portals/packages/employer-portal` root directory with following structure:

```
/build
  /assets
    /img
  /config
    client-config.json
    env.json
    portal.env.json
  /static
  asset-manifest.json
  error.html
  index.html
  loader.js
  maintenance.html
  robots.txt
```

For real production build, loader should be built and injected into HTML page, using following command

```
yarn postbuild
```

This will generate a `portal.zip` file inside `./fineos-portals/packages/employer-portal/output` that contains the contents of the `build` directory.
A `maintenance.zip` file will also be generated containing the contents of `./fineos-portals/packages/employer-portal/maintenance`.

## Testing

[Jest](https://jestjs.io/) and [react-testing-library](https://testing-library.com) are used for Unit testing and [Protractor](https://www.protractortest.org/#/) is used for e2e testing.

### Unit testing

For `employer-portal` in `./fineos-portals/packages/employer-portal` run:

```
yarn test
```

For `fineos-common` in `./fineos-portals/packages/fineos-common` run:

```
yarn test
```

Run unit tests with coverage table in terminal and report located in the `./coverage` directory:

```
yarn test:coverage
```

To check if application build proceeds without errors and all tests pass, in `./fineos-portals` run:

```
yarn test
```

this command will execute `yarn build:lib` and `yarn test` in both directories.

### E2E testing

E2E tests are triggered during executing scripts from `build.gradle` file as task `run_e2e_tests`

## Styling

For this application we use SCSS as our styling language.

For styling common components or provide common styles we use `./fineos-portals/packages/fineos-common/src/lib/styles` directory. It is broken down to following folders:

- `/functions` - common functions, i.e. background generator or rem from pixels
- `/mixins` - common mixins i.e. shadow or font mixins
- `/overrides`, - overrides for default styles
- `/variables` - constant values i.e. default spacing

## Deploy

[Jenkins](https://www.jenkins.io) pipeline is used for application deploy. To change a deploy process edit `Jenkinsfile`, to change a source config-file, define proper file path in jenkins project config, in `Script Path` field.

`Employer Portal` pipeline has following stages:

- `Checkout`
- `Clean`
- `Publish fineos-common`
- `Increment fineos-common`
- `Publish employer-portal`
- `Increment employer-portal`

It is possible to execute build with parameters, you can define following parameters in Jenkins UI:

- `buildBranch` - what branch to build
- `incrementFineosCommonDevVersion` - if should fineos-common dev version be incremented
- `incrementEmployerPortalDevVersion` - if should the employer-portal dev version be incremented

During running stages below, tasks defined in `build.gradle` are executed by running in `Jenkinsfile` command `./gradlew <task defined in build.gradle file>`.

`build.gradle` is as build configuration script, where build tasks are defined. For `employer-portal` there are following tasks defined:

- `test_employer_portal` - runs unit tests for project
- `build_employer_portal`- runs build script for project
- `publish_employer_portal` - publishes project
- `version_patch_employer_portal` - increments dev version

**NOTE:** There is also `build-fineos.gradle` file.

- `build.gradle` - defines tasks for deploying project using dev Jenkins
- `fineos-build.gradle` - defines tasks for deploying project using `Fineos` Jenkins
