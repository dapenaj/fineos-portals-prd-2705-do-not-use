const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');

const BUMP_VERSION_COMMIT = 'Bump version';

const BRANCH_PARAM = '--branch';

const branch = (
  process.argv.find(arg => arg.startsWith(BRANCH_PARAM + '=')) ||
  BRANCH_PARAM + '=develop'
).substr(BRANCH_PARAM.length + 1);
const shouldPerformPush = process.argv.includes('--push');

const sharedPackages = ['fineos-common'];

const targetPackages = ['employer-portal'];

const PACKAGE_JSON = 'package.json';

const sharedVersions = new Map();
const targetVersions = new Map();

function bumpBuildVersion() {
  function readPackageJson(packagePath) {
    return fs.readFileSync(path.resolve(packagePath, PACKAGE_JSON), 'utf8');
  }

  function writePackageJson(packagePath, content) {
    return fs.writeFileSync(
      path.resolve(packagePath, PACKAGE_JSON),
      content,
      'utf8'
    );
  }

  function bumpVersion(version) {
    const [, major, minor, patch, custom] = /^(\d+)\.(\d+)\.(\d+)-(\d+)$/.exec(
      version
    );
    return `${major}.${minor}.${patch}-${parseInt(custom, 10) + 1}`;
  }

  function updatePackageJSONProp(packageJsonContent, propertyName, value) {
    return packageJsonContent.replace(
      new RegExp(`("${propertyName}":\\s+")[^"]+"`, 'g'),
      `$1${value}"`
    );
  }

  for (const sharedPackage of sharedPackages) {
    const packagePath = path.resolve(
      __dirname,
      '..',
      'packages',
      sharedPackage
    );
    const packageJsonContent = readPackageJson(packagePath);
    const packageJson = JSON.parse(readPackageJson(packagePath));
    const newVersion = bumpVersion(packageJson.version);

    sharedVersions.set(sharedPackage, newVersion);

    writePackageJson(
      packagePath,
      updatePackageJSONProp(packageJsonContent, 'version', newVersion)
    );
  }

  for (const targetPackage of targetPackages) {
    const packagePath = path.resolve(
      __dirname,
      '..',
      'packages',
      targetPackage
    );
    const packageJsonContent = readPackageJson(packagePath);
    const packageJson = JSON.parse(readPackageJson(packagePath));
    const newVersion = bumpVersion(packageJson.version);

    targetVersions.set(targetPackage, newVersion);

    let updatedPackageJson = updatePackageJSONProp(
      packageJsonContent,
      'version',
      newVersion
    );

    for (const [sharedPackage, sharedVersion] of sharedVersions) {
      updatedPackageJson = updatePackageJSONProp(
        updatedPackageJson,
        sharedPackage,
        sharedVersion
      );
    }

    writePackageJson(packagePath, updatedPackageJson);
  }
}

function getPackageJsonPaths() {
  return sharedPackages
    .concat(targetPackages)
    .map(packageName => path.join('packages', packageName, PACKAGE_JSON));
}

function getPackagesVersions() {
  return Array.from(sharedVersions)
    .concat(Array.from(targetVersions))
    .map(([packageName, version]) => `${packageName}: ${version}`);
}

function parseCommit(commitMessage) {
  const hashPos = (commitMessage || '').indexOf('#');

  return hashPos > -1
    ? {
        date: new Date(commitMessage.substr(0, hashPos)),
        name: commitMessage.substr(hashPos + 1)
      }
    : null;
}

function execCmd(cmd) {
  console.log(cmd);
  const output = execSync(cmd, { encoding: 'utf8' });
  console.log(output);
  return output;
}

const last100Commits = execCmd(`git log -100 --format=%ai#%s`).split('\n');

const lastCommit = parseCommit(last100Commits[0]);

const lastBumpCommit = parseCommit(
  last100Commits.find(line => line.includes(BUMP_VERSION_COMMIT))
);

function isAfter(date, otherDay) {
  return (
    date.getUTCFullYear() > otherDay.getUTCFullYear() ||
    date.getUTCMonth() > otherDay.getUTCMonth() ||
    date.getUTCDate() > otherDay.getUTCDate()
  );
}

if (!lastBumpCommit) {
  throw new Error(
    'No bump commit found in last 100 commits, please update version manually'
  );
  return;
}

if (isAfter(lastCommit.date, lastBumpCommit.date)) {
  if (execCmd(`git diff --staged`)) {
    throw new Error(
      'Version should be updated, but there are other staged files, cannot proceed'
    );
    return;
  }

  if (shouldPerformPush) {
    execCmd(`git checkout ${branch}`);
    execCmd(`git pull --rebase`);
  }

  bumpBuildVersion();

  execCmd(`git add ${getPackageJsonPaths().join(' ')}`);
  execCmd(`git commit -n -m "${BUMP_VERSION_COMMIT} (${getPackagesVersions().join('; ')})"`);

  if (shouldPerformPush) {
    execCmd(`git push origin ${branch}`);
  }
} else {
  // It is causing build error, needs to be resolved
  // const message = `No need to bump the version; lastCommit is on ${lastCommit.date.toISOString()} and lastBumpCommit in ${lastBumpCommit.date.toISOString()}`
  const message = 'No need to bump the version';
  console.log(message);
}
